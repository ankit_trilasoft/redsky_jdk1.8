<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<head>
<%-- <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRGCK2dYb6E9n4B1E2585HyXOYBiBTkm8DZjmBAmvbTx4LAPz_RRfRfSg" type="text/javascript"></script>--%>
	<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
		<title><fmt:message key="partnerDetailAgent.title"/></title>   
    	<meta name="heading" content="<fmt:message key='partnerDetailAgent.heading'/>"/> 
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
		<title><fmt:message key="partnerDetail.title"/></title>   
    	<meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/> 
    </sec-auth:authComponent>
     <link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<style>
<%@ include file="/common/calenderStyle.css"%>
.textareaUpper {
    background-color: #E3F1FE;
    border: 1px solid #ABADB3;
    color: #000000;
    font-family: arial,verdana;
    font-size: 12px;   
    text-decoration: none;
}
div.wrapper-img{ width:570px;}
div.thumbline
  {
  margin:2px;
  border:none;
  float:left;
  text-align:center;
 
  background:transparent url(images/agent_prf_bg.png) no-repeat scroll 0 0;
  height:152px;
  padding:20px 20px 15px;
  width:238px;
  }
div.thumbline img
  {
  display:inline;
  
  margin:3px 40px 0px 0px;
  border:1px solid #ffffff;
  }
.ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
  
  div.upload-preview{ padding:5px 10px; background:#fff; border:1px solid #0099cc; position:relative;line-height:20px;
  			  color:#0099cc; border-radius:2px; text-align:left;  cursor:pointer; font-size:12px; font-weight:bold; font-family:arial,helvetica,serif; }
div.upload-preview i{font-size:15px;vertical-align:tex-top;}
.hide_file { position: absolute; z-index: 1000; opacity: 0; cursor: pointer; right: 0; top: 0; height: 100%; font-size: 24px; width: 100%;}
.preview {border: 5px solid #F5F5F5; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.255); cursor:default;}
</style>
<style>

	   .image-editor {
    	margin-left: 20px;
	   }

      .cropit-preview {
      	display:none;
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 200px;
        height: 200px;
      }


      .cropit-preview-image-container {
        cursor: move;
      }

      .image-size-label {
        margin-top: 10px;
        display: none;
      }
      .custom-file-upload {
        border: 1px solid #bfdbff;
        border-radius: 4px;
        padding: 6px 12px;
      }
      
    </style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	 
     <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
     <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	
   <!-- Modification closed here -->
   <%-- <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/croppie.css'/>" /> --%>
   <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/demo.css'/>" />
    <%-- <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/croppie.js"></script> --%>
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.cropit.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/demo.js"></script>
     <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
<style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}


</style>
<script>
      
     
      
</script>	

<script language="javascript" type="text/javascript">
 <c:if test="${partnerType =='AG'}">


function ShowCropitDetails() {
<c:if test="${!(partnerPublic.companyLogo == null || partnerPublic.companyLogo == '')}">
  document.getElementById('companyLogoShowHide').style.display = 'none';
  </c:if>
  document.getElementById('cropit-previewShowHide').style.display = 'block';
  document.getElementById('image-size-labelShowHide').style.display = 'block';
  document.getElementById('cropit-image-zoom-inputShowHide').style.display = 'block'; 
}



 var loadFile = function(event) {
    var output = document.getElementById('companyLogo1');
    companyLogo1.src = URL.createObjectURL(event.target.files[0]);
  };


 function Upload(event) { 
    var fileUpload = document.getElementById("fileLogo"); 
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result; 
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width; 
                    if (height > 400 || width > 400) {
                      alert("Height and Width must not exceed 400px."); 
                       document.forms['partnerPublicForm'].elements['fileLogoUploadFlag'].value="NO";
                      return false; 
                    }else{ 
                    loadFile(event)
                    return true;
                    }
                };
 
            }
        } else {
            alert("This browser does not support HTML5.");
            return false;
        }
    } else {
        alert("Please select a valid Image file.");
        return false;
    }
}
</c:if>
<sec-auth:authComponent componentId="module.script.partnerPublicForm.AgentScript">

window.onload = function() { 
	var elementsLen=document.forms['partnerPublicForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['partnerPublicForm'].elements[i].type=='text'){
				document.forms['partnerPublicForm'].elements[i].readOnly =false;
				document.forms['partnerPublicForm'].elements[i].className = 'input-text';					
		}else if(document.forms['partnerPublicForm'].elements[i].type=='textarea'){
			document.forms['partnerPublicForm'].elements[i].readOnly =false;
			document.forms['partnerPublicForm'].elements[i].className = 'textarea';
		}else{					
				document.forms['partnerPublicForm'].elements[i].disabled=false;
		}
	 }
	document.forms['partnerPublicForm'].elements['partnerPublic.partnerCode'].readOnly =true;
	document.forms['partnerPublicForm'].elements['partnerPublic.partnerCode'].className = 'input-textUpper';	
	autoPopulate_partner_billingCountry(document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry']);
	autoPopulate_partner_terminalCountry(document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry']);
	autoPopulate_partner_mailingCountry(document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry']);
}

</sec-auth:authComponent>

function findTruckNumber() {
 	  openWindow('findTruckNumber.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=partnerPrivate.truckID&fld_seventhDescription=seventhDescription',1024,500);
 	  
 }
 function findDriverFromPartner(){
 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=partnerPrivate.driverID');
 }
 function findPayToFromPartner(){
 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=partnerPrivate.payTo');
 }
 
 function truckTrailerPopup(){
 openWindow('truckTrailerPopup.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=partnerPrivate.trailerID');
 }
 
function valueEvalutionPercentage()
{
var commission = eval(document.forms['partnerPublicForm'].elements['partnerPublic.vendorCommission'].value);
if(commission>0 && commission<=100)
{
}else if(typeof(commission)=='undefined'){
	document.forms['partnerPublicForm'].elements['partnerPublic.vendorCommission'].value=0.0;
}else{
alert('Commission should not be greater than 100%');
document.forms['partnerPublicForm'].elements['partnerPublic.vendorCommission'].value=0.0;
}
}

function validateEmergencyEmail(email) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var address = document.forms['partnerPublicForm'].elements['partnerPrivate.emergencyEmail'].value;
   if(reg.test(address) == false) {
      alert('Please enter valid Email.');
      document.forms['partnerPublicForm'].elements['partnerPrivate.emergencyEmail'].value='';
      return false;
   }
}

/*
if('${partnerType}' != 'PP'){
	<configByCorp:fieldVisibility componentId="component.button.partnverPublicScript">
	
		window.onload = function() { 
			trap();
			var elementsLen=document.forms['partnerPublicForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++)
				{
					if(document.forms['partnerPublicForm'].elements[i].type=='text')
						{
							document.forms['partnerPublicForm'].elements[i].readOnly =true;
							document.forms['partnerPublicForm'].elements[i].className = 'input-textUpper';
							
						}
						else
						{
							
							document.forms['partnerPublicForm'].elements[i].disabled=true;
						}
			   }
	 	}
		
 	</configByCorp:fieldVisibility>
} 
 function trap() 
		  {
		  if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('calender.png')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0)
						{ 
							
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
							    document.images[i].src = 'images/navarrow.gif';
						}
						
					
					}
		    }
		  }
function test()
		{
			return false;
		}
		
function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		}
*/
function getHTTPObject() {
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
var http = getHTTPObject();

function regExMatch(element,evt){
	//alert(element.readOnly);
	if(!element.readOnly){
		var key='';
		if (window.event)
			 key = window.event.keyCode;
		if (evt)
			  key = evt.which;
		var alphaExp = /^[a-zA-Z0-9-_\.'&'\\'&'\/'&'\"'&'\+'&'\,'&'\''\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\='\' ']+$/;
		if(element.value.match(alphaExp)){
			return true;
		}
		else if ((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27)||(key==20)||(key==16)||(key==17)||(key==32)||(key==18)||(key==27)||(key==35)||(key==36)||(key==45)||(key==39)||(key==37)||(key==38)||(key==40)||(key==116)){
			   return true;
		}
		else{
			//close non english validation because INTM want to add german language.. // 18-Dec-13
			//alert('Only English Alphabets are allowed.');
			//element.value = '';//element.value.substring(0,element.value.length-2);
			return true;
		}
	}else{
		return false;
	}
}
</script>

<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

animatedcollapse.addDiv('pics', 'fade=1,hide=1')
animatedcollapse.addDiv('description', 'fade=1,hide=1')
animatedcollapse.addDiv('info', 'fade=1,hide=1')
animatedcollapse.addDiv('accinfo', 'fade=1,hide=1')
animatedcollapse.addDiv('vninfo', 'fade=1,hide=1')
animatedcollapse.addDiv('driver', 'fade=1,hide=1')
animatedcollapse.addDiv('comData', 'fade=1,hide=1')
animatedcollapse.addDiv('partnerportal', 'fade=0,hide=1')
animatedcollapse.init()

</script>
<script language="javascript" type="text/javascript">
function onLoad() {
	var f = document.getElementById('partnerPublicForm'); 
	f.setAttribute("autocomplete", "off"); 
}
</script>
<script type="text/javascript"> 
function copyBillToState(){
var abc=document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value=abc;
}
function copyMailToState(){
var abc=document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value;
document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value=abc;
}
function copyTerToState(){
var abc=document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value=abc;
}
function copyMailToTerToState(){
var abc=document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value;
document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value=abc;
}
		function copyBillToMail(){ 
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress1'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress1'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress2'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress2'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress2'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress2'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress3'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress3'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress3'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress3'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress4'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress4'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.billingEmail'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.billingEmail'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingEmail'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingEmail'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingCity'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingCity'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.billingFax'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.billingFax'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingFax'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingFax'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingZip'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingZip'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.billingPhone'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.billingPhone'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingPhone'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingPhone'].value;		
			//document.forms['partnerPublicForm'].elements['partnerPublic.mailingTelex'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingTelex'].value;	
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountryCode'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountryCode'].value
			var oriCountry = document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){		
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")		    				
				getMailingState(document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry']);
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = false;
				setTimeout("copyBillToState()",1000);
				}else{
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = true;
			}
			  	changeAddressPin();
		}
		function copyTermToMail(){ 
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress1'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress1'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress2'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress2'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress2'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress2'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress3'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress3'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress3'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress3'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress4'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress4'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingEmail'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingCity'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalCity'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.terminalFax'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalFax'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingFax'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalFax'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingZip'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalZip'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
			if(document.forms['partnerPublicForm'].elements['partnerPublic.terminalPhone'].value==undefined){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalPhone'].value="";
			}
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingPhone'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalPhone'].value;		
			//document.forms['partnerPublicForm'].elements['partnerPublic.mailingTelex'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalTelex'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value=document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value;		
			 document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountryCode'].value = document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountryCode'].value;

			var oriCountry = document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){			
				try{
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[billingStateIndex].text;
				}catch(e){}
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")		    	
		    	getMailingState(document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry']);
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = false;
				setTimeout("copyMailToState()",1000);
			}else{
				try{
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[billingStateIndex].text;
				}catch(e){}
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = true;
			}
			  	changeAddressPin();	
			
		}
		function copyBillToTerm(){ 
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress1'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress1'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress2'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress2'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress3'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress3'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress4'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress4'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingEmail'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCity'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingCity'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalFax'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingFax'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalZip'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingZip'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalPhone'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingPhone'].value;		
			//document.forms['partnerPublicForm'].elements['partnerPublic.terminalTelex'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingTelex'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountryCode'].value=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountryCode'].value
			var oriCountry = document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	getTerminalState(document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry']);
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = false;
				setTimeout("copyTerToState()",1000);
			}else{
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = true;
			
			}
		}
		function copyMailToTerm(){ 
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress1'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress1'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress2'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress2'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress3'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress3'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalAddress4'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingAddress4'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingEmail'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCity'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCity'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalFax'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingFax'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalZip'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingZip'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalPhone'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingPhone'].value;		
			//document.forms['partnerPublicForm'].elements['partnerPublic.terminalTelex'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingTelex'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value;		
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountryCode'].value=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountryCode'].value

			var oriCountry = document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){	
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	getTerminalState(document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry']);
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = false;
				setTimeout("copyMailToTerToState()",1000);
			}else{
				var billingStateIndex = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].selectedIndex;
				var billingStateText = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = true;
			}
		}
	</script>
<script type="text/javascript">
	function autoPopulate_partner_billingCountry1(targetElement) {
		
			var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerPublicForm'].elements['partnerPublic.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partnerPublic.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
		}
		function autoPopulate_partner_terminalCountry1(targetElement) {
		
			var terminalCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountryCode'].value=terminalCountryCode.substring(0,terminalCountryCode.indexOf(":")-1);
			targetElement.form.elements['partnerPublic.terminalCountry'].value=terminalCountryCode.substring(terminalCountryCode.indexOf(":")+2,terminalCountryCode.length);
		}
		function autoPopulate_partner_mailingCountry1(targetElement) {
			
			var mailingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountryCode'].value=mailingCountryCode.substring(0,mailingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partnerPublic.mailingCountry'].value=mailingCountryCode.substring(mailingCountryCode.indexOf(":")+2,mailingCountryCode.length);
	   }
 
	function autoPopulate_partner_billingInstruction1(targetElement) {
		
	}
	</script>
<script language="JavaScript">
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>
<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		//document.forms['partnerPublicForm'].elements[targetElelemnt].focus();
		//targetElement.focus = true;
		return false;
	}
	return true;
 }
</script>
<script type="text/javascript">
function requiredState(){
    var originCountry = document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value; 
	var r2=document.getElementById('redHiddenFalse');
	var r1=document.getElementById('redHidden');
	if(originCountry == 'United States' || originCountry == 'Canada' || originCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(originCountry)> -1);
		if(index != ''){
		r1.style.display = 'block';
		r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}

function autoPopulate_partner_billingCountry(targetElement) {	
   	var oriCountry = targetElement.value;
    var enbState = '${enbState}';
	var index = (enbState.indexOf(oriCountry)> -1);
	if(index != ''){
		document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = false;		
		if('${partnerType}' == 'OO'){
			document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].disabled = false;
		}
	}else{
		document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = true;		
		document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value = '';
		if('${partnerType}' == 'OO'){
			document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].value = '';
		}
	}
		
}
function autoPopulate_partner_terminalCountry(targetElement) {   
		var dCountry = targetElement.value;
		 var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != ''){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = false;		
		}else{
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value = '';
		}
	}	
	
function autoPopulate_partner_mailingCountry(targetElement) {   
	var dCountry = targetElement.value;
	var enbState = '${enbState}';
	var index = (enbState.indexOf(dCountry)> -1);
	if(index != ''){
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = false;
		}else{
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value = '';
		}
	}


var map;

function getBillingCountryCode(targetElement){
	var countryName=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseBillingCountry;
    http4.send(null);
}

function handleHttpResponseBillingCountry(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1){
 					document.forms['partnerPublicForm'].elements['partnerPublic.billingCountryCode'].value = results;
 					//document.forms['partnerPublicForm'].elements['partnerPublic.billingCountryCode'].select();
					 } else{                     
                 }
             }
        }
         
 function getMailingCountryCode(targetElement){
	var countryName=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponseMailingCountry;
    http5.send(null);
}

function handleHttpResponseMailingCountry(){
             if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                if(results.length>=1){
 					document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountryCode'].value = results;
 					//document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountryCode'].select();
 					 }else{                     
                 }
             }
        }
         
function getTerminalCountryCode(){
	var countryName=document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponseTerminalCountry;
    http6.send(null);
}

function handleHttpResponseTerminalCountry(){
             if (http6.readyState == 4){
                var results = http6.responseText
                results = results.trim();
                if(results.length>=1){
 					document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountryCode'].value = results;
 					//document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountryCode'].select();
					 } else{                     
                 }
             }
        }
        
function checkVendorName(){
	document.forms['partnerPublicForm'].elements['partnerPublic.agentParentName'].value=""; 
    var vendorId = document.forms['partnerPublicForm'].elements['partnerPublic.agentParent'].value;
    if(vendorId==''){
    	document.forms['partnerPublicForm'].elements['partnerPublic.agentParentName'].value=""; 
    }else{
    	document.forms['partnerPublicForm'].elements['parent'].value="found";
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http22.open("GET", url, true);
     http22.onreadystatechange = handleHttpResponsePA;
     http22.send(null);
    }
}
function handleHttpResponsePA(){
      if (http22.readyState == 4){
    	  		var partnerType = document.forms['partnerPublicForm'].elements['partnerType'].value;
                var results = http22.responseText
                results = results.trim();
                var res = results.split("#");
                document.forms['partnerPublicForm'].elements['parent'].value="";
                if(res.length>2){
                	if(res[2] == 'Approved'){
	           			document.forms['partnerPublicForm'].elements['partnerPublic.agentParentName'].value = res[1];
	           			
                	}else{
	           			if(partnerType ==  'AC'){
                			alert("Parent Account code is not approved" ); 
                		}else{
                			alert("Agent Parent code is not approved" );
                		}
					    document.forms['partnerPublicForm'].elements['partnerPublic.agentParent'].value="";
					    document.forms['partnerPublicForm'].elements['partnerPublic.agentParentName'].value="";
					    document.forms['partnerPublicForm'].elements['partnerPublic.agentParent'].select();
					    showAddressImageSub2();
					   // showOrHide(0);
	           		}  
               	}else{
                     if(partnerType ==  'AC'){
                			alert("Parent Account code is not valid" ); 
                		}else{
                			alert("Agent Parent code is not valid" );
                		}
                 	 document.forms['partnerPublicForm'].elements['partnerPublic.agentParent'].value="";
                 	 document.forms['partnerPublicForm'].elements['partnerPublic.agentParentName'].value="";
                 	 document.forms['partnerPublicForm'].elements['partnerPublic.agentParent'].select();
                 	 showAddressImageSub2();
                 	// showOrHide(0);
			   }
      }
}

</script>
<SCRIPT LANGUAGE="JavaScript">
function autoPopulate_partner_billingCountry1(targetElement) {
	       var red=document.getElementById("redHidden");
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = false;
			red.style.display='block';
		}else{
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = true;
			red.style.display='none';
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value = '';
		}
	}
	function autoPopulate_partner_terminalCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = false;
		}else{
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value = '';
		}
	}
	function autoPopulate_partner_mailingCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = false;
		}else{
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value = '';
		}
	}
</script>
<SCRIPT LANGUAGE="JavaScript">
function getBillingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
function getMailingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
function getTerminalState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}
	
</script>
<SCRIPT LANGUAGE="JavaScript">	

function getBillingState(targetElement) {
	var country = document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
	
function getMailingState(targetElement) {
	//#7148 -To add the status drop down for private party partner
	//var country = targetElement.value;
	var country=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
	countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
	
function getTerminalState(targetElement) {
	//#7148 -To add the status drop down for private party partner
	//var country = targetElement.value;
	var country=document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
	countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}	

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}


function handleHttpResponse2(){
	if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].text = '';
				document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].text = stateVal[1];
				document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].value = stateVal[0];				
				document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value='${partnerPublic.billingState}';
				}
		}			
		if('${partnerType}' == 'OO'){
			var targetElementLic = document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'];
			targetElementLic.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].options[i].text = '';
					document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].options[i].text = stateVal[1];
					document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].options[i].value = stateVal[0];
					document.forms['partnerPublicForm'].elements['partnerPrivate.licenseState'].value='${partnerPrivate.licenseState}';
					}
			}
		}
	}
}  
        
function handleHttpResponse3(){

             if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].text = '';
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].value = stateVal[0];
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value='${partnerPublic.mailingState}';
					}
					}
             }
        }  

function handleHttpResponse4(){

             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					try{
					if(res[i] == ''){
					document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].text = '';
					document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].text = stateVal[1];
					document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].value = stateVal[0];
					document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value='${partnerPublic.terminalState}';
					}
					}catch(e){}
					}
             }
        }  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http2 = getHTTPObject();
var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    var http5 = getHTTPObject1()
    var http6 = getHTTPObject1()
function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();    
    var updateBankRecordHttp = getHTTPObject2();    
</script>
<script>
function changeStatus(){
	document.forms['partnerPublicForm'].elements['formStatus'].value = '1';
}
function validateForm(){
	try{
	
		<c:if test="${partnerType =='AG'}">	
		var elementsLen=document.forms['partnerPublicForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
				document.forms['partnerPublicForm'].elements[i].disabled =false;
			}
		</c:if>
		document.forms['partnerPublicForm'].elements['partnerPrivateHire'].value = document.forms['partnerPublicForm'].elements['partnerPrivate.hire'].value;
		document.forms['partnerPublicForm'].elements['partnerPrivateTermination'].value = document.forms['partnerPublicForm'].elements['partnerPrivate.termination'].value;
	}catch(e){}
	document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = false;
	document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =false;
	if(document.forms['partnerPublicForm'].elements['partnerPublic.isPrivateParty'].type=='checkbox'){
	var pvtP= document.forms['partnerPublicForm'].elements['partnerPublic.isPrivateParty'].checked ;
	}else{
	var pvtP= document.forms['partnerPublicForm'].elements['partnerPublic.isPrivateParty'].value ;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.isAccount'].type=='checkbox'){
	var isAccount= document.forms['partnerPublicForm'].elements['partnerPublic.isAccount'].checked ;
	}else{
	var isAccount= document.forms['partnerPublicForm'].elements['partnerPublic.isAccount'].value ;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.isAgent'].type=='checkbox'){
	var isAgent= document.forms['partnerPublicForm'].elements['partnerPublic.isAgent'].checked ;
	}else{
	var isAgent= document.forms['partnerPublicForm'].elements['partnerPublic.isAgent'].value ;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.isVendor'].type=='checkbox'){
	var isvendor= document.forms['partnerPublicForm'].elements['partnerPublic.isVendor'].checked ;
	}else{
	var isvendor= document.forms['partnerPublicForm'].elements['partnerPublic.isVendor'].value ;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.isCarrier'].type=='checkbox'){
	var isCarrier= document.forms['partnerPublicForm'].elements['partnerPublic.isCarrier'].checked ;
	}else{
	var isCarrier= document.forms['partnerPublicForm'].elements['partnerPublic.isCarrier'].value ;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.isOwnerOp'].type=='checkbox'){
	var isOwnerOp= document.forms['partnerPublicForm'].elements['partnerPublic.isOwnerOp'].checked ;
	}else{
	var isOwnerOp= document.forms['partnerPublicForm'].elements['partnerPublic.isOwnerOp'].value ;
	}
	//alert(pvtP + "********" +isAccount+"********"+isAgent+"*********"+isvendor+"********"+isCarrier+"********"+isOwnerOp);
if('${partnerType}' == 'AG' || '${partnerType}' == 'AC'){
	<sec-auth:authComponent componentId="component.standard.accountPotralActivation">
	document.forms['partnerPublicForm'].elements['partnerPublic.partnerPortalActive'].disabled = false;
	</sec-auth:authComponent>
}
	if('${partnerType}' != 'PP' && '${chkAgentTrue}'!='Y' && '${partnerType}' != 'AG'){
		if(!(pvtP || isAccount || isvendor || isCarrier || isOwnerOp)){
			alert("Please select an appropriate partner type.");
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;\\#7112- Issue2 mention on this ticket.   
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			return false;
		}
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.lastName'].value.trim() == ''){
			alert('Please enter the last name');
			document.forms['partnerPublicForm'].elements['partnerPublic.lastName'].focus();
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			return false;
	}
	/* var comDivision = document.forms['partnerPublicForm'].elements['partnerPublic.corpID'].value;
	if(comDivision == 'SSCW'){
		if('${partnerType}' == 'PP' || '${partnerType}' == 'AC'){
			if(document.forms['partnerPublicForm'].elements['partnerPrivate.companyDivision'].value == ''){
				alert('Please select the company division');
				//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
				document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
				document.forms['partnerPublicForm'].elements['partnerPrivate.companyDivision'].focus();
				return false;
			}
		}
		if('${partnerType}' == 'OO'){
			
		}
	} */
	
	if(document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress1'].value.trim() == ''){
			alert('Please enter the billing address');
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			document.forms['partnerPublicForm'].elements['partnerPublic.billingAddress1'].focus();
			return false;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value == ''){
			alert('Please select the billing country');
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].focus();
			return false;
	}
	var billingCountry = document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
		if(billingCountry == 'United States' || billingCountry == 'Canada' || billingCountry == 'India' ){
	    	var enbState = '${enbState}';
			var index = (enbState.indexOf(billingCountry)> -1);
			if(index != ''){
		if(document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value == ''){
			alert('Please select the billing State.');
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].focus();
			return false;
		}
		}
	}
	if('${partnerType}' == 'AG' || '${partnerType}' == 'VN' || '${partnerType}' == 'CR'){
	var terminalCountry = document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
		if(terminalCountry == 'United States'){
			if(document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value == ''){
				alert('Please select the terminal state.');
				//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
				document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].focus();
				return false;
			}
		}
	}
	var mailingCountry = document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
	if(mailingCountry == 'United States'){
		if(document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value == ''){
			alert('Please select the mailing state.');
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].focus();
			return false;
		}
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.billingCity'].value.trim() == ''){
			alert('Please select the billing city');
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			document.forms['partnerPublicForm'].elements['partnerPublic.billingCity'].focus();
			return false;
	}
	if(document.forms['partnerPublicForm'].elements['partnerPublic.billingPhone'].value.trim() == ''){
			alert('Please select the billing phone');
			//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
			document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
			document.forms['partnerPublicForm'].elements['partnerPublic.billingPhone'].focus();
			return false;
	}
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	try{ 
	if(document.forms['partnerPublicForm'].elements['partnerPrivate.paymentMethod'].value==''){
		alert('Please select the Payment Method');
		document.forms['partnerPublicForm'].elements['partnerPrivate.paymentMethod'].focus();
		return false;
		
	}
	}catch(e){}
	try{ 
	if(document.forms['partnerPublicForm'].elements['partnerPrivate.creditTerms'].value==''){
			alert('Please select the Credit Terms');
			document.forms['partnerPublicForm'].elements['partnerPrivate.creditTerms'].focus();
			
			return false;
			
		}
	}catch(e){}
	</configByCorp:fieldVisibility>	
	if('${partnerType}' == 'CR'){
	var checkBoxSea = document.forms['partnerPublicForm'].elements['partnerPublic.sea'].checked;
	var checkBoxSurface = document.forms['partnerPublicForm'].elements['partnerPublic.surface'].checked;
	var checkBoxAir = document.forms['partnerPublicForm'].elements['partnerPublic.air'].checked;	
	if(checkBoxSea==false && checkBoxSurface==false && checkBoxAir==false){
	alert('Please Select the Carrier Detail As Sea, Surface or Air.');
	//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
	document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
	return false;
	}
	}
	<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
	var partnerType=document.forms['partnerPublicForm'].elements['partnerType'].value
	 if( document.forms['partnerPublicForm'].elements['partnerPublic.bankAccountNumber'].value==''&& partnerType=='AG'){
		alert('Please enter the Bank Account Number');
		//document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
		document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
		return false;
	}
	</configByCorp:fieldVisibility>
	getAgentCountryCode();
	<c:if test="${flagHit == '3'}">
	alert("sdxfgvhbjk")
	</c:if>
	
	var returnChk=document.forms['partnerPublicForm'].elements['chkVatValidation'].value;
		if(returnChk=='Y'){
		document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = false;
		return checkLength();
	}else{
		document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
		document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled =true;
		return false;
	}
		
	
}




function removePhotograph1(){
	document.forms['partnerPublicForm'].elements['partnerPublic.location1'].value = '';
	document.getElementById("userImage1").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph2(){
	document.forms['partnerPublicForm'].elements['partnerPublic.location2'].value = '';
	document.getElementById("userImage2").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph3(){
	document.forms['partnerPublicForm'].elements['partnerPublic.location3'].value = '';
	document.getElementById("userImage3").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph4(){
	document.forms['partnerPublicForm'].elements['partnerPublic.location4'].value = '';
	document.getElementById("userImage4").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
	
function isURL(){
	var urlStr=document.forms['partnerPublicForm'].elements['partnerPublic.url'].value;
	alert(urlStr);
	if (urlStr.indexOf(" ") != -1) {
		alert("Spaces are not allowed in a URL");
		document.forms['partnerPublicForm'].elements['partnerPublic.url'].value="";
		return false;
	}

	if (urlStr.value == "" || urlStr.value == null) {
		return true;
	}

  	urlStr = urlStr.value.toLowerCase();

	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var atom=validChars + '+';
	var urlPat=/^http:\/\/(\w*)\.([\-\+a-z0-9]*)\.(\w*)/;
	var matchArray=urlStr.match(urlPat);

	if(matchArray == null){
		alert("The URL seems incorrect check it begins with http:/\/ and it has 2 .'s");
		document.forms['partnerPublicForm'].elements['partnerPublic.url'].value="";
		return false;
	}
	return true;
}
	
	

function openTestPage(){
	//isURL();
	var url=document.forms['partnerPublicForm'].elements['partnerPublic.url'].value;
	//window.open(url);
	window.open().location.href='http://'+url;

}

function  checkLength(){
	var  partnerType=document.forms['partnerPublicForm'].elements['partnerType'].value;
 	if(partnerType=='AG')
	{
	var txt = document.forms['partnerPublicForm'].elements['partnerPublic.companyProfile'].value;
	var txt1 = document.forms['partnerPublicForm'].elements['partnerPublic.companyFacilities'].value;
	var txt2 = document.forms['partnerPublicForm'].elements['partnerPublic.companyCapabilities'].value;
	var txt3 = document.forms['partnerPublicForm'].elements['partnerPublic.companyDestiantionProfile'].value;
	if(txt.length >1500){
		alert('You can not enter more than 1500 characters in companyProfile');
		document.forms['partnerPublicForm'].elements['partnerPublic.companyProfile'].value = txt.substring(0,1498);
		return false;
	}
	if(txt1.length >1500){
		alert('You can not enter more than 1500 characters in companyFacilities');
		document.forms['partnerPublicForm'].elements['partnerPublic.companyFacilities'].value = txt1.substring(0,1498);
		return false;
	}
	if(txt2.length >1500){
		alert('You can not enter more than 1500 characters in companyCapabilities');
		document.forms['partnerPublicForm'].elements['partnerPublic.companyCapabilities'].value = txt2.substring(0,1498);
		return false;
	}
	if(txt3.length >1500){
		alert('You can not enter more than 1500 characters in companyDestiantionProfile');
		document.forms['partnerPublicForm'].elements['partnerPublic.companyDestiantionProfile'].value = txt3.substring(0,1498);
		return false;
	}
	}
}

function pinLocation(){
	  var $j = jQuery.noConflict();
	  
  	    $j('#mapModal').modal({show:true,backdrop: 'static',keyboard: false});
  	    
        var lat =  parseFloat(document.forms['partnerPublicForm'].elements['partnerPublic.latitude'].value);    
        var lng =  parseFloat(document.forms['partnerPublicForm'].elements['partnerPublic.longitude'].value);
        var uluru = {lat: lat, lng: lng};
        
        var lastName=document.getElementById('lastName').value;
        var geocoder = new google.maps.Geocoder();
    	 var map = new google.maps.Map(
         document.getElementById('map2'), {zoom: 16, center: uluru});
    	 var marker = new google.maps.Marker({position: uluru, map: map, title: lastName,draggable: true});
    	 var contentString ='<h5>'+lastName+'</h5>'+
         '<div id="bodyContent">'+getMailingAdress()+
         '</div>'+
         '</div>';

    	google.maps.event.addListener(marker, 'dragend', function(e) {
 
    	document.getElementById('lat').value = e.latLng.lat().toFixed(4) ; 
    	document.getElementById('lng').value = e.latLng.lng().toFixed(4);
    	
    	var latlng = {lat:parseFloat(document.getElementById('lat').value), lng: parseFloat(document.getElementById('lng').value)};
      	 geocoder.geocode({'location': latlng}, function(results, status) {
      	
       
      		marker.addListener('click', function() {
      			title: lastName,
                infowindow.open(map, marker);
              });
      		infowindow.setContent('<h5>'+lastName+'</h5>'+
      		         '<div id="bodyContent">Pin Address:'+results[0].formatted_address+
      		         '</div>'+
      		         '</div>');
      	 });     	
      	 
});
    	marker.addListener('click', function() {
           infowindow.open(map, marker);
         });
    	
    	 var infowindow = new google.maps.InfoWindow({
           content: contentString
         });      
}

function convertToMiles(){
	var km=document.forms['partnerPublicForm'].elements['partnerPublic.serviceRangeKms'].value;
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['partnerPublicForm'].elements['partnerPublic.serviceRangeMiles'].value=rounded;
}
function convertToKm(){
	var miles=document.forms['partnerPublicForm'].elements['partnerPublic.serviceRangeMiles'].value;
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerPublicForm'].elements['partnerPublic.serviceRangeKms'].value=rounded;
}

function convertToFeet(){
	var meters=document.forms['partnerPublicForm'].elements['partnerPublic.facilitySizeSQMT'].value;
	var feet=10.77*meters;
	var rounded=Math.round(feet);
	document.forms['partnerPublicForm'].elements['partnerPublic.facilitySizeSQFT'].value=rounded;
}

function convertToMeters(){
	var feet=document.forms['partnerPublicForm'].elements['partnerPublic.facilitySizeSQFT'].value;
	var meters=0.093*feet;
	var rounded=Math.round(meters);
	document.forms['partnerPublicForm'].elements['partnerPublic.facilitySizeSQMT'].value=rounded;
}



function openWebSite(){
var  www="www.";
var	http= "http://";
var theUrl = document.forms['partnerPublicForm'].elements['partnerPublic.url'].value;
	if(theUrl.substring(0,4)==www)
	{
		theUrl=http+theUrl;
	}
if(theUrl.match(/^(http|ftp)\:\/\/\w+([\.\-]\w+)*\.\w{2,4}(\:\d+)*([\/\.\-\?\&\%\#]\w+)*\/?$/i)){
   if(theUrl.substring(7,11)==www)
    {
	   	window.open(theUrl);
	}
	else{
		alert("Please enter a valid URL");
	  	theUrl.select();
    	theUrl.focus();
     	document.forms['partnerPublicForm'].elements['partnerPublic.url'].value='';
	  	return false;
		}
}
else
{
	alert("Please enter a valid URL");
    theUrl.select();
    theUrl.focus();
     document.forms['partnerPublicForm'].elements['partnerPublic.url'].value='';
    return false;
}
}

function findVanLineCommission(){
openWindow('findVanLineCommission.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=partnerPrivate.commission',1024,500);
}
</script>

<script language="JavaScript">	
</script>


<style type="text/css">
.myClass{ height:150px; width:auto}

.urClass{ height:auto; width:150px; }
input[type=checkbox]{vertical-align:middle;}
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none;} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>
<script language="javascript">
function secImage1(){


if(document.getElementById("userImage1").height > 150)
{
 document.getElementById("userImage1").style.height = "120px";
 document.getElementById("userImage1").style.width = "auto";
}
}
function secImage2(){

if(document.getElementById("userImage2").height > 150 )
{
 document.getElementById("userImage2").style.height = "120px";
 document.getElementById("userImage2").style.width = "auto";
}        

}
function secImage3(){
if(document.getElementById("userImage3").height > 150)
{
 document.getElementById("userImage3").style.height = "120px";
 document.getElementById("userImage3").style.width = "auto";
} 
}
function secImage4(){
if(document.getElementById("userImage4").height > 150)
{
 document.getElementById("userImage4").style.height = "120px";
 document.getElementById("userImage4").style.width = "auto";
} 
}

function addOperatorOwner(){
	var partentId = document.forms['partnerPublicForm'].elements['partnerPublic.partnerCode'].value;	
	window.open('editOwnerOperatorDetails.html?decorator=popup&popup=true&bucket=Partner&type=Driver&parentId='+partentId,'sqlExtractInputForm','height=300,width=750, scrollbars=yes,resizable=yes').focus();
	
} 
function addOperatorCarrier(){
	var partentId = document.forms['partnerPublicForm'].elements['partnerPublic.partnerCode'].value;	
	window.open('editOwnerOperatorDetails.html?decorator=popup&popup=true&bucket=Vendor&type=Tractor&parentId='+partentId,'sqlExtractInputForm','height=300,width=750, scrollbars=yes,resizable=yes').focus();
	
} 
function addOperatorVendor(){
	var partentId = document.forms['partnerPublicForm'].elements['partnerPublic.partnerCode'].value;	
	window.open('editOwnerOperatorDetails.html?decorator=popup&popup=true&bucket=Vendor&type=Tractor&parentId='+partentId,'sqlExtractInputForm','height=300,width=750, scrollbars=yes,resizable=yes').focus();	
} 

function openNotesPopupTab(targetElement){
	openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',755,500);
}
function checkVendorType(){
var vendorType = document.forms['partnerPublicForm'].elements['partnerPublic.typeOfVendor'].value;
if(vendorType=='Agent Broker'){
   document.getElementById("progressiveTypeId").style.display="block";
  }else {
  document.getElementById("progressiveTypeId").style.display="none";
  }
}
function checkDate(){
//alert("hi");
//showOrHide(1);
	var parent=document.forms['partnerPublicForm'].elements['parent'].value;
	var parentdescription=document.forms['partnerPublicForm'].elements['partnerPublic.agentParentName'].value;

	if((parent=="found")&&(parentdescription.trim()=="")){
		return false;
	}	
	try{
		
 var date1 = document.forms['partnerPublicForm'].elements['hire'].value;	
 var date2 = document.forms['partnerPublicForm'].elements['termination'].value;
 var mySplitResult = date1.split("-");
 var day = mySplitResult[0];
 var month = mySplitResult[1];
 var year = mySplitResult[2];
if(month == 'Jan')
 {
     month = "01";
 }
 else if(month == 'Feb')
 {
     month = "02";
 }
 else if(month == 'Mar')
 {
     month = "03"
 }
 else if(month == 'Apr')
 {
     month = "04"
 }
 else if(month == 'May')
 {
     month = "05"
 }
 else if(month == 'Jun')
 {
     month = "06"
 }
 else if(month == 'Jul')
 {
     month = "07"
 }
 else if(month == 'Aug')
 {
     month = "08"
 }
 else if(month == 'Sep')
 {
     month = "09"
 }
 else if(month == 'Oct')
 {
     month = "10"
 }
 else if(month == 'Nov')
 {
     month = "11"
 }
 else if(month == 'Dec')
 {
     month = "12";
 }
 var finalDate = day+"-"+month+"-"+year;
 var mySplitResult2 = date2.split("-");
 var day2 = mySplitResult2[0];
 var month2 = mySplitResult2[1];
 var year2 = mySplitResult2[2];
 if(month2 == 'Jan')
 {
     month2 = "01";
 }
 else if(month2 == 'Feb')
 {
     month2 = "02";
 }
 else if(month2 == 'Mar')
 {
     month2 = "03"
 }
 else if(month2 == 'Apr')
 {
     month2 = "04"
 }
 else if(month2 == 'May')
 {
     month2 = "05"
 }
 else if(month2 == 'Jun')
 {
     month2 = "06"
 }
 else if(month2 == 'Jul')
 {
     month2 = "07"
 }
 else if(month2 == 'Aug')
 {
     month2 = "08"
 }
 else if(month2 == 'Sep')
 {
     month2 = "09"
 }
 else if(month2 == 'Oct')
 {
     month2 = "10"
 }
 else if(month2 == 'Nov')
 {
     month2 = "11"
 }
 else if(month2 == 'Dec')
 {
     month2 = "12";
 }
 /*if(date1 == null || date1 == ''){
	alert("Please Select Hire Date.");
	document.forms['partnerPublicForm'].elements['termination'].value='';
	return false;
 }*/
var finalDate2 = day2+"-"+month2+"-"+year2;
date1 = finalDate.split("-");
date2 = finalDate2.split("-");
var sDate = Calendar.parseDate(finalDate,false);
var eDate = Calendar.parseDate(finalDate2,false);
var daysApart = 0;
if((eDate != null && eDate != undefined))
	daysApart = Math.round((eDate-sDate)/86400000);

if(daysApart<1)
{
	if(document.forms['partnerPublicForm'].elements['termination'].value != ''){
 		 alert("Termination Date should be greater than Hire Date.");
  			document.forms['partnerPublicForm'].elements['termination'].value='';
  			return false;
	}
} 
	}catch(e){}

return true;
}

function notExists(){
	alert("The Account information has not been saved yet, please save account information to continue");
	
}
 function chkDmmCmm(){
	var val = document.forms['partnerPublicForm'].elements['partnerPublic.partnerType'].value
	var orgVal='${partnerPublic.partnerType}';
	if((orgVal=='CMM' || orgVal=='DMM') && (val=='' || val=='NAA' || val=='KGA') ){
		alert("You cannot change partner type value to blank or KGA.");
		document.forms['partnerPublicForm'].elements['partnerPublic.partnerType'].value=orgVal;
		return false;
	}
 }
 function resetBillingState(target) {
	 	var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	    //alert(url);
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse12;
	     http2.send(null);
		
		}
 function handleHttpResponse12(){
		if (http2.readyState == 4){
	        var results = http2.responseText
	        results = results.trim();
	        res = results.split("@");
	        targetElement = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].text = '';
					document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					//document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex=true;
					document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].text = stateVal[1];
					document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].options[i].value = stateVal[0];
					document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value='${partnerPublic.billingState}';
				}
			}	
			
		}
	}  
 function resetmailingState(target) {
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	    //alert(url);
	     http4.open("GET", url, true);
	     http4.onreadystatechange = handleHttpResponse13;
	     http4.send(null);
		
		}
function handleHttpResponse13(){
		if (http4.readyState == 4){
	        var results = http4.responseText
	        results = results.trim();
	        res = results.split("@");
	        targetElement = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].text = '';
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					//document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex=true;
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].options[i].value = stateVal[0];
					document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value='${partnerPublic.mailingState}';
				}
			}	
			
		}
	}
function resetterminalState(target) {
	var country = target;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http14.open("GET", url, true);
     http14.onreadystatechange = handleHttpResponse144;
     http14.send(null);
	
	}
function handleHttpResponse144(){
	if (http14.readyState == 4){
        var results = http14.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].text = '';
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				//document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex=true;
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].text = stateVal[1];
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].options[i].value = stateVal[0];
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value='${partnerPublic.terminalState}';
			}
		}	
		
	}
}
var http14 = getHTTPObject();
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 function reset_partner_billingCountry() {  
	   	var oriCountry ='${partnerPublic.billingCountry}';
	   	var red=document.getElementById("redHidden");
	   	var enbState = '${enbState}';
		var index = (enbState.indexOf(oriCountry)> -1);
		if(index != '' && oriCountry != ''){
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = false;
			/* if(enbState != ''){
				document.getElementById("redHiddenFalse").style.display = 'none';
				red.style.display='block';
			} */
			resetBillingState(oriCountry);
		}else{
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = true;
			red.style.display='none';
			document.getElementById("redHiddenFalse").style.display='block';
			document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value = '';
			
		}
			
	}
 function reset_partner_mailingCountry() {   
		var dCountry ='${partnerPublic.mailingCountry}';
		var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != '' && dCountry != ''){
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = false;
				resetmailingState(dCountry);	
			}else{
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = true;
				document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value = '';
			}
		}
 function reset_partner_terminalCountry() {   
		var dCountry ='${partnerPublic.terminalCountry}';
		var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != ''){
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = false;
				resetterminalState(dCountry);	
			}else{
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = true;
				document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].value = '';
			}
		}
 function disabledAll(){
		var elementsLen=document.forms['partnerPublicForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['partnerPublicForm'].elements[i].type=='text'){
		     <c:if test="${partnerType !='AG'}">
				document.forms['partnerPublicForm'].elements[i].readOnly =true;
			    document.forms['partnerPublicForm'].elements[i].className = 'input-textUpper';
			</c:if>
			<c:if test="${partnerType =='AG'}">
			   document.forms['partnerPublicForm'].elements[i].readOnly =false;
			   document.forms['partnerPublicForm'].elements[i].className = 'input-text';
			</c:if>
			}else if(document.forms['partnerPublicForm'].elements[i].type=='textarea'){
			<c:if test="${partnerType !='AG'}">
				document.forms['partnerPublicForm'].elements[i].readOnly =true;
				document.forms['partnerPublicForm'].elements[i].className = 'textareaUpper';
			</c:if>
			<c:if test="${partnerType =='AG'}">
			   document.forms['partnerPublicForm'].elements[i].readOnly =false;
			    document.forms['partnerPublicForm'].elements[i].className = 'textarea';
		    </c:if>
			}else{
				<c:if test="${partnerType !='AG'}">
				document.forms['partnerPublicForm'].elements[i].disabled=true;
			    </c:if>
			<c:if test="${partnerType =='AG'}">
				document.forms['partnerPublicForm'].elements['button.save'].disabled=false;
			    document.forms['partnerPublicForm'].elements['button.add'].disabled=true;
			    document.forms['partnerPublicForm'].elements['Reset'].disabled=true;
			</c:if>
			} 
		}
	}

 function getAgentCountryCode(){ 	
 var countryCode = document.forms['partnerPublicForm'].elements['agentCountryCode'].value;
 var vatNumberTotal = document.forms['partnerPublicForm'].elements['partnerPublic.vatNumber'].value;
 var chk = document.forms['partnerPublicForm'].elements['vatAllowCountry'].value;
 var vatNumber =vatNumberTotal.substring(0,2)
 var len  = vatNumberTotal.length;
 if(chk=="Yes"){
 if(vatNumberTotal!=''){	 
 if(countryCode!=vatNumber){
 	var type='VAT# does not match with Country Code.';
 	var agree = confirm(type+"\n"+"Click Ok to continue,"+"\n"+" or Click Cancel. ");
 	if(agree){	
 	 	document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
 		return true;
 	 }else{
 		document.forms['partnerPublicForm'].elements['chkVatValidation'].value="N";	
 		 return false;
 		
 	  }
 }
 <configByCorp:fieldVisibility componentId="component.field.partner.vatValidationAllow">
 if(countryCode==vatNumber){
 	if(len>2){
 	progressBarAutoSave('1');
 	var url="validVatCode.html?ajax=1&decorator=simple&popup=true&vatNumber="+encodeURI(vatNumberTotal)+"&vatCountryCode="+encodeURI(countryCode);
 	httpVat.open("GET", url, true);
     httpVat.onreadystatechange = handleHttpResponseVatValidation;
     httpVat.send(null);
 	}else{
 		var type='VAT# code have only Country Code.';
 		var agree = confirm(type+"\n"+"Click Ok to continue,"+"\n"+" or Click Cancel. ");
 		if(agree){
 	 		document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
 	 		return true;
 		 }else{
 			document.forms['partnerPublicForm'].elements['chkVatValidation'].value="N";
 	 		return false;	
 		  }
 	}
 }
 </configByCorp:fieldVisibility >
 
 }else{
 	var type='VAT# code is blank.';
 	var agree = confirm(type+"\n"+"Click Ok to continue,"+"\n"+" or Click Cancel. ");
 	if(agree){
 		document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
 		return true;
 	 }else{
 		document.forms['partnerPublicForm'].elements['chkVatValidation'].value="N";
 	    return false;	
 	  }
 }
 }else{
	 document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
		return true; 
 }
 <configByCorp:fieldVisibility componentId="component.field.partner.vatValidationIgnore">
 document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
 return true;
 </configByCorp:fieldVisibility >  
 }

 function handleHttpResponseVatValidation(){
 	if (httpVat.readyState == 4) {
 	 	var results = httpVat.responseText;
 		results = results.trim(); 
        var res = results.split('^'); 
        if(res[0]=='N'){
            var type=res[1];
 		   var agree = confirm(""+type+"\n"+"Click Ok to continue,"+"\n"+" or Click Cancel. ");
 			if(agree){
 	 			document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
 				return true;
 			}else{
 			document.forms['partnerPublicForm'].elements['chkVatValidation'].value="N";
 			   return false;	
 			}
     	 }else{
     		document.forms['partnerPublicForm'].elements['chkVatValidation'].value="Y";
         	return true;
 		}
 	}
      progressBarAutoSave('0');
 }


 function getHTTPObject()
 {
     var xmlhttp;
     if(window.XMLHttpRequest)
     {
         xmlhttp = new XMLHttpRequest();
     }
     else if (window.ActiveXObject)
     {
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         if (!xmlhttp)
         {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
         }
     }
     return xmlhttp;
 }
 var httpVat = getHTTPObject();
 var httpCC = getHTTPObject();

 function getVatCountryCode(country){
	 	var country = country.value;
	 	country=country.trim();
	 	var flag = "0"; 
		<c:forEach var="entry" items="${countryWithBranch}">
		var countryName="${entry.key}";	
		countryName=countryName.trim();
		var vatFlag="${entry.value}";
		vatFlag=vatFlag.trim();
		var vatArr=vatFlag.split("#");
		if((country==countryName) && (flag !="1")){
    		document.forms['partnerPublicForm'].elements['agentCountryCode'].value=vatArr[1] ;
    		document.forms['partnerPublicForm'].elements['vatAllowCountry'].value=vatArr[0] ;
    		flag ="1"
		}
		</c:forEach>
 }
 function enableStateListBill(){ 
		var billCon=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(billCon)> -1);
		  if(index != ''){
		  		document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = false;
		  }else{
			  document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].disabled = true;
		  }	        
	}
 function enableStateListTerm(){ 
		var termCon=document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(termCon)> -1);
		  if(index != ''){
		  		document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = false;
		  }else{
			  document.forms['partnerPublicForm'].elements['partnerPublic.terminalState'].disabled = true;
		  }	        
	}
 function enableStateListMail(){ 
		var mailCon=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(mailCon)> -1);
		  if(index != ''){
		  		document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = false;
		  }else{
			  document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].disabled = true;
		  }	        
	}
 function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");	
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=PUB&excludeFPU=TRUE&partnerCode=${partnerPublic.partnerCode}";
		  http12.open("GET", url, true); 
		  http12.onreadystatechange = handleHttpResponse223444; 
	      http12.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=PUB&excludeFPU=FALSE&partnerCode=${partnerPublic.partnerCode}";
		  http12.open("GET", url, true); 
	      http12.onreadystatechange = handleHttpResponse223444; 
	      http12.send(null); 
	 }
}
 function handleHttpResponse223444(){
	   if (http12.readyState == 4)
	   {
	   			  var results = http12.responseText
		               results = results.trim();					               
					if(results=="")	{
					
					}
					else{
					
					} 
	   } 
	}
	var http12 = getHTTPObject();
	function getHTTPObject()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	function openWindowMethod()
	{
		window.open("updatePartnerPublicSection.html?id=${partnerPrivate.id}&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,width=350,height=250");
	}
	function openDefaultChildAccount(){ 
	checkVendorName();
	<c:if test="${not empty partnerPublic.id && partnerType =='AC'}">
	var agentParent=document.forms['partnerPublicForm'].elements['partnerPublic.agentParent'].value;
	window.open("updateDefaultChildAccount.html?id=${partnerPrivate.id}&agentParent="+agentParent+"&decorator=popup&popup=true","mywindow","menubar=0,scrollbars=1,resizable=1,width=750,height=550");
	</c:if>
	}
	function autoCompleterAjaxBillingCity(){
		var countryName = document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
		var stateNameOri = document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].value;
		var cityNameOri = document.forms['partnerPublicForm'].elements['partnerPublic.billingCity'].value;
		var countryNameOri = "";
		<c:forEach var="entry" items="${countryCod}">
		if(countryName=="${entry.value}"){
			countryNameOri="${entry.key}";
		}
		</c:forEach>
		if(cityNameOri!=''){
			var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
			$("#billingCity").autocomplete({				 
			      source: data		      
			    });
		}
	}
	function autoCompleterAjaxMailingCity(){
		var countryName = document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
		var stateNameDes = document.forms['partnerPublicForm'].elements['partnerPublic.mailingState'].value;
		var cityNameDes = document.forms['partnerPublicForm'].elements['partnerPublic.mailingCity'].value;		
		var countryNameDes = "";
		<c:forEach var="entry" items="${countryCod}">
		if(countryName=="${entry.value}"){
			countryNameDes="${entry.key}";
		}
		</c:forEach>
		if(cityNameDes!=''){
		var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
		$( "#mailingCity" ).autocomplete({				 
		      source: data		      
		    });
		}
	}
	function GetSelectedItem(target) {
		var customerType=target;
		if(customerType=='Just Say Yes'){
		  	document.forms['partnerPublicForm'].elements['partnerPrivate.customerFeedback'].value='Just Say Yes';	  
	   }
		else if(customerType=='Quality Survey'){		
		  	document.forms['partnerPublicForm'].elements['partnerPrivate.customerFeedback'].value='Quality Survey';	
	   }
	}
</script>
<script>
      $(function() {
     
      
      var options = {
      type: 'image/png',
      originalSize: 'false',
      smallImage: 'stretch', 
      minZoom: 'fit'
      }
      
       $('.image-editor').cropit(options);
        $("#partnerPublicForm").submit(function() {  
          // Move cropped image data to hidden input
          var imageData = $('.image-editor').cropit('export'); 
          $('.hidden-image-data').val(imageData);

        });
      });
            	
      
    </script>


</head>
<c:if test="${partnerType == 'AG' || partnerType == 'AC'  || partnerType == 'PP' || partnerType == 'CR' || partnerType == 'VN' || partnerType == 'OO'}">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partnerPublic.id}" />
<c:set var="fileID" value="%{partnerPublic.id}"/>
<c:set var="PPID" value="%{partnerPublic.id}"/>
<s:hidden name="PPID" id="PPID" value="%{partnerPublic.id}" />
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>
<s:hidden name="noteFor" id="noteFor" value="Partner"></s:hidden>
</c:if>
<div class="modal fade" id="mapModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width:500px;>
			 
				    <div class="modal-body" style="font-weight:600;">
				    <div class="modal-header header-bdr">
        <button type="button" class="close" style="text-align:right !important;" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="line-height:1.32 !important">Location Finder</h4>
      </div>
					<div id="map2" style="text-align:center;width: 500px; height: 400px"></div>
					<s:hidden  id="lat" name="lat"/>
					<s:hidden id="lng" name="lng" />
				
			      <div class="modal-footer">
				        <button type="button" id="btnSave" class="btn btn-danger" data-dismiss="modal" onclick="return saveNewPinLocation();">OK</button>
				   </div>
			    </div>
				    
			</div>
</div>

<s:form id="partnerPublicForm" name="partnerPublicForm" enctype="multipart/form-data" onsubmit="return checkDate();" action="savePartnerPublic.html" method="post" validate="true">
<c:set var="checkCompensationYear" value="N" />
<c:if test="${partnerType != 'AC'}">
<c:set var="checkCompensationYear" value="Y" />	
</c:if>
<s:hidden name="checkCompensationYear" value="${checkCompensationYear}"/>
<s:hidden name="checkCodefromDefaultAccountLine" /> 
 <s:hidden name="newlineid" id="newlineid" value=" "/>
	<s:hidden name="chkAgentTrue" />
	<s:hidden name="parent" />
	<s:hidden name="chkVatValidation" />
	<s:hidden name="agentCountryCode"/>
	<s:hidden name="vatAllowCountry"/>
	<s:hidden name="partnerPublic.terminalTelex"/>	
	<s:hidden name="partnerPublic.billingTelex"/>	
	<s:hidden name="partnerPublic.mailingTelex"/>
	<s:hidden name="description" />
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="partnerPublic.isPartnerExtract" />
<s:hidden name="partnerPublic.sentToAccounting" />
<configByCorp:fieldVisibility componentId="component.field.SO.Extarct">
	<s:hidden name="isDecaExtract" value="1"/>
	</configByCorp:fieldVisibility>
	<s:hidden name="partnerPublic.id" />
	<s:hidden name="partnerPublic.corpID" />
	<s:hidden name="partnerPrivate.corpID" /> 
	<s:hidden name="partnerPublic.isAccountParty" />
	<s:hidden name="partnerPublic.billingCountryCode" />
	<s:hidden name="partnerPublic.mailingCountryCode" />
	<s:hidden name="partnerPublic.terminalCountryCode" />
	<c:if test="${partnerType != 'PP'}">
	<s:hidden name="partnerPrivate.collection" />
	</c:if> 
	<c:if test="${partnerType != 'PP' && partnerType != 'OO' && partnerType != 'CR'}">
	<s:hidden name="partnerPrivate.vatBillingGroup" />
	</c:if>
	<s:hidden name="partnerPrivate.converted" />	
	<s:hidden name="partnerPrivate.localName" />
	<s:hidden name="partnerPrivate.customerFeedback" />
	<s:hidden name="partnerPublic.location1" />
	<s:hidden name="partnerPublic.location2" />
	<s:hidden name="partnerPublic.location3" />
	<s:hidden name="partnerPublic.location4" />
	<s:hidden name="partnerPublic.companyLogo" />

<s:hidden name="id" />	
<s:hidden name="parentId" value="%{partnerPublic.partnerCode}"/>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<c:set var="idOfWhom" value="${partnerPublic.id}" scope="session"/>
<c:set var="noteID" value="${partnerPublic.partnerCode}" scope="session"/>
<c:set var="idOfTasks" value="" scope="session"/>
<c:set var="noteFor" value="Partner" scope="session"/>
<c:if test="${empty partnerPublic.id }">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty partnerPublic.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
  <c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>
				
				<c:url value="accountPortalPartnerFiles.html" var="urlforFileCabinet">
                <c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
                <c:param name="partnerType" value="AC"/>
                <c:param name="id" value="${partnerPublic.id}"/>
                <c:param name="lastName" value="${partnerPublic.lastName}"/>
                <c:param name="status" value="${partnerPublic.status}"/>
                </c:url>
				
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	<s:hidden name="partnerPublic.vanLastLocation" />
	<s:hidden name="partnerPublic.nextVanLocation" />
	<s:hidden name="partnerPublic.vanLastReportTime" />
	<s:hidden name="partnerPublic.vanAvailCube" />
	<s:hidden name="partnerPublic.currentVanAgency" />
	<s:hidden name="partnerPublic.currentVanID" />
	<s:hidden name="partnerPublic.currentTractorAgency" />
	<s:hidden name="partnerPublic.currentTractorID" />	
	<s:hidden id="partnerNotes" name="partnerNotes" value="<%=request.getParameter("partnerNotes") %>"/>
		<s:hidden name="pCode" value="%{partnerPublic.partnerCode}"/>
		<c:set var="pCode" value="${partnerPublic.partnerCode}" />
	<%
    String value=(String)pageContext.getAttribute("pCode") ;
    Cookie cookie = new Cookie("TempnotesId",value);
    cookie.setMaxAge(3600);
    response.addCookie(cookie);
    %>
	<c:set var="partnerNotes" value="<%=request.getParameter("partnerNotes") %>" />
		<c:if test="${not empty partnerPublic.vanLastReportOn}">
		 <s:text id="customerFileVanLastReportOnFormattedValue" name="${FormDateValue}"> <s:param name="value" value="partnerPublic.vanLastReportOn" /></s:text>
			 <s:hidden name="partnerPublic.vanLastReportOn" value="%{customerFileVanLastReportOnFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty partnerPublic.vanLastReportOn}">
		 <s:hidden name="partnerPublic.vanLastReportOn"/> 
	 </c:if>
	 <c:if test="${not empty partnerPublic.nextReportOn}">
		 <s:text id="customerFileNextReportOnFormattedValue" name="${FormDateValue}"> <s:param name="value" value="partnerPublic.nextReportOn" /></s:text>
			 <s:hidden name="partnerPublic.nextReportOn" value="%{customerFileNextReportOnFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty partnerPublic.nextReportOn}">
		 <s:hidden name="partnerPublic.nextReportOn"/> 
	 </c:if>
	<s:hidden name="popupval" value="${papam.popup}" />
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		<ul>
						<c:if test="${usertype!='ACCOUNT'}">
			<sec-auth:authComponent componentId="module.tab.partner.cmmDMMTabVisibility">
				<c:if test="${partnerPublic.partnerType == 'CMM' or partnerPublic.partnerType == 'DMM'}">
					<c:if test="${partnerType == 'AC'}">
						<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Detail</span></a></li>
						<c:if test="${not empty partnerPublic.id}">
							<c:if test="${checkTransfereeInfopackage==true}">
								<li><a href="editContractPolicy.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
							</c:if>
						</c:if>
					</c:if>
				</c:if>
			</sec-auth:authComponent>
			
			
			<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				<c:if test="${partnerType == 'AC'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'VN'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Vendor Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'PP'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Private Party Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'CR'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Carrier Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'OO'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Owner Ops</span></a></li>
				</c:if>
				
				<c:if test="${not empty partnerPublic.id}">
					<configByCorp:fieldVisibility componentId="component.button.partnverPublicScript">
						<c:if test="${partnerType == 'AC'}">
							<li><a href="editNewAccountProfile.html?id=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
						</c:if>
						<c:if test="${partnerType == 'AG'}">
							<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
						</c:if>
						<c:if test="${sessionCorpID!='TSFT' }">
						<c:if test="${partnerType == 'AC' || partnerType == 'AG' || partnerType == 'VN'}">
							<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
						</c:if>
						</c:if>
						<configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
						<c:if test="${partnerType == 'AC'}">
							<li><a href="editProfileInfo.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
						</c:if>
						</configByCorp:fieldVisibility>
						<c:if test="${partnerType == 'CR' || partnerType == 'OO'}">
							<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
						</c:if>
						<c:if test="${partnerType == 'OO'}">
							<li><a href="standardDeductionsList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Standard Deductions</span></a></li>
						</c:if>
					</configByCorp:fieldVisibility>
				</c:if>
				
				<c:if test="${partnerType == 'AC'}">
					<c:if test="${not empty partnerPublic.id}">
						<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
						<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
						<li><a href="accountContactList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
						</configByCorp:fieldVisibility>
						<c:if test="${checkTransfereeInfopackage==true}">
						<li><a href="editContractPolicy.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
						</c:if>
						<c:if test="${partnerPublic.partnerPortalActive == true}">
						 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation"><li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
						</c:if>
					</c:if>
				
				</c:if>
				<c:if test="${partnerType == 'PP'}">
				<c:if test="${not empty partnerPublic.id}">
				<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				</c:if>
				</c:if>
				
				<c:if test="${partnerType == 'AG'}">
					<c:if test="${not empty partnerPublic.id}">
						<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
						<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
						<li><a href="baseList.html?id=${partnerPublic.id}"><span>Base</span></a></li>
						
					</c:if>
				</c:if>
				<c:if test="${partnerType == 'VN'}">
					<c:if test="${not empty partnerPublic.id}">
						<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					    <li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Users & Contact</span></a></li>
					</c:if>
				
				</c:if>
				
				<c:if test="${paramValue == 'View'}">
					<li><a href="partnerView.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
			</sec-auth:authComponent> 
				   
		    <sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
			   	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			   	 <li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li><!--
  					
			   	<c:if test="${partnerPrivate.partnerPortalActive == true}">
		  			<li><a href="partnerUsersList.html?id=${partnerPublic.id}"><span>Users & Contacts</span></a></li>
				</c:if>
				-->
			    	<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
			    <li><a href="partnerAgent.html"><span>Agent List</span></a></li>
		    </sec-auth:authComponent>
			<c:if test="${partnerType == 'AG'}">
				 <c:if test="${not empty partnerPublic.id && sessionCorpID == 'SSCW'}">
				  <c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
							<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>
				  </c:if>
				  <c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
							<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
				  </c:if>
				  <li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
				  </c:if>
		   </c:if>
		   <c:if test="${partnerPublic.partnerPortalActive == true && partnerType == 'AG'}">
		        <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
			       <li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			       
			       <li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
			       	</configByCorp:fieldVisibility>
			</c:if>
			
			<c:if test="${not empty partnerPublic.id && partnerType == 'AG' && sessionCorpID != 'TSFT'}">

			       <li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
	
			 </c:if>
			
				<c:if test="${partnerType == 'VN' || partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
				  </c:if>
				</c:if>
				
				<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partnerPublic.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				 </c:if>
				</c:if>
				</c:if>
				<c:if test="${usertype=='ACCOUNT'}">
				<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Detail</span></a></li>
				
				<li><a href="editContractPolicy.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Policy</span></a></li>				
				
				<li><a href="${url}"><span>FAQ</span></a></li>
				  </c:if>
				</c:if>
				<li><a href="${urlforFileCabinet}"><span>Partner File Cabinet</span></a></li>
				</c:if>
				<c:if test="${(partnerType == 'PP' || partnerType == 'OO' || partnerType == 'CR') && not empty partnerPublic.id}">
				<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${partnerPublic.id}&secondID=${partnerPrivate.id}&tableName=partnerpublic,partnerprivate&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				</c:if>
				<c:if test="${(partnerType != 'PP' && partnerType != 'OO' && partnerType != 'CR') && not empty partnerPublic.id}">
				<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${partnerPublic.id}&tableName=partnerpublic&from=PartnerPublic&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				</c:if>
				
		</ul>
	</div>
	<div class="spn" style="!line-height:0px;">&nbsp;</div>
	</div>

	<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
	<div id="content" align="center">
	<div id="liquid-round-top" >
	<div class="top"><span></span></div>
	<div class="center-content">
	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:70%">
		<tbody>
			<tr>
	<c:if test="${partnerType =='AG'}">
		<td> 		
		 <div class="image-editor"> 
         <label class="custom-file-upload" onclick="ShowCropitDetails();">
          <input type="file" hidden="true" class="cropit-image-input" > 
          <i class="fa fa-cloud-upload"></i> Upload Logo
         </label> 
         <div class="cropit-preview" id= "cropit-previewShowHide"></div> 
        <div class="image-size-label" id= "image-size-labelShowHide">
          Resize image
        </div>
        <input type="range" class="cropit-image-zoom-input" id= "cropit-image-zoom-inputShowHide" style="width:200px;display:none">
        <input type="hidden" name="imageDataFileLogo" class="hidden-image-data" />
               <c:if test="${!(partnerPublic.companyLogo == null || partnerPublic.companyLogo == '')}">
       <div  style="position: relative; width: 200px; height: 200px;margin-top: 8px;" id="companyLogoShowHide">
       <div style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;" ><img id="companyLogo1"  src="UserImage?location=${partnerPublic.companyLogo}" alt=""  /> </div>
       </div>
       </c:if> 
      </div>
 
			</td>
	</c:if>
				<td>
		
				
				<c:choose>
					<c:when test="${partnerType == 'PP'}">
					<s:hidden name="partnerPublic.partnerType"/>
							<table cellspacing="2" cellpadding="2" border="0">
							<tbody>
								<tr>
									<td width="12px"></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.firstName' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.middleInitial' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.lastName' /><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode' /><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.status' /></td>
									<c:if  test="${compDivFlag == 'Yes'}">
									<td align="left" class="listwhitetext">Company Division<%-- <c:if test="${partnerPublic.corpID == 'SSCW'}"><font color="red" size="2">*</font></c:if> --%></td>
									</c:if>
									<td align="left" class="listwhitetext">External Ref.</td>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<td align="left" class="listwhitetext" width="65px">Do&nbsp;Not&nbsp;Merge</td>
									</sec-auth:authComponent>
									
								</tr>
								<tr>
									<td width="12px"></td>
									<td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="partnerPublic.partnerPrefix" list="%{prefixList}" cssStyle="width:50px" headerKey="" headerValue=""/></td>
									<td align="left" class="listwhitetext"><s:textfield name="partnerPublic.firstName" required="true" cssClass="input-text" cssStyle="width:145px" maxlength="15" tabindex="" onkeyup="regExMatch(this,event);"/></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.middleInitial" cssClass="input-text" required="true" cssStyle="width:13px" maxlength="1" onkeyup="regExMatch(this,event);" onkeydown="return onlyCharsAllowed(event)" tabindex="" /></td>
									<td align="left" class="listwhitetext"><s:textfield id="lastName" key="partnerPublic.lastName" required="true" cssClass="input-text" size="30" maxlength="80" tabindex="1" onchange="validCheck5(this);changeAddressPin();" /></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.partnerSuffix" cssStyle="width:30px" required="true" cssClass="input-text" maxlength="10" onkeyup="regExMatch(this,event);" onkeydown="return onlyCharsAllowed(event)" tabindex="1" /></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.partnerCode" required="true" cssClass="input-textUpper" size="8" maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" tabindex="" /></td>
									<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:95px" onchange="return checkStatus(this),changeStatus();" tabindex="" /></td>
									<c:if  test="${compDivFlag == 'Yes'}">
									<td><s:select cssClass="list-menu" name="partnerPrivate.companyDivision" list="%{companyDivis}" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
									</c:if>
									<c:if  test="${compDivFlag != 'Yes'}">
									<s:hidden name="partnerPrivate.companyDivision"/>
									</c:if>
									<s:hidden name="partnerPrivate.status" value="Approved"/>
									<c:if test="${partnerType!='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-text" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" /></td>
									</c:if>
									<c:if test="${partnerType=='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-textUpper" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
									</c:if>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<c:set var="ischeckedMerge" value="false"/>
										<c:if test="${partnerPublic.doNotMerge}">
												<c:set var="ischeckedMerge" value="true"/>
										</c:if>
										<td class="listwhitetext" width="" align="center"><s:checkbox key="partnerPublic.doNotMerge" onclick="changeStatus();"  value="${ischeckedMerge}" fieldValue="true"/></td>
									</sec-auth:authComponent>
									
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${partnerType == 'OO'}">
					<s:hidden name="partnerPublic.partnerType"/>
							<table cellspacing="2" cellpadding="2" border="0" style="padding-left:15px;">
							<tbody>
								<tr>
								<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.firstName' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.middleInitial' /></td>
									<td align="left" class="listwhitetext">Legal Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix' /></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode' /><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.status' /></td>
									<td align="left" class="listwhitetext">External Ref.</td>
									<td align="left" class="listwhitetext">Start Date</td>
									<td align="left" class="listwhitetext"></td>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<td align="left" class="listwhitetext" width="">Do&nbsp;Not&nbsp;Merge</td>
									</sec-auth:authComponent>
										</tr>
								<tr>
									<td align="left" class="listwhitetext"><s:textfield name="partnerPublic.partnerPrefix" cssStyle="width:25px" required="true" cssClass="input-text" maxlength="3" onfocus="onLoad();" onkeydown="return onlyCharsAllowed(event)" onkeyup="regExMatch(this,event);"  tabindex="" /></td>
									<td align="left" class="listwhitetext"><s:textfield name="partnerPublic.firstName" required="true" cssClass="input-text" cssStyle="width:135px" maxlength="15" tabindex="" onkeyup="regExMatch(this,event);"/></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.middleInitial" cssClass="input-text" required="true" cssStyle="width:13px" maxlength="1" onkeyup="regExMatch(this,event);" onkeydown="return onlyCharsAllowed(event)" tabindex="" /></td>
									<td align="left" class="listwhitetext"><s:textfield id="lastName" key="partnerPublic.lastName" required="true" cssClass="input-text" size="30" maxlength="80" tabindex="1" onchange="validCheck5(this),changeAddressPin();" /></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.partnerSuffix" cssStyle="width:30px" required="true" cssClass="input-text" maxlength="10" onkeyup="regExMatch(this,event);" onkeydown="return onlyCharsAllowed(event)" tabindex="" /></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.partnerCode" required="true" cssClass="input-textUpper" size="9" maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" tabindex="" /></td>
									<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:95px" onchange="return checkStatus(this),changeStatus();" tabindex="" /></td>
									<c:if test="${partnerType!='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-text" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" /></td>
									</c:if>
									<c:if test="${partnerType=='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-textUpper" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
									</c:if>
									<c:if test="${not empty partnerPublic.startDate}">
										<s:text id="partnerstartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerPublic.startDate" /></s:text>
										<td width="50px"><s:textfield cssClass="input-text" id="startDate" readonly="true" name="partnerPublic.startDate" value="%{partnerstartDateFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
										<td align="left" style="padding-top:1px;"><img id="startDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									<c:if test="${empty partnerPublic.startDate}">
										<td width="50px"><s:textfield cssClass="input-text" id="startDate" readonly="true" name="partnerPublic.startDate" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
										<td align="left" style="padding-top:1px;"><img id="startDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<c:set var="ischeckedMerge" value="false"/>
										<c:if test="${partnerPublic.doNotMerge}">
											<c:set var="ischeckedMerge" value="true"/>
										</c:if>
										<td class="listwhitetext" width="" align="center" ><s:checkbox key="partnerPublic.doNotMerge" onclick="changeStatus();"  value="${ischeckedMerge}" fieldValue="true"/></td>
									</sec-auth:authComponent>
									
								</tr>
								<tr>
								<td align="left" class="listwhitetext" colspan="4">Alias Name</td>
								</tr>
								<tr>
								<td align="left" class="listwhitetext" colspan="4"><s:textfield key="partnerPublic.aliasName" required="true" cssClass="input-text" size="50" maxlength="80" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${partnerType == 'AC'}">
						<table border="0" cellpadding="3" cellspacing="0">
							<tbody>
								<tr>
									<td width="12px"></td>
									<td align="left" class="listwhitetext">Legal Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode' /><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.status' /></td>
									<c:if  test="${compDivFlag == 'Yes'}">
									<td align="left" class="listwhitetext">Company Division<%-- <c:if test="${partnerPublic.corpID == 'SSCW'}"><font color="red" size="2">*</font></c:if> --%></td>
									</c:if>
									<td align="left" class="listwhitetext">External Ref.</td>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<td align="left" class="listwhitetext" width="">Do&nbsp;Not&nbsp;Merge</td>
									</sec-auth:authComponent>
									</tr>
								<tr>
									<s:hidden name="partnerPublic.firstName" />
									<s:hidden name="partnerPublic.partnerSuffix" />
									<s:hidden name="partnerPublic.partnerPrefix" />
									<s:hidden name="partnerPublic.middleInitial" />
									<td width="12px"></td>
									<td align="left" class="listwhitetext"><s:textfield id="lastName" key="partnerPublic.lastName" required="true" cssClass="input-text" size="50" maxlength="80" tabindex="" onchange="validCheck5(this),changeAddressPin();" /></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="9" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
									<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:100px" onchange="return checkStatus(this),changeStatus();" tabindex="" /></td>
									<c:if  test="${compDivFlag == 'Yes'}">
									<td><s:select cssClass="list-menu" name="partnerPrivate.companyDivision" list="%{companyDivis}" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
									</c:if>
									<c:if  test="${compDivFlag != 'Yes'}">
							        <s:hidden name="partnerPrivate.companyDivision"/>							
							        </c:if>
									<c:if test="${partnerType!='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-text" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" /></td>
									</c:if>
									<c:if test="${partnerType=='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-textUpper" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
									</c:if>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<c:set var="ischeckedMerge" value="false"/>
										<c:if test="${partnerPublic.doNotMerge}">
											<c:set var="ischeckedMerge" value="true"/>
										</c:if>
										<td class="listwhitetext" width="" align="center" ><s:checkbox key="partnerPublic.doNotMerge" onclick="changeStatus();"  value="${ischeckedMerge}" fieldValue="true"/></td>
									</sec-auth:authComponent>
									<c:if test="${usertype!='ACCOUNT' }">
										<configByCorp:fieldVisibility componentId="component.field.partner.partnerTypeDropDown">
											<td align="left" class="listwhitetext">Type <s:select cssClass="list-menu" name="partnerPublic.partnerType" list="%{pType}" cssStyle="width:150px" onchange="chkDmmCmm()" /></td>
										</configByCorp:fieldVisibility>
									</c:if>
									<configByCorp:fieldVisibility componentId="component.field.partner.partnerTypeDropDownOther">
										<s:hidden name="partnerPublic.partnerType" />
									</configByCorp:fieldVisibility>
									</tr>
									<tr>
									<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4">Alias Name</td>
								</tr>
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4"><s:textfield key="partnerPublic.aliasName" required="true" cssClass="input-text" size="50" maxlength="80" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<table cellspacing="0" cellpadding="3" border="0">
							<tbody>
								<tr>
								<td width="12px"></td>
									<td align="left" class="listwhitetext">Legal Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode' /><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.status' /></td>
									<td align="left" class="listwhitetext">External Ref.</td>
									<c:if test="${partnerType == 'VN' || partnerType == 'CR'}">
										<td align="left" class="listwhitetext">Start Date</td>
										<td align="left" class="listwhitetext"></td>
									</c:if>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<td align="left" class="listwhitetext" width="">Do&nbsp;Not&nbsp;Merge</td>
									</sec-auth:authComponent>
								
								</tr>
								<tr>
								<s:hidden name="partnerPublic.partnerType"/>
										<s:hidden name="partnerPublic.firstName" />
									<s:hidden name="partnerPublic.partnerSuffix" />
									<s:hidden name="partnerPublic.partnerPrefix" />
									<s:hidden name="partnerPublic.middleInitial" />
									<td width="12px"></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.lastName" id="lastName" required="true" cssClass="input-text" size="50" maxlength="80" tabindex="" onchange="validCheck5(this),changeAddressPin();" /></td>
									<td align="left" class="listwhitetext"><s:textfield key="partnerPublic.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="9" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
									<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:105px" onchange="return checkStatus(this),changeStatus();" tabindex="" /></td>
									<c:if test="${partnerType!='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-text" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" /></td>
									</c:if>
									<c:if test="${partnerType=='AG'}">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-textUpper" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
									</c:if>
									<c:if test="${partnerType == 'VN' || partnerType == 'CR'}">
										<c:if test="${not empty partnerPublic.startDate}">
											<s:text id="partnerstartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerPublic.startDate" /></s:text>
											<td width="50px"><s:textfield cssClass="input-text" id="startDate" readonly="true" name="partnerPublic.startDate" value="%{partnerstartDateFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
											<td align="left"><img id="startDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
										</c:if>
										<c:if test="${empty partnerPublic.startDate}">
											<td width="50px"><s:textfield cssClass="input-text" id="startDate" readonly="true" name="partnerPublic.startDate" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
											<td align="left"><img id="startDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
										</c:if>
									</c:if>
									<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<c:set var="ischeckedMerge" value="false"/>
										<c:if test="${partnerPublic.doNotMerge}">
											<c:set var="ischeckedMerge" value="true"/>
										</c:if>
										<td class="listwhitetext" width="" align="center"><s:checkbox key="partnerPublic.doNotMerge" onclick="changeStatus();"  value="${ischeckedMerge}" fieldValue="true"/></td>
									</sec-auth:authComponent>
									
								</tr>
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4">Alias Name</td>
								</tr>
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4"><s:textfield key="partnerPublic.aliasName" required="true" cssClass="input-text" size="50" maxlength="80" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
							</tbody>
						</table>
					</c:otherwise>
				</c:choose>
				
				<c:if test="${partnerType == 'AG'}">
				
				
		
				
			<%-- 	<div style="float:left;margin-left:20px;">
				<table>
				<tr>
				<c:if test="${!(partnerPublic.companyLogo == null || partnerPublic.companyLogo == '')}">
					<td align="left" class="listwhitetext" colspan=""><img id="companyLogo1" class="urClass preview" src="UserImage?location=${partnerPublic.companyLogo}" alt=""  /></td>
				</c:if>
				<c:if test="${partnerPublic.companyLogo == null || partnerPublic.companyLogo == ''}">
					<td align="left" class="listwhitetext" colspan=""><img id="companyLogo1" src="${pageContext.request.contextPath}/images/no-picture-agent.jpg" alt="" width="150" height="100" class="preview" /></td>
				</c:if> 
				</tr>
				<c:if test="${partnerPublic.companyLogo == null || partnerPublic.companyLogo == ''}">
				<td align="left" valign="middle" class="listwhitetext" style="width:90px"><div style="padding:8.8px 5px;border:1px solid #50869f;">Company&nbsp;Logo</div></td>
			</c:if>
			 <c:if test="${!(partnerPublic.companyLogo == null || partnerPublic.companyLogo == '')}">
				<td align="left" valign="middle" class="listwhitetext" ><div style="padding:8.8px 5px;border:1px solid #50869f;">Company&nbsp;Logo</div></td>
			</c:if> 
			<td align="left" valign="bottom" class="listwhitetext">
			<input type="file" accept="image/*" name="fileLogo" id="fileLogo" onchange="return Upload(event)" >
			<s:hidden name="fileLogoUploadFlag" />
			<div class="upload-preview"><i class="fa fa-cloud-upload"></i> &nbsp;CHOOSE FILES <s:file name="fileLogo" cssStyle="width:128px;" id="fileLogo" cssClass="hide_file" required="true" />
			</div>
			<!--<s:file name="fileLogo" id="fileLogo" cssClass="text file" required="true" />  -->
			<!-- <input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph1();" /> -->
			</td> 				
				</tr>
		
				</table>
          
          <script>           
            
        </script>
				 </div> --%>
				 
				
				 
				 </c:if>
				 </td></tr></table>
				<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="100%">
					<tbody>
						<tr>
							<td align="center" colspan="10" class="vertlinedata"></td>
						</tr>
						<tr>
							<td class="listwhitetext" style="width:50px; height:5px;"></td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="3" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="10px"></td>
							<td class="listwhitetext"><b>Billing Address</b><font color="red" size="2">*</font></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.billingAddress1" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode' /><font color="red" size="2">*</font></td>
							<td><s:select cssClass="list-menu" name="partnerPublic.billingCountry" list="%{countryDesc}" cssStyle="width:163px" headerKey="" headerValue="" onchange="requiredState();changeStatus();getBillingCountryCode(this);autoPopulate_partner_billingCountry(this);getBillingState(this);getVatCountryCode(this);enableStateListBill();" tabindex="" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax' /></td>
							<td><s:textfield cssClass="input-text" name="partnerPublic.billingFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							
							
							<c:if test="${partnerType == 'OO' || partnerType == 'CR'}">
							<c:if test="${empty partnerPrivate.id}">
									<td align="right" style="width:100px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>

							</c:if>
							<c:if test="${not empty partnerPrivate.id}">
								<c:choose>
									<c:when test="${partnerNotes == '0' || partnerNotes == '' || partnerNotes == null}">
										<td align="right" style="width:100px"><img id="partnerNotesImage2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage2&fieldId=partnerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage2&fieldId=partnerNotes&decorator=popup&popup=true',800,600);" ></a></td>
									</c:when>
									<c:otherwise>
											<td align="right" style="width:100px"><img id="partnerNotesImage2" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage2&fieldId=partnerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage2&fieldId=partnerNotes&decorator=popup&popup=true',800,600);" ></a></td>
									</c:otherwise>
								</c:choose> 
							</c:if>
							</c:if>
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.billingAddress2" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" id="redHiddenFalse" class="listwhitetext"><fmt:message key='partner.billingState' /></td>
							<td align="right" id="redHidden" class="listwhitetext"><fmt:message key='partner.billingState' /><font  color="red" size="2">*</font></td>
							<td><s:select id="billState" name="partnerPublic.billingState" cssClass="list-menu" list="%{bstates}" cssStyle="width:163px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone' /><font color="red" size="2">*</font></td>
							<td><s:textfield cssClass="input-text" name="partnerPublic.billingPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.billingAddress3" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity' /><font color="red" size="2">*</font></td>
							<td><s:textfield cssClass="input-text" name="partnerPublic.billingCity" id="billingCity" cssStyle="width:160px" maxlength="50" onkeydown="return onlyCharsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);autoCompleterAjaxBillingCity();"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail' /></td>
							<td><s:textfield cssClass="input-text"  name="partnerPublic.billingEmail" size="30" maxlength="65" tabindex="" onchange="checkBillingEmails(this)"/></td>
								
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.billingAddress4" size="40" maxlength="40" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip' /></td>
							<td><s:textfield cssClass="input-text" name="partnerPublic.billingZip" cssStyle="width:160px" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<c:if test="${partnerType == 'PP'}">
							<td align="right" class="listwhitetext">Storage&nbsp;E-Mail</td>
							<td><s:textfield cssClass="input-text" name="partnerPrivate.storageEmail" size="30" maxlength="65" onchange="checkStoreageEmails()" /></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG' || partnerType == '' }">
					<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
							<tr>
								<td width="10px"></td>
								<td class="listwhitetext"><b>Terminal Address</b></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.terminalAddress1" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCountryCode' /></td>
								<td><s:select cssClass="list-menu" name="partnerPublic.terminalCountry" list="%{countryDesc}" cssStyle="width:163px" headerKey="" headerValue="" onchange="changeStatus();getTerminalCountryCode(this);autoPopulate_partner_terminalCountry(this);getTerminalState(this);enableStateListTerm();"
									tabindex="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalFax' /></td>
								<td><s:textfield cssClass="input-text" name="partnerPublic.terminalFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.terminalAddress2" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalState' /></td>
								<td><s:select id="partnerAddForm_partner_terminalState" name="partnerPublic.terminalState" list="%{tstates}" cssClass="list-menu" cssStyle="width:163px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalPhone' /></td>
								<td><s:textfield cssClass="input-text" name="partnerPublic.terminalPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.terminalAddress3" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCity' /></td>
								<td><s:textfield cssClass="input-text" name="partnerPublic.terminalCity" cssStyle="width:160px" maxlength="50" onkeydown="return onlyCharsAllowed(event)" onkeyup="regExMatch(this,event);" tabindex="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalEmail' /></td>
								<td><s:textfield cssClass="input-text" name="partnerPublic.terminalEmail" size="30" maxlength="65" tabindex="" onchange="checkTerminalEmails(this);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.terminalAddress4" size="40" maxlength="30" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalZip' /></td>
								<td><s:textfield cssClass="input-text" name="partnerPublic.terminalZip" cssStyle="width:160px" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="1"></td>
								<c:if test="${usertype!='ACCOUNT'}">
								<td colspan="3" align="right"><input type="button" class="cssbutton" style="width:162px; height:25px" value="Copy from billing address" onClick="copyBillToTerm();changeStatus();" /></td>
								<td colspan="2" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from mailing address" onClick="copyMailToTerm();changeStatus();" /></td>
								</c:if>
							</tr>
						</tbody>
					</table>
				</c:if>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;padding:0px;">
					<tbody>
						<tr>
							<td align="center" colspan="10" class="vertlinedata"></td>
						</tr>
						<tr>
							<td class="listwhitetext" style="width:50px; height:5px;"></td>
						</tr>
					</tbody>
				</table>
				<table border="0" style="margin:0px;padding:0px;">
					<tr>
						<td valign="top">
						<table cellspacing="3" cellpadding="2" border="0" style="margin:0px;padding:0px;">
							<tbody>
								<tr>
									<td width="10px"></td>
									<td class="listwhitetext"><b>Mailing Address</b></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" id="mailingAddress1" name="partnerPublic.mailingAddress1" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);" onchange="changeAddressPin();"/></td>
									<td align="right" class="listwhitetext" style="width: 47px;"><fmt:message key='partner.mailingCountryCode' />&nbsp;</td>
									<td><s:select cssClass="list-menu" id="mailingCountry" name="partnerPublic.mailingCountry" list="%{countryDesc}" cssStyle="width:176px" headerKey="" headerValue="" onchange="changeStatus();getMailingCountryCode(this);autoPopulate_partner_mailingCountry(this);getMailingState(this);enableStateListMail();changeAddressPin();"
										tabindex="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax' />&nbsp;</td>
									<td><s:textfield cssClass="input-text" name="partnerPublic.mailingFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" id="mailingAddress2" name="partnerPublic.mailingAddress2" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);" onchange="changeAddressPin();"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState' /></td>
									<td><s:select id="mailingState" name="partnerPublic.mailingState" cssClass="list-menu" list="%{mstates}" cssStyle="width:176px" headerKey="" headerValue="" onchange="changeStatus();changeAddressPin();" tabindex="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone' /></td>
									<td><s:textfield cssClass="input-text" name="partnerPublic.mailingPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.mailingAddress3" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity' /></td>
									<td><s:textfield cssClass="input-text" name="partnerPublic.mailingCity" id="mailingCity" cssStyle="width:173px" maxlength="50" onkeydown="return onlyCharsAllowed(event)" onkeyup="regExMatch(this,event);autoCompleterAjaxMailingCity();" tabindex="" onchange="changeAddressPin();"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail' /></td>
									<td><s:textfield cssClass="input-text" name="partnerPublic.mailingEmail" size="30" maxlength="65" tabindex=""  onchange="checkMailingEmails(this);"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.mailingAddress4" size="40" maxlength="40" tabindex="" onkeyup="regExMatch(this,event);"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip' /></td>
									<td><s:textfield cssClass="input-text" id="mailingZip" name="partnerPublic.mailingZip" cssStyle="width:173px" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);" onchange="changeAddressPin();"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG'}">
										<td colspan="3"></td>
										<c:if test="${usertype!='ACCOUNT'}">
										<td colspan="2" align="left"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from terminal address" onClick="copyTermToMail();changeStatus();" /></td>
										<td colspan="2" align="left"><input type="button" class="cssbutton" style="width:182px; height:25px" value="Copy from billing address" onClick="copyBillToMail();changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'||partnerType == 'AC'||partnerType == 'OO'||partnerType == 'PP'}">
										<tr>
											<td></td>
											<td colspan="6" style="margin: 0px;padding: 0px;">
											<table style="margin: 0px">
												<tr>
												    <td align="right" class="listwhitetext" width="">Latitude</td>
													<td><s:textfield cssClass="input-text" name="partnerPublic.latitude" size="18" onkeyup="regExMatch(this,event);" readonly="true"/></td>
													<td align="right" class="listwhitetext" width="">Longitude</td>
													<td><s:textfield cssStyle="width: 123px;" cssClass="input-text" name="partnerPublic.longitude" size="18" onkeyup="regExMatch(this,event);" readonly="true"/></td>
													<td align="right" class="listwhitetext" width=""></td>
												    <c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'||partnerType == 'AC'||partnerType == 'OO'||partnerType == 'PP'}">
									                <td align="left" class="listwhitetext"><input type="button" class="cssbutton" onclick="pinLocation();" name="Location Pin" value="Location Pin" style="width:100px; height:25px" /></td>
										            </c:if>
												</tr>
											</table>
											</td>
										</tr>
										<tr>
											<td></td>
											<td colspan="8" style="margin: 0px;padding: 0px;">
											<table style="margin: 0px">
												<tr>
													<td align="right" class="listwhitetext">Website Url:</td>
													<td align="left"><s:textfield cssClass="input-text" name="partnerPublic.url" size="58" onkeyup="regExMatch(this,event);"/></td>
													<td align="left"><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" onclick="return openWebSite();" value="Go to site" style="width:98px; height:25px" /></c:if></td>
													<td align="right" class="listwhitetext" width="8">&nbsp;</td>
													<td align="right" class="listwhitetext" width="88px">Year Established</td>
													<td align="left"><s:textfield cssClass="input-text" name="partnerPublic.yearEstablished" size="9" maxlength="4" onchange="onlyNumeric(this)" /></td>
												</tr>
											</table>
											</td>
										</tr>
									</c:if>
									<c:if test="${partnerType == 'PP' || partnerType == 'AC'  || partnerType == 'OO' || partnerType == ''}">
										<td colspan="4" align="right"><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" style="width:162px; height:25px" value="Copy from billing address" onClick="copyBillToMail();changeStatus();" /></c:if></td>
									</c:if>
									<%--<c:if test="${partnerType == 'VN' || partnerType == 'AC'}">
										<tr>
											<td></td>
											<td colspan="8" style="margin: 0px;padding: 0px;">
											<table style="margin: 0px">
												<tr>
													<td align="right" class="listwhitetext" width="">Website Url:</td>
													<td align="left"><s:textfield cssClass="input-text" name="partnerPublic.url" size="60" /></td>
													<td align="left"><input type="button" class="cssbutton" onclick="return openWebSite();" value="Go to site" style="width:75px; height:25px" /></td>
													<td align="right" class="listwhitetext" width="28">&nbsp;</td>
												</tr>
											</table>
											</td>
										</tr>
									</c:if>
								 --%><c:if test="${partnerType == 'PP'}"> 
								 <tr>
								 <td colspan="15">
								 <table width="100%" cellpadding="2">
								<tr> 
								<td  align="right" class="listwhitetext" ><fmt:message key='accountInfo.accountHolder' /></td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.accountHolder" list="%{sale}" cssStyle="width:136px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
							    <td align="right" class="listwhitetext" width="">ID#</td>
								<td><s:textfield cssClass="input-text" name="partnerPublic.OMNINumber" cssStyle="width:158px" maxlength="10" onkeyup="regExMatch(this,event);"/></td>													
								<td align="right" class="listwhitetext"><fmt:message key='accountInfo.creditTerms' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"> <font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
								<td><s:select cssClass="list-menu" name="partnerPrivate.creditTerms" list="%{creditTerms}" cssStyle="width:178px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
							    </tr>
							    <tr> 
								<td  align="right" class="listwhitetext">Accounting&nbsp;Hold</td>
								<td class="listwhitetext" align="left" ><s:checkbox key="partnerPrivate.accountingHold" onclick="changeStatus();" cssStyle="margin:0px;"/></td>
								</td>
								<td align="right" class="listwhitetext">Collection</td>
								<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.collection" list="%{collectionList}" cssStyle="width:162px;" headerKey="" headerValue="" onchange="changeStatus();"/></td>								
		                        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
								<td align="right" class="listwhitetext" width="80px">VAT&nbsp;Billing&nbsp;Group<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"></configByCorp:fieldVisibility></td>
				                <td align="left"><s:select cssClass="list-menu" name="partnerPrivate.vatBillingGroup" list="%{vatBillingGroups}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:150px" tabindex=""/></td>
								</configByCorp:fieldVisibility>
								<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
								<td align="right" class="listwhitetext" width="30">RUT#&nbsp;</td>
								<td class="listwhitetext">					
								<s:textfield cssClass="input-text" name="partnerPrivate.rutTaxNumber" id="rutTaxNumber" size="25" maxlength="30" />
								</td>
								</configByCorp:fieldVisibility>
								</tr>
								</table>
								</td>
								</tr>
					<c:if test="${company.qualitySurvey==true}">
					<tr>
					 <td colspan="15">
					<table width="100%" cellpadding="2" class="detailTabLabel">
					<tr> 								
					<td align="left" class="listwhitetext" width="130"><b><u>Customer&nbsp;Feedback</u></b></td>														 	
					<td align="left" class="listwhitetext">
					<c:if test="${qualitySurvey=='checked' && justSayYes=='checked'}">			
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom;margin:0px;" checked="checked" onclick="GetSelectedItem(this.value)" />
						&nbsp;&nbsp;Quality Survey &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Quality Survey" style="vertical-align:bottom;margin:0px;" onclick="GetSelectedItem(this.value)" />
					</c:if>
					<c:if test="${qualitySurvey!='checked' && justSayYes=='checked'}">			
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom;margin:0px;" checked="checked" onclick="GetSelectedItem(this.value)" />
						&nbsp;&nbsp;Quality Survey &nbsp;	<input type="radio" name="partnerPrivateCustomerFeedback" value="Quality Survey" style="vertical-align:bottom;margin:0px;" onclick="GetSelectedItem(this.value)" />
					</c:if>
					<c:if test="${qualitySurvey=='checked' && justSayYes!='checked'}">
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom;margin:0px;" onclick="GetSelectedItem(this.value)" />		
						&nbsp;&nbsp;Quality Survey &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" checked="checked" style="vertical-align:bottom;margin:0px;" value="Quality Survey" onclick="GetSelectedItem(this.value)"/>
					</c:if>
					</td>
					</tr>
					</table>
					</td>
					</tr>
					</c:if>										
					</c:if>
					<c:if test="${partnerType != 'PP'}">
						<s:hidden name="partnerPrivate.accountingHold" />
					</c:if>
					<c:if test="${partnerType == 'VN' || partnerType == 'AC'}">
						<tr>
							<td colspan="10">
								<table style="margin: 0px; padding: 0px;" cellpadding="0">
									<tr>
										<c:if test="${partnerType == 'VN'}">
										<td align="left" class="listwhitetext">Type&nbsp;of&nbsp;Vendor&nbsp;</td>
										<td><s:select cssClass="list-menu" name="partnerPublic.typeOfVendor" list="%{typeOfVendor}" value="%{selectedJobList}"	multiple="true" headerKey="" headerValue="" cssStyle="width:120px; height:62px" onchange="changeStatus(),checkVendorType();" /></td>
											<c:set var="isProgressiveChecked" value="false" />
											<c:if test="${partnerPrivate.progressiveType}">
												<c:set var="isProgressiveChecked" value="true" />
											</c:if>
										<td align="right" id="progressiveTypeId" class="listwhitetext" style="padding-top: 20px; ! padding-top: 0px;">
										&nbsp;Progressive&nbsp;Type&nbsp;<s:checkbox key="partnerPrivate.progressiveType" onclick="changeStatus()" value="${isProgressiveChecked}" fieldValue="true" /></td>
										</c:if>
										<td align="right" class="listwhitetext" width="70">Commission</td>
										<td align="left" class="listwhitetext" width="92"><s:textfield cssClass="input-text" name="partnerPublic.vendorCommission" size="6" maxlength="6" onchange="onlyFloat(this);valueEvalutionPercentage();" />%</td>
										<c:if test="${partnerType == 'VN'}">
										<td align="right" class="listwhitetext">Minimum&nbsp;to&nbsp;Company</td>
										<td align="left" width="90"><s:textfield cssClass="input-text" name="partnerPublic.minimumDFB" size="10" maxlength="9" onchange="isNumeric(this)" /></td>
										</c:if>
									</tr>
									<c:if test="${partnerType == 'VN'}">
									<tr>
										<td colspan="6" class="listwhitetext" align="left" style="font-size: 10px; font-style: italic;"><b>&nbsp; Use Control + mouse to select	multiple Vendor Type</b></td>
									</tr>
									</c:if>
								</table>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
			</td>
			<td valign="top">
			<div id="map" style="width: 200px; height: 200px"></div>
			
    
			</td>
			</tr>		
			<c:if test="${partnerType == 'VN'}">									
			<tr>
			<td  colspan="8">	
			<c:if test="${not empty partnerPublic.id}">
			<table class="detailTabLabel" border="0" width="100%">
 		  <tbody> 
 		  <div id="newmnav" style="!margin-bottom:5px;">		  		  
		<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Expiry&nbsp;List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div>
<div class="spn">&nbsp;</div>
<tr><td align="left" colspan="7">
<div style=" width:892px; overflow-x:auto; overflow-y:auto;">
<s:set name="ownerOperatorList" value="ownerOperatorList" scope="request"/>
<display:table name="ownerOperatorList" class="table" requestURI="" id="ownerOperatorList"  defaultsort="2" pagesize="10" style="width:870px">
    <display:column title="Expiry&nbsp;For">
    <a href="javascript:openWindow('editOwnerOperatorDetails.html?id=${ownerOperatorList.id}&parentId=${parentId}&bucket=Vendor&type=Tractor&decorator=popup&popup=true',750,300)">
    <c:out value="${ownerOperatorList.fieldName}"></c:out></a>
    </display:column>
    <display:column property="expiryDate"  title="Expiry&nbsp;Date"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortProperty="required" sortable="true" title="Required" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${ownerOperatorList.required}">checked="checked"</c:if>/> 
    </display:column>
	</display:table>
	<c:if test="${usertype!='ACCOUNT'}">

	<input type="button" class="cssbutton1" onclick="addOperatorVendor();" 
	        value="Add&nbsp;Expirations" style="width:130px; height:25px"/> 
     </c:if>
</div>
</td></tr> 
</tbody>
</table>
</c:if>  
</td>   
</tr>
<table width="100%" class="detailTabLabel">
<tr>
					<td height="10" align="left" class="listwhitetext" colspan="8" style="margin: 0px;">
							<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
								<tr>
									<td class="headtab_left"></td>
									<td class="headtab_center">&nbsp;Payment Data</td>
									<td width="28" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_special">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
								</table>
								<tr>
										<td align="right" class="listwhitetext"  width="75px">Tax&nbsp;ID</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.taxId"  size="25" maxlength="20" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext">Tax&nbsp;ID&nbsp;Type</td>
										<td><s:select name="partnerPrivate.taxIdType" cssClass="list-menu" list="%{taxIdType}" cssStyle="width:155px" onchange="changeStatus();"/></td>
										<td align="right" class="listwhitetext" width="130px"><fmt:message key='accountInfo.paymentMethod' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
									  <td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.paymentMethod" list="%{paytype}" cssStyle="width:105px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>

									
									</tr>
							</table>
								
								</td>
								</tr>
								<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('info')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Information</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="info">
					<table width="100%" class="detailTabLabel">
						<tr>
							<td valign="top">
							<table border="0" class="detailTabLabel">
								<tr>
										<td align="right" class="listwhitetext" width="72">VAT #</td>
										<td class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="partnerPublic.vatNumber"   size="25" maxlength="23" tabindex="12" onkeyup="regExMatch(this,event);"/></td>
	 								<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
									<td align="right" class="listwhitetext" width="72">RUT#&nbsp;</td>
									<td class="listwhitetext">					
									<s:textfield cssClass="input-text" name="partnerPrivate.rutTaxNumber" id="rutTaxNumber" size="25" maxlength="30" />
									</td>
									</configByCorp:fieldVisibility>
	 								</tr>
									</table>
									</td>
								</tr>
							</table>
							</div>
							</td>
						</tr>
					
						
</c:if>					
					
				</table>
			<c:if test="${partnerType == 'OO'}">
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
						<div onClick="javascript:animatedcollapse.toggle('driver')" style="margin: 0px">
							<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
								<tr>
									<td class="headtab_left"></td>
									<td class="headtab_center">&nbsp;Owner&nbsp;Ops</td>
									<td width="28" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_special">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
							</table>
						</div>
						<div id="driver">
							<table class="detailTabLabel" border="0" cellpadding="3" cellspacing="0" width="100%">
								<tbody>
									<tr><td height="5px"></td></tr>
									
									<tr>
									<td colspan="13">
									<table class="detailTabLabel" border="0">
									<tr>
										<td align="right" width="115px" class="listwhitetext"><fmt:message key='partner.driverAgency'/>#</td>
										<td><s:select cssClass="list-menu" name="partnerPrivate.driverAgency" list="%{driverAgencyList}" cssStyle="width:118px;height:20px" headerKey="" headerValue=""  /></td>
										<td align="right" class="listwhitetext" width="130px">VL&nbsp;Code</td>
										<td width="60"><s:textfield cssClass="input-text" name="partnerPrivate.validNationalCode" maxlength="15" cssStyle="width:115px;" onkeyup="regExMatch(this,event);"/></td>
									<td align="right" class="listwhitetext" width="100">Truck&nbsp;ID</td>
					<td width="150"><s:textfield cssClass="input-text" name="partnerPrivate.truckID" id="validCarrier" cssStyle="width:115px;" maxlength="20" readonly="true"/>
					<img align="top" class="openpopup" width="17" height="20" onclick="findTruckNumber();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
					
									</tr>
									<tr>
										<td align="right" class="listwhitetext">Long%</td>
										<td><s:textfield cssClass="input-text" name="partnerPrivate.longPercentage" cssStyle="width:115px;" maxlength="10" onkeyup="regExMatch(this,event);" /></td>
										<td align="right" class="listwhitetext" width="144px">Driver Commission</td>										
										<td width="155px;">
										<s:textfield cssClass="input-text" name="partnerPrivate.commission" maxlength="10" cssStyle="width:115px;" readonly="true" />
										<img class="openpopup" width="17" height="20" style="vertical-align:top;" onclick="findVanLineCommission();"  src="<c:url value='/images/open-popup.gif'/>" />
										</td>
										
										<td align="right" class="listwhitetext">Driver&nbsp;ID</td>
						               <td><s:textfield cssClass="input-text" name="partnerPrivate.driverID"  cssStyle="width:115px;" maxlength="20" readonly="true"/>
						               <img align="top" class="openpopup" width="17" style="vertical-align:top;" height="20" onclick="findDriverFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>										
									</tr>
									<tr>
										<td align="right" class="listwhitetext">License Number</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.licenseNumber"  cssStyle="width:115px;" maxlength="16" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext">State</td>
										<td><s:select name="partnerPrivate.licenseState" cssClass="list-menu" list="%{bstates}" cssStyle="width:118px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
								
										<td align="right" class="listwhitetext">Pay&nbsp;To&nbsp;ID</td>
						               <td><s:textfield cssClass="input-text" name="partnerPrivate.payTo" cssStyle="width:115px;" maxlength="20" readonly="true"/>
						               <img align="top" class="openpopup" width="17" height="20" onclick="findPayToFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
									</tr>
									<tr>
										<td align="right" class="listwhitetext">Tax ID</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.taxId"  cssStyle="width:115px;" maxlength="20"  onkeyup="regExMatch(this,event);" /></td>

										<td align="right" class="listwhitetext">Tax ID Type</td>
										<td colspan="1"><s:select name="partnerPrivate.taxIdType" cssClass="list-menu" list="%{taxIdType}" cssStyle="width:118px" onchange="changeStatus();"/></td>
									<td align="right" class="listwhitetext">Trailer&nbsp;ID</td>
						               <td><s:textfield cssClass="input-text" name="partnerPrivate.trailerID"  cssStyle="width:115px;" maxlength="20" readonly="true"/>
						               <img align="top" class="openpopup" width="17" height="20" onclick="truckTrailerPopup();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
									</tr>
									<tr>
									<td align="right" class="listwhitetext">Fleet</td>
									<td>
									<s:select name="partnerPrivate.fleet" cssClass="list-menu" list="%{fleet}" cssStyle="width:118px" headerKey="" headerValue="" onchange="changeStatus();"/>
									</td>
									<td align="right" class="listwhitetext">Hire</td>
									<!--<s:hidden name="partnerPublic.agentParentName"></s:hidden>
									--><c:if test="${not empty partnerPrivate.hire}">
									<s:hidden name="partnerPrivateHire"/>
								<s:text id="partnerHireDateFormattedValue2" name="${FormDateValue}">
									<s:param name="value" value="partnerPrivate.hire" />
								</s:text>
								<td width=""><s:textfield cssClass="input-text" id="hire" readonly="true" name="partnerPrivate.hire" value="%{partnerHireDateFormattedValue2}" cssStyle="width:115px;" maxlength="11" onkeydown="return onlyDel(event,this)" />
								<img id="hire_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty partnerPrivate.hire}">
							<s:hidden name="partnerPrivateHire"/>
								<td width=""><s:textfield cssClass="input-text" id="hire" readonly="true" name="partnerPrivate.hire" cssStyle="width:115px;" maxlength="11" onkeydown="return onlyDel(event,this)" />
								<img id="hire_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
									
									<td align="right" class="listwhitetext">Termination</td>
							<c:if test="${not empty partnerPrivate.termination}">
								<s:text id="partnerTerminationDateValue2" name="${FormDateValue}">
									<s:param name="value" value="partnerPrivate.termination" />
								</s:text>
								<s:hidden name="partnerPrivateTermination"/>
								<td width=""><s:textfield cssClass="input-text" id="termination" readonly="true" name="partnerPrivate.termination" value="%{partnerTerminationDateValue2}" cssStyle="width:115px;" maxlength="11" required="true"  onkeydown="return onlyDel(event,this)"/>
								<img id="termination_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty partnerPrivate.termination}">
							<s:hidden name="partnerPrivateTermination"/>
								<td width=""><s:textfield cssClass="input-text" id="termination" readonly="true" name="partnerPrivate.termination" cssStyle="width:115px;" maxlength="11" required="true"  onkeydown="return onlyDel(event,this)"/>
								<img id="termination_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</tr>
							<tr><td align="right">Company&nbsp;Escrow</td>
							<td><s:textfield cssClass="input-text" name="partnerPrivate.companyEscrow"  cssStyle="text-align:right;width: 115px;" onchange="onlyFloat(this);" onkeyup="regExMatch(this,event);"/></td>
							<td align="right">Personal&nbsp;Escrow</td>
							<td><s:textfield cssClass="input-text" name="partnerPrivate.personalEscrow"  cssStyle="text-align:right;width: 115px;" onchange="onlyFloat(this);" onkeyup="regExMatch(this,event);"/></td>
							<td align="right">Driver&nbsp;Type</td>
							<td colspan="1"><s:select name="partnerPrivate.driverType" cssClass="list-menu" list="%{driverType}" cssStyle="width: 118px;" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							</tr>
							<tr>
							<td align="right" class="listwhitetext">Birth</td>
							<td width="145px;">
							<c:if test="${not empty partnerPrivate.birth}">
								<s:text id="partnerBirthDateFormattedValue2" name="${FormDateValue}">
									<s:param name="value" value="partnerPrivate.birth" />
								</s:text>
								<s:textfield cssClass="input-text" cssStyle="vertical-align:top;width: 115px;" id="birth" readonly="true" name="partnerPrivate.birth" value="%{partnerBirthDateFormattedValue2}"  maxlength="11" onkeydown="return onlyDel(event,this)" />
								<img id="birth_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							</c:if>
							<c:if test="${empty partnerPrivate.birth}">
								<s:textfield cssClass="input-text" cssStyle="vertical-align:top;width: 115px;" id="birth" readonly="true" name="partnerPrivate.birth"  maxlength="11" onkeydown="return onlyDel(event,this)" />
								<img id="birth_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							</c:if>
							</td>
							<td align="right" class="listwhitetext">Commission Plan</td>
							<td><s:select name="partnerPrivate.commissionPlan" cssClass="list-menu" list="%{planCommissionList}" cssStyle="width:118px" headerKey="" headerValue="" /></td>
							
							<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
							<td align="right" class="listwhitetext" width="72">RUT#&nbsp;</td>
							<td class="listwhitetext">					
							<s:textfield cssClass="input-text" name="partnerPrivate.rutTaxNumber" id="rutTaxNumber" size="25" maxlength="30" />
							</td>
							</configByCorp:fieldVisibility>
							</tr>
									</table>
									</td>
									</tr>
									
									
									<tr>
									<td colspan="13">
									<table class="detailTabLabel" border="0">
									<tr>
										<c:set var="ischecked" value="false"/>
										<c:if test="${partnerPrivate.invoiceUploadCheck}">
											<c:set var="ischecked" value="true"/>
										</c:if>
										<c:set var="nodispatch" value="false"/>
										<c:if test="${partnerPrivate.noDispatch}">
											<c:set var="nodispatch" value="true"/>
										</c:if>
										<td align="right" class="listwhitetext" width="217">Don't send to invoice upload</td>
										<td  class="listwhitetext" width="8px" ><s:checkbox key="partnerPrivate.invoiceUploadCheck" onclick="changeStatus();"  value="${ischecked}" fieldValue="true" tabindex=""/></td>
										<td align="right" class="listwhitetext" width="270">Don't generate unauthorized invoice</td>
				                        <td class="listwhitetext" colspan="3" ><s:checkbox key="partnerPrivate.stopNotAuthorizedInvoices" onclick="changeStatus();"  value="${partnerPrivate.stopNotAuthorizedInvoices}" fieldValue="true" tabindex=""/></td>
				                        <td align="right" class="listwhitetext" width="218px">Don't Dispatch</td>
										<td  class="listwhitetext" width="8px" ><s:checkbox key="partnerPrivate.noDispatch" onclick="changeStatus();"  value="${nodispatch}" fieldValue="true" tabindex=""/></td>
									</tr>
									<tr>
				                     	<c:set var="ischeck" value="false"/>
										<c:if test="${partnerPrivate.payableUploadCheck}">
											<c:set var="ischeck" value="true"/>
										</c:if>
										<c:set var="ischeckAllDriver" value="false"/>
										<c:if test="${partnerPrivate.soAllDrivers}">
											<c:set var="ischeckAllDriver" value="true"/>
										</c:if>
										<td align="right" class="listwhitetext">Don't send to payable upload</td>
										<td  class="listwhitetext" width="8px" ><s:checkbox key="partnerPrivate.payableUploadCheck" onclick="changeStatus();"  value="${ischeck}" fieldValue="true" tabindex=""/></td>
				                     	<td align="right" class="listwhitetext" width="">Don't copy auth # from previous S/O#</td>
				                        <td  class="listwhitetext" colspan="3" ><s:checkbox key="partnerPrivate.doNotCopyAuthorizationSO" onclick="changeStatus();"  value="${partnerPrivate.doNotCopyAuthorizationSO}" fieldValue="true" tabindex=""/></td>
				                        <configByCorp:fieldVisibility componentId="component.field.partner.showAlldrivers">
				                        <td align="right" class="listwhitetext" width="180">Show All Drivers List</td>
										<td  class="listwhitetext" width="8px" ><s:checkbox key="partnerPrivate.soAllDrivers" onclick="changeStatus();"  value="${ischeckAllDriver}" fieldValue="true" tabindex=""/></td>
										</configByCorp:fieldVisibility>
				                    </tr>
									</table>
									</td>
									
									</tr>
				                    <tr>
				                    <td  colspan="7">	
				                    <c:if test="${not empty partnerPublic.id}">
				<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <!--<tr><td align="left" height="5px"></td></tr> 		  
		-->
	
	<div id="newmnav" style="!margin-bottom:5px;">
	<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Expiry&nbsp;List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div>
<div class="spn">&nbsp;</div>
<tr><td align="left" colspan="7">
<div style=" width:892px; overflow-x:auto; overflow-y:auto;">
<s:set name="ownerOperatorList" value="ownerOperatorList" scope="request"/>
<display:table name="ownerOperatorList" class="table" requestURI="" id="ownerOperatorList"  defaultsort="2" pagesize="10" style="width:100%;">
    <display:column title="Expiry&nbsp;For">
    <a href="javascript:openWindow('editOwnerOperatorDetails.html?id=${ownerOperatorList.id}&parentId=${parentId}&bucket=Partner&type=Driver&decorator=popup&popup=true',750,300)">
    <c:out value="${ownerOperatorList.fieldName}"></c:out></a>
    </display:column>
    <display:column property="expiryDate"  title="Expiry&nbsp;Date"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortProperty="required" sortable="true" title="Required" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${ownerOperatorList.required}">checked="checked"</c:if>/>
    </display:column>
	</display:table>
	
<c:if test="${usertype!='ACCOUNT'}">
	<input type="button" class="cssbutton1" onclick="addOperatorOwner();" 
	        value="Add&nbsp;Expirations" style="width:130px; height:25px"/> </c:if>
     
</div>
</td></tr> 
<tr><td height="10" align="left" class="listwhitetext" style="margin: 0px;" colspan="10">
<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
<tr>
									<td class="headtab_left"></td>
									<td class="headtab_center" colspan="10">&nbsp;Emergency&nbsp;Contact</td>
									<td width="28" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_special">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
								</table></td></tr>
<tr>
										<td align="right" class="listwhitetext" width="84px">Contact&nbsp;Name</td>
										<td width="155px"><s:textfield cssClass="input-text" name="partnerPrivate.emergencyContact"  size="20" maxlength="20" onkeyup="regExMatch(this,event);" /></td>
										<td align="right" class="listwhitetext" width="39px">Number</td>
										<td width="150px"><s:textfield cssClass="input-text" name="partnerPrivate.emergencyPhone"  size="14" maxlength="30" onkeydown="return onlyPhoneNumsAllowed(event)" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext" width="43px">Email</td>
										<td><s:textfield cssClass="input-text" name="partnerPrivate.emergencyEmail"  size="34" maxlength="30" onchange="validateEmergencyEmail(this);" onkeyup="regExMatch(this,event);"/></td>
									</tr>
</tbody>
</table>
</c:if>  
</td>   
</tr>
								</tbody>				
							</table>
						</div>
					</td>
				</tr>
				<c:if test="${partnerType == 'OO'}">
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('vninfo')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Corporate Hierarchy</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="vninfo">
					<table class="detailTabLabel" border="0" style="margin: 0px;">
						<tr>
							<td align="left" height="5px" ></td>
						</tr>
						<tr>
						<td align="right" class="listwhitetext">Parent</td>
							<td width="100px"><s:textfield cssClass="input-textUpper" id="agentParentAccount" name="partnerPublic.agentParent" size="7" readonly="true"  maxlength="8"  tabindex="" onkeyup="regExMatch(this,event);"/>
							<c:if test="${usertype!='ACCOUNT'}">
							 <img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('searchPartner.html?&partnerType=OO&parentDataUpdate=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerPublic.agentParentName&fld_code=partnerPublic.agentParent');document.getElementById('agentParentAccount').focus();" src="<c:url value='/images/open-popup.gif'/>" />
							 </c:if>
							 </td>
							<td align="left"><s:textfield  cssStyle="width:250px" cssClass="input-textUpper"  key="partnerPublic.agentParentName"  required="true" readonly="true"  /> </td>
							<td align="right" class="listwhitetext"  width="80px">Merge Into</td>
						<td width="100px"><s:textfield cssClass="input-textUpper"  name="partnerPublic.mergeInto" size="10" readonly="true"  maxlength="8"  tabindex="" />
						</tr>
					</table>
					</div>
					</td>
				</tr>
			</c:if>
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
						<div onClick="javascript:animatedcollapse.toggle('comData')" style="margin: 0px">
							<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
								<tr>
									<td class="headtab_left"></td>
									<td class="headtab_center">&nbsp;Payment Data</td>
									<td width="28" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_special">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
							</table>
						</div>
						<div id="comData">
							<table class="detailTabLabel" border="0" cellpadding="3" cellspacing="0" width="">
								<tbody>
									<tr><td height="5px"></td></tr>
									<tr>
										<td align="right" class="listwhitetext" width="85px">Card Number</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.cardNumber"  size="20" maxlength="16" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext" width="60px">Card Status</td>
										<td width="130px"><s:select cssClass="list-menu" name="partnerPrivate.cardStatus" list="{'','Active','Inactive'}" cssStyle="width:115px" onchange="changeStatus();"/></td>
										<td align="right" class="listwhitetext" width="60px">Account ID</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.accountId"  size="14" maxlength="20" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext" width="60px">Customer ID</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.customerId"  size="14" maxlength="20" onkeyup="regExMatch(this,event);"/></td>
									</tr>
									
									<tr>
									<td align="right" class="listwhitetext"><fmt:message key='accountInfo.paymentMethod' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
									<td class="listwhitetext" colspan="2"><s:select cssClass="list-menu" name="partnerPrivate.paymentMethod" list="%{paytype}" cssStyle="width:148px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>

									<td colspan="5">
										<table style="width: 100%;">
											<tbody>
												<tr>
													<c:set var="ischeckedFP" value="false" />
													<c:if test="${partnerPrivate.fuelPurchase}">
														<c:set var="ischeckedFP" value="true" />
													</c:if>
													<td align="left" style="padding:0px 12px 0px;" class="listwhitetext">Fuel Purchase<s:checkbox key="partnerPrivate.fuelPurchase" value="${ischeckedFP}" fieldValue="true" onclick="changeStatus();" /></td>
												
													<c:set var="ischeckedEC" value="false" />
													<c:if test="${partnerPrivate.expressCash}">
														<c:set var="ischeckedEC" value="true" />
													</c:if>
													<td align="left" style="padding-left:45px;padding-right:24px;" class="listwhitetext">Express Cash<s:checkbox key="partnerPrivate.expressCash" value="${ischeckedEC}" fieldValue="true" onclick="changeStatus();" /></td>
												
													<c:set var="ischeckedYA" value="false" />
													<c:if test="${partnerPrivate.yruAccess}">
														<c:set var="ischeckedYA" value="true" />
													</c:if>
													<td align="center" class="listwhitetext">YRU Access<s:checkbox key="partnerPrivate.yruAccess" value="${ischeckedYA}" fieldValue="true" onclick="changeStatus();" /></td>
												</tr>
											</tbody>
										</table>
										</td>
									</tr>
									<tr>
									<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
								    <td align="right" class="listwhitetext" width="80px">VAT&nbsp;Billing&nbsp;Group<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"></configByCorp:fieldVisibility></td> 
				                    <td align="left"><s:select cssClass="list-menu" name="partnerPrivate.vatBillingGroup" list="%{vatBillingGroups}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:130px" tabindex=""/></td>
								     </configByCorp:fieldVisibility>
									</tr>
								</tbody>				
							</table>
						</div>
					</td>
				</tr>	
			</c:if>
				
								
			<tr>
				<td height="" align="left" class="listwhitetext" colspan="2">
				<c:set var="isBrokerFlag" value="false" /> 
				<c:set var="seaFlag" value="false" /> 
				<c:set var="surfaceFlag" value="false" /> 
				<c:set var="airFlag" value="false" /> 
				<c:if test="${partnerPublic.sea}">
					<c:set var="seaFlag" value="true" />
				</c:if> <c:if test="${partnerPublic.surface}">
					<c:set var="surfaceFlag" value="true" />
				</c:if> <c:if test="${partnerPublic.air}">
					<c:set var="airFlag" value="true" />
				</c:if> <c:choose>
					<c:when test="${partnerType == 'CR'}">
						<s:hidden name="partnerPublic.typeOfVendor" />
						<s:hidden name="partnerPublic.vendorCommission" />
						<s:hidden name="partnerPublic.minimumDFB" />
						<s:hidden name="partnerPrivate.companyDivision" />
						<s:hidden name="partnerPrivate.fleet" />
						<table style="margin-bottom: 0px;width: 100%;">
							<tbody>
								<tr>
									<td colspan="4" align="left">
									<table border="0" style="">
										<tr>
											<td align="right" class="listwhitetext" width="80">Tracking Url:</td>
											<td align="left"><s:textfield cssClass="input-text" name="partnerPublic.trackingUrl" size="34" maxlength="150" onkeyup="regExMatch(this,event);"/></td>
										<td></td><td></td>
										<td align="right"class="listwhitetext">Pay&nbsp;To&nbsp;ID</td>
						               <td><s:textfield cssClass="input-text" name="partnerPrivate.payTo"  size="12" maxlength="20" readonly="true"/>
						               <img align="top" class="openpopup" width="17" height="20" onclick="findPayToFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						                <td align="right"class="listwhitetext">Contact&nbsp;Name</td>
						               <td><s:textfield cssClass="input-text" name="partnerPublic.contactName"  size="24" maxlength="45" onkeyup="regExMatch(this,event);"/>
										</tr>
									</table>
									<table style="">
										<tr>
											<td width="10px"></td>
											<td class="listwhitetext"><b>Carrier Detail</b><font color="red" size="2">*</font></td>
											<td style="margin-left:30px" class="listwhitetext"><s:checkbox key="partnerPublic.sea" value="${seaFlag}" fieldValue="true" tabindex="" />Sea</td>
											<td style="margin-left:10px" class="listwhitetext"><s:checkbox key="partnerPublic.surface" value="${surfaceFlag}" fieldValue="true" tabindex="" />Surface</td>
											<td style="margin-left:10px" class="listwhitetext"><s:checkbox key="partnerPublic.air" value="${airFlag}" fieldValue="true" tabindex="" />Air</td>
											<td align="right" class="listwhitetext"  width="75px">Unigroup&nbsp;Carrier&nbsp;Code</td>
										    <td width="130px"><s:textfield cssClass="input-text" name="partnerPublic.ugwwCarrierCode"cssStyle="width:117px;"  size="12" maxlength="8" /></td>
										</tr>
									</table>
									</td>
								</tr>
								<tr>
								<td colspan="12">
									
				      <c:if test="${not empty partnerPublic.id}">
				<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <div id="newmnav" style="!margin-bottom:5px;">		  		  
		<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Expiry&nbsp;List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div>
<div class="spn">&nbsp;</div>
<tr><td align="left" colspan="7">
<div style=" width:892px; overflow-x:auto; overflow-y:auto;">
<s:set name="ownerOperatorList" value="ownerOperatorList" scope="request"/>
<display:table name="ownerOperatorList" class="table" requestURI="" id="ownerOperatorList"  defaultsort="2" pagesize="10" style="width:870px">
    <display:column title="Expiry&nbsp;For">
    <a href="javascript:openWindow('editOwnerOperatorDetails.html?id=${ownerOperatorList.id}&parentId=${parentId}&bucket=Vendor&type=Tractor&decorator=popup&popup=true',750,300)">
    <c:out value="${ownerOperatorList.fieldName}"></c:out></a>
    </display:column>
    <display:column property="expiryDate"  title="Expiry&nbsp;Date"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortProperty="required" sortable="true" title="Required" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${ownerOperatorList.required}">checked="checked"</c:if>/>
    </display:column>
	</display:table>
	
<c:if test="${usertype!='ACCOUNT'}">
	<input type="button" class="cssbutton1" onclick="addOperatorCarrier();" 
	        value="Add&nbsp;Expirations" style="width:130px; height:25px"/> </c:if>
     
</div>
</td></tr> 
</tbody>
</table>
</c:if>  
<table width="100%" class="detailTabLabel">
<tr>
					<td height="10" align="left" class="listwhitetext" colspan="10" style="margin: 0px;">
							<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
								<tr>
									<td class="headtab_left"></td>
									<td class="headtab_center">&nbsp;Payment Data</td>
									<td width="28" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_special">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
								</table>
								<tr>
										<td align="right" class="listwhitetext" width="75px">Tax&nbsp;ID</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.taxId"  size="15" maxlength="20"  onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext">Tax&nbsp;ID&nbsp;Type</td>
										<td><s:select name="partnerPrivate.taxIdType" cssClass="list-menu" list="%{taxIdType}" cssStyle="width:118px" onchange="changeStatus();"/></td>
										<td align="right" class="listwhitetext" width="130px"><fmt:message key='accountInfo.paymentMethod' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
									  <td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.paymentMethod" list="%{paytype}" cssStyle="width:118px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
									<td align="right" class="listwhitetext" width="130px">1099/CORP</td>
									<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.corp" list="%{corp}" cssStyle="width:118px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
									
									</tr>
									<tr>
									<td align="right" class="listwhitetext"  width="75px">Cargo&nbsp;Insurance</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.cargoInsurance"  size="15" maxlength="10" onchange="onlyFloat(this);" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext"  width="75px">Liability&nbsp;Insurance</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.liabilityInsurance"  size="15" maxlength="10" onchange="onlyFloat(this);" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext"  width="75px">MC#</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.mc"  size="15" maxlength="7" onchange="isNumeric(this);" onkeyup="regExMatch(this,event);"/></td>
										<td align="right" class="listwhitetext"  width="75px">DOT#</td>
										<td width="130px"><s:textfield cssClass="input-text" name="partnerPrivate.dot"  size="15" maxlength="7" onchange="isNumeric(this);" onkeyup="regExMatch(this,event);"/></td>
										
									</tr>
									<tr>
									<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
								    <td align="right" class="listwhitetext" width="80px">VAT&nbsp;Billing&nbsp;Group <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"></configByCorp:fieldVisibility></td>
				                    <td align="left"><s:select cssClass="list-menu" name="partnerPrivate.vatBillingGroup" list="%{vatBillingGroups}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:110px" tabindex=""/></td>
								     </configByCorp:fieldVisibility>
									</tr>
							</table>
								
								</td>
								</tr>
											<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('info')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Information</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="info">
					<table width="100%" class="detailTabLabel">
						<tr>
							<td valign="top">
							<table border="0" class="detailTabLabel" width="">
								<tr>
										<td align="right" class="listwhitetext" width="72">VAT #</td>
										<td class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="partnerPublic.vatNumber"   size="25" maxlength="23" tabindex="12"  onkeyup="regExMatch(this,event);"/></td>
	 								<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
									<td align="right" class="listwhitetext" width="72">RUT#&nbsp;</td>
									<td class="listwhitetext">					
									<s:textfield cssClass="input-text" name="partnerPrivate.rutTaxNumber" id="rutTaxNumber" size="25" maxlength="30" />
									</td>
									</configByCorp:fieldVisibility>
	 								</tr>
									</table>
									</td>
								</tr>
							</table>
							</div>
							</td>
						</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${partnerType == 'AC'}">
						<s:hidden name="partnerPublic.sea" />
						<s:hidden name="partnerPublic.surface" />
						<s:hidden name="partnerPublic.air" />
						<s:hidden name="partnerPublic.typeOfVendor" />
						<%-- <s:hidden name="partnerPublic.vendorCommission" /> --%>
						<s:hidden name="partnerPublic.minimumDFB" />
						<c:if test="${not empty partnerPrivate.hire}">
		 <s:text id="miscellaneouspreApprovalDate1" name="${FormDateValue}"> <s:param name="value" value="partnerPrivate.hire" /></s:text>
			 <s:hidden  name="partnerPrivate.hire" value="%{miscellaneouspreApprovalDate1}" /> 
			 <s:hidden  name="partnerPrivateHire"/>
	 </c:if>
	 <c:if test="${empty partnerPrivate.hire}">
		 <s:hidden name="partnerPrivate.hire"/> 
		  <s:hidden  name="partnerPrivateHire"/>
	 </c:if><c:if test="${not empty partnerPrivate.termination}">
		 <s:text id="miscellaneouspreApprovalDate2" name="${FormDateValue}"> <s:param name="value" value="partnerPrivate.termination" /></s:text>
			 <s:hidden  name="partnerPrivate.termination" value="%{miscellaneouspreApprovalDate2}" /> 
			 <s:hidden name="partnerPrivateTermination"/>
	 </c:if>
	 <c:if test="${empty partnerPrivate.termination}">
		 <s:hidden name="partnerPrivate.termination"/> 
		 <s:hidden name="partnerPrivateTermination"/>
	 </c:if>
					</c:when>
					<c:when test="${partnerType == 'PP'}">
						<!--<s:hidden name="partnerPublic.status" value="Approved" />
						--><c:if test="${not empty partnerPublic.startDate}">
							<fmt:formatDate var="updatedOnFormattedValue" value="${partnerPublic.startDate}" pattern="${displayDateTimeFormat}"/>
							<s:hidden name="partnerPublic.startDate" value="${updatedOnFormattedValue}"/>
						</c:if>
						<s:hidden name="" />
						<!-- For SSCW AND TSFT-->
						<configByCorp:fieldVisibility componentId="component.field.vanline">
							<s:hidden name="partnerPublic.isAccount" />
							<s:hidden name="partnerPublic.isAgent" />
							<s:hidden name="partnerPublic.isVendor" />
							<s:hidden name="partnerPublic.isCarrier" />
							<s:hidden name="partnerPublic.isOwnerOp" />
							<s:hidden name="partnerPublic.isPrivateParty" value="true" />
						</configByCorp:fieldVisibility>
						
						<s:hidden name="partnerPublic.typeOfVendor" />
						<s:hidden name="partnerPublic.vendorCommission" />
						<s:hidden name="partnerPublic.minimumDFB" />
						<s:hidden name="partnerPublic.sea" />
						<s:hidden name="partnerPublic.surface" />
						<s:hidden name="partnerPublic.air" />
					</c:when>
					<c:when test="${partnerType == 'OO'}">
						<c:if test="${not empty partnerPublic.effectiveDate}">
							<s:text id="effectiveDateFormattedValue" name="${FormDateValue}">
								<s:param name="value" value="partnerPublic.effectiveDate" />
							</s:text>
							<td><s:hidden cssClass="input-text" id="effectiveDate" name="partnerPublic.effectiveDate" value="%{effectiveDateFormattedValue}" /></td>
						</c:if>
						<s:hidden name="partnerPublic.sea" />
						<s:hidden name="partnerPublic.surface" />
						<s:hidden name="partnerPublic.air" />
						<s:hidden name="partnerPublic.typeOfVendor" />
						<s:hidden name="partnerPublic.vendorCommission" />
						<s:hidden name="partnerPublic.minimumDFB" />
						<s:hidden name="partnerPrivate.companyDivision" />
					</c:when>
					<c:when test="${partnerType == 'AG' || partnerType == 'VN'}">
					<s:hidden name="partnerPrivate.companyDivision" />
						<s:hidden name="partnerPublic.nationalAccount" />
						<s:hidden name="partnerPublic.billingInstructionMessage" />
						<s:hidden name="partnerPublic.commitionType" />
						<s:hidden name="partnerPrivate.fleet" />
						<c:if test="${partnerType == 'AG'}">
							<s:hidden name="partnerPublic.typeOfVendor" />
							<s:hidden name="partnerPublic.vendorCommission" />
						<s:hidden name="partnerPublic.minimumDFB" />
							<c:if test="${not empty partnerPublic.startDate}">
								<fmt:formatDate var="updatedOnFormattedValue" value="${partnerPublic.startDate}" pattern="${displayDateTimeFormat}"/>
								<s:hidden name="partnerPublic.startDate" value="${updatedOnFormattedValue}"/>
							</c:if>
						</c:if>
						<s:hidden name="partnerPublic.sea" />
						<s:hidden name="partnerPublic.surface" />
						<s:hidden name="partnerPublic.air" />
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose></td>
			</tr>
			<c:if test="${partnerType == 'AG'}">
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('pics'); secImage1();secImage2();secImage3(); secImage4()" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Photographs</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="pics">
					<table border="0" cellpadding="1" cellspacing="1" style="margin:0px;">
						<tbody>
							<tr>
								<td colspan="2">
								<table style="margin:0px;" border="0">
								</table>
								</td>
							</tr>							
							
							<tr>
								<td>								
								<table>
								<tr>
										<c:if test="${!(partnerPublic.location1 == null || partnerPublic.location1 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" class="urClass" src="UserImage?location=${partnerPublic.location1}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location1 == null || partnerPublic.location1 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
									
										<c:if test="${partnerPublic.location1 == null || partnerPublic.location1 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph1</td>
										</c:if>
										<c:if test="${!(partnerPublic.location1 == null || partnerPublic.location1 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph1</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph1();" /></c:if></td>
									</tr>
								</table>
								
								</td>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location2 == null || partnerPublic.location2 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" class="urClass" src="UserImage?location=${partnerPublic.location2}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location2 == null || partnerPublic.location2 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location2 == null || partnerPublic.location2 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph2</td>
										</c:if>
										<c:if test="${!(partnerPublic.location2 == null || partnerPublic.location2 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph2</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file1" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph2();" /></c:if></td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location3 == null || partnerPublic.location3 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" class="urClass" src="UserImage?location=${partnerPublic.location3}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location3 == null || partnerPublic.location3 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location3 == null || partnerPublic.location3 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph3</td>
										</c:if>
										<c:if test="${!(partnerPublic.location3 == null || partnerPublic.location3 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph3</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file2" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph3();" /></c:if></td>
									</tr>
								</table>
								</td>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location4 == null || partnerPublic.location4 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" class="urClass" src="UserImage?location=${partnerPublic.location4}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location4 == null || partnerPublic.location4 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location4 == null || partnerPublic.location4 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph4</td>
										</c:if>
										<c:if test="${!(partnerPublic.location4 == null || partnerPublic.location4 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph4</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file3" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph4();" /></c:if></td>
									</tr>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('description')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Description</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="description">
					<table>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Company Profile</td>
							<td align="left"><s:textarea cssClass="textarea" name="partnerPublic.companyProfile" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Facilities</td>
							<td align="left"><s:textarea cssClass="textarea" name="partnerPublic.companyFacilities" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Capabilities</td>
							<td align="left"><s:textarea cssClass="textarea" name="partnerPublic.companyCapabilities" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Destination Profile</td>
							<td align="left"><s:textarea cssClass="textarea" name="partnerPublic.companyDestiantionProfile" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('info')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Information</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="info">
					<table width="100%" class="detailTabLabel">
						<tr>
							<td valign="top">
							<table border="0" class="detailTabLabel">
								<tr>
									<td colspan="9" width="100%" valign="top">
									<table width="100%" cellspacing="0" cellpadding="2" class="detailTabLabel">
										<tr height="5px"></tr>
										<tr>
											<td align="right"  class="listwhitetext" width="">Facility Size</td>
											<td><s:textfield cssClass="input-text" name="partnerPublic.facilitySizeSQMT" size="14" onchange="onlyFloat(this);convertToFeet()" /><b>(in SQMT)</b></td>
											<td align="right"  class="listwhitetext" width="">Facility Size</td>
											<td><s:textfield cssClass="input-text" name="partnerPublic.facilitySizeSQFT" size="15" onchange="onlyFloat(this);convertToMeters()" /><b>(in SQFT)</b></td>											
											<td align="right"  class="listwhitetext" width="">Harmony</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.utsNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<configByCorp:fieldVisibility componentId="component.field.Alternative.NetworkGroup">
											<td align="left" colspan="2" class="listwhitetext" style="padding-left:60px;" >Network Group<s:checkbox key="partnerPublic.networkGroup" onclick="changeStatus();"  value="${partnerPublic.networkGroup}" fieldValue="true" /></td>
											</configByCorp:fieldVisibility>
											<td align="left" colspan="2" class="listwhitetext" style="padding-left:21px;" >UGWW Network Group<s:checkbox key="partnerPublic.ugwwNetworkGroup " onclick="changeStatus();"  value="${partnerPublic.ugwwNetworkGroup}" fieldValue="true" /></td>
										</tr>
										<tr>
											<td align="right"  class="listwhitetext" width="">FIDI</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right"  class="listwhitetext" width="">OMNI Number</td>
											<td><s:textfield cssClass="input-text" name="partnerPublic.OMNINumber" size="16" onkeyup="regExMatch(this,event);"/></td>
											<configByCorp:fieldVisibility componentId="component.field.Alternative.UTSCompanyType">
											<td align="right"  class="listwhitetext" width="">Harmony Member Type</td>
											<td colspan="5"><s:select cssStyle="width:250px;" cssClass="list-menu" name="partnerPublic.UTSmovingCompanyType" list="%{UTSmovingCompanyType}"  /></td>
										    </configByCorp:fieldVisibility>										    
										</tr>
										<tr>
											<td align="right" class="listwhitetext" width="">AMSA</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">WERC</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">IAM</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>											
											
										</tr>
										<tr>
										
											<td align="right" class="listwhitetext" width="">PAIMA</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.PAIMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">LACMA</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.LACMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">Eurovan Network</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.eurovanNetwork" list="{'Y','N'}" headerKey="" headerValue="" /></td>											
											
										</tr>
										
										<tr>
											<td align="right" valign="top"class="listwhitetext" width="">VanLine Affiliation(s)</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.vanLineAffiliation" list="%{vanlineAffiliations}" value="%{multiplVanlineAffiliations}" multiple="true" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
											<td align="right" valign="top" class="listwhitetext" width="">Service Lines</td>
											<td><s:select cssClass="list-menu" name="partnerPublic.serviceLines" list="%{serviceLines}" multiple="true" value="%{multiplServiceLines}" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
											<td align="right" valign="top" class="listwhitetext" width="">Quality Certifications</td>
											<td colspan="3"><s:select cssClass="list-menu" name="partnerPublic.qualityCertifications" list="%{qualityCertifications}" value="%{multiplequalityCertifications}" multiple="true" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
										</tr>
										<tr>
											<td colspan="8" height="2px"></td>
										</tr>
										<tr>
										<configByCorp:fieldVisibility componentId="component.field.Alternative.BillingCurrency">
										<td align="right"  class="listwhitetext" width="">Default&nbsp;Billing&nbsp;Currency</td>
										<td colspan="2"><s:select cssClass="list-menu" name="partnerPublic.billingCurrency" list="%{billingCurrency}" headerKey="" headerValue="" /></td>
										</configByCorp:fieldVisibility>
										<td colspan="3" align="left" class="listwhitetext" width=""><font style="font-style: italic; font: bold; ">* Use Control + mouse to select multiple <b>Selections</b></font></td>
										</tr>								
										<tr>
											<td></td>
											
										</tr>
										<tr>
										
										<td align="right" class="listwhitetext" width="72">Bank Code
							  			 <configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
							   			<font color="red" size="2">*</font>
							  			 </configByCorp:fieldVisibility>
							  			 </td>
										<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPublic.bankCode" size="20" maxlength="45" tabindex="10" onkeyup="regExMatch(this,event);"/></td>	
																			
 										<td align="right" class="listwhitetext">VAT #
							  			 </td>
										<td class="listwhitetext" ><s:textfield cssClass="input-text" name="partnerPublic.vatNumber" size="25" maxlength="23" tabindex="12" onkeyup="regExMatch(this,event);"/></td>
	 									
	 									<td align="right" class="listwhitetext">Bank Account #
						    				<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
						    					<font color="red" size="2">*</font>
						    				</configByCorp:fieldVisibility>
						    			</td>
						    			 
						    			<td class="listwhitetext" colspan="5"><s:textfield cssClass="input-text" name="partnerPublic.bankAccountNumber" size="35" maxlength="40" tabindex="12" onkeyup="regExMatch(this,event);"/>
						    			<img align="top" width="17" height="20" onclick="bankInfoList('${partnerPublic.partnerCode}',this); reposition();" src="/redsky/images/plus-small.png">
						    			</td>
										</tr>
					</table>
					</td>
					</tr>
					</table>
					</td>
					</tr>
					</table>
					</div>
					</td>
				</tr>
			</c:if>
			<c:if test="${partnerType=='AC'}">
			<c:if test="${usertype!='ACCOUNT'}">
							<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('pics'); secImage1();secImage2();secImage3(); secImage4()" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Logo/Photographs</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="pics">
		<table border="0" cellpadding="1" cellspacing="1" style="margin:0px;">
						<tbody>
							<tr>
								<td colspan="2">
								<table style="margin:0px;" border="0">
								</table>
								</td>
							</tr>
							<tr>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location1 == null || partnerPublic.location1 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" class="urClass" src="UserImage?location=${partnerPublic.location1}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location1 == null || partnerPublic.location1 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location1 == null || partnerPublic.location1 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Logo</td>
										</c:if>
										<c:if test="${!(partnerPublic.location1 == null || partnerPublic.location1 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Logo</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph1();" /></c:if></td>
									</tr>
								</table>
								</td>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location2 == null || partnerPublic.location2 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" class="urClass" src="UserImage?location=${partnerPublic.location2}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location2 == null || partnerPublic.location2 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location2 == null || partnerPublic.location2 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph2</td>
										</c:if>
										<c:if test="${!(partnerPublic.location2 == null || partnerPublic.location2 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph2</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file1" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph2();" /></c:if></td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location3 == null || partnerPublic.location3 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" class="urClass" src="UserImage?location=${partnerPublic.location3}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location3 == null || partnerPublic.location3 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location3 == null || partnerPublic.location3 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph3</td>
										</c:if>
										<c:if test="${!(partnerPublic.location3 == null || partnerPublic.location3 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph3</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file2" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph3();" /></c:if></td>
									</tr>
								</table>
								</td>
								<td>
								<table>
									<tr>
										<c:if test="${!(partnerPublic.location4 == null || partnerPublic.location4 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" class="urClass" src="UserImage?location=${partnerPublic.location4}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${partnerPublic.location4 == null || partnerPublic.location4 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${partnerPublic.location4 == null || partnerPublic.location4 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph4</td>
										</c:if>
										<c:if test="${!(partnerPublic.location4 == null || partnerPublic.location4 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph4</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file3" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph4();" /></c:if></td>
									</tr>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
					</td>
				</tr>
				</c:if>
			<c:if test="${usertype=='ACCOUNT'}">
							<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('pics');" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Logo/Photographs</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="pics">
    								<table width="100%" style="padding-left:10px;">    
								      <tr>
								      	<td colspan="5">
								      		<div class="wrapper-img">
											    <c:if test="${(partnerPublic.location1 == null || partnerPublic.location1 == '')
											    && (partnerPublic.location2 == null || partnerPublic.location2 == '')
											    && (partnerPublic.location3 == null || partnerPublic.location3 == '')
											    && (partnerPublic.location4 == null || partnerPublic.location4 == '')
											    }">
												    <br>
												    <b>No photo available.</b>
											    </c:if>  
      											<c:if test="${!(partnerPublic.location1 == null || partnerPublic.location1 == '')}">      
													<div class="thumbline">
												 		<img id="userImage1" class="urClass"  src="UserImage?location=${partnerPublic.location1}" alt=""  onload="secImage1()" style="border:thin solid #219DD1; vertical-align: middle; "/>
												 	</div>
												</c:if>
												<c:if test="${!(partnerPublic.location2 == null || partnerPublic.location2 == '')}">
													<div class="thumbline">
														<img id="userImage2" class="urClass" src="UserImage?location=${partnerPublic.location2}" alt=""  onload="secImage2()"    style="border:thin solid #219DD1; vertical-align: middle; "/>
													</div>
												</c:if>
												<c:if test="${!(partnerPublic.location3 == null || partnerPublic.location3 == '')}">
													<div class="thumbline">
														<img id="userImage3" class="urClass" src="UserImage?location=${partnerPublic.location3}" alt="" onload="secImage3()"  style="border:thin solid #219DD1; vertical-align: middle; "/>
													</div>
												</c:if>
												<c:if test="${!(partnerPublic.location4 == null || partnerPublic.location4 == '')}">
													<div class="thumbline">
														<img id="userImage4" class="urClass" src="UserImage?location=${partnerPublic.location4}" alt="" onload="secImage4()"   style="border:thin solid #219DD1; vertical-align: middle;"/>
													</div>
												</c:if>
											</div>
      									</td>
      								</tr>     
								</table>					
					
					</div>
					</td>
				</tr>
				</c:if>
				<c:if test="${usertype!='ACCOUNT'}">
					<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('info')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Information</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="info">
					<table width="100%" class="detailTabLabel">
						<tr>
							<td valign="top">
							<table border="0" class="detailTabLabel">
								<tr>
										
										<td align="right" class="listwhitetext" width="72">Bank Code
							  			 </td>
										<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPublic.bankCode" size="20" maxlength="45" tabindex="10" onkeyup="regExMatch(this,event);"/></td>	
																				
 										<td align="right" class="listwhitetext">VAT #
							  			 </td>
										<td class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="partnerPublic.vatNumber" size="25" maxlength="23" tabindex="12" onkeyup="regExMatch(this,event);"/></td>
	 									
	 									<td align="right" class="listwhitetext">Bank Account #</td>
						    			<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPublic.bankAccountNumber" size="35" maxlength="40" tabindex="12" onkeyup="regExMatch(this,event);"/></td>
								
										<configByCorp:fieldVisibility componentId="component.field.Alternative.BillingCurrency">
										<td align="right"  class="listwhitetext" width="">Default&nbsp;Billing&nbsp;Currency</td>
										<td colspan="2"><s:select cssClass="list-menu" name="partnerPublic.billingCurrency" list="%{billingCurrency}" headerKey="" headerValue="" /></td>
										</configByCorp:fieldVisibility>
										 									
	 									</tr>
									</table>
									</td>
								</tr>
							</table>
							</div>
							</td>
						</tr>
						</c:if>
			</c:if>
			<c:if test="${partnerType == 'AC'}">
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('vninfo')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Corporate Hierarchy</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="vninfo">
					<table class="detailTabLabel" border="0" style="margin: 0px;">
						<tr>
							<td align="left" height="5px" ></td>
						</tr>
						<tr>
							<!--<td align="right" class="listwhitetext"><fmt:message key='partner.effectiveDate' /></td>
							<c:if test="${not empty partnerPublic.effectiveDate}">
								<s:text id="partnerEffectiveDateFormattedValue" name="${FormDateValue}">
									<s:param name="value" value="partnerPublic.effectiveDate" />
								</s:text>
								<td width="50px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partnerPublic.effectiveDate" value="%{partnerEffectiveDateFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
								<td align="left" style="padding-top:1px;"><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerPublicForm'].effectiveDate,'calender2',document.forms['partnerPublicForm'].dateFormat.value); return false;" /></td>
							</c:if>
							<c:if test="${empty partnerPublic.effectiveDate}">
								<td width="50px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partnerPublic.effectiveDate" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
								<td align="left" style="padding-top:1px;"><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerPublicForm'].effectiveDate,'calender2',document.forms['partnerPublicForm'].dateFormat.value); return false;" /></td>
							</c:if>
							--><td align="right" class="listwhitetext">Parent Account</td>
							<td width="100px"><s:textfield cssClass="input-textUpper" id="agentParentAccount" name="partnerPublic.agentParent" size="7" readonly="true"  maxlength="8"  tabindex="" onkeyup="regExMatch(this,event);"/>
							<c:if test="${usertype!='ACCOUNT'}">
							 <img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('searchPartner.html?&partnerType=AC&parentDataUpdate=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerPublic.agentParentName&fld_code=partnerPublic.agentParent');document.getElementById('agentParentAccount').focus();" src="<c:url value='/images/open-popup.gif'/>" />
							 </c:if>
							 </td>
							<td align="left"><s:textfield  cssStyle="width:250px" cssClass="input-textUpper"  key="partnerPublic.agentParentName"  required="true" readonly="true"  /> </td>
							
							<td>&nbsp;</td>
							<c:if test="${not empty partnerPublic.id}">
								<c:if test="${usertype!='ACCOUNT'}">
									<c:if test="${partnerType=='AC'}">									
										<td align="left"><input type="button"  id="childAgent" tabindex="24" value="Child Account" class="cssbuttonB"  onclick="window.open('getChildAgents.html?id=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
									</c:if>
								</c:if>
							</c:if>
							<td align="right" class="listwhitetext"  width="80px">Merge Into</td>
						<td width="100px"><s:textfield cssClass="input-textUpper"  name="partnerPublic.mergeInto" size="10" readonly="true"  maxlength="8"  tabindex="" />
						</tr>
					</table>
					</div>
					</td>
				</tr>
			</c:if>
			<c:if test="${partnerType == 'VN' || partnerType == 'AG'}">
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('vninfo')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Corporate Hierarchy</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="vninfo">
					<table class="detailTabLabel" border="0" style="margin: 0px;">
					<tr>
							<td align="left" height="5px" ></td>
						</tr>					
						<tr>
							<!--<td align="right" class="listwhitetext"><fmt:message key='partner.effectiveDate' /></td>
							<c:if test="${not empty partnerPublic.effectiveDate}">
								<s:text id="partnerEffectiveDateFormattedValue2" name="${FormDateValue}">
									<s:param name="value" value="partnerPublic.effectiveDate" />
								</s:text>
								<td width="50px"><s:textfield cssClass="input-text" id="effectiveDate22" readonly="true" name="partnerPublic.effectiveDate" value="%{partnerEffectiveDateFormattedValue2}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
								<td align="left" style="padding-top:1px;"><img id="calender22" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerPublicForm'].effectiveDate22,'calender22',document.forms['partnerPublicForm'].dateFormat.value); return false;" /></td>
							</c:if>
							<c:if test="${empty partnerPublic.effectiveDate}">
								<td width="50px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partnerPublic.effectiveDate" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
								<td align="left" style="padding-top:1px;"><img id="calender22" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerPublicForm'].effectiveDate,'calender22',document.forms['partnerPublicForm'].dateFormat.value); return false;" /></td>
							</c:if>
							--><td align="right" class="listwhitetext"><fmt:message key='partner.agentParent' /></td>
							<td width="100px"><s:textfield cssClass="input-textUpper" name="partnerPublic.agentParent" id="agentParentAccount" size="7" maxlength="8" readonly="true"   tabindex="" onkeyup="regExMatch(this,event);"/> <img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('venderPartners.html?&partnerType=AG&parentDataUpdate=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerPublic.agentParentName&fld_code=partnerPublic.agentParent');document.getElementById('agentParentAccount').focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield  cssStyle="width:250px" cssClass="input-textUpper"  key="partnerPublic.agentParentName"  required="true" readonly="true"  onkeyup="regExMatch(this,event);"/> </td>
						<td align="right" class="listwhitetext"  width="80px">Agent Group</td>
						<c:if test="${partnerType != 'AG'}">
						<td width="100px"><s:textfield cssClass="input-textUpper"  name="partnerPublic.agentGroup" size="13" readonly="true"  maxlength="30"  tabindex="" />
						</c:if>
						<c:if test="${partnerType == 'AG'}">
						<td width="100px"><s:textfield cssClass="input-text"  name="partnerPublic.agentGroup" size="13"   maxlength="30"  tabindex="" />
						</c:if>
						<td align="right" class="listwhitetext"  width="80px">Merge Into</td>
						<td width="100px"><s:textfield cssClass="input-textUpper"  name="partnerPublic.mergeInto" size="10" readonly="true"  maxlength="8"  tabindex="" />
						</tr>
						
					</table>
					</div>
					</td>
				</tr>
			</c:if>
		<!-- #7689-Agent Group Field in Partner Public --> 
			<c:if test="${partnerType != 'VN' && partnerType != 'AG'}">
			<s:hidden name="partnerPublic.agentGroup"></s:hidden>
			</c:if>
			 <c:if test="${partnerType=='AC'}">
			 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			 <tr>
					<td height="10" align="left" class="listwhitetext" colspan="10">
					<div onClick="javascript:animatedcollapse.toggle('partnerportal')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Partner Portal Detail</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="partnerportal">
					<table  cellpadding="1" cellspacing="1" class="detailTabLabel">
					<tr>
							<td align="left" height="5px" ></td>
						</tr>
						<tr>
							<c:set var="isActiveFlag1" value="false" />
							<c:if test="${partnerPublic.partnerPortalActive}">
								<c:set var="isActiveFlag1" value="true" />
							</c:if>
							<c:set var="isActiveFlag" value="false" />
							<c:if test="${partnerPublic.viewChild}">
								<c:set var="isActiveFlag" value="true" />
							</c:if>
							<td width="129" align="right" valign="middle" class="listwhitetext">Partner Portal:</td>
							<td width="1px"><s:checkbox key="partnerPublic.partnerPortalActive" value="${isActiveFlag1}" fieldValue="true" onclick="changeStatus()"  cssStyle="margin:0px" tabindex="21"  /></td>
							<td width="180" align="left"><fmt:message key='customerFile.portalIdActive' /></td>
							
							<c:set var="isActiveDonotSendEmailToAgent" value="false" />
							<c:if test="${partnerPublic.doNotSendEmailtoAgentUser}">
								<c:set var="isActiveDonotSendEmailToAgent" value="true" />
							</c:if>
							
							<td width="129" align="right" valign="middle" class="listwhitetext">Do Not Send Email:</td>
							<td width="1px"><s:checkbox key="partnerPublic.doNotSendEmailtoAgentUser" value="${isActiveDonotSendEmailToAgent}" fieldValue="true" onclick="changeStatus()"  tabindex="21"  /></td>
							<td width="18" align="left">Agent</td>
							
						</tr>
						<tr>
						   <c:if test="${partnerType=='AC'}">
							<td align="right" class="listwhitetext">Associated Account&nbsp;</td>
							</c:if>
							<c:if test="${partnerType=='AG'}">
							<td align="right" class="listwhitetext">Associated Agent&nbsp;</td>
							</c:if>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.associatedAgents" size="30" maxlength="65" tabindex="22" onkeyup="regExMatch(this,event);"/></td>
							<c:if test="${partnerType=='AC'}">
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Account Records:</td>
							</c:if>
							<c:if test="${partnerType=='AG'}">
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Agent Records:</td>
							</c:if>
							<td width="16"><s:checkbox key="partnerPublic.viewChild" value="${isActiveFlag}" fieldValue="true" tabindex="23"  /></td>
							<td><fmt:message key='customerFile.portalIdActive' /></td>
							<c:if test="${not empty partnerPublic.id}">
							<c:if test="${usertype!='ACCOUNT'}">
								<c:if test="${partnerType=='AC'}">
								
									<td align="left"><input type="button"  id="childAgent" tabindex="24" value="Child Account" class="cssbuttonB"  onclick="window.open('getChildAgents.html?id=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
								</c:if>
								<c:if test="${partnerType=='AG'}">
									<td align="left"><input type="button"  id="childAgent" tabindex="24" value="Child Agent" class="cssbuttonB"  onclick="window.open('getChildAgents.html?id=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
								</c:if>
								</c:if>
							</c:if>
							
						</tr>
						
					</table>
					</div>
					</td>
				</tr>
				</configByCorp:fieldVisibility>
			 </c:if>
          <c:if test="${partnerType=='AG'}">
			<tr>
					<td height="10" align="left" class="listwhitetext" colspan="10">
					<div onClick="javascript:animatedcollapse.toggle('partnerportal')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Partner Portal Detail</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="partnerportal">
					<table  cellpadding="1" cellspacing="1" class="detailTabLabel">
					<tr>
							<td align="left" height="5px" ></td>
						</tr>
						<tr>
							<c:set var="isActiveFlag1" value="false" />
							<c:if test="${partnerPublic.partnerPortalActive}">
								<c:set var="isActiveFlag1" value="true" />
							</c:if>
							<c:set var="isActiveFlag" value="false" />
							<c:if test="${partnerPublic.viewChild}">
								<c:set var="isActiveFlag" value="true" />
							</c:if>
							<td width="129" align="right" valign="middle" class="listwhitetext">Partner Portal:</td>
							<td width="1px"><s:checkbox key="partnerPublic.partnerPortalActive" value="${isActiveFlag1}" fieldValue="true" onclick="changeStatus()"  cssStyle="margin:0px" tabindex="21"  /></td>
							<td width="180" align="left"><fmt:message key='customerFile.portalIdActive' /></td>
							
							<c:set var="isActiveDonotSendEmailToAgent" value="false" />
							<c:if test="${partnerPublic.doNotSendEmailtoAgentUser}">
								<c:set var="isActiveDonotSendEmailToAgent" value="true" />
							</c:if>
							
							<td width="129" align="right" valign="middle" class="listwhitetext">Do Not Send Email:</td>
							<td width="1px"><s:checkbox key="partnerPublic.doNotSendEmailtoAgentUser" value="${isActiveDonotSendEmailToAgent}" fieldValue="true" onclick="changeStatus()"  tabindex="21"  /></td>
							<td width="18" align="left">Agent</td>
							
						</tr>
						<tr>
						   <c:if test="${partnerType=='AC'}">
							<td align="right" class="listwhitetext">Associated Account&nbsp;</td>
							</c:if>
							<c:if test="${partnerType=='AG'}">
							<td align="right" class="listwhitetext">Associated Agent&nbsp;</td>
							</c:if>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPublic.associatedAgents" size="30" maxlength="65" tabindex="22" onkeyup="regExMatch(this,event);"/></td>
							<c:if test="${partnerType=='AC'}">
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Account Records:</td>
							</c:if>
							<c:if test="${partnerType=='AG'}">
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Agent Records:</td>
							</c:if>
							<td width="16"><s:checkbox key="partnerPublic.viewChild" value="${isActiveFlag}" fieldValue="true" tabindex="23"  /></td>
							<td><fmt:message key='customerFile.portalIdActive' /></td>
							<c:if test="${not empty partnerPublic.id}">
							<c:if test="${usertype!='ACCOUNT'}">
								<c:if test="${partnerType=='AC'}">
								
									<td align="left"><input type="button"  id="childAgent" tabindex="24" value="Child Account" class="cssbuttonB"  onclick="window.open('getChildAgents.html?id=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
								</c:if>
								<c:if test="${partnerType=='AG'}">
									<td align="left"><input type="button"  id="childAgent" tabindex="24" value="Child Agent" class="cssbuttonB"  onclick="window.open('getChildAgents.html?id=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
								</c:if>
								</c:if>
							</c:if>
							
						</tr>
						
					</table>
					</div>
					</td>
				</tr>
		</c:if>
			<!-- For SSCW AND TSFT-->
			<configByCorp:fieldVisibility componentId="component.field.vanline">
			<c:set var="isAg" value="false" />
			<c:if test="${sessionCorpID =='TSFT' && partnerType=='AG'}">
			<c:set var="isAG" value="true" />
			</c:if>
			<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
			<c:if test="${partnerType == 'AC' || partnerType == 'VN'  || partnerType == 'CR' || partnerType == 'OO'|| isAG}">
				<tr>
					<table style="margin-bottom: 0px;width: 100%;">
							<tbody>
								<tr>
									<td align="center" colspan="15" class="vertlinedata"></td>
								</tr>
								<tr>
									<td height="10px"></td>
								</tr>
								<tr>
									<c:set var="ischeckedPP" value="false" />
									<c:if test="${partnerPublic.isPrivateParty}">
										<c:set var="ischeckedPP" value="true" />
									</c:if>
									<td align="center" class="listwhitetext"><fmt:message key='partner.private' /><s:checkbox key="partnerPublic.isPrivateParty" value="${ischeckedPP}" fieldValue="true" onclick="changeStatus();" /></td>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'AC'}">
											<c:set var="ischeckedAC" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="partnerPublic.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();"  /></td>
										</c:if>
										<c:if test="${partnerType != 'AC'}">
											<c:set var="ischeckedAC" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="partnerPublic.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedAC" value="false" />
										<c:if test="${partnerPublic.isAccount}">
											<c:set var="ischeckedAC" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="partnerPublic.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id && isAG}">
									<c:if test="${partnerType == 'AG'}">
									
									<c:set var="ischeckedAG" value="true" /> 
											<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="partnerPublic.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									<c:if test="${partnerType != 'AG'}">
										
									<c:set var="ischeckedAG" value="false" /> 
											<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="partnerPublic.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<s:hidden name="partnerPublic.isAgent"  />
									</c:if>
									<c:if test="${not empty partnerPublic.id && isAG}">
										<c:set var="ischeckedAG" value="false" /> 
										<c:if test="${partnerPublic.isAgent}">
											<c:set var="ischeckedAG" value="true" /> 
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="partnerPublic.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
									
									<s:hidden name="partnerPublic.isAgent"  />
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'VN'}">
											<c:set var="ischeckedVN" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="partnerPublic.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'VN'}">
										<c:set var="ischeckedVN" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="partnerPublic.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedVN" value="false" />
										<c:if test="${partnerPublic.isVendor}">
											<c:set var="ischeckedVN" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="partnerPublic.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'CR'}">
											<c:set var="ischeckedCR" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="partnerPublic.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'CR'}">
										<c:set var="ischeckedCR" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="partnerPublic.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedCR" value="false" />
										<c:if test="${partnerPublic.isCarrier}">
											<c:set var="ischeckedCR" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="partnerPublic.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'OO'}">
											<c:set var="ischeckedOO" value="true" />
											<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="partnerPublic.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'OO'}">
											<c:set var="ischeckedOO" value="false" />
											<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="partnerPublic.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedOO" value="false" />
										<c:if test="${partnerPublic.isOwnerOp}">
											<c:set var="ischeckedOO" value="true" />
										</c:if>
										<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="partnerPublic.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
								</tr>
							</tbody>
						</table>
				</tr>
			</c:if>
			<c:if test="${partnerType == 'AG'}"> 
			<c:if test="${!isAG}"> 
			<s:hidden name="partnerPublic.isPrivateParty" />
			<s:hidden name="partnerPublic.isAccount" />
			<s:hidden name="partnerPublic.isAgent" />
			<s:hidden name="partnerPublic.isVendor" />
			<s:hidden name="partnerPublic.isCarrier" />
			<s:hidden name="partnerPublic.isOwnerOp" />
			</c:if>
			</c:if>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.partner.AgentListTab"> 
		<c:if test="${!isAG}"> 
			<s:hidden name="partnerPublic.isPrivateParty" />
			<s:hidden name="partnerPublic.isAccount" />
			<s:hidden name="partnerPublic.isAgent" />
			<s:hidden name="partnerPublic.isVendor" />
			<s:hidden name="partnerPublic.isCarrier" />
			<s:hidden name="partnerPublic.isOwnerOp" />
		</c:if>	
		</sec-auth:authComponent>
		</configByCorp:fieldVisibility>
		
		<!-- For STAR-->
		<configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		<c:if test="${partnerType != 'AG'}">
			<tr>
					<table style="margin-bottom: 0px;width: 100%;">
							<tbody>
								<tr>
									<td align="center" colspan="15" class="vertlinedata"></td>
								</tr>
								<tr>
									<td height="10px"></td>
								</tr>
								<tr>
							
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'PP'}">
											<c:set var="ischeckedPP" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.private' /><s:checkbox key="partnerPublic.isPrivateParty" value="${ischeckedPP}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'PP'}">
											<c:set var="ischeckedPP" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.private' /><s:checkbox key="partnerPublic.isPrivateParty" value="${ischeckedPP}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedPP" value="false" />
										<c:if test="${partnerPublic.isPrivateParty}">
											<c:set var="ischeckedPP" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.private' /><s:checkbox key="partnerPublic.isPrivateParty" value="${ischeckedPP}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'AC'}">
											<c:set var="ischeckedAC" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="partnerPublic.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'AC'}">
											<c:set var="ischeckedAC" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="partnerPublic.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedAC" value="false" />
										<c:if test="${partnerPublic.isAccount}">
											<c:set var="ischeckedAC" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="partnerPublic.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'AG'}">
											<!--<c:set var="ischeckedAG" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="partnerPublic.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
										-->
										<s:hidden name="partnerPublic.isAgent" />
										</c:if>
										<c:if test="${partnerType != 'AG'}">
											<!--<c:set var="ischeckedAG" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="partnerPublic.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
										-->
										<s:hidden name="partnerPublic.isAgent" />
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<!--<c:set var="ischeckedAG" value="false" />
										<c:if test="${partnerPublic.isAgent}">
											<c:set var="ischeckedAG" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="partnerPublic.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
									-->
										<s:hidden name="partnerPublic.isAgent" />
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'VN'}">
											<c:set var="ischeckedVN" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="partnerPublic.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'VN'}">
										<c:set var="ischeckedVN" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="partnerPublic.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedVN" value="false" />
										<c:if test="${partnerPublic.isVendor}">
											<c:set var="ischeckedVN" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="partnerPublic.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'CR'}">
											<c:set var="ischeckedCR" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="partnerPublic.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'CR'}">
										<c:set var="ischeckedCR" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="partnerPublic.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedCR" value="false" />
										<c:if test="${partnerPublic.isCarrier}">
											<c:set var="ischeckedCR" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="partnerPublic.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty partnerPublic.id}">
										<c:if test="${partnerType == 'OO'}">
											<c:set var="ischeckedOO" value="true" />
											<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="partnerPublic.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'OO'}">
											<c:set var="ischeckedOO" value="false" />
											<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="partnerPublic.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty partnerPublic.id}">
										<c:set var="ischeckedOO" value="false" />
										<c:if test="${partnerPublic.isOwnerOp}">
											<c:set var="ischeckedOO" value="true" />
										</c:if>
										<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="partnerPublic.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
								
						
								</tr>
							</tbody>
						</table>
				</tr>
				</c:if>	
				
					<c:if test="${partnerType=='AG'}">
								 <s:hidden name="partnerPublic.isPrivateParty" />
				                 <s:hidden name="partnerPublic.isAccount" />
				                 <s:hidden name="partnerPublic.isAgent" />
				                 <s:hidden name="partnerPublic.isVendor" />
				                 <s:hidden name="partnerPublic.isCarrier" />
				                 <s:hidden name="partnerPublic.isOwnerOp" />
					</c:if>
		</configByCorp:fieldVisibility>
		</tbody>
	</table>
	<c:if test="${not empty partnerPublic.id}">
	<c:if test="${partnerType == 'AC'}">
	<c:if test="${usertype!='ACCOUNT'}">
	<table width="100%" border="0" style="margin:0px;padding:0px;">
		<tr>
		<td align="center" colspan="15" class="vertlinedata"></td>
		</tr>	     	
	<tr align="right">
	<td align="right" colspan="2"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="openWindowMethod();"/></td>
	</tr>
	<tr align="right">
	<c:set var="isExcludePublicFromParentUpdate" value="false" />
	<c:if test="${partnerPrivate.excludePublicFromParentUpdate}">
	<c:set var="isExcludePublicFromParentUpdate" value="true" />
	</c:if>
	<td align="right" class="listwhitetext" colspan="2">Exclude from Parent updates&nbsp;
	<s:checkbox key="partnerPrivate.excludePublicFromParentUpdate" id="excludeFPU" value="${isExcludePublicFromParentUpdate}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>
	</tr>											
	</table>
	</c:if>	
	</c:if>
	</c:if>
	</div>
	<div class="bottom-header" style="margin-top:48px;"><span></span></div>
	</div>
	</div>
	</div>
	<table border="0" style="width:800px;">
		<tbody>
			<tr>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn' /></b></td>
				<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partnerPublic.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="partnerPublic.createdOn" value="${containerCreatedOnFormattedValue}" />
				<td width="120px"><fmt:formatDate value="${partnerPublic.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></b></td>
				<c:if test="${not empty partnerPublic.id}">
					<s:hidden name="partnerPublic.createdBy" />
					<td><s:label name="createdBy" value="%{partnerPublic.createdBy}" /></td>
				</c:if>
				<c:if test="${empty partnerPublic.id}">
					<s:hidden name="partnerPublic.createdBy" value="${pageContext.request.remoteUser}" />
					<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>
				<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn' /></b></td>
				<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partnerPublic.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="partnerPublic.updatedOn" value="${containerUpdatedOnFormattedValue}" />
				<td width="120px"><fmt:formatDate value="${partnerPublic.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></b></td>
				<c:if test="${not empty partnerPublic.id}">
					<s:hidden name="partnerPublic.updatedBy" />
					<td style="width:85px"><s:label name="updatedBy" value="%{partnerPublic.updatedBy}" /></td>
				</c:if>
				<c:if test="${empty partnerPublic.id}">
					<s:hidden name="partnerPublic.updatedBy" value="${pageContext.request.remoteUser}" />
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>
			</tr>
		</tbody>
	</table>
	<c:set var="button1"><c:if test="${usertype!='ACCOUNT'}">
			<c:if test="${partnerType!='AG'}">
			<s:submit cssClass="cssbutton" key="button.save" cssStyle="width:60px;height:25px" onclick="return validateForm();" />
			</c:if>
			<c:if test="${partnerType=='AG'}">
			<c:if test="${sessionCorpID !='TSFT'}">
			<s:submit cssClass="cssbutton" key="Send Request" cssStyle="width:90px;height:25px" onclick="return validateForm();"  />
			</c:if>
			<c:if test="${sessionCorpID =='TSFT'}">
			<s:submit cssClass="cssbutton" key="button.save" cssStyle="width:90px;height:25px" onclick="return validateForm();"  />
			</c:if>
			</c:if>
			<c:if test="${not empty partnerPublic.id && partnerType != 'AG' }">
	         	<input type="button" class="cssbutton" onclick="location.href='<c:url value="/editPartnerPublic.html?partnerType=${partnerType}"/>'" value="<fmt:message key="button.add"/>" style="width:60px; height:25px"/>
	       	</c:if>
			<s:reset cssClass="cssbutton" key="Reset" onclick="reset_partner_billingCountry();reset_partner_mailingCountry();reset_partner_terminalCountry();"  cssStyle="width:60px; height:25px " />
			</c:if>
	</c:set>
	<c:out value="${button1}" escapeXml="false" />
	<c:set var="isTrue" value="false" scope="session" />
	<c:if test="${partnerType == 'AG' || partnerType == 'PP' || partnerType == 'CR' || partnerType == 'VN'}">
		<c:if test="${not empty partnerPublic.effectiveDate}">
			<s:text id="effectiveDateFormattedValue" name="${FormDateValue}">
				<s:param name="value" value="partnerPublic.effectiveDate" />
			</s:text>
			<td><s:hidden cssClass="input-text" id="effectiveDate" name="partnerPublic.effectiveDate" value="%{effectiveDateFormattedValue}" /></td>
		</c:if>
		<s:hidden name="partnerPublic.rank" />
	</c:if>
	<c:if test="${partnerType ==  'AC'}">
		<s:hidden name="partnerPublic.rank" />
		<c:if test="${not empty partnerPublic.startDate}">
			<fmt:formatDate var="updatedOnFormattedValue" value="${partnerPublic.startDate}" pattern="${displayDateTimeFormat}"/>
			<s:hidden name="partnerPublic.startDate" value="${updatedOnFormattedValue}"/>
		</c:if>
		<s:hidden name="partnerPublic.terminalAddress1" />
		<s:hidden name="partnerPublic.terminalAddress2" />
		<s:hidden name="partnerPublic.terminalAddress3" />
		<s:hidden name="partnerPublic.terminalAddress4" />
		<s:hidden name="partnerPublic.terminalEmail" />
		<s:hidden name="partnerPublic.terminalCity" />
		<s:hidden name="partnerPublic.terminalFax" />
		<s:hidden name="partnerPublic.terminalZip" />
		<s:hidden name="partnerPublic.terminalCountry" />
		<s:hidden name="partnerPublic.terminalPhone" />
		<s:hidden name="partnerPublic.terminalState" />
		<s:hidden name="partnerPublic.companyProfile" />
		<s:hidden name="partnerPublic.companyFacilities" />
		<s:hidden name="partnerPublic.companyCapabilities" />
		<s:hidden name="partnerPublic.companyDestiantionProfile" />
		<s:hidden name="partnerPublic.serviceRangeKms" />
		<s:hidden name="partnerPublic.serviceRangeMiles" />
		<s:hidden name="partnerPublic.fidiNumber" />
		<s:hidden name="partnerPublic.OMNINumber" />
		<s:hidden name="partnerPublic.IAMNumber" />
		<s:hidden name="partnerPublic.AMSANumber" />
		<s:hidden name="partnerPublic.PAIMA" />
		<s:hidden name="partnerPublic.LACMA" />
		<s:hidden name="partnerPublic.eurovanNetwork" />
		<s:hidden name="partnerPublic.WERCNumber" />
		<s:hidden name="partnerPublic.facilitySizeSQFT" />
		<s:hidden name="partnerPublic.facilitySizeSQMT" />
		<s:hidden name="partnerPublic.qualityCertifications" />
		<s:hidden name="partnerPublic.vanLineAffiliation" />
		<s:hidden name="partnerPublic.serviceLines" />
		<s:hidden name="partnerPublic.isAgent" />
	</c:if>
	<c:if test="${partnerType == 'OO'}">
		<s:hidden name="partnerPublic.terminalAddress1" />
		<s:hidden name="partnerPublic.terminalAddress2" />
		<s:hidden name="partnerPublic.terminalAddress3" />
		<s:hidden name="partnerPublic.terminalAddress4" />
		<s:hidden name="partnerPublic.terminalEmail" />
		<s:hidden name="partnerPublic.terminalCity" />
		<s:hidden name="partnerPublic.terminalFax" />
		<s:hidden name="partnerPublic.terminalZip" />
		<s:hidden name="partnerPublic.terminalCountry" />
		<s:hidden name="partnerPublic.terminalPhone" />
		<s:hidden name="partnerPublic.terminalState" />
	</c:if>
	<c:if test="${partnerType == 'OO' || partnerType == 'CR' || partnerType == 'VN'}">
		<s:hidden name="partnerPublic.companyProfile" />		
		<s:hidden name="partnerPublic.companyFacilities" />
		<s:hidden name="partnerPublic.companyCapabilities" />
		<s:hidden name="partnerPublic.companyDestiantionProfile" />
		<s:hidden name="partnerPublic.serviceRangeKms" />
		<s:hidden name="partnerPublic.serviceRangeMiles" />
		<s:hidden name="partnerPublic.fidiNumber" />
		<s:hidden name="partnerPublic.OMNINumber" />
		<s:hidden name="partnerPublic.IAMNumber" />
		<s:hidden name="partnerPublic.AMSANumber" />
		<s:hidden name="partnerPublic.PAIMA" />
		<s:hidden name="partnerPublic.LACMA" />
		<s:hidden name="partnerPublic.eurovanNetwork" />
		<s:hidden name="partnerPublic.WERCNumber" />
		<s:hidden name="partnerPublic.facilitySizeSQFT" />
		<s:hidden name="partnerPublic.facilitySizeSQMT" />
		<s:hidden name="partnerPublic.qualityCertifications" />
		<s:hidden name="partnerPublic.vanLineAffiliation" />
		<s:hidden name="partnerPublic.serviceLines" />
		<s:hidden name="partnerPublic.isAgent" />
	</c:if>
	
	<c:if test="${partnerType == 'PP'}">		
		<s:hidden name="partnerPublic.companyProfile" />		
		<s:hidden name="partnerPublic.companyFacilities" />
		<s:hidden name="partnerPublic.companyCapabilities" />
		<s:hidden name="partnerPublic.companyDestiantionProfile" />
		<s:hidden name="partnerPublic.serviceRangeKms" />
		<s:hidden name="partnerPublic.serviceRangeMiles" />
		<s:hidden name="partnerPublic.fidiNumber" />		
		<s:hidden name="partnerPublic.IAMNumber" />
		<s:hidden name="partnerPublic.AMSANumber" />
		<s:hidden name="partnerPublic.PAIMA" />
		<s:hidden name="partnerPublic.LACMA" />
		<s:hidden name="partnerPublic.eurovanNetwork" />
		<s:hidden name="partnerPublic.WERCNumber" />
		<s:hidden name="partnerPublic.facilitySizeSQFT" />
		<s:hidden name="partnerPublic.facilitySizeSQMT" />
		<s:hidden name="partnerPublic.qualityCertifications" />
		<s:hidden name="partnerPublic.vanLineAffiliation" />
		<s:hidden name="partnerPublic.serviceLines" />
		<s:hidden name="partnerPublic.aliasName" />
		<s:hidden name="partnerPublic.isAgent" />
		<s:hidden name="partnerPublic.terminalAddress1" />
		<s:hidden name="partnerPublic.terminalAddress2" />
		<s:hidden name="partnerPublic.terminalAddress3" />
		<s:hidden name="partnerPublic.terminalAddress4" />
		<s:hidden name="partnerPublic.terminalEmail" />
		<s:hidden name="partnerPublic.terminalCity" />
		<s:hidden name="partnerPublic.terminalFax" />
		<s:hidden name="partnerPublic.terminalZip" />
		<s:hidden name="partnerPublic.terminalCountry" />
		<s:hidden name="partnerPublic.terminalPhone" />
		<s:hidden name="partnerPublic.terminalState" />

	</c:if>
	
	<c:if test="${partnerType == 'PP' || partnerType == 'CR'}">
		<s:hidden name="partnerPublic.agentParent" />
		
	</c:if>
	<s:hidden name="partnerPublic.reloContact" />
		<s:hidden name="partnerPublic.reloContactEmail" />
		<s:hidden name="partnerPublic.reloServices" />
		
</s:form>
<script type="text/javascript">
   function checkBillingEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['partnerPublicForm'].elements['partnerPublic.billingEmail'].value="";
		        
		 return false;
		 } 
	}
	function checkTerminalEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value="";
		        
		 return false;
		 } 
	}
	function checkMailingEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['partnerPublicForm'].elements['partnerPublic.terminalEmail'].value="";
		        
		 return false;
		 } 
	}
	function checkStoreageEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['partnerPublicForm'].elements['partnerPrivate.storageEmail'].value="";
		        
		 return false;
		 } 
	}
	
	
try{
	<c:if test="${justSayYes=='checked'}">
		GetSelectedItem('Just Say Yes');
	</c:if>
	<c:if test="${qualitySurvey=='checked'}">
		GetSelectedItem('Quality Survey');
	</c:if>
   }
catch(e){}

try{
var param = '<%=session.getAttribute("paramView")%>';
if(param == 'View'){
	document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
}
}
catch(e){}
try{
var  permissionTest  = "";
    <sec-auth:authComponent componentId="module.field.partner.status">  		
        <c:if test="${partnerType != 'AG' && partnerType != 'AC'}">
        permissionTest  = <%=(Integer)request.getAttribute("module.field.partner.status" + "Permission")%>;
        </c:if>
        <c:if test="${partnerType == 'AG' || partnerType == 'AC'}">
        permissionTest  = 0;
        </c:if>
	</sec-auth:authComponent>
	 <c:if test="${partnerType == 'AG'}">
		<sec-auth:authComponent componentId="module.field.partner.statusAgent">  
		     permissionTest  =14;
		</sec-auth:authComponent>
	</c:if>
	 <c:if test="${partnerType == 'AC'}">
	 <sec-auth:authComponent componentId="module.field.partner.statusAccount">  
		permissionTest  = 14;
	</sec-auth:authComponent>
	</c:if>
	if(permissionTest > 2){
		document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = false;
	}else{
		document.forms['partnerPublicForm'].elements['partnerPublic.status'].disabled = true;
		
	}

}
catch(e){}
</script>
<script type="text/javascript">
try{
	var  permissionTest1  = "";
	<sec-auth:authComponent componentId="module.partnerPublic.section.partnerPortalDetail.edit">  		
	 	permissionTest1  = <%=(Integer)request.getAttribute("module.partnerPublic.section.partnerPortalDetail.edit" + "Permission")%>;
	</sec-auth:authComponent>
    if(permissionTest1 > 2){
    	<sec-auth:authComponent componentId="component.standard.accountPotralActivation">
		document.forms['partnerPublicForm'].elements['partnerPublic.partnerPortalActive'].disabled = false;
		</sec-auth:authComponent>
		document.forms['partnerPublicForm'].elements['partnerPublic.associatedAgents'].disabled = false;
		document.forms['partnerPublicForm'].elements['partnerPublic.viewChild'].disabled = false;
		document.getElementById("childAgent").disabled = false;
	}else{
		<sec-auth:authComponent componentId="component.standard.accountPotralActivation">
		document.forms['partnerPublicForm'].elements['partnerPublic.partnerPortalActive'].disabled = true;
		</sec-auth:authComponent>
		document.forms['partnerPublicForm'].elements['partnerPublic.associatedAgents'].readOnly = true;
		document.forms['partnerPublicForm'].elements['partnerPublic.associatedAgents'].className = 'input-textUpper';
		document.forms['partnerPublicForm'].elements['partnerPublic.viewChild'].disabled = true;
		document.getElementById("childAgent").disabled = true;
	}
  }catch(e){}
</script>

<script type="text/javascript">
	try{
	requiredState();	
	autoPopulate_partner_billingCountry(document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry']);
	}catch(e){}
	try{
		 var enbState = '${enbState}';
		 var billCon=document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry'].value;
		 if(billCon=="") {
				document.getElementById('billState').disabled = true;
				document.getElementById('billState').value ="";
		 }else{	
		  		if(enbState.indexOf(billCon)> -1){
		  			getBillingState();
					document.getElementById('billState').disabled = false; 
					document.getElementById('billState').value='${partnerPublic.billingState}';
				}else{
					document.getElementById('billState').disabled = true;
					document.getElementById('billState').value ="";
				} 
			}
		 }catch(e){} 
 </script>
<script type="text/javascript">  
 	try{
 	autoPopulate_partner_terminalCountry(document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry']);
 	}catch(e){}
	try{
		 var enbState = '${enbState}';
		 var termCon=document.forms['partnerPublicForm'].elements['partnerPublic.terminalCountry'].value;
		 if(termCon=="")	 {
				document.getElementById('partnerAddForm_partner_terminalState').disabled = true;
				document.getElementById('partnerAddForm_partner_terminalState').value ="";
		 }else{
		  		if(enbState.indexOf(termCon)> -1){
			  		//#7148 -To add the status drop down for private party partner
		  			getTerminalState();
					document.getElementById('partnerAddForm_partner_terminalState').disabled = false; 
					document.getElementById('partnerAddForm_partner_terminalState').value='${partnerPublic.terminalState}';
				}else{
					document.getElementById('partnerAddForm_partner_terminalState').disabled = true;
					document.getElementById('partnerAddForm_partner_terminalState').value ="";
				} 
			}
		 }catch(e){}
</script>
<script type="text/javascript">  
	try{
	autoPopulate_partner_mailingCountry(document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry']);        
	}catch(e){}
	try{
		 var enbState = '${enbState}';
		 var mailCon=document.forms['partnerPublicForm'].elements['partnerPublic.mailingCountry'].value;
		 if(mailCon=="")	 {
				document.getElementById('partnerAddForm_partner_mailingState').disabled = true;
				document.getElementById('partnerAddForm_partner_mailingState').value ="";
		 }else{
		  		if(enbState.indexOf(mailCon)> -1){
		  		//#7148 -To add the status drop down for private party partner
		  			getMailingState();
					document.getElementById('partnerAddForm_partner_mailingState').disabled = false; 
					document.getElementById('partnerAddForm_partner_mailingState').value='${partnerPublic.mailingState}';
				}else{
					document.getElementById('partnerAddForm_partner_mailingState').disabled = true;
					document.getElementById('partnerAddForm_partner_mailingState').value ="";
				} 
			}
		 }catch(e){}	
</script>
<SCRIPT type="text/javascript">
 try{
 checkVendorType();
 } catch(e){}
</SCRIPT>

<script type="text/javascript">
try{
<c:if test="${donoFlag=='1'}">
document.forms['partnerPublicForm'].elements['partnerPublic.doNotMerge'].disabled = true;
</c:if>
}catch(e){}
</script>
<script type="text/javascript">
try{
secImage1();
}
catch(e){}
try{
secImage2();
}
catch(e){}
try{
secImage3();
}
catch(e){}

try{
secImage4();
}
catch(e){}
</script>
<script type="text/javascript">
try{
	 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	  disabledAll();
	 	</sec-auth:authComponent>
}
catch(e){}
</script>

 <script type="text/javascript">
   setOnSelectBasedMethods(['checkDate()']);
   setCalendarFunctionality();
</script>
 <script type="text/javascript">
try{
<c:if test="${not empty partnerPublic.id && partnerPublic.status=='Approved' && partnerType=='AG'}">
var checkRoleAG = 0;
//Modified By subrat BUG #6471 
<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
	checkRoleAG  = 14;
</sec-auth:authComponent>
if(checkRoleAG < 14){
	disabledAll();
}
</c:if>

<c:if test="${not empty partnerPublic.id && partnerPublic.corpID=='TSFT' && partnerType=='AC'}">
	var checkRole = 0;
	<sec-auth:authComponent componentId="module.field.partner.statusAccount">  
		checkRole  = 14;
	</sec-auth:authComponent>

	if(checkRole < 14){
		disabledAll();
	}
</c:if>
}
catch(e){}

function checkCmmDmmDisable(sessionCorpID){
	if(sessionCorpID != 'TSFT'){
		if(sessionCorpID != 'UTSI'){
			if('${partnerPublic.partnerType}' == 'DMM' || '${partnerPublic.partnerType}' == 'CMM'){
					var elementsLen=document.forms['partnerPublicForm'].elements.length;
					for(i=0;i<=elementsLen-1;i++){
						if(document.forms['partnerPublicForm'].elements[i].type=='text'){
							document.forms['partnerPublicForm'].elements[i].readOnly =true;
							document.forms['partnerPublicForm'].elements[i].className = 'input-textUpper';
						}else if(document.forms['partnerPublicForm'].elements[i].type=='textarea'){
							document.forms['partnerPublicForm'].elements[i].readOnly =true;
							document.forms['partnerPublicForm'].elements[i].className = 'textareaUpper';
						}else if(document.forms['partnerPublicForm'].elements[i].type=='submit' || document.forms['partnerPublicForm'].elements[i].type=='reset' || document.forms['partnerPublicForm'].elements[i].type=='button'){
							document.forms['partnerPublicForm'].elements[i].style.display='none';
						}else{
							document.forms['partnerPublicForm'].elements[i].disabled=true;
						} 
				}
			}
		}
	}
}

<c:if test="${not empty partnerPublic.id && partnerType=='AC'}">
		checkCmmDmmDisable('${sessionCorpID}');
</c:if>
getVatCountryCode(document.forms['partnerPublicForm'].elements['partnerPublic.billingCountry']);

function replaceLocation(){
	 var getURL = window.location.href;
	 var aPosition = getURL.indexOf("savePartnerPublic");
	 if(aPosition != -1 ){
	  var replString = getURL.substr(aPosition,getURL.length-1);
	  getURL=getURL.replace(replString,"editPartnerPublic.html");
	  var urlTemp = getURL+"?id=${partnerPublic.id}&partnerType=${partnerType}";
	     location.replace(urlTemp); 
	 }else{
	  window.location.reload();
	 }
	}
	
function saveValidationForBankInfoById(id){
	 var flag =true;
	if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
		flag=false;
	}
	return flag;
}
	
	function saveValidationForBankInfo(){
			var bankNoList='';
			var currencyList='';
			var id='';
			var valCheck='';
			var flag=true;
			
		    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
		        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
		 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
		         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
		         	  valCheck=document.getElementById(id).value;
		         	  if(valCheck==null || valCheck ==undefined || valCheck==''){
		         	   if(bankNoList==''){
		         		  bankNoList=id+": "+valCheck;
		               }else{
		            	   bankNoList= bankNoList+"~"+id+": "+valCheck;
		               }
		         	  }
		 	       }	
		 	     }else{	   
		 	         id=document.forms['partnerBankInfoDetail'].bankNoList.id;
		 	         valCheck=document.forms['partnerBankInfoDetail'].bankNoList.value;
		 	        if(valCheck==null || valCheck ==undefined || valCheck==''){
		 	       	 bankNoList=id+": "+valCheck;
		 	        }
		 	     }	        
		 	 }
			 currencyList='';
			 id='';
			 valCheck='';
			
		    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
		        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
		 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
		         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
		         	  id=id.replace('ban','cur');
		         	  valCheck=document.getElementById(id).value;
		         	  if(valCheck==null || valCheck ==undefined || valCheck==''){
		         	   if(currencyList==''){
		         		  currencyList=id+": "+valCheck;
		               }else{
		            	   currencyList= currencyList+"~"+id+": "+valCheck;
		               }
		         	  }
		 	       }	
		 	     }else{	   
		 	         id=document.forms['partnerBankInfoDetail'].currencyList.id;
		 	         valCheck=document.forms['partnerBankInfoDetail'].currencyList.value;
		 	        if(valCheck==null || valCheck ==undefined || valCheck==''){
		 	       	 currencyList=id+":"+valCheck;
		 	        }
		 	     }	        
		 	 }
		   
		    if(bankNoList!=''){
		    	var bankArray=bankNoList.split("~");
				for ( var i=0; i<bankArray.length && flag; i++){
					id=bankArray[i].split(":")[0].trim();
					id=id.replace("ban","");
					id=id.replace("cur","");
					try{
					if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
						flag=false;
					}
					}catch(e){
						flag=false;
					}
				}
		    }
		    if(currencyList!=''){
			    var currArray=currencyList.split("~");
				for ( var i=0; i<currArray.length && flag; i++){
					id=currArray[i].split(":")[0].trim();
					id=id.replace("ban","");
					id=id.replace("cur","");
					try{

					if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
						flag=false;
					}
					}catch(e){
						flag=false;
					}
				}
		    }
		   
			return flag;
		
	}
	function addRow(tableID,onlysave) {
		var flag=saveValidationForBankInfo();
		if(!flag){
		alert("Please fill the required details.");
		document.getElementById('addLine').disabled=false;
		document.getElementById('addLine').value="Save&AddLine";
		}else{
			if(onlysave!=""){
		document.getElementById('addLine').disabled=true;
		document.getElementById('addLine').value="Processing....";	
			}
		 setTimeout(function(){saveBankInfo(tableID,onlysave);}, 3000); 
		}
		}
function add(tableID){
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount); 
	var newlineid=document.getElementById('newlineid').value; 
	if(newlineid.trim()==''){
	    newlineid=rowCount;
	  }else{
	  newlineid=newlineid+"~"+rowCount;
      
	  }
	
	document.getElementById('newlineid').value=newlineid;
	 
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("select");
	 element1.setAttribute("class", "list-menu" );
	 element1.id="cur"+rowCount;
	 element1.name="currencyList";
	 element1.style.width="60px"
	var catogeryList='${billingCurrency}';
	catogeryList=catogeryList.replace('{','').replace('}','');
	var catogeryarray=catogeryList.split(",");
	var optioneleBlankCategory= document.createElement("option");
	optioneleBlankCategory.value="";
	optioneleBlankCategory.text=""; 
	element1.options.add(optioneleBlankCategory);  
	for(var i=0;i<catogeryarray.length;i++){
		var value=catogeryarray[i].split("=");
	var optionele= document.createElement("option");
	optionele.value=value[0];
	optionele.text=value[1]; 
	element1.options.add(optionele);             
	}
	cell1.appendChild(element1);

	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "text";
	element2.style.width="100px";
	element2.setAttribute("class", "input-text" );
	element2.id='ban'+rowCount;
	element2.name="bankNoList";
	cell2.appendChild(element2);
	
	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type ="checkbox";
	element3.style.width="30px";
	element3.id='chk'+rowCount;
	element3.name="checkList";
	element3.value=true;
	element3.checked=true;
	cell3.appendChild(element3);

}
function saveBankInfo(tableID,onlysave){
	var bankNoList='';
	var currencyList='';
	var checkList='';
	var id='';
	var valCheck='';
	var newlineid=document.getElementById('newlineid').value; 
    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
         	  valCheck=document.getElementById(id).value;
         	   if(bankNoList==''){
         		  bankNoList=id+": "+valCheck;
               }else{
            	   bankNoList= bankNoList+"~"+id+": "+valCheck;
               }
 	       }	
 	     }else{	   
 	         id=document.forms['partnerBankInfoDetail'].bankNoList.id;
 	         valCheck=document.forms['partnerBankInfoDetail'].bankNoList.value;
 	       	 bankNoList=id+": "+valCheck;
 	     }
       
 	 }
	 currencyList='';
	 id='';
	 valCheck='';
    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
    	
        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
         	  id=id.replace('ban','cur');
         	  valCheck=document.getElementById(id).value;
         	   if(currencyList==''){
         		  currencyList=id+": "+valCheck;
               }else{
            	   currencyList= currencyList+"~"+id+": "+valCheck;
               }
 	       }	
 	     }else{	 
 	         id=document.forms['partnerBankInfoDetail'].currencyList.id;
 	         valCheck=document.forms['partnerBankInfoDetail'].currencyList.value;
 	       	 currencyList=id+":"+valCheck;
 	     }	        
 	 }

   	 checkList='';
	 id='';
	 valCheck='';
	
    if(document.forms['partnerBankInfoDetail'].checkList!=undefined && document.forms['partnerBankInfoDetail'].checkList.size!=0){
        if(document.forms['partnerBankInfoDetail'].checkList.length!=undefined){
 	      for (i=0; i<document.forms['partnerBankInfoDetail'].checkList.length; i++){	        	           
         	   id=document.forms['partnerBankInfoDetail'].checkList[i].id;
         	  valCheck=document.getElementById(id).checked;
         	   if(checkList==''){
         		  checkList=id+": "+valCheck;
               }else{
            	   checkList= checkList+"~"+id+": "+valCheck;
               }
 	       }	
 	     }else{	  
 	         id=document.forms['partnerBankInfoDetail'].checkList.id;
 	         valCheck=document.forms['partnerBankInfoDetail'].checkList.checked;
 	       	 checkList=id+":"+valCheck;
 	     }	        
 	 }
	   var url = "saveBankInfolist.html";
	  var parameters = "ajax=1&decorator=simple&popup=true&newlineid="+newlineid+"&bankPartnerCode=${partnerPublic.partnerCode}&bankNoList="+ encodeURI(bankNoList)+"&currencyList="+ encodeURI(currencyList)+"&checkList="+ encodeURI(checkList);
	  httpBankInfo.open("POST", url, true); 
	  httpBankInfo.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  httpBankInfo.setRequestHeader("Content-length", parameters .length);
	  httpBankInfo.setRequestHeader("Connection", "close");
	  httpBankInfo.onreadystatechange = handleHttpResponseBankInfo;
	  httpBankInfo.onreadystatechange = function(){handleHttpResponseBankInfo(tableID,onlysave);};
	  httpBankInfo.send(parameters);    
}
            
function handleHttpResponseBankInfo(tableID,onlysave){
         if (httpBankInfo.readyState == 4){
           var results = httpBankInfo.responseText
           results = results.trim();
           document.getElementById('newlineid').value='';
	       	var table = document.getElementById(tableID);
    		var rowCount = table.rows.length;
           		ajax_hideTooltip();
           		bankInfoList('${partnerPublic.partnerCode}',position1);
           		if(onlysave!="")
       			{
    	setTimeout(function(){add(tableID);}, 1000); 
       			}
           if(results.length>0){
           } }  } 
var httpBankInfo = getXMLHttpRequestObject();


function updateBankRecord(id){
	var flag=saveValidationForBankInfoById(id);
	if(flag){
		var bankAccountNumber=document.getElementById('ban'+id).value;
		var currency=document.getElementById('cur'+id).value;
		var status=document.getElementById('chk'+id).checked;;
		var fieldId=id;
		var url="updateBankRecordAjax.html?ajax=1&decorator=simple&popup=true&bankPartnerCode=${partnerPublic.partnerCode}&bankAccountNumber="+bankAccountNumber+"&currency="+currency+"&status="+status+"&fieldId="+fieldId;
		updateBankRecordHttp.open("GET", url, true);
		updateBankRecordHttp.onreadystatechange = handleHttpResponseupdateBankRecord;
		updateBankRecordHttp.send(null);
	}
	else{
			alert("Please fill the required details.");
		}
}
function handleHttpResponseupdateBankRecord(){
	   if (updateBankRecordHttp.readyState == 4){
	      var results = updateBankRecordHttp.responseText
	   }
}


function getXMLHttpRequestObject()
{
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}

</script>
<script type="text/javascript">
try{
	<c:if test="${flagHit=='2'}">
	 <c:redirect url="/editPartnerPublic.html?id=${partnerPublic.id}&partnerType=${partnerType}"/>
	</c:if>
}catch(e){}

</script>

<script type="text/javascript">
function validCheck5(targetElement){
	var chArr = new Array("#");
 	for ( var i = 0; i < chArr.length; i++ ) {
 		var origString = document.forms['partnerPublicForm'].elements['partnerPublic.lastName'].value;
 		origString = origString.split(chArr[i]).join(''); 
 		document.forms['partnerPublicForm'].elements['partnerPublic.lastName'].value = origString;
 		}
	}
</script>

<script type="text/javascript">
var position1='';
function bankInfoList(bankPartnerCode,position){
	 var agentCheck=false;
	 <c:if test="${not empty partnerPublic.id && partnerPublic.status=='Approved' && partnerType=='AG'}">
	 	<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
	 	agentCheck=true;
		</sec-auth:authComponent>
	 </c:if>
	 var url="partnerBankInfoDetail.html?ajax=1&decorator=simple&popup=true&bankPartnerCode="+bankPartnerCode+"&agentCheck="+agentCheck;
	 position1=position;
	 ajax_showTooltip(url,position);	
}

function checkEmail() {
		var status = false;     
		var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		     if (document.forms['partnerPublicForm'].elements['partnerPrivate.storageEmail'].value.search(emailRegEx) == -1) {
		    	 document.forms['partnerPublicForm'].elements['partnerPrivate.storageEmail'].value="";
		          alert("Please enter a valid email address.");
		     }
		     else {
		          status = true;
		     }
		     return status;
		}
		
function checkStatus(target){
	var publicStatus=target.value;
	var checkStatus='${containsData}';
	//alert(publicStatus+"~~~~~~~~~"+checkStatus[0]);
	//alert(publicStatus+"--"+checkStatus[1]);
	//alert(publicStatus+"~~~~~~~~~"+checkStatus[2]);
	var CheckPartnerType=checkStatus[2];
	var checkPartnerName="";
	if(CheckPartnerType=='V'){
		checkPartnerName="Vendor Code";
	}
	if(CheckPartnerType=='B'){
		checkPartnerName="Bill To Code";
		
	}
	    	if(publicStatus=='Inactive' && checkStatus[0]=='Y'){
	    		var retVal = confirm("Partnercode has been added in Default Account Line. If you want to continue ? "+checkPartnerName +" would be removed from Default Account Line. ");
	               if( retVal == true ){
	            	   document.forms['partnerPublicForm'].elements['checkCodefromDefaultAccountLine'].value="Y"+"~"+CheckPartnerType;
	                  return true;
	               }
	               else{
	            	   document.forms['partnerPublicForm'].elements['checkCodefromDefaultAccountLine'].value="N"+"~"+CheckPartnerType;
	            	   document.forms['partnerPublicForm'].elements['partnerPublic.status'].value='${partnerPublic.status}';
	                  return false;
	               }
	    		
	    		 return false;
	    	}else{
	    		document.forms['partnerPublicForm'].elements['checkCodefromDefaultAccountLine'].value="N"+"~"+CheckPartnerType;
	   				return true;
	    	}
	  return true;
	  }

    </script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<script language="javascript">

function changeAddressPin()
{ 

	var lastName=document.getElementById('lastName').value;

    var lookupAddress = lastName+" "+getMailingAdress(); 
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': lookupAddress}, function(results, status) {
        if (status === 'OK') {
     	   document.forms['partnerPublicForm'].elements['partnerPublic.latitude'].value=results[0].geometry.location.lat();
           document.forms['partnerPublicForm'].elements['partnerPublic.longitude'].value=results[0].geometry.location.lng();
           
           initMap();
           }});
}


function getMailingAdress(){

    var address1= document.getElementById('mailingAddress1').value; 
	var address2=document.getElementById('mailingAddress2').value;
	var mailingCity=document.getElementById('mailingCity').value;
	var mailingZip=document.getElementById('mailingZip').value;
	var mailingState=document.getElementById('mailingState').value;
	var mailingCountry=document.getElementById('mailingCountry').value;
	
	var mailingAdress = address1+" "+address2+" "+mailingCity+" "+mailingZip+" "+mailingState+" "+mailingCountry;
	
	return mailingAdress;
}
var GOOGLE_MAP_KEY = googlekey;
window.onload = function() { 
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=initMap&key='+GOOGLE_MAP_KEY; 
	  document.body.appendChild(script);
}

function initMap() { 
	
	if (document.forms['partnerPublicForm'].elements['partnerPublic.latitude'].value != '' && document.forms['partnerPublicForm'].elements['partnerPublic.longitude'].value != ''){
	
	var lat = parseFloat(document.forms['partnerPublicForm'].elements['partnerPublic.latitude'].value);    
    var lng = parseFloat(document.forms['partnerPublicForm'].elements['partnerPublic.longitude'].value);
    
    var uluru = {lat: lat, lng: lng};
	var lastName=document.getElementById('lastName').value;

  	var contentString ='<h5>'+lastName+'</h5>'+
      '<div id="bodyContent">'+getMailingAdress()+
      '</div>'+
      '</div>';
      
   	var map = new google.maps.Map(document.getElementById('map'), {zoom: 16, mapTypeControl: false,   streetViewControl: false, center: uluru});
        
   	var marker = new google.maps.Marker({position: uluru, map: map});

    marker.addListener('click', function() {
             infowindow.open(map, marker);           
             });
  
  	var infowindow = new google.maps.InfoWindow({
              content: contentString
             });
	
}}


function reposition()
{
	var mycontentdiv = document.getElementById('ajax_tooltip_content'); 
	var myarrowdiv = document.getElementById('ajax_tooltip_arrow');
	
	mycontentdiv.style.left='-282px';
	
	myarrowdiv.style.left='0px';
	
	myarrowdiv.style.transform = 'rotate(180deg)';
	
	
	
}
function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 
   	
   	function saveNewPinLocation()
   	{
   	if(document.getElementById("lat").value!='' && document.getElementById("lng").value!='')
   	{
   	 document.forms['partnerPublicForm'].elements['partnerPublic.latitude'].value=document.getElementById("lat").value;
     document.forms['partnerPublicForm'].elements['partnerPublic.longitude'].value=document.getElementById("lng").value;
   
     initMap();
     }
   	}
   	

</script>