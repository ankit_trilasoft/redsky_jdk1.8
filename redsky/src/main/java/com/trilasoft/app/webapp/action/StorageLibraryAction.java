package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.BookStorage;
import com.trilasoft.app.model.Storage;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.BookStorageManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.LocationManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.StorageLibraryManager;
import com.trilasoft.app.service.StorageManager;


public class StorageLibraryAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;
	private StorageLibraryManager storageLibraryManager;
	private List storageLibraries;
	private List stoLoRearranges;
	private String sessionCorpID;
	private Long id;
	private StorageLibrary storageLibrary; 
	private RefMasterManager refMasterManager;
	private Map<String, String> storageType;
	private Map<String, String> storageOwner;
    
	private Map<String, String> storageMode;
	private String locationId;
	public StorageLibraryAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public void prepare() throws Exception {
	}

	public String storageLibraryList() {
		storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
		storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
		storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
		storageLibraries = storageLibraryManager.storageLibraryList(sessionCorpID);
		return SUCCESS;
	}

	public String  showForm()
	{
		storageLibrary = new StorageLibrary();
		storageLibrary.setCreatedOn(new Date());
		storageLibrary.setUpdatedOn(new Date());
		storageLibrary.setCorpId(sessionCorpID);
		storageLibrary.setAvailVolumeCbm(new BigDecimal(0));
		storageLibrary.setAvailVolumeCft(new BigDecimal(0));
		storageLibrary.setVolumeCbm(new BigDecimal(0));
		storageLibrary.setVolumeCft(new BigDecimal(0));
		storageLibrary.setUsedVolumeCft(new BigDecimal(0));
		storageLibrary.setUsedVolumeCbm(new BigDecimal(0));
		storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
		storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
		storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
		
		storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
		return SUCCESS;
	}
	
	private List<SystemDefault> sysDefaultDetail;
	private CustomerFileManager customerFileManager;
	private List maxStorage;
	private StorageManager storageManager;
	private Long autoSequenceNumber;
	private BookStorageManager bookStorageManager;
	
    public String save() throws Exception {
	
		if (cancel != null) {
			return "cancel";
		}
		boolean isNew = (storageLibrary.getId() == null);   
		if(isNew){
			storageLibrary.setCreatedOn(new Date());
        }
		storageLibrary.setCorpId(sessionCorpID);				
		storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
		storageLibrary.setUpdatedOn(new Date());
		storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
		storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");	
		storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
		if(storageLibrary.getAvailVolumeCbm()==null || storageLibrary.getAvailVolumeCbm().equals("")){
			storageLibrary.setAvailVolumeCbm(new BigDecimal(0));
		}
		if(storageLibrary.getAvailVolumeCft()==null || storageLibrary.getAvailVolumeCft().equals("")){
			storageLibrary.setAvailVolumeCft(new BigDecimal(0));
		}
		if(storageLibrary.getVolumeCbm()==null || storageLibrary.getVolumeCbm().equals("")){			
			storageLibrary.setVolumeCbm(new BigDecimal(0));
		}
		if(storageLibrary.getVolumeCft()==null || storageLibrary.getVolumeCft().equals("")){
			storageLibrary.setVolumeCft(new BigDecimal(0));
		}
		if(storageLibrary.getUsedVolumeCft()==null || storageLibrary.getUsedVolumeCft().equals("")){
			storageLibrary.setUsedVolumeCft(new BigDecimal(0));
		}
		if(storageLibrary.getUsedVolumeCbm()==null || storageLibrary.getUsedVolumeCbm().equals("")){
			storageLibrary.setUsedVolumeCbm(new BigDecimal(0));
		}
        try
		 {
		storageLibrary=storageLibraryManager.save(storageLibrary);
		if(isNew==true && storageLibrary.getLocationId()!=null && !(storageLibrary.getLocationId().equals(""))){
			Storage storage = new Storage();
			storage.setCreatedOn(new Date());
			storage.setCreatedBy(getRequest().getRemoteUser());
			storage.setUpdatedOn(new Date());
			storage.setUpdatedBy(getRequest().getRemoteUser());
			storage.setVolume(new BigDecimal(0));
			storage.setMeasQuantity(new BigDecimal(0));
			storage.setCorpID(sessionCorpID);
			storage.setLocationId(storageLibrary.getLocationId());
			storage.setStorageId(storageLibrary.getStorageId());
			
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					storage.setUnit(systemDefault.getWeightUnit());
					storage.setVolUnit(systemDefault.getVolumeUnit());
				}
			}

			maxStorage = storageManager.findMaximum();
			if (maxStorage.get(0) == null) {
				autoSequenceNumber = Long.parseLong("1");
			} else {
				autoSequenceNumber = Long.parseLong(maxStorage.get(0).toString()) + 1;
			}
			storage.setIdNum(autoSequenceNumber);
			storage.setShipNumber(sessionCorpID+"000000000");
			storage.setTicket(new Long(0));
			storage=storageManager.save(storage);
			
			BookStorage bookStorageNew = new BookStorage();
			bookStorageNew.setStorage(storage);
			bookStorageNew.setCorpID(sessionCorpID);
			bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
			bookStorageNew.setCreatedOn(new Date());
			bookStorageNew.setUpdatedOn(new Date());
			bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
			bookStorageNew.setWhat("L");
			bookStorageNew.setShipNumber(sessionCorpID+"000000000");
			bookStorageNew.setIdNum(storage.getIdNum());
			bookStorageNew.setDated(new Date());
			
			if (storage.getPieces() == null) {
				bookStorageNew.setPieces(0);
			} else {
				bookStorageNew.setPieces(storage.getPieces());
			}
			bookStorageNew.setStorageId(storageLibrary.getStorageId());
			bookStorageNew.setOldLocation("");
			bookStorageNew.setLocationId(storageLibrary.getLocationId());
			bookStorageNew.setTicket(new Long(0));
			bookStorageManager.save(bookStorageNew);
		}
		}catch(Exception e){
            errorMessage("StorageId already exist, please use another one");	
			return INPUT;
		}
				
		String key = (isNew) ? "Storage Unit has been added successfully." : "Storage Unit has been updated successfully.";
		saveMessage(getText(key));
		return SUCCESS;
	}

	public String delete() {
		try {
			storageLibraryManager.remove(id);			
			storageLibraries = storageLibraryManager.storageLibraryList(sessionCorpID);
			saveMessage(getText("Storage Unit has been deleted successfully."));
		} catch (Exception ex) {
			storageLibraries = storageLibraryManager.storageLibraryList(sessionCorpID);
		}
		storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
		storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
		storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
		return SUCCESS;
	}
	
	public String edit() {
		if(id!=null){
			storageLibrary = storageLibraryManager.get(id);
		}
		storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
		storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
		storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
		return SUCCESS;
	}

	public String search(){		
	storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
	storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
	storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
	//System.out.println(storageLibrary);
	if(storageLibrary!=null){
	storageLibraries=storageLibraryManager.storageLibrarySearch(storageLibrary.getStorageId(),storageLibrary.getStorageType(),storageLibrary.getStorageMode());	
	}else{
		storageLibraries=storageLibraryManager.getAll();	
	}
	return SUCCESS;
   }
	
	public String storageLibraryUnitSearch(){		
	storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
	storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
	storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
	storageLibraries=storageLibraryManager.storageLibraryUnitSearch(storageLibrary.getStorageId(),storageLibrary.getStorageType(),storageLibrary.getStorageMode());
	return SUCCESS;
   }

	public String findStorageLibraryList() {
		storageType = refMasterManager.findByParameter(sessionCorpID, "STORAGE_TYPE");
		storageMode = refMasterManager.findByParameter(sessionCorpID, "STORAGEMODE");
		storageOwner = refMasterManager.findByParameter(sessionCorpID, "OWNER");
		storageLibraries = storageLibraryManager.findStorageLibraryList(sessionCorpID);
		return SUCCESS;
	}
	
	public String findStoLocRearrangeList() {
		stoLoRearranges = storageLibraryManager.findStoLocRearrangeList(sessionCorpID);
		return SUCCESS;
	}
	
	public String searchStoLocRearrange() {
		stoLoRearranges = storageLibraryManager.searchStoLocRearrangeList(sessionCorpID,locationId,storageId);
		return SUCCESS;
	}
	private List ls;
	private String ticket;	
	private String storageId;
	private Set locations;
	private Map<String, String> storageTypeList;
	private LocationManager locationManager;
	@SkipValidation
	public String locationRearrange() {
		storageTypeList = refMasterManager.findByParameter(sessionCorpID, "LOCTYPE");
		ls = locationManager.findByLocationRearrList(sessionCorpID,locationId,type, warehouse);
		locations = new HashSet(ls);
		return SUCCESS;
	}
	private String type;
	private String warehouse;
	/*@SkipValidation
	public String searchLocationRearrange() {		
		ls = locationManager.searchByLocationRearrList(locationId,type, warehouse);
		locations = new HashSet(ls);
		return SUCCESS;
	}*/
	
	
	private static Map<String, String> house;
	private String whouse;
	private String locationId1;
	private List allLocations;
	private static Map<String, String> locTYPE;
	private String locationType;
	public String findAllLocationId(){
		allLocations=locationManager.findAllLocationId(sessionCorpID);
		locTYPE = refMasterManager.findByParameter(sessionCorpID, "LOCTYPE");
		house = refMasterManager.findByParameterWhareHouse(sessionCorpID, "HOUSE");
		return SUCCESS;
		
	}
	
	public String searchLocationForStorage() {
		String locationId = locationId1 != null ? locationId1 : "";
		String type = locationType != null ? locationType : "";
		allLocations = locationManager.searchLocationForStorage(locationId, type,sessionCorpID,whouse);
		locTYPE = refMasterManager.findByParameter(sessionCorpID, "LOCTYPE");
		house = refMasterManager.findByParameterWhareHouse(sessionCorpID, "HOUSE");
		return SUCCESS;
	}
	
	
	public List getStorageLibraries() {
		return storageLibraries;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setStorageLibraryManager(StorageLibraryManager storageLibraryManager) {
		this.storageLibraryManager = storageLibraryManager;
	}

	public StorageLibrary getStorageLibrary() {
		return storageLibrary;
	}

	public void setStorageLibrary(StorageLibrary storageLibrary) {
		this.storageLibrary = storageLibrary;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setStorageLibraries(List storageLibraries) {
		this.storageLibraries = storageLibraries;
	}

	

	public Map<String, String> getStorageMode() {
		return storageMode;
	}

	public void setStorageMode(Map<String, String> storageMode) {
		this.storageMode = storageMode;
	}


	public List getStoLoRearranges() {
		return stoLoRearranges;
	}

	public void setStoLoRearranges(List stoLoRearranges) {
		this.stoLoRearranges = stoLoRearranges;
	}

	
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	public Set getLocations() {
		return locations;
	}

	public void setLocations(Set locations) {
		this.locations = locations;
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public Map<String, String> getStorageTypeList() {
		return storageTypeList;
	}

	public void setStorageTypeList(Map<String, String> storageTypeList) {
		this.storageTypeList = storageTypeList;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public Map<String, String> getStorageType() {
		return storageType;
	}

	public void setStorageType(Map<String, String> storageType) {
		this.storageType = storageType;
	}

	public Map<String, String> getStorageOwner() {
		return storageOwner;
	}

	public void setStorageOwner(Map<String, String> storageOwner) {
		this.storageOwner = storageOwner;
	}

	public List getAllLocations() {
		return allLocations;
	}

	public void setAllLocations(List allLocations) {
		this.allLocations = allLocations;
	}

	public static Map<String, String> getHouse() {
		return house;
	}

	public static void setHouse(Map<String, String> house) {
		StorageLibraryAction.house = house;
	}

	public static Map<String, String> getLocTYPE() {
		return locTYPE;
	}

	public static void setLocTYPE(Map<String, String> locTYPE) {
		StorageLibraryAction.locTYPE = locTYPE;
	}

	public String getLocationId1() {
		return locationId1;
	}

	public void setLocationId1(String locationId1) {
		this.locationId1 = locationId1;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getWhouse() {
		return whouse;
	}

	public void setWhouse(String whouse) {
		this.whouse = whouse;
	}

	
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setStorageManager(StorageManager storageManager) {
		this.storageManager = storageManager;
	}

	public void setBookStorageManager(BookStorageManager bookStorageManager) {
		this.bookStorageManager = bookStorageManager;
	}
	
}
