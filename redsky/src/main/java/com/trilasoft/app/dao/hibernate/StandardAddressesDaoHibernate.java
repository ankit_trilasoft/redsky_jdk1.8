package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.StandardAddressesDao;
import com.trilasoft.app.model.StandardAddresses;


	public class StandardAddressesDaoHibernate extends GenericDaoHibernate<StandardAddresses, Long> implements StandardAddressesDao {   
		  
	    public StandardAddressesDaoHibernate() {   
	        super(StandardAddresses.class);   
	    }   
	    public List searchByDetail(String job,String addressLine1,String city,String countryCode,String state){
	    
	    	return getHibernateTemplate().find("from StandardAddresses where ( job like'%"+job.replaceAll("'", "''").replaceAll(":", "''")+"%' or job is null )  and  ( addressLine1 like '%"+addressLine1.replaceAll("'", "''").replaceAll(":", "''")+"%' or addressLine1 is null ) and ( city like '%"+city.replaceAll("'", "''").replaceAll(":", "''")+"%'  or city is null ) and ( countryCode like '%"+countryCode.replaceAll("'", "''").replaceAll(":", "''")+"%' or countryCode is null ) and ( state like '%"+state.replaceAll("'", "''").replaceAll(":", "''")+"%' or state is null)");
	}
		public List getByJobType(String jobType) {
			// TODO Auto-generated method stub
			return getHibernateTemplate().find("from StandardAddresses where job='"+jobType+"'");
		}
		
		public List searchByDetailForPopup(String job,String addressLine1,String city,String countryCode,String state){
		    
	    	return getHibernateTemplate().find("from StandardAddresses where  job = '"+job+"'   and  ( addressLine1 like '%"+addressLine1.replaceAll("'", "''").replaceAll(":", "''")+"%' or addressLine1 is null ) and ( city like '%"+city.replaceAll("'", "''").replaceAll(":", "''")+"%'  or city is null ) and ( countryCode like '%"+countryCode.replaceAll("'", "''").replaceAll(":", "''")+"%' or countryCode is null ) and ( state like '%"+state.replaceAll("'", "''").replaceAll(":", "''")+"%' or state is null)");
		}
		
		public List getAddressLine1(String jobType, String corpid){
			List addressList = new ArrayList();
			String query = "Select addressline1 from standardaddresses where corpID='"+corpid+"' and (job='"+jobType+"' or job ='')";
			addressList = this.getSession().createSQLQuery(query).list();
			return addressList; 
		}
		
		public StandardAddresses getStandardAddressDetail(String residenceName){
			List addressList = new ArrayList();
			StandardAddresses standardAddresses = new StandardAddresses();
			addressList = getHibernateTemplate().find("from StandardAddresses where addressLine1='"+residenceName+"' ");
			if(addressList!=null && !addressList.isEmpty()){
				standardAddresses = (StandardAddresses) addressList.get(0);
			}
			return standardAddresses;
		}
	
	}
