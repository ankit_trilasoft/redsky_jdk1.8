package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Commission;

public interface CommissionManager extends GenericManager<Commission, Long>{
	Map<Long, Double> getAutoCommisssionMap(String sessionCorpID, String shipNumber); 
	BigDecimal findTotalExpenseOfChargeACC(String sessionCorpID, String shipNumber, String tempAccId);
}
