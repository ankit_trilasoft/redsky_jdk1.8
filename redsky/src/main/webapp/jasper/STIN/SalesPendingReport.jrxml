<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="SalesPendingReport"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="737"
		 pageHeight="612"
		 columnWidth="701"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="0" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Begin Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT	'SalesPendingReport-R-PDF-STIN.jrxml',
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate,
	CONCAT(app_user.`first_name`," ",app_user.`last_name`)AS Estimator,
	if(customerfile.companyDivision is null || customerfile.companyDivision='','',customerfile.companyDivision) AS customerfile_companyDivision,
	customerfile.comptetive AS customerfile_comptetive,
	DATE_FORMAT(customerfile.survey,'%d-%b-%y') AS customerfile_survey,
	customerfile.sequenceNumber AS customerfile_sequenceNumber,
	customerfile.estimator AS customerfile_estimator,
	trim(customerfile.originDayPhone) AS customerfile_originDayPhone,
	DATE_FORMAT(customerfile.moveDate,'%d-%b-%y') AS customerfile_moveDate,
	customerfile.job AS customerfile_job,
	Concat(if(customerfile.prefix is null || trim(customerfile.prefix)='',"",trim(customerfile.prefix)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)=''),trim(customerfile.firstName),concat(" ",trim(customerfile.firstName)))),"",if(customerfile.middleInitial is null || trim(customerfile.middleInitial)='',"",if( (customerfile.prefix is null || trim(customerfile.prefix)='') AND (customerfile.firstName is null || trim(customerfile.firstName)=''),trim(customerfile.middleInitial),concat(" ",trim(customerfile.middleInitial)))),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)='') AND (customerfile.firstName is null || trim(customerfile.firstName)='') AND (customerfile.middleInitial is null || trim(customerfile.middleInitial)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))) AS Customer_name,
	Concat(if(customerfile.originCity is null || trim(customerfile.originCity)='',"",trim(customerfile.originCity)),"",if(customerfile.originState is null || trim(customerfile.originState)='',"",if(customerfile.originCity is null || trim(customerfile.originCity)='',trim(refmaster_co.description),concat(", ",trim(refmaster_co.description))))) AS Customerfile_OriginCityState
FROM customerfile customerfile
	LEFT OUTER JOIN `app_user` app_user ON customerfile.`estimator` = app_user.`username` and  app_user.`corpID` = $P{Corporate ID}
        LEFT OUTER JOIN `refmaster` refmaster_co on customerfile.`originState`= refmaster_co.`code` and refmaster_co.`parameter`='STATE' and refmaster_co.bucket2=customerfile.originCountryCode and refmaster_co.`corpID` in ('TSFT',$P{Corporate ID})
	LEFT OUTER JOIN serviceorder serviceorder on serviceorder.sequencenumber=customerfile.sequencenumber and serviceorder.corpid=$P{Corporate ID}
WHERE customerfile.`corpID` = $P{Corporate ID}
	AND (customerfile.salesStatus="Pending" or customerfile.salesStatus="LostSale")
	AND (customerfile.controlFlag='C' AND customerfile.`movetype`='BookedMove')
	AND customerfile.`survey` >= $P{Begin Date}
	AND customerfile.`survey` <= $P{End Date}
order by customerfile.companyDivision]]></queryString>

	<field name="SalesPendingReport-R-PDF-STIN.jrxml" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="Estimator" class="java.lang.String"/>
	<field name="customerfile_companyDivision" class="java.lang.String"/>
	<field name="customerfile_comptetive" class="java.lang.String"/>
	<field name="customerfile_survey" class="java.lang.String"/>
	<field name="customerfile_sequenceNumber" class="java.lang.String"/>
	<field name="customerfile_estimator" class="java.lang.String"/>
	<field name="customerfile_originDayPhone" class="java.lang.String"/>
	<field name="customerfile_moveDate" class="java.lang.String"/>
	<field name="customerfile_job" class="java.lang.String"/>
	<field name="Customer_name" class="java.lang.String"/>
	<field name="Customerfile_OriginCityState" class="java.lang.String"/>

	<sortField name="customerfile_companyDivision" />
	<sortField name="customerfile_estimator" />
	<sortField name="customerfile_survey" />
	<sortField name="Customer_name" />


		<group  name="CompDiv" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{customerfile_companyDivision}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="100"
						y="2"
						width="401"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="2"
						width="102"
						height="18"
						key="staticText-134"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="estimator" >
			<groupExpression><![CDATA[$F{Estimator}]]></groupExpression>
			<groupHeader>
			<band height="23"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="59"
						y="7"
						width="0"
						height="0"
						key="staticText-110"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Static Text]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="3"
						width="61"
						height="18"
						key="staticText-122"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[Estimator : ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="56"
						y="3"
						width="445"
						height="18"
						forecolor="#990033"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Estimator}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="65"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="606"
						y="47"
						width="95"
						height="18"
						key="staticText-28"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin Day Phone]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="701"
						height="19"
						key="staticText-118"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Customer File Sales Pending Report]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="34"
						y="47"
						width="59"
						height="18"
						key="staticText-123"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Survey Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="164"
						y="47"
						width="167"
						height="18"
						key="staticText-126"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Customer Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="47"
						width="25"
						height="18"
						key="staticText-127"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Job]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="401"
						y="47"
						width="138"
						height="18"
						key="staticText-128"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin City/State]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="539"
						y="47"
						width="68"
						height="18"
						key="staticText-129"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Competitive]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="100"
						y="47"
						width="56"
						height="18"
						key="staticText-130"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Move Date]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="76"
						y="19"
						width="548"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[("Survey between  "+
new SimpleDateFormat("dd-MMM-yy").format($P{Begin Date})+
"  and  "+
new SimpleDateFormat("dd-MMM-yy").format($P{End Date})
)]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="331"
						y="47"
						width="70"
						height="18"
						key="staticText-133"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CF # ]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="17"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="34"
						y="0"
						width="65"
						height="17"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_survey}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="164"
						y="0"
						width="167"
						height="17"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Customer_name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="31"
						height="17"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="101"
						y="0"
						width="62"
						height="17"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_moveDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="566"
						y="0"
						width="23"
						height="17"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_comptetive}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="606"
						y="0"
						width="95"
						height="17"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_originDayPhone}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="409"
						y="0"
						width="150"
						height="17"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Customerfile_OriginCityState}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="331"
						y="0"
						width="70"
						height="17"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_sequenceNumber}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="27"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="540"
						y="8"
						width="123"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="665"
						y="8"
						width="36"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="8"
						width="145"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{currentdate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="165"
						y="8"
						width="374"
						height="19"
						key="staticText-82"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<text><![CDATA[Customer File Sales Pending Report]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="5"
						width="700"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
