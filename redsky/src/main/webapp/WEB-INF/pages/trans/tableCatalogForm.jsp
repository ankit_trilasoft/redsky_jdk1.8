<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>Table Catalog</title>   
    <meta name="heading" content="Table Catlog"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>
    .headtab_bg_center {
     width: 70%;
	}
	div#mydiv{margin-top: -45px;}
</style>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

function validateField()
{
	
	var tableName=document.forms['tableCatalogForm'].elements['tableCatalog.tableName'].value;
	tableName=tableName.trim();
	if(tableName=="")
	{
		alert('Please Enter Table Name.');
		document.forms['tableCatalogForm'].elements['tableCatalog.tableName'].focus();
		return false;
	}
}
</script>	
	</head>
	<s:form id="tableCatalogForm" action="saveTableCatalog" onsubmit="return validateField();" method="post" validate="true">
	<div id="newmnav">
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Table Catalog Details</span></a></li>	  	
		<li><a href="tableCatalogList.html" ><span>Table Catalog List</span></a></li>					  	
	  </ul>
</div>
<div class="spn">&nbsp;</div>
	<s:hidden name="id" value="${tableCatalog.id}" />
	<s:hidden name="tableCatalog.id" />	
	
	<div id="content" align="center">
	<div id="liquid-round">
    <div class="top" style="!margin-top:2px;"><span></span></div>
    <div class="center-content">
	<table cellspacing="1" cellpadding="2" border="0">
	<tr><td class="listwhitetext" align="right">Table Name<font color="red" size="2">*</font></td>
	<td style="width:200px;"><s:textfield cssClass="input-text" key="tableCatalog.tableName" maxlength="45" size="30" /></td>
	<td align="right" class="listwhitetext" >Total Field</td>
	<td><s:textfield cssClass="input-text" key="tableCatalog.totalField" onchange="checkNumber('tableCatalogForm','tableCatalog.totalField','Invalid Number.');" maxlength="5" size="3" /></td>
	</tr>
	
	<tr><td valign="top" class="listwhitetext" >Table Description</td>
	<td colspan="3"><s:textarea name="tableCatalog.tableDescription" cssStyle="width:178px;" rows="4" cssClass="textarea" /></td>
	</tr>
	
	<tr><td class="listwhitetext" >Foreignkey Field1</td><td><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyField1" maxlength="45" size="30" /></td>
	<td class="listwhitetext" >Foreignkey Table1</td>
	<td colspan="2"><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyTable1" maxlength="45" size="30"/></td>
	</tr>
	
    <tr><td class="listwhitetext" >Foreignkey Field2</td>
    <td><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyField2" maxlength="45" size="30"/></td>
    <td class="listwhitetext" >Foreignkey Table2</td>
    <td colspan="2"><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyTable2" maxlength="45" size="30"/></td>
    </tr>
	
	<tr><td class="listwhitetext" >Foreignkey Field3</td>
	<td><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyField3" maxlength="45" size="30" /></td>
	<td class="listwhitetext" >Foreignkey Table3</td>
	<td colspan="2"><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyTable3" maxlength="45" size="30" /></td>
	</tr>
	
	<tr><td class="listwhitetext" >Foreignkey Field4</td>
	<td><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyField4" maxlength="45" size="30" /></td>
	<td class="listwhitetext" >Foreignkey Table4</td>
	<td colspan="2"><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyTable4" maxlength="45" size="30" /></td>
	</tr>
	
	<tr><td class="listwhitetext" >Foreignkey Field5</td>
	<td><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyField5" maxlength="45" size="30" /></td>
	<td class="listwhitetext" >Foreignkey Table5</td>
	<td colspan="2"><s:textfield cssClass="input-text" key="tableCatalog.foreignkeyTable5" maxlength="45" size="30" /></td>
	</tr>
	</table>
	</div><div class="bottom-header"><span></span></div></div></div>
	<table width="90%">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${tableCatalog.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="tableCatalog.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${tableCatalog.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty tableCatalog.id}">
								<s:hidden name="tableCatalog.createdBy"/>
								<td ><s:label name="createdBy" value="%{tableCatalog.createdBy}"/></td>
							</c:if>
							<c:if test="${empty tableCatalog.id}">
								<s:hidden name="tableCatalog.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${tableCatalog.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="tableCatalog.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${tableCatalog.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty tableCatalog.id}">
							<s:hidden name="tableCatalog.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{tableCatalog.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty tableCatalog.id}">
							<s:hidden name="tableCatalog.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
			
			
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick=""/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
	
	</s:form>