package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DsFamilyDetails; 
public interface DsFamilyDetailsManager extends GenericManager<DsFamilyDetails, Long>{

	List getDsFamilyDetailsList(Long id, String sessionCorpID);
	public void setCFileFamilySize(Long id, String sessionCorpID);
	Long findRemoteDsFamilyDetails(Long networkId, Long id);
	void deleteNetworkDsFamilyDetails(Long customerFileId, Long networkId);
	int updateFamilyDetailsNetworkId(Long id);
	public String getDsFamilyDetailsBlankFieldId(String ikeaflag,Long cid, String sessionCorpID);
	public Long getDsFamilyIdByRelationship(Long cid,String sessionCorpID,String relationshipVal);
	public List findRelationshipList(Long cid,String sessionCorpID);
	public int findRelationshipList1(Long cid,String sessionCorpID,Long id);
	public List findSpouseDetails(Long id, String sessionCorpID);
	public int findRelationshipList2(Long cid,String sessionCorpID);
}
