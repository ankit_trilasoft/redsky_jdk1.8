<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
  <head>
    <title>Date Tag (Data Tag) Example!</title>
  </head>
  <body>
    <h1><font color="#000080">Current Date Format</font></h1>
      <table border="1" width="35%" bgcolor="ffffcc">
        <tr>
          <td width="50%"><b><font color="#000080">Date Format</font></b></td>
          <td width="50%"><b><font color="#000080">Date</font></b></td>
        </tr>
        <tr>
          <td width="50%">Day/Month/Year</td>
          <td width="50%"><s:datetimepicker name="currentDate" label="Format (yyyy-MM-dd)" displayFormat="yyyy-MM-dd" /></td>
        </tr>
        <tr>
          <td width="50%">Month/Day/Year</td>
          <td width="50%"><s:date name="currentDate" format="MM/dd/yyyy" /></td>
        </tr>
        <tr>
          <td width="50%">Month/Day/Year</td>
          <td width="50%"><s:date name="currentDate" format="MM/dd/yy" /></td>
        </tr>
        <tr>
          <td width="50%">Month/Day/Year Hour<B>:</B>Minute</td>
          <td width="50%"><s:date name="currentDate" format="MM/dd/yy hh:mm" /></td>
        </tr>
        <tr>
          <td width="50%">Month/Day/Year Hour<B>:</B>Minute<B>:</B>Second</td>
          <td width="50%"><s:date name="currentDate" format="MM/dd/yy hh:mm:ss" /></td>
        </tr>
        <tr>
          <td width="50%">Nice Date (Current Date & Time)</td>
          <td width="50%"><s:date name="currentDate" nice="false" /></td>
        </tr>
        <tr>
          <td width="50%">Nice Date</td>
          <td width="50%"><s:date name="currentDate" nice="true" /></td>
        </tr>
      </table>
      
  </body>
</html> 