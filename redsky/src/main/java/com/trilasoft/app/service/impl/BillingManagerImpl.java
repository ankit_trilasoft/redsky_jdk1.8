/**
 * Implementation of BillingManager interface.
 * Set the Dao for communication with the data layer.
 * @param 		dao
 * This class represents the basic "Billing" object in Redsky that allows for Billing  of Shipment.
 * @Class Name	BillingManagerImpl
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List; 
import org.appfuse.service.impl.GenericManagerImpl; 
import com.trilasoft.app.dao.BillingDao;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.service.BillingManager;

 
public class BillingManagerImpl extends GenericManagerImpl<Billing, Long> implements BillingManager { 
    BillingDao billingDao; 
    
 
    public BillingManagerImpl(BillingDao billingDao) { 
        super(billingDao); 
        this.billingDao = billingDao; 
    } 
 
    public List findByBillToCode(String billToCode, String sessionCorpID) { 
        return billingDao.findByBillToCode(billToCode, sessionCorpID);
    } 
    public List checkById(Long id) { 
        return billingDao.checkById(id); 
    }
    public int updateServiceOrder(String billToC, String billToN, String shipNumber , String vendorCode){
    	return billingDao.updateServiceOrder(billToC, billToN, shipNumber, vendorCode);   
    }
    public List findInsurarFromAccLine(String insurar, String vendorCode, String shipNumber){
    	return billingDao.findInsurarFromAccLine(insurar, vendorCode, shipNumber);   
    }
    public List findDefaultBookingAgentDetail(String sessionCorpID){
    	return billingDao.findDefaultBookingAgentDetail(sessionCorpID);   
    }
    public List findQuantity(String shipNumber)
    {
    	return billingDao.findQuantity(shipNumber);
    }
    public List getChargeRate(String charge,String billingContract,String sessionCorpID){
    	return billingDao.getChargeRate(charge, billingContract,sessionCorpID);
    }
    public List getInsuranceChargeRate(String charge,String billingContract ,String sessionCorpID)
    {
    	return billingDao.getInsuranceChargeRate(charge, billingContract,sessionCorpID);
    }
    public int updateTicket(String shipNumber,String storageMeasurement){
    	return billingDao.updateTicket(shipNumber, storageMeasurement);
    }
    public List insuranceValues(String insuranceOption, String sessionCorpID){
	return billingDao.insuranceValues(insuranceOption,sessionCorpID);
    }
    public List previewStroageBilling(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID)
    {
    	return billingDao.previewStroageBilling(beginDate, endDate, condition, conditionInEx,conditionBookingCodeType,  invoiceBillingDate,sessionCorpID);
    }
    public List previewStroageBillingSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate)
    {
    	return billingDao.previewStroageBillingSearch(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, shipNumbers, firstName, lastName, billsTo, cycles, charges, sessionCorpID,invoiceBillingDate);
    }
    public List<Billing> getShipNumbers(String beginDate, String endDate,String sessionCorpID)
    {
    	return billingDao.getShipNumbers(beginDate, endDate, sessionCorpID);
    }
    public List findBillContractAccountList(String historicalContractsValue,String billToCode, String job, Date createdon, String corpID,String companyDivision, String bookingAgentCode){
    	return billingDao.findBillContractAccountList(historicalContractsValue,billToCode, job, createdon, corpID,companyDivision, bookingAgentCode);
    
    }
    public List<Billing>getByShipNumber(String shipNumber)
    {
    	return billingDao.getByShipNumber(shipNumber);
    }
    
    public int updateBillingFromStorage( String billThrough,String shipNumber, String loginUser, Date updatedOn)
    {
    	return billingDao.updateBillingFromStorage(billThrough, shipNumber, loginUser, updatedOn);
    }

	public List findDateDifference(Date invoiceBillingDate, Date dateFrom) {
		return billingDao.findDateDifference(invoiceBillingDate, dateFrom);
	}
	public List priceContract(String contract){
	return billingDao.priceContract(contract);
}
	public List findReviewStatus(String shipNumber){
		return billingDao.findReviewStatus(shipNumber);
	}
	public List findCompanyCode(String corpID) {
		return billingDao.findCompanyCode(corpID);
	}
	
	public List findBookingAgent(String corpID) {
		return billingDao.findBookingAgent(corpID);
	}

	public List getPrimaryBillToCode(Long id,String corpID) { 
		return billingDao.getPrimaryBillToCode(id, corpID) ;
	}

	public void updatePresentCount(Double countTotalNumber, String sessionCorpID, Long id) {
		billingDao.updatePresentCount(countTotalNumber, sessionCorpID, id);
	}

	public List getPresentCount(String sessionCorpID) {
		return billingDao.getPresentCount(sessionCorpID);
	}

	public List getTotalCount(String sessionCorpID) {
		return billingDao.getTotalCount(sessionCorpID);
	}

	public List getMaxId(String sessionCorpID) {
		return billingDao.getMaxId(sessionCorpID);
	}
	
	public List getDateAdd(Date invoiceBillingDate) {
		return billingDao.getDateAdd(invoiceBillingDate);
	}
	
	public List autoStorageBillingProc(String payMentod, String billToCode, String corpId){
		return billingDao.autoStorageBillingProc(payMentod, billToCode, corpId);
	}

	public List findByBillTo2Code(String billTo2Code, String sessionCorpID) {
		// TODO Auto-generated method stub
		 return billingDao.findByBillTo2Code(billTo2Code, sessionCorpID);
	}

	public List findByPrivateBillToCode(String privatePartyBillingCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		 return billingDao.findByPrivateBillToCode(privatePartyBillingCode, sessionCorpID);
	}

	public List findByVendorCode(String vendorCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		 return billingDao.findByVendorCode(vendorCode, sessionCorpID);
	}

	public void updateStorageMeasurementWT(String storageMeasurement, String shipNumber) {
		billingDao.updateStorageMeasurementWT(storageMeasurement, shipNumber);
	}

	public List accRefComDivision(String companyDivision, String billToCode) {
		return billingDao.accRefComDivision(companyDivision, billToCode);
	}

	public List contractBillingInstCode(String contract)
	{
		return billingDao.contractBillingInstCode(contract);
	}
	
	public List getChargePerValue(String charge, String billingContract){
		return billingDao.getChargePerValue(charge, billingContract);
	}
	public List searchByBillingID(String billToAuthority, String billToReference, String billingId, String billingIdOut){
		return billingDao.searchByBillingID(billToAuthority, billToReference, billingId, billingIdOut);
	}

	public List previewVatStroageBilling(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String invoiceBillingDate, String sessionCorpID) {
		return billingDao.previewVatStroageBilling(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, invoiceBillingDate, sessionCorpID);
	}
	
	public List previewStorageBillingForNetworkAgent(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID, String radioContractType){
		return billingDao.previewStorageBillingForNetworkAgent(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, invoiceBillingDate, sessionCorpID, radioContractType);
	}
	public List previewStorageBillingForNetworkAgentCount(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID, String radioContractType){
		return billingDao.previewStorageBillingForNetworkAgentCount(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, invoiceBillingDate, sessionCorpID, radioContractType);
	}
	public List previewVatStroageBillingSearch(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String shipNumbers, String firstName, String lastName, String billsTo, String cycles, String charges, String sessionCorpID, String invoiceBillingDate) {
		
		return billingDao.previewVatStroageBillingSearch(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, shipNumbers, firstName, lastName, billsTo, cycles, charges, sessionCorpID, invoiceBillingDate);
	}
	public void updateContract(String contract, String sequenceNumber){
		billingDao.updateContract(contract,sequenceNumber);
	}

	public boolean getweeklyBilling(String sessionCorpID) {
		return billingDao.getweeklyBilling(sessionCorpID);
	}

	public List dynamicPreviewVatStroageBilling(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String invoiceBillingDate, String sessionCorpID) {
		return billingDao.dynamicPreviewVatStroageBilling(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, invoiceBillingDate, sessionCorpID);
	}

	public List dynamicPreviewVatStroageBillingSearch(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String shipNumbers, String firstName, String lastName, String billsTo, String cycles, String charges, String sessionCorpID, String invoiceBillingDate) {
		return billingDao.dynamicPreviewVatStroageBillingSearch(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, shipNumbers, firstName, lastName, billsTo, cycles, charges, sessionCorpID, invoiceBillingDate);
	}
	public List baseBuyRate(String insuranceOption, String sessionCorpID){
		return billingDao.baseBuyRate(insuranceOption, sessionCorpID);
	}

	public int updateBillingBilltocode(String billingId, String accountCode, String accountName, String billtocode) {
		return billingDao.updateBillingBilltocode(billingId, accountCode, accountName,billtocode);
	}

	public String findVendorCurrency(String partnerCode, String sessionCorpID) {
		return billingDao.findVendorCurrency(partnerCode, sessionCorpID);
	}
	public List getBillingCurrencyAndExchangeRate(String sessionCorpID,String shipNumber,String contract){
		return billingDao.getBillingCurrencyAndExchangeRate(sessionCorpID,shipNumber,contract);
	}
	
	public int updateAccountLineBillToCode(String billToCode, String billToName, Long sid, String billToCodeAcc) {
		return billingDao.updateAccountLineBillToCode(billToCode, billToName, sid, billToCodeAcc);
	}

	public List previewStorageBillingNetworkAgentSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate, String radioContractType)
    {
    	return billingDao.previewStorageBillingNetworkAgentSearch(beginDate, endDate, condition, conditionInEx, conditionBookingCodeType, shipNumbers, firstName, lastName, billsTo, cycles, charges, sessionCorpID,invoiceBillingDate,radioContractType);
    }

	public int updateAgentBillingFromStorage(String toDates, String shipNumber, String loginUser, Date date,String externalCorpID) {
		return billingDao.updateAgentBillingFromStorage(toDates, shipNumber, loginUser, date,externalCorpID);
	}
	public String findBillToCodeFromBilling(Long sid,String sessionCorpID){
		return billingDao.findBillToCodeFromBilling(sid,sessionCorpID);
	}
	public String findSecBillToCodeFromBilling(Long sid,String sessionCorpID){
		return billingDao.findSecBillToCodeFromBilling(sid,sessionCorpID);
	}
	public String findPrivateBillToCodeFromBilling(Long sid,String sessionCorpID){
		return billingDao.findPrivateBillToCodeFromBilling(sid,sessionCorpID);
	}
	public int updateAccountLineBillToCode1(String billToCode, String billToName, Long sid, String billToCodeAcc1) {
		return billingDao.updateAccountLineBillToCode1(billToCode, billToName, sid, billToCodeAcc1);
	}
	public int updateAccountLineBillToCode2(String billToCode, String billToName, Long sid, String billToCodeAcc2) {
		return billingDao.updateAccountLineBillToCode2(billToCode, billToName, sid, billToCodeAcc2);
	}

	public String getPartnerStorageMail(String partnerCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		//return billingDao.getPartnerStorageMail(partnerCode,sessionCorpID);
		return billingDao.getPartnerStorageMail(partnerCode, sessionCorpID);
		
	}
	public String findBillingIdNameAjax(String billToCode,String sessionCorpID){
		return billingDao.findBillingIdNameAjax(billToCode, sessionCorpID);
	}

	public boolean getDmmInsurancePolicy(String sessionCorpID, String contract) {
		return billingDao.getDmmInsurancePolicy(sessionCorpID, contract);
	}

	public int updateBillingNoInsurance(String billingId, String noInsurance, String updatedBy) {
		return billingDao.updateBillingNoInsurance(billingId, noInsurance, updatedBy);
	}

	public List findReviewStatusAccrue(Long sid) {
		return billingDao.findReviewStatusAccrue(sid);
	}

	public boolean findVendorCode(String corpID, String partnerCode) {
		return billingDao.findVendorCode(corpID, partnerCode);
	}

	public int updateBillingNetworkBilltocode(String billingId, String accountCode, String accountName, String billtocode) {
		return billingDao.updateBillingNetworkBilltocode(billingId, accountCode, accountName, billtocode);
	}

	public int updateAccNetworkBilltocode(String sameInstanceAccId, String accountCode, String accountName, String billtocode, String sameInstanceBillingId) {
		return billingDao.updateAccNetworkBilltocode(sameInstanceAccId, accountCode, accountName, billtocode, sameInstanceBillingId);
	}
	
	public String getSellBuyRate(String billToCode, String sessionCorpID){
		return billingDao.getSellBuyRate(billToCode,sessionCorpID);
	}

	public void updateBillingRevenueRecognition(String postDate, String shipNumber, String loginUser,String corpId) {
		 billingDao.updateBillingRevenueRecognition( postDate,  shipNumber,  loginUser, corpId);
		
	}
	public void updateOIValuation(Double salesTaxForOI, String shipNumber, String loginUser,String corpId){
		billingDao.updateOIValuation(salesTaxForOI, shipNumber, loginUser, corpId);
	}

	public String findBillToCode(Long id, String sessionCorpID) {
		// TODO Auto-generated method stub
		return 	billingDao.findBillToCode(id, sessionCorpID);
	}


	
	}
