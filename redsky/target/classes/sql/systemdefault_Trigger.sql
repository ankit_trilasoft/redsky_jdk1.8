


delimiter $$

CREATE trigger redsky.trigger_add_history_systemdefault
BEFORE UPDATE on redsky.systemdefault
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



 IF (OLD.email <> NEW.email ) THEN
      select CONCAT(fieldNameList,'systemdefault.email~') into fieldNameList;
      select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
      select CONCAT(newValueList,NEW.email,'~') into newValueList;
  END IF;


   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'systemdefault.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;






 IF (OLD.commodity <> NEW.commodity ) THEN
      select CONCAT(fieldNameList,'systemdefault.commodity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.commodity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.commodity,'~') into newValueList;
  END IF;




 IF (OLD.paid <> NEW.paid ) THEN
      select CONCAT(fieldNameList,'systemdefault.paid~') into fieldNameList;
      select CONCAT(oldValueList,OLD.paid,'~') into oldValueList;
      select CONCAT(newValueList,NEW.paid,'~') into newValueList;
  END IF;




 IF ((OLD.postDate <> NEW.postDate) or (OLD.postDate is null and NEW.postDate is not null)
      or (OLD.postDate is not null and NEW.postDate is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.postDate~') into fieldNameList;
       IF(OLD.postDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.postDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.postDate is not null) THEN
        	select CONCAT(oldValueList,OLD.postDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.postDate is not null) THEN
        	select CONCAT(newValueList,NEW.postDate,'~') into newValueList;
      	END IF;

   END IF;




 IF (OLD.accountPath <> NEW.accountPath ) THEN
      select CONCAT(fieldNameList,'systemdefault.accountPath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountPath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountPath,'~') into newValueList;
  END IF;



 IF (OLD.address1 <> NEW.address1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.address1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address1,'~') into newValueList;
  END IF;




 IF (OLD.address2 <> NEW.address2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.address2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address2,'~') into newValueList;
  END IF;



 IF (OLD.address3 <> NEW.address3 ) THEN
      select CONCAT(fieldNameList,'systemdefault.address3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address3,'~') into newValueList;
  END IF;




IF ((OLD.arBatch <> NEW.arBatch) or (OLD.arBatch is null and NEW.arBatch is not null)
      or (OLD.arBatch is not null and NEW.arBatch is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.arBatch~') into fieldNameList;
        IF(OLD.arBatch is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.arBatch is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.arBatch is not null) then
            select CONCAT(oldValueList,OLD.arBatch,'~') into oldValueList;
        END IF;
        IF(NEW.arBatch is not null) then
            select CONCAT(newValueList,NEW.arBatch,'~') into newValueList;
        END IF;
    END IF;




    
 IF (OLD.atlasAgent <> NEW.atlasAgent ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlasAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlasAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlasAgent,'~') into newValueList;
  END IF;


  
 IF (OLD.atlasAuthority <> NEW.atlasAuthority ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlasAuthority~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlasAuthority,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlasAuthority,'~') into newValueList;
  END IF;



  
 IF (OLD.atlasEnd <> NEW.atlasEnd ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlasEnd~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlasEnd,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlasEnd,'~') into newValueList;
  END IF;




  
 IF (OLD.atlasSequence <> NEW.atlasSequence ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlasSequence~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlasSequence,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlasSequence,'~') into newValueList;
  END IF;



  
 IF (OLD.atlsAuthority <> NEW.atlsAuthority ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsAuthority~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsAuthority,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsAuthority,'~') into newValueList;
  END IF;


  
 IF (OLD.atlsBillName <> NEW.atlsBillName ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsBillName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsBillName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsBillName,'~') into newValueList;
  END IF;

  IF (OLD.storageBilling <> NEW.storageBilling ) THEN
      select CONCAT(fieldNameList,'systemdefault.storageBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageBilling,'~') into newValueList;
  END IF;

  IF (OLD.SITBilling <> NEW.SITBilling ) THEN
      select CONCAT(fieldNameList,'systemdefault.SITBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SITBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SITBilling,'~') into newValueList;
  END IF;
  
  IF (OLD.TPSBilling <> NEW.TPSBilling ) THEN
      select CONCAT(fieldNameList,'systemdefault.TPSBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TPSBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TPSBilling,'~') into newValueList;
  END IF;
  
  IF (OLD.SCSBilling <> NEW.SCSBilling ) THEN
      select CONCAT(fieldNameList,'systemdefault.SCSBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCSBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCSBilling,'~') into newValueList;
  END IF;
  
 IF (OLD.atlsBillTo <> NEW.atlsBillTo ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsBillTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsBillTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsBillTo,'~') into newValueList;
  END IF;



  
 IF (OLD.atlsCommand <> NEW.atlsCommand ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsCommand~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsCommand,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsCommand,'~') into newValueList;
  END IF;



  
 IF (OLD.atlsSlsm <> NEW.atlsSlsm ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsSlsm~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsSlsm,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsSlsm,'~') into newValueList;
  END IF;



  
 IF (OLD.atlsSpecification <> NEW.atlsSpecification ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsSpecification~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsSpecification,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsSpecification,'~') into newValueList;
  END IF;



  
 IF (OLD.atlsSrce <> NEW.atlsSrce ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsSrce~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsSrce,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsSrce,'~') into newValueList;
  END IF;




  
 IF (OLD.atlsSrnm <> NEW.atlsSrnm ) THEN
      select CONCAT(fieldNameList,'systemdefault.atlsSrnm~') into fieldNameList;
      select CONCAT(oldValueList,OLD.atlsSrnm,'~') into oldValueList;
      select CONCAT(newValueList,NEW.atlsSrnm,'~') into newValueList;
  END IF;




  
 IF (OLD.authNumber <> NEW.authNumber ) THEN
      select CONCAT(fieldNameList,'systemdefault.authNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.authNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.authNumber,'~') into newValueList;
  END IF;



  
 IF (OLD.backup <> NEW.backup ) THEN
      select CONCAT(fieldNameList,'systemdefault.backup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.backup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.backup,'~') into newValueList;
  END IF;



  
 IF (OLD.beginStars <> NEW.beginStars ) THEN
      select CONCAT(fieldNameList,'systemdefault.beginStars~') into fieldNameList;
      select CONCAT(oldValueList,OLD.beginStars,'~') into oldValueList;
      select CONCAT(newValueList,NEW.beginStars,'~') into newValueList;
  END IF;



  
 IF (OLD.blankAddress <> NEW.blankAddress ) THEN
      select CONCAT(fieldNameList,'systemdefault.blankAddress~') into fieldNameList;
      select CONCAT(oldValueList,OLD.blankAddress,'~') into oldValueList;
      select CONCAT(newValueList,NEW.blankAddress,'~') into newValueList;
  END IF;



  
 IF (OLD.bucket1 <> NEW.bucket1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.bucket1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket1,'~') into newValueList;
  END IF;




  
 IF (OLD.bucket2 <> NEW.bucket2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.bucket2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket2,'~') into newValueList;
  END IF;




  
 IF (OLD.byClient <> NEW.byClient ) THEN
      select CONCAT(fieldNameList,'systemdefault.byClient~') into fieldNameList;
      select CONCAT(oldValueList,OLD.byClient,'~') into oldValueList;
      select CONCAT(newValueList,NEW.byClient,'~') into newValueList;
  END IF;


   IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'systemdefault.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;


   IF (OLD.cmpy <> NEW.cmpy ) THEN
      select CONCAT(fieldNameList,'systemdefault.cmpy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cmpy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cmpy,'~') into newValueList;
  END IF;


   IF (OLD.CMPYABBRV <> NEW.CMPYABBRV ) THEN
      select CONCAT(fieldNameList,'systemdefault.CMPYABBRV~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CMPYABBRV,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CMPYABBRV,'~') into newValueList;
  END IF;



   IF (OLD.company <> NEW.company ) THEN
      select CONCAT(fieldNameList,'systemdefault.company~') into fieldNameList;
      select CONCAT(oldValueList,OLD.company,'~') into oldValueList;
      select CONCAT(newValueList,NEW.company,'~') into newValueList;
  END IF;



   IF (OLD.coordinator <> NEW.coordinator ) THEN
      select CONCAT(fieldNameList,'systemdefault.coordinator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.coordinator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.coordinator,'~') into newValueList;
  END IF;



   IF (OLD.daCode <> NEW.daCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.daCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.daCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.daCode,'~') into newValueList;
  END IF;


   IF (OLD.dispatch <> NEW.dispatch ) THEN
      select CONCAT(fieldNameList,'systemdefault.dispatch~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dispatch,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dispatch,'~') into newValueList;
  END IF;



   IF (OLD.doagntcde <> NEW.doagntcde ) THEN
      select CONCAT(fieldNameList,'systemdefault.doagntcde~') into fieldNameList;
      select CONCAT(oldValueList,OLD.doagntcde,'~') into oldValueList;
      select CONCAT(newValueList,NEW.doagntcde,'~') into newValueList;
  END IF;



   IF (OLD.doIndex <> NEW.doIndex ) THEN
      select CONCAT(fieldNameList,'systemdefault.doIndex~') into fieldNameList;
      select CONCAT(oldValueList,OLD.doIndex,'~') into oldValueList;
      select CONCAT(newValueList,NEW.doIndex,'~') into newValueList;
  END IF;



   IF (OLD.domestic <> NEW.domestic ) THEN
      select CONCAT(fieldNameList,'systemdefault.domestic~') into fieldNameList;
      select CONCAT(oldValueList,OLD.domestic,'~') into oldValueList;
      select CONCAT(newValueList,NEW.domestic,'~') into newValueList;
  END IF;



   IF (OLD.dorecncl <> NEW.dorecncl ) THEN
      select CONCAT(fieldNameList,'systemdefault.dorecncl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dorecncl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dorecncl,'~') into newValueList;
  END IF;


   IF (OLD.doRegistrationNumber <> NEW.doRegistrationNumber ) THEN
      select CONCAT(fieldNameList,'systemdefault.doRegistrationNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.doRegistrationNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.doRegistrationNumber,'~') into newValueList;
  END IF;


   IF (OLD.exclGls <> NEW.exclGls ) THEN
      select CONCAT(fieldNameList,'systemdefault.exclGls~') into fieldNameList;
      select CONCAT(oldValueList,OLD.exclGls,'~') into oldValueList;
      select CONCAT(newValueList,NEW.exclGls,'~') into newValueList;
  END IF;



   IF (OLD.export <> NEW.export ) THEN
      select CONCAT(fieldNameList,'systemdefault.export~') into fieldNameList;
      select CONCAT(oldValueList,OLD.export,'~') into oldValueList;
      select CONCAT(newValueList,NEW.export,'~') into newValueList;
  END IF;



   IF (OLD.fax <> NEW.fax ) THEN
      select CONCAT(fieldNameList,'systemdefault.fax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fax,'~') into newValueList;
  END IF;



   IF (OLD.glContract <> NEW.glContract ) THEN
      select CONCAT(fieldNameList,'systemdefault.glContract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glContract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glContract,'~') into newValueList;
  END IF;



   IF (OLD.glupldForm <> NEW.glupldForm ) THEN
      select CONCAT(fieldNameList,'systemdefault.glupldForm~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glupldForm,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glupldForm,'~') into newValueList;
  END IF;



   IF (OLD.highPc <> NEW.highPc ) THEN
      select CONCAT(fieldNameList,'systemdefault.highPc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.highPc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.highPc,'~') into newValueList;
  END IF;


   IF (OLD.IMPORT <> NEW.IMPORT ) THEN
      select CONCAT(fieldNameList,'systemdefault.IMPORT~') into fieldNameList;
      select CONCAT(oldValueList,OLD.IMPORT,'~') into oldValueList;
      select CONCAT(newValueList,NEW.IMPORT,'~') into newValueList;
  END IF;



   IF (OLD.OInterval <> NEW.OInterval ) THEN
      select CONCAT(fieldNameList,'systemdefault.OInterval~') into fieldNameList;
      select CONCAT(oldValueList,OLD.OInterval,'~') into oldValueList;
      select CONCAT(newValueList,NEW.OInterval,'~') into newValueList;
  END IF;


   IF (OLD.internat <> NEW.internat ) THEN
      select CONCAT(fieldNameList,'systemdefault.internat~') into fieldNameList;
      select CONCAT(oldValueList,OLD.internat,'~') into oldValueList;
      select CONCAT(newValueList,NEW.internat,'~') into newValueList;
  END IF;



   IF (OLD.invcewh <> NEW.invcewh ) THEN
      select CONCAT(fieldNameList,'systemdefault.invcewh~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invcewh,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invcewh,'~') into newValueList;
  END IF;



   IF (OLD.invoicePath <> NEW.invoicePath ) THEN
      select CONCAT(fieldNameList,'systemdefault.invoicePath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invoicePath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invoicePath,'~') into newValueList;
  END IF;



   IF (OLD.lateBegin <> NEW.lateBegin ) THEN
      select CONCAT(fieldNameList,'systemdefault.lateBegin~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lateBegin,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lateBegin,'~') into newValueList;
  END IF;


   IF (OLD.lateEnd <> NEW.lateEnd ) THEN
      select CONCAT(fieldNameList,'systemdefault.lateEnd~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lateEnd,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lateEnd,'~') into newValueList;
  END IF;


  

  
    IF ((OLD.lastAccount <> NEW.lastAccount) or (OLD.lastAccount is null and NEW.lastAccount is not null)
      or (OLD.lastAccount is not null and NEW.lastAccount is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastAccount~') into fieldNameList;
        IF(OLD.lastAccount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastAccount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastAccount is not null) then
            select CONCAT(oldValueList,OLD.lastAccount,'~') into oldValueList;
        END IF;
        IF(NEW.lastAccount is not null) then
            select CONCAT(newValueList,NEW.lastAccount,'~') into newValueList;
        END IF;
    END IF;


    IF ((OLD.lastAccount <> NEW.lastAccount) or (OLD.lastAccount is null and NEW.lastAccount is not null)
      or (OLD.lastAccount is not null and NEW.lastAccount is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastAccount~') into fieldNameList;
        IF(OLD.lastAccount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastAccount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastAccount is not null) then
            select CONCAT(oldValueList,OLD.lastAccount,'~') into oldValueList;
        END IF;
        IF(NEW.lastAccount is not null) then
            select CONCAT(newValueList,NEW.lastAccount,'~') into newValueList;
        END IF;
    END IF;


    IF ((OLD.LASTEQUIP <> NEW.LASTEQUIP) or (OLD.LASTEQUIP is null and NEW.LASTEQUIP is not null)
      or (OLD.LASTEQUIP is not null and NEW.LASTEQUIP is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.LASTEQUIP~') into fieldNameList;
        IF(OLD.LASTEQUIP is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.LASTEQUIP is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.LASTEQUIP is not null) then
            select CONCAT(oldValueList,OLD.LASTEQUIP,'~') into oldValueList;
        END IF;
        IF(NEW.LASTEQUIP is not null) then
            select CONCAT(newValueList,NEW.LASTEQUIP,'~') into newValueList;
        END IF;
    END IF;



    IF ((OLD.lastIn <> NEW.lastIn) or (OLD.lastIn is null and NEW.lastIn is not null)
      or (OLD.lastIn is not null and NEW.lastIn is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastIn~') into fieldNameList;
        IF(OLD.lastIn is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastIn is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastIn is not null) then
            select CONCAT(oldValueList,OLD.lastIn,'~') into oldValueList;
        END IF;
        IF(NEW.lastIn is not null) then
            select CONCAT(newValueList,NEW.lastIn,'~') into newValueList;
        END IF;
    END IF;



    IF ((OLD.lastJob <> NEW.lastJob) or (OLD.lastJob is null and NEW.lastJob is not null)
      or (OLD.lastJob is not null and NEW.lastJob is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastJob~') into fieldNameList;
        IF(OLD.lastJob is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastJob is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastJob is not null) then
            select CONCAT(oldValueList,OLD.lastJob,'~') into oldValueList;
        END IF;
        IF(NEW.lastJob is not null) then
            select CONCAT(newValueList,NEW.lastJob,'~') into newValueList;
        END IF;
    END IF;



    IF ((OLD.lastLoss <> NEW.lastLoss) or (OLD.lastLoss is null and NEW.lastLoss is not null)
      or (OLD.lastLoss is not null and NEW.lastLoss is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastLoss~') into fieldNameList;
        IF(OLD.lastLoss is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastLoss is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastLoss is not null) then
            select CONCAT(oldValueList,OLD.lastLoss,'~') into oldValueList;
        END IF;
        IF(NEW.lastLoss is not null) then
            select CONCAT(newValueList,NEW.lastLoss,'~') into newValueList;
        END IF;
    END IF;



    IF ((OLD.lastMaster <> NEW.lastMaster) or (OLD.lastMaster is null and NEW.lastMaster is not null)
      or (OLD.lastMaster is not null and NEW.lastMaster is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastMaster~') into fieldNameList;
        IF(OLD.lastMaster is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastMaster is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastMaster is not null) then
            select CONCAT(oldValueList,OLD.lastMaster,'~') into oldValueList;
        END IF;
        IF(NEW.lastMaster is not null) then
            select CONCAT(newValueList,NEW.lastMaster,'~') into newValueList;
        END IF;
    END IF;



    IF ((OLD.lastOut <> NEW.lastOut) or (OLD.lastOut is null and NEW.lastOut is not null)
      or (OLD.lastOut is not null and NEW.lastOut is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastOut~') into fieldNameList;
        IF(OLD.lastOut is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastOut is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastOut is not null) then
            select CONCAT(oldValueList,OLD.lastOut,'~') into oldValueList;
        END IF;
        IF(NEW.lastOut is not null) then
            select CONCAT(newValueList,NEW.lastOut,'~') into newValueList;
        END IF;
    END IF;



    IF ((OLD.lastSequence <> NEW.lastSequence) or (OLD.lastSequence is null and NEW.lastSequence is not null)
      or (OLD.lastSequence is not null and NEW.lastSequence is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lastSequence~') into fieldNameList;
        IF(OLD.lastSequence is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lastSequence is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lastSequence is not null) then
            select CONCAT(oldValueList,OLD.lastSequence,'~') into oldValueList;
        END IF;
        IF(NEW.lastSequence is not null) then
            select CONCAT(newValueList,NEW.lastSequence,'~') into newValueList;
        END IF;
    END IF;


IF ((OLD.lastStars <> NEW.lastStars) or (OLD.lastStars is null and NEW.lastStars is not null)
      or (OLD.lastStars is not null and NEW.lastStars is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.lastStars~') into fieldNameList;
       IF(OLD.lastStars is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.lastStars is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.lastStars is not null) THEN
        	select CONCAT(oldValueList,OLD.lastStars,'~') into oldValueList;
     	END IF;
      	IF(NEW.lastStars is not null) THEN
        	select CONCAT(newValueList,NEW.lastStars,'~') into newValueList;
      	END IF;

   END IF;



    IF (OLD.loadAtLast <> NEW.loadAtLast ) THEN
      select CONCAT(fieldNameList,'systemdefault.loadAtLast~') into fieldNameList;
      select CONCAT(oldValueList,OLD.loadAtLast,'~') into oldValueList;
      select CONCAT(newValueList,NEW.loadAtLast,'~') into newValueList;
  END IF;



 IF (OLD.localTemp <> NEW.localTemp ) THEN
      select CONCAT(fieldNameList,'systemdefault.localTemp~') into fieldNameList;
      select CONCAT(oldValueList,OLD.localTemp,'~') into oldValueList;
      select CONCAT(newValueList,NEW.localTemp,'~') into newValueList;
  END IF;


   IF (OLD.longDtypes <> NEW.longDtypes ) THEN
      select CONCAT(fieldNameList,'systemdefault.longDtypes~') into fieldNameList;
      select CONCAT(oldValueList,OLD.longDtypes,'~') into oldValueList;
      select CONCAT(newValueList,NEW.longDtypes,'~') into newValueList;
  END IF;



    IF ((OLD.lowPc <> NEW.lowPc) or (OLD.lowPc is null and NEW.lowPc is not null)
      or (OLD.lowPc is not null and NEW.lowPc is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.lowPc~') into fieldNameList;
        IF(OLD.lowPc is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lowPc is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lowPc is not null) then
            select CONCAT(oldValueList,OLD.lowPc,'~') into oldValueList;
        END IF;
        IF(NEW.lowPc is not null) then
            select CONCAT(newValueList,NEW.lowPc,'~') into newValueList;
        END IF;
    END IF;


    IF (OLD.masterCard <> NEW.masterCard ) THEN
      select CONCAT(fieldNameList,'systemdefault.masterCard~') into fieldNameList;
      select CONCAT(oldValueList,OLD.masterCard,'~') into oldValueList;
      select CONCAT(newValueList,NEW.masterCard,'~') into newValueList;
  END IF;



 IF ((OLD.midPc <> NEW.midPc) or (OLD.midPc is null and NEW.midPc is not null)
      or (OLD.midPc is not null and NEW.midPc is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.midPc~') into fieldNameList;
        IF(OLD.midPc is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.midPc is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.midPc is not null) then
            select CONCAT(oldValueList,OLD.midPc,'~') into oldValueList;
        END IF;
        IF(NEW.midPc is not null) then
            select CONCAT(newValueList,NEW.midPc,'~') into newValueList;
        END IF;
    END IF;




     IF ((OLD.miles <> NEW.miles) or (OLD.miles is null and NEW.miles is not null)
      or (OLD.miles is not null and NEW.miles is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.miles~') into fieldNameList;
        IF(OLD.miles is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.miles is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.miles is not null) then
            select CONCAT(oldValueList,OLD.miles,'~') into oldValueList;
        END IF;
        IF(NEW.miles is not null) then
            select CONCAT(newValueList,NEW.miles,'~') into newValueList;
        END IF;
    END IF;



    
    IF (OLD.milesPath <> NEW.milesPath ) THEN
      select CONCAT(fieldNameList,'systemdefault.milesPath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.milesPath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.milesPath,'~') into newValueList;
  END IF;



    IF (OLD.minPayCode <> NEW.minPayCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.minPayCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minPayCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minPayCode,'~') into newValueList;
  END IF;




    IF (OLD.miscVl <> NEW.miscVl ) THEN
      select CONCAT(fieldNameList,'systemdefault.miscVl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.miscVl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.miscVl,'~') into newValueList;
  END IF;





     IF ((OLD.MLastIn <> NEW.MLastIn) or (OLD.MLastIn is null and NEW.MLastIn is not null)
      or (OLD.MLastIn is not null and NEW.MLastIn is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.MLastIn~') into fieldNameList;
        IF(OLD.MLastIn is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.MLastIn is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.MLastIn is not null) then
            select CONCAT(oldValueList,OLD.MLastIn,'~') into oldValueList;
        END IF;
        IF(NEW.MLastIn is not null) then
            select CONCAT(newValueList,NEW.MLastIn,'~') into newValueList;
        END IF;
    END IF;





     IF ((OLD.MLastOut <> NEW.MLastOut) or (OLD.MLastOut is null and NEW.MLastOut is not null)
      or (OLD.MLastOut is not null and NEW.MLastOut is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.MLastOut~') into fieldNameList;
        IF(OLD.MLastOut is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.MLastOut is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.MLastOut is not null) then
            select CONCAT(oldValueList,OLD.MLastOut,'~') into oldValueList;
        END IF;
        IF(NEW.MLastOut is not null) then
            select CONCAT(newValueList,NEW.MLastOut,'~') into newValueList;
        END IF;
    END IF;





    IF (OLD.numDays <> NEW.numDays ) THEN
      select CONCAT(fieldNameList,'systemdefault.numDays~') into fieldNameList;
      select CONCAT(oldValueList,OLD.numDays,'~') into oldValueList;
      select CONCAT(newValueList,NEW.numDays,'~') into newValueList;
  END IF;





    IF (OLD.other <> NEW.other ) THEN
      select CONCAT(fieldNameList,'systemdefault.other~') into fieldNameList;
      select CONCAT(oldValueList,OLD.other,'~') into oldValueList;
      select CONCAT(newValueList,NEW.other,'~') into newValueList;
  END IF;



     IF ((OLD.otherPcPay <> NEW.otherPcPay) or (OLD.otherPcPay is null and NEW.otherPcPay is not null)
      or (OLD.otherPcPay is not null and NEW.otherPcPay is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.otherPcPay~') into fieldNameList;
        IF(OLD.otherPcPay is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.otherPcPay is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.otherPcPay is not null) then
            select CONCAT(oldValueList,OLD.otherPcPay,'~') into oldValueList;
        END IF;
        IF(NEW.otherPcPay is not null) then
            select CONCAT(newValueList,NEW.otherPcPay,'~') into newValueList;
        END IF;
    END IF;




    IF (OLD.packScreen <> NEW.packScreen ) THEN
      select CONCAT(fieldNameList,'systemdefault.packScreen~') into fieldNameList;
      select CONCAT(oldValueList,OLD.packScreen,'~') into oldValueList;
      select CONCAT(newValueList,NEW.packScreen,'~') into newValueList;
  END IF;




    IF (OLD.pdfprinter <> NEW.pdfprinter ) THEN
      select CONCAT(fieldNameList,'systemdefault.pdfprinter~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pdfprinter,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pdfprinter,'~') into newValueList;
  END IF;




    IF (OLD.persionalPath <> NEW.persionalPath ) THEN
      select CONCAT(fieldNameList,'systemdefault.persionalPath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.persionalPath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.persionalPath,'~') into newValueList;
  END IF;





    IF (OLD.phone <> NEW.phone ) THEN
      select CONCAT(fieldNameList,'systemdefault.phone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.phone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.phone,'~') into newValueList;
  END IF;





    IF (OLD.prompt1 <> NEW.prompt1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt1,'~') into newValueList;
  END IF;




    IF (OLD.prompt10 <> NEW.prompt10 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt10~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt10,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt10,'~') into newValueList;
  END IF;





    IF (OLD.prompt2 <> NEW.prompt2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt2,'~') into newValueList;
  END IF;





    IF (OLD.prompt3 <> NEW.prompt3 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt3,'~') into newValueList;
  END IF;



    IF (OLD.prompt4 <> NEW.prompt4 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt4,'~') into newValueList;
  END IF;



 IF (OLD.prompt5 <> NEW.prompt5 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt5,'~') into newValueList;
  END IF;



   IF (OLD.prompt6 <> NEW.prompt6 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt6~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt6,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt6,'~') into newValueList;
  END IF;


   IF (OLD.prompt7 <> NEW.prompt7 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt7~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt7,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt7,'~') into newValueList;
  END IF;

   IF (OLD.prompt8 <> NEW.prompt8 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt8~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt8,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt8,'~') into newValueList;
  END IF;


   IF (OLD.prompt9 <> NEW.prompt9 ) THEN
      select CONCAT(fieldNameList,'systemdefault.prompt9~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prompt9,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prompt9,'~') into newValueList;
  END IF;


   IF (OLD.regagent <> NEW.regagent ) THEN
      select CONCAT(fieldNameList,'systemdefault.regagent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.regagent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.regagent,'~') into newValueList;
  END IF;


   IF (OLD.remarks1 <> NEW.remarks1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.remarks1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.remarks1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.remarks1,'~') into newValueList;
  END IF;



   IF (OLD.remarks2 <> NEW.remarks2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.remarks2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.remarks2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.remarks2,'~') into newValueList;
  END IF;



   IF (OLD.remarks3 <> NEW.remarks3 ) THEN
      select CONCAT(fieldNameList,'systemdefault.remarks3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.remarks3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.remarks3,'~') into newValueList;
  END IF;



   IF (OLD.remarks4 <> NEW.remarks4 ) THEN
      select CONCAT(fieldNameList,'systemdefault.remarks4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.remarks4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.remarks4,'~') into newValueList;
  END IF;



   IF (OLD.remarks5 <> NEW.remarks5 ) THEN
      select CONCAT(fieldNameList,'systemdefault.remarks5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.remarks5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.remarks5,'~') into newValueList;
  END IF;



   IF (OLD.rrWindowPath <> NEW.rrWindowPath ) THEN
      select CONCAT(fieldNameList,'systemdefault.rrWindowPath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rrWindowPath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rrWindowPath,'~') into newValueList;
  END IF;



   IF (OLD.runReports <> NEW.runReports ) THEN
      select CONCAT(fieldNameList,'systemdefault.runReports~') into fieldNameList;
      select CONCAT(oldValueList,OLD.runReports,'~') into oldValueList;
      select CONCAT(newValueList,NEW.runReports,'~') into newValueList;
  END IF;



   IF (OLD.runStars <> NEW.runStars ) THEN
      select CONCAT(fieldNameList,'systemdefault.runStars~') into fieldNameList;
      select CONCAT(oldValueList,OLD.runStars,'~') into oldValueList;
      select CONCAT(newValueList,NEW.runStars,'~') into newValueList;
  END IF;



   IF (OLD.SCloseCSO <> NEW.SCloseCSO ) THEN
      select CONCAT(fieldNameList,'systemdefault.SCloseCSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCloseCSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCloseCSO,'~') into newValueList;
  END IF;




   IF (OLD.scoreForm <> NEW.scoreForm ) THEN
      select CONCAT(fieldNameList,'systemdefault.scoreForm~') into fieldNameList;
      select CONCAT(oldValueList,OLD.scoreForm,'~') into oldValueList;
      select CONCAT(newValueList,NEW.scoreForm,'~') into newValueList;
  END IF;



   IF (OLD.sending <> NEW.sending ) THEN
      select CONCAT(fieldNameList,'systemdefault.sending~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sending,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sending,'~') into newValueList;
  END IF;


   IF (OLD.smtpaddr <> NEW.smtpaddr ) THEN
      select CONCAT(fieldNameList,'systemdefault.smtpaddr~') into fieldNameList;
      select CONCAT(oldValueList,OLD.smtpaddr,'~') into oldValueList;
      select CONCAT(newValueList,NEW.smtpaddr,'~') into newValueList;
  END IF;



   IF (OLD.other <> NEW.other ) THEN
      select CONCAT(fieldNameList,'systemdefault.other~') into fieldNameList;
      select CONCAT(oldValueList,OLD.other,'~') into oldValueList;
      select CONCAT(newValueList,NEW.other,'~') into newValueList;
  END IF;


   IF (OLD.special1 <> NEW.special1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.special1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.special1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.special1,'~') into newValueList;
  END IF;

 IF (OLD.special2 <> NEW.special2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.special2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.special2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.special2,'~') into newValueList;
  END IF;



 IF ((OLD.sendInvoice <> NEW.sendInvoice) or (OLD.sendInvoice is null and NEW.sendInvoice is not null)
      or (OLD.sendInvoice is not null and NEW.sendInvoice is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.sendInvoice~') into fieldNameList;
       IF(OLD.sendInvoice is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.sendInvoice is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.sendInvoice is not null) THEN
        	select CONCAT(oldValueList,OLD.sendInvoice,'~') into oldValueList;
     	END IF;
      	IF(NEW.sendInvoice is not null) THEN
        	select CONCAT(newValueList,NEW.sendInvoice,'~') into newValueList;
      	END IF;

   END IF;






 IF ((OLD.srunDate <> NEW.srunDate) or (OLD.srunDate is null and NEW.srunDate is not null)
      or (OLD.srunDate is not null and NEW.srunDate is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.srunDate~') into fieldNameList;
       IF(OLD.srunDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.srunDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.srunDate is not null) THEN
        	select CONCAT(oldValueList,OLD.srunDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.srunDate is not null) THEN
        	select CONCAT(newValueList,NEW.srunDate,'~') into newValueList;
      	END IF;

   END IF;



   
 IF ((OLD.STestDate <> NEW.STestDate) or (OLD.STestDate is null and NEW.STestDate is not null)
      or (OLD.STestDate is not null and NEW.STestDate is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.STestDate~') into fieldNameList;
       IF(OLD.STestDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.STestDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.STestDate is not null) THEN
        	select CONCAT(oldValueList,OLD.STestDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.STestDate is not null) THEN
        	select CONCAT(newValueList,NEW.STestDate,'~') into newValueList;
      	END IF;

   END IF;



    IF (OLD.storage <> NEW.storage ) THEN
      select CONCAT(fieldNameList,'systemdefault.storage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storage,'~') into newValueList;
  END IF;



 IF (OLD.storageJob <> NEW.storageJob ) THEN
      select CONCAT(fieldNameList,'systemdefault.storageJob~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageJob,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageJob,'~') into newValueList;
  END IF;


 IF (OLD.tcktwh <> NEW.tcktwh ) THEN
      select CONCAT(fieldNameList,'systemdefault.tcktwh~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tcktwh,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tcktwh,'~') into newValueList;
  END IF;




 IF (OLD.telex <> NEW.telex ) THEN
      select CONCAT(fieldNameList,'systemdefault.telex~') into fieldNameList;
      select CONCAT(oldValueList,OLD.telex,'~') into oldValueList;
      select CONCAT(newValueList,NEW.telex,'~') into newValueList;
  END IF;




 IF (OLD.tin <> NEW.tin ) THEN
      select CONCAT(fieldNameList,'systemdefault.tin~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tin,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tin,'~') into newValueList;
  END IF;




 IF (OLD.trackPath <> NEW.trackPath ) THEN
      select CONCAT(fieldNameList,'systemdefault.trackPath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.trackPath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.trackPath,'~') into newValueList;
  END IF;





 IF (OLD.typeMiles <> NEW.typeMiles ) THEN
      select CONCAT(fieldNameList,'systemdefault.typeMiles~') into fieldNameList;
      select CONCAT(oldValueList,OLD.typeMiles,'~') into oldValueList;
      select CONCAT(newValueList,NEW.typeMiles,'~') into newValueList;
  END IF;




 IF (OLD.usewhpay <> NEW.usewhpay ) THEN
      select CONCAT(fieldNameList,'systemdefault.usewhpay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.usewhpay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.usewhpay,'~') into newValueList;
  END IF;




 IF (OLD.vanLine <> NEW.vanLine ) THEN
      select CONCAT(fieldNameList,'systemdefault.vanLine~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLine,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLine,'~') into newValueList;
  END IF;




 IF (OLD.vlContract <> NEW.vlContract ) THEN
      select CONCAT(fieldNameList,'systemdefault.vlContract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vlContract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vlContract,'~') into newValueList;
  END IF;




 IF (OLD.vlName <> NEW.vlName ) THEN
      select CONCAT(fieldNameList,'systemdefault.vlName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vlName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vlName,'~') into newValueList;
  END IF;




 IF (OLD.vlPrefix <> NEW.vlPrefix ) THEN
      select CONCAT(fieldNameList,'systemdefault.vlPrefix~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vlPrefix,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vlPrefix,'~') into newValueList;
  END IF;



 IF (OLD.weightLimit <> NEW.weightLimit ) THEN
      select CONCAT(fieldNameList,'systemdefault.weightLimit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weightLimit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weightLimit,'~') into newValueList;
  END IF;



  
 IF ((OLD.willAdvis <> NEW.willAdvis) or (OLD.willAdvis is null and NEW.willAdvis is not null)
      or (OLD.willAdvis is not null and NEW.willAdvis is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.willAdvis~') into fieldNameList;
       IF(OLD.willAdvis is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.willAdvis is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.willAdvis is not null) THEN
        	select CONCAT(oldValueList,OLD.willAdvis,'~') into oldValueList;
     	END IF;
      	IF(NEW.willAdvis is not null) THEN
        	select CONCAT(newValueList,NEW.willAdvis,'~') into newValueList;
      	END IF;

   END IF;


     
 IF ((OLD.willAdvise <> NEW.willAdvise) or (OLD.willAdvise is null and NEW.willAdvise is not null)
      or (OLD.willAdvise is not null and NEW.willAdvise is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.willAdvise~') into fieldNameList;
       IF(OLD.willAdvise is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.willAdvise is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.willAdvise is not null) THEN
        	select CONCAT(oldValueList,OLD.willAdvise,'~') into oldValueList;
     	END IF;
      	IF(NEW.willAdvise is not null) THEN
        	select CONCAT(newValueList,NEW.willAdvise,'~') into newValueList;
      	END IF;

   END IF;


   
 IF (OLD.workDymension <> NEW.workDymension ) THEN
      select CONCAT(fieldNameList,'systemdefault.workDymension~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workDymension,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workDymension,'~') into newValueList;
  END IF;



 IF (OLD.zip <> NEW.zip ) THEN
      select CONCAT(fieldNameList,'systemdefault.zip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.zip,'~') into newValueList;
  END IF;




IF (OLD.matchgl <> NEW.matchgl ) THEN
       select CONCAT(fieldNameList,'systemdefault.matchgl~') into fieldNameList;
       IF(OLD.matchgl = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.matchgl = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.matchgl = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.matchgl = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




   
IF (OLD.jobContractIsystem <> NEW.jobContractIsystem ) THEN
       select CONCAT(fieldNameList,'systemdefault.jobContractIsystem~') into fieldNameList;
       IF(OLD.jobContractIsystem = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.jobContractIsystem = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.jobContractIsystem = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.jobContractIsystem = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



   
   
IF (OLD.postBybranch <> NEW.postBybranch ) THEN
       select CONCAT(fieldNameList,'systemdefault.postBybranch~') into fieldNameList;
       IF(OLD.postBybranch = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.postBybranch = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.postBybranch = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.postBybranch = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




   
   
IF (OLD.holidays <> NEW.holidays ) THEN
       select CONCAT(fieldNameList,'systemdefault.holidays~') into fieldNameList;
       IF(OLD.holidays = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.holidays = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.holidays = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.holidays = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



   
 IF (OLD.baseCurrency <> NEW.baseCurrency ) THEN
      select CONCAT(fieldNameList,'systemdefault.baseCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.baseCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.baseCurrency,'~') into newValueList;
  END IF;




 IF (OLD.companyCode <> NEW.companyCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.companyCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyCode,'~') into newValueList;
  END IF;




 IF (OLD.flatFeesRate <> NEW.flatFeesRate ) THEN
      select CONCAT(fieldNameList,'systemdefault.flatFeesRate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flatFeesRate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flatFeesRate,'~') into newValueList;
  END IF;





 IF (OLD.feetLimits <> NEW.feetLimits ) THEN
      select CONCAT(fieldNameList,'systemdefault.feetLimits~') into fieldNameList;
      select CONCAT(oldValueList,OLD.feetLimits,'~') into oldValueList;
      select CONCAT(newValueList,NEW.feetLimits,'~') into newValueList;
  END IF;





 IF (OLD.forworkTickets <> NEW.forworkTickets ) THEN
      select CONCAT(fieldNameList,'systemdefault.forworkTickets~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forworkTickets,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forworkTickets,'~') into newValueList;
  END IF;




    
 IF ((OLD.counted <> NEW.counted) or (OLD.counted is null and NEW.counted is not null)
      or (OLD.counted is not null and NEW.counted is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.counted~') into fieldNameList;
       IF(OLD.counted is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.counted is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.counted is not null) THEN
        	select CONCAT(oldValueList,OLD.counted,'~') into oldValueList;
     	END IF;
      	IF(NEW.counted is not null) THEN
        	select CONCAT(newValueList,NEW.counted,'~') into newValueList;
      	END IF;

   END IF;



   
 IF ((OLD.workOrder <> NEW.workOrder) or (OLD.workOrder is null and NEW.workOrder is not null)
      or (OLD.workOrder is not null and NEW.workOrder is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.workOrder~') into fieldNameList;
       IF(OLD.workOrder is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.workOrder is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.workOrder is not null) THEN
        	select CONCAT(oldValueList,OLD.workOrder,'~') into oldValueList;
     	END IF;
      	IF(NEW.workOrder is not null) THEN
        	select CONCAT(newValueList,NEW.workOrder,'~') into newValueList;
      	END IF;

   END IF;





 IF (OLD.workTicket <> NEW.workTicket ) THEN
      select CONCAT(fieldNameList,'systemdefault.workTicket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workTicket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workTicket,'~') into newValueList;
  END IF;





 IF (OLD.dayAt1 <> NEW.dayAt1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.dayAt1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dayAt1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dayAt1,'~') into newValueList;
  END IF;




 IF (OLD.dayAt2 <> NEW.dayAt2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.dayAt2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dayAt2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dayAt2,'~') into newValueList;
  END IF;





 IF (OLD.fromOvertime <> NEW.fromOvertime ) THEN
      select CONCAT(fieldNameList,'systemdefault.fromOvertime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fromOvertime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fromOvertime,'~') into newValueList;
  END IF;





 IF (OLD.fromPaying <> NEW.fromPaying ) THEN
      select CONCAT(fieldNameList,'systemdefault.fromPaying~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fromPaying,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fromPaying,'~') into newValueList;
  END IF;





 IF (OLD.lunch1 <> NEW.lunch1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.lunch1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lunch1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lunch1,'~') into newValueList;
  END IF;





 IF (OLD.lunch2 <> NEW.lunch2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.lunch2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lunch2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lunch2,'~') into newValueList;
  END IF;





 IF (OLD.overTime1 <> NEW.overTime1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.overTime1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.overTime1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.overTime1,'~') into newValueList;
  END IF;





 IF (OLD.overTime2 <> NEW.overTime2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.overTime2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.overTime2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.overTime2,'~') into newValueList;
  END IF;





 IF (OLD.weekdays1 <> NEW.weekdays1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.weekdays1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weekdays1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weekdays1,'~') into newValueList;
  END IF;





 IF (OLD.weekdays2 <> NEW.weekdays2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.weekdays2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weekdays2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weekdays2,'~') into newValueList;
  END IF;




 IF (OLD.workDay <> NEW.workDay ) THEN
      select CONCAT(fieldNameList,'systemdefault.workDay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workDay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workDay,'~') into newValueList;
  END IF;





 IF (OLD.csoDensity <> NEW.csoDensity ) THEN
      select CONCAT(fieldNameList,'systemdefault.csoDensity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.csoDensity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.csoDensity,'~') into newValueList;
  END IF;





 IF (OLD.iata <> NEW.iata ) THEN
      select CONCAT(fieldNameList,'systemdefault.iata~') into fieldNameList;
      select CONCAT(oldValueList,OLD.iata,'~') into oldValueList;
      select CONCAT(newValueList,NEW.iata,'~') into newValueList;
  END IF;




 IF (OLD.smtpServer <> NEW.smtpServer ) THEN
      select CONCAT(fieldNameList,'systemdefault.smtpServer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.smtpServer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.smtpServer,'~') into newValueList;
  END IF;



   
   
IF (OLD.usesCodes <> NEW.usesCodes ) THEN
       select CONCAT(fieldNameList,'systemdefault.usesCodes~') into fieldNameList;
       IF(OLD.usesCodes = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.usesCodes = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.usesCodes = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.usesCodes = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




   
   
IF (OLD.adeena <> NEW.adeena ) THEN
       select CONCAT(fieldNameList,'systemdefault.adeena~') into fieldNameList;
       IF(OLD.adeena = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.adeena = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.adeena = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.adeena = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



   
   
IF (OLD.prefillBooker <> NEW.prefillBooker ) THEN
       select CONCAT(fieldNameList,'systemdefault.prefillBooker~') into fieldNameList;
       IF(OLD.prefillBooker = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.prefillBooker = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.prefillBooker = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.prefillBooker = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   
   
IF (OLD.techmate <> NEW.techmate ) THEN
       select CONCAT(fieldNameList,'systemdefault.techmate~') into fieldNameList;
       IF(OLD.techmate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.techmate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.techmate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.techmate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   
   
IF (OLD.zipCode <> NEW.zipCode ) THEN
       select CONCAT(fieldNameList,'systemdefault.zipCode~') into fieldNameList;
       IF(OLD.zipCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.zipCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.zipCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.zipCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   
IF (OLD.blankJob <> NEW.blankJob ) THEN
       select CONCAT(fieldNameList,'systemdefault.blankJob~') into fieldNameList;
       IF(OLD.blankJob = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.blankJob = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.blankJob = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.blankJob = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




   
IF (OLD.thirdParty <> NEW.thirdParty ) THEN
       select CONCAT(fieldNameList,'systemdefault.thirdParty~') into fieldNameList;
       IF(OLD.thirdParty = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.thirdParty = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.thirdParty = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.thirdParty = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



	  
	 IF ((OLD.mcExpire <> NEW.mcExpire) or (OLD.mcExpire is null and NEW.mcExpire is not null)
	      or (OLD.mcExpire is not null and NEW.mcExpire is null)) THEN

	       select CONCAT(fieldNameList,'systemdefault.mcExpire~') into fieldNameList;
	       IF(OLD.mcExpire is null) THEN
			select CONCAT(oldValueList," ",'~') into oldValueList;
		END IF;
		IF(NEW.mcExpire is null) THEN
			select CONCAT(newValueList," ",'~') into newValueList;
		END IF;

		IF(OLD.mcExpire is not null) THEN
			select CONCAT(oldValueList,OLD.mcExpire,'~') into oldValueList;
		END IF;
		IF(NEW.mcExpire is not null) THEN
			select CONCAT(newValueList,NEW.mcExpire,'~') into newValueList;
		END IF;

	   END IF;



   IF (OLD.state <> NEW.state ) THEN
      select CONCAT(fieldNameList,'systemdefault.state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.state,'~') into newValueList;
  END IF;



 IF (OLD.userPath <> NEW.userPath ) THEN
      select CONCAT(fieldNameList,'systemdefault.userPath~') into fieldNameList;
      select CONCAT(oldValueList,OLD.userPath,'~') into oldValueList;
      select CONCAT(newValueList,NEW.userPath,'~') into newValueList;
  END IF;




	 IF ((OLD.postDate1 <> NEW.postDate1) or (OLD.postDate1 is null and NEW.postDate1 is not null)
	      or (OLD.postDate1 is not null and NEW.postDate1 is null)) THEN

	       select CONCAT(fieldNameList,'systemdefault.postDate1~') into fieldNameList;
	       IF(OLD.postDate1 is null) THEN
			select CONCAT(oldValueList," ",'~') into oldValueList;
		END IF;
		IF(NEW.postDate1 is null) THEN
			select CONCAT(newValueList," ",'~') into newValueList;
		END IF;

		IF(OLD.postDate1 is not null) THEN
			select CONCAT(oldValueList,OLD.postDate1,'~') into oldValueList;
		END IF;
		IF(NEW.postDate1 is not null) THEN
			select CONCAT(newValueList,NEW.postDate1,'~') into newValueList;
		END IF;

	   END IF;





 IF (OLD.serviceInclude <> NEW.serviceInclude ) THEN
      select CONCAT(fieldNameList,'systemdefault.serviceInclude~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceInclude,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceInclude,'~') into newValueList;
  END IF;




 IF (OLD.serviceExclude <> NEW.serviceExclude ) THEN
      select CONCAT(fieldNameList,'systemdefault.serviceExclude~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceExclude,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceExclude,'~') into newValueList;
  END IF;




 IF (OLD.weightUnit <> NEW.weightUnit ) THEN
      select CONCAT(fieldNameList,'systemdefault.weightUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weightUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weightUnit,'~') into newValueList;
  END IF;





 IF (OLD.volumeUnit <> NEW.volumeUnit ) THEN
      select CONCAT(fieldNameList,'systemdefault.volumeUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.volumeUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.volumeUnit,'~') into newValueList;
  END IF;





 IF (OLD.isUnigroup <> NEW.isUnigroup ) THEN
      select CONCAT(fieldNameList,'systemdefault.isUnigroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.isUnigroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.isUnigroup,'~') into newValueList;
  END IF;



 IF (OLD.uvl <> NEW.uvl ) THEN
      select CONCAT(fieldNameList,'systemdefault.uvl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.uvl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.uvl,'~') into newValueList;
  END IF;





 IF (OLD.mvl <> NEW.mvl ) THEN
      select CONCAT(fieldNameList,'systemdefault.mvl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mvl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mvl,'~') into newValueList;
  END IF;




 IF (OLD.accountingSystem <> NEW.accountingSystem ) THEN
      select CONCAT(fieldNameList,'systemdefault.accountingSystem~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountingSystem,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountingSystem,'~') into newValueList;
  END IF;




 IF (OLD.accountingInterface <> NEW.accountingInterface ) THEN
      select CONCAT(fieldNameList,'systemdefault.accountingInterface~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountingInterface,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountingInterface,'~') into newValueList;
  END IF;


  
 IF (OLD.filterCoordinator <> NEW.filterCoordinator ) THEN
      select CONCAT(fieldNameList,'systemdefault.filterCoordinator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.filterCoordinator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.filterCoordinator,'~') into newValueList;
  END IF;



  
 IF (OLD.invExtractSeq <> NEW.invExtractSeq ) THEN
      select CONCAT(fieldNameList,'systemdefault.invExtractSeq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invExtractSeq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invExtractSeq,'~') into newValueList;
  END IF;



  
 IF (OLD.payExtractSeq <> NEW.payExtractSeq ) THEN
      select CONCAT(fieldNameList,'systemdefault.payExtractSeq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payExtractSeq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payExtractSeq,'~') into newValueList;
  END IF;



  
 IF (OLD.prtExtractSeq <> NEW.prtExtractSeq ) THEN
      select CONCAT(fieldNameList,'systemdefault.prtExtractSeq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prtExtractSeq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prtExtractSeq,'~') into newValueList;
  END IF;



  
  
 IF (OLD.minimumHoursGurantee <> NEW.minimumHoursGurantee ) THEN
      select CONCAT(fieldNameList,'systemdefault.minimumHoursGurantee~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minimumHoursGurantee,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minimumHoursGurantee,'~') into newValueList;
  END IF;



  
	 IF ((OLD.payrollFreezeFrom <> NEW.payrollFreezeFrom) or (OLD.payrollFreezeFrom is null and NEW.payrollFreezeFrom is not null)
	      or (OLD.payrollFreezeFrom is not null and NEW.payrollFreezeFrom is null)) THEN

	       select CONCAT(fieldNameList,'systemdefault.payrollFreezeFrom~') into fieldNameList;
	       IF(OLD.payrollFreezeFrom is null) THEN
			select CONCAT(oldValueList," ",'~') into oldValueList;
		END IF;
		IF(NEW.payrollFreezeFrom is null) THEN
			select CONCAT(newValueList," ",'~') into newValueList;
		END IF;

		IF(OLD.payrollFreezeFrom is not null) THEN
			select CONCAT(oldValueList,OLD.payrollFreezeFrom,'~') into oldValueList;
		END IF;
		IF(NEW.payrollFreezeFrom is not null) THEN
			select CONCAT(newValueList,NEW.payrollFreezeFrom,'~') into newValueList;
		END IF;

	   END IF;





	 IF ((OLD.payrollFreezeTo <> NEW.payrollFreezeTo) or (OLD.payrollFreezeTo is null and NEW.payrollFreezeTo is not null)
	      or (OLD.payrollFreezeTo is not null and NEW.payrollFreezeTo is null)) THEN

	       select CONCAT(fieldNameList,'systemdefault.payrollFreezeTo~') into fieldNameList;
	       IF(OLD.payrollFreezeTo is null) THEN
			select CONCAT(oldValueList," ",'~') into oldValueList;
		END IF;
		IF(NEW.payrollFreezeTo is null) THEN
			select CONCAT(newValueList," ",'~') into newValueList;
		END IF;

		IF(OLD.payrollFreezeTo is not null) THEN
			select CONCAT(oldValueList,OLD.payrollFreezeTo,'~') into oldValueList;
		END IF;
		IF(NEW.payrollFreezeTo is not null) THEN
			select CONCAT(newValueList,NEW.payrollFreezeTo,'~') into newValueList;
		END IF;

	   END IF;





  
 IF (OLD.salesCommisionRate <> NEW.salesCommisionRate ) THEN
      select CONCAT(fieldNameList,'systemdefault.salesCommisionRate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.salesCommisionRate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.salesCommisionRate,'~') into newValueList;
  END IF;




  
 IF (OLD.grossMarginThreshold <> NEW.grossMarginThreshold ) THEN
      select CONCAT(fieldNameList,'systemdefault.grossMarginThreshold~') into fieldNameList;
      select CONCAT(oldValueList,OLD.grossMarginThreshold,'~') into oldValueList;
      select CONCAT(newValueList,NEW.grossMarginThreshold,'~') into newValueList;
  END IF;





  
 IF (OLD.subContcExtractSeq <> NEW.subContcExtractSeq ) THEN
      select CONCAT(fieldNameList,'systemdefault.subContcExtractSeq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subContcExtractSeq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subContcExtractSeq,'~') into newValueList;
  END IF;





  
 IF (OLD.toDoRuleExecutionTime <> NEW.toDoRuleExecutionTime ) THEN
      select CONCAT(fieldNameList,'systemdefault.toDoRuleExecutionTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.toDoRuleExecutionTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.toDoRuleExecutionTime,'~') into newValueList;
  END IF;






	 IF ((OLD.lastRunDate <> NEW.lastRunDate) or (OLD.lastRunDate is null and NEW.lastRunDate is not null)
	      or (OLD.lastRunDate is not null and NEW.lastRunDate is null)) THEN

	       select CONCAT(fieldNameList,'systemdefault.lastRunDate~') into fieldNameList;
	       IF(OLD.lastRunDate is null) THEN
			select CONCAT(oldValueList," ",'~') into oldValueList;
		END IF;
		IF(NEW.lastRunDate is null) THEN
			select CONCAT(newValueList," ",'~') into newValueList;
		END IF;

		IF(OLD.lastRunDate is not null) THEN
			select CONCAT(oldValueList,OLD.lastRunDate,'~') into oldValueList;
		END IF;
		IF(NEW.lastRunDate is not null) THEN
			select CONCAT(newValueList,NEW.lastRunDate,'~') into newValueList;
		END IF;

	   END IF;



	   

  
 IF (OLD.dailyOperationalLimit <> NEW.dailyOperationalLimit ) THEN
      select CONCAT(fieldNameList,'systemdefault.dailyOperationalLimit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dailyOperationalLimit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dailyOperationalLimit,'~') into newValueList;
  END IF;



  

  
 IF (OLD.minServiceDay <> NEW.minServiceDay ) THEN
      select CONCAT(fieldNameList,'systemdefault.minServiceDay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minServiceDay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minServiceDay,'~') into newValueList;
  END IF;





  
 IF (OLD.dailyOpLimitPC <> NEW.dailyOpLimitPC ) THEN
      select CONCAT(fieldNameList,'systemdefault.dailyOpLimitPC~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dailyOpLimitPC,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dailyOpLimitPC,'~') into newValueList;
  END IF;





  
 IF (OLD.website <> NEW.website ) THEN
      select CONCAT(fieldNameList,'systemdefault.website~') into fieldNameList;
      select CONCAT(oldValueList,OLD.website,'~') into oldValueList;
      select CONCAT(newValueList,NEW.website,'~') into newValueList;
  END IF;





  
 IF (OLD.maxAttempt <> NEW.maxAttempt ) THEN
      select CONCAT(fieldNameList,'systemdefault.maxAttempt~') into fieldNameList;
      select CONCAT(oldValueList,OLD.maxAttempt,'~') into oldValueList;
      select CONCAT(newValueList,NEW.maxAttempt,'~') into newValueList;
  END IF;






  
 IF (OLD.excludeIPs <> NEW.excludeIPs ) THEN
      select CONCAT(fieldNameList,'systemdefault.excludeIPs~') into fieldNameList;
      select CONCAT(oldValueList,OLD.excludeIPs,'~') into oldValueList;
      select CONCAT(newValueList,NEW.excludeIPs,'~') into newValueList;
  END IF;






  
 IF (OLD.toDoRuleExecutionTime2 <> NEW.toDoRuleExecutionTime2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.toDoRuleExecutionTime2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.toDoRuleExecutionTime2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.toDoRuleExecutionTime2,'~') into newValueList;
  END IF;






  
 IF (OLD.surveyExternalIPs <> NEW.surveyExternalIPs ) THEN
      select CONCAT(fieldNameList,'systemdefault.surveyExternalIPs~') into fieldNameList;
      select CONCAT(oldValueList,OLD.surveyExternalIPs,'~') into oldValueList;
      select CONCAT(newValueList,NEW.surveyExternalIPs,'~') into newValueList;
  END IF;





  
 IF (OLD.standardCountryHauling <> NEW.standardCountryHauling ) THEN
      select CONCAT(fieldNameList,'systemdefault.standardCountryHauling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.standardCountryHauling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.standardCountryHauling,'~') into newValueList;
  END IF;





  
 IF (OLD.standardCountrycharges <> NEW.standardCountrycharges ) THEN
      select CONCAT(fieldNameList,'systemdefault.standardCountrycharges~') into fieldNameList;
      select CONCAT(oldValueList,OLD.standardCountrycharges,'~') into oldValueList;
      select CONCAT(newValueList,NEW.standardCountrycharges,'~') into newValueList;
  END IF;





     IF ((OLD.healthWelfareRate <> NEW.healthWelfareRate) or (OLD.healthWelfareRate is null and NEW.healthWelfareRate is not null)
      or (OLD.healthWelfareRate is not null and NEW.healthWelfareRate is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.healthWelfareRate~') into fieldNameList;
        IF(OLD.healthWelfareRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.healthWelfareRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.healthWelfareRate is not null) then
            select CONCAT(oldValueList,OLD.healthWelfareRate,'~') into oldValueList;
        END IF;
        IF(NEW.healthWelfareRate is not null) then
            select CONCAT(newValueList,NEW.healthWelfareRate,'~') into newValueList;
        END IF;
    END IF;




    
  
 IF (OLD.rateDeptEmail <> NEW.rateDeptEmail ) THEN
      select CONCAT(fieldNameList,'systemdefault.rateDeptEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rateDeptEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rateDeptEmail,'~') into newValueList;
  END IF;





  
 IF (OLD.agentIdRegistrationDistribution <> NEW.agentIdRegistrationDistribution ) THEN
      select CONCAT(fieldNameList,'systemdefault.agentIdRegistrationDistribution~') into fieldNameList;
      select CONCAT(oldValueList,OLD.agentIdRegistrationDistribution,'~') into oldValueList;
      select CONCAT(newValueList,NEW.agentIdRegistrationDistribution,'~') into newValueList;
  END IF;




   
IF (OLD.claimShowCportal <> NEW.claimShowCportal ) THEN
       select CONCAT(fieldNameList,'systemdefault.claimShowCportal~') into fieldNameList;
       IF(OLD.claimShowCportal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.claimShowCportal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.claimShowCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.claimShowCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




  
 IF (OLD.claimContactPerson <> NEW.claimContactPerson ) THEN
      select CONCAT(fieldNameList,'systemdefault.claimContactPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimContactPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimContactPerson,'~') into newValueList;
  END IF;



  
  
 IF (OLD.claimEmail <> NEW.claimEmail ) THEN
      select CONCAT(fieldNameList,'systemdefault.claimEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimEmail,'~') into newValueList;
  END IF;


  
  
 IF (OLD.claimFax <> NEW.claimFax ) THEN
      select CONCAT(fieldNameList,'systemdefault.claimFax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimFax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimFax,'~') into newValueList;
  END IF;


  
  
 IF (OLD.claimPhone <> NEW.claimPhone ) THEN
      select CONCAT(fieldNameList,'systemdefault.claimPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimPhone,'~') into newValueList;
  END IF;


  
  
 IF (OLD.valuationDetails1 <> NEW.valuationDetails1 ) THEN
      select CONCAT(fieldNameList,'systemdefault.valuationDetails1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.valuationDetails1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.valuationDetails1,'~') into newValueList;
  END IF;



  
 IF (OLD.valuationDetails2 <> NEW.valuationDetails2 ) THEN
      select CONCAT(fieldNameList,'systemdefault.valuationDetails2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.valuationDetails2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.valuationDetails2,'~') into newValueList;
  END IF;


  
  
 IF (OLD.valuationDetails3 <> NEW.valuationDetails3 ) THEN
      select CONCAT(fieldNameList,'systemdefault.valuationDetails3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.valuationDetails3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.valuationDetails3,'~') into newValueList;
  END IF;



  
  
 IF (OLD.valuationDetails4 <> NEW.valuationDetails4 ) THEN
      select CONCAT(fieldNameList,'systemdefault.valuationDetails4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.valuationDetails4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.valuationDetails4,'~') into newValueList;
  END IF;



  
  
 IF (OLD.valuationDetails5 <> NEW.valuationDetails5 ) THEN
      select CONCAT(fieldNameList,'systemdefault.valuationDetails5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.valuationDetails5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.valuationDetails5,'~') into newValueList;
  END IF;



  
  
 IF (OLD.commissionJob <> NEW.commissionJob ) THEN
      select CONCAT(fieldNameList,'systemdefault.commissionJob~') into fieldNameList;
      select CONCAT(oldValueList,OLD.commissionJob,'~') into oldValueList;
      select CONCAT(newValueList,NEW.commissionJob,'~') into newValueList;
  END IF;



  
     IF ((OLD.currencyMargin <> NEW.currencyMargin) or (OLD.currencyMargin is null and NEW.currencyMargin is not null)
      or (OLD.currencyMargin is not null and NEW.currencyMargin is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.currencyMargin~') into fieldNameList;
        IF(OLD.currencyMargin is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.currencyMargin is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.currencyMargin is not null) then
            select CONCAT(oldValueList,OLD.currencyMargin,'~') into oldValueList;
        END IF;
        IF(NEW.currencyMargin is not null) then
            select CONCAT(newValueList,NEW.currencyMargin,'~') into newValueList;
        END IF;
    END IF;




 IF (OLD.currencyPullScheduling <> NEW.currencyPullScheduling ) THEN
      select CONCAT(fieldNameList,'systemdefault.currencyPullScheduling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currencyPullScheduling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currencyPullScheduling,'~') into newValueList;
  END IF;





 IF (OLD.country <> NEW.country ) THEN
      select CONCAT(fieldNameList,'systemdefault.country~') into fieldNameList;
      select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
      select CONCAT(newValueList,NEW.country,'~') into newValueList;
  END IF;




 IF (OLD.companyDivisionAcctgCodeUnique <> NEW.companyDivisionAcctgCodeUnique ) THEN
      select CONCAT(fieldNameList,'systemdefault.companyDivisionAcctgCodeUnique~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivisionAcctgCodeUnique,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivisionAcctgCodeUnique,'~') into newValueList;
  END IF;




 IF (OLD.networkCoordinator <> NEW.networkCoordinator ) THEN
      select CONCAT(fieldNameList,'systemdefault.networkCoordinator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkCoordinator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkCoordinator,'~') into newValueList;
  END IF;




 IF (OLD.defaultDistanceCalc <> NEW.defaultDistanceCalc ) THEN
      select CONCAT(fieldNameList,'systemdefault.defaultDistanceCalc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultDistanceCalc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultDistanceCalc,'~') into newValueList;
  END IF;





   
IF (OLD.vatCalculation <> NEW.vatCalculation ) THEN
       select CONCAT(fieldNameList,'systemdefault.vatCalculation~') into fieldNameList;
       IF(OLD.vatCalculation = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.vatCalculation = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.vatCalculation = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.vatCalculation = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




 IF (OLD.defaultDistanceCalc <> NEW.defaultDistanceCalc ) THEN
      select CONCAT(fieldNameList,'systemdefault.defaultDistanceCalc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultDistanceCalc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultDistanceCalc,'~') into newValueList;
  END IF;




  
  
     IF ((OLD.pensionRate1 <> NEW.pensionRate1) or (OLD.pensionRate1 is null and NEW.pensionRate1 is not null)
      or (OLD.pensionRate1 is not null and NEW.pensionRate1 is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.pensionRate1~') into fieldNameList;
        IF(OLD.pensionRate1 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate1 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pensionRate1 is not null) then
            select CONCAT(oldValueList,OLD.pensionRate1,'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate1 is not null) then
            select CONCAT(newValueList,NEW.pensionRate1,'~') into newValueList;
        END IF;
    END IF;




  
     IF ((OLD.pensionRate2 <> NEW.pensionRate2) or (OLD.pensionRate2 is null and NEW.pensionRate2 is not null)
      or (OLD.pensionRate2 is not null and NEW.pensionRate2 is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.pensionRate2~') into fieldNameList;
        IF(OLD.pensionRate2 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate2 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pensionRate2 is not null) then
            select CONCAT(oldValueList,OLD.pensionRate2,'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate2 is not null) then
            select CONCAT(newValueList,NEW.pensionRate2,'~') into newValueList;
        END IF;
    END IF;





  
     IF ((OLD.pensionRate3 <> NEW.pensionRate3) or (OLD.pensionRate3 is null and NEW.pensionRate3 is not null)
      or (OLD.pensionRate3 is not null and NEW.pensionRate3 is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.pensionRate3~') into fieldNameList;
        IF(OLD.pensionRate3 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate3 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pensionRate3 is not null) then
            select CONCAT(oldValueList,OLD.pensionRate3,'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate3 is not null) then
            select CONCAT(newValueList,NEW.pensionRate3,'~') into newValueList;
        END IF;
    END IF;





  
     IF ((OLD.pensionRate4 <> NEW.pensionRate4) or (OLD.pensionRate4 is null and NEW.pensionRate4 is not null)
      or (OLD.pensionRate4 is not null and NEW.pensionRate4 is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.pensionRate4~') into fieldNameList;
        IF(OLD.pensionRate4 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate4 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pensionRate4 is not null) then
            select CONCAT(oldValueList,OLD.pensionRate4,'~') into oldValueList;
        END IF;
        IF(NEW.pensionRate4 is not null) then
            select CONCAT(newValueList,NEW.pensionRate4,'~') into newValueList;
        END IF;
    END IF;






    IF ((OLD.pensionEffectiveDate1 <> NEW.pensionEffectiveDate1) or (OLD.pensionEffectiveDate1 is null and NEW.pensionEffectiveDate1 is not null)
      or (OLD.pensionEffectiveDate1 is not null and NEW.pensionEffectiveDate1 is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.pensionEffectiveDate1~') into fieldNameList;
       IF(OLD.pensionEffectiveDate1 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.pensionEffectiveDate1 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.pensionEffectiveDate1 is not null) THEN
        	select CONCAT(oldValueList,OLD.pensionEffectiveDate1,'~') into oldValueList;
     	END IF;
      	IF(NEW.pensionEffectiveDate1 is not null) THEN
        	select CONCAT(newValueList,NEW.pensionEffectiveDate1,'~') into newValueList;
      	END IF;

   END IF;



   

    IF ((OLD.pensionEffectiveDate2 <> NEW.pensionEffectiveDate2) or (OLD.pensionEffectiveDate2 is null and NEW.pensionEffectiveDate2 is not null)
      or (OLD.pensionEffectiveDate2 is not null and NEW.pensionEffectiveDate2 is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.pensionEffectiveDate2~') into fieldNameList;
       IF(OLD.pensionEffectiveDate2 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.pensionEffectiveDate2 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.pensionEffectiveDate2 is not null) THEN
        	select CONCAT(oldValueList,OLD.pensionEffectiveDate2,'~') into oldValueList;
     	END IF;
      	IF(NEW.pensionEffectiveDate2 is not null) THEN
        	select CONCAT(newValueList,NEW.pensionEffectiveDate2,'~') into newValueList;
      	END IF;

   END IF;



   

    IF ((OLD.pensionEffectiveDate3 <> NEW.pensionEffectiveDate3) or (OLD.pensionEffectiveDate3 is null and NEW.pensionEffectiveDate3 is not null)
      or (OLD.pensionEffectiveDate3 is not null and NEW.pensionEffectiveDate3 is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.pensionEffectiveDate3~') into fieldNameList;
       IF(OLD.pensionEffectiveDate3 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.pensionEffectiveDate3 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.pensionEffectiveDate3 is not null) THEN
        	select CONCAT(oldValueList,OLD.pensionEffectiveDate3,'~') into oldValueList;
     	END IF;
      	IF(NEW.pensionEffectiveDate3 is not null) THEN
        	select CONCAT(newValueList,NEW.pensionEffectiveDate3,'~') into newValueList;
      	END IF;

   END IF;




   

    IF ((OLD.pensionEffectiveDate4 <> NEW.pensionEffectiveDate4) or (OLD.pensionEffectiveDate4 is null and NEW.pensionEffectiveDate4 is not null)
      or (OLD.pensionEffectiveDate4 is not null and NEW.pensionEffectiveDate4 is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.pensionEffectiveDate4~') into fieldNameList;
       IF(OLD.pensionEffectiveDate4 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.pensionEffectiveDate4 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.pensionEffectiveDate4 is not null) THEN
        	select CONCAT(oldValueList,OLD.pensionEffectiveDate4,'~') into oldValueList;
     	END IF;
      	IF(NEW.pensionEffectiveDate4 is not null) THEN
        	select CONCAT(newValueList,NEW.pensionEffectiveDate4,'~') into newValueList;
      	END IF;

   END IF;


   

   
IF (OLD.costElement <> NEW.costElement ) THEN
       select CONCAT(fieldNameList,'systemdefault.costElement~') into fieldNameList;
       IF(OLD.costElement = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.costElement = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.costElement = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.costElement = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.transDoc <> NEW.transDoc ) THEN
      select CONCAT(fieldNameList,'systemdefault.transDoc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.transDoc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.transDoc,'~') into newValueList;
  END IF;




  

 IF (OLD.minRec <> NEW.minRec ) THEN
      select CONCAT(fieldNameList,'systemdefault.minRec~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minRec,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minRec,'~') into newValueList;
  END IF;




  

 IF (OLD.maxRec <> NEW.maxRec ) THEN
      select CONCAT(fieldNameList,'systemdefault.maxRec~') into fieldNameList;
      select CONCAT(oldValueList,OLD.maxRec,'~') into oldValueList;
      select CONCAT(newValueList,NEW.maxRec,'~') into newValueList;
  END IF;




  

 IF (OLD.minPay <> NEW.minPay ) THEN
      select CONCAT(fieldNameList,'systemdefault.minPay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minPay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minPay,'~') into newValueList;
  END IF;




  

 IF (OLD.maxPay <> NEW.maxPay ) THEN
      select CONCAT(fieldNameList,'systemdefault.maxPay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.maxPay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.maxPay,'~') into newValueList;
  END IF;




  

 IF (OLD.suspenseDefault <> NEW.suspenseDefault ) THEN
      select CONCAT(fieldNameList,'systemdefault.suspenseDefault~') into fieldNameList;
      select CONCAT(oldValueList,OLD.suspenseDefault,'~') into oldValueList;
      select CONCAT(newValueList,NEW.suspenseDefault,'~') into newValueList;
  END IF;




  

 IF (OLD.currentRec <> NEW.currentRec ) THEN
      select CONCAT(fieldNameList,'systemdefault.currentRec~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currentRec,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currentRec,'~') into newValueList;
  END IF;




  

 IF (OLD.currentPay <> NEW.currentPay ) THEN
      select CONCAT(fieldNameList,'systemdefault.currentPay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currentPay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currentPay,'~') into newValueList;
  END IF;




  

 IF (OLD.vanLineCompanyDivision <> NEW.vanLineCompanyDivision ) THEN
      select CONCAT(fieldNameList,'systemdefault.vanLineCompanyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLineCompanyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLineCompanyDivision,'~') into newValueList;
  END IF;




  

 IF (OLD.uvlContract <> NEW.uvlContract ) THEN
      select CONCAT(fieldNameList,'systemdefault.uvlContract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.uvlContract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.uvlContract,'~') into newValueList;
  END IF;




  

 IF (OLD.uvlBillToCode <> NEW.uvlBillToCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.uvlBillToCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.uvlBillToCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.uvlBillToCode,'~') into newValueList;
  END IF;




  

 IF (OLD.uvlBillToName <> NEW.uvlBillToName ) THEN
      select CONCAT(fieldNameList,'systemdefault.uvlBillToName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.uvlBillToName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.uvlBillToName,'~') into newValueList;
  END IF;




  

 IF (OLD.mvlContract <> NEW.mvlContract ) THEN
      select CONCAT(fieldNameList,'systemdefault.mvlContract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mvlContract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mvlContract,'~') into newValueList;
  END IF;




 

 IF (OLD.mvlBillToCode <> NEW.mvlBillToCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.mvlBillToCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mvlBillToCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mvlBillToCode,'~') into newValueList;
  END IF;




 

 IF (OLD.mvlBillToName <> NEW.mvlBillToName ) THEN
      select CONCAT(fieldNameList,'systemdefault.mvlBillToName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mvlBillToName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mvlBillToName,'~') into newValueList;
  END IF;




 

 IF (OLD.commissionable <> NEW.commissionable ) THEN
      select CONCAT(fieldNameList,'systemdefault.commissionable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.commissionable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.commissionable,'~') into newValueList;
  END IF;




 

 IF (OLD.truckRequired <> NEW.truckRequired ) THEN
      select CONCAT(fieldNameList,'systemdefault.truckRequired~') into fieldNameList;
      select CONCAT(oldValueList,OLD.truckRequired,'~') into oldValueList;
      select CONCAT(newValueList,NEW.truckRequired,'~') into newValueList;
  END IF;




 

 IF (OLD.validation <> NEW.validation ) THEN
      select CONCAT(fieldNameList,'systemdefault.validation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.validation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.validation,'~') into newValueList;
  END IF;




 

 IF (OLD.unitVariable <> NEW.unitVariable ) THEN
      select CONCAT(fieldNameList,'systemdefault.unitVariable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.unitVariable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.unitVariable,'~') into newValueList;
  END IF;






 IF (OLD.autoPayablePosting <> NEW.autoPayablePosting ) THEN
       select CONCAT(fieldNameList,'systemdefault.autoPayablePosting~') into fieldNameList;
       IF(OLD.autoPayablePosting = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.autoPayablePosting = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.autoPayablePosting = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.autoPayablePosting = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.billingCheck <> NEW.billingCheck ) THEN
       select CONCAT(fieldNameList,'systemdefault.billingCheck~') into fieldNameList;
       IF(OLD.billingCheck = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.billingCheck = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.billingCheck = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.billingCheck = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  

 IF (OLD.validation <> NEW.validation ) THEN
      select CONCAT(fieldNameList,'systemdefault.validation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.validation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.validation,'~') into newValueList;
  END IF;





 IF (OLD.unitVariable <> NEW.unitVariable ) THEN
      select CONCAT(fieldNameList,'systemdefault.unitVariable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.unitVariable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.unitVariable,'~') into newValueList;
  END IF;






 IF (OLD.automaticReconcile <> NEW.automaticReconcile ) THEN
       select CONCAT(fieldNameList,'systemdefault.automaticReconcile~') into fieldNameList;
       IF(OLD.automaticReconcile = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.automaticReconcile = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.automaticReconcile = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.automaticReconcile = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





   
  
     IF ((OLD.vanlineMinimumAmount <> NEW.vanlineMinimumAmount) or (OLD.vanlineMinimumAmount is null and NEW.vanlineMinimumAmount is not null)
      or (OLD.vanlineMinimumAmount is not null and NEW.vanlineMinimumAmount is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.vanlineMinimumAmount~') into fieldNameList;
        IF(OLD.vanlineMinimumAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.vanlineMinimumAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.vanlineMinimumAmount is not null) then
            select CONCAT(oldValueList,OLD.vanlineMinimumAmount,'~') into oldValueList;
        END IF;
        IF(NEW.vanlineMinimumAmount is not null) then
            select CONCAT(newValueList,NEW.vanlineMinimumAmount,'~') into newValueList;
        END IF;
    END IF;








 IF (OLD.POS <> NEW.POS ) THEN
       select CONCAT(fieldNameList,'systemdefault.POS~') into fieldNameList;
       IF(OLD.POS = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.POS = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.POS = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.POS = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.vanlineSettleColourStatus <> NEW.vanlineSettleColourStatus ) THEN
       select CONCAT(fieldNameList,'systemdefault.vanlineSettleColourStatus~') into fieldNameList;
       IF(OLD.vanlineSettleColourStatus = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.vanlineSettleColourStatus = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.vanlineSettleColourStatus = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.vanlineSettleColourStatus = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




 IF (OLD.costingDetailShow <> NEW.costingDetailShow ) THEN
       select CONCAT(fieldNameList,'systemdefault.costingDetailShow~') into fieldNameList;
       IF(OLD.costingDetailShow = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.costingDetailShow = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.costingDetailShow = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.costingDetailShow = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.driverCommissionSetUp <> NEW.driverCommissionSetUp ) THEN
       select CONCAT(fieldNameList,'systemdefault.driverCommissionSetUp~') into fieldNameList;
       IF(OLD.driverCommissionSetUp = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.driverCommissionSetUp = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.driverCommissionSetUp = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.driverCommissionSetUp = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.rating <> NEW.rating ) THEN
       select CONCAT(fieldNameList,'systemdefault.rating~') into fieldNameList;
       IF(OLD.rating = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.rating = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.rating = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.rating = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.distribution <> NEW.distribution ) THEN
       select CONCAT(fieldNameList,'systemdefault.distribution~') into fieldNameList;
       IF(OLD.distribution = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.distribution = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.distribution = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.distribution = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.noInsurance <> NEW.noInsurance ) THEN
       select CONCAT(fieldNameList,'systemdefault.noInsurance~') into fieldNameList;
       IF(OLD.noInsurance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noInsurance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noInsurance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noInsurance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.detailedList <> NEW.detailedList ) THEN
       select CONCAT(fieldNameList,'systemdefault.detailedList~') into fieldNameList;
       IF(OLD.detailedList = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.detailedList = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.detailedList = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.detailedList = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.noInsuranceCportal <> NEW.noInsuranceCportal ) THEN
       select CONCAT(fieldNameList,'systemdefault.noInsuranceCportal~') into fieldNameList;
       IF(OLD.noInsuranceCportal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noInsuranceCportal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noInsuranceCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noInsuranceCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





 IF (OLD.lumpSum <> NEW.lumpSum ) THEN
       select CONCAT(fieldNameList,'systemdefault.lumpSum~') into fieldNameList;
       IF(OLD.lumpSum = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.lumpSum = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.lumpSum = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.lumpSum = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;









 IF (OLD.ownbillingVanline <> NEW.ownbillingVanline ) THEN
       select CONCAT(fieldNameList,'systemdefault.ownbillingVanline~') into fieldNameList;
       IF(OLD.ownbillingVanline = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.ownbillingVanline = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.ownbillingVanline = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.ownbillingVanline = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




  
     IF ((OLD.vanlineMinimumAmount <> NEW.vanlineMinimumAmount) or (OLD.vanlineMinimumAmount is null and NEW.vanlineMinimumAmount is not null)
      or (OLD.vanlineMinimumAmount is not null and NEW.vanlineMinimumAmount is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.vanlineMinimumAmount~') into fieldNameList;
        IF(OLD.vanlineMinimumAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.vanlineMinimumAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.vanlineMinimumAmount is not null) then
            select CONCAT(oldValueList,OLD.vanlineMinimumAmount,'~') into oldValueList;
        END IF;
        IF(NEW.vanlineMinimumAmount is not null) then
            select CONCAT(newValueList,NEW.vanlineMinimumAmount,'~') into newValueList;
        END IF;
    END IF;



  
     IF ((OLD.vanlineMaximumAmount <> NEW.vanlineMaximumAmount) or (OLD.vanlineMaximumAmount is null and NEW.vanlineMaximumAmount is not null)
      or (OLD.vanlineMaximumAmount is not null and NEW.vanlineMaximumAmount is null)) THEN
        select CONCAT(fieldNameList,'systemdefault.vanlineMaximumAmount~') into fieldNameList;
        IF(OLD.vanlineMaximumAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.vanlineMaximumAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.vanlineMaximumAmount is not null) then
            select CONCAT(oldValueList,OLD.vanlineMaximumAmount,'~') into oldValueList;
        END IF;
        IF(NEW.vanlineMaximumAmount is not null) then
            select CONCAT(newValueList,NEW.vanlineMaximumAmount,'~') into newValueList;
        END IF;
    END IF;






 IF (OLD.miscellaneousdefaultdriver <> NEW.miscellaneousdefaultdriver ) THEN
      select CONCAT(fieldNameList,'systemdefault.miscellaneousdefaultdriver~') into fieldNameList;
      select CONCAT(oldValueList,OLD.miscellaneousdefaultdriver,'~') into oldValueList;
      select CONCAT(newValueList,NEW.miscellaneousdefaultdriver,'~') into newValueList;
  END IF;




 IF (OLD.dosPartnerCode <> NEW.dosPartnerCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.dosPartnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dosPartnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dosPartnerCode,'~') into newValueList;
  END IF;






 IF (OLD.operationMgmtBy <> NEW.operationMgmtBy ) THEN
      select CONCAT(fieldNameList,'systemdefault.operationMgmtBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.operationMgmtBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.operationMgmtBy,'~') into newValueList;
  END IF;






 IF (OLD.driverCrewType <> NEW.driverCrewType ) THEN
      select CONCAT(fieldNameList,'systemdefault.driverCrewType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverCrewType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverCrewType,'~') into newValueList;
  END IF;






 IF (OLD.insurancePerUnit <> NEW.insurancePerUnit ) THEN
      select CONCAT(fieldNameList,'systemdefault.insurancePerUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insurancePerUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insurancePerUnit,'~') into newValueList;
  END IF;







 IF (OLD.unitForInsurance <> NEW.unitForInsurance ) THEN
      select CONCAT(fieldNameList,'systemdefault.unitForInsurance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.unitForInsurance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.unitForInsurance,'~') into newValueList;
  END IF;






 IF (OLD.minReplacementvalue <> NEW.minReplacementvalue ) THEN
      select CONCAT(fieldNameList,'systemdefault.minReplacementvalue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minReplacementvalue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minReplacementvalue,'~') into newValueList;
  END IF;








 IF (OLD.minInsurancePerUnit <> NEW.minInsurancePerUnit ) THEN
      select CONCAT(fieldNameList,'systemdefault.minInsurancePerUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minInsurancePerUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minInsurancePerUnit,'~') into newValueList;
  END IF;






 IF (OLD.storageEmail <> NEW.storageEmail ) THEN
      select CONCAT(fieldNameList,'systemdefault.storageEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageEmail,'~') into newValueList;
  END IF;






 IF (OLD.vanlineExceptionChargeCode <> NEW.vanlineExceptionChargeCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.vanlineExceptionChargeCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanlineExceptionChargeCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanlineExceptionChargeCode,'~') into newValueList;
  END IF;






  
 
    IF (OLD.weightLimits <> NEW.weightLimits ) THEN
      select CONCAT(fieldNameList,'systemdefault.weightLimits~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weightLimits,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weightLimits,'~') into newValueList;
  END IF;
   
   IF (OLD.spCode <> NEW.spCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.spCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.spCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.spCode,'~') into newValueList;
  END IF;
   
    IF (OLD.dosscac <> NEW.dosscac ) THEN
      select CONCAT(fieldNameList,'systemdefault.dosscac~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dosscac,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dosscac,'~') into newValueList;
  END IF;
   
    IF (OLD.paymentApplicationExtractSeq <> NEW.paymentApplicationExtractSeq ) THEN
      select CONCAT(fieldNameList,'systemdefault.paymentApplicationExtractSeq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.paymentApplicationExtractSeq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.paymentApplicationExtractSeq,'~') into newValueList;
  END IF;
   
     IF (OLD.ownbillingBilltoCodes <> NEW.ownbillingBilltoCodes ) THEN
      select CONCAT(fieldNameList,'systemdefault.ownbillingBilltoCodes~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ownbillingBilltoCodes,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ownbillingBilltoCodes,'~') into newValueList;
  END IF;
   
     IF (OLD.customerSurveyEmail <> NEW.customerSurveyEmail ) THEN
      select CONCAT(fieldNameList,'systemdefault.customerSurveyEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.customerSurveyEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.customerSurveyEmail,'~') into newValueList;
  END IF;
   
  IF (OLD.soExtractSeq <> NEW.soExtractSeq ) THEN
      select CONCAT(fieldNameList,'systemdefault.soExtractSeq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.soExtractSeq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.soExtractSeq,'~') into newValueList;
  END IF;
   
   IF (OLD.passwordPolicy <> NEW.passwordPolicy ) THEN
       select CONCAT(fieldNameList,'systemdefault.passwordPolicy~') into fieldNameList;
       IF(OLD.passwordPolicy = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.passwordPolicy = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.passwordPolicy = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.passwordPolicy = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.accountLineAccountPortalFlag <> NEW.accountLineAccountPortalFlag ) THEN
       select CONCAT(fieldNameList,'systemdefault.accountLineAccountPortalFlag~') into fieldNameList;
       IF(OLD.accountLineAccountPortalFlag = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accountLineAccountPortalFlag = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accountLineAccountPortalFlag = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accountLineAccountPortalFlag = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     IF (OLD.hubWarehouseOperationalLimit <> NEW.hubWarehouseOperationalLimit ) THEN
      select CONCAT(fieldNameList,'systemdefault.hubWarehouseOperationalLimit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.hubWarehouseOperationalLimit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.hubWarehouseOperationalLimit,'~') into newValueList;
  END IF;
   
     IF (OLD.localJobs <> NEW.localJobs ) THEN
      select CONCAT(fieldNameList,'systemdefault.localJobs~') into fieldNameList;
      select CONCAT(oldValueList,OLD.localJobs,'~') into oldValueList;
      select CONCAT(newValueList,NEW.localJobs,'~') into newValueList;
  END IF;
   
   IF (OLD.contractChargesMandatory <> NEW.contractChargesMandatory ) THEN
      select CONCAT(fieldNameList,'systemdefault.contractChargesMandatory~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractChargesMandatory,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractChargesMandatory,'~') into newValueList;
  END IF;
   
   IF ((OLD.maxupdatedon <> NEW.maxupdatedon) or (OLD.maxupdatedon is null and NEW.maxupdatedon is not null)
      or (OLD.maxupdatedon is not null and NEW.maxupdatedon is null)) THEN

       select CONCAT(fieldNameList,'systemdefault.maxupdatedon~') into fieldNameList;
       IF(OLD.maxupdatedon is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.maxupdatedon is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.maxupdatedon is not null) THEN
        	select CONCAT(oldValueList,OLD.maxupdatedon,'~') into oldValueList;
     	END IF;
      	IF(NEW.maxupdatedon is not null) THEN
        	select CONCAT(newValueList,NEW.maxupdatedon,'~') into newValueList;
      	END IF;

   END IF;
   
    IF (OLD.receivableVat <> NEW.receivableVat ) THEN
      select CONCAT(fieldNameList,'systemdefault.receivableVat~') into fieldNameList;
      select CONCAT(oldValueList,OLD.receivableVat,'~') into oldValueList;
      select CONCAT(newValueList,NEW.receivableVat,'~') into newValueList;
  END IF;
   
    IF (OLD.payableVat <> NEW.payableVat ) THEN
      select CONCAT(fieldNameList,'systemdefault.payableVat~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableVat,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableVat,'~') into newValueList;
  END IF;
  
   IF (OLD.defaultSortforFileCabinet <> NEW.defaultSortforFileCabinet ) THEN
      select CONCAT(fieldNameList,'systemdefault.defaultSortforFileCabinet~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultSortforFileCabinet,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultSortforFileCabinet,'~') into newValueList;
  END IF;
  
  
   IF (OLD.sortOrder <> NEW.sortOrder ) THEN
      select CONCAT(fieldNameList,'systemdefault.sortOrder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sortOrder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sortOrder,'~') into newValueList;
  END IF;
  
   IF (OLD.prefix <> NEW.prefix ) THEN
      select CONCAT(fieldNameList,'systemdefault.prefix~') into fieldNameList;
      select CONCAT(oldValueList,OLD.prefix,'~') into oldValueList;
      select CONCAT(newValueList,NEW.prefix,'~') into newValueList;
  END IF;
  
   IF (OLD.sequence <> NEW.sequence ) THEN
      select CONCAT(fieldNameList,'systemdefault.sequence~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sequence,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sequence,'~') into newValueList;
  END IF;
  
  IF (OLD.daystoManageAlert <> NEW.daystoManageAlert ) THEN
      select CONCAT(fieldNameList,'systemdefault.daystoManageAlert~') into fieldNameList;
      select CONCAT(oldValueList,OLD.daystoManageAlert,'~') into oldValueList;
      select CONCAT(newValueList,NEW.daystoManageAlert,'~') into newValueList;
  END IF;

  IF (OLD.reciprocityjobtype <> NEW.reciprocityjobtype ) THEN
      select CONCAT(fieldNameList,'systemdefault.reciprocityjobtype~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reciprocityjobtype,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reciprocityjobtype,'~') into newValueList;
  END IF;

  IF (OLD.service <> NEW.service ) THEN
      select CONCAT(fieldNameList,'systemdefault.service~') into fieldNameList;
      select CONCAT(oldValueList,OLD.service,'~') into oldValueList;
      select CONCAT(newValueList,NEW.service,'~') into newValueList;
  END IF;

  IF (OLD.warehouse <> NEW.warehouse ) THEN
      select CONCAT(fieldNameList,'systemdefault.warehouse~') into fieldNameList;
      select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
      select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
  END IF;

  IF (OLD.payableActual <> NEW.payableActual ) THEN
      select CONCAT(fieldNameList,'systemdefault.payableActual~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableActual,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableActual,'~') into newValueList;
  END IF;

  IF (OLD.receivableActual <> NEW.receivableActual ) THEN
      select CONCAT(fieldNameList,'systemdefault.receivableActual~') into fieldNameList;
      select CONCAT(oldValueList,OLD.receivableActual,'~') into oldValueList;
      select CONCAT(newValueList,NEW.receivableActual,'~') into newValueList;
  END IF;

  
  IF (OLD.payableAccrued <> NEW.payableAccrued ) THEN
      select CONCAT(fieldNameList,'systemdefault.payableAccrued~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableAccrued,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableAccrued,'~') into newValueList;
  END IF;

   IF (OLD.receivableAccrued <> NEW.receivableAccrued ) THEN
      select CONCAT(fieldNameList,'systemdefault.receivableAccrued~') into fieldNameList;
      select CONCAT(oldValueList,OLD.receivableAccrued,'~') into oldValueList;
      select CONCAT(newValueList,NEW.receivableAccrued,'~') into newValueList;
  END IF;
  
  
   IF (OLD.intBillToCode <> NEW.intBillToCode ) THEN
      select CONCAT(fieldNameList,'systemdefault.intBillToCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.intBillToCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.intBillToCode,'~') into newValueList;
  END IF;
  
  
   IF (OLD.intBillToName <> NEW.intBillToName ) THEN
      select CONCAT(fieldNameList,'systemdefault.intBillToName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.intBillToName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.intBillToName,'~') into newValueList;
  END IF;
  
  
 IF (OLD.intContract <> NEW.intContract ) THEN
      select CONCAT(fieldNameList,'systemdefault.intContract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.intContract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.intContract,'~') into newValueList;
  END IF;
  
  
 IF (OLD.defaultDivisionCharges <> NEW.defaultDivisionCharges ) THEN
      select CONCAT(fieldNameList,'systemdefault.defaultDivisionCharges~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultDivisionCharges,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultDivisionCharges,'~') into newValueList;
  END IF;
  
IF (OLD.passwordPolicyForExternalUser <> NEW.passwordPolicyForExternalUser ) THEN
       select CONCAT(fieldNameList,'systemdefault.passwordPolicyForExternalUser~') into fieldNameList;
       IF(OLD.passwordPolicyForExternalUser = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.passwordPolicyForExternalUser = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.passwordPolicyForExternalUser = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.passwordPolicyForExternalUser = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



 CALL add_tblHistory (OLD.id,"systemdefault", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());

END 
$$


delimiter;
