package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.InventoryPackingDao;
import com.trilasoft.app.model.InventoryPacking;
import com.trilasoft.app.service.InventoryPackingManager;

public class InventoryPackingManagerImpl extends GenericManagerImpl<InventoryPacking, Long> implements InventoryPackingManager{
    InventoryPackingDao inventoryPackingDao; 
	public InventoryPackingManagerImpl(InventoryPackingDao inventoryPackingDao) {
		super(inventoryPackingDao);
		this.inventoryPackingDao=inventoryPackingDao;
	}
	public List searchCriteria(Long sid,String  room,String  article,String sessionCorpID){
		return inventoryPackingDao.searchCriteria(sid, room, article, sessionCorpID);		
	}
	public List seachInventoryThroughPieceId(Long sid,String pieceID,String sessionCorpID){
		return inventoryPackingDao.seachInventoryThroughPieceId(sid, pieceID, sessionCorpID);
	}
	public List findArticleByPieceId(Long sid, String pieceID,String sessionCorpID){
		return inventoryPackingDao.findArticleByPieceId(sid, pieceID, sessionCorpID);
	}
}
