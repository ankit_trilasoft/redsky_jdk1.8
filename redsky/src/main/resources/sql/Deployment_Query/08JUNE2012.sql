ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `CAR_vendorCodeEXSO` VARCHAR(25) AFTER `VIS_expiryReminder3MosPriorToExpiry`, ADD COLUMN `COL_vendorCodeEXSO` VARCHAR(25) AFTER `CAR_vendorCodeEXSO`, ADD COLUMN `TRG_vendorCodeEXSO` VARCHAR(25) AFTER `COL_vendorCodeEXSO`, ADD COLUMN `HOM_vendorCodeEXSO` VARCHAR(25) AFTER `TRG_vendorCodeEXSO`, ADD COLUMN `RNT_vendorCodeEXSO` VARCHAR(25) AFTER `HOM_vendorCodeEXSO`, ADD COLUMN `LAN_vendorCodeEXSO` VARCHAR(25) AFTER `RNT_vendorCodeEXSO`, ADD COLUMN `MMG_vendorCodeEXSO` VARCHAR(25) AFTER `LAN_vendorCodeEXSO`, ADD COLUMN `ONG_vendorCodeEXSO` VARCHAR(25) AFTER `MMG_vendorCodeEXSO`, ADD COLUMN `PRV_vendorCodeEXSO` VARCHAR(25) AFTER `ONG_vendorCodeEXSO`, ADD COLUMN `AIO_vendorCodeEXSO` VARCHAR(25) AFTER `PRV_vendorCodeEXSO`, ADD COLUMN `EXP_vendorCodeEXSO` VARCHAR(25) AFTER `AIO_vendorCodeEXSO`, ADD COLUMN `RPT_vendorCodeEXSO` VARCHAR(25) AFTER `EXP_vendorCodeEXSO`, ADD COLUMN `SCH_vendorCodeEXSO` VARCHAR(25) AFTER `RPT_vendorCodeEXSO`, ADD COLUMN `TAX_vendorCodeEXSO` VARCHAR(25) AFTER `SCH_vendorCodeEXSO`, ADD COLUMN `TAC_vendorCodeEXSO` VARCHAR(25) AFTER `TAX_vendorCodeEXSO`, ADD COLUMN `TEN_vendorCodeEXSO` VARCHAR(25) AFTER `TAC_vendorCodeEXSO`, ADD COLUMN `VIS_vendorCodeEXSO` VARCHAR(25) AFTER `TEN_vendorCodeEXSO`, ADD COLUMN `SET_vendorCodeEXSO` VARCHAR(25) AFTER `VIS_vendorCodeEXSO`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `validation` VARCHAR(45) DEFAULT NULL AFTER `commissionable`;

ALTER TABLE `redsky`.`inventorydata` MODIFY COLUMN `atricle` VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`subcontractorcharges` ADD COLUMN `job` VARCHAR(3) ;

update networkdatafields set type ='' where fieldName='serviceType';

 
