<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

  
<head>   
    <title>Resource Details</title>   
    <meta name="heading" content="Resource"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">

    function validation(){
    	var type = document.forms['resourceForm'].elements['itemsJbkEquip.type'].value;
    	var descript = document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value;
    	if(type.length == 0 || descript.length == 0){
        	alert('Enter Mandatory Field(s)');
        	return false;
    	}else{
        	return true;
    	}
    }
    
    function closeWIN(){
    	window.close();
    }
    </script>

</head>
<style type=text/css>
.input-textRight {
    border: 1px solid #219DD1;
    color: #000000;
    font-family: arial,verdana;
    font-size: 12px;
    text-align:right;
    height: 15px;
    text-decoration: none;
}
div.autocomplete {
      position:absolute;
      width:250px;
      background-color:white;
      border:1px solid #888;
      margin:0px;
      padding:0px;
      z-index:99999;
    }
    div.autocomplete ul {
      list-style-type:none;
      margin:0px;
      padding:0px;
    }
    div.autocomplete ul li.selected { background-color: #ffb;}
    div.autocomplete ul li {
      list-style-type:none;
      display:block;
      margin:0;
      padding:0px;
      padding-left:3px;   
      cursor:pointer;
    }
</style>

<c:set var="buttons"> 
    <input type="submit"" class="cssbutton1" onclick="return validation();" value="Save" style="width:60px; height:25px" tabindex="83"/> 
	<input type="button" class="cssbutton1" onclick="closeWIN();" value="Close" style="width:60px; height:25px" tabindex="83"/> 
</c:set>

<s:form name="resourceForm" id="resourceForm" action="saveResource.html?btntype=yes&decorator=popup&popup=true" method="post" validate="true">

<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="itemsJbkEquip.id" />
<s:hidden name="id" value="${itemsJbkEquip.id}"/>  
<s:hidden name="itemsJbkEquip.shipNum" value="<%=request.getParameter("shipNum")%>"/>

<div id="layer1" style="width:500px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Resources<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:95%" onkeydown="">
<table class="" cellspacing="0" cellpadding="0" border="0" width="95%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0"  style="margin-left:30px">
<tbody>
<tr>
<td align="left" width="75px" class="listwhitetext" valign="top"></td>
<td align="left" width="75px" class="listwhitetext" valign="bottom">Category<font color="red" size="2">*</font></td>
<td align="left" width="75px" class="listwhitetext" valign="bottom">Resource<font color="red" size="2">*</font></td> 
<td align="left" width="75px" class="listwhitetext" valign="bottom">Est. Quantity<font color="red" size="2"></font></td>  
</tr>
<tr>
<td align="left" width="75px" class="listwhitetext" valign="top"></td>
<td align="left" class="listwhitetext" valign="top"><s:select cssClass="list-menu" name="itemsJbkEquip.type" onchange="setDesc('TYPE');" id="type" list="%{resourceCategory}" cssStyle="width:145px"  tabindex="28" /></td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="itemsJbkEquip.descript" id="autocomplete" onkeyup="getResources1(this.value);"  maxlength="20"  required="true" cssClass="input-text" size="20" />
<span id="indicator1" style="display: none"><img src="/images/spinner.gif" alt="Working..." /></span>
<div id="autocomplete_choices" class="autocomplete" ></div> 
</td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="itemsJbkEquip.cost" onchange="checkChar();" maxlength="6"  required="true" cssClass="input-text" size="20" tabindex="1" /></td>
</tr>
<tr>
<td align="left" width="75px" class="listwhitetext" valign="top"></td>
<td align="left" width="75px" class="listwhitetext" valign="top">Comments</td>
<td align="left" width="75px" class="listwhitetext" valign="bottom"></td> 
<td align="left" width="75px" class="listwhitetext" valign="bottom"></td>  
</tr>
<tr>
<td  align="left" valign="top" colspan="5">
<s:textarea name="itemsJbkEquip.comments" required="true" cssClass="textarea" tabindex="1" cols="67" rows="5"  
	onkeydown="limitText(150);" 
	onkeyup="limitText(150);"/></td> 
 </tr>

<tr><td align="left" height="15px"></td></tr>
<tr>
     <td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">						
		<c:out value="${buttons}" escapeXml="false" />					
	</td>
	</tr>
	<tr></tr>
	<tr></tr>
</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 



<table width="500px" style="display:none;">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${itemsJbkEquip.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="itemsJbkEquip.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${itemsJbkEquip.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>						
						<c:if test="${not empty itemsJbkEquip.id}">
								<s:hidden name="itemsJbkEquip.createdBy"/>
								<td ><s:label name="createdBy" value="%{itemsJbkEquip.createdBy}"/></td>
							</c:if>
							<c:if test="${empty itemsJbkEquip.id}">
								<s:hidden name="itemsJbkEquip.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${itemsJbkEquip.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="itemsJbkEquip.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${itemsJbkEquip.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty itemsJbkEquip.id}">
							<s:hidden name="itemsJbkEquip.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{itemsJbkEquip.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty itemsJbkEquip.id}">
							<s:hidden name="itemsJbkEquip.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
 
</div>
</s:form> 

<script type="text/javascript">  
try{
	 <c:if test="${btntype=='YES'}">
		parent.window.opener.document.location.reload();
		window.close();
	 </c:if>	
	 <c:if test="${btntype=='DUPCHECK'}">
		alert('Category Resource is already exist.');
		document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value='';
	 </c:if>	
}catch(e){
	alert(e);
}
</script>
<script type="text/javascript">
function checkChar(){
	var strString = document.forms['resourceForm'].elements['itemsJbkEquip.cost'].value;
	 if(parseInt(strString) == strString){
		 return true;
	 }else if(/^\d+\.\d+$/.test(strString)){
		  return true;
	 }else{
		 alert('Enter valid Number');
		 document.forms['resourceForm'].elements['itemsJbkEquip.cost'].value='';
		 return false;
	 }
	return true;
}

function limitText(size) {
	 	var descLimit = document.forms['resourceForm'].elements['itemsJbkEquip.comments'].value;
	 	if(descLimit.length > size){
	 		descLimit = descLimit.substring(0, size);
	 		document.forms['resourceForm'].elements['itemsJbkEquip.comments'].value = descLimit;
	 	}
 } 
</script>	

<script type="text/javascript" >

function getResources(str){
	var type = document.forms['resourceForm'].elements['itemsJbkEquip.type'].value;
	var resr = document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value;
	if(resr.length > 0){
		var url="getResource1.html?ajax=1&decorator=simple&popup=true&type="+type+"&resource="+resr;
		http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse2;
	    http2.send(null);
	}else{
	}
}

function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
             alert(results);
            var res = results.split("@"); 
           // document.getElementById('autocomplete').value=results.innerHTML;
          //  document.getElementById('autocomplete_choices').value=results.innerHTML;
         //   document.getElementById('autocomplete_choices').innerHTML=results;
	} 
}
	
</script>


<script type="text/javascript">
var http3 = getHTTPObject();
var http2 = getHTTPObject(); 
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
</script>

<script type="text/javascript">

function getResources1(str){
	checkChar();
	var type = document.forms['resourceForm'].elements['itemsJbkEquip.type'].value;
	var resr = document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value;
	
	if(resr.length+1 > 0){
		document.getElementById('autocomplete_choices').style.display='block';
		new Ajax.Autocompleter("autocomplete", "autocomplete_choices", "/redsky/getResource.html?decorator=simple&popup=true&type="+type+"&resource="+resr, {afterUpdateElement : getSelectionId});
	}
}
function checkChar() {
	var capString = document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value;
	
	 valid(document.forms['resourceForm'].elements['itemsJbkEquip.descript'],'special');
		 var chArr = new Array("%","&","'","|","\\","\"");
	 	 var origString = document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value;
	 	for ( var i = 0; i < chArr.length; i++ ) {
	 		origString = origString.split(chArr[i]).join(''); 
	 	}
	 	document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value = origString;
}
</script>

<script type="text/javascript">
window.onload=function(){
	document.getElementById('autocomplete_choices').style.display='none';
	try{
	var type = document.forms['resourceForm'].elements['itemsJbkEquip.type'].value;
	}catch(e){}
//	var resr = document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value;
//	new Ajax.Autocompleter("autocomplete","autocomplete_choices", "/redsky/getResource.html?decorator=simple&popup=true&type="+type, {afterUpdateElement : getSelectionId});
	setDesc();
}

function getSelectionId(text, li) {
//		alert('hello');
//	    alert (li.id);
}

function checkValue(result){
	 document.getElementById('autocomplete').value=result.innerHTML;
	}
function getResourcesAjax(){
	var type = document.forms['resourceForm'].elements['itemsJbkEquip.type'].value;
	resr='';
	if(type.length > 0){
		var url="getResource1.html?ajax=1&decorator=simple&popup=true&type="+type+"&resource="+resr;
		http3.open("GET", url, true);
	    http3.onreadystatechange = handleHttpResponse3;
	    http3.send(null);
	}
}
function handleHttpResponse3(){
	if (http3.readyState == 4){
            var results = http3.responseText;
            results = results.trim();
            var res = results.split("@"); 
            for(i=0;i<res.length;i++){

			//	document.getElementById('resourceForm_Resource').options[i].text=res[i];
            //	document.getElementById('ab')=res[i];
            	//document.getElementById('resourceForm_Resource').options[i].text=res[i];
				//document.forms['resourceForm'].elements['Resource'].options[i].text = res[i];
			//	document.forms['resourceForm'].elements['Resource'].options[i].value = res[i];
        	
            //	document.getElementById('resourceForm_Resource').value=res[i];
            	 alert(res[i]);
            }
	} 
}
function setDesc(str){
//	getResourcesAjax();
	var type = document.forms['resourceForm'].elements['itemsJbkEquip.type'].value;
	if(type.length > 0){
		document.forms['resourceForm'].elements['itemsJbkEquip.descript'].readOnly = false;
	}else{
		document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value='';
		document.forms['resourceForm'].elements['itemsJbkEquip.descript'].readOnly = true;
	}
	if(str == 'TYPE'){
		document.forms['resourceForm'].elements['itemsJbkEquip.descript'].value='';
	}
}
</script>
	