<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Work Order</title>   
<meta name="heading" content="Tool Tip"/> 
<style>
.table thead th, .tableHeaderTable td {height:15px;}
</style>
</head>
<s:form action="" name="scopePopupOI">
<table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" width="272" style="!margin-top:5px;margin-bottom:5px">	
	<tbody>	
	<tr><td class="subcontent-tab" style="height:17px;font-size:12px;background:url(images/basic-inner-blue.png) repeat-x scroll 0 0 #3dafcb;border-bottom: 1px solid #3dafcb;border-top: 1px solid #3dafcb; color: #fff;font-family: arial,verdana;"><font>Scope of &nbsp;${operationsIntelligence.workorder}</font></td></tr>
	<tr>
	<td valign="top">			
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;padding-top:10px;" >
	<tbody>
	<tr>
	<td style="width:11px;margin-top:5px;"> </td>
	<td valign="top">
	<%--   <s:textarea cssStyle="width:420px;height:150px;resize:vertical;" cssClass="input-text pr-f11" id="scopeOfWorkOrder" value="${scopeOfWorkOrder}" name="scopeOfWorkOrder1"> </s:textarea>  --%>
 	<span style="white-space:pre-warp;width:300px;font-size:11px;"><c:out value="${operationsIntelligence.scopeOfWorkOrder}"></c:out></span>
 	
	</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	
<c:if test="${usertype != 'ACCOUNT' }">
    <table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0"  style="width:100%;padding-top:10px;margin-bottom:5px;" >
    <tr>
    <td><b># Computers</b></td>
    <td><b># Employee</b></td>
    </tr>
    <tr><td>
    <s:textfield id="numberOfComputer" name="operationsIntelligence.numberOfComputer" readonly="true" maxlength="6" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyNumberAllowed(this)"></s:textfield>
    </td>
    <td><s:textfield id="numberOfEmployee" name="operationsIntelligence.numberOfEmployee" readonly="true" maxlength="6" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyNumberAllowed(this)"></s:textfield></td>
    </tr>
    
     <tr>
    <td><b>Sales Tax</b></td>
    <td><b>Equipment Rental</b></td>
    </tr>
    <tr><td>
    <tr><td>
    <s:textfield id="salesTax" name="operationsIntelligence.salesTax" maxlength="15" readonly="true" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyFloatAllowed(this)"></s:textfield>
    </td>
    <td><s:textfield id="consumables" name="operationsIntelligence.consumables" maxlength="15"  readonly="true" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyFloatAllowed(this)"></s:textfield></td>
    <c:set var="consumablePercentFlag" value="false"/>
	<c:if test="${operationsIntelligence.estimateConsumablePercentage}">
		<c:set var="consumablePercentFlag" value="true"/>
	</c:if>
	<td align="left" width="23" valign="top" ><s:checkbox id="estimateConsumablePercentage" key="operationsIntelligence.estimateConsumablePercentage" value="${consumablePercentFlag}" /></td>
    </tr>
     <tr>
     <td >AB5 Surcharge</td>
    <td >Edit AB5 Surcharge</td>
    </tr>

    <tr>
    <td><s:textfield id="aB5Surcharge" name="operationsIntelligence.aB5Surcharge" maxlength="15"  readonly="true" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyFloatAllowed(this)"></s:textfield></td>
    <c:set var="consumablePercentFlag" value="false"/>
	<c:if test="${operationsIntelligence.editAB5Surcharge}">
		<c:set var="editAB5Surcharge" value="true"/>
	</c:if>
	<td align="left" width="23" valign="top" ><s:checkbox id="editAB5Surcharge" key="operationsIntelligence.editAB5Surcharge" value="${editAB5Surcharge}" /></td>
    </tr>
   
    </tbody>
    </table>
    
     <s:set name="crewList" value="crewList" scope="request"/>
    <c:if test="${crewList !=null &&  crewList!='[]'}"> 
     <display:table name="crewList" class="table-price" requestURI="" id="crewList" style="margin-bottom:5px;"> 
     <display:column title="Name">${crewList.firstName} ${crewList.lastName}</display:column>
    <%--  <display:column property="lastName" title="Last&nbsp;Name"></display:column> --%>
      <display:column title="Crew&nbsp;Type">
     <c:if test="${crewList.contractor }">
     Contractor
     </c:if>
     <c:if test="${!(crewList.contractor )}">
      Employee
     </c:if> 
      </display:column>
    </display:table>
    </c:if>
    
    
    </c:if>
	
	</tbody>
</table>
</s:form >
	   
