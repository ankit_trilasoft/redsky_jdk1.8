<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title>News  Upload</title>   
    <meta name="heading" content="New Update Upload"/>  

<style type="text/css">
    #mainPopup {
	padding-left:0px;
	padding-right:0px;
	
}
div#page {
margin:0;
padding:0;
text-align:center;

}
.center-content {
    background: url("../images/rightside.gif") repeat-y scroll right top transparent;
    margin: -1px 0 -50px;
    padding: 1px 25px 1px 20px;
    position: relative;
}
</style>
</head>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
</script>  

<script language="javascript" type="text/javascript">
       
       
       function cancel(){
       var corpID=document.forms['fileUploadForm'].elements['corpID'].value
       	location.href="newsUpdates.html?decorator=popup&popup=true&corpID="+corpID;
       }
         
       function setFlagValue(){
		document.forms['fileUploadForm'].elements['hitFlag'].value = '1';
	   }
	   
	  
	   
	   function ge(dateObj){
			  return new Date(new Date(dateObj.getFullYear(), dateObj.getUTCMonth(),1)-1);
		  	}
	   	   
	   function changeStatus(){
		   var effDate =document.forms['fileUploadForm'].elements['newsUpdate.effectiveDate'].value;
		   var endDate =document.forms['fileUploadForm'].elements['newsUpdate.endDisplayDate'].value;
		   var days =document.forms['fileUploadForm'].elements['newsUpdate.retention'].value;
		   if(days==null ||days==""){
			   	 alert("Please select the Valid Month");
			   	return false;
			    	}
			var   days1=parseInt(days);
			 //  alert(days1+1);
			var mySplitResult = effDate.split("-");
			var day = mySplitResult[0];
			var month = mySplitResult[1];
			var year = mySplitResult[2];
			  
			 if(month == 'Jan'){
			       month = "01";
			   }else if(month == 'Feb'){
			       month = "02";
			   }else if(month == 'Mar'){
			       month = "03"
			   }else if(month == 'Apr'){
			       month = "04"
			   }else if(month == 'May'){
			       month = "05"
			   }else if(month == 'Jun'){
			       month = "06"
			   }else if(month == 'Jul'){
			       month = "07"
			   }else if(month == 'Aug'){
			       month = "08"
			   }else if(month == 'Sep'){
			       month = "09"
			   }else if(month == 'Oct'){
			       month = "10"
			   }else if(month == 'Nov'){
			       month = "11"
			   }else if(month == 'Dec'){
			       month = "12";
			   }
			  year = "20"+year;
			  var dateObj = new Date(year,month-1,day);
			   // alert("previous "+dateObj);
			 if(effDate==null || effDate=="")
			 {
				 
				endDate ="";
			 }else{
				   dateObj.setMonth(dateObj.getMonth()+(days1+1));
				   document.forms['fileUploadForm'].elements['newsUpdate.endDisplayDate'].value = Calendar.printDate(new Date(ge(dateObj)),"%d-%b-%y");
				  
			   }
	   }
	   
	  
	   function fillEndDate(){
			var effDate =document.forms['fileUploadForm'].elements['newsUpdate.effectiveDate'].value;
			var endDate =document.forms['fileUploadForm'].elements['newsUpdate.endDisplayDate'].value;
			var days =document.forms['fileUploadForm'].elements['newsUpdate.retention'].value;
			 var   days1=parseInt(days);
			 // alert(effDate)
		  if(effDate!=null && effDate!='' && days!=null && days!=''){			 			  
			var mySplitResult = effDate.split("-");
			var day = mySplitResult[0];
			var month = mySplitResult[1];
			var year = mySplitResult[2];
			   
			  if(month == 'Jan')
			   {
			       month = "01";
			   }
			   else if(month == 'Feb')
			   {
			       month = "02";
			   }
			   else if(month == 'Mar')
			   {
			       month = "03"
			   }
			   else if(month == 'Apr')
			   {
			       month = "04"
			   }
			   else if(month == 'May')
			   {
			       month = "05"
			   }
			   else if(month == 'Jun')
			   {
			       month = "06"
			   }
			   else if(month == 'Jul')
			   {
			       month = "07"
			   }
			   else if(month == 'Aug')
			   {
			       month = "08"
			   }
			   else if(month == 'Sep')
			   {
			       month = "09"
			   }
			   else if(month == 'Oct')
			   {
			       month = "10"
			   }
			   else if(month == 'Nov')
			   {
			       month = "11"
			   }
			   else if(month == 'Dec')
			   {
			       month = "12";
			   }
			  year = "20"+year;
			  var dateObj = new Date(year,month-1,day);
			   // alert("previous "+dateObj);
			   dateObj.setMonth(dateObj.getMonth()+(days1+1));
			   document.forms['fileUploadForm'].elements['newsUpdate.endDisplayDate'].value = Calendar.printDate(new Date(ge(dateObj)),"%d-%b-%y"); 
	   
	   }
	   }
	   
	   
	   var formatDate = function (formatDate, formatString) {
			if(formatDate instanceof Date) {
				var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				var monthsArr = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				
				var yyyy = formatDate.getFullYear();
				var yy = yyyy.toString().substring(2);
				var m = formatDate.getMonth();
				alert(m)
				var min = monthsArr[m];
				alert(min);
				var m = m < 10 ? "0" + m : m;
				var d = formatDate.getDate();
				var dd = d < 10 ? "0" + d : d;
				
				var h = formatDate.getHours();
				var hh = h < 10 ? "0" + h : h;
				var n = formatDate.getMinutes();
				var nn = n < 10 ? "0" + n : n;
				var s = formatDate.getSeconds();
				var ss = s < 10 ? "0" + s : s;

				formatString = formatString.replace(/yyyy/i, yyyy);
				formatString = formatString.replace(/yy/i, yy);
				alert(min);
				formatString = formatString.replace(/mmm/i, min);
				alert(formatString);
			    //formatString = formatString.replace(/mm/i, mm);
				//formatString = formatString.replace(/m/i, m);
				formatString = formatString.replace(/dd/i, dd);
				formatString = formatString.replace(/d/i, d);
				formatString = formatString.replace(/hh/i, hh);
				formatString = formatString.replace(/h/i, h);
				formatString = formatString.replace(/nn/i, nn);
				formatString = formatString.replace(/n/i, n);
				formatString = formatString.replace(/ss/i, ss);
				formatString = formatString.replace(/s/i, s);
			return formatString;
			} else {
				return "";
			}
		}
	   </script>
	   <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
	   
	    <script language="javascript" type="text/javascript">
			<%@ include file="/common/formCalender.js"%>
		</script> 
	   
	    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
		<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

	<!-- Modification closed here -->
<script>
function winLoad(){
	parent.window.opener.document.location.reload(); 
	//window.opener.getSubmitParent();
	window.close();
}



function progressBar(tar){
showOrHide(tar);
} 

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
   try{
   document.getElementById("successMessages").style.visibility='hidden'; 
   }catch (e) { 
   }
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
function ValidateUpload(){

	var fileValue = document.forms['fileUploadForm'].elements['file'].value; 
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	}
			
	else{ 
		var extension=fileValue.substring(fileValue.indexOf('.')+1, fileValue.length);
			document.forms['fileUploadForm'].elements['fileExt'].value = extension;
			if(extension=='xls' || extension=='xlsx'){
				submitForUpload();
				progressBar('1');
				return true;
       		}else{
         		alert( "Please select a valid file format.xls and .xlsx are only allowed" ) ;
         		return false;
         	}
	}
} 


var basestring="window"+new Date().getTime();

function zfill(n){ //this function to fill in zeros so there will always be 2 digits for time display
n=n+'';
while(n.length<2)n="0"+n;
return n;
}

function writeWindow(){
var n=new Date();
var x=window.open('', 'newWin'+n.getTime(), "width=300,height=100");
var txt='';
txt+='<html>';
txt+='<head><title>A dynamic page</title></head>';
txt+='<body>';
txt+='This content was written to the window dynamically. ';
txt+='The time is '+zfill(n.getHours())+':'+zfill(n.getMinutes())+':'+zfill(n.getSeconds());
txt+='</body>';
txt+='</html>';
x.document.write(txt);
x.focus();
}

function openWindow(url,w,h,tb,stb,l,mb,sb,rs,x,y){
var pos=(document.layers)? ',screenX='+x+',screenY='+y: ',left='+x+',top='+y;
tb=(tb)?'yes':'no';
stb=(stb)?'yes':'no';
l=(l)?'yes':'no';
mb=(mb)?'yes':'no';
sb=(sb)?'yes':'no';
rs=(rs)?'yes':'no';
var txt='';
txt+='scrollbars='+sb;
txt+=',width='+w;
txt+=',height='+h;
txt+=',toolbar='+tb;
txt+=',status='+stb;
txt+=',menubar='+mb;
txt+=',links='+l;
txt+=',resizable='+rs;
var x=window.open(url, 'newWin'+new Date().getTime(), txt+pos);
x.focus();
}


function submitForUpload(){
	document.forms['fileUploadForm'].action = "uploadXlsFileProcessing.html";
	document.forms['fileUploadForm'].submit();
}



</script>

<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:800px;
}
</style>
</head> 
 <DIV ID="layerH">
 <table border="0" width="100%" align="middle">
 <tr>
 <td align="center">
	<font size="4" color="#1666C9"><b><blink>Uploading...</blink></b></font>
	</td>
	</tr>
	</table>
</DIV> 
<s:form id="fileUploadForm" name="fileUploadForm" enctype="multipart/form-data" method="post" validate="true"> 
<s:hidden name="btnType" value="<%=request.getParameter("buttonType") %>"/>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="details" value="<%=request.getParameter("details")%>"/>
	<s:hidden name="functionality" value="<%=request.getParameter("functionality")%>"/>
<s:hidden name="newsUpdate.id" value="%{newsUpdate.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="id1" value="<%=request.getParameter("id")%>"/>
<s:hidden name="corpID" value="<%= request.getParameter("corpID")%>"/>
<s:hidden name="formStatus" value="" />

<c:set var="corpID" value="<%=request.getParameter("corpID") %>"/>



<s:hidden name="fileExt" />

<c:if test="${not empty newsUpdate.id}">
</c:if>



	
 <div id="Layer3" style="width:100%; margin:0px; padding:0px">
 <div id="newmnav">
		  <ul>
		     <li id="newmnav1" style="background:#FFF"><a class="current"><span>File&nbsp;Upload&nbsp;Detail</span></a></li>
		   
		  </ul>
</div>	

		
        <div class="spn" style="!line-height:7px;">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="detailTabLabel" border="0">
	 	<tbody>  	
		  	<tr>
			<td align="right" class="listwhitetext" width="90">Release Date<font color="red" size="2">*</font></td>
			<c:if test="${not empty newsUpdate.publishedDate}">
				<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="newsUpdate.publishedDate" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="publishedDate" name="newsUpdate.publishedDate" value="%{customerFileMoveDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onblur="" onselect="fillEndDate()"/>
				<img id="publishedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""&nbsp;&nbsp;&nbsp/></td>
			</c:if>
			<c:if test="${empty newsUpdate.publishedDate}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="publishedDate" name="newsUpdate.publishedDate" value="%{customerFileMoveDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onblur="" onselect="fillEndDate();"/>
				<img id="publishedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""/></td>
			</c:if>
	           <td align="right" class="listwhitetext">Display Start Date<font color="red" size="2">*</font></td>
			<c:if test="${not empty newsUpdate.effectiveDate}">
				<s:text id="effectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="newsUpdate.effectiveDate" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="effectiveDate" name="newsUpdate.effectiveDate" value="%{effectiveDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onblur="" onselect="fillEndDate();"/>
				<img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="getId(this.retention)"/></td>
			</c:if>
			<c:if test="${empty newsUpdate.effectiveDate}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="effectiveDate" name="newsUpdate.effectiveDate" required="true" size="7" maxlength="11" readonly="true" onblur="" onkeydown="return onlyDel(event,this)" onselect="fillEndDate();"/>
				<img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="getId(this.retention)"/></td>
			</c:if>
			
			<td class="listwhitetext" align="right" width="50">Retention<font color="red" size="2">*</font><br>(months)&nbsp;&nbsp;&nbsp;</td>
			<td align="left" ><s:select cssClass="list-menu" name="newsUpdate.retention" list="{1,2,3}" onchange="changeStatus();" required="true" cssStyle="width:50px" headerKey="" headerValue=""/> </td>
			<td align="right" class="listwhitetext">Display End Date</td>
			<c:if test="${not empty newsUpdate.endDisplayDate}">
			<s:text id="endDisplayDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="newsUpdate.endDisplayDate"/></s:text>
			<td align="left" style=""><s:textfield cssClass="input-text" id="endDisplayDate" name="newsUpdate.endDisplayDate" value="%{endDisplayDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  onblur="" />
			<img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
			<c:if test="${empty newsUpdate.endDisplayDate}">
			<td align="left" style=""><s:textfield cssClass="input-text" id="endDisplayDate" name="newsUpdate.endDisplayDate" value="%{endDisplayDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  onblur="" />
			<img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
			
			
			
			</tr>
		
			</tbody>
	
	</td>
	</tr>
	</tbody>
	</table>
	
	
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						<tr>
							<td align="right" class="listwhitetext" width="90">File&nbsp;To&nbsp;Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4" width="200px"> <s:file name="file" size="25"/></td>
							<%-- <td align="right" class="listwhitetext strbg" width="130px"><a href="${pageContext.request.contextPath}/images/Test excel sheetfinal.xls" >Sample Template </a></td> --%>
						</tr> 
						<tr><td height="5px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>						
						<td align="right" colspan="2">
						<input type="button" class="cssbutton" name="uploadFile" value="Upload File" cssStyle="width:100px; height:27px" onclick=" return ValidateUpload(); "/>
                                     
                        </td>
                        <td>
                        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:27px" key="Reset"/></td>                       
                        </tr>
                        <tr><td height="50px"></td></tr>  
					</tbody>
				</table> 
	   </div>
	<div class="bottom-header"><span></span></div>
	</div>
	</div>
       
  
      
       
     
</s:form>

<script type="text/javascript">  
 showOrHide(0);
</script>
 <script type="text/javascript">
try{
	<c:if test="${hitFlag=='1'}">
	winLoad();
	</c:if>
	<c:if test="${hitFlag=='0'}">
	alert('${fileNotExists}');
	</c:if>	
}catch(e){}
 </script>
 <script type="text/javascript">
    Form.focusFirstElement($("fileUploadForm"));
</script>
<script type="text/javascript">
var fieldTarget;
function getId(fieldId){
	fieldTarget = fieldId.split('_')[0];
}
function hitOnSelect(){
	if(fieldTarget == 'effectiveDate'){
		fillEndDate();	
	}
}
	setOnSelectBasedMethods(["hitOnSelect()"]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">

   function addfileUploadForm(){
	
   			var url = "editNewsUpdate.html";	 
			document.forms['fileUploadForm'].action= url;	 
 			document.forms['fileUploadForm'].submit();
			return true;
}
   
		   function isNumberKey(evt){
		         var charCode = (evt.which) ? evt.which : event.keyCode
		         if (charCode > 31 && (charCode < 48 || charCode > 57))
		          
		        	 return false;
		             return true;
		      }
	       
</script>

