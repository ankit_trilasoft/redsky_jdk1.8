package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SystemDefaultDao;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.SystemDefaultManager;

public class SystemDefaultManagerImpl extends GenericManagerImpl<SystemDefault, Long> implements SystemDefaultManager {
	SystemDefaultDao systemDefaultDao;

	public SystemDefaultManagerImpl(SystemDefaultDao systemDefaultDao) {
		super(systemDefaultDao);
		this.systemDefaultDao = systemDefaultDao;
	}

	public List<SystemDefault> findByCorpID(String corpId) {
		return systemDefaultDao.findByCorpID(corpId);
	}

	public List findIdByCorpID(String corpId) {
		return systemDefaultDao.findIdByCorpID(corpId);
	}

	public List findInvExtractSeq(String corpID) {
		return systemDefaultDao.findInvExtractSeq(corpID);
	}

	public int updateInvExtractSeq(String invExtractSeq, String corpID) {
		return systemDefaultDao.updateInvExtractSeq(invExtractSeq, corpID);
	}
	public int updateMamutInvExtractSeq(String invExtractSeq, String corpID) {
		return systemDefaultDao.updateMamutInvExtractSeq(invExtractSeq, corpID);
	}
	
	public List findPayExtractSeq(String corpID) {
		return systemDefaultDao.findPayExtractSeq(corpID);
	}

	public List findMamutPayExtractSeq(String corpID) {
		return systemDefaultDao.findMamutPayExtractSeq(corpID);
	}

	public int updatePayExtractSeq(String payExtractSeq, String corpID) {
		return systemDefaultDao.updatePayExtractSeq(payExtractSeq, corpID);
	}

	public int updateMamutPayExtractSeq(String payExtractSeq, String corpID) {
		return systemDefaultDao.updateMamutPayExtractSeq(payExtractSeq, corpID);
	}
	
	public List findPrtExtractSeq(String corpID) {
		return systemDefaultDao.findPrtExtractSeq(corpID);
	}

	public int updatePrtExtractSeq(String prtExtractSeq, String corpID) {
		return systemDefaultDao.updatePrtExtractSeq(prtExtractSeq, corpID);
	}

	public void updateDailyOpsLimit(String dailyOperationalLimit, String dailyOpLimitPC, String minServiceDay, String sessionCorpID) {
		systemDefaultDao.updateDailyOpsLimit(dailyOperationalLimit, dailyOpLimitPC, minServiceDay, sessionCorpID);
	}
	public int updateSAPExtractSeq(String invExtractSeq, String corpID){
		return systemDefaultDao.updateSAPExtractSeq(invExtractSeq,corpID); 
	}
	public List findSAPExtractSeq(String corpID){
		return systemDefaultDao.findSAPExtractSeq(corpID);
	}
	public List findSAPExtractSeqForUGCN(String corpID){
		return systemDefaultDao.findSAPExtractSeqForUGCN(corpID);
	}
	public int updateSAPPrtExtractSeq(String prtExtractSeq, String corpID) {
		return systemDefaultDao.updateSAPPrtExtractSeq(prtExtractSeq, corpID);
	}
	public List findSAPPayExtractSeq(String corpID){
		return systemDefaultDao.findSAPPayExtractSeq(corpID);
	}
	public int updateSAPInvExtractSeq(String invExtractSeq, String corpID){
		return systemDefaultDao.updateSAPInvExtractSeq(invExtractSeq, corpID);
	}
	public int updateInvExtractSeqUTSI(String invExtractSeq, String corpID) {
		return systemDefaultDao.updateInvExtractSeqUTSI(invExtractSeq, corpID);
	}
	public List findInvExtractSeqUTSI(String corpID) {
		return systemDefaultDao.findInvExtractSeqUTSI(corpID);
	}
	public List findPrtExtractSeqUTSI(String corpID) {
		return systemDefaultDao.findPrtExtractSeqUTSI(corpID);
	}
	public int updatePrtExtractSeqUTSI(String prtExtractSeq, String corpID){
		return systemDefaultDao.updatePrtExtractSeqUTSI(prtExtractSeq, corpID);
	}
	public List findPayExtractSeqUTSI(String corpID){
		return systemDefaultDao.findPayExtractSeqUTSI(corpID);
	}
	public int updatePayExtractSeqUTSI(String payExtractSeq, String corpID){
		return systemDefaultDao.updatePayExtractSeqUTSI(payExtractSeq, corpID);
	}
	public int updateSeqNumber(String refType, String seq, String sessionCorpID){
		return systemDefaultDao.updateSeqNumber(refType, seq, sessionCorpID);
	}
	public List getjaournalValidationValue(String sessionCorpID){
		return systemDefaultDao.getjaournalValidationValue(sessionCorpID);
	}
	public int updateJournalValidation(String validationStr,String sessionCorpID){
		return systemDefaultDao.updateJournalValidation( validationStr, sessionCorpID);
	}
	public List findPaymentApplicationExtractSeq(String corpID){
		return systemDefaultDao.findPaymentApplicationExtractSeq(corpID);
	}
	public int updatePaymentApplicationExtractSeq(String invExtractSeq, String corpID){
		return systemDefaultDao.updatePaymentApplicationExtractSeq(invExtractSeq, corpID);
	}
	public void updateSoextractSeq(String sessionCorpID,String soextractSeq){
		systemDefaultDao.updateSoextractSeq(sessionCorpID,soextractSeq);
	}
	public String getCorpIdUnit(String corpID, String WnVol){
		return systemDefaultDao.getCorpIdUnit(corpID, WnVol);
	}
	public String OIServicesFromSystemDefault(String corpID)
	{
		return systemDefaultDao.OIServicesFromSystemDefault(corpID);
	}
}
