<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>


<style type="text/css">
					h2 {background-color: #FBBFFF}
				</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
</script>
<script language="javascript" type="text/javascript">
function myDate() {
	
	if(navigator.appName == 'Microsoft Internet Explorer'){
			document.forms['refMasterForm'].elements['dateFormat'].value="dd/MM/yyyy";
	}
   if(navigator.appName == 'Netscape'){
			document.forms['refMasterForm'].elements['dateFormat'].value="MM/dd/yyyy";
	}
	

	}
</script>
<script language="javascript" type="text/javascript"
	SRC="/scripts/calendar.js">
</script>

<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
   </script>

</head>

<s:form id="refMasterForm" action="saveRefMasterwh" method="post" validate="true">
	<s:hidden name="refMaster.id" value="%{refMaster.id}" />
	<s:hidden id="dateFormat" name="dateFormat" />
	<div id="Layer1">
		<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
	<table class="detailTabLabel" cellspacing="0" cellpadding="0"
		border="0">
		<tbody>
			<tr>
			<td width="0" align="right"><img width="2" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
					<td width="100" class="detailActiveTabLabel content-tab"><a
					href="refMasters.html"><font color="white">Refmaster Detail</font></a></td>
				
				<td width="0" align="left"><img width="2" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
			
			
			
				
				
				<td width="4"></td>



			</tr>
		</tbody>
	</table>

	<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
		<tbody>

						<tr>
							<td class="listwhitetext" align="right">Code</td>
							<td align="left" ><s:textfield
								name="refMaster.code" size="10" maxlength="20"  cssClass="input-text" onfocus="myDate();"
								 /> 
								</td>
							<td class="listwhitetext" align="right">Description</td>
							<td ><s:textfield
								name="refMaster.description" size="50" maxlength="60" 
								   cssClass="input-text" /></td>
							<td align="left" class="listwhite"></td>



						</tr>

						<tr>
						<td class="listwhitetext" align="right">Field Length</td>
							<td ><s:textfield
								name="refMaster.fieldLength" size="7" maxlength="7" cssClass="input-text" onkeydown="return onlyNumsAllowed(event)"
								 /></td>
							
<td class="listwhitetext" align="right">Number</td>
							<td ><s:textfield name="refMaster.number"
								size="7" maxlength="7" cssClass="input-text" onkeydown="return onlyFloatNumsAllowed(event)"/></td>

						</tr>
						<tr>
							<td class="listwhitetext" align="right">Bucket</td>
							<td ><s:textfield name="refMaster.bucket"
								size="7" maxlength="7" cssClass="input-text" /></td>
							<td align="left" class="listwhite"></td>

<td align="left" class="listwhite"></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Bucket2</td>
							<td ><s:textfield name="refMaster.bucket2"
								size="7" maxlength="7" cssClass="input-text" /></td>
						<td align="left" class="listwhite"></td>
						<td align="left" class="listwhite"></td>
						</tr>
					
						<tr>
							<td class="listwhitetext" align="right">Address</td>
							<td align="left" ><s:textfield
								name="refMaster.address1" size="30" maxlength="30" cssClass="input-text" /></td>
								<td class="listwhitetext" align="right">City</td>
							<td ><s:textfield name="refMaster.city"
								size="20" maxlength="20" cssClass="input-text"/></td>
							
						</tr>

						<tr>
							<td class="listwhite"></td>
							<td align="left" ><s:textfield
								name="refMaster.address2" size="30" maxlength="30" cssClass="input-text" />
							</td>
							<td class="listwhitetext" align="right">Zip</td>
							<td ><s:textfield name="refMaster.zip"
								size="11" maxlength="10" cssClass="input-text"/></td>

						</tr>
						
						<tr>
							<td class="listwhitetext" align="right">State</td>
							<td><s:select name="refMaster.state" 
																cssClass="list-menu"	list="%{state}" cssStyle="width:45px" /></td>
							
							<td class="listwhitetext" align="right">Branch</td>
							<td ><s:textfield name="refMaster.branch"
								size="11" maxlength="8"  cssClass="input-text"/></td>
						
							
						</tr>
						<tr>
							
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Allow No More Payroll
							Entry on or before</td>
										<c:if test="${not empty refMaster.stopDate}">
											<s:text id="FormatedInvoiceDate" name="FormDateValue"> <s:param name="value" value="refMaster.stopDate" /> </s:text>
											<td align="left"><s:textfield cssClass="input-text" id="stopDate" name="refMaster.stopDate" value="%{FormatedInvoiceDate}" cssStyle="width:65px" maxlength="11" readonly="true" />
											<img id="calender2" src="/images/calender.png" HEIGHT=20 WIDTH=20 ALIGN=TOP onclick="cal.select(document.forms['refMasterForm'].stopDate,'calender2',document.forms['refMasterForm'].dateFormat.value);  return false;" />
											</td>



										</c:if>
										<c:if test="${empty refMaster.stopDate}">
											<td align="left"><s:textfield cssClass="input-text"
												id="stopDate" name="refMaster.stopDate" required="true"
												cssStyle="width:65px" maxlength="11" readonly="true" /><img
												id="calender2" src="/images/calender.png" HEIGHT=20
												WIDTH=20 ALIGN=TOP
												onclick="cal.select(document.forms['refMasterForm'].stopDate,'calender2',document.forms['refMasterForm'].dateFormat.value);  return false;" />
											</td>
										</c:if>
												
							

						</tr>

	<tr>
		<s:hidden name="refMaster.corpID"/>				
				
						<table>
		<tbody>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.createdOn'/></td>
						<s:hidden name="refMaster.createdOn" />
						<td><s:date name="refMaster.createdOn" format="dd-MMM-yyyy"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.createdBy' /></td>
						<c:if test="${not empty refMaster.id}">
										<s:hidden name="refMaster.createdBy" />
										<td><s:label name="createdBy"
											value="%{refMaster.createdBy}" /></td>
									</c:if>
									<c:if test="${empty refMaster.id}">
										<s:hidden name="refMaster.createdBy"
											value="${pageContext.request.remoteUser}" />
										<td><s:label name="createdBy"
											value="${pageContext.request.remoteUser}" /></td>
									</c:if>
						
						
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.updatedOn'/></td>
						<s:hidden name="refMaster.updatedOn"/>
						<td><s:date name="refMaster.updatedOn" format="dd-MMM-yyyy"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.updatedBy' /></td>
						<s:hidden name="refMaster.updatedBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
				</tbody>
			</table>
	
						</tr>

</tbody>
	</table>

		</div>
						






	<li class="buttonBar bottom"><s:submit cssClass="button"
		method="save" key="button.save" /> <c:if
		test="${not empty refMaster.id}">
		
	</c:if> <s:reset type="button" key="Reset" />
	</li>
	
</table>
</div>
</s:form>

<script type="text/javascript"> 
    Form.focusFirstElement($("refMasterForm")); 
</script>



				