<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %>
  <script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>

</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput(); 
       var valueafterchange;
</script>   
<script type="text/javascript">
function expense(){
	
 
   var estimate=0;
   
    if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt")
      {
      	var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
      	var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
  		 estimate =((quantity*rate)/100);
  		//alert(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value);
  		 document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = estimate;
     }
     else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="flat")
     {
      if(document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value >0 || document.forms['estimateForm'].elements['accountLine.estimateRate'].value >0)
        {
        	alert("please enter the estimated value of flat"); 
        	document.forms['accountLineForms'].elements['accountLine.estimateExpense'].focus();
        }
      }  
    else
    {
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
      	var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
  		 estimate =(quantity*rate);
  		 document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = estimate;
        
    }
     var passThroughPercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
     document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=(passThroughPercentage*estimate)/100;
   }
   
   function revisionExpanse(){
	
 
   var estimate=0;
   
    if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt")
      {
      	var quantity = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
      	var rate = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
  		 revision =((quantity*rate)/100);
  		//alert(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value);
  		 document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = revision;
     }
     else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="flat")
     {
      if(document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value >0 || document.forms['revisionForm'].elements['accountLine.revisionRate'].value >0)
        {
        	alert("please enter the revisiond value of flat"); 
        	document.forms['accountLineForms'].elements['accountLine.revisionExpense'].focus();
        }
      }  
    else
    {
       var quantity = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
      	var rate = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
  		 revision =(quantity*rate);
  		 document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = revision;
        
    }
     var passThroughPercentage = document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
     document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=(passThroughPercentage*revision)/100;
   }
   function forVendoeCode()
   {
      // alert("hi");
      document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=			document.forms['accountLineForms'].elements['accountLine.entitlementVendorCode'].value;
      document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value= document.forms['accountLineForms'].elements['accountLine.entitlementVendorName'].value;
   }

function convertedAmount()
{
  // alert("Raj");
   var x= document.forms['accountLineForms'].elements['accountLine.localAmount'].value ;
   var a= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value ;
   document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=x/a ;
}
</script>	
<script type="text/javascript">


function roundNumber(numberToRound) {

	//alert("Hello from numberToRound")
	var numberField = numberToRound;	//document.roundform.numberfield; // Field where the number appears
	//alert(numberToRound);
	var rnum = numberToRound;// numberField.value;
	var rlength = 2; // The number of decimal places to round to
	if (rnum > 8191 && rnum < 10485) {
		rnum = rnum-5000;
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
		newnumber = newnumber+5000;
	} else {
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	}
	//numberField.value = newnumber;
	//alert("Resultant number"+newnumber)
	return newnumber;
}


</script>


<script language="JavaScript">
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>


<script type="text/javascript">

function magnify_content(targetElement) 
	{
		//alert("In Magnifier");
		//var tempMagnifier=targetElement.value;
		//alert("Hello");
		//alert(targetElement.value);
		document.forms['estimateForm'].elements['magnifier'].value=targetElement.value;
	}

</script>
<script type="text/javascript">

 function checkData()
  {
	 var f = document.getElementById('estimateForm');
   		 f.setAttribute("autocomplete", "off");
  }




</script>
  
<head>   
    <title><fmt:message key="accountLineDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineDetail.heading'/>"/>
 </head>

<s:form id="accountLineForms" name="accountLineForms" action="saveAccountLineEstimate" method="post" validate="true">   
  <%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %> 
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
	
	<div id="newmnav">
  <ul>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  <li><a href="servicePartners.html?id=${serviceOrder.id}"><span>Partner</span></a></li>
  <li id="newmnav1" style="background:#FFF "><a href="accountLineList.html?sid=${serviceOrder.id}" class="current" ><span>Accounting<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
  <li><a href="containers.html?id=${serviceOrder.id}"><span>Container</span></a></li>
  <c:if test="${serviceOrder.job !='INT'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	<c:if test="${serviceOrder.job =='INT'}">
	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	</c:if>
	</sec-auth:authComponent>
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  <li><a href="costings.html?id=${serviceOrder.id}"><span>Invoice</span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
</div><div class="spn">&nbsp;</div><br>

 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" onfocus="calcNetWeight();"  size="15"  cssClass="input-textUpper" readonly="true"/></td><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountry" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountry" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Entitlement</span></a></li>
  <li id="newmnav1" style="background:#FFF "><a href="accountLineEstimateList.html?sid=${serviceOrder.id}" class="current"><span>Estimate<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
  <li><a href="accountLineRevisionList.html?sid=${serviceOrder.id}"><span>Revision</span></a></li>
 <li ><a href="accountLineSummaryList.html?sid=${serviceOrder.id}"><span>Summary</span></a></li>
  
</ul>
</td></tr>
</tbody></table>
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
					
					<tr></tr><tr></tr>
					<tr><font><b>Entitlement Detail</b></font></tr>
                     <tr>
     					<td align="right" class="listwhitetext"><fmt:message key="accountLine.categoryType"/><font color="red" size="2">*</font></td>
  						<td align="left" class="listwhitetext"><s:select list="%{category}" name="accountLine.category"/></td>
     					<td align="right" class="listwhitetext"><fmt:message key="accountLine.entitlementVendorCode"/></td>
  						<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.entitlementVendorCode" size="8" readonly="true" maxlength="10"  /></td>
  						<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrder.id}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.entitlementVendorName&fld_code=accountLine.entitlementVendorCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
     			
  						<td align="right" class="listwhitetext"><fmt:message key="accountLine.entitlementVendorName"/></td>
  						<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.entitlementVendorName" size="15" readonly="true" maxlength="20"  /></td>
  						
  						<td  align="right" class="listwhitetext"><fmt:message key="accountLine.entitlementAmmount"/></td>
						<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.entitlementAmmount" size="8"  maxlength="10" onchange="onlyNumeric(this); isNumeric(this);" /></td>
		 	</tr>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr></tr><tr></tr>
<tr><font><b>Estimate Detail</b></font></tr><tr></tr><tr></tr>

		 	
		  
     		<tr>
     					<td colspan="2" align="right" class="listwhitetext"><fmt:message key="accountLine.vendorCode"/></td>
  						<td  align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.vendorCode" readonly="true" size="8" maxlength="10"  onfocus="forVendoeCode();" /></td>
  						<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrder.id}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
  						<td width="60" align="right" class="listwhitetext"><fmt:message key="accountLine.estimateVendorName"/></td>
  						<td align="right" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.estimateVendorName" readonly="true" size="15" maxlength="20"  onfocus="forVendoeCode();"/></td>
  						<td colspan="2" align="right" class="listwhitetext"><fmt:message key="accountLine.chargeCode"/></td>
					    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.chargeCode" size="8"   maxlength="10" readonly="true" onchange="onlyNumeric(this); isNumeric(this);" /></td>
		 			    <td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=accountLine.glCode&fld_code=accountLine.chargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
		 				<td  align="right" class="listwhitetext"><fmt:message key="accountLine.reference"/></td>
 					    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.reference" size="8"   maxlength="10"  /></td>
					
  			</tr>
 			 
		 	<tr>
 					
		 	
 					<td  align="right" class="listwhitetext"><fmt:message key="accountLine.basis" /></td>
					<td align="left" class="listwhitetext"><s:select list="%{basis}" name="accountLine.basis" onchange="expense();"/></td>
		 			<td align="right" class="listwhitetext"><fmt:message key="accountLine.estimateQuantity"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.estimateQuantity" size="8"   maxlength="10" onchange="onlyNumeric(this);expense();"  /></td>
		 			<td  align="right" class="listwhitetext"><fmt:message key="accountLine.estimateRate"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.estimateRate" size="8"   maxlength="10" onchange="onlyNumeric(this); expense();" /></td>
		 			<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.estimateExpense"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.estimateExpense" size="8"   maxlength="10" onchange="onlyNumeric(this); expense();" /></td>
		 			<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.estimatePassPercentage"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.estimatePassPercentage" size="8"   maxlength="10" onchange="onlyNumeric(this); expense();" /></td>
		 			<td align="right" class="listwhitetext"><fmt:message key="accountLine.estimateRevenueAmount"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.estimateRevenueAmount" size="8"   maxlength="10" onchange="onlyNumeric(this); expense();" /></td>
		 	</tr>
		 </tbody>
		 </table>
		 <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr></tr><tr></tr>
<tr><font><b>Revision Detail</b></font></tr>
<tr>
		 	
		 	
		 	<tr>
 					<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.revisionQuantity"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.revisionQuantity" size="8"   maxlength="10" onchange="onlyNumeric(this); revisionExpanse();" /></td>
		 	
 					<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.revisionRate"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.revisionRate" size="8"   maxlength="10" onchange="onlyNumeric(this); revisionExpanse();" /></td>
		 	
 					<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.revisionExpense"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.revisionExpense" size="8"   maxlength="10" onchange="onlyNumeric(this); revisionExpanse();" /></td>
		 	
 					
 					<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.revisionPassPercentage"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.revisionPassPercentage" size="8"   maxlength="10" onchange="onlyNumeric(this); revisionExpanse();" /></td>
		 	
 					<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.revisionRevenueAmount"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.revisionRevenueAmount" size="8"   maxlength="10" onchange="onlyNumeric(this); revisionExpanse();" /></td>
		 	</tr>
		  </tbody>
     </table>
     <tr>
     <td>
     <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr></tr><tr></tr>
<tr><font><b>Payable Processing</b></font></tr>
<tr>
<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.invoiceNumber"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.invoiceNumber" size="8"   maxlength="10" onchange="onlyNumeric(this);"  /></td>

<td align="right" class="listwhitetext"><fmt:message key="accountLine.invoiceDate"/></td>

		<c:if test="${not empty accountLine.invoiceDate}">
			<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.invoiceDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" value="%{accountLineFormattedValue}" size="7" maxlength="11" readonly="true" />
			<img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].invoiceDate,'calender',document.forms['accountLineForms'].dateFormat.value); return false;" /></a></td>
			</c:if>
			<c:if test="${empty accountLine.invoiceDate}">
			<td><s:textfield cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" required="true" size="7" maxlength="11" readonly="true"  />
			<img id="calender" align="top"src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].invoiceDate,'calender',document.forms['accountLineForms'].dateFormat.value); return false;" /></a></td>
			</c:if>

<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.exchangeRate"/></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.exchangeRate" size="8"   maxlength="10" onkeydown="return onlyfloatAllowed(event)" onchange="convertedAmount();" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="accountLine.receivedDate"/></td>

		<c:if test="${not empty accountLine.receivedDate}">
			<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.receivedDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" value="%{accountLineFormattedValue}" size="7" maxlength="11" readonly="true" />
			<img id="calender2" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].receivedDate,'calender2',document.forms['accountLineForms'].dateFormat.value); return false;" /></a></td>
			</c:if>
			<c:if test="${empty accountLine.receivedDate}">
			<td><s:textfield cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" required="true" size="7" maxlength="11" readonly="true"  />
			<img id="calender2" align="top"src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].receivedDate,'calender2',document.forms['accountLineForms'].dateFormat.value); return false;" /></a></td>
			</c:if>

<td align="right" class="listwhitetext"><fmt:message key="accountLine.valueDate"/></td>

		<c:if test="${not empty accountLine.valueDate}">
			<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.valueDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="valueDate" name="accountLine.valueDate" value="%{accountLineFormattedValue}" size="7" maxlength="11" readonly="true" />
			<img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].valueDate,'calender3',document.forms['accountLineForms'].dateFormat.value); return false;" /></a></td>
			</c:if>
			<c:if test="${empty accountLine.valueDate}">
			<td><s:textfield cssClass="input-text" id="valueDate" name="accountLine.valueDate" required="true" size="7" maxlength="11" readonly="true"  />
			<img id="calender3" align="top"src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].valueDate,'calender3',document.forms['accountLineForms'].dateFormat.value); return false;" /></a></td>
			</c:if>
		<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.country"/></td>
		<td align="left" class="listwhitetext"><s:select cssClass="input-text" key="accountLine.country" list="%{country}"/></td>
		<td width="220px" align="right" class="listwhitetext"><fmt:message key="accountLine.note"/></td>
		<td align="left" class="listwhitetext" width="40px"><img class="openpopup" src="${pageContext.request.contextPath}/images/postit.gif" HEIGHT=20 WIDTH=20 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=AccountLine&subType=Payable&decorator=popup&popup=true',460,320);"/></td>
</tr>
<tr>
    
    
    <td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.localAmount"/></td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.localAmount" size="8"   maxlength="10" onkeydown="return onlyfloatAllowed(event)" onchange="convertedAmount();" /></td>

 <td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.convertedAmount"/></td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.actualExpense" size="8"   maxlength="10" onkeydown="return onlyfloatAllowed(event)" /></td>

 <td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.status"/></td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.status" size="8"   maxlength="10" onchange="onlyNumeric(this);"  /></td>

<td width="100px" align="right" class="listwhitetext"><fmt:message key="accountLine.glCode"/></td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.glCode" size="8"   maxlength="10" onchange="onlyNumeric(this);"  /></td>

</tr>
		<tr>
<td  align="right" class="listwhitetext"><fmt:message key="accountLine.note"/></td>
<td colspan="4" align="left" class="listwhitetext"><s:textarea name="accountLine.note"  rows="1" cols="50" /></td>


</tr>
			
			
			
</tr>
     </table>
     </tbody>
     </table>
		    <s:submit cssClass="cssbuttonA" method="estimateSave" key="button.save" cssStyle="width:55px; height:25px" />   
            <input type="button" class="cssbutton1" value="Add" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/accountLineEstimateList.html?sid=${ServiceOrderID}"/>'" />   
     
     <s:hidden name="accountLine.corpID"/>
     
     <s:hidden name="accountLine.sequenceNumber"/>
     <s:hidden name="accountLine.shipNumber"/>
     <s:hidden name="accountLine.createdBy"/>
     <s:hidden name="accountLine.updatedBy"/>
    <s:hidden name="accountLine.unit"/>
    
	<s:hidden name="secondDescription"/>
	<s:hidden name="fourthDescription"/>
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription"/>
     
     
           
         
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</s:form>	
	