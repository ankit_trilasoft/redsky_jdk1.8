package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.AgentBaseSCACDao;
import com.trilasoft.app.model.AgentBaseSCAC;
import com.trilasoft.app.service.AgentBaseSCACManager;

public class AgentBaseSCACManagerImpl extends GenericManagerImpl<AgentBaseSCAC, Long> implements AgentBaseSCACManager {
	AgentBaseSCACDao agentBaseSCACDao;

	public AgentBaseSCACManagerImpl(AgentBaseSCACDao agentBaseSCACDao) {
		super(agentBaseSCACDao);
		this.agentBaseSCACDao = agentBaseSCACDao;
	}

	public List getbaseSCACList(String partnerCode, String baseCode, String sessionCorpID) {
		return agentBaseSCACDao.getbaseSCACList(partnerCode, baseCode, sessionCorpID);
	}

	public List getSCACList(String sessionCorpID) {
		return agentBaseSCACDao.getSCACList(sessionCorpID);
	}

	public List isExisted(String baseCode, String partnerCode, String code, String sessionCorpID) {
		return agentBaseSCACDao.isExisted(baseCode, partnerCode, code, sessionCorpID);
	}
}
