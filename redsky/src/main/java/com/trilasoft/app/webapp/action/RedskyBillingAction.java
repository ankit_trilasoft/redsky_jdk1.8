package com.trilasoft.app.webapp.action;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable; 
import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.trilasoft.app.dao.hibernate.RedskyBillingDaoHibernate.DTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.model.RedskyBilling;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.RedskyBillingManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.RefMasterManager;

public class RedskyBillingAction extends BaseAction implements Preparable {

	private Long id;
	private String sessionCorpID;
	private  RedskyBilling redskyBilling;
	private  RedskyBillingManager redskyBillingManager;
	private  CompanyManager companyManager;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;

	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;

	private String corpIDParam;
	private List corpIDList; 
	private String corpID;
	private int year;
    private int month;
    private List toalCountOfSO;
    private String totalOfSo;
    private String companyName;
    private List companyfullName;
    private int totalCount;
    private List billingDetailList;
    private List billingList; 
    private static Map<String,String>rskyBillGroup;
	private RefMasterManager refMasterManager;
	private String redSkyBillingName;
	private Company company;
	private  List monthList = new ArrayList<String>();
	private int proc;
	private Date beginDate;
	Date currentdate = new Date();
	
	public RedskyBillingAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	static final Logger logger = Logger.getLogger(RedskyBilling.class);
    public void prepare() throws Exception { 
    	 
    	try {
    	Calendar cal = Calendar.getInstance();
        int currentMonth = Integer.parseInt((new SimpleDateFormat("MM").format(cal.getTime())));
        currentMonth =currentMonth-2; 
    	String[] months = new DateFormatSymbols().getShortMonths(); 
        for (int i = currentMonth; i < months.length; i++) {
            String month = months[i]; 
            if(month!=null && (!(month.trim().equals("")))) {
             monthList.add(month);
            }
        }
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	
	}
    public String getComboList(String sessionCorpID){
   	 rskyBillGroup = refMasterManager.findByParameter(sessionCorpID, "RSKYBillGroup");
   	 return SUCCESS;
    }
    public String  editRedskyBilling(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	 getComboList( sessionCorpID);
    	corpIDParam=sessionCorpID; 
    	companyfullName = companyManager.getcompanyName(sessionCorpID);
    	if(companyfullName!=null && !companyfullName.isEmpty() && companyfullName.get(0)!=null)
		{
			companyName=companyfullName.get(0).toString(); 
		}
		corpIDList =companyManager.getcompanyName(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    public String fingRedskyBillingList()
	{
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List redskyBillingList =redskyBillingManager.putRedskyBillingList(redSkyBillingName);
   
    	String user = getRequest().getRemoteUser();
    	Iterator it = redskyBillingList.iterator();
    	BigDecimal bd = null;
    	int cont=0;
		  while(it.hasNext()){
			  try{
				String serviceOrderId=it.next().toString();
			    Long  idSo =Long.parseLong(serviceOrderId);
				serviceOrder=serviceOrderManager.getForOtherCorpid(idSo);
				trackingStatus=trackingStatusManager.getForOtherCorpid(idSo);
			    String tempCorpId =	serviceOrder.getCorpID();
			    String	defaultBillingRate =companyManager.defaultBillingRate(tempCorpId);
                     if(!defaultBillingRate.equalsIgnoreCase("")){
				     bd = new BigDecimal(defaultBillingRate);
                     }
				String ids=redskyBillingManager.checkByShipNumber(serviceOrder.getCorpID(),serviceOrder.getShipNumber());
				
				if ((ids!=null)&&(!ids.equalsIgnoreCase(""))){				
					redskyBilling = redskyBillingManager.getForOtherCorpid(Long.parseLong(ids));
					redskyBilling.setCorpID(serviceOrder.getCorpID());
					redskyBilling.setCompanyDivision(serviceOrder.getCompanyDivision());
					redskyBilling.setShipNumber(serviceOrder.getShipNumber());
					redskyBilling.setLastName(serviceOrder.getLastName());
					redskyBilling.setFirstName(serviceOrder.getFirstName());
					redskyBilling.setJob(serviceOrder.getJob());
					redskyBilling.setRouting(serviceOrder.getRouting());
					redskyBilling.setBillToName(serviceOrder.getBillToName());
					redskyBilling.setRedskyBillDate(serviceOrder.getRedskyBillDate());
					redskyBilling.setDeliveryA(trackingStatus.getDeliveryA());
					redskyBilling.setPackingA(trackingStatus.getPackA());
					redskyBilling.setLoadA(trackingStatus.getLoadA());
					redskyBilling.setCreatedBy(user);
					redskyBilling.setUpdatedBy(user);
					redskyBilling.setCreatedOn(new Date());
					redskyBilling.setUpdatedOn(new Date());
					redskyBillingManager.save(redskyBilling);
				}else{
					redskyBilling = new RedskyBilling();	
					redskyBilling.setCorpID(serviceOrder.getCorpID());
					redskyBilling.setCompanyDivision(serviceOrder.getCompanyDivision());
					redskyBilling.setShipNumber(serviceOrder.getShipNumber());
					redskyBilling.setLastName(serviceOrder.getLastName());
					redskyBilling.setFirstName(serviceOrder.getFirstName());
					redskyBilling.setJob(serviceOrder.getJob());
					redskyBilling.setRouting(serviceOrder.getRouting());
					redskyBilling.setBillToName(serviceOrder.getBillToName());
					redskyBilling.setRedskyBillDate(serviceOrder.getRedskyBillDate());
					redskyBilling.setDeliveryA(trackingStatus.getDeliveryA());
					redskyBilling.setPackingA(trackingStatus.getPackA());
					redskyBilling.setLoadA(trackingStatus.getLoadA());
					redskyBilling.setCreatedBy(user);
					redskyBilling.setUpdatedBy(user);
					redskyBilling.setCreatedOn(new Date());
					redskyBilling.setUpdatedOn(new Date());
					redskyBilling.setBillingRate(bd);
					redskyBillingManager.save(redskyBilling);
					cont++;
				}
			  }catch(Exception e){
				  e.printStackTrace();
			  }
		  }
		  corpIDParam=sessionCorpID; 
		  corpIDList =companyManager.getcompanyName(sessionCorpID);
		  String key ="";
		  if(redskyBillingList.size()>0){
		      if(redskyBillingList.size()>1){
		    	  if((redskyBillingList.size()-cont)>0){
		    		  key = (redskyBillingList.size()-cont)+" lines have been updated in redskybilling table.<BR>"+cont+" lines have been inserted in redskybilling table.";
		    	  }else{
		    		  key = redskyBillingList.size()+" lines have been inserted in redskybilling table.";
		    	  }
		      }else{ 
			    	  if((redskyBillingList.size()-cont)>0){
			    		  key = (redskyBillingList.size()-cont)+" lines have been updated in redskybilling table.<BR>"+cont+" lines have been inserted in redskybilling table.";
			    	  }else{
			    		  key = redskyBillingList.size()+" lines have been inserted in redskybilling table.";
			    	  }		    	   
		      }
		      saveMessage(getText(key)); 
		  }else{
			  key = "No data to insert in redskybilling table.";
			  saveMessage(getText(key)); 
		  }
		 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
	}
    public String redskyBillingList(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	if(!sessionCorpID.equalsIgnoreCase("TSFT")){
			corpID=sessionCorpID;
		}else{
			corpID=companyManager.getcorpIDName(corpID);
		}
		toalCountOfSO=redskyBillingManager.gettotalCountOfSo(corpID,year);
		
		companyfullName = companyManager.getcompanyName(corpID);
		
			if(companyfullName!=null && !companyfullName.isEmpty() && companyfullName.get(0)!=null)
			{
				companyName=companyfullName.get(0).toString(); 
			}
			if(toalCountOfSO!=null && !toalCountOfSO.isEmpty() && toalCountOfSO.get(0)!=null)
			{
				totalOfSo=toalCountOfSO.get(0).toString();				
			}
		billingDetailList=redskyBillingManager.getRedskyBillingList(corpID,year);
			if(!billingDetailList.isEmpty())
			{
				
			} 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;	
    }
    
    public String redskyBillingListDetails( )
	{
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		billingList=redskyBillingManager.getRedskyBillingListDetails(corpID,month,year);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
    public String runRedskyBilling( )
	{
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	   getComboList( sessionCorpID);
           corpIDParam=sessionCorpID; 
 		 corpIDList =companyManager.getcorpID();
 		 if(proc==1){
		  int s=redskyBillingManager.getDetailsByProcedure(proc,beginDate);
		    String key = "Procedure for UGJP has been executed successfully.";
	   	   saveMessage(getText(key));
	   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
 		 }else if(proc==2){
 			 int s=redskyBillingManager.getDetailsByProcedure(proc,beginDate);
 			 String key = "Procedure for UGWW has been executed successfully.";
 		   	   saveMessage(getText(key));
 		   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 			return SUCCESS;
 		 }else if(proc==3){
 			 int s=redskyBillingManager.getDetailsByProcedure(proc,beginDate);
 			 String key = "Procedure for RedSky has been executed successfully. ";
 		   	   saveMessage(getText(key));
 		   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 			return SUCCESS;
 		 }else if(proc==4){
 			 int s=redskyBillingManager.getDetailsByProcedure(proc,beginDate);
 			 String key = "Procedure for Load Delivery date has been executed successfully.";
 		   	   saveMessage(getText(key));
 		   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 			return SUCCESS;
 		 }
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
     
    public String redskyBillingListExtract(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	
		if(!sessionCorpID.equalsIgnoreCase("TSFT")){
			corpID=sessionCorpID;
		}
    	String fileHeader = new String();
    	billingList=redskyBillingManager.getRedskyBillingListDetails(corpID,month,year);
    try{
    	if(!(billingList.isEmpty())){
    		File deliveryFile = new File("BillingExtract");
    		HttpServletResponse response = getResponse();
    		ServletOutputStream out = response.getOutputStream();
    		response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-Disposition", "attachment; filename="+deliveryFile.getName()+".xls");
    		response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			fileHeader=" CORPID"+"\t" + " Divn" + "\t" +" RS Bill Date" + "\t"+" Job" + "\t"+" Routing"+ "\t"+" SO#"+ "\t"+" Shipper"+ "\t"+" Biller" + "\t"+" Packing Date"+ "\t"+" Load Date"+ 
			"\t"+" Delivery Date" +"\t"+" Invoice Number" +"\t"+" Invoice Date" + "\n";
    	    out.write(fileHeader.getBytes());
    	    
    	    Iterator it=billingList.iterator();
    	    while(it.hasNext()){
    	    	Object extracts = (Object) it.next();
    	    	out.write(("" + ((DTO) extracts).getCorpID().toString() + "\t").getBytes());   	    	
    	    	out.write(("" + ((DTO) extracts).getCompanyDivision().toString() + "\t").getBytes());    	    	
    	    	out.write(("" + ((DTO) extracts).getRedskyBillDate().toString() + "\t").getBytes());    	    	
    	    	out.write(("" + ((DTO) extracts).getJob().toString() + "\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getRouting().toString() +"\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getShipNumber().toString() + "\t").getBytes());   	    
    	    	out.write(("" + ((DTO) extracts).getShipper().toString() + "\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getBilltoName().toString() + "\t").getBytes());    		
    	    	out.write(("" + ((DTO) extracts).getPackingA().toString() + "\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getLoadA().toString() + "\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getDeliveryA().toString() + "\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getInvoiceNumber().toString() + "\t").getBytes());
    	    	out.write(("" + ((DTO) extracts).getInvoiceDate().toString() + "\n").getBytes());
    	    }
    	    out.flush();
	    	out.close();
    	}else{
    		File deliveryFile = new File("BillingExtract");
    		HttpServletResponse response = getResponse();
    		ServletOutputStream out = ServletActionContext.getResponse().getOutputStream();
    		response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-Disposition", "attachment; filename="+deliveryFile.getName()+".xls");
    		response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			fileHeader = "NO Record Found , thanks";
    	    out.write(fileHeader.getBytes());
    	}
    }catch(Exception e){
    	e.printStackTrace();
    }
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RedskyBilling getRedskyBilling() {
		return redskyBilling;
	}

	public void setRedskyBilling(RedskyBilling redskyBilling) {
		this.redskyBilling = redskyBilling;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setRedskyBillingManager(RedskyBillingManager redskyBillingManager) {
		this.redskyBillingManager = redskyBillingManager;
	}
	public List getCorpIDList() {
		return corpIDList;
	}
	public void setCorpIDList(List corpIDList) {
		this.corpIDList = corpIDList;
	}
	public String getCorpIDParam() {
		return corpIDParam;
	}
	public void setCorpIDParam(String corpIDParam) {
		this.corpIDParam = corpIDParam;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List getBillingDetailList() {
		return billingDetailList;
	}
	public void setBillingDetailList(List billingDetailList) {
		this.billingDetailList = billingDetailList;
	}
	public List getCompanyfullName() {
		return companyfullName;
	}
	public void setCompanyfullName(List companyfullName) {
		this.companyfullName = companyfullName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public List getToalCountOfSO() {
		return toalCountOfSO;
	}
	public void setToalCountOfSO(List toalCountOfSO) {
		this.toalCountOfSO = toalCountOfSO;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getTotalOfSo() {
		return totalOfSo;
	}
	public void setTotalOfSo(String totalOfSo) {
		this.totalOfSo = totalOfSo;
	}
	public CompanyManager getCompanyManager() {
		return companyManager;
	}
	public List getBillingList() {
		return billingList;
	}
	public void setBillingList(List billingList) {
		this.billingList = billingList;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public static Map<String, String> getRskyBillGroup() {
		return rskyBillGroup;
	}
	public static void setRskyBillGroup(Map<String, String> rskyBillGroup) {
		RedskyBillingAction.rskyBillGroup = rskyBillGroup;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public String getRedSkyBillingName() {
		return redSkyBillingName;
	}
	public void setRedSkyBillingName(String redSkyBillingName) {
		this.redSkyBillingName = redSkyBillingName;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public int getProc() {
		return proc;
	}
	public void setProc(int proc) {
		this.proc = proc;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public List getMonthList() {
		return monthList;
	}
	public void setMonthList(List monthList) {
		this.monthList = monthList;
	}
	
	

	
	
	
	

}
