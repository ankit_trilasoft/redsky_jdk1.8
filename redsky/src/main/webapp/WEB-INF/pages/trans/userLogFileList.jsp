<%@ include file="/common/taglibs.jsp"%>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>
div.error, span.error, li.error, div.message {
width:450px;
}

 span.pagelinks { 
display:block;
font-size:0.85em;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-15px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}


form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
#inner-content{margin-top:33px !important;}
#successMessages{margin-bottom:30px !important;}
</style>

<head> 
<script language="JavaScript" type="text/javascript" SRC="/scripts/masks.js"></script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
 <script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['userlogfileList'].elements['userlogfile.userName'].value = "";
			document.forms['userlogfileList'].elements['userlogfile.userType'].value = "";
			document.forms['userlogfileList'].elements['userlogfile.login'].value = "";
			document.forms['userlogfileList'].elements['activeIps'].checked = false;
			
}

function findUserPermission(userName,position) { 
  var url="findUserPermission.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&userNamePermission=" + encodeURI(userName);
  ajax_showTooltip(url,position);	
  }
  function findLogTime(userName,position) {
  var loginDate=document.forms['userlogfileList'].elements['userlogfile.login'].value ; 
  var url="findLogTime.html?ajax=1&decorator=simple&popup=true&userNamePermission=" + encodeURI(userName)+"&loginDate="+encodeURI(loginDate);
  ajax_showTooltip(url,position);	
  }
 
 function userLogFileListDetail(){ 
 document.forms['userlogfileList'].action ='searchUserLogFilesDetail.html';
 document.forms['userlogfileList'].submit(); 
 }
</script> 
    <title><fmt:message key="userlogfileList.title"/></title>   
    <meta name="heading" content="<fmt:message key='userlogfileList.heading'/>"/>   
</head>  
 <body>  
<s:form id="userlogfileList" name="userlogfileList" action="searchUserLogFiles" method="post">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<div id="Layer1" style="width: 100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">		
<table class="table" border="1" style="width:100%">
<thead>
<tr>
<th>User Name</th>
<th>User Type</th>
<th>Login Date</th>

</tr></thead> 
<tbody>
  <tr>
  <td>
       <s:textfield name="userlogfile.userName" required="true" cssClass="input-text" size="18" />
   </td>
   <td>
   <s:select name="userlogfile.userType" list="%{usertype}"  cssStyle="width:100px" cssClass="list-menu" headerKey="" headerValue="All" />
      <%-- <s:textfield name="userlogfile.userType" required="true" cssClass="input-text" size="18"/> --%>
   </td>
   <td>
<c:if test="${not empty userlogfile.login}"> 
       <s:text id="customerFiledate1FormattedValue" name="${FormDateValue}" ><s:param name="value" value="userlogfile.login" /></s:text>
       <s:textfield name="userlogfile.login" id="logindate" required="true" value="%{customerFiledate1FormattedValue}" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7" readonly="true" /> 
       <img id="logindate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
 </c:if>
 <c:if test="${empty userlogfile.login}"> 
        <s:textfield name="userlogfile.login" id="logindate" required="true" cssClass="input-text" size="7" readonly="true" onkeydown="return onlyDel(event,this)"/> 
       <img id="logindate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
   </c:if>
   </td> 
   </tr>
   <tr>
   <td ></td>
			<td align="center" style="border-left: hidden;" class="listwhitetext">Exclude IPs<s:checkbox key="activeIps"  /></td>
   <td style="border-left: hidden;">
       <s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;" key="button.search"/>  
       <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
       
   </td> 
  </tr>
  </tbody>
 </table>
 </div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>	
</div>	
</s:form> 
<table cellspacing="0" cellpadding="0" border="0" width="100%">
   	<tbody>
	<tr><td height="10px"></td></tr>
	<tr><td>
	<s:set name="userlogfileList" value="userlogFile" scope="request"/>
	<div id="newmnav" >
		  <ul style="width:500px"> 
		    <li id="newmnav1" style="background:#FFF;margin-top:5px; "><a class="current"><span>Log Summary<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		    <li style="margin-top:5px; "><a onclick="userLogFileListDetail();"><span>Log Detail</span></a></li>
		  </ul>
		</div>
		<div class="spn" style="!line-height:0.8em;">&nbsp;</div>
<fmt:setTimeZone value="${sessionTimeZone}" scope="session"/>
<c:set var="displayDateFormat" value="dd-MMM-yy" scope="session"/>
<c:set var="displayDateTimeFormat" value="dd-MMM-yyyy HH:mm" scope="session"/>
<c:set var="displayDateTimeEditFormat" value="dd-MMM-yyyy HH:mm" scope="session"/>
<c:set var="corpTimeZone" value="${sessionTimeZone}" scope="session"/> 
   <display:table name="userlogfileList" class="table" requestURI="" id="userlogfileList"  defaultsort="2" defaultorder="descending" export="true" pagesize="10">   
    <%-- <display:column property="userName" sortable="true" titleKey="userlogfileList.userName"/>--%>
    <display:column sortable="true" sortProperty="userName" titleKey="userlogfileList.userName"  ><a href="#" title="User Permission List " onclick="findUserPermission('${userlogfileList.userName}',this);"/><c:out value="${userlogfileList.userName}" /></a>
   </display:column>
    <display:column  sortable="true" sortProperty="lastLogin" title="Last&nbsp;Login&nbsp;Time">
    <span style="text-align:right;float:right;"><img  align="top" class="openpopup"  height="16" width="16" src="${pageContext.request.contextPath}/images/time-login.png" title="Login Logout Time" onclick="findLogTime('${userlogfileList.userName}',this);"/>
    </span>
    <fmt:formatDate value="${userlogfileList.lastLogin}" pattern="${displayDateTimeFormat}"/> 
     </display:column>
    <display:column property="lastname" sortable="true" title="Agent Name"/>
    <display:column property="terminalCountry" sortable="true" title="Terminal Country"/>
    <display:column property="userType" sortable="true" titleKey="userlogfileList.userType"/>
    <display:column property="corpID" sortable="true" titleKey="userlogfileList.corpID"/>
   <%-- <display:column property="IPaddress" sortable="true" titleKey="userlogfileList.IPaddress"/>--%>
    <display:column title="DNS" >
    <FORM ACTION="https://mxtoolbox.com/SuperTool.aspx?action=ptr%3a${userlogfileList.IPaddress}&run=toolpage" METHOD=post target="_blank" style="margin-top:1px; ">
  	<%-- <INPUT TYPE="hidden"   NAME=ip VALUE="${userlogfileList.IPaddress}" SIZE=15 maxlength=15/> --%>
    <INPUT  TYPE=submit NAME=Submit VALUE="Check DNS: ${userlogfileList.IPaddress}" style="background:transparent url('images/check-dns-small.png') no-repeat; border:0px; margin:0px; padding:0px 0px 5px 5px;height:28px; width:191px;text-align:left;"/>
	</FORM>
	</display:column>
  <%--  <display:column   titleKey="userlogfileList.Login" sortable="true" sortProperty="login" >
    <fmt:formatDate value="${userlogfileList.login}" pattern="${displayDateTimeFormat}"/> 
    </display:column>
    <display:column   titleKey="userlogfileList.Logout"  sortable="true" sortProperty="logout" >
    <fmt:formatDate value="${userlogfileList.logout}" pattern="${displayDateTimeFormat}"/> 
    </display:column> --%>
    <display:setProperty name="export.excel.filename" value="Company List.xls"/>   
    <display:setProperty name="export.csv.filename" value="company List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="company List.pdf"/>   
</display:table>   
  </td></tr></tbody></table>
 
 <script type="text/javascript">  
try{
if('${activeIps}'=='true')
   	{
   		document.forms['userlogfileList'].elements['activeIps'].checked = true;
   	}
   	}
   	catch(e){}
   try{
   if('${activeIps}'=='false')
   	{
   		document.forms['userlogfileList'].elements['activeIps'].checked = false;
   	}
   	}
   	catch(e){}

</script> 
 
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script> 
</body>

