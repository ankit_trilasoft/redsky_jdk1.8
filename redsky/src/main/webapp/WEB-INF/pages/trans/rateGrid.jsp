<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="<fmt:message key='rateGrid.heading'/>"/> 

<title><fmt:message key="rateGrid.title"/></title> 
</head>
<style type="text/css">

</style>
<script language="javascript" type="text/javascript">

function winLoad(){
if(document.forms['rateGrid'].elements['btnType'].value=='updateParent'){
parent.window.opener.document.location.reload(); 
}
}


function traceQuantity2Change(szDivID,targetElement)
{
if(targetElement.value=='')
{
targetElement.value='0.0';
}
var dfd = document.forms['rateGrid'].elements['onSiteQuantity2Check'].value;
if(dfd == ''){
	document.forms['rateGrid'].elements['onSiteQuantity2Check'].value = szDivID+'#'+targetElement.value;
}else{
if(dfd.indexOf(szDivID+'#') >-1){
	var splitDFD = dfd.split(',');
	var dfd = '';
	for(i = 0; i<splitDFD.length ; i++){
		if(splitDFD[i].indexOf(szDivID+'#') > -1){
			splitDFD[i] = szDivID+'#'+targetElement.value;
		}
		if(dfd == ''){
			dfd = splitDFD[i]
		}else{
			dfd = dfd+','+splitDFD[i];
		}
	}
	
}
	else{

 dfd=document.forms['rateGrid'].elements['onSiteQuantity2Check'].value = dfd + ',' + szDivID+'#'+targetElement.value;
}
 document.forms['rateGrid'].elements['onSiteQuantity2Check'].value = dfd.replace( ',,' , ',' );
}

}

function traceChange(szDivID,targetElement)
{
if(targetElement.value=='')
{
targetElement.value='0.0';
}
var dfd = document.forms['rateGrid'].elements['onSiteStatusCheck'].value;
if(dfd == ''){
	document.forms['rateGrid'].elements['onSiteStatusCheck'].value = szDivID+'#'+targetElement.value;
}else{
if(dfd.indexOf(szDivID+'#') >-1){
	var splitDFD = dfd.split(',');
	var dfd = '';
	for(i = 0; i<splitDFD.length ; i++){
		if(splitDFD[i].indexOf(szDivID+'#') > -1){
			splitDFD[i] = szDivID+'#'+targetElement.value;
		}
		if(dfd == ''){
			dfd = splitDFD[i]
		}else{
			dfd = dfd+','+splitDFD[i];
		}
	}
	
}
	else{

 dfd=document.forms['rateGrid'].elements['onSiteStatusCheck'].value = dfd + ',' + szDivID+'#'+targetElement.value;
}
 document.forms['rateGrid'].elements['onSiteStatusCheck'].value = dfd.replace( ',,' , ',' );
}

}

function traceRateChange(szDivID,targetElement)
{
if(targetElement.value=='')
{
targetElement.value='0';
}	
var dfd = document.forms['rateGrid'].elements['onSiteRateCheck'].value;
if(dfd == ''){
	document.forms['rateGrid'].elements['onSiteRateCheck'].value = szDivID+'#'+targetElement.value;
}else{
if(dfd.indexOf(szDivID+'#') >-1){
	var splitDFD = dfd.split(',');
	var dfd = '';
	for(i = 0; i<splitDFD.length ; i++){
		if(splitDFD[i].indexOf(szDivID+'#') > -1){
			splitDFD[i] = szDivID+'#'+targetElement.value;
		}
		if(dfd == ''){
			dfd = splitDFD[i]
		}else{
			dfd = dfd+','+splitDFD[i];
		}
	}
	
}
	else{

 dfd=document.forms['rateGrid'].elements['onSiteRateCheck'].value = dfd + ',' + szDivID+'#'+targetElement.value;
}
 document.forms['rateGrid'].elements['onSiteRateCheck'].value = dfd.replace( ',,' , ',' );
}

}

function traceBuyRateChange(szDivID,targetElement)
{
if(targetElement.value=='')
{
targetElement.value='0';
}	
var dfd = document.forms['rateGrid'].elements['onSiteBuyRateCheck'].value;
if(dfd == ''){
	document.forms['rateGrid'].elements['onSiteBuyRateCheck'].value = szDivID+'#'+targetElement.value;
}else{
if(dfd.indexOf(szDivID+'#') >-1){
	var splitDFD = dfd.split(',');
	var dfd = '';
	for(i = 0; i<splitDFD.length ; i++){
		if(splitDFD[i].indexOf(szDivID+'#') > -1){
			splitDFD[i] = szDivID+'#'+targetElement.value;
		}
		if(dfd == ''){
			dfd = splitDFD[i]
		}else{
			dfd = dfd+','+splitDFD[i];
		}
	}
	
}
	else{

 dfd=document.forms['rateGrid'].elements['onSiteBuyRateCheck'].value = dfd + ',' + szDivID+'#'+targetElement.value;
}
 document.forms['rateGrid'].elements['onSiteBuyRateCheck'].value = dfd.replace( ',,' , ',' );
}

}

function submitAction(){
	var tempPageNumber= <%= request.getParameter("d-2633051-p") %> ;
	if(tempPageNumber=='' || tempPageNumber=='0' || tempPageNumber=='null'){
		tempPageNumber='1';
		}
		var url = "saverateGrid.html?btnType=updateParent&decorator=popup&popup=true&d-2633051-p="+tempPageNumber;
		document.forms['rateGrid'].action = url;
	   document.forms['rateGrid'].submit();
		
	}
function addAction(){
		var url = "addRowToGrid.html?btnType=addrow&decorator=popup&popup=true&d-2633051-p=${rateListSize}";
		document.forms['rateGrid'].action = url;
		document.forms['rateGrid'].submit();
		
	}
function uploadData(){
     var twoDGridUnit= document.forms['rateGrid'].elements['twoDGridUnit'].value 
	 location.href ='uploadXlsFileRateGrid.html?charge=${charges.id}&contractName=${charges.contract}&twoDGridUnit='+twoDGridUnit+'&buttonType=upload&decorator=popup&popup=true','width=600,height=550,scrollbars=yes';
 }
function uploadAndReplace(){
    var twoDGridUnit= document.forms['rateGrid'].elements['twoDGridUnit'].value
    location.href ='uploadReplaceFileRateGrid.html?charge=${charges.id}&contractName=${charges.contract}&twoDGridUnit='+twoDGridUnit+'&buttonType=uploadReplace&decorator=popup&popup=true','width=600,height=550,scrollbars=yes';
 }
function exportRates(){
	location.href='exportRateGrid.html?chid=${charges.id}&contractName=${charges.contract}';
}

	function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110); 
	}
	var count=0;
	function numbersonly(myfield, e)
	{
		var key;
		var keychar;

		var val = myfield.value;
				if(val.indexOf(".")<0)
				{
				count=0;
				}
	    
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
	
			keychar = String.fromCharCode(key);
	
		if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
		   return true;
		else if ((("0123456789").indexOf(keychar) > -1))
		   return true;
		else if ((count<1)&&(keychar == "."))
		{
			
		   count++;
		   return true;
		   
		}
		else if ((count>0)&&(keychar == "."))
		{
		   return false;
		}
		
		else
		   return false;
	}	
function dotNotAllowed(target,fieldName){
		var dotValue=target.value;
		var id=target.id;
		if(dotValue=='.' && dotValue.length==1 ){
			alert("Please enter valid value.");
			document.getElementById(id).value='';
			document.getElementById(id).select();
			return false;
		    }
	     }
 function isFloat(targetElement)
	{   
	var i;
	var s = targetElement.value;
	var id = targetElement.id;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        if (c == ".") {
        
        }else{
        alert("Only numbers are allowed here");
        //alert(targetElement.id);
        document.getElementById(id).value='';
        document.getElementById(id).select();
        return false;
        }
    }
    }
    return true;
}

function checkDistanceValue(){
	var chk=document.forms['rateGrid'].elements['twoDGridUnit'].value;
	var count1=0;
	var buyRatenew;
	var ratenew;
	if(chk=='DMI' || chk=='DKM'){
		var chkZero=0;
		var len=0;
		 try{
			 <c:forEach items="${rateGridList}" var="name"> 
				len++; 
		  		 var temp = '${name.id}';
		  		var distanceIds= document.getElementById('quantity2'+temp).value;
		  		var quantityIds= document.getElementById('quantity1'+temp).value;
		  		if(distanceIds==0){
		  	  		chkZero++;
		   		}
		  		else{
		  		var readDot=0;
		  		 for(var i=0;i<distanceIds.length;i++){
						if(distanceIds.charAt(i)=='.'){
							readDot++;
							if(readDot>1){
							    alert('Please enter correct value in distance.');
							    document.getElementById('quantity2'+temp).value='';
							    document.getElementById('quantity2'+temp).focus();
							    readDot=0;
							    return false;
							}
						}
					 }
		  		}
		  		if(quantityIds!=0){
		  			var readDot=0;
			  		 for(var i=0;i<quantityIds.length;i++){
							if(quantityIds.charAt(i)=='.'){
								readDot++;
								if(readDot>1){
								    alert('Please enter correct value in quantity.');
								    document.getElementById('quantity1'+temp).value='';
								    document.getElementById('quantity1'+temp).focus();
								    readDot=0;
								    return false;
								}
							}
						 }	
			  	}
		  		buyRatenew =document.getElementById('buyRate'+temp).value; 
				 ratenew =document.getElementById('rate1'+temp).value;
				//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0)){
					if((buyRatenew==0 && ratenew==0)){
					count1++; 
				 document.getElementById('quantity1'+temp).value='';
				 document.getElementById('buyRate'+temp).value='';
				 document.getElementById('rate1'+temp).value='';
				
				 }
		  	</c:forEach>
 		 }
		 catch(e){}
		 try{
		 var addValue =document.getElementById('quantity2new').value;
		 var newQuantity=document.getElementById('quantitynew').value;
		 if(addValue==0){
	 			chkZero++;
	 		}else{
	 		var readDot=0;
			 for(var i=0;i<addValue.length;i++){
				if(addValue.charAt(i)=='.'){
					readDot++;
					if(readDot>1){
					    alert('Please enter correct value in distance.');
					    document.getElementById('quantity2new').value='';
					    document.getElementById('quantity2new').focus();
					    readDot=0;
					    return false;
					}
				}
			 }
	 		}
	 		if(newQuantity!=0){
	 			var readDot=0;
				 for(var i=0;i<newQuantity.length;i++){
					if(newQuantity.charAt(i)=='.'){
						readDot++;
						if(readDot>1){
						    alert('Please enter correct value in quantity.');
						    document.getElementById('quantitynew').value='';
						    document.getElementById('quantitynew').focus();
						    readDot=0;
						    return false;
						}
					}
				 }
		 	}
	 		buyRatenew =document.getElementById('buyRatenew').value; 
	 		ratenew =document.getElementById('ratenew').value;
	 		//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0))
	 			if((buyRatenew==0 && ratenew==0))
	 		{
	 			count1++;
				 document.getElementById('quantitynew').value='';
				 document.getElementById('buyRatenew').value='';
				 document.getElementById('ratenew').value='';
				 document.getElementById('quantitynew').value.focus();
				 return false;
			}
		 }catch(e){}
	    if(chkZero==len){
			alert("Since 2D unit is selected, at least the distance for one line is required.");
			return false;
		}
	    else{
	    	if(count1==0){
		    submitAction();
	    	}
		}
	}
	 else{
		 var count=0;
		 var buyRatenew;
		 var ratenew;
		 try{
			 <c:forEach items="${rateGridList}" var="name"> 
				var temp = '${name.id}';
		  		var quantityIds= document.getElementById('quantity1'+temp).value;
		  		if(quantityIds!=0){
		  			var readDot=0;
			  		 for(var i=0;i<quantityIds.length;i++){
							if(quantityIds.charAt(i)=='.'){
								readDot++;
								if(readDot>1){
								    alert('Please enter correct value in quantity.');
								    document.getElementById('quantity1'+temp).value='';
								    document.getElementById('quantity1'+temp).focus();
								    readDot=0;
								    return false;
								}
							}
						 }	
			  	}
		  		 buyRatenew =document.getElementById('buyRate'+temp).value; 
				 ratenew =document.getElementById('rate1'+temp).value;
				//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0)){
					if((buyRatenew==0 && ratenew==0)){
					 count++; 
				 document.getElementById('quantity1'+temp).value='';
				 document.getElementById('buyRate'+temp).value='';
				 document.getElementById('rate1'+temp).value='';
				
				// return false;
				 }
		  		
		  </c:forEach>
 		 	}
		 catch(e){}
		 try{
			 var addValue =document.getElementById('quantitynew').value;
			 if(addValue!=0){
		 		var readDot=0;
				 for(var i=0;i<addValue.length;i++){
					if(addValue.charAt(i)=='.'){
						readDot++;
						if(readDot>1){
						    alert('Please enter correct value in quantity.');
						    document.getElementById('quantitynew').value='';
						    document.getElementById('quantitynew').focus();
						    readDot=0;
						    return false;
						}
					}
				 }
		 		}
			 buyRatenew =document.getElementById('buyRatenew').value; 
			 ratenew =document.getElementById('ratenew').value;
			//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0)){
				if((buyRatenew==0 && ratenew==0)){
				 count++; 
			 document.getElementById('quantitynew').value='';
			 document.getElementById('buyRatenew').value='';
			 document.getElementById('ratenew').value='';
			 document.getElementById('quantitynew').value.focus();
			 return false;
			 }
			 }catch(e){}
			 
			 if(count==0){
				 submitAction();
			 }
	}
}
function addNewGrid(){
	
	var chk=document.forms['rateGrid'].elements['twoDGridUnit'].value;
	var count1=0;
	var buyRatenew;
	var ratenew;
	if(chk=='DMI' || chk=='DKM'){
		 try{
			 <c:forEach items="${rateGridList}" var="name"> 
				var temp = '${name.id}';
		  		var distanceIds= document.getElementById('quantity2'+temp).value
		  		var quantityIds= document.getElementById('quantity1'+temp).value
		  		if(distanceIds!=0){
		  	  		var readDot=0;
		  			 for(var i=0;i<distanceIds.length;i++){
						if(distanceIds.charAt(i)=='.'){
							readDot++;
							if(readDot>1){
							    alert('Please enter correct value in distance.');
							    document.getElementById('quantity2'+temp).value='';
							    document.getElementById('quantity2'+temp).focus();
							    readDot=0;
							    return false;
							}
						}
					 }
		  		}
		  		if(quantityIds!=0){
		  			var readDot=0;
			  		 for(var i=0;i<quantityIds.length;i++){
							if(quantityIds.charAt(i)=='.'){
								readDot++;
								if(readDot>1){
								    alert('Please enter correct value in quantity.');
								    document.getElementById('quantity1'+temp).value='';
								    document.getElementById('quantity1'+temp).focus();
								    readDot=0;
								    return false;
								}
							}
						 }	
			  	}
		  		
		  		 buyRatenew =document.getElementById('buyRate'+temp).value; 
				 ratenew =document.getElementById('rate1'+temp).value;
				//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0)){
					if((buyRatenew==0 && ratenew==0)){
						
					count1++; 
				 document.getElementById('quantity1'+temp).value='';
				 document.getElementById('buyRate'+temp).value='';
				 document.getElementById('rate1'+temp).value='';
				}
		  	</c:forEach>
 		 }
		 catch(e){}
		 try{
		 var addValue =document.getElementById('quantity2new').value;
		 var newQuantity=document.getElementById('quantitynew').value;
		 if(addValue!=0){
	 		var readDot=0;
			 for(var i=0;i<addValue.length;i++){
				if(addValue.charAt(i)=='.'){
					readDot++;
					if(readDot>1){
					    alert('Please enter correct value in distance.');
					    document.getElementById('quantity2new').value='';
					    document.getElementById('quantity2new').focus();
					    readDot=0;
					    return false;
					}
				}
			 }
	 		}
	 		if(newQuantity!=0){
	 			var readDot=0;
				 for(var i=0;i<newQuantity.length;i++){
					if(newQuantity.charAt(i)=='.'){
						readDot++;
						if(readDot>1){
						    alert('Please enter correct value in quantity.');
						    document.getElementById('quantitynew').value='';
						    document.getElementById('quantitynew').focus();
						    readDot=0;
						    return false;
						}
					}
				 }
		 	}
	 		
	 		buyRatenew =document.getElementById('buyRatenew').value; 
	 		ratenew =document.getElementById('ratenew').value;
	 		//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0))
	 			if((buyRatenew==0 && ratenew==0))
	 		{
	 				
	 			count1++;
				 document.getElementById('quantitynew').value='';
				 document.getElementById('buyRatenew').value='';
				 document.getElementById('ratenew').value='';
				 document.getElementById('quantitynew').value.focus();
				 return false;
			}
		 }catch(e){}
		if(count1==0){
	    addAction();
		}
		}
	 else{
		 
		 var count=0;
		 var buyRatenew;
		 var ratenew;
		try{
			 <c:forEach items="${rateGridList}" var="name"> 
				var temp = '${name.id}';
		  		var quantityIds= document.getElementById('quantity1'+temp).value
		  		if(quantityIds!=0){
		  			var readDot=0;
			  		 for(var i=0;i<quantityIds.length;i++){
							if(quantityIds.charAt(i)=='.'){
								readDot++;
								if(readDot>1){
									count++;
								    alert('Please enter correct value in quantity.');
								    document.getElementById('quantity1'+temp).value='';
								    document.getElementById('quantity1'+temp).focus()
								    readDot=0;
								    return false;
								}
							}
						 }	
			  	}
		  		
		  		 buyRatenew =document.getElementById('buyRate'+temp).value; 
				 ratenew =document.getElementById('rate1'+temp).value;
				//if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0)){
					if((buyRatenew==0 && ratenew==0)){
					count++;
				 document.getElementById('quantity1'+temp).value='';
				 document.getElementById('buyRate'+temp).value='';
				 document.getElementById('rate1'+temp).value='';
				}
		  </c:forEach>
 		 	}
		 catch(e){}
		 try{
			 var addValue =document.getElementById('quantitynew').value;
			 buyRatenew =document.getElementById('buyRatenew').value; 
			 ratenew =document.getElementById('ratenew').value;
			 if(addValue!=0){
		 		var readDot=0;
				 for(var i=0;i<addValue.length;i++){
					if(addValue.charAt(i)=='.'){
						readDot++;
						if(readDot>1){
							count++;
						    alert('Please enter correct value in quantity.');
						    readDot=0;
						    document.getElementById('quantitynew').value='';
						    document.getElementById('quantitynew').value.focus();
						    return false;
						}
					}
				 }
		 		}
			 //if((buyRatenew==0 && ratenew!=0) || (buyRatenew==0 && ratenew==0) || (buyRatenew!=0 && ratenew==0)){
				 if((buyRatenew==0 && ratenew==0)){
				 count++;
			 document.getElementById('quantitynew').value='';
			 document.getElementById('buyRatenew').value='';
			 document.getElementById('ratenew').value='';
			 document.getElementById('quantitynew').value.focus();
			 return false;
			 }
			 }
			 
		 catch(e){}
			 if(count==0){
			 
				 addAction();
				 }
			 else{
				 count=0;
				 }
			 }
}
function checkReadOnly(){
    var chkCorpId= '${contracts.owner}';
    var corpId='${rateGridCorpId}';
    if(corpId!=chkCorpId){
      var len = document.forms['rateGrid'].length;
      for (var i=0 ;i<len ;i++){ 
    	  document.forms['rateGrid'].elements[i].disabled = true;
      }
   }
}




function onchangeCheckValue(id,fieldname){
	var chk=document.forms['rateGrid'].elements['twoDGridUnit'].value;
	var count1=0;
	var buyRatenew;
	var ratenew;
	var temp = id;
	var msg='';
	if(chk=='DMI' || chk=='DKM'){
		var chkZero=0;
		var len=0;
		 try{
				if((document.getElementById('buyRate'+temp).value==0 || document.getElementById('buyRate'+temp).value=='') && (document.getElementById('rate1'+temp).value==0 || document.getElementById('rate1'+temp).value=='')){
					count1++;
					if(fieldname!='Quantity'){
						if(fieldname=='rate1'){
							msg='Sell Rate';
						}
						if(fieldname=='buyRate'){
							msg='Buy Rate';
						}
					if(document.getElementById(fieldname+temp).value==0){
					alert(msg+" cannot be 0");
					document.getElementById(fieldname+temp).value='';
				 	document.getElementById(fieldname+temp).focus();				 	
				 	return false;
					}
					}
				 }
 		 }
		 catch(e){} 
	}
	 else{
		 try{
				if((document.getElementById('buyRate'+temp).value==0 || document.getElementById('buyRate'+temp).value=='') && (document.getElementById('rate1'+temp).value==0 || document.getElementById('rate1'+temp).value=='')){
					count1++;
					if(fieldname!='Quantity'){
						if(fieldname=='rate1'){
							msg='Sell Rate';
						}
						if(fieldname=='buyRate'){
							msg='Buy Rate';
						}
					if(document.getElementById(fieldname+temp).value==0){
					alert(msg+" cannot be 0");
					document.getElementById(fieldname+temp).value='';
				 	document.getElementById(fieldname+temp).focus();
				 	return false;
					}
					}
				 }
 		 	}
		 catch(e){}
		 
			 if(count1==0){
				 return true;
			 }
	}
}
</script>



<body >
<s:form id="rateGrid" name="rateGrid"  method="post">
<s:token/>
<s:hidden name="btnType" value="<%=request.getParameter("btnType")%>" />
<s:hidden name="chid" value="<%=request.getParameter("chid")%>" />
<s:hidden name="contractName" value="<%=request.getParameter("contract")%>" />
<s:hidden name="onSiteQuantity2Check" />
<s:hidden name="onSiteStatusCheck"/>
<s:hidden name="onSiteRateCheck"/>
<s:hidden name="onSiteBuyRateCheck"/>
<c:set var="twoDGridUnit"  value="<%=request.getParameter("twoDGridUnit")%>" />
<s:hidden name="twoDGridUnit"  value="<%=request.getParameter("twoDGridUnit")%>" />
<div id="Layer1" style="width: 500px">
<div id="otabs" style="float:left;margin-bottom:0px;">
<ul>
	<li><a class="current"><span>Rate Grid</span></a></li>
	</ul>
	</div>
	<div style="padding-top:2px;"><s:textfield name="charges.charge" readonly="true"  cssClass="input-textUpper" maxlength="8" size="20" /></div>
  <div class="spn">&nbsp;&nbsp;&nbsp;&nbsp;</div> 
<c:set var="rowCount"  value="1" />
<display:table name="rateGridList" class="table" requestURI="" id="rateGridId"   export="false" pagesize="10" style="width:125%;margin-top:0px;" >

<c:if test="${not empty rateGridId.id}">
<c:if test="${rowCount!='1'}">
	<display:column  title="Distance&nbsp;(Up To)">
	<c:if test="${twoDGridUnit=='' || twoDGridUnit=='ND' }" >
	<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2${rateGridId.id}" required="true" value="${rateGridId.quantity2}" cssClass="input-text" maxlength="8" size="15"  disabled="true"/>
	</c:if>
	<c:if test="${twoDGridUnit=='DMI' || twoDGridUnit=='DKM' }" >
	<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2${rateGridId.id}" required="true" value="${rateGridId.quantity2}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceQuantity2Change(${rateGridId.id},this);return dotNotAllowed(this,'rateGrid.quantity2');"  onblur="return isFloat(this);" />
	</c:if>
	
	</display:column>
	<display:column  title="Quantity&nbsp;(Up To)">
	<s:textfield cssStyle="text-align:right" name="rateGrid.quantity1" id="quantity1${rateGridId.id}" required="true" value="${rateGridId.quantity1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceChange(${rateGridId.id},this);onchangeCheckValue(${rateGridId.id},'Quantity');return dotNotAllowed(this,'rateGrid.quantity1');"   onblur="return isFloat(this);" /></display:column>
	<display:column title="Sell Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.rate1" required="true" id="rate1${rateGridId.id}" value="${rateGridId.rate1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceRateChange(${rateGridId.id},this);onchangeCheckValue(${rateGridId.id},'rate1');return dotNotAllowed(this,'rateGrid.rate1');" onblur="return isFloat(this);"  /></display:column>
	<display:column title="Buy Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.buyRate" required="true" id="buyRate${rateGridId.id}" value="${rateGridId.buyRate}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceBuyRateChange(${rateGridId.id},this);onchangeCheckValue(${rateGridId.id},'buyRate');return dotNotAllowed(this,'rateGrid.buyRate');" onblur="return isFloat(this);"  /></display:column>
</c:if>
<c:if test="${rowCount=='1'}">
	<display:column  title="Distance&nbsp;(Up To) ">
	<c:if test="${twoDGridUnit=='' || twoDGridUnit=='ND' }" >
	<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2${rateGridId.id}" required="true" value="${rateGridId.quantity2}" cssClass="input-text" maxlength="8" size="15"  disabled="true"/>&nbsp;Min.Qty
	</c:if>
	<c:if test="${twoDGridUnit=='DMI' || twoDGridUnit=='DKM' }" >
	<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2${rateGridId.id}" required="true" value="${rateGridId.quantity2}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceQuantity2Change(${rateGridId.id},this);return dotNotAllowed(this,'rateGrid.quantity2');"  onblur="return isFloat(this);"  />&nbsp;Min.Qty
	</c:if>
	
	</display:column>
	<display:column  title="Quantity&nbsp;(Up To) ">
	<s:textfield cssStyle="text-align:right" name="rateGrid.quantity1" id="quantity1${rateGridId.id}" required="true" value="${rateGridId.quantity1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceChange(${rateGridId.id},this);onchangeCheckValue(${rateGridId.id},'Quantity');return dotNotAllowed(this,'rateGrid.quantity1');"  onblur="return isFloat(this);"  />&nbsp;Min.Qty</display:column>
	<display:column title="Sell Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.rate1" required="true" id="rate1${rateGridId.id}" value="${rateGridId.rate1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceRateChange(${rateGridId.id},this);onchangeCheckValue(${rateGridId.id},'rate1');return dotNotAllowed(this,'rateGrid.rate1');" onblur="return isFloat(this);"  /></display:column>
	<display:column title="Buy Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.buyRate" required="true" id="buyRate${rateGridId.id}" value="${rateGridId.buyRate}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceBuyRateChange(${rateGridId.id},this);onchangeCheckValue(${rateGridId.id},'buyRate');return dotNotAllowed(this,'rateGrid.buyRate');" onblur="return isFloat(this);"  /></display:column>
</c:if>
</c:if>
<c:if test="${empty rateGridId.id}">
<c:if test="${rowCount!='1'}">
<display:column  title="Distance&nbsp;(Up To)">
<c:if test="${twoDGridUnit=='' || twoDGridUnit=='ND' }" >
<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2new" required="true" value="${rateGridId.quantity2}"  cssClass="input-text" maxlength="8" size="15"  disabled="true"/>
</c:if>
<c:if test="${twoDGridUnit=='DMI' || twoDGridUnit=='DKM' }" >
<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2new" required="true" value="${rateGridId.quantity2}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceQuantity2Change('newGrid',this);return dotNotAllowed(this,'rateGrid.quantity2');"  onblur="return isFloat(this);" />
</c:if>
</display:column>
<display:column  title="Quantity&nbsp;(Up To)"><s:textfield cssStyle="text-align:right" name="rateGrid.quantity1" id="quantitynew" required="true" value="${rateGridId.quantity1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceChange('newGrid',this);return dotNotAllowed(this,'rateGrid.quantity1');"  onblur="return isFloat(this);" /></display:column>
<display:column title="Sell Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.rate1" required="true" id="ratenew" value="${rateGridId.rate1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceRateChange('newGrid',this);return dotNotAllowed(this,'rateGrid.rate1');" onblur="return isFloat(this);" /></display:column>
<display:column title="Buy Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.buyRate" required="true" id="buyRatenew" value="${rateGridId.buyRate}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="20" onchange="traceBuyRateChange('buyRatenew',this);return dotNotAllowed(this,'rateGrid.buyRate');" onblur="return isFloat(this);" /></display:column>
</c:if>
<c:if test="${rowCount=='1'}">
<display:column  title="Distance&nbsp;(Up To)">
<c:if test="${twoDGridUnit=='' || twoDGridUnit=='ND' }" >
<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2new" required="true" value="${rateGridId.quantity2}"  cssClass="input-text" maxlength="8" size="15"  disabled="true"/>&nbsp;Min.Qty
</c:if>
<c:if test="${twoDGridUnit=='DMI' || twoDGridUnit=='DKM' }" >
<s:textfield cssStyle="text-align:right" name="rateGrid.quantity2" id="quantity2new" required="true" value="${rateGridId.quantity2}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceQuantity2Change('newGrid',this);return dotNotAllowed(this,'rateGrid.quantity2');"  onblur="return isFloat(this);"/>&nbsp;Min.Qty
</c:if>
</display:column>
<display:column  title="Quantity&nbsp;(Up To)"><s:textfield cssStyle="text-align:right" name="rateGrid.quantity1" id="quantitynew" required="true" value="${rateGridId.quantity1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="8" size="15"  onchange="traceChange('newGrid',this);return dotNotAllowed(this,'rateGrid.quantity1');" onblur="return isFloat(this);"/>&nbsp;Min.Qty</display:column>
<display:column title="Sell Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.rate1" required="true" id="ratenew" value="${rateGridId.rate1}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceRateChange('newGrid',this);return dotNotAllowed(this,'rateGrid.rate1');" onblur="return isFloat(this);" /></display:column>
<display:column title="Buy Rate"><s:textfield cssStyle="text-align:right" name="rateGrid.buyRate" required="true" id="buyRatenew" value="${rateGridId.buyRate}" onkeypress="return numbersonly(this,event);" cssClass="input-text" maxlength="10" size="20" onchange="traceBuyRateChange('buyRatenew',this);return dotNotAllowed(this,'rateGrid.buyRate');" onblur="return isFloat(this);" /></display:column>
</c:if>
</c:if>
<display:setProperty name="basic.msg.empty_list_row" value="<tr class='empty' id='empty'><td colspan='{0}'>Click on Add button to add rows</td></tr>"/>  
<c:set var="rowCount"  value="2" />
</display:table>

<table class="detailTabLabel" border="0">
			<tbody>  	
				  	<tr>
				  		<td align="left" height="3px" colspan="4"></td>
		  	</tr>
		  	
		  	<tr>
		  	
		  	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Add" onclick="return addNewGrid();" />   </td> 
		  	<td><input type="button" class="cssbutton" style="width:55px; height:25px" value="Save" onclick="return checkDistanceValue();"/> </td>
		  	<td> <input type="button" class="cssbutton" style="width:82px; height:25px" value="Upload Rates" onclick="uploadData();"/> </td>
		  	<td> <input type="button" class="cssbutton" style="width:145px; height:25px" value="Upload & Replace Rates" onclick="uploadAndReplace();"/> </td>
		  	<td> <input type="button" class="cssbutton" style="width:100px; height:25px" value="Export to Excel" onclick="exportRates()"/> </td>
		  	</tr>
		  <%-- 	<tr>
		  	<td align="right" class="listwhitetext strbg" colspan="4"><a href="${pageContext.request.contextPath}/images/SampleUploadFile_2DRateGrid.xls" >Sample Template </a></td>
		  	</tr>--%>
			</tbody>
   </table></div>
   
   
</s:form>
<script language="javascript" type="text/javascript">
try{ 
	// comment for ignoring refresh on save  - Gautam 
	//winLoad();
	}
catch(e){}
try{checkReadOnly();}
catch(e){}
</script>


  </body>
   