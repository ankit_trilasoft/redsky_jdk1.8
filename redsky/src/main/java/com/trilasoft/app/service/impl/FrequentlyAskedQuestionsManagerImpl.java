package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.FrequentlyAskedQuestionsDao;
import com.trilasoft.app.model.FrequentlyAskedQuestions;
import com.trilasoft.app.service.FrequentlyAskedQuestionsManager;


public class FrequentlyAskedQuestionsManagerImpl extends GenericManagerImpl<FrequentlyAskedQuestions, Long> implements FrequentlyAskedQuestionsManager { 
	FrequentlyAskedQuestionsDao frequentlyAskedQuestionsDao;
	public FrequentlyAskedQuestionsManagerImpl(FrequentlyAskedQuestionsDao frequentlyAskedQuestionsDao) {
		super(frequentlyAskedQuestionsDao); 
        this.frequentlyAskedQuestionsDao = frequentlyAskedQuestionsDao; 
		// TODO Auto-generated constructor stub
	}
	public List getNewFaqByPartnerCode(String partnerCode,String corpId){
		return frequentlyAskedQuestionsDao.getNewFaqByPartnerCode(partnerCode, corpId);
	}
	public List getFaqByPartnerCode(String partnerCode,String corpId){
		return frequentlyAskedQuestionsDao.getFaqByPartnerCode(partnerCode, corpId);
	}
	public Boolean getExcludeFPU(String partnerCode,String corpId){
		return frequentlyAskedQuestionsDao.getExcludeFPU(partnerCode, corpId);
	}
	public int checkFrequentlyAskedQuestions(String question,String answer,String language,String partnerCode,String sessionCorpID){
		return frequentlyAskedQuestionsDao.checkFrequentlyAskedQuestions(question, answer, language, partnerCode, sessionCorpID);
	}
	public List getFaqReference(String partnerCode, String corpID,Long parentId){
		return frequentlyAskedQuestionsDao.getFaqReference(partnerCode, corpID, parentId);
	}
	public List findMaximum(String language,String partnerCode,String corpID){
		return frequentlyAskedQuestionsDao.findMaximum(language, partnerCode, corpID);
	}
   public int checkFrequentlySequence(String language,String partnerCode,Long sequenceNumber,String sessionCorpID){
	   return frequentlyAskedQuestionsDao.checkFrequentlySequence(language, partnerCode,sequenceNumber,sessionCorpID);
   }
   public List getFAQByCMMDMMPartnerCode(String partnerCode,String corpId){
	   return frequentlyAskedQuestionsDao.getFAQByCMMDMMPartnerCode(partnerCode,corpId);
   }
   public List getFAQByPartnerCode(String partnerCode,String corpId){
	   return frequentlyAskedQuestionsDao.getFAQByPartnerCode(partnerCode,corpId);
   }
   public List getFAQByAdmin(String corpId){
	   return frequentlyAskedQuestionsDao.getFAQByAdmin(corpId);
   }
   public List findMaximumNumber(String language,String corpID){
	   return frequentlyAskedQuestionsDao.findMaximumNumber(language,corpID);  
   }
   public void deleteagentParentPolicyFile(String oldAgentParent,String partnerCode,String sessionCorpID){
	   frequentlyAskedQuestionsDao.deleteagentParentPolicyFile( oldAgentParent, partnerCode, sessionCorpID);
   }
   public void deleteChildDocument(Long id,String sessionCorpID){
	   frequentlyAskedQuestionsDao.deleteChildDocument( id, sessionCorpID);
   }
public int deletedChildFAQ(String partnerCode, String sessionCorpID) {
	return frequentlyAskedQuestionsDao.deletedChildFAQ(partnerCode, sessionCorpID);
}
}
