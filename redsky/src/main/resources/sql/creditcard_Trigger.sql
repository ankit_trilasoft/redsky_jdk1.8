

>>>>>
20-trigger_add_history_creditcard
DELIMITER $$
create trigger redsky.trigger_add_history_creditcard BEFORE UPDATE on redsky.creditcard
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
   
   

   IF ((OLD.ccApproved <> NEW.ccApproved) or (OLD.ccApproved is null and NEW.ccApproved is not null)
	  or (OLD.ccApproved is not null and NEW.ccApproved is null)) THEN
       select CONCAT(fieldNameList,'creditcard.ccApproved~') into fieldNameList;
       IF(OLD.ccApproved is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ccApproved is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ccApproved is not null) THEN
        	select CONCAT(oldValueList,OLD.ccApproved,'~') into oldValueList;
     	END IF;
      	IF(NEW.ccApproved is not null) THEN
        	select CONCAT(newValueList,NEW.ccApproved,'~') into newValueList;
      	END IF;
   END IF;

   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'creditcard.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;

   IF ((OLD.ccApprovedBillAmount <> NEW.ccApprovedBillAmount) or (OLD.ccApprovedBillAmount is null and NEW.ccApprovedBillAmount is not null) 
	  or (OLD.ccApprovedBillAmount is not null and NEW.ccApprovedBillAmount is null)) THEN
		select CONCAT(fieldNameList,'creditcard.ccApprovedBillAmount~') into fieldNameList;
		IF(OLD.ccApprovedBillAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.ccApprovedBillAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.ccApprovedBillAmount is not null) then
			select CONCAT(oldValueList,OLD.ccApprovedBillAmount,'~') into oldValueList;
		END IF;
		IF(NEW.ccApprovedBillAmount is not null) then
			select CONCAT(newValueList,NEW.ccApprovedBillAmount,'~') into newValueList;
		END IF; 
	END IF;

   IF (OLD.ccAuthorizationBillNumber <> NEW.ccAuthorizationBillNumber ) THEN
       select CONCAT(fieldNameList,'creditcard.ccAuthorizationBillNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ccAuthorizationBillNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ccAuthorizationBillNumber,'~') into newValueList;
   END IF;

   IF (OLD.ccExpires <> NEW.ccExpires ) THEN
       select CONCAT(fieldNameList,'creditcard.ccExpires~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ccExpires,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ccExpires,'~') into newValueList;
   END IF;

   IF (OLD.ccName <> NEW.ccName ) THEN
       select CONCAT(fieldNameList,'creditcard.ccName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ccName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ccName,'~') into newValueList;
   END IF;

   IF (OLD.ccNumber <> NEW.ccNumber ) THEN
       select CONCAT(fieldNameList,'creditcard.ccNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ccNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ccNumber,'~') into newValueList;
   END IF;

   IF (OLD.ccType <> NEW.ccType ) THEN
       select CONCAT(fieldNameList,'creditcard.ccType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ccType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ccType,'~') into newValueList;
   END IF;

   IF ((OLD.ccVlTransDate <> NEW.ccVlTransDate) or (OLD.ccVlTransDate is null and NEW.ccVlTransDate is not null)
	  or (OLD.ccVlTransDate is not null and NEW.ccVlTransDate is null)) THEN
       select CONCAT(fieldNameList,'creditcard.ccVlTransDate~') into fieldNameList;
       IF(OLD.ccVlTransDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ccVlTransDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ccVlTransDate is not null) THEN
        	select CONCAT(oldValueList,OLD.ccVlTransDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.ccVlTransDate is not null) THEN
        	select CONCAT(newValueList,NEW.ccVlTransDate,'~') into newValueList;
      	END IF;
   END IF;

   IF (OLD.securityCodeNo <> NEW.securityCodeNo ) THEN
       select CONCAT(fieldNameList,'creditcard.securityCodeNo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.securityCodeNo,'~') into oldValueList;
       select CONCAT(newValueList,NEW.securityCodeNo,'~') into newValueList;
   END IF;
   IF (OLD.expireYear <> NEW.expireYear ) THEN
       select CONCAT(fieldNameList,'creditcard.expireYear~') into fieldNameList;
       select CONCAT(oldValueList,OLD.expireYear,'~') into oldValueList;
       select CONCAT(newValueList,NEW.expireYear,'~') into newValueList;
   END IF;
         
     IF (OLD.expireMonth <> NEW.expireMonth ) THEN
       select CONCAT(fieldNameList,'creditcard.expireMonth~') into fieldNameList;
       select CONCAT(oldValueList,OLD.expireMonth,'~') into oldValueList;
       select CONCAT(newValueList,NEW.expireMonth,'~') into newValueList;
   END IF;
   
    IF (OLD.ccNumber1 <> NEW.ccNumber1 ) THEN
       select CONCAT(fieldNameList,'creditcard.ccNumber1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ccNumber1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ccNumber1,'~') into newValueList;
   END IF;  
   IF (OLD.primaryFlag <> NEW.primaryFlag ) THEN
       select CONCAT(fieldNameList,'creditcard.primaryFlag~') into fieldNameList;
       IF(OLD.primaryFlag = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.primaryFlag = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.primaryFlag = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.primaryFlag = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;  
   
   
   
   
   
   
  IF ((OLD.authorizationDate <> NEW.authorizationDate) or (OLD.authorizationDate is null and NEW.authorizationDate is not null)
	  or (OLD.authorizationDate is not null and NEW.authorizationDate is null)) THEN
       select CONCAT(fieldNameList,'creditcard.authorizationDate~') into fieldNameList;
       IF(OLD.authorizationDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.authorizationDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.authorizationDate is not null) THEN
        	select CONCAT(oldValueList,OLD.authorizationDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.authorizationDate is not null) THEN
        	select CONCAT(newValueList,NEW.authorizationDate,'~') into newValueList;
      	END IF;
   END IF;
   
   
   
    IF ((OLD.expiryDate <> NEW.expiryDate) or (OLD.expiryDate is null and NEW.expiryDate is not null)
	  or (OLD.expiryDate is not null and NEW.expiryDate is null)) THEN
       select CONCAT(fieldNameList,'creditcard.expiryDate~') into fieldNameList;
       IF(OLD.expiryDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.expiryDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.expiryDate is not null) THEN
        	select CONCAT(oldValueList,OLD.expiryDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.expiryDate is not null) THEN
        	select CONCAT(newValueList,NEW.expiryDate,'~') into newValueList;
      	END IF;
   END IF;  
   
     
   
   CALL add_tblHistory (OLD.id,"creditcard", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$
