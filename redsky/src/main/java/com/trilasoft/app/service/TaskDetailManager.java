package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.TaskDetail;

public interface TaskDetailManager extends GenericManager<TaskDetail, Long> {

public void saveTaskDetails(Long id, String fieldName, String fieldValue, String sessionCorpID, String selectedWO,String userType);

public List getTaskFromWorkOrder(String shipNumber, String selectedWO);

public String getValueFromTaskDetails(Long id, String shipNumber);

public void updateTransferField(Long id, String shipNumber);

}
