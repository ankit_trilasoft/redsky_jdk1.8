<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "Cartons" in Redsky.
 * @File Name	cartonForm
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        1-Dec-2008
 * --%>



<%@ include file="/common/taglibs.jsp"%>
 <%@ taglib prefix="s" uri="/struts-tags" %> 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>   
    <title><fmt:message key="cartonDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='cartonDetail.heading'/>"/> 
  


</head> 
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script> 
 <script language="JavaScript">
$('input,select,textfield :visible').each(function (i) {
	$(this).attr('tabindex', i + 1);

	});
var links = document.getElementsByTagName( 'a' );
for( var i = 0, j =  links.length; i < j; i++ ) {
    links[i].setAttribute( 'tabindex', '-1' );
}
  </script>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
 <s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="cartonForm"  action="saveCarton" onsubmit="return submit_form()" method="post" validate="true" >
<c:set var="ischecked" value="true"/>
<c:set var="cssClassView" value="input-textUpper"/>
<configByCorp:fieldVisibility componentId="component.field.Van.Carton.Crate.Description">
	<c:set var="ischecked" value="false"/>
	<c:set var="cssClassView" value="input-text"/>
</configByCorp:fieldVisibility>   
<s:hidden name="carton.id" value="%{carton.id}"/>   

<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="calOpener" value="notOPen" />
<s:hidden name="carton.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="sid" value="%{serviceOrder.id}"/>
<s:hidden name="carton.unit1"/> 
<s:hidden name="carton.unit2"/>	
<s:hidden name="calculatecheck"/>
<s:hidden  name="carton.corpID" />	
<s:hidden  name="carton.ugwIntId" />	
<s:hidden name="carton.serviceOrderId" value="%{serviceOrder.id}"/>
  <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
	<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
	<s:hidden id="countCortonNotes" name="countCortonNotes" value="<%=request.getParameter("countCortonNotes") %>"/>
	<c:set var="countCortonNotes" value="<%=request.getParameter("countCortonNotes") %>" />
	<s:hidden name="customerFile.id" />
	<s:hidden name="shipSize" />
    <s:hidden name="minShip" />
    <s:hidden name="countShip" />
    <s:hidden name="minChild" />
    <s:hidden name="maxChild" />
    <s:hidden name="countChild" />
	
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
    <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
    <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
    <c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
    <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
    <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>
			   
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
    <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
    <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.cartons' }">
    <c:redirect url="/cartons.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.vehicles' }">
    <c:redirect url="/vehicles.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.containers' }">
    <c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.servicepartners' }">
    <c:redirect url="/servicePartnerss.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
    <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
	</c:if>
	<div id="Layer3" style="width:100%">
	<div id="newmnav"  style="float:left;">
         
         
            <ul>
              <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
              <li><a onclick="setReturnString('gototab.serviceorder');return ContainerAutoSave('none');"><span>S/O Details</span></a></li>
              </sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.container.billingTab">
              <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
             	<li><a onclick="setReturnString('gototab.billing');return ContainerAutoSave('none');" ><span>Billing</span></a></li>
              </sec-auth:authComponent></sec-auth:authComponent>
                  <sec-auth:authComponent componentId="module.tab.container.accountingTab">
              <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			   <c:otherwise> 
			      <li><a onclick="setReturnString('gototab.accounting');return ContainerAutoSave('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			   <c:otherwise> 
			      <li><a onclick="setReturnString('gototab.newAccounting');return ContainerAutoSave('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>		      
		       <sec-auth:authComponent componentId="module.tab.container.forwardingTab"> 
              <li id="newmnav1" style="background:#FFF"><a class="current" onclick="setReturnString('gototab.containers'); return ContainerAutoSave('none');"><span>Forwarding</span></a></li>
              </sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.container.domesticTab">
              <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
              <li><a onclick="setReturnString('gototab.domestic');return  ContainerAutoSave('none');"><span>Domestic</span></a></li>
              </c:if>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a onclick="setReturnString('gototab.domestic');return  ContainerAutoSave('none');"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.container.statusTab">
              <li><a onclick="setReturnString('gototab.status');return  ContainerAutoSave('none');"><span>Status</span></a></li>
              </sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.container.ticketTab">
              <li><a onclick="setReturnString('gototab.ticket');return ContainerAutoSave('none');"><span>Ticket</span></a></li>
              </sec-auth:authComponent>
               <configByCorp:fieldVisibility componentId="component.standard.claimTab">
               <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
              <li><a onclick="setReturnString('gototab.claims');return  ContainerAutoSave('none');"><span>Claims</span></a></li>
              </sec-auth:authComponent>
              </configByCorp:fieldVisibility>
               <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			      <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
                <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
              <li><a onclick="setReturnString('gototab.customerfile');return ContainerAutoSave('none');"><span>Customer File</span></a></li>
            </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.container.reportTab"> 
			<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Billing&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
      			<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
  				</sec-auth:authComponent>
  			<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
    			 <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
   			</sec-auth:authComponent>
            </ul>
        </div>
        <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float:left;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td> 
  		<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>
        <div class="spn">&nbsp;</div>
      
        </div>
        

<div id="Layer1"  onkeydown="changeStatus();" style="width:100%">
  <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>

 <div id="newmnav" style="float:left;">   
 <ul>
   <sec-auth:authComponent componentId="module.tab.container.sSContainertTab">
  <li><a onclick="setReturnString('gototab.containers');return ContainerAutoSave('none');"  ><span>SS Container</span></a></li>
 </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.container.pieceCountTab">
  <li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.cartons');return ContainerAutoSave('none');"  class="current"><span>Piece Count</span></a></li>
 </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.vehicleTab">
  <li><a onclick="setReturnString('gototab.vehicles');return ContainerAutoSave('none');"  ><span>Vehicle</span></a></li>
 </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.routingTab">
  <li><a onclick="setReturnString('gototab.servicepartners');return ContainerAutoSave('none');"  ><span>Routing</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.consigneeInstructionsTab">
  <li><a href="editConsignee.html?sid=${serviceOrder.id}"><span>Consignee Instructions</span></a></li>
  </sec-auth:authComponent>
    
    <c:if test="${countBondedGoods >= 0}" >
  <li><a href="customs.html?id=${serviceOrder.id}"><span>Customs</span></a></li>
  </c:if>
  
  </ul>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float:left;"><tr>
		<c:if test="${not empty carton.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${carton.id > minChild}" >
  		<a><img align="middle" onclick="goPrevChild();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${carton.id == minChild}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<td width="20px" align="left">
  		<c:if test="${carton.id < maxChild}" >
  		<a><img align="middle" onclick="goNextChild();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${carton.id == maxChild}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countChild != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSOChild(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="SO Ticket List" title="SO Piece List" /></a> 
		</c:if>
		<c:if test="${countChild == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>

<div class="spn">&nbsp;</div>
<div id="content" align="center" style="!margin-top:-20px;">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="0" cellpadding="0"	border="0" style="width:100%;margin-bottom:0px; padding-bottom:0px;">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="margin-bottom:0px; padding-bottom:0px;">
				<tbody> 
  <tr>
  <td align="right" width="50px" class="listwhitetext">Status</td>
 <c:set var="isStatusFlag" value="false"/>
 <c:if test="${carton.status}">
 <c:set var="isStatusFlag" value="true"/>
 </c:if>
 <td align="left" width="20px" valign="bottom"><s:checkbox key="carton.status" value="${isStatusFlag}" onchange="statusValidation(this);"   fieldValue="true"  tabindex="3" /></td>
  </tr>	
   			
  <tr>
  <td align="right" class="listwhitetext"><fmt:message key="carton.idNumber"/></td>
  <td  align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" id="idNum" name="carton.idNumber" size="1"  maxlength="2" readonly="true" tabindex="" onchange="onlyNumeric(this);" /></td>  
<td align="right"  width="172px" class="listwhitetext"><fmt:message key="carton.cartonType1"/></td>
<td align="left" width="239px">
<s:textfield cssClass="input-text" id="cartonType" name="carton.cartonType" size="30" maxlength="15" readonly="true" onchange="changeStatus();" tabindex="" onblur="copyCType(),checkUnilPopulate();"/>
<img class="openpopup" width="17" height="20" align="top" onclick= "changeStatus();dataPopulate();" src="<c:url value='/images/open-popup.gif'/>"/>
</td>

 <td align="left" class="listwhitetext">Description</td>
  <td  align="left" class="listwhitetext" ><s:textfield cssClass="${cssClassView}" id="description" name="carton.description" size="50" tabindex="" maxlength="100" readonly="${ischecked}" /></td>  
  </tr>
  
  <tr>
  <td align="right" width="" class="listwhitetext"><fmt:message key="container.containerNumber"/></td>
 <td  align="left" class="listwhitetext" width="100px"><s:select  id="cntnrNumber" cssClass="list-menu" key="carton.cntnrNumber"  list="%{containerNumberList}"   cssStyle="width:110px" onchange="changeStatus();" tabindex="" headerKey="" headerValue="" /></td>

<td align="right" class="listwhitetext"><fmt:message key="carton.pieces"/></td>
<td  align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.pieces" size="1"  maxlength="4"  onchange="isNumeric(this);" tabindex="" /></td>
 
  <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
 <c:if test="${empty carton.id}">
							<td width="" colspan="5" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty carton.id}">
							<c:choose>
								<c:when test="${countCortonNotes == '0' || countCortonNotes == '' || countCortonNotes == null}">
								<td width="" colspan="5" align="right"><img id="countCortonNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${carton.id }&notesId=${carton.id }&noteFor=Carton&subType=Carton&imageId=countCortonNotesImage&fieldId=countCortonNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${carton.id }&notesId=${carton.id }&noteFor=Carton&subType=Carton&imageId=countCortonNotesImage&fieldId=countCortonNotes&decorator=popup&popup=true',755,500);" ></a></td>
								</c:when>
								<c:otherwise>
								<td width="" colspan="5" align="right"><img id="countCortonNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${carton.id }&notesId=${carton.id}&noteFor=Carton&subType=Carton&imageId=countCortonNotesImage&fieldId=countCortonNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${carton.id }&notesId=${carton.id }&noteFor=Carton&subType=Carton&imageId=countCortonNotesImage&fieldId=countCortonNotes&decorator=popup&popup=true',755,500);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
</sec-auth:authComponent>
</tr>
</tbody>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="margin-bottom:0px; padding-bottom:0px;"  width="100%">
<tr>
<td align="left"  width="100%" class="vertlinedata"></td>
</tr>
</table>
<table>
<tbody>
<tr>
<td  align="right" class="listwhitetext" width="73"><fmt:message key="carton.length"/></td>
<td   align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.length" size="10" maxlength="5"  onchange="onlyFloat(this);" tabindex="" /></td>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="carton.width"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.width" size="10"  maxlength="5" onchange="onlyFloat(this);" tabindex="" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="carton.height"/></td>
<td align="left" class="listwhitetext" >
<s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.height" size="10"  maxlength="5" onchange="onlyFloat(this);" tabindex="" />
</td>
<td class="listwhitetext" ><fmt:message key='labels.lenunit'/>
<s:select name="carton.unit3" cssClass="list-menu"  list="%{lengthunits}" onchange="unitChangeVolume(this),changeStatus();" tabindex="" />
</td>
</tr>
</tbody>
</table>


<table width="100%" class="colored_bg" align="left" style="margin-bottom:0px; padding-bottom:0px;" border="0">
<tr>
<td align="left" style="margin-bottom: 0px;">
<fieldset >
   <legend>Pounds & Cft
   <c:if test="${weightType == 'lbscft'}">			
						<INPUT type="radio" name="weightType" checked="checked" value="lbscft" id="weight1" onclick="check(this);changeStatus();">
						</div>
						</c:if>
						<c:if test="${weightType != 'lbscft'}">			
						<INPUT type="radio" name="weightType" value="lbscft" id="weight1" onclick="check(this);changeStatus();">
						</div>
						</c:if>
   </legend>
<table  style="margin-bottom:0px; padding-bottom:0px;">
<tr>
  <td align="right"  class="listwhitetext" width="70px"><fmt:message key="carton.grossWeight"/></td>
  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.grossWeight" size="10" maxlength="10" onchange="onlyFloat(this); calcNetWeight();" tabindex="" /></td>
  
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="carton.emptyContWeight"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.emptyContWeight" size="10" maxlength="10" onchange="onlyFloat(this); changeStatus();calcNetWeight();" tabindex="" /></td>
</tr>
<tr>
  <td align="right"  class="listwhitetext"><fmt:message key="carton.netWeight"/></td>
  <td  align="left"  class="listwhitetext"><s:textfield cssClass="input-text"  cssStyle="text-align:right" name="carton.netWeight" size="10" maxlength="10" onchange="onlyFloat(this);calcTare();" tabindex="" /></td>

</tr>
<tr>
  <td align="right"  class="listwhitetextAcct" width="85px"><fmt:message key="carton.volume"/></td>
  <td  class="listwhitetextAcct">
  <s:textfield cssClass="input-textUpper" name="carton.volume" size="10" cssStyle="text-align:right" readonly="true" maxlength="15"  onchange="onlyFloat(this);" tabindex="" />

  </td>
  </tr>
  <tr>
  <td  align="right" class="listwhitetextAcct"><fmt:message key="carton.density"/></td>
 <td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="carton.density"  size="10" readonly="true"  maxlength="5"  tabindex=""  />
 </td> </tr>
 <tr>
 <td></td>
 <td>
  <input type="button" class="cssbuttonA" style="width:70px; height:24px"  name="calc" value="Calculate" onclick="calculate('carton.unit2','carton.unit3')" onmouseover="return chkSelect('calc');"  tabindex="" /> 

  </td> 
  </tr>
    <tr height="10px"></tr>
  </table>
  </fieldset>
  </td>

  <td>
  <fieldset>
  <legend>Metric Units
  <c:if test="${weightType == 'kgscbm'}">
  <INPUT type="radio" name="weightType" value="kgscbm" id="weight2" checked="checked" onclick="check(this);changeStatus();"></div>
                        </c:if>
						<c:if test="${weightType != 'kgscbm'}">
 <INPUT type="radio" name="weightType" value="kgscbm" id="weight2" onclick="check(this);changeStatus();"></div>
                        </c:if>
  
  </legend>
  <table style="margin-bottom:0px;">
<tr>
  <td align="right"  class="listwhitetext" width="70px"><fmt:message key="carton.grossWeight"/></td>
  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.grossWeightKilo" size="10" maxlength="10" onchange="onlyFloat(this); calcNetWeightKilo();" tabindex="" /></td>
  
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="carton.emptyContWeight"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="carton.emptyContWeightKilo" size="10" maxlength="10" onchange="onlyFloat(this); changeStatus();calcNetWeightKilo();" tabindex="" /></td>
</tr>
<tr>
  <td align="right"  class="listwhitetext"><fmt:message key="carton.netWeight"/></td>
  <td  align="left"  class="listwhitetext"><s:textfield cssClass="input-text"  cssStyle="text-align:right" name="carton.netWeightKilo" size="10" maxlength="10" onchange="onlyFloat(this);calcTareKilo();" tabindex="" /></td>
</tr>
<tr>
  <td align="right"  class="listwhitetextAcct" width="85px"><fmt:message key="carton.volume"/></td>
  <td  class="listwhitetextAcct">
  <s:textfield cssClass="input-textUpper" name="carton.volumeCbm" size="10" cssStyle="text-align:right" readonly="true" maxlength="15"  onchange="onlyFloat(this);" tabindex="" />

  </td>
  </tr>
  <tr>
  <td  align="right" class="listwhitetextAcct"><fmt:message key="carton.density"/></td>
 <td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="carton.densityMetric"  size="10" readonly="true"  tabindex=""  maxlength="5"  />
 </td> </tr>
 <tr>
 <td></td>
 <td>
  <input type="button" class="cssbuttonA" style="width:70px; height:24px"  name="calc" value="Calculate" onclick="calculate('carton.unit2','carton.unit3')" onmouseover="return chkSelect('calc');"  tabindex="" /> 

  </td> 
  </tr>
  <tr height="10px"></tr>	
</table>
</fieldset>
</td>
  </tr>
  </table>
  </td>
  </tr>
 
  </table>

									
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 								
			<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${carton.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="carton.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${carton.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty carton.id}">
								<s:hidden name="carton.createdBy"/>
								<td ><s:label name="createdBy" value="%{carton.createdBy}"/></td>
							</c:if>
							<c:if test="${empty carton.id}">
								<s:hidden name="carton.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${carton.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="carton.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${carton.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty carton.id}">
							<s:hidden name="carton.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{carton.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty carton.id}">
							<s:hidden name="carton.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>					
			</div>	
					               
      			 <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return calcNetWeight();" onmouseover="return chkSelect('all');"/>   
            <s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px" />
            <input type="button" class="cssbuttonA" style="width:90px; height:24px"  value="Save And Add" onclick="saveAndAdd();" onmouseover="return chkSelect('all');" /> 
            

      <s:hidden name="description" />
    <s:hidden name="sixthDescription" />
    <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
   </s:form>   
  
<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript">
function statusValidation(targetElementStatus){
	var massage="";
	if(targetElementStatus.checked==false){
		massage="Are you sure you wish to deactivate this row?"
	}else{
		massage="Are you sure you wish to activate this row?"
	}  
	var agree=confirm(massage);
	if (agree){
		
	}else{
		if(targetElementStatus.checked==false){
			document.forms['cartonForm'].elements['carton.status'].checked=true;
			}else{
				document.forms['cartonForm'].elements['carton.status'].checked=false;
			}
	}
}

<sec-auth:authComponent componentId="module.script.form.agentScript">
	window.onload = function() { 
	 	trap();
	}
 </sec-auth:authComponent>
 
 function trap(){
	  if(document.images){
	   	for(i=0;i<document.images.length;i++){
	      	if(document.images[i].src.indexOf('nav')>0){
				document.images[i].onclick= right; 
	      		document.images[i].src = 'images/navarrow.gif';  
			}
	      }
       }
  }
		  
  function right(e) {
		//var msg = "Sorry, you don't have permission.";
	if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
	}
	if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
	}
	else return true;
  }
var form_submitted = false;

function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}
</script>  
 <script>
// A function for auto save funtionality.
function ContainerAutoSave(clickType){
 progressBarAutoSave('1');
     if ('${autoSavePrompt}' == 'No'){
	    var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	    var id1 = document.forms['cartonForm'].elements['serviceOrder.id'].value;
			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
     		   noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
     		}

			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.accounting'){
   		          noSaveAction = 'accountLineList.html?sid='+id1;
             }
			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
 		          noSaveAction = 'pricingList.html?sid='+id1;
           }
			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
    		         noSaveAction = 'containers.html?id='+id1;
             }
			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.billing'){
      	       noSaveAction = 'editBilling.html?id='+id1;
             }

			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.domestic'){
            			 noSaveAction = 'editMiscellaneous.html?id='+id1;
             }

			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.status'){
      			<c:if test="${serviceOrder.job=='RLO'}">
        	  		noSaveAction = 'editDspDetails.html?id='+id1; 
        	      </c:if>
        	   	<c:if test="${serviceOrder.job!='RLO'}">
        		  	noSaveAction =  'editTrackingStatus.html?id='+id1;
          	    </c:if>
             }

			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.ticket'){
   		       noSaveAction = 'customerWorkTickets.html?id='+id1;
             }

			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.claims'){
          	   noSaveAction = 'claims.html?id='+id1;
             }
             if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.cartons'){
          	   noSaveAction = 'cartons.html?id='+id1;
             }
             if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
          	   noSaveAction = 'vehicles.html?id='+id1;
             }
             if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
         	   noSaveAction = 'servicePartnerss.html?id='+id1;
             }
             if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.containers'){
           	  noSaveAction = 'containers.html?id='+id1;
             }

			if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
      	       noSaveAction = 'editCustomerFile.html?id='+cidVal;
             }
             processAutoSave(document.forms['cartonForm'], 'saveCarton!saveOnTabChange.html', noSaveAction);
    }
	else{
	if(!(clickType == 'save')){
    var id1 = document.forms['cartonForm'].elements['serviceOrder.id'].value;
    var jobNumber = document.forms['cartonForm'].elements['serviceOrder.shipNumber'].value;

    if (document.forms['cartonForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='cartonDetail.heading'/>");
        if(agree){
            document.forms['cartonForm'].action = 'saveCarton!saveOnTabChange.html';
            document.forms['cartonForm'].submit();
        }else{
            if(id1 != ''){
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
                	var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
        }
        }
    }else{
    if(id1 != ''){

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                location.href = 'containers.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }
                if(document.forms['cartonForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
                	var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
    }
    }
}
}
}

function changeStatus(){
    document.forms['cartonForm'].elements['formStatus'].value = '1';
}
// End of function.
</script>
    
<script type="text/javascript">
// A function for calculate weight,volume and density .
function calculate(weightUnit,volumeUnit) {
    var weightUnitKgs = document.forms['cartonForm'].elements['carton.unit2'].value;
    var volumeUnitCbm = document.forms['cartonForm'].elements['carton.unit3'].value ;
    var weightUnitKgs1 = document.forms['cartonForm'].elements['carton.unit1'].value ;
	var mode=document.forms['cartonForm'].elements['serviceOrder.mode'].value
	var netWeight=eval(document.forms['cartonForm'].elements['carton.netWeight'].value);
	var grossWeight= eval(document.forms['cartonForm'].elements['carton.grossWeight'].value);
	var netWeightKilo=eval(document.forms['cartonForm'].elements['carton.netWeightKilo'].value);
	var grossWeightKilo= eval(document.forms['cartonForm'].elements['carton.grossWeightKilo'].value);
	var density=0;
	var factor = 0;
	var factor1 = 0;
	if(volumeUnitCbm == 'Inches')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 0.0005787;
		}
	}
	if(volumeUnitCbm == 'Ft')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 1;
		}
	}
	if(volumeUnitCbm == 'Cm')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 0.0000353;
		}
	}
	if(volumeUnitCbm == 'Mtr')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 35.3147;
		}
	}
	if(volumeUnitCbm == 'Inches')
	{
		if(weightUnitKgs == 'Cbm')
		{
			factor1 = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft')
	{
		if(weightUnitKgs == 'Cbm')
		{
			factor1 = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm')
	{
		if(weightUnitKgs == 'Cbm')
		{
			factor1 = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr')
	{
		if(weightUnitKgs == 'Cbm')
		{
			factor1 = 1;
		}
	}
	var density = (document.forms['cartonForm'].elements['carton.width'].value * document.forms['cartonForm'].elements['carton.height'].value * document.forms['cartonForm'].elements['carton.length'].value );
		if(weightUnitKgs == 'Cbm'){
		document.forms['cartonForm'].elements['carton.volume'].value = Math.round(density*factor1*35.3147*1000)/1000;
		var volume = document.forms['cartonForm'].elements['carton.volume'].value;
		document.forms['cartonForm'].elements['carton.volumeCbm'].value = Math.round(density*factor1*1000)/1000;
		var volumeConvertedCbm = document.forms['cartonForm'].elements['carton.volumeCbm'].value;
		}
		if(weightUnitKgs == 'Cft'){
		document.forms['cartonForm'].elements['carton.volume'].value = Math.round(density*factor*1000)/1000;
		var volume = document.forms['cartonForm'].elements['carton.volume'].value;
		document.forms['cartonForm'].elements['carton.volumeCbm'].value = Math.round(density*factor*0.0283168*1000)/1000;
		var volumeConvertedCbm = document.forms['cartonForm'].elements['carton.volumeCbm'].value;
		}
	
	if(netWeight==undefined||netWeight==0||grossWeight==undefined||grossWeight==0||volume==undefined||volume==0 )
	    {
		  document.forms['cartonForm'].elements['carton.density'].value=0;
		}  
	else
	 {
	  if(mode=='Air')
	   {
		  density=grossWeight/volume;
		  document.forms['cartonForm'].elements['carton.density'].value=Math.round(density*100)/100;
	   }
	  else
	   {
		   density=netWeight/volume;
		   document.forms['cartonForm'].elements['carton.density'].value=Math.round(density*100)/100;
	   }
	 }
	 
	 if(netWeightKilo==undefined || netWeightKilo==0 || grossWeightKilo==undefined || grossWeightKilo==0 || volumeConvertedCbm==undefined||volumeConvertedCbm==0)
	    { 
		  document.forms['cartonForm'].elements['carton.densityMetric'].value=0;
		}  
	else
	 {
	  if(mode=='Air')
	   {  
		  densityMetric=grossWeightKilo/volumeConvertedCbm;
		  document.forms['cartonForm'].elements['carton.densityMetric'].value=Math.round(densityMetric*100)/100;
	   }
	  else
	   {
	       densityMetric=netWeightKilo/volumeConvertedCbm;
		   document.forms['cartonForm'].elements['carton.densityMetric'].value=Math.round(densityMetric*100)/100;
	   }
	 }
 }
 function calcNetWeight(){
   if(document.forms['cartonForm'].elements['carton.grossWeight'].value=='')
   {
     document.forms['cartonForm'].elements['carton.grossWeight'].value=0;
   } 
   if(document.forms['cartonForm'].elements['carton.emptyContWeight'].value=='')
   {
     document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
   } 
   var Q1 = eval(document.forms['cartonForm'].elements['carton.grossWeight'].value);
   var Q2 = eval(document.forms['cartonForm'].elements['carton.emptyContWeight'].value);

   var E1='';
	if(Q1<Q2)
	{
		alert("Grossweight should be greater than Tareweight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E2;
		var E3 = Q1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
		document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=E4;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E4;
		
	}
	else if(Q1 != undefined && Q2 != undefined)
			{
			E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E2;
		var E3 = E1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E4;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=Q4;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=Q6;
			}
			else if(Q1 == undefined && Q2 == undefined)
			{
			
			}
			else if(Q2 == undefined)
			{
			E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['cartonForm'].elements['carton.netWeight'].value=E2;
				document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E4;
			}
			else if(Q1 == undefined)
			{
			alert("Grossweight should be greater than Tareweight");
			document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
			document.forms['cartonForm'].elements['carton.netWeight'].value=0;
			}
}

function calcTare()
{
if(document.forms['cartonForm'].elements['carton.emptyContWeight'].value=='')
{
document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
}
if(document.forms['cartonForm'].elements['carton.grossWeight'].value=='')
{
document.forms['cartonForm'].elements['carton.grossWeight'].value=0;
}
var Q1 = eval(document.forms['cartonForm'].elements['carton.grossWeight'].value);
var Q2 = eval(document.forms['cartonForm'].elements['carton.emptyContWeight'].value);
var Q3 = eval(document.forms['cartonForm'].elements['carton.netWeight'].value);
var E1='';
	if(Q1<Q3)
	{
		alert("Gross weight should be greater than net weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E2;
		var E3 = Q1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E4;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
	}
	else if(Q1 != undefined && Q3 != undefined)
	{
		E1=Q1-Q3;
		var E2=Math.round(E1*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=E2;
		var E3 = E1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=E4;
		var E5 = Q3*0.4536;
		var E6=Math.round(E5*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E6;
	}
	else if(Q3 == undefined)
			{
			document.forms['cartonForm'].elements['carton.grossWeight'].value=0;
			document.forms['cartonForm'].elements['carton.netWeightKilo'].value=0;
			document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
			document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
			document.forms['cartonForm'].elements['carton.netWeight'].value=0;
			document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=0;
			}
	
}
// End of function.


function calcNetWeightKilo(){
// function for calculating the net weight on changing the gross weight and tare weight.
if(document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=='')
{
document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
}
if(document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=='')
{
document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=0;
}
var Q1 = eval(document.forms['cartonForm'].elements['carton.grossWeightKilo'].value);
var Q2 = eval(document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value);

var E1='';
	if(Q1<Q2)
	{
		alert("Gross weight should be greater than tare weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E2;
		var E3 = Q1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
		document.forms['cartonForm'].elements['carton.grossWeight'].value=E4;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E4;
	}
	else
	{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E2;
		var E3 = E1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E4;
		var Q3 = Q1*2.2046;
		var Q4=Math.round(Q3*10000)/10000;
		document.forms['cartonForm'].elements['carton.grossWeight'].value=Q4;
		var Q5 = Q2*2.2046;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=Q6;
	}
}
function calcTareKilo()
{
if(document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=='')
{
document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
}
if(document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=='')
{
document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=0;
}
var Q1 = eval(document.forms['cartonForm'].elements['carton.grossWeightKilo'].value);
var Q2 = eval(document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value);
var Q3 = eval(document.forms['cartonForm'].elements['carton.netWeightKilo'].value);
var E1='';
	if(Q1<Q3)
	{
		alert("Gross weight should be greater than net weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeightKilo'].value=E2;
		var E3 = Q1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E4;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
	}
	else if(Q1 != undefined && Q3 != undefined)
	{
		E1=Q1-Q3;
		var E2=Math.round(E1*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=E2;
		var E3 = E1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=E4;
		var E5 = Q3*2.2046;
		var E6=Math.round(E5*10000)/10000;
		document.forms['cartonForm'].elements['carton.netWeight'].value=E6;
	}else if(Q3 == undefined)
			{
			document.forms['cartonForm'].elements['carton.grossWeight'].value=0;
			document.forms['cartonForm'].elements['carton.netWeightKilo'].value=0;
			document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=0;
			document.forms['cartonForm'].elements['carton.emptyContWeight'].value=0;
			document.forms['cartonForm'].elements['carton.netWeight'].value=0;
			document.forms['cartonForm'].elements['carton.grossWeightKilo'].value=0;
			}
	
}
// End of function.
</script>
<script type="text/javascript">
function saveAndAdd(){
	calcNetWeight();
	var url = "saveAddcartonForm.html";
	document.forms['cartonForm'].action = url;
	document.forms['cartonForm'].submit();
}
</script>
<script language="JavaScript">
// function for validations.
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==67) || (keyCode==86); 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36) || (keyCode==110) || (keyCode==67) || (keyCode==86); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="0";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
     return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	var f = document.getElementById('cartonForm'); 
		f.setAttribute("autocomplete", "off");
	}
	// End of function.
	function notExists(){
	alert("The Carton information has not been saved yet.");
}
// End of function.
// function for length,width,height net weight,gross weight and tare weight validation.
function chkSelect(toDo)
   {
         if(toDo == 'calc')
         {
         	if (checkFloat('cartonForm','carton.length','Invalid data in Length') == false)
           {
              document.forms['cartonForm'].elements['carton.length'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.width','Invalid data in Width') == false)
           {
              document.forms['cartonForm'].elements['carton.width'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.height','Invalid data in Height') == false)
           {
              document.forms['cartonForm'].elements['carton.height'].focus();
              return false
           }
         }
         else
         {
        	if (checkFloat('cartonForm','carton.length','Invalid data in Length') == false)
           {
              document.forms['cartonForm'].elements['carton.length'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.width','Invalid data in Width') == false)
           {
              document.forms['cartonForm'].elements['carton.width'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.height','Invalid data in Height') == false)
           {
              document.forms['cartonForm'].elements['carton.height'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.volume','Invalid data in Volume') == false)
           {
              document.forms['cartonForm'].elements['carton.length'].value='0';
              document.forms['cartonForm'].elements['carton.width'].value='0';
              document.forms['cartonForm'].elements['carton.height'].value='0';
              document.forms['cartonForm'].elements['carton.volume'].value='0';
              
              document.forms['cartonForm'].elements['carton.volume'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.grossWeight','Invalid data in GrossWeight') == false)
           {
              document.forms['cartonForm'].elements['carton.grossWeight'].value='0'; 
              document.forms['cartonForm'].elements['carton.grossWeight'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.emptyContWeight','Invalid data in Tare') == false)
           {
              document.forms['cartonForm'].elements['carton.emptyContWeight'].value='0';
              document.forms['cartonForm'].elements['carton.emptyContWeight'].focus();
              return false
           }
           if (checkFloat('cartonForm','carton.netWeight','Invalid data in NetWeight') == false)
           {
              document.forms['cartonForm'].elements['carton.netWeight'].value='0';
              document.forms['cartonForm'].elements['carton.netWeight'].focus();
              return false
           }
           if(document.forms['cartonForm'].elements['carton.grossWeight'].value=='.')
           {
           document.forms['cartonForm'].elements['carton.grossWeight'].value='0';
           }
           if(document.forms['cartonForm'].elements['carton.emptyContWeight'].value=='.')
           {
           document.forms['cartonForm'].elements['carton.emptyContWeight'].value='0';
           }
           }
   }
function changeCalOpenarvalue()
		  {
		  	document.forms['cartonForm'].elements['calOpener'].value='open';
		  }
// End of function.

function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['cartonForm'].elements['carton.unit1'].value='Lbs';
  document.forms['cartonForm'].elements['carton.unit2'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['cartonForm'].elements['carton.unit1'].value='Kgs';
  document.forms['cartonForm'].elements['carton.unit2'].value='Cbm';
  }
} 
// End of function.

function checkUnilPopulate(){
if(document.forms['cartonForm'].elements['calculatecheck'].value=="Ok"){
  var t = document.forms['cartonForm'].elements['carton.unit2'].value;
  var volume = document.forms['cartonForm'].elements['carton.volume'].value;
  var emptyContWeight = document.forms['cartonForm'].elements['carton.emptyContWeight'].value;
  if(t == 'Cbm'){
		document.forms['cartonForm'].elements['carton.volumeCbm'].value=volume;
		document.forms['cartonForm'].elements['carton.volume'].value = Math.round(volume*35.3147*1000)/1000;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=emptyContWeight;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=Math.round(emptyContWeight*2.2046*1000)/1000;
		}
		if(t == 'Cft'){
		document.forms['cartonForm'].elements['carton.volume'].value = volume;
		document.forms['cartonForm'].elements['carton.volumeCbm'].value = Math.round(volume*0.0283*1000)/1000;
		document.forms['cartonForm'].elements['carton.emptyContWeight'].value=emptyContWeight;
		document.forms['cartonForm'].elements['carton.emptyContWeightKilo'].value=Math.round(emptyContWeight*0.4536*1000)/1000;
		}
  if(t=='Cbm'){
  document.getElementById('weight2').checked=true;
  document.forms['cartonForm'].elements['carton.unit1'].value='Kgs';
  document.forms['cartonForm'].elements['carton.unit2'].value='Cbm';
  }
  else{
  document.getElementById('weight1').checked=true;
  document.forms['cartonForm'].elements['carton.unit1'].value='Lbs';
  document.forms['cartonForm'].elements['carton.unit2'].value='Cft';
  }
} 
document.forms['cartonForm'].elements['calculatecheck'].value="";
}
function dataPopulate(){
openWindow('refCrates.html?decorator=popup&popup=true&fld_sixthDescription=carton.unit3&fld_seventhDescription=carton.unit2&fld_fifthDescription=carton.volume&fld_fourthDescription=carton.height&fld_thirdDescription=carton.width&fld_secondDescription=carton.length&fld_description=carton.emptyContWeight&fld_code=carton.cartonType');
document.forms['cartonForm'].elements['calculatecheck'].value="Ok";
document.forms['cartonForm'].elements['carton.cartonType'].select();
}
var http5 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['cartonForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['cartonForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['cartonForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['cartonForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'cartons.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['cartonForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['cartonForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrl(id)
	{
		location.href = "cartons.html?id="+id;
	}
function goPrevChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['cartonForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['cartonForm'].elements['carton.id'].value;
	var url="cartonPrev.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function goNextChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['cartonForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['cartonForm'].elements['carton.id'].value;
	var url="cartonNext.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShipChild(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editCarton.html?id='+results;
             }
       }     
function findCustomerOtherSOChild(position) {
 var sidNum=document.forms['cartonForm'].elements['serviceOrder.id'].value;
 var soIdNum=document.forms['cartonForm'].elements['carton.id'].value;
 var url="cartonSO.html?ajax=1&decorator=simple&popup=true&sidNum=" + encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrlChild(id)
	{
		location.href = "editCarton.html?id="+id;
	}
	
function copyCType() {	
  	document.getElementById('description').value= document.getElementById('cartonType').value;    
}
</script> 
<script type="text/javascript">
	try{  	
  		document.getElementById('idNum').focus();
  	}catch(e){}
</script>
<%-- Shifting Closed Here --%>