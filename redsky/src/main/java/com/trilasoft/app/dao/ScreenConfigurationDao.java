package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.ScreenConfiguration;

public interface ScreenConfigurationDao extends GenericDao<ScreenConfiguration, Long> {   
      public List checkById(Long id);
      public List<ScreenConfiguration>searchByCorpIdTableAndField(String corpId,String tableName,String fieldName) ;
}