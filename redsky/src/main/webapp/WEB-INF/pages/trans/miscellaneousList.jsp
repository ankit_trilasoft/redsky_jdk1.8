<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="servicePartnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='servicePartnerList.heading'/>"/>   
</head>  
<c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/> 
<%-- 
<s:hidden name="agent.id" value="<%=request.getParameter("id")%>"/>  
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>

--%>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/> 


<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" />ServiceOrder</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td> 
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editBilling.html?id=<%=request.getParameter("id")%>"/>Bill</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="servicePartners.html?id=${serviceOrder.id}" >Partner</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="containers.html?id=<%=request.getParameter("id")%>"/>Pack</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab">Date</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab">Long</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editInternational.html?id=<%=request.getParameter("id")%>"/>Status</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="customerWorkTickets.html?id=${serviceOrder.id}" >Ticket</a></td>
<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="costings.html?id=${serviceOrder.id}" >Invoicing</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="claims.html?id=${serviceOrder.id}"/>Claim</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editCustomerFile.html?id=${customerFile.id}"/>CustomerFile</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
</tr>
</tbody>
</table>

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		  	<tr><td align="left" class="listwhite"><fmt:message key="billing.shipper"/></td><td colspan="2" align="left" class="listwhite"><s:textfield name="serviceOrder.firstName" size="15" readonly="true" onfocus="calculate();"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.lastName" size="13" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.originCity" size="10" readonly="true"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.originCountryCode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.job"   size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.commodity"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.commodity"  size="6" readonly="true"/></td><td align="left" class="listwhite"><fmt:message key="billing.routing"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.routing"   size="1" readonly="true"/></td></tr>
		  	<tr><td align="left" class="listwhite"></td></tr>
		  	<tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		  <tr><td align="left" class="listwhite"></td></tr>
		 <tr><td align="right" class="listwhite"><fmt:message key="billing.jobNo"/></td><td align="left" class="listwhite"><s:textfield name="customerFileNumber" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="left" class="listwhite"><fmt:message key="billing.registrationNo"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.registrationNumber"  size="13" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.destination"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.destinationCity" size="10" readonly="true"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.destinationCountryCode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.mode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="serviceOrder.billToName"/></td><td colspan="3" align="left" class="listwhite"><s:textfield name="serviceOrder.billToName"   size="20" readonly="true"/></td></tr>
	</tbody>

 </table>


 

<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Partner List</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
				
			</tr>
   </tbody>
   </table>   
   </div>
   
<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editServicePartner.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
       
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>  
  
<s:set name="servicePartners" value="servicePartners" scope="request"/>   
<display:table name="servicePartners" class="table" requestURI="" id="servicePartnerList"  export="true"  defaultsort="1" pagesize="10">   
    
    <display:column property="carrierNumber" sortable="true" titleKey="servicePartner.carrierNumber" href="editServicePartner.html"    
        paramId="id" paramProperty="id"/>   
    <display:column property="partnerType" sortable="true" titleKey="servicePartner.partnerType"/>    
    <display:column property="carrierCode" sortable="true" titleKey="servicePartner.carrierCode"/>
    <display:column property="carrierName" sortable="true" titleKey="servicePartner.carrierName"/>
    <display:column property="city" sortable="true" titleKey="servicePartner.city"/>
    <display:column property="carrierDeparture" sortable="true" titleKey="servicePartner.carrierDeparture"/>
    <display:column property="carrierTD" sortable="true" titleKey="servicePartner.carrierTD" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="carrierArrival" sortable="true" titleKey="servicePartner.carrierArrival"/>
    <display:column property="carrierTA" sortable="true" titleKey="servicePartner.carrierTA" format="{0,date,dd-MMM-yyyy}"/>
    </display:table>   
  
  <c:out value="${buttons}" escapeXml="false" />
    
<script type="text/javascript">   
    highlightTableRows("servicePartnerList");   




</script>  