package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.jsp.PageContext;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.model.AgentBaseSCAC;
import com.trilasoft.app.model.AgentRequest;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.FrequentlyAskedQuestions;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.PartnerBankInformation;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PolicyDocument;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.service.AccountContactManager;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.AgentBaseManager;
import com.trilasoft.app.service.AgentBaseSCACManager;
import com.trilasoft.app.service.AgentRequestManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractPolicyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.FrequentlyAskedQuestionsManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PartnerVanLineRefManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RefZipGeoCodeMapManager;
import com.trilasoft.app.service.PartnerBankInfoManager;

import java.net.URL;
public class PartnerPublicAction extends BaseAction implements Preparable{

	private Long id;

	private Set partnerPublicList;
	private Set agentList;

	private PartnerPublic partnerPublic;
	private AgentRequest agentRequest;
	
	private FrequentlyAskedQuestionsManager frequentlyAskedQuestionsManager;

	private PartnerPrivate partnerPrivate;

	private PartnerPublic partner;

	private PartnerPublicManager partnerPublicManager;

	private PartnerPrivateManager partnerPrivateManager;

	private RefMasterManager refMasterManager;

	private AgentBaseManager agentBaseManager;

	private AgentBaseSCACManager agentBaseSCACManager;

	private PartnerVanLineRefManager partnerVanLineRefManager;
	
	private PartnerAccountRefManager partnerAccountRefManager;
	
	private PartnerAccountRef partnerAccountRef;
	
	private AccountContactManager accountContactManager;
	
	private ContractPolicyManager contractPolicyManager;
	
	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;

	private String hitflag;
	
	private String isDecaExtract;

	private Map<String, String> partnerStatus;

	private Map<String, String> actionType;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();

	private String countryCodeSearch;

	private String stateSearch;

	private String countrySearch;

	private String citySearch;

	private String partnerType;
	
	private Map<String, String> countryDesc;

	private List multiplequalityCertifications;

	private List multiplVanlineAffiliations;

	private List multiplServiceLines;
	private List ownerOperatorList = new ArrayList();
	private Map<String, String> bstates;

	private Map<String, String> tstates;

	private Map<String, String> mstates;

	private CustomerFileManager customerFileManager;

	private PartnerManager partnerManager;
	private Company company;
	private CompanyManager companyManager;
	private Map<String, String> typeOfVendor;
	
	private Map<String, String> corp;

	private Map<String, String> vanlineAffiliations;

	private Map<String, String> serviceLines;

	private Map<String, String> qualityCertifications;
	
	private Map<String, String> fleet; 
	private Set partners;
	private File file;

	private File file1;

	private File file2;

	private File file3;

	private String fileFileName;

	private String file1FileName;

	private String file2FileName;

	private String file3FileName;

	private String popupval;

	private String exList;

	private List partnerListNew;

	private String flag;

	private String formClose;
	private String parentId;//Enhancement for ControlExpirations
	private String paramView = "";

	private AccountProfile accountProfile;
	 private String agentCountryCode;
	private AccountProfileManager accountProfileManager;

	private String destination;

	private String origin;

	private List companyDivis = new ArrayList();
	
	private String partnerCodeCount;
	
	private String compDiv = "";

	private String userCheck;
	private String partnerCorpid;
	private String userCheckMaster;
	private String jobRelo;
	private String logQuery = "";
	private Set<Role> userRole;
	private String userType ;
	private Map<String, String> creditTerms;
	private Boolean isIgnoreInactive = false; 
	private Map<String,String> driverAgencyList;
	private Map<String, String> paytype;
    private Map<String, String> serviceRelos1= new HashMap<String, String>();
    private Map<String, String>  serviceRelos;
	private Map<String, String> serviceRelos2= new HashMap<String, String>(); 
	private Map<String, String> driverType;
    private List donotMergelist;
	private String donoFlag;
	private NotesManager notesManager;
	private String partnerNotes;
	private DataSecurityFilterManager dataSecurityFilterManager;
	private DataSecuritySetManager dataSecuritySetManager;
	private DataSecuritySet dataSecuritySet;
	private DataSecurityFilter dataSecurityFilter;
	private Boolean checkTransfereeInfopackage;
    private String tempTransPort;
    private String usertype; 
    private String validVatCode;
    private String billingCountry;
    private Map<String,String> bankCode;
    private String enbState;
    private Map<String, String> countryCod;
    private String chkAgentTrue;
    private Date partnerPrivateHire;
    private Date partnerPrivateTermination;
    private String vanlineCodeSearch;
    private Map<String, String> countryWithBranch;
    private String flagHit;
    private  Map<String, String> planCommissionList;
    Date currentdate = new Date();
    private boolean cmmdmmflag=false;
    private String searchListOfVendorCode;
    private Map<String, String> listOfVendorCode;
    private  Map<String, String> vatBillingGroups = new LinkedHashMap<String, String>();
    private String compDivFlag;
    private String bankNoList;
    private String currencyList;
    private String checkList;
    private String newlineid;
    private PartnerBankInformation partnerBankInformation;
    private PartnerBankInfoManager partnerBankInfoManager;
    private List partnerBankInfoList;
    private String bankPartnerCode;
    private String partnerbanklist;
    private String loginCorpId;
    private String checkCodefromDefaultAccountLine;
    private Integer compensationYear;
    private boolean checkFieldVisibility=false;
    private String checkCompensationYear="";
    private int parentCompensationYear=0;
    private AgentRequestManager agentRequestManager;
    private String counter;
    private File  fileLogo;
    private String fileLogoUploadFlag;
    private String fileLogoFileName;
    private String imageDataFileLogo;
    private String oiJobList;
    
	static final Logger logger = Logger.getLogger(PartnerPublicAction.class);
    public void prepare() throws Exception {
		enbState = customerFileManager.enableStateList(sessionCorpID);
		countryWithBranch=partnerPublicManager.getCountryWithBranch("COUNTRY",sessionCorpID);
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
		cmmdmmflag = true;
		}
		compDivFlag=company.getCompanyDivisionFlag();
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
			oiJobList=company.getOiJob();  
		  }
	}
    
    public String saveOnTabChange() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
		String s = save();
		validateFormNav = "OK";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}

	public PartnerPublicAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
		userRole = user.getRoles();
		userType=user.getUserType();
		loginCorpId= user.getCorpID();
	}

	@SkipValidation
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		getRequest().setAttribute("soLastName","");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}

		getComboList(sessionCorpID);
		partnerPublic = new PartnerPublic();
		partnerPublic.setIsAccount(true);
		partnerPublic.setIsPrivateParty(true);
		partnerPublic.setIsVendor(true);
		partnerPublic.setIsAgent(true);
		partnerPublic.setIsCarrier(true);
		partnerPublic.setIsOwnerOp(true);
		isIgnoreInactive = true;
		partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicList("", sessionCorpID, "", "", "","", "", "", "", "", false, false, false, false, false, false, true, "","","",cmmdmmflag,"","","","","","","","",""));
		//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String getVatCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		validVatCode= partnerPublicManager.getCountryCode(billingCountry,"COUNTRY",sessionCorpID);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String jobReloName;
//	 Method to search partner
	List partnerReloList=new ArrayList();
	@SkipValidation
	public String searchPartnerRelo() { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		if((jobReloName!=null)&&(!jobReloName.equalsIgnoreCase(""))){
			jobReloName=jobReloName.replaceAll("_", "");
		}else{
			jobReloName="";
		}
		try { 
			String lastName;
			String partnerCode;
			String billingCountryCode;
			String billingState;
			String billingCountry;
			listOfVendorCode = refMasterManager.findByParameter(sessionCorpID, "TYPEOFVENDOR");
			if (partnerPublic != null) {
				if (partnerPublic.getLastName() == null) {
					lastName = "";
				} else {
					lastName = partnerPublic.getLastName();
				}
				if (partnerPublic.getPartnerCode() == null) {
					partnerCode = "";
				} else {
					partnerCode = partnerPublic.getPartnerCode();
				}
				if (partnerPublic.getTerminalCountryCode() == null) {
					billingCountryCode = "";
				} else {
					billingCountryCode = partnerPublic.getTerminalCountryCode();
				}	
				if (partnerPublic.getTerminalState() == null) {
					billingState = "";
				} else {
					billingState = partnerPublic.getTerminalState();
				}
				if (partnerPublic.getTerminalCountry() == null) {
					billingCountry = "";
				} else {
					billingCountry = partnerPublic.getTerminalCountry();
				}
				if (searchListOfVendorCode == null) {
					searchListOfVendorCode = "";
				}				
				partnerReloList=partnerManager.findPartnerRelo(lastName,partnerCode,billingCountryCode,billingState,billingCountry,jobRelo,jobReloName,searchListOfVendorCode,sessionCorpID,cmmdmmflag);
			}else{
				partnerReloList=partnerManager.findPartnerRelo("","","","","",jobRelo,jobReloName,"",sessionCorpID,cmmdmmflag);
			}
		}catch(Exception e){
			partnerReloList=partnerManager.findPartnerRelo("","","","","",jobRelo,jobReloName,"",sessionCorpID,cmmdmmflag);
			 e.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	@SkipValidation
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		List notePartner = notesManager.countForPartnerNotes(partnerPublic.getPartnerCode());
		
		if (notePartner.isEmpty()) {
			partnerNotes = "0";
		} else {
			if ((notePartner.get(0)) == "0") {
				partnerNotes = "0";
			} else {
				partnerNotes = ((notePartner).get(0)).toString();
			}
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String partnerPublicSearchByPartnerView() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}

		getComboList(sessionCorpID);
		try {
			String firstName;
			String lastName;
			String aliasName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String status;
			String extRefernece;

			if (partnerPublic != null) {
				firstName = partnerPublic.getFirstName() == null ? "" : partnerPublic.getFirstName();
				lastName = partnerPublic.getLastName() == null ? "" : partnerPublic.getLastName();
				partnerCode = partnerPublic.getPartnerCode() == null ? "" : partnerPublic.getPartnerCode();
				aliasName=partnerPublic.getAliasName()==null ? "" :partnerPublic.getAliasName();
				billingCountryCode = countryCodeSearch == null ? "" : countryCodeSearch;
				billingStateCode = stateSearch == null ? "" : stateSearch;
				billingCountry = countrySearch == null ? "" : countrySearch;
				status = partnerPublic.getStatus() == null ? "" : partnerPublic.getStatus();
				
				if (partnerPublic.getIsAccount() == null) {
					partnerPublic.setIsAccount(false);
				}
				if (partnerPublic.getIsAgent() == null) {
					partnerPublic.setIsAgent(false);
				}
				if (partnerPublic.getIsCarrier() == null) {
					partnerPublic.setIsCarrier(false);
				}
				if (partnerPublic.getIsOwnerOp() == null) {
					partnerPublic.setIsOwnerOp(false);
				}
				if (partnerPublic.getIsPrivateParty() == null) {
					partnerPublic.setIsPrivateParty(false);
				}
				if (partnerPublic.getIsVendor() == null) {
					partnerPublic.setIsVendor(false);
				}
				
				if (isIgnoreInactive == null) {
					isIgnoreInactive = false;
				}
				
				extRefernece = partnerPrivate.getExtReference() == null ? "" : partnerPrivate.getExtReference();

				partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicListByPartnerView(partnerType, sessionCorpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, partnerPublic.getIsPrivateParty(), partnerPublic.getIsAccount(), partnerPublic.getIsAgent(), partnerPublic.getIsVendor(), partnerPublic.getIsCarrier(), partnerPublic.getIsOwnerOp(), isIgnoreInactive, extRefernece));
			} else {
				partnerPublic = new PartnerPublic();
				partnerPublic.setIsAccount(true);
				partnerPublic.setIsPrivateParty(true);
				partnerPublic.setIsVendor(true);
				partnerPublic.setIsAgent(true);
				partnerPublic.setIsCarrier(true);
				partnerPublic.setIsOwnerOp(true);
				isIgnoreInactive = true;
				partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicListByPartnerView("", sessionCorpID, "", "", "","", "", "", "", "", true, true, true, true, true, true, true, ""));
			}
		} catch (Exception e) {
			partnerPublic = new PartnerPublic();
			partnerPublic.setIsAccount(true);
			partnerPublic.setIsPrivateParty(true);
			partnerPublic.setIsVendor(true);
			partnerPublic.setIsAgent(true);
			partnerPublic.setIsCarrier(true);
			partnerPublic.setIsOwnerOp(true);
			isIgnoreInactive = true;
			partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicListByPartnerView("", sessionCorpID, "", "", "", "", "","", "", "", false, false, false, false, false, false, false, ""));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	 private String paramValue;
	@SkipValidation
	public String agentPublicSearch() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getRequest().setAttribute("soLastName","");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		if(paramValue!=null && paramValue.equalsIgnoreCase("View")) {
			getRequest().getSession().setAttribute("paramView", paramValue);	
		}
		if (paramView.equalsIgnoreCase("View")) {
			getRequest().getSession().setAttribute("paramView", paramView);
		} 

		getComboList(sessionCorpID);
		try {
			String firstName;
			String lastName;
			String aliasName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String status;
			String extRefernece;
			String vanlineCode;	
			String typeOfVendor;

			if (agentRequest != null) {
				
				lastName = agentRequest.getLastName() == null ? "" : agentRequest.getLastName();			
				partnerCode = agentRequest.getPartnerCode() == null ? "" : agentRequest.getPartnerCode();
				aliasName=agentRequest.getAliasName()==null ? "" :agentRequest.getAliasName();
				billingCountryCode = countryCodeSearch == null ? "" : countryCodeSearch;
				billingStateCode = stateSearch == null ? "" : stateSearch;
				billingCountry ="";
				status = agentRequest.getStatus() == null ? "" : agentRequest.getStatus();
				//vanlineCode = vanlineCodeSearch == null? "" :vanlineCodeSearch;
				//typeOfVendor=agentRequest.getTypeOfVendor()==null?"":agentRequest.getTypeOfVendor();
				
				if (agentRequest.getIsAgent() == null) {
					agentRequest.setIsAgent(false);
				}
				
				
			//	extRefernece = partnerPrivate.getExtReference() == null ? "" : partnerPrivate.getExtReference();

				agentList = new LinkedHashSet(agentRequestManager.getAgentRequestList(partnerCode, sessionCorpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, agentRequest.getIsAgent(),isIgnoreInactive));
			} else {
				agentRequest = new AgentRequest();
				
				agentRequest.setIsAgent(true);
				
				isIgnoreInactive = true;
				agentList = new LinkedHashSet(agentRequestManager.getAgentRequestList("", sessionCorpID, "", "", "", "", "", "", "",false, isIgnoreInactive));
				}
		} catch (Exception e) {
			agentRequest = new AgentRequest();
		
			agentRequest.setIsAgent(true);
		
			isIgnoreInactive = true;
			agentList = new LinkedHashSet(agentRequestManager.getAgentRequestList("", sessionCorpID, "", "", "", "", billingCountry, "", "", agentRequest.getIsAgent(), isIgnoreInactive));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String partnerPublicSearch() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getRequest().setAttribute("soLastName","");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}    

		getComboList(sessionCorpID);
		try {
			String firstName;
			String lastName;
			String aliasName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String status;
			String extRefernece;
			String vanlineCode;	
			String typeOfVendor;

			if (partnerPublic != null) {
				firstName = partnerPublic.getFirstName() == null ? "" : partnerPublic.getFirstName();
				lastName = partnerPublic.getLastName() == null ? "" : partnerPublic.getLastName();			
				partnerCode = partnerPublic.getPartnerCode() == null ? "" : partnerPublic.getPartnerCode();
				aliasName=partnerPublic.getAliasName()==null ? "" :partnerPublic.getAliasName();
				billingCountryCode = countryCodeSearch == null ? "" : countryCodeSearch;
				billingStateCode = stateSearch == null ? "" : stateSearch;
				billingCountry = countrySearch == null ? "" : countrySearch;
				status = partnerPublic.getStatus() == null ? "" : partnerPublic.getStatus();
				vanlineCode = vanlineCodeSearch == null? "" :vanlineCodeSearch;
				typeOfVendor=partnerPublic.getTypeOfVendor()==null?"":partnerPublic.getTypeOfVendor();
				if (partnerPublic.getIsAccount() == null) {
					partnerPublic.setIsAccount(false);
				}
				if (partnerPublic.getIsAgent() == null) {
					partnerPublic.setIsAgent(false);
				}
				if (partnerPublic.getIsCarrier() == null) {
					partnerPublic.setIsCarrier(false);
				}
				if (partnerPublic.getIsOwnerOp() == null) {
					partnerPublic.setIsOwnerOp(false);
				}
				if (partnerPublic.getIsPrivateParty() == null) {
					partnerPublic.setIsPrivateParty(false);
				}
				if (partnerPublic.getIsVendor() == null) {
					partnerPublic.setIsVendor(false);
				}
				
				if (isIgnoreInactive == null) {
					isIgnoreInactive = false;
				}
				
				extRefernece = partnerPrivate.getExtReference() == null ? "" : partnerPrivate.getExtReference();

				partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicList(partnerType, sessionCorpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, partnerPublic.getIsPrivateParty(), partnerPublic.getIsAccount(), partnerPublic.getIsAgent(), partnerPublic.getIsVendor(), partnerPublic.getIsCarrier(), partnerPublic.getIsOwnerOp(), isIgnoreInactive, extRefernece,vanlineCode,typeOfVendor,cmmdmmflag,partnerPublic.getFidiNumber(),partnerPublic.getOMNINumber(),partnerPublic.getAMSANumber(),partnerPublic.getWERCNumber(),partnerPublic.getIAMNumber(),partnerPublic.getUtsNumber(),partnerPublic.getEurovanNetwork(),partnerPublic.getPAIMA(),partnerPublic.getLACMA()));
			} else {
				partnerPublic = new PartnerPublic();
				partnerPublic.setIsAccount(true);
				partnerPublic.setIsPrivateParty(true);
				partnerPublic.setIsVendor(true);
				partnerPublic.setIsAgent(true);
				partnerPublic.setIsCarrier(true);
				partnerPublic.setIsOwnerOp(true);
				isIgnoreInactive = true;
				partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicList("", sessionCorpID, "", "", "","", "", "", "", "", true, true, true, true, true, true, true, "","","",cmmdmmflag,"","","","","","","","",""));
			}
		} catch (Exception e) {
			partnerPublic = new PartnerPublic();
			partnerPublic.setIsAccount(true);
			partnerPublic.setIsPrivateParty(true);
			partnerPublic.setIsVendor(true);
			partnerPublic.setIsAgent(true);
			partnerPublic.setIsCarrier(true);
			partnerPublic.setIsOwnerOp(true);
			isIgnoreInactive = true;
			partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicList("", sessionCorpID, "", "", "", "", "","", "", "", false, false, false, false, false, false, false, "","","",cmmdmmflag,"","","","","","","","",""));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String searchMergeList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getComboList(sessionCorpID);
		try {
			String firstName;
			String lastName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String status;
			String billingCity;
			String aliasName;

			if (partnerPublic != null) {
				if (partnerPublic.getFirstName() == null) {
					firstName = "";
				} else {
					firstName = partnerPublic.getFirstName();
				}
				if (partnerPublic.getLastName() == null) {
					lastName = "";
				} else {
					lastName = partnerPublic.getLastName();
				}
				if (partnerPublic.getAliasName() == null) {
					aliasName = "";
				} else {
					aliasName = partnerPublic.getAliasName();
				}
				if (partnerPublic.getPartnerCode() == null) {
					partnerCode = "";
				} else {
					partnerCode = partnerPublic.getPartnerCode();
				}
				if (countryCodeSearch == null) {
					billingCountryCode = "";
				} else {
					billingCountryCode = countryCodeSearch;
				}
				if (stateSearch == null) {
					billingStateCode = "";
				} else {
					billingStateCode = stateSearch;
				}
				if (countrySearch == null) {
					billingCountry = "";
				} else {
					billingCountry = countrySearch;
				}
				if (partnerPublic.getStatus() == null) {
					status = "";
				} else {
					status = partnerPublic.getStatus();
				}

				if (citySearch == null) {
					billingCity = "";
				} else {
					billingCity = citySearch;
				}

				if (partnerPublic.getIsAccount() == null) {
					partnerPublic.setIsAccount(false);
				}
				if (partnerPublic.getIsAgent() == null) {
					partnerPublic.setIsAgent(false);
				}
				if (partnerPublic.getIsCarrier() == null) {
					partnerPublic.setIsCarrier(false);
				}
				if (partnerPublic.getIsOwnerOp() == null) {
					partnerPublic.setIsOwnerOp(false);
				}
				if (partnerPublic.getIsPrivateParty() == null) {
					partnerPublic.setIsPrivateParty(false);
				}
				if (partnerPublic.getIsVendor() == null) {
					partnerPublic.setIsVendor(false);
				}

				partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicMergeList(partnerType, sessionCorpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, partnerPublic.getIsPrivateParty(), partnerPublic.getIsAccount(), partnerPublic.getIsAgent(), partnerPublic.getIsVendor(), partnerPublic.getIsCarrier(), partnerPublic.getIsOwnerOp(), billingCity));
			} else {
				partnerPublic = new PartnerPublic();
				partnerPublic.setIsAccount(true);
				partnerPublic.setIsPrivateParty(true);
				partnerPublic.setIsVendor(true);
				partnerPublic.setIsAgent(true);
				partnerPublic.setIsCarrier(true);
				partnerPublic.setIsOwnerOp(true);

				partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicMergeList("", sessionCorpID, "", "", "","", "", "", "", "", true, true, true, true, true, true, ""));
			}
		} catch (Exception e) {
			partnerPublic = new PartnerPublic();
			partnerPublic.setIsAccount(true);
			partnerPublic.setIsPrivateParty(true);
			partnerPublic.setIsVendor(true);
			partnerPublic.setIsAgent(true);
			partnerPublic.setIsCarrier(true);
			partnerPublic.setIsOwnerOp(true);

			partnerPublicList = new HashSet(partnerPublicManager.getPartnerPublicMergeList("", sessionCorpID, "", "", "","", "", "", "", "", false, false, false, false, false, false, ""));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	 private String justSayYes;
		private String qualitySurvey;
		private String containsData;

	private List selectedJobList;
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getComboList(sessionCorpID);
		if (paramView.equalsIgnoreCase("View")) {
			getRequest().getSession().setAttribute("paramView", paramView);
		}
		if (id != null) {
			partnerPublic = partnerPublicManager.get(id);
	    	try{
	    		selectedJobList = new ArrayList(); 
	    		String tv1 = new String();
	    		tv1 = partnerPublic.getTypeOfVendor().trim();
	    		if (tv1.indexOf(",") == 0) {
	    			tv1 = tv1.substring(1);
	    		}
	    		String[] ac = tv1.split(",");
	    		int arrayLength2 = ac.length;	
	    		for (int j = 0; j < arrayLength2; j++) {
	    			String qacCerti = ac[j];
	    			if (qacCerti.indexOf("(") == 0) {
	    				qacCerti = qacCerti.substring(1);
	    			}
	    			if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf(")") == qacCerti.length() - 1)) {
	    				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
	    			}
	    			if (qacCerti.indexOf("'") == 0) {
	    				qacCerti = qacCerti.substring(1);
	    			}
	    			if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf("'") == qacCerti.length() - 1)) {
	    				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
	    			}

	    			selectedJobList.add(qacCerti.trim());
	    		}		
	    		partnerPublic.setTypeOfVendor(selectedJobList.toString().replace("[", "").replace("]", ""));
	    		}catch(Exception e){
	    			e.printStackTrace();
	    		}
	    		
			parentId=partnerPublic.getPartnerCode();
			
			if (partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).isEmpty()) {
				partnerPrivate = new PartnerPrivate();
				partnerPrivate.setLastName(partnerPublic.getLastName());
				partnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
				partnerPrivate.setPartnerPublicId(partnerPublic.getId()); 
				partnerPrivate.setExtReference("");
				donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
				if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
					donoFlag="1";
				}
				if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
					partnerPrivate.setCorpID("VOER");
				}else{
					partnerPrivate.setCorpID(sessionCorpID);
				}
				partnerPrivate.setCompanyDivision(partnerPrivate.getCompanyDivision());
				partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
				partnerPrivate.setCreatedOn(new Date());
				partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
				partnerPrivate.setUpdatedOn(new Date());
				partnerPrivate.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
				partnerPrivate = partnerPrivateManager.save(partnerPrivate);
				try{
					if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
					createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
				    createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
					}
				    }catch (Exception e) {
						String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
				    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
				    	 e.printStackTrace(); 
					}
				
			} else {
				partnerPrivate = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).get(0);
				donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
				if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
					donoFlag="1";
				}
			}
			try {
				multiplequalityCertifications = new ArrayList();
				multiplVanlineAffiliations = new ArrayList();
				multiplServiceLines = new ArrayList();
				if(partnerPublic.getQualityCertifications()!=null && !partnerPublic.getQualityCertifications().trim().equals(""))
				{
				String[] qc = partnerPublic.getQualityCertifications().split(",");
				int arrayLength1 = qc.length;
				for (int j = 0; j < arrayLength1; j++) {
					String qacCerti = qc[j];
					if (qacCerti.indexOf("(") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf(")") == qacCerti.length() - 1)) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}
					if (qacCerti.indexOf("'") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf("'") == qacCerti.length() - 1)) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}

					multiplequalityCertifications.add(qacCerti.trim());
				}
				}
			   } catch (Exception ex) {
				ex.printStackTrace();
			  }try{
				  if(partnerPublic.getVanLineAffiliation()!=null && !partnerPublic.getVanLineAffiliation().trim().equals(""))
				  {
				String[] va = partnerPublic.getVanLineAffiliation().split(",");
				int arrayLength2 = va.length;
				for (int k = 0; k < arrayLength2; k++) {
					String vanCer = va[k];
					if (vanCer.indexOf("(") == 0) {
						vanCer = vanCer.substring(1);
					}
					if (!vanCer.equalsIgnoreCase("") && (vanCer.lastIndexOf(")") == vanCer.length() - 1)) {
						vanCer = vanCer.substring(0, vanCer.length() - 1);
					}
					if (vanCer.indexOf("'") == 0) {
						vanCer = vanCer.substring(1);
					}
					if (!vanCer.equalsIgnoreCase("") && (vanCer.lastIndexOf("'") == vanCer.length() - 1)) {
						vanCer = vanCer.substring(0, vanCer.length() - 1);
					}

					multiplVanlineAffiliations.add(vanCer.trim());
				}
				  }
			    } catch (Exception ex) {
					ex.printStackTrace();
				}
			    try{
			    	if(partnerPublic.getServiceLines()!=null && !partnerPublic.getServiceLines().trim().equals(""))
			    	{
				String[] servLi = partnerPublic.getServiceLines().split(",");
				int arrayLength3 = servLi.length;
				for (int i = 0; i < arrayLength3; i++) {
					String serLines = servLi[i];
					if (serLines.indexOf("(") == 0) {
						serLines = serLines.substring(1);
					}
					if (!serLines.equalsIgnoreCase("") && (serLines.lastIndexOf(")") == serLines.length() - 1)) {
						serLines = serLines.substring(0, serLines.length() - 1);
					}
					if (serLines.indexOf("'") == 0) {
						serLines = serLines.substring(1);
					}
					if (!serLines.equalsIgnoreCase("") && (serLines.lastIndexOf("'") == serLines.length() - 1)) {
						serLines = serLines.substring(0, serLines.length() - 1);
					}

					multiplServiceLines.add(serLines.trim());
				}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			bstates = customerFileManager.findDefaultStateList(partnerPublic.getBillingCountryCode(), sessionCorpID);
			try{
				if(partnerPrivate !=null && partnerPrivate.getCompanyDivision()!=null && (!(partnerPrivate.getCompanyDivision().toString().trim().equals("")))){
					
					if(companyDivis.contains(partnerPrivate.getCompanyDivision())){
						
					}else{
						companyDivis.add(partnerPrivate.getCompanyDivision());	
					}
					}
				}
			catch (Exception exception) {
				exception.printStackTrace();
			}
			List terminalCountryCode = partnerManager.getCountryCode(partnerPublic.getTerminalCountry());
			List mailIngCountryCode = partnerManager.getCountryCode(partnerPublic.getMailingCountry());
			try {
				if (!terminalCountryCode.isEmpty()) {
					tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
				} else {
					tstates = customerFileManager.findDefaultStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID);
				}
				if (!mailIngCountryCode.isEmpty()) {
					mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
				} else {
					mstates = customerFileManager.findDefaultStateList(partnerPublic.getMailingCountryCode(), sessionCorpID);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			agentCountryCode=partnerPublicManager.getCountryCode(partnerPublic.getBillingCountry(),"COUNTRY",sessionCorpID);
			containsData=partnerPrivateManager.getPartnerCodeFromDefaultTemplate(sessionCorpID,partnerPublic.getPartnerCode());
			

		} else {
			partnerPublic = new PartnerPublic();
			partnerPublic.setStatus("New");
			bstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			tstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			mstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			partnerPublic.setCorpID(sessionCorpID);
			partnerPublic.setCreatedOn(new Date());
			partnerPublic.setUpdatedOn(new Date());
			/*String userRoles=userRole.toString();
			userRoles=userRoles.replace("[", "");
			userRoles=userRoles.replace("]", "");
			String [] roles=userRoles.split(",");
			String forloopFlag="";
			for(String role:roles){
				if(!(forloopFlag.equals("OK"))){*/
				if((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))){
					partnerPublic.setIsAgent(true);
					//forloopFlag="OK";
				 
			}
			if(partnerType!=null && partnerType.equals("AG")){
				partnerPublic.setIsAgent(true);
			}
			if(chkAgentTrue!=null && chkAgentTrue.equalsIgnoreCase("Y")){
				partnerPublic.setIsAgent(true);
			}
			partnerPrivate = new PartnerPrivate();
			partnerPrivate.setAccountHolder(getRequest().getRemoteUser().toString().toUpperCase());
		}
		

			try{
			   if(partnerPrivate.getCustomerFeedback()==null || partnerPrivate.getCustomerFeedback().equalsIgnoreCase("")){			
				  if(company.getJustSayYes()==true && company.getQualitySurvey()==true){
					    	justSayYes="checked";
				  }else if(company.getJustSayYes()==true && company.getQualitySurvey()==false){
					    	justSayYes="checked";
				  }else if(company.getJustSayYes()==false && company.getQualitySurvey()==true){
					    	qualitySurvey="checked";
				  }else{	
					  justSayYes="checked";
				}
			  }else{
				  if(partnerPrivate.getCustomerFeedback().equalsIgnoreCase("Just Say Yes")){
					justSayYes="checked";
				  }else{
					qualitySurvey="checked";
				  }
			   }
			}catch(Exception e){e.printStackTrace();}
		getNotesForIconChange();
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		ownerOperatorList=partnerPublicManager.findOwnerOperatorList(parentId, sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public void createPartnerAccountRef(PartnerPublic partnerPublic,PartnerPrivate partnerPrivate , String refType){
		try{
			String permKey = sessionCorpID +"-"+"component.AcctRef.autoGenerateAccRefWithPartner";
			boolean visibilityForPUTT=false;
			 visibilityForPUTT=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
			 if(visibilityForPUTT){
			 List tempCount = partnerAccountRefManager.countChkAccountRef(partnerPrivate.getPartnerCode(),refType,sessionCorpID);
			 if(tempCount.get(0).toString().trim().equalsIgnoreCase("0")){
				    partnerAccountRef = new PartnerAccountRef();
		        	partnerAccountRef.setCorpID(sessionCorpID);    
		        	partnerAccountRef.setRefType(refType);
		        	String actg="";
		        	String PartnerCode=(partnerPrivate.getPartnerCode());
		        	if(PartnerCode.substring(0, 1).matches("[0-9]")){
						actg=PartnerCode;	
					}else{
					actg=PartnerCode.substring(1, PartnerCode.length());
					}
		        	String val = "000000";
					if(actg.toString().length()<=6){
					val = val.substring(actg.toString().length()); 
					actg=val+actg;
					}
					if(partnerAccountRef.getRefType()!=null && partnerAccountRef.getRefType().toString().trim().equalsIgnoreCase("R")){
					    actg="D"+actg;
					}else{
						actg="C"+actg;	
					}
					partnerAccountRef.setAccountCrossReference(actg);
		        	partnerAccountRef.setPartnerCode(partnerPrivate.getPartnerCode());
		        	partnerAccountRef.setUpdatedOn(new Date());
		        	partnerAccountRef.setCreatedOn(new Date());
		        	partnerAccountRef.setCreatedBy(getRequest().getRemoteUser());
			        partnerAccountRef.setUpdatedBy(getRequest().getRemoteUser());
			        //partnerAccountRef.setPartner(partner);  	
			        partnerAccountRef = partnerAccountRefManager.save(partnerAccountRef);
			 }
			 }

			}catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
		    	 e.printStackTrace(); 
			}
	}
	
	public String partnerDetailsPage() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getComboList(sessionCorpID);
			getRequest().getSession().setAttribute("paramView", paramView);
			partnerType="AC";
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User)auth.getPrincipal();
			List partnerPublicId = partnerPublicManager.findPartnerId(user.getParentAgent(),sessionCorpID);
			try{
				id=Long.parseLong(partnerPublicId.get(0).toString());
			partnerPublic=partnerPublicManager.get(id);
			selectedJobList = new ArrayList(); 
			String tv1 = new String();
			tv1 = partnerPublic.getTypeOfVendor().trim();
			if (tv1.indexOf(",") == 0) {
				tv1 = tv1.substring(1);
			}
			String[] ac = tv1.split(",");
			int arrayLength2 = ac.length;	
			for (int j = 0; j < arrayLength2; j++) {
				String qacCerti = ac[j];
				if (qacCerti.indexOf("(") == 0) {
					qacCerti = qacCerti.substring(1);
				}
				if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
					qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
				}
				if (qacCerti.indexOf("'") == 0) {
					qacCerti = qacCerti.substring(1);
				}
				if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
					qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
				}

				selectedJobList.add(qacCerti.trim());
			}		
			partnerPublic.setTypeOfVendor(selectedJobList.toString().replace("[", "").replace("]", ""));
			}catch(Exception e){
				e.printStackTrace();
			}
			parentId=partnerPublic.getPartnerCode();
			if (partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).isEmpty()) {
				partnerPrivate = new PartnerPrivate();
				partnerPrivate.setLastName(partnerPublic.getLastName());
				partnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
				partnerPrivate.setPartnerPublicId(partnerPublic.getId()); 
				partnerPrivate.setExtReference("");
				donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
				if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
					donoFlag="1";
				}
				if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
					partnerPrivate.setCorpID("VOER");
				}else{
					partnerPrivate.setCorpID(sessionCorpID);
				}
				partnerPrivate.setCompanyDivision(partnerPrivate.getCompanyDivision());
				partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
				partnerPrivate.setCreatedOn(new Date());
				partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
				partnerPrivate.setUpdatedOn(new Date());
				partnerPrivate.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
				partnerPrivate = partnerPrivateManager.save(partnerPrivate);
				try{
					if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
						createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
						createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
					}
					
				    }catch (Exception e) {
						String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
				    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
				    	 e.printStackTrace(); 
					}
			} else {
				partnerPrivate = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).get(0);
				donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
				if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
					donoFlag="1";
				}
			}
			try {
				multiplequalityCertifications = new ArrayList();
				multiplVanlineAffiliations = new ArrayList();
				multiplServiceLines = new ArrayList();
				String[] qc = partnerPublic.getQualityCertifications().split(",");
				int arrayLength1 = qc.length;
				for (int j = 0; j < arrayLength1; j++) {
					String qacCerti = qc[j];
					if (qacCerti.indexOf("(") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}
					if (qacCerti.indexOf("'") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}

					multiplequalityCertifications.add(qacCerti.trim());
				}
				String[] va = partnerPublic.getVanLineAffiliation().split(",");
				int arrayLength2 = va.length;
				for (int k = 0; k < arrayLength2; k++) {
					String vanCer = va[k];
					if (vanCer.indexOf("(") == 0) {
						vanCer = vanCer.substring(1);
					}
					if (vanCer.lastIndexOf(")") == vanCer.length() - 1) {
						vanCer = vanCer.substring(0, vanCer.length() - 1);
					}
					if (vanCer.indexOf("'") == 0) {
						vanCer = vanCer.substring(1);
					}
					if (vanCer.lastIndexOf("'") == vanCer.length() - 1) {
						vanCer = vanCer.substring(0, vanCer.length() - 1);
					}

					multiplVanlineAffiliations.add(vanCer.trim());
				}

				String[] servLi = partnerPublic.getServiceLines().split(",");
				int arrayLength3 = servLi.length;
				for (int i = 0; i < arrayLength3; i++) {
					String serLines = servLi[i];
					if (serLines.indexOf("(") == 0) {
						serLines = serLines.substring(1);
					}
					if (serLines.lastIndexOf(")") == serLines.length() - 1) {
						serLines = serLines.substring(0, serLines.length() - 1);
					}
					if (serLines.indexOf("'") == 0) {
						serLines = serLines.substring(1);
					}
					if (serLines.lastIndexOf("'") == serLines.length() - 1) {
						serLines = serLines.substring(0, serLines.length() - 1);
					}

					multiplServiceLines.add(serLines.trim());
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try{
				if(partnerPrivate !=null && partnerPrivate.getCompanyDivision()!=null && (!(partnerPrivate.getCompanyDivision().toString().trim().equals("")))){
					
					if(companyDivis.contains(partnerPrivate.getCompanyDivision())){
						
					}else{
						companyDivis.add(partnerPrivate.getCompanyDivision());	
					}
					}
				}
			catch (Exception exception) {
				exception.printStackTrace();
			}
			bstates = customerFileManager.findDefaultStateList(partnerPublic.getBillingCountryCode(), sessionCorpID);
			List terminalCountryCode = partnerManager.getCountryCode(partnerPublic.getTerminalCountry());
			List mailIngCountryCode = partnerManager.getCountryCode(partnerPublic.getMailingCountry());
			try {
				if (!terminalCountryCode.isEmpty()) {
					tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
				} else {
					tstates = customerFileManager.findDefaultStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID);
				}
				if (!mailIngCountryCode.isEmpty()) {
					mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
				} else {
					mstates = customerFileManager.findDefaultStateList(partnerPublic.getMailingCountryCode(), sessionCorpID);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		getNotesForIconChange();
		ownerOperatorList=partnerPublicManager.findOwnerOperatorList(parentId, sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		
		if(exList != null && exList.equalsIgnoreCase("true")){
			partnerPublic = (PartnerPublic) getRequest().getSession().getAttribute("partnerPublic");
			partnerPrivate = (PartnerPrivate) getRequest().getSession().getAttribute("partnerPrivate");
			
			getRequest().getSession().removeAttribute("partnerPublic");
			getRequest().getSession().removeAttribute("partnerPrivate");
		}
		try{
			String permKey = sessionCorpID +"-"+"component.tab.commission.compensation";
			checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			}catch(Exception e){
				e.printStackTrace();
			}
		
		if(!sessionCorpID.equals("TSFT") && (partnerType!=null && !partnerType.equalsIgnoreCase("") && partnerType.equalsIgnoreCase("AG"))){
			getComboList(sessionCorpID);
			
			saveAgentPartnerData(partnerPublic,sessionCorpID);
			
		}else{
		/*String userRoles=userRole.toString();
		userRoles=userRoles.replace("[", "");
		userRoles=userRoles.replace("]", "");
		String [] roles=userRoles.split(",");
		String forloopFlag="";
		for(String role:roles){
			if(!(forloopFlag.equals("OK"))){*/
			if((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))){
				partnerPublic.setIsAgent(true);
				//forloopFlag="OK";
			
		}
		getComboList(sessionCorpID);
		boolean isNew = (partnerPublic.getId() == null);
		//Save Start For Customer Servey Question  
        if(!isNew){
        	//for #8855 deca extract
        	if(isDecaExtract!=null && isDecaExtract.equals("1")){
        		partnerPublic.setIsPartnerExtract(false);
        	}
        }

		if (partnerPublic.getFirstName() == null) {
			partnerPublic.setFirstName("");
		}

		if (isNew && partnerPublic.getIsAgent() != null && partnerPublic.getIsAgent() == true) {
			partnerPublic.setCorpID("TSFT");
		} else {
			//partnerPublic.setCorpID(sessionCorpID);
		} 
	
		try{
			if(partnerPrivate !=null && partnerPrivate.getCompanyDivision()!=null && (!(partnerPrivate.getCompanyDivision().toString().trim().equals("")))){
				
				if(companyDivis.contains(partnerPrivate.getCompanyDivision())){
					
				}else{
					companyDivis.add(partnerPrivate.getCompanyDivision());	
				}
				}
			}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		partnerPublic.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			if(exList == null && (popupval == null || !popupval.equalsIgnoreCase("true"))){ 
				String countryCode = "";
			 	if(partnerPublic.getBillingCountryCode() != null){ 
			 		countryCode = partnerPublic.getBillingCountryCode(); 
			 	}else{ 
			 		countryCode = partnerPublic.getTerminalCountryCode(); 
			 	} 
			 	String BillingCity="";
			 	if(partnerPublic !=null && partnerPublic.getBillingCity() !=null){
			 		BillingCity=partnerPublic.getBillingCity().trim();	
			 	}
			 	partnerListNew = partnerManager.getPartnerListNew(partnerPublic.getLastName(),countryCode,sessionCorpID,BillingCity);
			 	if(partnerListNew.size() > 0){
			 		getRequest().getSession().setAttribute("partnerPublic", partnerPublic);
			 		getRequest().getSession().setAttribute("partnerPrivate", partnerPrivate);
			 		String key = "Similar Partners records are listed, if you find that the partner you were trying to add is in this list, press Record Found else press Record Not Found to save your request.";
			 		saveMessage(getText(key)); 
			 		return "PartnerEX"; 
			 	} 
		
			}
			 
			partnerPublic.setCreatedOn(new Date());
			donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
			if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
				donoFlag="1";
			}

			String maxPartnerCode;
			try {
				if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
					maxPartnerCode = "T10001";
				} else {
					List maxCode = partnerPublicManager.findMaxByCode("T");

					maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
					if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
						maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
					}
				}
			} catch (Exception ex) {
				maxPartnerCode = "T10001";
				ex.printStackTrace();
			}

			partnerPublic.setPartnerCode(maxPartnerCode);
			partnerPublic.setAgentParent(maxPartnerCode);
			partnerPublic.setAgentParentName(partnerPublic.getLastName());
			

		}
		//partnerPublic.setAgentParentName(partnerPublic.getLastName());
		partnerPublic.setUpdatedOn(new Date());
		
		
		
		try {
			if(partnerPublic.getPartnerPortalActive()!=null)
			{
			boolean portalIdActive = (partnerPublic.getPartnerPortalActive() == (true));
			if (portalIdActive) {
				String prefix = "";
				String prefixDataSet = "";
				if(partnerType.equalsIgnoreCase("AC")){
					prefix = "DATA_SECURITY_FILTER_SO_BILLTOCODE_" + partnerPublic.getPartnerCode();
					prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + partnerPublic.getPartnerCode();
				}else{
					prefix = "DATA_SECURITY_FILTER_AGENT_" + partnerPublic.getPartnerCode();
					prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
				}
				List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
				List agentFilterSetList = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
				if ((agentFilterList ==null || agentFilterList.isEmpty()) && (agentFilterSetList == null || agentFilterSetList.isEmpty())) {
					List nameList = new ArrayList();
					if(partnerType.equalsIgnoreCase("AC")){
						nameList.add("BILLTOCODE");
					}else{
						nameList.add("BOOKINGAGENTCODE");
						nameList.add("BROKERCODE");
						nameList.add("ORIGINAGENTCODE");
						nameList.add("ORIGINSUBAGENTCODE");
						nameList.add("DESTINATIONAGENTCODE");
						nameList.add("DESTINATIONSUBAGENTCODE");
					}
					dataSecuritySet = new DataSecuritySet();
					Iterator it = nameList.iterator();
					while (it.hasNext()) {
						String fieldName = it.next().toString();
						String postFix = prefix + "_" + fieldName;
						dataSecurityFilter = new DataSecurityFilter();
						if(partnerType.equalsIgnoreCase("AC")){
							dataSecurityFilter.setName(prefix);
						}else{
							dataSecurityFilter.setName(postFix);
						}
						dataSecurityFilter.setTableName("serviceorder");
						dataSecurityFilter.setFieldName(fieldName.toLowerCase());
						dataSecurityFilter.setFilterValues(partnerPublic.getPartnerCode().toString());
						dataSecurityFilter.setCorpID(sessionCorpID);
						dataSecurityFilter.setCreatedOn(new Date());
						dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
						dataSecurityFilter.setUpdatedOn(new Date());
						dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
						dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
						dataSecuritySet.addFilters(dataSecurityFilter);
					}
					dataSecuritySet.setName(prefixDataSet);
					dataSecuritySet.setDescription(prefixDataSet);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySet.setCreatedOn(new Date());
					dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
					dataSecuritySet.setUpdatedOn(new Date());
					dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
				}
				List associatedList = new ArrayList();
				boolean childViewActive = (partnerPublic.getViewChild() == (true));
				if (childViewActive) {
					getChildAgentlist(partnerPublic.getPartnerCode(), associatedList);
					//partnerPublic = partnerPublicManager.get(id);
					associatedList.add(partnerPublic.getAgentParent());
					Iterator it2 = associatedList.iterator();
					while (it2.hasNext()) {
						String pcode = it2.next().toString().trim();
						if (!pcode.equalsIgnoreCase("")) {
							String prefix1 = "";
							String prefixDataSet1 = "";
							if(partnerType.equalsIgnoreCase("AC")){
								prefix1 = "DATA_SECURITY_FILTER_SO_BILLTOCODE_" + pcode;
								prefixDataSet1 = "DATA_SECURITY_SET_SO_BILLTOCODE_" + pcode;
							}else{
								prefix1 = "DATA_SECURITY_FILTER_AGENT_" + pcode;
								prefixDataSet1 = "DATA_SECURITY_SET_AGENT_" + pcode;
							}
							
							
							List agentFilterList1 = dataSecurityFilterManager.getAgentFilterList(prefix1, sessionCorpID);
							List agentFilterSetList1 = dataSecuritySetManager.getDataSecuritySet(prefixDataSet1, sessionCorpID);
							if ((agentFilterList1 ==null || agentFilterList1.isEmpty()) && (agentFilterSetList1 ==null ||agentFilterSetList1.isEmpty())) {
								List nameList = new ArrayList();
								if(partnerType.equalsIgnoreCase("AC")){
									nameList.add("BILLTOCODE");
								}else{
									nameList.add("BOOKINGAGENTCODE");
									nameList.add("BROKERCODE");
									nameList.add("ORIGINAGENTCODE");
									nameList.add("ORIGINSUBAGENTCODE");
									nameList.add("DESTINATIONAGENTCODE");
									nameList.add("DESTINATIONSUBAGENTCODE");
								}
								dataSecuritySet = new DataSecuritySet();
								Iterator it3 = nameList.iterator();
								while (it3.hasNext()) {
									String fieldName = it3.next().toString();
									String postFix = prefix1 + "_" + fieldName;
									dataSecurityFilter = new DataSecurityFilter();
									if(partnerType.equalsIgnoreCase("AC")){
										dataSecurityFilter.setName(prefix1);
									}else{
										dataSecurityFilter.setName(postFix);
									}
									dataSecurityFilter.setTableName("serviceorder");
									dataSecurityFilter.setFieldName(fieldName.toLowerCase());
									dataSecurityFilter.setFilterValues(pcode);
									dataSecurityFilter.setCorpID(sessionCorpID);
									dataSecurityFilter.setCreatedOn(new Date());
									dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
									dataSecurityFilter.setUpdatedOn(new Date());
									dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
									dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
									dataSecuritySet.addFilters(dataSecurityFilter);
								}
								dataSecuritySet.setName(prefixDataSet1);
								dataSecuritySet.setDescription(prefixDataSet1);
								dataSecuritySet.setCorpID(sessionCorpID);
								dataSecuritySet.setCreatedOn(new Date());
     							dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
     							dataSecuritySet.setUpdatedOn(new Date());
     							dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
								dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
							}
						  }
					   }
				    }
			     }
		}
		      } catch (Exception ex) {	
		    	  ex.printStackTrace();
		      }
		
		if (partnerPublic.getIsPrivateParty() != null && partnerPublic.getIsPrivateParty() == true) {
			if (partnerPublic.getBillingState() == null || partnerPublic.getBillingState().trim().length() <= 0) {
				partnerPublic.setBillingState("  ");
			}
			if (partnerPublic.getBillingCity() == null || partnerPublic.getBillingCity().trim().length() <= 0) {
				partnerPublic.setBillingCity("  ");
			}
			if (partnerPublic.getMailingState() == null || partnerPublic.getMailingState().trim().length() <= 0) {
				partnerPublic.setMailingState("  ");
			}
			if (partnerPublic.getMailingCity() == null || partnerPublic.getMailingCity().trim().length() <= 0) {
				partnerPublic.setMailingCity("  ");
			}
		}

		List terminalCountryCode = partnerManager.getCountryCode(partnerPublic.getTerminalCountry());
		if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
			partnerPublic.setTerminalCountryCode(terminalCountryCode.get(0).toString());
			tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
		}

		List mailIngCountryCode = partnerManager.getCountryCode(partnerPublic.getMailingCountry());

		if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
			partnerPublic.setMailingCountryCode(mailIngCountryCode.get(0).toString());
			mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
		}
		if(partnerPublic.getBillingCountryCode()==null|| partnerPublic.getBillingCountryCode().trim().equals(""))
		{
		List billingCountryCode = partnerManager.getCountryCode(partnerPublic.getBillingCountryCode());
		if (!billingCountryCode.isEmpty() && (billingCountryCode.get(0)!=null)) {
			partnerPublic.setBillingCountryCode(billingCountryCode.get(0).toString());
		}
		}

		if (partnerPublic.getBillingCountry() == null || partnerPublic.getBillingCountry().trim().equals("")) {
			partnerPublic.setBillingCountry("");
			partnerPublic.setBillingCountryCode("");
		}
		if (partnerPublic.getTerminalCountry() == null || partnerPublic.getTerminalCountry().trim().equals("")) {
			partnerPublic.setTerminalCountry("");
			partnerPublic.setTerminalCountryCode("");
		}
		if (partnerPublic.getMailingCountry() == null || partnerPublic.getMailingCountry().trim().equals("")) {
			partnerPublic.setMailingCountry("");
			partnerPublic.setMailingCountryCode("");
		}
		if (partnerPublic.getBillingState() == null) {
			partnerPublic.setBillingState("");
		}
		 
		if (partnerPublic.getTerminalCity() == null) {
			partnerPublic.setTerminalCity("");
		}
		if (partnerPublic.getTerminalCountryCode() == null) {
			partnerPublic.setTerminalCountryCode("");
		}
		if (partnerPublic.getTerminalState() == null) {
			partnerPublic.setTerminalState("");
		}

		try {
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
			if (file != null) {

				if (file.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				

				//String rightUploadDir = uploadDir.replace("redsky", "userData");

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file.exists()) {
					InputStream stream = new FileInputStream(file);

					OutputStream bos = new FileOutputStream(uploadDir + fileFileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
					partnerPublic.setLocation1(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
				}
			}
			if (file1 != null) {
				if (file1.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

				String rightUploadDir = uploadDir.replace("redsky", "userData");*/

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file1.exists()) {
					InputStream stream = new FileInputStream(file1);

					OutputStream bos = new FileOutputStream(uploadDir + file1FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
					partnerPublic.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
				}
			}

			if (file2 != null) {
				if (file2.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

				String rightUploadDir = uploadDir.replace("redsky", "userData");*/

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file2.exists()) {
					InputStream stream = new FileInputStream(file2);

					OutputStream bos = new FileOutputStream(uploadDir + file2FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
					partnerPublic.setLocation3(dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
				}
			}

			if (file3 != null) {
				if (file3.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

				String rightUploadDir = uploadDir.replace("redsky", "userData");*/

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file3.exists()) {
					InputStream stream = new FileInputStream(file3);

					OutputStream bos = new FileOutputStream(uploadDir + file3FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
					partnerPublic.setLocation4(dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
				}
			}

		} catch (Exception ex) {
			
			ex.printStackTrace();
		}
      /*   if(partnerPublic.getAliasName()==null){
        	 partnerPublic.setAliasName(partnerPublic.getLastName()); 
         } else if(partnerPublic.getAliasName().toString().trim().equals("")){
        	 
        	 partnerPublic.setAliasName(partnerPublic.getLastName()); 
         }*/
         if(partnerPublic.getPartnerType()!=null && (!(partnerPublic.getPartnerType().toString().trim().equals("")))  && partnerPublic.getStatus().equalsIgnoreCase("Approved") && (partnerPublic.getPartnerType().equalsIgnoreCase("CMM") || partnerPublic.getPartnerType().equalsIgnoreCase("DMM"))){
        	 partnerPublic.setCorpID("TSFT");
        	 List contPolicyList = partnerPublicManager.getPolicyByPartnerCode(partnerPublic.getPartnerCode(), sessionCorpID);
        	 Iterator iteratePolicy = contPolicyList.iterator();
        	 while(iteratePolicy.hasNext()){
        		 ContractPolicy contractPolicy = (ContractPolicy)iteratePolicy.next();
        		 contractPolicy.setCorpID("TSFT");
        		 contractPolicyManager.save(contractPolicy);
        		 List policyDocumentList=contractPolicyManager.findDocumentList(partnerPublic.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        		 Iterator documentList = policyDocumentList.iterator();
        		 while(documentList.hasNext()){
        			 PolicyDocument policyDocument = ((PolicyDocument)documentList.next());
        			 policyDocument.setCorpID(partnerPublic.getCorpID());
        		 }
        	 }
        	 
        	 List faqList = partnerPublicManager.getFAQByPartnerCode(partnerPublic.getPartnerCode(), sessionCorpID);
        	 Iterator iterateFaq = faqList.iterator();
        	 while(iterateFaq.hasNext()){
        		 FrequentlyAskedQuestions faq = (FrequentlyAskedQuestions)iterateFaq.next();
        		 faq.setCorpId("TSFT");
        		 frequentlyAskedQuestionsManager.save(faq);
        	 }
        	 if(partnerPublic.getPartnerPortalActive()){
        		 if(partnerPublic.getAgentParent()!=null && !partnerPublic.getAgentParent().equalsIgnoreCase("")){
        			String partnerTypeVal = "";
        			List partnerPublicList =partnerPublicManager.findPartnerpublicCode(partnerPublic.getAgentParent());
	     				if((partnerPublicList!=null && !partnerPublicList.isEmpty() && partnerPublicList.get(0)!=null) && (partnerPublic.getPartnerCode() !=null && partnerPublic.getAgentParent() !=null) &&  (!(partnerPublic.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent())))) {
	     					if(partnerPublicList!=null && !partnerPublicList.isEmpty() && partnerPublicList.get(0)!=null){
		     					PartnerPublic partnerPublicObj = (PartnerPublic)partnerPublicList.get(0);
		     					partnerTypeVal = partnerPublicObj.getPartnerType();
	     					}
	 					}else if((partnerPublic.getPartnerCode() !=null && partnerPublic.getAgentParent() !=null) &&  (partnerPublic.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()))){
	 						partnerTypeVal	=partnerPublic.getPartnerType();
	 					}
     					if((partnerTypeVal!=null && !partnerTypeVal.equalsIgnoreCase("")) && (partnerTypeVal.equalsIgnoreCase("CMM") || partnerTypeVal.equalsIgnoreCase("DMM"))){
     						try{
	     						String	prefixDataFilter = "DATA_SECURITY_FILTER_SO_BILLTOCODE_"+partnerPublic.getPartnerCode().trim();
	     						String	prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_"+partnerPublic.getPartnerCode().trim();
	     						String tempCorpId = "";
	     						String corpIdStr = "";
	     						List corpIdList = companyManager.getcorpIDList();
	     						if(corpIdList!=null && !corpIdList.isEmpty()){
	     							corpIdStr = corpIdList.toString();
		     						corpIdStr = corpIdStr.replace("[", "").replace("]", "");
	     							for(String str:corpIdStr.split("\\,")){
	 					        		if(tempCorpId!=null && !tempCorpId.equalsIgnoreCase("")){
	 					        			tempCorpId = tempCorpId+","+"'"+str.trim()+"'";
	 					        		}else{
	 					        			tempCorpId = "'"+str.trim()+"'";
	 					        		}
	     							}
		     						List corpIdFilterList = dataSecurityFilterManager.getOtherCorpidFilterList(prefixDataFilter, tempCorpId);
		     						if (corpIdFilterList == null || corpIdFilterList.isEmpty()) {
		     							Iterator listIterator= corpIdList.iterator();
		     					    	while (listIterator.hasNext()) {
		     					    		String corpIdVal = listIterator.next().toString();
		     								dataSecurityFilter = new DataSecurityFilter();
		     								dataSecurityFilter.setName(prefixDataFilter);
		     								dataSecurityFilter.setTableName("serviceorder");
		     								dataSecurityFilter.setFieldName("billtocode");
		     								dataSecurityFilter.setFilterValues(partnerPublic.getPartnerCode().toString());
		     								dataSecurityFilter.setCorpID(corpIdVal);
		     								dataSecurityFilter.setCreatedOn(new Date());
		     								dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
		     								dataSecurityFilter.setUpdatedOn(new Date());
		     								dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
		     								dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
		     							
		     								dataSecuritySet = new DataSecuritySet();
		     								dataSecuritySet.addFilters(dataSecurityFilter);
			     							dataSecuritySet.setName(prefixDataSet);
			     							dataSecuritySet.setDescription(prefixDataSet);
			     							dataSecuritySet.setCorpID(corpIdVal);
			     							dataSecuritySet.setCreatedOn(new Date());
			     							dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
			     							dataSecuritySet.setUpdatedOn(new Date());
			     							dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
			     							dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
		     					    	}
		     						}
	     						}
	     					}catch(Exception e){
	     						e.printStackTrace();
	     					}
     					}
     				
        		 }
        	 }
         } else{
        	 //partnerPublic.setCorpID(sessionCorpID);
         }
         if(partnerPublic.getTypeOfVendor()==null){
 			partnerPublic.setTypeOfVendor("");
 		}
         if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
        	 partnerPublic.setCorpID("VOER");
         }
		partnerPublic = partnerPublicManager.save(partnerPublic);
		partnerPublicManager.updateAgentParentName(partnerPublic.getLastName(),partnerPublic.getPartnerCode(),partnerPublic.getUpdatedBy());
		
		try{
		selectedJobList = new ArrayList(); 
		String tv1 = new String();
		if(partnerPublic.getTypeOfVendor()!=null && !partnerPublic.getTypeOfVendor().trim().equals(""))
		{
		tv1 = partnerPublic.getTypeOfVendor();
		if (tv1.indexOf(",") == 0) {
			tv1 = tv1.substring(1);
		}
		String[] ac = tv1.split(",");
		int arrayLength2 = ac.length;	
		for (int j = 0; j < arrayLength2; j++) {
			String qacCerti = ac[j];
			if (qacCerti.indexOf("(") == 0) {
				qacCerti = qacCerti.substring(1);
			}
			if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
			}
			if (qacCerti.indexOf("'") == 0) {
				qacCerti = qacCerti.substring(1);
			}
			if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
			}

			selectedJobList.add(qacCerti.trim());
		}
		}
		partnerPublic.setTypeOfVendor(selectedJobList.toString().replace("[", "").replace("]", ""));
		}catch(Exception e){
			e.printStackTrace();
		}
		if (isNew) {
			partnerPrivate.setLastName(partnerPublic.getLastName());
			partnerPrivate.setStatus(partnerPublic.getStatus());
			partnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
			partnerPrivate.setPartnerPublicId(partnerPublic.getId());
			if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
				partnerPrivate.setCorpID("VOER");
				donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
				if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
					donoFlag="1";
				}
			}else{
				partnerPrivate.setCorpID(sessionCorpID);
				donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
				if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
					donoFlag="1";
				}
			}
			partnerPrivate.setCompanyDivision(partnerPrivate.getCompanyDivision());
			partnerPrivate.setDriverAgency(partnerPrivate.getDriverAgency());
			partnerPrivate.setValidNationalCode(partnerPrivate.getValidNationalCode());
			partnerPrivate.setInvoiceUploadCheck(partnerPrivate.getInvoiceUploadCheck());
			partnerPrivate.setLongPercentage(partnerPrivate.getLongPercentage());
			partnerPrivate.setPayableUploadCheck(partnerPrivate.getPayableUploadCheck());
			partnerPrivate.setStopNotAuthorizedInvoices(partnerPrivate.getStopNotAuthorizedInvoices());
			partnerPrivate.setDoNotCopyAuthorizationSO(partnerPrivate.getDoNotCopyAuthorizationSO());
			
			partnerPrivate.setCardNumber(partnerPrivate.getCardNumber());
			partnerPrivate.setCardStatus(partnerPrivate.getCardStatus());
			partnerPrivate.setCustomerId(partnerPrivate.getCustomerId());
			partnerPrivate.setAccountId(partnerPrivate.getAccountId());
			partnerPrivate.setLicenseNumber(partnerPrivate.getLicenseNumber());
			partnerPrivate.setLicenseState(partnerPrivate.getLicenseState());
			partnerPrivate.setFuelPurchase(partnerPrivate.getFuelPurchase());
			partnerPrivate.setExpressCash(partnerPrivate.getExpressCash());
			partnerPrivate.setYruAccess(partnerPrivate.getYruAccess());
			partnerPrivate.setUTSCompanyType(partnerPrivate.getUTSCompanyType());			
			partnerPrivate.setPaymentMethod(partnerPrivate.getPaymentMethod());
			partnerPrivate.setTaxId(partnerPrivate.getTaxId());
			partnerPrivate.setTaxIdType(partnerPrivate.getTaxIdType());
			partnerPrivate.setEmergencyContact(partnerPrivate.getEmergencyContact());
			partnerPrivate.setEmergencyEmail(partnerPrivate.getEmergencyEmail());
			partnerPrivate.setEmergencyPhone(partnerPrivate.getEmergencyPhone());
			partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
			partnerPrivate.setCreatedOn(new Date());
			partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
			partnerPrivate.setUpdatedOn(new Date());
            partnerPrivate.setFleet(partnerPrivate.getFleet());
            partnerPrivate.setHire(partnerPrivate.getHire());
            partnerPrivate.setTermination(partnerPrivate.getTermination());
            partnerPrivate.setBirth(partnerPrivate.getBirth());
			partnerPrivate.setExtReference(partnerPrivate.getExtReference());
			partnerPrivate.setCommission(partnerPrivate.getCommission());
			partnerPrivate.setTruckID(partnerPrivate.getTruckID());
			partnerPrivate.setDriverID(partnerPrivate.getDriverID());
			partnerPrivate.setPayTo(partnerPrivate.getPayTo());
			partnerPrivate.setCorp(partnerPrivate.getCorp());
			partnerPrivate.setCargoInsurance(partnerPrivate.getCargoInsurance());
			partnerPrivate.setLiabilityInsurance(partnerPrivate.getLiabilityInsurance());
			partnerPrivate.setMc(partnerPrivate.getMc());
			partnerPrivate.setDot(partnerPrivate.getDot());
			partnerPrivate.setTrailerID(partnerPrivate.getTrailerID());
			partnerPrivate.setProgressiveType(partnerPrivate.getProgressiveType());
			partnerPrivate.setPersonalEscrow(partnerPrivate.getPersonalEscrow());
			partnerPrivate.setCommissionPlan(partnerPrivate.getCommissionPlan());
			partnerPrivate.setCompanyEscrow(partnerPrivate.getCompanyEscrow());
			partnerPrivate.setDriverType(partnerPrivate.getDriverType());
			partnerPrivate.setNoDispatch(partnerPrivate.getNoDispatch());
			partnerPrivate.setSoAllDrivers(partnerPrivate.getSoAllDrivers());
			partnerPrivate.setPartnerPortalActive(partnerPrivate.getPartnerPortalActive());
			partnerPrivate.setAssociatedAgents(partnerPrivate.getAssociatedAgents());
			partnerPrivate.setViewChild(partnerPrivate.getViewChild());
			partnerPrivate.setNoInsurance(false);
			partnerPrivate.setDoNotInvoice(false);
			partnerPrivate.setDetailedList(false);
			partnerPrivate.setLumpSum(false);
			partnerPrivate.setLumpSumAmount(new BigDecimal(0.0));
			partnerPrivate.setLumpPerUnit("");
			partnerPrivate.setMinReplacementvalue("");
			if(partnerPrivate.getExcludeFromParentFaqUpdate() == null){
				partnerPrivate.setExcludeFromParentFaqUpdate(new Boolean(false));
			}
			if(partnerPrivate.getExcludeFromParentPolicyUpdate() == null){
				partnerPrivate.setExcludeFromParentPolicyUpdate(new Boolean(false));
			}
			
			partnerPrivate.setExcludeFromParentUpdate(false);
			partnerPrivate.setExcludeFromParentQualityUpdate(false);
			partnerPrivate.setExcludeFromParentQualityUpdateRelo(false);
			if(partnerPrivate.getExcludePublicFromParentUpdate()==null){
				partnerPrivate.setExcludePublicFromParentUpdate(false);
			}else{
				if(partnerPrivate.getExcludePublicFromParentUpdate()==true){
					partnerPrivate.setExcludePublicFromParentUpdate(true);
				}
				if(partnerPrivate.getExcludePublicFromParentUpdate()==false){
					partnerPrivate.setExcludePublicFromParentUpdate(false);
				}
			}			
			partnerPrivate.setExcludePublicReloSvcsParentUpdate(false);
			if(partnerPrivate.getCreditTerms()!=null){
				partnerPrivate.setCreditTerms(partnerPrivate.getCreditTerms());
			}
			if(partnerPrivate.getCustomerFeedback()!=null && !partnerPrivate.getCustomerFeedback().equalsIgnoreCase("") && (partnerPrivate.getCustomerFeedback().equalsIgnoreCase("Just Say Yes")||partnerPrivate.getCustomerFeedback().equalsIgnoreCase("Quality Survey"))){}else{
				if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) 
						&& company.getJustSayYes()==true && company.getQualitySurvey()==true){
				    	partnerPrivate.setCustomerFeedback("Just Say Yes");
					  }else if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) 
							  && company.getJustSayYes()==true && company.getQualitySurvey()==false){
						    	partnerPrivate.setCustomerFeedback("Just Say Yes");
					  }else if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) 
							  && company.getJustSayYes()==false && company.getQualitySurvey()==true){
						    	partnerPrivate.setCustomerFeedback("Quality Survey");
					  }else{	
						  partnerPrivate.setCustomerFeedback("Quality Survey");
				}
			}
			partnerPrivate.setStorageEmail(partnerPrivate.getStorageEmail());
			partnerPrivate.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
			if(checkFieldVisibility){
			int year = Calendar.getInstance().get(Calendar.YEAR);
			partnerPrivate.setCompensationYear(year);
			}
			if(partnerPublic.getAgentParent()!=null && (!partnerPublic.getAgentParent().equals(""))){
			 parentCompensationYear=partnerPrivateManager.getParentCompensationYear(partnerPublic.getCorpID(),partnerPublic.getAgentParent());
			if(parentCompensationYear!=0 && checkFieldVisibility){
				partnerPrivate.setCompensationYear(parentCompensationYear);
			}
			}
			try{
				partnerPrivate.setRutTaxNumber(partnerPrivate.getRutTaxNumber());
			}catch(Exception e){
				e.printStackTrace();
			}		
			partnerPrivate = partnerPrivateManager.save(partnerPrivate);
			try{
				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
				 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
				 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
				}
				 
			    }catch (Exception e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
			    	 e.printStackTrace(); 
				}
		}
		if (!isNew) {
			
			PartnerPrivate partnerPrivateOld = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).get(0);
			if(partnerPrivate.getCompanyDivision() != null) {
				partnerPrivateOld.setCompanyDivision(partnerPrivate.getCompanyDivision());
			}
			if(partnerPrivate.getDriverAgency() != null) {
				partnerPrivateOld.setDriverAgency(partnerPrivate.getDriverAgency());
			}
			if(partnerPrivate.getValidNationalCode() != null) {
				partnerPrivateOld.setValidNationalCode(partnerPrivate.getValidNationalCode());
			}
			if(partnerPrivate.getInvoiceUploadCheck() != null) {
				partnerPrivateOld.setInvoiceUploadCheck(partnerPrivate.getInvoiceUploadCheck());
			}
			if(partnerPrivate.getLongPercentage() != null) {
				partnerPrivateOld.setLongPercentage(partnerPrivate.getLongPercentage());
			}
			if(partnerPrivate.getPayableUploadCheck() != null) {
				partnerPrivateOld.setPayableUploadCheck(partnerPrivate.getPayableUploadCheck());
			}
			if(partnerPrivate.getStopNotAuthorizedInvoices() != null) {
				partnerPrivateOld.setStopNotAuthorizedInvoices(partnerPrivate.getStopNotAuthorizedInvoices());
			}
			if(partnerPrivate.getDoNotCopyAuthorizationSO() != null) {
				partnerPrivateOld.setDoNotCopyAuthorizationSO(partnerPrivate.getDoNotCopyAuthorizationSO());
			}
			if(partnerPrivate.getFuelPurchase() != null) {
				partnerPrivateOld.setFuelPurchase(partnerPrivate.getFuelPurchase());
			}
			if(partnerPrivate.getExpressCash() != null) {
				partnerPrivateOld.setExpressCash(partnerPrivate.getExpressCash());
			}
			if(partnerPrivate.getYruAccess() != null) {
				partnerPrivateOld.setYruAccess(partnerPrivate.getYruAccess());
			}
			if(partnerPrivate.getCardNumber() != null) {
				partnerPrivateOld.setCardNumber(partnerPrivate.getCardNumber());
			}
			if(partnerPrivate.getCardStatus() != null) {
				partnerPrivateOld.setCardStatus(partnerPrivate.getCardStatus());
			}
			if(partnerPrivate.getCustomerId() != null) {
				partnerPrivateOld.setCustomerId(partnerPrivate.getCustomerId());
			}
			if(partnerPrivate.getAccountId() != null) {
				partnerPrivateOld.setAccountId(partnerPrivate.getAccountId());
			}
			if(partnerPrivate.getLicenseNumber() != null) {
				partnerPrivateOld.setLicenseNumber(partnerPrivate.getLicenseNumber());
			}
			if(partnerPrivate.getUTSCompanyType()!=null) {
				partnerPrivateOld.setUTSCompanyType(partnerPrivate.getUTSCompanyType());
			}			
			if(partnerPrivate.getLicenseState() != null) {
				partnerPrivateOld.setLicenseState(partnerPrivate.getLicenseState());
			}
			if(partnerPrivate.getPaymentMethod() != null) {
				partnerPrivateOld.setPaymentMethod(partnerPrivate.getPaymentMethod());
			}
			
			if(partnerPrivate.getTaxId() != null) {
				partnerPrivateOld.setTaxId(partnerPrivate.getTaxId());
			}
			if(partnerPrivate.getTaxIdType() != null) {
				partnerPrivateOld.setTaxIdType(partnerPrivate.getTaxIdType());
			}
			if(partnerPrivate.getExtReference() != null) {
				partnerPrivateOld.setExtReference(partnerPrivate.getExtReference());
			}

			if(partnerPrivate.getEmergencyContact()!= null){
				partnerPrivateOld.setEmergencyContact(partnerPrivate.getEmergencyContact());
			}
			if(partnerPrivate.getEmergencyEmail() != null){
				partnerPrivateOld.setEmergencyEmail(partnerPrivate.getEmergencyEmail());
			}
			if(partnerPrivate.getEmergencyPhone() != null){
				partnerPrivateOld.setEmergencyPhone(partnerPrivate.getEmergencyPhone());
			}

			if(partnerPrivate.getCommission() != null) {
				partnerPrivateOld.setCommission(partnerPrivate.getCommission());
			}
			if(partnerPrivate.getFleet() != null ){
				partnerPrivateOld.setFleet(partnerPrivate.getFleet());
			}
			/*if(partnerPrivate.getHire() != null ){
				partnerPrivateOld.setHire(partnerPrivate.getHire());				
			}
			if(partnerPrivate.getTermination() !=null ){
				partnerPrivateOld.setTermination(partnerPrivate.getTermination());				
			}*/
			partnerPrivateOld.setHire(getPartnerPrivateHire());				
			partnerPrivateOld.setTermination(getPartnerPrivateTermination());				
			if(partnerPrivate.getBirth() != null){
				partnerPrivateOld.setBirth(partnerPrivate.getBirth());
			}
			if(partnerPrivate.getTruckID() != null){
				partnerPrivateOld.setTruckID(partnerPrivate.getTruckID());
			}
			if(partnerPrivate.getDriverID() != null){
				partnerPrivateOld.setDriverID(partnerPrivate.getDriverID());
			}
			if(partnerPrivate.getPayTo() != null){
				partnerPrivateOld.setPayTo(partnerPrivate.getPayTo());
			}
			if(partnerPrivate.getCargoInsurance()!=null){
				partnerPrivateOld.setCargoInsurance(partnerPrivate.getCargoInsurance());				
			}
			if(partnerPrivate.getCorp() !=null){
				partnerPrivateOld.setCorp(partnerPrivate.getCorp());
			}
			if(partnerPrivate.getLiabilityInsurance() !=null){
				partnerPrivateOld.setLiabilityInsurance(partnerPrivate.getLiabilityInsurance());
			}
			if(partnerPrivate.getMc() !=null){
				partnerPrivateOld.setMc(partnerPrivate.getMc());
			}
			if(partnerPrivate.getDot() !=null){
				partnerPrivateOld.setDot(partnerPrivate.getDot());
			}
			if(partnerPrivate.getTrailerID() !=null){
				partnerPrivateOld.setTrailerID(partnerPrivate.getTrailerID());
			}
			if(partnerPrivate.getProgressiveType()!= null){
				partnerPrivateOld.setProgressiveType(partnerPrivate.getProgressiveType());
		    	}
			if(partnerPrivate.getAccountingHold()!= null){
				partnerPrivateOld.setAccountingHold(partnerPrivate.getAccountingHold());
		    	}
			if(partnerPrivate.getCollection()!= null){
				partnerPrivateOld.setCollection(partnerPrivate.getCollection());
		    	}
			if(partnerPrivate.getCustomerFeedback()!= null){
				partnerPrivateOld.setCustomerFeedback(partnerPrivate.getCustomerFeedback());
		    	}
			if(partnerPrivate.getAccountHolder()!= null){
				partnerPrivateOld.setAccountHolder(partnerPrivate.getAccountHolder());
		    	}
			if(partnerPrivate.getCreditTerms()!=null){
				partnerPrivateOld.setCreditTerms(partnerPrivate.getCreditTerms());
			}
			if(partnerPrivate.getPersonalEscrow()!=null){
				partnerPrivateOld.setPersonalEscrow(partnerPrivate.getPersonalEscrow());
			}if(partnerPrivate.getCompanyEscrow()!=null){
				partnerPrivateOld.setCompanyEscrow(partnerPrivate.getCompanyEscrow());
			}
			if(partnerPrivate.getCommissionPlan()!=null){
				partnerPrivateOld.setCommissionPlan(partnerPrivate.getCommissionPlan());
			}
			if(partnerPrivate.getDriverType()!=null){
				partnerPrivateOld.setDriverType(partnerPrivate.getDriverType());
			}
			if(partnerPrivate.getNoDispatch()!=null){
				partnerPrivateOld.setNoDispatch(partnerPrivate.getNoDispatch());
			}
			if(partnerPrivate.getSoAllDrivers()!=null){
				partnerPrivateOld.setSoAllDrivers(partnerPrivate.getSoAllDrivers());
			}
			if(partnerPrivate.getPartnerPortalActive()!=null){
				partnerPrivateOld.setPartnerPortalActive(partnerPrivate.getPartnerPortalActive());
				
			}
			if(partnerPrivate.getAssociatedAgents()!=null){
				partnerPrivateOld.setAssociatedAgents(partnerPrivate.getAssociatedAgents());
					}
			if(partnerPrivate.getViewChild()!=null){
				partnerPrivateOld.setViewChild(partnerPrivate.getViewChild());
				
			}
			if(partnerPrivate.getExcludeFromParentFaqUpdate()==null){
				partnerPrivateOld.setExcludeFromParentFaqUpdate(false);
			}
			if(partnerPrivate.getExcludeFromParentQualityUpdate()==null){
				partnerPrivateOld.setExcludeFromParentQualityUpdate(false);
			}
			if(partnerPrivate.getExcludeFromParentQualityUpdateRelo()==null){
				partnerPrivateOld.setExcludeFromParentQualityUpdateRelo(false);
			}
			if(partnerPrivate.getExcludeFromParentPolicyUpdate()==null){
				partnerPrivateOld.setExcludeFromParentPolicyUpdate(false);
			}
			if(partnerPrivate.getExcludeFromParentUpdate()==null){
				partnerPrivateOld.setExcludeFromParentUpdate(false);
			}
			if(partnerPrivate.getExcludePublicFromParentUpdate()==null){
				partnerPrivateOld.setExcludePublicFromParentUpdate(false);
			}else{
				if(partnerPrivate.getExcludePublicFromParentUpdate()==true){
					partnerPrivateOld.setExcludePublicFromParentUpdate(true);
				}
				if(partnerPrivate.getExcludePublicFromParentUpdate()==false){
					partnerPrivateOld.setExcludePublicFromParentUpdate(false);
				}
			}
			partnerPrivateOld.setStorageEmail(partnerPrivate.getStorageEmail());
			partnerPrivateOld.setStatus(partnerPublic.getStatus());
			partnerPrivateOld.setUpdatedBy(partnerPublic.getUpdatedBy());
			partnerPrivateOld.setUpdatedOn(new Date());
			partnerPrivateOld.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
			
			
			if(partnerPublic.getIsAccount()!=null){
			if(checkFieldVisibility && partnerPublic.getIsAccount() && checkCompensationYear.equalsIgnoreCase("Y")){
				int year = Calendar.getInstance().get(Calendar.YEAR);
				partnerPrivateOld.setCompensationYear(year);
			}
			}
			if(partnerPublic.getAgentParent()!=null && (!partnerPublic.getAgentParent().equals(""))){
				 parentCompensationYear=partnerPrivateManager.getParentCompensationYear(partnerPublic.getCorpID(),partnerPublic.getAgentParent());
				if(parentCompensationYear!=0 && checkFieldVisibility){
					partnerPrivateOld.setCompensationYear(parentCompensationYear);
				}
				}
			try{
				if(partnerPrivate.getRutTaxNumber()!=null){
					partnerPrivateOld.setRutTaxNumber(partnerPrivate.getRutTaxNumber());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			partnerPrivate = partnerPrivateManager.save(partnerPrivateOld);
			try{
				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){ 	
				    createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
				 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
				}
				 
			    }catch (Exception e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
			    	 e.printStackTrace(); 
				}
			partnerPrivateManager.updateLastName(sessionCorpID,partnerPublic.getLastName(), partnerPublic.getId()); 
			donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
			if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
				donoFlag="1";
			}
		}
		 if(partnerPublic.getPartnerType()!=null && partnerPublic.getStatus() !=null &&  (!(partnerPublic.getStatus().equalsIgnoreCase("New"))) && (partnerPublic.getPartnerType().equalsIgnoreCase("CMM") || partnerPublic.getPartnerType().equalsIgnoreCase("DMM"))){
        	 List corpIdList = companyManager.findCorpIdList();
        	 if(corpIdList!=null && (!(corpIdList.isEmpty()))){
        		 Iterator iter = corpIdList.iterator();
		         while (iter.hasNext()) {
		        	 String partnerCodeCorpId = iter.next().toString();
		        	 String[] strPartner = partnerCodeCorpId.split("#");
		        	 String otherCorpId= strPartner[0];
		        	 String partnrCode= strPartner[1];
		        	 PartnerPrivate otherPartnerPrivate=null;
		        	 if(!(partnrCode.equalsIgnoreCase("NA"))){
		        	 //PartnerPublic oPartnerPublic = partnerPublicManager.getPartnerByCode(partnrCode);
		        	 //if(oPartnerPublic.getUtsNumber().equalsIgnoreCase("Y")){
		        	 List partnrPrivateList = partnerPrivateManager.getPartnerPrivateByPartnerCode(otherCorpId, partnerPublic.getPartnerCode());
			         if(partnrPrivateList!=null && (!(partnrPrivateList.isEmpty())) && (partnrPrivateList.get(0)!=null)){
				            Long partnrPrivateId = Long.parseLong(partnrPrivateList.get(0).toString());
				        	otherPartnerPrivate = partnerPrivateManager.getForOtherCorpid(partnrPrivateId);
				        	otherPartnerPrivate.setStatus(partnerPublic.getStatus());
				        	partnerPrivateManager.save(otherPartnerPrivate);
				        	try{
				        		if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
				        		}
								 
							    }catch (Exception e) {
									String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
							    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
							    	 e.printStackTrace(); 
								}
			          }else{
			        		otherPartnerPrivate = new PartnerPrivate(); 
			        		otherPartnerPrivate.setAccountHolder(getRequest().getRemoteUser().toString().toUpperCase());
			        		otherPartnerPrivate.setLastName(partnerPublic.getLastName());
			        		otherPartnerPrivate.setStatus(partnerPublic.getStatus());
			        		otherPartnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
			        		otherPartnerPrivate.setPartnerPublicId(partnerPublic.getId());
		     				otherPartnerPrivate.setCorpID(otherCorpId);	
			     			otherPartnerPrivate.setCompanyDivision("");
			     			
			     			otherPartnerPrivate.setUTSCompanyType(partnerPrivate.getUTSCompanyType());			
			     			
			     			otherPartnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
			     			otherPartnerPrivate.setCreatedOn(new Date());
			     			otherPartnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
			     			otherPartnerPrivate.setUpdatedOn(new Date());
			     			otherPartnerPrivate.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
			     			partnerPrivateManager.save(otherPartnerPrivate);
			     			try{
			     				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
			     				}
								 
							    }catch (Exception e) {
									String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
							    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
							    	 e.printStackTrace(); 
								}
			     		
			        	  }
		              }
        	    }
        	 }	 
         } 
		String paramCheck = (String) getRequest().getSession().getAttribute("paramView");
		if (paramCheck != null && paramCheck.equalsIgnoreCase("View") && isNew && partnerType.equalsIgnoreCase("AC")) {
			accountProfile = new AccountProfile();
			accountProfile.setStage("Prospect");
			accountProfile.setPartnerCode(partnerPublic.getPartnerCode());
			accountProfile.setCorpID(sessionCorpID);
			accountProfile.setCreatedOn(new Date());
			accountProfile.setCreatedBy(getRequest().getRemoteUser());
			accountProfile.setUpdatedOn(new Date());
			accountProfile.setUpdatedBy(getRequest().getRemoteUser());
			accountProfileManager.save(accountProfile);
		}

		bstates = customerFileManager.findDefaultStateList(partnerPublic.getBillingCountryCode(), sessionCorpID);
		tstates = customerFileManager.findDefaultStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID);
		mstates = customerFileManager.findDefaultStateList(partnerPublic.getMailingCountryCode(), sessionCorpID);
		try {
			multiplequalityCertifications = new ArrayList();
			multiplVanlineAffiliations = new ArrayList();
			multiplServiceLines = new ArrayList();
			String qc1 = new String();
			if(partnerPublic.getQualityCertifications()!=null && !partnerPublic.getQualityCertifications().trim().equals("")){
			qc1 = partnerPublic.getQualityCertifications().trim();
			if (qc1.indexOf(",") == 0) {
				qc1 = qc1.substring(1);
			}
			String[] qc = qc1.trim().split(",");
			int arrayLength1 = qc.length;
			for (int j = 0; j < arrayLength1; j++) {
				String qacCerti = qc[j];
				if (qacCerti.indexOf("(") == 0) {
					qacCerti = qacCerti.substring(1);
				}
				if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
					qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
				}
				if (qacCerti.indexOf("'") == 0) {
					qacCerti = qacCerti.substring(1);
				}
				if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
					qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
				}

				multiplequalityCertifications.add(qacCerti.trim());
			}
			}
			partnerPublic.setQualityCertifications(multiplequalityCertifications.toString().replace("[", "").replace("]", ""));
			String va1 = new String();
			if(partnerPublic.getVanLineAffiliation()!=null && !partnerPublic.getVanLineAffiliation().trim().equals(""))
			{
			va1 = partnerPublic.getVanLineAffiliation().trim();
			if (va1.indexOf(",") == 0) {
				va1 = va1.substring(1);
			}
			String[] va = va1.split(",");
			int arrayLength2 = va.length;
			for (int k = 0; k < arrayLength2; k++) {
				String vanCer = va[k];
				if (vanCer.indexOf("(") == 0) {
					vanCer = vanCer.substring(1);
				}
				if (vanCer.lastIndexOf(")") == vanCer.length() - 1) {
					vanCer = vanCer.substring(0, vanCer.length() - 1);
				}
				if (vanCer.indexOf("'") == 0) {
					vanCer = vanCer.substring(1);
				}
				if (vanCer.lastIndexOf("'") == vanCer.length() - 1) {
					vanCer = vanCer.substring(0, vanCer.length() - 1);
				}

				multiplVanlineAffiliations.add(vanCer.trim());
			}
			}
			partnerPublic.setVanLineAffiliation(multiplVanlineAffiliations.toString().replace("[", "").replace("]", ""));
			String servLi1 = new String();
			if(partnerPublic.getServiceLines()!=null && !partnerPublic.getServiceLines().trim().equals(""))
			{
			servLi1 = partnerPublic.getServiceLines().trim();
			if (servLi1.indexOf(",") == 0) {
				servLi1 = servLi1.substring(1);
			}
			String[] servLi = servLi1.split(",");
			int arrayLength3 = servLi.length;
			for (int l = 0; l < arrayLength3; l++) {
				String serLines = servLi[l];
				if (serLines.indexOf("(") == 0) {
					serLines = serLines.substring(1);
				}
				if (serLines.lastIndexOf(")") == serLines.length() - 1) {
					serLines = serLines.substring(0, serLines.length() - 1);
				}
				if (serLines.indexOf("'") == 0) {
					serLines = serLines.substring(1);
				}
				if (serLines.lastIndexOf("'") == serLines.length() - 1) {
					serLines = serLines.substring(0, serLines.length() - 1);
				}

				multiplServiceLines.add(serLines.trim());
			}
			}
			partnerPublic.setServiceLines(multiplServiceLines.toString().replace("[", "").replace("]", ""));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
		if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
			donoFlag="1";
		}
		if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
			String key = (isNew) ? "partner.added" : "partner.updated";
			saveMessage(getText(key));
		}
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		hitflag = "1";
		//id = partnerPublic.getId();
		ownerOperatorList=partnerPublicManager.findOwnerOperatorList(parentId, sessionCorpID);
		if(checkCodefromDefaultAccountLine!=null && !checkCodefromDefaultAccountLine.equalsIgnoreCase("")){
			try{
			String[] templateDetail = checkCodefromDefaultAccountLine.split("~");
       	 	String checkFromTemplate= templateDetail[0];
       	 	String partnerType= templateDetail[1];
       	 	String userName=getRequest().getRemoteUser();
       	 	if(checkFromTemplate.equalsIgnoreCase("Y")){
       	 		partnerPublicManager.updateDefaultAccountLine(sessionCorpID,partnerPublic.getPartnerCode(),partnerType,userName);
       	 	}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		getNotesForIconChange();
		}
		flagHit="2";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String getChildAgentlist(String parentAgentCode, List agentList) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		List associatedAgentsList = dataSecuritySetManager.getAssociatedAgents(parentAgentCode, sessionCorpID);
		if (!associatedAgentsList.isEmpty()) {

			if (associatedAgentsList.get(0) != null) {
				String ag = associatedAgentsList.toString();
				ag = ag.replace("[", "");
				ag = ag.replace("]", "");
				String[] arrayid1 = ag.split(",");
				int arrayLength1 = arrayid1.length;
				for (int i = 0; i < arrayLength1; i++) {
					agentList.add(arrayid1[i]);
					if (getChildAgentlist(arrayid1[i], agentList) == null) {
						break;
					}

				}
			}

		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private String creditTerm;
	private String partnerPrefix;
	@SkipValidation
	public String savePopUp() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getComboList(sessionCorpID);		
		boolean isNew = (partner.getId() == null);

		if (partner.getFirstName() == null) {
			partner.setFirstName("");
		}

		/*if (isNew && partner.getIsAgent() != null && partner.getIsAgent() == true) {
			partner.setCorpID("TSFT");
		} else {*/
			//partner.setCorpID(sessionCorpID);
		//}

		partner.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			/*
			 * if(exList == null && (popupval == null ||
			 * !popupval.equalsIgnoreCase("true"))){ String countryCode="";
			 * if(partnerPublic.getBillingCountryCode() != null){ countryCode =
			 * partnerPublic.getBillingCountryCode(); }else{ countryCode =
			 * partnerPublic.getTerminalCountryCode(); } partnerListNew =
			 * partnerManager.getPartnerListNew(partnerPublic.getLastName(),countryCode,sessionCorpID);
			 * if(partnerListNew.size() > 0){
			 * getRequest().getSession().setAttribute("partner", partnerPublic);
			 * String key = "Similar Partners records are listed, if you find
			 * that the partner you were trying to add is in this list, press
			 * Record Found else press Record Not Found to save your request.";
			 * saveMessage(getText(key)); return "PartnerEX"; } }
			 */

			partner.setCreatedOn(new Date());

			String maxPartnerCode;
			try {
				if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
					maxPartnerCode = "T10001";
				} else {
					List maxCode = partnerPublicManager.findMaxByCode("T");

					maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
					if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
						maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
					}
				}
			} catch (Exception ex) {
				maxPartnerCode = "T10001";
				ex.printStackTrace();
			}

			partner.setPartnerCode(maxPartnerCode);
			partner.setAgentParent(maxPartnerCode);

		}
		partner.setUpdatedOn(new Date());

		if (partner.getIsPrivateParty() == true) {
			if (partner.getBillingState() == null || partner.getBillingState().trim().length() <= 0) {
				partner.setBillingState("  ");
			}
			if (partner.getBillingCity() == null || partner.getBillingCity().trim().length() <= 0) {
				partner.setBillingCity("  ");
			}
			if (partner.getMailingState() == null || partner.getMailingState().trim().length() <= 0) {
				partner.setMailingState("  ");
			}
			if (partner.getMailingCity() == null || partner.getMailingCity().trim().length() <= 0) {
				partner.setMailingCity("  ");
			}
		}

		List terminalCountryCode = partnerManager.getCountryCode(partner.getTerminalCountry());
		if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
			partner.setTerminalCountryCode(terminalCountryCode.get(0).toString());
			tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
		}

		List mailIngCountryCode = partnerManager.getCountryCode(partner.getMailingCountry());

		if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
			partner.setMailingCountryCode(mailIngCountryCode.get(0).toString());
			mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
		}

		if (partner.getBillingCountry() == null || partner.getBillingCountry().trim().equals("")) {
			partner.setBillingCountry("");
			partner.setBillingCountryCode("");
		}

		if (partner.getTerminalCountry() == null || partner.getTerminalCountry().trim().equals("")) {
			partner.setTerminalCountry("");
			partner.setTerminalCountryCode("");
		}
		if (partner.getMailingCountry() == null || partner.getMailingCountry().trim().equals("")) {
			partner.setMailingCountry("");
			partner.setMailingCountryCode("");
		}
		if (partner.getBillingState() == null) {
			partner.setBillingState("");
		}

		if (partner.getTerminalCity() == null) {
			partner.setTerminalCity("");
		}
		if (partner.getTerminalCountryCode() == null) {
			partner.setTerminalCountryCode("");
		}
		if (partner.getTerminalState() == null) {
			partner.setTerminalState("");
		}
		if(partnerPrefix !=null || !partnerPrefix.equalsIgnoreCase(""))
		{
			partner.setPartnerPrefix(partnerPrefix);
		}
		else
		{
			partner.setPartnerPrefix("");
		}
		if(partner.getBillingEmail()!=null)
		partner.setBillingEmail(partner.getBillingEmail().trim());
		if(partner.getMailingEmail()!=null)
		partner.setMailingEmail(partner.getMailingEmail().trim());
		if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
			partner.setCorpID("VOER");
		}
		partner = partnerPublicManager.save(partner);

		if (isNew) {
			partnerPrivate = new PartnerPrivate();
			partnerPrivate.setStatus(partner.getStatus());
			partnerPrivate.setLastName(partner.getLastName());
			partnerPrivate.setPartnerCode(partner.getPartnerCode());
			partnerPrivate.setPartnerPublicId(partner.getId());
			if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
				partnerPrivate.setCorpID("VOER");
			}else{
				partnerPrivate.setCorpID(sessionCorpID);
			}
			if (compDiv != null) {
				partnerPrivate.setCompanyDivision(compDiv);
			}
			if(creditTerm !=null || !creditTerm.equalsIgnoreCase(""))
			{
				partnerPrivate.setCreditTerms(creditTerm);
			}
			else
			{
				partnerPrivate.setCreditTerms("");
			}

			partnerPrivate.setCreatedBy(partner.getCreatedBy());
			partnerPrivate.setCreatedOn(new Date());
			partnerPrivate.setUpdatedBy(partner.getUpdatedBy());
			partnerPrivate.setUpdatedOn(new Date());			

			partnerPrivate = partnerPrivateManager.save(partnerPrivate);
			try{
				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
				 	createPartnerAccountRef(partner,partnerPrivate , "R");
				 	createPartnerAccountRef(partner,partnerPrivate , "P");
				}
				 
			    }catch (Exception e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
			    	 e.printStackTrace(); 
				}
		}
		if (!isNew) {
			partnerPrivate = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partner.getPartnerCode()).get(0);
			if (compDiv != null) {
				partnerPrivate.setCompanyDivision(compDiv);
			}
			partnerPrivate.setStatus(partner.getStatus());
			if(creditTerm !=null || !creditTerm.equalsIgnoreCase(""))
			{
				partnerPrivate.setCreditTerms(creditTerm);
			}
			else
			{
				partnerPrivate.setCreditTerms("");
			}
			
			partnerPrivate = partnerPrivateManager.save(partnerPrivate);
			try{
				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
				 	createPartnerAccountRef(partner,partnerPrivate , "R");
				 	createPartnerAccountRef(partner,partnerPrivate , "P");
				}
				 
			    }catch (Exception e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
			    	 e.printStackTrace(); 
				}
		}

		bstates = customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
		tstates = customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
		mstates = customerFileManager.findDefaultStateList(partner.getMailingCountryCode(), sessionCorpID);
		try {
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
			String key = (isNew) ? "partner.added" : "partner.updated";
			saveMessage(getText(key));
		}
		hitflag = "1";
		formClose = "close";		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private Map<String, String>  prefixList = new HashMap<String, String>(); ;
	
	///#9684 - UTS_COMPANY_TYPE parameter is no longer used in RedSky/
	//private Map <String,String> companyUTSType;
	private Map <String,String> billingCurrency;
	private Map <String,String> movingCompanyType;
	private Map <String,String> UTSmovingCompanyType;
	private Map <String,String> pType;
	private Map <String,String> taxIdType;
	private Map<String, String> collectionList;
	 private Map<String, String> agentRequestStatus;
	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		pType= refMasterManager.findByParameter(sessionCorpID, "AgentType");
		//companyUTSType = refMasterManager.findByParameter(corpId, "UTS_Company_Type");
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		billingCurrency = refMasterManager.findCodeOnleByParameter(corpId, "DEFAULT_BILLING_CURRENCY");
		movingCompanyType = refMasterManager.findByParameter(corpId, "MovingCompanyType");
		UTSmovingCompanyType = refMasterManager.findByParameter(corpId, "UTSMovingCompanyType");
		driverAgencyList=refMasterManager.findByParameter(sessionCorpID,"DRIVERAGENCY"); 
		sale = refMasterManager.findUser(corpId, "ROLE_SALE_COORD");
		prefixList=refMasterManager.findByParameter(corpId, "PREFFIX");
		countryDesc = refMasterManager.findCountry(corpId, "COUNTRY");
		typeOfVendor = refMasterManager.findByParameter(corpId, "TYPEOFVENDOR");
		corp=refMasterManager.findByParameter(corpId, "CORP");
		vanlineAffiliations = refMasterManager.findByParameter(corpId, "VANLINEAFFILIATIONS");
		serviceLines = refMasterManager.findByParameter(corpId, "SERVICELINES");
		qualityCertifications = refMasterManager.findByParameter(corpId, "QUALITYCERTIFICATIONS");
		companyDivis = customerFileManager.findOpenCompanyDivision(sessionCorpID);
		partnerStatus = refMasterManager.findByParameter(corpId, "PARTNER_STATUS");
		fleet=refMasterManager.findByParameter(corpId, "FLEET");
		creditTerms = refMasterManager.findByParameter(corpId, "CrTerms");
		try{
		//creditTerms=CommonUtil.sortByKey(creditTerms);
		}catch(Exception e){e.printStackTrace();}
		paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
		driverType = refMasterManager.findByParameter(corpId, "DRIVER_TYPE");
		bankCode= refMasterManager.findByParameter(corpId, "bankCode");
		taxIdType= refMasterManager.findByParameter(corpId, "TAXID_TYPE");
		countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
		planCommissionList = refMasterManager.findByParameter(sessionCorpID, "COMMISSIONPLAN");
		collectionList = refMasterManager.findByParameter(sessionCorpID,"COLLECTION");
		vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
		actionType = new LinkedHashMap<String, String>();
		actionType.put("PP", "Private Party");
		actionType.put("AC", "Account");
		actionType.put("AG", "Agent");
		actionType.put("VN", "Vendor");
		actionType.put("CR", "Carrier");
		actionType.put("OO", "Owner Ops");	
		company= companyManager.findByCorpID(corpId).get(0);
		agentRequestStatus = refMasterManager.findByParameter(corpId, "AGENTREQUESTSTATUS");
		if(!corpId.equalsIgnoreCase("TSFT")){
			agentRequestStatus.remove("ReRequest");	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String mergePartnerConfirm() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
		
	}
	@SkipValidation
	public String convertPartner(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		getComboList(sessionCorpID);
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		//System.out.println("\n\n\n\n\n userCheck"+userCheck);
		int i=partnerPublicManager.updateConvertPartnerCorpid(userCheck);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	@SkipValidation
	public String checkPartnerCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		partnerCodeCount=partnerPublicManager.findPartnerCodeCount(userCheck);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private String partnerVanLineCode;
	private List vanLineList;
	@SkipValidation
	public String getVanLineCodeList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		vanLineList=partnerPublicManager.getVanLine(partnerVanLineCode,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	@SkipValidation
	public String mergePartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		StringBuffer query = new StringBuffer();
		Set partnerCorpidSet=new HashSet();
		getComboList(sessionCorpID);
		logQuery="";
		id = Long.parseLong(userCheckMaster);
		partnerPublic = partnerPublicManager.get(id);
		partnerPublic = createPartnerPublic(partnerPublic);

		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		if (partnerCorpid.indexOf(",") == 0) {
			partnerCorpid = partnerCorpid.substring(1);
		}
		if (partnerCorpid.lastIndexOf(",") == partnerCorpid.length() - 1) {
			partnerCorpid = partnerCorpid.substring(0, partnerCorpid.length() - 1);
		}
		String[] arrayData=partnerCorpid.split(",");  
		int arrayDataLength = arrayData.length;
  		for(int i=0;i<arrayDataLength;i++)
  		 {
  			partnerCorpidSet.add(arrayData[i]);
  		 }
		PartnerPublic partnerPublicOLD = partnerPublicManager.get(id);
		query = partnerPublicManager.updateMaregedParter(userCheck, partnerPublic.getPartnerCode(), sessionCorpID);
		String masterPartnerCode=partnerPublicOLD.getPartnerCode();
		String partnerCodewithoutMaster=userCheck.replaceAll(masterPartnerCode, "");
		try{
		partnerCodewithoutMaster=partnerCodewithoutMaster.trim();
		if (partnerCodewithoutMaster.indexOf(",") == 0) {
			partnerCodewithoutMaster = partnerCodewithoutMaster.substring(1);
		}
		if (partnerCodewithoutMaster.lastIndexOf(",") == partnerCodewithoutMaster.length() - 1) {
			partnerCodewithoutMaster = partnerCodewithoutMaster.substring(0, partnerCodewithoutMaster.length() - 1);
		}
		}catch(Exception e){
			e.printStackTrace();	
		}
		if(partnerCorpidSet.size()>1){ 
			List partnerPrivateListForDiffCorpId = partnerPrivateManager.getPartnerPrivateId(masterPartnerCode);
			Iterator itPartnerPrivate = partnerPrivateListForDiffCorpId.iterator();
			while(itPartnerPrivate.hasNext()) {
				String idValue = itPartnerPrivate.next().toString();
				Long idPartnerPrivate=Long.parseLong(idValue);
				PartnerPrivate partnerPrivateForDiffCorpId = partnerPrivateManager.get(idPartnerPrivate);
				List isExistPPrivate = partnerPrivateManager.isExistPartnerPrivate(partnerPrivateForDiffCorpId.getCorpID(),  partnerPublic.getPartnerCode());
				if(isExistPPrivate.isEmpty()){
					createPartnerPrivate(partnerPrivateForDiffCorpId);
				}
			}
				List partnerPrivateListwithoutMaster = partnerPrivateManager.getPartnerPrivateId(partnerCodewithoutMaster);
				Iterator itPartnerPrivatewithoutMaster = partnerPrivateListwithoutMaster.iterator();
				while(itPartnerPrivatewithoutMaster.hasNext()) {
					String idValue = itPartnerPrivatewithoutMaster.next().toString();
					Long idPartnerPrivatewithoutMaster=Long.parseLong(idValue);
					PartnerPrivate partnerPrivatewithoutMaster = partnerPrivateManager.get(idPartnerPrivatewithoutMaster);
					List isExistPPrivatewithoutMaster = partnerPrivateManager.isExistPartnerPrivate(partnerPrivatewithoutMaster.getCorpID(),  partnerPublic.getPartnerCode());
					if(isExistPPrivatewithoutMaster.isEmpty()){
						createPartnerPrivate(partnerPrivatewithoutMaster);
					}		
			}
		//query = partnerPublicManager.updateMaregedParterPrivate(userCheck, partnerPublic.getPartnerCode(),partnerPublic.getId());
		}else{ 
			query = partnerPublicManager.updateMaregedParterSameCorpid(userCheck, partnerPublic.getPartnerCode(), sessionCorpID);	
		if(!partnerPrivateManager.getPartnerPrivateListByPartnerCode(partnerPublicOLD.getCorpID(), partnerPublicOLD.getPartnerCode()).isEmpty() && (partnerPrivateManager.getPartnerPrivateListByPartnerCode(partnerPublicOLD.getCorpID(), partnerPublicOLD.getPartnerCode()).get(0)!=null)){
			partnerPrivate = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(partnerPublicOLD.getCorpID(), partnerPublicOLD.getPartnerCode()).get(0);
			partnerPrivate = createPartnerPrivate(partnerPrivate);
		}else{
			partnerPrivate = new PartnerPrivate();
			partnerPrivate.setCorpID(sessionCorpID);
			partnerPrivate = createPartnerPrivate(partnerPrivate);
		}
		
		}
		query = query.append(partnerPublicManager.updatePartnerVanLineRef(userCheck, partnerPublic.getPartnerCode(), partnerPublic.getCorpID(),partnerCodewithoutMaster,masterPartnerCode));
		query = query.append(partnerPublicManager.updatePartnerVanLineRef(userCheck, partnerPublic.getPartnerCode(), partnerPublic.getCorpID(),partnerCodewithoutMaster,masterPartnerCode));
		
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		for(int i = 0; i < arrayLength; i++) {
			String partnerCode = arrayid[i];
			/*
			List partnerPrivateListForDiffCorpId = partnerPrivateManager.getPartnerPrivate(partnerCode);
			Iterator itPartnerPrivate = partnerPrivateListForDiffCorpId.iterator();
			while(itPartnerPrivate.hasNext()) {
				PartnerPrivate partnerPrivateForDiffCorpId = (PartnerPrivate)itPartnerPrivate.next();
				List isExistPPrivate = partnerPrivateManager.isExistPartnerPrivate(partnerPrivateForDiffCorpId.getCorpID(), partnerPrivate.getPartnerCode());
				if(isExistPPrivate.isEmpty()){
					createPartnerPrivate(partnerPrivateForDiffCorpId);
				}
			}
			*/
			List contPolicyList = partnerPublicManager.getContPolicyByPartnerCode(partnerCode, sessionCorpID);
			Iterator itContPolicy = contPolicyList.iterator();
			while(itContPolicy.hasNext()) {
				ContractPolicy contractPolicy = (ContractPolicy)itContPolicy.next();
				List isExistContractPolicy = partnerPublicManager.isExistContractPolicy(contractPolicy.getCorpID(), partnerPublic.getPartnerCode());
				if(isExistContractPolicy.isEmpty()){
					createContractPolicy(contractPolicy);
				}
			}
			
			List accProfileList = partnerPublicManager.getAccProfileByPartnerCode(partnerCode, sessionCorpID);
			Iterator itAccProfile = accProfileList.iterator();
			while(itAccProfile.hasNext()) {
				AccountProfile accountProfile = (AccountProfile)itAccProfile.next();
				List isExistAccProfile = partnerPublicManager.isExistAccProfile(accountProfile.getCorpID(), partnerPublic.getPartnerCode());
				if(isExistAccProfile.isEmpty()){
					createAccountProfile(accountProfile);
				}
				
			}
			
			List accContactList = partnerPublicManager.getAccContactByPartnerCode(partnerCode, sessionCorpID);
			Iterator itAccContact = accContactList.iterator();
			while(itAccContact.hasNext()) {
				AccountContact accountContact = (AccountContact)itAccContact.next();
				List isExistAccCont = partnerPublicManager.isExistAccContact(accountContact.getCorpID(), partnerPublic.getPartnerCode());
				if(isExistAccCont.isEmpty()){
					createAccountContact(accountContact);
				}
			}
			
			List accRefList = partnerPublicManager.getAccLineRefByPartnerCode(partnerCode, sessionCorpID);
			Iterator itAccRef = accRefList.iterator();
			while(itAccRef.hasNext()) {
				PartnerAccountRef partnerAccountRef = (PartnerAccountRef) itAccRef.next();
				List isExistAccRef = partnerAccountRefManager.checkCompayCode(partnerAccountRef.getCompanyDivision(), partnerAccountRef.getCorpID(), partnerPublic.getPartnerCode());
				if(isExistAccRef.isEmpty()){
					createPartnerAccountRef(partnerAccountRef);
				}
			}

			List agentBaseList = partnerPublicManager.getAgentBaseByPartnerCode(partnerCode, sessionCorpID);
			Iterator itAgentBase = agentBaseList.iterator();
			while(itAgentBase.hasNext()) {
				AgentBase agentBase = (AgentBase) itAgentBase.next();
				List agentBaseSCACList = partnerPublicManager.getAgentBaseSCACByPartnerCode(partnerCode, agentBase.getBaseCode(), sessionCorpID);
				
				List isExisted = agentBaseManager.isExisted(agentBase.getBaseCode(), agentBase.getCorpID(), partnerPublic.getPartnerCode());
				if(isExisted.isEmpty()){
					agentBase = createAgentBase(agentBase);
				}
				
				Iterator itSCAC = agentBaseSCACList.iterator();
				while(itSCAC.hasNext()) {
					AgentBaseSCAC agentBaseSCAC = (AgentBaseSCAC) itSCAC.next();
					
					List isExistedSCAC = agentBaseSCACManager.isExisted(agentBase.getBaseCode(), partnerPublic.getPartnerCode(), agentBaseSCAC.getSCACCode(), agentBaseSCAC.getCorpID());
					if(isExistedSCAC.isEmpty()){
						createAgentBaseSCAC(agentBaseSCAC, agentBase); 
					}
				}
			}
		}
		query  = query.append(partnerPublicManager.updateCodes(userCheck, partnerPublic.getPartnerCode(), partnerPublic.getLastName(), sessionCorpID));
		
		saveMessage(getText("Selected partners has been merged successfully to new partner code "+partnerPublic.getPartnerCode()+".")); 
		hitflag="1";
		logQuery = query.toString();
		query = new StringBuffer();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	private PartnerPublic createPartnerPublic(PartnerPublic partnerPublic) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		PartnerPublic partnerPublicNew = new PartnerPublic();
		try {
			//BeanUtils.copyProperties(partnerPublicNew, partnerPublic);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(partnerPublicNew, partnerPublic); 
			partnerPublicNew.setId(null);
			partnerPublicNew.setCorpID("TSFT");

			String maxPartnerCode;
			try {
				if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
					maxPartnerCode = "T10001";
				} else {
					List maxCode = partnerPublicManager.findMaxByCode("T");

					maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
					if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
						maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
					}
				}
			} catch (Exception ex) {
				maxPartnerCode = "T10001";
				ex.printStackTrace();
			}

			partnerPublicNew.setPartnerCode(maxPartnerCode);
			partnerPublicNew.setAgentParent(maxPartnerCode);
			partnerPublicNew.setUpdatedOn(new Date());
			partnerPublicNew.setUpdatedBy("MERG_WIZ");
			partnerPublicNew.setCreatedOn(new Date());
			partnerPublicNew.setCreatedBy("MERG_WIZ");
			partnerPublicNew = partnerPublicManager.save(partnerPublicNew);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return partnerPublicNew;
	}

	private PartnerPrivate createPartnerPrivate(PartnerPrivate partnerPrivate) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		PartnerPrivate partnerPrivateNew = new PartnerPrivate();
		try {
			//BeanUtils.copyProperties(partnerPrivateNew, partnerPrivate);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(partnerPrivateNew, partnerPrivate); 
			
			partnerPrivateNew.setId(null);
			partnerPrivateNew.setLastName(partnerPublic.getLastName());
			partnerPrivateNew.setPartnerCode(partnerPublic.getPartnerCode());
			partnerPrivateNew.setStatus(partnerPublic.getStatus());
			partnerPrivateNew.setPartnerPublicId(partnerPublic.getId());
			partnerPrivateNew.setCorpID(partnerPrivate.getCorpID());
			partnerPrivateNew.setCreatedBy("MERG_WIZ");
			partnerPrivateNew.setCreatedOn(new Date());
			partnerPrivateNew.setUpdatedBy("MERG_WIZ");
			partnerPrivateNew.setUpdatedOn(new Date());
			partnerPrivateNew = partnerPrivateManager.save(partnerPrivateNew);
			try{
				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
				 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
				 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
				}
				 
			    }catch (Exception e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
			    	 e.printStackTrace(); 
				}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return partnerPrivateNew;
	}
	
	private ContractPolicy createContractPolicy(ContractPolicy contractPolicy){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ContractPolicy contractPolicyNew = new ContractPolicy();
		try {
			//BeanUtils.copyProperties(contractPolicyNew, contractPolicy);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(contractPolicyNew, contractPolicy); 
			contractPolicyNew.setId(null);
			contractPolicyNew.setPartnerCode(partnerPublic.getPartnerCode());
			contractPolicyNew = contractPolicyManager.save(contractPolicyNew);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return contractPolicyNew;
	}
	
	private AccountProfile createAccountProfile(AccountProfile accountProfile){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		AccountProfile accountProfileNew = new AccountProfile();
		try {
			//BeanUtils.copyProperties(accountProfileNew, accountProfile);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(accountProfileNew, accountProfile); 
			
			accountProfileNew.setId(null);
			accountProfileNew.setPartnerCode(partnerPublic.getPartnerCode());
			accountProfileNew = accountProfileManager.save(accountProfileNew);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return accountProfileNew;
	}
	
	private AccountContact createAccountContact(AccountContact accountContact){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		AccountContact accountContactNew = new AccountContact();
		try {
			//BeanUtils.copyProperties(accountContactNew, accountContact);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(accountContactNew, accountContact); 
			
			accountContactNew.setId(null);
			accountContactNew.setPartnerCode(partnerPublic.getPartnerCode());
			accountContactNew = accountContactManager.save(accountContactNew);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return accountContactNew;
	}
	
	private PartnerAccountRef createPartnerAccountRef(PartnerAccountRef partnerAccountRef) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		PartnerAccountRef partnerAccountRefNew  = new PartnerAccountRef();
		try {
			//BeanUtils.copyProperties(partnerAccountRefNew, partnerAccountRef);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(partnerAccountRefNew, partnerAccountRef); 
			
			partnerAccountRefNew.setId(null);
			partnerAccountRefNew.setPartnerCode(partnerPublic.getPartnerCode());
			partnerAccountRefNew = partnerAccountRefManager.save(partnerAccountRefNew);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return partnerAccountRefNew;
	}

	private AgentBase createAgentBase(AgentBase agentBase) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		AgentBase agentBaseNew = new AgentBase();
		try {
			//BeanUtils.copyProperties(agentBaseNew, agentBase);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(agentBaseNew, agentBase); 
			
			agentBaseNew.setId(null);
			agentBaseNew.setPartnerCode(partnerPublic.getPartnerCode());
			agentBaseNew = agentBaseManager.save(agentBaseNew);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return agentBaseNew;
	}
	
	private AgentBaseSCAC createAgentBaseSCAC(AgentBaseSCAC agentBaseSCAC, AgentBase agentBase) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		AgentBaseSCAC agentBaseSCACNew = new AgentBaseSCAC();
		try {
			//BeanUtils.copyProperties(agentBaseSCACNew, agentBaseSCAC);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(agentBaseSCACNew, agentBaseSCAC); 
			
			agentBaseSCACNew.setId(null);
			agentBaseSCACNew.setPartnerCode(partnerPublic.getPartnerCode());
			agentBaseSCACNew.setBaseCode(agentBase.getBaseCode());
			agentBaseSCACNew = agentBaseSCACManager.save(agentBaseSCACNew);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return agentBaseSCACNew;
	}
	private Boolean excludeFPU;	
	public String partnerReloSvcs(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerPublic=partnerPublicManager.get(id);
		excludeFPU=partnerPublicManager.getExcludeFPU(partnerPublic.getPartnerCode(),sessionCorpID);
		serviceRelos= refMasterManager.findServiceByJob("RLO",sessionCorpID, "SERVICE");
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		String rempTransPort=partnerPublic.getTransPort();
		Iterator mapIterator = serviceRelos.entrySet().iterator();
		int j=0;
		while (mapIterator.hasNext()) {
	    	  Map.Entry entry = (Map.Entry) mapIterator.next();
	          String key = (String) entry.getKey();
	          String value=(String) entry.getValue();
	          if(j==0)
				{
	        	  serviceRelos1.put(key, value);
					j=1;
				}
				else if(j==1){
					serviceRelos2.put(key, value);
					j=0;
				}
		
	   }
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String savePartnerReloSvcs(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String reloContact = partnerPublic.getReloContact();
		String reloContactEmail = partnerPublic.getReloContactEmail();
		String reloService = partnerPublic.getReloServices();
		
		if (reloService.equals("") || reloService.equals("#")) {
			reloService = "";
		} else {
			reloService = reloService.trim();
			if (reloService.indexOf("#") == 0) {
				reloService = reloService.substring(1);
			}
			if (reloService.lastIndexOf("#") == reloService.length() - 1) {
				reloService = reloService.substring(0, reloService.length() - 1);
			}
		}
		String transport="";
		
		serviceRelos= refMasterManager.findServiceByJob("RLO",sessionCorpID, "SERVICE");
		Iterator mapIterator = serviceRelos.entrySet().iterator();
		int j=0;
		while (mapIterator.hasNext()) {
	    	  Map.Entry entry = (Map.Entry) mapIterator.next();
	          String key = (String) entry.getKey();
	          String value=(String) entry.getValue();
	          if(j==0)	{
	        	  serviceRelos1.put(key, value);
	        	  j=1;
	          }else if(j==1){
					serviceRelos2.put(key, value);
					j=0;
	          }
		
	   }
		partnerPublic=partnerPublicManager.get(id);
		excludeFPU=partnerPublicManager.getExcludeFPU(partnerPublic.getPartnerCode(),sessionCorpID);
		PartnerPublic partnerPublicNew = new PartnerPublic();
		try{
			BeanUtilsBean beanUtilsPartnerPublic = BeanUtilsBean.getInstance();
			beanUtilsPartnerPublic.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsPartnerPublic.copyProperties(partnerPublicNew, partnerPublic);
	    		String serviceDoc=tempTransPort.trim().toString();
	    		if(serviceDoc.equals("")||serviceDoc.equals("#"))
	    		{	    	  	  }	    	  	 
	    		else
	    	  	  {
	    	  		serviceDoc=serviceDoc.trim();
	    	  	    if(serviceDoc.indexOf("#")==0)
	    	  		 {
	    	  	    	serviceDoc=serviceDoc.substring(1);
	    	  		 }
	    	  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1)
	    	  		 {
	    	  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
	    	  		 } 
	    	  	  } 
	    		serviceDoc=serviceDoc.replaceAll("#", ","); 
    		partnerPublicNew.setTransPort(serviceDoc);
			partnerPublicNew.setReloContact(reloContact);
			partnerPublicNew.setReloContactEmail(reloContactEmail);
			partnerPublicNew.setReloServices(reloService);
			partnerPublicNew.setUpdatedOn(new Date());
			partnerPublicNew.setUpdatedBy(getRequest().getRemoteUser());
			partnerPublic = partnerPublicManager.save(partnerPublicNew);
			hitflag = "1";
		}catch (Exception e) {
			e.printStackTrace();
		}
		saveMessage(getText("Relo Svcs has been updated successfully.")); 		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String updateDefaultChildAccount(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerPrivate=partnerPrivateManager.get(id);
		company=companyManager.findByCorpID(sessionCorpID).get(0);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private Boolean agentCheck;
	public String searchBankInfo(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if(agentCheck==null){
				agentCheck=false;
			}
			billingCurrency = refMasterManager.findCodeOnleByParameter(sessionCorpID, "DEFAULT_BILLING_CURRENCY");
			partnerBankInfoList = partnerBankInfoManager.findBankData(bankPartnerCode, sessionCorpID);
		} catch (Exception e) {
	    	 return "error";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	public String updateBankRecordAjax(){
		String fieldId=getRequest().getParameter("fieldId");
		String bankAccountNumber=getRequest().getParameter("bankAccountNumber");
		String currency=getRequest().getParameter("currency");
		String status=getRequest().getParameter("status");
		if(status==null){
			status="false";
		}
		try{
		partnerBankInformation =partnerBankInfoManager.get(Long.parseLong(fieldId));
		partnerBankInformation.setBankAccountNumber(bankAccountNumber);
		partnerBankInformation.setCurrency(currency);
		partnerBankInformation.setStatus(Boolean.parseBoolean(status));
		partnerBankInfoManager.save(partnerBankInformation);
		}catch(Exception e){e.printStackTrace();}
		return SUCCESS;
	}
	public String updatePrivateBankRecordAjax(){
		String fieldId=getRequest().getParameter("fieldId");
		String bankAccountNumber=getRequest().getParameter("bankAccountNumber");
		String currency=getRequest().getParameter("currency");
		String status=getRequest().getParameter("status");
		if(status==null){
			status="false";
		}
		try{
		partnerBankInformation =partnerBankInfoManager.get(Long.parseLong(fieldId));
		partnerBankInformation.setBankAccountNumber(bankAccountNumber);
		partnerBankInformation.setCurrency(currency);
		partnerBankInformation.setStatus(Boolean.parseBoolean(status));
		partnerBankInfoManager.save(partnerBankInformation);
		}catch(Exception e){}
		return SUCCESS;
	}
	
	public String saveBankInfo(){
		Map<String,String> bankInfo=new HashMap<String,String>();
		
		String[] idandValue=null;
		
		
		String key="";
		String value="";
		String tempVal="";
		if(bankNoList.trim().toString().length()>1 && currencyList.trim().toString().length()>1)
		{
		String[] arrayStr =bankNoList.split("~");
		   for (String string : arrayStr) {
			   idandValue= string.split(":");
			   key=string.split(":")[0].toString().trim().replace("ban", "");
			   value=string.split(":")[1].toString().trim();
			   if(bankInfo.containsKey(key)){
				   tempVal=bankInfo.get(key)+"";
				   value=tempVal+"~"+value;
				   bankInfo.put(key, value);
			   }else{
				   bankInfo.put(key, value);
			   }
		   }
		   arrayStr =currencyList.split("~");
		   for (String string : arrayStr) {
			   idandValue= string.split(":");
			   key=string.split(":")[0].toString().trim().replace("cur", "");
			   value=string.split(":")[1].toString().trim();
			   if(bankInfo.containsKey(key)){
				   tempVal=bankInfo.get(key)+"";
				   value=tempVal+"~"+value;
				   bankInfo.put(key, value);
			   }else{
				   bankInfo.put(key, value);
			   }	   
		   }
		   arrayStr =checkList.split("~");
		   for (String string : arrayStr) {
			   idandValue= string.split(":");
			   key=string.split(":")[0].toString().trim().replace("chk", "");
			   value=string.split(":")[1].toString().trim();
			   if(bankInfo.containsKey(key)){
				   tempVal=bankInfo.get(key)+"";
				   value=tempVal+"~"+value;
				   bankInfo.put(key, value);
			   }else{
				   bankInfo.put(key, value);
			   }	   
		   }
		
		   
		}
		   Set st= new HashSet<String>();
		   for(String str:newlineid.split("~")){
			   st.add(str);
		   }
		   if(st.isEmpty()){
			   st=null;
		   }
		 for(Map.Entry<String, String> rec:bankInfo.entrySet()){
			 if(rec.getValue().split("~")[1]!=null && !rec.getValue().split("~")[1].toString().equalsIgnoreCase("")){
			 if(st!=null && !st.isEmpty() && st.contains(rec.getKey())){
				 partnerBankInformation = new PartnerBankInformation(); 
				 partnerBankInformation.setCreatedBy(getRequest().getRemoteUser());
				 partnerBankInformation.setCreatedOn(new Date());
				 
			 }else{
				 partnerBankInformation = partnerBankInfoManager.get(Long.parseLong(rec.getKey()));
			 }
				partnerBankInformation.setCorpId(sessionCorpID);		        		
				partnerBankInformation.setUpdatedBy(getRequest().getRemoteUser());
				partnerBankInformation.setUpdatedOn(new Date());
				partnerBankInformation.setBankAccountNumber(rec.getValue().split("~")[0]);
				partnerBankInformation.setCurrency(rec.getValue().split("~")[1]);
				partnerBankInformation.setStatus(Boolean.parseBoolean(rec.getValue().split("~")[2]));
				partnerBankInformation.setPartnerCode(bankPartnerCode);
				partnerBankInfoManager.save(partnerBankInformation);
			 }
			 
		 }
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	 private String corpid;
	 private Long agentPartnerId;
     private String lastNameFlag;
     private List agentlist;
     private EmailSetupManager emailSetupManager;
     private String email;
     private String agentEditURL;
     
 	public String writeByteArraysToFile(String fileName, byte[] content,String userName) throws IOException {
		/*String uploadDir = "/usr/local/redskydoc/VOER/"+ userName+ "/";
		String sessionCorpID = "VOER";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + userName + "/");*/
		
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
		System.out.println("upload dir"+uploadDir);
        File file = new File(uploadDir);
        if (!file.exists()) {
        	file.mkdirs();
		}
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(uploadDir+fileName));
        writer.write(content);
        writer.flush();
        writer.close();
        
        return file.getAbsolutePath() + Constants.FILE_SEP + fileName;
 
    }
     
	@SkipValidation
	public void saveAgentPartnerData(PartnerPublic partnerPublics,String corpid) throws Exception {
		
		agentRequest=agentRequestManager.getByID(partnerPublics.getId());
		Map<String,String> partnerPublicData = new HashMap<String,String>();
		Map<String,String> agentRequestData = new HashMap<String,String>();
		partnerPublicData = BeanUtils.describe(partnerPublics);
		PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
		if(agentRequest.getId()!=null && partnerPublics.getCorpID().equals(agentRequest.getCorpID()))
		{
			Long oldId = agentRequest.getId(); 
			String craetedBy=agentRequest.getCreatedBy();
			Date agentCreatedOn=agentRequest.getCreatedOn();
			//Long partnerId=agentRequest.getPartnerId();
			//String partnerCode=agentRequest.getPartnerCode();
			String comapnylogo=agentRequest.getCompanyLogo();
				agentRequestData = BeanUtils.describe(agentRequest);
				 Iterator keysIt=partnerPublicData.entrySet().iterator();
				 String changeFields="";
				List<String> list = new ArrayList();
					while (keysIt.hasNext()) {
						Map.Entry entry = (Map.Entry) keysIt.next();
						String key = (String) entry.getKey(); 
						
								
					if(partnerPublicData.containsKey(key) && agentRequestData.containsKey(key)){
						if((partnerPublicData.get(key) !=null && agentRequestData.get(key)!=null && (!(partnerPublicData.get(key).toString().equalsIgnoreCase(agentRequestData.get(key).toString())))) 
								|| ((partnerPublicData.get(key) != null || (!(partnerPublicData.get(key).trim().equals("")))) && ((agentRequestData.get(key)==null) &&  ((agentRequestData.get(key).trim().equals("")))))){
							if(!(key.trim().equalsIgnoreCase("corpID")) && !(key.trim().equalsIgnoreCase("updatedOn")) && !(key.trim().equalsIgnoreCase("updatedby")) && !(key.trim().equalsIgnoreCase("createdBy")) && !(key.trim().equalsIgnoreCase("createdOn")) && !(key.trim().equalsIgnoreCase("id"))  && !(key.trim().equalsIgnoreCase("status")) )
								if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
									  beanUtilsBean.setProperty(agentRequest,key.toString().trim(),beanUtilsBean.getProperty(partnerPublics, key.trim()));
									}
								changeFields=changeFields+""+key+",";
							
						}
					}
					}
			   
				if (changeFields.endsWith(",")) {
					changeFields = changeFields.substring(0, changeFields.length() - 1);
					System.out.println(changeFields);
					} 
			
		
					
/*				if(partnerPublicData.containsKey(key) && ((partnerPublicData.get(key) !=null))){
				if(key !=null &&  (!(key.trim().equals(""))) && (!(key.trim().equalsIgnoreCase("id"))) && (!(key.trim().equalsIgnoreCase("CorpID"))) && (!(key.trim().equalsIgnoreCase("status"))) && (!(key.trim().equalsIgnoreCase("isAgent"))) ) {
				if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
				  beanUtilsBean.setProperty(agentRequest,key.toString().trim(),beanUtilsBean.getProperty(partnerPublics, key.trim()));
				}
				}
					}
*/						
					
                String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
    			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );  
    			if(imageDataFileLogo !=null && (!(imageDataFileLogo.trim().equals("")))) {
    				
    				imageDataFileLogo= imageDataFileLogo.substring(imageDataFileLogo.indexOf("base64,")+7);
    				
    				byte[] base64EncodedData = Base64.decodeBase64(imageDataFileLogo.getBytes());
    				 
    		        try {
    		      String  filePath =writeByteArraysToFile(UUID.randomUUID().toString()+".jpg", base64EncodedData,"admin");
    		      agentRequest.setCompanyLogo(filePath);
    				} catch (IOException ex) {
    					// TODO Auto-generated catch block
    					ex.printStackTrace();
    				} 
    		        }
    			try {
    				 uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
    				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
    				if (file != null) {

    					if (file.length() > 41943040) {
    						addActionError(getText("maxLengthExceeded"));
    						//return INPUT;
    					}

    					

    					//String rightUploadDir = uploadDir.replace("redsky", "userData");

    					File dirPath = new File(uploadDir);
    					if (!dirPath.exists()) {
    						dirPath.mkdirs();
    					}

    					if (file.exists()) {
    						InputStream stream = new FileInputStream(file);

    						OutputStream bos = new FileOutputStream(uploadDir + fileFileName);
    						int bytesRead = 0;
    						byte[] buffer = new byte[8192];

    						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
    							bos.write(buffer, 0, bytesRead);
    						}

    						bos.close();
    						stream.close();

    						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
    						agentRequest.setLocation1(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
    					}
    				}
    				if (file1 != null) {
    					if (file1.length() > 41943040) {
    						addActionError(getText("maxLengthExceeded"));
    						//return INPUT;
    					}

    					/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

    					String rightUploadDir = uploadDir.replace("redsky", "userData");*/

    					File dirPath = new File(uploadDir);
    					if (!dirPath.exists()) {
    						dirPath.mkdirs();
    					}

    					if (file1.exists()) {
    						InputStream stream = new FileInputStream(file1);

    						OutputStream bos = new FileOutputStream(uploadDir + file1FileName);
    						int bytesRead = 0;
    						byte[] buffer = new byte[8192];

    						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
    							bos.write(buffer, 0, bytesRead);
    						}

    						bos.close();
    						stream.close();

    						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
    						agentRequest.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
    					}
    				}

    				if (file2 != null) {
    					if (file2.length() > 41943040) {
    						addActionError(getText("maxLengthExceeded"));
    						//return INPUT;
    					}

    					/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

    					String rightUploadDir = uploadDir.replace("redsky", "userData");*/

    					File dirPath = new File(uploadDir);
    					if (!dirPath.exists()) {
    						dirPath.mkdirs();
    					}

    					if (file2.exists()) {
    						InputStream stream = new FileInputStream(file2);

    						OutputStream bos = new FileOutputStream(uploadDir + file2FileName);
    						int bytesRead = 0;
    						byte[] buffer = new byte[8192];

    						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
    							bos.write(buffer, 0, bytesRead);
    						}

    						bos.close();
    						stream.close();

    						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
    						agentRequest.setLocation3(dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
    					}
    				}

    				if (file3 != null) {
    					if (file3.length() > 41943040) {
    						addActionError(getText("maxLengthExceeded"));
    						//return INPUT;
    					}

    					/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

    					String rightUploadDir = uploadDir.replace("redsky", "userData");*/

    					File dirPath = new File(uploadDir);
    					if (!dirPath.exists()) {
    						dirPath.mkdirs();
    					}

    					if (file3.exists()) {
    						InputStream stream = new FileInputStream(file3);

    						OutputStream bos = new FileOutputStream(uploadDir + file3FileName);
    						int bytesRead = 0;
    						byte[] buffer = new byte[8192];

    						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
    							bos.write(buffer, 0, bytesRead);
    						}

    						bos.close();
    						stream.close();

    						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
    						agentRequest.setLocation4(dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
    					}
    				}

    			} catch (Exception ex) {
    				System.out.println("\n\n\n\n\n---- >>>> error while uploading photograph" + ex);
    				ex.printStackTrace();
    			}
    			agentRequest.setChangeRequestColumn(changeFields);
    			agentRequest.setCorpID(agentRequest.getCorpID());
                agentRequest.setCreatedBy(craetedBy);
                agentRequest.setUpdatedBy(getRequest().getRemoteUser());
                agentRequest.setId(oldId);
                agentRequest.setIsAgent(true);
                agentRequest.setCounter("0");
                //agentRequest.setPartnerId(partnerId);
                //agentRequest.setPartnerCode(partnerCode);
                agentRequest.setCreatedOn(agentCreatedOn);
                agentRequest.setUpdatedOn(new Date());
               if (partnerPublics.getId() == null){
               	 agentRequest.setStatus(partnerPublics.getStatus()); 
                }else{
                   agentRequest.setStatus("Edit");
                }
               
               if (partnerPublics.getBillingCountryCode() == null) {
            	   agentRequest.setBillingCountryCode("");
				}
               if (partnerPublics.getBillingState()== null) {
            	   agentRequest.setBillingState("");
				}
               if (partnerPublics.getLastName()== null) {
            	   agentRequest.setLastName("");
				}
               if (partnerPublics.getAliasName()== null) {
            	   agentRequest.setAliasName("");
				}
               if (partnerPublics.getBillingCountry()== null) {
            	   agentRequest.setBillingCountry("");
				}
              agentRequestManager.save(agentRequest);
              String url=getRequest().getRequestURL().toString();
  			String  requestURL=url.substring(0,url.lastIndexOf("/"));
  			String msgText1="";
  			String subject="";
  			 String link = "";
  			 String website="";
  			 website=requestURL;
  			 website=website+"/pricingList.html?";
  				link = "<a href=\""+website+"\"></a>";
  				msgText1 = msgText1+"<br><br>http://localhost:8080/redsky/searchAgent.html"+link+"";
  				String messageText1 = "<html><table>"+msgText1+"</table></html>";
  	     String key = "Agent update request has been received. We will verify the changes and get back to you. To track the changes, use the Agent Request tab."; 
  	     saveMessage(getText(key));	 
		}
			 
	else{
		agentRequest=new AgentRequest();
		agentRequestData = BeanUtils.describe(agentRequest);
		partnerPublic=partnerPublicManager.get(partnerPublics.getId());
		Map<String,String> partnerPublicOldData = new HashMap<String,String>();
		partnerPublicOldData= BeanUtils.describe(partnerPublic);
		 Iterator keysIt=partnerPublicData.entrySet().iterator(); 
		 String changeFields="";
			List<String> list = new ArrayList();
				while (keysIt.hasNext()) {
					
					Map.Entry entry = (Map.Entry) keysIt.next();
					String key = (String) entry.getKey(); 
					
							
					if(partnerPublicData.containsKey(key) && ((partnerPublicData.get(key) !=null))){
						if(key !=null &&  (!(key.trim().equals(""))) && (!(key.trim().equalsIgnoreCase("id"))) && (!(key.trim().equalsIgnoreCase("CorpID"))) && (!(key.trim().equalsIgnoreCase("status"))) && (!(key.trim().equalsIgnoreCase("isAgent"))) ) {
						if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
									  beanUtilsBean.setProperty(agentRequest,key.toString().trim(),beanUtilsBean.getProperty(partnerPublics, key.trim()));
								}
							
						
					}
				}
					
					if(partnerPublicData.containsKey(key) && partnerPublicOldData.containsKey(key)){
						if((partnerPublicData.get(key) !=null && partnerPublicOldData.get(key)!=null && (!(partnerPublicData.get(key).toString().equalsIgnoreCase(partnerPublicOldData.get(key).toString())))) 
								|| ((partnerPublicData.get(key) != null && (!(partnerPublicData.get(key).trim().equals("")))) && ((partnerPublicOldData.get(key)==null) ||  ((partnerPublicOldData.get(key).trim().equals("")))))){							if(key !=null &&  (!(key.trim().equals(""))) && (!(key.trim().equalsIgnoreCase("id"))) && (!(key.trim().equalsIgnoreCase("CorpID"))) && (!(key.trim().equalsIgnoreCase("status")))  && (!(key.trim().equalsIgnoreCase("isAgent")))) {
											if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
								}
							changeFields=changeFields+""+key+",";
						
					}
				}
				}}
				
		   
				
			if (changeFields.endsWith(",")) {
				changeFields = changeFields.substring(0, changeFields.length() - 1);
				System.out.println(changeFields);
				} 
		/*	while (keysIt.hasNext()) {
				try {
				Map.Entry entry = (Map.Entry) keysIt.next();
				String key = (String) entry.getKey(); 
		
		if(partnerPublicData.containsKey(key) && ((partnerPublicData.get(key) !=null))){
		if(key !=null &&  (!(key.trim().equals(""))) && (!(key.trim().equalsIgnoreCase("id"))) && (!(key.trim().equalsIgnoreCase("CorpID"))) && (!(key.trim().equalsIgnoreCase("status")))  && (!(key.trim().equalsIgnoreCase("isAgent")))) {
		if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
		  beanUtilsBean.setProperty(agentRequest,key.toString().trim(),beanUtilsBean.getProperty(partnerPublics, key.trim()));
		}
		}
			}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}*/
			if (partnerPublics.getId() == null){
		    	 agentRequest.setStatus("New"); 
		     }else{
		        agentRequest.setStatus("Edit");
		     }
			agentRequest.setChangeRequestColumn(changeFields);
			 agentRequest.setPartnerId(partnerPublics.getId());
			 agentRequest.setIsAgent(true);
			 agentRequest.setCorpID(corpid);
			 String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
 			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );  
 			if(imageDataFileLogo !=null && (!(imageDataFileLogo.trim().equals("")))) {
 				
 				imageDataFileLogo= imageDataFileLogo.substring(imageDataFileLogo.indexOf("base64,")+7);
 				
 				byte[] base64EncodedData = Base64.decodeBase64(imageDataFileLogo.getBytes());
 				 
 		        try {
 		      String  filePath =writeByteArraysToFile(UUID.randomUUID().toString()+".jpg", base64EncodedData,"admin");
 		      agentRequest.setCompanyLogo(filePath);
 				} catch (IOException ex) {
 					// TODO Auto-generated catch block
 					ex.printStackTrace();
 				} 
 		        }
			 try {
				 uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
				if (file != null) {

					if (file.length() > 41943040) {
						addActionError(getText("maxLengthExceeded"));
						//return INPUT;
					}

					

					//String rightUploadDir = uploadDir.replace("redsky", "userData");

					File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
						dirPath.mkdirs();
					}

					if (file.exists()) {
						InputStream stream = new FileInputStream(file);

						OutputStream bos = new FileOutputStream(uploadDir + fileFileName);
						int bytesRead = 0;
						byte[] buffer = new byte[8192];

						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
							bos.write(buffer, 0, bytesRead);
						}

						bos.close();
						stream.close();

						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
						agentRequest.setLocation1(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
					}
				}
				if (file1 != null) {
					if (file1.length() > 41943040) {
						addActionError(getText("maxLengthExceeded"));
						//return INPUT;
					}

					/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

					String rightUploadDir = uploadDir.replace("redsky", "userData");*/

					File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
						dirPath.mkdirs();
					}

					if (file1.exists()) {
						InputStream stream = new FileInputStream(file1);

						OutputStream bos = new FileOutputStream(uploadDir + file1FileName);
						int bytesRead = 0;
						byte[] buffer = new byte[8192];

						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
							bos.write(buffer, 0, bytesRead);
						}

						bos.close();
						stream.close();

						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
						agentRequest.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
					}
				}

				if (file2 != null) {
					if (file2.length() > 41943040) {
						addActionError(getText("maxLengthExceeded"));
						//return INPUT;
					}

					/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

					String rightUploadDir = uploadDir.replace("redsky", "userData");*/

					File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
						dirPath.mkdirs();
					}

					if (file2.exists()) {
						InputStream stream = new FileInputStream(file2);

						OutputStream bos = new FileOutputStream(uploadDir + file2FileName);
						int bytesRead = 0;
						byte[] buffer = new byte[8192];

						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
							bos.write(buffer, 0, bytesRead);
						}

						bos.close();
						stream.close();

						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
						agentRequest.setLocation3(dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
					}
				}

				if (file3 != null) {
					if (file3.length() > 41943040) {
						addActionError(getText("maxLengthExceeded"));
						//return INPUT;
					}

					/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

					String rightUploadDir = uploadDir.replace("redsky", "userData");*/

					File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
						dirPath.mkdirs();
					}

					if (file3.exists()) {
						InputStream stream = new FileInputStream(file3);

						OutputStream bos = new FileOutputStream(uploadDir + file3FileName);
						int bytesRead = 0;
						byte[] buffer = new byte[8192];

						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
							bos.write(buffer, 0, bytesRead);
						}

						bos.close();
						stream.close();

						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
						agentRequest.setLocation4(dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
					}
				}

			} catch (Exception ex) {
				System.out.println("\n\n\n\n\n---- >>>> error while uploading photograph" + ex);
				ex.printStackTrace();
			}
			 
			 agentRequest.setCreatedBy(getRequest().getRemoteUser());
		     agentRequest.setUpdatedBy(getRequest().getRemoteUser());
		     agentRequest.setCreatedOn(new Date());
		     agentRequest.setUpdatedOn(new Date());
		     agentRequest.setCounter("0");
		     if (partnerPublics.getBillingCountryCode() == null) {
          	   agentRequest.setBillingCountryCode("");
				}
             if (partnerPublics.getBillingState()== null) {
          	   agentRequest.setBillingState("");
				}
             if (partnerPublics.getLastName()== null) {
          	   agentRequest.setLastName("");
				}
             if (partnerPublics.getAliasName()== null) {
          	   agentRequest.setAliasName("");
				}
             if (partnerPublics.getBillingCountry()== null) {
          	   agentRequest.setBillingCountry("");
				}
		 	 agentRequest=agentRequestManager.save(agentRequest);
		    String url=getRequest().getRequestURL().toString();
			String  requestURL=url.substring(0,url.lastIndexOf("/"));
			String msgText1="";
			String subject="";
			 String link = "";
			 String website="";
			 website=requestURL;
			 website=website+"/pricingList.html?";
				link = "<a href=\""+website+"\"></a>";
				msgText1 = msgText1+"<br><br>http://localhost:8080/redsky/searchAgent.html"+link+"";
				String messageText1 = "<html><table>"+msgText1+"</table></html>";
	            String key = "Agent update request has been received. We will verify the changes and get back to you. To track the changes, use the Agent Request tab.";
	            saveMessage(getText(key));	
               }
             }
	@SkipValidation
	public String agentPublicList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		getRequest().setAttribute("soLastName","");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		if (paramView.equalsIgnoreCase("View")) {
			getRequest().getSession().setAttribute("paramView", paramView);
		}
		getComboList(sessionCorpID);
		agentRequest = new AgentRequest();
		
		agentRequest.setIsAgent(true);
		
		isIgnoreInactive = true;
		//agentList = new HashSet(agentRequestManager.getAgentRequestList("", sessionCorpID,"","","","","","","",true,isIgnoreInactive));
		//agentList = new HashSet(agentRequestManager.getAgentRequestList(partnerCode, sessionCorpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, agentRequest.getIsAgent(),isIgnoreInactive));
		
		//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String name;
	private Long agentCountId;
	
	@SkipValidation
	 public String countResendRequest()
	   {
		System.out.println("zsxdfgvhbjk----------"+counter);
		System.out.println("zsxdfgvhbjk-eeeeee---------"+agentCountId);
		agentRequest=agentRequestManager.get(agentCountId);
		agentRequest.setCounter(counter);
		
		 return SUCCESS;
	   }
	 public String editNewNotesForAgentRequest(){
		 

			try {/*
			getComboList(sessionCorpID);
			accountNotesFor = "SO";
			checkCF="SO";
			checkSO="SO";		
			if (id1 != null) {
				linkFileList=notesManager.getMyFileList(id);
				// linkFile=notesManager.getMyFileList(id);
				if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
					linkFile = linkFileList.get(0).toString();
					if( linkFile!= null && !(linkFile.equals(""))){
						myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
					}
				}
				if(sessionCorpID.equalsIgnoreCase("TSFT")){
				serviceOrder = serviceOrderManager.getForOtherCorpid(id1);
				}else{
					serviceOrder = serviceOrderManager.get(id1);	
				}
				trackingStatus = trackingStatusManager.get(id1);
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						bookingAgentFlag ="BA";
					}else if(checkAgent.equalsIgnoreCase("NA")){
						networkAgentFlag ="NA";
					}else if(checkAgent.equalsIgnoreCase("OA")){
						originAgentFlag ="OA";
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						subOriginAgentFlag ="SOA";
					}else if(checkAgent.equalsIgnoreCase("DA")){
						destAgentFlag ="DA";
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						subDestAgentFlag ="SDA";
					} 
				  }
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName", customerFile.getLastName());
				notes = notesManager.get(id);
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
				maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
				minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
				countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
				maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
				minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
				countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
			} else {
				if(sessionCorpID.equalsIgnoreCase("TSFT")){
				serviceOrder = serviceOrderManager.getForOtherCorpid(id);
				}else{
					serviceOrder = serviceOrderManager.get(id);
				}
				trackingStatus = trackingStatusManager.get(id);
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						bookingAgentFlag ="BA";
					}else if(checkAgent.equalsIgnoreCase("NA")){
						networkAgentFlag ="NA";
					}else if(checkAgent.equalsIgnoreCase("OA")){
						originAgentFlag ="OA";
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						subOriginAgentFlag ="SOA";
					}else if(checkAgent.equalsIgnoreCase("DA")){
						destAgentFlag ="DA";
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						subDestAgentFlag ="SDA";
					} 
				}
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName", customerFile.getLastName());
				notes = new Notes();
				notes.setNoteStatus("NEW");
				notes.setCorpID(serviceOrder.getCorpID());
				notes.setNotesId(serviceOrder.getShipNumber());
				notes.setCustomerNumber(customerFile.getSequenceNumber());
				notes.setNoteType("Service Order");
				notes.setRemindTime("00:00");
				notes.setCreatedOn(new Date());
				notes.setSystemDate(new Date());
				notes.setUpdatedOn(new Date());
				notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
				notes.setRemindInterval("15");
				//notes.setForwardDate(new Date());
				notes.setNoteSubType(subType);
				notes.setNotesKeyId(serviceOrder.getId());
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Service Order", "NOTESUBTYPE");
			}
			if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
				memoIntegrationFlag="Y";
			}else{
				memoIntegrationFlag="N";
			}				  		
			
			List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
			if(shipNumberList!=null && !shipNumberList.isEmpty()){
			Iterator it = shipNumberList.iterator();
	         while (it.hasNext()) {
	        	String agentFlag=(String)it.next();
	        	if(agentFlag.equalsIgnoreCase("BA")){
					bookingAgFlag ="BA";
				}else if(agentFlag.equalsIgnoreCase("NA")){
					networkAgFlag ="NA";
				}else if(agentFlag.equalsIgnoreCase("OA")){
					originAgFlag ="OA";
				}else if(agentFlag.equalsIgnoreCase("SOA")){
					subOriginAgFlag ="SOA";
				}else if(agentFlag.equalsIgnoreCase("DA")){
					destAgFlag ="DA";
				}else if(agentFlag.equalsIgnoreCase("SDA")){
					subDestAgFlag ="SDA";
				} 
	         }
		    }
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			supplier=notesManager.findSupplier(customerFile.getId());
			System.out.println("supplier is : "+supplier);
			noteForNextPrev = "ServiceOrder";
			List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
			signatureNote = signatureNoteList.get(0).toString();
			*/}catch(Exception e){
				e.printStackTrace();
			}
			return SUCCESS;
		
	 }
	 @SkipValidation
		public String updatelist() {/*
			//System.out.println(subType);
			getComboList(sessionCorpID);
			//if(subType == null){
			subType = "";
			//}
			if (noteFor.equals("agentQuotes")) {
				notess = notesListFromCustomer(subType);
			}
			if (noteFor.equals("agentQuotesSO")) {
				notess = notesListFromServiceOrder(subType);
			}
			if (noteFor.equals("CustomerFile")) {
				notess = notesListFromCustomer(subType);
			}
			if (noteFor.equals("ServiceOrder")) {
				notess = notesListFromServiceOrder(subType);
			}
			if (noteFor.equals("WorkTicket")) {
				notess = notesListFromWorkTicket(subType);
			}
			if (noteFor.equals("Claim")) {
				notess = notesListFromClaim(subType);
			}
			if (noteFor.equals("CreditCard")) {
				notess = notesListFromCreditCard(subType);
			}
			if (noteFor.equals("ServicePartner")) {
				notess = notesListFromServicePartner(subType);
			}
			if (noteFor.equals("AccountLine")) {
				notess = notesListFromAccountLine(subType);
			}
			if (noteFor.equals("Carton")) {
				notess = notesListFromCarton(subType);
			}
			if (noteFor.equals("Container")) {
				notess = notesListFromContainer(subType);
			}
			if (noteFor.equals("Vehicle")) {
				notess = notesListFromVehicle(subType);
			}
			if (noteFor.equals("Partner")) {
				notess = notesListFromPartner(subType);
			}
			if (noteFor.equals("Crew")) {
				notess = notesListFromPayroll(subType);
			}
			if (noteFor.equals("MSS")) {
				notess = notesListFromMss(subType);
			}
			if (noteFor.equals("OperationsIntelligence")) {
				notess = notesListFromOI(subType);
			}
			//
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: voxmeIntergartionFlag "+voxmeIntergartionFlag);
					
				}
				 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
					mmValidation= "Yes";
					if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
						mmValidation= "No";
					}
				}
				}
			//
			return SUCCESS;
		*/
		 return SUCCESS;}
	 @SkipValidation
		public String getInfo() {
		 
		 
		 
		 
		 return SUCCESS;}	
	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public Set getPartnerPublicList() {
		return partnerPublicList;
	}

	public void setPartnerPublicList(Set partnerPublicList) {
		this.partnerPublicList = partnerPublicList;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public Map<String, String> getActionType() {
		return actionType;
	}

	public void setActionType(Map<String, String> actionType) {
		this.actionType = actionType;
	}

	public Map<String, String> getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(Map<String, String> partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getCountryCodeSearch() {
		return countryCodeSearch;
	}

	public void setCountryCodeSearch(String countryCodeSearch) {
		this.countryCodeSearch = countryCodeSearch;
	}

	public String getCountrySearch() {
		return countrySearch;
	}

	public void setCountrySearch(String countrySearch) {
		this.countrySearch = countrySearch;
	}

	public String getStateSearch() {
		return stateSearch;
	}

	public void setStateSearch(String stateSearch) {
		this.stateSearch = stateSearch;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public Map<String, String> getCountryDesc() {
		return countryDesc;
	}

	public void setCountryDesc(Map<String, String> countryDesc) {
		this.countryDesc = countryDesc;
	}

	public List getMultiplequalityCertifications() {
		return multiplequalityCertifications;
	}

	public void setMultiplequalityCertifications(List multiplequalityCertifications) {
		this.multiplequalityCertifications = multiplequalityCertifications;
	}

	public List getMultiplServiceLines() {
		return multiplServiceLines;
	}

	public void setMultiplServiceLines(List multiplServiceLines) {
		this.multiplServiceLines = multiplServiceLines;
	}

	public List getMultiplVanlineAffiliations() {
		return multiplVanlineAffiliations;
	}

	public void setMultiplVanlineAffiliations(List multiplVanlineAffiliations) {
		this.multiplVanlineAffiliations = multiplVanlineAffiliations;
	}

	public void setBstates(Map<String, String> bstates) {
		this.bstates = bstates;
	}

	public void setMstates(Map<String, String> mstates) {
		this.mstates = mstates;
	}

	public void setTstates(Map<String, String> tstates) {
		this.tstates = tstates;
	}

	public Map<String, String> getBstates() {
		if (partnerPublic != null) {
			return (bstates != null && !bstates.isEmpty()) ? bstates : customerFileManager.findDefaultStateList(partnerPublic.getBillingCountryCode(), sessionCorpID);
		} else {
			return (bstates != null && !bstates.isEmpty()) ? bstates : customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
		}

	}

	public Map<String, String> getTstates() {
		if (partnerPublic != null) {
			List terminalCountryCode = partnerManager.getCountryCode(partnerPublic.getTerminalCountry());
			try {
				if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			List terminalCountryCode = partnerManager.getCountryCode(partner.getTerminalCountry());
			try {
				if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return tstates;
	}

	public Map<String, String> getMstates() {
		if (partnerPublic != null) {
			List mailIngCountryCode = partnerManager.getCountryCode(partnerPublic.getMailingCountry());
			try {
				if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(partnerPublic.getMailingCountryCode(), sessionCorpID);

				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			List mailIngCountryCode = partnerManager.getCountryCode(partner.getMailingCountry());
			try {
				if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(partner.getMailingCountryCode(), sessionCorpID);

				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return mstates;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Map<String, String> getTypeOfVendor() {
		return typeOfVendor;
	}

	public void setTypeOfVendor(Map<String, String> typeOfVendor) {
		this.typeOfVendor = typeOfVendor;
	}

	public Map<String, String> getQualityCertifications() {
		return qualityCertifications;
	}

	public void setQualityCertifications(Map<String, String> qualityCertifications) {
		this.qualityCertifications = qualityCertifications;
	}

	public Map<String, String> getServiceLines() {
		return serviceLines;
	}

	public void setServiceLines(Map<String, String> serviceLines) {
		this.serviceLines = serviceLines;
	}

	public Map<String, String> getVanlineAffiliations() {
		return vanlineAffiliations;
	}

	public void setVanlineAffiliations(Map<String, String> vanlineAffiliations) {
		this.vanlineAffiliations = vanlineAffiliations;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getFile1() {
		return file1;
	}

	public void setFile1(File file1) {
		this.file1 = file1;
	}

	public String getFile1FileName() {
		return file1FileName;
	}

	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	public File getFile2() {
		return file2;
	}

	public void setFile2(File file2) {
		this.file2 = file2;
	}

	public String getFile2FileName() {
		return file2FileName;
	}

	public void setFile2FileName(String file2FileName) {
		this.file2FileName = file2FileName;
	}

	public File getFile3() {
		return file3;
	}

	public void setFile3(File file3) {
		this.file3 = file3;
	}

	public String getFile3FileName() {
		return file3FileName;
	}

	public void setFile3FileName(String file3FileName) {
		this.file3FileName = file3FileName;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public String getExList() {
		return exList;
	}

	public void setExList(String exList) {
		this.exList = exList;
	}

	public List getPartnerListNew() {
		return partnerListNew;
	}

	public void setPartnerListNew(List partnerListNew) {
		this.partnerListNew = partnerListNew;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public PartnerPublic getPartner() {
		return partner;
	}

	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFormClose() {
		return formClose;
	}

	public void setFormClose(String formClose) {
		this.formClose = formClose;
	}

	public String getParamView() {
		return paramView;
	}

	public void setParamView(String paramView) {
		this.paramView = paramView;
	}

	public AccountProfile getAccountProfile() {
		return accountProfile;
	}

	public void setAccountProfile(AccountProfile accountProfile) {
		this.accountProfile = accountProfile;
	}

	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}
	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getCompDiv() {
		return compDiv;
	}

	public void setCompDiv(String compDiv) {
		this.compDiv = compDiv;
	}

	public String getCitySearch() {
		return citySearch;
	}

	public void setCitySearch(String citySearch) {
		this.citySearch = citySearch;
	}

	public String getUserCheck() {
		return userCheck;
	}

	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}

	public String getUserCheckMaster() {
		return userCheckMaster;
	}

	public void setUserCheckMaster(String userCheckMaster) {
		this.userCheckMaster = userCheckMaster;
	}

	public void setAgentBaseManager(AgentBaseManager agentBaseManager) {
		this.agentBaseManager = agentBaseManager;
	}

	public void setAgentBaseSCACManager(AgentBaseSCACManager agentBaseSCACManager) {
		this.agentBaseSCACManager = agentBaseSCACManager;
	}

	public void setPartnerVanLineRefManager(PartnerVanLineRefManager partnerVanLineRefManager) {
		this.partnerVanLineRefManager = partnerVanLineRefManager;
	}

	public void setPartnerAccountRefManager(PartnerAccountRefManager partnerAccountRefManager) {
		this.partnerAccountRefManager = partnerAccountRefManager;
	}
	public void setAccountContactManager(AccountContactManager accountContactManager) {
		this.accountContactManager = accountContactManager;
	}

	public void setContractPolicyManager(ContractPolicyManager contractPolicyManager) {
		this.contractPolicyManager = contractPolicyManager;
	}

	public String getLogQuery() {
		return logQuery;
	}

	public void setLogQuery(String logQuery) {
		this.logQuery = logQuery;
	}

	public Boolean getIsIgnoreInactive() {
		return isIgnoreInactive;
	}

	public void setIsIgnoreInactive(Boolean isIgnoreInactive) {
		this.isIgnoreInactive = isIgnoreInactive;
	}

	/**
	 * @return the ownerOperatorList
	 */
	public List getOwnerOperatorList() {
		return ownerOperatorList;
	}

	/**
	 * @param ownerOperatorList the ownerOperatorList to set
	 */
	public void setOwnerOperatorList(List ownerOperatorList) {
		this.ownerOperatorList = ownerOperatorList;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public void setPaytype(Map<String, String> paytype) {
		this.paytype = paytype;
	}

	public Map<String, String> getServiceRelos1() {
		return serviceRelos1;
	}

	public void setServiceRelos1(Map<String, String> serviceRelos1) {
		this.serviceRelos1 = serviceRelos1;
	}

	public Map<String, String> getServiceRelos2() {
		return serviceRelos2;
	}

	public void setServiceRelos2(Map<String, String> serviceRelos2) {
		this.serviceRelos2 = serviceRelos2;
	}

	public Map<String, String> getServiceRelos() {
		return serviceRelos;
	}

	public void setServiceRelos(Map<String, String> serviceRelos) {
		this.serviceRelos = serviceRelos;
	}

    public Map<String, String> getFleet() {
		return fleet;
	}

	public void setFleet(Map<String, String> fleet) {
		this.fleet = fleet;
	}

	public Map<String, String> getCorp() {
		return corp;
	}

	public void setCorp(Map<String, String> corp) {
		this.corp = corp;
	}
	
	public String getJobRelo() {
		return jobRelo;
	}

	public void setJobRelo(String jobRelo) {
		this.jobRelo = jobRelo;
	}

	public Set getPartners() {
		return partners;
	}

	public void setPartners(Set partners) {
		this.partners = partners;
	}

	public String getPartnerCorpid() {
		return partnerCorpid;
	}

	public void setPartnerCorpid(String partnerCorpid) {
		this.partnerCorpid = partnerCorpid;
	} 

	public String getPartnerCodeCount() {
		return partnerCodeCount;
	}

	public void setPartnerCodeCount(String partnerCodeCount) {
		this.partnerCodeCount = partnerCodeCount;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public Map<String, String> getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(Map<String, String> creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getCreditTerm() {
		return creditTerm;
	}

	public void setCreditTerm(String creditTerm) {
		this.creditTerm = creditTerm;
	}

	public String getPartnerPrefix() {
		return partnerPrefix;
	}

	public void setPartnerPrefix(String partnerPrefix) {
		this.partnerPrefix = partnerPrefix;
	}

	
	public Map<String, String> getDriverAgencyList() {
		return driverAgencyList;
	}

	public void setDriverAgencyList(Map<String, String> driverAgencyList) {
		this.driverAgencyList = driverAgencyList;
	}


	public Map<String, String> getBillingCurrency() {
		return billingCurrency;
	}
	public void setBillingCurrency(Map<String, String> billingCurrency) {
		this.billingCurrency = billingCurrency;
	}
/*	public Map<String, String> getCompanyUTSType() {
		return companyUTSType;
	}
	public void setCompanyUTSType(Map<String, String> companyUTSType) {
		this.companyUTSType = companyUTSType;
	}*/
	public Map<String, String> getMovingCompanyType() {
		return movingCompanyType;
	}

	public void setMovingCompanyType(Map<String, String> movingCompanyType) {
		this.movingCompanyType = movingCompanyType;
	}

	public Map<String, String> getUTSmovingCompanyType() {
		return UTSmovingCompanyType;
	}

	public void setUTSmovingCompanyType(Map<String, String> uTSmovingCompanyType) {
		UTSmovingCompanyType = uTSmovingCompanyType;
	}

	public Map<String, String> getDriverType() {
		return driverType;
	}

	public void setDriverType(Map<String, String> driverType) {
		this.driverType = driverType;
	}
	public Set<Role> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<Role> userRole) {
		this.userRole = userRole;
	}



	public String getDonoFlag() {
		return donoFlag;
	}

	public void setDonoFlag(String donoFlag) {
		this.donoFlag = donoFlag;
	}

	public List getDonotMergelist() {
		return donotMergelist;
	}

	public void setDonotMergelist(List donotMergelist) {
		this.donotMergelist = donotMergelist;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getPartnerNotes() {
		return partnerNotes;
	}

	public void setPartnerNotes(String partnerNotes) {
		this.partnerNotes = partnerNotes;
	}

	public Map<String, String> getpType() {
		return pType;
	}

	public void setpType(Map<String, String> pType) {
		this.pType = pType;
	}

	public void setDataSecurityFilterManager(
			DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}

	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}

	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}

	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}

	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}

	public void setDataSecuritySetManager(
			DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}

	public String getTempTransPort() {
		return tempTransPort;
	}

	public void setTempTransPort(String tempTransPort) {
		this.tempTransPort = tempTransPort;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Map<String, String> getBankCode() {
		return bankCode;
	}

	public void setBankCode(Map<String, String> bankCode) {
		this.bankCode = bankCode;
	}

	public String getAgentCountryCode() {
		return agentCountryCode;
	}

	public void setAgentCountryCode(String agentCountryCode) {
		this.agentCountryCode = agentCountryCode;
	}

	public String getValidVatCode() {
		return validVatCode;
	}

	public void setValidVatCode(String validVatCode) {
		this.validVatCode = validVatCode;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public Map<String, String> getTaxIdType() {
		return taxIdType;
	}

	public void setTaxIdType(Map<String, String> taxIdType) {
		this.taxIdType = taxIdType;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public String getChkAgentTrue() {
		return chkAgentTrue;
	}

	public void setChkAgentTrue(String chkAgentTrue) {
		this.chkAgentTrue = chkAgentTrue;
	}

	public Boolean getExcludeFPU() {
		return excludeFPU;
	}

	public void setExcludeFPU(Boolean excludeFPU) {
		this.excludeFPU = excludeFPU;
	}

	public void setPartnerPrivateHire(Date partnerPrivateHire){
		this.partnerPrivateHire = partnerPrivateHire;
	}
	public Date getPartnerPrivateHire(){
		return partnerPrivateHire;
	}
	public void setPartnerPrivateTermination(Date partnerPrivateTermination){
		this.partnerPrivateTermination = partnerPrivateTermination;
	}
	public Date getPartnerPrivateTermination(){
		return partnerPrivateTermination;
	}

	public String getVanlineCodeSearch() {
		return vanlineCodeSearch;
	}

	public void setVanlineCodeSearch(String vanlineCodeSearch) {
		this.vanlineCodeSearch = vanlineCodeSearch;
	}

	public List getVanLineList() {
		return vanLineList;
	}

	public void setVanLineList(List vanLineList) {
		this.vanLineList = vanLineList;
	}

	public String getPartnerVanLineCode() {
		return partnerVanLineCode;
	}

	public void setPartnerVanLineCode(String partnerVanLineCode) {
		this.partnerVanLineCode = partnerVanLineCode;
	}

	public void setFrequentlyAskedQuestionsManager(
			FrequentlyAskedQuestionsManager frequentlyAskedQuestionsManager) {
		this.frequentlyAskedQuestionsManager = frequentlyAskedQuestionsManager;
	}

	public Map<String, String> getCountryWithBranch() {
		return countryWithBranch;
	}

	public void setCountryWithBranch(Map<String, String> countryWithBranch) {
		this.countryWithBranch = countryWithBranch;
	}

	public List getSelectedJobList() {
		return selectedJobList;
	}

	public void setSelectedJobList(List selectedJobList) {
		this.selectedJobList = selectedJobList;
	}

	public String getJobReloName() {
		return jobReloName;
	}

	public void setJobReloName(String jobReloName) {
		this.jobReloName = jobReloName;
	}

	public String getFlagHit() {
		return flagHit;
	}

	public void setFlagHit(String flagHit) {
		this.flagHit = flagHit;
	}

	public Map<String, String> getPlanCommissionList() {
		return planCommissionList;
	}

	public void setPlanCommissionList(Map<String, String> planCommissionList) {
		this.planCommissionList = planCommissionList;
	}

	public Map<String, String> getCollectionList() {
		return collectionList;
	}

	public void setCollectionList(Map<String, String> collectionList) {
		this.collectionList = collectionList;
	}

	public String getIsDecaExtract() {
		return isDecaExtract;
	}

	public void setIsDecaExtract(String isDecaExtract) {
		this.isDecaExtract = isDecaExtract;
	}

	public String getJustSayYes() {
		return justSayYes;
	}

	public void setJustSayYes(String justSayYes) {
		this.justSayYes = justSayYes;
	}

	public String getQualitySurvey() {
		return qualitySurvey;
	}

	public void setQualitySurvey(String qualitySurvey) {
		this.qualitySurvey = qualitySurvey;
	}

	public List getPartnerReloList() {
		return partnerReloList;
	}

	public void setPartnerReloList(List partnerReloList) {
		this.partnerReloList = partnerReloList;
	}

	public String getSearchListOfVendorCode() {
		return searchListOfVendorCode;
	}

	public void setSearchListOfVendorCode(String searchListOfVendorCode) {
		this.searchListOfVendorCode = searchListOfVendorCode;
	}

	public Map<String, String> getListOfVendorCode() {
		return listOfVendorCode;
	}

	public void setListOfVendorCode(Map<String, String> listOfVendorCode) {
		this.listOfVendorCode = listOfVendorCode;
	}

	public PartnerAccountRef getPartnerAccountRef() {
		return partnerAccountRef;
	}

	public void setPartnerAccountRef(PartnerAccountRef partnerAccountRef) {
		this.partnerAccountRef = partnerAccountRef;
	}

	public Map<String, String> getVatBillingGroups() {
		return vatBillingGroups;
	}

	public void setVatBillingGroups(Map<String, String> vatBillingGroups) {
		this.vatBillingGroups = vatBillingGroups;
	}

	public String getCompDivFlag() {
		return compDivFlag;
	}

	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}

	public void setPartnerBankInfoManager(
			PartnerBankInfoManager partnerBankInfoManager) {
		this.partnerBankInfoManager = partnerBankInfoManager;
	}

	public List getPartnerBankInfoList() {
		return partnerBankInfoList;
	}

	public void setPartnerBankInfoList(List partnerBankInfoList) {
		this.partnerBankInfoList = partnerBankInfoList;
	}

	public String getBankPartnerCode() {
		return bankPartnerCode;
	}

	public void setBankPartnerCode(String bankPartnerCode) {
		this.bankPartnerCode = bankPartnerCode;
	}

	public String getPartnerbanklist() {
		return partnerbanklist;
	}

	public void setPartnerbanklist(String partnerbanklist) {
		this.partnerbanklist = partnerbanklist;
	}



	public String getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(String currencyList) {
		this.currencyList = currencyList;
	}

	public String getCheckList() {
		return checkList;
	}

	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}

	public String getNewlineid() {
		return newlineid;
	}

	public void setNewlineid(String newlineid) {
		this.newlineid = newlineid;
	}

	public String getBankNoList() {
		return bankNoList;
	}

	public void setBankNoList(String bankNoList) {
		this.bankNoList = bankNoList;
	}

	public Boolean getAgentCheck() {
		return agentCheck;
	}

	public void setAgentCheck(Boolean agentCheck) {
		this.agentCheck = agentCheck;
	}

	public String getContainsData() {
		return containsData;
	}

	public void setContainsData(String containsData) {
		this.containsData = containsData;
	}

	public String getCheckCodefromDefaultAccountLine() {
		return checkCodefromDefaultAccountLine;
	}

	public void setCheckCodefromDefaultAccountLine(
			String checkCodefromDefaultAccountLine) {
		this.checkCodefromDefaultAccountLine = checkCodefromDefaultAccountLine;
	}

	public Integer getCompensationYear() {
		return compensationYear;
	}

	public void setCompensationYear(Integer compensationYear) {
		this.compensationYear = compensationYear;
	}

	public boolean isCheckFieldVisibility() {
		return checkFieldVisibility;
	}

	public void setCheckFieldVisibility(boolean checkFieldVisibility) {
		this.checkFieldVisibility = checkFieldVisibility;
	}

	public String getCheckCompensationYear() {
		return checkCompensationYear;
	}

	public void setCheckCompensationYear(String checkCompensationYear) {
		this.checkCompensationYear = checkCompensationYear;
	}

	public int getParentCompensationYear() {
		return parentCompensationYear;
	}

	public void setParentCompensationYear(int parentCompensationYear) {
		this.parentCompensationYear = parentCompensationYear;
	}

	public Map<String, String> getPrefixList() {
		return prefixList;
	}

	public void setPrefixList(Map<String, String> prefixList) {
		this.prefixList = prefixList;
	}
	public AgentRequestManager getAgentRequestManager() {
		return agentRequestManager;
	}

	public void setAgentRequestManager(AgentRequestManager agentRequestManager) {
		this.agentRequestManager = agentRequestManager;
	}
	public AgentRequest getAgentRequest() {
		return agentRequest;
	}

	public void setAgentRequest(AgentRequest agentRequest) {
		this.agentRequest = agentRequest;
	}
	public String getLastNameFlag() {
		return lastNameFlag;
	}

	public void setLastNameFlag(String lastNameFlag) {
		this.lastNameFlag = lastNameFlag;
	}

	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}

	public PartnerPrivateManager getPartnerPrivateManager() {
		return partnerPrivateManager;
	}
	public List getAgentlist() {
	return agentlist;
    }

    public void setAgentlist(List agentlist) {
	this.agentlist = agentlist;
    }
    public String getCorpid() {
		return corpid;
	}

	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}

	
    public Long getAgentPartnerId() {
		return agentPartnerId;
	}

	public void setAgentPartnerId(Long agentPartnerId) {
		this.agentPartnerId = agentPartnerId;
	}

	public Set getAgentList() {
		return agentList;
	}

	public void setAgentList(Set agentList) {
		this.agentList = agentList;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public Long getAgentCountId() {
		return agentCountId;
	}

	public void setAgentCountId(Long agentCountId) {
		this.agentCountId = agentCountId;
	}

	public EmailSetupManager getEmailSetupManager() {
		return emailSetupManager;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public File getFileLogo() {
		return fileLogo;
	}

	public void setFileLogo(File fileLogo) {
		this.fileLogo = fileLogo;
	}

	public String getFileLogoFileName() {
		return fileLogoFileName;
	}

	public void setFileLogoFileName(String fileLogoFileName) {
		this.fileLogoFileName = fileLogoFileName;
	}

	public Map<String, String> getAgentRequestStatus() {
		return agentRequestStatus;
	}

	public void setAgentRequestStatus(Map<String, String> agentRequestStatus) {
		this.agentRequestStatus = agentRequestStatus;
	}

	public String getAgentEditURL() {
		return agentEditURL;
	}

	public void setAgentEditURL(String agentEditURL) {
		this.agentEditURL = agentEditURL;
	}

	public String getFileLogoUploadFlag() {
		return fileLogoUploadFlag;
	}

	public void setFileLogoUploadFlag(String fileLogoUploadFlag) {
		this.fileLogoUploadFlag = fileLogoUploadFlag;
	}

	public String getImageDataFileLogo() {
		return imageDataFileLogo;
	}

	public void setImageDataFileLogo(String imageDataFileLogo) {
		this.imageDataFileLogo = imageDataFileLogo;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public String getLoginCorpId() {
		return loginCorpId;
	}

	public void setLoginCorpId(String loginCorpId) {
		this.loginCorpId = loginCorpId;
	}
}