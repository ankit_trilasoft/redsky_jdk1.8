<%@ include file="/common/taglibs.jsp"%> 


<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
       <title>Redsky Billing</title> 
    	<meta name="heading" content="Redsky Billing"/> 
    <script language="javascript">
    		function linkToDetail(val){
        		document.forms['billingDetailList'].elements['month'].value=val;
				document.forms['billingDetailList'].action="redskyBillingListDetails.html";
				document.forms['billingDetailList'].submit();
			}
    </script>
</head>
<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
<tr>
<c:if test="${empty totalOfSo}">
 <td class="listblacktextBig"><b><u>Company Name:</u></b> ${companyName}     Year: ${year}    ServiceOrder Generated: NIL
 </c:if>
 <c:if test="${not empty totalOfSo}">
 <td class="listblacktextBig"><b><u>Company Name:</u></b> ${companyName}    Year: ${year}   ServiceOrder Generated:<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${totalOfSo}" /></td>
</c:if>
</tr>
<tr><td height="7px"></td></tr>
</table>

 <s:form id="billingDetailList" name="billingDetailList" action="" method="post" validate="true" >


<s:hidden name="corpID" value="%{corpID}"/>
<s:hidden name="month" />

<s:hidden name="year" value="%{year}"/>



<display:table name="billingDetailList" requestURI=""  id="billingDetailListID"  class="table" pagesize="12" style="width:100%;margin-top: 1px;" >  
    <display:column property="corpID" escapeXml="true" sortable="true"   titleKey="BillingDetail.corpid" style="width: 10%; cursor: default;"/>
    <display:column property="monthName" escapeXml="true" sortable="true" titleKey="BillingDetail.monthName" style="width: 10%; cursor: default;"/>
   <display:column property="soCount" escapeXml="true"  sortable="true" title="# of Billing Transactions" style="width: 10%; cursor: default;"/> 
   <display:column style="width: 10%; cursor: default;" titleKey="BillingDetail.Details"><a href="#" onclick ="linkToDetail(${billingDetailListID.month})">Details</a></display:column>
   <display:column style="width: 10%; cursor: default;" title="Excel Extract"><a href="redskyBillingListExtract.html?month=${billingDetailListID.month}&corpID=${billingDetailListID.corpID}&year=${billingDetailListID.year} ">Extract</display:column>
 
  </display:table>  
</s:form>

