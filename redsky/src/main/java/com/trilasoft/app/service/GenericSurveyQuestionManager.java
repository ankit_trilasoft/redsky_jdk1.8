package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.GenericSurveyQuestion;


public interface GenericSurveyQuestionManager extends GenericManager<GenericSurveyQuestion, Long>{
	
	public List findRecords(String sessionCorpID);
	public Map<List, Map> getSurveyUserList(String joType,String routingType,String sessionCorpID,Long sid,Long cid,boolean isBookingAgent,boolean isOriginAgent,boolean isdestinationAgent, String language);
	public String checkForBA(Long sid,String sessionCorpID);
	public String checkForOA(Long sid,String sessionCorpID);
	public String checkForDA(Long sid,String sessionCorpID);
}
