<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Recent Tariff List</title> 
<meta name="heading" content="Recent Tariff List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
!margin-top:-18px;
padding:0px;
text-align:right;
width:100%;
}
#otabs {
margin-bottom:0;
!margin-bottom:-20px;
margin-left:40px;
position:relative;
}
</style>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			
}

function getRecentTariffs(target){
	location.href="searchRecentTariffs.html?let="+target;

}


</script>
</head>
<c:set var="buttons"> 
     <input type="button" class="cssbutton1" style="width:120px; height:25px"  
        onclick="location.href='<c:url value="/agentPricingList.html"/>'"  
        value="Proceed to Wizard"/>  

</c:set>
	<s:form id="recentTariffs" name="recentTariffs" action='recentTariffs' method="post" validate="true">   
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Recent Tariff List</span></a></li>
		  </ul>
		  
	</div>
	
	<span style="padding-left:10px;!padding-left:160px;font-size:10px;color:#003366;">(Click to see country listing) 
	<a onClick="getRecentTariffs('a')" style="text-decoration: underline; cursor: pointer;">A</a>
	<a onClick="getRecentTariffs('b')" style="text-decoration: underline; cursor: pointer;">B</a>
	<a onClick="getRecentTariffs('c')" style="text-decoration: underline; cursor: pointer;">C</a>
	<a onClick="getRecentTariffs('d')" style="text-decoration: underline; cursor: pointer;">D</a>
	<a onClick="getRecentTariffs('e')" style="text-decoration: underline; cursor: pointer;">E</a>
	<a onClick="getRecentTariffs('f')" style="text-decoration: underline; cursor: pointer;">F</a>
	<a onClick="getRecentTariffs('g')" style="text-decoration: underline; cursor: pointer;">G</a>
	<a onClick="getRecentTariffs('h')" style="text-decoration: underline; cursor: pointer;">H</a>
	<a onClick="getRecentTariffs('i')" style="text-decoration: underline; cursor: pointer;">I</a>
	<a onClick="getRecentTariffs('j')" style="text-decoration: underline; cursor: pointer;">J</a>
	<a onClick="getRecentTariffs('k')" style="text-decoration: underline; cursor: pointer;">K</a>
	<a onClick="getRecentTariffs('l')" style="text-decoration: underline; cursor: pointer;">L</a>
	<a onClick="getRecentTariffs('m')" style="text-decoration: underline; cursor: pointer;">M</a>
	<a onClick="getRecentTariffs('n')" style="text-decoration: underline; cursor: pointer;">N</a>
	<a onClick="getRecentTariffs('o')" style="text-decoration: underline; cursor: pointer;">O</a>
	<a onClick="getRecentTariffs('p')" style="text-decoration: underline; cursor: pointer;">P</a>
	<a onClick="getRecentTariffs('q')" style="text-decoration: underline; cursor: pointer;">Q</a>
	<a onClick="getRecentTariffs('r')" style="text-decoration: underline; cursor: pointer;">R</a>
	<a onClick="getRecentTariffs('s')" style="text-decoration: underline; cursor: pointer;">S</a>
	<a onClick="getRecentTariffs('t')" style="text-decoration: underline; cursor: pointer;">T</a>
	<a onClick="getRecentTariffs('u')" style="text-decoration: underline; cursor: pointer;">U</a>
	<a onClick="getRecentTariffs('v')" style="text-decoration: underline; cursor: pointer;">V</a>
	<a onClick="getRecentTariffs('w')" style="text-decoration: underline; cursor: pointer;">W</a>
	<a onClick="getRecentTariffs('x')" style="text-decoration: underline; cursor: pointer;">X</a>
	<a onClick="getRecentTariffs('y')" style="text-decoration: underline; cursor: pointer;">Y</a>
	<a onClick="getRecentTariffs('z')" style="text-decoration: underline; cursor: pointer;">Z</a>
	</span>
	
	<!--<span style="padding-left:100px;!padding-left:300px;font-size:13px;color:#003366;">List of Currently Available Tariffs</span>
	--><div class="spnblk">&nbsp;</div>
	
	<display:table name="recentTariffsList" class="table" requestURI="" id="recentTariffsId" export="true" defaultsort="5" defaultorder="descending" pagesize="10" style="width:100%" >   
	<display:column property="description"  group="1" sortable="true" title="Country"/>
	<display:column property="partnerName"  sortable="true" title="Agent"/>
   	<display:column property="tariffApplicability"  sortable="true" title="Tariff Applicability"/>
   	<display:column property="metroCity"  sortable="true" title="Service Area"/>
   	<display:column property="effectiveDate"  sortable="true" title="Effective" format="{0,date,dd-MMM-yyyy}"/>
   	<display:column property="tariffScope"  sortable="true" title="Scope"/>
   	<display:setProperty name="export.excel.filename" value="Recent Tariff List.xls"/>
	<display:setProperty name="export.csv.filename" value="Recent Tariff List.csv"/>
   	</display:table>
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
<c:redirect url="/agentPricingList.html"></c:redirect>
</s:form> 

<script type="text/javascript"> 

</script> 