<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="roleList.title"/></title>
    <meta name="heading" content="<fmt:message key='roleList.heading'/>"/>
<style>
 span.pagelinks {display:block; font-size:0.85em; !font-size:0.90em; margin-bottom:5px; !margin-bottom:6px;
			     margin-top:-33px; !margin-top:-37px; padding:2px 0px; text-align:right;	width:85%;}

</style>
</head>
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
///alert("NAME"+name);
  var url="findUserByRole.html?ajax=1&decorator=simple&popup=true&name=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }

</script>

<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}
</script>

<div id="newmnav">
<ul>  
   <li><a href="myUsersRoles.html"><span>Users</span></a></li>
  <li id="newmnav1"><a class="current""><span>Roles</span></a></li>
</ul>
</div>
<!-- <div class="spnblk" style="width:80%;clear:both;">&nbsp;</div> -->


<s:set name="roles" value="roles" scope="request"/>
<display:table name="roles" requestURI="" defaultsort="1" id="roles" class="table" pagesize="50" style="width:100%;margin-top:1px;!margin-top:0px;" >
<%--
<display:column><a onclick="javascript:openWindow('menuItems.html?id=${roles.id}&perRec=${roles.name}&decorator=popup&popup=true')"><c:out value="${roles.name}" /></a></display:column>
<display:column property="name" titleKey="role.name"><a onclick="javascript:openWindow('menuItems.html?id=${roles.id}&perRec=${roles.name}&decorator=popup&popup=true')"><c:out value="${roles.name}" /></a></display:column>
--%>
<display:column property="name" escapeXml="true" sortable="true" titleKey="role.name"   style="width: 25%" paramId="id" paramProperty="id"/>
<display:column title="Assign Menu" style="width:15%" ><input type="button" style="min-width:100px" class="cssbutton" onclick="javascript:openWindow('menuItems.html?id=${roles.id}&perRec=${roles.name}&decorator=popup&popup=true');" value="<fmt:message key="button.assignMenuItem"/>"  /></display:column> 
<display:column property="description" escapeXml="true" sortable="true" titleKey="role.description" style="width:40%"/>
<display:column property="corpID" titleKey="role.corpID" style="width: 10%"/>
<display:column title="UserList" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserPermission('${roles.name}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
</display:table> 
<script type="text/javascript">
    // highlightTableRows("roles");
</script>