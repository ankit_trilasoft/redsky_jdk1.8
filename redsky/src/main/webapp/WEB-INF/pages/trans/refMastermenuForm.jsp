<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>


<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
	<script language="JavaScript">
	
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
</script>
<script language="javascript" type="text/javascript">
 window.onload=function(){
	  var corpID =document.forms['refMasterForm'].elements['refMaster.corpID'].value;
    var a= document.forms['refMasterForm'].elements.length;
    var refcorpid='${sessionCorpID}';
    if(${refMaster.id != null}){
   if(${sessionCorpID!='TSFT'}){ 
	   if(corpID!=refcorpid || ${editable==false}){
		      
		      for(i=0;i<a;i++)
		      {
		       if(document.forms['refMasterForm'].elements[i].type=='text') {
		       document.forms['refMasterForm'].elements[i].readOnly=true;
		       document.forms['refMasterForm'].elements[i].className = 'input-textUpper';
		       }else{
		           document.forms['refMasterForm'].elements[i].disabled=true;   
		       }
		      }  
   }
  }    
} 
 
}
</script>


<script language="javascript" type="text/javascript">

	
    function myDate() {
	if(document.forms['refMasterForm'].elements['refMaster.stopDate'].value == "null" ){
		document.forms['refMasterForm'].elements['refMaster.stopDate'].value="";
		}
		}
		
	function clear_fields()
	{
			document.forms['refMasterForm'].elements['refMaster.code'].value = "";
			document.forms['refMasterForm'].elements['refMaster.description'].value = "";
			document.forms['refMasterForm'].elements['refMaster.parameter'].value = "";
			document.forms['refMasterForm'].elements['refMaster.fieldLength'].value = "";
			document.forms['refMasterForm'].elements['refMaster.number'].value = "";
			document.forms['refMasterForm'].elements['refMaster.bucket'].value = "";
			document.forms['refMasterForm'].elements['refMaster.bucket2'].value = "";			
}
function checkLength(length)
{
	var count = document.forms['refMasterForm'].elements['refMaster.code'].value.length;
	if(count>length)
	{
		var param=document.forms['refMasterForm'].elements['parameter'].value
		alert("Reference code for "+param+ " cannot be more than "+length);
		var codeValue = document.forms['refMasterForm'].elements['refMaster.code'].value;
		document.forms['refMasterForm'].elements['refMaster.code'].value = codeValue.substring(0,length);
	}
}

	function changeStatus(){
	  document.forms['refMasterForm'].elements['formStatus'].value = '1';
	}
	
	function autoSave(clickType){
	
	if(!(clickType == 'save')){
	if(checkCode()){
	if ('${autoSavePrompt}' == 'No'){
	var noSaveAction;
	var parameter=document.forms['refMasterForm'].elements['parameter'].value;
	if(document.forms['refMasterForm'].elements['gotoPageString'].value == 'gototab.refMasterList'){
		noSaveAction = 'refMastermenus.html?parameter='+parameter;
	}
	processAutoSave(document.forms['refMasterForm'],'saveRefMastermenu!saveOnTabChange.html', noSaveAction );
	
	}else{
	var id1 = document.forms['refMasterForm'].elements['refMaster.id'].value;
	var parameter=document.forms['refMasterForm'].elements['parameter'].value;
	if (document.forms['refMasterForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the Reference Master Menu Form");
		if(agree){
			document.forms['refMasterForm'].action = 'saveRefMastermenu!saveOnTabChange.html';
			document.forms['refMasterForm'].submit();
		}else{	
			if(id1 != ''){
			if(document.forms['refMasterForm'].elements['gotoPageString'].value == 'gototab.refMasterList'){
				location.href = 'refMastermenus.html?parameter='+parameter;
			    }
			  }
			}
	}else{
	if(id1 != ''){
		if(document.forms['refMasterForm'].elements['gotoPageString'].value == 'gototab.refMasterList'){
		location.href = 'refMastermenus.html?parameter='+parameter;
			
					}
				
		      	}
		  	 }
	       }
	 	}
		}
	 }
	// Added this method for Bug 6583
	function checkCode(){
		 var code=document.forms['refMasterForm'].elements['refMaster.code'].value;
		 if(code.trim()==""){
		  		alert("Code is a required field.");
		  return false;
		 }
		 var description=document.forms['refMasterForm'].elements['refMaster.description'].value;
		 if(description.includes(" :")){
			alert("Usage of space between text and : is not allowed. Please remove the space and continue");
			  return false;
		 }
		 return true;
	}

</script>
<title><fmt:message key="refMasterDetail.title" /></title>
  <meta name="heading"
	content="<fmt:message key='refMasterDetail.heading'/>" />
</head>
<s:form name="refMasterForm" id="refMasterForm" action="saveRefMastermenu" method="post" validate="true">
	<s:hidden name="refMaster.id" value="%{refMaster.id}"/>
	
	<c:set var="parameter" value="<%=request.getParameter("parameter")%>" />
	<s:hidden name="parameter" value="${parameter}" />
	<c:set var="refMaster.id" value="<%=request.getParameter("id")%>" scope="session"/>
	<c:set var="fieldLength" value="<%=request.getParameter("l")%>" />
	<s:hidden name="fieldLength" value="${fieldLength}"/>
	<c:set var="refMaster.corpID" value="%{refMaster.corpID}"/>
	<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID")%>" />
    <s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID")%>"/>	 
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value=""/>
	<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
	<c:when test="${gotoPageString == 'gototab.refMasterList' }">
	    <c:redirect url="/refMastermenus.html?parameter=${parameter}"/>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	</c:if>
 <div id="Layer1" style="width:100%" >  
 <div id="newmnav">
		  <ul>
	<c:choose>
		  <c:when test="${tsftFlag==true && hybridFlag==false}">
		    <li><a href="refMastermenus.html?parameter=${parameter}"><span>Reference List</span></a></li>
          </c:when>
		  <c:otherwise>
	    	  <li><a onclick="setReturnString('gototab.refMasterList');return autoSave('none');"><span>Reference List</span></a></li>
		 </c:otherwise>
    </c:choose>
			    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Reference Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		 
		  <c:if test="${not empty refMaster.id}">
		    	<!--<li><a onclick="openWindow('auditList.html?id=${refMaster.id}&tableName=refmaster&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>-->
		    	<li><a onclick="window.open('auditList.html?id=${refMaster.id}&tableName=refmaster&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		    	</c:if>
		    <c:if test="${empty refMaster.id}">
		    	<li><a><span>Audit</span></a></li>
		    </c:if>
		     </ul>
</div><div class="spn">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content" >
	<table class="" cellspacing="1" cellpadding="3" border="0" >
	<tr>
	<td></td>
   <c:if test="${empty parameter }" >
      <td  class="listwhitetext" align="left" style="font-weight:bold; ">Add details-new</td>
   </c:if>
   <c:if test="${not empty parameter }" >
      <td  class="listwhitetext" align="left" style="font-weight:bold; ">Add details-<c:out value="${parameter}"/></td>
   	  <td  class="listwhitetext" align="left" style="font-weight:bold;" colspan="7">Description-<c:out value="${controlParameterDescription}"/></td>
   </c:if>
   </tr>
     			 		<tr> 	
							<td class="listwhitetext" align="right">Code<font color="red" size="2">*</font></td>
							 <c:if test="${sessionCorpID=='TSFT'}" >
							   <td align="left" width="150px"><s:textfield name="refMaster.code" size="15" maxlength="${fieldLength}"  cssClass="input-text" onfocus="myDate();"  /></td>
							</c:if>
							<c:if test="${sessionCorpID !='TSFT'}" >
							   <td align="left" width="150px"><s:textfield name="refMaster.code" size="15" maxlength="${fieldLength}"  cssClass="input-text" onfocus="myDate();" onkeyup="checkLength('${fieldLength}');" /></td>
							</c:if>
							<td class="listwhitetext" align="right">Label1</td>
							<td><s:textfield name="refMaster.label1" size="15" maxlength="50" cssClass="input-text" /></td>
							
							<td class="listwhitetext" align="right">Flex1</td>
							<td><s:textfield name="refMaster.flex1" size="30" maxlength="100" cssClass="input-text" /></td>
							<td class="listwhitetext" align="right">Label5</td>
							<td><s:textfield name="refMaster.label5" size="15" maxlength="50" cssClass="input-text" /></td>
							
							<td class="listwhitetext" align="right" width="50">Flex5</td>
							<td><s:textfield name="refMaster.flex5" size="30" maxlength="100" cssClass="input-text" /></td>
				      	</tr>
				      	<tr>			
							<td class="listwhitetext" align="right">Field Length</td>
	 					 <c:if test="${sessionCorpID=='TSFT'}" >
	 					    <td align="left"><s:textfield cssStyle="text-align:right" name="refMaster.fieldLength" size="15" maxlength="20" cssClass="input-text" value="${fieldLength}" onkeydown="return onlyNumsAllowed(event)"/></td>
					    </c:if>
					    <c:if test="${sessionCorpID!='TSFT'}" >
	 					    <td align="left"><s:textfield cssStyle="text-align:right" name="refMaster.fieldLength" readonly="true" size="15" maxlength="20" cssClass="input-text" value="${fieldLength}" onkeydown="return onlyNumsAllowed(event)"/></td>
					  	 </c:if>
					  	 <td class="listwhitetext" align="right">Label2</td>
							<td><s:textfield name="refMaster.label2" size="15" maxlength="50" cssClass="input-text" /></td>
							
					  	 <td class="listwhitetext" align="right">Flex2</td>
							<td><s:textfield name="refMaster.flex2" size="30" maxlength="100" cssClass="input-text" /></td>
							<td class="listwhitetext" align="right">Label6</td>
							<td><s:textfield name="refMaster.label6" size="15" maxlength="50" cssClass="input-text" /></td>
							
							<td class="listwhitetext" align="right">Flex6</td>
							<td><s:textfield name="refMaster.flex6" size="30" maxlength="100" cssClass="input-text" /></td>
					  	</tr> 
						<tr>
							<td class="listwhitetext" align="right">Bucket</td>
							<td align="left"><s:textfield name="refMaster.bucket" size="15" maxlength="20" cssClass="input-text" /></td>
							<%-- <c:if test="${sessionCorpID=='STIN' && refMaster.parameter=='TCKTSERVC'}" >
							<td class="listwhitetext" align="right">Warehouse</td>
							<td><s:select list="%{wareHouse}" name="refMaster.serviceWarehouse" cssStyle="width:98px;" cssClass="list-menu"/></td>
							</c:if> --%>
						     <td class="listwhitetext" align="right">Label3</td>
							<td><s:textfield name="refMaster.label3" size="15" maxlength="50" cssClass="input-text" /></td>
							
						    <td class="listwhitetext" align="right">Flex3</td>
							<td ><s:textfield name="refMaster.flex3" size="30" maxlength="100" cssClass="input-text" /></td> 
						    <td class="listwhitetext" align="right">Label7</td>
							<td><s:textfield name="refMaster.label7" size="15" maxlength="50" cssClass="input-text" /></td>
							
						    <td class="listwhitetext" align="right">Flex7</td>
							<td ><s:textfield name="refMaster.flex7" size="30" maxlength="100" cssClass="input-text" /></td>                 
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Bucket2</td>
							<td ><s:textfield name="refMaster.bucket2" size="15" maxlength="20" cssClass="input-text" /></td>
							
							<td class="listwhitetext" align="right">Label4</td>
							<td><s:textfield name="refMaster.label4" size="15" maxlength="50" cssClass="input-text" /></td>
							
							
							<td class="listwhitetext" align="right">Flex4</td>
							<c:if test="${parameter != 'DOCUMENT' && parameter != 'PORTALDOCTYPE'}" >
							<td ><s:textfield name="refMaster.flex4" size="30" maxlength="100" cssClass="input-text" /></td>
							</c:if>
							<c:if test="${parameter == 'DOCUMENT' || parameter == 'PORTALDOCTYPE'}" >
							<td><s:select cssClass="list-menu" name="refMaster.flex4" list="%{documentCategoryList}" headerKey="" headerValue="" cssStyle="width:175px" /></td>
							</c:if>
							<td class="listwhitetext" align="right">Label8</td>
							<td><s:textfield name="refMaster.label8" size="15" maxlength="50" cssClass="input-text" /></td>
							
							<td class="listwhitetext" align="right">Flex8</td>
							<td ><s:textfield name="refMaster.flex8" size="30" maxlength="100" cssClass="input-text" /></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" rowspan="2">Description</td>
							<td rowspan="2"><s:textarea name="refMaster.description" cssStyle="width:195px;height:40px;" cssClass="textarea" /></td>
							
							<td class="listwhitetext" align="right">CorpId</td>
							<td><s:textfield name="refMaster.corpID" id="refcorp" size="15" maxlength="10" readonly="true" cssClass="input-textUpper" /></td>
							<td class="listwhitetext" align="right">Language</td>
							<td><s:select list="%{languageList}" name="refMaster.language" cssStyle="width:98px;" cssClass="list-menu"/></td>
							<td class="listwhitetext" align="right">Sequence&nbsp;No.</td>
							<td><s:select list="%{sequenceList}" name="refMaster.sequenceNo" headerKey="0" headerValue="" cssStyle="width:99px;" cssClass="list-menu"/></td>
						</tr>

						<tr>
							<td class="listwhitetext" align="right">Number</td>
							<td  ><s:textfield cssStyle="text-align:right" name="refMaster.number" size="15" maxlength="20" cssClass="input-text" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
							<td class="listwhitetext" align="right">Status</td>
							
							<td><s:select list="{'Active','Inactive'}" name="refMaster.status" cssStyle="width:98px;" cssClass="list-menu"/></td>
							
							<c:if test="${parameter == 'COUNTRY'}" >
							<td class="listwhitetext" align="right">VAT #</td>
							<td><s:select cssClass="list-menu" name="refMaster.branch" list="%{branch}" headerKey="" headerValue="" cssStyle="width:80px" /></td>
						</c:if>
						<c:if test="${parameter != 'COUNTRY'}" >							
							<s:hidden name="refMaster.branch" />
						</c:if>
						 <%-- <c:if test="${parameter == 'DOCUMENT' || parameter == 'PORTALDOCTYPE'}" >
							<td class="listwhitetext" align="right">Document&nbsp;Category<font color="red" size="2">*</font></td>
							<td><s:select cssClass="list-menu" name="refMaster.flex4" list="%{documentCategoryList}" headerKey="" headerValue="" cssStyle="width:80px" /></td>
						</c:if>  --%>
						<%-- <c:if test="${parameter != 'DOCUMENT'}" >							
							<s:hidden name="refMaster.documentCategory" />
						</c:if>	 --%>
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Manager</td>
							<td  ><s:textfield name="refMaster.billCrew" size="15" maxlength="20" cssClass="input-text"/></td>
							<c:if test="${childParameterFlag == true || childParameterFlag == 'true'}" >
							<td class="listwhitetext" align="right" >Child&nbsp;Parameter<div style="font-weight:bold;float:right;">&nbsp;(${childParameterValue})</div></td>
							<td colspan="5" rowspan="2">
							 <s:select name="refMaster.childParameterValues" list="%{childParameterValues}"
										value="%{multiplParentParameterValue}" multiple="true" cssClass="list-menu"
										cssStyle="width:515px;height:78px" headerKey="" headerValue="" />
							 <%-- <select class="list-menu pr-f11"multiple="multiple"   style="width:100px;" id="refMaster.parentParameterValues" name="refMaster.parentParameterValues" >
    								<c:forEach var="chrms2" items="${parentParameterValues}" varStatus="loopStatus">
										<c:choose>
											<c:when test="${chrms2.key ==refMaster.parentParameterValues}">
												<c:set var="selectedInd" value=" selected"></c:set>
											</c:when>
											<c:otherwise>
												<c:set var="selectedInd" value=""></c:set>
											</c:otherwise>
										</c:choose>
										
										<option value="<c:out value='${chrms2.key}' />" <c:out value='${selectedInd}' />>
											<c:out value="${chrms2.value}"></c:out>
    									</option>
									 </c:forEach>
							</select>  --%>
							
							</td>
							</c:if>
							<configByCorp:fieldVisibility componentId="component.field.opsMgmt.warehouse">
							<c:if test="${refMaster.parameter=='TCKTSERVC'}" >
							<td class="listwhitetext" align="right">Warehouse<div style="font-weight:bold;float:right;"></div></td>
							<td rowspan="2" valign="top">
							 <s:select name="refMaster.serviceWarehouse" list="%{wareHouse}" value="%{multiplWareHouseValue}" multiple="true" cssClass="list-menu" cssStyle="width:172px;height:48px" headerKey="" headerValue="" />							
							</td>
							</c:if>
							</configByCorp:fieldVisibility>
						</tr>
						<c:if test="${empty parameter}" >
						    <tr>
							<td class="listwhitetext" align="right">Parameter</td>
							<td><s:textfield name="refMaster.parameter" size="15" maxlength="20"  cssClass="input-text" /></td>
						    </tr>
	                     </c:if>
	                     <c:if test="${not empty parameter }" >
	                     	<s:hidden name="refMaster.parameter" value="${parameter}"/>
						 </c:if>
						 <c:if test="${parameter == 'COUNTRY'}" >
							<tr>
								<td class="listwhitetext" align="right">Address</td>
								<td colspan="5"><s:textarea name="refMaster.address1"  cssStyle="width:370px;height:30px;" cssClass="textarea" /></td>
								
							</tr>
						</c:if>
						
						<tr>
						<td style="height:10px;"></td>							
						</tr>
						<!--
	                    <tr>
							<td class="listwhitetext" align="right">Flaxi1</td>
							<td ><s:textfield name="refMaster.flaxi1" size="15" maxlength="100" cssClass="input-text" /></td>
							
						</tr>
						 <tr>
							<td class="listwhitetext" align="right">Flaxi2</td>
							<td ><s:textfield name="refMaster.flaxi2" size="15" maxlength="100" cssClass="input-text" /></td>
							
						</tr>
						 <tr>
							<td class="listwhitetext" align="right">Flaxi3</td>
							<td ><s:textfield name="refMaster.flaxi3" size="15" maxlength="100" cssClass="input-text" /></td>
							
						</tr>
						 <tr>
							<td class="listwhitetext" align="right">Flaxi4</td>
							<td ><s:textfield name="refMaster.flaxi4" size="15" maxlength="100" cssClass="input-text" /></td>
							
						</tr>
	                     --></table>
	    </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		   		
		   <table class="detailTabLabel" border="0" style="width:90%">
		   <tbody>
					<tr>
						<td  align="right" class="listwhitetext"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<fmt:formatDate var="refMasterCreatedOn" value="${refMaster.createdOn}" pattern="${displayDateTimeFormat}"/>
						<s:hidden name="refMaster.createdOn" value="${refMasterCreatedOn}"/>
						<td><fmt:formatDate value="${refMaster.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext"><b><fmt:message key='customerFile.createdBy' /></b></td>
						<c:if test="${not empty refMaster.id}">
						<s:hidden name="refMaster.createdBy" />
						<td><s:label name="createdBy" value="%{refMaster.createdBy}" /></td>
						</c:if>
						<c:if test="${empty refMaster.id}">
						<s:hidden name="refMaster.createdBy" value="${pageContext.request.remoteUser}" />
						<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
						</c:if>	
						<td align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<fmt:formatDate var="refMasterUpdatedOn" value="${refMaster.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<s:hidden name="refMaster.updatedOn" value="${refMasterUpdatedOn}"/>
						<td><fmt:formatDate value="${refMaster.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
						<td align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty refMaster.id}">
						<s:hidden name="refMaster.updatedBy" />
						<td><s:label name="updatedBy" value="%{refMaster.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty refMaster.id}">
						<s:hidden name="refMaster.updatedBy" value="${pageContext.request.remoteUser}" />
						<td><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
						</c:if>
					</tr>
			</tbody>
		</table>
	<c:if test="${tsftFlag==false || editable==true }">
	<c:if test="${sessionCorpID==refMaster.corpID || refMaster.id== null}">
			 <table style="margin-top:10px;"><tr>
		    			<td align="left">				
						<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save" onmouseover="checkLength('${fieldLength}');" onclick="return checkCode();"/> 
		    			</td>
		    			<!-- 
		    			<c:if test="${empty parameter}" >
						<td><input type="button" class="cssbuttonA" value="Add"  style="width:55px; height:25px" onclick="location.href='<c:url value="/editRefMastermenu.html"/>'" /></td>
						</c:if>
						<c:if test="${not empty parameter }" >
	                    <td><input type="button" class="cssbuttonA" value="Add"  style="width:55px; height:25px" onclick="location.href='<c:url value="/editRefMastermenu.html?parameter=${parameter}&l=${fieldLength}"/>'" /></td>
						</c:if>
						 -->
	    							
						<s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:choose>
							<c:when test="${submitType == 'job'}">
							<td align="left"><s:reset type="button" cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" /></td>
							</c:when>
							<c:otherwise>
							<td align="left"><s:reset type="button" cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" /></td>
							</c:otherwise>
	 					</c:choose> 
	 					<td align="left"><s:submit cssClass="cssbutton" method="cancel" key="button.cancel" cssStyle="width:55px; height:25px" /></td>
								<%-- 	
									<c:if test="${not empty refMaster.id}">
									 <td align="left"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="delete" key="button.delete" /></td>
									</c:if>
								--%>	
						</tr>
						</table>
						</c:if>	
</c:if>

	</div>
	
	<c:if test="${parameter != 'COUNTRY'}" >
		<s:hidden name="refMaster.address1"  />
	</c:if>
	<s:hidden name="refMaster.address2"  />
	<s:hidden name="refMaster.city"/>
	<s:hidden name="refMaster.state"/>
	<s:hidden name="refMaster.zip"/>
	<s:hidden name="refMaster.globallyUniqueIdentifier"/>

	
	
	
	<c:if test="${not empty refMaster.stopDate}">
	<fmt:formatDate var="stopDateFormattedValue" value="${refMaster.stopDate}" pattern="${displayDateTimeFormat}"/>
	<s:hidden name="refMaster.stopDate" value="${stopDateFormattedValue}"/>
	</c:if>
	

</s:form >

<script type="text/javascript"> 
	try{
	var f = document.getElementById('refMasterForm'); 
	f.setAttribute("autocomplete", "off");
	}
	catch(e){}
    Form.focusFirstElement($("refMasterForm")); 
</script>



