package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsPreviewTripDao;
import com.trilasoft.app.model.DsPreviewTrip;

public class DsPreviewTripDaoHibernate extends GenericDaoHibernate<DsPreviewTrip, Long> implements DsPreviewTripDao {

	public DsPreviewTripDaoHibernate() {
		super(DsPreviewTrip.class);
	}

	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsPreviewTrip where id = "+id+" ");
	}

	

}
