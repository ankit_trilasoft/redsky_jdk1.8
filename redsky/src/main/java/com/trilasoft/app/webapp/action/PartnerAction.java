package com.trilasoft.app.webapp.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Address;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction;
import org.jaxen.function.SubstringFunction;
import org.springframework.aop.ThrowsAdvice;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.PartnerDaoHibernate.DriverLocationListDTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.service.AccountContactManager;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.AgentBaseManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractPolicyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.EntitlementManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PartnerRatesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
public class PartnerAction extends BaseAction implements Preparable {

	private AgentBase agentBase;

	private AgentBaseManager agentBaseManager;
	
	private Long sid;
	
	private ServiceOrderManager serviceOrderManager;
	
	private ServiceOrder serviceOrder;
	
	private AccountContactManager accountContactManager;

	private AccountContact accountContact;

	private AccountProfileManager accountProfileManager;

	private AccountProfile accountProfile;

	private PartnerManager partnerManager;

	private PartnerRatesManager partnerRatesManager;

	private Set partners;

	private List partnerRatess;

	private Partner partner;

	private CustomerFile customerFile;

	private CustomerFileManager customerFileManager;

	private Long id;

	private String origin;

	private String destination;
	
	private String code;
	
	private List findAcctRefNumList;
	
	private List findPartnerProfileList;
	
	private String findFor;
	
	private String customerVendor="";

	private Long maxId;

	private List ls;

	private List multiAuthorization = new ArrayList();

	private List maxPartnerCode;

	private Long newPartnerCode;

	private Boolean checkPartnerCode;

	private String dupPartnerCode;

	private List dupPartnerCodeList;

	private String accPartnerCode;

	private List refMasters;

	private RefMasterManager refMasterManager;

	private List carrierCodeList = new ArrayList();

	private Map<String, String> ratetype;

	private Map<String, String> state;

	private Map<String, String> country;

	private Map<String, String> countryDesc;

	private Map<String, String> rank;

	private Map<String, String> house;

	private Map<String, String> isdriver;

	private Map<String, String> billgrp;

	private Map<String, String> billinst;

	private Map<String, String> paytype;

	private Map<String, String> payopt;

	private Map<String, String> schedule;

	private Map<String, String> psh;

	private Map<String, String> glcodes;

	private  Map<String, String> coord = new LinkedHashMap<String, String>();

	private  Map<String, String> sale = new LinkedHashMap<String, String>();

	
	private  Map<String, String> all_user = new LinkedHashMap<String, String>();

	private Map<String, String> ocountry;

	private Map<String, String> lead;

	private Map<String, String> currency;

	private Map<String, String> industry;

	private Map<String, String> revenue;

	// As on Workflow Related Enhancements April 5 2009
	
	private  Map<String, String> pricing = new LinkedHashMap<String, String>();

	private  Map<String, String> billing = new LinkedHashMap<String, String>();


	private  Map<String, String> payable = new LinkedHashMap<String, String>();

	private  Map<String, String> coordinator = new LinkedHashMap<String, String>();


	private String sessionCorpID;
	
	private String sessionUserName;

	private String companyDivision;

	// Enhansment after 27-02-2008
	
	private String partnerType;
	private String ownerName;

	private String popupval;
	private List prefixList;

	private Map<String, String> typeOfVendor;

	private Map<String, String> partnerStatus;

	private Map<String, String> storageBillingGroup;

	private String partnerCode;

	private List vendorCodeList;

	private List<AccountProfile> accountProfiles;

	private List accountContacts;

	private List baseAgent;

	private List stageunits;

	private static List pType;

	private DataSecurityFilterManager dataSecurityFilterManager;

	private DataSecuritySetManager dataSecuritySetManager;

	private List companyDivis = new ArrayList();

	private String flag;

	private String compDiv;

	private String hitFlag;

	private String gotoPageString;

	private String validateFormNav;
	private String jobRelo;
	private Map<String, String> bstates;

	private Map<String, String> tstates;

	private Map<String, String> mstates;
	
	private Map<String, String> driverType;

	public Map<String, String> getDriverType() {
		return driverType;
	}

	public void setDriverType(Map<String, String> driverType) {
		this.driverType = driverType;
	}
	private String formClose;

	private DataSecurityFilter dataSecurityFilter;

	private DataSecuritySet dataSecuritySet;

	private User user;

	private UserManager userManager;

	private String userCheck;

	private String partnerId;

	private String buttonType;

	private static Map<String, String> PWD_HINT;

	private String userId;

	private Set<Role> userRoles;

	private Set<DataSecuritySet> permissions;

	private List partnerUsersList;

	private RoleManager roleManager;

	private String userName;

	private String totalRevenue;

	private List partnerChildAgentList;

	private String perId;

	private Map<String, String> creditTerms;
	private Boolean checkTransfereeInfopackage;

	// According to new search Enhancement//

	private String countryCodeSearch;

	private String stateSearch;
	
	private String vanlineCode;

	private String countrySearch;

	private Map<String, String> actionType;
	
	private Map<String, String> actionTypeView;
	
	private String paramView = "";
	
	private ContractPolicyManager contractPolicyManager;
	
	private ContractPolicy contractPolicy;
	
	private List partnerAgentList;
	
	private String compDivision = "";

	private File file;

	private File file1;
	private File file2;
	private File file3;

	private String fileFileName;
	private String file1FileName;
	private String file2FileName;
	private String file3FileName;
	private List multiplequalityCertifications;
	private List multiplVanlineAffiliations;
	private List multiplServiceLines;

	private Map<String, String> facilitySize;

	private Map<String, String> vanlineAffiliations;

	private Map<String, String> serviceLines;

	private Map<String, String> qualityCertifications;
	
	private List partnerRating;
	
	private BigDecimal rating = new BigDecimal("0");
	
	private BigDecimal rating6m = new BigDecimal("0");
	
	private BigDecimal rating2y = new BigDecimal("0");
	
	private BigDecimal ratingACC = new BigDecimal("0");
	
	private BigDecimal rating6mACC = new BigDecimal("0");
	
	private BigDecimal rating2yACC = new BigDecimal("0");
	
	private String ratingValue;
	
	private String year;
	
	private List ratingFeedback;
	
	private String corpId;
	
	private List partnerListNew;
	
	private String exList;
	
	private PartnerPublic partnerPublic;
	
	private PartnerPublicManager partnerPublicManager; 
	
	private PartnerAccountRefManager partnerAccountRefManager;
	
	private PartnerPrivate partnerPrivate;
	
	private PartnerPrivateManager partnerPrivateManager; 
	
	private Boolean isIgnoreInactive = false; 
	
	private List partnerRatingSixMonths;
	private  Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
	private List partnerRatingOneYr;
	
	private List partnerRatingTwoYr;
	private List partnerCodeRecord = new ArrayList();
	private String vlCode;
	private Map<String, String> driverTypeMap;	
	private List totalAmmountList;
	private String totalAmmount;
	private String sentMailFlag;
	private String mailStatus;
	private String mailFailure; 
	private String emailTo;
	private Company company;
	private CompanyManager companyManager;
	private String confirmPasswordNew;
	private List gender;
    private List basedAtNameList;
    private String basedAtCode;
    private List JobFunction;
    private EntitlementManager entitlementManager;
    private String vanlineCodeSearch;
    private String userJobType;
    private String search;
    private String userRolesValue;
    private List availRoles;
    private List<SystemDefault> sysDefaultDetail;
    private String fromEmail;
    private String orgAddress;
    private String destAddress;
    private EmailSetupManager  emailSetupManager;
    private Boolean cmmDmmFlag=false;
    private Map<String, String> listOfVendorCode;
    private String searchListOfVendorCode;
    private String billToCode;
    private String countryCode;
    private String subOrigin;
    private String subDestin;
    private int defaultContactPersonCount=0;
    private String companyDivisionAcctgCodeUnique;
    private boolean disableUserContract =false;
	private Boolean agentSearchValidation;
	private Boolean accountSearchValidation;
	
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

    Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(PartnerAction.class);
    
	public PartnerAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.sessionUserName= user.getUsername();
	}
    private Map<String, String> countryCod;
	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}
    private String enbState;
	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public void prepare() throws Exception {
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax"));
		if(getRequest().getParameter("ajax") == null){ 
		getComboList(sessionCorpID);
        if (getRequest().getMethod().equalsIgnoreCase("post")) {
            if (getRequest().getParameter("user.id")!=null && (!getRequest().getParameter("user.id").equalsIgnoreCase(""))) {
                 user = userManager.getUser(getRequest().getParameter("user.id"));
            }
        }
        company = companyManager.findByCorpID(sessionCorpID).get(0);
		bstates = customerFileManager.findDefaultStateList("", sessionCorpID);
		tstates = customerFileManager.findDefaultStateList("", sessionCorpID);
		mstates = customerFileManager.findDefaultStateList("", sessionCorpID);
		countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
		enbState = customerFileManager.enableStateList(sessionCorpID);
		gender=new ArrayList();
		gender.add("Male");
		gender.add("Female");
		}
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				
				 if(systemDefault.getAgentSearchValidation() !=null && systemDefault.getAgentSearchValidation()==true){
		            	agentSearchValidation=true;
			      }
				 if(systemDefault.getAccountSearchValidation() !=null && systemDefault.getAccountSearchValidation()==true){
					 accountSearchValidation=true;
			      }
				 else
				 {
               accountSearchValidation=false;
				 }
			}
		}

	}

	@SkipValidation
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		/* Code added by kunal for ticket number: 6176 */
		getRequest().setAttribute("soLastName","");
		//company = companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		/* code completed for 6176 */
		String parameter =  (String) getRequest().getSession().getAttribute("paramView");
		if(parameter != null && parameter.equalsIgnoreCase("View")){
			getRequest().getSession().removeAttribute("paramView");
		}
		
		//partners = new HashSet(partnerManager.getAll());
		partner = new Partner();
		partner.setIsAccount(true);
		partner.setIsPrivateParty(true);
		partner.setIsVendor(true);
		partner.setIsAgent(true);
		partner.setIsCarrier(true);
		partner.setIsOwnerOp(true);
		isIgnoreInactive = true;
		partners = new HashSet(partnerManager.getPartnerPopupListAdmin("", sessionCorpID, "", "", "","", "", "", "", "",false,false,false,false,false,false,true, "","","","","","","","","","","",""));
		//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String partnerAgentList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		
		String parameter =  (String) getRequest().getSession().getAttribute("paramView");
		if(parameter != null && parameter.equalsIgnoreCase("View")){
			getRequest().getSession().removeAttribute("paramView");
		}
		
		partners = new HashSet(partnerManager.getPartnerPopupListAdmin("", sessionCorpID, "", "", "","", "", "", "", "",false,false,true,false,false,false,true,"","","","","","","","","","","",""));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerManager.remove(partner.getId());
		saveMessage(getText("partner.deleted"));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String creditTerm;
	private String partnerPrefix;
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		if(paramView.equalsIgnoreCase("View")){
			getRequest().getSession().setAttribute("paramView", paramView);
		}
		List compDivFlag = customerFileManager.findByCorpId(sessionCorpID);
		if(!compDivFlag.isEmpty() && (compDivFlag.get(0)!=null)){
			compDivision = (String)compDivFlag.get(0);
		}
		if (id != null) {
			partner = partnerManager.get(id);
			creditTerm=partner.getCreditTerms();
			partnerPrefix=partner.getPartnerPrefix();
			try{
				multiplequalityCertifications= new ArrayList();
				multiplVanlineAffiliations= new ArrayList();
				multiplServiceLines= new ArrayList();
				String[] qc = partner.getQualityCertifications().split(",");
				int arrayLength1 = qc.length;
				for (int j = 0; j < arrayLength1; j++) {
					String qacCerti=qc[j];
					if (qacCerti.indexOf("(") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}
					if (qacCerti.indexOf("'") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}
					
					multiplequalityCertifications.add(qacCerti.trim());
				}
					String[] va = partner.getVanLineAffiliation().split(",");
					int arrayLength2 = va.length;
					for (int k = 0; k < arrayLength2; k++) {
						String vanCer=va[k];
						if (vanCer.indexOf("(") == 0) {
							vanCer = vanCer.substring(1);
						}
						if (vanCer.lastIndexOf(")") == vanCer.length() - 1) {
							vanCer = vanCer.substring(0, vanCer.length() - 1);
						}
						if (vanCer.indexOf("'") == 0) {
							vanCer = vanCer.substring(1);
						}
						if (vanCer.lastIndexOf("'") == vanCer.length() - 1) {
							vanCer = vanCer.substring(0, vanCer.length() - 1);
						}
						
						multiplVanlineAffiliations.add(vanCer.trim());
				}
					
					String[] servLi = partner.getServiceLines().split(",");
					int arrayLength3 = servLi.length;
					for (int i = 0; i < arrayLength3; i++) {
						String serLines=servLi[i];
						if (serLines.indexOf("(") == 0) {
							serLines = serLines.substring(1);
						}
						if (serLines.lastIndexOf(")") == serLines.length() - 1) {
							serLines = serLines.substring(0, serLines.length() - 1);
						}
						if (serLines.indexOf("'") == 0) {
							serLines = serLines.substring(1);
						}
						if (serLines.lastIndexOf("'") == serLines.length() - 1) {
							serLines = serLines.substring(0, serLines.length() - 1);
						}
						
						multiplServiceLines.add(serLines.trim());
				}
				}catch(Exception ex){
					ex.printStackTrace();
				
			}
			bstates = customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
			List terminalCountryCode = partnerManager.getCountryCode(partner.getTerminalCountry());
			List mailIngCountryCode = partnerManager.getCountryCode(partner.getMailingCountry());
			try {
				if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
					tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
				} else {
					tstates = customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
				}
				if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
					mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
				} else {
					mstates = customerFileManager.findDefaultStateList(partner.getMailingCountryCode(), sessionCorpID);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			accountProfiles = accountProfileManager.findAccountProfile(sessionCorpID, partner.getPartnerCode());
			accountContacts = accountContactManager.getAccountContacts(partner.getPartnerCode(), sessionCorpID);
			
			if (!(accountProfiles == null || accountProfiles.isEmpty()) && (accountProfiles.get(0)!=null)) {
				accountProfile =(AccountProfile) accountProfileManager.findAccountProfile(sessionCorpID, partner.getPartnerCode()).get(0);
			}else{
				accountProfile = new AccountProfile();
			}
			
				
			List contractPolicyList = contractPolicyManager.getContractPolicy(partner.getPartnerCode(), sessionCorpID);
			if(!contractPolicyList.isEmpty() && (contractPolicyList.get(0)!=null)){
				contractPolicy = (ContractPolicy) contractPolicyList.get(0);
			}else{
				contractPolicy = new ContractPolicy();
			}
			
			partnerRatess = new ArrayList(partner.getPartnerRatess());
			baseAgent = agentBaseManager.getbaseListByPartnerCode(partner.getPartnerCode());
			
			if(paramView.equalsIgnoreCase("View") && partnerType.equalsIgnoreCase("AC")){
				
				if (!(accountProfiles == null || accountProfiles.isEmpty())) {
					for (AccountProfile accountProfile1 : accountProfiles) {
						if(accountProfile1.getStage().equalsIgnoreCase("Prospect")){
							getRequest().getSession().setAttribute("paramView", paramView);
							return SUCCESS;
						}else{
							hitFlag = "3";
							return "view";
			    			}
		    			}
				}else{
					hitFlag = "3";
					return "view";
	    			}
			}
		
		} else {
			partner = new Partner();
			partner.setStatus("New");
			bstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			tstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			mstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			partner.setCorpID(sessionCorpID);
			partner.setCreatedOn(new Date());
			partner.setUpdatedOn(new Date());
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		if(exList != null && exList.equalsIgnoreCase("true")){
			partner = (Partner) getRequest().getSession().getAttribute("partner");
			getRequest().getSession().removeAttribute("partner");
		}
		
		//getComboList(sessionCorpID);
		if (cancel != null) {
			return "cancel";
		}

		if (delete != null) {
			return delete();
		}
		boolean isNew = (partner.getId() == null);
		
		if(partner.getFirstName() == null){
			partner.setFirstName("");
		}

		partner.setCorpID(sessionCorpID);
		partner.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			if(exList == null && (popupval == null || !popupval.equalsIgnoreCase("true"))){
				String countryCode="";
				if(partner.getBillingCountryCode() != null){
					countryCode = partner.getBillingCountryCode();
				}else{
					countryCode = partner.getTerminalCountryCode();
				}
				String BillingCity="";
			 	if(partner !=null && partner.getBillingCity() !=null){
			 		BillingCity=partner.getBillingCity().trim();	
			 	}
				partnerListNew = partnerManager.getPartnerListNew(partner.getLastName(),countryCode,sessionCorpID,BillingCity);
				if(partnerListNew.size() > 0){
					getRequest().getSession().setAttribute("partner", partner);
					String key = "Similar Partners records are listed, if you find that the partner you were trying to add is in this list, press Record Found else press Record Not Found to save your request.";
					saveMessage(getText(key));
					return "PartnerEX";
				}
			}
			
			
			partner.setCreatedOn(new Date());

			String maxPartnerCode;
			try {
				if ((partnerManager.findPartnerCode("P10001")).isEmpty()) {
					maxPartnerCode = "P10001";
				} else {
					List maxCode = partnerManager.findMaxByCode();

					maxPartnerCode = "P" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
					if (!(partnerManager.findPartnerCode(maxPartnerCode) == null || (partnerManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
						maxPartnerCode = "P" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
					}
				}
			} catch (Exception ex) {
				maxPartnerCode = "P10001";
				ex.printStackTrace();
			}

			partner.setPartnerCode(maxPartnerCode);
			partner.setAgentParent(maxPartnerCode);

		}
		partner.setUpdatedOn(new Date());
		
		if (partner.getIsPrivateParty() == true) {
			if (partner.getBillingState() == null || partner.getBillingState().trim().length() <= 0) {
				partner.setBillingState("  ");
			}
			if (partner.getBillingCity() == null || partner.getBillingCity().trim().length() <= 0) {
				partner.setBillingCity("  ");
			}
			if (partner.getMailingState() == null || partner.getMailingState().trim().length() <= 0) {
				partner.setMailingState("  ");
			}
			if (partner.getMailingCity() == null || partner.getMailingCity().trim().length() <= 0) {
				partner.setMailingCity("  ");
			}
		}

		List terminalCountryCode = partnerManager.getCountryCode(partner.getTerminalCountry());
		if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
			partner.setTerminalCountryCode(terminalCountryCode.get(0).toString());
			tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
		}

		List mailIngCountryCode = partnerManager.getCountryCode(partner.getMailingCountry());

		if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
			partner.setMailingCountryCode(mailIngCountryCode.get(0).toString());
			mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
		}

		
		try {
			boolean portalIdActive = (partner.getPartnerPortalActive() == (true));
			if (portalIdActive) {
				String prefix = "DATA_SECURITY_FILTER_AGENT_" + partner.getPartnerCode();
				String prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partner.getPartnerCode();
				List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
				List agentFilterSetList = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
				if ((agentFilterList ==null || agentFilterList.isEmpty()) && (agentFilterSetList == null || agentFilterSetList.isEmpty())) {
					List nameList = new ArrayList();
					nameList.add("BOOKINGAGENTCODE");
					nameList.add("BROKERCODE");
					nameList.add("ORIGINAGENTCODE");
					nameList.add("ORIGINSUBAGENTCODE");
					nameList.add("DESTINATIONAGENTCODE");
					nameList.add("DESTINATIONSUBAGENTCODE");
					dataSecuritySet = new DataSecuritySet();
					Iterator it = nameList.iterator();
					while (it.hasNext()) {
						String fieldName = it.next().toString();
						String postFix = prefix + "_" + fieldName;
						dataSecurityFilter = new DataSecurityFilter();
						dataSecurityFilter.setName(postFix);
						dataSecurityFilter.setTableName("serviceorder");
						dataSecurityFilter.setFieldName(fieldName.toLowerCase());
						dataSecurityFilter.setFilterValues(partner.getPartnerCode().toString());
						dataSecurityFilter.setCorpID(sessionCorpID);
						dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
						dataSecuritySet.addFilters(dataSecurityFilter);
					}
					dataSecuritySet.setName(prefixDataSet);
					dataSecuritySet.setDescription(prefixDataSet);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
				}
				List associatedList = new ArrayList();
				childAgentCodeMap = dataSecuritySetManager.getChildAgentCodeMap();	
				boolean childViewActive = (partner.getViewChild() == (true));
				if (childViewActive) {
					getChildAgentlist(partner.getPartnerCode(), associatedList);
					associatedList.add(partner.getAgentParent());
					//System.out.println("\n\n\n\n\n\n associatedList---->" + associatedList);
					Iterator it2 = associatedList.iterator();
					while (it2.hasNext()) {
						String pcode = it2.next().toString().trim();
						if (!pcode.equalsIgnoreCase("")) {
							String prefix1 = "DATA_SECURITY_FILTER_AGENT_" + pcode;
							String prefixDataSet1 = "DATA_SECURITY_SET_AGENT_" + pcode;
							List agentFilterList1 = dataSecurityFilterManager.getAgentFilterList(prefix1, sessionCorpID);
							List agentFilterSetList1 = dataSecuritySetManager.getDataSecuritySet(prefixDataSet1, sessionCorpID);
							if ((agentFilterList1  ==null || agentFilterList1.isEmpty()) && (agentFilterSetList1 ==null || agentFilterSetList1.isEmpty())) {
								List nameList = new ArrayList();
								nameList.add("BOOKINGAGENTCODE");
								nameList.add("BROKERCODE");
								nameList.add("ORIGINAGENTCODE");
								nameList.add("ORIGINSUBAGENTCODE");
								nameList.add("DESTINATIONAGENTCODE");
								nameList.add("DESTINATIONSUBAGENTCODE");
								dataSecuritySet = new DataSecuritySet();
								Iterator it3 = nameList.iterator();
								while (it3.hasNext()) {
									String fieldName = it3.next().toString();
									String postFix = prefix1 + "_" + fieldName;
									dataSecurityFilter = new DataSecurityFilter();
									dataSecurityFilter.setName(postFix);
									dataSecurityFilter.setTableName("serviceorder");
									dataSecurityFilter.setFieldName(fieldName.toLowerCase());
									dataSecurityFilter.setFilterValues(pcode);
									dataSecurityFilter.setCorpID(sessionCorpID);
									dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
									dataSecuritySet.addFilters(dataSecurityFilter);
								}
								dataSecuritySet.setName(prefixDataSet1);
								dataSecuritySet.setDescription(prefixDataSet1);
								dataSecuritySet.setCorpID(sessionCorpID);
								dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
							}
						}

					}

				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		if (partner.getBillingCountry() == null || partner.getBillingCountry().trim().equals("")) {
			partner.setBillingCountry("");
			partner.setBillingCountryCode("");
		}
		if (partner.getTerminalCountry() == null || partner.getTerminalCountry().trim().equals("")) {
			partner.setTerminalCountry("");
			partner.setTerminalCountryCode("");
		}
		if (partner.getMailingCountry() == null || partner.getMailingCountry().trim().equals("")) {
			partner.setMailingCountry("");
			partner.setMailingCountryCode("");
		}
		if (partner.getBillingState() == null) {
			partner.setBillingState("");
		}
		
				
		if (partner.getTerminalCity() == null) {
			partner.setTerminalCity("");
		}
		if (partner.getTerminalCountryCode() == null) {
			partner.setTerminalCountryCode("");
		}
		if (partner.getTerminalState() == null) {
			partner.setTerminalState("");
		}
		
		try{
		if(file != null){
		
	        if ( file.length() > 41943040) {
	            addActionError(getText("maxLengthExceeded"));
	            return INPUT;
	        }
	        
	        String uploadDir =ServletActionContext.getServletContext().getRealPath("/images") +"/" ;
	        
	        String rightUploadDir = uploadDir.replace("redsky", "userData");  
	        
	        File dirPath = new File(rightUploadDir);
	        if (!dirPath.exists()) {
	            dirPath.mkdirs();
	        }
	        
	        if(file.exists()){
	        InputStream stream = new FileInputStream(file);
	        
	        OutputStream bos = new FileOutputStream(rightUploadDir + fileFileName);
	        int bytesRead = 0;
	        byte[] buffer = new byte[8192];

	        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
	            bos.write(buffer, 0, bytesRead);
	        }

	        bos.close();  
	        stream.close();
	        
	        getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
	        partner.setLocation1(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
	        }
	        }
		
		if(file1 != null){
			     if ( file1.length() > 41943040) {
		            addActionError(getText("maxLengthExceeded"));
		            return INPUT;
		        }
		        
		        String uploadDir =
		            ServletActionContext.getServletContext().getRealPath("/images") + "/" ;
		        
		        String rightUploadDir = uploadDir.replace("redsky", "userData");  
		        
		        File dirPath = new File(rightUploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
		        
		        if(file1.exists()){
		        InputStream stream = new FileInputStream(file1);
		        
		        OutputStream bos = new FileOutputStream(rightUploadDir + file1FileName);
		        int bytesRead = 0;
		        byte[] buffer = new byte[8192];

		        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
		            bos.write(buffer, 0, bytesRead);
		        }

		        bos.close();  
		        stream.close();
		        
		        getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
		        partner.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
		        }
		        }
		
		if(file2 != null){
			      if ( file2.length() > 41943040) {
		            addActionError(getText("maxLengthExceeded"));
		            return INPUT;
		        }
		        
		        String uploadDir =ServletActionContext.getServletContext().getRealPath("/images") + "/" ;
		        
		        String rightUploadDir = uploadDir.replace("redsky", "userData");  
		        
		        File dirPath = new File(rightUploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
		        
		        if(file2.exists()){
		        InputStream stream = new FileInputStream(file2);
		        
		        OutputStream bos = new FileOutputStream(rightUploadDir + file2FileName);
		        int bytesRead = 0;
		        byte[] buffer = new byte[8192];

		        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
		            bos.write(buffer, 0, bytesRead);
		        }

		        bos.close();  
		        stream.close();
		        
		        getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
		        partner.setLocation3(dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
		        }
		        }
		
		if(file3 != null){
			     if ( file3.length() > 41943040) {
		            addActionError(getText("maxLengthExceeded"));
		            return INPUT;
		        }
		        
		        String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") +  "/" ;
		        
		        String rightUploadDir = uploadDir.replace("redsky", "userData");  
		        
		        File dirPath = new File(rightUploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
		        
		        if(file3.exists()){
		        InputStream stream = new FileInputStream(file3);
		        
		        OutputStream bos = new FileOutputStream(rightUploadDir + file3FileName);
		        int bytesRead = 0;
		        byte[] buffer = new byte[8192];

		        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
		            bos.write(buffer, 0, bytesRead);
		        }

		        bos.close();  
		        stream.close();
		        
		        getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
		        partner.setLocation4(dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
		        }
		        }
		
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		partner=partnerManager.save(partner);
		
		String paramCheck = (String)getRequest().getSession().getAttribute("paramView");
		if(paramCheck != null && paramCheck.equalsIgnoreCase("View") && isNew && partnerType.equalsIgnoreCase("AC")){
			accountProfile = new AccountProfile();
			accountProfile.setStage("Prospect");
			accountProfile.setPartnerCode(partner.getPartnerCode());
			accountProfile.setCorpID(sessionCorpID);
			accountProfile.setCreatedOn(new Date());
			accountProfile.setCreatedBy(getRequest().getRemoteUser());
			accountProfile.setUpdatedOn(new Date());
			accountProfile.setUpdatedBy(getRequest().getRemoteUser());
			accountProfileManager.save(accountProfile);
		}
		
		

		formClose = "close";
		bstates = customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
		// tstates = customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
		try{
		multiplequalityCertifications= new ArrayList();
		multiplVanlineAffiliations= new ArrayList();
		multiplServiceLines= new ArrayList();
	    String qc1= new String();
		qc1=partner.getQualityCertifications().trim();
		if (qc1.indexOf(",") == 0) {
			qc1 = qc1.substring(1);
		}
		String[] qc = qc1.trim().split(",");
		int arrayLength1 = qc.length;
		for (int j = 0; j < arrayLength1; j++) {
			String qacCerti=qc[j];
			if (qacCerti.indexOf("(") == 0) {
				qacCerti = qacCerti.substring(1);
			}
			if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
			}
			if (qacCerti.indexOf("'") == 0) {
				qacCerti = qacCerti.substring(1);
			}
			if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
			}
			
			multiplequalityCertifications.add(qacCerti.trim());
		}
		partner.setQualityCertifications(multiplequalityCertifications.toString().replace("[", "").replace("]", ""));
		String va1= new String();
		va1=partner.getVanLineAffiliation().trim();
		if (va1.indexOf(",") == 0) {
			va1 = va1.substring(1);
		}
		String[] va = va1.split(",");
			int arrayLength2 = va.length;
			for (int k = 0; k < arrayLength2; k++) {
				String vanCer=va[k];
				if (vanCer.indexOf("(") == 0) {
					vanCer = vanCer.substring(1);
				}
				if (vanCer.lastIndexOf(")") == vanCer.length() - 1) {
					vanCer = vanCer.substring(0, vanCer.length() - 1);
				}
				if (vanCer.indexOf("'") == 0) {
					vanCer = vanCer.substring(1);
				}
				if (vanCer.lastIndexOf("'") == vanCer.length() - 1) {
					vanCer = vanCer.substring(0, vanCer.length() - 1);
				}
				
				multiplVanlineAffiliations.add(vanCer.trim());
		}
			partner.setVanLineAffiliation(multiplVanlineAffiliations.toString().replace("[", "").replace("]", ""));
			String servLi1= new String();
			servLi1=partner.getServiceLines().trim();
			if (servLi1.indexOf(",") == 0) {
				servLi1 = servLi1.substring(1);
			}
			String[] servLi = servLi1.split(",");
			int arrayLength3 = servLi.length;
			for (int l = 0; l < arrayLength3; l++) {
				String serLines=servLi[l];
				if (serLines.indexOf("(") == 0) {
					serLines = serLines.substring(1);
				}
				if (serLines.lastIndexOf(")") == serLines.length() - 1) {
					serLines = serLines.substring(0, serLines.length() - 1);
				}
				if (serLines.indexOf("'") == 0) {
					serLines = serLines.substring(1);
				}
				if (serLines.lastIndexOf("'") == serLines.length() - 1) {
					serLines = serLines.substring(0, serLines.length() - 1);
				}
				
				multiplServiceLines.add(serLines.trim());
				
		}
			partner.setServiceLines(multiplServiceLines.toString().replace("[", "").replace("]", ""));
		}catch(Exception ex)
		{
			ex.printStackTrace();	
		}
		if (!popupval.equalsIgnoreCase("true")) {
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "partner.added" : "partner.updated";
				saveMessage(getText(key));
			}
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		if (!isNew) {
			return INPUT;
		} else {
			maxId = Long.parseLong(((partnerManager.findMaximum().get(0)).toString()));
			//System.out.println(maxId);
			partner = partnerManager.get(maxId);
			return SUCCESS;
		}

	}
	
	public String getChildAgentlist(String parentAgentCode, List agentList) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		List	associatedAgentsList=new ArrayList();
		String parentCorpID =company.getParentCorpId();
		if(childAgentCodeMap!=null && childAgentCodeMap.containsKey("TSFT~"+parentAgentCode.trim())){
			associatedAgentsList=childAgentCodeMap.get("TSFT~"+parentAgentCode.trim());
		}else if(childAgentCodeMap!=null && childAgentCodeMap.containsKey(parentCorpID.trim()+"~"+parentAgentCode.trim())){
			associatedAgentsList=childAgentCodeMap.get(parentCorpID.trim()+"~"+parentAgentCode.trim());
		}else{
			associatedAgentsList=childAgentCodeMap.get(sessionCorpID.trim()+"~"+parentAgentCode.trim());
		}
		//List associatedAgentsList = dataSecuritySetManager.getAssociatedAgents(parentAgentCode, sessionCorpID);
		if (associatedAgentsList!=null && !associatedAgentsList.isEmpty()) {
			if (associatedAgentsList.get(0) != null) {
				String ag = associatedAgentsList.toString();
				ag = ag.replace("[", "");
				ag = ag.replace("]", "");
				String[] arrayid1 = ag.split(",");
				int arrayLength1 = arrayid1.length;
				for (int i = 0; i < arrayLength1; i++) {
					agentList.add(arrayid1[i]);
					if (getChildAgentlist(arrayid1[i], agentList) == null) {
						break;
					}

				}
			}

		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String findAcctRefNumList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		companyDivisionAcctgCodeUnique= partnerAccountRefManager.findAccountInterface(sessionCorpID);
		findAcctRefNumList = partnerManager.findAcctRefNum(code, sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	  }
	@SkipValidation
	public String findPartnerProfileList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
			//partner = partnerManager.get(id);
			partnerPublic = partnerPublicManager.get(id);
			if(!partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).isEmpty() && (partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).get(0)!=null)){
				partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getPartnerCode()).get(0);
			}		
			findPartnerProfileList = partnerManager.findPartnerProfile(partnerPublic.getPartnerCode(), sessionCorpID);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	  }
	private String checkOption;
	@SkipValidation
	public String partnerUsersList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner = partnerManager.get(id);
		/* Code added by kunal for ticket number: 6176 */
		//company = companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		/* code completed for 6176 */
		partnerPublic = partnerPublicManager.get(id);
		//String prefixDataSet="";
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		//User user = (User) auth.getPrincipal();
		//if(user.getRoles().toString().contains("ROLE_AGENT"))
        //{
		//	prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
		//	partnerUsersList = dataSecuritySetManager.getPartnerUsersList(prefixDataSet, sessionCorpID);
        //}else{
		List donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, partnerPublic.getPartnerCode());
		if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null") && sessionCorpID!=null && sessionCorpID.equalsIgnoreCase("TSFT")){
			disableUserContract=true;
		}
		if(checkOption==null)
		{
			checkOption="BOTH";
		}
        	partnerUsersList=dataSecuritySetManager.getAgentsUsersList(partnerPublic.getPartnerCode(), sessionCorpID,checkOption);
         //}
        	
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String partnerUserUnassignedList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		/* Code added by kunal for ticket number: 6176 */
		List<Company> listCompany = companyManager.findByCorpID(sessionCorpID);
		company = listCompany.get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		/* code completed for 6176 */
		String prefixDataSet="";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if(user.getUserType().toString().trim().equalsIgnoreCase("AGENT"))
        {
			prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
        }else{
        	prefixDataSet = partnerPublic.getPartnerCode();
        }
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
private String agentContact;
	@SkipValidation
	public String assignDataSet() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
		childAgentCodeMap = dataSecuritySetManager.getChildAgentCodeMap();	
		String prefixDataSet = "";
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		List associatedList = new ArrayList();		
		//partnerPrivate = (PartnerPrivate)partnerPrivateManager.findPartnerCode(partnerPublic.getPartnerCode()).get(0);		
		boolean childViewActive = (partnerPublic.getViewChild() == (true));
		if (childViewActive) {
			getChildAgentlist(partnerPublic.getPartnerCode(), associatedList);
			
			associatedList.add(partnerPublic.getPartnerCode());
			Iterator it = associatedList.iterator();
			while (it.hasNext()) {
				String pcode = it.next().toString().trim();
				if (!pcode.equalsIgnoreCase("")) {
					prefixDataSet = "DATA_SECURITY_SET_AGENT_" + pcode;
					userCheck = userCheck.trim();
					if (userCheck.indexOf(",") == 0) {
						userCheck = userCheck.substring(1);
					}
					if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
						userCheck = userCheck.substring(0, userCheck.length() - 1);
					}
					String[] arrayid = userCheck.split(",");
					int arrayLength = arrayid.length;
					for (int i = 0; i < arrayLength; i++) {
						id = Long.parseLong(arrayid[i]);
						user = userManager.getUser(id.toString());
						user.addRole(roleManager.getRole("ROLE_AGENT"));
						user.addRole(roleManager.getRole("ROLE_USER"));
						dataSecuritySet = (DataSecuritySet) dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID).get(0);
						user.addPermissions(dataSecuritySet);
						if (buttonType.equalsIgnoreCase("Assign as User")) {
							partnerManager.disableUser(id, sessionCorpID, "Enable");
						}
						if (buttonType.equalsIgnoreCase("Assign as Contact")) {
							partnerManager.disableUser(id, sessionCorpID, "Disable");
						}
					}
				}

			}
		} else {

			prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
			userCheck = userCheck.trim();
			if (userCheck.indexOf(",") == 0) {
				userCheck = userCheck.substring(1);
			}
			if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
				userCheck = userCheck.substring(0, userCheck.length() - 1);
			}
			String[] arrayid = userCheck.split(",");
			int arrayLength = arrayid.length;
			for (int i = 0; i < arrayLength; i++) {
				id = Long.parseLong(arrayid[i]);
				user = userManager.getUser(id.toString());
				user.addRole(roleManager.getRole("ROLE_AGENT"));
				user.addRole(roleManager.getRole("ROLE_USER"));
				dataSecuritySet = (DataSecuritySet) dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID).get(0);
				user.addPermissions(dataSecuritySet);
				if (buttonType.equalsIgnoreCase("Assign as User")) {
					partnerManager.disableUser(id, sessionCorpID, "Enable");
				}
				if (buttonType.equalsIgnoreCase("Assign as Contact")) {
					partnerManager.disableUser(id, sessionCorpID, "Disable");
				}
			}
		}
		List notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT", sessionCorpID);
		if (!notesSet.isEmpty() && (notesSet.get(0)!=null)) {
			dataSecuritySet = (DataSecuritySet) dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT", sessionCorpID).get(0);
			user.addPermissions(dataSecuritySet);
		}
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
		hitFlag = "1";
		}catch(Exception ex)
		{
			ex.printStackTrace();	
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
   private Map<String, String> userJobTypeMap;
   private List availPermissions;
	@SkipValidation
	public String addUsersFromPartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		availRoles = userManager.findUserRoles(sessionCorpID);
		childAgentCodeMap = dataSecuritySetManager.getChildAgentCodeMap();	
		
		List associatedList = new ArrayList();
		String prefixDataSet = "";
		availPermissions=new ArrayList();	
		if(!partnerType.equalsIgnoreCase("VN")){
		boolean childViewActive = (partnerPublic.getViewChild() == (true));
		if (childViewActive) {
			getChildAgentlist(partnerPublic.getPartnerCode(), associatedList);
			associatedList.add(partnerPublic.getPartnerCode());
			Iterator it = associatedList.iterator();
			while (it.hasNext()) {
				String pcode = it.next().toString().trim();
				if (!pcode.equalsIgnoreCase("")) {
					if(partnerType.equalsIgnoreCase("AC")){
						prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + pcode;
					}else{
						prefixDataSet = "DATA_SECURITY_SET_AGENT_" + pcode;
					}
					List dataset = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
					if (!dataset.isEmpty() && (dataset.get(0)!=null)) {						
						availPermissions.add(prefixDataSet);
					}
				}
			}
		} else {
			if(partnerType.equalsIgnoreCase("AC")){
				prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + partnerPublic.getPartnerCode();
			}else{
				prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
			}
			List dataset = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
			if (!dataset.isEmpty()) {
				availPermissions.add(prefixDataSet);
			}
		}
		}
		List notesSet = new ArrayList();
		if(partnerType.equalsIgnoreCase("AC")){
			notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS", sessionCorpID);
			if (!notesSet.isEmpty()) {
				availPermissions.add("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS");
			}
		}else{
			notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT", sessionCorpID);
			if (!notesSet.isEmpty() && (notesSet.get(0)!=null)) {
				availPermissions.add("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT");
			}
		}
       // availPermissions=userManager.getPermissionList();
        
		userJobTypeMap=new LinkedHashMap<String, String>() ;		
		ocountry = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		PWD_HINT = refMasterManager.findByParameter(sessionCorpID, "PWD_HINT");
		//company = companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		gender=new ArrayList();
		gender.add("Male");
		gender.add("Female");
		if (userId != null) {
			user = userManager.getUser(userId);
			String areaValue=user.getFunctionalArea();
			if(areaValue!=null && !(areaValue.equals(""))){
					String areaValue1[]=areaValue.split(","); 
					for(int i=0 ; i<areaValue1.length; i++){
						userJobTypeMap.put(areaValue1[i], areaValue1[i]);
					}
			      }
			if(!user.isEnabled() &&(user.getCorpID().equalsIgnoreCase(sessionCorpID)) ){
				agentContact="true";
			}
			sentMailFlag="Y";
		} else {
			user = new User();
			Address address = new Address();
			address.setAddress(partnerPublic.getTerminalAddress1());
			address.setCity(partnerPublic.getTerminalCity());
			address.setPostalCode(partnerPublic.getTerminalZip());
			address.setProvince(partnerPublic.getTerminalState());
			address.setCountry(partnerPublic.getTerminalCountryCode());
		
			user.setAddress(address);
			user.setEmail(partnerPublic.getTerminalEmail());
			user.setWebsite(partnerPublic.getUrl());
			user.setBranch(partnerPublic.getPartnerCode());
			user.setBasedAt(partnerPublic.getPartnerCode());
			user.setBasedAtName(partnerPublic.getLastName());
			user.setFaxNumber(partnerPublic.getTerminalFax());
			user.setPhoneNumber(partnerPublic.getTerminalPhone());
			user.setWorkStartTime("00:00");
			user.setWorkEndTime("00:00");
			user.setPasswordReset(true);
			user.setCreatedOn(new Date());
			user.setUpdatedOn(new Date());
			if(partnerType.equals("AG")){
				user.setUserType("AGENT");
			}
			if(partnerType.equals("AC")){
			    user.setUserType("ACCOUNT");
			}
			if(partnerType.equals("VN")){
			    user.setUserType("PARTNER");
			}
			user.setPasswordHintQues(" Default");
			user.setPasswordHint("Default");
			user.setDefaultSearchStatus(true);
			user.setCorpID(sessionCorpID);
	        try{	            
	            Iterator it2=availPermissions.iterator();
		            while(it2.hasNext()){
		            	String filterSet=(String)it2.next();
		            	user.addPermissions((DataSecuritySet)userManager.getPermission(filterSet).get(0));
		            }	            
	            }catch(Exception ex){
	            	ex.printStackTrace();
	            }			
		}
		try{
			if(partnerPublic.getPartnerCode()!=null){
			defaultContactPersonCount = userManager.getDefaultContactPersonCount(partnerPublic.getPartnerCode(), partnerPublic.getCorpID());
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String saveUserFromPartner() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ocountry = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		//company = companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		availRoles = userManager.findUserRoles(sessionCorpID);
		partnerPublic = partnerPublicManager.get(id);
		childAgentCodeMap = dataSecuritySetManager.getChildAgentCodeMap();	
		List associatedList = new ArrayList();
		String prefixDataSet = "";
		availPermissions=new ArrayList();
		if(!partnerType.equalsIgnoreCase("VN")){
		boolean childViewActive = (partnerPublic.getViewChild() == (true));
		if (childViewActive) {
			getChildAgentlist(partnerPublic.getPartnerCode(), associatedList);
			associatedList.add(partnerPublic.getPartnerCode());
			Iterator it = associatedList.iterator();
			while (it.hasNext()) {
				String pcode = it.next().toString().trim();
				if (!pcode.equalsIgnoreCase("")) {
					if(partnerType.equalsIgnoreCase("AC")){
						prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + pcode;
					}else{
						prefixDataSet = "DATA_SECURITY_SET_AGENT_" + pcode;
					}
					List dataset = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
					if (!dataset.isEmpty() && (dataset.get(0)!=null)) {						
						availPermissions.add(prefixDataSet);
					}
				}
			}
		} else {
			if(partnerType.equalsIgnoreCase("AC")){
				prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + partnerPublic.getPartnerCode();
			}else{
				prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
			}
			List dataset = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
			if (!dataset.isEmpty()) {
				availPermissions.add(prefixDataSet);
			}
		}
		}
		List notesSet = new ArrayList();
		if(partnerType.equalsIgnoreCase("AC")){
			notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS", sessionCorpID);
			if (!notesSet.isEmpty()) {
				availPermissions.add("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS");
			}
		}else{
			notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT", sessionCorpID);
			if (!notesSet.isEmpty() && (notesSet.get(0)!=null)) {
				availPermissions.add("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT");
			}
		}
       // availPermissions=userManager.getPermissionList();

		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {				
				fromEmail=systemDefault.getEmail();
				}
		  }
		if(fromEmail==null ||fromEmail.equals("")){
			fromEmail= "support@redskymobility.com";	
		}
		StringBuffer sb=new StringBuffer();
		//partner = partnerManager.get(id);
	
		userJobTypeMap=new LinkedHashMap<String, String>() ;
		boolean isNew = (user.getId() == null);
		user.setCorpID(sessionCorpID);
		//user.setPassword(StringUtil.encodePassword(user.getPassword(), algorithm));
		//user.setConfirmPassword(user.getPassword());
		
		 Set<Role> oldUserRoles=new HashSet<Role>();
	        oldUserRoles.addAll(user.getRoles());
	    	String[] userRolesss = getRequest().getParameterValues("userRolesValue");
	    	Set<Role> newRoleList=new HashSet<Role>();
	    	Set<Role> removeRoleList=new HashSet<Role>();
	    	if(userRolesss!=null){
		    	for(String str:userRolesss){
		    		if(!oldUserRoles.contains(roleManager.getRole(str))){
		    			newRoleList.add(roleManager.getRole(str));
		    		}
		    	}
		    	user.getRoles().addAll(newRoleList);	    	
	    	}else{
	    		user.getRoles().clear();
	    	}
	    	for(Iterator<Role> i = oldUserRoles.iterator(); i.hasNext(); ) {
	    		  Role item = i.next();
	    		  if(!Arrays.asList(getRequest().getParameterValues("userRolesValue")).contains((item.getName()))){
	    			  removeRoleList.add(item);
	      			}
	    		}
	    	if((removeRoleList!=null)&&(!removeRoleList.isEmpty())){
	        	if((removeRoleList!=null)&&(!removeRoleList.isEmpty())){
	            	if((removeRoleList!=null && !removeRoleList.isEmpty()) && user!=null && user.getId()!=null){
	            		String temp="";
	            		for(Role r:removeRoleList){
	            			if(temp.equalsIgnoreCase("")){
	            				temp="'"+r.getId()+"'";
	            			}else{
	            				temp=temp+",'"+r.getId()+"'";
	            			}
	            		}
	            		userManager.updateUserRoleInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
	            	}
	        	}
	    		user.getRoles().removeAll(removeRoleList);
	    	}
	    	
			String[] userRoles = getRequest().getParameterValues("userRolesValue");
		
			StringBuilder result1 = new StringBuilder();
			if (userRoles.length > 0) {
				if(userRoles[0]!=null && !"".equals(userRoles[0])){
					user.addRole(roleManager.getRole(userRoles[0]));
		            for (int i=1; i < userRoles.length; i++) {
		            	user.addRole(roleManager.getRole(userRoles[i]));
		            }	
				}
				
	        }
	    	Set<DataSecuritySet> newPermissionsList=new HashSet<DataSecuritySet>();
	    	Set<DataSecuritySet> removepermissionsList=new HashSet<DataSecuritySet>();

	        try{
	            Set<DataSecuritySet> DataSecuritySet23=new HashSet<DataSecuritySet>();
	            DataSecuritySet23.addAll(user.getDatasecuritysets());

    	    	String[] userpermissionss = getRequest().getParameterValues("userPermissionValue");
    	    	if(userpermissionss!=null){
        	    	for(String str:userpermissionss){
        	    		if(!DataSecuritySet23.contains((DataSecuritySet)userManager.getPermission(str).get(0))){
        	    			newPermissionsList.add((DataSecuritySet)userManager.getPermission(str).get(0));
        	    		}
        	    	} 
        	    	user.getDatasecuritysets().addAll(newPermissionsList);
    	    	}else{
    	    		user.getDatasecuritysets().clear();
    	    	}
    	    	for(Iterator<DataSecuritySet> i = DataSecuritySet23.iterator(); i.hasNext(); ) {
    	    		DataSecuritySet item = i.next();
    	    		  if(!Arrays.asList(getRequest().getParameterValues("userPermissionValue")).contains((item.getName()))){
    	    			  removepermissionsList.add(item);
    	      			}
    	    		}
    	    	if((removepermissionsList!=null)&&(!removepermissionsList.isEmpty())){
        	    	if((removepermissionsList!=null)&&(!removepermissionsList.isEmpty())){
        	    		if((removepermissionsList!=null && !removepermissionsList.isEmpty()) && user!=null && user.getId()!=null){
        	        		String temp="";
        	        		for(DataSecuritySet r:removepermissionsList){
        	        			if(temp.equalsIgnoreCase("")){
        	        				temp="'"+r.getId()+"'";
        	        			}else{
        	        				temp=temp+",'"+r.getId()+"'";
        	        			}
        	        		}
        	        		userManager.updateUserInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
        	        	}	
    	    		user.getDatasecuritysets().removeAll(removepermissionsList);
    	    	}
    	    	}
	            String[] permissions = getRequest().getParameterValues("userPermissionValue");
	            for (int i = 0; permissions != null && i < permissions.length; i++) {
	                String permissionName = permissions[i];
	                user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName).get(0));
	            }
	            }catch(Exception ex){
	            	ex.printStackTrace();
	            }
	            try{
	                if( (!(user.getUserType().toString().trim().equalsIgnoreCase("AGENT")) ) && (!(user.getUserType().toString().trim().equalsIgnoreCase("PARTNER"))) ) {       	
	              	 String permissionName1 = "DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT";
	                   user.removePermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));       	
	                }
	              }catch (Exception e) {
	            	  e.printStackTrace();
	      			// TODO: handle exception
	      		}
	              try {
						if(user.getUserType().toString().trim().equalsIgnoreCase("AGENT")){
							  String permissionName1 = "DATA_SECURITY_SET_SO_JOB_Agent";
						     // user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
								List permissionList12=userManager.getPermission(permissionName1);
						    	if(permissionList12==null || permissionList12.isEmpty()){
						    		dataSecuritySet = new DataSecuritySet();	                	
						      Set<String> vanlineJob=partnerManager.getVanlineJobDetails();
						      for (String string : vanlineJob) {
						    	  String permissionFilter="DATA_SECURITY_FILTER_SO_JOB_"+string.trim();
						    	  List agentFilterList = dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID);
						    	  if (agentFilterList.isEmpty()) {
						    		dataSecurityFilter = new DataSecurityFilter();					
									dataSecurityFilter.setName(permissionFilter);				
									dataSecurityFilter.setTableName("serviceorder");
									dataSecurityFilter.setFieldName("JOB".toLowerCase());
									dataSecurityFilter.setFilterValues(string.trim());
									dataSecurityFilter.setCorpID(sessionCorpID);
									dataSecurityFilter.setCreatedOn(new Date());
									dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
									dataSecurityFilter.setUpdatedOn(new Date());
									dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
									dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
									dataSecuritySet.addFilters(dataSecurityFilter);
						    	  }else{	                		 
						    		 dataSecuritySet.addFilters((DataSecurityFilter)dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID).get(0));
						    	  }
							}
						        dataSecuritySet.setName(permissionName1);
								dataSecuritySet.setDescription(permissionName1);
								dataSecuritySet.setCorpID(sessionCorpID);
								dataSecuritySet.setCreatedOn(new Date());
								dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
								dataSecuritySet.setUpdatedOn(new Date());
								dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
								dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
								user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
						    	}else{
						    	 DataSecuritySet DataSecuritySet1=((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
						    	 Set<String> vanlineJob=partnerManager.getVanlineJobDetails();
						          for (String string : vanlineJob) {
						        	  String permissionFilter="DATA_SECURITY_FILTER_SO_JOB_"+string.trim();
						        	  List agentFilterList = dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID);
						        	 if (agentFilterList!=null && !agentFilterList.isEmpty()) {
						    	      DataSecuritySet1.addFilters((DataSecurityFilter)dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID).get(0));
						        	  }else{
						        		    dataSecurityFilter = new DataSecurityFilter();					
						  					dataSecurityFilter.setName(permissionFilter);				
						  					dataSecurityFilter.setTableName("serviceorder");
						  					dataSecurityFilter.setFieldName("JOB".toLowerCase());
						  					dataSecurityFilter.setFilterValues(string.trim());
						  					dataSecurityFilter.setCorpID(sessionCorpID);
						  					dataSecurityFilter.setCreatedOn(new Date());
						  					dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
						  					dataSecurityFilter.setUpdatedOn(new Date());
						  					dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
						  					dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
						  				  DataSecuritySet1.addFilters(dataSecurityFilter);
						        		  DataSecuritySet1.setName(permissionName1);
						        		  DataSecuritySet1.setDescription(permissionName1);
						        		  DataSecuritySet1.setCorpID(sessionCorpID);
						        		  DataSecuritySet1.setCreatedOn(new Date());
						        		  DataSecuritySet1.setCreatedBy(getRequest().getRemoteUser());
						        		  DataSecuritySet1.setUpdatedOn(new Date());
						        		  DataSecuritySet1.setUpdatedBy(getRequest().getRemoteUser());
										dataSecuritySetManager.saveDataSecuritySet(DataSecuritySet1);  
						        	  }		                	 
						          }
						          user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
						    	}
						  }
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
				}
					
	              if((user.getUserType().toString().trim().equalsIgnoreCase("AGENT") ) || (user.getUserType().toString().trim().equalsIgnoreCase("PARTNER")) ){       	
	              	String permissionName1 = "DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT";
	                  user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
	              }else if((user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT"))){
	              	String permissionName1 = "DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS";
	                  user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
	              }else{
	              }
	              try{
	                  if(!(user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT")))
	                  {
	                  	String permissionName1 = "DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS";
	                      user.removePermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
	                     
	                  }}catch (Exception e) {
	                	  e.printStackTrace();
	          			// TODO: handle exception
	          		}

	                  
		String[] job = getRequest().getParameterValues("userJobType");
    	StringBuilder result = new StringBuilder();
    	if (job.length > 0) {
            result.append(job[0]);
            for (int i=1; i < job.length; i++) {
                result.append(",");
                result.append(job[i]);
            }
        }
    	user.setFunctionalArea(result.toString()); 
		String areaValue=user.getFunctionalArea();
		if(areaValue!=null && !(areaValue.equals(""))){
				String areaValue1[]=areaValue.split(","); 
				for(int i=0 ; i<areaValue1.length; i++){
					userJobTypeMap.put(areaValue1[i], areaValue1[i]);
				}
		      }    	
		Integer originalVersion = user.getVersion();
		/*if(partnerType.equals("AG")){
			user.addRole(roleManager.getRole("ROLE_AGENT"));
		}*/
		if(partnerType.equals("AC")){
			//user.addRole(roleManager.getRole("ROLE_CORP_ACCOUNT"));
			user.setEnabled(true);
		}
		//user.addRole(roleManager.getRole("ROLE_USER"));
			if(!user.getContact())
			{
			user.setEnabled(true);
			}else{
				if(partnerType.equals("AG")){
				user.setEnabled(false);
				user.setUsername(user.getEmail());
				user.setCorpID("TSFT");
				}
				if(partnerType.equals("VN")){
					user.setEnabled(false);
					user.setUsername(user.getEmail());
					user.setCorpID(sessionCorpID);
				}
			}	
		if (isNew) {			
			user.setCreatedOn(new Date());
			user.setCreatedBy(getRequest().getRemoteUser());
			user.setDoNotEmail(false);
			Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
			String algorithm = (String)	getConfiguration().get(Constants.ENC_ALGORITHM);
			if (algorithm == null) { 
				  log.debug("assuming testcase, setting algorithm to 'SHA'");
				  algorithm = "SHA"; 
			}
			String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
	        Random r = new Random();
	        int te=0;
	        for(int i=1;i<=8;i++){
	         te=r.nextInt(62);
	         sb.append(str.charAt(te));
	        }
	        user.setPassword(StringUtil.encodePassword(sb.toString(), algorithm));
	        user.setConfirmPassword(StringUtil.encodePassword(sb.toString(), algorithm));
	        
	        Date compareWithDate=null;
			if(company.getPartnerPasswordExpiryDuration()!=null){
				compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));	
			}else{
				compareWithDate=DateUtils.addDays(getCurrentDate(), (60));
			}
			user.setPwdexpiryDate(compareWithDate);
		}
		user.setUpdatedOn(new Date());
		user.setUpdatedBy(getRequest().getRemoteUser());
	
		try{
			if(partnerPublic.getPartnerCode()!=null){
			defaultContactPersonCount = userManager.getDefaultContactPersonCount(partnerPublic.getPartnerCode(), partnerPublic.getCorpID());
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		try {
			user.setParentAgent(partnerPublic.getPartnerCode());
			userManager.saveUser(user);
	    	if((newRoleList!=null && !newRoleList.isEmpty()) && user!=null && user.getId()!=null){
	    		String temp="";
	    		for(Role r:newRoleList){
	    			if(temp.equalsIgnoreCase("")){
	    				temp="'"+r.getId()+"'";
	    			}else{
	    				temp=temp+",'"+r.getId()+"'";
	    			}
	    		}
	    		userManager.updateUserRoleInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
	    	}
        	if((newPermissionsList!=null && !newPermissionsList.isEmpty()) && user!=null && user.getId()!=null){
        		String temp="";
        		for(DataSecuritySet r:newPermissionsList){
        			if(temp.equalsIgnoreCase("")){
        				temp="'"+r.getId()+"'";
        			}else{
        				temp=temp+",'"+r.getId()+"'";
        			}
        		}
        		userManager.updateUserInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
        	}
			String msgkey="";
			if(user.getContact() && (partnerType.equals("AG")|| partnerType.equals("VN"))){
				msgkey = "Contacts has been added successfully.";
			}else{
				msgkey = "User has been added successfully.";
			}
			
			userId= user.getId().toString();
			if(isNew && partnerType.equalsIgnoreCase("AC")){
				  msgkey = "User has been added and email has been sent successfully.";
				  String toAddress = user.getEmail();
				  String subject = "RedSky User Information";
				  String msgText="<html><head></head><body>";
				  msgText = msgText + " Dear "+user.getFirstName()+",<br><br>"+"You have been successfully registered in RedSky"+"'"+"s Global Mobility Move Management System. Here you will find all details regarding your removal(s)."+"<br><br>"+"Please click on the hyperlink below and use the username and password stated to log in."+"<br>"+"Username: "+user.getUsername()+"<br>";			  
				  msgText = msgText + " Password: "+ sb.toString() +"<br>"+"Go to: http://skyrelo.com"+"<br><br>"+"You will be prompted upon initial log in to change the password to one of your choosing. We track usage, uploads and edits by User ID so please do not share your ID with anyone including staff at your office."+"<br><br>"+"The features should be fairly easy to use but if you require assistance, or if you have any questions or have problems logging in please contact us by e-mail at "+fromEmail+"";
				  msgText= msgText + "<br><br><br>"+"With best regards," +"<br>"+ " "+"<br>"+"RedSky Team";
				  msgText= msgText + "</body></html>";
				  try {
					String from=fromEmail;
					String tempRecipient="";
					String tempRecipientArr[]=toAddress.split(",");
					if(!isNew)
						{
						for(String str1:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str1;
						}
					}
						toAddress=tempRecipient;
				  }
					
					emailSetupManager.globalEmailSetupProcess(from, toAddress, "", "", "", msgText, subject, sessionCorpID,"",user.getUsername(),"");
				  }catch (Exception mex){
					mex.printStackTrace();
					
			}
			String key="";
			if(user.getContact() && partnerType.equals("AG")){
			       key = (isNew) ? msgkey  : "Contacts has been updated successfully";
			}else{
			       key = (isNew) ? msgkey  : "User has been updated successfully";
			}
			saveMessage(getText(key));
			sentMailFlag="Y";
			hitFlag="1";		
		}} catch (UserExistsException e) {
			String key1 = "";
			if(user.getContact() && partnerType.equals("AG")){
			   key1 = "Contacts already exist, please select a new E-Mail";	
			}else{
				 key1 = "User already exist, please select a new userName";
			}
			errorMessage(getText(key1));
			e.printStackTrace();
			user.setVersion(originalVersion);
			hitFlag="0";
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	
	@SkipValidation
	public String resetPwdAndSendEmail() throws AddressException, MessagingException {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		  user = userManager.getUser(userId);
		  //company = companyManager.findByCorpID(sessionCorpID).get(0);
		  sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {				
					fromEmail=systemDefault.getEmail();
					}
			   }
			if(fromEmail==null ||fromEmail.equals("")){
				fromEmail= "support@redskymobility.com";	
			}
		  checkTransfereeInfopackage = company.getTransfereeInfopackage();
		  String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
	      StringBuffer sb=new StringBuffer();
	      Random r = new Random();
	      int te=0;
	      for(int i=1;i<=8;i++){
	         te=r.nextInt(62);
	         sb.append(str.charAt(te));
	      } 
		  String toAddress = emailTo;
		  String subject = "RedSky User Information";
		  String msgText="<html><head></head><body>";
		  msgText = msgText + " Dear "+user.getFirstName()+",<br><br>"+"You have been successfully registered in RedSky"+"'"+"s Global Mobility Move Management System. Here you will find all details regarding your removal(s) / relocations."+"<br><br>"+"Please click on the hyperlink below and use the username and password stated to log in."+"<br><br>"+"Username: "+user.getUsername()+"<br><br>";			  
		  msgText = msgText + " Password: "+ sb.toString() +"<br><br>"+"Go to: <a href='http://skyrelo.com'>http://skyrelo.com</a>"+"<br><br>"+"You will be prompted upon initial log in to change the password to one of your choosing. We track usage, uploads and edits by User ID so please do not share your ID with anyone including staff at your office."+"<br><br>";
		  msgText = msgText + " In the account portal, you will be able to initiate removals and relocations and track their progress. You will also be able to view / upload invoices and other document related to the move in the File Cabinet associated with the shipment."+"<br><br>";
		  msgText = msgText + " If you require assistance or if you have any questions or have problems logging in please contact us by e-mail at <a href='mailto:agents@redskymobility.com'>agents@redskymobility.com</a>. Once you login to RedSky, the Guide section on the top of the page contains a user guide to help you get started."+"<br><br>";
		  msgText = msgText + " Note - RedSky is fully tested and supported on the latest versions Firefox only. Although RedSky will work on IE, Safari and Chrome, we do not guarantee consistent behavior on browsers other than Firefox. RedSky supports browsers set up to display websites in English only; it may not work consistently on browsers running languages other than English.";
		  msgText= msgText + "<br><br><br><br>"+"With best regards," +"<br>"+ " "+"<br>"+"RedSky Team";
		  msgText= msgText + "</body></html>";
		  try {
			String from =fromEmail;
			
			String tempRecipient="";
			String tempRecipientArr[]=toAddress.split(",");
			for(String str1:tempRecipientArr){
				if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
					tempRecipient += str1;
				}
			}
			toAddress=tempRecipient;
			
			emailSetupManager.globalEmailSetupProcess(from, toAddress, "", "", "", msgText, subject, sessionCorpID,"",user.getUsername(),"");
			mailStatus = "sent";
			String algorithm = (String)	getConfiguration().get(Constants.ENC_ALGORITHM);
			if (algorithm == null) { 
				  log.debug("assuming testcase, setting algorithm to 'SHA'");
				  algorithm = "SHA"; 
			}
			String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
	    	confirmPasswordNew=passwordNew;
	    	Date compareWithDate=null;
			if(company.getPartnerPasswordExpiryDuration()!=null){
				compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));	
			}else{
				compareWithDate=DateUtils.addDays(getCurrentDate(), (60));
			} 
	    	customerFileManager.resetPassword(passwordNew, confirmPasswordNew, user.getUsername(),compareWithDate);
		  }catch (Exception mex){
			mex.printStackTrace();
			mailStatus = "notSent";
			mailFailure=mex.toString(); 
		}		  
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	
	 private java.util.Date getCurrentDate() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String dateStr = dfm.format(new Date());
			Date currentDate = null;
			try {
				currentDate = dfm.parse(dateStr);
				
			} catch (java.text.ParseException e) {
				
				e.printStackTrace();
			}			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return currentDate;
			
		}
	
	 @SkipValidation 
	 public String basedAtNameCheck(){  
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
				if(basedAtCode!=null){
					basedAtNameList=partnerManager.basedAtName(basedAtCode,sessionCorpID);
		        }else{
		        	String message="The partner Code is not exist";
		        	errorMessage(getText(message));
		        }				
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS; 
			}
			
	@SkipValidation
	public String disableFromPartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{

		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		userCheck = userCheck.trim();
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			partnerManager.disableUser(id, sessionCorpID, "Disable");
		}
		String prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	@SkipValidation
	public String enableFromPartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		userCheck = userCheck.trim();
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			partnerManager.disableUser(id, sessionCorpID, "Enable");
		}
		String prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	@SkipValidation
	public String deleteUserFromPartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		userCheck = userCheck.trim();
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			user=userManager.getUser(id.toString());
			userManager.removeUser(user.getId().toString());
		}
		String prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	@SkipValidation
	public String assignAdminRoleFromPartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		userCheck = userCheck.trim();
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			user=userManager.getUser(id.toString());
			user.addRole(roleManager.getRole("ROLE_AGENT_ADMIN"));
		}
		String prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
		}catch(Exception ex)
		{
			ex.printStackTrace();	
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	@SkipValidation
	public String sendEmailToAgents() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
		//partner = partnerManager.get(Long.parseLong(partnerId));
		partnerPublic = partnerPublicManager.get(Long.parseLong(partnerId));
		user=userManager.getUser(userId);
		try {
			String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
		     StringBuffer sb=new StringBuffer();
		     Random r = new Random();
		     int te=0;
		     for(int i=1;i<=8;i++){
		         te=r.nextInt(62);
		         sb.append(str.charAt(te));
		     }
			   	getConfiguration().get(Constants.ENCRYPT_PASSWORD);
			   	String algorithm = (String)
			   	getConfiguration().get(Constants.ENC_ALGORITHM);
			   	if (algorithm == null) { // should only happen for test case
						  log.debug("assuming testcase, setting algorithm to 'SHA'");
						  algorithm = "SHA"; 
					}
			   	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
		
			userManager.updatePassword(passwordNew, user.getId());
			List fromEmailAddress=userManager.getFromEmailAddress(user.getCorpID());
			String from = fromEmailAddress.get(0).toString();
			String subject="";
			String mailTo=user.getEmail();
			String tempRecipient="";
			String tempRecipientArr[]=mailTo.split(",");
			for(String str1:tempRecipientArr){
				if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
					tempRecipient += str1;
				}
			}
			mailTo=tempRecipient;
			subject="Partner Login Information";
		
			String msgText1 = "";
			msgText1 = msgText1 +"\n\nWelcome to RedSky. http://sscwpartner.skyrelo.com";
			
			msgText1 = msgText1 + "\n\nYour user log-in : " + user.getUsername() + "\nYour temporary password : " + sb.toString() + "\n";
			
			msgText1 = msgText1 +  "\nUpon your initial log-in, you will be prompted to change the password to your own private password. \n(N.B. RedSky data and information are confidential to us and our registered partner users like you. \nPlease help us ensure this confidentiality is protected by not sharing your password with anyone.) " +
			"\n\nWe invite you to update your agent profile and input your rates to allow us to present your service capabilities to prospective clients moving from or to your location.";
			
			msgText1 = msgText1 +"\n\nFor current shipments we are working on as partners, we request you to input the information and upload the documents relevant to your role (origin or destination services) in the shipping process."; 
			
			msgText1 = msgText1 + "\n\nFor a step-by-step guide on RedSky partner agent access and usage, please visit the following web address:  http://www.secor-group.com/redsky/agentportal";
			
			msgText1 = msgText1 + " \n\nWe would appreciate hearing any form of feedback from your end. \nPlease e-mail us at adasilva@sscw.com.\nThank you for choosing to be a part of RedSky.\n\nBest regards,\n\n RedSky Team";
			
			emailSetupManager.globalEmailSetupProcess(from, mailTo, "", "", "", msgText1, subject, sessionCorpID,"",user.getUsername(),"");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		String prefixDataSet = "DATA_SECURITY_SET_AGENT_" + partnerPublic.getPartnerCode();
		partnerUsersList = dataSecuritySetManager.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
		}catch(Exception ex)
		{
			ex.printStackTrace();	
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	@SkipValidation
	public String requestedPartnerAccessList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner = partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		/* Code added by kunal for ticket number: 6176 */
		List<Company> listCompany = companyManager.findByCorpID(sessionCorpID);
		company = listCompany.get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		/* code completed for 6176 */
		partnerUsersList=dataSecuritySetManager.getRequestedPartner(partnerPublic.getPartnerCode(),sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private List userRoleSet;
	@SkipValidation
	public String findUserRole(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		userRoleSet=dataSecuritySetManager.getUserRoleSet(userName, sessionCorpID);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String validateUser() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		List checkUserName = dataSecuritySetManager.validateUserName(userName, sessionCorpID);
		if (checkUserName.isEmpty()) {
			totalRevenue = "UserName available";
		} else {
			totalRevenue = "UserName already exist, please select a new userName";
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String getChildAgents() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerPublic = partnerPublicManager.get(id);
		partnerChildAgentList = partnerManager.getPartnerChildAgentsList(partnerPublic.getPartnerCode(), sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	@SkipValidation
	public String addressDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partner = partnerManager.get(partner.getId());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String editDetail() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partner = partnerManager.get(partner.getId());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	// Method to search partner
	@SkipValidation
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		if (findFor == null) {
			findFor = "";
		}

		boolean lastName = (partner.getLastName() == null);
		boolean partnerCode = (partner.getPartnerCode() == null);
		boolean billingCountryCode = (partner.getBillingCountryCode() == null);
		boolean billingStateCode = (partner.getBillingState() == null);
		boolean billingCountry = (partner.getBillingCountry() == null);
		if (!lastName || !partnerCode || !billingCountryCode || !billingStateCode || !billingCountry) {
			partners = new HashSet(partnerManager.searchForPartner(partner.getLastName(), partner.getPartnerCode(), partner.getBillingCountryCode(), partner.getBillingCountry(), partner.getBillingState(), findFor));
		}

		partners = new HashSet(partnerManager.searchForPartner(partner.getLastName(), partner.getPartnerCode(), partner.getBillingCountryCode(), partner.getBillingCountry(), partner.getBillingState(), findFor));	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	@SkipValidation
	public String matchCarrierCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		if (perId != null) {
			carrierCodeList = partnerManager.findByOwnerOps(perId);
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
 private String partnerRecord;
	@SkipValidation
    public String checkValidNationalCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
  	  if(vlCode !=null)
  	  {
  		partnerCodeRecord=partnerManager.checkValidNationalCode(vlCode,sessionCorpID);
  		if(partnerCodeRecord!=null && !partnerCodeRecord.isEmpty() && (partnerCodeRecord.get(0)!=null)){
  			String[] str1 = partnerCodeRecord.get(0).toString().split("~");  			
  			partnerRecord = str1[0]+","+str1[1];
  		}
  	  }  	
  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
  	  return SUCCESS;
    }
	private String geoAddress;
	private String lastName;
	private String city;
	private String geoState;
	private String zipCode;
	private String geoCountry;
	private String rangeWithin;
	private String geoLongitude;
	private String geoLatitude;
	private Boolean isUts;
	private Boolean isFidi;
	private Boolean isOmniNo;
	private Boolean isAmsaNo;
	private Boolean isWercNo;
	private Boolean isIamNo;
	private String partnerOption;
	public String getGeoLatitude() {
		return geoLatitude;
	}

	public void setGeoLatitude(String geoLatitude) {
		this.geoLatitude = geoLatitude;
	}

	public String getGeoLongitude() {
		return geoLongitude;
	}

	public void setGeoLongitude(String geoLongitude) {
		this.geoLongitude = geoLongitude;
	}

	public String getRangeWithin() {
		return rangeWithin;
	}

	public void setRangeWithin(String rangeWithin) {
		this.rangeWithin = rangeWithin;
	}
    @SkipValidation
	public String geoPortsPoint(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
    	ocountry = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");    
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
    private List partnersMapList;
	public InputStream getGeoPortsPointMarkup()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ByteArrayInputStream output = null;
		String minLatitude="";
		String maxLatitude="";
		String minLogitude="";
		String maxLogitude="";
		
			if(partner.getIsAgent() == null){
				partner.setIsAgent(true);
			}
			if(partner.getIsCarrier() == null){
				partner.setIsCarrier(false);
			}
			if(partner.getIsVendor() == null){
				partner.setIsVendor(false);
			}
			if(partner.getIsAccount() == null){
				partner.setIsAccount(false);
			}
			if(geoLatitude==null)
			{
				geoLatitude="";
			}
			if(isUts==null)	{
				isUts=false;
			}
			if(isFidi==null){
				isFidi=false;
			}
			if(isOmniNo==null){
				isOmniNo=false;
			}
			if(isAmsaNo==null){
				isAmsaNo=false;
			}
			if(isWercNo==null){
				isWercNo=false;
			}
			if(isIamNo==null){
				isIamNo=false;
			}

			if(!geoLatitude.equals(""))
			{
				if(!rangeWithin.equals(""))
				{
					Double val1 =Double.parseDouble(geoLatitude)-((Double.parseDouble(rangeWithin)/60));
					Double val2 =Double.parseDouble(geoLatitude)+((Double.parseDouble(rangeWithin)/60));
					if(val1>val2)
					{
						minLatitude= val2.toString();				
						maxLatitude= val1.toString();				
					}
					else
					{
						minLatitude= val1.toString();				
						maxLatitude= val2.toString();				
					}
			    }
			}

			if(!geoLongitude.equals(""))
			{
				if(!rangeWithin.equals(""))
				{
					
					Double val1 =Double.parseDouble(geoLongitude)-((Double.parseDouble(rangeWithin)/60));
					Double val2 =Double.parseDouble(geoLongitude)+((Double.parseDouble(rangeWithin)/60));
					if(val1>val2)
					{
						minLogitude= val2.toString();				
						maxLogitude= val1.toString();				
					}
					else
					{
						minLogitude= val1.toString();				
						maxLogitude= val2.toString();				
					}
			    }
			}
			
			if(geoLongitude==null)
			{
				geoLongitude="";
			}
			if(rangeWithin.trim().toString().equals(""))
			{
				rangeWithin="100";	
			}
			partnersMapList = partnerManager.getPartnerGeoMapDataList(sessionCorpID,partner.getIsAgent(), partner.getIsVendor(), partner.getIsCarrier(),minLatitude,maxLatitude,minLogitude,maxLogitude,isUts,isFidi,isOmniNo,isAmsaNo,isWercNo,isIamNo,partner.getIsAccount(),geoCountry, lastName);
		if(partnersMapList!=null)
		  {
	  		StringBuilder ports = new StringBuilder();
			int count = 0;
			Iterator it=partnersMapList.iterator();
			TreeMap <Double, String>tMap = new TreeMap<Double, String>();
			while(it.hasNext()){		 
				Object []obj=(Object[]) it.next(); 
				double distance = getDistance(Double.parseDouble(geoLatitude), Double.parseDouble(geoLongitude), ((BigDecimal) obj[7]).doubleValue(), ((BigDecimal) obj[8]).doubleValue());							
				tMap.put(distance, obj[0]+"~"+obj[13]+"~"+obj[12]+"~"+obj[11]+"~"+obj[10]+"~"+obj[5]+"~"+obj[4]+"~"+obj[3]+"~"+obj[2]+"~"+obj[9]+"~"+obj[6]+"~"+obj[1]+"~"+obj[7]+"~"+obj[8]+"~"+obj[14]);
			}
			Set set = tMap.entrySet();
			Iterator i = set.iterator();
			while(i.hasNext()) {
				count++;
			Map.Entry me = (Map.Entry)i.next();
	                    Object partnerRec[]=(Object[]) me.getValue().toString().split("~");
			if (count > 1)
				ports.append(",");
			else
				ports.append("");
			ports.append("{\"id\":" + partnerRec[0] + ", \"carrierStatus\": \"" + partnerRec[1] + "\",  \"accountStatus\": \"" + partnerRec[14] + "\", \"vendorStatus\": \"" + partnerRec[2] +  "\", \"agentStatus\": \"" + partnerRec[3] + "\", \"partnerRateGridStatus\": \"" + partnerRec[4] + "\", \"zip\": \"" + partnerRec[5] + "\", \"state\": \"" + partnerRec[6] + "\", \"city\": \"" + partnerRec[7] + "\", \"country\": \"" + partnerRec[8] + "\", \"partnerName\": \"" + partnerRec[9] + "\", \"address\": \"" + partnerRec[10] + "\", \"name\": \"" + partnerRec[11] + "\", \"latitude\": " + partnerRec[12] + ", \"longitude\": " + partnerRec[13] + "}");
			}
			String prependStr = "{ \"count\":" + count + ", \"ports\":[";
			String appendStr = "]}";
			String s=prependStr + ports.toString() + appendStr;
    		        output=new ByteArrayInputStream(s.getBytes());		  }		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return output;
	}
	private double getDistance(double originLatitude, double originLongitude,
			double destinationLatitude, double destinationLongitude) {
		double distance = distance(originLatitude,
				originLongitude, destinationLatitude, destinationLongitude, "M");
		return distance;
	}	
	private static double distance(double lat1, double lon1, double lat2,
			double lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}
		return (dist);
	}
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
	

	@SkipValidation
	public String listGeoMaps()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ocountry = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");		
		try {
			if (partner != null) {
				if(partner.getIsAgent() == null){
					partner.setIsAgent(true);
				}
				if(partner.getIsCarrier() == null){
					partner.setIsCarrier(false);
				}
				if(partner.getIsVendor() == null){
					partner.setIsVendor(false);
				}
				if(isUts==null)	{
					isUts=false;
				}
				if(isFidi==null){
					isFidi=false;
				}
				if(isOmniNo==null){
					isOmniNo=false;
				}
				if(isAmsaNo==null){
					isAmsaNo=false;
				}
				if(isWercNo==null){
					isWercNo=false;
				}
				if(isIamNo==null){
					isIamNo=false;
				}
				
				partners = new HashSet();
			} else {
				partner = new Partner();
				partner.setIsVendor(false);
				partner.setIsAgent(true);
				partner.setIsCarrier(false);
				rangeWithin="100";
				if(isUts==null)	{
					isUts=false;
				}
				if(isFidi==null){
					isFidi=false;
				}
				if(isOmniNo==null){
					isOmniNo=false;
				}
				if(isAmsaNo==null){
					isAmsaNo=false;
				}
				if(isWercNo==null){
					isWercNo=false;
				}
				if(isIamNo==null){
					isIamNo=false;
				}
				
				partners = new HashSet();
			}
		} catch (Exception e) {
			partner = new Partner();
			partner.setIsVendor(false);
			partner.setIsAgent(true);
			partner.setIsCarrier(false);
			rangeWithin="100";
			if(isUts==null)
			{
				isUts=false;
			}
			if(isFidi==null)
			{
				isFidi=false;
			}
			if(isOmniNo==null)
			{
				isOmniNo=false;
			}
			if(isAmsaNo==null)
			{
				isAmsaNo=false;
			}
			if(isWercNo==null)
			{
				isWercNo=false;
			}
			if(isIamNo==null)
			{
				isIamNo=false;
			}
			
			partners = new HashSet();
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String listByAccount() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		String thirdArgument = "";
		String firstArgument = "";
		String secondArgument = "";
		String account = "account";
		partners = new HashSet(partnerManager.findForPartner(firstArgument, secondArgument, thirdArgument, account));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	// Method to get partner list who are broker used in accounting
	@SkipValidation
	public String listByBroker() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByBroker());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;
	}
/*
	@SkipValidation
	public String listByBrokerActgCode() {
		partners = new HashSet(partnerManager.findByBrokerAccRef());
		return SUCCESS;
	}
*/
	// Method to get partner list who are carriers used in accounting
	@SkipValidation
	public String listByCarriers() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByCarriers(sessionCorpID));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;
	}

	// Method to get partner list who are carriers used in accounting and accountLineInterface is Yes
	/*@SkipValidation
	public String listByCarrierActgCode() {
		partners = new HashSet(partnerManager.findByCarriersAccRef());
		return SUCCESS;
	}*/

	// Method to get partner list who are agents used in accounting
	@SkipValidation
	public String listByAgent() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByAgent());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;

	}

	// Method to get partner list who are agents used in subcontractor
	@SkipValidation
	public String listByCarrierCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByOwner());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;

	}

	// Method to get partner list by there origin details used in rate requests
	@SkipValidation
	public String listByOrigin() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByOrigin(origin));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;
	}
	
	@SkipValidation
	public String listOfClassifiedAgent() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerClassifiedList = partnerManager.findByOriginAgent(origin);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	// Method to get partner list by there origin details used in rate requests and AccountInterFace is Yes
	@SkipValidation
	public String listByOriginActgCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByOriginAccRef(origin));	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String getownerLastNameByCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ownerName = partnerManager.findByOwnerOpLastNameByCode(partnerCode);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	// Method to get partner list by there destination details used in rate requests
	private String userType;
	@SkipValidation
	public String listByDestination() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByDestination(destination,userType));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;
	}

	// Method to get partner list by there destination details used in rate requests and AccountInterFace is Yes
	@SkipValidation
	public String listByDestActgCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByDestAccRef(destination));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	// Method to get partner list who are frieght forwarded by there origin details used in rate requests
	@SkipValidation
	public String listByOfreight() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByFreight(origin));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	// Function for getting the values for all comboboxes.
	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		logger.warn(" getComboList() AJAX Call : "+getRequest().getParameter("ajax")); 
		try{
		prefixList=refMasterManager.findByParameters(corpId, "PREFFIX");
		ratetype = refMasterManager.findByParameter(corpId, "RATETYPE");
		state = refMasterManager.findByParameter(corpId, "STATE");
		country = refMasterManager.findCountryForPartner(corpId, "COUNTRY");
		countryDesc = refMasterManager.findCountry(corpId, "COUNTRY");
		rank = refMasterManager.findByParameter(corpId, "RANK");
		house = refMasterManager.findByParameter(corpId, "HOUSE");
		billgrp = refMasterManager.findByParameter(corpId, "BILLGRP");
		isdriver = refMasterManager.findByParameter(corpId, "ISDRIVER");
		billinst = refMasterManager.findByParameter(corpId, "BILLINST");
		paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
		payopt = refMasterManager.findByParameter(corpId, "PAYOPT");
		schedule = refMasterManager.findByParameter(corpId, "SCHEDULE");
		psh = refMasterManager.findByParameter(corpId, "PSH");
		glcodes = refMasterManager.findByParameter(corpId, "GLCODES");
		sale = refMasterManager.findUser(corpId, "ROLE_SALE");
		coord = refMasterManager.findUser(corpId, "ROLE_COORD");
		all_user = refMasterManager.findUser(corpId, "ALL_USER");
		storageBillingGroup = refMasterManager.findByParameter(corpId, "BILLGRP");
		typeOfVendor = refMasterManager.findByParameter(corpId, "TYPEOFVENDOR");
		partnerStatus = refMasterManager.findByParameter(corpId, "PARTNER_STATUS");
		ocountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		currency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		lead = refMasterManager.findByParameter(corpId, "LEAD");
		industry = refMasterManager.findByParameter(corpId, "INDUSTRY");
		revenue = refMasterManager.findByParameter(corpId, "REVENUE");
		JobFunction=refMasterManager.findByParameters(corpId, "JOBFUNCTION");

		pricing = refMasterManager.findUser(corpId, "ROLE_PRICING");
		billing = refMasterManager.findUser(corpId, "ROLE_BILLING");
		payable = refMasterManager.findUser(corpId, "ROLE_PAYABLE");
		coordinator = refMasterManager.findUser(corpId, "ROLE_COORD");
		stageunits = new ArrayList();
		stageunits.add("Prospect");
		stageunits.add("Quote");
		stageunits.add("Panel");
		stageunits.add("Contract");
		stageunits.add("Major");
		creditTerms = refMasterManager.findByParameter(corpId, "CrTerms");
		companyDivis = customerFileManager.findCompanyDivision(sessionCorpID);
		facilitySize=refMasterManager.findByParameter(corpId, "FACILITYSIZE");
		vanlineAffiliations=refMasterManager.findByParameter(corpId, "VANLINEAFFILIATIONS");
		serviceLines=refMasterManager.findByParameter(corpId, "SERVICELINES");
		qualityCertifications=refMasterManager.findByParameter(corpId, "QUALITYCERTIFICATIONS");
		actionType = new LinkedHashMap<String, String>();
		actionType.put("PP", "Private Party");
		actionType.put("AC", "Account");
		actionType.put("AG", "Agent");
		actionType.put("VN", "Vendor");
		actionType.put("CR", "Carrier");
		actionType.put("OO", "Driver");
		actionType.put("RSKY", "redskypartner");
		
		actionTypeView = new LinkedHashMap<String, String>();
		actionTypeView.put("PP", "Private Party");
		//actionTypeView.put("RSKY", "redskypartner"); 9183 - Not Able to Search Partner
		actionTypeView.put("AC", "Account");
		actionTypeView.put("AG", "Agent");
		logger.warn(" getComboList() End AJAX Call : "+getRequest().getParameter("ajax"));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@SkipValidation
	public String partnerRatesList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partner = partnerManager.get(id);
		partnerRatess = new ArrayList(partner.getPartnerRatess());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String popupListPartnerVanLine() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		//getComboList(sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		 if(company!=null){
		      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
		  		cmmDmmFlag = true;
		  		}
		 }
		try {
			String firstName;
			String lastName;
			String aliasName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String vanLineCODE;
			if (partner != null) {
				if (partner.getFirstName() == null) {
					firstName = "";
				} else {
					firstName = partner.getFirstName();
				}
				if (partner.getLastName() == null) {
					lastName = "";
				} else {
					lastName = partner.getLastName();
				}
				if (partner.getAliasName() == null) {
					aliasName = "";
				} else {
					aliasName = partner.getAliasName();
				}
				if (partner.getPartnerCode() == null) {
					partnerCode = "";
				} else {
					partnerCode = partner.getPartnerCode();
				}
				if (countryCodeSearch == null) {
					billingCountryCode = "";
				} else {
					billingCountryCode = countryCodeSearch;
				}
				if (stateSearch == null) {
					billingStateCode = "";
				} else {
					billingStateCode = stateSearch;
				}
				if (countrySearch == null) {
					billingCountry = "";
				} else {
					billingCountry = countrySearch;
				}
				if (vanlineCode == null) {
					vanLineCODE = "";
				} else {
					vanLineCODE = vanlineCode;
				}
				
				if(partner.getIsAccount() == null){
					partner.setIsAccount(false);
				}
				
				if(partner.getIsAgent() == null){
					partner.setIsAgent(false);
				}
				
				if(partner.getIsCarrier() == null){
					partner.setIsCarrier(false);
				}
				
				if(partner.getIsOwnerOp() == null){
					partner.setIsOwnerOp(false);
				}
				
				if(partner.getIsPrivateParty() == null){
					partner.setIsPrivateParty(false);
				}
				
				if(partner.getIsVendor() == null){
					partner.setIsVendor(false);
				}
																																									
				partners = new HashSet(partnerManager.getPartnerVanLinePopupList("", sessionCorpID,firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, vanLineCODE, partner.getIsPrivateParty(), partner.getIsAccount(), partner.getIsAgent(), partner.getIsVendor(), partner.getIsCarrier(), partner.getIsOwnerOp(), cmmDmmFlag));
			} else {
				partner = new Partner();
				partner.setIsAccount(true);
				partner.setIsPrivateParty(true);
				partner.setIsVendor(true);
				partner.setIsAgent(true);
				partner.setIsCarrier(true);
				partner.setIsOwnerOp(true);
				partners = new HashSet(partnerManager.getPartnerVanLinePopupList("", sessionCorpID, "","", "", "", "", "", "", "",true,true,true,true,true,true,cmmDmmFlag));
			}
		} catch (Exception e) {
			partner = new Partner();
			partner.setIsAccount(true);
			partner.setIsPrivateParty(true);
			partner.setIsVendor(true);
			partner.setIsAgent(true);
			partner.setIsCarrier(true);
			partner.setIsOwnerOp(true);
			partners = new HashSet(partnerManager.getPartnerVanLinePopupList("", sessionCorpID, "", "", "","", "", "", "", "",false,false,false,false,false,false,cmmDmmFlag));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String searchMiscellaneousListPopUpForCarrier(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
	String lastName = "";
	String partnerCode = "";
	if (!(partner == null)) {
	if ((partner.getLastName() == null)) {
		lastName = "";
	} else {
		lastName = partner.getLastName();
	}
	
	if ((partner.getPartnerCode() == null)) {
		partnerCode = "";
	} else {
		partnerCode = partner.getPartnerCode();
	}
	partners = new HashSet(partnerManager.getCarrierListForHeavy(sessionCorpID,lastName, partnerCode));
	}else{
		partners = new HashSet(partnerManager.getCarrierListForHeavy(sessionCorpID,"", ""));
	     }	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	// Function for getting the list of partners in popup ForContractAgentForm
	@SkipValidation
	public String popupListForContractAgentForm(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String lastName = "";
		String partnerCode = "";
		String billingCountryCode = "";
		String billingCountry = "";
		String billingStateCode = "";
		String extReference="";
		String aliasName="";
		
		if (!(partner == null)) { 
			if ((partner.getLastName() == null)) {
				lastName = "";
			} else {
				lastName = partner.getLastName();
			}
			
			if ((partner.getPartnerCode() == null)) {
				partnerCode = "";
			} else {
				partnerCode = partner.getPartnerCode();
			}
			
			if (partner.getBillingCountryCode() != null) {
				billingCountryCode = partner.getBillingCountryCode();
			}else if(partner.getTerminalCountryCode() != null){
				billingCountryCode = partner.getTerminalCountryCode();
			}else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry();
			}else{
				billingCountry = "";
			}
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference();
			}
			if ((partner.getAliasName()==null)) {
				aliasName = "";
			} else {
				aliasName = partner.getAliasName();
			}
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingCountry,billingStateCode));
			partners = new HashSet(partnerManager.getPartnerPopupListForContractAgentForm(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor));
			} else {
				partners = new HashSet(partnerManager.getPartnerPopupListForContractAgentForm(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor));
			}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	} 
	
	
	@SkipValidation
	public String crewPopupList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
			String lastName = "";
			String partnerCode = "";
			String billingCountryCode = "";
			String billingCountry = "";
			String billingStateCode = "";
			String extReference="";
			String aliasName="";
			
			if (!(partner == null)) { 
				if ((partner.getLastName() == null)) {
					lastName = "";
				} else {
					lastName = partner.getLastName();
				}
				
				if ((partner.getPartnerCode() == null)) {
					partnerCode = "";
				} else {
					partnerCode = partner.getPartnerCode();
				}
				
				if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
					billingCountryCode = partner.getBillingCountryCode();
				}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
					billingCountryCode = partner.getTerminalCountryCode();
				}else if (origin != null && (!"".equals(origin))) {
					billingCountryCode=origin;
				}else if (destination != null && (!"".equals(destination))) {
					billingCountryCode=destination;
				}
				else {
					billingCountryCode = "";
				}
				
				if (partner.getBillingState() != null) {
					billingStateCode = partner.getBillingState();
				}else if(partner.getTerminalState() != null){
					billingStateCode = partner.getTerminalState();
				}else{
					billingStateCode = "";
				}
				
				if (partner.getBillingCountry() != null) {
					billingCountry = partner.getBillingCountry();
				}else if(partner.getTerminalCountry() != null){
					billingCountry = partner.getTerminalCountry();
				}else{
					billingCountry = "";
				}
				if ((partner.getExtReference() == null)) {
					extReference = "";
				} else {
					extReference = partner.getExtReference();
				}
				if ((partner.getAliasName()==null)) {
					aliasName = "";
				} else {
					aliasName = partner.getAliasName();
				}
				partners = new HashSet(partnerManager.getCrewPartnerPopupList(sessionCorpID, lastName,aliasName, partnerCode, billingCountry, billingStateCode,extReference));

			} else {
				partners = new HashSet(partnerManager.getCrewPartnerPopupList(sessionCorpID, "", "", "","","",""));
			}
			}catch(Exception e){
				
				e.printStackTrace();
			}			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	// Function for getting the list of partners in popup
	private String RedSkyAgentPartner;
	@SkipValidation
	public String popupList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
		String lastName;
		String partnerCode;
		String billingCountryCode;
		String billingCountry;
		String billingStateCode;
		String extReference;
		String aliasName;
		String vanlineCode="";
		if(vanlineCodeSearch!=null){
			vanlineCode=vanlineCodeSearch.trim();
		}
		if(partner !=null){
		lastName = partner.getLastName() == null ? "" : partner.getLastName().trim();
		partnerCode = partner.getPartnerCode() == null ? "" : partner.getPartnerCode().trim();
		aliasName=partner.getAliasName()==null ? "" :partner.getAliasName().trim();
			if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}else if (origin != null && (!"".equals(origin))) {
				billingCountryCode=origin;
			}else if (destination != null && (!"".equals(destination))) {
				billingCountryCode=destination;
			}
			else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else{
				billingCountry = "";
			}
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
			/*if ((partner.getAliasName()==null)) {
				aliasName = "";
			} else {
				aliasName = partner.getAliasName();
			}*/
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingCountry,billingStateCode));
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid);
				customerFile = serviceOrder.getCustomerFile();
				partners = new HashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,customerFile.getId(),extReference));	
			}else{
			if (flag != null && flag.trim().length() > 0) {
				partners = new HashSet(partnerManager.getPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag));
			} else {
				partners = new HashSet(partnerManager.getPartnerPopupList(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag));
			}
		    }
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());
		} else {
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid); 
				customerFile = serviceOrder.getCustomerFile();
				partners = new HashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, "", "","", "", "", "",customerFile.getId(),""));	
			}else{
			if (flag != null && flag.trim().length() > 0) {
				partners = new HashSet(partnerManager.getPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, "","", "", "", "", "","",vanlineCode,cmmDmmFlag));
			} else {
				if("YES".equals(search)){
					
				}
				partners = new HashSet(partnerManager.getPartnerPopupList(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode,cmmDmmFlag));
			}
			}
		}
		}catch(Exception e){
			
			e.printStackTrace();
		}
		if("RSKY".equals(partnerType)){
			RedSkyAgentPartner="RSKY";
		}	
		//}
		//getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private List partnerClassifiedList = new ArrayList();
	@SkipValidation
	public String searchClassifiedPartner() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
		String lastName;
		String partnerCode;
		String billingCountryCode;
		String billingCountry;
		String billingStateCode;
		String extReference;
		String aliasName;
		String vanlineCode="";
		if(vanlineCodeSearch!=null){
			vanlineCode=vanlineCodeSearch.trim();
		}
		if(partner !=null){
		lastName = partner.getLastName() == null ? "" : partner.getLastName().trim();
		partnerCode = partner.getPartnerCode() == null ? "" : partner.getPartnerCode().trim();
		aliasName=partner.getAliasName()==null ? "" :partner.getAliasName().trim();
			if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}else if (origin != null && (!"".equals(origin))) {
				billingCountryCode=origin;
			}else if (destination != null && (!"".equals(destination))) {
				billingCountryCode=destination;
			}
			else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else{
				billingCountry = "";
			}
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
			/*if ((partner.getAliasName()==null)) {
				aliasName = "";
			} else {
				aliasName = partner.getAliasName();
			}*/
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingCountry,billingStateCode));
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid);
				customerFile = serviceOrder.getCustomerFile();
				partnerClassifiedList = partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,customerFile.getId(),extReference);	
			}else{
				if (flag != null && flag.trim().length() > 0) {
					partnerClassifiedList = partnerManager.getPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag);
				} else {
					partnerClassifiedList = partnerManager.getPartnerPopupList(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag);
				}
		    }
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());
		} else {
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid); 
				customerFile = serviceOrder.getCustomerFile();
				partnerClassifiedList = partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, "", "","", "", "", "",customerFile.getId(),"");	
			}else{
				if (flag != null && flag.trim().length() > 0) {
					partnerClassifiedList = partnerManager.getPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, "","", "", "", "", "","",vanlineCode,cmmDmmFlag);
				} else {
					partnerClassifiedList = partnerManager.getPartnerPopupList(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode,cmmDmmFlag);
				}
			}
		}
		}catch(Exception e){
			
			e.printStackTrace();
		}
		if("RSKY".equals(partnerType)){
			RedSkyAgentPartner="RSKY";
		}	
		//}
		//getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String popupAgentList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
		String lastName;
		String partnerCode;
		String billingCountryCode;
		String billingCountry;
		String billingStateCode;
		String extReference;
		String aliasName;
		String agentGroup;
		String vanlineCode="";
		if(vanlineCodeSearch!=null){
			vanlineCode=vanlineCodeSearch.trim();
		}
		if(partner !=null){
		lastName = partner.getLastName() == null ? "" : partner.getLastName().trim();
		partnerCode = partner.getPartnerCode() == null ? "" : partner.getPartnerCode().trim();
		aliasName=partner.getAliasName()==null ? "" :partner.getAliasName().trim();
		agentGroup=partner.getAgentGroup()==null ? "" :partner.getAgentGroup().trim();
			if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}else if (origin != null && (!"".equals(origin))) {
				billingCountryCode=origin;
			}else if (destination != null && (!"".equals(destination))) {
				billingCountryCode=destination;
			}
			else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else{
				billingCountry = "";
			}
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
			if (flag != null && flag.trim().length() > 0) {
				partners = new HashSet(partnerManager.getAgentPopupListCompDiv(partnerType, sessionCorpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag, agentGroup));
			} else {
				partners = new HashSet(partnerManager.getAgentPopupList(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag, agentGroup));
			}	    
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());
		   } else {
			if (flag != null && flag.trim().length() > 0) {
				partners = new HashSet(partnerManager.getAgentPopupListCompDiv(partnerType, sessionCorpID, compDiv, "","", "", "", "", "","",vanlineCode,cmmDmmFlag,""));
			} else {
				if("YES".equals(search)){
					
				}
				partners = new HashSet(partnerManager.getAgentPopupList(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode,cmmDmmFlag,""));
			}
		}
		}catch(Exception e){
			
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	
	
	@SkipValidation
	public String popupListWithBookingAgentSet() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
		String lastName;
		String partnerCode;
		String billingCountryCode;
		String billingCountry;
		String billingStateCode;
		String extReference;
		String aliasName;
		String vanlineCode="";
		if(vanlineCodeSearch!=null){
			vanlineCode=vanlineCodeSearch.trim();
		}
		if(partner !=null){
		lastName = partner.getLastName() == null ? "" : partner.getLastName().trim();
		partnerCode = partner.getPartnerCode() == null ? "" : partner.getPartnerCode().trim();
		aliasName=partner.getAliasName()==null ? "" :partner.getAliasName().trim();
			if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}else if (origin != null && (!"".equals(origin))) {
				billingCountryCode=origin;
			}else if (destination != null && (!"".equals(destination))) {
				billingCountryCode=destination;
			}
			else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else{
				billingCountry = "";
			}
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
			partners = new HashSet(partnerManager.getPartnerPopupListWithBookingAgentSet(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag));
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());
		} else {
		  partners = new HashSet(partnerManager.getPartnerPopupListWithBookingAgentSet(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode,cmmDmmFlag));
			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		if("RSKY".equals(partnerType)){
			RedSkyAgentPartner="RSKY";
		}	
		//}
		//getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String popupQuotationList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
			String lastName = "";
			String partnerCode = "";
			String billingCountryCode = "";
			String billingCountry = "";
			String billingStateCode = "";
			String extReference="";
			String aliasName="";
			String vanlineCode="";
			if(vanlineCodeSearch!=null){
				vanlineCode=vanlineCodeSearch.trim();
			}			
			if (!(partner == null)) { 
				if ((partner.getLastName() == null)) {
					lastName = "";
				} else {
					lastName = partner.getLastName();
				}
				
				if ((partner.getPartnerCode() == null)) {
					partnerCode = "";
				} else {
					partnerCode = partner.getPartnerCode();
				}
				
				if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
					billingCountryCode = partner.getBillingCountryCode();
				}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
					billingCountryCode = partner.getTerminalCountryCode();
				}else if (origin != null && (!"".equals(origin))) {
					billingCountryCode=origin;
				}else if (destination != null && (!"".equals(destination))) {
					billingCountryCode=destination;
				}
				else {
					billingCountryCode = "";
				}
				
				if (partner.getBillingState() != null) {
					billingStateCode = partner.getBillingState();
				}else if(partner.getTerminalState() != null){
					billingStateCode = partner.getTerminalState();
				}else{
					billingStateCode = "";
				}
				
				if (partner.getBillingCountry() != null) {
					billingCountry = partner.getBillingCountry();
				}else if(partner.getTerminalCountry() != null){
					billingCountry = partner.getTerminalCountry();
				}else{
					billingCountry = "";
				}
				// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
				if ((partner.getExtReference() == null)) {
					extReference = "";
				} else {
					extReference = partner.getExtReference();
				}
				if ((partner.getAliasName()==null)) {
					aliasName = "";
				} else {
					aliasName = partner.getAliasName();
				}
				// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingCountry,billingStateCode));
				if (partnerType.equals("DF")) {
					serviceOrder = serviceOrderManager.get(sid);
					customerFile = serviceOrder.getCustomerFile();
					partners = new HashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,customerFile.getId(),extReference));	
				}else{
				if (flag != null && flag.trim().length() > 0) {
					partners = new HashSet(partnerManager.getQuotationPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag));
				} else {
					partners = new HashSet(partnerManager.getQuotationPartnerPopupList(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag));
				}
			    }
			} else {
				if (partnerType.equals("DF")) {
					serviceOrder = serviceOrderManager.get(sid); 
					customerFile = serviceOrder.getCustomerFile();
					partners = new HashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, "", "","", "", "", "",customerFile.getId(),""));	
				}else{
				if (flag != null && flag.trim().length() > 0) {
					partners = new HashSet(partnerManager.getQuotationPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, "","", "", "", "", "","",vanlineCode,cmmDmmFlag));
				} else {
					if("YES".equals(search)){
						
					}
					partners = new HashSet(partnerManager.getQuotationPartnerPopupList(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode,cmmDmmFlag));
				}
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			if("RSKY".equals(partnerType)){
				RedSkyAgentPartner="RSKY";
			}			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	@SkipValidation
	public String popupAllDriverList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String lastName = "";
		String partnerCode = "";
		String billingCountryCode = "";
		String billingCountry = "";
		String billingStateCode = "";
		String extReference="";
		String aliasName="";
		
		if (!(partner == null)) { 
			if ((partner.getLastName() == null)) {
				lastName = "";
			} else {
				lastName = partner.getLastName();
			}
			
			if ((partner.getPartnerCode() == null)) {
				partnerCode = "";
			} else {
				partnerCode = partner.getPartnerCode();
			}
			
			if (partner.getBillingCountryCode() != null) {
				billingCountryCode = partner.getBillingCountryCode();
			}else if(partner.getTerminalCountryCode() != null){
				billingCountryCode = partner.getTerminalCountryCode();
			}else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry();
			}else{
				billingCountry = "";
			}
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference();
			}
			if ((partner.getAliasName()==null)) {
				aliasName = "";
			} else {
				aliasName = partner.getAliasName();
			}
			partners = new HashSet(partnerManager.getPartnerPopupAllList(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference));
			}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String networkGroupPopupList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String lastName = "";
		String partnerCode = "";
		String billingCountryCode = "";
		String billingCountry = "";
		String billingStateCode = "";
		String extReference="";
		String aliasName="";
		String vanlineCode="";
		if(vanlineCodeSearch!=null){
			vanlineCode=vanlineCodeSearch.trim();
		}
		if (!(partner == null)) { 
			if ((partner.getLastName() == null)) {
				lastName = "";
			} else {
				lastName = partner.getLastName().trim();
			}
			
			if ((partner.getPartnerCode() == null)) {
				partnerCode = "";
			} else {
				partnerCode = partner.getPartnerCode().trim();
			}
			
			if (partner.getBillingCountryCode() != null) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else{
				billingCountry = "";
			} 
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
			if ((partner.getAliasName()==null)) {
				aliasName = "";
			} else {
				aliasName = partner.getAliasName().trim();
			} 
				
			partners = new HashSet(partnerManager.getPartnerNetworkGroupPopupList(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode));
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());	 
		} else {
			
				partners = new HashSet(partnerManager.getPartnerNetworkGroupPopupList(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode));
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"); 
		return SUCCESS;
		
	}
	
	@SkipValidation
	public String findVendorCode  (){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		serviceOrder = serviceOrderManager.getForOtherCorpid(sid); 
		customerFile = serviceOrder.getCustomerFile();
		if (partnerType.equals("DF")) {
		partners = new HashSet(partnerManager.findVendorCode(sid,sessionCorpID,serviceOrder.getJob(),customerFile.getId())); 
		} else {
		partner = new Partner();
		partner.setIsAccount(true);
		partner.setIsPrivateParty(true);
		partner.setIsVendor(true);
		partner.setIsAgent(true);
		partner.setIsCarrier(true);
		partner.setIsOwnerOp(true);
		partners = new HashSet(partnerManager.getPartnerPopupListAccount("", sessionCorpID, "", "","","", "", "", "", "",false,false,false,false,false,false,"",false));
		//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;   
	}
	
	@SkipValidation
	public String findVendorCodePartnerPayableProcessing  (){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partner = new Partner();
		partner.setIsAccount(true);
		partner.setIsPrivateParty(true);
		partner.setIsVendor(true);
		partner.setIsAgent(true);
		partner.setIsCarrier(true);
		partner.setIsOwnerOp(true);
		partners = new HashSet(partnerManager.getPartnerPopupListAccount("", sessionCorpID, "", "","", "", "", "", "", "",false,false,false,false,false,false,"",false));
		//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;   
	}
	@SkipValidation 
	public String searchPartnerAccount(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
		String firstName;
		String lastName = "";
		String partnerCode = "";
		String billingCountryCode = "";
		String billingCountry = "";
		String billingStateCode = ""; 
		String status;
		String extReference="";
		String aliasName="";
		if (partner != null) {
			if (partner.getFirstName() == null) {
				firstName = "";
			} else {
				firstName = partner.getFirstName().trim();
			}
			if (partner.getLastName() == null) {
				lastName = "";
			} else {
				lastName = partner.getLastName().trim();
			}
			if (partner.getAliasName() == null) {
				aliasName = "";
			} else {
				aliasName = partner.getAliasName().trim();
			}
			if (partner.getPartnerCode() == null) {
				partnerCode = "";
			} else {
				partnerCode = partner.getPartnerCode().trim();
			}
			if (partner.getBillingCountryCode() != null) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}else if(countryCodeSearch == null){
				billingCountryCode = "";
			} else {
				billingCountryCode = countryCodeSearch.trim();
			} 
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else if (stateSearch == null) {
				billingStateCode = "";
			} else {
				billingStateCode = stateSearch.trim();
			}
			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else if (countrySearch == null) {
				billingCountry = "";
			} else {
				billingCountry = countrySearch.trim();
			}
			
			if (partner.getStatus() == null) {
				status = "";
			} else {
				status = partner.getStatus().trim();
			}
			// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid); 
				customerFile = serviceOrder.getCustomerFile();
				partners = new HashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,customerFile.getId(),extReference));	
			}else{
				partners =  new HashSet(partnerManager.getPartnerPopupListAccount(partnerType, sessionCorpID,firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, partner.getIsPrivateParty(), partner.getIsAccount(), partner.getIsAgent(), partner.getIsVendor(), partner.getIsCarrier(), partner.getIsOwnerOp(),extReference,cmmDmmFlag));
			}
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("partnerCode", partner.getFirstName());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());	
		} else {
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid); 
				customerFile = serviceOrder.getCustomerFile();
				partners = new HashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, "", "","","", "", "",customerFile.getId(),extReference));	
			}else{ 
				partner = new Partner();
				partner.setIsAccount(true);
				partner.setIsPrivateParty(true);
				partner.setIsVendor(true);
				partner.setIsAgent(true);
				partner.setIsCarrier(true);
				partner.setIsOwnerOp(true);
				partners = new HashSet(partnerManager.getPartnerPopupListAccount("", sessionCorpID, "", "","", "", "", "", "", "",true,true,true,true,true,true,extReference,cmmDmmFlag));	
			}
		}
		}catch (Exception e) {
			partner = new Partner();
			partner.setIsAccount(true);
			partner.setIsPrivateParty(true);
			partner.setIsVendor(true);
			partner.setIsAgent(true);
			partner.setIsCarrier(true);
			partner.setIsOwnerOp(true);
			partners = new HashSet(partnerManager.getPartnerPopupListAccount("", sessionCorpID, "", "","", "", "", "", "", "",false,false,false,false,false,false ," ",cmmDmmFlag));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	@SkipValidation 
	public String findAgentCompanyDivision(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
    	partners=new HashSet(partnerManager.findBookingAgentCode(companyDivision,sessionCorpID));    	
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
	
	@SkipValidation
	public String findAgentCompanyDivisionVanline() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		//getComboList(sessionCorpID);
		try {
			String firstName;
			String lastName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String vanLineCODE;
			String aliasName;
			
			if (partner != null) {
				if (partner.getFirstName() == null) {
					firstName = "";
				} else {
					firstName = partner.getFirstName();
				}
				if (partner.getLastName() == null) {
					lastName = "";
				} else {
					lastName = partner.getLastName();
				}
				if (partner.getPartnerCode() == null) {
					partnerCode = "";
				} else {
					partnerCode = partner.getPartnerCode();
				}
				if (countryCodeSearch == null) {
					billingCountryCode = "";
				} else {
					billingCountryCode = countryCodeSearch;
				}
				if (stateSearch == null) {
					billingStateCode = "";
				} else {
					billingStateCode = stateSearch;
				}
				if (countrySearch == null) {
					billingCountry = "";
				} else {
					billingCountry = countrySearch;
				}
				if (vanlineCode == null) {
					vanLineCODE = "";
				} else {
					vanLineCODE = vanlineCode;
				}
				if (partner.getAliasName() == null) {
					aliasName = "";
				} else {
					aliasName = partner.getAliasName();
				}
				partners = new HashSet(partnerManager.getAgentCompanyDivisionVanline("", sessionCorpID,firstName, lastName, partnerCode, billingCountryCode, billingCountry, billingStateCode, vanLineCODE,aliasName, companyDivision));
			} else {
				partners = new HashSet(partnerManager.getAgentCompanyDivisionVanline("", sessionCorpID, "", "", "", "", "", "", "","",companyDivision));
			}
		} catch (Exception e) {
			partners = new HashSet(partnerManager.getAgentCompanyDivisionVanline("", sessionCorpID, "", "", "", "", "", "", "","",companyDivision));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String searchBookingAgent(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String lastName = "";
		String partnerCode = "";
		String billingCountryCode = "";
		String billingCountry = "";
		String billingStateCode = "";
		if (!(partner == null)) {
			if ((partner.getLastName() == null)) {
				lastName = "";
			} else {
				lastName = partner.getLastName();
			}
			
			if ((partner.getPartnerCode() == null)) {
				partnerCode = "";
			} else {
				partnerCode = partner.getPartnerCode();
			}
			
			if (partner.getBillingCountryCode() != null) {
				billingCountryCode = partner.getBillingCountryCode();
			}else if(partner.getTerminalCountryCode() != null){
				billingCountryCode = partner.getTerminalCountryCode();
			}else {
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry();
			}else{
				billingCountry = "";
			}
			partners=new HashSet(partnerManager.searchBookingAgent(companyDivision,sessionCorpID, lastName, partnerCode, billingCountryCode, billingCountry, billingStateCode));
		}else{
			partners=new HashSet(partnerManager.searchBookingAgent(companyDivision,sessionCorpID, "","", "", "", ""));
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String popupListAdmin() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		try {
			String firstName;
			String lastName;
			String aliasName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String status;
			String vanlineCode;
			String typeOfVendor;
			if (partner != null) {
				if (partner.getFirstName() == null) {
					firstName = "";
				} else {
					firstName = partner.getFirstName();
				}
				if (partner.getLastName() == null) {
					lastName = "";
				} else {
					lastName = partner.getLastName();
				}
				if (partner.getAliasName()== null) {
					aliasName= "";
				} else {
					aliasName = partner.getAliasName();
				}
				if (partner.getPartnerCode() == null) {
					partnerCode = "";
				} else {
					partnerCode = partner.getPartnerCode();
				}
				if (countryCodeSearch == null) {
					billingCountryCode = "";
				} else {
					billingCountryCode = countryCodeSearch;
				}
				if (stateSearch == null) {
					billingStateCode = "";
				} else {
					billingStateCode = stateSearch;
				}
				if (countrySearch == null) {
					billingCountry = "";
				} else {
					billingCountry = countrySearch;
				}
				if (partner.getStatus() == null) {
					status = "";
				} else {
					status = partner.getStatus();
				}
				if (vanlineCodeSearch == null) {
					vanlineCode = "";
				} else {
					vanlineCode = vanlineCodeSearch.trim();
				}
				
				if(partner.getIsAccount() == null){
					partner.setIsAccount(false);
				}
				if(partner.getIsAgent() == null){
					partner.setIsAgent(false);
				}
				if(partner.getIsCarrier() == null){
					partner.setIsCarrier(false);
				}
				if(partner.getIsOwnerOp() == null){
					partner.setIsOwnerOp(false);
				}
				if(partner.getIsPrivateParty() == null){
					partner.setIsPrivateParty(false);
				}
				if(partner.getIsVendor() == null){
					partner.setIsVendor(false);
				}
				
				if (isIgnoreInactive == null) {
					isIgnoreInactive = false;
				}
				typeOfVendor=partner.getTypeOfVendor()==null?"":partner.getTypeOfVendor();																																					
				partners = new HashSet(partnerManager.getPartnerPopupListAdmin(partnerType, sessionCorpID,firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, partner.getIsPrivateParty(), partner.getIsAccount(), partner.getIsAgent(), partner.getIsVendor(), partner.getIsCarrier(), partner.getIsOwnerOp(), isIgnoreInactive, partner.getExtReference(),vanlineCode,typeOfVendor,partner.getFidiNumber(),partner.getOMNINumber(),partner.getAMSANumber(),partner.getWERCNumber(),partner.getIAMNumber(),partner.getUtsNumber(),partner.getEurovanNetwork(),partner.getPAIMA(),partner.getLACMA()));
			} else {
				partner = new Partner();
				partner.setIsAccount(true);
				partner.setIsPrivateParty(true);
				partner.setIsVendor(true);
				partner.setIsAgent(true);
				partner.setIsCarrier(true);
				partner.setIsOwnerOp(true);
				isIgnoreInactive = true;
				partners = new HashSet(partnerManager.getPartnerPopupListAdmin("", sessionCorpID, "", "", "","", "", "", "", "",true,true,true,true,true,true,true,"","","","","","","","","","","",""));
			}
		} catch (Exception e) {
			partner = new Partner();
			partner.setIsAccount(true);
			partner.setIsPrivateParty(true);
			partner.setIsVendor(true);
			partner.setIsAgent(true);
			partner.setIsCarrier(true);
			partner.setIsOwnerOp(true);
			isIgnoreInactive = true;
			partners = new HashSet(partnerManager.getPartnerPopupListAdmin("", sessionCorpID, "", "", "", "","","", "", "",false,false,false,false,false,false,false,"","","","","","","","","","","",""));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String checkMultiAuthorization() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		multiAuthorization = partnerManager.findMultiAuthorization(accPartnerCode);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public Map<String, String> getBstates() {
		return (bstates != null && !bstates.isEmpty()) ? bstates : customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
	}

	public Map<String, String> getTstates() {
		// return (tstates!=null && !tstates.isEmpty())?tstates:customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
		List terminalCountryCode = partnerManager.getCountryCode(partner.getTerminalCountry());
		try {
			if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
				return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);

			} else {
				return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return tstates;
	}

	public Map<String, String> getMstates() {
		List mailIngCountryCode = partnerManager.getCountryCode(partner.getMailingCountry());
		try {
			if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
				return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);

			} else {
				return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(partner.getMailingCountryCode(), sessionCorpID);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return mstates;
	}

	@SkipValidation
	public String agentDetails() {
		//getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partner = partnerManager.get(id);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String forwardingDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		partner = partnerManager.get(id);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String saveAgentDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		boolean isNew = (partner.getId() == null);
		partner.setCorpID(sessionCorpID);
		partner.setUpdatedOn(new Date());
		partnerManager.save(partner);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "Agent Info has been added successfully" : "Agent Info has been updated successfully";
			saveMessage(getText(key));
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String saveForwardingDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		boolean isNew = (partner.getId() == null);
		partner.setCorpID(sessionCorpID);
		partner.setUpdatedOn(new Date());
		partnerManager.save(partner);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "Forwarding Details has been added successfully" : "Forwarding Details has been updated successfully";
			saveMessage(getText(key));
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String saveAccountInfo() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//System.out.println("\n\n\n saveAccountInfo() in Action");
		//getComboList(sessionCorpID);
		boolean isNew = (partner.getId() == null);
		partner.setCorpID(sessionCorpID);
		// partner.setCreatedOn(new Date());
		partner.setUpdatedOn(new Date());
		//System.out.println("\n\n\n partner.getUpdatedBy()-->>" + partner.getUpdatedBy());
		//System.out.println("\n\n\n partner.getCreateddBy()-->>" + partner.getCreatedBy());
		/*
		 * if(partner.getId()!=null) { partner.setCreatedBy(""); partner.setCreatedBy(partner.getCreatedBy()); }
		 */

		partnerManager.save(partner);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "Agent Info has been added successfully" : "Agent Info has been updated successfully";
			saveMessage(getText(key));
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String accountInfoPage() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		partner = partnerManager.get(id);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String vendorNameForActgCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		if (partnerCode != null) {
			vendorCodeList = partnerManager.findPartnerForActgCode(partnerCode, sessionCorpID, companyDivision);
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String actgCodeFlag;
	public String getActgCodeFlag() {
		return actgCodeFlag;
	}

	public void setActgCodeFlag(String actgCodeFlag) {
		this.actgCodeFlag = actgCodeFlag;
	}

	@SkipValidation
	public String vendorNameForActgCodeLine() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		 if(company!=null){
		      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
		  		cmmDmmFlag = true;
		  		}
		 }
		if (partnerCode != null) {
			vendorCodeList = partnerManager.findPartnerForActgCodeLine(partnerCode, sessionCorpID, companyDivision,actgCodeFlag,cmmDmmFlag);
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String vendorNameForUniqueActgCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		 if(company!=null){
		      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
		  		cmmDmmFlag = true;
		  		}
		 }
		if (partnerCode != null) {
			vendorCodeList = partnerManager.vendorNameForUniqueActgCode(partnerCode, sessionCorpID, cmmDmmFlag);
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String s = save();
		validateFormNav = "OK";		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}

	@SkipValidation
	public String agentList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		//getComboList(sessionCorpID);
		List <Object> partnerList1 = new ArrayList<Object>();
		List pList = partnerManager.getAllAgent(sessionUserName);
		Iterator it = pList.iterator();
		while(it.hasNext()){		 
			String partnerCode = it.next().toString();
			List partnerList = partnerManager.findPartnerCode(partnerCode);
			if(!partnerList.isEmpty() && (partnerList.get(0)!=null)){
				try{
					partnerList1.add((Partner)partnerList.get(0));
				}catch(Exception ex){;
					ex.printStackTrace();
				}
			}
		}
		partnerAgentList = partnerList1;		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String agentListSearch(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//getComboList(sessionCorpID);
		
		String firstName;
		String lastName;
		String partnerCode;
		String countryCode;
		String stateCode;
		String country;
		String status;

		if (partner != null) {
			if (partner.getFirstName() == null) {
				firstName = "";
			} else {
				firstName = partner.getFirstName();
			}
			if (partner.getLastName() == null) {
				lastName = "";
			} else {
				lastName = partner.getLastName();
			}
			if (partner.getPartnerCode() == null) {
				partnerCode = "";
			} else {
				partnerCode = partner.getPartnerCode();
			}
			if (countryCodeSearch == null) {
				countryCode = "";
			} else {
				countryCode = countryCodeSearch;
			}
			if (stateSearch == null) {
				stateCode = "";
			} else {
				stateCode = stateSearch;
			}
			if (countrySearch == null) {
				country = "";
			} else {
				country = countrySearch;
			}
			if (partner.getStatus() == null) {
				status = "";
			} else {
				status = partner.getStatus();
			}
			
		List <Object> partnerList1 = new ArrayList<Object>();
		List pList = partnerManager.getAllAgent(sessionUserName);
		String partnerCodes=pList.toString();
		
		String partnerIds="";
		if(!pList.isEmpty())
		{
		StringBuffer partnerIdsBuffer = new StringBuffer("");
		String pId=pList.toString();
		pId=pId.replace("[", "");
		pId=pId.replace("]", "");
		String []partnerIdsArray=pId.split(",");
		int arrayLength = partnerIdsArray.length;
		for (int i = 0; i < arrayLength; i++) {
		partnerIdsBuffer.append("'");
		partnerIdsBuffer.append(partnerIdsArray[i].trim());
		partnerIdsBuffer.append("'");
		partnerIdsBuffer.append(",");

		}
		partnerIds = new String(partnerIdsBuffer);
		if (!partnerIds.equals("")) {
		partnerIds = partnerIds.substring(0, partnerIds.length() - 1);
		} else if (partnerIds.equals("")) {
		partnerIds = new String("''");
		}
		}
		
		partnerAgentList=partnerManager.searchAgentList(firstName,lastName,partnerCode,countryCode,stateCode, country, status, partnerIds, sessionCorpID );
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String partnerRatingList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner=partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		
		BigDecimal multiplyVal = new BigDecimal(100);
		
		/*partnerRating = partnerManager.ratingList(partnerPublic.getPartnerCode(), sessionCorpID);
		SortedMap <String, BigDecimal> yearMapP = new TreeMap<String, BigDecimal>();
		SortedMap <String, BigDecimal> yearMapN = new TreeMap<String, BigDecimal>();
		
		Iterator it = partnerRating.iterator();
		
		String key = new String("");
		String samekey = new String("");
		String year = "";
		BigDecimal pValue = new BigDecimal("0");
		BigDecimal nValue = new BigDecimal("0");
		
		BigDecimal ratingTempPos = new BigDecimal("0");
		BigDecimal ratingTempCount = new BigDecimal("0");
		
		while(it.hasNext()){	
			Object listObject = it.next();
			if(((HibernateDTO) listObject).getYear() == null){
				year = "";
			}else{
				year =((HibernateDTO) listObject).getYear().toString();
			}
			
			if(((HibernateDTO) listObject).getOaeValuationP() != null){
				pValue = new BigDecimal(((HibernateDTO) listObject).getOaeValuationP().toString());
			}
			
			if(((HibernateDTO) listObject).getOaeValuationN() != null){
				nValue = new BigDecimal(((HibernateDTO) listObject).getOaeValuationN().toString());
			}
			
			ratingTempPos = ratingTempPos.add(pValue);
			ratingTempCount = ratingTempCount.add(pValue).add(nValue.abs());
			
			key = year;
			if (key.equals(samekey)){
				BigDecimal valuePos = yearMapP.get(key);
				if(valuePos != null){
					pValue = pValue.add(valuePos);
				}
				
				BigDecimal valueNag = yearMapN.get(key);
				if(valueNag != null){
					nValue = nValue.add(valueNag);
				}
				
				yearMapP.put(year, pValue);
				yearMapN.put(year, nValue);
			}else{
				yearMapP.put(year, pValue);
				yearMapN.put(year, nValue);
			}
			samekey = year;
		}
		try{
			BigDecimal ratingTemp = (ratingTempPos.divide(ratingTempCount, 3, 0)).multiply(multiplyVal);
			rating = new BigDecimal(Math.round(ratingTemp.floatValue()));
		}catch(Exception ex){}
		
		getRequest().setAttribute("yearMapP", yearMapP);
		getRequest().setAttribute("yearMapN", yearMapN);*/
		
		BigDecimal posSix = new BigDecimal("0");
		BigDecimal negSix = new BigDecimal("0");
		
		BigDecimal pos1 = new BigDecimal("0");
		BigDecimal neg1 = new BigDecimal("0");
		
		BigDecimal pos2 = new BigDecimal("0");
		BigDecimal neg2 = new BigDecimal("0");
		
		partnerRatingSixMonths = partnerManager.ratingListSixMonths(partnerPublic.getPartnerCode(), sessionCorpID);
		Iterator it6m = partnerRatingSixMonths.iterator();
		while(it6m.hasNext()){	
			Object [] row = (Object[])it6m.next();
			posSix = posSix.add(new BigDecimal(row[2].toString()));
			negSix = negSix.add(new BigDecimal(row[3].toString()));
		}
		
		partnerRatingOneYr = partnerManager.ratingListOneYr(partnerPublic.getPartnerCode(), sessionCorpID);
		Iterator it1y = partnerRatingOneYr.iterator();
		while(it1y.hasNext()){	
			Object [] row = (Object[])it1y.next();
			pos1 = pos1.add(new BigDecimal(row[2].toString()));
			neg1 = neg1.add(new BigDecimal(row[3].toString()));
		}

		partnerRatingTwoYr = partnerManager.ratingListTwoYr(partnerPublic.getPartnerCode(), sessionCorpID);
		Iterator it2y = partnerRatingTwoYr.iterator();
		while(it2y.hasNext()){	
			Object [] row = (Object[])it2y.next();
			pos2 = pos2.add(new BigDecimal(row[2].toString()));
			neg2 = neg2.add(new BigDecimal(row[3].toString()));
		}
		
		BigDecimal ratingTempCount6m = posSix.add(negSix.abs());
		BigDecimal ratingTempCount1y = pos1.add(neg1.abs());
		BigDecimal ratingTempCount2y = pos2.add(neg2.abs()); 
		try{
			
			if(ratingTempCount6m !=null && ratingTempCount6m.doubleValue() !=0.00){
			BigDecimal ratingTemp6m = (posSix.divide(ratingTempCount6m, 3, 0)).multiply(multiplyVal);
			rating6m = new BigDecimal(Math.round(ratingTemp6m.floatValue()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}	
		try{
			if(ratingTempCount1y !=null && ratingTempCount1y.doubleValue() !=0.00){
			BigDecimal ratingTemp1y = (pos1.divide(ratingTempCount1y, 3, 0)).multiply(multiplyVal);
			rating = new BigDecimal(Math.round(ratingTemp1y.floatValue()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}	
		try{
			if(ratingTempCount2y !=null && ratingTempCount2y.doubleValue() !=0.00){
			BigDecimal ratingTemp2y = (pos2.divide(ratingTempCount2y, 3, 0)).multiply(multiplyVal);
			rating2y = new BigDecimal(Math.round(ratingTemp2y.floatValue()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		getRequest().setAttribute("pos6", posSix);
		getRequest().setAttribute("neg6", negSix);
		getRequest().setAttribute("pos1", pos1);
		getRequest().setAttribute("neg1", neg1);
		getRequest().setAttribute("pos2", pos2);
		getRequest().setAttribute("neg2", neg2);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String partnerAccountRatingList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		logger.warn(" partnerAccountRatingList() AJAX Call : "+getRequest().getParameter("ajax")); 
		partnerPublic = partnerPublicManager.get(id);
		BigDecimal multiplyVal = new BigDecimal(100);
		
		BigDecimal posSix = new BigDecimal("0");
		BigDecimal negSix = new BigDecimal("0");
		
		BigDecimal pos1 = new BigDecimal("0");
		BigDecimal neg1 = new BigDecimal("0");
		
		BigDecimal pos2 = new BigDecimal("0");
		BigDecimal neg2 = new BigDecimal("0");
		
		List partnerAccRatingSixMonths = partnerManager.accountRatingList(partnerPublic.getPartnerCode(), sessionCorpID, "183");
		Iterator it6m = partnerAccRatingSixMonths.iterator();
		while(it6m.hasNext()){	
			Object [] row = (Object[])it6m.next();
			posSix = posSix.add(new BigDecimal(row[0].toString()));
			negSix = negSix.add(new BigDecimal(row[1].toString()));
		}
		
		List partnerAccRatingOneYr = partnerManager.accountRatingList(partnerPublic.getPartnerCode(), sessionCorpID, "365");
		Iterator it1y = partnerAccRatingOneYr.iterator();
		while(it1y.hasNext()){	
			Object [] row = (Object[])it1y.next();
			pos1 = pos1.add(new BigDecimal(row[0].toString()));
			neg1 = neg1.add(new BigDecimal(row[1].toString()));
		}

		List partnerAccRatingTwoYr = partnerManager.accountRatingList(partnerPublic.getPartnerCode(), sessionCorpID, "730");
		Iterator it2y = partnerAccRatingTwoYr.iterator();
		while(it2y.hasNext()){	
			Object [] row = (Object[])it2y.next();
			pos2 = pos2.add(new BigDecimal(row[0].toString()));
			neg2 = neg2.add(new BigDecimal(row[1].toString()));
		}
		
		BigDecimal ratingTempCount6m = posSix.add(negSix.abs());
		BigDecimal ratingTempCount1y = pos1.add(neg1.abs());
		BigDecimal ratingTempCount2y = pos2.add(neg2.abs());
		try{
			if(ratingTempCount6m !=null && ratingTempCount6m.doubleValue() !=0.00){
			BigDecimal ratingTemp6m = (posSix.divide(ratingTempCount6m, 3, 0)).multiply(multiplyVal);
			rating6mACC = new BigDecimal(Math.round(ratingTemp6m.floatValue()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		try{
			if(ratingTempCount1y !=null && ratingTempCount1y.doubleValue() !=0.00){
			BigDecimal ratingTemp1y = (pos1.divide(ratingTempCount1y, 3, 0)).multiply(multiplyVal);
			ratingACC = new BigDecimal(Math.round(ratingTemp1y.floatValue()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}	
		try{
			if(ratingTempCount2y !=null && ratingTempCount2y.doubleValue() !=0.00){
			BigDecimal ratingTemp2y = (pos2.divide(ratingTempCount2y, 3, 0)).multiply(multiplyVal);
			rating2yACC = new BigDecimal(Math.round(ratingTemp2y.floatValue()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		getRequest().setAttribute("pos6ACC", posSix);
		getRequest().setAttribute("neg6ACC", negSix);
		getRequest().setAttribute("pos1ACC", pos1);
		getRequest().setAttribute("neg1ACC", neg1);
		getRequest().setAttribute("pos2ACC", pos2);
		getRequest().setAttribute("neg2ACC", neg2);
		logger.warn(" partnerAccountRatingList() End AJAX Call : "+getRequest().getParameter("ajax"));		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	
	}
	private int agentSixMnthPos=0;
	private int agentSixMnthNeg=0;
	private BigDecimal agentSixMnthAvg = new BigDecimal("0");
	private int agentOneYrPos=0;
	private int agentOneYrNeg=0;
	private BigDecimal agentOneYrAvg = new BigDecimal("0");
	private int agentTwoYrPos=0;
	private int agentTwoYrNeg=0;
	private BigDecimal agentTwoYrAvg = new BigDecimal("0");
	@SkipValidation
	public String agentRatingCalculationList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Agent Rating Calculation Start");
		logger.warn(" agentRatingCalculationList() AJAX Call : "+getRequest().getParameter("ajax"));
		BigDecimal multiplyVal = new BigDecimal(100);
		BigDecimal tempSixMnthAvg = new BigDecimal("0");
		BigDecimal tempOneYrAvg = new BigDecimal("0");
		BigDecimal tempTwoYrAvg = new BigDecimal("0");
		try{
			List agentRatingSixMonths = partnerManager.agentRatingList(partnerCode, sessionCorpID, "183");
			Iterator itr = agentRatingSixMonths.iterator();
			while(itr.hasNext()){	
				Object [] row = (Object[])itr.next();
				if(row[2].toString().contains("Positive")){
					if(row[0]!=null && !row[0].equals("")){
						String posTemp = row[0].toString();
						int tempPos =  Double.valueOf(posTemp).intValue();
						agentSixMnthPos = agentSixMnthPos+tempPos;
					}
				}else{
					if(row[0]!=null && !row[0].equals("")){
						String negTemp = row[0].toString();
						int tempNeg =  Double.valueOf(negTemp).intValue();
						agentSixMnthNeg = agentSixMnthNeg+tempNeg;
					}
				}
				if(row[1]!=null && !row[1].equals("")){
					tempSixMnthAvg = tempSixMnthAvg.add(new BigDecimal(row[1].toString()));
				}
			}
			if(tempSixMnthAvg.intValue()>0){
				BigDecimal agentSixMnth =new BigDecimal(agentSixMnthPos+agentSixMnthNeg);
				if(agentSixMnth !=null && agentSixMnth.doubleValue() !=0.00){
				agentSixMnthAvg = tempSixMnthAvg.divide(agentSixMnth, 1, RoundingMode.HALF_UP);
				}
			}
			
			List agentRatingOneYr = partnerManager.agentRatingList(partnerCode, sessionCorpID, "365");
			Iterator itr1 = agentRatingOneYr.iterator();
			while(itr1.hasNext()){	
				Object [] row = (Object[])itr1.next();
				if(row[2].toString().contains("Positive")){
					if(row[0]!=null && !row[0].equals("")){
						String posTemp = row[0].toString();
						int tempPos =  Double.valueOf(posTemp).intValue();
						agentOneYrPos = agentOneYrPos+tempPos;
					}
				}else{
					if(row[0]!=null && !row[0].equals("")){
						String negTemp = row[0].toString();
						int tempNeg =  Double.valueOf(negTemp).intValue();
						agentOneYrNeg = agentOneYrNeg+tempNeg;
					}
				}
				if(row[1]!=null && !row[1].equals("")){
					tempOneYrAvg = tempOneYrAvg.add(new BigDecimal(row[1].toString()));
				}
			}
			if(tempOneYrAvg.intValue()>0){
				BigDecimal agentOneYr =new BigDecimal(agentOneYrPos+agentOneYrNeg);
				if(agentOneYr !=null && agentOneYr.doubleValue() !=0.00){
				agentOneYrAvg = tempOneYrAvg.divide(agentOneYr, 1, RoundingMode.HALF_UP);
				}
			}
			
			List agentRatingTwoYr = partnerManager.agentRatingList(partnerCode, sessionCorpID, "730");
			Iterator itr2 = agentRatingTwoYr.iterator();
			while(itr2.hasNext()){	
				Object [] row = (Object[])itr2.next();
				if(row[2].toString().contains("Positive")){
					if(row[0]!=null && !row[0].equals("")){
						String posTemp = row[0].toString();
						int tempPos =  Double.valueOf(posTemp).intValue();
						agentTwoYrPos = agentTwoYrPos+tempPos;
					}
				}else{
					if(row[0]!=null && !row[0].equals("")){
						String negTemp = row[0].toString();
						int tempNeg =  Double.valueOf(negTemp).intValue();
						agentTwoYrNeg = agentTwoYrNeg+tempNeg;
					}
				}
				if(row[1]!=null && !row[1].equals("")){
					tempTwoYrAvg = tempTwoYrAvg.add(new BigDecimal(row[1].toString()));
				}
			}
			if(tempTwoYrAvg.intValue()>0){
				BigDecimal agentTwoYr =new BigDecimal(agentTwoYrPos+agentTwoYrNeg);
				if(agentTwoYr !=null && agentTwoYr.doubleValue() !=0.00){
				agentTwoYrAvg = tempTwoYrAvg.divide(agentTwoYr, 1, RoundingMode.HALF_UP);
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Agent Rating Calculation End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String accountRatingFeedbackList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner=partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		ratingFeedback = partnerManager.getAccountFeedbackList(partnerPublic.getPartnerCode(), year,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String ratingFeedbackList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner=partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		ratingFeedback = partnerManager.getFeedbackList(partnerPublic.getPartnerCode(), year,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String getAgentRatingFeedbackList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ratingFeedback = partnerManager.getAgentRatingFeedbackList(partnerCode, year,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	List partnerReloList=new ArrayList();
	@SkipValidation
	public String listByDestinationRelo() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		if((jobReloName!=null)&&(!jobReloName.equalsIgnoreCase(""))){
			jobReloName=jobReloName.replaceAll("_", "");
		}else{
			jobReloName="";
		}
		listOfVendorCode = refMasterManager.findByParameter(corpId, "TYPEOFVENDOR");
		partnerReloList = partnerManager.findByDestinationRelo(destination,jobRelo,jobReloName,sessionCorpID);
		// System.out.println(partners);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
private String jobReloName;
	
	
	@SkipValidation
	public String partnerActivityList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//partner=partnerManager.get(id);
		partnerPublic = partnerPublicManager.get(id);
		List partnerActivity = partnerManager.getPartnerActivityList(partnerPublic.getPartnerCode(), sessionCorpID);
		SortedMap <String, BigDecimal> yearMap = new TreeMap<String, BigDecimal>();
		SortedMap <String, BigDecimal> weightMap = new TreeMap<String, BigDecimal>();
		
		Iterator it = partnerActivity.iterator();
		
		String key = new String("");
		String samekey = new String("");
		String year = "";
		BigDecimal mapValue = new BigDecimal("0");
		BigDecimal mapWeightValue = new BigDecimal("0");
		
		while(it.hasNext()){	
			Object listObject = it.next();
			if(((HibernateDTO) listObject).getYear() == null){
				year = "";
			}else{
				year =((HibernateDTO) listObject).getYear().toString();
			}
			
			if(((HibernateDTO) listObject).getOaCount() != null){
				mapValue = new BigDecimal(((HibernateDTO) listObject).getOaCount().toString());
			}
			
			if(((HibernateDTO) listObject).getActualWeight() != null){
				mapWeightValue = new BigDecimal(((HibernateDTO) listObject).getActualWeight().toString());
			}
			
			key = year;
			if (key.equals(samekey)){
				BigDecimal valuePos = yearMap.get(key);
				if(valuePos != null){
					mapValue = mapValue.add(valuePos);
				}
				yearMap.put(year, mapValue);
				
				BigDecimal valueWeight = weightMap.get(key);
				if(valueWeight != null){
					mapWeightValue = mapWeightValue.add(valueWeight);
				}
				weightMap.put(year, mapWeightValue);
				
			}else{
				yearMap.put(year, mapValue);
				weightMap.put(year, mapWeightValue);
			}
			samekey = year;
		}
		
		
		// SSCW Activity
		List partnerSSCWActivity = partnerManager.getPartnerSSCWActivityList(partnerPublic.getPartnerCode(),sessionCorpID);
		SortedMap <String, BigDecimal> yearMapSSCW = new TreeMap<String, BigDecimal>();
		SortedMap <String, BigDecimal> weightMapSSCW = new TreeMap<String, BigDecimal>();
		
		Iterator itSSCW = partnerSSCWActivity.iterator();
		
		String keySSCW = new String("");
		String samekeySSCW = new String("");
		String yearSSCW = "";
		BigDecimal mapValueSSCW = new BigDecimal("0");
		BigDecimal mapWeightValueSSCW = new BigDecimal("0");
		Iterator iteratorSSCW = yearMap.entrySet().iterator();
		while (iteratorSSCW.hasNext()) {
			Map.Entry entry = (Map.Entry) iteratorSSCW.next();
			String yearEQ = (String) entry.getKey();
			if(!yearEQ.equals(yearSSCW)){
				
				BigDecimal valuePos = yearMapSSCW.get(yearEQ);
				if(valuePos == null || valuePos.doubleValue()==0){
					yearMapSSCW.put(yearEQ, new BigDecimal("0"));
				}
				
				BigDecimal valueWeightSSCW = weightMapSSCW.get(yearEQ);
				if(valueWeightSSCW == null || valueWeightSSCW.doubleValue() == 0){
					weightMapSSCW.put(yearEQ, new BigDecimal("0"));
				}
			}
		}
		while(itSSCW.hasNext()){	
			Object listObject = itSSCW.next();
			if(((HibernateDTO) listObject).getYear() == null){
				yearSSCW = "";
			}else{
				yearSSCW =((HibernateDTO) listObject).getYear().toString();
			}
			
			if(((HibernateDTO) listObject).getOaCount() != null){
				mapValueSSCW = new BigDecimal(((HibernateDTO) listObject).getOaCount().toString());
			}
			
			if(((HibernateDTO) listObject).getActualWeight() != null){
				mapWeightValueSSCW = new BigDecimal(((HibernateDTO) listObject).getActualWeight().toString());
			} else{
				mapWeightValueSSCW = new BigDecimal(0);
			}
			keySSCW = yearSSCW;
			if (keySSCW.equals(samekeySSCW)){
				BigDecimal valuePos = yearMapSSCW.get(keySSCW);
				if(valuePos != null){
					mapValueSSCW = mapValueSSCW.add(valuePos);
				}
				yearMapSSCW.put(yearSSCW, mapValueSSCW);
				
				BigDecimal valueWeightSSCW = weightMapSSCW.get(keySSCW);
				if(valueWeightSSCW != null){
					mapWeightValueSSCW = mapWeightValueSSCW.add(valueWeightSSCW);
				}
				weightMapSSCW.put(yearSSCW, mapWeightValueSSCW);
				
			}else{
				yearMapSSCW.put(yearSSCW, mapValueSSCW);
				weightMapSSCW.put(yearSSCW, mapWeightValueSSCW);
			}
			samekeySSCW = yearSSCW;
		}
		
		getRequest().setAttribute("yearMap", yearMap);
		getRequest().setAttribute("weightMap", weightMap);
		getRequest().setAttribute("yearMapSSCW", yearMapSSCW);
		getRequest().setAttribute("weightMapSSCW", weightMapSSCW);
		corpId = sessionCorpID.toUpperCase();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String partnerAccountActivityList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		// SSCW Activity
		logger.warn(" partnerAccountActivityList() AJAX Call : "+getRequest().getParameter("ajax")); 
		partnerPublic = partnerPublicManager.get(id);
		List partnerSSCWActivity = partnerManager.getPartnerAccountActivityList(partnerPublic.getPartnerCode(),sessionCorpID);
		SortedMap <String, BigDecimal> yearMapSSCW = new TreeMap<String, BigDecimal>();
		SortedMap <String, BigDecimal> weightMapSSCW = new TreeMap<String, BigDecimal>();
		SortedMap <String, BigDecimal> revenueMapSSCW = new TreeMap<String, BigDecimal>();
		
		Iterator itSSCW = partnerSSCWActivity.iterator();
		
		String keySSCW = new String("");
		String samekeySSCW = new String("");
		String yearSSCW = "";
		BigDecimal mapValueSSCW = new BigDecimal("0");
		BigDecimal mapWeightValueSSCW = new BigDecimal("0");
		BigDecimal mapRevenueSSCW = new BigDecimal("0");
		
		while(itSSCW.hasNext()){	
			Object listObject = itSSCW.next();
			if(((HibernateDTO) listObject).getYear() == null){
				yearSSCW = "";
			}else{
				yearSSCW =((HibernateDTO) listObject).getYear().toString();
			}
			
			if(((HibernateDTO) listObject).getOaCount() != null){
				mapValueSSCW = new BigDecimal(((HibernateDTO) listObject).getOaCount().toString());
			}
			
			if(((HibernateDTO) listObject).getActualWeight() != null){
				mapWeightValueSSCW = new BigDecimal(((HibernateDTO) listObject).getActualWeight().toString());
			}
			
			if(((HibernateDTO) listObject).getActualRevenue() != null){
				mapRevenueSSCW = new BigDecimal(((HibernateDTO) listObject).getActualRevenue().toString());
			}
			
			keySSCW = yearSSCW;
			//if (keySSCW.equals(samekeySSCW)){
				BigDecimal valuePos = yearMapSSCW.get(keySSCW);
				if(valuePos != null){
					mapValueSSCW = mapValueSSCW.add(valuePos);
				}
				yearMapSSCW.put(yearSSCW, mapValueSSCW);
				
				BigDecimal valueWeightSSCW = weightMapSSCW.get(keySSCW);
				if(valueWeightSSCW != null){
					mapWeightValueSSCW = mapWeightValueSSCW.add(valueWeightSSCW);
				}
				weightMapSSCW.put(yearSSCW, mapWeightValueSSCW);
				
				BigDecimal valueRevenueSSCW = revenueMapSSCW.get(keySSCW);
				if(valueRevenueSSCW != null){
					mapRevenueSSCW = mapRevenueSSCW.add(valueRevenueSSCW);
				}
				revenueMapSSCW.put(yearSSCW, mapRevenueSSCW);
				
			/*}else{
				yearMapSSCW.put(yearSSCW, mapValueSSCW);
				weightMapSSCW.put(yearSSCW, mapWeightValueSSCW);
				revenueMapSSCW.put(yearSSCW, mapRevenueSSCW);
			}
			samekeySSCW = yearSSCW;*/
		}
		
		getRequest().setAttribute("yearMapSSCW", yearMapSSCW);
		getRequest().setAttribute("weightMapSSCW", weightMapSSCW);
		getRequest().setAttribute("revenueMapSSCW", revenueMapSSCW);
		corpId = sessionCorpID.toUpperCase();
		logger.warn(" partnerAccountActivityList() End AJAX Call : "+getRequest().getParameter("ajax")); 		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String driverId;
	private String driverVanId;
	private String driverName;
	private String driverCell;
	private String vLocation;
	public List driverInvolvementDtoList;
	private String homeAddress;
	private List nextDriverLocationList;
	@SkipValidation
	public String driverInvolvementMap(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
	driverInvolvment();
	driverDropDownList=partnerManager.driverDropDownList(sessionCorpID);
		if(driverId==null)
		{
			driverId="";
		}
		homeAddress=partnerManager.getDriverHomeAddress(driverId);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String nextDriverLocationValue;
	@SkipValidation
	public String editNextDriverLocation(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		nextDriverLocationList=partnerManager.nextDriverLocationList(driverId, sessionCorpID);
		if(nextDriverLocationList!=null && (!(nextDriverLocationList.isEmpty())) && (nextDriverLocationList.get(0)!=null)){
			String str[]=nextDriverLocationList.get(0).toString().split("~");
			nextDriverLocationValue=str[0]+"~"+str[1]+"~"+str[2]+"~"+str[3];
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String driverLocationMap(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		driverType = refMasterManager.findByParameter(corpId, "DRIVER_TYPE");		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private List dLocationList;
	@SkipValidation
	public String driverLocationSearchList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		dLocationList=partnerManager.getDriverLocationList(sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
    private List driverInvolvementList;
	   @SkipValidation
	public String driverInvolvementPoint(){
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
		
		
	 public InputStream getDriverInvolvementPointMarkup(){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
			ByteArrayInputStream output = null;
			if(driverId==null)
			{
				driverId="";
			}
			driverInvolvementList = partnerManager.getAllInvolvementOfDriverMap(driverId,sessionCorpID);
			if(driverInvolvementList!=null)
			  {
		  		StringBuilder ports = new StringBuilder();
				int count = 0;
				Iterator it=driverInvolvementList.iterator();
				
				while(it.hasNext()){
					count++;
					Object []obj=(Object[]) it.next();
					if (count > 1)
						ports.append(",");
					else
						ports.append("");                                                                                                                                                       																																																																																												
					ports.append("{\"driverId\": \"" + obj[0] + "\", \"regNo\": \"" + obj[1] + "\", \"location\": \"" + obj[2] + "\", \"loadType\": \"" + obj[3] + "\", \"keyDate\": \"" + obj[4] + "\", \"name\": \"" + obj[5] +  "\", \"estWt\": \"" + obj[6] +  "\", \"estCube\": \"" + obj[7] +  "\", \"shipNumber\": \"" + obj[8] +  "\", \"listDate\": \"" + obj[9] +  "\", \"address\": \"" + obj[10] +  "\", \"actWt\": \"" + obj[11] +  "\", \"actCube\": \"" + obj[12] +  "\", \"job\": \"" + obj[13] +  "\", \"vanLineNationalCode\": \"" + obj[14] +  "\", \"selfHaul\": \"" + obj[15] +  "\", \"estimatedRevenue\": \"" + obj[16] +  "\", \"driverCell\": \"" + obj[17] +  "\", \"mile\": \"" + obj[18] + "\", \"tripNumber\": \"" + obj[19] + "\" }");
				}
				
				String prependStr = "{ \"count\":" + count + ", \"ports\":[";
				String appendStr = "]}";
				String s=prependStr + ports.toString() + appendStr;
	    		        output=new ByteArrayInputStream(s.getBytes());		  
	    	   }			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return output;
		 
		   
	   }
	 
	 
	 String validPartnerCode;
	 @SkipValidation
	  public String findParentAgentCode(){
		  try {
			  if(partnerCode!=null){
				  validPartnerCode = partnerManager.codeCheckPartner(partnerCode,sessionCorpID,partnerType);
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
			return SUCCESS; 
	  }
		
    private List driverLocList;
    private Map<String, String> driverDropDownList;
    private Map<String, String> driverTypeDropDownList;
    private String driverTypeValue;
    public String getDriverTypeValue() {
		return driverTypeValue;
	}

	public void setDriverTypeValue(String driverTypeValue) {
		this.driverTypeValue = driverTypeValue;
	}

	@SkipValidation
	public String driverLocationPoint(){  
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
    	driverType = refMasterManager.findByParameter(corpId, "DRIVER_TYPE");    	
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public InputStream getDriverLocationPointMarkup()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		ByteArrayInputStream output = null;
		driverLocList = partnerManager.getAllDriverLocation(driverTypeValue,sessionCorpID);
		if(driverLocList!=null)
		  {
	  		StringBuilder ports = new StringBuilder();
			int count = 0;
			Iterator it=driverLocList.iterator();
			
			while(it.hasNext()){
				count++;
				Object []obj=(Object[]) it.next();
				if (count > 1)
					ports.append(",");
				else
					ports.append("");
				ports.append("{\"partnerCode\": \"" + obj[0] + "\", \"name\": \"" + obj[1] + "\", \"vanLastLocation\": \"" + obj[4] + "\", \"reportTime\": \"" + obj[5] + "\", \"vanAvailCube\": \"" + obj[6] + "\", \"driverAgency\": \"" + obj[2] +  "\", \"validNationalCode\": \"" + obj[3] + "\", \"currentVanID\": \"" + obj[7] + "\"}");
			}
			
			String prependStr = "{ \"count\":" + count + ", \"ports\":[";
			String appendStr = "]}";
			String s=prependStr + ports.toString() + appendStr;
    		        output=new ByteArrayInputStream(s.getBytes());		  
    	   }	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return output;
	}
	int tempLdDl=0;
	public String driverInvolvment(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
	if(driverId==null)
	{
		driverId="";
	}
	driverInvolvementList = partnerManager.getAllInvolvementOfDriver(driverId,sessionCorpID);	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
	}
	

	@SkipValidation
	public String listByNetworkPartner(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.findByNetwork());		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findAmount(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		totalAmmountList = partnerManager.getAmmount(partnerCode,sessionCorpID);
		if(totalAmmountList!=null && !totalAmmountList.isEmpty() && (totalAmmountList.get(0)!=null)){
			totalAmmount = totalAmmountList.get(0).toString();	
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String listByOriginforClientDirective() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		if(origin!=null && !origin.equals("")){
			countryCode=origin;
		}
		if(destination!=null && !destination.equals("")){
			countryCode=destination;
		}
		if(subOrigin!=null && !subOrigin.equals("")){
			countryCode=subOrigin;
		}
		if(subDestin!=null && !subDestin.equals("")){
			countryCode=subDestin;
		}
		partners = new LinkedHashSet(partnerManager.findByOriginforClientDirective(countryCode,sessionCorpID,billToCode));	
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;
	}
	private String partnerNameAutoCopmlete;
	private String partnerNameType;
	private List partnerDetailsAutoComplete;
	private String partnerDetailsAutoCompleteGsonData;
	private String autocompleteFormName;
	private String autocompleteFieldCode;
	private String autocompleteFieldName;
	private String autocompleteDivId;
	private String autocompleteCondition;
	private String partnerNameId;
	private String paertnerCodeId;
	private String pricingAccountLineId;
	@SkipValidation
	  public String getPartnerDetailsForAutoCompleteNew(){   
		try {
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
			 
			 partners = new LinkedHashSet(partnerManager.findPartnerDetailsForAutoCompleteNew(partnerNameAutoCopmlete,autocompleteCondition,sessionCorpID,cmmDmmFlag,billToCode));
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
			return SUCCESS; 
	  }
	
	
	@SkipValidation
	public String popupListPartnerVanLineNew() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		//getComboList(sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		 if(company!=null){
		      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
		  		cmmDmmFlag = true;
		  		}
		 }
		try {
			String firstName;
			String lastName;
			String aliasName;
			String partnerCode;
			String billingCountryCode;
			String billingStateCode;
			String billingCountry;
			String vanLineCODE;
			
			if(origin!=null && !origin.equals("")){
				countryCode=origin;
			}
			if(destination!=null && !destination.equals("")){
				countryCode=destination;
			}
			if(subOrigin!=null && !subOrigin.equals("")){
				countryCode=subOrigin;
			}
			if(subDestin!=null && !subDestin.equals("")){
				countryCode=subDestin;
			}
			
			
			if (partner != null) {
				if (partner.getFirstName() == null) {
					firstName = "";
				} else {
					firstName = partner.getFirstName();
				}
				if (partner.getLastName() == null) {
					lastName = "";
				} else {
					lastName = partner.getLastName();
				}
				if (partner.getAliasName() == null) {
					aliasName = "";
				} else {
					aliasName = partner.getAliasName();
				}
				if (partner.getPartnerCode() == null) {
					partnerCode = "";
				} else {
					partnerCode = partner.getPartnerCode();
				}
				if (countryCodeSearch == null) {
					billingCountryCode = "";
				} else {
					billingCountryCode = countryCodeSearch;
				}
				if (stateSearch == null) {
					billingStateCode = "";
				} else {
					billingStateCode = stateSearch;
				}
				if (countrySearch == null) {
					billingCountry = "";
				} else {
					billingCountry = countrySearch;
				}
				if (vanlineCode == null) {
					vanLineCODE = "";
				} else {
					vanLineCODE = vanlineCode;
				}
				
				if(partner.getIsAccount() == null){
					partner.setIsAccount(false);
				}
				
				if(partner.getIsAgent() == null){
					partner.setIsAgent(false);
				}
				
				if(partner.getIsCarrier() == null){
					partner.setIsCarrier(false);
				}
				
				if(partner.getIsOwnerOp() == null){
					partner.setIsOwnerOp(false);
				}
				
				if(partner.getIsPrivateParty() == null){
					partner.setIsPrivateParty(false);
				}
				
				if(partner.getIsVendor() == null){
					partner.setIsVendor(false);
				}
				if(partner.getBillingInstructionCode()!=null)
				{
					billToCode=partner.getBillingInstructionCode().trim();
				}
																																									
				partners = new LinkedHashSet(partnerManager.getPartnerVanLinePopupListNew("", sessionCorpID,firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, vanLineCODE, partner.getIsPrivateParty(), partner.getIsAccount(), partner.getIsAgent(), partner.getIsVendor(), partner.getIsCarrier(), partner.getIsOwnerOp(), cmmDmmFlag,billToCode));
			} else {
				partner = new Partner();
				partner.setIsAccount(true);
				partner.setIsPrivateParty(true);
				partner.setIsVendor(true);
				partner.setIsAgent(true);
				partner.setIsCarrier(true);
				partner.setIsOwnerOp(true);
				if(partner.getBillingInstructionCode()!=null)
				{
					billToCode=partner.getBillingInstructionCode().trim();
				}
				partners = new LinkedHashSet(partnerManager.getPartnerVanLinePopupListNew("", sessionCorpID, "","", "", "", "", "", "", "",true,true,true,true,true,true,cmmDmmFlag, billToCode));
			}
		} catch (Exception e) {
			partner = new Partner();
			partner.setIsAccount(true);
			partner.setIsPrivateParty(true);
			partner.setIsVendor(true);
			partner.setIsAgent(true);
			partner.setIsCarrier(true);
			partner.setIsOwnerOp(true);
			if(partner.getBillingInstructionCode()!=null)
			{
				billToCode=partner.getBillingInstructionCode().trim();
			}
			partners = new LinkedHashSet(partnerManager.getPartnerVanLinePopupListNew("", sessionCorpID, "", "", "","", "", "", "", "",false,false,false,false,false,false,cmmDmmFlag,billToCode));
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key));
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String popupListNew() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String parameter = (String) getRequest().getSession().getAttribute("paramView");
		if (parameter != null && parameter.equalsIgnoreCase("View")) {
			getRequest().getSession().removeAttribute("paramView");
		}
		try{
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
			      if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
			  		cmmDmmFlag = true;
			  		}
			 }
		String lastName;
		String partnerCode;
		String billingCountryCode;
		String billingCountry;
		String billingStateCode;
		String extReference;
		String aliasName;
		String vanlineCode="";
		if(vanlineCodeSearch!=null){
			vanlineCode=vanlineCodeSearch.trim();
		}
		if(partner !=null){
		lastName = partner.getLastName() == null ? "" : partner.getLastName().trim();
		partnerCode = partner.getPartnerCode() == null ? "" : partner.getPartnerCode().trim();
		aliasName=partner.getAliasName()==null ? "" :partner.getAliasName().trim();
			if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) 
			{
					billingCountryCode = partner.getBillingCountryCode().trim();
			}
			else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode())))
			{
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}
			else if (origin != null && (!"".equals(origin))) 
			{
				billingCountryCode=origin;
			}
			else if (destination != null && (!"".equals(destination)))
			{
				billingCountryCode=destination;
			}
			else
			{
				billingCountryCode = "";
			}
			
			if (partner.getBillingState() != null)
			{
				billingStateCode = partner.getBillingState().trim();
			}
			else if(partner.getTerminalState() != null)
			{
				billingStateCode = partner.getTerminalState().trim();
			}
			else
			{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null)
			{
				billingCountry = partner.getBillingCountry().trim();
			}
			else if(partner.getTerminalCountry() != null)
			{
				billingCountry = partner.getTerminalCountry().trim();
			}
			else
			{
				billingCountry = "";
			}
			if ((partner.getExtReference() == null))
			{
				extReference = "";
			}
			else 
			{
				extReference = partner.getExtReference().trim();
			}
			if(partner.getBillingInstructionCode()!=null)
			{
				billToCode=partner.getBillingInstructionCode().trim();
			}
			
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid);
				customerFile = serviceOrder.getCustomerFile();
				partners = new LinkedHashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,customerFile.getId(),extReference));	
			}else{
			if (flag != null && flag.trim().length() > 0) {
				partners = new LinkedHashSet(partnerManager.getPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag));
			} else {
				partners = new LinkedHashSet(partnerManager.getPartnerPopupListNew(partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag,billToCode));
			}
		    }
			getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
			getRequest().getSession().setAttribute("lastName", partner.getLastName());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getBillingCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getBillingCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getBillingState());
			getRequest().getSession().setAttribute("billingCountryCode", partner.getTerminalCountryCode());
			getRequest().getSession().setAttribute("billingCountry", partner.getTerminalCountry());
			getRequest().getSession().setAttribute("billingStateCode", partner.getTerminalState());
			getRequest().getSession().setAttribute("billToCode", partner.getBillingInstructionCode());
		} else {
			if (partnerType.equals("DF")) {
				serviceOrder = serviceOrderManager.get(sid); 
				customerFile = serviceOrder.getCustomerFile();
				partners = new LinkedHashSet(partnerManager.searchDefaultVandor(sid,serviceOrder.getJob(),partnerType, sessionCorpID, "", "","", "", "", "",customerFile.getId(),""));	
			}else{
			if (flag != null && flag.trim().length() > 0) {
				partners = new LinkedHashSet(partnerManager.getPartnerPopupListCompDiv(partnerType, sessionCorpID, compDiv, "","", "", "", "", "","",vanlineCode,cmmDmmFlag));
			} else {
				if("YES".equals(search)){
					
				}
				partners = new LinkedHashSet(partnerManager.getPartnerPopupList(partnerType, sessionCorpID, "", "", "", "","","","",customerVendor,vanlineCode,cmmDmmFlag));
			}
			}
		}
		origin=serviceOrder.getOriginCountryCode();
		destination=serviceOrder.getDestinationCountryCode();
		subOrigin=serviceOrder.getOriginCountry();
		subDestin=serviceOrder.getDestinationCountry();
		
		}catch(Exception e){
			e.printStackTrace();
		}
		if("RSKY".equals(partnerType)){
			RedSkyAgentPartner="RSKY";
		}	
		//}
		//getRequest().getSession().setAttribute("partnerCode", partner.getPartnerCode());
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Set getPartners() {
		return partners;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Boolean getCheckPartnerCode() {
		return checkPartnerCode;
	}

	public CustomerFileManager getCustomerFileManager() {
		return customerFileManager;
	}

	public String getDestination() {
		return destination;
	}

	public String getDupPartnerCode() {
		return dupPartnerCode;
	}

	public List getDupPartnerCodeList() {
		return dupPartnerCodeList;
	}

	public String getFindFor() {
		return findFor;
	}

	public Long getId() {
		return id;
	}

	public List getLs() {
		return ls;
	}

	public Long getMaxId() {
		return maxId;
	}

	public List getMaxPartnerCode() {
		return maxPartnerCode;
	}

	public Long getNewPartnerCode() {
		return newPartnerCode;
	}

	public String getOrigin() {
		return origin;
	}

	public PartnerManager getPartnerManager() {
		return partnerManager;
	}

	public PartnerRatesManager getPartnerRatesManager() {
		return partnerRatesManager;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public Map<String, String> getStorageBillingGroup() {
		return storageBillingGroup;
	}

	public void setStorageBillingGroup(Map<String, String> storageBillingGroup) {
		this.storageBillingGroup = storageBillingGroup;
	}

	public Map<String, String> getTypeOfVendor() {
		return typeOfVendor;
	}

	public void setTypeOfVendor(Map<String, String> typeOfVendor) {
		this.typeOfVendor = typeOfVendor;
	}

	public String getPerId() {
		return perId;
	}

	public void setPerId(String perId) {
		this.perId = perId;
	}

	public List getCarrierCodeList() {
		return carrierCodeList;
	}

	public void setCarrierCodeList(List carrierCodeList) {
		this.carrierCodeList = carrierCodeList;
	}

	public String getFormClose() {
		return formClose;
	}

	public void setFormClose(String formClose) {
		this.formClose = formClose;
	}

	public List getMultiAuthorization() {
		return multiAuthorization;
	}

	public void setMultiAuthorization(List multiAuthorization) {
		this.multiAuthorization = multiAuthorization;
	}

	public String getAccPartnerCode() {
		return accPartnerCode;
	}

	public void setAccPartnerCode(String accPartnerCode) {
		this.accPartnerCode = accPartnerCode;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public List getVendorCodeList() {
		return vendorCodeList;
	}

	public void setVendorCodeList(List vendorCodeList) {
		this.vendorCodeList = vendorCodeList;
	}

	public Map<String, String> getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(Map<String, String> partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public Map<String, String> getCurrency() {
		return currency;
	}

	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}

	public Map<String, String> getIndustry() {
		return industry;
	}

	public void setIndustry(Map<String, String> industry) {
		this.industry = industry;
	}

	public Map<String, String> getLead() {
		return lead;
	}

	public void setLead(Map<String, String> lead) {
		this.lead = lead;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public Map<String, String> getRevenue() {
		return revenue;
	}

	public void setRevenue(Map<String, String> revenue) {
		this.revenue = revenue;
	}

	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}

	public AccountProfile getAccountProfile() {
		return accountProfile;
	}

	public void setAccountProfile(AccountProfile accountProfile) {
		this.accountProfile = accountProfile;
	}

	public List getAccountProfiles() {
		return accountProfiles;
	}

	public void setAccountProfiles(List accountProfiles) {
		this.accountProfiles = accountProfiles;
	}

	public List getStageunits() {
		return stageunits;
	}

	public void setStageunits(List stageunits) {
		this.stageunits = stageunits;
	}

	public AccountContact getAccountContact() {
		return accountContact;
	}

	public void setAccountContact(AccountContact accountContact) {
		this.accountContact = accountContact;
	}

	public List getAccountContacts() {
		return accountContacts;
	}

	public void setAccountContacts(List accountContacts) {
		this.accountContacts = accountContacts;
	}

	public void setAccountContactManager(AccountContactManager accountContactManager) {
		this.accountContactManager = accountContactManager;
	}

	public AgentBase getAgentBase() {
		return agentBase;
	}

	public void setAgentBase(AgentBase agentBase) {
		this.agentBase = agentBase;
	}

	public void setAgentBaseManager(AgentBaseManager agentBaseManager) {
		this.agentBaseManager = agentBaseManager;
	}

	/**
	 * @return the companyDivis
	 */
	public List getCompanyDivis() {
		return companyDivis;
	}

	/**
	 * @param companyDivis
	 *             the companyDivis to set
	 */
	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCompDiv() {
		return compDiv;
	}

	public void setCompDiv(String compDiv) {
		this.compDiv = compDiv;
	}

	/**
	 * @return the pricing
	 */
	public Map<String, String> getPricing() {
		return pricing;
	}

	/**
	 * @return the billing
	 */
	public Map<String, String> getBilling() {
		return billing;
	}

	/**
	 * @return the payable
	 */
	public Map<String, String> getPayable() {
		return payable;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public void setDataSecurityFilterManager(DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}

	public void setDataSecuritySetManager(DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}

	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}

	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}

	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}

	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}

	public List getPartnerUsersList() {
		return partnerUsersList;
	}

	public void setPartnerUsersList(List partnerUsersList) {
		this.partnerUsersList = partnerUsersList;
	}

	public String getUserCheck() {
		return userCheck;
	}

	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public static Map<String, String> getPWD_HINT() {
		return PWD_HINT;
	}

	public static void setPWD_HINT(Map<String, String> pwd_hint) {
		PWD_HINT = pwd_hint;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Set<DataSecuritySet> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<DataSecuritySet> permissions) {
		this.permissions = permissions;
	}

	public Set<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<Role> userRoles) {
		this.userRoles = userRoles;
	}

	public void setRoleManager(RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public List getPartnerChildAgentList() {
		return partnerChildAgentList;
	}

	public void setPartnerChildAgentList(List partnerChildAgentList) {
		this.partnerChildAgentList = partnerChildAgentList;
	}

	public Map<String, String> getCreditTerms() {
		return creditTerms;
	}

	public static List getPType() {
		return pType;
	}

	public static void setPType(List type) {
		pType = type;
	}

	public String getCountryCodeSearch() {
		return countryCodeSearch;
	}

	public void setCountryCodeSearch(String countryCodeSearch) {
		this.countryCodeSearch = countryCodeSearch;
	}

	public String getCountrySearch() {
		return countrySearch;
	}

	public void setCountrySearch(String countrySearch) {
		this.countrySearch = countrySearch;
	}

	public String getStateSearch() {
		return stateSearch;
	}

	public void setStateSearch(String stateSearch) {
		this.stateSearch = stateSearch;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setFindFor(String findFor) {
		this.findFor = findFor;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public Map<String, String> getRatetype() {
		return ratetype;
	}

	public Map<String, String> getState() {
		return state;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public Map<String, String> getCountryDesc() {
		return countryDesc;
	}

	public Map<String, String> getRank() {
		return rank;
	}

	public Map<String, String> getBillgrp() {
		return billgrp;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public Map<String, String> getIsdriver() {
		return isdriver;
	}

	public Map<String, String> getBillinst() {
		return billinst;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public Map<String, String> getPayopt() {
		return payopt;
	}

	public Map<String, String> getSchedule() {
		return schedule;
	}

	public Map<String, String> getPsh() {
		return psh;
	}

	public Map<String, String> getGlcodes() {
		return glcodes;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public Map<String, String> getAll_user() {
		return all_user;
	}

	public void setPartnerRatesManager(PartnerRatesManager partnerRatesManager) {
		this.partnerRatesManager = partnerRatesManager;
	}

	public List getPartnerRatess() {
		return partnerRatess;
	}

	public Map<String, String> getActionType() {
		return actionType;
	}

	public void setActionType(Map<String, String> actionType) {
		this.actionType = actionType;
	}

	public Map<String, String> getActionTypeView() {
		return actionTypeView;
	}

	public void setActionTypeView(Map<String, String> actionTypeView) {
		this.actionTypeView = actionTypeView;
	}

	public String getParamView() {
		return paramView;
	}

	public void setParamView(String paramView) {
		this.paramView = paramView;
	}

	public void setContractPolicyManager(ContractPolicyManager contractPolicyManager) {
		this.contractPolicyManager = contractPolicyManager;
	}

	public ContractPolicy getContractPolicy() {
		return contractPolicy;
	}

	public void setContractPolicy(ContractPolicy contractPolicy) {
		this.contractPolicy = contractPolicy;
	}

	public String getSessionUserName() {
		return sessionUserName;
	}

	public void setSessionUserName(String sessionUserName) {
		this.sessionUserName = sessionUserName;
	}

	public List getPartnerAgentList() {
		return partnerAgentList;
	}

	public void setPartnerAgentList(List partnerAgentList) {
		this.partnerAgentList = partnerAgentList;
	}

	public String getCompDivision() {
		return compDivision;
	}

	public void setCompDivision(String compDivision) {
		this.compDivision = compDivision;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the findAcctRefNumList
	 */
	public List getFindAcctRefNumList() {
		return findAcctRefNumList;
	}

	/**
	 * @param findAcctRefNumList the findAcctRefNumList to set
	 */
	public void setFindAcctRefNumList(List findAcctRefNumList) {
		this.findAcctRefNumList = findAcctRefNumList;
	}

	
	

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getFile1() {
		return file1;
	}

	public void setFile1(File file1) {
		this.file1 = file1;
	}

	public File getFile2() {
		return file2;
	}

	public void setFile2(File file2) {
		this.file2 = file2;
	}

	public File getFile3() {
		return file3;
	}

	public void setFile3(File file3) {
		this.file3 = file3;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	

	

	public String getFile1FileName() {
		return file1FileName;
	}

	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	public String getFile2FileName() {
		return file2FileName;
	}

	public void setFile2FileName(String file2FileName) {
		this.file2FileName = file2FileName;
	}

	public String getFile3FileName() {
		return file3FileName;
	}

	public void setFile3FileName(String file3FileName) {
		this.file3FileName = file3FileName;
	}

	/**
	 * @return the findPartnerProfileList
	 */
	public List getFindPartnerProfileList() {
		return findPartnerProfileList;
	}

	/**
	 * @param findPartnerProfileList the findPartnerProfileList to set
	 */
	public void setFindPartnerProfileList(List findPartnerProfileList) {
		this.findPartnerProfileList = findPartnerProfileList;
	}

	

	public List getMultiplequalityCertifications() {
		return multiplequalityCertifications;
	}

	public void setMultiplequalityCertifications(List multiplequalityCertifications) {
		this.multiplequalityCertifications = multiplequalityCertifications;
	}

	public Map<String, String> getFacilitySize() {
		return facilitySize;
	}

	public void setFacilitySize(Map<String, String> facilitySize) {
		this.facilitySize = facilitySize;
	}

	public Map<String, String> getQualityCertifications() {
		return qualityCertifications;
	}

	public void setQualityCertifications(Map<String, String> qualityCertifications) {
		this.qualityCertifications = qualityCertifications;
	}

	public Map<String, String> getServiceLines() {
		return serviceLines;
	}

	public void setServiceLines(Map<String, String> serviceLines) {
		this.serviceLines = serviceLines;
	}

	public Map<String, String> getVanlineAffiliations() {
		return vanlineAffiliations;
	}

	public void setVanlineAffiliations(Map<String, String> vanlineAffiliations) {
		this.vanlineAffiliations = vanlineAffiliations;
	}

	public List getMultiplServiceLines() {
		return multiplServiceLines;
	}

	public void setMultiplServiceLines(List multiplServiceLines) {
		this.multiplServiceLines = multiplServiceLines;
	}

	public List getMultiplVanlineAffiliations() {
		return multiplVanlineAffiliations;
	}

	public void setMultiplVanlineAffiliations(List multiplVanlineAffiliations) {
		this.multiplVanlineAffiliations = multiplVanlineAffiliations;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public List getUserRoleSet() {
		return userRoleSet;
	}

	public void setUserRoleSet(List userRoleSet) {
		this.userRoleSet = userRoleSet;
	}

	public List getPartnerRating() {
		return partnerRating;
	}

	public void setPartnerRating(List partnerRating) {
		this.partnerRating = partnerRating;
	}

	public BigDecimal getRating() {
		return rating;
	}

	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

	public List getRatingFeedback() {
		return ratingFeedback;
	}

	public void setRatingFeedback(List ratingFeedback) {
		this.ratingFeedback = ratingFeedback;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(String ratingValue) {
		this.ratingValue = ratingValue;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public List getPartnerListNew() {
		return partnerListNew;
	}

	public void setPartnerListNew(List partnerListNew) {
		this.partnerListNew = partnerListNew;
	}

	public String getExList() {
		return exList;
	}

	public void setExList(String exList) {
		this.exList = exList;
	}

	public String getVanlineCode() {
		return vanlineCode;
	}

	public void setVanlineCode(String vanlineCode) {
		this.vanlineCode = vanlineCode;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public Boolean getIsIgnoreInactive() {
		return isIgnoreInactive;
	}

	public void setIsIgnoreInactive(Boolean isIgnoreInactive) {
		this.isIgnoreInactive = isIgnoreInactive;
	}

	public List getPartnerRatingOneYr() {
		return partnerRatingOneYr;
	}

	public void setPartnerRatingOneYr(List partnerRatingOneYr) {
		this.partnerRatingOneYr = partnerRatingOneYr;
	}

	public List getPartnerRatingSixMonths() {
		return partnerRatingSixMonths;
	}

	public void setPartnerRatingSixMonths(List partnerRatingSixMonths) {
		this.partnerRatingSixMonths = partnerRatingSixMonths;
	}

	public List getPartnerRatingTwoYr() {
		return partnerRatingTwoYr;
	}

	public void setPartnerRatingTwoYr(List partnerRatingTwoYr) {
		this.partnerRatingTwoYr = partnerRatingTwoYr;
	}

	public BigDecimal getRating2y() {
		return rating2y;
	}

	public void setRating2y(BigDecimal rating2y) {
		this.rating2y = rating2y;
	}

	public BigDecimal getRating6m() {
		return rating6m;
	}

	public void setRating6m(BigDecimal rating6m) {
		this.rating6m = rating6m;
	}

	public Map<String, String> getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(Map<String, String> coordinator) {
		this.coordinator = coordinator;
	}

	public BigDecimal getRating2yACC() {
		return rating2yACC;
	}

	public void setRating2yACC(BigDecimal rating2yACC) {
		this.rating2yACC = rating2yACC;
	}

	public BigDecimal getRating6mACC() {
		return rating6mACC;
	}

	public void setRating6mACC(BigDecimal rating6mACC) {
		this.rating6mACC = rating6mACC;
	}

	public BigDecimal getRatingACC() {
		return ratingACC;
	}

	public void setRatingACC(BigDecimal ratingACC) {
		this.ratingACC = ratingACC;
	}

   public String getCustomerVendor() {
		return customerVendor;
	}

	public void setCustomerVendor(String customerVendor) {
		this.customerVendor = customerVendor;
	} 
	public String getJobRelo() {
		return jobRelo;
	}

	public void setJobRelo(String jobRelo) {
		this.jobRelo = jobRelo;
	}

	/**
	 * @return the partnerCodeRecord
	 */
	public List getPartnerCodeRecord() {
		return partnerCodeRecord;
	}

	/**
	 * @param partnerCodeRecord the partnerCodeRecord to set
	 */
	public void setPartnerCodeRecord(List partnerCodeRecord) {
		this.partnerCodeRecord = partnerCodeRecord;
	}

	/**
	 * @return the vlCode
	 */
	public String getVlCode() {
		return vlCode;
	}

	/**
	 * @param vlCode the vlCode to set
	 */
	public void setVlCode(String vlCode) {
		this.vlCode = vlCode;
	}

	/**
	 * @return the partnerRecord
	 */
	public String getPartnerRecord() {
		return partnerRecord;
	}

	/**
	 * @param partnerRecord the partnerRecord to set
	 */
	public void setPartnerRecord(String partnerRecord) {
		this.partnerRecord = partnerRecord;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGeoAddress() {
		return geoAddress;
	}

	public void setGeoAddress(String geoAddress) {
		this.geoAddress = geoAddress;
	}

	public String getGeoCountry() {
		return geoCountry;
	}

	public void setGeoCountry(String geoCountry) {
		this.geoCountry = geoCountry;
	}

	public String getGeoState() {
		return geoState;
	}

	public void setGeoState(String geoState) {
		this.geoState = geoState;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public List getPartnersMapList() {
		return partnersMapList;
	}

	public void setPartnersMapList(List partnersMapList) {
		this.partnersMapList = partnersMapList;
	}

	public Boolean getIsAmsaNo() {
		return isAmsaNo;
	}

	public void setIsAmsaNo(Boolean isAmsaNo) {
		this.isAmsaNo = isAmsaNo;
	}

	public Boolean getIsFidi() {
		return isFidi;
	}

	public void setIsFidi(Boolean isFidi) {
		this.isFidi = isFidi;
	}

	public Boolean getIsIamNo() {
		return isIamNo;
	}

	public void setIsIamNo(Boolean isIamNo) {
		this.isIamNo = isIamNo;
	}

	public Boolean getIsOmniNo() {
		return isOmniNo;
	}

	public void setIsOmniNo(Boolean isOmniNo) {
		this.isOmniNo = isOmniNo;
	}

	public Boolean getIsUts() {
		return isUts;
	}

	public void setIsUts(Boolean isUts) {
		this.isUts = isUts;
	}

	public Boolean getIsWercNo() {
		return isWercNo;
	}

	public void setIsWercNo(Boolean isWercNo) {
		this.isWercNo = isWercNo;
	}

	public String getPartnerOption() {
		return partnerOption;
	}

	public void setPartnerOption(String partnerOption) {
		this.partnerOption = partnerOption;
	}

	public List getDLocationList() {
		return dLocationList;
	}

	public void setDLocationList(List locationList) {
		dLocationList = locationList;
	}

	public List getDriverLocList() {
		return driverLocList;
	}

	public void setDriverLocList(List driverLocList) {
		this.driverLocList = driverLocList;
	}

	public String getCreditTerm() {
		return creditTerm;
	}

	public void setCreditTerm(String creditTerm) {
		this.creditTerm = creditTerm;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public List getDriverInvolvementList() {
		return driverInvolvementList;
	}

	public void setDriverInvolvementList(List driverInvolvementList) {
		this.driverInvolvementList = driverInvolvementList;
	}

	public List getDriverInvolvementDtoList() {
		return driverInvolvementDtoList;
	}

	public void setDriverInvolvementDtoList(List driverInvolvementDtoList) {
		this.driverInvolvementDtoList = driverInvolvementDtoList;
	}

	public List getPrefixList() {
		return prefixList;
	}

	public void setPrefixList(List prefixList) {
		this.prefixList = prefixList;
	}

	public String getPartnerPrefix() {
		return partnerPrefix;
	}

	public void setPartnerPrefix(String partnerPrefix) {
		this.partnerPrefix = partnerPrefix;
	}

	public String getVLocation() {
		return vLocation;
	}

	public void setVLocation(String location) {
		vLocation = location;
	}

	public String getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverCell() {
		return driverCell;
	}

	public void setDriverCell(String driverCell) {
		this.driverCell = driverCell;
	}
	private List trackingInfoListByShipNumber;
	private String sequenceNumber;
	@SkipValidation
    public String trackingInfoByShipNumber(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
	trackingInfoListByShipNumber = partnerManager.trackingInfoByShipNumber(sessionCorpID,sequenceNumber);	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    return SUCCESS;
    }
	
	@SkipValidation
	public String vendorNameAutocompleteAjaxlist() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partners = new HashSet(partnerManager.vendorNameAutocompleteAjaxlist(partnerNameAutoCopmlete));
		partnerType="VN";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		// System.out.println(partners);
		return SUCCESS;
	}
	
	

	public List getTrackingInfoListByShipNumber() {
		return trackingInfoListByShipNumber;
	}

	public void setTrackingInfoListByShipNumber(List trackingInfoListByShipNumber) {
		this.trackingInfoListByShipNumber = trackingInfoListByShipNumber;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public List getNextDriverLocationList() {
		return nextDriverLocationList;
	}

	public void setNextDriverLocationList(List nextDriverLocationList) {
		this.nextDriverLocationList = nextDriverLocationList;
	}

	public String getNextDriverLocationValue() {
		return nextDriverLocationValue;
	}

	public void setNextDriverLocationValue(String nextDriverLocationValue) {
		this.nextDriverLocationValue = nextDriverLocationValue;
	}

	public Map<String, String> getDriverDropDownList() {
		return driverDropDownList;
	}

	public String getDriverVanId() {
		return driverVanId;
	}

	public void setDriverVanId(String driverVanId) {
		this.driverVanId = driverVanId;
	}

	public Map<String, String> getDriverTypeDropDownList() {
		return driverTypeDropDownList;
	}

	public void setDriverTypeDropDownList(Map<String, String> driverTypeDropDownList) {
		this.driverTypeDropDownList = driverTypeDropDownList;
	}

	public Map<String, String> getDriverTypeMap() {
		return driverTypeMap;
	}

	public void setDriverTypeMap(Map<String, String> driverTypeMap) {
		this.driverTypeMap = driverTypeMap;
	}

	public List getTotalAmmountList() {
		return totalAmmountList;
	}

	public void setTotalAmmountList(List totalAmmountList) {
		this.totalAmmountList = totalAmmountList;
	}

	public String getTotalAmmount() {
		return totalAmmount;
	}

	public void setTotalAmmount(String totalAmmount) {
		this.totalAmmount = totalAmmount;
	}

	public String getSentMailFlag() {
		return sentMailFlag;
	}

	public void setSentMailFlag(String sentMailFlag) {
		this.sentMailFlag = sentMailFlag;
	}

	public String getMailStatus() {
		return mailStatus;
	}

	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}

	public String getMailFailure() {
		return mailFailure;
	}

	public void setMailFailure(String mailFailure) {
		this.mailFailure = mailFailure;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getConfirmPasswordNew() {
		return confirmPasswordNew;
	}

	public void setConfirmPasswordNew(String confirmPasswordNew) {
		this.confirmPasswordNew = confirmPasswordNew;
	}

	public List getGender() {
		return gender;
	}

	public void setGender(List gender) {
		this.gender = gender;
	}

	public List getBasedAtNameList() {
		return basedAtNameList;
	}

	public void setBasedAtNameList(List basedAtNameList) {
		this.basedAtNameList = basedAtNameList;
	}

	public String getBasedAtCode() {
		return basedAtCode;
	}

	public void setBasedAtCode(String basedAtCode) {
		this.basedAtCode = basedAtCode;
	}

	public String getCheckOption() {
		return checkOption;
	}

	public void setCheckOption(String checkOption) {
		this.checkOption = checkOption;
	}
	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}
	public EntitlementManager getEntitlementManager() {
		return entitlementManager;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}


	public void setEntitlementManager(EntitlementManager entitlementManager) {
		this.entitlementManager = entitlementManager;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getVanlineCodeSearch() {
		return vanlineCodeSearch;
	}

	public void setVanlineCodeSearch(String vanlineCodeSearch) {
		this.vanlineCodeSearch = vanlineCodeSearch;
	}

	public List getJobFunction() {
		return JobFunction;
	}

	public void setJobFunction(List jobFunction) {
		JobFunction = jobFunction;
	}

	public String getUserJobType() {
		return userJobType;
	}

	public void setUserJobType(String userJobType) {
		this.userJobType = userJobType;
	}


	public Map<String, String> getUserJobTypeMap() {
		return userJobTypeMap;
	}

	public void setUserJobTypeMap(Map<String, String> userJobTypeMap) {
		this.userJobTypeMap = userJobTypeMap;
	}

	public String getRedSkyAgentPartner() {
		return RedSkyAgentPartner;
	}

	public void setRedSkyAgentPartner(String redSkyAgentPartner) {
		RedSkyAgentPartner = redSkyAgentPartner;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	public String getAgentContact() {
		return agentContact;
	}

	public void setAgentContact(String agentContact) {
		this.agentContact = agentContact;
	}

	public String getUserRolesValue() {
		return userRolesValue;
	}

	public void setUserRolesValue(String userRolesValue) {
		this.userRolesValue = userRolesValue;
	}

	public List getAvailRoles() {
		return availRoles;
	}

	public void setAvailRoles(List availRoles) {
		this.availRoles = availRoles;
	}
	public String getJobReloName() {
		return jobReloName;
	}

	public void setJobReloName(String jobReloName) {
		this.jobReloName = jobReloName;
	}

	public String getOrgAddress() {
		return orgAddress;
	}

	public void setOrgAddress(String orgAddress) {
		this.orgAddress = orgAddress;
	}

	public String getDestAddress() {
		return destAddress;
	}

	public void setDestAddress(String destAddress) {
		this.destAddress = destAddress;
	}

	public List getAvailPermissions() {
		return availPermissions;
	}

	public void setAvailPermissions(List availPermissions) {
		this.availPermissions = availPermissions;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public List getPartnerReloList() {
		return partnerReloList;
	}

	public void setPartnerReloList(List partnerReloList) {
		this.partnerReloList = partnerReloList;
	}

	public Boolean getCmmDmmFlag() {
		return cmmDmmFlag;
	}

	public void setCmmDmmFlag(Boolean cmmDmmFlag) {
		this.cmmDmmFlag = cmmDmmFlag;
	}


	public String getSearchListOfVendorCode() {
		return searchListOfVendorCode;
	}

	public void setSearchListOfVendorCode(String searchListOfVendorCode) {
		this.searchListOfVendorCode = searchListOfVendorCode;
	}

	public Map<String, String> getListOfVendorCode() {
		return listOfVendorCode;
	}

	public void setListOfVendorCode(Map<String, String> listOfVendorCode) {
		this.listOfVendorCode = listOfVendorCode;
	}

	public String getValidPartnerCode() {
		return validPartnerCode;
	}

	public void setValidPartnerCode(String validPartnerCode) {
		this.validPartnerCode = validPartnerCode;
	}

	public String getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

	public String getPartnerNameAutoCopmlete() {
		return partnerNameAutoCopmlete;
	}

	public void setPartnerNameAutoCopmlete(String partnerNameAutoCopmlete) {
		this.partnerNameAutoCopmlete = partnerNameAutoCopmlete;
	}

	public String getPartnerNameType() {
		return partnerNameType;
	}

	public void setPartnerNameType(String partnerNameType) {
		this.partnerNameType = partnerNameType;
	}

	public List getPartnerDetailsAutoComplete() {
		return partnerDetailsAutoComplete;
	}

	public void setPartnerDetailsAutoComplete(List partnerDetailsAutoComplete) {
		this.partnerDetailsAutoComplete = partnerDetailsAutoComplete;
	}

	public String getPartnerDetailsAutoCompleteGsonData() {
		return partnerDetailsAutoCompleteGsonData;
	}

	public void setPartnerDetailsAutoCompleteGsonData(
			String partnerDetailsAutoCompleteGsonData) {
		this.partnerDetailsAutoCompleteGsonData = partnerDetailsAutoCompleteGsonData;
	}

	public String getAutocompleteFormName() {
		return autocompleteFormName;
	}

	public void setAutocompleteFormName(String autocompleteFormName) {
		this.autocompleteFormName = autocompleteFormName;
	}

	public String getAutocompleteFieldCode() {
		return autocompleteFieldCode;
	}

	public void setAutocompleteFieldCode(String autocompleteFieldCode) {
		this.autocompleteFieldCode = autocompleteFieldCode;
	}

	public String getAutocompleteFieldName() {
		return autocompleteFieldName;
	}

	public void setAutocompleteFieldName(String autocompleteFieldName) {
		this.autocompleteFieldName = autocompleteFieldName;
	}

	public String getAutocompleteDivId() {
		return autocompleteDivId;
	}

	public void setAutocompleteDivId(String autocompleteDivId) {
		this.autocompleteDivId = autocompleteDivId;
	}

	public String getAutocompleteCondition() {
		return autocompleteCondition;
	}

	public void setAutocompleteCondition(String autocompleteCondition) {
		this.autocompleteCondition = autocompleteCondition;
	}

	public String getPartnerNameId() {
		return partnerNameId;
	}

	public void setPartnerNameId(String partnerNameId) {
		this.partnerNameId = partnerNameId;
	}

	public String getPaertnerCodeId() {
		return paertnerCodeId;
	}

	public void setPaertnerCodeId(String paertnerCodeId) {
		this.paertnerCodeId = paertnerCodeId;
	}

	public String getPricingAccountLineId() {
		return pricingAccountLineId;
	}

	public void setPricingAccountLineId(String pricingAccountLineId) {
		this.pricingAccountLineId = pricingAccountLineId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getSubOrigin() {
		return subOrigin;
	}

	public void setSubOrigin(String subOrigin) {
		this.subOrigin = subOrigin;
	}

	public String getSubDestin() {
		return subDestin;
	}

	public void setSubDestin(String subDestin) {
		this.subDestin = subDestin;
	}

	public int getDefaultContactPersonCount() {
		return defaultContactPersonCount;
	}

	public void setDefaultContactPersonCount(int defaultContactPersonCount) {
		this.defaultContactPersonCount = defaultContactPersonCount;
	}

	public List getPartnerClassifiedList() {
		return partnerClassifiedList;
	}

	public void setPartnerClassifiedList(List partnerClassifiedList) {
		this.partnerClassifiedList = partnerClassifiedList;
	}

	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}

	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}

	public void setPartnerAccountRefManager(
			PartnerAccountRefManager partnerAccountRefManager) {
		this.partnerAccountRefManager = partnerAccountRefManager;
	}

	public int getAgentSixMnthPos() {
		return agentSixMnthPos;
	}

	public void setAgentSixMnthPos(int agentSixMnthPos) {
		this.agentSixMnthPos = agentSixMnthPos;
	}

	public int getAgentSixMnthNeg() {
		return agentSixMnthNeg;
	}

	public void setAgentSixMnthNeg(int agentSixMnthNeg) {
		this.agentSixMnthNeg = agentSixMnthNeg;
	}

	public int getAgentOneYrPos() {
		return agentOneYrPos;
	}

	public void setAgentOneYrPos(int agentOneYrPos) {
		this.agentOneYrPos = agentOneYrPos;
	}

	public int getAgentOneYrNeg() {
		return agentOneYrNeg;
	}

	public void setAgentOneYrNeg(int agentOneYrNeg) {
		this.agentOneYrNeg = agentOneYrNeg;
	}

	public int getAgentTwoYrPos() {
		return agentTwoYrPos;
	}

	public void setAgentTwoYrPos(int agentTwoYrPos) {
		this.agentTwoYrPos = agentTwoYrPos;
	}

	public int getAgentTwoYrNeg() {
		return agentTwoYrNeg;
	}

	public void setAgentTwoYrNeg(int agentTwoYrNeg) {
		this.agentTwoYrNeg = agentTwoYrNeg;
	}

	public BigDecimal getAgentSixMnthAvg() {
		return agentSixMnthAvg;
	}

	public void setAgentSixMnthAvg(BigDecimal agentSixMnthAvg) {
		this.agentSixMnthAvg = agentSixMnthAvg;
	}

	public BigDecimal getAgentOneYrAvg() {
		return agentOneYrAvg;
	}

	public void setAgentOneYrAvg(BigDecimal agentOneYrAvg) {
		this.agentOneYrAvg = agentOneYrAvg;
	}

	public BigDecimal getAgentTwoYrAvg() {
		return agentTwoYrAvg;
	}

	public void setAgentTwoYrAvg(BigDecimal agentTwoYrAvg) {
		this.agentTwoYrAvg = agentTwoYrAvg;
	}

	public boolean isDisableUserContract() {
		return disableUserContract;
	}

	public void setDisableUserContract(boolean disableUserContract) {
		this.disableUserContract = disableUserContract;
	}

	public Boolean getAgentSearchValidation() {
		return agentSearchValidation;
	}

	public void setAgentSearchValidation(Boolean agentSearchValidation) {
		this.agentSearchValidation = agentSearchValidation;
	}

	public Boolean getAccountSearchValidation() {
		return accountSearchValidation;
	}

	public void setAccountSearchValidation(Boolean accountSearchValidation) {
		this.accountSearchValidation = accountSearchValidation;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


}
