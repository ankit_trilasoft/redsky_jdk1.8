package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ContractPolicyDao;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.service.ContractPolicyManager;

public class ContractPolicyManagerImpl extends GenericManagerImpl<ContractPolicy, Long> implements ContractPolicyManager {   
	ContractPolicyDao contractPolicyDao;
	
	public ContractPolicyManagerImpl(ContractPolicyDao contractPolicyDao) {   
        super(contractPolicyDao);   
        this.contractPolicyDao = contractPolicyDao;   
    }   
	
	public List checkById(String code){
		return contractPolicyDao.checkById(code); 
	}

	public List getContractPolicy(String code, String sessionCorpID) {
		return contractPolicyDao.getContractPolicy(code, sessionCorpID);
	}
	public List findPolicyFileNewList(String code, String sessionCorpID) {
		return contractPolicyDao.findPolicyFileNewList(code, sessionCorpID);
	}
	public List findPolicyFileList(String code, String sessionCorpID) {
		return contractPolicyDao.findPolicyFileList(code, sessionCorpID);
	}
	public List findDocumentList(String code, String sectionName ,String language , String sessionCorpID) {
		return contractPolicyDao.findDocumentList(code,sectionName,language, sessionCorpID);
	}
	public List getPolicyReference(String partnerCode, String corpID,Long parentId){
		return contractPolicyDao.getPolicyReference(partnerCode, corpID,parentId);
	}
	public int checkSectionName(String sectionName ,String Language,String code, String sessionCorpID){
		return contractPolicyDao.checkSectionName(sectionName,Language,code,sessionCorpID);
	}
	public List getPolicyDocReference(String partnerCode, String corpID,Long parentId,String sectionName,String language){
		return contractPolicyDao.getPolicyDocReference(partnerCode, corpID, parentId, sectionName, language);
	}
	public int updatePolicyDocument(String dummsectionName,String dummlanguage,String code, String sectionName ,String language ,String sessionCorpID){
		return contractPolicyDao.updatePolicyDocument( dummsectionName, dummlanguage,code,sectionName ,language , sessionCorpID);
	}
	public int deletedDocument(String documentSectionName,String documentLanguage,String partnerCode,String sessionCorpID){
		return contractPolicyDao.deletedDocument(documentSectionName,documentLanguage,partnerCode,sessionCorpID);
	}
	public List getContractPolicyRef(String partnerCode,String sessionCorpID){
		return contractPolicyDao.getContractPolicyRef(partnerCode, sessionCorpID);
	}
	public List policyFilesList(String sessionCorpID){
		return contractPolicyDao.policyFilesList(sessionCorpID);
	}
	public int sectionNameCheck(String sectionName ,String Language, String sessionCorpID){
		return contractPolicyDao.sectionNameCheck(sectionName,Language,sessionCorpID);
		
	}
	public List documentFileList( String sectionName ,String language,String sessionCorpID){
		
		return contractPolicyDao.documentFileList(sectionName,language, sessionCorpID);
		
	}
	public void deleteagentParentPolicyFile(String oldAgentParent,String partnerCode,String sessionCorpID){
		contractPolicyDao.deleteagentParentPolicyFile(oldAgentParent,partnerCode, sessionCorpID);
	}
	public void deleteChildDocument(Long id2, String sessionCorpID){
		contractPolicyDao.deleteChildDocument(id2, sessionCorpID);
	}
	public void deleteChildDocumentFromPolicy(Long id2, String sessionCorpID){
		contractPolicyDao.deleteChildDocumentFromPolicy(id2, sessionCorpID);
	}
	public int checkSequenceNumber(Long sequenceNumber ,String Language,String code, String sessionCorpID){
		return contractPolicyDao.checkSequenceNumber(sequenceNumber, Language, code, sessionCorpID);
	}
	public int deletedPolicyContract(String partnerCode,String sessionCorpID){
		return contractPolicyDao.deletedPolicyContract(partnerCode, sessionCorpID);
	}
	public String findByBookingLastName(String partnerCode, String corpid) {
		return contractPolicyDao.findByBookingLastName(partnerCode, corpid);
	
	}
}
