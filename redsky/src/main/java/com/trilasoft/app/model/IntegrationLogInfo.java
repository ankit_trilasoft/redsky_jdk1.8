package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="integrationloginfo")

public class IntegrationLogInfo extends BaseObject{
	
	private Long id;
	private String source;
	private String destination;
	private String integrationId;
	private String sourceOrderNum;
	private String destOrderNum;
	private String operation;
	private String orderComplete;
	private String statusCode;
	private Date effectiveDate;
	private String detailedStatus;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String ugwwOA;
	private String ugwwDA;
	private String ugwwBA;
	private String corpID;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("source", source).append("destination", destination)
				.append("integrationId", integrationId)
				.append("sourceOrderNum", sourceOrderNum)
				.append("destOrderNum", destOrderNum)
				.append("operation", operation)
				.append("orderComplete", orderComplete)
				.append("statusCode", statusCode)
				.append("effectiveDate", effectiveDate)
				.append("detailedStatus", detailedStatus)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("ugwwOA", ugwwOA).append("ugwwDA", ugwwDA)
				.append("ugwwBA", ugwwBA).append("corpID", corpID).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof IntegrationLogInfo))
			return false;
		IntegrationLogInfo castOther = (IntegrationLogInfo) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(source, castOther.source)
				.append(destination, castOther.destination)
				.append(integrationId, castOther.integrationId)
				.append(sourceOrderNum, castOther.sourceOrderNum)
				.append(destOrderNum, castOther.destOrderNum)
				.append(operation, castOther.operation)
				.append(orderComplete, castOther.orderComplete)
				.append(statusCode, castOther.statusCode)
				.append(effectiveDate, castOther.effectiveDate)
				.append(detailedStatus, castOther.detailedStatus)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(ugwwOA, castOther.ugwwOA)
				.append(ugwwDA, castOther.ugwwDA)
				.append(ugwwBA, castOther.ugwwBA)
				.append(corpID, castOther.corpID).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(source)
				.append(destination).append(integrationId)
				.append(sourceOrderNum).append(destOrderNum).append(operation)
				.append(orderComplete).append(statusCode).append(effectiveDate)
				.append(detailedStatus).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(ugwwOA)
				.append(ugwwDA).append(ugwwBA).append(corpID).toHashCode();
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@Column
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	@Column
	public String getIntegrationId() {
		return integrationId;
	}
	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}
	@Column
	public String getSourceOrderNum() {
		return sourceOrderNum;
	}
	public void setSourceOrderNum(String sourceOrderNum) {
		this.sourceOrderNum = sourceOrderNum;
	}
	@Column
	public String getDestOrderNum() {
		return destOrderNum;
	}
	public void setDestOrderNum(String destOrderNum) {
		this.destOrderNum = destOrderNum;
	}
	@Column
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	@Column
	public String getOrderComplete() {
		return orderComplete;
	}
	public void setOrderComplete(String orderComplete) {
		this.orderComplete = orderComplete;
	}
	@Column
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	@Column
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	@Column
	public String getDetailedStatus() {
		return detailedStatus;
	}
	public void setDetailedStatus(String detailedStatus) {
		this.detailedStatus = detailedStatus;
	}
	@Column
	public String getUgwwOA() {
		return ugwwOA;
	}
	public void setUgwwOA(String ugwwOA) {
		this.ugwwOA = ugwwOA;
	}
	@Column
	public String getUgwwDA() {
		return ugwwDA;
	}
	public void setUgwwDA(String ugwwDA) {
		this.ugwwDA = ugwwDA;
	}
	@Column
	public String getUgwwBA() {
		return ugwwBA;
	}
	public void setUgwwBA(String ugwwBA) {
		this.ugwwBA = ugwwBA;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
}
