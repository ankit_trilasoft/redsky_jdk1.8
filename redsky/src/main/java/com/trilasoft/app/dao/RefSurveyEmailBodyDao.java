package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.RefSurveyEmailBody;

public interface RefSurveyEmailBodyDao extends GenericDao<RefSurveyEmailBody, Long>{

}
