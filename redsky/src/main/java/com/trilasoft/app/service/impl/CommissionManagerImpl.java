package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;



import com.trilasoft.app.dao.CommissionDao;
import com.trilasoft.app.model.Commission;
import com.trilasoft.app.service.CommissionManager;

public class CommissionManagerImpl extends GenericManagerImpl<Commission, Long>  implements CommissionManager{
	CommissionDao  commissionDao;
	public CommissionManagerImpl(CommissionDao  commissionDao){
		super(commissionDao);   
        this.commissionDao = commissionDao;
	}
	public Map<Long, Double> getAutoCommisssionMap(String sessionCorpID,String shipNumber) { 
		return commissionDao.getAutoCommisssionMap(sessionCorpID, shipNumber);
	}
	public BigDecimal findTotalExpenseOfChargeACC(String sessionCorpID, String shipNumber, String tempAccId) {
		return commissionDao.findTotalExpenseOfChargeACC(sessionCorpID, shipNumber, tempAccId);
	}
}
