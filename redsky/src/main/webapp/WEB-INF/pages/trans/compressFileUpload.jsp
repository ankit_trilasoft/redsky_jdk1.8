<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/scripts/login.js"%>
<head>
<title><fmt:message key="upload.title" /></title>
<meta name="heading" content="<fmt:message key='upload.heading'/>" />
<c:if test="${param.popup}">
	<link rel="stylesheet" type="text/css" media="all"
		href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" />
</c:if>
<style type="text/css">


/* collapse */


</style>

</head>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
<c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>"/>

<s:form action="compressFileUpload.html?id=${fileId}" enctype="multipart/form-data" method="post" onsubmit="return submit_form()" validate="true" id="uploadForm" name="uploadForm">

<s:hidden name="myFile.fileId"/>
<s:hidden name="myFile.fileSize"/>
<s:hidden name="myFile.isSecure"/>
<s:hidden name="myFile.coverPageStripped" value="true"/>
<s:hidden name="myFile.customerNumber"/>
<s:hidden name="myFileFor" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="forQuotation" name="forQuotation" />


<c:set var="fid" value="${myFile.id}"/>
<s:hidden name="fid" value="${myFile.id}"/>

<c:set var="description" value="${myFile.description}"/>

<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>"/>




<s:hidden name="customerFile.sequenceNumber" />



<div id="Layer1" style="width: 100%">
	<div id="Layer4">
		<div id="newmnav">
			<ul>
			
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Upload<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			</ul>
		</div>	
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
	</div>

	<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" width="">
		<tbody>
		<tr>
			<td colspan="5" width="90px">
				<div class="subcontent-tab" > Upload A Document</div>
			</td>
		</tr>
		
			<tr>
				<td width="" valign="top">
					<table cellpadding="1" cellspacing="1" border="0" style="margin:0px;width:600px;">
						<tr>
							<td align="right" class="listwhitetext" style="width:84px; !width:84px" height="30px">Document Type</td>
							<td width="100px"><s:textfield name="fileType" cssClass="text medium"  value="${myFile.fileType}"   cssStyle="width:245px"/></td>
							<td align="left" class="listwhitetext" width="">Map&nbsp;&nbsp;<s:textfield name="mapFolder" cssClass="text medium" value="${myFile.mapFolder}"   cssStyle="width:73px"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" height="70px">Description</td>
							<td colspan="3" valign="top" align="left"><s:textarea cssClass="textarea"  name="myFile.description" value="${description}"  rows="4" cols="63" /></td>
						</tr>
						<tr>
							<td class="listwhitetext" style="width:80px" align="right"><fmt:message key='myFile.file' /></td>
							<td><s:file name="file" label="%{getText('uploadForm.file')}" required="true" size="25" /></td>
								</tr>
						<tr>
		             <td></td>
						<td style="padding-top:12px;"><s:submit key="button.upload" name="myFile.upload" method="compressFileUpload" cssClass="cssbutton" cssStyle="margin-right:5px;margin-bottom:5px; height:25px; width:90px; padding:2px 0px 2px; font-size: 15"  /></td>
					</tr>
					</table>
				</td>
			
	
		</tbody>
	</table>

	<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.createdOn' /></b></td>
				<td style="width:130px"><fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${myFile.createdOn}" pattern="${displayDateTimeEditFormat}" />
					<s:hidden name="myFile.createdOn" value="${customerFileCreatedOnFormattedValue}" /> 
					<fmt:formatDate value="${myFile.createdOn}" pattern="${displayDateTimeFormat}" />
				</td>
				<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message key='billing.createdBy' /></b></td>
				<c:if test="${not empty myFile.id}">
					<s:hidden name="myFile.createdBy" />
					<td style="width:65px"><s:label name="createdBy" value="%{myFile.createdBy}" /></td>
				</c:if>
				<c:if test="${empty myFile.id}">
					<s:hidden name="myFile.createdBy" value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>				
			
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedOn' /></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${myFile.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="myFile.updatedOn" value="${customerFileupdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate value="${myFile.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedBy' /></b></td>
				<c:if test="${not empty myFile.id}">
					<s:hidden name="myFile.updatedBy"/>
					<td style="width:85px"><s:label name="updatedBy" value="%{myFile.updatedBy}"/></td>
				</c:if>
				<c:if test="${empty myFile.id}">
					<s:hidden name="myFile.updatedBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
			</tr>
		</tbody>
	</table>

</div>
<s:hidden name="hitflag" />
<s:hidden name="secure" value="${secure}"/>
</s:form>

<script type="text/javascript">
function winLoad(){
	parent.window.opener.document.location.reload(); 
	//window.opener.getSubmitParent();
	window.close();
}
</script>
<script type="text/javascript">
try{
	<c:if test="${hitflag=='1'}">
	winLoad();
	</c:if>
}catch(e){
	
}
</script>
