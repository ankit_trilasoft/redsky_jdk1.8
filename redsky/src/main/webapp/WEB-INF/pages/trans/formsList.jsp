<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
   
   <c:choose>
	<c:when test="${marked == 'yes'}">
		 <title><fmt:message key="bookMarkformList.title"/></title> 
    	<meta name="heading" content="<fmt:message key='bookMarkformList.heading'/>"/>     
	</c:when>
	<c:otherwise>
		   <title><fmt:message key="formsList.title"/></title>
    	 <meta name="heading" content="<fmt:message key='formsList.heading'/>"/>         
	</c:otherwise>
</c:choose>
   

   
    <script language="javascript" type="text/javascript">
		function clear_fields(){
			document.forms['searchForm'].elements['reports.menu'].value = "";
			document.forms['searchForm'].elements['reports.module'].value = "";
			document.forms['searchForm'].elements['reports.subModule'].value = "";
			document.forms['searchForm'].elements['reports.description'].value = "";
		}
	
	function userStatusCheck(targetElement){
	
    	if(targetElement.checked){
      		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
      		
      		if(userCheckStatus == ''){
	  			document.forms['searchForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			var userCheckStatus=	document.forms['searchForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
}
	
	function bookMarkForms(){
		var reportId = document.forms['searchForm'].elements['userCheck'].value;
		if(reportId =='' || reportId ==','){
			alert('Please select the one or more form to bookmark.');
		}else{
			location.href = 'bookMarkedF.html?userCheck='+reportId;
		}
}
	
	function removeBookMarkForms(){
		var reportId = document.forms['searchForm'].elements['userCheck'].value;
		if(reportId =='' || reportId ==','){
			alert('Please select the one or more form to remove bookmark.');
		}else{
			location.href = 'removeBookMarkedF.html?userCheck='+reportId;
		}
}
	
	function checkAll(){
	document.forms['searchForm'].elements['userCheck'].value = "";
	document.forms['searchForm'].elements['userCheck'].value = "";
	var len = document.forms['searchForm'].elements['bookMark'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['bookMark'][i].checked = true ;
		userStatusCheck(document.forms['searchForm'].elements['bookMark'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['searchForm'].elements['bookMark'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['bookMark'][i].checked = false ;
		userStatusCheck(document.forms['searchForm'].elements['bookMark'][i]);
	}
	document.forms['searchForm'].elements['userCheck'].value="";
}

function show(theTable){

     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}
	</script>
	
	<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:0px;
margin-top:-18px;
!margin-top:-33px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}


</style>
	
	
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="searchForms" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set> 
 
<s:form id="searchForm" action="searchForms" method="post" validate="true">   
<s:hidden name="userCheck" value="%{userCheck}"/>
<s:hidden name="marked" value="${marked}"/>  
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
	<thead>
		<tr>
			<th align="center">Form Category</th>
			<th align="center"><fmt:message key="reports.module"/>
			<th align="center"><fmt:message key="reports.subModule"/>
			<th align="center">Form Name</th>	
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td width="" align="left"><s:textfield name="reports.menu" required="true" cssClass="input-text" size="30"/></td>
			<td width="" align="left"><s:textfield name="reports.module" required="true" cssClass="input-text" size="30"/></td>
			<td width="" align="left"><s:textfield name="reports.subModule" required="true" cssClass="input-text" size="30"/></td>
			<td width="" align="left"><s:textfield name="reports.description" required="true" cssClass="input-text" size="30"/></td>
		</tr>
		<tr>
		    <td colspan="3"></td>	
			<td width="" align="center" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" />
<s:set name="reportss" value="reportss" scope="request"/> 
<div id="newmnav" style="width: 500px;float: left;margin-top:-15px;">
	<ul>
		<c:if test="${marked != 'yes'}">
			<li><a href="bookMarkedListF.html"><span>BookMarked Form List</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Form List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>

</c:if>
 <c:if test="${marked == 'yes'}">
 			<li id="newmnav1" style="background:#FFF "><a class="current"><span>BookMarked Form List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	  		<li><a href="formList.html"><span>Form List</span></a></li>
		
		 </c:if>	
    <div id="chkAllButton"  class="listwhitetext" style="display:none;" >
				<input type="radio" style="vertical-align: sub;" name="chk" onClick="checkAll()" /><strong>Check All</strong>
				<input type="radio" style="vertical-align: sub;" name="chk" onClick="uncheckAll()" /><strong>Uncheck All</strong>
	</div> 	
</ul></div>
<div class="spn">&nbsp;</div>		
<display:table name="reportss" class="table" requestURI="" id="reportsList" defaultsort="2" pagesize="10" style="width:100%;">
 <c:choose>
		<c:when test='${marked == "yes"}'>
			<display:column title=""><input type="checkbox" style="margin-left:10px;" id="bookMark" name="bookMark"   value="${reportsList.bid}"  onclick="userStatusCheck(this)"/></display:column>
  		</c:when>
		<c:otherwise>
			<c:choose>
	 			<c:when test='${reportsList.id == reportsList.brid && reportsList.bkMarkcreatedBy == pageContext.request.remoteUser}'>
   					<display:column title=""><input type="checkbox" style="margin-left:10px;" id="bookMark" name="bookMark"   value="${reportsList.id}"  onclick="userStatusCheck(this)"  disabled="disabled" checked /></display:column>
   				</c:when>
	 			<c:otherwise>
   					<display:column title=""><input type="checkbox" style="margin-left:10px;" id="bookMark" name="bookMark"   value="${reportsList.id}"  onclick="userStatusCheck(this)"  /></display:column>
	 			</c:otherwise>
 			</c:choose>
		</c:otherwise>
	</c:choose>	
    <display:column sortable="true" sortProperty="menu" title="Form Category" style="width:160px"><a href="javascript:openWindow('viewFormParam.html?id=${reportsList.id}&docsxfer=${reportsList.docsxfer}&list=main&decorator=popup&popup=true',575,650)"><c:out value="${reportsList.menu}" /></a></display:column>
    <display:column property="module" sortable="true" titleKey="reports.module"/>
    <display:column property="subModule" sortable="true" titleKey="reports.subModule"/>
    <display:column property="description" sortable="true" title="Form Name"/>
    <display:column property="reportComment" sortable="true" title="Form Description" style="width:300px"/>
      
    <display:setProperty name="paging.banner.item_name" value="itemsJbkEquip"/> 
    <display:setProperty name="paging.banner.items_name" value="itemsJbkEquip"/>
    
    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/>
 </display:table>
<c:if test="${marked == null || marked != 'yes'}">
<input type="button" class="cssbutton" style="width:150px;" onclick="bookMarkForms();" value="Save As Bookmark"/>
</c:if>
<c:if test="${marked == 'yes'}">
<input type="button" class="cssbutton" style="width:150px;" onclick="removeBookMarkForms();" value="Remove Bookmark"/>
</c:if>
</s:form>
<script type="text/javascript"> 
    highlightTableRows("reportsList"); 
   try{
    var len = document.forms['searchForm'].elements['bookMark'].length;
	}
catch(e){}
   try{
    if(len>1){
    	show('chkAllButton');
    }
}
catch(e){}
</script> 