package com.trilasoft.app.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest; 
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CheckListDao;
import com.trilasoft.app.dao.ToDoResultDao;
import com.trilasoft.app.dao.ToDoRuleDao;
import com.trilasoft.app.dao.RuleActionsDao;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TaskCheckList;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.RuleActions;



import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.TaskCheckListManager;
import com.trilasoft.app.service.ToDoResultManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.RuleActionsManager;
import com.trilasoft.app.webapp.action.ToDoRuleAction;

public class RuleActionsManagerImpl extends GenericManagerImpl<RuleActions, Long> implements RuleActionsManager {	private RuleActionsDao ruleActionsDao; 
private List toDoRules;
private RuleActions ruleActions;


private RuleActionsManager ruleManager;

Date currentdate = new Date();
static final Logger logger = Logger.getLogger(ToDoRuleAction.class);

 protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }

public RuleActionsManagerImpl(RuleActionsDao ruleActionsDao) {   
    super(ruleActionsDao);   
    this.ruleActionsDao = ruleActionsDao;   
} 
	

public List getFilterName(String sessionCorpID ,Long userId){
	return ruleActionsDao.getFilterName(sessionCorpID, userId);
}
public List getActionList(Long ruleId) {
	return ruleActionsDao.getActionList(ruleId);
}

public <RuleActions> List getToDoRuleByRuleNumber(Long ruleId, String corpId)
{
return 	ruleActionsDao.getToDoRuleByRuleNumber(ruleId, corpId);
}
public void updateToDoRole(String messageDisplayed,String roleList, Long id,String corpId)
{
	ruleActionsDao.updateToDoRole(messageDisplayed, roleList, id,corpId);
}
public void updateStatus(String activestatus,Long ruleId,Long id)
{
ruleActionsDao.updateStatus(activestatus,ruleId,id);

}

}
