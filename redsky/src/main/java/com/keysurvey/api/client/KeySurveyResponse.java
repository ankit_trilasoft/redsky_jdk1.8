package com.keysurvey.api.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KeySurveyResponse {

	private Long surveyId = null;
	private String respondentEmail = null;
	private Long respondentId = null;
	private Long formId = null;
	private String surveyStatus = null;
	private String code = null;
	private Date submittedDate = null;
	private List<KeySurveyAnswer> keySurveyAnswers = new ArrayList<KeySurveyAnswer>();

	public String getRespondentEmail() {
		return respondentEmail;
	}

	public void setRespondentEmail(String respondentEmail) {
		this.respondentEmail = respondentEmail;
	}

	public Long getRespondentId() {
		return respondentId;
	}

	public void setRespondentId(Long respondentId) {
		this.respondentId = respondentId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public String getSurveyStatus() {
		return surveyStatus;
	}

	public void setSurveyStatus(String surveyStatus) {
		this.surveyStatus = surveyStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}

	public List<KeySurveyAnswer> getKeySurveyAnswers() {
		return keySurveyAnswers;
	}

	public void setKeySurveyAnswers(List<KeySurveyAnswer> keySurveyAnswers) {
		this.keySurveyAnswers = keySurveyAnswers;
	}

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

}
