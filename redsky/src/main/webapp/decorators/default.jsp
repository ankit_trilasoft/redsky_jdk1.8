<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="/common/taglibs.jsp"%>
<configByCorp:timeZone/>
<configByCorp:AutoSavePrompt/>
<s:hidden name="userPortalCheckType"   value="${userPortalCheckType}"/>
<c:set var="userPortalCheckType"  value="${userPortalCheckType}"/>
<s:hidden name="soTab"   value="${soTab}"/>
<c:set var="soTab"  value="${soTab}"/>
<s:hidden name="soSortOrder"   value="${soSortOrder}"/>
<c:set var="soSortOrder"  value="${soSortOrder}"/>

<s:hidden name="csoSortOrder"   value="${csoSortOrder}"/>
<c:set var="csoSortOrder"  value="${csoSortOrder}"/>

<s:hidden name="ticketSortOrder"   value="${ticketSortOrder}"/>
<c:set var="ticketSortOrder"  value="${ticketSortOrder}"/>

<s:hidden name="claimSortOrder"   value="${claimSortOrder}"/>
<c:set var="claimSortOrder"  value="${claimSortOrder}"/>

<s:hidden name="orderForJob"   value="${orderForJob}"/>
<c:set var="orderForJob"  value="${orderForJob}"/>

<s:hidden name="orderForCso"   value="${orderForCso}"/>
<c:set var="orderForCso"  value="${orderForCso}"/>

<s:hidden name="orderForTicket"   value="${orderForTicket}"/>
<c:set var="orderForTicket"  value="${orderForTicket}"/>

<s:hidden name="orderForClaim"   value="${orderForClaim}"/>
<c:set var="orderForClaim"  value="${orderForClaim}"/>

<s:hidden name="defaultCss"   value="${defaultCss}"/>
<c:set var="defaultCss"  value="${defaultCss}"/>

<s:hidden name="soLastName"   value="${soLastName}"/>
<c:set var="soLastName"  value="${soLastName}" scope="request"/>

<s:hidden name="globalSessionCorp"   value="${globalSessionCorp}"/>
<c:set var="globalSessionCorp"  value="${globalSessionCorp}" scope="session"/>

<fmt:setTimeZone value="${sessionTimeZone}" scope="session"/>
<c:set var="displayDateFormat" value="dd-MMM-yy" scope="session"/>
<c:set var="displayDateTimeFormat" value="dd-MMM-yyyy HH:mm" scope="application"/>
<c:set var="displayDateTimeEditFormat" value="dd-MMM-yyyy HH:mm" scope="application"/>
<c:set var="corpTimeZone" value="${sessionTimeZone}" scope="session"/>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
      <meta http-equiv="X-UA-Compatible" content="IE=11" />
        <%@ include file="/common/meta.jsp" %>
        <title>
        	 <decorator:title/> | 
        	    	
		        	 <c:choose>
						 <c:when test="${soLastName != '' and soLastName != null}" >${soLastName}</c:when>
						 <c:otherwise><fmt:message key="webapp.name"/></c:otherwise>
					</c:choose>						
		</title>       
              
        <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/theme.css'/>" />
        <link rel="stylesheet" type="text/css" media="print" href="<c:url value='/styles/${appConfig["csstheme"]}/print.css'/>" />
        <script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/ajaxtabs.js"></script>	
		<script type="text/javascript" src="scripts/ajax.js"></script>
		<script type="text/javascript" src="<c:url value='/scripts/animatedcollapse.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/builder.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/global.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/listDetail.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/masks.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/scripts/prototype.js'/>"></script> 
	    <script type="text/javascript" src="<c:url value='/scripts/scriptaculous.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/effects.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/controls.js'/>"></script>
		
		<script type="text/javascript" src="<c:url value='/scripts/switchcontent.js'/>"></script>		
		<script type="text/javascript" src="<c:url value='/scripts/util.js'/>"></script>   


        <decorator:head/>       
		
		<script language="javascript" type="text/javascript">
			//244 Key
			//googlekey="ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghTViisTyXk0c2uqtCoGXXmEzaGBCxSq-HcJ4NBL347UBrPbjJvca7lhow"
			
			//242 Key
			//googlekey="ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSi0QNsrZBMM_79QcmFyvqnFHZpUhQoMcREIv7XGmwzlaBod-kAkPoOLw";
			
			//Skyrelo Key
			//googlekey="ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w";
			
			
		  //function loadjscssfile(filename, filetype){
		  //if (filetype=="css"){ //if filename is an external CSS file
		  
		  //var fileref=document.createElement("link")
		  //fileref.setAttribute("rel", "stylesheet")
		  //fileref.setAttribute("type", "text/css")
		  //var defaultCss = '${defaultCss}';
		  //alert(defaultCss);
		  //fileref.setAttribute("href", 'styles/'+defaultCss+'/'+defaultCss+'.css')
		 //}
		 //if (typeof fileref!="undefined")
		  //document.getElementsByTagName("head")[0].appendChild(fileref)
		//}
	</script>
	
<style type="text/css">
body {margin:0; padding:0 0 100px 0; 
} 
div#footer {
position:absolute; bottom:0; left:0; width:98%; height:25px; 
background:#EFEFEF; color:black; z-index:500;
border-top:1px solid #CCCCCC;
 }
@media screen{
body>div#footer {
position:fixed;
  }
 }
* html body {
overflow:hidden;
 } 
* html div#content{
height:100%; overflow:auto;
 } 
#overlay { filter:alpha(opacity=70); -moz-opacity:0.7; 
-khtml-opacity: 0.7; opacity: 0.7; 
position:fixed; width:100%; height:100%;left:0px;top:0px; 
z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.opaque{
filter:alpha(opacity=100); -moz-opacity:1.0; -khtml-opacity: 1.0; opacity: 1.0; 
position:absolute; width:100%; height:100%;left:0px;top:0px; z-index:9999; 
margin:0px auto -1px auto;
background-color:red;
}
.rounded-top {  
border:1px solid #AFAFAF;
-moz-border-radius-topleft: .8em; -webkit-border-top-left-radius:.8em; -moz-border-radius-topright: .8em; 
-webkit-border-top-right-radius:.8em; border-top-left-radius: 0.8em; border-top-right-radius: 0.8em; 
box-shadow: -2px -2px 5px #666; border-bottom:none;
   }
.rounded-bottom {   
border:1px solid #AFAFAF;
-moz-border-radius-bottomleft: .8em; -webkit-border-bottom-left-radius:.8em; -moz-border-radius-bottomright: .8em; 
-webkit-border-bottom-right-radius:.8em; border-bottom-left-radius: 0.8em; border-bottom-right-radius: 0.8em; 
box-shadow: 0px 3px 3px #666; border-top:none;  
}
</style>
</head>

<body<decorator:getProperty property="body.id" writeEntireProperty="true"/>
<decorator:getProperty property="body.class" writeEntireProperty="true"/> >

<div id="overlay">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr> <td height="200px"></td> </tr>
<tr>
    <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
     <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
    </td>
  </tr>
    <tr>
   <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
    <img src="<c:url value='/images/ajax-loader.gif'/>" />       
     </td>
   </tr>
 </table>
 </td>
</tr>
</table>  
</div>   
</div>
   
      <div id="footer">
        <jsp:include page="/common/footer.jsp"/>
        </div>
   
        <div id="header" class="clearfix">
      <s:hidden name="sessionExp" value="${sessionExp}"/>
        <c:if test="${sessionExp!='true'}">
			<authz:authorize ifNotGranted="ROLE_ANONYMOUS">
				<jsp:include page="/common/header.jsp"/>
			</authz:authorize>
		</c:if>
			<authz:authorize ifAnyGranted="ROLE_ANONYMOUS">
				<jsp:include page="/common/loginHeader.jsp"/>			
			</authz:authorize>
        </div>       
				
        <div id="content"  class="clearfix" style="padding-top:4px; margin-left:0px;">       
 
   <!-- menu navigation starts here -->
   <c:if test="${sessionExp!='true'}">
   <c:if test="${!pwdreset}">
        <authz:authorize ifNotGranted="ROLE_ANONYMOUS">
            <div class="nav" style="z-index:600;">
                <div class="wrapper">
                    <h2 class="accessibility">Navigation</h2>
                    <jsp:include page="/common/menu.jsp"/>
                </div>
                
            </div>
			</authz:authorize>	
			<br clear="all" />
	</c:if>
	</c:if>
	<!-- end navigation -->
   
   <authz:authorize ifNotGranted="ROLE_ANONYMOUS">
    <c:if test="${sessionExp!='true'}">
			<c:if test="${!pwdreset}">
				<%@ include file="/common/breadcrumb.jsp" %>
			</c:if>	
		</c:if>
			  <div id="inner-content">
	            <div id="main">
	                <%@ include file="/common/messages.jsp" %>
	
	                <decorator:body/>
	            </div>
			</authz:authorize>
			<authz:authorize ifAnyGranted="ROLE_ANONYMOUS">
	                <%@ include file="/common/messages.jsp" %>
	
	                <decorator:body/>
			</authz:authorize>
			</div>
            <c:set var="currentMenu" scope="request"><decorator:getProperty property="meta.menu"/></c:set>
            <c:if test="${currentMenu == 'AdminMenu'}">
            <div id="sub">
                <menu:useMenuDisplayer name="Velocity" config="cssVerticalMenu.vm" permissions="rolesAdapter">
                    <menu:displayMenu name="AdminMenu"/>
                </menu:useMenuDisplayer>
            
            	</div>
            </c:if>            
        </div>
       <div id="clearfooter"></div>   
   <div id="mydiv" style="position:absolute; font-size:11px; color:#6693c6;" ></div>
</body>
</html>
<script language="JavaScript" type="text/javascript">
	function progressBarAutoSave(tar){
	showOrHideAutoSave(tar);
	}

	function showOrHideAutoSave(value) {
	    if (value==0) {
	        if (document.layers)
	           document.layers["overlay"].visibility='hide';
	        else
	           document.getElementById("overlay").style.visibility='hidden';
	   }
	   else if (value==1) {
	       if (document.layers)
	          document.layers["overlay"].visibility='show';
	       else
	          document.getElementById("overlay").style.visibility='visible';
	   }
	}
	
	var form_submitted = false;

	function submit_form()
	{
	showOrHideAutoSave('1');
	  if (form_submitted)
	  {
	    alert ("Your form has already been submitted. Please wait...");
	    return false;
	  }
	  else
	  {
	    form_submitted = true;
	    return true;
	  }
	}
</script>
<script type="text/javascript">
showOrHideAutoSave(0);
var bobexample=new switchcontent("switchgroup1", "div") //Limit scanning of switch contents to just "div" elements
bobexample.setStatus('<img src="images/collapse_open.jpg" align="absmiddle" border="0">', '<img src="images/collapse_close.jpg" align="absmiddle" border="0">')
bobexample.setColor('black', 'black')
bobexample.setPersist(true)
bobexample.collapsePrevious(false) //Only one content open at any given time
bobexample.init() ;


</script>
	
		
		 