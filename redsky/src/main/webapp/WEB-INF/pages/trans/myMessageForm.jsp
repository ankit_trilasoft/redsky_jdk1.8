<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="myMessageDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='myMessageDetail.heading'/>"/>  
    <style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>

<!--<script language="javascript" type="text/javascript" SRC="/scripts/mktree.js">
</script>
<LINK REL="stylesheet" HREF="/styles/mktree.css"> 

--></head>

<s:form id="myMessageForm" action="saveMyMessage" method="post" validate="true"> 
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<!-- 
<table>
<tbody>
<tr>
			<td rowspan="25" valign="top" align="left" width="120px">
				<ul class="mktree" id="tree1">
					<li>Folders
						<ul>
							<li><font color="red" >Inbox</font></li>
							<li><a href="myOutboxMessages.html?status=sent&loggedInUser=${pageContext.request.remoteUser}" >Outbox</a></li>
							<li><a href="editMyMessage.html" >Compose</a></li>
						</ul>
					</li>
				</ul>
			</td>
<td> -->
<div id="newmnav">
		    <ul>
			  <li><a href="myMessages.html?fromUser=${pageContext.request.remoteUser}"><span>Inbox</span></a></li>
			  <li><a href="myOutboxMessages.html?status=sent&loggedInUser=${pageContext.request.remoteUser}"><span>Outbox</span></a></li>
			  <li id="newmnav1" style="background:#FFF"><a class="current" href="editMyMessage.html" ><span>Compose<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><br>
<div id="Layer1">

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table>
					<tr>
						<td align="right" class="listwhite"><fmt:message key="myMessage.toUser"/></td>
						<td align="left" class="listwhite" colspan="4"><s:textfield name="myMessage.toUser" size="80" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key="myMessage.subject"/></td>
						<td align="left" class="listwhite" colspan="4"><s:textfield name="myMessage.subject" size="80" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key="myMessage.message"/></td>
						<td align="left" class="listwhite" colspan="6"><s:textarea name="myMessage.message"  cols="80" rows="10" /></td>
					</tr>
				</table>
				<table>
					<tr>
						<td align="right" class="listwhite" style="width:100px"><fmt:message key="myMessage.sentOn"/></td>
						<c:if test="${not empty myMessage.sentOn}">
							<s:text id="myMessageSentOnDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="myMessage.sentOn"/></s:text>
							<td><s:textfield id="sentOn" name="myMessage.sentOn" value="%{myMessageSentOnDateFormattedValue}" size="9" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="sentOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
						</c:if>
						<c:if test="${ empty myMessage.sentOn}">
							<td align="right" class="listwhite"><s:textfield id="sentOn" name="myMessage.sentOn" size="9" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="sentOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
						</c:if>
						<td align="right" class="listwhite" style="width:100px"><fmt:message key="myMessage.readOn"/></td>
						<c:if test="${not empty myMessage.readOn}">
							<s:text id="myMessageReadOnDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="myMessage.readOn"/></s:text>
							<td><s:textfield id="readOn" name="myMessage.readOn" value="%{myMessageReadOnDateFormattedValue}" size="9" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="readOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
						</c:if>
						<c:if test="${ empty myMessage.readOn}">
							<td align="right" class="listwhite"><s:textfield id="readOn" name="myMessage.readOn" size="9" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="readOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
						</c:if>
						<td align="right" class="listwhite" style="width:100px"><fmt:message key="myMessage.status" /></td>
						<td align="left" class="listwhite"><s:textfield name="myMessage.status" readonly="true"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>  
<!-- 
</td>
</tr>
</tbody>
</table> 
 -->
<s:hidden name="myMessage.id" />
<s:hidden name="myMessage.corpID" />
<s:hidden name="myMessage.noteId" />
<c:if test="${not empty myMessage.id}">
<s:hidden name="myMessage.fromUser" />
</c:if>
<c:if test="${empty myMessage.id}">
<s:hidden name="myMessage.fromUser" value="${pageContext.request.remoteUser}"/>
</c:if>
       	<c:if test="${empty myMessage.id}">       
        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.send"/>  
        </c:if> 
        <c:if test="${not empty myMessage.id}">    
            <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="delete" key="button.delete" onclick="return confirmDelete('myMessage')"/>   
        </c:if>   
        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="cancel" key="button.cancel"/>   
    
</s:form>   

<script type="text/javascript">   
    Form.focusFirstElement($("myMessageForm"));   
</script>  
 <script type="text/javascript">
 	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
 </script>