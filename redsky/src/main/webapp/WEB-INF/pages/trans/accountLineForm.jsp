<%@ include file="/common/taglibs.jsp"%>   
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>

</script>

<script type="text/javascript">
<!--
//&&&&&&&&&&& FOR PASS THROUGH &&&&&&&&&&&
  function passRevenue()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.revenueExpense'].value;
		totalRevenue();
	}
 function passInsurance()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value;
		totalRevenue();
	}
 function passOrigin()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.originRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value;
		totalRevenue();
	}
 function passFreight()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.freightRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value;
		totalRevenue();
	}
 function passDestination()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.destinationRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value;
		totalRevenue();
	}
 function passOther()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.otherRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value;
		totalRevenue();
	}
 function passOther2()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.other2RevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value;
		totalRevenue();
	}
 function passCommision()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.commisionRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value;
		totalRevenue();
	}
 function passFlatFee()
	{
		//alert(" Hi assigingn FlatFee");
		document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRevenueAmount'].value=document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value;
		totalRevenue();
	}
//&&&&&&&&&&&******************&&&&&&&&&&&

//&&&&&&&&&&&  For Total Expense &&&&&&&&&&&
 function totalExpense()
	{
		//alert(" Hi assigingn totalExpense");
		var insuranceExpense;
		insuranceExpense=0;
		var originExpense;
		originExpense=0;
		var freightExpense;
		freightExpense=0;
		var destinationExpense;
		destinationExpense=0;
		var otherExpense;
		otherExpense=0;
		var other2Expense;
		other2Expense=0;
		var commisionExpense;
		commisionExpense=0;
		var flatFeeExpense;
		flatFeeExpense=0;
		
		if(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value!='')
		{
			//alert("Assigning insuranceExpense");
			insuranceExpense=document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value;

		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value!='')
		{
			//alert("Assigning originExpense");
			originExpense=document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value!='')
		{
			//alert("Assigning freightExpense");
			freightExpense=document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value!='')
		{
			//alert("Assigning destinationExpense");
			destinationExpense=document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value!='')
		{
			//alert("Assigning otherExpense");
			otherExpense=document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value!='')
		{
			//alert("Assigning other2Expense");
			other2Expense=document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value!='')
		{
			//alert("Assigning commisionExpense");
			commisionExpense=document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value!='')
		{
			//alert("Assigning flatFeeExpense");
			flatFeeExpense=document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value;
		}

			//alert("Assigning Total Expense");
			document.forms['quotationCostingForm'].elements['quotationCosting.totalExpense'].value=roundNumber(Number(insuranceExpense)+Number(originExpense)+Number(freightExpense)+Number(destinationExpense)+Number(otherExpense)+Number(other2Expense)+Number(flatFeeExpense)+Number(commisionExpense));
	}
 
 
 //&&&&&&&&&&&  For Total Expense &&&&&&&&&&&
 function totalRevenue()
	{
		//alert(" Hi assigingn totalExpense");
		var revenueRevenueAmount;
		revenueRevenueAmount=0;
		var originRevenueAmount;
		originRevenueAmount=0;
		var freightRevenueAmount;
		freightRevenueAmount=0;
		var destinationRevenueAmount;
		destinationRevenueAmount=0;
		var insuranceRevenueAmount;
		insuranceRevenueAmount=0;
		var otherRevenueAmount;
		otherRevenueAmount=0;
		//var other2RevenueAmount;
		// other2RevenueAmount=0;
		var commisionRevenueAmount;
		commisionRevenueAmount=0;
		var flatFeeRevenueAmount;
		flatFeeRevenueAmount=0;
		
		if(document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value!='')
		{
			//alert("Assigning insuranceRevenue");
			revenueRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value;
		}
		
		 
		
		if(document.forms['quotationCostingForm'].elements['quotationCosting.originRevenueAmount'].value!='')
		{
			//alert("Assigning originRevenueAmount");
			originRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.originRevenueAmount'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.freightRevenueAmount'].value!='')
		{
			//alert("Assigning freightRevenueAmount");
			freightRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.freightRevenueAmount'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.destinationRevenueAmount'].value!='')
		{
			//alert("Assigning destinationRevenueAmount");
			destinationRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.destinationRevenueAmount'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRevenueAmount'].value!='')
		 {
			//alert("Assigning insuranceRevenueAmount");
			 insuranceRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRevenueAmount'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.otherRevenueAmount'].value!='')
		{
			//alert("Assigning otherRevenueAmount");
			otherRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.otherRevenueAmount'].value;
		}
		/*
		
		// if(document.forms['quotationCostingForm'].elements['quotationCosting.other2RevenueAmount'].value!='')
		{
			//alert("Assigning other2RevenueAmount");
			// other2RevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.other2RevenueAmount'].value;
		}
		
		*/
		if(document.forms['quotationCostingForm'].elements['quotationCosting.commisionRevenueAmount'].value!='')
		{
			//alert("Assigning commisionRevenueAmount");
			commisionRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.commisionRevenueAmount'].value;
		}
		if(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRevenueAmount'].value!='')
		{
			//alert("Assigning flatFeeRevenueAmount");
			flatFeeRevenueAmount=document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRevenueAmount'].value;
		}

			//alert("Assigning Total RevenueAmount");
			document.forms['quotationCostingForm'].elements['quotationCosting.totalRevenueAmount'].value=roundNumber(Number(revenueRevenueAmount)+Number(originRevenueAmount)+Number(freightRevenueAmount)+Number(destinationRevenueAmount)+Number(insuranceRevenueAmount)+Number(otherRevenueAmount)+Number(flatFeeRevenueAmount)+Number(commisionRevenueAmount));
			// grossMarginPerCalculation();
	}
//-->
</script>


<script type="text/javascript">
<!--
function revenueCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.revenueQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Revenue quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.revenueQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.revenueRate'].value > 0))
 	 			{
 	 				alert("Please enter Revenue Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.revenueRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].value > 0))
 	 			{
 	 				alert("Please enter Revenue Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.revenueQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.revenueRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.revenueDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].focus();
		 					}
		 				}
				    else
						{
						
								tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].focus();
		 					}
							
						}
						
			 }
			 totalRevenue();
 		}
 		
 		function revenueDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.revenueQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.revenueRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.revenueQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.revenueRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.revenueDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].focus();
		 					}
		 				}
				    else
						{
						
								tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.revenueUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.revenueRevenueAmount'].focus();
		 					}
							
						}
						
			 }
			 totalRevenue();
 		}
 		
 		function insuranceCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceQuantity'].value > 0))
 	 			{
 	 				alert("Please enter insurance quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.insuranceQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRate'].value > 0))
 	 			{
 	 				alert("Please enter insurance Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].value > 0))
 	 			{
 	 				alert("Please enter insurance Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.insuranceQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function insuranceDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.insuranceQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.insuranceQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.insuranceRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.insuranceDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.insuranceUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.insuranceExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		
 		
 		function originCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.originQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Origin quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.originQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.originRate'].value > 0))
 	 			{
 	 				alert("Please enter Origin Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.originRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].value > 0))
 	 			{
 	 				alert("Please enter Origin Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.originQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.originRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.originDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function originDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.originQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.originRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.originQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.originRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.originDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.originUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.originExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		
 		
 		
 		function freightCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.freightQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Freight quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.freightQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.freightRate'].value > 0))
 	 			{
 	 				alert("Please enter Freight Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.freightRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].value > 0))
 	 			{
 	 				alert("Please enter Freight Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.freightQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.freightRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.freightDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function freightDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.freightQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.freightRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.freightQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.freightRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.freightDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.freightUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.freightExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}


 		function destinationCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.destinationQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Destination quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.destinationQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.destinationRate'].value > 0))
 	 			{
 	 				alert("Please enter Destination Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.destinationRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].value > 0))
 	 			{
 	 				alert("Please enter Destination Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.destinationQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.destinationRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.destinationDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function destinationDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.destinationQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.destinationRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.destinationQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.destinationRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.destinationDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.destinationUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.destinationExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}

 		function otherCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.otherQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Other quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.otherQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.otherRate'].value > 0))
 	 			{
 	 				alert("Please enter Other Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.otherRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].value > 0))
 	 			{
 	 				alert("Please enter Other Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.otherQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.otherRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.otherDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function otherDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.otherQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.otherRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.otherQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.otherRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.otherDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.otherUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.otherExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}


 		function other2Calc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.other2Quantity'].value > 0))
 	 			{
 	 				alert("Please enter Other quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.other2Quantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.other2Rate'].value > 0))
 	 			{
 	 				alert("Please enter Other Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.other2Rate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].value > 0))
 	 			{
 	 				alert("Please enter Other Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.other2Quantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.other2Rate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.other2DivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function other2DropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.other2Quantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.other2Rate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.other2Quantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.other2Rate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.other2DivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.other2Unit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.other2Expense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}

 		function commisionCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.commisionQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Commision quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.commisionQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.commisionRate'].value > 0))
 	 			{
 	 				alert("Please enter Commision Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.commisionRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].value > 0))
 	 			{
 	 				alert("Please enter Commision Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.commisionQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.commisionRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.commisionDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function commisionDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.commisionQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.commisionRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.commisionQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.commisionRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.commisionDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.commisionUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.commisionExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}

 		function flatFeeCalc()
 	 	{
 	 		if(!(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeQuantity'].value > 0))
 	 			{
 	 				alert("Please enter Flat Fee quantity");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeQuantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRate'].value > 0))
 	 			{
 	 				alert("Please enter Flat Fee Rate");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRate'].focus();
 	 				
 	 			}
		   
 	 	    else if(!(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].value > 0))
 	 			{
 	 				alert("Please enter Flat Fee Unit");
 	 				document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}
 		
 		function flatFeeDropdownCalculation()
 	 	{
 	 		if((document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeQuantity'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRate'].value > 0) && (document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].value > 0))
 	 		 { 
		 	 		var d=document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeQuantity'].value*document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeRate'].value;
					//alert(d);
					var tempAmount;
		 			if(document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeDivMult'].value=='/')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].focus();
		 					}
		 				}
				    else
						{
						
							tempAmount=d*document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeUnit'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=roundNumber(tempAmount);
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].value=0;
		 					  document.forms['quotationCostingForm'].elements['quotationCosting.flatFeeExpense'].focus();
		 					}
							
						}
						
			 }
			 totalExpense();
 		}


//-->
</script>

<script type="text/javascript">

<!--
function roundNumber(numberToRound) {

	//alert("Hello from numberToRound")
	var numberField = numberToRound;	//document.roundform.numberfield; // Field where the number appears
	//alert(numberToRound);
	var rnum = numberToRound;// numberField.value;
	var rlength = 2; // The number of decimal places to round to
	if (rnum > 8191 && rnum < 10485) {
		rnum = rnum-5000;
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
		newnumber = newnumber+5000;
	} else {
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	}
	//numberField.value = newnumber;
	//alert("Resultant number"+newnumber)
	return newnumber;
}

//-->
</script>


<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>
<script type="text/javascript">
<!--
function grossMarginPerCalculation()
	{
		var grossMarginPer;
		grossMarginPer=document.forms['quotationCostingForm'].elements['quotationCosting.totalRevenueAmount'].value-document.forms['quotationCostingForm'].elements['quotationCosting.totalExpense'].value;
		document.forms['quotationCostingForm'].elements['quotationCosting.grossRevenueAmount'].value=roundNumber(grossMarginPer);
		//alert(grossMarginPer);
		if(grossMarginPer > 0)
		  {
		  	//alert("Hi Madhu");
			document.forms['quotationCostingForm'].elements['quotationCosting.grossMarginPer'].value=roundNumber((grossMarginPer*100)/document.forms['quotationCostingForm'].elements['quotationCosting.totalRevenueAmount'].value);
		  }
	}

//-->
 function reCalculation()
	 {
	 	revenueDropdownCalculation();
	 	insuranceDropdownCalculation();
	 	originDropdownCalculation();
	 	freightDropdownCalculation();
	 	destinationDropdownCalculation();
	 	otherDropdownCalculation();
	 	other2DropdownCalculation();
	 	commisionDropdownCalculation();
	 	flatFeeDropdownCalculation();
	 	
	 	totalExpense();
	 	totalRevenue();
	 	grossMarginPerCalculation();
	 }
</script>

<script type="text/javascript">
<!--
function magnify_content(targetElement) 
	{
		//alert("In Magnifier");
		//var tempMagnifier=targetElement.value;
		//alert("Hello");
		//alert(targetElement.value);
		document.forms['quotationCostingForm'].elements['magnifier'].value=targetElement.value;
	}
//-->
</script>
<script type="text/javascript">
<!--
 function checkData()
  {
	 var f = document.getElementById('quotationCostingForm');
   		 f.setAttribute("autocomplete", "off");
  }
//-->
</script>

<head>   
    <title><fmt:message key="quotationCostingDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='quotationCostingDetail.heading'/>"/>   
</head>  

<s:form id="quotationCostingForm" name="quotationCostingForm" action="saveQuotationCosting" method="post" validate="true">      
 
<s:hidden name="quotationCosting.id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>

<s:hidden name="quotationCosting.shipNumber" value="%{serviceOrder.shipNumber}"/> 
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="quotationCosting.contract"/>

<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>

<s:hidden name="customerFile.id"/>







		<div id="newmnav">
		  <ul>
		  <c:if test="${serviceOrder.job =='RLO'}">
		    <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
		    </c:if>
		  <c:if test="${serviceOrder.job !='RLO'}">
		    <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
		    </c:if>
		    <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
		    	<li><a  href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		    </sec-auth:authComponent>
  			<li><a href="servicePartners.html?id=${serviceOrder.id}"><span>Partner</span></a></li>
  			
  			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Accounting<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  			<li><a href="containers.html?id=${serviceOrder.id}"><span>Container</span></a></li>
  
  			<c:if test="${serviceOrder.job !='INT'}">
  			<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  			</c:if>
  			<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	        <c:if test="${serviceOrder.job =='INT'}">
	        <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	        </c:if>
	        </sec-auth:authComponent>
  			<c:if test="${serviceOrder.job =='RLO'}">
  			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  			</c:if>
  		<c:if test="${serviceOrder.job !='RLO'}">
  		<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  		</c:if>
  		<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
  		<li ><a href="costings.html?id=${serviceOrder.id}"><span>Invoice</span></a></li>
  		<configByCorp:fieldVisibility componentId="component.standard.claimTab">
  		<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  		</configByCorp:fieldVisibility>
  		<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
		</ul>
		</div><div class="spn">&nbsp;</div><br>
	

<%--

<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>										
<tr>
  <td align="left">
  <ul id="content-nav">
  <li class="cssbuttonActive"><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}">S/O Details</a></li>
  <li class="cssbuttonActive"><a  href="editBilling.html?id=${serviceOrder.id}">Billing Info</a></li>
  <li class="cssbuttonActive"><a href="servicePartners.html?id=${serviceOrder.id}">Partner</a></li>
  <li class="current"><a>Costing</a></li>
  <li class="cssbuttonActive"><a href="containers.html?id=${serviceOrder.id}">Container</a></li>
  
  <c:if test="${serviceOrder.job !='INT'}">
  <li class="cssbuttonActive"><a href="editMiscellaneous.html?id=${serviceOrder.id}">Domestic</a></li>
  </c:if>
  
  <li class="cssbuttonActive"><a href="editTrackingStatus.html?id=${serviceOrder.id}">Status</a></li>
  <li class="cssbuttonActive"><a href="customerWorkTickets.html?id=${serviceOrder.id}">Ticket</a></li>
  <li class="cssbuttonActive"><a href="costings.html?id=${serviceOrder.id}">Invoice</a></li>
  <li class="cssbuttonActive"><a href="claims.html?id=${serviceOrder.id}">Claim</a></li>
  <li class="cssbuttonActive"><a href="editCustomerFile.html?id=${customerFile.id}">Customer File</a></li>
</ul>
</td></tr>
</tbody></table>

   
   --%>
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" onfocus="checkData();"  size="15"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountry" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountry" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>
 
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  <li id="newmnav1" style="background:#FFF "><a href="editQuotationCosting.html?id=${serviceOrder.id}"><span>Entitlement<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
  <li><a href="editQuotationCostingEstimate.html?id=${serviceOrder.id}"><span>Estimate</span></a></li>
  <li><a href="editQuotationCostingRevision.html?id=${serviceOrder.id}"><span>Revision</span></a></li>
 <li ><a href="editQuotationCostingSummary.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
  
</ul>
</td></tr>
</tbody></table>

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
                     <tr>
     <td align="right" class="listwhitetext"><fmt:message key="costing.idNumber"/></td>
  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="costing.idNumber" size="1" maxlength="2" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" /></td>
     </tr>
     <tr>
  <td align="right" class="listwhitetext"><fmt:message key="costing.containerNumber"/></td>
  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="costing.containerNumber" size="13" maxlength="14" onkeydown="return onlyPhoneNumsAllowed(event)" /></td>
  </tr>
  <tr>
 <td width="100px" align="right" class="listwhitetext"><fmt:message key="costing.NoOfcontainer"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="costing.numberOfContainer" size="1"  maxlength="1" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" /></td>
</tr>
  <tr>
  <td align="right" class="listwhitetext"><fmt:message key="costing.sealNumber"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="costing.sealNumber" size="13" maxlength="14" onkeydown="return onlyPhoneNumsAllowed(event)" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="costing.size"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="container.size"  size="1"  maxlength="3" onkeydown="return onlyAlphaNumericAllowed(event)" /></td>
 </tr>
 
 <%--
 <tr>
  <td align="right" class="listwhitetext"><fmt:message key="costing.grossWeight"/></td>
  <td colspan="5" align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="costing.grossWeight" size="13" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="calcNetWeight();" /></td>
  <td align="right" class="listwhitetext"><fmt:message key='labels.units'/></td>
<td width="200px"><s:radio name="container.unit1" list="%{weightunits}" /></td>
<td colspan="2"  align="right" class="listwhitetext"><fmt:message key="container.volume"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="container.volume" size="13" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="calculate();" /></td>
<td  colspan="3" align="right" class="listwhitetext"><fmt:message key='labels.volunit'/></td>
<td width="200px"><s:radio  name="container.unit2" list="%{volumeunits}" /></td>
 
</tr>
--%>








<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											
										</tbody>
									</table>
<%--


<div id="newmnav">
		  <ul>
		    <li style="background:#FFF "><a class="current"><span>Entitlement<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  			<li><a href="editQuotationCostingEstimate.html?id=${serviceOrder.id}"><span>Estimate</span></a></li>
  			<li><a href="editQuotationCostingRevision.html?id=${serviceOrder.id}"><span>Revision</span></a></li>
  			<li ><a href="editQuotationCostingSummary.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
  		</ul>
		</div><div class="spn">&nbsp;</div><br>
	
	--%>
	 <%--

 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left" class="listwhitebox">
  <ul id="content-nav">
  <li  class="current"><a href="EntitlementForm.html">Entitlement</a></li>
  <li  class="cssbuttonActive"><a href="EstimateForm.html">Estimate</a></li>
  <li  class="cssbuttonActive"><a href="RevisionForm.html">Revision</a></li>
  <li  class="cssbuttonActive"><a href="SummeryForm.html">Summary</a></li>
  
  </ul>
</td></tr>
</tbody></table>
   --%>
 <%--

<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr>
		  		<td align="right" colspan="2"><fmt:message key="jobcosting.magnifier"/></td>
				<td align="left"><s:textfield name="magnifier" required="true" readonly="true" cssClass="input-textUpper" size="45"/></td>
			</tr>
		  	
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>
  --%>
 <%-- 
 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<c:set var="count" value="1"/>
		<tr><td class="listwhitebox">
 		  <table class="detailTabLabel" border="0">
				<tbody>
					<tr><td align="left" height="5px"></td></tr>
					
					<tr><td align="left" colspan="5">
					<table class="detailTabLabel" border="0">
 		  			<tbody>
 		  				<tr><td align="left"  width="10px"><fmt:message key="jobcosting.seqNo"/></td>
							<td align="left" width="30px"><fmt:message key="jobcosting.quoteType"/></td>
							<td><td><td align="left" width="190px"><fmt:message key="entitle.vendor.name"/></td></td><td>
							<td align="left"><fmt:message key="entitled.amount"/></td>
						</tr>
		    		</tbody>
 					</table></td></tr>
					
					
					
					
					<c:set var="billingContract" value="${quotationCosting.contract}"/>
					<c:set var="serviceOrderID" value="${serviceOrder.id}"/>
					<tr>
						<td align="left" width="1"><c:out value="${count}"/>.</td>
						<td align="left" width="10">Revenue</td>
						<td align="left" width="10"><s:textfield readonly="true" name="quotationCosting.revenueName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.revenueName&fld_code=firstDescription');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.revenueRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
						<s:hidden name="quotationCosting.revenueVendorCode"/>
					</tr>
					<!-- 
						<td align="left"><s:textfield readonly="true" name="quotationCosting.revenueVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.revenueName&fld_code=quotationCosting.revenueVendorCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.revenueName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><s:textfield readonly="true" name="quotationCosting.revenueChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.revenueChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						
						<td align="left"><s:textfield name="quotationCosting.revenueReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.revenueQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="revenueDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.revenueRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="revenueDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.revenueDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="revenueDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.revenueUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="revenueCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"></td>
						<td align="center"></td>
						<td align="left"><s:textfield name="quotationCosting.revenueRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					
					</tr>
					
					
	  				<c:set var="insurancePassThrough" value="false"/>
	  				<c:if test="${quotationCosting.insurancePassThrough}">
			  			<c:set var="insurancePassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="originPassThrough" value="false"/>
			  		<c:if test="${quotationCosting.originPassThrough}">
			  			<c:set var="originPassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="freightPassThrough" value="false"/>
			  		<c:if test="${quotationCosting.freightPassThrough}">
			  			<c:set var="freightPassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="destinationPassThrough" value="false"/>
			  		<c:if test="${quotationCosting.destinationPassThrough}">
			  			<c:set var="destinationPassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="otherPassThrough" value="false"/>
			  		<c:if test="${quotationCosting.otherPassThrough}">
			  			<c:set var="otherPassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="other2PassThrough" value="false"/>
			  		<c:if test="${quotationCosting.other2PassThrough}">
			  			<c:set var="other2PassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="commisionPassThrough" value="false"/>
			  		<c:if test="${quotationCosting.commisionPassThrough}">
			  			<c:set var="commisionPassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="flatFeePassThrough" value="false"/>
			  		<c:if test="${quotationCosting.flatFeePassThrough}">
			  			<c:set var="flatFeePassThrough" value="true"/>
			  		</c:if>
			  		
			  		<c:set var="priceApproved" value="false"/>
			  		<c:if test="${quotationCosting.priceApproved}">
			  			<c:set var="priceApproved" value="true"/>
			  		</c:if>
			  		-->	
					<!-- 
					
					<tr>
						<td align="left">2.</td>
						<td align="left">insurance</td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.insuranceVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.insuranceName&fld_code=quotationCosting.insuranceVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.insuranceName" required="true"  cssClass="input-textUpper" size="10" title="${quotationCosting.insuranceName}" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><s:textfield readonly="true" name="quotationCosting.insuranceChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.insuranceChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.insuranceReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.insuranceQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="insuranceDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.insuranceRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="insuranceDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.insuranceDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="insuranceDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.insuranceUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="insuranceCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.insuranceExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.insurancePassThrough" value="${insurancePassThrough}" fieldValue="true" onclick="passinsurance();"/></td>
						<td align="left"><s:textfield name="quotationCosting.insuranceRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					</tr>
					 -->
					 <!--
					<s:hidden name="quotationCosting.insuranceVendorCode"/>
					<s:hidden name="quotationCosting.insuranceName"/>
					<s:hidden name="quotationCosting.insuranceChargeCode"/>
					<s:hidden name="quotationCosting.insuranceReference"/>
					<s:hidden name="quotationCosting.insuranceQuantity"/>
					<s:hidden name="quotationCosting.insuranceRate"/>
					
					<s:hidden name="quotationCosting.insuranceDivMult"/>
					<s:hidden name="quotationCosting.insuranceUnit"/>
					<s:hidden name="quotationCosting.insuranceExpense"/>
					<s:hidden name="quotationCosting.insurancePassThrough"/>
					<s:hidden name="quotationCosting.insuranceRevenueAmount"/>
					-->
  					
					<tr>
						<td align="left">2.</td>
						<td align="left">Origin</td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.originName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=OA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.originName&fld_code=firstDescription');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield name="quotationCosting.originRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					    <s:hidden name="quotationCosting.originVendorCode"/>
					<!--
						<td align="left"><s:textfield readonly="true" name="quotationCosting.originVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=OA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.originName&fld_code=quotationCosting.originVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.originName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.originChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.originChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.originReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.originQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="originDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.originRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="originDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.originDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="originDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.originUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="originCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.originExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.originPassThrough" value="${originPassThrough}" fieldValue="true" onclick="passOrigin();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.originRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					-->
					</tr>
					
					<tr>
						<td align="left">3.</td>
						<td align="left">Freight</td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.freightName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=FW&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.freightName&fld_code=quotationCosting.freightVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						<td align="left"><s:textfield name="quotationCosting.freightRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					    <s:hidden name="quotationCosting.freightVendorCode"/>
					<!--
						<td align="left"><s:textfield readonly="true" name="quotationCosting.freightVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=FW&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.freightName&fld_code=quotationCosting.freightVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.freightName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.freightChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.freightChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.freightReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.freightQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="freightDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.freightRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="freightDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.freightDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="freightDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.freightUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="freightCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.freightExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.freightPassThrough" value="${freightPassThrough}" fieldValue="true" onclick="passFreight();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.freightRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					-->
					</tr>
					<tr>
						<td align="left">4.</td>
						<td align="left">Destination</td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.destinationName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=DA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.destinationName&fld_code=firstDescription');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield name="quotationCosting.destinationRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					    <s:hidden name="quotationCosting.destinationVendorCode"/>
					<!--
						<td align="left"><s:textfield readonly="true" name="quotationCosting.destinationVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=DA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.destinationName&fld_code=quotationCosting.destinationVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.destinationName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><s:textfield readonly="true" name="quotationCosting.destinationChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.destinationChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.destinationReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.destinationQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="destinationDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.destinationRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="destinationDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.destinationDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="destinationDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.destinationUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="destinationCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.destinationExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.destinationPassThrough" value="${destinationPassThrough}" fieldValue="true" onclick="passDestination();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.destinationRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					-->
					</tr>
					<tr>
						<td align="left">5.</td>
						<td align="left">Insurance</td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.otherName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.otherName&fld_code=firstDescription');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield name="quotationCosting.otherRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					    <s:hidden name="quotationCosting.otherVendorCode"/>
					<!--
						<td align="left"><s:textfield readonly="true" name="quotationCosting.otherVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.otherName&fld_code=quotationCosting.otherVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.otherName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><s:textfield readonly="true" name="quotationCosting.otherChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.otherChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.otherReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.otherQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="otherDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.otherRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="otherDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.otherDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="otherDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.otherUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="otherCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.otherExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.otherPassThrough" value="${otherPassThrough}" fieldValue="true" onclick="passOther();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.otherRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					-->
					</tr>
					<tr>
						<td align="left">6.</td>
						<td align="left">Other</td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.other2Name" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.other2Name&fld_code=firstDescription');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield name="quotationCosting.other2RevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					    <s:hidden name="quotationCosting.other2VendorCode"/>
					<!--
						<td align="left"><s:textfield readonly="true" name="quotationCosting.other2VendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.other2Name&fld_code=quotationCosting.other2VendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.other2Name" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.other2ChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.other2ChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.other2Reference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.other2Quantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="other2DropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.other2Rate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="other2DropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.other2DivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="other2DropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.other2Unit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="other2Calc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.other2Expense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.other2PassThrough" value="${other2PassThrough}" fieldValue="true" onclick="passOther2();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.other2RevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					-->
					</tr>
					<tr>
						<td align="left">7.</td>
						<td align="left">Commision</td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.commisionName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.commisionName&fld_code=quotationCosting.commisionVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						<s:hidden name="quotationCosting.commisionVendorCode"/>
						<!-- <td  align="left"><s:textfield readonly="true" name="quotationCosting.commisionVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>-->
						<!--<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.commisionName&fld_code=quotationCosting.commisionVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.commisionName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><s:textfield readonly="true" name="quotationCosting.commisionChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.commisionChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.commisionReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.commisionQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="commisionDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.commisionRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="commisionDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.commisionDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="commisionDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.commisionUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="commisionCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.commisionExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.commisionPassThrough" value="${commisionPassThrough}" fieldValue="true" onclick="passCommision();"/></td>-->
						<td align="left"><s:textfield name="quotationCosting.commisionRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					</tr>
					<tr>
						<td align="left">8.</td>
						<td align="left">Flat Fees</td>
						<td align="left"><s:textfield readonly="true" name="quotationCosting.flatFeeName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.flatFeeName&fld_code=firstDescription');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield name="quotationCosting.flatFeeRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					    <s:hidden name="quotationCosting.flatFeeVendorCode"/>
					<!--  
						<td align="left"><s:textfield readonly="true" name="quotationCosting.flatFeeVendorCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('quotationVendor.html?sid=${serviceOrderID}&quoteType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=quotationCosting.flatFeeName&fld_code=quotationCosting.flatFeeVendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
						<td align="left"><s:textfield readonly="true" name="quotationCosting.flatFeeName" required="true"  cssClass="input-textUpper" size="10" onmousemove="magnify_content(this);"/></td>					
						<td align="left"><s:textfield readonly="true" name="quotationCosting.flatFeeChargeCode" required="true"  cssClass="input-textUpper" size="5" onmousemove="magnify_content(this);"/></td>
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=quotationCosting.flatFeeChargeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						<td align="left"><s:textfield name="quotationCosting.flatFeeReference" required="true"  cssClass="input-text" size="7" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.flatFeeQuantity" required="true"  cssClass="input-text" size="4" onkeydown="return onlyFloatNumsAllowed(event)" onchange="flatFeeDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.flatFeeRate" required="true"  cssClass="input-text" cssStyle="width:35px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="flatFeeDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:select name="quotationCosting.flatFeeDivMult"  list="{'/','*'}" cssStyle="width:15px" cssClass="list-menu" onchange="flatFeeDropdownCalculation();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.flatFeeUnit" required="true"  cssClass="input-text" cssStyle="width:30px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="flatFeeCalc();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.flatFeeExpense" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalExpense();" onmousemove="magnify_content(this);"/></td>
						<td align="center"><s:checkbox name="quotationCosting.flatFeePassThrough" value="${flatFeePassThrough}" fieldValue="true" onclick="passFlatFee();" onmousemove="magnify_content(this);"/></td>
						<td align="left"><s:textfield name="quotationCosting.flatFeeRevenueAmount" required="true"  cssClass="input-text" size="7" onkeydown="return onlyFloatNumsAllowed(event)" onchange="totalRevenue();" onmousemove="magnify_content(this);"/></td>
					-->
					</tr>
					<tr><td align="left" height="5px" class="subcontent-tab" width="650px" colspan="17"></td></tr>
					<tr>    
							<%--  <td><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.add"/></td> --%>
							
							<td align="left" colspan="4"><fmt:message key="jobcosting.total"/></td>
							
							<td align="left"><s:textfield name="quotationCosting.totalRevenueAmount" required="true"  cssClass="input-textUpper" readonly="true" size="7" onmousemove="magnify_content(this);"/></td>
							<td align="left" colspan="1"></td>
							<!-- 
							<td align="left" colspan="1"><input type="button" class="cssbuttonActive" name="Calc" value="Calc"  onclick="reCalculation();"/></td>
							<td align="left" colspan="1"><s:textfield name="quotationCosting.totalExpense" required="true"  cssClass="input-textUpper" readonly="true" size="7" onmousemove="magnify_content(this);"/></td>
						   -->	 
					</tr>
					
					<!-- 
					<tr>
							<td align="right" colspan="3"><fmt:message key="jobcosting.priceApproved"/></td>
							<td align="left" colspan="9"><s:checkbox name="quotationCosting.priceApproved" value="${priceApproved}" fieldValue="true"/></td>
							<td align="right" colspan="2"><fmt:message key="jobcosting.grossMargin"/></td>
							<td align="left"><s:textfield name="quotationCosting.grossRevenueAmount" required="true"  cssClass="input-textUpper" readonly="true" size="7" onmousemove="magnify_content(this);"/></td>
					</tr>
					
					<tr>	<td align="right" colspan="10"></td>
							
							<td align="right" colspan="2"><input type="button" class="cssbuttonActive" name="Calc" value="Calc"  onclick="reCalculation();"/></td>
							
							<td align="right" colspan="2"><fmt:message key="jobcosting.grossMarginPer"/></td>
							<td align="left"><s:textfield name="quotationCosting.grossMarginPer" required="true"  cssClass="input-textUpper" readonly="true" size="7" onmousemove="magnify_content(this);"/></td>
					</tr>
					-->
					
				</tbody>
			</table>
		  </td></tr>
 		  
 		 
 		 <tr><td colspan="3">
 		  <table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='quotationCosting.createdOn'/></td>
						<s:hidden name="quotationCosting.createdOn" />
						<td><s:date name="quotationCosting.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='quotationCosting.createdBy' /></td>
						
						<td>
						<c:if test="${not empty quotationCosting.id}">
							<s:label name="createdBy" value="%{quotationCosting.createdBy}"/>
							<s:hidden name="quotationCosting.createdBy"/>
						</c:if>
						<c:if test="${empty quotationCosting.id}">
							<s:label name="createdBy" value="${pageContext.request.remoteUser}"/>
							<s:hidden name="quotationCosting.createdBy" value="${pageContext.request.remoteUser}" />
						</c:if>
						</td>
						
						<td align="right" class="listwhitetext"><fmt:message key='quotationCosting.updatedOn'/></td>
						<s:hidden name="quotationCosting.updatedOn"/>
						<td><s:date name="quotationCosting.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='quotationCosting.updatedBy' /></td>
						<s:hidden name="quotationCosting.updatedBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="quotationCosting.updatedBy" /></td>
					</tr>
					
				</tbody>
			</table></td></tr>
 		 
 		 
 		 
  </tbody>
  </table>
  
        
        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save"/> 
        <td></td> 
         <!-- 
         <s:reset type="button" cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.reset"/>-->
  
		
		<s:hidden name="firstDescription"/>
		<s:hidden name="secondDescription"/>
		<s:hidden name="thirdDescription"/>
		<s:hidden name="fourthDescription"/>
		<s:hidden name="fifthDescription"/>
		<s:hidden name="sixthDescription"/>
		<s:hidden name="magnifier"/>
		<s:hidden name="quotationCosting.corpID"/>
		<s:hidden name="quotationCosting.insuranceRevenueAmount"/>
		
		
</s:form>   
  
<script type="text/javascript">   
    Form.focusFirstElement($("quotationCostingForm"));   
</script>