package com.trilasoft.app.dao.hibernate;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.velocity.runtime.directive.Parse;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessResourceFailureException;

import com.trilasoft.app.dao.RuleActionsDao;
import com.trilasoft.app.dao.hibernate.CustomerFileDaoHibernate.DTOMerge;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.RuleActions;


public class RuleActionsDaoHibernate extends GenericDaoHibernate<RuleActions, Long> implements RuleActionsDao{ 	private List toDoRules;
	
private HibernateUtil hibernateUtil;

public void setHibernateUtil(HibernateUtil hibernateUtil) {
	this.hibernateUtil = hibernateUtil;
}

public RuleActionsDaoHibernate() {   
    super(RuleActions.class);   
    }

public List getFilterName(String sessionCorpID ,Long userId){
	String queryfilter="";
	queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
List tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
return 	tempuserfilter;
}

public List<RuleActions> getActionList(Long ruleId) {
			return getHibernateTemplate().find("from RuleActions where ruleId='"+ruleId+"' and id in (select id  from RuleActions where ruleId='"+ruleId+"')");
	
}
public <RuleActions> List getToDoRuleByRuleNumber(Long ruleId, String corpId)
{
List<RuleActions>  list =  (List<RuleActions>)getHibernateTemplate().find("from RuleActions  where ruleId='"+ruleId+"' and corpID='"+corpId+"' and status=true");
System.out.println("listttttt--------RuleActions----------"+list);
return list;
}
public void updateToDoRole(String messageDisplayed,String roleList, Long id,String corpId) {
	String messagedisplayed="";
	List size=new ArrayList();
	try{
	size = this.getSession().createSQLQuery("select messagedisplayed from todoruleactions where corpID='"+corpId+"' and ruleId='"+id+"'").list();
		messagedisplayed=size.get(0).toString();
	}
	catch(Exception e)
	{
		
	}
	getHibernateTemplate().bulkUpdate("update ToDoRule set messagedisplayed ='"+messagedisplayed+"',rolelist='"+roleList+"' where id=?",id);
	
}
public void updateStatus(String activestatus,Long ruleId,Long id)
{
	getHibernateTemplate().bulkUpdate("update RuleActions set status="+activestatus+" where ruleId='"+ruleId+"' and id='"+id+"'" );

}
}


