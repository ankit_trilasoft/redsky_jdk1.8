package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CompensationSetup;

public interface CompensationSetupManager extends GenericManager<CompensationSetup,Long> {
	
	public List getListFromCompensationSetup(String corpId, Long contractId,String contractName);

}
