<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>Process XML List</title>   
    <meta name="heading" content="Process XML List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   

  <s:form id="xmlForm" method="post" validate="true">
<display:table name="processedXMLList" class="table" requestURI="" id="processedXMLList" pagesize="100" style="width:100%;margin-bottom:0px;"  size="100"> 
 			 <display:column title=""> 
				<img id="plus_${processedXMLList_rowNum}" onclick ="openAccountLineArea('${processedXMLList.fileName }','C',this,this.id,'${processedXMLList.transactionId }')"" src="${pageContext.request.contextPath}/images/plus-small.png" style="display: block;" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				<img id="sub_${processedXMLList_rowNum}" onclick ="closeAccountLineArea(this)"" src="${pageContext.request.contextPath}/images/minus-small.png" ALIGN=TOP style="display: none;" />
			   <c:if test="${processedXMLList.accIdList!='null'}">
			    <input type="hidden" id="accId${processedXMLList.fileName}"  name="accId${processedXMLList.fileName}" value="${processedXMLList.accIdList}" />
			    </c:if>
			</display:column> 
			<display:column property="fileName" title="File Name" />
		 <display:column title ="File Type">
				<c:if test="${fn:startsWith(processedXMLList.fileName, 'rt')}">
					Rating
				</c:if>
				<c:if test="${fn:startsWith(processedXMLList.fileName, 'ds')}">
					Distribution
				</c:if>
			</display:column>
			<display:column property="processedOn" title="Processed Date" format="{0,date,dd-MMM-yyyy}"/>  
			<display:column title ="Processed Time" value="${fn:substring(processedXMLList.batchId, 8, 100)}" />
			<display:column> <input type="checkbox" value="${processedXMLList.fileName }" id="${processedXMLList.transactionId }" onclick ="return updateFileNo(this)"/></display:column>
</display:table>
</s:form> 
