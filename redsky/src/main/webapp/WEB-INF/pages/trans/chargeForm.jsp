<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="chargeDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='chargeDetail.heading'/>"/>   
<style>
input[type="radio"] { vertical-align:bottom; !vertical-align:middle;}
.subcontenttabChild { background: url("images/greylinebg.gif") repeat scroll 0 0 #DCDCDC; font-family: Arial,Helvetica,sans-serif;font-size: 12px;font-weight: bold;height:22px;
    line-height: 1.5em;padding: 2px 3px 0 5px;text-decoration: none;}
.rt-chk {vertical-align: middle;margin-top: -1px;}
</style>
</head>     
<s:form id="chargeForm" name="chargeForm" action="saveCharges.html?btnType=redirect" method="post" validate="true" onsubmit="return  submit_chargeForm();">   
<s:hidden name="btnType" value="<%=request.getParameter("btnType")%>" />
<c:set var="btnType" value="<%=request.getParameter("btnType")%>"/> 
<s:hidden name="countCharges" value="${countCharges}"/>
<s:hidden name="charges.id" value="%{charges.id}"/>
<s:hidden name="charges.corpID"/>
<s:hidden name="charges.EstimateActual"/>
<s:hidden name="charges.realprice"/>
<s:hidden name="charges.realwords"/>
<s:hidden name="charges.price1"/>
<s:hidden name="charges.extraestimatepreset"/>
<s:hidden name="charges.extrarevisedpreset"/>
<s:hidden name="charges.quantity2source"/>
<s:hidden name="charges.item2"/>
<s:hidden name="charges.quantity2Type"/>
<s:hidden name="charges.quantityType"/>
<s:hidden name="charges.sortGroup"/>
<s:hidden name="charges.systemFunction"/>
<s:hidden name="charges.tariff"/>
<s:hidden name="charges.extra1"/>
<s:hidden name="charges.extraItemEstimate"/>
<s:hidden name="charges.extraItemRevised"/>
<s:hidden name="charges.extraEstimate"/>
<s:hidden name="charges.extraRevised"/>
<s:hidden name="charges.extraEstimateSource"/>
<s:hidden name="charges.extraRevisedSource"/>
<s:hidden name="costElementFlag1"/>
<s:hidden name="submitCheck" value="T"/>
<s:hidden name="contractId" value="<%=request.getParameter("contractId")%>"/> 
<c:set var="contractId" value="<%=request.getParameter("contractId")%>"/> 
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="rateGridZeroCount" value="%{rateGridCount }" />
<c:choose>
<c:when test="${gotoPageString == 'gototab.chargeList' }">
    <c:redirect url="/charge23.html?id=${contractId}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.contract' }">
    <c:redirect url="/searchContracts.html"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.glType' }">
    <c:redirect url="/glCommissionTypeList.html?contract=${charges.contract}&charge=${charges.charge}&glId=${charges.id}&contractId=${contractId}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<div id="layer4" style="width:100%;">
<div id="newmnav">
	<ul>
	  	<li><a onmouseover="return chkSelect(),divideMultiplyOfCharges();" onclick="setReturnString('gototab.chargeList');return autoSave();"><span>Charge List</span></a></li>
	  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Charge Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	    <li><a href="editContract.html?id=${contractId}" onclick="setReturnString('gototab.chargeList');return autoSave();" ><span>Contract Details</span></a></li>
	    <li><a onmouseover="return chkSelect(),divideMultiplyOfCharges();" onclick="setReturnString('gototab.contract');return autoSave();"><span>Contract List</span></a></li>
	    <c:if test="${chargeDiscountFlag}">
	    <li><a href="discountList.html?contractId=${contractId}&chargeId=${charges.id}&contract=${charges.contract}&chargeCode=${charges.charge}"><span>Discount</span></a></li>
	    </c:if>
	    <li><a onmouseover="return chkSelect(),divideMultiplyOfCharges();" onclick="setReturnString('gototab.glType');return autoSave();"><span>GL&nbsp;Type</span></a></li>
	    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${charges.id}&tableName=charges&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>	    
  	</ul>
	</div><div class="spn">&nbsp;</div>
</div>

 <div id="Layer1" onkeydown="changeStatus();" style="width:100%">
 <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content"> 
  <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
		<tbody>	
  <tr>
  <td align="right" class="listwhitetext" width="45px"><fmt:message key="charges.charge"/><font color="red" size="2">*</font></td>
  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.charge" size="31" onchange="checkChargeForContractAvailability();" onfocus = "myDate();" maxlength="25" tabindex=""/></td>  
    <!--<td align="right" class="listwhitetext" width="50px"><fmt:message key="charges.estimateActual"/></td>-->
    <!--<td><s:select cssClass="list-menu" name="charges.estimateActual" list="{'A','E','N'}" cssStyle="width:45px"/></td>-->
    <td align="right" class="listwhitetext"><fmt:message key="charges.description"/><font color="red" size="2">*</font></td>  
    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.description" size="45"  onkeypress="return checkSpecialCharacter(event);" onkeyup="checkChar();" onchange="checkChar();" maxlength="45" tabindex="" /></td>
   
 	<td align="right" class="listwhitetext">Contract&nbsp;Name</td>  
    <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="charges.contract" size="40"  maxlength="45" tabindex="" readonly="true" /></td>
 	<td align="right" class="listwhitetext">Status</td>  
	<td align="left" ><s:checkbox key="charges.status" cssStyle="margin:0px;"  fieldValue="true"  onclick="changeStatus()" /></td>
  	</tr>
  	<tr>
 	<td align="right" class="listwhitetext" ><fmt:message key="charges.bucket"/></td>  
    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.bucket" size="8"  maxlength="7" tabindex=""/></td>
    
    <td align="right" class="listwhitetext" width="75">Basis</td>
	<td align="left" class="listwhitetext"><s:select cssClass="list-menu" onchange="changeStatus();" name="charges.basis" list="%{basis}" headerKey=" " headerValue=" " cssStyle="width:85px"></s:select></td>
    
   
    <configByCorp:fieldVisibility componentId="component.field.execSaleAdditionalComm">
 	<td align="right" class="listwhitetext">Exclude Sales Additional Commission</td>  
	<td align="left" ><s:checkbox key="charges.excludeSalesAdditionalCommission" cssStyle="margin:0px;"  fieldValue="true"  onclick="changeStatus()" /></td>
	</configByCorp:fieldVisibility>
   </tr>
     </tbody>
</table>
</td>
</tr>
<tr>
<td height="10" width="100%" align="left" >
<div  onClick="javascript:animatedcollapse.toggle('ledger')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;General Ledger and Control Data
</td>
<td width="28px" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
<div  id="ledger">
<table border="0" cellspacing="0" cellpadding="3" style="margin:0px;">
<tbody>
<tr>
	<tr><td height="2px"></td></tr>
			     <c:if test="${costElementFlag==true}">
		<tr>
		<td class="listwhitetext" align="right">Cost Element<font color="red" size="2">*</font></td>
		<td class="listwhitetext" align="left"><s:textfield cssClass="input-text" size="15" maxlength="40" name="charges.costElement" onchange="changeStatus();makePopulateCostDescription();" onkeydown="return enterNotAllow(event);"/>
		<c:if test ="${charges.corpID == contracts.owner}">
			<img id="charges.costPopup" class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="openBookingAgentPopWindow();document.forms['chargeForm'].elements['charges.costElement'].focus();" src="<c:url value='/images/open-popup.gif'/>" />		
		</c:if></td>
		<td class="listwhitetext" align="right">Description</td>		
		<td class="listwhitetext" align="left" colspan="2">
		<s:textfield cssClass="input-text" size="35" maxlength="50" name="charges.scostElementDescription" onchange="changeStatus();makePopulateCostElement()" onkeydown="return enterNotAllow(event);"/></td>
		 <td align="right" class="listwhitetext">Receivable&nbsp;Currency</td>
		 <td><s:select cssClass="list-menu" name="charges.contractCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:60px" onchange="changeStatus();" /></td>
		 <td align="right" class="listwhitetext">Payable&nbsp;Currency</td>
		 <td><s:select cssClass="list-menu" name="charges.payableContractCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:60px" onchange="changeStatus();" /></td>
		 <configByCorp:fieldVisibility componentId="component.field.Tax.GST">
		 <td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;HSN/SAC&nbsp;Code</td>
		 <td><s:select cssClass="list-menu" name="charges.GSTHSNCode" list="%{GSTHSNCodeMap}" headerKey="" headerValue="" cssStyle="width:80px" onchange="changeStatus();" /></td>
	     </configByCorp:fieldVisibility>
		</tr>
		<tr>
    	<td align="right" class="listwhitetext" ><fmt:message key="charges.gl"/></td>  
	    <td align="left"><s:textfield cssClass="input-textUpper" size="15" name="charges.gl" readonly="true"/></td>
	    <td align="right" class="listwhitetext"><fmt:message key="charges.expGl"/></td>  
	    <td align="left"><s:textfield cssClass="input-textUpper" size="15" name="charges.expGl" readonly="true"/></td>
	    </tr>
				</c:if>
			     <c:if test="${costElementFlag!=true}">
		<tr>
		<td class="listwhitetext" align="right"></td>
		<td class="listwhitetext" align="left"><s:hidden name="charges.costElement"/></td>		
		<td class="listwhitetext" align="right"></td>		
		<td class="listwhitetext" align="left"><s:hidden name="charges.scostElementDescription"/></td>
		</tr>
		<tr>
    	<td align="right" class="listwhitetext" width=""><fmt:message key="charges.gl"/></td>  
	    <td align="left"><s:select cssClass="list-menu" id="glcodes" name="charges.gl" list="%{glcodes}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
	    <td align="right" class="listwhitetext"><fmt:message key="charges.expGl"/></td>  
	    <td align="left"><s:select cssClass="list-menu" id="glcodess" name="charges.expGl" list="%{glcodes}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
	     <td align="right" class="listwhitetext">Receivable&nbsp;Currency</td>
		 <td><s:select cssClass="list-menu" name="charges.contractCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:80px" onchange="changeStatus();" /></td>
	     <td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;Payable&nbsp;Currency</td>
		 <td><s:select cssClass="list-menu" name="charges.payableContractCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:80px" onchange="changeStatus();" /></td>
	     <configByCorp:fieldVisibility componentId="component.field.Tax.GST">
		 <td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;HSN/SAC&nbsp;Code</td>
		 <td><s:select cssClass="list-menu" name="charges.GSTHSNCode" list="%{GSTHSNCodeMap}" headerKey="" headerValue="" cssStyle="width:80px" onchange="changeStatus();" /></td>
	     </configByCorp:fieldVisibility>
	    </tr>
	</c:if>

    <tr><td height="2px"></td></tr>
    <tr>
     <td align="left" colspan="20" class="vertlinedata" style="position:absolute;width:95%;"></td>
    </tr>
    <tr>
    <td align="right" class="listwhitetext" colspan="">Mode</td>
 <td align="left"><s:select cssClass="list-menu" name="charges.mode" list="%{mode}"  cssStyle="width:110px" onchange="changeStatus();"/></td> <c:set var="isVipFlag" value="false"/>
								<c:if test="${charges.storageFlag}">
									<c:set var="isVipFlag" value="true"/>
								</c:if>
						<td align="right" class="listwhitetext" colspan="">Storage</td>
				      	<td align="left" ><s:checkbox key="charges.storageFlag" value="${isVipFlag}" fieldValue="true" onclick="changeStatus()" cssStyle="margin:0px;" /></td>
    				      	<td align="right" class="listwhitetext" width=""><fmt:message key="charges.storageType"/></td>  
                        <td align="left" width="60"><s:select cssClass="list-menu" name="charges.storageType" list="%{storageType}" headerKey="" headerValue="" cssStyle="width:80px" onchange="changeStatus();"/></td>

                    <td colspan="5" align="right">                    
                    <table class="detailTabLabel">
                    <tr>
                    <c:set var="isIncludeFlag" value="false"/>
								<c:if test="${charges.includeLHF}">
									<c:set var="isIncludeFlag" value="true"/>
								</c:if>
						<td align="right" class="listwhitetext" colspan="">&nbsp;Include&nbsp;in&nbsp;LHF</td>
				      	<td align="left" ><s:checkbox key="charges.includeLHF" value="${isIncludeFlag}"  fieldValue="true"  onclick="changeStatus()" /></td>
    
                       <c:set var="isPrintFlag" value="false"/>
								<c:if test="${charges.printOnInvoice}">
									<c:set var="isPrintFlag" value="true"/>
								</c:if>
						<td align="right" class="listwhitetext" colspan="">Don't&nbsp;Print&nbsp;on&nbsp;Invoice</td>
				      	<td align="left"><s:checkbox key="charges.printOnInvoice" value="${isPrintFlag}" fieldValue="true"  onclick="changeStatus()" /></td>
				      	
				      	<c:set var="isRollUpInvoiceFlag" value="false"/>
								<c:if test="${charges.rollUpInInvoice}">
									<c:set var="isRollUpInvoiceFlag" value="true"/>
								</c:if>
						<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
							<td align="right" class="listwhitetext" colspan="">Roll&nbsp;Up&nbsp;In&nbsp;Invoice</td>
					      	<td align="left"><s:checkbox key="charges.rollUpInInvoice" value="${isRollUpInvoiceFlag}" fieldValue="true"  onclick="changeStatus()" /></td>
    				 	</configByCorp:fieldVisibility>
    				 </tr>
    				 </table>
    				 <td>         
 </tr>
 <tr>
 <td align="right" class="listwhitetext" colspan="">Origin&nbsp;Country</td>
 <td align="left"><s:select cssClass="list-menu" name="charges.originCountry" list="%{ocountry}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();"/></td>
 <td align="right" class="listwhitetext" colspan="" >&nbsp;Destination&nbsp;Country</td>
 <td align="left"><s:select cssClass="list-menu" name="charges.destinationCountry" list="%{dcountry}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();"/></td>
   
  <td colspan="2" align="right" >                    
  <table cellspacing="0" cellpadding="0" class="detailTabLabel" style="margin:0px;padding:0px">
  <tr>
  <td align="right" class="listwhitetext" >Comm%&nbsp;&nbsp;<%-- <fmt:message key="charges.commission"/> --%></td> 
  <td align="left"><s:textfield cssClass="input-text" name="charges.commission" size="16"  maxlength="6"  cssStyle="width:76px;margin-right:21px;" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
   <configByCorp:fieldVisibility componentId="component.field.Charge.EarnoutShow">
 	<td align="right" class="listwhitetext" colspan="">&nbsp;&nbsp;Earnout%&nbsp;</td>
 	<td align="left"><s:textfield cssClass="input-text" name="charges.earnout" size="16"  maxlength="6"  cssStyle="width:76px" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
 </configByCorp:fieldVisibility> 
 </tr>
 </table>
 </td>
  <td colspan="5">                    
   <table class="detailTabLabel">
   <tr>
 <td align="right" class="listwhitetext" colspan="">Commissionable</td>
 <td align="left"><s:checkbox name="charges.commissionable" value="${charges.commissionable }" /></td>
  <td width="98" align="right" colspan="" class="listwhitetext">Country&nbsp;Flexibility</td>
 <td align="right"><s:checkbox name="charges.countryFlexibility" value="${charges.countryFlexibility }" /></td>
 <configByCorp:fieldVisibility componentId="component.tab.commission.compensation">
 <td align="right" class="listwhitetext" colspan="">&nbsp;&nbsp;&nbsp;&nbsp;Compensation</td>
 <td align="left"><s:checkbox name="charges.compensation" value="${charges.compensation}" /></td>
 </configByCorp:fieldVisibility>
 </tr>
 </table>
 </td>
 </tr>
 <configByCorp:fieldVisibility componentId="component.field.charge.servicedetail.show">
 <tr>
 <td align="left" class="listwhitetext" >Service&nbsp;Group&nbsp;-&nbsp;Detail</td> 
  <td align="left" colspan="9"><s:textfield cssClass="input-text" name="charges.servicedetail" size="15"  maxlength="35"  cssStyle="width:100px;"/></td>
   
 </tr> 
 </configByCorp:fieldVisibility>
</tbody>
	</table>
	</div>
		</td>
	</tr>
<tr>
	<td height="10" width="100%" align="left" >
	<div  onClick="javascript:animatedcollapse.toggle('all')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left"
</td>
<td class="headtab_center" >&nbsp;Charge Calculation (Quantity*Price [adjustment factor])
</td>
<td width="28px" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
	
	
	<div id="all" style="width:100%;" >
	<table border="0" style="width:100%;margin-bottom:7px;" cellpadding="0" cellspacing="0">
<tr>
	<td height="10" width="100%" align="left" >
	<div class="subcontenttabChild"  style="cursor: pointer;margin: 0px;width:99%;padding-bottom:0px;">
	&nbsp;Primary Quantity&nbsp;&nbsp;( Entitlement )&nbsp;<font style="margin-left:155px;">Primary Quantity&nbsp;&nbsp;( Estimate )</font>&nbsp;<font style="margin-left:110px;">Primary Quantity&nbsp;&nbsp;( Revised/Actual)</font>
	</div>
	
	<div id="quantity" style="width:100%">
	<table border="0" class="detailTabLabel">
<tbody>
<tr>
<td style="width:300px">
<table class="detailTabLabel">
<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>Unit</b> &nbsp;
	<s:textfield cssStyle="text-align:right;width:155px;" cssClass="input-text" name="charges.item" size="26"  maxlength="20"tabindex="" /> 
    
</tr>
<c:choose>
	<c:when test="${charges.quantity1 == 'AskUser0'}" >
		<tr>
		<td align="left" class="listwhitetext"><input  type="radio" checked="checked" name="charges.quantity1" value="AskUser0" onclick="changeStatus();" tabindex="">Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantity1" value="Preset0" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" name="charges.quantityPreset" size="16" onblur="return isFloat(this);" onkeyup="valid(this,'special')" onkeydown="return onlyNumsAllowed(event)" onclick="selectRadioButton('charges.quantity1','Preset0');" maxlength="8" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantity1" value="FromField0" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantitySource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantity1','FromField0');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.quantity1 == 'Preset0'}" >
		<tr>
		<td align="left" class="listwhitetext"><input  type="radio"  name="charges.quantity1" value="AskUser0" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.quantity1" value="Preset0" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext">
		<s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityPreset" size="16" onblur="return isFloat(this);" onkeyup="valid(this,'special')" onkeydown="return onlyNumsAllowed(event)"  onclick="selectRadioButton('charges.quantity1','Preset0');" maxlength="8" tabindex="" /></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantity1" value="FromField0" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantitySource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantity1','FromField0');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.quantity1 == 'FromField0'}" >
		<tr>
		<td align="left" class="listwhitetext"><input  type="radio" checked="checked" name="charges.quantity1" value="AskUser0" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantity1" value="Preset0" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext">
		<s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityPreset" size="16"  onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" onclick="selectRadioButton('charges.quantity1','Preset0');" maxlength="8" tabindex="" /></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.quantity1" value="FromField0" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantitySource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantity1','FromField0');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:otherwise>
		<tr>
		<td align="left" class="listwhitetext"><input  type="radio" name="charges.quantity1" value="AskUser0" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantity1" value="Preset0" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext">
		<s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityPreset" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onclick="selectRadioButton('charges.quantity1','Preset0');" onkeyup="valid(this,'special')" size="16"  maxlength="8" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantity1" value="FromField0" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantitySource"  list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantity1','FromField0');" cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 
	</c:otherwise>
</c:choose>
</table>
</td>
<td style="width:300px">
<table class="detailTabLabel">
<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>Unit</b> &nbsp; 
	<s:textfield cssStyle="text-align:right;width:155px;" cssClass="input-text" name="charges.quantityItemEstimate" size="26"  maxlength="20" tabindex=""/> 
    <!--<s:select cssClass="list-menu" id="units" name="charges.quantityItemEstimate" list="{'','Lbs','Kg','Each','CFT','CBM'}" cssStyle="width:110px" onchange="changeStatus();"/></td>
--></tr>
<c:choose>
	<c:when test="${charges.quantityEstimate == 'AskUser1'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser1" type="radio" checked="checked" name="charges.quantityEstimate" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityEstimate" value="Preset1" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext">
		<s:textfield cssClass="input-text" onclick="selectRadioButton('charges.quantityEstimate','Preset1');" cssStyle="text-align:right;width:106px;" key="charges.quantityEstimatePreset" size="16" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityEstimate" value="FromField1" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantityEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantityEstimate','FromField1');" cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.quantityEstimate == 'Preset1'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser1" type="radio"  name="charges.quantityEstimate" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.quantityEstimate" value="Preset1" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext">
		<s:textfield cssStyle="text-align:right;width:106px;" cssClass="input-text" key="charges.quantityEstimatePreset" size="16" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onclick="selectRadioButton('charges.quantityEstimate','Preset1');" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityEstimate" value="FromField1" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantityEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantityEstimate','FromField1');" cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.quantityEstimate == 'FromField1'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser1" type="radio" checked="checked" name="charges.quantityEstimate" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityEstimate" value="Preset1" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityEstimatePreset" size="16"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onclick="selectRadioButton('charges.quantityEstimate','Preset1');" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.quantityEstimate" value="FromField1" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantityEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantityEstimate','FromField1');"cssStyle="width:110px" onchange="changeStatus(); " tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:otherwise>
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser1" type="radio" name="charges.quantityEstimate" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityEstimate" value="Preset1" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityEstimatePreset" onclick="selectRadioButton('charges.quantityEstimate','Preset1');" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" size="16"  maxlength="8" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityEstimate" value="FromField1" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantityEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantityEstimate','FromField1');" cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 
	</c:otherwise>
</c:choose>
</table>
</td>
<td style="width:300px">
<table class="detailTabLabel">
<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>Unit</b> &nbsp;
	<s:textfield cssStyle="text-align:right;width:155px;" cssClass="input-text" name="charges.quantityItemRevised" size="26"  maxlength="20" tabindex=""/>  
    <!--<s:select cssClass="list-menu" id="units" name="charges.quantityItemRevised" list="{'','Lbs','Kg','Each','CFT','CBM'}" cssStyle="width:110px" onchange="changeStatus();"/></td>
--></tr>
<c:choose>
	<c:when test="${charges.quantityRevised == 'AskUser2'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser2" type="radio" checked="checked" name="charges.quantityRevised" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityRevised" value="Preset2" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext">
		<s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityRevisedPreset" size="16"  maxlength="8" onkeyup="valid(this,'special')" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onclick="selectRadioButton('charges.quantityRevised','Preset2');" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityRevised" value="FromField2" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantityRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantityRevised','FromField2');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.quantityRevised == 'Preset2'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser2" type="radio"  name="charges.quantityRevised" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.quantityRevised" value="Preset2" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right;width:106px;" cssClass="input-text" key="charges.quantityRevisedPreset" size="16"  maxlength="8" onkeyup="valid(this,'special')" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onclick="selectRadioButton('charges.quantityRevised','Preset2');" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityRevised" value="FromField2" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext">
		<s:select cssClass="list-menu" name="charges.quantityRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " onkeyup="valid(this,'special')" onclick="selectRadioButton('charges.quantityRevised','FromField2');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.quantityRevised == 'FromField2'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser2" type="radio" checked="checked" name="charges.quantityRevised" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityRevised" value="Preset2" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityRevisedPreset" size="16"  maxlength="8" onkeyup="valid(this,'special')" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onclick="selectRadioButton('charges.quantityRevised','Preset2');" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.quantityRevised" value="FromField2" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantityRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " onkeyup="valid(this,'special')" onclick="selectRadioButton('charges.quantityRevised','FromField2');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 		
	</c:when>
	<c:otherwise>
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser2" type="radio" name="charges.quantityRevised" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityRevised" value="Preset2" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantityRevisedPreset" size="16"  maxlength="8" onblur="return isFloat(this);" onkeyup="valid(this,'special')" onkeydown="return onlyNumsAllowed(event)" onclick="selectRadioButton('charges.quantityRevised','Preset2');" tabindex=""/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.quantityRevised" value="FromField2" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantityRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.quantityRevised','FromField2');"cssStyle="width:110px" onchange="changeStatus();" tabindex=""/></td>
		</tr> 
	</c:otherwise>
</c:choose>
</table>
</td>
</tr>
</tbody>
	</table>
	</div>
	</td>
	</tr>
	
	<%--
	/* by ashish Extra data for Formula is not use in future we can use 
	<tr>
	<td height="10" width="100%" align="left" >
	
	<div  onClick="javascript:animatedcollapse.toggle('formula')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
  <td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Extra data for Formula&nbsp;( Entitlement )&nbsp;<font style="margin-left:80px;">Extra data for Formula&nbsp;( Estimate )</font>&nbsp;<font style="margin-left:100px;">Extra data for Formula&nbsp;( Revised/Actual )</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_small" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
	
	<div id="formula" style="width:100%">
	<table border="0">
<tbody>
<tr>
<td style="width:300px">
<table>
<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>Unit</b> &nbsp; 
	<s:textfield cssStyle="text-align:right" cssClass="input-text" name="charges.item2" size="26"  maxlength="20" tabindex="24"/> 
    <!--<s:select cssClass="list-menu" id="units" name="charges.item2" list="{'','Lbs','Kg','Each','CFT','CBM'}" cssStyle="width:110px" onchange="changeStatus();"/></td>
--></tr>
<c:choose>
	<c:when test="${charges.extra1 == 'AskUser'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" checked="checked" name="charges.extra1" onclick="changeStatus();" tabindex="25"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extra1" value="Preset" onclick="changeStatus();" tabindex="26">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.pricePre" size="16"  maxlength="45" tabindex="27"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extra1" value="FromField" onclick="changeStatus();" tabindex="28">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantity2source" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="29"/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.extra1 == 'Preset'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio"  name="charges.extra1" onclick="changeStatus();" tabindex="25"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.extra1" value="Preset" onclick="changeStatus();" tabindex="26">Preset <td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right" cssClass="input-text" key="charges.pricePre" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" size="16"  maxlength="8" tabindex="27"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extra1" value="FromField" onclick="changeStatus();" tabindex="28">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantity2source" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="29"/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.extra1 == 'FromField'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" checked="checked" name="charges.extra1" onclick="changeStatus();" tabindex="25"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extra1" value="Preset" onclick="changeStatus();" tabindex="26">Preset <td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right" cssClass="input-text" key="charges.pricePre" size="16"  onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="27"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.extra1" value="FromField" onclick="changeStatus();" tabindex="28">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantity2source" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="29"/></td>
		</tr> 		
	</c:when>
	<c:otherwise>
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" name="charges.extra1" onclick="changeStatus();" tabindex="25"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extra1" value="Preset" onclick="changeStatus();" tabindex="26">Preset <td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right" cssClass="input-text" key="charges.pricePre" size="16"  onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8"tabindex="27" /></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extra1" value="FromField" onclick="changeStatus();" tabindex="28">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.quantity2source" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="29"/></td>
		</tr> 
	</c:otherwise>
</c:choose>
</table>
</td>
<td style="width:300px">
<table>
<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>Unit</b> &nbsp; 
	<s:textfield cssStyle="text-align:right" cssClass="input-text" name="charges.extraItemEstimate" size="26"  maxlength="20" tabindex="30"/> 
    <!--<s:select cssClass="list-menu" id="units" name="charges.extraItemEstimate" list="{'','Lbs','Kg','Each','CFT','CBM'}" cssStyle="width:110px" onchange="changeStatus();"/></td>
--></tr>
<c:choose>
	<c:when test="${charges.extraEstimate == 'AskUser'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" checked="checked" name="charges.extraEstimate" onclick="changeStatus();" tabindex="31"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraEstimate" value="Preset" onclick="changeStatus();" tabindex="32">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="charges.extraEstimatePreset" size="16"  onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="33"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraEstimate" value="FromField" onclick="changeStatus();" tabindex="34">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="35"/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.extraEstimate == 'Preset'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio"  name="charges.extraEstimate" onclick="changeStatus();" tabindex="31"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.extraEstimate" value="Preset" onclick="changeStatus();" tabindex="32">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.extraEstimatePreset" cssStyle="text-align:right" size="16"  onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="33"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraEstimate" value="FromField" onclick="changeStatus();" tabindex="34">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="35"/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.extraEstimate == 'FromField'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" checked="checked" name="charges.extraEstimate" onclick="changeStatus();" tabindex="31"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraEstimate" value="Preset" onclick="changeStatus();" tabindex="32">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.extraEstimatePreset" size="16"  onblur="return isFloat(this);" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="33"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.extraEstimate" value="FromField" onclick="changeStatus();" tabindex="34">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="35"/></td>
		</tr> 		
	</c:when>
	<c:otherwise>
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" name="charges.extraEstimate" onclick="changeStatus();" tabindex="31"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraEstimate" value="Preset" onclick="changeStatus();" tabindex="32">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.extraEstimatePreset" size="16" cssStyle="text-align:right" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="33"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraEstimate" value="FromField" onclick="changeStatus();" tabindex="34">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraEstimateSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="35"/></td>
		</tr> 
	</c:otherwise>
</c:choose>
</table>
</td>
<td style="width:300px">
<table>
<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>Unit</b> &nbsp; 
	<s:textfield cssStyle="text-align:right" cssClass="input-text" name="charges.extraItemRevised" size="26"  maxlength="20" tabindex="36"/> 
    <!--<s:select cssClass="list-menu" id="units" name="charges.extraItemRevised" list="{'','Lbs','Kg','Each','CFT','CBM'}" cssStyle="width:110px" onchange="changeStatus();"/></td>
--></tr>
<c:choose>
	<c:when test="${charges.extraRevised == 'AskUser'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" checked="checked" name="charges.extraRevised" onclick="changeStatus();" tabindex="37"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraRevised" value="Preset" onclick="changeStatus();">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.extraRevisedPreset" size="16"  onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" /></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraRevised" value="FromField" onclick="changeStatus();">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();"/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.extraRevised == 'Preset'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio"  name="charges.extraRevised" onclick="changeStatus();" tabindex="37"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.extraRevised" value="Preset" onclick="changeStatus();" tabindex="38">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="charges.extraRevisedPreset" size="16" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="39"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraRevised" value="FromField" onclick="changeStatus();" tabindex="40">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="41"/></td>
		</tr> 		
	</c:when>
	<c:when test="${charges.extraRevised == 'FromField'}" >
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" checked="checked" name="charges.extraRevised" onclick="changeStatus();" tabindex="37"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraRevised" value="Preset" onclick="changeStatus();" tabindex="38">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.extraRevisedPreset" size="16"  onblur="return isFloat(this);" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="39"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" checked="checked" name="charges.extraRevised" value="FromField" onclick="changeStatus();" tabindex="40">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();"tabindex="41"/></td>
		</tr> 		
	</c:when>
	<c:otherwise>
		<tr>
		<td align="left" class="listwhitetext"><input value="AskUser" type="radio" name="charges.extraRevised" onclick="changeStatus();" tabindex="37"> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraRevised" value="Preset" onclick="changeStatus();" tabindex="38">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.extraRevisedPreset" size="16"  onblur="return isFloat(this);" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex="39"/></td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext"><input type="radio" name="charges.extraRevised" value="FromField" onclick="changeStatus();" tabindex="40">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.extraRevisedSource" list="%{quantityField}" headerKey=" " headerValue=" " cssStyle="width:110px" onchange="changeStatus();" tabindex="41"/></td>
		</tr> 
	</c:otherwise>
</c:choose>
</table>
</td>
</tr>
</tbody>
	</table>
	</div>
</td>
</tr>


	
<tr>
by ashish end the coment 
--%>
<%-- <tr>
	<td height="10" width="100%" align="left" >
		<div class="subcontenttabChild"  style="cursor: pointer;margin: 0px;width:99%;padding-bottom:0px;">
			&nbsp;Secondary Quantity (Distance)
		</div>
		<div id="quantity" style="width:100%">
		<table border="0" class="detailTabLabel" >
		<tbody>
			<tr>
				<td style="width:300px">
					<table class="detailTabLabel">
						<tr>
							<td align="right" class="listwhitetext" width="28"></td>
							<td align="left" class="listwhitetext" colspan="2"><b>2D Unit</b> &nbsp;
							<s:select cssClass="list-menu" name="charges.twoDGridUnit" list="%{twoDType}" cssStyle="width:105px;height:20px" onchange="selectRadioButton('charges.priceType','RateGrid');" /> 
   								
   							 </td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
		</div>
	</td>
</tr> --%>
<tr>
<td height="10" width="100%" align="left" >
  <div class="subcontenttabChild" style="cursor: pointer;margin: 0px;width:99%;">
&nbsp;Price&nbsp;&nbsp; &nbsp;
<font style="margin-left:71px;">Receivable&nbsp;&nbsp; </font>
<font style="margin-left:275px;"> Payable &nbsp;&nbsp;</font>
</div>	
	
<div id="price" >
<table border="0" style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0"><tr><td>
<table border="0" style="margin:0px;padding:0px;" >
<tbody>
<tr>
<c:set var="ischecked" value="false"/>
				<c:if test="${charges.checkBreakPoints}">
				<c:set var="ischecked" value="true"/>
				</c:if>
<c:choose>
	<c:when test="${charges.priceType == 'AskUser'}" >
		<tr>
		<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio" checked="checked" name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:106px;" key="charges.quantity2preset" size="16" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onclick="selectRadioButton('charges.priceType','Preset')" onkeyup="valid(this,'special')" maxlength="8" tabindex="" onchange="changeStatus();"/></td>
		
		</tr>
		<%-- <tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.priceType','FromField')"cssStyle="width:110px"  onchange="changeStatus();"tabindex=""/></td>
		</tr> --%>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" value="RateGrid" onclick="changeStatus(); " tabindex="">Rate Grid</td>
		
		
		</tr> 
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
		
		</tr>		
	</c:when>
	<c:when test="${charges.priceType == 'Preset'}" >
		<tr>
			<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio"  name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" checked="checked" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.quantity2preset" size="16" cssStyle="text-align:right;width:106px;" onclick="selectRadioButton('charges.priceType','Preset')" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
			
		</tr>
		<%-- <tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.priceType','FromField')"cssStyle="width:110px"  onchange="changeStatus();" tabindex=""/></td>
		</tr> --%>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
			
			
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
			
		
		</tr>		
	</c:when>
	<c:when test="${charges.priceType == 'FromField'}" >
		<tr>
		<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio" checked="checked" name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.quantity2preset" size="16" onclick="selectRadioButton('charges.priceType','Preset')" onblur="return isFloat(this);" cssStyle="text-align:right;width:106px;" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
		
		</tr>
		<%-- <tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" checked="checked" value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.priceType','FromField')"cssStyle="width:110px"  onchange="changeStatus();" tabindex=""/></td>
		</tr> --%>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
		
		
		
		</tr> 
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
		
		</tr>		
	</c:when>
	<c:when test="${charges.priceType == 'RateGrid'}" >
		<tr>
		<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio" checked="checked" name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.quantity2preset" size="16" onclick="selectRadioButton('charges.priceType','Preset')" cssStyle="text-align:right;width:106px;" onblur="return isFloat(this);"onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')"  maxlength="8" tabindex=""/></td>
		
		</tr>
		<%-- <tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType"  value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" "onclick="selectRadioButton('charges.priceType','FromField')" cssStyle="width:110px"  onchange="changeStatus();" tabindex=""/></td>
		</tr> --%>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" checked="checked" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
		
		
		</tr> 
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
		
		
		</tr>		
	</c:when>
	<c:when test="${charges.priceType== 'FromTariff'}" >
		<tr>
		<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio" checked="checked" name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.quantity2preset" size="16" cssStyle="text-align:right;width:106px;" onclick="selectRadioButton('charges.priceType','Preset')" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
		
		</tr>
		<%-- <tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" checked="checked" value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" "onclick="selectRadioButton('charges.priceType','FromField')" cssStyle="width:110px"  onchange="changeStatus();" tabindex=""/></td>
		</tr> --%>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
		
		
		</tr> 
		
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" onclick="changeStatus();" tabindex="">Build Formula </td>
		
		
		</tr>		
	</c:when>
	<c:when test="${charges.priceType == 'BuildFormula'}" >
		<tr>
			<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio" checked="checked" name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="43">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.quantity2preset" size="16" onclick="selectRadioButton('charges.priceType','Preset')" cssStyle="text-align:right;width:106px;" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
			
		</tr>
		<%-- <tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.priceType','FromField')" cssStyle="width:110px"  onchange="changeStatus();" tabindex=""/></td>
		</tr> --%>
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
		
		
		
		</tr>
		
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" checked="checked" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
		
		
		</tr>		
	</c:when>
	<c:otherwise>
		<tr>
			<td align="left" class="listwhitetext" width="90" style="padding-left:5px;"><input value="AskUser" type="radio" name="charges.priceType" onclick="changeStatus();" tabindex=""> Ask User?</td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="Preset" onclick="changeStatus();" tabindex="">Preset <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.quantity2preset" size="16"  onclick="selectRadioButton('charges.priceType','Preset')" cssStyle="text-align:right;width:106px;" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="8" tabindex=""/></td>
			
		</tr>
		<%-- <tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="FromField" onclick="changeStatus();" tabindex="">From Field <td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.priceSource" list="%{priceField}" headerKey=" " headerValue=" " onclick="selectRadioButton('charges.priceType','FromField')" cssStyle="width:110px"  onchange="changeStatus();" tabindex=""/></td>
		</tr> --%>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.priceType" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
			
			
			
			</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;"><input type="radio" name="charges.priceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
			
			
		
		</tr>
	</c:otherwise>
</c:choose>
</tr>

<tr>
<td  colspan="4">
<table style="margin:0px;padding:0px;">
<tr>
<td  align="left" class="listwhitetext" style="padding-left:5px;"><s:textarea name="charges.priceFormula" cssClass="textarea" wrap="10" rows="5" cols="60" onclick="selectRadioButton('charges.priceType','BuildFormula')" tabindex=""/></td>
<td align="left" class="listwhitetext"><input type="button" class="cssbutton1" name="TestFormula" value="Test Formula" style="width:90px;height:25px" onclick="return checktextArea('charges.priceType','charges.priceFormula','BuildFormula');" tabindex=""/></td>
</tr>
</table>
</td>
</tr>
</tbody>
</table></td>
<td valign="top" class="vertlinedata_vert"></td>
<td valign="top">
<table style="margin:0px;padding:0px;">
	<tbody>
		<tr>
			<td align="left" style="height:17px;"></td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;width:75px;">
				<input type="radio" name="charges.payablePriceType" value="Preset" onclick="changeStatus();" tabindex=""/>Preset 
			</td>
			<td align="left" class="listwhitetext" >
				<s:textfield cssClass="input-text" cssStyle="text-align:right" key="charges.payablePreset" size="16" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onclick="selectRadioButton('charges.payablePriceType','Preset')" onkeyup="valid(this,'special')" maxlength="8" tabindex="" onchange="changeStatus();"/>
			</td>
		</tr>
		
		<tr>
		<td align="left" class="listwhitetext" style="padding-left:5px;height:25px;"><input type="radio"  name="charges.payablePriceType" value="RateGrid" onclick="changeStatus();" tabindex="">Rate Grid</td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="padding-left:5px;" colspan="3"><input type="radio" name="charges.payablePriceType" value="BuildFormula" onclick="changeStatus();" tabindex="">Build Formula </td>
		</tr>
		
		<tr>
			<td  colspan="5" align="left" class="listwhitetext" style="padding-left:5px;margin-top:5px;"><s:textarea name="charges.expensePrice" cssStyle="margin-top:5px;" cssClass="textarea" wrap="10" rows="5" cols="60" onclick="selectRadioButton('charges.payablePriceType','BuildFormula')" tabindex=""/></td>
			<td align="left" class="listwhitetext"><input type="button" class="cssbutton1" name="TestFormula" value="Test Formula" style="width:90px;height:25px" onclick="return checktextArea('charges.payablePriceType','charges.expensePrice','BuildFormula');" tabindex=""/></td>
		</tr>
	</tbody>
</table>

</td>
</tr></table>

<table class="detailTabLabel" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<!-- <tr><td colspan="15" class="vertlinedata" align="center"></td></tr> -->
		<tr><td colspan="15">
		<div class="subcontenttabChild"  style="cursor: pointer;margin: 0px;width:99%;padding-bottom:0px;">
			&nbsp;Grid Setup
		</div>
		</td>
		</tr>
	</tbody>
</table>



<table style="margin:4px 0px 0px 0px; padding:0px;" border="0">
<tbody>
	<tr>

<td align="left" valign="top" class="listwhitetext" style="width:235px;">
<c:if test="${not empty charges.id}">
<input type="button" class="cssbutton" style="width:100px;margin-right:12px;" name="Rate Grid" value="Rate Grid" onclick="selectRadioButton('charges.priceType','RateGrid');selectRadioButton('charges.payablePriceType','RateGrid');openGrid();" tabindex="">
</c:if>
<s:checkbox key="charges.checkBreakPoints" value="${ischecked}" fieldValue="true" onclick="changeStatus();" cssClass="rt-chk"/>Use Break Points
</td>
<td align="right" class="listwhitetext" width="75">Incremental&nbsp;Step</td>
<td><input type="text" class="input-text" oncopy="return false" onpaste="return false" name="charges.incrementalStep" id="incrementalStep" value="${charges.incrementalStep}" maxlength="6" size="6" Style="width:40px;" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter()"></td>

	
	<td align="right" class="listwhitetext" style="padding-left:15px;">Secondary Quantity (Distance Unit)</td>
	<td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.twoDGridUnit" list="%{twoDType}" cssStyle="width:70px;" onchange="selectRadioButton('charges.priceType','RateGrid');" /> </td>
	</tr>
	<tr><td colspan="15" style="height:2px;"></td></tr>
</table>
 <table cellspacing="0" cellpadding="0" class="detailTabLabel" style="margin:0px 0px 10px 0px; padding:0px;width:83%">
 <tr>
 	 <td align="left" class="listwhitetext" style="width:115px;padding-left:3px;"><b>Multiplying Factor:</b></td>
	<c:if test="${empty charges.multquantity }" >
		<td align="left" class="listwhitetext"  id="none" style="width:75px;"><input type="radio" name="charges.multquantity" value="N" > None</input></td>
		<td align="left" class="listwhitetext" style="width:125px;"><input type="radio" name="charges.multquantity" value="P" checked="checked" > Primary Quantity </input></td>
		<td align="left" class="listwhitetext"  id="secondary" style="width:144px;"><input type="radio" name="charges.multquantity" value="S"  > Secondary Quantity </input></td>
		<td align="left" class="listwhitetext" id="both" colspan="3"><input type="radio" name="charges.multquantity" value="B" >Both Primary and Secondary  </input></td>
	</c:if>
	
	<c:if test="${charges.multquantity=='P'}" >
		<td align="left" class="listwhitetext"  id="none" style="width:75px;"><input type="radio" name="charges.multquantity" value="N" > None</input></td>
		<td align="left" class="listwhitetext" style="width:125px;"><input type="radio" name="charges.multquantity" value="P" checked="checked" > Primary Quantity </input></td>
		<td align="left" class="listwhitetext"  id="secondary" style="width:144px;"><input type="radio" name="charges.multquantity" value="S" > Secondary Quantity </input></td>
		<td align="left" class="listwhitetext" id="both" colspan="3"><input type="radio" name="charges.multquantity" value="B" >Both Primary and Secondary  </input></td>
	</c:if>
	<c:if test="${charges.multquantity=='S'}" >
		<td align="left" class="listwhitetext"  id="none" style="width:75px;"><input type="radio" name="charges.multquantity" value="N" > None</input></td>
		<td align="left" class="listwhitetext" style="width:125px;"><input type="radio" name="charges.multquantity" value="P"  > Primary Quantity </input></td>
		<td align="left" class="listwhitetext" style="width:144px;"><input type="radio" name="charges.multquantity" value="S" checked="checked" > Secondary Quantity </input></td>
		<td align="left" class="listwhitetext"  id="both" colspan="3"><input type="radio" name="charges.multquantity" value="B" >Both Primary and Secondary  </input></td>
	</c:if>
	<c:if test="${charges.multquantity=='N'}" >
		<td align="left" class="listwhitetext"  id="none" style="width:75px;"><input type="radio" name="charges.multquantity" value="N" checked="checked" > None</input></td>
		<td align="left" class="listwhitetext" style="width:125px;"><input type="radio" name="charges.multquantity" value="P"  > Primary Quantity </input></td>
		<td align="left" class="listwhitetext" style="width:144px;"><input type="radio" name="charges.multquantity" value="S" > Secondary Quantity </input></td>
		<td align="left" class="listwhitetext"  id="both" colspan="3"><input type="radio" name="charges.multquantity" value="B" >Both Primary and Secondary  </input></td>
	</c:if>
	<c:if test="${charges.multquantity=='B'}" >
		<td align="left" class="listwhitetext"  id="none" style="width:75px;"><input type="radio" name="charges.multquantity" value="N" > None</input></td>
		<td align="left" class="listwhitetext" style="width:125px;"><input type="radio" name="charges.multquantity" value="P"  > Primary Quantity </input></td>
		<td align="left" class="listwhitetext" style="width:144px;" ><input type="radio" name="charges.multquantity" value="S"  > Secondary Quantity </input></td>
		<td align="left" class="listwhitetext" id="both" colspan="3"><input type="radio" name="charges.multquantity" value="B" checked="checked" >Both Primary and Secondary  </input></td>
	</c:if>
	</tr>
	</table>	

</div>
</td>
</tr>

 <c:if test="${vatCalc==true}">
<tr>
<td height="10" width="100%" align="left" >
<div class="subcontenttabChild"  style="cursor: pointer;margin:0px;padding-bottom:0px;width:99%;">
	&nbsp;VAT
	</div>
	<div id="vat" >
<table border="0" class="detailTabLabel">
<tbody>
<tr>
 <c:set var="ischecked" value="false"/>
 <c:if test="${charges.VATExclude}">
 <c:set var="ischecked" value="true"/>
 </c:if>
 <td align="right" class="listwhitetext" width="80px" style="border:none;">VAT Excluded </td>
 <td><s:checkbox key="charges.VATExclude" value="${ischecked}" fieldValue="true"  /></td>
 </tr>
</tbody>
</table>
</div>
</td>
</tr>
</c:if>
<c:if test="${vatCalc!=true}">
<s:hidden name="charges.VATExclude" value="${charges.VATExclude}"/>
</c:if>
<tr>

<td height="10" width="100%" align="left" >
<div class="subcontenttabChild"  style="cursor: pointer;margin:0px;padding-bottom:0px;width:99%;">
	&nbsp;Amount&nbsp;Adjustment&nbsp;Factors
	</div>
	<div id="adjustment" >
<table class="detailTabLabel">
<tbody>

</tbody>
</table>
<table class="no-mp">
<tbody>
<%-- 

							<tr>
								<c:set var="d2dFlag" value="false"/>
								<c:if test="${charges.askUser}">
								<c:set var="d2dFlag" value="true"/>
								</c:if>
								<td align="right" class="listwhitetext" style="padding-left:5px;"><fmt:message key="charges.askUser"/></td>
								<td align="left" class="listwhitetext"><s:checkbox name="charges.askUser" value="${d2dFlag}" fieldValue="true" onclick="changeStatus();" tabindex=""/></td>
								
							</tr>
--%>
	
<tr>
		<td align="left" class="listwhitetext"></td>
		</tr>
		<tr>						
<tr>
	<td align="right" class="listwhitetext" style="padding-left:5px;width:80px;"><fmt:message key="charges.divideMultiply"/></td>
	<td align="left" class="listwhitetext"><s:select size="1px" name="charges.divideMultiply"  list="{'Division','Multiply'}" headerKey=" " headerValue=" " cssStyle="width:85px" cssClass="list-menu" onchange="changeStatus();" tabindex=""/></td>
	<td align="right" class="listwhitetext" style="width:42px;"><fmt:message key="charges.perValue"/></td>  
	<td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right" cssClass="input-text" key="charges.perValue" size="20" maxlength="500"tabindex="" onchange="return isFloat(this);"/></td>	    	 
	<td align="left" class="listwhitetext"></td>
	<td align="right" class="listwhitetext" style="width:50px;"><fmt:message key="charges.perItem"/></td>  
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="charges.perItem" size="20"  maxlength="500"tabindex="" /></td> 
	<td align="left" class="listwhitetext"></td>
	<td align="right" class="listwhitetext" style="width:50px;">Minimum</td> 
	<td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right" cssClass="input-text" key="charges.minimum" size="18" onblur="return isFloat(this);" onkeydown="return onlyNumsAllowed(event)" onchange="valid(this,'special')" maxlength="7" tabindex="" /></td>   
</tr>
</tbody>
</table>
</div>
</td>
</tr>
<configByCorp:fieldVisibility componentId="component.field.Charge.DeviationShow">
<tr>
<td height="10" width="100%" align="left" >
<div class="subcontenttabChild"  style="cursor: pointer;margin:0px;padding-bottom:0px;width:99%;">
	&nbsp;Deviation&nbsp;Factors
	</div>
	<div id="adjustment" >
<table class="detailTabLabel">
<tbody>
<tr>
	<td align="right" class="listwhitetext" style="padding-left:5px;"><b><fmt:message key="charges.deviation"/></b></td>
	<td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="charges.deviation" list="%{deviationTypeList}" cssStyle="width:180px;height:20px"  /> </td>
   	<c:if test="${not empty charges.id}"> 
   		<td align="left" class="listwhitetext">	<input type="button"  class="cssbutton1" name="sellRateDeviations" value="Sell Rate Country Deviation" style="width:170px;height:24px" onclick="openSellDeviation();"/></td> 						
		<td align="left" class="listwhitetext">	<input type="button"  class="cssbutton1" name="buyRateDeviations" value="Buy Rate Country Deviation" style="width:170px;height:24px" onclick="openBuyDeviation();"/></td> 
	</c:if>
	<c:if test="${empty charges.id}"> 
		<td align="left" class="listwhitetext">	<input type="button"  class="cssbutton1" name="sellRateDeviations" value="Sell Rate Country Deviation" style="width:170px;height:24px" onclick="openSellDeviation();" disabled="true"/></td> 						
		<td align="left" class="listwhitetext">	<input type="button"  class="cssbutton1" name="buyRateDeviations" value="Buy Rate Country Deviation" style="width:170px;height:24px" onclick="openBuyDeviation();" disabled="true" /></td> 
	</c:if>
</tr>
<tr>
	<td align="right" class="listwhitetext" style="padding-left:5px;" colspan="2">
			<s:hidden name="charges.buyDependSell" />
			<s:checkbox name="buyDependSell" value="${charges.buyDependSell}" fieldValue="true" onclick="setDependValue(this);changeStatus();" cssStyle="vertical-align:middle;" />
			Buy Rate dependent on Sell Rate
			</td>
			
</tr>

</tbody>
</table>
</div>
</td>
</tr>
</configByCorp:fieldVisibility> 
</table>
</div>
</td></tr>

<tr>
<td height="10" width="100%" align="left" >
<div  onClick="javascript:animatedcollapse.toggle('vanline')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;VanLine Factors
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		

	
	<div id="vanline" >

<table border="0" cellspacing="3" cellpadding="1" style="margin-bottom:5px;">
<tbody>
<tr>
<c:set var="ischecked100" value="false"/>
<c:if test="${charges.service}">
<c:set var="ischecked100" value="true"/>
</c:if>
<td align="right" class="listwhitetext">Charge&nbsp;Cost&nbsp;Level</td>  
<td align="left"><s:select cssClass="list-menu" id="glcodess" name="charges.otherCost" list="%{costLevel}" headerKey=" " headerValue=" " cssStyle="width:150px" onchange="changeStatus();" tabindex=""/></td>
<td align="right" class="listwhitetext">Apply&nbsp;Discount</td>  
<td align="left"><s:select cssClass="list-menu" id="glcodess" name="charges.useDiscount" list="%{discount}" headerKey=" " headerValue=" " cssStyle="width:150px" onchange="changeStatus();" tabindex=""/></td>
<td align="right" class="listwhitetext" style="padding-left:5px;"></td> 
<td align="right"  class="listwhitetext">Get&nbsp;Service&nbsp;Area&nbsp;Data</td>
<td align="left" valign="" class="listwhitetext"><s:checkbox key="charges.service" value="${ischecked100}" fieldValue="true" onclick="changeStatus();" tabindex="" cssStyle="margin:0px;" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext" style="padding-left:5px;">Item</td>  
<td align="left"><s:textfield cssClass="input-text" key="charges.item400n" cssStyle="width:146px" size="24"  maxlength="10" tabindex=""/></td>
<td align="right" class="listwhitetext">SubItem</td>  
<td align="left"><s:textfield cssClass="input-text" key="charges.subItem" cssStyle="width:146px" size="24"  maxlength="10" tabindex=""/></td>

</tr>
</tbody>
</table>
</div>
</td>
</tr>
<tr>
<td height="10" width="100%" align="left" >

<div  onClick="javascript:animatedcollapse.toggle('amount')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Amount Description
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
	
	<div id="amount" >
<table border="0">
<tbody>
<tr>
	<c:set var="originFlag" value="false"/>
			
	  		<c:if test="${charges.useOptionalWording}">
	  			<c:set var="originFlag" value="true"/>
	  		</c:if>
	<td align="right"><s:checkbox name="charges.useOptionalWording" value="${originFlag}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
	<td align="left" class="listwhitetext"><fmt:message key="charges.useOptionalWording"/></td>  
	
</tr><tr><td></td></tr>
<tr>
<td colspan="6" style="padding-left:84px;"><s:textarea cssClass="textarea" name="charges.wording"  rows="5" cols="90"  tabindex=""/>
<p id='test'></p>
</td>
<td align="left" class="listwhitetext"><input type="button"  class="cssbutton1" name="TestWording" value="Test Wording" style="width: 100px;height:25px" onclick="testWordingUsingAction();" tabindex=""/></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>

<c:if test="${charges.contract=='Driver Settlement'}">
<tr>
	<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('settlement')" style="margin: 0px"  >
			<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
				<tr>
					<td class="headtab_left"></td>
					<td class="headtab_center" >&nbsp;Settlement Frequency</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div>		
		<div id="settlement" >
			<table border="0">
				<tbody>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td colspan="3">
						<table border="0" style="margin:0px;padding:0px;">
						<tr>
						<td align="right" width="20px"></td>
						<td align="right" class="listwhitetext">Deduction Type</td>
						<td class="listwhitetext" valign="top" ><s:radio name="charges.deductionType" list="{'Per Pay','Per Trip','Per Schedule'}" onclick="changeStatus(),checkFeq(this);"/></td>
						</tr>
						</table>
					
					</td>
					</tr>
					
					<tr id="hid">
						<td align="right" width="20px"></td>
						<td colspan="8">
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;">
						<tr>
						<td align="right" valign="top"  class="listwhitetext">Frequency</td>
						<td align="right" width="3px"></td>
						<td class="listwhitetext" valign="top"><s:select cssClass="list-menu"  name="charges.frequency" list="%{contact_frequency}" cssStyle="width:120px"  headerKey="" headerValue="" onchange="changeStatus();return changeFeq();"/></td>
						</tr>
						</table>
						</td>
					</tr>
					
					
					<tr id="hid1">
					<td align="right" width="20px"></td>
					<td colspan="8">					
					<fieldset style="padding-top:4px;">
					<legend>Schedule Detail</legend>
					<table class="detailTabLabel" cellspacing="2" cellpadding="2">
					<tr>
						<c:if test="${not empty charges.id}">
							<td colspan="9" class="listwhitetext-hcrew" style="padding-left:10px; padding-right:10px;border-radius:4px;height:24px;line-height:12px;">Work Days :
								<s:radio name="charges.workDay" list="%{workDays}"/>
							</td>
						</c:if>
						<c:if test="${empty charges.id}">
							<td colspan="9" class="listwhitetext-hcrew" style="padding-left:10px; padding-right:10px;border-radius:4px;height:24px;line-height:12px;">Work Days :
								<s:radio name="charges.workDay" list="%{workDays}" key="6"/>
							</td>
						</c:if>
					</tr>
					<tr>
						<td align="right" width="0px"></td>
						<c:if test="${charges.week1}">
				  			<c:set var="week1" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.week1" id="week1" value="${week1}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Week 1</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.jan}">
				  			<c:set var="jan" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.jan" value="${jan}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Jan</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.jul}">
				  			<c:set var="jul" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.jul" value="${jul}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Jul</td>
					</tr>
					<tr>
						<td align="right" width="0px"></td>
						<c:if test="${charges.week2}">
				  			<c:set var="week2" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.week2" id="week2" value="${week2}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Week 2</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.feb}">
				  			<c:set var="feb" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.feb" value="${feb}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Feb</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.aug}">
				  			<c:set var="aug" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.aug" value="${aug}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Aug</td>
					</tr>
					<tr>
					
						<td align="right" width="0px"></td>
						<c:if test="${charges.week3}">
				  			<c:set var="week3" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.week3" id="week3" value="${week3}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Week 3</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.mar}">
				  			<c:set var="mar" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.mar" value="${mar}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Mar</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.sep}">
				  			<c:set var="sep" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.sep" value="${sep}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Sep</td>
					</tr>
					<tr>
						<td align="right" width="0px"></td>
						<c:if test="${charges.week4}">
				  			<c:set var="week4" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.week4" id="week4" value="${week4}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Week 4</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.apr}">
				  			<c:set var="apr" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.apr" value="${apr}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Apr</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.oct}">
				  			<c:set var="oct" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.oct" value="${oct}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Oct</td>
					</tr>
					<tr>
						<td align="right" width="0px"></td>
						<c:if test="${charges.week5}">
				  			<c:set var="week5" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.week5" id="week5" value="${week5}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Week 5</td>
						<td align="right" width="20px"></td>
					
						<c:if test="${charges.may}">
				  			<c:set var="may" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.may" value="${may}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">May</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.nov}">
				  			<c:set var="nov" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.nov" value="${nov}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Nov</td>
					</tr>
					
					<tr>
						<td align="right" width="0px"></td>
						<c:if test="${charges.week6}">
				  			<c:set var="week6" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.week6" id="week6" value="${week6}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Week 6</td>
						<td align="right" width="20px"></td>
					
						<c:if test="${charges.jun}">
				  			<c:set var="jun" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.jun" value="${jun}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Jun</td>
						<td align="right" width="20px"></td>
						
						<c:if test="${charges.decem}">
				  			<c:set var="dec" value="true"/>
				  		</c:if>
						<td align="right"><s:checkbox name="charges.decem" value="${dec}" fieldValue="true" onclick="changeStatus();" tabindex=""/>
						<td align="left" class="listwhitetext">Dec</td>
					</tr>
					</table>
					</fieldset>
					</td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
	</td>
</tr>
</c:if>

<tr height="20px" ></tr>
	</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	</div>
	
	  <table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px;"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<td valign="top"></td>
							<td style="width:120px">
								<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${charges.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="charges.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${charges.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty charges.id}">
								<s:hidden name="charges.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{charges.createdBy}"/></td>
							</c:if>
							<c:if test="${empty charges.id}">
								<s:hidden name="charges.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${charges.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="charges.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${charges.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty charges.id}">
								<s:hidden name="charges.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{charges.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty charges.id}">
								<s:hidden name="charges.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" onclick="return fillDescription();" onmouseover="return chkSelect();" method="save" key="button.save" tabindex=""/>   
        <s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" onclick="setPayableRadioButton();" onmousemove="myDate();" tabindex=""/>
   <c:if test="${not empty charges.id}"> 
         <input type="button" class="cssbutton1" style="width:100px; height:25px" onclick="location.href='<c:url   value="/copyCharge.html?id=${charges.id}&contractId=${contractId}"/>'" value="<fmt:message  key="button.copyCharge"/>" style="width:110px; height:25px"/> 
        </c:if> 
        <c:if test="${btnType == 'redirect' }">
	<c:redirect url="/editCharges.html?id=${charges.id }&contractId=${contractId }"/>
</c:if> 
</s:form>  

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('all', 'fade=1,show=1,')
	animatedcollapse.addDiv('ledger', 'fade=1,hide=1')
	animatedcollapse.addDiv('formula', 'fade=1,hide=1')
	animatedcollapse.addDiv('vanline', 'fade=1,hide=1')
	animatedcollapse.addDiv('amount', 'fade=1,hide=1,')
	animatedcollapse.addDiv('settlement', 'fade=1,hide=1,')
	animatedcollapse.init()
</script> 

<script type="text/javascript">
setTimeout("selectRadioButtonOnLoad('charges.payablePriceType','${charges.payablePriceType}')",2000);
function setPayableRadioButton(){
	setTimeout("selectRadioButtonOnLoad('charges.payablePriceType','${charges.payablePriceType}')",1000);
}
function selectRadioButtonOnLoad(groupName,fieldValue){
	 var radioGroup = document['chargeForm'][groupName];
	 for (var i=0; i<radioGroup.length; i++){
	    if(radioGroup[i].value == fieldValue){
	    	radioGroup[i].checked=true;
	    }
	 }
} 
</script>

 <script language="javascript">
 function openGrid(){
   var twoDG=document.forms['chargeForm'].elements['charges.twoDGridUnit'].value 
   window.open('rateGrid.html?chid=${charges.id}&contract=${charges.contract}&decorator=popup&popup=true&twoDGridUnit='+twoDG,'','width=670,height=550,scrollbars=yes');
}

 function openSellDeviation(){
	 window.open('countryDeviations.html?charge=${charges.id}&contract=${charges.contract}&deviationType=sell&decorator=popup&popup=true&btn=sell','','width=600,height=550,scrollbars=yes');
}  
 function openBuyDeviation(){
	 window.open('countryDeviations.html?charge=${charges.id}&contract=${charges.contract}&deviationType=buy&decorator=popup&popup=true&btn=buy','','width=600,height=550,scrollbars=yes');
}  
 function check2D(){
	 var twoDG=document.forms['chargeForm'].elements['charges.twoDGridUnit'].value; 
	 var costElementFlag1=document.forms['chargeForm'].elements['costElementFlag1'].value;
	 costElementFlag1=costElementFlag1.trim();
	  if(twoDG=='' || twoDG=='ND'){
		var countNonZeroGrid =document.forms['chargeForm'].elements['rateGridZeroCount'].value; 
		if(countNonZeroGrid > 0){
		alert("Since Distance is available in the Grid, Please choose the 2D Unit.");
		return false;
	 	} else{
	 	 if(costElementFlag1=='YES'){
			return checkCostElement();
		}else{
			return true;
			}
		} 
	 	} 
		else{
		 if(costElementFlag1=='YES'){
			return checkCostElement();
			}else{
			return true;
			}
		} 
}
 function checkCostElement(){
	 var costEl=document.forms['chargeForm'].elements['charges.costElement'].value;
	 if(costEl.trim()=='')
	 {
		 alert("Cost Element is required field.");
		 document.forms['chargeForm'].elements['charges.costElement'].focus();
		 return false;
	 }
	 else
	 {
		 return true;
	 }
 }
 /*function selectForMultiplingFactor(){
	 var chkedData = document.forms['chargeForm'].elements['charges.twoDGridUnit'].value;
	 var fact1 = document.getElementById("secondaryFactor1");
	 var fact2= document.getElementById("secondaryFactor2");
	 if(chkedData == 'ND'){
	 		fact1.style.display = 'block';
	 		fact2.style.display = 'none';
	 }
	 else{
	 		fact1.style.display = 'none';
	 		fact2.style.display = 'block';
	 }
} */

 function setDependValue(target){
	 var val= target.checked;
	 if(val==true){
		document.forms['chargeForm'].elements['charges.buyDependSell'].value='Y'
	}else{
		 document.forms['chargeForm'].elements['charges.buyDependSell'].value='N' 
	}
}
 function checkedBuySell(){
	var buySellValue =document.forms['chargeForm'].elements['charges.buyDependSell'].value;
	if(buySellValue=='Y'){
		document.forms['chargeForm'].elements['buyDependSell'].checked=true;
	}else{
		document.forms['chargeForm'].elements['buyDependSell'].checked=false;
	}
}
 </script>
<script language="javascript" type="text/javascript">
    function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
	
	function displayGrid(){
		var d1=document.forms['chargeForm'].elements['charges.tariff'].value;
		if(d1=='Contract'){
			document.getElementById("rateGrid6").style.display="block";
		}else{
			document.getElementById("rateGrid6").style.display="none";
		}
	}
	
	/* 
	 function selectRadioButton2(){
		var xx=document.forms['chargeForm'].elements['charges.quantityEstimate'];
		if(document.forms['chargeForm'].elements['charges.quantityEstimateSource'].value!=' '){
			xx[2].checked=true;
		}
	} 
	
	 function selectRadioPreset2(){
		var xx=document.forms['chargeForm'].elements['charges.quantityEstimate'];
		if(document.forms['chargeForm'].elements['charges.quantityEstimatePreset'].value!=' '){
			xx[1].checked=true;
		}
	} 
	
	 function selectRadioButton1(){
		var xx=document.forms['chargeForm'].elements['charges.quantity1'];
		if(document.forms['chargeForm'].elements['charges.quantitySource'].value!=' '){
			xx[2].checked=true;
		}
	} 
	 function selectRadioPreset1(){
		var xx=document.forms['chargeForm'].elements['charges.quantity1'];
		if(document.forms['chargeForm'].elements['charges.quantityPreset'].value!=' '){
			xx[1].checked=true;
		}
	} 
	
	
	 function selectRadioButton3(){
		var xx=document.forms['chargeForm'].elements['charges.quantityRevised'];
		xx[2].checked=true;
	} 
	
	 function selectRadioPreset33(){
		var xx=document.forms['chargeForm'].elements['charges.quantityRevised'];
		xx[1].checked=true;
	} 
	
 	function selectRadioforPricePreset(){
		var xx=document.forms['chargeForm'].elements['charges.priceType'];
		xx[1].checked=true;
	} 
 	function selectRadioforPriceFromField(){
		var xx=document.forms['chargeForm'].elements['charges.priceType'];
		xx[2].checked=true;
	} 
 function selectRadioforPriceRateGrid(){
		var xx=document.forms['chargeForm'].elements['charges.priceType'];
		xx[3].checked=true;
	} 
	
 function selectRadioforPriceBuildFormula(groupName,fieldName){
		 var radioGroup = document['chargeForm'][groupName];
		 for (var i=0; i<radioGroup.length; i++){
		    if(radioGroup[i].value == fieldName){
		    	radioGroup[i].checked=true;
		    }
		 }
	} 
	 */
	
	function selectRadioButton(groupName,fieldValue){
		 var radioGroup = document['chargeForm'][groupName];
		 for (var i=0; i<radioGroup.length; i++){
		    if(radioGroup[i].value == fieldValue){
		    	radioGroup[i].checked=true;
		    	
		    }
		 }
	}
	
	
	
	function testFor(){
		var charge = document.forms['chargeForm'].elements['charges.charge'].value;
		var billingContract = document.forms['chargeForm'].elements['charges.contract'].value;
		document.forms['chargeForm'].action = 'testFormula.html?charge='+charge+'&billingContract='+billingContract;
		document.forms['chargeForm'].submit();
	}
	
	function validField(fld) {
		var charge = document.forms['chargeForm'].elements['charges.quantity1'].value;
		return false; // test for only one decimal point
	// other validations for this field to be added here
		return true;
	}
 function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	}
	
</script>

<script>
function testChargeFormula(expressionFormula){
     var charge = document.forms['chargeForm'].elements['charges.charge'].value;
	 var billingContract = document.forms['chargeForm'].elements['charges.contract'].value;
//	 var expressionFormula=document.forms['chargeForm'].elements['charges.priceFormula'].value;
	 var fromField=document.forms['chargeForm'].elements['charges.quantitySource'].value;
	
	 for(var i=0;i<100;i++){
	  	expressionFormula = expressionFormula.replace("+", "%2B");
	 }
	
	 var url="testFormula.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+"&expressionFormula=" +expressionFormula;
    
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponsetestFormula;
     http2.send(null);
	}
	
function handleHttpResponsetestFormula(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       alert(results);
    }
}
	
function testWordingUsingAction(){
//alert(document.forms['chargeForm'].elements['charges.priceFormula'].value);
     var id = '${charges.id}';
     var wording = document.forms['chargeForm'].elements['charges.wording'].value;
	 var perValue = document.forms['chargeForm'].elements['charges.perValue'].value;
	 var minimumValue=document.forms['chargeForm'].elements['charges.minimum'].value;
	 var description=document.forms['chargeForm'].elements['charges.description'].value;
	 var formQuantity=document.forms['chargeForm'].elements['charges.item'].value;
	 var discountValue=document.forms['chargeForm'].elements['charges.useDiscount'].value;
	 for(var i=0;i<100;i++){
	  	wording = wording.replace("+", "~");
	 }
	 for(var i=0;i<100;i++){
		  	wording = wording.replace("&", "`");
		 }
	 var url="testWording.html?ajax=1&decorator=simple&popup=true&wording=" + encodeURIComponent(wording)+"&perValue=" + encodeURI(perValue)+"&minimumValue=" +encodeURI(minimumValue)+"&description="+encodeURI(description)+"&formQuantity="+encodeURI(formQuantity)+"&discountValue="+encodeURI(discountValue)+"&id="+encodeURI(id);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponsetestWording;
     http2.send(null);
	}

function handleHttpResponsetestWording(){
             if (http2.readyState == 4){
              var results =http2.responseText;
              $("#test").html(results);
              results = $("#test").text();
              $("#test").hide()
              results =results.trim();
                alert(results);
             }
        }

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();
    
    
    function isFloat(targetElement){   
    	var i;
	var s = targetElement.value;
	if(s=='.'){
		 alert("Only numbers are allowed here");
	        document.getElementById(targetElement.id).value='';
	        document.getElementById(targetElement.id).select();
	        return false;
		}else{
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        if (c == ".") {
        
        }else{
        alert("Only numbers are allowed here");
        //alert(targetElement.id);
        document.getElementById(targetElement.id).value='';
        document.getElementById(targetElement.id).select();
        return false;
        }
    }
    }
	}
    return true;
}


function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110) || (keyCode==67) || (keyCode==86); 
	}
	
	
	
function checktextArea(groupName,fieldId,fieldValue){
		 var radioGroup = document['chargeForm'][groupName];
		 for (var i=0; i<radioGroup.length; i++){
		    if(radioGroup[i].value == fieldValue){
		    	if(radioGroup[i].checked != true){
		    		alert('Please select the Build Formula Radio button');
					return false;
		    	}else if(radioGroup[i].checked == true){
		    		var formula=document.forms['chargeForm'].elements[fieldId].value
		    	 	formula=formula.trim();
		    		if(formula==""){
				  		alert('Please enter the formula in the text Area');
				  		return false;
			    	 }
		    		var expressionFormula=document.forms['chargeForm'].elements[fieldId].value;
		    		testChargeFormula(expressionFormula);
		    	}
		    }
		 }
}
	var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

function chkSelect(){
	for (var i=0; i < document.forms['chargeForm'].elements['charges.deductionType'].Length; i++){
		if (document.forms['chargeForm'].elements['charges.deductionType'][i].checked){
		   var rad_val = document.forms['chargeForm'].elements['charges.deductionType'][i].value;
		   if(rad_val == 'Per Schedule'){
		   
		   		var week1 = document.forms['chargeForm'].elements['charges.week1'].checked;
				var week2 = document.forms['chargeForm'].elements['charges.week2'].checked;
				var week3 = document.forms['chargeForm'].elements['charges.week3'].checked;
				var week4 = document.forms['chargeForm'].elements['charges.week4'].checked;
				var week5 = document.forms['chargeForm'].elements['charges.week5'].checked;
				var week6 = document.forms['chargeForm'].elements['charges.week6'].checked;
				if(week1==false && week2==false && week3==false && week4==false && week5==false && week6==false){
					alert('Please select atleast one week for per schedule');
					return false;
				}
					
				var m1 = document.forms['chargeForm'].elements['charges.jan'].checked;
				var m2 = document.forms['chargeForm'].elements['charges.feb'].checked;
				var m3 = document.forms['chargeForm'].elements['charges.mar'].checked;
				var m4 = document.forms['chargeForm'].elements['charges.apr'].checked;
				var m5 = document.forms['chargeForm'].elements['charges.may'].checked;
				var m6 = document.forms['chargeForm'].elements['charges.jun'].checked;
				var m7 = document.forms['chargeForm'].elements['charges.jul'].checked;
				var m8 = document.forms['chargeForm'].elements['charges.aug'].checked;
				var m9 = document.forms['chargeForm'].elements['charges.sep'].checked;
				var m10 = document.forms['chargeForm'].elements['charges.oct'].checked;
				var m11 = document.forms['chargeForm'].elements['charges.nov'].checked;
				var m12 = document.forms['chargeForm'].elements['charges.decem'].checked;
				
				if(m1 == false && m2 == false && m3 == false && m4 == false && m5 == false && m6 == false && m7 == false && m8 == false && m9 == false && m10 == false && m11 == false && m12 == false ){
					alert('Please select atleast one month for per schedule');
					return false;
				}
		   }
		}
	}

           
		if (checkFloat('chargeForm','charges.quantityPreset','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.quantityPreset'].focus();
              return false
           }
          if (checkFloat('chargeForm','charges.quantityEstimatePreset','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.quantityEstimatePreset'].focus();
              return false
           }
          if (checkFloat('chargeForm','charges.quantityRevisedPreset','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.quantityRevisedPreset'].focus();
              return false
           }
           
           if (checkFloat('chargeForm','charges.pricePre','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.pricePre'].focus();
              return false
           }
           
           if (checkFloat('chargeForm','charges.extraEstimatePreset','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.extraEstimatePreset'].focus();
              return false
           }
           
           if (checkFloat('chargeForm','charges.extraRevisedPreset','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.extraRevisedPreset'].focus();
              return false
           }
           
           if (checkFloat('chargeForm','charges.quantity2preset','Invalid data in Preset') == false)
           {
              document.forms['chargeForm'].elements['charges.quantity2preset'].focus();
              return false
           }
           if (checkFloat('chargeForm','charges.payablePreset','Invalid data in Preset payable') == false)
           {
              document.forms['chargeForm'].elements['charges.payablePreset'].focus();
              return false
           }
           
           
           if (checkFloat('chargeForm','charges.perValue','Invalid data in Value') == false)
           {
              document.forms['chargeForm'].elements['charges.perValue'].focus();
              return false
           }
           
           if (checkFloat('chargeForm','charges.minimum','Invalid data in Minimum') == false)
           {
              document.forms['chargeForm'].elements['charges.minimum'].focus();
              return false
           }
}
	
	
function changeStatus(){
    document.forms['chargeForm'].elements['formStatus'].value = '1';
}
	
	function autoSave(clickType){
		var costEl=document.forms['chargeForm'].elements['charges.costElement'].value;
		 var costElementFlag1=document.forms['chargeForm'].elements['costElementFlag1'].value;
	if(((document.forms['chargeForm'].elements['charges.charge'].value).ltrim() == '') || ((document.forms['chargeForm'].elements['charges.description'].value).ltrim() == '')){
		alert('Please enter new charge code and description.');
		return false;
	}
	 if((costElementFlag1.trim()=='YES')&&(costEl.trim()==''))
	 {
		 alert("Cost Element is required field.");
		 document.forms['chargeForm'].elements['charges.costElement'].focus();
		 return false;
	 }
	if(!(clickType == 'save')){
	
	if ('${autoSavePrompt}' == 'No'){
		var id1 = document.forms['chargeForm'].elements['charges.id'].value;
		var contract=document.forms['chargeForm'].elements['charges.contract'].value;
		var charge=document.forms['chargeForm'].elements['charges.charge'].value;
		var id2=document.forms['chargeForm'].elements['charges.id'].value;
		var contractId=document.forms['chargeForm'].elements['contractId'].value;
		var id3=document.forms['chargeForm'].elements['contractId'].value;
		if (document.forms['chargeForm'].elements['formStatus'].value == '1'){
		document.forms['chargeForm'].action = 'saveCharges!saveOnTabChange.html';
		document.forms['chargeForm'].submit();
		}
		else
		{
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.chargeList'){
				location.href = 'charge23.html?id='+contractId;
				}
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.contract'){
				location.href = 'searchContracts.html';
				}
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.glType'){
				location.href = 'glCommissionTypeList.html?contract='+contract+'&charge='+charge+'&id='+id2+'&glId='+id2+'&contractId='+contractId;
				}
		
		}
	}
	else{
	var id1 = document.forms['chargeForm'].elements['charges.id'].value;
	var contract=document.forms['chargeForm'].elements['charges.contract'].value;
	var charge=document.forms['chargeForm'].elements['charges.charge'].value;
	var id2=document.forms['chargeForm'].elements['charges.id'].value;
	var contractId=document.forms['chargeForm'].elements['contractId'].value;
	
	if (document.forms['chargeForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='chargeDetail.heading'/>");
		if(agree){
			document.forms['chargeForm'].action = 'saveCharges!saveOnTabChange.html';
			document.forms['chargeForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.chargeList'){
				location.href = 'charge23.html?id='+contractId;
				}
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.contract'){
				location.href = 'searchContracts.html';
				}
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.glType'){
				location.href = 'glCommissionTypeList.html?contract='+contract+'&charge='+charge+'&id='+id2+'&glId='+id2+'&contractId='+contractId;
				}
					
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.chargeList'){
				location.href = 'charge23.html?id='+contractId;
				}
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.contract'){
				location.href = 'searchContracts.html';
				}
			if(document.forms['chargeForm'].elements['gotoPageString'].value == 'gototab.glType'){
				location.href = 'glCommissionTypeList.html?contract='+contract+'&charge='+charge+'&glId='+id2+'&contractId='+contractId;
				
				}
			
			
	}
	}
}
}
}

function testWording()
{
	var wording=document.forms['chargeForm'].elements['charges.wording'].value;
	var value=document.forms['chargeForm'].elements['charges.perValue'].value;
	var description=document.forms['chargeForm'].elements['charges.description'].value;
	var formQuantity=document.forms['chargeForm'].elements['charges.item'].value;
	
	var minCharge=document.forms['chargeForm'].elements['charges.minimum'].value;
	
	var replaceBilingValue='0.0';
	var replacePerValueValue='0.0'
	var i = wording.indexOf("+");
	if(i<0)
	{
		if(((wording.length)-1)>wording.lastIndexOf('"'))
		{
			alert('Not correct');
		}else{
		
		 if(wording.indexOf('charges.description')>0)
		 {
		 	alert(description);
		 }
		else
		{
		alert('wording is:- \n'+wording.replace('"','').replace('"',''));
		}
		}
		
	}
	
	
	if(i>0 || wording.indexOf("+", 0)>=0)
	{
	var chk = wording.substring(0,wording.indexOf("+"));
	var chkReplace=(chk.replace('"','').replace('"',''));
	var chk1 = wording.substring(wording.indexOf("+"),wording.length);
	var chk2 = chk1.substring(1,chk1.length);
	var chk3 = chk2.substring(chk2.indexOf("+"),chk2.length);
	var chk4 = chk3.substring(1,chk3.length);
	var chk5 = chk4.substring(0,chk4.indexOf("+"));
	var chk6 = chk4.substring(chk4.indexOf("+"),chk4.length);
	var chk7 = chk6.substring(1,chk6.length);
	var chk8 = chk7.substring(chk7.indexOf("+"),chk7.length);
	var chk9 = chk8.substring(1,chk8.length);
	var chk9Replace=(chk9.replace('"','').replace('"',''));
	
	var firstSub =wording.substring(0,wording.indexOf('+')+1);
	var secondSub =wording.substring(wording.indexOf('+')+1,wording.length);
   	var replaceBilling =secondSub.substring(0,secondSub.indexOf('+')+1);
    
          
     var thirdSub =secondSub.substring(secondSub.indexOf('+')+1,secondSub.length);
   
     var notReplace=thirdSub.substring(0,thirdSub.indexOf('+')+1);
    
     var fourthSub = thirdSub.substring(thirdSub.indexOf('+')+1,thirdSub.length);
    
     var replacePerValue=fourthSub.substring(0,fourthSub.indexOf('+')+1);
    
     var fifthNotReplace =fourthSub.substring(fourthSub.indexOf('+')+1,fourthSub.length);
     var fifthNotReplaceReplace=(fifthNotReplace.replace('"','').replace('"',''));
    
     
     if(chk5.indexOf('form.qty')>0)
     {
     	chk5=formQuantity;
     }
     
     if(replacePerValue=='')
     {
     	replacePerValueValue='';
     }
     
     if(chk5=='')
     {
     	chk5=chk5;
     }
     
     if(replaceBilling.indexOf('billing.cycle')>0)
     {
     	replaceBilingValue='0.0'
     }
     
     if(replaceBilling.indexOf('$')>0)
     {
     	replaceBilingValue='$'
     }
     
     
     if(replaceBilling.indexOf('billing.onHand')>0)
     {
     	replaceBilingValue='0.0'
     }
     if(replaceBilling.indexOf('charges.perValue')>0)
     {
     	replaceBilingValue=value;
     }
     if(replaceBilling.indexOf('charges.minimum')>0)
     {
     	replaceBilingValue=minCharge;
     }
     
     if(replacePerValue.indexOf('charges.minimum')>0)
     {
     	replacePerValueValue=minCharge;
     }
     if(replacePerValue.indexOf('charges.perValue')>0)
     {
     	replacePerValueValue=value;
     }
     if(replacePerValue.indexOf('billing.onHand')>0)
     {
     	replacePerValueValue='0.0';
     }
     if(replacePerValue.indexOf('billing.cycle')>0)
     {
     	replacePerValueValue='0.0';
     }
	
	if(((wording.length)-1)>wording.lastIndexOf('"'))
	{
		alert('Not correct');
	
	}
	else
	{
	//alert(chk5);
	//alert(replacePerValueValue);
	//alert(chk9Replace);
	alert('wording is:- \n'+ chkReplace + replaceBilingValue + chk5 + replacePerValueValue + fifthNotReplaceReplace);
	
	}
	}
}
var form_submitted = false;

function submit_chargeForm()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  { 
    var divideMultiplyReturn =divideMultiplyOfCharges();
    if(divideMultiplyReturn){
    	 form_submitted = true;
    }
    return divideMultiplyReturn;
  }
}

function divideMultiplyOfCharges()
{
	var submitCheck=document.forms['chargeForm'].elements['submitCheck'].value;
	var divideMultiplyValue=document.forms['chargeForm'].elements['charges.divideMultiply'].value;
	var chargePerValue=document.forms['chargeForm'].elements['charges.perValue'].value;
	if((chargePerValue > 0)&&(divideMultiplyValue == " ")){
			alert("Please select divide or multiply factor in Price to adjust the amount.");	
		 	document.forms['chargeForm'].elements['charges.divideMultiply'].focus();
		  	return false;	
	}else if(submitCheck!='T'){
		return false;
	}else{
		var returncheck2D=check2D(); 
		return returncheck2D;
	}
}

function fillDescription(){
	if(((document.forms['chargeForm'].elements['charges.charge'].value).ltrim() == '') || ((document.forms['chargeForm'].elements['charges.description'].value).ltrim() == '')){
		alert('Please enter new charge code and description.');
		return false;
	}
	 var costEl=document.forms['chargeForm'].elements['charges.costElement'].value;
	 var costElementFlag1=document.forms['chargeForm'].elements['costElementFlag1'].value;
	 if((costElementFlag1.trim()=='YES')&&(costEl.trim()==''))
	 {
		 alert("Cost Element is required field.");
		 document.forms['chargeForm'].elements['charges.costElement'].focus();
		 return false;
	 }
	chkSelect();
	return true;
}

function checkChargeForContractAvailability() {
	var chargeCde= document.forms['chargeForm'].elements['charges.charge'].value;
	var contractCde= document.forms['chargeForm'].elements['charges.contract'].value;
	document.forms['chargeForm'].elements['submitCheck'].value="F";
	var url="checkChargeForContractExists.html?ajax=1&decorator=simple&popup=true&chargeCde="+encodeURI(chargeCde)+"&contractCde="+encodeURI(contractCde);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse5;
    http4.send(null);
}
	
function handleHttpResponse5()
        {
		  if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                var res = results.split("@");
                if(res.length-1 >= 1){
                alert("Charge Code is already in use  for this Contract.");
                document.forms['chargeForm'].elements['charges.charge'].value ='';
                } else {
                
                }
                document.forms['chargeForm'].elements['submitCheck'].value="T";
			}	
        }
        
        
function changeFeq(){
	var feq = document.forms['chargeForm'].elements['charges.frequency'].value;

	if(feq == ''){
		document.forms['chargeForm'].elements['charges.week1'].checked = false;
		document.forms['chargeForm'].elements['charges.week2'].checked = false;
		document.forms['chargeForm'].elements['charges.week3'].checked = false;
		document.forms['chargeForm'].elements['charges.week4'].checked = false;
		document.forms['chargeForm'].elements['charges.week5'].checked = false;
		document.forms['chargeForm'].elements['charges.week6'].checked = false;
		
		document.forms['chargeForm'].elements['charges.jan'].checked = false;
		document.forms['chargeForm'].elements['charges.feb'].checked = false;
		document.forms['chargeForm'].elements['charges.mar'].checked = false;
		document.forms['chargeForm'].elements['charges.apr'].checked = false;
		document.forms['chargeForm'].elements['charges.may'].checked = false;
		document.forms['chargeForm'].elements['charges.jun'].checked = false;
		document.forms['chargeForm'].elements['charges.jul'].checked = false;
		document.forms['chargeForm'].elements['charges.aug'].checked = false;
		document.forms['chargeForm'].elements['charges.sep'].checked = false;
		document.forms['chargeForm'].elements['charges.oct'].checked = false;
		document.forms['chargeForm'].elements['charges.nov'].checked = false;
		document.forms['chargeForm'].elements['charges.decem'].checked = false;
	}
	
	if(feq == 'Weekly'){
		document.forms['chargeForm'].elements['charges.week1'].checked = true;
		document.forms['chargeForm'].elements['charges.week2'].checked = true;
		document.forms['chargeForm'].elements['charges.week3'].checked = true;
		document.forms['chargeForm'].elements['charges.week4'].checked = true;
		document.forms['chargeForm'].elements['charges.week5'].checked = true;
		document.forms['chargeForm'].elements['charges.week6'].checked = true;
		
		document.forms['chargeForm'].elements['charges.jan'].checked = true;
		document.forms['chargeForm'].elements['charges.feb'].checked = true;
		document.forms['chargeForm'].elements['charges.mar'].checked = true;
		document.forms['chargeForm'].elements['charges.apr'].checked = true;
		document.forms['chargeForm'].elements['charges.may'].checked = true;
		document.forms['chargeForm'].elements['charges.jun'].checked = true;
		document.forms['chargeForm'].elements['charges.jul'].checked = true;
		document.forms['chargeForm'].elements['charges.aug'].checked = true;
		document.forms['chargeForm'].elements['charges.sep'].checked = true;
		document.forms['chargeForm'].elements['charges.oct'].checked = true;
		document.forms['chargeForm'].elements['charges.nov'].checked = true;
		document.forms['chargeForm'].elements['charges.decem'].checked = true;
	}
	
	if(feq == 'Monthly'){
		document.forms['chargeForm'].elements['charges.week1'].checked = false;
		document.forms['chargeForm'].elements['charges.week2'].checked = false;
		document.forms['chargeForm'].elements['charges.week3'].checked = false;
		document.forms['chargeForm'].elements['charges.week4'].checked = false;
		document.forms['chargeForm'].elements['charges.week5'].checked = false;
		document.forms['chargeForm'].elements['charges.week6'].checked = false;
		
		document.forms['chargeForm'].elements['charges.jan'].checked = true;
		document.forms['chargeForm'].elements['charges.feb'].checked = true;
		document.forms['chargeForm'].elements['charges.mar'].checked = true;
		document.forms['chargeForm'].elements['charges.apr'].checked = true;
		document.forms['chargeForm'].elements['charges.may'].checked = true;
		document.forms['chargeForm'].elements['charges.jun'].checked = true;
		document.forms['chargeForm'].elements['charges.jul'].checked = true;
		document.forms['chargeForm'].elements['charges.aug'].checked = true;
		document.forms['chargeForm'].elements['charges.sep'].checked = true;
		document.forms['chargeForm'].elements['charges.oct'].checked = true;
		document.forms['chargeForm'].elements['charges.nov'].checked = true;
		document.forms['chargeForm'].elements['charges.decem'].checked = true;
		
		alert('Please select one week of your choice.');
	}
	
	if(feq == 'Semi-Monthly'){
		document.forms['chargeForm'].elements['charges.week1'].checked = false;
		document.forms['chargeForm'].elements['charges.week2'].checked = false;
		document.forms['chargeForm'].elements['charges.week3'].checked = false;
		document.forms['chargeForm'].elements['charges.week4'].checked = false;
		document.forms['chargeForm'].elements['charges.week5'].checked = false;
		document.forms['chargeForm'].elements['charges.week6'].checked = false;
		
		document.forms['chargeForm'].elements['charges.jan'].checked = true;
		document.forms['chargeForm'].elements['charges.feb'].checked = true;
		document.forms['chargeForm'].elements['charges.mar'].checked = true;
		document.forms['chargeForm'].elements['charges.apr'].checked = true;
		document.forms['chargeForm'].elements['charges.may'].checked = true;
		document.forms['chargeForm'].elements['charges.jun'].checked = true;
		document.forms['chargeForm'].elements['charges.jul'].checked = true;
		document.forms['chargeForm'].elements['charges.aug'].checked = true;
		document.forms['chargeForm'].elements['charges.sep'].checked = true;
		document.forms['chargeForm'].elements['charges.oct'].checked = true;
		document.forms['chargeForm'].elements['charges.nov'].checked = true;
		document.forms['chargeForm'].elements['charges.decem'].checked = true;
		
		alert('Please select two weeks, either Week1 and Week3 or Week2 and Week4.');
	}
	
	if(feq == 'Quarterly'){
		document.forms['chargeForm'].elements['charges.week1'].checked = false;
		document.forms['chargeForm'].elements['charges.week2'].checked = false;
		document.forms['chargeForm'].elements['charges.week3'].checked = false;
		document.forms['chargeForm'].elements['charges.week4'].checked = false;
		document.forms['chargeForm'].elements['charges.week5'].checked = false;
		document.forms['chargeForm'].elements['charges.week6'].checked = false;
		
		document.forms['chargeForm'].elements['charges.jan'].checked = false;
		document.forms['chargeForm'].elements['charges.feb'].checked = false;
		document.forms['chargeForm'].elements['charges.mar'].checked = false;
		document.forms['chargeForm'].elements['charges.apr'].checked = false;
		document.forms['chargeForm'].elements['charges.may'].checked = false;
		document.forms['chargeForm'].elements['charges.jun'].checked = false;
		document.forms['chargeForm'].elements['charges.jul'].checked = false;
		document.forms['chargeForm'].elements['charges.aug'].checked = false;
		document.forms['chargeForm'].elements['charges.sep'].checked = false;
		document.forms['chargeForm'].elements['charges.oct'].checked = false;
		document.forms['chargeForm'].elements['charges.nov'].checked = false;
		document.forms['chargeForm'].elements['charges.decem'].checked = false;
		
		alert('Please select four months and one week.');
	}
	
	if(feq == 'Annually'){
		document.forms['chargeForm'].elements['charges.week1'].checked = false;
		document.forms['chargeForm'].elements['charges.week2'].checked = false;
		document.forms['chargeForm'].elements['charges.week3'].checked = false;
		document.forms['chargeForm'].elements['charges.week4'].checked = false;
		document.forms['chargeForm'].elements['charges.week5'].checked = false;
		document.forms['chargeForm'].elements['charges.week6'].checked = false;
		
		document.forms['chargeForm'].elements['charges.jan'].checked = false;
		document.forms['chargeForm'].elements['charges.feb'].checked = false;
		document.forms['chargeForm'].elements['charges.mar'].checked = false;
		document.forms['chargeForm'].elements['charges.apr'].checked = false;
		document.forms['chargeForm'].elements['charges.may'].checked = false;
		document.forms['chargeForm'].elements['charges.jun'].checked = false;
		document.forms['chargeForm'].elements['charges.jul'].checked = false;
		document.forms['chargeForm'].elements['charges.aug'].checked = false;
		document.forms['chargeForm'].elements['charges.sep'].checked = false;
		document.forms['chargeForm'].elements['charges.oct'].checked = false;
		document.forms['chargeForm'].elements['charges.nov'].checked = false;
		document.forms['chargeForm'].elements['charges.decem'].checked = false;
		
		alert('Please select one month and one week.');
	}
	
}        

function checkFeq(target){
	<c:if test="${charges.contract=='Driver Settlement'}">
		var el = document.getElementById('hid');
		var el1 = document.getElementById('hid1');
		if(target.value == 'Per Schedule'){
			el.style.display = 'block';
			el.style.visibility = 'visible';
			el1.style.display = 'block';
			el1.style.visibility = 'visible';
		}else{
			el.style.display = 'none';
			el1.style.display = 'none';
		}
	</c:if>
}

function checkFeqOnload(){
	<c:if test="${charges.contract=='Driver Settlement'}">
		var el = document.getElementById('hid');
		var el1 = document.getElementById('hid1');
		if('${charges.deductionType}' == 'Per Schedule'){
			el.style.display = 'block';
			el.style.visibility = 'visible';
			el1.style.display = 'block';
			el1.style.visibility = 'visible';
		}else{
			el.style.display = 'none';
			el1.style.display = 'none';
		}
	</c:if>
}

function checkChar() {
		 var chArr = new Array("%","&","'","|","\\","\"",",");
	 	 var origString = document.forms['chargeForm'].elements['charges.description'].value;
	 	for ( var i = 0; i < chArr.length; i++ ) {
	 		origString = origString.split(chArr[i]).join(''); 
	 	}
	 	document.forms['chargeForm'].elements['charges.description'].value = origString;
}

function checkSpecialCharacter(evt){
var keyCode = evt.which ? evt.which : evt.keyCode;
var shiftkey=evt.shiftKey;
var ctrl =  evt.which ? evt.ctrlKey:evt.modifiers & Event.CONTROL_MASK;
if (ctrl && keyCode==118) //CTRL+V
{
return false;
}
var isValid = (shiftkey && keyCode == 35) || (keyCode == 93) || (keyCode == 92)||(shiftkey && keyCode == 124)||(shiftkey && keyCode == 60)||(shiftkey && keyCode == 62);
return !(isValid);  
}
function openBookingAgentPopWindow(){
window.open("openCostElementPopup.html?decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=800, height=480,top=0, left=0,menubar=no")
}

function makePopulateCostElement(){
	var costDesCode=document.forms['chargeForm'].elements['charges.scostElementDescription'].value;
	var url="costDesDetailCode.html?ajax=1&decorator=simple&popup=true&costDes=" + encodeURI(costDesCode);
    http13.open("GET", url, true);
    http13.onreadystatechange = httpPortCostDescriptionCode;
    http13.send(null);
}

function httpPortCostDescriptionCode()
        {
             if (http13.readyState == 4)
             {
                var results = http13.responseText
                results = results.trim();
                if(results.length>3)
               	{
               	 	var recArr=results.split('~');
 					document.forms['chargeForm'].elements['charges.costElement'].value = recArr[0]; 				
 					document.forms['chargeForm'].elements['charges.gl'].value = recArr[1]; 				
 					document.forms['chargeForm'].elements['charges.expGl'].value = recArr[2]; 				 					 					
 					
                }
                 else
                 {
                   alert("Please enter valid description");
                   document.forms['chargeForm'].elements['charges.costElement'].value="";
				   document.forms['chargeForm'].elements['charges.scostElementDescription'].value = "";
				   document.forms['chargeForm'].elements['charges.gl'].value = ""; 				
 				   document.forms['chargeForm'].elements['charges.expGl'].value = ""; 				 					 					
				   
                 }
             }
        }

function getHTTPObject5()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http13 = getHTTPObject5();

function makePopulateCostDescription(){
	var costElCode=document.forms['chargeForm'].elements['charges.costElement'].value;
	var url="costElDetailCode.html?ajax=1&decorator=simple&popup=true&costEl=" + encodeURI(costElCode);
    http14.open("GET", url, true);
    http14.onreadystatechange = httpPortCostElementCode;
    http14.send(null);
}

function httpPortCostElementCode()
        {
             if (http14.readyState == 4)
             {
                var results = http14.responseText
                results = results.trim();
                if(results.length>3)
               	{
               	var recArr=results.split('~');
 					document.forms['chargeForm'].elements['charges.scostElementDescription'].value = recArr[0]; 				
 					document.forms['chargeForm'].elements['charges.gl'].value = recArr[1]; 				
 					document.forms['chargeForm'].elements['charges.expGl'].value = recArr[2]; 				 					 					
                }
                 else
                 {
                   alert("Please enter valid cost element");
                   document.forms['chargeForm'].elements['charges.costElement'].value="";
				   document.forms['chargeForm'].elements['charges.scostElementDescription'].value = "";
				   document.forms['chargeForm'].elements['charges.gl'].value = ""; 				
 				   document.forms['chargeForm'].elements['charges.expGl'].value = ""; 				 					 					
                 }
             }
        }

function getHTTPObject4()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http14 = getHTTPObject4();

    function enterNotAllow(evt){
    	var keyCode = evt.which ? evt.which : evt.keyCode;
    	if(keyCode==13)
    	{
        	return false;
    	}
    	else
    	{
        	return true;
    	}
    }

    function checkReadOnly(){
        var chkCorpId= '${contracts.owner}';
        var corpId='${charges.corpID}';
        if(corpId!=chkCorpId){
          var len = document.forms['chargeForm'].length;
          for (var i=0 ;i<len ;i++){
              if(document.forms['chargeForm'].elements[i].name == 'Rate Grid'){
            	  document.forms['chargeForm'].elements[i].disabled = false;
               }else{
        	  		document.forms['chargeForm'].elements[i].disabled = true;
               }
              try{
            	  if(document.forms['chargeForm'].elements[i].name == 'sellRateDeviations' || document.forms['chargeForm'].elements[i].name == 'buyRateDeviations' ){
                	  document.forms['chargeForm'].elements[i].disabled = false;
                   }
              }catch(e){}
          }
       }
    }
    
    function onlyFloat2(e) {
        var t;
        var n = e.value;
        var r = 0;
        for (t = 0; t < n.length; t++) {
            var i = n.charAt(t);
            if (i == ".") {
                r = r + 1
            }
            if ((i < "0" || i > "9") && i != "." || r > "1") {
                document.getElementById(e.id).value ="";
                alert ("Special characters are not allowed.");
                document.getElementById(e.id).select();
                return false;
            }
        }
        return true;
    }
    
    function ValidationSpecialCharcter(){
    	var iChars = "!`@#$%^&*()+=-[]\\\';,/{}|\":<>?~_";   

    	var data = document.forms['chargeForm'].elements['charges.incrementalStep'].value;

    	for (var i = 0; i < data.length; i++)

    	{      
    	    if (iChars.indexOf(data.charAt(i)) != -1)
    	    {    
    	    alert ("Special characters are not allowed.");
    	    document.forms['chargeForm'].elements['charges.incrementalStep'].value = "";
    	    return false; 
    	    } 
    	} 
    	}
    
</script>
<%-- Shifting Closed Here --%>
 
<script type="text/javascript">   
	checkFeqOnload();
</script>
<script type="text/javascript">
try{
	checkedBuySell();
	<c:if test="${costElementFlag==true}">
		document.forms['chargeForm'].elements['costElementFlag1'].value='YES';
	</c:if>
	<c:if test="${costElementFlag!=true}">
		document.forms['chargeForm'].elements['costElementFlag1'].value='NO';
	</c:if>
}
catch(e){}
try{checkReadOnly();}
catch(e){}
</script>