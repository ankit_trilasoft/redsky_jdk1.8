delimiter $$

CREATE trigger redsky.trigger_add_history_contractpolicy
BEFORE UPDATE on redsky.contractpolicy
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



  



   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'contractpolicy.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;

    


    




    




   IF (OLD.partnerCode <> NEW.partnerCode ) THEN
      select CONCAT(fieldNameList,'contractpolicy.partnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerCode,'~') into newValueList;
  END IF;

    


    




   IF (OLD.policy <> NEW.policy ) THEN
      select CONCAT(fieldNameList,'contractpolicy.policy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.policy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.policy,'~') into newValueList;
  END IF;

    


    




   IF (OLD.sectionName <> NEW.sectionName ) THEN
      select CONCAT(fieldNameList,'contractpolicy.sectionName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sectionName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sectionName,'~') into newValueList;
  END IF;

    


    




   IF (OLD.language <> NEW.language ) THEN
      select CONCAT(fieldNameList,'contractpolicy.language~') into fieldNameList;
      select CONCAT(oldValueList,OLD.language,'~') into oldValueList;
      select CONCAT(newValueList,NEW.language,'~') into newValueList;
  END IF;

    


    




   IF (OLD.fileName <> NEW.fileName ) THEN
      select CONCAT(fieldNameList,'contractpolicy.fileName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileName,'~') into newValueList;
  END IF;

    





   IF (OLD.fileSize <> NEW.fileSize ) THEN
      select CONCAT(fieldNameList,'contractpolicy.fileSize~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileSize,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileSize,'~') into newValueList;
  END IF;

    





   IF (OLD.documentType <> NEW.documentType ) THEN
      select CONCAT(fieldNameList,'contractpolicy.documentType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.documentType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.documentType,'~') into newValueList;
  END IF;

    





   IF (OLD.documentName <> NEW.documentName ) THEN
      select CONCAT(fieldNameList,'contractpolicy.documentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.documentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.documentName,'~') into newValueList;
  END IF;

    





   IF (OLD.parentId <> NEW.parentId ) THEN
      select CONCAT(fieldNameList,'contractpolicy.parentId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parentId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parentId,'~') into newValueList;
  END IF;

    





   IF (OLD.docSequenceNumber <> NEW.docSequenceNumber ) THEN
      select CONCAT(fieldNameList,'contractpolicy.docSequenceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.docSequenceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.docSequenceNumber,'~') into newValueList;
  END IF;

    





   IF (OLD.jobType <> NEW.jobType ) THEN
      select CONCAT(fieldNameList,'contractpolicy.jobType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
  END IF;

    




 CALL add_tblHistory (OLD.id,"contractpolicy", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;