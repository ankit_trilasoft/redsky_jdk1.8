package com.trilasoft.app.dao.hibernate.dto;

import java.text.SimpleDateFormat;

public class ExtractHibernateDTO {
	
// Flat file extraction
private Object corpId;
private Boolean qualitySurvey;
private Object sequenceNumber;
private Object partnerCode;
private Object billtoCode;
private Object transferee;
private Object mode;
private Object fromTo;
private Object originCountry;
private Object destCountry;
private Object originCountrycode;
private Object destinationCountrycode;
private Object originCity;
private Object destinationCity;
private Object survey;
private Object beginPacking;
private Object beginload;
private Object requiredDeliveryDate;
private Object invoiceDate;
private Object accountContact;
private Object authorizationReceivedDt;
private Object authorizedVolumeCBM;
private Object estimatedVolumeCBM;
private Object shippedVolumeCBM;
private Object distanceKM;
private Object totalAmtInvoicedEUR;
private Object doorToDoorEUR;
private Object allAditionalCharges;
private Object recommendation;
private Object preMoveServicsRating;
private Object originServicesRating;
private Object destServicesRating;
private Object overAllRating;
private Object complaints;
private Object nationality;
private Object assignmentType;
private Object quoteStatus;

// Claim file Extraction

private Object clientName_Claim;
private Object account_Claim;
private Object serviceOrder_Claim;
private Object claimNumber_Claim;
private Object lossNumber1_Claim;
private Object fromTo_Claim;
private Object handleByInsurer_Claim;
private Object type_Claim;
private Object cause_Claim;
private Object causeBy_Claim;
private Object dateReceived_Claim;
private Object claimAmt_Claim;
private Object claimCurrency_Claim;
private Object paidAmt_Claim;
private Object paidCurrency_Claim;
private Object claimStatus_Claim;
private Object statusDt_Claim;
private Object remark_Claim;

public Object getSequenceNumber() {
	return sequenceNumber;
}
public void setSequenceNumber(Object sequenceNumber) {
	this.sequenceNumber = sequenceNumber;
}
public Object getMode() {
	return mode;
}
public void setMode(Object mode) {
	this.mode = mode;
}

public Object getSurvey() {
	return survey;
}
public void setSurvey(Object survey) {
	this.survey = survey;
}
public Object getTransferee() {
	return transferee;
}
public void setTransferee(Object transferee) {
	this.transferee = transferee;
}
public Object getOriginCountrycode() {
	return originCountrycode;
}
public void setOriginCountrycode(Object originCountrycode) {
	this.originCountrycode = originCountrycode;
}
public Object getDestinationCountrycode() {
	return destinationCountrycode;
}
public void setDestinationCountrycode(Object destinationCountrycode) {
	this.destinationCountrycode = destinationCountrycode;
}
public Object getOriginCity() {
	return originCity;
}
public void setOriginCity(Object originCity) {
	this.originCity = originCity;
}
public Object getDestinationCity() {
	return destinationCity;
}
public void setDestinationCity(Object destinationCity) {
	this.destinationCity = destinationCity;
}
public Object getBeginPacking() {
	return beginPacking;
}
public void setBeginPacking(Object beginPacking) {
	this.beginPacking = beginPacking;
}
public Object getBeginload() {
	return beginload;
}
public void setBeginload(Object beginload) {
	this.beginload = beginload;
}
public Object getRequiredDeliveryDate() {
	return requiredDeliveryDate;
}
public void setRequiredDeliveryDate(Object requiredDeliveryDate) {
	this.requiredDeliveryDate = requiredDeliveryDate;
}
public Object getInvoiceDate() {
	return invoiceDate;
}
public void setInvoiceDate(Object invoiceDate) {
	this.invoiceDate = invoiceDate;
}
public Object getPartnerCode() {
	return partnerCode;
}
public void setPartnerCode(Object partnerCode) {
	this.partnerCode = partnerCode;
}
public Object getBilltoCode() {
	return billtoCode;
}
public void setBilltoCode(Object billtoCode) {
	this.billtoCode = billtoCode;
}
public Object getAccountContact() {
	return accountContact;
}
public void setAccountContact(Object accountContact) {
	this.accountContact = accountContact;
}
public Object getAuthorizationReceivedDt() {
	return authorizationReceivedDt;
}
public void setAuthorizationReceivedDt(Object authorizationReceivedDt) {
	this.authorizationReceivedDt = authorizationReceivedDt;
}
public Object getAuthorizedVolumeCBM() {
	return authorizedVolumeCBM;
}
public void setAuthorizedVolumeCBM(Object authorizedVolumeCBM) {
	this.authorizedVolumeCBM = authorizedVolumeCBM;
}
public Object getEstimatedVolumeCBM() {
	return estimatedVolumeCBM;
}
public void setEstimatedVolumeCBM(Object estimatedVolumeCBM) {
	this.estimatedVolumeCBM = estimatedVolumeCBM;
}
public Object getShippedVolumeCBM() {
	return shippedVolumeCBM;
}
public void setShippedVolumeCBM(Object shippedVolumeCBM) {
	this.shippedVolumeCBM = shippedVolumeCBM;
}
public Object getDistanceKM() {
	return distanceKM;
}
public void setDistanceKM(Object distanceKM) {
	this.distanceKM = distanceKM;
}
public Object getTotalAmtInvoicedEUR() {
	return totalAmtInvoicedEUR;
}
public void setTotalAmtInvoicedEUR(Object totalAmtInvoicedEUR) {
	this.totalAmtInvoicedEUR = totalAmtInvoicedEUR;
}
public Object getDoorToDoorEUR() {
	return doorToDoorEUR;
}
public void setDoorToDoorEUR(Object doorToDoorEUR) {
	this.doorToDoorEUR = doorToDoorEUR;
}
public Object getAllAditionalCharges() {
	return allAditionalCharges;
}
public void setAllAditionalCharges(Object allAditionalCharges) {
	this.allAditionalCharges = allAditionalCharges;
}
public Object getRecommendation() {
	return recommendation;
}
public void setRecommendation(Object recommendation) {
	this.recommendation = recommendation;
}
public Object getPreMoveServicsRating() {
	return preMoveServicsRating;
}
public void setPreMoveServicsRating(Object preMoveServicsRating) {
	this.preMoveServicsRating = preMoveServicsRating;
}
public Object getOriginServicesRating() {
	return originServicesRating;
}
public void setOriginServicesRating(Object originServicesRating) {
	this.originServicesRating = originServicesRating;
}
public Object getDestServicesRating() {
	return destServicesRating;
}
public void setDestServicesRating(Object destServicesRating) {
	this.destServicesRating = destServicesRating;
}
public Object getOverAllRating() {
	return overAllRating;
}
public void setOverAllRating(Object overAllRating) {
	this.overAllRating = overAllRating;
}
public Object getComplaints() {
	return complaints;
}
public void setComplaints(Object complaints) {
	this.complaints = complaints;
}

public Object getAssignmentType() {
	return assignmentType;
}
public void setAssignmentType(Object assignmentType) {
	this.assignmentType = assignmentType;
}
/* Claim Methods */
public Object getClientName_Claim() {
	return clientName_Claim;
}
public void setClientName_Claim(Object clientName_Claim) {
	this.clientName_Claim = clientName_Claim;
}
public Object getAccount_Claim() {
	return account_Claim;
}
public void setAccount_Claim(Object account_Claim) {
	this.account_Claim = account_Claim;
}
public Object getServiceOrder_Claim() {
	return serviceOrder_Claim;
}
public void setServiceOrder_Claim(Object serviceOrder_Claim) {
	this.serviceOrder_Claim = serviceOrder_Claim;
}

public void setFromTo_Claim(Object fromTo_Claim) {
	this.fromTo_Claim = fromTo_Claim;
}
public Object getHandleByInsurer_Claim() {
	return handleByInsurer_Claim;
}
public void setHandleByInsurer_Claim(Object handleByInsurer_Claim) {
	this.handleByInsurer_Claim = handleByInsurer_Claim;
}
public Object getType_Claim() {
	return type_Claim;
}
public void setType_Claim(Object type_Claim) {
	this.type_Claim = type_Claim;
}
public Object getCause_Claim() {
	return cause_Claim;
}
public void setCause_Claim(Object cause_Claim) {
	this.cause_Claim = cause_Claim;
}
public Object getCauseBy_Claim() {
	return causeBy_Claim;
}
public void setCauseBy_Claim(Object causeBy_Claim) {
	this.causeBy_Claim = causeBy_Claim;
}
public Object getDateReceived_Claim() {
	return dateReceived_Claim;
}
public void setDateReceived_Claim(Object dateReceived_Claim) {
	this.dateReceived_Claim = dateReceived_Claim;
}
public Object getClaimAmt_Claim() {
	return claimAmt_Claim;
}
public void setClaimAmt_Claim(Object claimAmt_Claim) {
	this.claimAmt_Claim = claimAmt_Claim;
}
public Object getPaidAmt_Claim() {
	return paidAmt_Claim;
}
public void setPaidAmt_Claim(Object paidAmt_Claim) {
	this.paidAmt_Claim = paidAmt_Claim;
}
public Object getClaimStatus_Claim() {
	return claimStatus_Claim;
}
public void setClaimStatus_Claim(Object claimStatus_Claim) {
	this.claimStatus_Claim = claimStatus_Claim;
}
public Object getStatusDt_Claim() {
	return statusDt_Claim;
}
public void setStatusDt_Claim(Object statusDt_Claim) {
	this.statusDt_Claim = statusDt_Claim;
}

public Object getFromTo_Claim() {
	return fromTo_Claim;
}
public Object getClaimCurrency_Claim() {
	return claimCurrency_Claim;
}
public void setClaimCurrency_Claim(Object claimCurrency_Claim) {
	this.claimCurrency_Claim = claimCurrency_Claim;
}
public Object getPaidCurrency_Claim() {
	return paidCurrency_Claim;
}
public void setPaidCurrency_Claim(Object paidCurrency_Claim) {
	this.paidCurrency_Claim = paidCurrency_Claim;
}
public Object getRemark_Claim() {
	return remark_Claim;
}
public void setRemark_Claim(Object remark_Claim) {
	this.remark_Claim = remark_Claim;
}
public Object getClaimNumber_Claim() {
	return claimNumber_Claim;
}
public void setClaimNumber_Claim(Object claimNumber_Claim) {
	this.claimNumber_Claim = claimNumber_Claim;
}
public Object getLossNumber1_Claim() {
	return lossNumber1_Claim;
}
public void setLossNumber1_Claim(Object lossNumber1_Claim) {
	this.lossNumber1_Claim = lossNumber1_Claim;
}


public Object getCorpId() {
	return corpId;
}
public void setCorpId(Object corpId) {
	this.corpId = corpId;
}
public Boolean getQualitySurvey() {
	return qualitySurvey;
}
public void setQualitySurvey(Boolean qualitySurvey) {
	this.qualitySurvey = qualitySurvey;
}
@Override
public String toString() {
	String returnString = sequenceNumber + "@ "+ quoteStatus + "@ " + partnerCode + "@ " + billtoCode + "@ "
			+ accountContact + "@ " +  transferee + "@ " + mode + "@ " + originCountrycode + " -> "
			+ destinationCountrycode + "@ " + originCity +"@ " + destinationCity +
			"@ " + ((authorizationReceivedDt!=null)?new SimpleDateFormat("yyyy/MM/dd").format(authorizationReceivedDt):"") + "@ "
			+ ((survey!=null)?new SimpleDateFormat("yyyy/MM/dd").format(survey):"") + 
			"@ " + ((beginPacking!=null)?new SimpleDateFormat("yyyy/MM/dd").format(beginPacking):"") + "@ "
			+ ((beginload!=null)?new SimpleDateFormat("yyyy/MM/dd").format(beginload):"") + 
			"@ " + ((requiredDeliveryDate!=null)?new SimpleDateFormat("yyyy/MM/dd").format(requiredDeliveryDate):"") + 
			"@ " + ((invoiceDate!=null)?new SimpleDateFormat("yyyy/MM/dd").format(invoiceDate):"") +
			"@ " + ((authorizedVolumeCBM != null)?authorizedVolumeCBM:"") + "@ " + ((estimatedVolumeCBM != null)?estimatedVolumeCBM:"") + "@ "
			+ ((shippedVolumeCBM != null)?shippedVolumeCBM:"") + "@ " + ((distanceKM != null)?distanceKM:"") + "@ " + ((totalAmtInvoicedEUR != null)?totalAmtInvoicedEUR:"")
			+ "@ " + ((doorToDoorEUR != null)?doorToDoorEUR:"") + "@ " + ((allAditionalCharges != null)?allAditionalCharges:"") + "@ " + ((assignmentType != null)?assignmentType:"");
	if(qualitySurvey != null && qualitySurvey ){
			returnString += "@"+ ((recommendation != null)?recommendation:"") + "@ " + ((preMoveServicsRating != null)?preMoveServicsRating:"") + "@ "
			+ ((originServicesRating != null)?originServicesRating:"") + "@ " + ((destServicesRating != null)?destServicesRating:"") + "@ "
			+ ((overAllRating != null)?overAllRating:"") + "@ " + ((complaints != null)?complaints:"")+ "@ " + ((nationality != null)?nationality:"");
	}
	return returnString;
}
public Object getQuoteStatus() {
	return quoteStatus;
}
public void setQuoteStatus(Object quoteStatus) {
	this.quoteStatus = quoteStatus;
}

public Object getNationality() {
	return nationality;
}
public void setNationality(Object nationality) {
	this.nationality = nationality;
}
}
