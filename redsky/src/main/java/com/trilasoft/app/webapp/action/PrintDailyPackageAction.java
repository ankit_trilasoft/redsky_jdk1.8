/**
 * This class interacts with Ware House and date to generate pdf.
 * @Class Name	PrintDailyPackageAction
 * @Author      Ashis Kumar Mohanty
 * @Version     V01.0
 * @Since       1.0
 * @Date        07-Jan-2013
 * Extended by BaseAction.
 */
package com.trilasoft.app.webapp.action;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PrintDailyPackageManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.WorkTicketManager;

public class PrintDailyPackageAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	
	private Company company;
	private PrintDailyPackage printDailyPackage;
	
	private ReportsManager reportsManager;
	private CompanyManager companyManager;
	private WorkTicketManager workTicketManager;
	private PrintDailyPackageManager printDailyPackageManager;
	private RefMasterManager refMasterManager;
	private ServiceOrderManager serviceOrderManager;
	
	private List<JRParameter> reportParameters;
	private List<PrintDailyPackage> printDailyPackageList;
	private List<PrintDailyPackage> printDailyPackageChildList;
	private List mode;
	private List printAutoCompleteList;
	private List multiplServiceTypes;
	private List reportDescList;
	private List commonAjaxList;
	
	private Map<String, String> wtServiceType;
	private  Map<String, String> yesno;
	private Map<String, String> moduleReport;
	private  Map<String,String> house;
	
	private Long id;
	private String fromDt;	 
	private String toDt;
	private String whouse;
	private String warehouse;
	private String fileType;
	private Long childId;
	private String printURL;
	private String jrxmlName;
	private String parameters;
	private String description;
	
	private String formName;
	private String serviceType;
	private String serviceMode;
	private String military;
	
	private String returnAjaxStringValue;
	private Date printDate;
	private String module;
	private String pathSeparator;
	private String path;
	private String tabId;
	private Map<String, String> jobs;
	private Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	
	static final Logger logger = Logger.getLogger(PrintDailyPackageAction.class);
	Date currentdate = new Date();
	
	public PrintDailyPackageAction() {
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(auth != null){
				User user = (User) auth.getPrincipal(); 
				this.sessionCorpID = user.getCorpID();
			}
		} catch (Exception e) {e.printStackTrace();} 
	}

	public void prepare() throws Exception {

	}
	public void getComboList() throws Exception {
		wtServiceType = refMasterManager.findByParameter(sessionCorpID, "TCKTSERVC");
		yesno = refMasterManager.findByParameter(sessionCorpID, "YESNO");
		mode = refMasterManager.findByParameters(sessionCorpID, "MODE");
		moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE_REPORT");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
	}
	
	private List multiplJobType;
	@SkipValidation
	public String edit() throws Exception {
		getComboList();
		if (id != null) {
			printDailyPackage = printDailyPackageManager.get(id);
			reportDescList = reportsManager.getdescription(sessionCorpID,printDailyPackage.getModule());
			printDailyPackageChildList = printDailyPackageManager.findChildByParentId(id);
			if(printDailyPackage.getWtServiceType() != null && !"All".equals(printDailyPackage.getWtServiceType())){
				multiplServiceTypes = new ArrayList();
				String[] serviceType = printDailyPackage.getWtServiceType().split(",");
				for (String sType : serviceType) {
					multiplServiceTypes.add(sType.trim());
				}
			}
			if(printDailyPackage.getJob()!=null && !printDailyPackage.equals("")){
				multiplJobType = new ArrayList();
				String[] jobType = printDailyPackage.getJob().split(",");
				for (String jType : jobType){
					multiplJobType.add(jType.trim());
				}
			}
		} else {
			printDailyPackage = new PrintDailyPackage(); 
			printDailyPackage.setCreatedOn(new Date());
			printDailyPackage.setUpdatedOn(new Date());
		} 
		
		return SUCCESS;
	}
	
	public String printPackageChildAjax() throws Exception {
		printDailyPackageChildList = printDailyPackageManager.findChildByParentId(id);
		return SUCCESS;
	}
	
	public String getFormCommentAjax(){
		returnAjaxStringValue = reportsManager.getReportCommentByJrxmlName(jrxmlName,sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	public String getReportNamesAjax(){
		commonAjaxList = reportsManager.getReportCommentByModule(module,sessionCorpID);
		returnAjaxStringValue = commonAjaxList.toString().substring(1, commonAjaxList.toString().length()-1);
		returnAjaxStringValue = returnAjaxStringValue.replaceAll(", ", ",");
		return SUCCESS;
	}
	
	@SkipValidation
	public String searchPrintPackage() throws Exception {
		wtServiceType = refMasterManager.findByParameter(sessionCorpID, "TCKTSERVC");
		yesno = refMasterManager.findByParameter(sessionCorpID, "YESNO");
		mode = refMasterManager.findByParameters(sessionCorpID, "MODE");
		
		printDailyPackageList = printDailyPackageManager.searchPrintPackage(formName, sessionCorpID, serviceType, serviceMode, military);
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String addChildLine() throws Exception {
		getComboList();
				
		if (id != null) {
			printDailyPackage = printDailyPackageManager.get(id);
			
			PrintDailyPackage printChild = new PrintDailyPackage();
			printChild.setParentId(id);
			printChild.setCorpId(printDailyPackage.getCorpId());
			printDailyPackage.setStatus(true);
			
			/*printChild.setFormName(printDailyPackage.getFormName());
			printChild.setNoOfCopies(printDailyPackage.getNoOfCopies());
			printChild.setMode(printDailyPackage.getMode());
			printChild.setMilitary(printDailyPackage.getMilitary());*/
			
			printDailyPackageManager.save(printChild);
		}
		printDailyPackageChildList = printDailyPackageManager.findChildByParentId(id);
		if(printDailyPackage.getWtServiceType() != null && !"All".equals(printDailyPackage.getWtServiceType())){
			multiplServiceTypes = new ArrayList();
			String[] serviceType = printDailyPackage.getWtServiceType().split(",");
			for (String sType : serviceType) {
				multiplServiceTypes.add(sType.trim());
			}
		}
		if(printDailyPackage.getJob()!=null && !printDailyPackage.equals("")){
			multiplJobType = new ArrayList();
			String[] jobType = printDailyPackage.getJob().split(",");
			for (String jType : jobType){
				multiplJobType.add(jType.trim());
			}
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String save() throws Exception {
			boolean isNew = (printDailyPackage.getId() == null);  
				if (isNew) { 
					printDailyPackage.setCreatedOn(new Date());
		        } 
				printDailyPackage.setCorpId(sessionCorpID);
				printDailyPackage.setUpdatedOn(new Date());
				printDailyPackage.setUpdatedBy(getRequest().getRemoteUser());
				printDailyPackage.setCreatedBy(getRequest().getRemoteUser());
				if(printDailyPackage.getWtServiceType() == null || "".equals(printDailyPackage.getWtServiceType())){
					printDailyPackage.setWtServiceType("All");
				}
				printDailyPackage.setStatus(true);
				printDailyPackageManager.save(printDailyPackage);
			    String key = "Print Daily Package details have been saved.";
			    saveMessage(getText(key));
			    printDailyPackageManager.deleteChild(sessionCorpID);
				return SUCCESS;
	}
	
	@SkipValidation
	public String print(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		wtServiceType = refMasterManager.findByParameter(sessionCorpID, "TCKTSERVC");
		yesno = refMasterManager.findByParameter(sessionCorpID, "YESNO");
		mode = refMasterManager.findByParameters(sessionCorpID, "MODE");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		printDailyPackageList = printDailyPackageManager.getByCorpId(sessionCorpID,tabId);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String printPackageAutocomplete(){
		printAutoCompleteList = printDailyPackageManager.findAutoCompleterList(jrxmlName,sessionCorpID);
		return SUCCESS;
	}
	
	public String getAllTicketByWHAjax(){
		List<WorkTicket> workTicketList = workTicketManager.getWorkTicketList("All", warehouse, printDate, sessionCorpID,"","","","");
		returnAjaxStringValue = "0";
		if(workTicketList != null)
			returnAjaxStringValue = String.valueOf(workTicketList.size());
		return SUCCESS;
	}
	
	public String delete() throws Exception {
		printDailyPackageManager.remove(id);
		printDailyPackageManager.deleteByParentId(id);
		
		printDailyPackageList = printDailyPackageManager.getByCorpId(sessionCorpID,"waste");
		
		return SUCCESS;
	}
	
	public String wasteBasketList() throws Exception {
		printDailyPackageList = printDailyPackageManager.getByCorpId(sessionCorpID,"waste");
		return SUCCESS;
	}
	
	public String wasteBasket() throws Exception {
		PrintDailyPackage printDailyPackage = printDailyPackageManager.get(id);
		printDailyPackage.setStatus(false);
		printDailyPackageManager.save(printDailyPackage);
		return SUCCESS;
	}
	
	public String recover() throws Exception {
			PrintDailyPackage printDailyPackage = printDailyPackageManager.get(id);
			printDailyPackage.setStatus(true);
			printDailyPackageManager.save(printDailyPackage);
			return SUCCESS;
	}
	
	public String deleteChild() throws Exception {
		printDailyPackageManager.remove(childId);
		
		printURL = "?id="+id;
		return SUCCESS;
	}
	
	@SkipValidation
	public String savePrintChildAjax() throws Exception {
		PrintDailyPackage child = printDailyPackageManager.get(childId);
		if(jrxmlName != null){
			child.setJrxmlName(jrxmlName.trim());
		}
		child.setFormDesc(description);
		child.setParameters(parameters);
		
		printDailyPackageManager.save(child);
		returnAjaxStringValue = "Success";
		return SUCCESS;
	}
	
	public List<JasperPrint> getParameter(PrintDailyPackage printDailyPackage,PrintDailyPackage printChild,String flag,JasperReport reportTemplate,List<JasperPrint> jasperPrints,Object obj,Map parameters,JRParameter[] allParameters){
				
		String ticket = "";
		String ship = "";
		if("WorkTicket".equals(flag)){
			WorkTicket wt = (WorkTicket)obj;
			ticket = wt.getTicket().toString();
			ship = wt.getShipNumber();
		}else if("ServiceOrder".equals(flag)){
			ServiceOrder so = (ServiceOrder)obj;
			ship = so.getShipNumber();
		}
		for (JRParameter parameter : allParameters) {
			if (!parameter.isSystemDefined()) {
				String paramName = parameter.getName();
				
				if(paramName.equalsIgnoreCase("User Name")){
					parameters.put(parameter.getName(), getRequest().getRemoteUser());
				}else if(paramName.equalsIgnoreCase("Service Order Number")){
					parameters.put(parameter.getName(), ship);
				}else if(paramName.equalsIgnoreCase("Warehouse")){
					parameters.put(parameter.getName(), whouse);
				}else if(paramName.equalsIgnoreCase("Work Ticket Number")){
					parameters.put(parameter.getName(), ticket);
				}else if(paramName.equalsIgnoreCase("Corporate ID")){
					parameters.put(parameter.getName(), sessionCorpID);
				}else if(paramName.equalsIgnoreCase("enddate")){
					parameters.put(parameter.getName(), printDate);
				}else if(paramName.equalsIgnoreCase("begindate")){
					parameters.put(parameter.getName(), printDate);
				}else{
					parameters.put(parameter.getName(), "");
				}
			}
		}
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
					
		JasperPrint jasperPrint = new JasperPrint();
		JasperPrint jasperPrint2 = new JasperPrint();
		
		Set<String> jrxmlSet = new HashSet<String>();
		jrxmlSet.add("DriverReceiptNew.jrxml");
		//jrxmlSet.add("AA_tkt_by_tkt_number.jrxml");
		
		System.out.println("Generate Report for "+ship+" : "+ticket+" Jrxml Name: "+printChild.getJrxmlName());
		try {
			jasperPrint = reportsManager.generateReport(reportTemplate, parameters);
			boolean isflag = true;
			for (int i = 1; i <= printDailyPackage.getNoOfCopies(); i++) {
				if(jrxmlSet.contains(printChild.getJrxmlName().trim()) && printDailyPackage.getNoOfCopies() > 1 &&
						jasperPrint != null && jasperPrint.getPages().size() > 1){
					if(i > 1){
						if(isflag){
							jasperPrint2 = reportsManager.generateReport(reportTemplate, parameters);
							jasperPrint2.getPages().remove(jasperPrint2.getPages().size()-1);
							isflag = false;
						}
						jasperPrints.add(jasperPrint2);
					}else{
						jasperPrints.add(jasperPrint);
					}
				}else{
					jasperPrints.add(jasperPrint);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrints;
	}
	
	private boolean jobFlag = false;
	private boolean serviceflag = false;
	private boolean modeFlag = false;
	private boolean militaryFlag = false;
	private List serviceArrList = new ArrayList();
	private List jobArrList = new ArrayList();		
	@SkipValidation
	public String printReports() throws Exception {
		Long startTime = System.currentTimeMillis();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		pathSeparator = System.getProperty("file.separator");
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
		HttpServletResponse response = getResponse();
		ServletOutputStream ouputStream = response.getOutputStream();
		JasperReport reportTemplate = null;
		path = ServletActionContext.getServletContext().getRealPath("/jasper");
		Set<String> SOList = new HashSet<String>();
		List<PrintDailyPackage> printList = printDailyPackageManager.getByCorpId(sessionCorpID,tabId);
		if(printList == null || printList.size() == 0 || printList.isEmpty()){
			createPDF(response,false);
			return null;
		}
		List<WorkTicket> workTicketList = workTicketManager.getWorkTicketList("All", whouse, printDate, sessionCorpID,"","","","");
		for (WorkTicket workTicket : workTicketList) {
			if(SOList != null && (!(SOList.toString().indexOf(workTicket.getShipNumber().trim()) >= 0))){
				SOList.clear();
			}
			miscellaneous = miscellaneousManager.get(workTicket.getServiceOrderId());
			for (PrintDailyPackage printDailyPackage : printList) {
				List<PrintDailyPackage> printChildList = printDailyPackageManager.findChildByParentId(printDailyPackage.getId());
				for (PrintDailyPackage printChild : printChildList) {
					if (printChild.getJrxmlName() != null && printChild.getJrxmlName().trim().length() > 0){
						if((printDailyPackage.getOnePerSOperDay() != null && "Y".equals(printDailyPackage.getOnePerSOperDay().trim())) 
								&& SOList.contains(workTicket.getShipNumber().trim()+printChild.getJrxmlName().trim())){
							break;
						}
						boolean flag = false;
						Map parameters = new HashMap();
						Reports report = reportsManager.getReportTemplateForWT(printChild.getJrxmlName(), printDailyPackage.getModule(), sessionCorpID);
						if(report != null){
								
							reportTemplate = JasperCompileManager.compileReport(path+pathSeparator+sessionCorpID+pathSeparator+ report.getReportName());
							JRParameter[] allParameters = reportTemplate.getParameters();
							
							if(!"All".equals(printDailyPackage.getWtServiceType().trim())){
								String serviceArr[] = printDailyPackage.getWtServiceType().split(",");
								
								for (String service : serviceArr) {
									serviceArrList.add(service.trim());
								}
								String wtService =workTicket.getService();
									if(serviceArrList.contains(wtService)){
											serviceflag = true;
										}else{
											serviceflag = false;
										}
									serviceArrList = new ArrayList();
								if(printDailyPackage.getJob()!=null && !printDailyPackage.getJob().equalsIgnoreCase("")){
									String jobArr[] = printDailyPackage.getJob().split(",");
									for (String job : jobArr) {
										jobArrList.add(job.trim());
									}
									String wtJob = workTicket.getJobType();
										if(jobArrList.contains(wtJob)){
											jobFlag = true;
										}else{
											jobFlag = false;
										}										
									}else{
										jobFlag = true;
									}
								jobArrList= new ArrayList();
								if(printDailyPackage.getMode() != null && !"".equals(printDailyPackage.getMode())){
									String wtMode = workTicket.getJobMode();
									if(printDailyPackage.getMode().equals(wtMode)){
										modeFlag = true;
									}else{
										modeFlag = false;
									}												
									}else{
										modeFlag = true;
									}
								if(printDailyPackage.getMilitary()!=null && !printDailyPackage.getMilitary().equalsIgnoreCase("")){
									String misMilitaryShip = miscellaneous.getMillitaryShipment();
									if(printDailyPackage.getMilitary().equalsIgnoreCase(misMilitaryShip)){
										militaryFlag = true;
									}else{
										militaryFlag = false;
									}
									}else{
										militaryFlag = true;
									}
								if(serviceflag && jobFlag && modeFlag && militaryFlag){
									flag =true;
								}
							}else if("All".equals(printDailyPackage.getWtServiceType().trim())){
								if(printDailyPackage.getJob()!=null && !printDailyPackage.getJob().equalsIgnoreCase("")){
									String jobArr[] = printDailyPackage.getJob().split(",");
									for (String job : jobArr) {
										jobArrList.add(job.trim());
									}
									String wtJob = workTicket.getJobType();
										if(jobArrList.contains(wtJob)){
											jobFlag = true;
										}else{
											jobFlag = false;
										}										
									}else{
										jobFlag = true;
									}
								jobArrList= new ArrayList();
								if(printDailyPackage.getMode() != null && !"".equals(printDailyPackage.getMode())){
									String wtMode = workTicket.getJobMode();
									if(printDailyPackage.getMode().equals(wtMode)){
										modeFlag = true;
									}else{
										modeFlag = false;
									}												
									}else{
										modeFlag = true;
									}
								if(printDailyPackage.getMilitary()!=null && !printDailyPackage.getMilitary().equalsIgnoreCase("")){
									String misMilitaryShip = miscellaneous.getMillitaryShipment();
									if(printDailyPackage.getMilitary().equalsIgnoreCase(misMilitaryShip)){
										militaryFlag = true;
									}else{
										militaryFlag = false;
									}
									}else{
										militaryFlag = true;
									}
								if(jobFlag && modeFlag && militaryFlag){
									flag =true;
								}
							}
							if(flag){
								try {
									System.out.println(workTicket.getShipNumber()+" : "+workTicket.getTicket()+" : "+printChild.getJrxmlName());
									jasperPrints = getParameter(printDailyPackage,printChild,"WorkTicket",reportTemplate,jasperPrints,workTicket,parameters,allParameters);
									if(printDailyPackage.getOnePerSOperDay() != null && "Y".equals(printDailyPackage.getOnePerSOperDay().trim())){
										SOList.add(workTicket.getShipNumber()+printChild.getJrxmlName());
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
			//System.out.println(SOList);
		}

		if (jasperPrints != null && jasperPrints.size() > 0) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(new Date()));
			String dateFrom = nowYYYYMMDD1.toString();
			String fileName = "Print_Daily_Package-"+ dateFrom;
			
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
			
			JRExporter exporterPDF = new JRPdfExporter();
			exporterPDF.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, true);
			exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrints);
			exporterPDF.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterPDF.exportReport();
			
			ouputStream.write(byteArrayOutputStream.toByteArray());
			ouputStream.close();
		}else{
			createPDF(response,true);
		}
		
		Long timeTaken = System.currentTimeMillis()-startTime;
		logger.warn("\n\nTime taken to find and merge All jrxml Pdf : "+timeTaken+"\n\n");
		return null;
}
	
	public void createPDF(HttpServletResponse response,boolean isAdminSetUp){
		Document document = new Document();
	    try{
	        response.setContentType("application/pdf");
	        PdfWriter.getInstance(document, response.getOutputStream());
	        document.open();
	        document.add(new Paragraph("WareHouse: "+whouse));
	        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(printDate));
			String dateFrom = nowYYYYMMDD1.toString();
	        document.add(new Paragraph("Date: "+dateFrom));
	        if(isAdminSetUp){
		        document.add(new Paragraph("There is nothing to print for the selected date and WH "));
	        }else{
	        	document.add(new Paragraph("There is no Admin form SetUp to print for the selected date and WH "));
	        }
	    }catch(Exception e){
	        e.printStackTrace();
	    }
	    document.close();
	}
	
	private String billToCodeList;
	@SkipValidation
	public String getBillToCodeAjax(){
		billToCodeList = reportsManager.getBillToCodeByFormName(formName,sessionCorpID);
		if(billToCodeList!=null && !billToCodeList.isEmpty()){
		 returnAjaxStringValue = billToCodeList.toString().replace("(", "").replace(")", "").replace("'", "").trim();		 	
		}
		return SUCCESS;
	}

	public ReportsManager getReportsManager() {
		return reportsManager;
	}

	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public WorkTicketManager getWorkTicketManager() {
		return workTicketManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public List<JRParameter> getReportParameters() {
		return reportParameters;
	}

	public void setReportParameters(List<JRParameter> reportParameters) {
		this.reportParameters = reportParameters;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFromDt() {
		return fromDt;
	}

	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}

	public String getToDt() {
		return toDt;
	}

	public void setToDt(String toDt) {
		this.toDt = toDt;
	}

	public String getWhouse() {
		return whouse;
	}

	public void setWhouse(String whouse) {
		this.whouse = whouse;
	}

	public PrintDailyPackageManager getPrintDailyPackageManager() {
		return printDailyPackageManager;
	}

	public void setPrintDailyPackageManager(
			PrintDailyPackageManager printDailyPackageManager) {
		this.printDailyPackageManager = printDailyPackageManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public PrintDailyPackage getPrintDailyPackage() {
		return printDailyPackage;
	}

	public void setPrintDailyPackage(PrintDailyPackage printDailyPackage) {
		this.printDailyPackage = printDailyPackage;
	}

	public List<PrintDailyPackage> getPrintDailyPackageList() {
		return printDailyPackageList;
	}

	public void setPrintDailyPackageList(
			List<PrintDailyPackage> printDailyPackageList) {
		this.printDailyPackageList = printDailyPackageList;
	}

	public Map<String, String> getWtServiceType() {
		return wtServiceType;
	}

	public void setWtServiceType(Map<String, String> wtServiceType) {
		this.wtServiceType = wtServiceType;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getYesno() {
		return yesno;
	}

	public void setYesno(Map<String, String> yesno) {
		this.yesno = yesno;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public List<PrintDailyPackage> getPrintDailyPackageChildList() {
		return printDailyPackageChildList;
	}

	public void setPrintDailyPackageChildList(
			List<PrintDailyPackage> printDailyPackageChildList) {
		this.printDailyPackageChildList = printDailyPackageChildList;
	}

	public Long getChildId() {
		return childId;
	}

	public void setChildId(Long childId) {
		this.childId = childId;
	}

	public String getPrintURL() {
		return printURL;
	}

	public void setPrintURL(String printURL) {
		this.printURL = printURL;
	}

	public String getJrxmlName() {
		return jrxmlName;
	}

	public void setJrxmlName(String jrxmlName) {
		this.jrxmlName = jrxmlName;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public List getPrintAutoCompleteList() {
		return printAutoCompleteList;
	}

	public void setPrintAutoCompleteList(List printAutoCompleteList) {
		this.printAutoCompleteList = printAutoCompleteList;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceMode() {
		return serviceMode;
	}

	public void setServiceMode(String serviceMode) {
		this.serviceMode = serviceMode;
	}

	public String getMilitary() {
		return military;
	}

	public void setMilitary(String military) {
		this.military = military;
	}

	public List getMultiplServiceTypes() {
		return multiplServiceTypes;
	}

	public void setMultiplServiceTypes(List multiplServiceTypes) {
		this.multiplServiceTypes = multiplServiceTypes;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public List getReportDescList() {
		return reportDescList;
	}

	public void setReportDescList(List reportDescList) {
		this.reportDescList = reportDescList;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public Map<String, String> getModuleReport() {
		return moduleReport;
	}

	public void setModuleReport(Map<String, String> moduleReport) {
		this.moduleReport = moduleReport;
	}

	public List getCommonAjaxList() {
		return commonAjaxList;
	}

	public void setCommonAjaxList(List commonAjaxList) {
		this.commonAjaxList = commonAjaxList;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getPathSeparator() {
		return pathSeparator;
	}

	public void setPathSeparator(String pathSeparator) {
		this.pathSeparator = pathSeparator;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public Map<String, String> getJobs() {
		return jobs;
	}

	public void setJobs(Map<String, String> jobs) {
		this.jobs = jobs;
	}

	public String getBillToCodeList() {
		return billToCodeList;
	}

	public void setBillToCodeList(String billToCodeList) {
		this.billToCodeList = billToCodeList;
	}

	public List getMultiplJobType() {
		return multiplJobType;
	}

	public void setMultiplJobType(List multiplJobType) {
		this.multiplJobType = multiplJobType;
	}

	public boolean isJobFlag() {
		return jobFlag;
	}

	public void setJobFlag(boolean jobFlag) {
		this.jobFlag = jobFlag;
	}

	public boolean isServiceflag() {
		return serviceflag;
	}

	public void setServiceflag(boolean serviceflag) {
		this.serviceflag = serviceflag;
	}

	public boolean isModeFlag() {
		return modeFlag;
	}

	public void setModeFlag(boolean modeFlag) {
		this.modeFlag = modeFlag;
	}

	public boolean isMilitaryFlag() {
		return militaryFlag;
	}

	public void setMilitaryFlag(boolean militaryFlag) {
		this.militaryFlag = militaryFlag;
	}

	public List getServiceArrList() {
		return serviceArrList;
	}

	public void setServiceArrList(List serviceArrList) {
		this.serviceArrList = serviceArrList;
	}

	public List getJobArrList() {
		return jobArrList;
	}

	public void setJobArrList(List jobArrList) {
		this.jobArrList = jobArrList;
	}
	
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	
}
