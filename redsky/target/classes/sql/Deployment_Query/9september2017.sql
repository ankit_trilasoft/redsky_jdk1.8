// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


ALTER TABLE `redsky`.`cportalresourcemgmt` ADD COLUMN `mode` VARCHAR(20) NULL  AFTER `docSequenceNumber` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `status` bit(1) DEFAULT true ;
update company set status =true ;

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `doNotSendEmailtoAgentUser` BIT(1) NULL DEFAULT b'0'  AFTER `ugwwCarrierCode` ;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `welcomeMailSentOn` DATETIME NULL  AFTER `updaterUpdatedOn` ;



