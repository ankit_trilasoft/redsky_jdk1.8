/**
 * 
 */
package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.collections.list.SetUniqueList;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.webapp.util.GetSortedMap;

/**
 * @author GVerma
 *
 */
public class ServiceOrderActionNew  extends BaseAction implements Preparable{

	// Variables declaration 
	private String sessionCorpID; // used for carrying user company code (corpid).
	private String usertype; // User type of login user
	private Date pricePointUserStartDate; // price point start date get from user 
	private String companyCode;  // login user  Company division 
	private boolean activeStatus = false; // login user default search check 
	private String bookingAppUserCode; // login user booking agent code
	private boolean acctDisplayEntitled = false; // login user account Display Entitled
	private boolean acctDisplayEstimate = false;// login user account Display Estimate
	private boolean acctDisplayRevision = false;// login user account Display Revision
	static final Logger logger = Logger.getLogger(ServiceOrderActionNew.class); // Log4j logger instance 
    Date currentdate = new Date(); // Current date for logging 
    private Long id; // Id used for carrying service order id
    private ServiceOrder agentServiceOrderCombo; // service order instance for agent type user login 
    private Map<String,String> localParameter; // local collection for refmaster values
    private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>(); // contains all parameter value of refmaster
    private ServiceOrder serviceOrderCombo; // serviceorder object for getcombolist 
    private Company company; // compnay intance
    private SystemDefault systemDefault; // system default instance
    private ServiceOrder serviceOrder; // page object of service order
    private String shipSize ; //  maximum service order in CF used for so navigation
    private String minShip ;//  minimum service order in CF used for so navigation
    private String countShip ;//  number of service order in CF used for so navigation
    private String validateFormNav ; // used for tab choosen display
    private CustomerFile customerFile ; // CF instance used on jsp
    private Miscellaneous miscellaneous;// miscellaneous instance used on jsp
    private Billing billing;// billing instance used on jsp
    private TrackingStatus trackingStatus ; // TrackingStatus instance used on jsp
	private Map<String, String> originAddress; // additional address map
	private Map<String, String> destinationAddress; // additional address map
	private String countOriginDetailNotes; // for notes count
	private String countDestinationDetailNotes;// for notes count
	private String countWeightDetailNotes;// for notes count
	private String countVipDetailNotes;// for notes count
	private String countServiceOrderNotes;// for notes count
	private String countCustomerFeedbackNotes;// for notes count
	private String companies=""; // for check of company division at company level
	private List companyDivis=new ArrayList(); //company division list
	private Boolean isNetworkRecord = false; // checking  network agent
	private String weightType; // unit for weight and vol
	private String disableChk; // unit check system default
    
    
    // Combolist Collection variables 
    private List packingServiceList; // List of refmaster parameter --->PackingService value contain refmaster desciption
    private Map<String,String> grpCountryList; // Map of refmaster parameter --->COUNTRY value contain refmaster <desciption,desciption>
    private Map<String,String> grpContinentList; // Map of refmaster parameter --->COUNTRY value contain refmaster <flex4 , flex4>
	private Map<String,String> grpModeList; // Map of refmaster parameter --->MODE value contain refmaster <desciption,desciption>
	private Map<String,String> grpAgeStatusList; // Map of refmaster parameter --->groupagestatus value contain refmaster <code,desciption>
	private Map<String, String> countryCod; // Map of refmaster parameter --->COUNTRY value contain refmaster <code , desciption>
	private Map<String, String> state;  // Map of refmaster parameter --->STATE value contain refmaster <code , desciption>
	private Map<String, String> ocountry; //  replica grpCountryList
    private Map<String, String> dcountry;//  replica grpCountryList
	private Map<String, String> job; // Map of refmaster parameter --->JOB value contain refmaster <code , desciption>
	private Map<String, String> relocationServices;// Map of refmaster parameter --->RELOCATIONSERVICES value contain refmaster <code , flex1>
	private Map<String, String> routing; // Map of refmaster parameter --->ROUTING value contain refmaster <code , Description>
	private Map<String, String> commodit;// Map of refmaster parameter --->COMMODIT value contain refmaster <code , Description>
	private Map<String, String> commodits; // Map of refmaster parameter --->COMMODITS value contain refmaster <code , Description>
	private Map<String, String> service;// Map of refmaster parameter --->SERVICE value contain refmaster <code , Description> and flex1 is not relo
	private Map<String, String> reloService;// Map of refmaster parameter --->SERVICE value contain refmaster <code , Description> and flex1 is relo
	private Map<String, String> pkmode;// Map of refmaster parameter --->PKMODE value contain refmaster <code , Description> 
	private Map<String, String> loadsite;// Map of refmaster parameter --->LOADSITE value contain refmaster <code , Description> 
	private Map<String, String> specific;// Map of refmaster parameter --->SPECIFIC value contain refmaster <code , Description> 
	private Map<String, String> jobtype;// Map of refmaster parameter --->JOBTYPE value contain refmaster <code , Description> 
	private Map<String, String> military;// Map of refmaster parameter --->MILITARY value contain refmaster <code , code> 
	private Map<String, String> tcktservc;// Map of refmaster parameter --->TCKTSERVC value contain refmaster <code , Description> 
	private Map<String, String> dept;// Map of refmaster parameter --->DEPT value contain refmaster <code , Description> 
	private Map<String, String> house;// Map of refmaster parameter --->HOUSE value contain refmaster <code , Description> 
	private Map<String, String> confirm;// Map of refmaster parameter --->CONFIRM value contain refmaster <code , Description> 
	public 	Map<String, String> tcktactn;// Map of refmaster parameter --->TCKTACTN value contain refmaster <code , Description> 
	private Map<String, String> woinstr;// Map of refmaster parameter --->WOINSTR value contain refmaster <code , Description> 
	private Map<String, String> paytype;// Map of refmaster parameter --->PAYTYPE value contain refmaster <code , Description> 
	private Map<String, String> yesno;// Map of refmaster parameter --->YESNO value contain refmaster <code , Description> 
	private Map<String, String>glcodes;// Map of refmaster parameter --->GLCODES value contain refmaster <code ,code:Description> 
	private Map<String, String>isdriver;// Map of refmaster parameter --->ISDRIVER value contain refmaster <code , Description> 
	private Map<String, String> partnerType;// Map of refmaster parameter --->partnerType value contain refmaster <code , Description> 
	private Map<String, String> SITOUTTA;// Map of refmaster parameter --->SITOUTTA value contain refmaster <code , Description> 
	private Map<String, String> omni;// Map of refmaster parameter --->omni value contain refmaster <code , Description> 
	private Map<String, String> QUOTESTATUS;// Map of refmaster parameter --->QUOTESTATUS value contain refmaster <code , Description> 
	private Map<String, String> Special;// Map of refmaster parameter --->SPECIAL value contain refmaster <code:Description , code:Description> 
	private Map<String, String> Peak;// Map of refmaster parameter --->PEAK value contain refmaster <code , Description> 
	private Map<String, String> Howpack;// Map of refmaster parameter --->HOWPACK value contain refmaster <code , Description> 
	private Map<String, String> AGTPER;// Map of refmaster parameter --->AGTPER value contain refmaster <code , Description> 
	private Map<String, String> EQUIP;// Map of refmaster parameter --->EQUIP value contain refmaster <code , Description> 
	private Map<String, String> sale;// User of ROLE_SALE of session corpid  value contain User <username , alias> 
	private Map<String, String> qc;//  User of ROLE_QC of session corpid  value contain User <username , alias>
	private Map<String, String> coord;// User of ROLE_COORD of session corpid  value contain User <username , alias>
	private Map<String, String> JOB_STATUS;// Map of refmaster parameter --->JOB_STATUS value contain refmaster <code , Description> 
	private Map<String, String> statusReason;// Map of refmaster parameter --->STATUSREASON value contain refmaster <code , Description> 
	private Map<String, String> category;// Map of refmaster parameter --->ACC_CATEGORY value contain refmaster <code , Description> 
	private Map<String, String> qualitySurveyBy;// Map of refmaster parameter --->QUALITYSURVEYBY value contain refmaster <code , Description> 
	private Map<String,String> basis;// Map of refmaster parameter --->ACC_BASIS value contain refmaster <code , Description> 
	private Map<String,String> country;// Map of refmaster parameter --->CURRENCY value contain refmaster <code , code> 
	private Map<String,String> payingStatus;// Map of refmaster parameter --->ACC_STATUS value contain refmaster <code , Description> 
	private Map<String, String> ostates;// Map of refmaster parameter --->STATE value contain refmaster <code , Description> and bucket2 is so country code
	private Map<String, String> dstates;// Map of refmaster parameter --->STATE value contain refmaster <code , Description> and bucket2 is so country code
	private Map<String, String> pricingData;//  User of ROLE_PRICING of session corpid  value contain User <username , alias>
	private Map<String, String> billingData;// User of ROLE_BILLING,ROLE_BILLING_ARM,ROLE_BILLING_CAJ,ROLE_BILLING_SSC of session corpid  value contain User <username , alias>
	private Map<String, String> payableData;// User of ROLE_PAYABLE of session corpid  value contain User <username , alias>
	private List mode ;// Map of refmaster parameter --->MODE value contain refmaster  Description
	private List customerFeedBack; // Map of refmaster parameter --->customerFeedBack value contain refmaster  Description
	private Map<String,String> oaEvaluation; // Map of refmaster parameter --->OAEVALUATION value contain refmaster <code , Description> 
	private Map<String,String> daEvaluation;// Map of refmaster parameter --->DAEVALUATION value contain refmaster <code , Description> 
	private  Map<String,String> euVatList; // Map of refmaster parameter --->EUVAT value contain refmaster <code , Description> 
	private  Map<String,String> euVatPercentList; // Map of refmaster parameter --->EUVAT value contain refmaster <code , flex1> 
	private  Map<String,String> payVatList; // Map of refmaster parameter --->PAYVATDESC value contain refmaster <code , Description> 
	private  Map<String,String> payVatPercentList; // Map of refmaster parameter --->PAYVATDESC value contain refmaster <code , flex1> 
	private Map<String, String>  assignmentTypes; // Map of refmaster parameter --->AssignmentType value contain refmaster <code , flex1>  where flex1 is not ROLE_CORP_ACC_ASML
	private Map<String, String>  collectiveAssignmentTypes; // Map of refmaster parameter --->AssignmentType value contain refmaster <code , Description> 
	private Map<String, String> flagCarrierList; // Map of refmaster parameter --->FLAGCARRIER value contain refmaster <code , Description> 
	private Map<String, String> languageList; // Map of refmaster parameter --->LANGUAGE value contain refmaster <flex1 , code> 
    private String reloJobFlag; //  refmaster parameter JOB flex3 value
    private Map<String,String> grpStatusList; // Map of refmaster parameter --->CUST_STATUS value contain refmaster <code , Description> 
	private Map<String, String> shipmentType; // Map of refmaster parameter --->SHIPTYPE value contain refmaster <code , Description> 
	private Map<String, String> customerSettled; // Map of refmaster parameter --->CustomerSettled value contain refmaster <code , Description> 
	private Map<String, String> accEstmateStatus;// Map of refmaster parameter --->AccEstmateStatus value contain refmaster <code , Description> 
	private List weightunits;  //weight unit list
	private List volumeunits; // Vol unit list
    private List feedback; // feed back value yes/no
    private List lengthunits; // length unit list
	private Map<String, String> responseEvaluation; // response map
	private Map<String, String> coordinatorList; //User of ROLE_COORD of session corpid  value contain User <username , alias>  based on so jobtype
	private Map<String, String> estimatorList;//User of ROLE_SALE of session corpid  value contain User <username , alias>  based on so jobtype
	private  Map<String,String>estVatList; // replica euVatList
	private  Map<String,String>estVatPersentList; //replica euVatPercentList
	private String jsonCountryCodeText; // json string of country code used in jsp  
	
	// IOC of  service layer dependency
    private ServiceOrderManager serviceOrderManager; 
    private ErrorLogManager errorLogManager;
    private RefMasterManager refMasterManager;
    private CompanyManager companyManager;
    private SystemDefaultManager systemDefaultManager;
    private CustomerFileManager customerFileManager ;
    private MiscellaneousManager miscellaneousManager;
    private BillingManager billingManager;
    private TrackingStatusManager trackingStatusManager;
    private AdAddressesDetailsManager adAddressesDetailsManager ;
    private  NotesManager notesManager;
	
	public ServiceOrderActionNew(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
        pricePointUserStartDate=user.getPricePointStartDate();
        companyCode=user.getCompanyDivision();
        if(user.isDefaultSearchStatus()){
 		   activeStatus=true;
 	   }else{
 		   activeStatus=false;
 	   }       
       bookingAppUserCode=user.getBookingAgent();
       if(user.getAcctDisplayEntitled()!=null){
        	acctDisplayEntitled=user.getAcctDisplayEntitled();
       }
       if(user.getAcctDisplayEstimate()!=null){
        	acctDisplayEstimate=user.getAcctDisplayEstimate();
       }
       if(user.getAcctDisplayRevision()!=null){
        	acctDisplayRevision=user.getAcctDisplayRevision();
       }
	}
	
	public void prepare() throws Exception {
		 company=companyManager.findByCorpID(sessionCorpID).get(0);
		 systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
		 getComboList(sessionCorpID);
		 // Compny level flags check.
		 if(company.getCompanyDivisionFlag()!=null){
			 companies=	 company.getCompanyDivisionFlag();
		 }
		 // system default check
		 if(systemDefault.getUnitVariable()!=null && systemDefault.getUnitVariable().equalsIgnoreCase("Yes"))
         	disableChk ="Y";    	            	
             else
         	disableChk ="N";
	}

//  Method for getting the values for all comboboxes in the dropdown list.
	@SkipValidation
	public Map<String,String> findByParameterLocal(String parameter){ 
		localParameter = new HashMap<String, String>();
		if(parameterMap.containsKey(parameter)){
			localParameter = parameterMap.get(parameter);
		}
		return localParameter;
	}
	
	
	public String returnCountryCode(String bucket2) {
		String countryCode = "";
		try {
			if (bucket2.equalsIgnoreCase("United States")) {
				countryCode = "USA";
				} else if (bucket2.equalsIgnoreCase("India")) {
					countryCode = "IND";
				} else if (bucket2.equalsIgnoreCase("Canada")) {
					countryCode = "CAN";
				} else {
					countryCode = bucket2;
				}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return countryCode;
	}
	@SkipValidation
    public String getComboList(String corpId){
	Long StartTime = System.currentTimeMillis();
    
		
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	 try {
		 if((usertype.trim().equalsIgnoreCase("AGENT"))){ 
		id = new Long(getRequest().getParameter("id").toString());
		 if(id!=null){
			 agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(id);
			 corpId=agentServiceOrderCombo.getCorpID();
			}
		 }
	 } catch (Exception e) {
		e.printStackTrace();
	 }
	 
	 try {
		if(serviceOrderCombo!=null){
			}else{
			try {
				id = new Long(getRequest().getParameter("id").toString());
				 if(id!=null){
					 serviceOrderCombo = serviceOrderManager.get(id);
					}
			} catch (Exception e) {
				e.printStackTrace();
			}
			}
	} catch (Exception e1) {
		e1.printStackTrace();
	}
    	     try {
    	    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Getting parameter values from combolist");
    	    	 String parameters="'CUST_STATUS','groupagestatus','STATE','COUNTRY', 'JOB','ROUTING','COMMODIT','COMMODITS','PKMODE','LOADSITE', 'SPECIFIC','JOBTYPE','MILITARY','SPECIAL','HOWPACK','AGTPER','EQUIP','PEAK','TCKTSERVC','DEPT','HOUSE', 'CONFIRM','TCKTACTN','WOINSTR','PAYTYPE','STATUSREASON','GLCODES','ISDRIVER','YESNO','SHIPTYPE','partnerType','SITOUTTA','omni',  'QUOTESTATUS','JOB_STATUS','ACC_CATEGORY','QUALITYSURVEYBY','ACC_BASIS','ACC_STATUS','CustomerSettled','AccEstmateStatus'" +
    	    	 		",'RELOCATIONSERVICES','PackingService','CUST_STATUS','MODE','customerfeedback','OAEVALUATION','DAEVALUATION','CURRENCY','EUVAT','AssignmentType','SERVICE','FLAGCARRIER','LANGUAGE','PAYVATDESC'";
    	    	 logger.warn("Parameters: "+parameters);
    	    	 LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
    	    	 packingServiceList= new ArrayList();
    	    	 grpCountryList = new LinkedHashMap<String, String>();
    	    	 ocountry = new LinkedHashMap<String, String>();
    	    	 dcountry = new LinkedHashMap<String, String>();
    	    	 grpContinentList = new LinkedHashMap<String, String>();
    	    	 grpModeList = new LinkedHashMap<String, String>();
    	    	 relocationServices = new LinkedHashMap<String, String>();
    	    	 mode = new ArrayList();
    	    	 customerFeedBack = new ArrayList();
    	    	 oaEvaluation = new LinkedHashMap<String, String>();
    	    	 daEvaluation = new LinkedHashMap<String, String>();
    	    	 country = new LinkedHashMap<String, String>();
    	    	 euVatList = new LinkedHashMap<String, String>();
    	    	 euVatPercentList = new LinkedHashMap<String, String>();
    	    	 payVatList = new LinkedHashMap<String, String>();
    	    	 payVatPercentList = new LinkedHashMap<String, String>();
    	    	 Special = new LinkedHashMap<String, String>(); 
    	    	 glcodes = new LinkedHashMap<String, String>();
    	    	 military = new TreeMap<String, String>();
    	    	 assignmentTypes = new LinkedHashMap<String, String>();
    	    	 collectiveAssignmentTypes =  new LinkedHashMap<String, String>();
    	    	 reloService = new LinkedHashMap<String, String>();
    	    	 service = new LinkedHashMap<String, String>();
    	    	 ostates = new LinkedHashMap<String, String>();
    			 dstates = new LinkedHashMap<String, String>();
    			 state = new LinkedHashMap<String, String>();
    			 countryCod = new LinkedHashMap<String, String>();
    			 routing = new TreeMap<String, String>();
    			 job = new TreeMap<String, String>();
    			 flagCarrierList = new LinkedHashMap<String, String>();
    			 languageList = new LinkedHashMap<String, String>();
    			 commodits = new TreeMap<String, String>();
    			 commodit = new TreeMap<String, String>();
    			 Gson gson = new Gson();
    			 String CompanyDivisionCombo=getRequest().getParameter("serviceOrder.companyDivision");
    			 if(CompanyDivisionCombo!=null && (!(CompanyDivisionCombo.toString().equals("")))){
    					
    				}else{
    					if(serviceOrderCombo!=null){
    					CompanyDivisionCombo=serviceOrderCombo.getCompanyDivision();
    					}
    				}
    			 String jobCombo=getRequest().getParameter("serviceOrder.job");
    				if(jobCombo!=null && (!(jobCombo.toString().equals("")))){
    					
    				}else{
    					if(serviceOrderCombo!=null){
    					jobCombo=serviceOrderCombo.getJob();	
    					}
    				}
    				String originCountry="";
		 			String destCountry ="";
		 			if(serviceOrderCombo!=null){
		 				if(serviceOrderCombo.getOriginCountryCode()!=null){
		 					String temp=getRequest().getParameter("serviceOrder.OriginCountryCode");
		 		            if(temp!=null && (!(temp.toString().equals("")))){
		 					
		 				    }else{
		 					temp = serviceOrderCombo.getOriginCountryCode();
		 				    }
		 					originCountry = returnCountryCode(temp);
		 				}
		 				if(serviceOrderCombo.getDestinationCountryCode()!=null){
		 					String temp=getRequest().getParameter("serviceOrder.destinationCountryCode");
		 		            if(temp!=null && (!(temp.toString().equals("")))){
		 					
		 				    }else{
		 					temp = serviceOrderCombo.getDestinationCountryCode();
		 				    }
		 					destCountry = returnCountryCode(temp);
		 				}
		 			}	
    	    	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
    	    	 for (RefMasterDTO refObj : allParamValue) {
    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("PackingService")){
    	    		 		packingServiceList.add(refObj.getDescription().trim());
    	    		 	}else if(refObj.getParameter().trim().equals("COUNTRY")){
    	    		 		grpCountryList.put(refObj.getDescription().trim(),refObj.getDescription().trim());
    	    		 		countryCod.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 	}else if(refObj.getParameter().trim().equals("COUNTRY") && !refObj.getFlex4().trim().equals("") ){
    	    		 		grpContinentList.put(refObj.getFlex4().trim(),refObj.getFlex4().trim());    
    	    		 	}else if(refObj.getParameter().trim().equals("MODE")){
    	    		 		grpModeList.put(refObj.getDescription().trim(),refObj.getDescription().trim());
    	    		 		mode.add(refObj.getDescription().trim());
    	    		 	}else if(refObj.getParameter().trim().equals("RELOCATIONSERVICES")){
    	    		 		relocationServices.put(refObj.getCode().trim(), refObj.getFlex1().trim());
    	    		 	}else if (refObj.getParameter().trim().equals("customerfeedback")){
    	    		 		customerFeedBack.add(refObj.getDescription().trim());
    	    		 	}else if (refObj.getParameter().trim().equals("OAEVALUATION")){
    	    		 		oaEvaluation.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
    	    		 	}else if (refObj.getParameter().trim().equals("DAEVALUATION")){
    	    		 		daEvaluation.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
    	    		 	}else if (refObj.getParameter().trim().equals("CURRENCY")){
    	    		 		country.put(refObj.getCode().trim(),refObj.getCode().trim()); 
    	    		 	}else if (refObj.getParameter().trim().equals("EUVAT")){
    	    		 		euVatList.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
    	    		 		euVatPercentList.put(refObj.getCode().trim(),refObj.getFlex1().trim());
    	    		 	}else if (refObj.getParameter().trim().equals("PAYVATDESC")){
    	    		 		payVatList.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
    	    		 		payVatPercentList.put(refObj.getCode().trim(),refObj.getFlex1().trim());
    	    		 	}else if (refObj.getParameter().trim().equals("SPECIAL")){
    	    		 		Special.put(refObj.getCode().trim() + " : "+ refObj.getDescription().trim(),refObj.getCode().trim() + " : "+ refObj.getDescription().trim()); 
    	    		 	}else if (refObj.getParameter().trim().equals("GLCODES")){
    	    		 		glcodes.put(refObj.getCode().trim() ,refObj.getCode().trim() + " : "+ refObj.getDescription().trim()); 
    	    		 	}else if (refObj.getParameter().trim().equals("MILITARY")){
    	    		 		military.put(refObj.getCode().trim() ,refObj.getCode().trim() ); 
    	    		 	}else if(refObj.getParameter().trim().equals("AssignmentType")){
    	    		 		collectiveAssignmentTypes.put(refObj.getCode().trim() ,refObj.getDescription().trim() );
    	    		 		if(refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("ROLE_CORP_ACC_ASML")){
    	    		 			assignmentTypes.put(refObj.getCode().trim() ,refObj.getDescription().trim() ); 
    	    		 		}
    	    	 		}else if (refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals("Relo"))){
    	    		 		reloService.put(refObj.getCode().trim() ,refObj.getDescription().trim() ); 
    	    		 	}else if (refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("Relo"))){
    	    		 		service.put(refObj.getCode().trim() ,refObj.getDescription().trim() ); 
    	    		 	}else if (refObj.getParameter().trim().equals("JOB")){
    	    		 		if(CompanyDivisionCombo!=null && (!(CompanyDivisionCombo.toString().equals(""))) && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(CompanyDivisionCombo)) ){
    	    		 			job.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 		}else{
    	    		 			job.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 		}
    	    		 		if( jobCombo!=null && (!(jobCombo.toString().equals(""))) && refObj.getCode().trim().equals(jobCombo) && refObj.getFlex3()!=null  ){
    	    		 			reloJobFlag = refObj.getFlex3().trim();
        	    		 	}
    	    		 	}else if(refObj.getParameter().trim().equals("COMMODITS")){
    	    		 		if(jobCombo!=null && (!(jobCombo.toString().equals("")))&& (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(jobCombo)) ){
    	    		 			commodits.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 		}else{
    	    		 			commodits.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 		}
    	    		 	}else if(refObj.getParameter().trim().equals("COMMODIT")){
    	    		 		if(jobCombo!=null && (!(jobCombo.toString().equals(""))) && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(jobCombo)) ){
    	    		 			commodit.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 		}else{
    	    		 			commodit.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 		}
    	    		 	}else if(refObj.getParameter().trim().equals("FLAGCARRIER")){
    	    		 		flagCarrierList.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 	}else if(refObj.getParameter().trim().equals("LANGUAGE")){
    	    		 		 if(refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") ){
    	    		 			 languageList.put(refObj.getFlex1().trim(),refObj.getCode().trim());
    	    		 		 }
    	    		 	}else {
    	    		 		if(refObj.getParameter().trim().equals("STATE")){
    	    		 			
    	    		 			if(originCountry.equals(refObj.getBucket2().trim()))
    	    		 				ostates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
    	    		 			if(destCountry.equals(refObj.getBucket2().trim()))
    	    		 				dstates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
    	    		 		}
    	    		 		if(parameterMap.containsKey(refObj.getParameter().trim())){
    	    		 			tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
    	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
    	    		 		}else{
    	    		 			tempParemeterMap = new LinkedHashMap<String, String>();
    	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
    	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
    	    		 		}
    	    		 	}
    	    		
				}
    	    	mode = SetUniqueList.decorate(mode);
				grpStatusList=findByParameterLocal( "CUST_STATUS");
				grpAgeStatusList=findByParameterLocal( "groupagestatus");
				state = findByParameterLocal( "STATE");
				ocountry.putAll(grpCountryList);
				dcountry.putAll(grpCountryList);
				routing = findByParameterLocal("ROUTING");
				routing = GetSortedMap.sortByValues(routing);
				pkmode = findByParameterLocal("PKMODE");
				pkmode = GetSortedMap.sortByValues(pkmode);
				loadsite =findByParameterLocal("LOADSITE");
				specific = findByParameterLocal("SPECIFIC");
				jobtype = findByParameterLocal("JOBTYPE");
				Howpack =findByParameterLocal("HOWPACK");
				AGTPER = findByParameterLocal("AGTPER");
				EQUIP = findByParameterLocal("EQUIP");
				Peak = findByParameterLocal("PEAK");
				tcktservc = findByParameterLocal("TCKTSERVC");
				dept= findByParameterLocal("DEPT");
				house= findByParameterLocal("HOUSE");
				confirm= findByParameterLocal("CONFIRM");
				tcktactn= findByParameterLocal("TCKTACTN");
				woinstr= findByParameterLocal("WOINSTR");
				paytype = findByParameterLocal("PAYTYPE");
				statusReason = findByParameterLocal("STATUSREASON");
				isdriver = findByParameterLocal("ISDRIVER");
				yesno = findByParameterLocal("YESNO");
				shipmentType = findByParameterLocal("SHIPTYPE");
				partnerType = findByParameterLocal("partnerType");
				SITOUTTA = findByParameterLocal("SITOUTTA");
				omni = findByParameterLocal("omni");
				QUOTESTATUS = findByParameterLocal("QUOTESTATUS");
				JOB_STATUS  =findByParameterLocal("JOB_STATUS");
				category = findByParameterLocal("ACC_CATEGORY");
				qualitySurveyBy = findByParameterLocal("QUALITYSURVEYBY");
				category.put("", "");
				basis=findByParameterLocal("ACC_BASIS");
				basis.put("", "");
				payingStatus=findByParameterLocal("ACC_STATUS");
				customerSettled = findByParameterLocal("CustomerSettled");
				accEstmateStatus = findByParameterLocal("AccEstmateStatus");
				weightunits = new ArrayList();
				weightunits.add("Lbs");
				weightunits.add("Kgs");
				volumeunits = new ArrayList();
				volumeunits.add("Cft");
				volumeunits.add("Cbm");
				lengthunits=new ArrayList();
				lengthunits.add("Ft");
				lengthunits.add("Mtr");
				feedback=new ArrayList();
				feedback.add("Yes");
				feedback.add("No");
				responseEvaluation=new HashMap();
				responseEvaluation.put(new String("-1"),"N");
				responseEvaluation.put(new String("1"),"Y");
				estVatList=euVatList;
				estVatPersentList=euVatPercentList;
				jsonCountryCodeText = gson.toJson(countryCod);
				
				// User view  changes
				
				String roles ="'ROLE_COORD','ROLE_SALE','ROLE_QC','ROLE_PRICING','ROLE_PAYABLE','ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC'";
				coord = new TreeMap<String, String>();
				coordinatorList = new TreeMap<String, String>();
				sale = new TreeMap<String, String>();
				estimatorList = new TreeMap<String, String>();
				qc = new TreeMap<String, String>();
				pricingData = new TreeMap<String, String>();
				billingData = new TreeMap<String, String>();
				payableData = new TreeMap<String, String>();
				List <UserDTO> allUser = refMasterManager.getAllUser(corpId,roles);
				for (UserDTO userDTO : allUser) {
					if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
						if(jobCombo!=null && (!(jobCombo.toString().equals("")))  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo)  ) ){
							coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
						}
						coord.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){
						if(jobCombo!=null && (!(jobCombo.toString().equals(""))) && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo)  ) ){
							estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
						}
						sale.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_QC")){
						qc.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
						pricingData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
						billingData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
						payableData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}
					
			} catch (Exception e) {
				logger.error(" ERROR in ServiceOrderAction getComboList ");
				e.printStackTrace();
			}
	  	
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
			  
			return SUCCESS;
   }
	
	public String edit(){
		serviceOrder = serviceOrderManager.get(id);
		customerFile = serviceOrder.getCustomerFile();
		miscellaneous = miscellaneousManager.get(id);
		System.out.println("IN Edit method");
		if(id!=null){
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			try{
			Boolean isNetwork=false;
			isNetworkRecord = false;
			if(serviceOrder.getIsNetworkRecord()==null){
				isNetwork=false;
			}
			else if(serviceOrder.getIsNetworkRecord()){
				isNetwork=true;
				if(serviceOrder.getBookingAgentCode()!=null && !serviceOrder.getBookingAgentCode().toString().equals(""))
					isNetworkRecord = true;
			}
			if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
				weightType="lbscft";
			}
			else{
				weightType="kgscbm";
			}
			}catch(Exception ex){
				logger.warn("Error in Edit method");
				ex.printStackTrace();
			}
		}
		
		 ostates = customerFileManager.findDefaultStateList(serviceOrder.getOriginCountryCode(), sessionCorpID);
	     dstates = customerFileManager.findDefaultStateList(serviceOrder.getDestinationCountryCode(), sessionCorpID);
	     
	     originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
	  	 destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());	
	  	 // Company Division list
	  	companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,serviceOrder.getBookingAgentCode());
	  	 
		// Navigation of Serviceorder
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		getNotesForIconChange();
		return SUCCESS;
	}
	
//  Method getting the count of notes according to which the icon color changes in form
    
    
	@SkipValidation 
    public String getNotesForIconChange() { 
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Map<String, String> soDetailNotes = new LinkedHashMap<String, String>(); 
			String noteSubType="'VipPerson','Origin','Destination','WeightDetail','ServiceOrder','Feedback'";
			soDetailNotes = notesManager.countSoDetailNotes(serviceOrder.getShipNumber(),noteSubType,sessionCorpID);
			
			if(soDetailNotes.containsKey("Origin")){
				countOriginDetailNotes = soDetailNotes.get("Origin");
			}else {
				countOriginDetailNotes = "0" ;
			}
			if(soDetailNotes.containsKey("Destination")){
				countDestinationDetailNotes = soDetailNotes.get("Destination");
			}else {
				countDestinationDetailNotes =  "0"; 
			}
			if(soDetailNotes.containsKey("WeightDetail") ){
				countWeightDetailNotes =  soDetailNotes.get("WeightDetail"); 
			}else {
				countWeightDetailNotes = "0";
			}
			if(soDetailNotes.containsKey("VipPerson")){
				countVipDetailNotes =  soDetailNotes.get("VipPerson"); 
			}else {
				countVipDetailNotes ="0";
			}
			if(soDetailNotes.containsKey("ServiceOrder")  ){
				countServiceOrderNotes =  soDetailNotes.get("ServiceOrder"); 
			}else {
				countServiceOrderNotes = "0";
			}
			if(soDetailNotes.containsKey("Feedback") ){
				countCustomerFeedbackNotes =  soDetailNotes.get("Feedback"); 
			}else {
				countCustomerFeedbackNotes ="0";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return CANCEL;
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }   
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Date getPricePointUserStartDate() {
		return pricePointUserStartDate;
	}

	public void setPricePointUserStartDate(Date pricePointUserStartDate) {
		this.pricePointUserStartDate = pricePointUserStartDate;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public boolean isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getBookingAppUserCode() {
		return bookingAppUserCode;
	}

	public void setBookingAppUserCode(String bookingAppUserCode) {
		this.bookingAppUserCode = bookingAppUserCode;
	}

	public boolean isAcctDisplayEntitled() {
		return acctDisplayEntitled;
	}

	public void setAcctDisplayEntitled(boolean acctDisplayEntitled) {
		this.acctDisplayEntitled = acctDisplayEntitled;
	}

	public boolean isAcctDisplayEstimate() {
		return acctDisplayEstimate;
	}

	public void setAcctDisplayEstimate(boolean acctDisplayEstimate) {
		this.acctDisplayEstimate = acctDisplayEstimate;
	}

	public boolean isAcctDisplayRevision() {
		return acctDisplayRevision;
	}

	public void setAcctDisplayRevision(boolean acctDisplayRevision) {
		this.acctDisplayRevision = acctDisplayRevision;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public List getPackingServiceList() {
		return packingServiceList;
	}

	public void setPackingServiceList(List packingServiceList) {
		this.packingServiceList = packingServiceList;
	}

	public Map<String, String> getGrpCountryList() {
		return grpCountryList;
	}

	public void setGrpCountryList(Map<String, String> grpCountryList) {
		this.grpCountryList = grpCountryList;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public ServiceOrder getAgentServiceOrderCombo() {
		return agentServiceOrderCombo;
	}

	public void setAgentServiceOrderCombo(ServiceOrder agentServiceOrderCombo) {
		this.agentServiceOrderCombo = agentServiceOrderCombo;
	}

	public Map<String, String> getLocalParameter() {
		return localParameter;
	}

	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}

	public Map<String, LinkedHashMap<String, String>> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(
			Map<String, LinkedHashMap<String, String>> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public ServiceOrder getServiceOrderCombo() {
		return serviceOrderCombo;
	}

	public void setServiceOrderCombo(ServiceOrder serviceOrderCombo) {
		this.serviceOrderCombo = serviceOrderCombo;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public Map<String, String> getGrpContinentList() {
		return grpContinentList;
	}

	public void setGrpContinentList(Map<String, String> grpContinentList) {
		this.grpContinentList = grpContinentList;
	}

	public Map<String, String> getGrpModeList() {
		return grpModeList;
	}

	public void setGrpModeList(Map<String, String> grpModeList) {
		this.grpModeList = grpModeList;
	}

	public Map<String, String> getGrpAgeStatusList() {
		return grpAgeStatusList;
	}

	public void setGrpAgeStatusList(Map<String, String> grpAgeStatusList) {
		this.grpAgeStatusList = grpAgeStatusList;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public Map<String, String> getState() {
		return state;
	}

	public void setState(Map<String, String> state) {
		this.state = state;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}

	public Map<String, String> getCommodits() {
		return commodits;
	}

	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}

	public Map<String, String> getService() {
		return service;
	}

	public void setService(Map<String, String> service) {
		this.service = service;
	}

	public Map<String, String> getReloService() {
		return reloService;
	}

	public void setReloService(Map<String, String> reloService) {
		this.reloService = reloService;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public void setPkmode(Map<String, String> pkmode) {
		this.pkmode = pkmode;
	}

	public Map<String, String> getLoadsite() {
		return loadsite;
	}

	public void setLoadsite(Map<String, String> loadsite) {
		this.loadsite = loadsite;
	}

	public Map<String, String> getSpecific() {
		return specific;
	}

	public void setSpecific(Map<String, String> specific) {
		this.specific = specific;
	}

	public Map<String, String> getJobtype() {
		return jobtype;
	}

	public void setJobtype(Map<String, String> jobtype) {
		this.jobtype = jobtype;
	}

	public Map<String, String> getMilitary() {
		return military;
	}

	public void setMilitary(Map<String, String> military) {
		this.military = military;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public Map<String, String> getDept() {
		return dept;
	}

	public void setDept(Map<String, String> dept) {
		this.dept = dept;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public Map<String, String> getConfirm() {
		return confirm;
	}

	public void setConfirm(Map<String, String> confirm) {
		this.confirm = confirm;
	}

	public Map<String, String> getTcktactn() {
		return tcktactn;
	}

	public void setTcktactn(Map<String, String> tcktactn) {
		this.tcktactn = tcktactn;
	}

	public Map<String, String> getWoinstr() {
		return woinstr;
	}

	public void setWoinstr(Map<String, String> woinstr) {
		this.woinstr = woinstr;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public void setPaytype(Map<String, String> paytype) {
		this.paytype = paytype;
	}

	public Map<String, String> getYesno() {
		return yesno;
	}

	public void setYesno(Map<String, String> yesno) {
		this.yesno = yesno;
	}

	public Map<String, String> getGlcodes() {
		return glcodes;
	}

	public void setGlcodes(Map<String, String> glcodes) {
		this.glcodes = glcodes;
	}

	public Map<String, String> getIsdriver() {
		return isdriver;
	}

	public void setIsdriver(Map<String, String> isdriver) {
		this.isdriver = isdriver;
	}

	public Map<String, String> getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(Map<String, String> partnerType) {
		this.partnerType = partnerType;
	}

	public Map<String, String> getSITOUTTA() {
		return SITOUTTA;
	}

	public void setSITOUTTA(Map<String, String> sITOUTTA) {
		SITOUTTA = sITOUTTA;
	}

	public Map<String, String> getOmni() {
		return omni;
	}

	public void setOmni(Map<String, String> omni) {
		this.omni = omni;
	}

	public Map<String, String> getQUOTESTATUS() {
		return QUOTESTATUS;
	}

	public void setQUOTESTATUS(Map<String, String> qUOTESTATUS) {
		QUOTESTATUS = qUOTESTATUS;
	}

	public Map<String, String> getSpecial() {
		return Special;
	}

	public void setSpecial(Map<String, String> special) {
		Special = special;
	}

	public Map<String, String> getPeak() {
		return Peak;
	}

	public void setPeak(Map<String, String> peak) {
		Peak = peak;
	}

	public Map<String, String> getHowpack() {
		return Howpack;
	}

	public void setHowpack(Map<String, String> howpack) {
		Howpack = howpack;
	}

	public Map<String, String> getAGTPER() {
		return AGTPER;
	}

	public void setAGTPER(Map<String, String> aGTPER) {
		AGTPER = aGTPER;
	}

	public Map<String, String> getEQUIP() {
		return EQUIP;
	}

	public void setEQUIP(Map<String, String> eQUIP) {
		EQUIP = eQUIP;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public Map<String, String> getQc() {
		return qc;
	}

	public void setQc(Map<String, String> qc) {
		this.qc = qc;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public Map<String, String> getJOB_STATUS() {
		return JOB_STATUS;
	}

	public void setJOB_STATUS(Map<String, String> jOB_STATUS) {
		JOB_STATUS = jOB_STATUS;
	}

	public Map<String, String> getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(Map<String, String> statusReason) {
		this.statusReason = statusReason;
	}

	public Map<String, String> getCategory() {
		return category;
	}

	public void setCategory(Map<String, String> category) {
		this.category = category;
	}

	public Map<String, String> getQualitySurveyBy() {
		return qualitySurveyBy;
	}

	public void setQualitySurveyBy(Map<String, String> qualitySurveyBy) {
		this.qualitySurveyBy = qualitySurveyBy;
	}

	public Map<String, String> getBasis() {
		return basis;
	}

	public void setBasis(Map<String, String> basis) {
		this.basis = basis;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public Map<String, String> getPayingStatus() {
		return payingStatus;
	}

	public void setPayingStatus(Map<String, String> payingStatus) {
		this.payingStatus = payingStatus;
	}

	public Map<String, String> getOstates() {
		return ostates;
	}

	public void setOstates(Map<String, String> ostates) {
		this.ostates = ostates;
	}

	public Map<String, String> getDstates() {
		return dstates;
	}

	public void setDstates(Map<String, String> dstates) {
		this.dstates = dstates;
	}

	public Map<String, String> getPricingData() {
		return pricingData;
	}

	public void setPricingData(Map<String, String> pricingData) {
		this.pricingData = pricingData;
	}

	public Map<String, String> getBillingData() {
		return billingData;
	}

	public void setBillingData(Map<String, String> billingData) {
		this.billingData = billingData;
	}

	public Map<String, String> getPayableData() {
		return payableData;
	}

	public void setPayableData(Map<String, String> payableData) {
		this.payableData = payableData;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public List getCustomerFeedBack() {
		return customerFeedBack;
	}

	public void setCustomerFeedBack(List customerFeedBack) {
		this.customerFeedBack = customerFeedBack;
	}

	public Map<String, String> getOaEvaluation() {
		return oaEvaluation;
	}

	public void setOaEvaluation(Map<String, String> oaEvaluation) {
		this.oaEvaluation = oaEvaluation;
	}

	public Map<String, String> getDaEvaluation() {
		return daEvaluation;
	}

	public void setDaEvaluation(Map<String, String> daEvaluation) {
		this.daEvaluation = daEvaluation;
	}

	public Map<String, String> getEuVatList() {
		return euVatList;
	}

	public void setEuVatList(Map<String, String> euVatList) {
		this.euVatList = euVatList;
	}

	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}

	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}

	public Map<String, String> getPayVatList() {
		return payVatList;
	}

	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}

	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}

	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}

	public Map<String, String> getAssignmentTypes() {
		return assignmentTypes;
	}

	public void setAssignmentTypes(Map<String, String> assignmentTypes) {
		this.assignmentTypes = assignmentTypes;
	}

	public Map<String, String> getCollectiveAssignmentTypes() {
		return collectiveAssignmentTypes;
	}

	public void setCollectiveAssignmentTypes(
			Map<String, String> collectiveAssignmentTypes) {
		this.collectiveAssignmentTypes = collectiveAssignmentTypes;
	}

	public Map<String, String> getFlagCarrierList() {
		return flagCarrierList;
	}

	public void setFlagCarrierList(Map<String, String> flagCarrierList) {
		this.flagCarrierList = flagCarrierList;
	}

	public Map<String, String> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(Map<String, String> languageList) {
		this.languageList = languageList;
	}

	public String getReloJobFlag() {
		return reloJobFlag;
	}

	public void setReloJobFlag(String reloJobFlag) {
		this.reloJobFlag = reloJobFlag;
	}

	public Map<String, String> getGrpStatusList() {
		return grpStatusList;
	}

	public void setGrpStatusList(Map<String, String> grpStatusList) {
		this.grpStatusList = grpStatusList;
	}

	public Map<String, String> getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(Map<String, String> shipmentType) {
		this.shipmentType = shipmentType;
	}

	public Map<String, String> getCustomerSettled() {
		return customerSettled;
	}

	public void setCustomerSettled(Map<String, String> customerSettled) {
		this.customerSettled = customerSettled;
	}

	public Map<String, String> getAccEstmateStatus() {
		return accEstmateStatus;
	}

	public void setAccEstmateStatus(Map<String, String> accEstmateStatus) {
		this.accEstmateStatus = accEstmateStatus;
	}

	public List getWeightunits() {
		return weightunits;
	}

	public void setWeightunits(List weightunits) {
		this.weightunits = weightunits;
	}

	public List getVolumeunits() {
		return volumeunits;
	}

	public void setVolumeunits(List volumeunits) {
		this.volumeunits = volumeunits;
	}

	public List getFeedback() {
		return feedback;
	}

	public void setFeedback(List feedback) {
		this.feedback = feedback;
	}

	public List getLengthunits() {
		return lengthunits;
	}

	public void setLengthunits(List lengthunits) {
		this.lengthunits = lengthunits;
	}

	public Map<String, String> getResponseEvaluation() {
		return responseEvaluation;
	}

	public void setResponseEvaluation(Map<String, String> responseEvaluation) {
		this.responseEvaluation = responseEvaluation;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}

	public Map<String, String> getEstimatorList() {
		return estimatorList;
	}

	public void setEstimatorList(Map<String, String> estimatorList) {
		this.estimatorList = estimatorList;
	}

	public Map<String, String> getEstVatList() {
		return estVatList;
	}

	public void setEstVatList(Map<String, String> estVatList) {
		this.estVatList = estVatList;
	}

	public Map<String, String> getEstVatPersentList() {
		return estVatPersentList;
	}

	public void setEstVatPersentList(Map<String, String> estVatPersentList) {
		this.estVatPersentList = estVatPersentList;
	}

	public String getJsonCountryCodeText() {
		return jsonCountryCodeText;
	}

	public void setJsonCountryCodeText(String jsonCountryCodeText) {
		this.jsonCountryCodeText = jsonCountryCodeText;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}

	public Map<String, String> getOriginAddress() {
		return originAddress;
	}

	public void setOriginAddress(Map<String, String> originAddress) {
		this.originAddress = originAddress;
	}

	public Map<String, String> getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(Map<String, String> destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getCountOriginDetailNotes() {
		return countOriginDetailNotes;
	}

	public void setCountOriginDetailNotes(String countOriginDetailNotes) {
		this.countOriginDetailNotes = countOriginDetailNotes;
	}

	public String getCountDestinationDetailNotes() {
		return countDestinationDetailNotes;
	}

	public void setCountDestinationDetailNotes(String countDestinationDetailNotes) {
		this.countDestinationDetailNotes = countDestinationDetailNotes;
	}

	public String getCountWeightDetailNotes() {
		return countWeightDetailNotes;
	}

	public void setCountWeightDetailNotes(String countWeightDetailNotes) {
		this.countWeightDetailNotes = countWeightDetailNotes;
	}

	public String getCountVipDetailNotes() {
		return countVipDetailNotes;
	}

	public void setCountVipDetailNotes(String countVipDetailNotes) {
		this.countVipDetailNotes = countVipDetailNotes;
	}

	public String getCountServiceOrderNotes() {
		return countServiceOrderNotes;
	}

	public void setCountServiceOrderNotes(String countServiceOrderNotes) {
		this.countServiceOrderNotes = countServiceOrderNotes;
	}

	public String getCountCustomerFeedbackNotes() {
		return countCustomerFeedbackNotes;
	}

	public void setCountCustomerFeedbackNotes(String countCustomerFeedbackNotes) {
		this.countCustomerFeedbackNotes = countCustomerFeedbackNotes;
	}

	public String getCompanies() {
		return companies;
	}

	public void setCompanies(String companies) {
		this.companies = companies;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public Boolean getIsNetworkRecord() {
		return isNetworkRecord;
	}

	public void setIsNetworkRecord(Boolean isNetworkRecord) {
		this.isNetworkRecord = isNetworkRecord;
	}

	public String getWeightType() {
		return weightType;
	}

	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public String getDisableChk() {
		return disableChk;
	}

	public void setDisableChk(String disableChk) {
		this.disableChk = disableChk;
	}
	
	

}

