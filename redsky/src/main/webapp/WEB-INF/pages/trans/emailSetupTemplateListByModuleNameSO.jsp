<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>   
    <title>Email Setup Template List</title>   
    <meta name="heading" content="Email Setup Template List"/>  
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tinymce.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/customtable-email.css'/>" />
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/customtooltip.css'/>" />
    
 <style>
 span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px; text-align:right;width:99%;
}
.errorChk input[type=checkbox] {
  outline: 2px solid #5bb85d;
}

/* 10-06-2019 */
.custom-tab-dv{
position:relative;
}
.custom-tab-dv .pagelinks{
position:absolute;
top:15px;
}
/*End */
</style>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
 <c:set var="tableName" value="serviceorder" scope="session"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<div id="layer1" style="width:100%" class="custom-tab-dv"> 
	<div id="newmnav">		  
		  <ul>
		   
	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	            <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	            	<c:if test="${(fn:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
		  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		       		<li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
            	</c:if>
            </c:if>
            	</sec-auth:authComponent>
            	</configByCorp:fieldVisibility>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		       <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		       		<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
            	</c:if>
            	<c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
            		<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
            	</c:if>
            </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
             </sec-auth:authComponent>
            </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
            <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             <c:choose>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		     </c:choose>
		     </c:if> 
		     </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
      	  <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
			  		<li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
			  </sec-auth:authComponent>
			</c:if>
            <sec-auth:authComponent componentId="module.tab.serviceorder.forwardingTab">
			  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
			  <c:if test="${usertype!='ACCOUNT'}">
			    <c:if test="${serviceOrder.job !='RLO'}">	
			       <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">     
					<c:if test="${forwardingTabVal!='Y'}">
						<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
					</c:if>
					<c:if test="${forwardingTabVal=='Y'}">
						<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
					</c:if>
				</c:if>	  	
			  </c:if>
			  </c:if>
			  </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.serviceorder.domesticTab">
				<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
					<c:if test="${serviceOrder.job !='RLO'}">  
						<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
					</c:if>
				</c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
		      <c:if test="${serviceOrder.job =='INT'}">
		         <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		      </c:if>
		    </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.serviceorder.statusTab">
				<c:if test="${serviceOrder.job =='RLO'}"> 
				 	<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
				</c:if>
	           	<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
				</c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.summary.summaryTab">
			  	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
			</sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	            <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	            	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	            </c:if>
            </sec-auth:authComponent> 
            <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	            <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
		            <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		            	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		            </c:if>
	            </sec-auth:authComponent> 
            </configByCorp:fieldVisibility>
			
			
			<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<configByCorp:fieldVisibility componentId="component.voxme.inventory">
					 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
				</configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.serviceorder.customerFileTab">	
				<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.customerFile.reportTab">
		    <configByCorp:fieldVisibility componentId="component.tab.customerFile.Reports">
		    <c:if test="${customerFile.moveType=='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&routingfilter=${serviceOrder.routing}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=Quote&reportSubModule=Quote&decorator=popup&popup=true','forms','height=500,width=770,top=20, left=210, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		   	<c:if test="${customerFile.moveType!='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&routingfilter=${serviceOrder.routing}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=500,width=770,top=20, left=210, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		    </configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.serviceorder.auditTab">				
		    	<li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder,miscellaneous&decorator=popup&popup=true','audit','height=420,width=760,top=20, left=200, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		   	</sec-auth:authComponent>
		   <li id="newmnav1" style="background:#FFF "><a class="current"><span>View Emails<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</ul>
		</div><div class="spn">&nbsp;</div>

	<display:table name="emailSetupTemplateList" class="table" requestURI="" id="emailSetupTemplateList" pagesize="10" style="width:99%;margin-top:1px;margin-left: 5px; " >
	
		<display:column property="emailTo" title="To" sortable="true" maxLength="35" style="width:50px;"></display:column>
		<display:column property="emailFrom" title="From" sortable="true" maxLength="35" style="width:50px;"></display:column>
		<display:column title="Description" sortable="true" style="width:150px;">
			<div id="emailTemplateDetails${emailSetupTemplateList.id}" class="modal fade"></div>
			<a onclick="openEmailTemplatePopUp('${emailSetupTemplateList.id}','${serviceOrder.id}')" />${emailSetupTemplateList.saveAs}</a>
			<div class="modal fade" id="ajaxRunningDiv${emailSetupTemplateList.id}" aria-hidden="true">
				<div class="loading-indicator">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw"" style="font-size:54px;color:grey;" id="loading-indicator${emailSetupTemplateList.id}"></i>
				</div>
			</div>
		</display:column>
		<display:column property="emailSubject" title="Subject" sortable="true" style="width:250px;"></display:column>
		<display:column title="Last Sent On" style="width:50px;" >
			<c:choose> 
				<c:when test="${emailSetupTemplateList.emailStatus=='SaveForEmail'}">
					<div ALIGN="center">
						<img  src="${pageContext.request.contextPath}/images/ques-small.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP title="Ready For Sending"/>
					</div>
				</c:when>
				<c:when test="${fn:indexOf(emailSetupTemplateList.emailStatus,'SaveForEmail')>-1}">
					<div ALIGN="center">
						<img  src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP title="Fail"/>
					</div>
				</c:when> 							
				<c:otherwise>
					<fmt:parseDate pattern="MM/dd/yyyy HH:mm" value="${emailSetupTemplateList.emailStatus}" var="parsedEmailStatusDate" />
					<fmt:formatDate pattern="dd-MMM-yy HH:mm" value="${parsedEmailStatusDate}" var="formattedEmailStatusDate" />
					<c:out value="${formattedEmailStatusDate}"></c:out>
				</c:otherwise>			
			</c:choose>
		</display:column>
		<display:column title="Action" headerClass="centeralign" style="text-align:center;width:100px;">
			<button type="button" style="width:4.6em;" class="btn btn-success btn-blue btn-xs" id="load${emailSetupTemplateList.id}" onclick="sendMailFromTemplate('${serviceOrder.id}','${emailSetupTemplateList.id}')"  data-loading-text="<i class='fa fa-spinner fa-spin ' style='font-size:17px;'></i>"><i class='fa fa-paper-plane-o'></i>&nbsp;Send</button>
			<div id="emailTemplateDetailsEdit${emailSetupTemplateList.id}" class="modal fade"></div>
			<button type="button" style="width:4em;" class="btn btn-primary btn-xs" id="editLoad${emailSetupTemplateList.id}" onclick="editMailFromTemplate('${serviceOrder.id}','${emailSetupTemplateList.id}','edit${emailSetupTemplateList.id}')" ><i class='fa fa-pencil'></i>&nbsp;Edit</button>
			<div class="modal fade" id="sendMail${emailSetupTemplateList.id}" aria-hidden="true">
				<div class="modal-dialog" style="width:35%;">
					<div class="modal-content">
						<div class="modal-body" id="modalAlert">
				      		<div class="alert alert-success" id="getSuccessMsg${emailSetupTemplateList.id}">
				      			<strong>${returnAjaxStringValue}</strong>
				    		</div>
				    		<div class="alert alert-danger" id="getErrorMsg${emailSetupTemplateList.id}">
							  <strong>${returnAjaxStringValue}</strong>
							</div>
				    	</div>
				    	<div class="modal-footer">
					        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Close</button>
					    </div>
					</div>
				</div>
			</div>
		</display:column>
	</display:table>
	<div class="modal fade" id="showFileErrorModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				    <div class="modal-body" style="font-weight:600;">
					    <div class="alert alert-danger" id="getFileErrorMsg">
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-sm btn-danger" onclick="redirectModal();" data-dismiss="modal">Close</button>
				    </div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function openEmailTemplatePopUp(parentId,sid){
	  var $j = jQuery.noConflict();
	  	$j("#loading-indicator"+parentId).show();
		$j("#ajaxRunningDiv"+parentId).modal({show:true,backdrop: 'static',keyboard: false});
		  	$j.ajax({
	            url: 'editEmailSetupTemplatePopUpAjax.html?id='+parentId+'&sid='+sid+'&decorator=modal&popup=true',
	            success: function(data){
	            	$j("#loading-indicator"+parentId).hide();
				    $j("#ajaxRunningDiv"+parentId).modal("hide");
				    $j("#emailTemplateDetails"+parentId).modal({show:true,backdrop: 'static',keyboard: false});
					$j("#emailTemplateDetails"+parentId).html(data);
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#emailTemplateDetails"+parentId).modal("hide");
	                $j("#loading-indicator"+parentId).hide();
				    $j("#ajaxRunningDiv"+parentId).modal("hide");
	             }   
    	});  
}
function closeModal(parentId){
	tinymce.remove('#emailBody'+parentId);
}
function closeModalEdit(parentId){
	tinymce.remove('#emailBody'+parentId);
}
function closeErrorModal(parentId){
	var $j = jQuery.noConflict();
    $j("#showErrorModal"+parentId).modal("hide");
}
function sendMailFromTemplate(sid,emailId){
	var $j = jQuery.noConflict();
  	$j('#load'+emailId).button('loading');
	new Ajax.Request('/redsky/sendEmailFromEmailTemplateAjax.html?ajax=1&sid='+sid+'&id='+emailId+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			    	var response = transport.responseText || "no response text";
			    	$j("#sendMail"+emailId).modal('show');
			    	var check = response.trim().indexOf("succesfully");
					if(check >= 0){
						$j("#getSuccessMsg"+emailId).html(response.trim());
						$j("#getSuccessMsg"+emailId).show();
						$j("#getErrorMsg"+emailId).hide();
					}else{
						$j("#getErrorMsg"+emailId).html(response.trim());
						$j("#getErrorMsg"+emailId).show();
						$j("#getSuccessMsg"+emailId).hide();
					}
			    	$j('#load'+emailId).button('reset');
			    },
			    onFailure: function(){ 
				}
			  });
}
function editMailFromTemplate(sid,emailId,editModalId){
	var $j = jQuery.noConflict();
	$j("#loading-indicator"+emailId).show();
	$j("#ajaxRunningDiv"+emailId).modal({show:true,backdrop: 'static',keyboard: false});
	  	$j.ajax({
            url: 'editEmailPopupFromEmailTemplateAjax.html?decorator=modal&popup=true&sid='+sid+'&id='+emailId+'&editModalId='+editModalId,
            success: function(data){
            	$j("#loading-indicator"+emailId).hide();
			    $j("#ajaxRunningDiv"+emailId).modal("hide");
			    $j("#emailTemplateDetailsEdit"+emailId).modal({show:true,backdrop: 'static',keyboard: false});
				$j("#emailTemplateDetailsEdit"+emailId).html(data);
            },
            error: function () {
                alert('Some error occurs');
                $j("#emailTemplateDetailsEdit"+emailId).modal("hide");
                $j("#loading-indicator"+emailId).hide();
			    $j("#ajaxRunningDiv"+emailId).modal("hide");
             }   
	});
}
function redirectModal(){
	var sid = "<%=request.getAttribute("sid")%>";
	var replaceURL = "findEmailSetupTemplateByModuleNameSO.html?sid="+sid+"";
	window.location.replace(replaceURL);
}
try{
    if('${fileError}' == 'NoFile'){
     	var $j = jQuery.noConflict();
 		$j('#getFileErrorMsg').html("<p>This file is currently not available.</p>");
 		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
    }
}catch(e){}
</script>