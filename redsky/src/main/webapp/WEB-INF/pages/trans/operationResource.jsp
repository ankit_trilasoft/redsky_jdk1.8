<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>
table-fc tbody th, .table tbody td {white-space:nowrap;}
.table td, .table th, .tableHeaderTable td {padding: 0.3em;}
.pr-f11{font-family:arial,verdana;font-size:11px;}
.header-rt {border-right:medium solid #003366 !important;} 

#overlay11 {
    filter:alpha(opacity=70);
    -moz-opacity:0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
    position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
    background:url(images/over-load.png);
}

</style>

<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
 	<title><fmt:message key="operationResource.title" /></title>
  <meta name="heading" content="<fmt:message key='operationResource.heading'/>" />
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
<%-- <c:if test="${serviceOrder.job == 'OFF'}"> --%>
<c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
		animatedcollapse.addDiv('resources', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
	</configByCorp:fieldVisibility>
</c:if> 
animatedcollapse.init();
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="fileID" value="%{serviceOrder.id}"/>

<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:if test="${quoteFlag !='y'}">
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
</c:if>
<c:if test="${quoteFlag =='y'}">
<c:set var="noteFor" value="agentQuotesSO" scope="session"/>
</c:if>

<c:if test="${empty serviceOrder.shipNumber}">
    <c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.shipNumber}">
<c:if test="${userType!='AGENT'}">
    <c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:if test="${userType=='AGENT'}">
    <c:set var="isTrue" value="false" scope="request"/>
</c:if>
</c:if>



<c:if test="${quoteFlag !='y'}">
<s:hidden name="noteFor" value="ServiceOrder" />
</c:if>
<c:if test="${quoteFlag =='y'}">
<s:hidden name="noteFor" value="agentQuotesSO" />
<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
</c:if>

<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form name="operationResourceForm" id="operationResourceForm" action="saveOperationResource" method="post" validate="true">
<s:hidden name="formStatus" value="" />
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<s:hidden name="accUpdateAjax" />
<s:hidden name="oId"  id= "oId" value=""/>
<s:hidden name="oiActiveId"  id= "oiActiveId" value=""/>
<s:hidden name="oiInactiveId"  id= "oiInactiveId" value=""/>
<s:hidden name="workOrderVal"  id= "workOrderVal" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/> 
<s:hidden name="cfContract" value="${billing.contract}" />
<s:hidden name="inComptetive" value="${customerFile.comptetive}"/>
<s:hidden  name="billing.contract" />
	<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
	<s:hidden name="tempId1"></s:hidden>
	<s:hidden name="tempId2"></s:hidden>
	<s:hidden name="transferDateFlag" />
	<s:hidden name="firstDescription" />
		<s:hidden name="routingSO" value="${serviceOrder.routing}" />
		<s:hidden name="modeSO" value="${serviceOrder.mode}"/>
				<s:hidden name="serviceOrder.contract" />
				<s:hidden name="serviceOrder.originCountryCode" id="originCountryCode"/>
				<s:hidden name="serviceOrder.destinationCountryCode" id="destinationCountryCode"/>
				<s:hidden name="serviceOrder.originCity" id="originCity"/>
				<s:hidden name="serviceOrder.destinationCity" id="destinationCity"/>
				<s:hidden name="serviceOrder.projectedActualExpense" />
				<s:hidden name="serviceOrder.packingMode" />
				<s:hidden name="serviceOrder.serviceType" />
				<s:hidden name="miscellaneous.netEstimateCubicMtr" />
				<s:hidden name="miscellaneous.estimatedNetWeight" />
				<s:hidden name="serviceOrder.orderBy" value="%{customerFile.orderBy}" /> 
			   <s:hidden name="serviceOrder.orderPhone" value="%{customerFile.orderPhone}" />
			    <s:hidden name="serviceOrder.payType" value="%{customerFile.billPayMethod}" /> 
				<s:hidden name="serviceOrder.actualNetWeight" value="%{miscellaneous.actualNetWeight}" /> 
				<s:hidden name="serviceOrder.actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" /> 
				<s:hidden name="serviceOrder.actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" /> 
				<s:hidden name="serviceOrder.estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" /> 
				<s:hidden name="serviceOrder.estimatedNetWeight" value="%{miscellaneous.estimatedNetWeight}" /> 
				<s:hidden name="serviceOrder.estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" />
				<s:hidden name="miscellaneous.netEstimateCubicFeet" /> 
				<s:hidden name="miscellaneous.netActualCubicMtr" />
				<s:hidden name="miscellaneous.netActualCubicFeet" />
				<s:hidden name="miscellaneous.estimatedNetWeightKilo" />
				<s:hidden name="miscellaneous.actualNetWeight"></s:hidden>
				<s:hidden name="accountIdCheck"/>
				<s:hidden name="serviceOrder.revisedTotalExpense" /> 
				<s:hidden name="serviceOrder.revisedTotalRevenue" /> 
				<s:hidden name="serviceOrder.revisedGrossMargin" />
				<s:hidden name="serviceOrder.revisedGrossMarginPercentage" /> 
				<s:hidden name="serviceOrder.entitledTotalAmount" /> 
				<s:hidden name="serviceOrder.estimatedTotalExpense" /> 
				<s:hidden name="serviceOrder.estimatedTotalRevenue" /> 
				<s:hidden name="serviceOrder.estimatedGrossMargin" /> 
				<s:hidden name="serviceOrder.distributedTotalAmount" />  
				<s:hidden name="serviceOrder.estimatedGrossMarginPercentage"/> 
				<s:hidden name="serviceOrder.actualExpense"/>
			    <s:hidden name="serviceOrder.actualRevenue"/> 
				<s:hidden name="serviceOrder.actualGrossMargin"/> 
				<s:hidden name="serviceOrder.actualGrossMarginPercentage"/>
				<s:hidden name="serviceOrder.projectedGrossMarginPercentage" />
				<s:hidden name="defaultTemplate" id="defaultTemplate"/>
				 <c:if test="${serviceOrder.job =='RLO'}">
				 <s:hidden  name="reloServiceType" value="${serviceOrder.serviceType}" />
				 </c:if>
				 <c:if test="${serviceOrder.job !='RLO'}">
				 <s:hidden  name="reloServiceType" value="" />
				 </c:if>
				<s:hidden name="serviceOrder.routing" />
				<s:hidden name="serviceOrder.projectedActualRevenue" />
				<s:hidden name="serviceOrder.projectedGrossMargin" />
				<s:hidden name="serviceOrder.actualAuto"/>
				<s:hidden name="serviceOrder.actualBoat"/>				
				<s:hidden name="serviceOrder.controlFlag" />
				<s:hidden name="serviceOrder.isSOExtract"/>
				<s:hidden name="isSOExtract"/>
				<s:hidden name="serviceOrder.mode"/>
		<s:hidden name="inEstimatedTotalExpense" value="${serviceOrder.estimatedTotalExpense}"/>
				<s:hidden name="secondDescription" />
			    <s:hidden name="thirdDescription" /> 
			    <s:hidden name="fourthDescription" /> 
				<s:hidden name="fifthDescription" />
				 <s:hidden name="sixthDescription" />
				 <s:hidden name="seventhDescription" /> 
				 <s:hidden name="eigthDescription" />
	             <s:hidden name="ninthDescription" />
	             <s:hidden name="tenthDescription" />
	             <s:hidden name="oldStatus" value="%{serviceOrder.status}" /> 
	             	<s:hidden name="oldStatusNumber" value="%{serviceOrder.statusNumber}" /> 
	             	<c:if test="${not empty serviceOrder.id}">
	<c:if test="${not empty billing.billComplete}">
	 <s:text id="billingBillingComplete" name="${FormDateValue}"> <s:param name="value" value="billing.billComplete" /></s:text>
	 <s:hidden  name="billing.billComplete" value="%{billingBillingComplete}" />  
	</c:if>
	<c:if test="${empty billing.billComplete}">
	<s:hidden  name="billing.billComplete" />
	</c:if>
	<s:hidden name="billing.billCompleteA" value="%{billing.billCompleteA}" />
	<c:if test="${not empty trackingStatus.deliveryA}">
	<s:text id="trackingStatusDeliveryA" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.deliveryA" /></s:text>
	<s:hidden  name="trackingStatus.deliveryA" value="%{trackingStatusDeliveryA}" />  
	</c:if>
	<c:if test="${empty trackingStatus.deliveryA}">
	<s:hidden  name="trackingStatus.deliveryA" />  
	</c:if>
	<s:hidden  name="claimCount" value="${claimCount}"/> 
	<s:hidden name="invoiceCount" value="${invoiceCount}"/> 
	</c:if>
	
	<div id="layer4" style="width:100%;">
	<div id="newmnav" style="float:none;">
		  <ul>
		         <ul>
            <c:if test="${quoteFlag !='y'}">
	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	                 <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
            <li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
                   </c:if>
            </sec-auth:authComponent>
            </configByCorp:fieldVisibility>
     
            <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
             <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
             <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
             <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
             </sec-auth:authComponent>
             </c:if>
              <c:if test="${quoteFlag =='y'}">
		       <li ><a href="QuotationFileForm.html?from=list&id=${serviceOrder.customerFileId}"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a href="editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>	
		    </c:if>    
		      <li id="newmnav1" style="background:#FFF "><a class="current" ><span>O&I<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		        <c:if test="${quoteFlag !='y'}">
		       <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  					<c:if test="${serviceOrder.job !='RLO'}"> 
  				<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
 				 </c:if>
  				</c:if> 
  				<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
  				<c:if test="${serviceOrder.job =='RLO'}"> 
  				<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  				</c:if>
 				 <c:if test="${serviceOrder.job !='RLO'}"> 
  				<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
 				 </c:if>
		      <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
		      <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		      </c:if>
		        <c:if test="${quoteFlag !='y'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			  <c:if test="${quoteFlag =='y'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}&Quote=y"><span>Survey Details</span></a></li>
			</c:if>
		        <c:if test="${quoteFlag !='y'}">
		        <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
		        </c:if>
		  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=OI&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
		   <li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>    
           <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
		  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
		  	</configByCorp:fieldVisibility>   
         </ul>
        </ul>
        </div>
        <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left" valign="top">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>         
        
        <div class="spn">&nbsp;</div>
      <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content"> 			
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%" >
		<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
				<tbody>
				<tr><td height="5px"></td></tr>
					<tr>
					&nbsp;
						<td align="right" class="listwhitetext" style="width:85px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.prefix' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom">Title / Rank</td>
						<td align="left" class="listwhitetext" width="5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.firstName' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="center" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.mi' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.lastName' /><font color="red" size="2">*</font></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber"><fmt:message key='serviceOrder.socialSecurityNumber'/></configByCorp:fieldVisibility></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
						<%-- Added by kunal for ticket number: 6092 and 6218(later) --%>
						<c:set var="isVipFlag" value="false" />
						<c:if test="${serviceOrder.vip}">
							<c:set var="isVipFlag" value="true" />
						</c:if>
						</td>
						</tr>
						<tr>
						<td align="left" style="width:85px"></td>
						<td align="left" style="width:5px"><s:textfield cssClass="input-textUpper" id="tabindexFlag" name="serviceOrder.prefix" cssStyle="width:25px" maxlength="15" onfocus="myDate();" readonly="true" tabindex=""/></td>
						<td align="left" style="width:3px">
						<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="serviceOrder.rank" cssStyle="width:55px" maxlength="7" readonly="true" tabindex=""/> </td>
						<td align="left" style="width:3px">
						<td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.firstName" cssStyle="width:155px" maxlength="80" readonly="true" tabindex=""  /></td>
						<td align="left" style="width:3px"></td>
						<td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.mi" cssStyle="width:13px" maxlength="1" readonly="true" tabindex="" /></td>
						<td align="left" style="width:17px"></td>
						<td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.lastName" cssStyle="width:165px" maxlength="80" required="true" readonly="true" tabindex="" /></td>
						<td align="left" style="width:3px"></td>
						<td align="left" class="listwhitetext" ><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber">
					    <s:textfield cssClass="input-textUpper" key="serviceOrder.socialSecurityNumber" size="7" maxlength="9" readonly="true" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="" cssStyle="width:88px"/>
					    </configByCorp:fieldVisibility></td>
						<td align="right"><s:checkbox id="serviceOrder.vip" name="serviceOrder.vip" value="${isVipFlag}" fieldValue="true" disabled="true" onchange="changeStatus();" onclick="setVipImage();" tabindex="" /></td>
						<td align="left" class="listwhitebox" valign="middle"><fmt:message key='serviceOrder.vip' /></td>
						<td width="85px"></td>						
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					<tr>
					 <td>
					   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="5px"></td>
						</tr>
							<tr>
								<td style="width:85px"></td>
								<td align="left" class="listwhitetext">S/O#</td>
								<td align="right" class="listwhitetext" style="width:7px"></td>								
								<c:choose>
								<c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
								<td></td>
								</c:when>
								<c:otherwise>
								<td align="left" class="listwhitetext">Status</td>
								</c:otherwise>
								</c:choose>
								<c:if test="${checkAccessQuotation}">
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td align="left" class="listwhitetext" valign="bottom"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.quoteStatus',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.quoteStatus'/></a></td>								
								</c:if>
								</c:if>
								<td align="right" class="listwhitetext" style="width:7px"></td>
								<td align="left" class="listwhitetext">Status Date</td>
								<td align="left" class="listwhitetext" style="width:7px"></td>								
							</tr>
							</tr>
							<tr>
								<td align="left" style="width:85px"></td>
								<td align="left" style="width:27px">
								<s:textfield cssClass="input-textUpper" name="serviceOrder.shipNumber" cssStyle="width:276px" size="22" maxlength="15" readonly="true" tabindex="" /></td>
								<td align="left" style="width:17px"></td>
								<c:choose>
								<c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
								<td><s:hidden name="serviceOrder.status" /></td>
								</c:when>
								<c:otherwise>
								<td><s:select cssClass="list-menu" name="serviceOrder.status" list="%{JOB_STATUS}" required="false" headerKey=""  headerValue=""  onchange="checkStatus();updateOperationIntilligence('${serviceOrder.id}','status',this,'ServiceOrder','${serviceOrder.shipNumber}');" cssStyle="width:168px" tabindex="" /></td>
								</c:otherwise>
								</c:choose>
								<c:if test="${checkAccessQuotation}">							
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td align="left" style=""><s:select cssClass="list-menu" key="serviceOrder.quoteStatus"  list="%{QUOTESTATUS}" cssStyle="width:168px" onchange="checkQuotation();changeStatus();" tabindex="" /></td>
								</c:if>
								
								</c:if>
								<c:if test="${not empty serviceOrder.statusDate}">
									<s:text id="statusDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.statusDate"  />
									</s:text>
									<td align="left" style="width:7px"></td>
									<td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:87px" maxlength="11" readonly="true" tabindex="" /></td>
								</c:if>
								<c:if test="${empty serviceOrder.statusDate}">
									<td align="left" style="width:7px"></td>
									<td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" cssStyle="width:87px" maxlength="11" readonly="true" tabindex="" /></td>
								</c:if>
								<td align="left" class="listwhitetext"></td>
								<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
									<c:if test="${empty serviceOrder.id}">
									<td style="position:absolute;right:45px;margin-top:0px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
									</c:if>
									<c:if test="${not empty serviceOrder.id}">
									<c:choose>
									<c:when test="${countForOINotes == '0' || countForOINotes == '' || countForOINotes == null}">
									<td style="position:absolute;right:45px;margin-top:0px;" align="right"><img id="countForOINotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OfficeMove&imageId=countForOINotesImage&fieldId=countForOINotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OfficeMove&decorator=popup&popup=true',800,600);" ></a></td>
									</c:when>
									<c:otherwise>
									<td style="position:absolute;right:45px;margin-top:0px;" align="right"> <img id="countForOINotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OfficeMove&imageId=countForOINotesImage&fieldId=countForOINotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OfficeMove&decorator=popup&popup=true',800,600);" ></a></td>
									</c:otherwise>
									</c:choose> 
									</c:if>
								</sec-auth:authComponent>
								<script type="text/javascript">
                                 /* cheackStatusReasonLoad() */;
                             	</script>                              
							</tr>
						</tbody>
					</table>
					</td>
					</tr>
					<tr>
						<td height="5px"></td>
						</tr>
						<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" style="width:85px"></td>
									<td align="left" class="listwhitetext">Origin&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
									<td align="left" class="listwhitetext" style="width:7px"></td>
									<td align="left" class="listwhitetext">Destination&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:85px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" cssStyle="width:220px" name="serviceOrder.originCityCode" size="22" maxlength="30" readonly="true" tabindex="" /></td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" name="serviceOrder.originCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex="" /></td>
									<td align="right" class="listwhitetext" style="width:17px"></td>
									<td align="left" class="listwhitetext" >
									<s:textfield cssClass="input-textUpper" key="serviceOrder.destinationCityCode" cssStyle="width:220px" size="22" maxlength="30" readonly="true" tabindex="" /></td>
									<td width="9px"></td>
									<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" name="serviceOrder.destinationCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex="" /></td>
									<td width="55px"></td>						     		
								</tr>
							</tbody>
						</table>
						</td>
					</tr>					
						<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
							<tbody>
							<tr>
							<td style="width:85px; !width: 85px"></td>
							<td align="left" class="listwhitetext">Code</td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:10px">Name</td>
							<td></td>
							
							<td align="left" class="listwhitetext" style="width:10px"><c:if  test="${companies == 'Yes'}">Company&nbsp;Division<font color="red" size="2">*</font></c:if></td>
							</tr>
								<tr>
									<td align="right" class="listwhitetext" style="">Booking&nbsp;Agent:&nbsp;</td>
									<td><s:textfield cssClass="input-text" id="soBookingAgentCodeId" name="serviceOrder.bookingAgentCode" cssStyle="width:135px;" maxlength="8" onchange="valid(this,'special');findBookingAgentName()"  tabindex=""/></td>
									<td style="width:20px"><img class="openpopup" width="" height="" id="serviceOrder.bookingAgentCode.img" onclick="openBookingAgentPopWindow(); document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].select();" src="<c:url value='/images/open-popup.gif'/>" /></td>
									<td align="left"><s:textfield cssClass="input-text" id="soBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="findPartnerDetails('soBookingAgentNameId','soBookingAgentCodeId','soBookingAgentNameDivId',' and (isAccount=true or isAgent=true or isVendor=true )','',event);" onchange="findPartnerDetailsByName('soBookingAgentCodeId','soBookingAgentNameId');"  size="44" maxlength="250" cssStyle="width:247px" tabindex="" />
									<div id="soBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
									</td>
									
									<td width="24px"></td>
									<c:if  test="${companies == 'Yes'}">
										<td align="left">
										<c:if test="${not empty serviceOrder.id}">
										<s:select  cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:145px" onchange="updateOperationIntilligence('${serviceOrder.id}','companyDivision',this,'ServiceOrder','${serviceOrder.shipNumber}');" headerKey="" headerValue="" tabindex="" />
										</c:if>
										<c:if test="${empty serviceOrder.id}">
										<s:select  cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:145px" onchange="populateInclusionExclusionData('default','1','F');getJobList('unload');changeStatus();checkTransferDate(); changebAgentCheckedBy();" headerKey="" headerValue="" tabindex="" />
										</c:if>
										</td>
									</c:if>
									<c:if  test="${companies != 'Yes'}">
										<s:hidden name="serviceOrder.companyDivision"/>
									</c:if>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					
					<tr>
						<td>
						<table class="detailTabLabel" border="0	" cellpadding="0" cellspacing="0" width="">
							<tbody>
							<tr><td height="5px"></td></tr>
									<tr>									 
									<td align="right" class="listwhitetext" style="width:85px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td align="left" class="listwhitetext" valign="bottom" style=""><div id="registrationHideLabel">
									<fmt:message key='serviceOrder.registrationNumber' /></div></td>
									<td align="right" class="listwhitetext" style="width:15px"></td>
									<td align="left" class="listwhitetext" valign="bottom" style="">
									<fmt:message key='serviceOrder.job' /><font color="red" size="2">*</font></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="right" class="listwhitetext" style="width:8px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="labelEstimator"><fmt:message key='serviceOrder.estimator' />
									</div></td>
								</tr>
								<tr>
									<td align="right">
									<div id="hidRegNum">
									<c:if test="${empty sysDefSequenceNum}">
									<img id="rateImage"  class="openpopup" width="16" height="16" align="top" src="${pageContext.request.contextPath}/images/arrow-loadern.gif" onclick="findJobSeq()"/>
									</c:if> 
									</div>
									</td> 
									<td align="left" style=""><div id="registrationHide"><s:textfield cssClass="input-text" name="serviceOrder.registrationNumber" id="regist" onchange="updateOperationIntilligence('${serviceOrder.id}','registrationNumber',this,'ServiceOrder','${serviceOrder.shipNumber}');" onkeyup="valid(this,'special')"
										cssStyle="width:135px;" maxlength="20" required="true" tabindex="" /></div></td>
									<td align="right" class="listwhitetext" style="width:20px"></td>									
									<td align="left" style="width:200px">
									<c:if test="${not empty serviceOrder.id}">
									<s:select id="jobService" cssClass="list-menu" key="serviceOrder.job" list="%{job}"
																			cssStyle="width:251px" headerKey="" headerValue=""
																			onchange="updateOperationIntilligence('${serviceOrder.id}','job',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" disabled="true"/>
									</c:if>		
									<c:if test="${empty serviceOrder.id}">
									<s:select id="jobService" cssClass="list-menu" key="serviceOrder.job" list="%{job}"
																			cssStyle="width:251px" headerKey="" headerValue=""
																			onchange="updateOperationIntilligence('${serviceOrder.id}','job',this,'ServiceOrder','${serviceOrder.shipNumber}');"  tabindex="" disabled="true"/>
									</c:if>									
										</td>
									<td align="right" class="listwhitetext" style="width:10px"></td>
									<script type="text/javascript">
									try{
                                     checkJobTypeOnLoad();
									}catch(e){}
                                    </script>
                                    <td align="left" style="width:5px"></td>
									<td align="left" style="width:10px">
									<div id="EstimatorService1">
									<s:select cssClass="list-menu" name="serviceOrder.estimator"
										list="%{sale}" id="estimator" cssStyle="width:145px" headerKey=""
										headerValue="" onchange="updateOperationIntilligence('${serviceOrder.id}','estimator',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
									</div>
										</td>
								<td width="5px"></td>
								</tr>
								<tr><td height="5px"></td></tr>
								<tr>
									
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:5px"></td>
								</tr>
                                <tr><td height="2px"></td></tr>
							
								<tr><td height="4px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom" ><div id="hidStatusSalesLabel"><fmt:message
										key='serviceOrder.salesMan' /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="hidStatusCommoditLabel"><fmt:message key='serviceOrder.commodity' /><font color="red" size="2">*</font></div>
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									
									<td align="right" class="listwhitetext" style="width:13px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><fmt:message
										key='serviceOrder.coordinator' /><font color="red" size="2">*</font></td>									
								</tr>
								<tr>
									<td align="left" style="width:25px"></td>
									<td ><div id="hidStatusSalesText"><s:select cssClass="list-menu"
										name="serviceOrder.salesMan" id="salesMan" list="%{estimatorList}"
										cssStyle="width:140px" headerKey="" headerValue=""
										onchange="updateOperationIntilligence('${serviceOrder.id}','salesMan',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<c:choose>
										<c:when test="${serviceOrder.job =='STO' || serviceOrder.job =='STL' ||serviceOrder.job =='STF' ||serviceOrder.job =='TPS'}">
											<td align="left"><div id="hidStatusCommoditText">
											<c:if test="${not empty serviceOrder.id}">
											<s:select
												cssClass="list-menu" name="serviceOrder.commodity"
												list="%{commodits}" cssStyle="width:251px"
												onchange="updateOperationIntilligence('${serviceOrder.id}','commodity',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
												</c:if>
												<c:if test="${empty serviceOrder.id}">
											<s:select
												cssClass="list-menu" name="serviceOrder.commodity"
												list="%{commodits}" cssStyle="width:251px"
												onchange="updateOperationIntilligence('${serviceOrder.id}','commodity',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
												</c:if>
												</div></td>
										</c:when>
										<c:otherwise>
											<td align="left"><div id="hidStatusCommoditText">
											<c:if test="${not empty serviceOrder.id}">
											<s:select
												cssClass="list-menu" name="serviceOrder.commodity"
												list="%{commodit}" cssStyle="width:251px"
												onchange="updateOperationIntilligence('${serviceOrder.id}','commodity',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
												</c:if>
										<c:if test="${empty serviceOrder.id}">
											<s:select
												cssClass="list-menu" name="serviceOrder.commodity"
												list="%{commodit}" cssStyle="width:251px"
												onchange="validAutoBoat();populateInclusionExclusionData('default','1','F');changeStatus();" tabindex="" />
												</c:if>		
												
												</div></td>
										</c:otherwise>
									</c:choose>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="right" style="width:5px"></td>
									<td><s:select cssClass="list-menu"
										name="serviceOrder.coordinator" id="coordinator" list="%{coordinatorList}"
										cssStyle="width:145px" headerKey="" headerValue=""
										onchange="updateOperationIntilligence('${serviceOrder.id}','coordinator',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" /></td>
										
								</tr>								
								   <tr><td height="5px"></td></tr>
								<tr>
									
									<td align="right" class="listwhitetext" style="width:25px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><fmt:message
										key='serviceOrder.projectManager' /></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>										
								</tr>
								<tr>
								<td align="right" style="width:25px"></td>
									<td><s:select cssClass="list-menu"
										name="serviceOrder.projectManager" id="projectManager" list="%{projectManagerList}"
										cssStyle="width:140px" headerKey="" headerValue=""
										onchange="updateOperationIntilligence('${serviceOrder.id}','projectManager',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" /></td>
								</tr>
									<tr>
									
									<td align="right" class="listwhitetext" style="width:25px; "></td>
									<td align="left" class="listwhitetext" style="padding-top:7px;" valign="bottom">
									O&I Overall Project View
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>										
								</tr>	
									<tr>
							    <td align="right" style="width:25px"></td>
								<td colspan="9"><textarea class="list-menu"
										name="serviceOrder.oIOverallProjectview" id="oIOverallProjectview" 
										style="width:574px; height:50px;" 
										onchange="updateOperationIntilligence('${serviceOrder.id}','oIOverallProjectview',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" maxlength="496" onkeypress="return checkLength();"><c:out value='${serviceOrder.oIOverallProjectview}'/></textarea></td>
								</tr>							
							</tbody>
						</table>
					</tr>
					</tbody>
					</table>
					
					<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;" class="detailTabLabel">
				<%-- 	<c:if test="${serviceOrder.job == 'OFF'}"> --%>
					    <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
  <tr>
	<td height="10" width="100%" align="left" style="margin:0px">
		<div id="resourcefar" onClick="javascript:animatedcollapse.toggle('resources'); findAllResource('');ajax_hideTooltip();"  style="margin:0px">
		     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Resources</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
	     </div>
	     <div id="resources" class="switchgroup1">
		 	<div id="resourcMapAjax">
		 		<jsp:include flush="true" page="resourceQuotationServiceOrderForm.jsp"></jsp:include>
		 	</div>
		</div>
</td>
</tr>
</configByCorp:fieldVisibility>
</c:if>
	</table>
	<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;width:100%;">				
<tr>
				<td width="100%" align="left" style="margin:0px">
					<div id="pricingShow" tabindex="136">
					<div onClick="javascript:animatedcollapse.toggle('pricing');loadSoPricing();ajax_hideTooltip();" style="margin:0px">
						<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
							<tr>
								<td class="headtab_left"></td>
								<td NOWRAP class="headtab_center">&nbsp;Pricing </td>
								<td width="28" valign="top" class="headtab_bg"></td>
								<td class="headtab_bg_center">&nbsp;</td>
								<td class="headtab_right"></td>
							</tr>
						</table>
					</div>  				
			<div id="pricing" class="switchgroup1" style="overflow-x:auto; width:1800px;">
			   <div id="pricingSoAjax">	
			   			 
			  </div>	
		  </div>
	</div>
				</td>
			</tr>
									
</table>
	</div>
<div class="bottom-header" style="margin-top:50px;z-index:999;"><span></span></div>
</div>
</div>		
</div>	

<div id="overlay11">
            <table cellspacing="0" cellpadding="0" border="0" width="100%" >
            <tr>
            <td align="center">
            <table cellspacing="0" cellpadding="3" align="center">
            <tr>
            <td height="200px"></td>
            </tr>
            <tr>
           <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
               <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
           </td>
           </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>



</s:form>
<%@ include file="/common/pricing.js"%>
<script type="text/javascript">
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay11"].visibility='show';
       	else
          document.getElementById("overlay11").style.visibility='visible';
   	}
}

var http2 = getHTTPObject(); 
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
</script>
<script type="text/javascript">
function transferResourceToWorkTicket(){
	window.open('transferResourceToWorkTicketForOI.html?decorator=popup&popup=true&id=${serviceOrder.id}&shipNumber=${serviceOrder.shipNumber}','accountProfileForm','height=500,width=1080,top=0, scrollbars=yes,resizable=yes').focus();
}
</script>

<script type="text/javascript">
function auditSetUp(id){

	
	window.open('auditList.html?decorator=popup&popup=true&id='+id+'&tableName=operationsintelligence&decorator=popup&popup=true','audit','height=400,width=790,top=100,left=150, scrollbars=yes,resizable=yes').focus();
}
</script>

<script type="text/javascript">
function createPrice(){
	var cfContract = document.forms['operationResourceForm'].elements['cfContract'].value;	
	var id='${serviceOrder.id}';	
	var SOcontract = cfContract;
	var contractType='';
	var contractName='';
	if(cfContract.length > 0){
		contractType = cfContract;
		contractName= 'Billing Contract';
	}else{
		alert('Enter Billing Contract');
	}
	
	if(contractType.length > 0){
		/* var conf = 'Continue with '+contractName+': '+contractType;
		if(confirm(conf)){ */
			var url="createPriceSO.html?tempId="+encodeURI(id)+"&serviceOrderId=${serviceOrder.id}&contractName=" + contractType+"&shipNumber=${serviceOrder.shipNumber}";
			document.forms['operationResourceForm'].action =url;
			document.forms['operationResourceForm'].submit();
		/* } */
	}
}

function operationsDetailsList(){
	window.open('findAllDistinctResources.html?decorator=popup&popup=true&id=${serviceOrder.id}&shipNumber=${serviceOrder.shipNumber}&flag=true','accountPortalForOIForm','height=500,width=1200,top=0, scrollbars=yes,resizable=yes').focus();
}

function createPriceForQuote(){
	var cfContract = document.forms['operationResourceForm'].elements['cfContract'].value;	
	var id='${serviceOrder.id}';	
	var SOcontract = cfContract;
	var contractType='';
	var contractName='';
	if(cfContract.length > 0){
		contractType = cfContract;
		contractName= 'Billing Contract';
	}else{
		alert('Enter Billing Contract');
	}
	
	if(contractType.length > 0){
		/* var conf = 'Continue with '+contractName+': '+contractType;
		if(confirm(conf)){ */
			var url="createPrice.html?tempId="+encodeURI(id)+"&serviceOrderId=${serviceOrder.id}&contractName=" + contractType+"&shipNumber=${serviceOrder.shipNumber}";
			document.forms['operationResourceForm'].action =url;
			document.forms['operationResourceForm'].submit();
		/* } */
	}
}

function copyEstimateOandI(){
	var url = "copyEstimateOandIPopUp.html?decorator=popup&popup=true&shipNumber=${serviceOrder.shipNumber}";
	window.open(url,"", "top=100,left=500,width=550,height=300");
}
</script>
<script type="text/JavaScript">
var httpResourceMap = getHTTPObject();
function getAllResourcMap(){
	var url="findAllResourceDateAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}";
	httpResourceMap.open("POST", url, true);
	httpResourceMap.onreadystatechange = handleHttpResponseForResourcesMap;
	httpResourceMap.send(null);
}
	
	function handleHttpResponseForResourcesMap(){
		if (httpResourceMap.readyState == 4){
            var results= httpResourceMap.responseText;
            dateStr=results.trim();
            setTimeout("setResourceDate()", 1000);
	 	}
	}
	function date_convert1(date){
		// date format should be 2012-09-03 00:00:00
		var intVal = date.split(' ')[0];
	   	var intval1 = intVal.split('-');
	   	
		var month=intval1[1]
		if(month == 01)month="Jan";
		if(month == 02)month="Feb";
		if(month == 03)month="Mar";
		if(month == 04)month="Apr";
		if(month == 05)month="May";
		if(month == 06)month="Jun";
		if(month == 07)month="Jul";
		if(month == 08)month="Aug";
		if(month == 09)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
	   	
		var datam = intval1[2]+"-"+month+"-"+intval1[0].substring(2,4);
		return datam;
	}
	var dateStr='';
	function setResourceDate(){
		if(dateStr.length > 0){
		   var mySplitResult = dateStr.split("_");
           for(var i = 0; i < mySplitResult.length; i++){
	           	var split1 = mySplitResult[i];
	           	var dayNdate = split1.split("*@");
	           	var bDate=dayNdate[1].trim();
	           	var eDate=dayNdate[2].trim();
	           	var bDateId='beginDt'+dayNdate[0].trim();
	           	var eDateId='endDt'+dayNdate[0].trim();
	           	
	           	document.getElementById(bDateId).value=date_convert1(bDate);
	           	document.getElementById(eDateId).value=date_convert1(eDate);
           }
		}
	}
	function volumeDisplay(){
		var weightVolSectionId = document.getElementById('weightVolMain');
		var fieldVal="0";
		<configByCorp:fieldVisibility componentId="component.field.weightVolume.Visibility">
		fieldVal="1";
		</configByCorp:fieldVisibility>
		if(weightVolSectionId != null && weightVolSectionId != undefined){
			if(fieldVal=="0"){
				document.getElementById('weightVolMain').style.display = 'none';
			}
		}
	}
</script>
<script type="text/javascript">
var catagoryName='';
var dayId='';
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e != null && e != undefined){
    	if(e.style.display == 'block')
    	   e.style.display = 'none';
    	else
    	   e.style.display = 'block';
    }
}
function setAnimatedColFocus(id){
		toggle_visibility("DayTable"+id);
		<c:if test="${serviceOrder.controlFlag != 'Q'}">
			//document.getElementById('date'+id).style.display='block';
		</c:if>
}
function setAnimatedColCatFocus(id,day){
		toggle_visibility(id);
}
</script>

<script type="text/javascript">
</script>
<script type="text/javascript">
var httpTemplate = getHTTPObject();
var httpResource = getHTTPObject();
function getDayTemplate(){
	var id = '${serviceOrder.id}';
	var contract = '${customerFile.contract}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource Template.');
	}else {
		showOrHide(1);
		var url ="resouceTemplateQuotation.html?ajax=1&shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}";
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	}
}
var tempCWMS;
function getDayCWMSTemplate(tempCWMS,act){
	var id = '${serviceOrder.id}';
	var contract = '${customerFile.contract}';
	
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource Template.');
	}else {
		showOrHide(1);
		var url ="resouceTemplateCWMSQuotation.html?ajax=1&shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}&distinctWorkOrder="+tempCWMS+"&action="+act+"" ;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	}
}
function handleHttpResponseForTemplate(){
	if (httpTemplate.readyState == 4){
        var results= httpTemplate.responseText;
        findAllResource('A');
	}
}
function findAllResource(target){
	if(target=='flag')
		{
		var woFlag="";
		 var workorder="";
		 <c:forEach var="entry" items="${findDistinctWorkOrder}">
		 var id="${entry.id}";
		 var workNo=document.getElementById('checkboxId'+id).checked;
		 var wo = document.getElementById ('workOrderOI'+id).value;
		if(workNo==true)
			{
		 if(workorder == "")
			 {
			 workorder = wo;
			 }else{
		 workorder = workorder+","+wo;
			 }
			}
		 </c:forEach>
		
		 if(workorder != "")
			 {
			 ajax_hideTooltip();
			 var resourceDiv = document.getElementById("resourcMapAjax");
			 
			    showOrHide(1);
				var shipNumber='${serviceOrder.shipNumber}';
			  $.get("findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true", 
						{ resouceTemplate: target,shipNumber:shipNumber,allWorkOrder:workorder},
						function(data){
							showOrHide(0);
							$("#resourcMapAjax").html(data);
							//applyImage();
							calculateRevenueForOI();
					});
	}else{
		alert("Please Select any Work Order");
		return false;
	}
		}else{
			var resourceDiv = document.getElementById("resourcMapAjax");
		    showOrHide(1);
			var shipNumber='${serviceOrder.shipNumber}';
		    $.get("findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true", 
					{ resouceTemplate: target,shipNumber:shipNumber},
					function(data){
						showOrHide(0);
						$("#resourcMapAjax").html(data);
						//applyImage();
				});
}
}
function findAllResourceActAll(target,target1){ 
	if(target=='flag')
		{
		var woFlag="";
		 var workorder="";
		 <c:forEach var="entry" items="${findDistinctWorkOrder}">
		 var id="${entry.id}";
		 var workNo=document.getElementById('checkboxId'+id).checked;
		 var wo = document.getElementById ('workOrderOI'+id).value;
		if(workNo==true)
			{
		 if(workorder == "")
			 {
			 workorder = wo;
			 }else{
		 workorder = workorder+","+wo;
			 }
			}
		 </c:forEach>
		
		 if(workorder != "")
			 {
			 ajax_hideTooltip();
			 var resourceDiv = document.getElementById("resourcMapAjax");
			 
			    showOrHide(1);
				var shipNumber='${serviceOrder.shipNumber}';
			  $.get("findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true", 
						{ resouceTemplate: target,shipNumber:shipNumber,allWorkOrder:workorder},
						function(data){
							showOrHide(0);
							$("#resourcMapAjax").html(data);
							//applyImage();
					});
	}else{
		alert("Please Select any Work Order");
		return false;
	}
		}else{
			var resourceDiv = document.getElementById("resourcMapAjax");
		    showOrHide(1);
			var shipNumber='${serviceOrder.shipNumber}';
			var resourceListForOIStatus='true'
			if(target1=='ALL'){
				resourceListForOIStatus='ALL'
			}
		    $.get("findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true", 
					{ resouceTemplate: target,shipNumber:shipNumber,resourceListForOIStatus:resourceListForOIStatus},
					
					function(data){ 
						showOrHide(0);
						$("#resourcMapAjax").html(data);
						//applyImage();
				});
}
}
function addResources(){

	var ship='${serviceOrder.shipNumber}';
	$.get("addResourcesAjax.html?ajax=1&decorator=simple&popup=true", 
			{ shipNumber:ship},
			function(data){				
			$("#resourcMapAjax").html(data);
			//applyImage();
		});
}
function copyWorkOrder(){
	 var workorder=document.getElementById("workOrderVal").value;
	 if(workorder != ""){
	 ajax_hideTooltip();
	    showOrHide(1);
		var ship='${serviceOrder.shipNumber}';
		$.get("copyWorkOrderToResources.html?ajax=1&decorator=simple&popup=true", 
				{ shipNumber:ship,distinctWorkOrder:workorder},
				function(data){				
				//$("#resourcMapAjax").html(data);
				 location.reload(true);
				 document.getElementById("workOrderVal").value='';
			});
	}
}

function setValues(rowId,targetElement){
	   var userCheckStatus = document.getElementById("workOrderVal").value;
	  if(targetElement.checked){
		  if(userCheckStatus == '' ){
			  document.getElementById("workOrderVal").value = rowId;
			}else{
				if(userCheckStatus.indexOf(rowId)>=0){
				}else{
					userCheckStatus = userCheckStatus + "," +rowId;
				}
				document.getElementById("workOrderVal").value = userCheckStatus.replace(', ,',",");
			}
	   }else{
			if(userCheckStatus == ''){
				document.getElementById("workOrderVal").value = rowId;
		     }else{
		    	 var check = userCheckStatus.indexOf(rowId);
		 		if(check > -1){
		 			var values = userCheckStatus.split(",");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== rowId) {
		 		    	 values.splice(i, 1);
		 		    	userCheckStatus = values.join(",");
		 		    	document.getElementById("workOrderVal").value = userCheckStatus;
		 		      }
		 		   }
		 		}else{
		 			userCheckStatus = userCheckStatus + "," +rowId;
		 			document.getElementById("workOrderVal").value = userCheckStatus.replace(', ,',",");
		 		}
		     }
	   }
	}

function insertWorkOrder()
{
   var distinctWorkOrder = document.getElementById("wo").value; 
   var template = document.getElementById("template").value; 
   var action = document.getElementById("action").value; 
	 var workorder="";
	 ajax_hideTooltip();
	 if(template=='Template')
		 {
		
		 contractForOICWMS('resourceTemplate',distinctWorkOrder,action);
		 //getDayCWMSTemplate(distinctWorkOrder,action);
		}
	 else
		 {
    var resourceDiv = document.getElementById("resourcMapAjax");
        showOrHide(1);
        var ship='${serviceOrder.shipNumber}';
        $.get("insertWorkOrderToResources.html?ajax=1&decorator=simple&popup=true", 
                { shipNumber:ship,distinctWorkOrder:distinctWorkOrder,template:template,action:action},
                function(data){ 
                	
               //$("#resourcMapAjax").html(data);
                 location.reload(true);
            });}
}

var act;

function contractForOICWMS(target,tempCWMS,act)
{ 
	  var contractCheck = '${contractForOI}';
	  if(contractCheck == null || contractCheck == '')
		{
		  var agree=confirm("No contract has been selected for the rates to be displayed for resources");
		/*   alert("No contract has been selected for the rates to be displayed for resources"); */
			if (agree){ 
				return false;
			}else{
				if(target== 'resourceTemplate')
					{
					getDayCWMSTemplate(tempCWMS,act);
					}else{
				addResources();
			}}
		  }else{
			  if(target== 'resourceTemplate')
				{
				 
				  var conf = 'Continue with Billing Contract : '+contractCheck;
				if(confirm(conf)){
				getDayCWMSTemplate(tempCWMS,act);
				}}else{
			addResources();
		  }
}}

function handleHttpResponseAddResources(){
	if (httpResourceAddResources.readyState == 4){
        var results= httpResourceAddResources.responseText; 
 	    var resourceDiv = document.getElementById("resourcMapAjax");
 	  	 resourceDiv.style.display = 'block';
 	    resourceDiv.innerHTML = results ;
 	}
}
var httpResourceAddResources = getHTTPObject();
function addResource(day){
	var catTempID = "catTempID"+day;
	var catTempVal = document.getElementById(catTempID).value;
	var id = '${serviceOrder.id}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource.');
	}else if(catTempVal.trim() == ''){
		alert("Select any Category for Day "+day);
		document.getElementById(catTempID).focus();
	}else{
		showOrHide(1);
		var categoryType='';
		if(catTempVal=='C'){categoryType='Crew'+day}else if(catTempVal=='M'){categoryType='Material'+day}else if(catTempVal=='E'){categoryType='Equipment'+day}else{categoryType='Vehicle'+day}
		catagoryName=categoryType;
		dayId=day;
		var url = "addRowToResourceTemplateGridAjax.html?ajax=1&tempId="+id+"&shipNumber=${serviceOrder.shipNumber}&day="+day+"&type="+catTempVal+"&categoryType="+categoryType;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	}
}

function dateAutoSave(dt,day){
	var beginDt="";
	var endDt
	if(document.getElementById('beginDt'+day)!=null){
	 beginDt = document.getElementById('beginDt'+day).value;
	}
	if(document.getElementById('endDt'+day)!=null){
	 endDt = document.getElementById('endDt'+day).value;
	}
	var daysDiff = getDateCompare(beginDt, endDt);
	if(daysDiff<0) {
		   alert("From date cannot be greater than To date");
		   document.getElementById('endDt'+day).value='';
		   return false;
	}else{
		var begin = convertDateFormat('beginDt'+day);
		var end = convertDateFormat('endDt'+day);
		if(begin.length > 0 && end.length > 0 ){
			if(true){
				var url = "autoSaveDateAjax.html?ajax=1&decorator=simple&popup=true&shipNum=${serviceOrder.shipNumber}&beginDt="+begin+"&endDt="+end+"&day="+day+"&btnType=addrow";
				http17.open("GET", url, true);
			    http17.onreadystatechange = handleHttpResponse17;
			    http17.send(null);
			}
		}
	}
}

</script>
<script language="javascript" type="text/javascript">
function fillEstimateQuantity(target,onlytemp,value)
{
	var estQty = 0;
	try{
		/* estQty=target.value;
		var letters = /[^0-9]/g;  
		 if(target.value.match(letters) != null)
			 {
			 alert("Please Enter Valid Value")
			 target.value = value;
			 return false;
			 }		 */
			 estQty=target.value;	 
			 var letters = /[^0-9]/g;  
		    var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
		     if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
		     {
		         if (target.value.split(".")[0].length != 0){
		              alert("Please Enter Valid Value")
		               target.value = value;
		              return false;
		         }
		     } 
			 
	}catch(e){
		estQty=parseFloat(target);
	}
	
	//var onlytemp = target.name.replace(/[^0-9]/g,"");
	var estHour1 = "resourceEstHour";
	var estimateQty1 = "estimateQuantity"; 
	var estimateQtyId = estimateQty1.concat(onlytemp);
	var estHourId = estHour1.concat(onlytemp);
	var estHour = document.getElementById(estHourId).value;
	document.getElementById(estimateQtyId).value = (estHour * estQty);
	var estimateExpenseId = "estimateExpense";
	var estimateRevenueId = "estimateRevenue";
	var estimateExpenseId1 = estimateExpenseId.concat(onlytemp);
	var estimateRevenueId1 = estimateRevenueId.concat(onlytemp);
	
	var estimateBuyRateId = 'estimateBuyRate';
	var estimateBuyRateId1 = estimateBuyRateId.concat(onlytemp);
	var estimateSellRateId = 'estimateSellRate';
	var estimateSellRateId1 = estimateSellRateId.concat(onlytemp);
	
	var estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
	var estimateRevenue =  (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
	
	document.getElementById(estimateExpenseId1).value =  estimateExpense;
	document.getElementById(estimateRevenueId1).value =  estimateRevenue;
	if(estHour == null || estHour == '' )
		{
		estHour=0.0;
		}
	/* var fieldValue="esthours='"+estHour+"' , quantitiy='"+estQty+"' ,estimatedexpense='"+estimateExpense+"' ,estimatedrevenue='"+estimateRevenue+"'";
	
	updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');  */

}

function fillEstimateQuantityByHours(target,value)
{
	 var letters = /[^0-9]/g;  
	var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
	 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
	 {
		 if (target.value.split(".")[0].length != 0){
	 alert("Please Enter Valid Value")
	 target.value = value;
	 return false;
	 } 
	 }
	var estHour = target.value;
	var onlytemp = target.name.replace(/[^0-9]/g,"");
	var estQty1 = "resourceQuantity";
	var estimateQty1 = "estimateQuantity"; 
	var estimateQtyId = estimateQty1.concat(onlytemp);
	var estQtyId = estQty1.concat(onlytemp);
	var estQty = document.getElementById(estQtyId).value;
	var estimateQty = estHour * estQty;
	document.getElementById(estimateQtyId).value = estimateQty;
	var estimateExpenseId = "estimateExpense";
	var estimateRevenueId = "estimateRevenue";
	var estimateExpenseId1 = estimateExpenseId.concat(onlytemp);
	var estimateRevenueId1 = estimateRevenueId.concat(onlytemp);
	var estimateBuyRateId = 'estimateBuyRate';
	var estimateBuyRateId1 = estimateBuyRateId.concat(onlytemp);
	var estimateSellRateId = 'estimateSellRate';
	var estimateSellRateId1 = estimateSellRateId.concat(onlytemp);
	var estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
	var estimateRevenue =  (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
	document.getElementById(estimateExpenseId1).value =  estimateExpense;
	document.getElementById(estimateRevenueId1).value = estimateRevenue 
	//var fieldValue="esthours='"+estHour+"' , estimatedquantity='"+estimateQty+"' ,estimatedexpense='"+estimateExpense+"' ,estimatedrevenue='"+estimateRevenue+"'";
	//updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');
	
}

function fillExpenseByQty(target)
{
	/* var letters = /[^0-9]/g;  
	 if(target.value.match(letters) != null)
		 {
		 alert("Please Enter Valid Value")
		 target.value = 0;
		 } */
		 
		    var letters = /[^0-9]/g;  
		    var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
		     if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
		     {
		         if (target.value.split(".")[0].length != 0){
			     alert("Please Enter Valid Value")
			     target.value = 0;
			         }
		     } 
		    
		 
	var estimateQty = target.value;
	var onlytemp = target.name.replace(/[^0-9]/g,"");
	var estBuyRate1 = "estimateBuyRate";
	var estimateExpense1 = "estimateExpense"; 
	var estBuyRateId = estBuyRate1.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var estBuyRate =  document.getElementById(estBuyRateId).value;
	document.getElementById(estimateExpenseId).value = estimateQty * estBuyRate;
}

function fillExpenseByRate(target)
{
	 var letters = /[^0-9]/g;  
	var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
	 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
	 {
		 if (target.value.split(".")[0].length != 0){
	 alert("Please Enter Valid Value")
	 target.value = 0;
		 }
	 } 
	
	 
	var estimateByRate = target.value;
	var onlytemp = target.name.replace(/[^0-9]/g,"");
	var estimateQuantityName = "estimateQuantity";
	var estimateExpense1 = "estimateExpense"; 
	var estimateQuantityId = estimateQuantityName.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var estimateQuantityValue =  document.getElementById(estimateQuantityId).value;
   var estimatexpense = estimateByRate * estimateQuantityValue;
	//document.getElementById(estimateExpenseId).value = estimatexpense;
	var estimatexpenseNew =""+estimatexpense;
	if(estimatexpenseNew==''){
		estimatexpenseNew = "0.00";
	}else{
		if(estimatexpenseNew.indexOf(".") == -1){
			estimatexpenseNew=estimatexpenseNew+".00";
		} 
		if((estimatexpenseNew.indexOf(".")+3 != estimatexpenseNew.length)){
			estimatexpenseNew=estimatexpenseNew+"0";
		}
	}
	document.getElementById(estimateExpenseId).value =estimatexpenseNew;
	var estimateBuyRateNew =""+estimateByRate;
	if(estimateBuyRateNew==''){
		estimateBuyRateNew = "0.00";
	}else{
		if(estimateBuyRateNew.indexOf(".") == -1){
			estimateBuyRateNew=estimateBuyRateNew+".00";
		} 
		if((estimateBuyRateNew.indexOf(".")+3 != estimateBuyRateNew.length)){
			estimateBuyRateNew=estimateBuyRateNew+"0";
		}
	}
	document.getElementById('estimateBuyRate'+onlytemp).value=estimateBuyRateNew;

	/* var fieldValue="estimatedexpense='"+estimatexpense+"' , estimatedbuyrate='"+estimateByRate+"'";
	updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');  */
}


function fillExpenseByRevQty(target,value,revExpense,revRevenue)
{
    var onlytemp = target.name.replace(/[^0-9]/g,"");
	 	 var letters = /[^0-9]/g;  
		 var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
		 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null))
		     {
			 if (target.value.split(".")[0].length != 0){
		          alert("Please Enter Valid Value")
		          document.getElementById(target.id).value = value;
		          /* document.getElementById("revisionexpense"+onlytemp).value = revExpense;
                  document.getElementById("revisionrevenue"+onlytemp).value = revRevenue; */
                  var revisedQuantityTemp =  document.getElementById("revisedQuantity"+onlytemp).value;
                  var revisionbuyrateTemp =  document.getElementById("revisionbuyrate"+onlytemp).value;
                  var revisionsellrateTemp =  document.getElementById("revisionsellrate"+onlytemp).value;
                  //document.getElementById("revisionrevenue"+onlytemp).value = revRevenue; */
                  var calc =value *revisedQuantityTemp* revisionbuyrateTemp;
                  document.getElementById("revisionexpense"+onlytemp).value =(Math.round(calc * 100) / 100); 
                 
                  var calcRev =value *revisedQuantityTemp* revisionsellrateTemp;
                  document.getElementById("revisionrevenue"+onlytemp).value =(Math.round(calcRev * 100) / 100); 
                  
                  return false;
			 }
		         }  
	var revisionQty = target.value;
	var estBuyRate1 = "revisionbuyrate";
	var estimateExpense1 = "revisionexpense"; 
	var revisionSellRate1 = "revisionsellrate";
	var revisionrevenue1 = "revisionrevenue";
	
	var revisedQuantity1 = "revisedQuantity";
	var revisedQuantity = revisedQuantity1.concat(onlytemp);
	var revisedQuantityValue = document.getElementById(revisedQuantity).value;
	
	var estBuyRateId = estBuyRate1.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var revisionSellRateId = revisionSellRate1.concat(onlytemp);
    var revisionrevenueId = revisionrevenue1.concat(onlytemp);
    var revisionBuyRateValue = document.getElementById(estBuyRateId).value;
    var revexpense = revisionQty * revisionBuyRateValue*revisedQuantityValue;
    var revisionExpenseValue = "0.00";
    revisionExpenseValue = (Math.round(revexpense * 100) / 100);
    revisionExpenseValue = Number(revisionExpenseValue);
	if (String(revisionExpenseValue).split(".").length < 2 || String(revisionExpenseValue).split(".")[1].length<=2 ){
		revisionExpenseValue = revisionExpenseValue.toFixed(2);
	}
    document.getElementById(estimateExpenseId).value = revisionExpenseValue;
    var revisionSellRateValue =  document.getElementById(revisionSellRateId).value;
    var revisionrevenue = revisionQty * revisionSellRateValue*revisedQuantityValue;
    var revisionRevenueValue = "0.00";
    revisionRevenueValue = (Math.round(revisionrevenue * 100) / 100);
    revisionRevenueValue = Number(revisionRevenueValue);
	if (String(revisionRevenueValue).split(".").length < 2 || String(revisionRevenueValue).split(".")[1].length<=2 ){
		revisionRevenueValue = revisionRevenueValue.toFixed(2);
	}
    document.getElementById(revisionrevenueId).value = revisionRevenueValue;
    /* var fieldValue="revisionquantity='"+revisionQty+"' , revisionexpense='"+revexpense+"' , revisionrevenue='"+revisionrevenue+"'";
    updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');  */
    var revisionQuantityValue = "0.00";
    revisionQuantityValue = document.getElementById("revisionQuantity"+onlytemp).value;
    revisionQuantityValue = Math.round(revisionQuantityValue*100)/100;
    revisionQuantityValue = Number(revisionQuantityValue);
	if (String(revisionQuantityValue).split(".").length < 2 || String(revisionQuantityValue).split(".")[1].length<=2 ){
		revisionQuantityValue = revisionQuantityValue.toFixed(2);
	}
    document.getElementById("revisionQuantity"+onlytemp).value = revisionQuantityValue;
}


function fillExpenseByRevisedQty(target,value,revExpense,revRevenue)
{
	var onlytemp = target.name.replace(/[^0-9]/g,"");
	var letters = /[^0-9]/g;  
         var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
         if( (target.value.match(letters) != null) && (target.value.match(decimal) == null))
             {
             if (target.value.split(".")[0].length != 0){
                  alert("Please Enter Valid Value")
                  document.getElementById(target.id).value = value;
                  var revisionQuantityTemp =  document.getElementById("revisionQuantity"+onlytemp).value;
                  var revisionbuyrateTemp =  document.getElementById("revisionbuyrate"+onlytemp).value;
                  var revisionsellrateTemp =  document.getElementById("revisionsellrate"+onlytemp).value;
                  //document.getElementById("revisionrevenue"+onlytemp).value = revRevenue; */
                  var calc =value *revisionQuantityTemp* revisionbuyrateTemp;
                  document.getElementById("revisionexpense"+onlytemp).value =(Math.round(calc * 100) / 100); 
                  
                  var calc =value *revisionQuantityTemp* revisionsellrateTemp;
                  document.getElementById("revisionrevenue"+onlytemp).value =(Math.round(calc * 100) / 100); 
                  return false;
             }
                 }  
    var revisedQty = target.value;
    var estBuyRate1 = "revisionbuyrate";
    var estimateExpense1 = "revisionexpense"; 
    var revisionSellRate1 = "revisionsellrate";
    var revisionrevenue1 = "revisionrevenue";
    
    var revisionQuantity1 = "revisionQuantity";
    var revisionQuantity = revisionQuantity1.concat(onlytemp);
    var revisionQuantityValue = document.getElementById(revisionQuantity).value;
    var estBuyRateId = estBuyRate1.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var revisionSellRateId = revisionSellRate1.concat(onlytemp);
    var revisionrevenueId = revisionrevenue1.concat(onlytemp);
    var revisionBuyRateValue = document.getElementById(estBuyRateId).value;
    var revexpense = revisedQty * revisionBuyRateValue*revisionQuantityValue;
    //document.getElementById(estimateExpenseId).value = (Math.round(revexpense * 100) / 100);
    var estimateExpenseIdValue = "0.00";
    estimateExpenseIdValue = (Math.round(revexpense * 100) / 100);
    estimateExpenseIdValue = Number(estimateExpenseIdValue);
	if (String(estimateExpenseIdValue).split(".").length < 2 || String(estimateExpenseIdValue).split(".")[1].length<=2 ){
		estimateExpenseIdValue = estimateExpenseIdValue.toFixed(2);
	}
    document.getElementById(estimateExpenseId).value = estimateExpenseIdValue;
    var revisionSellRateValue =  document.getElementById(revisionSellRateId).value;
    var revisionrevenue = revisedQty * revisionSellRateValue*revisionQuantityValue;
    //document.getElementById(revisionrevenueId).value = (Math.round(revisionrevenue * 100) / 100);
    var revisionRevenueIdValue = "0.00";
    revisionRevenueIdValue = (Math.round(revisionrevenue * 100) / 100);
    revisionRevenueIdValue = Number(revisionRevenueIdValue);
	if (String(revisionRevenueIdValue).split(".").length < 2 || String(revisionRevenueIdValue).split(".")[1].length<=2 ){
		revisionRevenueIdValue = revisionRevenueIdValue.toFixed(2);
	}
    document.getElementById(revisionrevenueId).value = revisionRevenueIdValue;
    /* var fieldValue="revisionquantity='"+revisionQty+"' , revisionexpense='"+revexpense+"' , revisionrevenue='"+revisionrevenue+"'";
    updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');  */
    
    var revisionQuantityNew = "0.00";
    revisionQuantityNew = document.getElementById("revisedQuantity"+onlytemp).value;
    revisionQuantityNew = (Math.round(revisionQuantityNew * 100) / 100);
    revisionQuantityNew = Number(revisionQuantityNew);
   	if (String(revisionQuantityNew).split(".").length < 2 || String(revisionQuantityNew).split(".")[1].length<=2 ){
   		revisionQuantityNew = revisionQuantityNew.toFixed(2);
   	}
  	document.getElementById("revisedQuantity"+onlytemp).value = revisionQuantityNew;
}

function changeExpanseBuyRevRate(target)
{
	 var letters = /[^0-9]/g;  
	var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
	 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
	 {
		 if (target.value.split(".")[0].length != 0){
	 alert("Please Enter Valid Value")
	 target.value = 0;
		 }
	 } 
	
	var estimateByRate = target.value;
	var onlytemp = target.name.replace(/[^0-9]/g,"");

	var revisedQuantityName = "revisedQuantity";
	var revisedQuantityId = revisedQuantityName.concat(onlytemp);
	var revisedQuantityValue =  document.getElementById(revisedQuantityId).value;
	  
	var estimateQuantityName = "revisionQuantity";
	var estimateExpense1 = "revisionexpense"; 
	var estimateQuantityId = estimateQuantityName.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var estimateQuantityValue =  document.getElementById(estimateQuantityId).value;
    var revisionxpensevalue = estimateByRate * estimateQuantityValue*revisedQuantityValue;
	//document.getElementById(estimateExpenseId).value = (Math.round(revisionxpensevalue * 100) / 100);
	var estimateExpenseIdValue = "0.00";
	estimateExpenseIdValue = (Math.round(revisionxpensevalue * 100) / 100);
	estimateExpenseIdValue = Number(estimateExpenseIdValue);
   	if (String(estimateExpenseIdValue).split(".").length < 2 || String(estimateExpenseIdValue).split(".")[1].length<=2 ){
   		estimateExpenseIdValue = estimateExpenseIdValue.toFixed(2);
   	}
    document.getElementById(estimateExpenseId).value = estimateExpenseIdValue;
	/* var fieldValue="revisionexpense='"+revisionxpensevalue+"' , revisionbuyrate='"+estimateByRate+"'";
	updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');  */
	var revisionBuyRateValue = "0.00";
	revisionBuyRateValue = document.getElementById("revisionbuyrate"+onlytemp).value;
	revisionBuyRateValue = Number(revisionBuyRateValue);
   	if (String(revisionBuyRateValue).split(".").length < 2 || String(revisionBuyRateValue).split(".")[1].length<=2 ){
   		revisionBuyRateValue = revisionBuyRateValue.toFixed(2);
   	}
    document.getElementById("revisionbuyrate"+onlytemp).value = revisionBuyRateValue;
}

 function changerevenueByRevSellRate(target)
{
	 var letters = /[^0-9]/g;  
	var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
	 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
	 { 
		 if (target.value.split(".")[0].length != 0){
			  alert("Please Enter Valid Value")
	            target.value = 0;
		 }
	 } 
   
	var estimateSellRate = target.value;
	var onlytemp = target.name.replace(/[^0-9]/g,"");
	
	var revisedQuantityName = "revisedQuantity";
    var revisedQuantityId = revisedQuantityName.concat(onlytemp);
    var revisedQuantityValue =  document.getElementById(revisedQuantityId).value;
	
	var estimateQuantityName = "revisionQuantity";
	var estimateExpense1 = "revisionrevenue"; 
	var estimateQuantityId = estimateQuantityName.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var estimateQuantityValue =  document.getElementById(estimateQuantityId).value;
    var revisionRevenueValue = "0.00";
	//document.getElementById(estimateExpenseId).value = (Math.round(revisionRevenueValue * 100) / 100);
    revisionRevenueValue = estimateSellRate * estimateQuantityValue*revisedQuantityValue;
    revisionRevenueValue =(Math.round(revisionRevenueValue * 100) / 100);
    revisionRevenueValue = Number(revisionRevenueValue);
   	if (String(revisionRevenueValue).split(".").length < 2 || String(revisionRevenueValue).split(".")[1].length<=2 ){
   		revisionRevenueValue = revisionRevenueValue.toFixed(2);
   	}
    document.getElementById("revisionrevenue"+onlytemp).value = revisionRevenueValue;
	/* var fieldValue="revisionrevenue='"+revisionxpensevalue+"' , revisionsellrate='"+estimateSellRate+"'";
	updateOperationIntilligence1(onlytemp,'',fieldValue,'OperationsIntelligence','${serviceOrder.shipNumber}');  */
	var revisionSellRateValue = "0.00";
	revisionSellRateValue = document.getElementById("revisionsellrate"+onlytemp).value;
	revisionSellRateValue =(Math.round(revisionSellRateValue * 100) / 100);
	revisionSellRateValue = Number(revisionSellRateValue);
   	if (String(revisionSellRateValue).split(".").length < 2 || String(revisionSellRateValue).split(".")[1].length<=2 ){
   		revisionSellRateValue = revisionSellRateValue.toFixed(2);
   	}
    document.getElementById("revisionsellrate"+onlytemp).value = revisionSellRateValue;
} 


function fillRevnueBySellRate(target)
{
	 var letters = /[^0-9]/g;  
	var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
	 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
	 {
		 if (target.value.split(".")[0].length != 0){
			  alert("Please Enter Valid Value")
	           target.value = 0;
		 }
	 } 
	 
	var estimateSellRate = target.value;
	var onlytemp = target.name.replace(/[^0-9]/g,"");
	var estimateQuantityName = "estimateQuantity";
	var estimateExpense1 = "estimateRevenue"; 
	var estimateQuantityId = estimateQuantityName.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var estimateQuantityValue =  document.getElementById(estimateQuantityId).value;
    var revisionxpensevalue = estimateSellRate * estimateQuantityValue;
	//document.getElementById(estimateExpenseId).value = estimateSellRate * estimateQuantityValue;
	var estimateRevenueNew = "0.00";
	estimateRevenueNew = estimateSellRate * estimateQuantityValue;
	estimateRevenueNew =(Math.round(estimateRevenueNew * 100) / 100);
	estimateRevenueNew = Number(estimateRevenueNew);
   	if (String(estimateRevenueNew).split(".").length < 2 || String(estimateRevenueNew).split(".")[1].length<=2 ){
   		estimateRevenueNew = estimateRevenueNew.toFixed(2);
   	}
	document.getElementById(estimateExpenseId).value =estimateRevenueNew;
	var estimateSellRateNew =""+estimateSellRate;
	var estimateSellRateNew = "0.00";
	estimateSellRateNew = estimateSellRate;
	estimateSellRateNew =(Math.round(estimateSellRateNew * 100) / 100);
	estimateSellRateNew = Number(estimateSellRateNew);
   	if (String(estimateSellRateNew).split(".").length < 2 || String(estimateSellRateNew).split(".")[1].length<=2 ){
   		estimateSellRateNew = estimateSellRateNew.toFixed(2);
   	}
	document.getElementById('estimateSellRate'+onlytemp).value=estimateSellRateNew;
}

/* var tempId;
var tempFieldName;
var tempTarget;
var tempTableName;
var tempShipNumber;
function setFieldName(id,fieldName,target,tableName,shipNumber){
	 tempId=id;
	 tempFieldName=fieldName;
	 tempTarget=target;
	 tempTableName=tableName;
	 tempShipNumber=shipNumber;	 
} 
function calcDelDate(){
	updateOperationIntilligence(tempId,tempFieldName,tempTarget,tempTableName,tempShipNumber);
	}*/
	
	
	
	function  checkLength(){

		var txt = document.forms['operationResourceForm'].elements['serviceOrder.oIOverallProjectview'].value;

		if(txt.length >495){
			alert('You can not enter more than 500 characters in O&I Overall Project View');
			document.forms['operationResourceForm'].elements['serviceOrder.oIOverallProjectview'].value = txt.substring(0,499);
			return false;
		}

		} 
	
function updateOperationIntilligence(id,fieldName,target,tableName,shipNumber)
{	
		var fieldValue='';
		showOrHide(1);
		if(fieldName==''){
			fieldValue=target;
		}else{
			fieldValue=target.value;
		}
		 var url = "updateOperationsIntelligenceAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+encodeURIComponent(fieldValue)+"&tableName="+tableName+"&shipNumber="+shipNumber;
		   http20.open("GET", url, true); 
			  http20.onreadystatechange = handleHttpResponse20;
			  http20.send(null);  
	}

function handleHttpResponse20(){
	if (http20.readyState == 4){		
            var results = http20.responseText
            results = results.trim();            
            showOrHide(0);                      
	}
}
var http20 = getHTTPObject();
</script>
<script type="text/javascript">
function findResource(type,descriptId,id){
	if(type.length >= 0 ){
		document.getElementById(descriptId).value='';
		document.getElementById('resourceQuantity'+id).value='';
		document.getElementById('estimateQuantity'+id).value='';
		document.getElementById('resourceEstHour'+id).value='';
	    document.getElementById('estimateBuyRate'+id).value='';
	    document.getElementById('estimateSellRate'+id).value=''
	    document.getElementById('estimateExpense'+id).value='';
	    document.getElementById('estimateRevenue'+id).value='';
		document.getElementById('revisionrevenue'+id).value='';
		document.getElementById('revisionbuyrate'+id).value='';
	    document.getElementById('revisionrevenue'+id).value='';
	    document.getElementById('revisionexpense'+id).value='';
	    document.getElementById('revisionsellrate'+id).value='';
		document.getElementById('revisionQuantity'+id).value='';
		document.getElementById('revisedQuantity'+id).value='';
	}else{
	}
}
</script>
<script type="text/javascript">
function findDistinctWorkOrder(position) {
	var serviceid=document.forms['operationResourceForm'].elements['serviceOrder.id'].value;
		var shipNumber='${serviceOrder.shipNumber}';
	 	var sessionCorpID='${serviceOrder.corpID}';
		var url="distinctWorkOrder.html?ajax=1&decorator=simple&popup=true&shipNumber=" + encodeURI(shipNumber)+"&sessionCorpID="+encodeURI(sessionCorpID);
	  	 ajax_showTooltip(url,position);
	  } 


var http20 = getHTTPObject();


function validateWorkOrderForType(id){
	var checkVal = "N";
	var workOrder=document.getElementById("workOrderOI"+id).value;
	if(workOrder == 'WO_'){
        checkVal = "Y" ;
    }
	if(workOrder=='' || checkVal == "Y"){
		alert("Please enter valid WO# "); 
		document.getElementById("type"+id).value='';
	}
}

</script>
 </script>
<script type="text/javascript">
var resourceDiv = document.getElementById("resourcMapAjax");
/* document.getElementById(pricingAddLine).style.display = 'block'; */
</script>
<script type="text/javascript">
function checkChar(descript,descriptId,categoryId,divId,id,target,value,ticket,corpid,shipNumber) {
	var resourcedescriptionTemp=document.getElementById('resourcedescriptionTemp'+id).value;
	if(ticket != null && ticket !='')
		{
	validateWorkTicketStatus(id,value,target,ticket,sessionCorpID,shipNumber,false);
	if(flagForAutoCommit="true"  && resourcedescriptionTemp!="1")
		{
	 	AutoCompleterAjaxCall(descriptId,categoryId,divId);
		}}else{
	AutoCompleterAjaxCall(descriptId,categoryId,divId);
	}
	}

function AutoCompleterAjaxCall(descriptId,categoryId,divId){
	var descriptVal = document.getElementById(descriptId).value;
	var type = document.getElementById(categoryId).value;
	document.getElementById('operationResourceForm').setAttribute("AutoComplete","off");
	 document.forms['operationResourceForm'].elements['tempId1'].value=descriptId;
	 document.forms['operationResourceForm'].elements['tempId2'].value=divId; 
	 new Ajax.Request('/redsky/resourceAutocomplete.html?ajax=1&tempCategory='+type+'&tempresource='+encodeURIComponent(descriptVal)+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      var mydiv = document.getElementById(divId);
                  document.getElementById(divId).style.display = "block";                
                  mydiv.innerHTML = response;
                  mydiv.show(); 
			    },
			    onFailure: function(){ alert('Something went wrong...') }
			  });
}

function checkValue(result){
	var targetID=document.forms['operationResourceForm'].elements['tempId1'].value;
	var targetID2=document.forms['operationResourceForm'].elements['tempId2'].value;
	document.getElementById(targetID).value=result.innerHTML;
	document.getElementById(targetID2).style.display = "none";
	var onlytemp = targetID.replace(/[^0-9]/g,"");
	var target = result.innerHTML
	sellReteAndBuyRateByDescription(onlytemp,'${serviceOrder.shipNumber}');
	//updateOperationIntilligence1(onlytemp,'description',target,'OperationsIntelligence','${serviceOrder.shipNumber}');
	/* sellReteAndBuyRateByDescription(description); */
	/* reloadDivInColumn(onlytemp,target) */
}
var http17 = getHTTPObject();
var http16 = getHTTPObject();
</script>
<script type="text/javascript">
try{
	document.getElementById('operationResourceForm').setAttribute("AutoComplete","off");
}catch(e){
}

function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
	var fieldValue="bookingAgentName='"+lastName+"' , bookingAgentCode='"+partnercode+"'";
	updateOperationIntilligence('${serviceOrder.id}','',fieldValue,'ServiceOrder','${serviceOrder.shipNumber}');
	 if(paertnerCodeId=='soBookingAgentCodeId'){
		findBookingAgentName();
		findCompanyDivisionByBookAg('onchange');
	}  
}

function convertItProperDateFormate(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  	   if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}
function openBookingAgentPopWindow(){
	<configByCorp:fieldVisibility componentId="component.field.vanline">
		var job =document.forms['operationResourceForm'].elements['serviceOrder.job'].value; 
			if(document.forms['operationResourceForm'].elements['transferDateFlag'].value!="yes"){ 
				var first = document.forms['operationResourceForm'].elements['serviceOrder.firstName'].value;
				var last = document.forms['operationResourceForm'].elements['serviceOrder.lastName'].value;
				javascript:openWindow('bookingAgentPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
			}
			else if(document.forms['operationResourceForm'].elements['transferDateFlag'].value=="yes"){
				var companyDivision = document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
				javascript:openWindow('bookingAgentCompanyDivision.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
			}
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		if(document.forms['operationResourceForm'].elements['transferDateFlag'].value!="yes"){ 
				var first = document.forms['operationResourceForm'].elements['serviceOrder.firstName'].value;
				var last = document.forms['operationResourceForm'].elements['serviceOrder.lastName'].value;
				javascript:openWindow('bookingAgentPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
		}
		else if(document.forms['operationResourceForm'].elements['transferDateFlag'].value=="yes"){
			var companyDivision = document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
			javascript:openWindow('bookingAgentCompanyDivision.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
		}
	</configByCorp:fieldVisibility>			
	}
</script>
 <script type="text/javascript">
 function ajax_hideTooltip(){if(ajax_tooltipObj){ajax_tooltipObj.style.display="none"}if(ajax_tooltipObjBig){ajax_tooltipObjBig.style.display="none"}}
 
function openWorkOrder()
{
	 var woFlag="";
	 var workorder="";
	 <c:forEach var="entry" items="${findDistinctWorkOrder}">
	 var id="${entry.id}";
	 var workNo=document.getElementById('checkboxId'+id).checked;
	 var wo = document.getElementById ('workOrderOI'+id).value;
	if(workNo==true)
		{
	 if(workorder == "")
		 {
		 workorder = wo
		 }else{
	 workorder = workorder+","+wo;
		 }
		}
	 </c:forEach>
	 if(workorder != "")
		 {
		ajax_hideTooltip();
	 var resourceDiv = document.getElementById("resourcMapAjax");
	    showOrHide(1);
		var shipNumber='${serviceOrder.shipNumber}';
	    $.get("findResourcesByWOAjax.html?ajax=1&decorator=simple&popup=true", 
				{woFlag:woFlag,shipNumber:shipNumber,workorder:workorder},
				function(data){
					showOrHide(0);
					$("#resourcMapAjax").html(data);
			});
}else{
	alert("Please Select any Work Order");
	return false;
}
	 }
</script>
<script type="text/javascript">
/* function deleteDataFromId(id,shipNumber,itemsJbkEquipId,ticket,sessionCorpID){
	var agree=confirm("Are you sure you wish to remove it?");
    if (agree){
    showOrHide(0);                      
    var url = "deletefromOIResorces.html?id="+id+"&shipNumber="+shipNumber+"&itemsJbkEquipId="+itemsJbkEquipId+"&workTicket="+ticket+"&sessionCorpID="+sessionCorpID;
	 http255.open("GET", url, true); 
	 http255.onreadystatechange = function() {handleHttpResponse255(id)};
	 http255.send(null);
}}
function handleHttpResponse255(id){
	if (http255.readyState == 4){		
            var results = http255.responseText
            showOrHide(0); 
            location.reload(true);
	}
} */
var http255 = getHTTPObject();
var http5 = getHTTPObject();
function goPrev() {
	progressBarAutoSave('1');
	var soIdNum = '${serviceOrder.id}';
	var seqNm ='${serviceOrder.sequenceNumber}';
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum = '${serviceOrder.id}';
	var seqNm ='${serviceOrder.sequenceNumber}';
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }   
 function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'operationResource.html?id='+results;
             }
       }
 
 function showDistinctWorkOrder(position)
 {
	 var shipNumber='${serviceOrder.shipNumber}';
	 var url="showDistinctWorkOrderForCopy.html?ajax=1&decorator=simple&popup=true&shipNumber=" + encodeURI(shipNumber);
	  ajax_showTooltip(url,position);	
 }
 
 function showWorkOrderForQuote(position)
 {
	 var shipNumber='${serviceOrder.shipNumber}';
	 var url="showWorkOrderForQuote.html?ajax=1&decorator=simple&popup=true&shipNumber=" + encodeURI(shipNumber);
	  ajax_showTooltip(url,position);	
 }
 
 function checkAll(ele) {
     var checkboxes = document.getElementsByName('checkBoxWO');
	   for (var i = 0; i < checkboxes.length; i++) {
           if (checkboxes[i].type == 'checkbox') {
               checkboxes[i].checked = true;
           }
       }
 }
 
 function uncheckAll(ele) {
     var checkboxes = document.getElementsByName('checkBoxWO');
     for (var i = 0; i < checkboxes.length; i++) {
         if (checkboxes[i].type == 'checkbox') {
             checkboxes[i].checked = false;
         }
     }
 }
 
 function insertDistinctWorkOrder(position)
 {
     var shipNumber='${serviceOrder.shipNumber}';
     var url="showDistinctWorkOrderForInsert.html?ajax=1&decorator=simple&popup=true&shipNumber=" + encodeURI(shipNumber);
      ajax_showTooltip(url,position);   
 }
 
 function findCustomerOtherSO(position) {
	 var sid='${serviceOrder.customerFileId}';
	 var soIdNum='${serviceOrder.id}';
	 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
	  ajax_showTooltip(url,position);	
	  }
 function goToUrl(id)
	{
	 <c:if test="${quoteFlag !='y'}">
		location.href = "editServiceOrderUpdate.html?id="+id;
		</c:if>
	     <c:if test="${quoteFlag =='y'}">
	     location.href = "editQuotationServiceOrderUpdate.html?id="+id;
	     </c:if>
	}

 function saveOIAjax()
 {
	var bac =  document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].value 
    var ban =  document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentName'].value 
	var sid='${serviceOrder.customerFileId}';
	var soIdNum='${serviceOrder.id}';
	var fieldValue="bookingAgentName='"+ban+"' , bookingAgentCode='"+bac+"'";
	updateOperationIntilligence('${serviceOrder.id}','',fieldValue,'ServiceOrder','${serviceOrder.shipNumber}');
 }
</script>
<script type="text/javascript">
function findBookingAgentName(){
    var bookingAgentCode = document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].value;
    if(bookingAgentCode == ''){
    	document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentName'].value = "";
    }
    if(bookingAgentCode!=''){
        try{    	
    document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentVanlineCode'].value = "";
        }catch(e){}
    if(bookingAgentCode != ''){
    showOrHide(1)
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    }   } }
function handleHttpResponse4(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		showOrHide(0);
	           			document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentName'].value = res[1];
                   		document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].select();
                	}else{
                		showOrHide(0);
	           			alert("Booking Agent code is not approved" ); 
					    document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Booking Agent code not valid");
                     document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].select();
               } } 
	      var ban = document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentName'].value;
	      var bac = document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].value;
	      var fieldValue="bookingAgentName='"+ban+"' , bookingAgentCode='"+bac+"'";
		 updateOperationIntilligence('${serviceOrder.id}','',fieldValue,'ServiceOrder','${serviceOrder.shipNumber}');
		} 
</script>
<script type="text/javascript">
      window.onload=function(){
    	  //findAllResource('');
  		//animatedcollapse.show(['resources']);
  		//applyImage();
  	  	calculateRevenueForOI();
  	  	loadSoPricing();
    	//animatedcollapse.show(['pricingShow']);
  	  var field = 'allOid';
	  var url = window.location.href;
	  if(url.indexOf('&' + field + '=') != -1){
		  removeParam("allOid");
	  }
}
      function contractForOI(target)
      {
    	  var contractCheck = '${contractForOI}';
    	  if(contractCheck == null || contractCheck == '')
			{
    		  var agree=confirm("No contract has been selected for the rates to be displayed for resources");
    		/*   alert("No contract has been selected for the rates to be displayed for resources"); */
    			if (agree){ 
    				return false;
    			}else{
    				if(target.id == 'resourceTemplate')
    					{
    					getDayTemplate();
    					}else{
    				addResources();
    			}}
    		  }else{
    			  if(target.id == 'resourceTemplate')
					{
    				  var conf = 'Continue with Billing Contract : '+contractCheck;
      				if(confirm(conf)){
					getDayTemplate();
      				}}else{
				addResources();
    		  }
      }}
</script>
<script type="text/javascript">
function downloadQuoteForOI()
{
  var shipNumber = '${serviceOrder.shipNumber}';
  var jrxmlName = 'OandIReport.jrxml';
  var saveInFileCabinet = 'true';
  var salesman='${pageContext.request.remoteUser}';
  var workorder="";
 <c:forEach var="entry" items="${showDistinctWorkOrder}">
	 var id="${entry.id}";
	 var workNo=document.getElementById('checkboxId'+id).checked;
	 var wo = document.getElementById ('workOrderOI'+id).value;
	 if(workNo==true)
		{
	 if(workorder == "")
		 {
		 workorder = wo;
		 }else{
	 workorder = workorder+"','"+wo;
		 }
		}
 </c:forEach>
 
 if(workorder == ""){
     alert("Please select any WO# ");
     return false;
 }
 workorder = "('"+workorder+"')";
// alert(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+workorder);
	 
// 	 if(workorder != "")
// 	 {
// 	 ajax_hideTooltip();
// 	 var resourceDiv = document.getElementById("resourcMapAjax");
// 	 showOrHide(1);
// 	}
ajax_hideTooltip();
   var agree = confirm("Do you want to upload the file in file cabinet?");
    if(agree){ 
     location.href="downloadQuoteForOI.html?jrxmlName="+jrxmlName+"&reportParameter_Service Order Number="+shipNumber+"&saveInFileCabinet="+saveInFileCabinet+"&reportParameter_User Name="+salesman+"&reportParameter_Work Order="+workorder;
    }else{
     location.href="downloadQuoteForOI.html?jrxmlName="+jrxmlName+"&reportParameter_Service Order Number="+shipNumber+"&reportParameter_User Name="+salesman+"&reportParameter_Work Order="+workorder;
    }
}

function downloadRevsQuoteForOI()
{
  var shipNumber = '${serviceOrder.shipNumber}';
  var jrxmlNameRev = 'OandIReportRevision.jrxml';
  var saveInFileCabinet = 'true';
  var salesman='${pageContext.request.remoteUser}';
  var workorder="";
 <c:forEach var="entry" items="${showDistinctWorkOrder}">
	 var id="${entry.id}";
	 var workNo=document.getElementById('checkboxId'+id).checked;
	 var wo = document.getElementById ('workOrderOI'+id).value;
	 if(workNo==true)
		{
	 if(workorder == "")
		 {
		 workorder = wo;
		 }else{
	 workorder = workorder+"','"+wo;
		 }
		}
 </c:forEach>
 
 if(workorder == ""){
     alert("Please select any WO# ");
     return false;
 }
 workorder = "('"+workorder+"')";
// alert(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+workorder);
	 
// 	 if(workorder != "")
// 	 {
// 	 ajax_hideTooltip();
// 	 var resourceDiv = document.getElementById("resourcMapAjax");
// 	 showOrHide(1);
// 	}
 ajax_hideTooltip();
 var agree = confirm("Do you want to upload the file in file cabinet?");
  if(agree){ 
 	location.href="downloadRevisionForOI.html?jrxmlNameRev="+jrxmlNameRev+"&reportParameter_Service Order Number="+shipNumber+"&saveInFileCabinet="+saveInFileCabinet+"&reportParameter_User Name="+salesman+"&reportParameter_Work Order="+workorder;
  }else{
   location.href="downloadRevisionForOI.html?jrxmlNameRev="+jrxmlNameRev+"&reportParameter_Service Order Number="+shipNumber+"&reportParameter_User Name="+salesman+"&reportParameter_Work Order="+workorder;
  }
    
    
    
}


</script>

<script type="text/javascript">
function checkStatus(){
     	var status=document.forms['operationResourceForm'].elements['serviceOrder.status'].value; 
     	var oldStatus=document.forms['operationResourceForm'].elements['oldStatus'].value;
     	var oldStatusNumber =document.forms['operationResourceForm'].elements['oldStatusNumber'].value;
     	var jobType = document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
         if(status=='CNCL'&& oldStatus!='CNCL'){
         var agree = confirm("Are you sure you want to cancel this Service Order?");
         if(agree){ 
         /* document.getElementById("reasonHid").style.display="block";
    	 document.getElementById("reasonHid1").style.display="block"; */
    	 autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
    	 }else{/* document.getElementById("reasonHid").style.display="none";
             	document.getElementById("reasonHid1").style.display="none"; */
             	document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';}
         }  else{
          if(status=='CNCL'){
          /* document.getElementById("reasonHid").style.display="block";
    	  document.getElementById("reasonHid1").style.display="block"; */
          }else {
                /* document.getElementById("reasonHid").style.display="none";
             	document.getElementById("reasonHid1").style.display="none"; */
             	}
             	}
          if(status=='HOLD'){
           		/*  document.getElementById("reasonHid2").style.display="block";
    	         document.getElementById("reasonHid3").style.visibility="visible"; */
           	}else{ 
           		/* document.getElementById("reasonHid2").style.display="none";
             	document.getElementById("reasonHid3").style.visibility="hidden"; */
             /* 	document.forms['serviceOrderForm'].elements['serviceOrder.nextCheckOn'].value="";  */
          } 
     	if((jobType!='STO')&& (jobType!='TPS')&& (jobType!='STF')&&(jobType!='STL')){
     	if(status=='CNCL' || status=='HOLD' || status=='REOP' || status=='2CLS'|| status=='CLSD'){
     	if(status=='CLSD' || status=='2CLS'){
     	if(oldStatus=='2CLS'||oldStatus=='CNCL'||oldStatus=='DWNLD'){
     	var agree = confirm("Are you sure you want to close this Service Order?");
         if(agree){
         autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
          if(status=='2CLS'){
          document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
          }
          else if(status=='CLSD'){
          document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
          }
        }else{
           document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['operationResourceForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	}
     	else if(oldStatus!='CNCL' && oldStatus!='DWNLD' && jobType!='RLO' ){
     	<c:if test="${not empty serviceOrder.id}">
     	if(document.forms['operationResourceForm'].elements['billing.billCompleteA'].value!='A' && document.forms['operationResourceForm'].elements['billing.billComplete'].value==''){ 
     	    alert('Job cannot be closed as billing has not been completed');
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	} else 
     	{
     	  if(document.forms['operationResourceForm'].elements['claimCount'].value!='0'){
     	   alert("There are open claim against the Service Order.") 
           document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}'; 
     	} else {
     	if(document.forms['operationResourceForm'].elements['trackingStatus.deliveryA'].value==''){
     	var agree = confirm("The service order has not been delivered, do you want to close this Service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['operationResourceForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          }  
     	} else { 
     	if(document.forms['operationResourceForm'].elements['trackingStatus.deliveryA'].value!=''){ 
     	var date1 = document.forms['operationResourceForm'].elements['trackingStatus.deliveryA'].value;	 
        var mySplitResult = date1.split("-");
        var day = mySplitResult[0];
        var month = mySplitResult[1];
        var year = mySplitResult[2]; 
        if(month == 'Jan') {
         month = "01";
        }
        else if(month == 'Feb') {
          month = "02";
        }
        else if(month == 'Mar') {
         month = "03"
        }
        else if(month == 'Apr') {
         month = "04"
        }
        else if(month == 'May') {
          month = "05"
        }
        else if(month == 'Jun') {
          month = "06"
        }
        else if(month == 'Jul') {
          month = "07"
        }
        else if(month == 'Aug') {
         month = "08"
        }
        else if(month == 'Sep') {
         month = "09"
        }
        else if(month == 'Oct')  {
         month = "10"
        }
        else if(month == 'Nov') {
         month = "11"
        }
        else if(month == 'Dec') {
         month = "12";
        }
        year="20"+year; 
        var finalDate = month+"-"+day+"-"+year;  
        date1 = finalDate.split("-"); 
        var deliveryADate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);  
        var sysDate = new Date();  
        var daysApart = Math.round((deliveryADate-sysDate)/86400000); 
     	 if(document.forms['operationResourceForm'].elements['trackingStatus.deliveryA'].value!='' && daysApart>0){
     	 var agree = confirm("It's been "+daysApart+ "days after delivery,do you want to close this Service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['operationResourceForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	
     	}     	}     	}     	}     	}
     	</c:if>
     	<c:if test="${empty serviceOrder.id}">
     	var agree = confirm("This is new service order, do you  want to cloase this service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['operationResourceForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          }  
     	</c:if>
     	} else if(oldStatus!='CNCL' && oldStatus!='DWNLD' && jobType=='RLO'){
     		<c:if test="${not empty serviceOrder.id}">
     		if(document.forms['operationResourceForm'].elements['billing.billCompleteA'].value!='A' && document.forms['operationResourceForm'].elements['billing.billComplete'].value==''){
         	    alert('Job cannot be closed as billing has not been completed');
                document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
         	}else if(document.forms['operationResourceForm'].elements['billing.billCompleteA'].value!='A' && document.forms['operationResourceForm'].elements['dspDetails.serviceCompleteDate'].value==''){
         		alert('Job cannot be closed as Service complete date is not present');
                document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
         	}
     		</c:if>
     	}  
     	}
     	if(status=='HOLD') {
        if(eval(oldStatusNumber)<10){
        document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
        }
     	if(!(eval(oldStatusNumber)<10)){
     	alert("You can't change current status to HOLD");
     	document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	}
     	}
     	if(status=='REOP'){
     	if(oldStatus=='HOLD'||oldStatus=='CNCL'||oldStatus=='2CLS'){
     	<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
     	alert("Cannot change current status to Reopen as the Customer File is not active.");
     	document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	</c:if>
     	<c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'> 
     	document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status; 
     	</c:if>
     	} 
     	else if (oldStatus!='HOLD' && oldStatus!='CNCL' && oldStatus!='2CLS'){
     	  alert("You can't change current status to Reopen");
     	  document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	} } }
     	else { 
     	if(status =='NEW' && oldStatus == 'DWNLD'){ 
     	autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
     	document.forms['operationResourceForm'].elements['serviceOrder.status'].value=status;
     	document.forms['operationResourceForm'].elements['serviceOrder.statusNumber'].value=1;
     	} else {
     	alert("You can select only Cancelled, Hold, Reopen, Ready To Close and Closed status");
     	document.forms['operationResourceForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
        document.forms['operationResourceForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
        }     	}     	}
     	else if((jobType=='STO')|| (jobType=='TPS')|| (jobType=='STF')||(jobType=='STL')){
     	if(status !='CNCL'){
     	 autoPopulate_customerFile_statusDate(document.forms['operationResourceForm'].elements['serviceOrder.statusDate']);
     	}
     	}
     } 
     
function autoPopulate_customerFile_statusDate(targetElement) { 
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec"; 
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	targetElement.form.elements['serviceOrder.statusDate'].value=datam; 
}


try{ 
    showOrHide(0);
}catch(e){}

function CopyOrderWithResources() {
	var quoteFlag = '${quoteFlag}';
	var sid =  '${serviceOrder.id}';
	var cid =  '${customerFile.id}';
	showOrHide(1);
	 new Ajax.Request('/redsky/addWithResourcesServiceOrderFromOI.html?sid='+sid+'&cid='+cid+'&decorator=simple&popup=true',
             {
               method:'get',
               onSuccess: function(transport){
                 var response = transport.responseText || "no response text";
                 if(quoteFlag != 'y'){
                 window.location.href = 'operationResource.html?id='+response.trim(); 
                 }else{
                	 window.location.href = 'operationResource.html?id='+response.trim()+'&quoteFlag=y';  
                 }
                 showOrHide1(0);
               },
               onFailure: function(){ 
                   }
             });
	}
function setFieldValue(id,result){
	document.forms['operationResourceForm'].elements[id].value = result;
	//document.getElementById(id).value = result;
}

     </script>