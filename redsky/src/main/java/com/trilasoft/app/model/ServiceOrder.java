/**
 * Implementation of <strong>ModelSupport</strong> that contains convenience methods.
 * This class represents the basic model on "ServiceOrder" object in Redsky that allows for Job management.
 * @Class Name	ServiceOrder
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.*; 
import org.appfuse.model.BaseObject;  
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany; 
import javax.persistence.Table; 
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "serviceorder")
/*@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")*/

@FilterDefs({
	/*@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") }),*/
	@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")}),
	@FilterDef(name = "serviceOrderNetworkSOFilter", parameters = { @ParamDef(name = "networkso", type = "boolean")}),
	@FilterDef(name = "serviceOrderModeFilter", parameters = { @ParamDef(name = "mode", type = "string")}),
	@FilterDef(name = "serviceOrderRoutingFilter", parameters = { @ParamDef(name = "routing", type = "string")}),
	@FilterDef(name = "serviceOrderJobFilter", parameters = { @ParamDef(name = "job", type = "string")}),
	@FilterDef(name = "agentFilter", parameters = { @ParamDef(name = "bookingagentcode", type = "string"),@ParamDef(name = "brokercode", type = "string"),@ParamDef(name = "originagentcode", type = "string"),@ParamDef(name = "originsubagentcode", type = "string"),@ParamDef(name = "destinationagentcode", type = "string"),@ParamDef(name = "destinationsubagentcode", type = "string"),@ParamDef(name = "forwardercode", type = "string")}),
	@FilterDef(name = "serviceOrderBILLTOVENDORFilter", parameters = { @ParamDef(name = "billtocode", type = "string"), @ParamDef(name = "vendorcode", type = "string")}),
	@FilterDef(name = "serviceOrderInlandFilter", parameters = { @ParamDef(name = "inlandcode", type = "string")}),
	@FilterDef(name = "serviceOrderBillToCodeFilter", parameters = { @ParamDef(name = "billtocode", type = "string")}),
	@FilterDef(name = "serviceOrderCompanyDivisionFilter", parameters = { @ParamDef(name = "companydivision", type = "string")}),
	@FilterDef(name = "serviceOrderRloPartnerPortalFilter", parameters = { @ParamDef(name = "car_vendorcode", type = "string"),@ParamDef(name = "col_vendorcode", type = "string"),@ParamDef(name = "trg_vendorcode", type = "string"),@ParamDef(name = "hom_vendorcode", type = "string"),@ParamDef(name = "rnt_vendorcode", type = "string"),@ParamDef(name = "set_vendorcode", type = "string"),@ParamDef(name = "lan_vendorcode", type = "string"),@ParamDef(name = "mmg_vendorcode", type = "string"),@ParamDef(name = "ong_vendorcode", type = "string"),@ParamDef(name = "prv_vendorcode", type = "string"),@ParamDef(name = "aio_vendorcode", type = "string"),@ParamDef(name = "exp_vendorcode", type = "string"),@ParamDef(name = "rpt_vendorcode", type = "string"),@ParamDef(name = "sch_schoolselected", type = "string"),@ParamDef(name = "tax_vendorcode", type = "string"),@ParamDef(name = "tac_vendorcode", type = "string"),@ParamDef(name = "ten_vendorcode", type = "string"),@ParamDef(name = "vis_vendorcode", type = "string"),@ParamDef(name = "wop_vendorcode", type="string"),@ParamDef(name = "rep_vendorcode",type="string"),@ParamDef(name = "rls_vendorcode", type = "string"),@ParamDef(name = "cat_vendorcode", type = "string"),@ParamDef(name = "cls_vendorcode", type = "string"),@ParamDef(name = "chs_vendorcode", type = "string"),@ParamDef(name = "dps_vendorcode", type = "string"),@ParamDef(name = "hsm_vendorcode", type = "string"),@ParamDef(name = "pdt_vendorcode", type = "string"),@ParamDef(name = "rcp_vendorcode", type = "string"),@ParamDef(name = "spa_vendorcode", type = "string"),@ParamDef(name = "tcs_vendorcode", type = "string"),@ParamDef(name = "mts_vendorcode", type = "string"),@ParamDef(name = "dss_vendorcode", type = "string"),@ParamDef(name = "hob_vendorcode", type = "string"),@ParamDef(name = "frl_vendorcode", type = "string"),@ParamDef(name = "apu_vendorcode", type = "string"),@ParamDef(name = "flb_vendorcode", type = "string"),@ParamDef(name = "ins_vendorcode", type = "string"),@ParamDef(name = "inp_vendorcode", type = "string"),@ParamDef(name = "eda_vendorcode", type = "string"),@ParamDef(name = "tas_vendorcode", type = "string")}),
	@FilterDef(name = "serviceOrderSalesPortalFilter", parameters = { @ParamDef(name = "salesman", type = "string")}),
	@FilterDef(name = "serviceOrderSalesBookingPortalFilter", parameters = { @ParamDef(name = "bookingagentcode", type = "string")}),
	@FilterDef(name = "serviceOrderBookingAgentCodeFilter", parameters = { @ParamDef(name = "bookingagentcode", type = "string")})
})

@Filters( {
	/*@Filter(name = "corpID", condition = "corpID = :forCorpID"),*/
	@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)"),
	@Filter(name = "serviceOrderNetworkSOFilter", condition = " (networkSO != :networkso) "),
	@Filter(name = "serviceOrderModeFilter", condition = " mode in (:mode) "),
	@Filter(name = "serviceOrderRoutingFilter", condition = " routing in (:routing)  "),
	@Filter(name = "serviceOrderJobFilter", condition = " job in (:job) "),
	@Filter(name = "agentFilter", condition = "( (bookingAgentCode in (:bookingagentcode))  or (brokerCode in (:brokercode))  or (originAgentCode in (:originagentcode)) or (originSubAgentCode in (:originsubagentcode))  or (destinationAgentCode in (:destinationagentcode))  or (destinationSubAgentCode in (:destinationsubagentcode))  or (forwarderCode in (:forwardercode)) )"),
	@Filter(name = "serviceOrderBILLTOVENDORFilter", condition = " ( (billToCode in (:billtocode))  or (vendorCode in (:vendorcode))) "),
	@Filter(name = "serviceOrderInlandFilter", condition = " inlandCode in (:inlandcode)  "),
	@Filter(name = "serviceOrderBillToCodeFilter", condition = " billToCode in (:billtocode)  "),
	@Filter(name = "serviceOrderCompanyDivisionFilter", condition = " companyDivision in (:companydivision)  "),
	@Filter(name = "serviceOrderRloPartnerPortalFilter", condition = " ( (CAR_vendorCode in (:car_vendorcode))  or (COL_vendorCode in (:col_vendorcode))  or (TRG_vendorCode in (:trg_vendorcode)) or (HOM_vendorCode in (:hom_vendorcode))  or (RNT_vendorCode in (:rnt_vendorcode))  or (SET_vendorCode in (:set_vendorcode))  or (LAN_vendorCode in (:lan_vendorcode)) or (MMG_vendorCode in (:mmg_vendorcode)) or (ONG_vendorCode in (:ong_vendorcode)) or (PRV_vendorCode in (:prv_vendorcode)) or (AIO_vendorCode in (:aio_vendorcode)) or (EXP_vendorCode in (:exp_vendorcode)) or (RPT_vendorCode in (:rpt_vendorcode)) or (SCH_schoolSelected in (:sch_schoolselected)) or (TAX_vendorCode in (:tax_vendorcode)) or (TAC_vendorCode in (:tac_vendorcode)) or (TEN_vendorCode in (:ten_vendorcode)) or (VIS_vendorCode in (:vis_vendorcode)) or (WOP_vendorCode in(:wop_vendorcode)) or (REP_vendorCode in(:rep_vendorcode)) or (RLS_vendorCode in (:rls_vendorcode)) or (CAT_vendorCode in (:cat_vendorcode)) or (CLS_vendorCode in (:cls_vendorcode)) or (CHS_vendorCode in (:chs_vendorcode)) or (DPS_vendorCode in (:dps_vendorcode)) or (HSM_vendorCode in (:hsm_vendorcode)) or (PDT_vendorCode in (:pdt_vendorcode)) or (RCP_vendorCode in (:rcp_vendorcode)) or (SPA_vendorCode in (:spa_vendorcode)) or (TCS_vendorCode in (:tcs_vendorcode)) or (MTS_vendorCode in (:mts_vendorcode)) or (DSS_vendorCode in (:dss_vendorcode)) or (HOB_vendorCode in (:hob_vendorcode)) or (FRL_vendorCode in (:frl_vendorcode)) or (APU_vendorCode in (:apu_vendorcode)) or (FLB_vendorCode in (:flb_vendorcode)) or (INS_vendorCode in (:ins_vendorcode)) or (INP_vendorCode in (:inp_vendorcode)) or (EDA_vendorCode in (:eda_vendorcode)) or (TAS_vendorCode in (:tas_vendorcode))) "),
	@Filter(name = "serviceOrderSalesPortalFilter", condition = " salesMan in (:salesman) "),
	@Filter(name = "serviceOrderSalesBookingPortalFilter", condition = "bookingAgentCode in (:bookingagentcode) "),
	@Filter(name = "serviceOrderBookingAgentCodeFilter", condition = "bookingAgentCode in (:bookingagentcode) ")
} )
public class ServiceOrder extends BaseObject implements ToDoRuleRecord {
	
	private boolean defaultAccountLineStatus;
	
	private String corpID;

	private Long id;
	
	private Long customerFileId;

	private String sequenceNumber;

	private String ship;

	private String shipNumber;

	private String controlFlag;
	
	private String registrationNumber;

	private Date statusDate;

	private String status;

	private String job;

	private String routing;

	private String serviceType;

	private String mode;

	private String packingMode;

	private String payType;

	private String coordinator;

	private String salesMan;

	private String prefix;

	private String firstName;

	private String lastName;

	private String mi;
	
	private String ugwIntId;

	private String originAddressLine1;

	private String suffix;

	private String originAddressLine2;

	private String originAddressLine3;

	private String originCity;

	private String originState;

	private String originCountryCode;

	
	private String originCountry;

	private String originZip;

	private String originDayPhone;

	private String originHomePhone;

	private String originFax;

	private String destinationAddressLine1;

	private String destinationAddressLine2;

	private String destinationAddressLine3;

	private String destinationCity;

	private String destinationCityCode;

	private String originCityCode;

	private String destinationState;

	private String destinationCountryCode;

	private String destinationCountry;

	private String destinationZip;

	private String destinationDayPhone;

	private String destinationHomePhone;

	private String destinationFax;

	private String contactName;
	private String contactNameExt;
	private String originContactEmail;
	private String contactPhone;

	private String contactFax;

	private String clientCode;

	private String clientName;

	private String clientReference;

	private String commodity;


	private String contract;

	private String socialSecurityNumber;

	
	private String consignee;

	private String destinationLoadSite;

	private String estimator;

	private Integer lastExtra;

	private double lastItem;

	private Date nextCheckOn;
	private String originContactLastName;
	private String contactLastName;
	private String originContactName;
	private String originContactNameExt;
	private String destinationContactEmail;
	private String originContactPhone;

	private String originContactWork;

	private String originLoadSite;

	private String orderBy;

	private String orderPhone;

	
	private String suffixNext;

	private String email;

	
	private String originCompany;

	private String destinationCompany;

	private String originMilitary;

	private String destinationMilitary;

	private String lastName2;

	private String firstName2;

	private String prefix2;
	
	private String initial2;

	private String andor;

	private String billToCode;

	private String billToName;
	private String serviceOrderFlagCarrier;

	private String originDayExtn;

	private String destinationDayExtn;

	private Boolean vip;
	private Boolean revised;
	private String unit1;
	
	private String companyDivision;
	
	private String portOfLading;
	
	private String portOfEntry;	

	private String originArea;

	private String destinationArea;

	private String tripNumber;

	private String email2;
	private String originContactExtn;

	private String destinationContactExtn;

	private String createdBy;

	private Date createdOn;
	private Date emailSent;
	private Date emailRecvd;
	private String updatedBy;

	private Date updatedOn;

	private String quoteStatus;

	private BigDecimal estimateGrossWeight;

	private BigDecimal actualGrossWeight;
		
	private BigDecimal estimatedNetWeight;

	private BigDecimal actualNetWeight;

	
	private Integer entitleNumberAuto;

	private Integer entitleBoatYN;

	private Integer estimateAuto;

	private Integer estimateBoat;

	private Integer actualAuto;

	private Integer actualBoat;

	
	

	private BigDecimal estimateCubicFeet;

	
	private BigDecimal actualCubicFeet;

	private String originMobile;

	private String destinationMobile;

	private String unit2;

	private String equipment;
	
	private String statusReason;
	
	private String bookingAgentCode;

	private String bookingAgentName;

	private BigDecimal entitledTotalAmount;

	private BigDecimal estimatedTotalExpense = new BigDecimal(0);

	private BigDecimal estimatedTotalRevenue = new BigDecimal(0);
    
	private BigDecimal distributedTotalAmount = new BigDecimal(0);
	
	private BigDecimal projectedDistributedTotalAmount = new BigDecimal(0);

	private BigDecimal estimatedGrossMargin = new BigDecimal(0);

	private BigDecimal estimatedGrossMarginPercentage = new BigDecimal(0);

	private BigDecimal revisedTotalExpense = new BigDecimal(0);

	private BigDecimal revisedTotalRevenue = new BigDecimal(0);

	private BigDecimal revisedGrossMargin = new BigDecimal(0);

	private BigDecimal revisedGrossMarginPercentage = new BigDecimal(0);;

	private BigDecimal actualExpense = new BigDecimal(0);
	
	private BigDecimal projectedActualExpense = new BigDecimal(0);

	private BigDecimal actualRevenue = new BigDecimal(0);

	private BigDecimal projectedActualRevenue = new BigDecimal(0);
	
	private BigDecimal actualGrossMargin = new BigDecimal(0);

	private BigDecimal actualGrossMarginPercentage = new BigDecimal(0);

	private String customerFeedback;
	
	private String clientOverallResponse;
	private String OAEvaluation;
	private String DAEvaluation;
	
	private String destinationEmail;

	private String destinationEmail2;

	private Integer statusNumber;

	
	
	private String bookingAgentCheckedBy;
	
	
	private String carrierDeparture;		
	private Date ETD;
	private Date ATD;
	private String carrierArrival;
	private Date ETA;
	private Date ATA;
	private String brokerCode;
	private String originAgentCode;
	private String originSubAgentCode;
	private String destinationAgentCode;
	private String destinationSubAgentCode;
	private String vendorCode;
	private String forwarderCode;
	private String rank;
	private String feedBack;
	private String inlandCode;
	private String transitDays;
	
	///////////////////requirement regarding distance unit
	private BigDecimal distance;
	private String distanceInKmMiles;
	private String gbl;
	private String quoteAccept;
	
	
	
	private String bookingAgentVanlineCode;
	private Boolean isNetworkRecord = false;
	private String bookingAgentShipNumber;
	private Date redskyBillDate;
    private CustomerFile customerFile;
	
	private Set<Claim> claims;

	private Set<Costing> costings;

	private Set<Container> containers;

	private Set<Carton> cartons;

	private Set<WorkTicket> workTickets;

	private Set<Vehicle> vehicles;

	private Set<ServicePartner> servicePartners;
	
	private Set<AccountLine> accountLines;
	
	private Set<Custom> customs;
	
	//private Set<CustomerServiceSurvey> customerServiceSurvey;
	
	private Boolean grpPref;
	
	private Date grpDeliveryDate;
	
	private String grpStatus;
	private Long grpID;
	private Boolean networkSO;
	private String packingService;
	private Boolean est;
	private Date sentDate;
	private Date returnDate;
	private String qualitySurveyExecutedBy;
	private String preMoveSurvey;
	private String originServices;
	private String destinationServices;
	private String overAll;
	private String recommendation;
	private String general;
	private String transferee;
	private String internal;
	private String shipmentType;
	private String originPreferredContactTime;
	private String destPreferredContactTime;
	private String mmCounselor;
	private Integer noOfEmailsSent;
	private BigDecimal projectedGrossMargin = new BigDecimal(0);
	private BigDecimal projectedGrossMarginPercentage = new BigDecimal(0);
	private String CAR_vendorCode; 
	private String COL_vendorCode;
	private String TRG_vendorCode;
	private String HOM_vendorCode;
	private String RNT_vendorCode;
	private String SET_vendorCode;
	private String LAN_vendorCode;
	private String MMG_vendorCode;
	private String ONG_vendorCode;
	private String PRV_vendorCode;
	private String AIO_vendorCode;
	private String EXP_vendorCode;
	private String RPT_vendorCode;
	private String SCH_schoolSelected;
	private String TAX_vendorCode;
	private String TAC_vendorCode;
	private String TEN_vendorCode;
	private String VIS_vendorCode;
	private Integer customerRating;
	private Boolean isSOExtract;
	private String incExcServiceType;
	private String incExcServiceTypeLanguage;
	private String WOP_vendorCode;
	private String REP_vendorCode;
	private String RLS_vendorCode;
	private String CAT_vendorCode;
	private String CLS_vendorCode;
	private String CHS_vendorCode;
	private String DPS_vendorCode;
	private String HSM_vendorCode;
	private String PDT_vendorCode;
	private String RCP_vendorCode;
	private String SPA_vendorCode;
	private String TCS_vendorCode;
	private String MTS_vendorCode;
	private String DSS_vendorCode;
	private String FRL_vendorCode;
	private String APU_vendorCode;
	private String quoteAcceptReason;
	private Date hsrgSoInsertRelovision;
	private String moveType;
	private String surveyorEvaluation;
	private String coordinatorEvaluation;
	private String projectManager;
	private String HOB_vendorCode;
	private String FLB_vendorCode;
	private String oaCoordinatorEval;
	private String daCoordinatorEval;
	private Boolean isUpdater;
	private String opsPerson;
	private String clientId;
	private String accrualReadyUpdatedBy;
	private Date accrualReadyDate;
	private String incExcDocTypeLanguage;
	private String incExcDocServiceType;
	private Set<InlandAgent> inlandAgent;
	private String recInvoiceNumber;
	private Boolean additionalAssistance;
	private Date accrueChecked;
	private Integer estimateOverallMarkupPercentage;
	private String surveyCoordinator;
	private Date updaterUpdatedOn;
	private Date welcomeMailSentOn;
	private String packingDays;
	private Date importPaperOn;
	private Boolean thirdPartyServiceRequired;
	private String quoteNumber;
	private Date survey;
	private String surveyTime;
	private String surveyTime2;
	private Date actualSurveyDate;
	private String  surveyEmailLanguage;
	private String originAgentName;
	private String originAgentContact;
	private String originAgentPhoneNumber;
	private Date priceSubmissionToAccDate;
	private Date priceSubmissionToTranfDate;
	private Date priceSubmissionToBookerDate;
	private Date quoteAcceptenceDate;
	private String originAgentEmail;
	private String INS_vendorCode;
	private String INP_vendorCode;
	private String EDA_vendorCode;
	private String TAS_vendorCode;
	private Boolean jimExtract = new Boolean(false);
	private String supplementGBL;
	//Bug 14068 - NPS score in RedSky
	private String npsScore;
	private Date reptHourReceived;
	private String oIOverallProjectview;
	private Date quoteStopEmail;
	private Date revisedDate;


	@Override
	public String toString() {
		return new ToStringBuilder(this).append("defaultAccountLineStatus",
				defaultAccountLineStatus).append("corpID", corpID).append("id",
				id).append("customerFileId", customerFileId).append(
				"sequenceNumber", sequenceNumber).append("ship", ship).append(
				"shipNumber", shipNumber).append("controlFlag", controlFlag)
				.append("registrationNumber", registrationNumber).append(
						"statusDate", statusDate).append("status", status)
				.append("job", job).append("routing", routing).append(
						"serviceType", serviceType).append("mode", mode)
				.append("packingMode", packingMode).append("payType", payType)
				.append("coordinator", coordinator)
				.append("salesMan", salesMan).append("prefix", prefix).append(
						"firstName", firstName).append("lastName", lastName)
				.append("mi", mi).append("originAddressLine1",
						originAddressLine1).append("suffix", suffix).append(
						"originAddressLine2", originAddressLine2).append(
						"originAddressLine3", originAddressLine3).append(
						"originCity", originCity).append("originState",
						originState).append("originCountryCode",
						originCountryCode).append("originCountry",
						originCountry).append("originZip", originZip).append(
						"originDayPhone", originDayPhone).append(
						"originHomePhone", originHomePhone).append("originFax",
						originFax).append("destinationAddressLine1",
						destinationAddressLine1).append(
						"destinationAddressLine2", destinationAddressLine2)
				.append("destinationAddressLine3", destinationAddressLine3)
				.append("destinationCity", destinationCity).append(
						"destinationCityCode", destinationCityCode).append(
						"originCityCode", originCityCode).append(
						"destinationState", destinationState).append(
						"destinationCountryCode", destinationCountryCode)
				.append("destinationCountry", destinationCountry).append(
						"destinationZip", destinationZip)
				.append("originContactLastName",originContactLastName)
				.append("contactLastName",contactLastName)
						.append("destinationDayPhone", destinationDayPhone).append(
						"destinationHomePhone", destinationHomePhone).append(
						"destinationFax", destinationFax).append("contactName",
						contactName).append("contactNameExt", contactNameExt)
				.append("originContactEmail", originContactEmail).append(
						"contactPhone", contactPhone).append("contactFax",
						contactFax).append("clientCode", clientCode).append(
						"clientName", clientName).append("clientReference",
						clientReference).append("commodity", commodity).append(
						"contract", contract).append("socialSecurityNumber",
						socialSecurityNumber).append("consignee", consignee)
				.append("destinationLoadSite", destinationLoadSite).append(
						"estimator", estimator).append("lastExtra", lastExtra)
				.append("lastItem", lastItem)
				.append("nextCheckOn", nextCheckOn).append("originContactName",
						originContactName).append("originContactNameExt",
						originContactNameExt).append("destinationContactEmail",
						destinationContactEmail).append("originContactPhone",
						originContactPhone).append("originContactWork",
						originContactWork).append("originLoadSite",
						originLoadSite).append("orderBy", orderBy).append(
						"orderPhone", orderPhone).append("suffixNext",
						suffixNext).append("email", email).append(
						"originCompany", originCompany).append(
						"destinationCompany", destinationCompany).append(
						"originMilitary", originMilitary).append(
						"destinationMilitary", destinationMilitary).append(
						"lastName2", lastName2)
				.append("firstName2", firstName2).append("prefix2", prefix2)
				.append("initial2", initial2).append("andor", andor).append(
						"billToCode", billToCode).append("billToName",
						billToName).append("originDayExtn", originDayExtn)
				.append("destinationDayExtn", destinationDayExtn).append("vip",
						vip).append("revised", revised).append("unit1", unit1)
				.append("companyDivision", companyDivision).append(
						"portOfLading", portOfLading).append("portOfEntry",
						portOfEntry).append("originArea", originArea).append(
						"destinationArea", destinationArea).append(
						"tripNumber", tripNumber).append("email2", email2)
				.append("originContactExtn", originContactExtn).append(
						"destinationContactExtn", destinationContactExtn)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("emailSent", emailSent).append("emailRecvd", emailRecvd)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("quoteStatus",
						quoteStatus).append("estimateGrossWeight",
						estimateGrossWeight).append("actualGrossWeight",
						actualGrossWeight).append("estimatedNetWeight",
						estimatedNetWeight).append("actualNetWeight",
						actualNetWeight).append("entitleNumberAuto",
						entitleNumberAuto).append("entitleBoatYN",
						entitleBoatYN).append("estimateAuto", estimateAuto)
				.append("estimateBoat", estimateBoat).append("actualAuto",
						actualAuto).append("actualBoat", actualBoat).append(
						"estimateCubicFeet", estimateCubicFeet).append(
						"actualCubicFeet", actualCubicFeet).append(
						"originMobile", originMobile).append(
						"destinationMobile", destinationMobile).append("unit2",
						unit2).append("equipment", equipment).append(
						"statusReason", statusReason).append(
						"bookingAgentCode", bookingAgentCode).append(
						"bookingAgentName", bookingAgentName).append(
						"entitledTotalAmount", entitledTotalAmount).append(
						"estimatedTotalExpense", estimatedTotalExpense).append(
						"estimatedTotalRevenue", estimatedTotalRevenue).append(
						"distributedTotalAmount", distributedTotalAmount)
				.append("projectedDistributedTotalAmount",
						projectedDistributedTotalAmount).append(
						"estimatedGrossMargin", estimatedGrossMargin).append(
						"estimatedGrossMarginPercentage",
						estimatedGrossMarginPercentage).append(
						"revisedTotalExpense", revisedTotalExpense).append(
						"revisedTotalRevenue", revisedTotalRevenue).append(
						"revisedGrossMargin", revisedGrossMargin).append(
						"revisedGrossMarginPercentage",
						revisedGrossMarginPercentage).append("actualExpense",
						actualExpense).append("projectedActualExpense",
						projectedActualExpense).append("actualRevenue",
						actualRevenue).append("projectedActualRevenue",
						projectedActualRevenue).append("actualGrossMargin",
						actualGrossMargin).append(
						"actualGrossMarginPercentage",
						actualGrossMarginPercentage).append("customerFeedback",
						customerFeedback).append("clientOverallResponse",
						clientOverallResponse).append("OAEvaluation",
						OAEvaluation).append("DAEvaluation", DAEvaluation)
				.append("destinationEmail", destinationEmail).append(
						"destinationEmail2", destinationEmail2).append(
						"statusNumber", statusNumber).append(
						"bookingAgentCheckedBy", bookingAgentCheckedBy).append(
						"carrierDeparture", carrierDeparture)
				.append("ETD", ETD).append("ATD", ATD).append("carrierArrival",
						carrierArrival).append("ETA", ETA).append("ATA", ATA)
				.append("brokerCode", brokerCode).append("originAgentCode",
						originAgentCode).append("originSubAgentCode",
						originSubAgentCode).append("destinationAgentCode",
						destinationAgentCode).append("destinationSubAgentCode",
						destinationSubAgentCode).append("vendorCode",
						vendorCode).append("forwarderCode", forwarderCode)
				.append("rank", rank).append("feedBack", feedBack).append(
						"inlandCode", inlandCode).append("transitDays",
						transitDays).append("distance", distance).append(
						"distanceInKmMiles", distanceInKmMiles).append("gbl",
						gbl).append("quoteAccept", quoteAccept).append(
						"bookingAgentVanlineCode", bookingAgentVanlineCode)
				.append("isNetworkRecord", isNetworkRecord).append(
						"bookingAgentShipNumber", bookingAgentShipNumber)
				.append("redskyBillDate", redskyBillDate).append("grpPref",
						grpPref).append("grpDeliveryDate", grpDeliveryDate)
				.append("grpStatus", grpStatus).append("grpID", grpID).append(
						"networkSO", networkSO).append("packingService", packingService)
						.append("est",est).append("sentDate",sentDate)
						.append("returnDate",returnDate)
						.append("qualitySurveyExecutedBy",qualitySurveyExecutedBy)
						.append("preMoveSurvey",preMoveSurvey).append("originServices",originServices)
						.append("destinationServices",destinationServices).append("overAll",overAll)
						.append("recommendation",recommendation).append("general",general)
						.append("transferee",transferee).append("internal",internal)
						.append("shipmentType",shipmentType)
						.append("originPreferredContactTime",originPreferredContactTime)
						.append("destPreferredContactTime",destPreferredContactTime)
						.append("ugwIntId",ugwIntId).append("mmCounselor",mmCounselor)
						.append("noOfEmailsSent",noOfEmailsSent)
						.append("projectedGrossMargin",projectedGrossMargin)
						.append("projectedGrossMarginPercentage",projectedGrossMarginPercentage)
						.append("serviceOrderFlagCarrier",serviceOrderFlagCarrier)
						.append("CAR_vendorCode",CAR_vendorCode)
	                    .append("COL_vendorCode",COL_vendorCode)
	                    .append("TRG_vendorCode",TRG_vendorCode)
	                    .append("HOM_vendorCode",HOM_vendorCode)
	                    .append("RNT_vendorCode",RNT_vendorCode)
	                    .append("SET_vendorCode",SET_vendorCode)
	                    .append("LAN_vendorCode",LAN_vendorCode)
	                    .append("MMG_vendorCode",MMG_vendorCode)
	                    .append("ONG_vendorCode",ONG_vendorCode)
	                    .append("PRV_vendorCode",PRV_vendorCode)
	                    .append("AIO_vendorCode",AIO_vendorCode)
	                    .append("EXP_vendorCode",EXP_vendorCode)
	                    .append("RPT_vendorCode",RPT_vendorCode)
	                    .append("SCH_schoolSelected",SCH_schoolSelected)
	                    .append("TAX_vendorCode",TAX_vendorCode)
	                    .append("TAC_vendorCode",TAC_vendorCode)
	                    .append("TEN_vendorCode",TEN_vendorCode)
	                    .append("VIS_vendorCode",VIS_vendorCode)
	                    .append("customerRating",customerRating)
	                    .append("incExcServiceType",incExcServiceType)
	                    .append("incExcServiceTypeLanguage",incExcServiceTypeLanguage)
	                    .append("WOP_vendorCode",WOP_vendorCode)
	                    .append("REP_vendorCode",REP_vendorCode)
	                    .append("RLS_vendorCode",RLS_vendorCode)
	                    .append("CAT_vendorCode", CAT_vendorCode)
	                    .append("CLS_vendorCode", CLS_vendorCode)
	                    .append("CHS_vendorCode", CHS_vendorCode)
	                    .append("DPS_vendorCode", DPS_vendorCode)
	                    .append("HSM_vendorCode", HSM_vendorCode)
	                    .append("PDT_vendorCode", PDT_vendorCode)
	                    .append("RCP_vendorCode", RCP_vendorCode)
	                    .append("SPA_vendorCode", SPA_vendorCode)
	                    .append("TCS_vendorCode", TCS_vendorCode)
	                    .append("MTS_vendorCode", MTS_vendorCode)
	                    .append("DSS_vendorCode",DSS_vendorCode)
	                    .append("quoteAcceptReason",quoteAcceptReason)
	                    .append("hsrgSoInsertRelovision",hsrgSoInsertRelovision)
	                    .append("moveType",moveType)
	                    .append("surveyorEvaluation",surveyorEvaluation)
	                    .append("coordinatorEvaluation",coordinatorEvaluation)
	                    .append("projectManager",projectManager)
	                    .append("HOB_vendorCode",HOB_vendorCode)
	                    .append("FLB_vendorCode",FLB_vendorCode)
	                    .append("oaCoordinatorEval",oaCoordinatorEval)
	                    .append("daCoordinatorEval",daCoordinatorEval)
	                    .append("isUpdater",isUpdater)
	                    .append("opsPerson", opsPerson)
	                    .append("clientId", clientId)
	                    .append("FRL_vendorCode", FRL_vendorCode)
	                    .append("APU_vendorCode",APU_vendorCode)
	                    .append("accrualReadyUpdatedBy",accrualReadyUpdatedBy)
	                    .append("accrualReadyDate",accrualReadyDate)
	                    .append("incExcDocTypeLanguage",incExcDocTypeLanguage)
	                    .append("incExcDocServiceType",incExcDocServiceType)
	                    .append("recInvoiceNumber",recInvoiceNumber)
	                    .append("additionalAssistance", additionalAssistance)
	                    .append("accrueChecked", accrueChecked)
	                    .append("estimateOverallMarkupPercentage", estimateOverallMarkupPercentage)
	                    .append("surveyCoordinator", surveyCoordinator)
	                    .append("updaterUpdatedOn", updaterUpdatedOn)
	                    .append("welcomeMailSentOn", welcomeMailSentOn)
	                    .append("packingDays",packingDays)
	                    .append("importPaperOn",importPaperOn)
	                    .append("thirdPartyServiceRequired",thirdPartyServiceRequired)
	                    .append("quoteNumber",quoteNumber)
	                    .append("survey",survey)
	                    .append("surveyTime",surveyTime)
	                    .append("surveyTime2",surveyTime2)
	                    .append("actualSurveyDate",actualSurveyDate)
	                    .append("surveyEmailLanguage",surveyEmailLanguage)
	                    .append("originAgentName",originAgentName)
	                    .append("originAgentContact",originAgentContact)
	                    .append("originAgentEmail",originAgentEmail)
	                    .append("originAgentPhoneNumber",originAgentPhoneNumber)
	                    .append("priceSubmissionToAccDate",priceSubmissionToAccDate)
	                    .append("priceSubmissionToTranfDate",priceSubmissionToTranfDate)
	                    .append("priceSubmissionToBookerDate",priceSubmissionToBookerDate)
	                    .append("quoteAcceptenceDate",quoteAcceptenceDate)
	                    .append("INS_vendorCode",INS_vendorCode).append("INP_vendorCode",INP_vendorCode).append("EDA_vendorCode",EDA_vendorCode).append("TAS_vendorCode",TAS_vendorCode)
	                    .append("jimExtract",jimExtract)
	                     .append("supplementGBL",supplementGBL)
	                     .append("npsScore",npsScore)
	                     .append("reptHourReceived",reptHourReceived)
	                     .append("oIOverallProjectview",oIOverallProjectview).append("quoteStopEmail",quoteStopEmail)
	                     .append("revisedDate",revisedDate)
	                    .toString();
						
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ServiceOrder))
			return false;
		ServiceOrder castOther = (ServiceOrder) other;
		return new EqualsBuilder().append(defaultAccountLineStatus,
				castOther.defaultAccountLineStatus).append(corpID,
				castOther.corpID).append(id, castOther.id).append(
				customerFileId, castOther.customerFileId).append(
				sequenceNumber, castOther.sequenceNumber).append(ship,
				castOther.ship).append(shipNumber, castOther.shipNumber)
				.append(controlFlag, castOther.controlFlag).append(
						registrationNumber, castOther.registrationNumber)
				.append(statusDate, castOther.statusDate).append(status,
						castOther.status).append(job, castOther.job).append(
						routing, castOther.routing).append(serviceType,
						castOther.serviceType).append(mode, castOther.mode)
				.append(packingMode, castOther.packingMode).append(payType,
						castOther.payType).append(coordinator,
						castOther.coordinator).append(salesMan,
						castOther.salesMan).append(prefix, castOther.prefix)
				.append(firstName, castOther.firstName).append(lastName,
						castOther.lastName).append(mi, castOther.mi).append(
						originAddressLine1, castOther.originAddressLine1)
				.append(suffix, castOther.suffix).append(originAddressLine2,
						castOther.originAddressLine2).append(
						originAddressLine3, castOther.originAddressLine3)
				.append(originCity, castOther.originCity).append(originState,
						castOther.originState).append(originCountryCode,
						castOther.originCountryCode).append(originCountry,
						castOther.originCountry).append(originZip,
						castOther.originZip).append(originDayPhone,
						castOther.originDayPhone).append(originHomePhone,
						castOther.originHomePhone).append(originFax,
						castOther.originFax).append(destinationAddressLine1,
						castOther.destinationAddressLine1).append(
						destinationAddressLine2,
						castOther.destinationAddressLine2).append(
						destinationAddressLine3,
						castOther.destinationAddressLine3).append(
						destinationCity, castOther.destinationCity).append(
						destinationCityCode, castOther.destinationCityCode)
				.append(originCityCode, castOther.originCityCode).append(
						destinationState, castOther.destinationState).append(
						destinationCountryCode,
						castOther.destinationCountryCode).append(
						destinationCountry, castOther.destinationCountry)
				.append(destinationZip, castOther.destinationZip).append(
						destinationDayPhone, castOther.destinationDayPhone)
				.append(destinationHomePhone, castOther.destinationHomePhone)
				.append(destinationFax, castOther.destinationFax).append(
						contactName, castOther.contactName).append(
						contactNameExt, castOther.contactNameExt).append(
						originContactEmail, castOther.originContactEmail)
				.append(contactPhone, castOther.contactPhone).append(
						contactFax, castOther.contactFax).append(clientCode,
						castOther.clientCode).append(clientName,
						castOther.clientName).append(clientReference,
						castOther.clientReference).append(commodity,
						castOther.commodity).append(contract,
						castOther.contract).append(socialSecurityNumber,
						castOther.socialSecurityNumber).append(consignee,
						castOther.consignee).append(destinationLoadSite,
						castOther.destinationLoadSite).append(estimator,
						castOther.estimator).append(lastExtra,
						castOther.lastExtra).append(lastItem,
						castOther.lastItem).append(nextCheckOn,
						castOther.nextCheckOn).append(originContactName,
						castOther.originContactName).append(
						originContactNameExt, castOther.originContactNameExt)
				.append(destinationContactEmail,
						castOther.destinationContactEmail).append(
						originContactPhone, castOther.originContactPhone)
				.append(originContactWork, castOther.originContactWork)
				.append(originContactLastName,castOther.originContactLastName)
				.append(contactLastName,castOther.contactLastName)
				.append(originLoadSite, castOther.originLoadSite).append(
						orderBy, castOther.orderBy).append(orderPhone,
						castOther.orderPhone).append(suffixNext,
						castOther.suffixNext).append(email, castOther.email)
				.append(originCompany, castOther.originCompany).append(
						destinationCompany, castOther.destinationCompany)
				.append(originMilitary, castOther.originMilitary).append(
						destinationMilitary, castOther.destinationMilitary)
				.append(lastName2, castOther.lastName2).append(firstName2,
						castOther.firstName2)
				.append(prefix2, castOther.prefix2).append(initial2,
						castOther.initial2).append(andor, castOther.andor)
				.append(billToCode, castOther.billToCode).append(billToName,
						castOther.billToName).append(originDayExtn,
						castOther.originDayExtn).append(destinationDayExtn,
						castOther.destinationDayExtn)
				.append(vip, castOther.vip).append(revised, castOther.revised)
				.append(unit1, castOther.unit1).append(companyDivision,
						castOther.companyDivision).append(portOfLading,
						castOther.portOfLading).append(portOfEntry,
						castOther.portOfEntry).append(originArea,
						castOther.originArea).append(destinationArea,
						castOther.destinationArea).append(tripNumber,
						castOther.tripNumber).append(email2, castOther.email2)
				.append(originContactExtn, castOther.originContactExtn).append(
						destinationContactExtn,
						castOther.destinationContactExtn).append(createdBy,
						castOther.createdBy).append(createdOn,
						castOther.createdOn).append(emailSent,
						castOther.emailSent).append(emailRecvd,
						castOther.emailRecvd).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(quoteStatus,
						castOther.quoteStatus).append(estimateGrossWeight,
						castOther.estimateGrossWeight).append(
						actualGrossWeight, castOther.actualGrossWeight).append(
						estimatedNetWeight, castOther.estimatedNetWeight)
				.append(actualNetWeight, castOther.actualNetWeight).append(
						entitleNumberAuto, castOther.entitleNumberAuto).append(
						entitleBoatYN, castOther.entitleBoatYN).append(
						estimateAuto, castOther.estimateAuto).append(
						estimateBoat, castOther.estimateBoat).append(
						actualAuto, castOther.actualAuto).append(actualBoat,
						castOther.actualBoat).append(estimateCubicFeet,
						castOther.estimateCubicFeet).append(actualCubicFeet,
						castOther.actualCubicFeet).append(originMobile,
						castOther.originMobile).append(destinationMobile,
						castOther.destinationMobile).append(unit2,
						castOther.unit2).append(equipment, castOther.equipment)
				.append(statusReason, castOther.statusReason).append(
						bookingAgentCode, castOther.bookingAgentCode).append(
						bookingAgentName, castOther.bookingAgentName).append(
						entitledTotalAmount, castOther.entitledTotalAmount)
				.append(estimatedTotalExpense, castOther.estimatedTotalExpense)
				.append(estimatedTotalRevenue, castOther.estimatedTotalRevenue)
				.append(distributedTotalAmount,
						castOther.distributedTotalAmount).append(
						projectedDistributedTotalAmount,
						castOther.projectedDistributedTotalAmount).append(
						estimatedGrossMargin, castOther.estimatedGrossMargin)
				.append(estimatedGrossMarginPercentage,
						castOther.estimatedGrossMarginPercentage).append(
						revisedTotalExpense, castOther.revisedTotalExpense)
				.append(revisedTotalRevenue, castOther.revisedTotalRevenue)
				.append(revisedGrossMargin, castOther.revisedGrossMargin)
				.append(revisedGrossMarginPercentage,
						castOther.revisedGrossMarginPercentage).append(
						actualExpense, castOther.actualExpense).append(
						projectedActualExpense,
						castOther.projectedActualExpense).append(actualRevenue,
						castOther.actualRevenue).append(projectedActualRevenue,
						castOther.projectedActualRevenue).append(
						actualGrossMargin, castOther.actualGrossMargin).append(
						actualGrossMarginPercentage,
						castOther.actualGrossMarginPercentage).append(
						customerFeedback, castOther.customerFeedback).append(
						clientOverallResponse, castOther.clientOverallResponse)
				.append(OAEvaluation, castOther.OAEvaluation).append(
						DAEvaluation, castOther.DAEvaluation).append(
						destinationEmail, castOther.destinationEmail).append(
						destinationEmail2, castOther.destinationEmail2).append(
						statusNumber, castOther.statusNumber).append(
						bookingAgentCheckedBy, castOther.bookingAgentCheckedBy)
				.append(carrierDeparture, castOther.carrierDeparture).append(
						ETD, castOther.ETD).append(ATD, castOther.ATD).append(
						carrierArrival, castOther.carrierArrival).append(ETA,
						castOther.ETA).append(ATA, castOther.ATA).append(
						brokerCode, castOther.brokerCode).append(
						originAgentCode, castOther.originAgentCode).append(
						originSubAgentCode, castOther.originSubAgentCode)
				.append(destinationAgentCode, castOther.destinationAgentCode)
				.append(destinationSubAgentCode,
						castOther.destinationSubAgentCode).append(vendorCode,
						castOther.vendorCode).append(forwarderCode,
						castOther.forwarderCode).append(rank, castOther.rank)
				.append(feedBack, castOther.feedBack).append(inlandCode,
						castOther.inlandCode).append(transitDays,
						castOther.transitDays).append(distance,
						castOther.distance).append(distanceInKmMiles,
						castOther.distanceInKmMiles).append(gbl, castOther.gbl)
				.append(quoteAccept, castOther.quoteAccept).append(
						bookingAgentVanlineCode,
						castOther.bookingAgentVanlineCode).append(
						isNetworkRecord, castOther.isNetworkRecord).append(
						bookingAgentShipNumber,
						castOther.bookingAgentShipNumber).append(
						redskyBillDate, castOther.redskyBillDate).append(
						grpPref, castOther.grpPref).append(grpDeliveryDate,
						castOther.grpDeliveryDate).append(grpStatus,
						castOther.grpStatus).append(grpID, castOther.grpID)
				.append(networkSO, castOther.networkSO).append(packingService, castOther.packingService)
				.append(est, castOther.est).append(sentDate,castOther.sentDate)
				.append(returnDate,castOther.returnDate).append(qualitySurveyExecutedBy,castOther.qualitySurveyExecutedBy)
				.append(preMoveSurvey,castOther.preMoveSurvey).append(originServices,castOther.originServices)
				.append(destinationServices,castOther.destinationServices).append(overAll,castOther.overAll)
				.append(recommendation,castOther.recommendation).append(general,castOther.general)
				.append(transferee,castOther.transferee)
				.append(internal,castOther.internal)
				.append(shipmentType,castOther.shipmentType)
				.append(originPreferredContactTime,castOther.originPreferredContactTime)
				.append(destPreferredContactTime,castOther.destPreferredContactTime)
				.append(ugwIntId,castOther.ugwIntId)
				.append(mmCounselor,castOther.mmCounselor)
				.append(noOfEmailsSent, castOther.noOfEmailsSent)
				.append(projectedGrossMargin, castOther.projectedGrossMargin)
				.append(projectedGrossMarginPercentage, castOther.projectedGrossMarginPercentage)
				.append(serviceOrderFlagCarrier, castOther.serviceOrderFlagCarrier)
				.append(CAR_vendorCode,castOther.CAR_vendorCode)
	            .append(COL_vendorCode,castOther.COL_vendorCode)
	            .append(TRG_vendorCode,castOther.TRG_vendorCode)
	            .append(HOM_vendorCode,castOther.HOM_vendorCode)
	            .append(RNT_vendorCode,castOther.RNT_vendorCode)
	            .append(SET_vendorCode,castOther.SET_vendorCode)
	            .append(LAN_vendorCode,castOther.LAN_vendorCode)
	            .append(MMG_vendorCode,castOther.MMG_vendorCode)
	            .append(ONG_vendorCode,castOther.ONG_vendorCode)
	            .append(PRV_vendorCode,castOther.PRV_vendorCode)
	            .append(AIO_vendorCode,castOther.AIO_vendorCode)
	            .append(EXP_vendorCode,castOther.EXP_vendorCode)
	            .append(RPT_vendorCode,castOther.RPT_vendorCode)
	            .append(SCH_schoolSelected,castOther.SCH_schoolSelected)
	            .append(TAX_vendorCode,castOther.TAX_vendorCode)
	            .append(TAC_vendorCode,castOther.TAC_vendorCode)
	            .append(TEN_vendorCode,castOther.TEN_vendorCode)
	            .append(VIS_vendorCode,castOther.VIS_vendorCode)
	            .append(customerRating,castOther.customerRating)
	            .append(incExcServiceType, castOther.incExcServiceType)
				.append(incExcServiceTypeLanguage, castOther.incExcServiceTypeLanguage)
				.append(WOP_vendorCode,castOther.WOP_vendorCode)
				.append(REP_vendorCode,castOther.REP_vendorCode)
				.append(RLS_vendorCode, castOther.RLS_vendorCode)
		        .append(CAT_vendorCode,castOther.CAT_vendorCode)
		        .append(CLS_vendorCode,castOther.CLS_vendorCode)
		        .append(CHS_vendorCode,castOther.CHS_vendorCode)
		        .append(DPS_vendorCode,castOther.DPS_vendorCode)
		        .append(HSM_vendorCode,castOther.HSM_vendorCode)
		        .append(PDT_vendorCode,castOther.PDT_vendorCode)
		        .append(RCP_vendorCode,castOther.RCP_vendorCode)
		        .append(SPA_vendorCode,castOther.SPA_vendorCode)
		        .append(TCS_vendorCode,castOther.TCS_vendorCode)
			    .append(MTS_vendorCode, castOther.MTS_vendorCode)
			    .append(DSS_vendorCode, castOther.DSS_vendorCode)
			    .append(quoteAcceptReason, castOther.quoteAcceptReason)
			    .append(hsrgSoInsertRelovision, castOther.hsrgSoInsertRelovision)
			    .append(moveType, castOther.moveType)
			    .append(surveyorEvaluation, castOther.surveyorEvaluation)
			    .append(coordinatorEvaluation, castOther.coordinatorEvaluation)
			    .append(projectManager, castOther.projectManager)
			    .append(HOB_vendorCode,castOther.HOB_vendorCode)
	            .append(FLB_vendorCode,castOther.FLB_vendorCode)
	            .append(oaCoordinatorEval,castOther.oaCoordinatorEval)
	            .append(daCoordinatorEval,castOther.daCoordinatorEval)
	            .append(isUpdater,castOther.isUpdater)
	            .append(opsPerson,castOther.opsPerson)
	            .append(clientId, castOther.clientId)
	            .append(FRL_vendorCode, castOther.FRL_vendorCode)
	            .append(APU_vendorCode, castOther.APU_vendorCode)
	            .append(accrualReadyUpdatedBy,castOther.accrualReadyUpdatedBy)
	            .append(accrualReadyDate,castOther.accrualReadyDate)
	            .append(incExcDocTypeLanguage,castOther.incExcDocTypeLanguage)
	            .append(incExcDocServiceType,castOther.incExcDocServiceType)
	            .append(recInvoiceNumber, castOther.recInvoiceNumber)
	            .append(additionalAssistance, castOther.additionalAssistance)
	            .append(accrueChecked, castOther.accrueChecked)
	            .append(estimateOverallMarkupPercentage, castOther.estimateOverallMarkupPercentage)
	            .append(surveyCoordinator, castOther.surveyCoordinator)
	            .append(updaterUpdatedOn, castOther.updaterUpdatedOn)
	            .append(welcomeMailSentOn, castOther.welcomeMailSentOn)
	            .append(packingDays,castOther.packingDays)
	            .append(importPaperOn,castOther.importPaperOn).append(thirdPartyServiceRequired, thirdPartyServiceRequired)
	            .append(quoteNumber,castOther.quoteNumber)
	            .append(survey,castOther.survey)
	            .append(surveyTime,castOther.surveyTime)
	            .append(surveyTime2,castOther.surveyTime2)
	            .append(actualSurveyDate,castOther.actualSurveyDate)
	            .append(surveyEmailLanguage,castOther.surveyEmailLanguage)
	            .append(originAgentName,castOther.originAgentName)
	            .append(originAgentContact,castOther.originAgentContact)
	            .append(originAgentEmail,castOther.originAgentEmail)
	            .append(priceSubmissionToAccDate,castOther.priceSubmissionToAccDate)
	            .append(originAgentPhoneNumber,castOther.originAgentPhoneNumber)
	            .append(priceSubmissionToTranfDate,castOther.priceSubmissionToTranfDate)
	            .append(priceSubmissionToBookerDate,castOther.priceSubmissionToBookerDate)
	            .append(quoteAcceptenceDate,castOther.quoteAcceptenceDate)
	            .append(INS_vendorCode,castOther.INS_vendorCode).append(INP_vendorCode,castOther.INP_vendorCode).append(EDA_vendorCode,castOther.EDA_vendorCode).append(TAS_vendorCode,castOther.TAS_vendorCode)
	            .append(jimExtract,castOther.jimExtract)
	            .append(supplementGBL,castOther.supplementGBL)
	            .append(npsScore, castOther.npsScore)
	            .append(reptHourReceived, castOther.reptHourReceived)
	            .append(oIOverallProjectview, castOther.oIOverallProjectview).append(quoteStopEmail,castOther.quoteStopEmail)
				.append(revisedDate,castOther.revisedDate).isEquals();

	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(defaultAccountLineStatus).append(
				corpID).append(id).append(customerFileId)
				.append(sequenceNumber).append(ship).append(shipNumber).append(
						controlFlag).append(registrationNumber).append(
						statusDate).append(status).append(job).append(routing)
				.append(serviceType).append(mode).append(packingMode).append(
						payType).append(coordinator).append(salesMan).append(
						prefix).append(firstName).append(lastName).append(mi)
				.append(originAddressLine1).append(suffix).append(
						originAddressLine2).append(originAddressLine3).append(
						originCity).append(originState).append(
						originCountryCode).append(originCountry).append(
						originZip).append(originDayPhone).append(
						originHomePhone).append(originFax).append(
						destinationAddressLine1)
				.append(destinationAddressLine2)
				.append(destinationAddressLine3).append(destinationCity)
				.append(destinationCityCode).append(originCityCode).append(
						destinationState).append(destinationCountryCode)
				.append(destinationCountry).append(destinationZip).append(
						destinationDayPhone).append(destinationHomePhone)
				.append(destinationFax).append(contactName).append(
						contactNameExt).append(originContactEmail).append(
						contactPhone).append(contactFax).append(clientCode)
				.append(clientName).append(clientReference).append(commodity)
				.append(contract).append(socialSecurityNumber)
				.append(consignee).append(destinationLoadSite)
				.append(estimator).append(lastExtra).append(lastItem).append(
						nextCheckOn).append(originContactName).append(
						originContactNameExt).append(destinationContactEmail)
				.append(originContactPhone).append(originContactWork).append(
						originLoadSite).append(orderBy).append(orderPhone)
				.append(suffixNext).append(email).append(originCompany).append(
						destinationCompany).append(originMilitary).append(
						destinationMilitary).append(lastName2).append(firstName2)
				.append(prefix2).append(initial2).append(andor).append(
						billToCode).append(billToName).append(originDayExtn)
				.append(destinationDayExtn).append(vip).append(revised).append(
						unit1).append(companyDivision).append(portOfLading)
				.append(portOfEntry).append(originArea).append(destinationArea)
				.append(tripNumber).append(email2).append(originContactExtn)
				.append(destinationContactExtn).append(createdBy).append(
						createdOn).append(emailSent).append(emailRecvd)
						.append(updatedBy).append(
						updatedOn).append(quoteStatus).append(
						estimateGrossWeight).append(actualGrossWeight).append(
						estimatedNetWeight).append(actualNetWeight).append(
						entitleNumberAuto).append(entitleBoatYN).append(
						estimateAuto).append(estimateBoat).append(actualAuto)
				.append(actualBoat).append(estimateCubicFeet).append(
						actualCubicFeet).append(originMobile).append(
						destinationMobile).append(unit2).append(equipment)
				.append(statusReason).append(bookingAgentCode).append(
						bookingAgentName).append(entitledTotalAmount).append(
						estimatedTotalExpense).append(estimatedTotalRevenue)
				.append(distributedTotalAmount).append(
						projectedDistributedTotalAmount).append(
						estimatedGrossMargin).append(
						estimatedGrossMarginPercentage).append(
						revisedTotalExpense).append(revisedTotalRevenue)
				.append(revisedGrossMargin).append(originContactLastName)
				.append(contactLastName)
				.append(revisedGrossMarginPercentage).append(actualExpense)
				.append(projectedActualExpense).append(actualRevenue).append(
						projectedActualRevenue).append(actualGrossMargin)
				.append(actualGrossMarginPercentage).append(customerFeedback)
				.append(clientOverallResponse).append(OAEvaluation).append(
						DAEvaluation).append(destinationEmail).append(
						destinationEmail2).append(statusNumber).append(
						bookingAgentCheckedBy).append(carrierDeparture).append(
						ETD).append(ATD).append(carrierArrival).append(ETA)
				.append(ATA).append(brokerCode).append(originAgentCode).append(
						originSubAgentCode).append(destinationAgentCode)
				.append(destinationSubAgentCode).append(vendorCode).append(
						forwarderCode).append(rank).append(feedBack).append(
						inlandCode).append(transitDays).append(distance)
				.append(distanceInKmMiles).append(gbl).append(quoteAccept)
				.append(bookingAgentVanlineCode).append(isNetworkRecord)
				.append(bookingAgentShipNumber).append(redskyBillDate).append(
						grpPref).append(grpDeliveryDate).append(grpStatus)
				.append(grpID).append(networkSO).append(packingService).append(packingService).append(est).append(sentDate)
				.append(returnDate).append(qualitySurveyExecutedBy).append(preMoveSurvey).append(originServices)
				.append(destinationServices).append(overAll).append(recommendation).append(general).append(transferee)
				.append(internal)
				.append(shipmentType)
				.append(originPreferredContactTime)
				.append(destPreferredContactTime)
				.append(ugwIntId)
				.append(mmCounselor)
				.append(noOfEmailsSent)
				.append(projectedGrossMargin)
				.append(projectedGrossMarginPercentage)
				.append(serviceOrderFlagCarrier)
				.append(CAR_vendorCode)
	            .append(COL_vendorCode)
	            .append(TRG_vendorCode)
	            .append(HOM_vendorCode)
	            .append(RNT_vendorCode)
	            .append(SET_vendorCode)
	            .append(LAN_vendorCode)
	            .append(MMG_vendorCode)
	            .append(ONG_vendorCode)
	            .append(PRV_vendorCode)
	            .append(AIO_vendorCode)
	            .append(EXP_vendorCode)
	            .append(RPT_vendorCode)
	            .append(SCH_schoolSelected)
	            .append(TAX_vendorCode)
	            .append(TAC_vendorCode)
	            .append(TEN_vendorCode)
	            .append(VIS_vendorCode)
	            .append(customerRating)
	            .append(incExcServiceType)
	            .append(incExcServiceTypeLanguage)
	            .append(WOP_vendorCode)
	            .append(REP_vendorCode)
	            .append(RLS_vendorCode)
	            .append(CAT_vendorCode)
		        .append(CLS_vendorCode)
		        .append(CHS_vendorCode)
		        .append(DPS_vendorCode)
		        .append(HSM_vendorCode)
		        .append(PDT_vendorCode)
		        .append(RCP_vendorCode)
		        .append(SPA_vendorCode)
		        .append(TCS_vendorCode)
		        .append(MTS_vendorCode)
		        .append(DSS_vendorCode)
		        .append(quoteAcceptReason)
		        .append(hsrgSoInsertRelovision)
		        .append(moveType)
		        .append(surveyorEvaluation)
		        .append(coordinatorEvaluation)
		        .append(projectManager)
		        .append(HOB_vendorCode)
	            .append(FLB_vendorCode)
	            .append(oaCoordinatorEval)
	            .append(daCoordinatorEval)
	            .append(isUpdater).append(opsPerson).append(clientId)
	            .append(FRL_vendorCode).append(APU_vendorCode)
	            .append(accrualReadyUpdatedBy)
	            .append(accrualReadyDate).append(incExcDocTypeLanguage)
	            .append(incExcDocServiceType)
	            .append(recInvoiceNumber)
	            .append(additionalAssistance)
	            .append(accrueChecked).append(estimateOverallMarkupPercentage)
	            .append(surveyCoordinator)
	            .append(updaterUpdatedOn).append(welcomeMailSentOn).append(packingDays)
	            .append(importPaperOn).append(thirdPartyServiceRequired)
	            .append(quoteNumber)
	            .append(survey)
	            .append(surveyTime)
	            .append(surveyTime2)
	            .append(actualSurveyDate)
	            .append(surveyEmailLanguage)
	            .append(originAgentName)
	            .append(originAgentContact)
	            .append(originAgentEmail)
	            .append(originAgentPhoneNumber)
	            .append(priceSubmissionToAccDate)
	            .append(priceSubmissionToTranfDate)
	            .append(priceSubmissionToBookerDate)
	            .append(quoteAcceptenceDate)
	            .append(INS_vendorCode).append(INP_vendorCode).append(EDA_vendorCode).append(TAS_vendorCode)                 
	            .append(jimExtract)
	            .append(supplementGBL)
	            .append(npsScore)
	            .append(reptHourReceived)
	            .append(oIOverallProjectview).append(quoteStopEmail)
	            .append(revisedDate).toHashCode();
	}


	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Claim> getClaims() {
		return claims;
	}

	public void setClaims(Set<Claim> claims) {
		this.claims = claims;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Costing> getCostings() {
		return costings;
	}

	public void setCostings(Set<Costing> costings) {
		this.costings = costings;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Container> getContainers() {
		return containers;
	}

	public void setContainers(Set<Container> containers) {
		this.containers = containers;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Carton> getCartons() {
		return cartons;
	}

	public void setCartons(Set<Carton> cartons) {
		this.cartons = cartons;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<WorkTicket> getWorkTickets() {
		return workTickets;
	}

	public void setWorkTickets(Set<WorkTicket> workTickets) {
		this.workTickets = workTickets;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<AccountLine> getAccountLines() {
		return accountLines;
	}

	public void setAccountLines(Set<AccountLine> accountLines) {
		this.accountLines = accountLines;
	}

	@ManyToOne
	@JoinColumn(name = "customerFileId", nullable = false, updatable = false, referencedColumnName = "id")
	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<ServicePartner> getServicePartners() {
		return servicePartners;
	}

	public void setServicePartners(Set<ServicePartner> servicePartners) {
		this.servicePartners = servicePartners;
	}
	
	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Custom> getCustoms() {
		return customs;
	}

	public void setCustoms(Set<Custom> customs) {
		this.customs = customs;
	}

	@Column(length = 1)
	public String getAndor() {
		return andor;
	}

	public void setAndor(String andor) {
		this.andor = andor;
	}

	

	@Column(length=500)
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Column(length = 10)
	public String getSuffixNext() {
		return suffixNext;
	}

	public void setSuffixNext(String suffixNext) {
		this.suffixNext = suffixNext;
	}

	
	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	@Column(length = 2)
	public String getShip() {
		return ship;
	}

	public void setShip(String ship) {
		this.ship = ship;
	}

	@Column(length = 20)
	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	@Column(length = 10)
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@Column(length = 9)
	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	@Column(length = 35)
	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	@Column(length = 10)
	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	@Column(length = 5)
	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	@Column(length = 40)
	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	@Column(length = 20)
	public String getContactFax() {
		return contactFax;
	}

	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	@Column(length = 20)
	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	@Column(length = 100)
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	@Column(length = 100)
	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	@Column(length = 280)
	public String getDestinationCompany() {
		return destinationCompany;
	}

	public void setDestinationCompany(String destinationCompany) {
		this.destinationCompany = destinationCompany;
	}

	@Column(length = 45)
	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	@Column(length = 3)
	public String getDestinationCountryCode() {
		return destinationCountryCode;
	}

	public void setDestinationCountryCode(String destinationCountryCode) {
		this.destinationCountryCode = destinationCountryCode;
	}

	

	@Column(length = 20)
	public String getDestinationDayPhone() {
		return destinationDayPhone;
	}

	public void setDestinationDayPhone(String destinationDayPhone) {
		this.destinationDayPhone = destinationDayPhone;
	}

	@Column(length = 20)
	public String getDestinationFax() {
		return destinationFax;
	}

	public void setDestinationFax(String destinationFax) {
		this.destinationFax = destinationFax;
	}

	@Column(length = 20)
	public String getDestinationHomePhone() {
		return destinationHomePhone;
	}

	public void setDestinationHomePhone(String destinationHomePhone) {
		this.destinationHomePhone = destinationHomePhone;
	}

	@Column(length = 7)
	public String getDestinationLoadSite() {
		return destinationLoadSite;
	}

	public void setDestinationLoadSite(String destinationLoadSite) {
		this.destinationLoadSite = destinationLoadSite;
	}

	@Column(length = 8)
	public String getDestinationMilitary() {
		return destinationMilitary;
	}

	public void setDestinationMilitary(String destinationMilitary) {
		this.destinationMilitary = destinationMilitary;
	}

	@Column(length = 2)
	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	@Column(length = 10)
	public String getDestinationZip() {
		return destinationZip;
	}

	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}

	@Column(length = 65)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 82)
	public String getEstimator() {
		return estimator;
	}

	public void setEstimator(String estimator) {
		this.estimator = estimator;
	}

	@Column(length = 80)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(length = 30)
	public String getFirstName2() {
		return firstName2;
	}

	public void setFirstName2(String firstName2) {
		this.firstName2 = firstName2;
	}

	@Column(length = 1)
	public String getInitial2() {
		return initial2;
	}

	public void setInitial2(String initial2) {
		this.initial2 = initial2;
	}

	@Column(length = 3)
	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	@Column(length = 80)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(length = 30)
	public String getLastName2() {
		return lastName2;
	}

	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}

	@Column(length = 1)
	public String getMi() {
		return mi;
	}

	public void setMi(String mi) {
		this.mi = mi;
	}

	@Column(length = 20)
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Column(length = 30)
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	@Column(length = 20)
	public String getOrderPhone() {
		return orderPhone;
	}

	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}

	@Column(length = 100)
	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	@Column(length = 280)
	public String getOriginCompany() {
		return originCompany;
	}

	public void setOriginCompany(String originCompany) {
		this.originCompany = originCompany;
	}

	@Column(length = 35)
	public String getOriginContactName() {
		return originContactName;
	}

	public void setOriginContactName(String originContactName) {
		this.originContactName = originContactName;
	}

	@Column(length = 20)
	public String getOriginContactPhone() {
		return originContactPhone;
	}

	public void setOriginContactPhone(String originContactPhone) {
		this.originContactPhone = originContactPhone;
	}

	@Column(length = 45)
	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	@Column(length = 3)
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}

	

	@Column(length = 5)
	public Integer getLastExtra() {
		return lastExtra;
	}

	public void setLastExtra(Integer lastExtra) {
		this.lastExtra = lastExtra;
	}

	@Column(length = 20)
	public double getLastItem() {
		return lastItem;
	}

	public void setLastItem(double lastItem) {
		this.lastItem = lastItem;
	}

	@Column(length = 20)
	public String getOriginDayPhone() {
		return originDayPhone;
	}

	public void setOriginDayPhone(String originDayPhone) {
		this.originDayPhone = originDayPhone;
	}

	@Column(length = 20)
	public String getOriginFax() {
		return originFax;
	}

	public void setOriginFax(String originFax) {
		this.originFax = originFax;
	}

	@Column(length = 20)
	public String getOriginHomePhone() {
		return originHomePhone;
	}

	public void setOriginHomePhone(String originHomePhone) {
		this.originHomePhone = originHomePhone;
	}

	@Column(length = 7)
	public String getOriginLoadSite() {
		return originLoadSite;
	}

	public void setOriginLoadSite(String originLoadSite) {
		this.originLoadSite = originLoadSite;
	}

	@Column(length = 8)
	public String getOriginMilitary() {
		return originMilitary;
	}

	public void setOriginMilitary(String originMilitary) {
		this.originMilitary = originMilitary;
	}

	@Column(length = 2)
	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	@Column(length = 10)
	public String getOriginZip() {
		return originZip;
	}

	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}

	@Column(length = 15)
	public String getPackingMode() {
		return packingMode;
	}

	public void setPackingMode(String packingMode) {
		this.packingMode = packingMode;
	}

	@Column(length = 5)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@Column(length = 15)
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Column(length = 10)
	public String getPrefix2() {
		return prefix2;
	}

	public void setPrefix2(String prefix2) {
		this.prefix2 = prefix2;
	}

	@Column(length = 20)
	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	@Column(length = 5)
	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	@Column(length = 20)
	public String getSalesMan() {
		return salesMan;
	}

	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	@Column
	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	@Column(length = 6)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(length = 8)
	public String getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

	@Column(length = 35)
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	@Column(length = 82)
	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 50)
	public String getDestinationAddressLine1() {
		return destinationAddressLine1;
	}

	public void setDestinationAddressLine1(String destinationAddressLine1) {
		this.destinationAddressLine1 = destinationAddressLine1;
	}

	@Column(length = 50)
	public String getDestinationAddressLine2() {
		return destinationAddressLine2;
	}

	public void setDestinationAddressLine2(String destinationAddressLine2) {
		this.destinationAddressLine2 = destinationAddressLine2;
	}

	@Column(length = 50)
	public String getDestinationAddressLine3() {
		return destinationAddressLine3;
	}

	public void setDestinationAddressLine3(String destinationAddressLine3) {
		this.destinationAddressLine3 = destinationAddressLine3;
	}

	@Column
	public Date getNextCheckOn() {
		return nextCheckOn;
	}

	public void setNextCheckOn(Date nextCheckOn) {
		this.nextCheckOn = nextCheckOn;
	}

	@Column(length = 50)
	public String getOriginAddressLine1() {
		return originAddressLine1;
	}

	public void setOriginAddressLine1(String originAddressLine1) {
		this.originAddressLine1 = originAddressLine1;
	}

	@Column(length = 50)
	public String getOriginAddressLine2() {
		return originAddressLine2;
	}

	public void setOriginAddressLine2(String originAddressLine2) {
		this.originAddressLine2 = originAddressLine2;
	}

	@Column(length = 50)
	public String getOriginAddressLine3() {
		return originAddressLine3;
	}

	public void setOriginAddressLine3(String originAddressLine3) {
		this.originAddressLine3 = originAddressLine3;
	}

	@Column(length = 20)
	public String getOriginContactWork() {
		return originContactWork;
	}

	public void setOriginContactWork(String originContactWork) {
		this.originContactWork = originContactWork;
	}

	@Column(length = 30)
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 3)
	public String getDestinationArea() {
		return destinationArea;
	}

	public void setDestinationArea(String destinationArea) {
		this.destinationArea = destinationArea;
	}

	@Column(length = 65)
	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	@Column(length = 3)
	public String getOriginArea() {
		return originArea;
	}

	public void setOriginArea(String originArea) {
		this.originArea = originArea;
	}

	@Column(length = 5)
	public String getDestinationContactExtn() {
		return destinationContactExtn;
	}

	public void setDestinationContactExtn(String destinationContactExtn) {
		this.destinationContactExtn = destinationContactExtn;
	}

	@Column(length = 5)
	public String getDestinationDayExtn() {
		return destinationDayExtn;
	}

	public void setDestinationDayExtn(String destinationDayExtn) {
		this.destinationDayExtn = destinationDayExtn;
	}

	@Column(length = 5)
	public String getOriginContactExtn() {
		return originContactExtn;
	}

	public void setOriginContactExtn(String originContactExtn) {
		this.originContactExtn = originContactExtn;
	}

	@Column(length = 5)
	public String getOriginDayExtn() {
		return originDayExtn;
	}

	public void setOriginDayExtn(String originDayExtn) {
		this.originDayExtn = originDayExtn;
	}

	@Column(length = 10)
	public String getTripNumber() {
		return tripNumber;
	}

	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}

	@Column
	public Boolean getVip() {
		return vip;
	}

	public void setVip(Boolean vip) {
		this.vip = vip;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 65)
	public String getDestinationEmail() {
		return destinationEmail;
	}

	public void setDestinationEmail(String destinationEmail) {
		this.destinationEmail = destinationEmail;
	}

	@Column(length = 65)
	public String getDestinationEmail2() {
		return destinationEmail2;
	}

	public void setDestinationEmail2(String destinationEmail2) {
		this.destinationEmail2 = destinationEmail2;
	}

	@Column(length = 50)
	public String getQuoteStatus() {
		return quoteStatus;
	}

	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}

	@Column(length = 255)
	public String getBillToName() {
		return billToName;
	}

	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}

	@Column(length = 5)
	public Integer getActualAuto() {
		return actualAuto;
	}

	public void setActualAuto(Integer actualAuto) {
		this.actualAuto = actualAuto;
	}

	@Column(length = 5)
	public Integer getActualBoat() {
		return actualBoat;
	}

	public void setActualBoat(Integer actualBoat) {
		this.actualBoat = actualBoat;
	}

	@Column(length = 20)
	public BigDecimal getActualCubicFeet() {
		return actualCubicFeet;
	}

	public void setActualCubicFeet(BigDecimal actualCubicFeet) {
		this.actualCubicFeet = actualCubicFeet;
	}

	@Column(length = 20)
	public BigDecimal getActualGrossWeight() {
		return actualGrossWeight;
	}

	public void setActualGrossWeight(BigDecimal actualGrossWeight) {
		this.actualGrossWeight = actualGrossWeight;
	}

	@Column(length = 20)
	public BigDecimal getActualNetWeight() {
		return actualNetWeight;
	}

	public void setActualNetWeight(BigDecimal actualNetWeight) {
		this.actualNetWeight = actualNetWeight;
	}

	@Column(length = 5)
	public Integer getEntitleBoatYN() {
		return entitleBoatYN;
	}

	public void setEntitleBoatYN(Integer entitleBoatYN) {
		this.entitleBoatYN = entitleBoatYN;
	}

	@Column(length = 5)
	public Integer getEntitleNumberAuto() {
		return entitleNumberAuto;
	}

	public void setEntitleNumberAuto(Integer entitleNumberAuto) {
		this.entitleNumberAuto = entitleNumberAuto;
	}

	@Column(length = 5)
	public Integer getEstimateAuto() {
		return estimateAuto;
	}

	public void setEstimateAuto(Integer estimateAuto) {
		this.estimateAuto = estimateAuto;
	}

	@Column(length = 5)
	public Integer getEstimateBoat() {
		return estimateBoat;
	}

	public void setEstimateBoat(Integer estimateBoat) {
		this.estimateBoat = estimateBoat;
	}

	@Column(length = 20)
	public BigDecimal getEstimateCubicFeet() {
		return estimateCubicFeet;
	}

	public void setEstimateCubicFeet(BigDecimal estimateCubicFeet) {
		this.estimateCubicFeet = estimateCubicFeet;
	}

	@Column(length = 20)
	public BigDecimal getEstimatedNetWeight() {
		return estimatedNetWeight;
	}

	public void setEstimatedNetWeight(BigDecimal estimatedNetWeight) {
		this.estimatedNetWeight = estimatedNetWeight;
	}

	@Column(length = 20)
	public BigDecimal getEstimateGrossWeight() {
		return estimateGrossWeight;
	}

	public void setEstimateGrossWeight(BigDecimal estimateGrossWeight) {
		this.estimateGrossWeight = estimateGrossWeight;
	}

	@Column(length = 10)
	public String getUnit1() {
		return unit1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}

	@Column(length = 10)
	public String getUnit2() {
		return unit2;
	}

	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}

	@Column(length = 20)
	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	@Column(length = 25)
	public String getDestinationMobile() {
		return destinationMobile;
	}

	public void setDestinationMobile(String destinationMobile) {
		this.destinationMobile = destinationMobile;
	}

	@Column(length = 25)
	public String getOriginMobile() {
		return originMobile;
	}

	public void setOriginMobile(String originMobile) {
		this.originMobile = originMobile;
	}

	@Column(length = 75)
	public String getDestinationCityCode() {
		return destinationCityCode;
	}

	public void setDestinationCityCode(String destinationCityCode) {
		this.destinationCityCode = destinationCityCode;
	}

	@Column(length = 75)
	public String getOriginCityCode() {
		return originCityCode;
	}

	public void setOriginCityCode(String originCityCode) {
		this.originCityCode = originCityCode;
	}

	@Column(length = 5)
	public Integer getStatusNumber() {
		return statusNumber;
	}

	public void setStatusNumber(Integer statusNumber) {
		this.statusNumber = statusNumber;
	}

	@Column
	public BigDecimal getActualGrossMargin() {
		return actualGrossMargin;
	}

	public void setActualGrossMargin(BigDecimal actualGrossMargin) {
		this.actualGrossMargin = actualGrossMargin;
	}

	@Column
	public BigDecimal getActualGrossMarginPercentage() {
		return actualGrossMarginPercentage;
	}

	public void setActualGrossMarginPercentage(
			BigDecimal actualGrossMarginPercentage) {
		this.actualGrossMarginPercentage = actualGrossMarginPercentage;
	}

	@Column
	public BigDecimal getEstimatedGrossMarginPercentage() {
		return estimatedGrossMarginPercentage;
	}

	public void setEstimatedGrossMarginPercentage(
			BigDecimal estimatedGrossMarginPercentage) {
		this.estimatedGrossMarginPercentage = estimatedGrossMarginPercentage;
	}

	@Column
	public BigDecimal getRevisedGrossMargin() {
		return revisedGrossMargin;
	}

	public void setRevisedGrossMargin(BigDecimal revisedGrossMargin) {
		this.revisedGrossMargin = revisedGrossMargin;
	}

	@Column
	public BigDecimal getRevisedGrossMarginPercentage() {
		return revisedGrossMarginPercentage;
	}

	public void setRevisedGrossMarginPercentage(
			BigDecimal revisedGrossMarginPercentage) {
		this.revisedGrossMarginPercentage = revisedGrossMarginPercentage;
	}

	@Column
	public BigDecimal getEntitledTotalAmount() {
		return entitledTotalAmount;
	}

	public void setEntitledTotalAmount(BigDecimal entitledTotalAmount) {
		this.entitledTotalAmount = entitledTotalAmount;
	}

	@Column
	public BigDecimal getActualExpense() {
		return actualExpense;
	}

	public void setActualExpense(BigDecimal actualExpense) {
		this.actualExpense = actualExpense;
	}

	@Column
	public BigDecimal getActualRevenue() {
		return actualRevenue;
	}

	public void setActualRevenue(BigDecimal actualRevenue) {
		this.actualRevenue = actualRevenue;
	}

	@Column
	public BigDecimal getEstimatedTotalExpense() {
		return estimatedTotalExpense;
	}

	public void setEstimatedTotalExpense(BigDecimal estimatedTotalExpense) {
		this.estimatedTotalExpense = estimatedTotalExpense;
	}

	@Column
	public BigDecimal getEstimatedTotalRevenue() {
		return estimatedTotalRevenue;
	}

	public void setEstimatedTotalRevenue(BigDecimal estimatedTotalRevenue) {
		this.estimatedTotalRevenue = estimatedTotalRevenue;
	}

	@Column
	public BigDecimal getRevisedTotalExpense() {
		return revisedTotalExpense;
	}

	public void setRevisedTotalExpense(BigDecimal revisedTotalExpense) {
		this.revisedTotalExpense = revisedTotalExpense;
	}

	@Column
	public BigDecimal getRevisedTotalRevenue() {
		return revisedTotalRevenue;
	}

	public void setRevisedTotalRevenue(BigDecimal revisedTotalRevenue) {
		this.revisedTotalRevenue = revisedTotalRevenue;
	}

	@Column
	public BigDecimal getEstimatedGrossMargin() {
		return estimatedGrossMargin;
	}

	public void setEstimatedGrossMargin(BigDecimal estimatedGrossMargin) {
		this.estimatedGrossMargin = estimatedGrossMargin;
	}
	@Column(length=8)
	public String getBookingAgentCode() {
		return bookingAgentCode;
	}

	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}
	@Column(length=255)
	public String getBookingAgentName() {
		return bookingAgentName;
	}

	public void setBookingAgentName(String bookingAgentName) {
		this.bookingAgentName = bookingAgentName;
	}
	@Column(length=100)
	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public boolean isDefaultAccountLineStatus() {
		return defaultAccountLineStatus;
	}

	public void setDefaultAccountLineStatus(boolean defaultAccountLineStatus) {
		this.defaultAccountLineStatus = defaultAccountLineStatus;
	}
	@Column(length=3)
	public String getCustomerFeedback() {
		return customerFeedback;
	}
  
	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}
	@Column
	public BigDecimal getDistributedTotalAmount() {
		return distributedTotalAmount;
	}

	public void setDistributedTotalAmount(BigDecimal distributedTotalAmount) {
		this.distributedTotalAmount = distributedTotalAmount;
	}
	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	@Column(length=50)
	public String getPortOfEntry() {
		return portOfEntry;
	}

	public void setPortOfEntry(String portOfEntry) {
		this.portOfEntry = portOfEntry;
	}
	@Column(length=50)
	public String getPortOfLading() {
		return portOfLading;
	}

	public void setPortOfLading(String portOfLading) {
		this.portOfLading = portOfLading;
	}
	@Column(length=4)
	public String getControlFlag() {
		return controlFlag;
	}

	public void setControlFlag(String controlFlag) {
		this.controlFlag = controlFlag;
	}

	/**
	 * @return the contactNameExt
	 */
	@Column(length=5)
	public String getContactNameExt() {
		return contactNameExt;
	}

	/**
	 * @param contactNameExt the contactNameExt to set
	 */
	public void setContactNameExt(String contactNameExt) {
		this.contactNameExt = contactNameExt;
	}

	/**
	 * @return the originContactNameExt
	 */
	@Column(length=5)
	public String getOriginContactNameExt() {
		return originContactNameExt;
	}

	/**
	 * @param originContactNameExt the originContactNameExt to set
	 */
	public void setOriginContactNameExt(String originContactNameExt) {
		this.originContactNameExt = originContactNameExt;
	}
	@Column(length = 15, insertable = false, updatable = false)
	public Long getCustomerFileId() {
		return customerFileId;
	}
	
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}

	/**
	 * @return the destinationContactEmail
	 */
	@Column(length = 65)
	public String getDestinationContactEmail() {
		return destinationContactEmail;
	}

	/**
	 * @param destinationContactEmail the destinationContactEmail to set
	 */
	public void setDestinationContactEmail(String destinationContactEmail) {
		this.destinationContactEmail = destinationContactEmail;
	}

	/**
	 * @return the originContactEmail
	 */
	@Column(length = 65)
	public String getOriginContactEmail() {
		return originContactEmail;
	}

	/**
	 * @param originContactEmail the originContactEmail to set
	 */
	public void setOriginContactEmail(String originContactEmail) {
		this.originContactEmail = originContactEmail;
	}

	
	/**
	 * @return the emailSent
	 */
	@Column
	public Date getEmailSent() {
		return emailSent;
	}

	/**
	 * @param emailSent the emailSent to set
	 */
	public void setEmailSent(Date emailSent) {
		this.emailSent = emailSent;
	}
	@Column(length = 25)
	public String getBookingAgentCheckedBy() {
		return bookingAgentCheckedBy;
	}

	public void setBookingAgentCheckedBy(String bookingAgentCheckedBy) {
		this.bookingAgentCheckedBy = bookingAgentCheckedBy;
	}

	/**
	 * @return the revised
	 */
	@Column
	public Boolean getRevised() {
		return revised;
	}

	/**
	 * @param revised the revised to set
	 */
	public void setRevised(Boolean revised) {
		this.revised = revised;
	}

	/**
	 * @return the aTA
	 */
	@Column
	public Date getATA() {
		return ATA;
	}

	/**
	 * @param ata the aTA to set
	 */
	public void setATA(Date ata) {
		ATA = ata;
	}

	/**
	 * @return the aTD
	 */
	@Column
	public Date getATD() {
		return ATD;
	}

	/**
	 * @param atd the aTD to set
	 */
	public void setATD(Date atd) {
		ATD = atd;
	}

	
	/**
	 * @return the eTA
	 */
	@Column
	public Date getETA() {
		return ETA;
	}

	/**
	 * @param eta the eTA to set
	 */
	public void setETA(Date eta) {
		ETA = eta;
	}

	/**
	 * @return the eTD
	 */
	@Column
	public Date getETD() {
		return ETD;
	}

	/**
	 * @param etd the eTD to set
	 */
	public void setETD(Date etd) {
		ETD = etd;
	}

	/**
	 * @return the carrierArrival
	 */
	@Column(length = 200)
	public String getCarrierArrival() {
		return carrierArrival;
	}

	/**
	 * @param carrierArrival the carrierArrival to set
	 */
	public void setCarrierArrival(String carrierArrival) {
		this.carrierArrival = carrierArrival;
	}

	/**
	 * @return the carrierDeparture
	 */
	@Column(length = 200)
	public String getCarrierDeparture() {
		return carrierDeparture;
	}

	/**
	 * @param carrierDeparture the carrierDeparture to set
	 */
	public void setCarrierDeparture(String carrierDeparture) {
		this.carrierDeparture = carrierDeparture;
	}
	@Column(length=25)
	public String getBrokerCode() {
		return brokerCode;
	}

	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	@Column(length=25)
	public String getDestinationAgentCode() {
		return destinationAgentCode;
	}

	public void setDestinationAgentCode(String destinationAgentCode) {
		this.destinationAgentCode = destinationAgentCode;
	}
	@Column(length=25)
	public String getDestinationSubAgentCode() {
		return destinationSubAgentCode;
	}

	public void setDestinationSubAgentCode(String destinationSubAgentCode) {
		this.destinationSubAgentCode = destinationSubAgentCode;
	}
	@Column(length=25)
	public String getOriginAgentCode() {
		return originAgentCode;
	}

	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}
	@Column(length=25)
	public String getOriginSubAgentCode() {
		return originSubAgentCode;
	}

	public void setOriginSubAgentCode(String originSubAgentCode) {
		this.originSubAgentCode = originSubAgentCode;
	}
	@Column(length=25)
	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getForwarderCode() {
		return forwarderCode;
	}

	public void setForwarderCode(String forwarderCode) {
		this.forwarderCode = forwarderCode;
	}
	@Column(length=3)
	public String getDAEvaluation() {
		return DAEvaluation;
	}

	public void setDAEvaluation(String evaluation) {
		DAEvaluation = evaluation;
	}
	@Column(length=3)
	public String getOAEvaluation() {
		return OAEvaluation;
	}

	public void setOAEvaluation(String evaluation) {
		OAEvaluation = evaluation;
	}
	@Column
	public String getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}

	/**
	 * @return the clientOverallResponse
	 */
	@Column(length=3)
	public String getClientOverallResponse() {
		return clientOverallResponse;
	}

	/**
	 * @param clientOverallResponse the clientOverallResponse to set
	 */
	public void setClientOverallResponse(String clientOverallResponse) {
		this.clientOverallResponse = clientOverallResponse;
	}

	/**
	 * @return the rank
	 */
	@Column(length=7)
	public String getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(String rank) {
		this.rank = rank;
	}
	@Column(length=10)
	public String getInlandCode() {
		return inlandCode;
	}

	public void setInlandCode(String inlandCode) {
		this.inlandCode = inlandCode;
	}

	@Column(length=8)
	public String getBookingAgentVanlineCode() {
		return bookingAgentVanlineCode;
	}

	public void setBookingAgentVanlineCode(String bookingAgentVanlineCode) {
		this.bookingAgentVanlineCode = bookingAgentVanlineCode;
	}
	
	@Column(length = 20)
	public String getGbl() {
		return gbl;
	}

	public void setGbl(String gbl) {
		this.gbl = gbl;
	}

	/**
	 * @return the distance
	 */
	@Column(length=20)
	public BigDecimal getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}

	/**
	 * @return the distanceInKmMiles
	 */
	@Column(length=5)
	public String getDistanceInKmMiles() {
		return distanceInKmMiles;
	}

	/**
	 * @param distanceInKmMiles the distanceInKmMiles to set
	 */
	public void setDistanceInKmMiles(String distanceInKmMiles) {
		this.distanceInKmMiles = distanceInKmMiles;
	}
	
	@Column
	public BigDecimal getProjectedActualExpense() {
		return projectedActualExpense;
	}

	public void setProjectedActualExpense(BigDecimal projectedActualExpense) {
		this.projectedActualExpense = projectedActualExpense;
	}

	@Column
	public BigDecimal getProjectedActualRevenue() {
		return projectedActualRevenue;
	}

	public void setProjectedActualRevenue(BigDecimal projectedActualRevenue) {
		this.projectedActualRevenue = projectedActualRevenue;
	}
	@Column
	public BigDecimal getProjectedDistributedTotalAmount() {
		return projectedDistributedTotalAmount;
	}

	public void setProjectedDistributedTotalAmount(
			BigDecimal projectedDistributedTotalAmount) {
		this.projectedDistributedTotalAmount = projectedDistributedTotalAmount;
	}
	@Column(length=1)
	public String getQuoteAccept() {
		return quoteAccept;
	}

	public void setQuoteAccept(String quoteAccept) {
		this.quoteAccept = quoteAccept;
	}
	
	public Boolean getIsNetworkRecord() {
		return isNetworkRecord;
	}

	public void setIsNetworkRecord(Boolean isNetworkRecord) {
		this.isNetworkRecord = isNetworkRecord;
	}

	public String getBookingAgentShipNumber() {
		return bookingAgentShipNumber;
	}

	public void setBookingAgentShipNumber(String bookingAgentShipNumber) {
		this.bookingAgentShipNumber = bookingAgentShipNumber;
	}
    @Column(length=25)
	public String getTransitDays() {
		return transitDays;
	}

	public void setTransitDays(String transitDays) {
		this.transitDays = transitDays;
	}
	@Column
	public Date getRedskyBillDate() {
		return redskyBillDate;
	}

	public void setRedskyBillDate(Date redskyBillDate) {
		this.redskyBillDate = redskyBillDate;
	}

	public Date getGrpDeliveryDate() {
		return grpDeliveryDate;
	}

	public void setGrpDeliveryDate(Date grpDeliveryDate) {
		this.grpDeliveryDate = grpDeliveryDate;
	}

	public Boolean getGrpPref() {
		return grpPref;
	}

	public void setGrpPref(Boolean grpPref) {
		this.grpPref = grpPref;
	}

	@Column(length=25)
	public String getGrpStatus() {
		return grpStatus;
	}

	public void setGrpStatus(String grpStatus) {
		this.grpStatus = grpStatus;
	}
	@Column(length=8)
	public Long getGrpID() {
		return grpID;
	}

	public void setGrpID(Long grpID) {
		this.grpID = grpID;
	}
	@Column
	public Boolean getNetworkSO() {
		return networkSO;
	}

	public void setNetworkSO(Boolean networkSO) {
		this.networkSO = networkSO;
	}
	@Column
	public String getPackingService() {
		return packingService;
	}

	public void setPackingService(String packingService) {
		this.packingService = packingService;
	} 
	public Boolean getEst() {
		return est;
	}
	public void setEst(Boolean est) {
		this.est = est;
	}
	@Column
	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	@Column
	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	@Column(length=25)
	public String getQualitySurveyExecutedBy() {
		return qualitySurveyExecutedBy;
	}

	public void setQualitySurveyExecutedBy(String qualitySurveyExecutedBy) {
		this.qualitySurveyExecutedBy = qualitySurveyExecutedBy;
	}
	@Column(length=2)
	public String getPreMoveSurvey() {
		return preMoveSurvey;
	}
	
	public void setPreMoveSurvey(String preMoveSurvey) {
		this.preMoveSurvey = preMoveSurvey;
	}
	@Column(length=2)
	public String getOriginServices() {
		return originServices;
	}
	
	public void setOriginServices(String originServices) {
		this.originServices = originServices;
	}
	@Column(length=2)
	public String getDestinationServices() {
		return destinationServices;
	}

	public void setDestinationServices(String destinationServices) {
		this.destinationServices = destinationServices;
	}
	@Column(length=2)
	public String getOverAll() {
		return overAll;
	}

	public void setOverAll(String overAll) {
		this.overAll = overAll;
	}
	@Column(length=2)
	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	@Column
	public String getGeneral() {
		return general;
	}

	public void setGeneral(String general) {
		this.general = general;
	}
	@Column
	public String getTransferee() {
		return transferee;
	}

	public void setTransferee(String transferee) {
		this.transferee = transferee;
	}
	@Column
	public String getInternal() {
		return internal;
	}

	public void setInternal(String internal) {
		this.internal = internal;
	}
	@Column
	public String getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}

	public Date getEmailRecvd() {
		return emailRecvd;
	}

	public void setEmailRecvd(Date emailRecvd) {
		this.emailRecvd = emailRecvd;
	}
	@Column
	public String getOriginPreferredContactTime() {
		return originPreferredContactTime;
	}

	public void setOriginPreferredContactTime(String originPreferredContactTime) {
		this.originPreferredContactTime = originPreferredContactTime;
	}
	
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}

	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}

	
	@Column
	public String getDestPreferredContactTime() {
		return destPreferredContactTime;
	}

	public void setDestPreferredContactTime(String destPreferredContactTime) {
		this.destPreferredContactTime = destPreferredContactTime;
	}

	@Column
	public String getOriginContactLastName() {
		return originContactLastName;
	}

	public void setOriginContactLastName(String originContactLastName) {
		this.originContactLastName = originContactLastName;
	}

	@Column
	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
	/*@OneToMany(mappedBy="serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<CustomerServiceSurvey> getCustomerServiceSurvey() {
		return customerServiceSurvey;
	}

	public void setCustomerServiceSurvey(
			Set<CustomerServiceSurvey> customerServiceSurvey) {
		this.customerServiceSurvey = customerServiceSurvey;
	}*/

	public String getMmCounselor() {
		return mmCounselor;
	}

	public void setMmCounselor(String mmCounselor) {
		this.mmCounselor = mmCounselor;
	}
	@Column
	public Integer getNoOfEmailsSent() {
		return noOfEmailsSent;
	}

	public void setNoOfEmailsSent(Integer noOfEmailsSent) {
		this.noOfEmailsSent = noOfEmailsSent;
	}
	@Column
	public BigDecimal getProjectedGrossMargin() {
		return projectedGrossMargin;
	}

	public void setProjectedGrossMargin(BigDecimal projectedGrossMargin) {
		this.projectedGrossMargin = projectedGrossMargin;
	}
	@Column
	public BigDecimal getProjectedGrossMarginPercentage() {
		return projectedGrossMarginPercentage;
	}

	public void setProjectedGrossMarginPercentage(
			BigDecimal projectedGrossMarginPercentage) {
		this.projectedGrossMarginPercentage = projectedGrossMarginPercentage;
	}
	@Column
	public String getServiceOrderFlagCarrier() {
		return serviceOrderFlagCarrier;
	}

	public void setServiceOrderFlagCarrier(String serviceOrderFlagCarrier) {
		this.serviceOrderFlagCarrier = serviceOrderFlagCarrier;
	}
	@Column
	public String getCAR_vendorCode() {
		return CAR_vendorCode;
	}

	public void setCAR_vendorCode(String cAR_vendorCode) {
		CAR_vendorCode = cAR_vendorCode;
	}
	@Column
	public String getCOL_vendorCode() {
		return COL_vendorCode;
	}

	public void setCOL_vendorCode(String cOL_vendorCode) {
		COL_vendorCode = cOL_vendorCode;
	}
	@Column
	public String getTRG_vendorCode() {
		return TRG_vendorCode;
	}

	public void setTRG_vendorCode(String tRG_vendorCode) {
		TRG_vendorCode = tRG_vendorCode;
	}
	@Column
	public String getHOM_vendorCode() {
		return HOM_vendorCode;
	}

	public void setHOM_vendorCode(String hOM_vendorCode) {
		HOM_vendorCode = hOM_vendorCode;
	}
	@Column
	public String getRNT_vendorCode() {
		return RNT_vendorCode;
	}

	public void setRNT_vendorCode(String rNT_vendorCode) {
		RNT_vendorCode = rNT_vendorCode;
	}
	@Column
	public String getSET_vendorCode() {
		return SET_vendorCode;
	}

	public void setSET_vendorCode(String sET_vendorCode) {
		SET_vendorCode = sET_vendorCode;
	}
	@Column
	public String getLAN_vendorCode() {
		return LAN_vendorCode;
	}

	public void setLAN_vendorCode(String lAN_vendorCode) {
		LAN_vendorCode = lAN_vendorCode;
	}
	@Column
	public String getMMG_vendorCode() {
		return MMG_vendorCode;
	}

	public void setMMG_vendorCode(String mMG_vendorCode) {
		MMG_vendorCode = mMG_vendorCode;
	}
	@Column
	public String getONG_vendorCode() {
		return ONG_vendorCode;
	}

	public void setONG_vendorCode(String oNG_vendorCode) {
		ONG_vendorCode = oNG_vendorCode;
	}
	@Column
	public String getPRV_vendorCode() {
		return PRV_vendorCode;
	}

	public void setPRV_vendorCode(String pRV_vendorCode) {
		PRV_vendorCode = pRV_vendorCode;
	}
	@Column
	public String getAIO_vendorCode() {
		return AIO_vendorCode;
	}

	public void setAIO_vendorCode(String aIO_vendorCode) {
		AIO_vendorCode = aIO_vendorCode;
	}
	@Column
	public String getEXP_vendorCode() {
		return EXP_vendorCode;
	}

	public void setEXP_vendorCode(String eXP_vendorCode) {
		EXP_vendorCode = eXP_vendorCode;
	}
	@Column
	public String getRPT_vendorCode() {
		return RPT_vendorCode;
	}

	public void setRPT_vendorCode(String rPT_vendorCode) {
		RPT_vendorCode = rPT_vendorCode;
	}
	@Column
	public String getSCH_schoolSelected() {
		return SCH_schoolSelected;
	}

	public void setSCH_schoolSelected(String sCH_schoolSelected) {
		SCH_schoolSelected = sCH_schoolSelected;
	}
	@Column
	public String getTAX_vendorCode() {
		return TAX_vendorCode;
	}

	public void setTAX_vendorCode(String tAX_vendorCode) {
		TAX_vendorCode = tAX_vendorCode;
	}
	@Column
	public String getTAC_vendorCode() {
		return TAC_vendorCode;
	}

	public void setTAC_vendorCode(String tAC_vendorCode) {
		TAC_vendorCode = tAC_vendorCode;
	}
	@Column
	public String getTEN_vendorCode() {
		return TEN_vendorCode;
	}

	public void setTEN_vendorCode(String tEN_vendorCode) {
		TEN_vendorCode = tEN_vendorCode;
	}
	@Column
	public String getVIS_vendorCode() {
		return VIS_vendorCode;
	}

	public void setVIS_vendorCode(String vIS_vendorCode) {
		VIS_vendorCode = vIS_vendorCode;
	}
	@Column
	public Integer getCustomerRating() {
		return customerRating;
	}

	public void setCustomerRating(Integer customerRating) {
		this.customerRating = customerRating;
	}
	@Column
	public Boolean getIsSOExtract() {
		return isSOExtract;
	}

	public void setIsSOExtract(Boolean isSOExtract) {
		this.isSOExtract = isSOExtract;
	}
	@Column
	public String getIncExcServiceType() {
		return incExcServiceType;
	}

	public void setIncExcServiceType(String incExcServiceType) {
		this.incExcServiceType = incExcServiceType;
	}
	@Column
	public String getIncExcServiceTypeLanguage() {
		return incExcServiceTypeLanguage;
	}

	public void setIncExcServiceTypeLanguage(String incExcServiceTypeLanguage) {
		this.incExcServiceTypeLanguage = incExcServiceTypeLanguage;
	}
	@Column
	public String getWOP_vendorCode() {
		return WOP_vendorCode;
	}
	@Column
	public String getREP_vendorCode() {
		return REP_vendorCode;
	}

	public void setWOP_vendorCode(String wOP_vendorCode) {
		WOP_vendorCode = wOP_vendorCode;
	}

	public void setREP_vendorCode(String rEP_vendorCode) {
		REP_vendorCode = rEP_vendorCode;
	}
	@Column
	public String getRLS_vendorCode() {
		return RLS_vendorCode;
	}

	public void setRLS_vendorCode(String rLS_vendorCode) {
		RLS_vendorCode = rLS_vendorCode;
	}
	@Column
	public String getCAT_vendorCode() {
		return CAT_vendorCode;
	}

	public void setCAT_vendorCode(String cAT_vendorCode) {
		CAT_vendorCode = cAT_vendorCode;
	}
	@Column
	public String getCLS_vendorCode() {
		return CLS_vendorCode;
	}

	public void setCLS_vendorCode(String cLS_vendorCode) {
		CLS_vendorCode = cLS_vendorCode;
	}
	@Column
	public String getCHS_vendorCode() {
		return CHS_vendorCode;
	}

	public void setCHS_vendorCode(String cHS_vendorCode) {
		CHS_vendorCode = cHS_vendorCode;
	}
	@Column
	public String getDPS_vendorCode() {
		return DPS_vendorCode;
	}

	public void setDPS_vendorCode(String dPS_vendorCode) {
		DPS_vendorCode = dPS_vendorCode;
	}
	@Column
	public String getHSM_vendorCode() {
		return HSM_vendorCode;
	}

	public void setHSM_vendorCode(String hSM_vendorCode) {
		HSM_vendorCode = hSM_vendorCode;
	}
	@Column
	public String getPDT_vendorCode() {
		return PDT_vendorCode;
	}

	public void setPDT_vendorCode(String pDT_vendorCode) {
		PDT_vendorCode = pDT_vendorCode;
	}
	@Column
	public String getRCP_vendorCode() {
		return RCP_vendorCode;
	}

	public void setRCP_vendorCode(String rCP_vendorCode) {
		RCP_vendorCode = rCP_vendorCode;
	}
	@Column
	public String getSPA_vendorCode() {
		return SPA_vendorCode;
	}

	public void setSPA_vendorCode(String sPA_vendorCode) {
		SPA_vendorCode = sPA_vendorCode;
	}
	@Column
	public String getTCS_vendorCode() {
		return TCS_vendorCode;
	}

	public void setTCS_vendorCode(String tCS_vendorCode) {
		TCS_vendorCode = tCS_vendorCode;
	}
	@Column
	public String getMTS_vendorCode() {
		return MTS_vendorCode;
	}

	public void setMTS_vendorCode(String mTS_vendorCode) {
		MTS_vendorCode = mTS_vendorCode;
	}
	@Column
	public String getDSS_vendorCode() {
		return DSS_vendorCode;
	}

	public void setDSS_vendorCode(String dSS_vendorCode) {
		DSS_vendorCode = dSS_vendorCode;
	}

	public String getQuoteAcceptReason() {
		return quoteAcceptReason;
	}

	public void setQuoteAcceptReason(String quoteAcceptReason) {
		this.quoteAcceptReason = quoteAcceptReason;
	}
	@Column
	public Date getHsrgSoInsertRelovision() {
		return hsrgSoInsertRelovision;
	}

	public void setHsrgSoInsertRelovision(Date hsrgSoInsertRelovision) {
		this.hsrgSoInsertRelovision = hsrgSoInsertRelovision;
	}
	@Column
	public String getMoveType() {
		return moveType;
	}

	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}
	@Column
	public String getSurveyorEvaluation() {
		return surveyorEvaluation;
	}

	public void setSurveyorEvaluation(String surveyorEvaluation) {
		this.surveyorEvaluation = surveyorEvaluation;
	}
	@Column
	public String getCoordinatorEvaluation() {
		return coordinatorEvaluation;
	}

	public void setCoordinatorEvaluation(String coordinatorEvaluation) {
		this.coordinatorEvaluation = coordinatorEvaluation;
	}
	@Column
	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
	@Column
	public String getHOB_vendorCode() {
		return HOB_vendorCode;
	}

	public void setHOB_vendorCode(String hOB_vendorCode) {
		HOB_vendorCode = hOB_vendorCode;
	}
	@Column
	public String getFLB_vendorCode() {
		return FLB_vendorCode;
	}

	public void setFLB_vendorCode(String fLB_vendorCode) {
		FLB_vendorCode = fLB_vendorCode;
	}
    @Column
	public String getOaCoordinatorEval() {
		return oaCoordinatorEval;
	}

	public void setOaCoordinatorEval(String oaCoordinatorEval) {
		this.oaCoordinatorEval = oaCoordinatorEval;
	}
    @Column
	public String getDaCoordinatorEval() {
		return daCoordinatorEval;
	}

	public void setDaCoordinatorEval(String daCoordinatorEval) {
		this.daCoordinatorEval = daCoordinatorEval;
	}
	@Column
	public Boolean getIsUpdater() {
		return isUpdater;
	}

	public void setIsUpdater(Boolean isUpdater) {
		this.isUpdater = isUpdater;
	}
	@Column
	public String getOpsPerson() {
		return opsPerson;
	}

	public void setOpsPerson(String opsPerson) {
		this.opsPerson = opsPerson;
	}
	@Column
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	@Column
	public String getFRL_vendorCode() {
		return FRL_vendorCode;
	}

	public void setFRL_vendorCode(String fRL_vendorCode) {
		FRL_vendorCode = fRL_vendorCode;
	}
	@Column
	public String getAPU_vendorCode() {
		return APU_vendorCode;
	}

	public void setAPU_vendorCode(String aPU_vendorCode) {
		APU_vendorCode = aPU_vendorCode;
	}
	@Column
	public String getAccrualReadyUpdatedBy() {
		return accrualReadyUpdatedBy;
	}

	public void setAccrualReadyUpdatedBy(String accrualReadyUpdatedBy) {
		this.accrualReadyUpdatedBy = accrualReadyUpdatedBy;
	}
	@Column
	public Date getAccrualReadyDate() {
		return accrualReadyDate;
	}

	public void setAccrualReadyDate(Date accrualReadyDate) {
		this.accrualReadyDate = accrualReadyDate;
	}

	public String getIncExcDocTypeLanguage() {
		return incExcDocTypeLanguage;
	}

	public void setIncExcDocTypeLanguage(String incExcDocTypeLanguage) {
		this.incExcDocTypeLanguage = incExcDocTypeLanguage;
	}

	public String getIncExcDocServiceType() {
		return incExcDocServiceType;
	}

	public void setIncExcDocServiceType(String incExcDocServiceType) {
		this.incExcDocServiceType = incExcDocServiceType;
	}
	@OneToMany(mappedBy = "serviceOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<InlandAgent> getInlandAgent() {
		return inlandAgent;
	}

	public void setInlandAgent(Set<InlandAgent> inlandAgent) {
		this.inlandAgent = inlandAgent;
	} 
	@Column
	public Boolean getAdditionalAssistance() {
		return additionalAssistance;
	}

	public void setAdditionalAssistance(Boolean additionalAssistance) {
		this.additionalAssistance = additionalAssistance;
	}

	@Column
	public String getRecInvoiceNumber() {
		return recInvoiceNumber;
	}

	public void setRecInvoiceNumber(String recInvoiceNumber) {
		this.recInvoiceNumber = recInvoiceNumber;
	}

	public Date getAccrueChecked() {
		return accrueChecked;
	}

	public void setAccrueChecked(Date accrueChecked) {
		this.accrueChecked = accrueChecked;
	}

	public Integer getEstimateOverallMarkupPercentage() {
		return estimateOverallMarkupPercentage;
	}

	public void setEstimateOverallMarkupPercentage(
			Integer estimateOverallMarkupPercentage) {
		this.estimateOverallMarkupPercentage = estimateOverallMarkupPercentage;
	}
	@Column
	public String getSurveyCoordinator() {
		return surveyCoordinator;
	}

	public void setSurveyCoordinator(String surveyCoordinator) {
		this.surveyCoordinator = surveyCoordinator;
	}
	@Column
	public Date getUpdaterUpdatedOn() {
		return updaterUpdatedOn;
	}

	public void setUpdaterUpdatedOn(Date updaterUpdatedOn) {
		this.updaterUpdatedOn = updaterUpdatedOn;
	}
	@Column
	public Date getWelcomeMailSentOn() {
		return welcomeMailSentOn;
	}

	public void setWelcomeMailSentOn(Date welcomeMailSentOn) {
		this.welcomeMailSentOn = welcomeMailSentOn;
	}
	 @Column(length=25)
	public String getPackingDays() {
		return packingDays;
	}

	public void setPackingDays(String packingDays) {
		this.packingDays = packingDays;
	}

	public Date getImportPaperOn() {
		return importPaperOn;
	}

	public void setImportPaperOn(Date importPaperOn) {
		this.importPaperOn = importPaperOn;
	}
    @Column
	public Boolean getThirdPartyServiceRequired() {
		return thirdPartyServiceRequired;
	}

	public void setThirdPartyServiceRequired(Boolean thirdPartyServiceRequired) {
		this.thirdPartyServiceRequired = thirdPartyServiceRequired;
	}

	public String getQuoteNumber() {
		return quoteNumber;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public Date getSurvey() {
		return survey;
	}

	public void setSurvey(Date survey) {
		this.survey = survey;
	}

	public String getSurveyTime() {
		return surveyTime;
	}

	public void setSurveyTime(String surveyTime) {
		this.surveyTime = surveyTime;
	}

	public String getSurveyTime2() {
		return surveyTime2;
	}

	public void setSurveyTime2(String surveyTime2) {
		this.surveyTime2 = surveyTime2;
	}

	public Date getActualSurveyDate() {
		return actualSurveyDate;
	}

	public void setActualSurveyDate(Date actualSurveyDate) {
		this.actualSurveyDate = actualSurveyDate;
	}

	public String getSurveyEmailLanguage() {
		return surveyEmailLanguage;
	}

	public void setSurveyEmailLanguage(String surveyEmailLanguage) {
		this.surveyEmailLanguage = surveyEmailLanguage;
	}

	public String getOriginAgentName() {
		return originAgentName;
	}

	public void setOriginAgentName(String originAgentName) {
		this.originAgentName = originAgentName;
	}

	public String getOriginAgentContact() {
		return originAgentContact;
	}

	public void setOriginAgentContact(String originAgentContact) {
		this.originAgentContact = originAgentContact;
	}

	public String getOriginAgentPhoneNumber() {
		return originAgentPhoneNumber;
	}

	public void setOriginAgentPhoneNumber(String originAgentPhoneNumber) {
		this.originAgentPhoneNumber = originAgentPhoneNumber;
	}

	public Date getPriceSubmissionToAccDate() {
		return priceSubmissionToAccDate;
	}

	public void setPriceSubmissionToAccDate(Date priceSubmissionToAccDate) {
		this.priceSubmissionToAccDate = priceSubmissionToAccDate;
	}

	public Date getPriceSubmissionToTranfDate() {
		return priceSubmissionToTranfDate;
	}

	public void setPriceSubmissionToTranfDate(Date priceSubmissionToTranfDate) {
		this.priceSubmissionToTranfDate = priceSubmissionToTranfDate;
	}

	public Date getPriceSubmissionToBookerDate() {
		return priceSubmissionToBookerDate;
	}

	public void setPriceSubmissionToBookerDate(Date priceSubmissionToBookerDate) {
		this.priceSubmissionToBookerDate = priceSubmissionToBookerDate;
	}

	public Date getQuoteAcceptenceDate() {
		return quoteAcceptenceDate;
	}

	public void setQuoteAcceptenceDate(Date quoteAcceptenceDate) {
		this.quoteAcceptenceDate = quoteAcceptenceDate;
	}

	public String getOriginAgentEmail() {
		return originAgentEmail;
	}

	public void setOriginAgentEmail(String originAgentEmail) {
		this.originAgentEmail = originAgentEmail;
	}

	public String getINS_vendorCode() {
		return INS_vendorCode;
	}

	public void setINS_vendorCode(String iNS_vendorCode) {
		INS_vendorCode = iNS_vendorCode;
	}

	public String getINP_vendorCode() {
		return INP_vendorCode;
	}

	public void setINP_vendorCode(String iNP_vendorCode) {
		INP_vendorCode = iNP_vendorCode;
	}

	public String getEDA_vendorCode() {
		return EDA_vendorCode;
	}

	public void setEDA_vendorCode(String eDA_vendorCode) {
		EDA_vendorCode = eDA_vendorCode;
	}

	public String getTAS_vendorCode() {
		return TAS_vendorCode;
	}

	public void setTAS_vendorCode(String tAS_vendorCode) {
		TAS_vendorCode = tAS_vendorCode;
	}

	public Boolean getJimExtract() {
		return jimExtract;
	}

	public void setJimExtract(Boolean jimExtract) {
		this.jimExtract = jimExtract;
	}
	public String getSupplementGBL() {
		return supplementGBL;
	}

	public void setSupplementGBL(String supplementGBL) {
		this.supplementGBL = supplementGBL;
	}
    @Column
	public String getNpsScore() {
		return npsScore;
	}

	public void setNpsScore(String npsScore) {
		this.npsScore = npsScore;
	}

	public Date getReptHourReceived() {
		return reptHourReceived;
	}

	public void setReptHourReceived(Date reptHourReceived) {
		this.reptHourReceived = reptHourReceived;
	}
	@Column(length=500)	
    public String getoIOverallProjectview() {
		return oIOverallProjectview;
	}

	public void setoIOverallProjectview(String oIOverallProjectview) {
		this.oIOverallProjectview = oIOverallProjectview;
	}
	@Column
	public Date getQuoteStopEmail() {
		return quoteStopEmail;
	}

	public void setQuoteStopEmail(Date quoteStopEmail) {
		this.quoteStopEmail = quoteStopEmail;
	}

	@Column
	public Date getRevisedDate() {
		return revisedDate;
	}

	public void setRevisedDate(Date revisedDate) {
		this.revisedDate = revisedDate;
	}




}
