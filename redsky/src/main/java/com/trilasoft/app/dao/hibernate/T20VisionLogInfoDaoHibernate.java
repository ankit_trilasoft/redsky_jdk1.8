package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.T20VisionLogInfoDao;
import com.trilasoft.app.model.T20VisionLogInfo;

public class T20VisionLogInfoDaoHibernate extends GenericDaoHibernate<T20VisionLogInfo, Long> implements T20VisionLogInfoDao{

	public T20VisionLogInfoDaoHibernate() {   
        super(T20VisionLogInfo.class);   
    } 
	public List findT20VisionLogList(String sessionCorpID){
		return getHibernateTemplate().find("from T20VisionLogInfo where corpID='"+sessionCorpID+"' order by createdOn desc");
	}
	public List searcht20VisionLogInfoList(String entryNumber,String invoiceNumber,String orderNumber,String orderNumberLine,String status,String sessionCorpID){
		if(invoiceNumber==null){
			invoiceNumber="";
		}
		if(orderNumber==null){
			orderNumber="";
		}
		if(status==null){
			status="";
		}
		String query="from T20VisionLogInfo where  invoiceNumber like '%"+ (invoiceNumber.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and orderNumber like '%"+ (orderNumber.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and orderNumberLine like '%"+ (orderNumberLine.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and status like '%"+ (status.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and corpID='"+sessionCorpID+"' order by createdOn desc";
		if(entryNumber==null){
			query=query+" and entryNumber like '%%'";	
		}else{
			query=query+" and entryNumber like '%"+entryNumber+"%'";	
		}
		if(orderNumberLine==null){
			query=query+" and orderNumberLine like '%%'";
			//query="from T20VisionLogInfo where entryNumber like '%"+ entryNumber+"%' and invoiceNumber like '%"+ (invoiceNumber.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and orderNumber like '%"+ (orderNumber.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and orderNumberLine like '%%' and status like '%"+ (status.toString()).replaceAll("'", "''").replaceAll(":", "''") +"%' and corpID='"+sessionCorpID+"'";		
		}else{
			query=query+" and orderNumberLine like '%"+orderNumberLine+"%'";
		}
		return getHibernateTemplate().find(query);
	}

}
