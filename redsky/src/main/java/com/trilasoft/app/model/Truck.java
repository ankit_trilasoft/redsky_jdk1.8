package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="truck")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class Truck extends BaseObject implements ListLinkData{
	
	//Mandatory field's
	private String corpID;
	private Long id;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String bucket;
	private String description;
	private String gl;
	private String country;
	private Long truckOrder;
	private Long idNum;
	private Long eqIdNum;
	private Long miles;
	private String axle;//new added field for heavy hauling
	private String wheelBase;//new added field for heavy hauling
	private BigDecimal cost = new BigDecimal(0);
	private BigDecimal charge = new BigDecimal(0);
	private BigDecimal otCharge = new BigDecimal(0);
	private BigDecimal dtCharge = new BigDecimal(0);
	private BigDecimal pound = new BigDecimal(0);
	
	private String glType;
	private String type;
	private String vin;
	private String tagNumber;
	private String state;
	private String truckType;
	private String localNumber;
	private String warehouse;
	private String make;
	private String year;
	private String model;//new added field for heavy hauling
	private String driver;
	private String ownerPayTo;//new added field for heavy hauling
	private String accountingType;//new added field for heavy hauling
	private String ownerPayToName;//new added field for heavy hauling
	private String payToTruck;//new added field for heavy hauling
	private String originalCost;//new added field for heavy hauling
	private String fleet;//new added field for heavy hauling
	private Date acquired;//new added field for heavy hauling
	//private String steerDrive1;//new added field for heavy hauling
	//private String drive1Drive2;//new added field for heavy hauling
	//private String drive2Rear;//new added field for heavy hauling
	
	private String trailerKind;//new added field for heavy hauling
	private String gpClass;//new added field for heavy hauling
	
	private String height;//new added field for heavy hauling
	private String weight;//new added field for heavy hauling
	private String width;//new added field for heavy hauling
	private String length;//new added field for heavy hauling
	private String gross;//new added field for heavy hauling
	
	private String kpinToAxle1;//new added field for heavy hauling
	private String axle1ToAxle2;//new added field for heavy hauling
	private String axle2ToAxle3;//new added field for heavy hauling
	private String axle3ToAxle4;//new added field for heavy hauling
	private String axle4ToAxle5;//new added field for heavy hauling
	private String axle5ToAxle6;//new added field for heavy hauling
	private String axle6ToAxle7;//new added field for heavy hauling
	private String axle7ToAxle8;//new added field for heavy hauling
	private String axle8ToAxle9;//new added field for heavy hauling
	private String axle9ToAxle10;//new added field for heavy hauling
	private String axle10ToAxle11;//new added field for heavy hauling	
	private String lastAxleRear;//new added field for heavy hauling
	private String status;//new added field for heavy hauling
	
	private String vlTruckNumber;
	private String docVehicle;
	private String cdlVehicle;
	
	private Boolean isGaraged;
	
	private Date dotInsp;
	private Date vlInsp;
	private Date pmDate;
	private Date milesdate;
	//private Date tagExp;
	//private Date outService;
	private Date inService;//new added field for heavy hauling
	private Date lastStInsp;
	//private Date stInspDue;
	private Date dotInspDue;
	private Date vlInspDue;
	private Date lastVlInsp;
    private String mustdispatch; 
    private String trailertype;
    private String conditionTO; 
    private String truckStatus;
    private Date inactiveDate;
    private BigDecimal capacity= new BigDecimal(0);
    private String agency;
	private String agencyTruckId;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("createdBy", createdBy).append("updatedBy",
				updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("bucket", bucket).append("description",
				description).append("gl", gl).append("country", country)
				.append("truckOrder", truckOrder).append("idNum", idNum)
				.append("eqIdNum", eqIdNum).append("miles", miles).append(
						"axle", axle).append("wheelBase", wheelBase).append(
						"cost", cost).append("charge", charge).append(
						"otCharge", otCharge).append("dtCharge", dtCharge)
				.append("pound", pound).append("glType", glType).append("type",
						type).append("vin", vin).append("tagNumber", tagNumber)
				.append("state", state).append("truckType", truckType).append(
						"localNumber", localNumber).append("warehouse",
						warehouse).append("make", make).append("year", year)
				.append("model", model).append("driver", driver).append(
						"ownerPayTo", ownerPayTo).append("accountingType",
						accountingType)
				.append("ownerPayToName", ownerPayToName).append("payToTruck",
						payToTruck).append("originalCost", originalCost)
				.append("fleet", fleet).append("acquired", acquired).append(
						"trailerKind", trailerKind).append("gpClass", gpClass)
				.append("height", height).append("weight", weight).append(
						"width", width).append("length", length).append(
						"gross", gross).append("kpinToAxle1", kpinToAxle1)
				.append("axle1ToAxle2", axle1ToAxle2).append("axle2ToAxle3",
						axle2ToAxle3).append("axle3ToAxle4", axle3ToAxle4)
				.append("axle4ToAxle5", axle4ToAxle5).append("axle5ToAxle6",
						axle5ToAxle6).append("axle6ToAxle7", axle6ToAxle7)
				.append("axle7ToAxle8", axle7ToAxle8).append("axle8ToAxle9",
						axle8ToAxle9).append("axle9ToAxle10", axle9ToAxle10)
				.append("axle10ToAxle11", axle10ToAxle11).append(
						"lastAxleRear", lastAxleRear).append("status", status)
				.append("vlTruckNumber", vlTruckNumber).append("docVehicle",
						docVehicle).append("cdlVehicle", cdlVehicle).append(
						"isGaraged", isGaraged).append("dotInsp", dotInsp)
				.append("vlInsp", vlInsp).append("pmDate", pmDate).append(
						"milesdate", milesdate).append("inService", inService)
				.append("lastStInsp", lastStInsp).append("dotInspDue",
						dotInspDue).append("vlInspDue", vlInspDue).append(
						"lastVlInsp", lastVlInsp).append("mustdispatch",
						mustdispatch).append("trailertype", trailertype)
				.append("conditionTO", conditionTO).append("truckStatus",
						truckStatus).append("inactiveDate", inactiveDate)
				.append("capacity", capacity).append("agency", agency).append(
						"agencyTruckId", agencyTruckId).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Truck))
			return false;
		Truck castOther = (Truck) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(createdBy, castOther.createdBy).append(
				updatedBy, castOther.updatedBy).append(createdOn,
				castOther.createdOn).append(updatedOn, castOther.updatedOn)
				.append(bucket, castOther.bucket).append(description,
						castOther.description).append(gl, castOther.gl).append(
						country, castOther.country).append(truckOrder,
						castOther.truckOrder).append(idNum, castOther.idNum)
				.append(eqIdNum, castOther.eqIdNum).append(miles,
						castOther.miles).append(axle, castOther.axle).append(
						wheelBase, castOther.wheelBase).append(cost,
						castOther.cost).append(charge, castOther.charge)
				.append(otCharge, castOther.otCharge).append(dtCharge,
						castOther.dtCharge).append(pound, castOther.pound)
				.append(glType, castOther.glType).append(type, castOther.type)
				.append(vin, castOther.vin).append(tagNumber,
						castOther.tagNumber).append(state, castOther.state)
				.append(truckType, castOther.truckType).append(localNumber,
						castOther.localNumber).append(warehouse,
						castOther.warehouse).append(make, castOther.make)
				.append(year, castOther.year).append(model, castOther.model)
				.append(driver, castOther.driver).append(ownerPayTo,
						castOther.ownerPayTo).append(accountingType,
						castOther.accountingType).append(ownerPayToName,
						castOther.ownerPayToName).append(payToTruck,
						castOther.payToTruck).append(originalCost,
						castOther.originalCost).append(fleet, castOther.fleet)
				.append(acquired, castOther.acquired).append(trailerKind,
						castOther.trailerKind).append(gpClass,
						castOther.gpClass).append(height, castOther.height)
				.append(weight, castOther.weight)
				.append(width, castOther.width)
				.append(length, castOther.length)
				.append(gross, castOther.gross).append(kpinToAxle1,
						castOther.kpinToAxle1).append(axle1ToAxle2,
						castOther.axle1ToAxle2).append(axle2ToAxle3,
						castOther.axle2ToAxle3).append(axle3ToAxle4,
						castOther.axle3ToAxle4).append(axle4ToAxle5,
						castOther.axle4ToAxle5).append(axle5ToAxle6,
						castOther.axle5ToAxle6).append(axle6ToAxle7,
						castOther.axle6ToAxle7).append(axle7ToAxle8,
						castOther.axle7ToAxle8).append(axle8ToAxle9,
						castOther.axle8ToAxle9).append(axle9ToAxle10,
						castOther.axle9ToAxle10).append(axle10ToAxle11,
						castOther.axle10ToAxle11).append(lastAxleRear,
						castOther.lastAxleRear)
				.append(status, castOther.status).append(vlTruckNumber,
						castOther.vlTruckNumber).append(docVehicle,
						castOther.docVehicle).append(cdlVehicle,
						castOther.cdlVehicle).append(isGaraged,
						castOther.isGaraged).append(dotInsp, castOther.dotInsp)
				.append(vlInsp, castOther.vlInsp).append(pmDate,
						castOther.pmDate)
				.append(milesdate, castOther.milesdate).append(inService,
						castOther.inService).append(lastStInsp,
						castOther.lastStInsp).append(dotInspDue,
						castOther.dotInspDue).append(vlInspDue,
						castOther.vlInspDue).append(lastVlInsp,
						castOther.lastVlInsp).append(mustdispatch,
						castOther.mustdispatch).append(trailertype,
						castOther.trailertype).append(conditionTO,
						castOther.conditionTO).append(truckStatus,
						castOther.truckStatus).append(inactiveDate,
						castOther.inactiveDate).append(capacity,
						castOther.capacity).append(agency, castOther.agency)
				.append(agencyTruckId, castOther.agencyTruckId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(bucket).append(description)
				.append(gl).append(country).append(truckOrder).append(idNum)
				.append(eqIdNum).append(miles).append(axle).append(wheelBase)
				.append(cost).append(charge).append(otCharge).append(dtCharge)
				.append(pound).append(glType).append(type).append(vin).append(
						tagNumber).append(state).append(truckType).append(
						localNumber).append(warehouse).append(make)
				.append(year).append(model).append(driver).append(ownerPayTo)
				.append(accountingType).append(ownerPayToName).append(
						payToTruck).append(originalCost).append(fleet).append(
						acquired).append(trailerKind).append(gpClass).append(
						height).append(weight).append(width).append(length)
				.append(gross).append(kpinToAxle1).append(axle1ToAxle2).append(
						axle2ToAxle3).append(axle3ToAxle4).append(axle4ToAxle5)
				.append(axle5ToAxle6).append(axle6ToAxle7).append(axle7ToAxle8)
				.append(axle8ToAxle9).append(axle9ToAxle10).append(
						axle10ToAxle11).append(lastAxleRear).append(status)
				.append(vlTruckNumber).append(docVehicle).append(cdlVehicle)
				.append(isGaraged).append(dotInsp).append(vlInsp)
				.append(pmDate).append(milesdate).append(inService).append(
						lastStInsp).append(dotInspDue).append(vlInspDue)
				.append(lastVlInsp).append(mustdispatch).append(trailertype)
				.append(conditionTO).append(truckStatus).append(inactiveDate)
				.append(capacity).append(agency).append(agencyTruckId)
				.toHashCode();
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length=20)
	public Long getIdNum() {
		return idNum;
	}

	public void setIdNum(Long idNum) {
		this.idNum = idNum;
	}

	@Column(length=10)
	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	@Column(length=1)
	public String getCdlVehicle() {
		return cdlVehicle;
	}
	
	public void setCdlVehicle(String cdlVehicle) {
		this.cdlVehicle = cdlVehicle;
	}
	@Column(precision=9,scale=2)
	public BigDecimal getCharge() {
		return charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(precision=9,scale=2)
	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=28)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Column(length=1)
	public String getDocVehicle() {
		return docVehicle;
	}

	public void setDocVehicle(String docVehicle) {
		this.docVehicle = docVehicle;
	}
	@Column
	public Date getDotInsp() {
		return dotInsp;
	}

	public void setDotInsp(Date dotInsp) {
		this.dotInsp = dotInsp;
	}
	@Column
	public Date getDotInspDue() {
		return dotInspDue;
	}

	public void setDotInspDue(Date dotInspDue) {
		this.dotInspDue = dotInspDue;
	}
	@Column
	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}
	@Column(precision=9,scale=2)
	public BigDecimal getDtCharge() {
		return dtCharge;
	}

	public void setDtCharge(BigDecimal dtCharge) {
		this.dtCharge = dtCharge;
	}
	@Column(length=20)
	public Long getEqIdNum() {
		return eqIdNum;
	}

	public void setEqIdNum(Long eqIdNum) {
		this.eqIdNum = eqIdNum;
	}
	@Column(length=20)
	public String getGl() {
		return gl;
	}

	public void setGl(String gl) {
		this.gl = gl;
	}
	@Column(length=7)
	public String getGlType() {
		return glType;
	}

	public void setGlType(String glType) {
		this.glType = glType;
	}
	@Column(length=1)
	public Boolean getIsGaraged() {
		return isGaraged;
	}

	public void setIsGaraged(Boolean isGaraged) {
		this.isGaraged = isGaraged;
	}
	@Column
	public Date getLastStInsp() {
		return lastStInsp;
	}

	public void setLastStInsp(Date lastStInsp) {
		this.lastStInsp = lastStInsp;
	}
	@Column
	public Date getLastVlInsp() {
		return lastVlInsp;
	}

	public void setLastVlInsp(Date lastVlInsp) {
		this.lastVlInsp = lastVlInsp;
	}
	@Column(length=20)
	public String getLocalNumber() {
		return localNumber;
	}

	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}
	@Column(length=15)
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}
	@Column(length=7)
	public Long getMiles() {
		return miles;
	}

	public void setMiles(Long miles) {
		this.miles = miles;
	}
	@Column
	public Date getMilesdate() {
		return milesdate;
	}

	public void setMilesdate(Date milesdate) {
		this.milesdate = milesdate;
	}
	@Column(length=4)
	public Long getTruckOrder() {
		return truckOrder;
	}

	public void setTruckOrder(Long truckOrder) {
		this.truckOrder = truckOrder;
	}
	@Column(precision=9,scale=2)
	public BigDecimal getOtCharge() {
		return otCharge;
	}

	public void setOtCharge(BigDecimal otCharge) {
		this.otCharge = otCharge;
	}
	
	@Column
	public Date getPmDate() {
		return pmDate;
	}

	public void setPmDate(Date pmDate) {
		this.pmDate = pmDate;
	}
	@Column(precision=11,scale=3)
	public BigDecimal getPound() {
		return pound;
	}

	public void setPound(BigDecimal pound) {
		this.pound = pound;
	}
	
	@Column(length=2)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	
	@Column(length=15)
	public String getTagNumber() {
		return tagNumber;
	}

	/**
	 * @return the ownerPayTo
	 */
	@Column(length = 8)
	public String getOwnerPayTo() {
		return ownerPayTo;
	}

	/**
	 * @param ownerPayTo the ownerPayTo to set
	 */
	public void setOwnerPayTo(String ownerPayTo) {
		this.ownerPayTo = ownerPayTo;
	}

	public void setTagNumber(String tagNumber) {
		this.tagNumber = tagNumber;
	}
	@Column(length=2)
	public String getTruckType() {
		return truckType;
	}

	public void setTruckType(String truckType) {
		this.truckType = truckType;
	}
	@Column(length=7)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=25)
	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}
	@Column
	public Date getVlInsp() {
		return vlInsp;
	}

	public void setVlInsp(Date vlInsp) {
		this.vlInsp = vlInsp;
	}
	@Column
	public Date getVlInspDue() {
		return vlInspDue;
	}

	public void setVlInspDue(Date vlInspDue) {
		this.vlInspDue = vlInspDue;
	}
	@Column(length=10)
	public String getVlTruckNumber() {
		return vlTruckNumber;
	}

	public void setVlTruckNumber(String vlTruckNumber) {
		this.vlTruckNumber = vlTruckNumber;
	}
	@Column(length=2)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column(length=4)
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	@Transient
	public String getListCode(){
		return localNumber;
	}
	@Transient
	public String getListDescription(){
		String lName = this.description.replaceAll("&", "%26");
		lName = lName.replaceAll("#", "%23");
		return lName;
	}
	
	@Transient
	public String getListSecondDescription() {
			if (this.ownerPayTo == null){
				ownerPayTo="";
			}
		return ownerPayTo;
	}

	@Transient
	public String getListThirdDescription() {
		if (this.ownerPayToName == null){
			ownerPayToName="";
		}
		return ownerPayToName;
	}

	@Transient
	public String getListFourthDescription() { 
		if (this.agency == null){
			agency="";
		}
		return agency;
	}

	@Transient
	public String getListFifthDescription() {
		return "";
	}

	@Transient
	public String getListSixthDescription() {
		return "";
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return "";
	}

	/**
	 * @return the country
	 */
	@Column( length=45 )
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return "";
	}

	/**
	 * @return the model
	 */
	@Column(length=20)
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the inService
	 */
	@Column
	public Date getInService() {
		return inService;
	}

	/**
	 * @param inService the inService to set
	 */
	public void setInService(Date inService) {
		this.inService = inService;
	}

	/**
	 * @return the acquired
	 */
	@Column
	public Date getAcquired() {
		return acquired;
	}

	/**
	 * @param acquired the acquired to set
	 */
	public void setAcquired(Date acquired) {
		this.acquired = acquired;
	}

	/**
	 * @return the originalCost
	 */
	@Column(length=10)
	public String getOriginalCost() {
		return originalCost;
	}

	/**
	 * @param originalCost the originalCost to set
	 */
	public void setOriginalCost(String originalCost) {
		this.originalCost = originalCost;
	}

	/**
	 * @return the payToTruck
	 */
	@Column(length=10)
	public String getPayToTruck() {
		return payToTruck;
	}

	/**
	 * @param payToTruck the payToTruck to set
	 */
	public void setPayToTruck(String payToTruck) {
		this.payToTruck = payToTruck;
	}

	/**
	 * @return the fleet
	 */
	@Column(length=20)
	public String getFleet() {
		return fleet;
	}

	/**
	 * @param fleet the fleet to set
	 */
	public void setFleet(String fleet) {
		this.fleet = fleet;
	}

	/**
	 * @return the axle
	 */
	@Column(length=2)
	public String getAxle() {
		return axle;
	}

	/**
	 * @param axle the axle to set
	 */
	public void setAxle(String axle) {
		this.axle = axle;
	}

	/**
	 * @return the wheelBase
	 */
	@Column(length=20)
	public String getWheelBase() {
		return wheelBase;
	}

	/**
	 * @param wheelBase the wheelBase to set
	 */
	public void setWheelBase(String wheelBase) {
		this.wheelBase = wheelBase;
	}

	
	/**
	 * @return the ownerPayToName
	 */
	@Column(length=255)
	public String getOwnerPayToName() {
		return ownerPayToName;
	}

	/**
	 * @param ownerPayToName the ownerPayToName to set
	 */
	public void setOwnerPayToName(String ownerPayToName) {
		this.ownerPayToName = ownerPayToName;
	}

	/**
	 * @return the accountingType
	 */
	@Column(length=60)
	public String getAccountingType() {
		return accountingType;
	}

	/**
	 * @param accountingType the accountingType to set
	 */
	public void setAccountingType(String accountingType) {
		this.accountingType = accountingType;
	}

	/**
	 * @return the axle1ToAxle2
	 */
	@Column(length=10)
	public String getAxle1ToAxle2() {
		return axle1ToAxle2;
	}

	/**
	 * @param axle1ToAxle2 the axle1ToAxle2 to set
	 */
	public void setAxle1ToAxle2(String axle1ToAxle2) {
		this.axle1ToAxle2 = axle1ToAxle2;
	}

	/**
	 * @return the axle2ToAxle3
	 */
	@Column(length=10)
	public String getAxle2ToAxle3() {
		return axle2ToAxle3;
	}

	/**
	 * @param axle2ToAxle3 the axle2ToAxle3 to set
	 */
	public void setAxle2ToAxle3(String axle2ToAxle3) {
		this.axle2ToAxle3 = axle2ToAxle3;
	}

	/**
	 * @return the axle3ToAxle4
	 */
	@Column(length=10)
	public String getAxle3ToAxle4() {
		return axle3ToAxle4;
	}

	/**
	 * @param axle3ToAxle4 the axle3ToAxle4 to set
	 */
	public void setAxle3ToAxle4(String axle3ToAxle4) {
		this.axle3ToAxle4 = axle3ToAxle4;
	}

	/**
	 * @return the axle4ToAxle5
	 */
	@Column(length=10)
	public String getAxle4ToAxle5() {
		return axle4ToAxle5;
	}

	/**
	 * @param axle4ToAxle5 the axle4ToAxle5 to set
	 */
	public void setAxle4ToAxle5(String axle4ToAxle5) {
		this.axle4ToAxle5 = axle4ToAxle5;
	}

	/**
	 * @return the gpClass
	 */
	@Column(length=10)
	public String getGpClass() {
		return gpClass;
	}

	/**
	 * @param gpClass the gpClass to set
	 */
	public void setGpClass(String gpClass) {
		this.gpClass = gpClass;
	}

	/**
	 * @return the gross
	 */
	@Column(length=10)
	public String getGross() {
		return gross;
	}

	/**
	 * @param gross the gross to set
	 */
	public void setGross(String gross) {
		this.gross = gross;
	}

	/**
	 * @return the height
	 */
	@Column(length=10)
	public String getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @return the kpinToAxle1
	 */
	@Column(length=10)
	public String getKpinToAxle1() {
		return kpinToAxle1;
	}

	/**
	 * @param kpinToAxle1 the kpinToAxle1 to set
	 */
	public void setKpinToAxle1(String kpinToAxle1) {
		this.kpinToAxle1 = kpinToAxle1;
	}

	/**
	 * @return the lastAxleRear
	 */
	@Column(length=10)
	public String getLastAxleRear() {
		return lastAxleRear;
	}

	/**
	 * @param lastAxleRear the lastAxleRear to set
	 */
	public void setLastAxleRear(String lastAxleRear) {
		this.lastAxleRear = lastAxleRear;
	}

	/**
	 * @return the length
	 */
	@Column(length=10)
	public String getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(String length) {
		this.length = length;
	}

	/**
	 * @return the trailerKind
	 */
	@Column(length=50)
	public String getTrailerKind() {
		return trailerKind;
	}

	/**
	 * @param trailerKind the trailerKind to set
	 */
	public void setTrailerKind(String trailerKind) {
		this.trailerKind = trailerKind;
	}

	/**
	 * @return the weight
	 */
	@Column(length=10)
	public String getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}

	/**
	 * @return the width
	 */
	@Column(length=10)
	public String getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	/**
	 * @return the status
	 */
	@Column(length=20)
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the axle5ToAxle6
	 */
	@Column(length=10)
	public String getAxle5ToAxle6() {
		return axle5ToAxle6;
	}

	/**
	 * @param axle5ToAxle6 the axle5ToAxle6 to set
	 */
	public void setAxle5ToAxle6(String axle5ToAxle6) {
		this.axle5ToAxle6 = axle5ToAxle6;
	}

	/**
	 * @return the axle6ToAxle7
	 */
	@Column(length=10)
	public String getAxle6ToAxle7() {
		return axle6ToAxle7;
	}

	/**
	 * @param axle6ToAxle7 the axle6ToAxle7 to set
	 */
	public void setAxle6ToAxle7(String axle6ToAxle7) {
		this.axle6ToAxle7 = axle6ToAxle7;
	}

	/**
	 * @return the axle7ToAxle8
	 */
	@Column(length=10)
	public String getAxle7ToAxle8() {
		return axle7ToAxle8;
	}

	/**
	 * @param axle7ToAxle8 the axle7ToAxle8 to set
	 */
	public void setAxle7ToAxle8(String axle7ToAxle8) {
		this.axle7ToAxle8 = axle7ToAxle8;
	}

	/**
	 * @return the axle8ToAxle9
	 */
	@Column(length=10)
	public String getAxle8ToAxle9() {
		return axle8ToAxle9;
	}

	/**
	 * @param axle8ToAxle9 the axle8ToAxle9 to set
	 */
	public void setAxle8ToAxle9(String axle8ToAxle9) {
		this.axle8ToAxle9 = axle8ToAxle9;
	}

	/**
	 * @return the axle9ToAxle10
	 */
	@Column(length=10)
	public String getAxle9ToAxle10() {
		return axle9ToAxle10;
	}

	/**
	 * @param axle9ToAxle10 the axle9ToAxle10 to set
	 */
	public void setAxle9ToAxle10(String axle9ToAxle10) {
		this.axle9ToAxle10 = axle9ToAxle10;
	}

	/**
	 * @return the axle10ToAxle11
	 */
	@Column(length=10)
	public String getAxle10ToAxle11() {
		return axle10ToAxle11;
	}

	/**
	 * @param axle10ToAxle11 the axle10ToAxle11 to set
	 */
	public void setAxle10ToAxle11(String axle10ToAxle11) {
		this.axle10ToAxle11 = axle10ToAxle11;
	}
	@Column(length=10)
	public String getMustdispatch() {
		return mustdispatch;
	}

	public void setMustdispatch(String mustdispatch) {
		this.mustdispatch = mustdispatch;
	}
	@Column(length=20)
	public String getTrailertype() {
		return trailertype;
	}

	public void setTrailertype(String trailertype) {
		this.trailertype = trailertype;
	}
	@Column(length=10)
	public String getConditionTO() {
		return conditionTO;
	}

	public void setConditionTO(String conditionTO) {
		this.conditionTO = conditionTO;
	}
    @Column
	public Date getInactiveDate() {
		return inactiveDate;
	}

	public void setInactiveDate(Date inactiveDate) {
		this.inactiveDate = inactiveDate;
	}
    @Column(length=2)
	public String getTruckStatus() {
		return truckStatus;
	}

	public void setTruckStatus(String truckStatus) {
		this.truckStatus = truckStatus;
	}

	/**
	 * @return the axle
	 */
	@Column
	public BigDecimal getCapacity() {
		return capacity;
	}

	public void setCapacity(BigDecimal capacity) {
		this.capacity = capacity;
	}
	@Column
	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}
	@Column(length=20)
	public String getAgencyTruckId() {
		return agencyTruckId;
	}

	public void setAgencyTruckId(String agencyTruckId) {
		this.agencyTruckId = agencyTruckId;
	}
	
}
