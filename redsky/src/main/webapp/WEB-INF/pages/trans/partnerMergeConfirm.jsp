<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Partner Merge Confirmation</title>   
    <meta name="heading" content="Partner Merge Confirmation"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<script type="text/javascript">
function closeWindow(){
	window.close();
	if (window.opener && !window.opener.closed) {
		window.opener.location.reload();
	}
}

function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
        if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
       if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
</script>   
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">

<style>
#layerH {
filter:alpha(opacity=50);
-moz-opacity:0.5;
-khtml-opacity: 0.5;
opacity: 0.5;
color: #ffffff;
font-family: Arial;
font-size:18px;
font-weight:bold;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

div.msg{
 margin-left:90px;
 margin-top:80px;
}


.rounded-top {
   -moz-border-radius-topleft: .8em;
   -webkit-border-top-left-radius:.8em;
   -moz-border-radius-topright: .8em;
   -webkit-border-top-right-radius:.8em;
   border:1px solid #AFAFAF;
   border-bottom:none;
   
   }
.rounded-bottom {
   -moz-border-radius-bottomleft: .8em;
   -webkit-border-bottom-left-radius:.8em;
   -moz-border-radius-bottomright: .8em;
   -webkit-border-bottom-right-radius:.8em;
   border:1px solid #AFAFAF;
   border-top:none;
   
}
</style>
</head>

<div ID="layerH">
	<div class="msg">
		<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
				<td align="center">
					<table cellspacing="0" cellpadding="3" align="center">
						<tr>
							<td height="200px"></td>
						</tr>
						<tr>
			       			<td style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px"><font color="#0F0F0F" face="Arial" size="2px" weight="bold">Partner merging is in progress....</font></td>
				       	</tr>
			      	 	<tr>
		      				<td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px"><img src="<c:url value='/images/ajax-loader.gif'/>" /></td>
		       			</tr>
		       		</table>
		       	</td>
			</tr>
		</table>
	</div>
</div>

<s:form id="addPartner" action="mergePartner.html?decorator=popup&popup=true" method="post" >  
<s:hidden name="userCheck"/>
<s:hidden name="userCheckMaster"/>
<s:hidden name="partnerCorpid"/>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table>
					<tbody>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<c:if test="${hitflag != 1}" >
							<tr>
								<td align="left" class="listwhitetext" colspan="6">Are you sure you wish to merge these partners ( ${userCheck} ).</td>
						  	</tr>
					  	</c:if>
					  	<c:if test="${hitflag == 1}" >
					  		<tr>
								<td align="left" class="listwhitetext" colspan="6"><B><U>New Records insert in the following tables with new partner code ${partnerPublic.partnerCode}</U></B></td>
						  	</tr>
					  		<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>Partner Public</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>Partner Private</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>Agent Base</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>Agent Base SCAC</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>ContractPolicy</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>AccountProfile</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>AccountContact</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>PartnerAccountRef</B></td>
						  	</tr>
						  	<tr>
								<td align="left" class="listwhitetext" colspan="6"></td>
						  	</tr>
					  		<tr>
								<td align="left" class="listwhitetext" colspan="6"><B>Updated Tables</B></td>
						  	</tr>
					  		<tr>
								<td align="left" class="listwhitetext" colspan="6">${logQuery}.</td>
					  		</tr>
					  	</c:if>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<c:if test="${hitflag != 1}" >
				<td>
					<s:submit type="button" name="mergeBtn" cssClass="cssbutton" cssStyle="width:105px; height:25px; margin-left:30px;" align="top" value="Merge" onclick="progressBar('1');"/> 
					<input type="button" name="clsoe" class="cssbutton" style="width:105px; height:25px; margin-left:30px;" align="top" value="Close" onclick="window.close();"/>  
				</td>
			</c:if>
			<c:if test="${hitflag == 1}" >
				<td>
					<input type="button" name="clsoe" class="cssbutton" style="width:105px; height:25px; margin-left:30px;" align="top" value="Close" onclick="closeWindow();"/>  
				</td>
			</c:if>
		</tr>	
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
<script type="text/javascript"> 
try{
showOrHide(0);
}
catch(e){}
</script> 