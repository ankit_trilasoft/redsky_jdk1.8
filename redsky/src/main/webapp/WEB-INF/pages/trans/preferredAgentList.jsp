<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title><fmt:message key="preferredAgent.title"/></title>   
    <meta name="heading" content="<fmt:message key='preferredAgent.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function activBtn(temp){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
    var len = temp.length;
    var val="" ;
    if(len == undefined){
		if(temp.checked){
			val= temp.value;
		}
    }
		
	  for(var i = 0; i < len; i++) {
		if(temp[i].checked) {
			val = temp[i].value;
		}
	}

	if(val=='AG'){
		document.forms['addPartner'].elements['chkAgentTrue'].value='Y';
	}else{
		document.forms['addPartner'].elements['chkAgentTrue'].value='';	
	}
}

function findVanLineCode(partnerCode,position){
	var url="findVanLineCodeList.html?ajax=1&decorator=simple&popup=true&partnerVanLineCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);
}
</script>
<style>
span.pagelinks {display:block;margin-bottom:0px;!margin-bottom:2px;margin-top:-10px;!margin-top:-17px;padding:2px 0px;text-align:right;width:99%;
!width:98%;font-size:0.85em;}
form {margin-top:-10px;!margin-top:0px;}
div#main {margin:-5px 0 0;}
input[type="checkbox"]{vertical-align:middle;}
</style>
<script type="text/javascript">
   try{
   	if('<%=session.getAttribute("agentCodeSession")%>'=='null')
   	{
   		
   		document.forms['preferredagentlist'].elements['preferredAgent.getPreferredAgentCode'].value='';
   	}
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("agentCodeSession")%>'!='null')
   	{	
   		
   		document.forms['preferredagentlist'].elements['preferredAgent.getPreferredAgentCode'].value='<%=session.getAttribute("agentCodeSession")%>';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("agentNameSession")%>'=='null')
   	{
   		document.forms['preferredagentlist'].elements['preferredAgent.getPreferredAgentName'].value='';
   	}
   	}
   	catch(e){}
	try{
	if('<%=session.getAttribute("agentNameSession")%>'!='null')
   	{
   		document.forms['preferredagentlist'].elements['preferredAgent.getPreferredAgentName'].value='<%=session.getAttribute("agentNameSession")%>';
   	} 
   	}
   	
   	catch(e){}
   	
   	try{
   	if('<%=session.getAttribute("statusSession")%>'=='null')
   	{
   		document.forms['preferredagentlist'].elements['activeCheck'].checked = false;
   	}
	}
	catch(e){}
	try{
	if('<%=session.getAttribute("statusSession")%>'=='true')
   	{
   		document.forms['preferredagentlist'].elements['activeCheck'].checked = true;
   	} 
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("statusSession")%>'=='false')
   	{
   		document.forms['preferredagentlist'].elements['activeCheck'].checked = false;
   	}
   	}
   	catch(e){}
   try{
   	if('${activeCheck}'=='true')
   	{
   		document.forms['preferredagentlist'].elements['activeCheck'].checked = true;
   	}
   	}
   	catch(e){}
</script>
</head>
<c:set var="addbuttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editPreferredAgent.html?partnerCode=${partnerCode}&partnerId=${partnerId}&partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/> 
</c:set>

<c:set var="buttons">         
        <a id="showButton" class="groupadd" style="height:14px;"><fmt:message key="button.addByGroup"/> </a>  
</c:set>

<c:set var="savebuttons">   
         <input type="button" class="groupaddsave" style="width:55px; height:24px;margin-left:5px;"  onclick="saveAgentGroup()"  
        value="<fmt:message key="button.save"/>"/>   
</c:set>
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search" />
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>
</c:set>   

<s:form id="preferredagentlist" action='${empty param.popup?"searchAgentDetail.html":"searchAgentDetail.html?decorator=popup&popup=true"}' method="post" >
<s:hidden name="partnerCode" value="%{partnerCode}"/> 
<s:hidden name="partnerId" value="%{partnerId}"/>   
<s:hidden name="partnerType" value="%{partnerType}"/>


<div id="layer5" style="width:100%">
<div id="newmnav">
	<div class="spnblk" style="margin-top:10px;">&nbsp;</div>
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>
		<c:if test="${partnerType == 'AC'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
			<li><a href="editNewAccountProfile.html?id=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
			<li><a href="editPartnerPrivate.html?partnerId=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
			<configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
			<li><a href="editProfileInfo.html?partnerId=${partnerId}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
			</configByCorp:fieldVisibility>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<c:if test="${not empty partnerPrivate.id}">
			<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
				<li><a href="accountContactList.html?id=${partnerId}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
				</configByCorp:fieldVisibility>
				<c:if test="${checkTransfereeInfopackage==true}">
				<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
			</configByCorp:fieldVisibility>
			</c:if>
			<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
					<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partnerPublic.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>	
				<c:if test="${not empty partnerPrivate.id}">
					<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
				</c:if>
				</c:if>

		<c:if test="${partnerType == 'AG'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Agent Detail</span></a></li>
			<li><a href="findPartnerProfileList.html?code=${partnerPrivate.partnerCode}&partnerType=${partnerType}&id=${partnerId}"><span>Agent Profile</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
			<li><a href="baseList.html?id=${partnerId}"><span>Base</span></a></li>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			   <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
	       			<li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
	       			<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
			        </configByCorp:fieldVisibility>
			</c:if>
			
			       <li id="AGR"><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			 
			 
			  	
		</c:if>
		<c:if test="${partnerType == 'VN'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Vendor Detail</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Users & contacts</span></a></li>		
		</c:if>
		<c:if test="${partnerType == 'CR'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Carrier Detail</span></a></li>
		</c:if>
		<c:if test="${partnerType == 'CR' || partnerType == 'VN'}">
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
	</ul>
 	
</div>
<div class="spnblk" style="margin-top:-10px;">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:20px;!margin-top:-2px;"><span></span></div>

<div class="center-content">
<table class="table" border="0" style="width:99%;">
	<thead>
		<tr>
			<th><fmt:message key="preferredAgent.preferredAgentCode"/></th>
			<th><fmt:message key="preferredAgent.preferredAgentName"/></th>
			<th><fmt:message key="preferredAgent.agentGroup"/></th>
			<th><fmt:message key="preferredAgent.status"/></th>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="preferredAgent.preferredAgentCode" size="8" cssClass="input-text" /></td>
			<td><s:textfield name="preferredAgent.preferredAgentName" size="35" cssClass="input-text"/></td>
			<td>
			<s:select title="" cssClass="list-menu" name="agentGroup" list="%{agentGroupList}" cssStyle="width:120px;"/>
			</td>
			<td>
			<s:select title="" cssClass="list-menu" name="statusType" list="{'All','Active','Inactive'}" cssStyle="width:75px"/>
			</td>
		</tr>
		<tr>
			<td colspan="11">
				<table style="margin:0px; padding:0px;width:100%;" cellpadding="0" cellspacing="0" >
					<tbody>
						<tr>
												
							<td width="130px" style="border:none;text-align:right;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
						</tr>
					</tbody>	
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:35px;!margin-top:45px;"><span></span></div>
</div>
</div>
</div>
</s:form>
<div id="layer1" style="width:100%">
<div id="newmnav" style="margin-top:-10px; ">
	<ul>
		 <li id=""><a><span>Preferred Agent List</span></a></li>
	</ul>
</div>
<div style="float:left;height:25px;margin:0px;">
<div style="float:left;">
<c:if test="${empty param.popup}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
</div>
<div id="All" style="display:none;float:left;">
<table style="margin:0px;" cellpadding="0" cellspacing="0">
<tr>
<td style="vertical-align:top;margin-left:10px;">
<s:select title="" cssClass="list-menu" id="agentGrouplist" name="agentGrouplist" list="%{agentGroupList}" cssStyle="width:88px;height:23px;border-radius:3px;margin-left:5px;"/>
</td>
<td>
<c:if test="${empty param.popup}">
<c:out value="${savebuttons}" escapeXml="false" />
</c:if>
</td>
</tr>
</table>
</div>
</div>
<div class="spnblk">&nbsp;</div> 


<s:set name="preferredAgentList" value="preferredAgentList" scope="request"/>  
<display:table name="preferredAgentList" class="table" requestURI="" id="preferredAgentList" export="false" defaultsort="2" pagesize="10" style="width:99%;margin-left:5px;">  		
	<c:if test="${empty param.popup}">
<display:column property="preferredAgentCode" style="width:150px" sortable="true" titleKey="preferredAgent.preferredAgentCode" href="editPreferredAgent.html?partnerId=${partnerId}&partnerType=${partnerType}" paramId="id" paramProperty="id"/>   
</c:if>
<c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="preferredAgent.preferredAgentCode"/>
</c:if>   
	<display:column property="preferredAgentName" sortable="true" titleKey="preferredAgent.preferredAgentName" style="width:390px"></display:column>
	<display:column property="agentGroup" sortable="true" titleKey="preferredAgent.agentGroup"></display:column>
	<display:column  style="width:90px" title="Status">
	<div align="">
       		<s:checkbox name="preferredAgent.status" value="${preferredAgentList.status }" onclick="updateStatus('${preferredAgentList.id}','${preferredAgentList.preferredAgentCode}',this);"/>
     </div>
     </display:column>
     
	<%-- <c:if test="${preferredAgentList!='[]'}">
	<c:if test="${preferredAgentList.status=='true' }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
   	<c:if test="${preferredAgentList.status=='false' }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>	
	</c:if> --%>	
	
    <%-- <display:column property="status" sortable="true" titleKey="preferredAgent.status" style="width:100px"/> --%>
    <display:setProperty name="paging.banner.item_name" value="Partner Public"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Public"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>
<div style="float:left;">
<c:if test="${empty param.popup}">
<c:out value="${addbuttons}" escapeXml="false" />
</c:if>
</div>
</div>
<c:set var="isTrue" value="false" scope="session"/>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<script type="text/javascript">
function clear_fields(){
 	document.forms['preferredagentlist'].elements['preferredAgent.preferredAgentCode'].value = '';
	document.forms['preferredagentlist'].elements['preferredAgent.preferredAgentName'].value = '';
	document.getElementById("status").checked =false;
}

/* function showAgentGroup(){
	var elem = document.getElementById("All");
	elem.style.display = 'inline';
} */

jQuery(document).ready(function(){
    jQuery('#showButton').live('click', function(event) {        
         jQuery('#All').toggle('show');
    });
});

function saveAgentGroup(){
	var agentGrp=document.getElementById('agentGrouplist').value;
	if(agentGrp==''){
		alert("Please select Agent Group");
		return false;
	}
	var partnerCode=document.forms['preferredagentlist'].elements['partnerCode'].value;
	var partnerId=document.forms['preferredagentlist'].elements['partnerId'].value;
	var partnerType=document.forms['preferredagentlist'].elements['partnerType'].value;
	location.href='saveAgentGroup.html?agentGroup='+encodeURIComponent(agentGrp)+'&partnerCode='+encodeURI(partnerCode)+'&partnerId='+encodeURI(partnerId)+'&partnerType='+encodeURI(partnerType);
	progressBarAutoSave('1');
}

function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject();
    var http4 = getHTTPObject();

function updateStatus(targetElement, targetElement2,targetElement3){
	var did = targetElement;
	var agentCode=targetElement2;
	if(targetElement3.checked){
		 var url="updateAgentStatus.html?decorator=simple&popup=true&preIdNum="+encodeURI(did)+"&status="+encodeURI(true);
		 http4.open("GET", url, true); 
	     http4.onreadystatechange = handleHttpResponseStatus; 
	     http4.send(null);
	}else{
		 var url="updateAgentStatus.html?decorator=simple&popup=true&preIdNum="+encodeURI(did)+"&status="+encodeURI(false);
		 http5.open("GET", url, true); 
	     http5.onreadystatechange = handleHttpResponseStatus; 
	     http5.send(null);
	}
}
function handleHttpResponseStatus(){
}


</script>
