<%@ include file="/common/taglibs.jsp"%>   

<head>    
    <title><fmt:message key="contractDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='contractDetail.heading'/>"/>
    
 </head>
  <c:set var="currencySign" value="${currencySign}"/> 
   <s:form id="contractForm" name="contractForm" action="saveContract" method="post" validate="true" onsubmit = "return checkJobValidation('save');"> 
    <s:hidden name="secondDescription" />
		<s:hidden name="thirdDescription" />
		<s:hidden name="firstDescription" />
		<s:hidden name="fourthDescription" />
		<s:hidden name="fifthDescription" />
		<s:hidden name="sixthDescription" />
		<s:hidden name="seventhDescription"/>
		<s:hidden name="eigthDescription" />
	    <s:hidden name="ninthDescription" />
	    <s:hidden name="tenthDescription" />
    <s:hidden name="contract.id" value="%{contract.id}"/> 
    <s:hidden name="contract.owner" />
    <s:hidden name="contract.corpID" />
 	<s:hidden name="contract.published" />
    <s:hidden name="id" value="<%=request.getParameter("id")%>"/>
    <c:set var="contract.id" value="<%=request.getParameter("id")%>"/>   
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 	<s:hidden name="UTSI" id="UTSI" value="%{checkUTSI}"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.contractList' }">
    <c:redirect  url="/contracts.html"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.chargeList' }">
    <c:redirect  url="/charge23.html?id=${contract.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountList' }">
    <c:redirect url="/contractAccountList.html?id=${contract.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.materialList' }">
	<c:redirect url="/contractMaters.html?id=${contract.id}&itemType=M&cont=${contract.contract}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.acctTemplateList' }">
	<c:redirect url="/acctTemplateList.html?id=${contract.id}&contract=${contract.contract}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.equipmentList' }">
	<c:redirect url="/contractMaters.html?id=${contract.id}&itemType=E&cont=${contract.contract}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.agentContractLists' }">
	<c:redirect url="/agentContractLists.html?id=${contract.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<div id="layer4" style="width:100%;">
<div id="newmnav">
		  <ul>
		    <li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.contractList');return checkContract('none');"><span>Contract List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Contract Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <c:if test="${not empty contract.id}">
		    <li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.chargeList');return checkContract('none');"><span>Charge List</span></a></li>
		    <li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.accountList');return checkContract('none');"><span>Account List</span></a></li>
		    <!--<li><a onclick="openWindow('auditList.html?id=${contract.id}&tableName=contract&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>-->
		    <!-- Move In Admin menu
		    
		    <li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.materialList');return checkContract('none');"><span>Material</span></a></li>
			<li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.equipmentList');return checkContract('none');"><span>Equipment</span></a></li>
			
		     -->
		      
		    <li><a onclick="window.open('auditList.html?id=${contract.id}&tableName=contract&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>	
		    <!--<li><a href="charge23.html?id=${contract.id}"><span>Charge List</span></a></li>
		    <li><a href="contractAccountList.html?id=${contract.id}"><span>Account List</span></a></li>
		    -->
		     <li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.acctTemplateList');return checkContract('none');"><span>Acct Template</span></a></li>
		      <c:if test="${checkUTSI==true}">
		      <li><a onmouseover="completeTimeString();return chkSelect();" onclick="setReturnString('gototab.agentContractLists');return checkContract('none');"><span>Agent List</span></a></li>
		      </c:if>
		      	<configByCorp:fieldVisibility componentId="component.tab.commission.compensation">
			    	<li><a href="editCompensationSetupForm.html?contractid =${contract.id}&contractName=${contract.contract}"><span>Compensation SetUp</span></a></li>
				</configByCorp:fieldVisibility>
		       </c:if>
  </ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
 <div id="Layer1" style="width:100%" onkeydown="changeStatus();"> 
 <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
  <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
			<tbody>
			<tr><td height="4px"></td></tr>
			<tr>

<td align="right" class="listwhitetext"><fmt:message key='contract.contract'/><font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield name="contract.contract" size="25" maxlength="65" required="true" onchange="checkOldContract(this);" cssClass="input-text" tabindex="1"/></td>
<td align="right" class="listwhitetext" width="57px"><fmt:message key='contract.jobType'/></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield name="contract.jobType" size="39" maxlength="65" cssClass="input-text" tabindex="2"/></td>
<td><input type="button" class="cssbuttonA" style="width:100px; height:22px" name="validate_job"  value="Validate job List" onclick="return checkJobValidation();" tabindex="3"/></td>
<td align="right" class="listwhitetext" width="100px" colspan="2"><fmt:message key='partnerAccountRef.companyDivision'/></td>
<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="contract.companyDivision" list="%{companyDivis}"  headerKey="" headerValue="" cssStyle="width:58px" onchange="changeStatus();" tabindex="4" /></td>
<c:if test="${checkUTSI==true || tenancyBillingContractType==true}">
<td align="right" class="listwhitetext" width="75px" colspan="2">Contract Type</td>
<td align="left"><s:select cssClass="list-menu"  id="companyAgentTypeDivision" name="contract.contractType" list="%{agentType1}" onchange="chhkCmmDmm()" cssStyle="width:110px" tabindex="5"/></td>
</c:if>
<c:if test="${!(checkUTSI==true || tenancyBillingContractType==true)}">
<s:hidden  name="contract.contractType"/>
</c:if>
<td align="right" width=""  class="listwhitetext"><configByCorp:fieldVisibility componentId="contract.recClearing">Rec&nbsp;Clearing</configByCorp:fieldVisibility></td>
<td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="contract.recClearing"><s:textfield cssClass="input-text" key="contract.recClearing" size="7"  tabindex="5" maxlength="20" /></configByCorp:fieldVisibility></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='contract.description'/><font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield name="contract.description" size="25" maxlength="65" required="true" cssClass="input-text" tabindex="6"/></td>
<td align="right" class="listwhitetext"><fmt:message key='contract.begin'/></td>
<c:if test="${not empty contract.begin}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="contract.begin"/></s:text>
			<td><s:textfield cssClass="input-text" id="begin" name="contract.begin" value="%{customerFileMoveDateFormattedValue}" readonly="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" /><img id="begin_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>

			</c:if>
			<c:if test="${empty contract.begin}">
			<td><s:textfield cssClass="input-text" id="begin" name="contract.begin"  cssStyle="width:65px;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="begin_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
<td align="right" class="listwhitetext"><fmt:message key='contract.ending'/></td>
<c:if test="${not empty contract.ending}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="contract.ending"/></s:text>
			<td><s:textfield cssClass="input-text" id="ending" name="contract.ending" value="%{customerFileMoveDateFormattedValue}" readonly="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" /><img id="ending_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty contract.ending}">
			<td><s:textfield cssClass="input-text" id="ending" name="contract.ending"  cssStyle="width:65px;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="ending_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:set var="isTariffChecked" value="false"/>
			<c:if test="${contract.displayonAgentTariff}">
				 <c:set var="isTariffChecked" value="true"/>
            </c:if>
           <td align="right" class="listwhitetext" colspan="3" >Display for Agent Tariff <s:checkbox key="contract.displayonAgentTariff" value="${isTariffChecked}" fieldValue="true" cssStyle="vertical-align:sub;!vertical-align:top;" tabindex="7"/>
           </td>
           <td align="right" width=""  class="listwhitetext"><configByCorp:fieldVisibility componentId="contract.payClearing">Pay&nbsp;Clearing</configByCorp:fieldVisibility></td>
           <td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="contract.payClearing"><s:textfield cssClass="input-text" key="contract.payClearing" size="7"  maxlength="20" /></configByCorp:fieldVisibility></td>
		</tr>
		</tbody>
		</table>

<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
<tbody>
<tr>
<td class="" style="width:100%; height:23px;!height:18px;">
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px; cursor:default; " >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Billing
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
</td>
	</tr>
		<tr>
		<td colspan="6">
			<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0">
			<tbody>
		<tr>
<td align="right" class="listwhitetext" width="">Pay&nbsp;Method</td>
<td align="left"><s:select cssClass="list-menu" name="contract.payMethod" list="%{paytype}"  onchange="changeStatus()" cssStyle="width:193px" headerKey="" headerValue="" tabindex="8"/></td>
	
<td align="right" class="listwhitetext"><fmt:message key='contract.billingInstructionCode'/></td>
<td><s:select cssClass="list-menu" name="contract.billingInstructionCode" list="%{billinginstruction}" headerKey=" " headerValue=" " cssStyle="width:193px" onchange="changeStatus();" tabindex="9"/></td>
       <!--<td align="left" class="listwhitetext"><s:textfield name="contract.end" required="true" size="40" maxlength="65" cssClass="input-text" /> </td>       
 -->
 <c:set var="isinternalCostChecked" value="false"/>
			<c:if test="${contract.internalCostContract}">
				 <c:set var="isInternalCostChecked" value="true"/>
            </c:if>
 <td align="right" class="listwhitetext">Internal&nbsp;Cost&nbsp;Contract</td>
 <td width="20"><s:checkbox name="contract.internalCostContract"  value="${isInternalCostChecked}" tabindex="10" cssStyle="margin:0px;padding:0px;" /> </td>
 <c:set var="isfXRateOnActualization" value="false"/>
			<c:if test="${contract.fXRateOnActualizationDate}">
				 <c:set var="isfXRateOnActualization" value="true"/>
            </c:if>
  <td align="left" width="70px" class="listwhitetext">FX&nbsp;Rate&nbsp;On&nbsp;Actualization</td>
 <td><s:checkbox name="contract.fXRateOnActualizationDate"  value="${isfXRateOnActualization}" tabindex="11" cssStyle="margin:0px;padding:0px;" /> </td>
 </tr>
 <tr>
 <td align="right" class="listwhitetext">Receivable&nbsp;Currency<font color="red" size="2">*</font></td>
 <td>
 	<s:select cssClass="list-menu" name="contract.contractCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:193px" tabindex="12" onchange="changeStatus();" />
 </td>
<td align="right" class="listwhitetext">Payable&nbsp;Currency<font color="red" size="2">*</font></td>
 <td>
 	<s:select cssClass="list-menu" name="contract.payableContractCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:193px" tabindex="13" onchange="changeStatus();" />
 </td>
<c:set var="isdmmInsurancePolicyChecked" value="false"/>
	<c:if test="${contract.dmmInsurancePolicy}">
	   <c:set var="isdmmInsurancePolicyChecked" value="true"/>
    </c:if>
 <td align="right" class="listwhitetext">DMM&nbsp;Insurance&nbsp;Policy</td>
 <td><s:checkbox name="contract.dmmInsurancePolicy"  value="${isdmmInsurancePolicyChecked}" cssClass="mc-0" tabindex="14" /> </td>
 </tr>
 <tr>
<configByCorp:fieldVisibility componentId="component.tab.contract.clientPostingGroup">
 <td align="right" class="listwhitetext">Client&nbsp;Posting&nbsp;Group</td>
 <td align="left" class="listwhitetext" ><s:textfield key="contract.clientPostingGroup"  cssClass="input-text" cssStyle="width:190px" size="40" maxlength="254" tabindex="15" onkeyup="regExMatch(this,event);"/></td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext">Account&nbsp;Code</td>
<td align="left" width="92"><s:textfield cssClass="input-text" id="accountCodeIdContractA" name="contract.accountCode"  cssStyle="width:60px;" tabindex="16" maxlength="8" onchange="valid(this,'special');changeStatus();findAccToName();"  />
 <img class="openpopup" style="vertical-align:top;"width="17" id="accountCodePopupImgContract" height="20" onclick="assignBillToCode();" src="<c:url value='/images/open-popup.gif'/>" /></td>
<td align="right" class="listwhitetext" style="width:100px;">Account&nbsp;Name</td>
<td align="left" colspan="3"><s:textfield cssClass="input-textUpper" id="accountNameIdContractA" name="contract.accountName" cssStyle="width:190px;"  tabindex="17" maxlength="250"  readonly="true"     />
 <div id="accountNameDivContract" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
</td>																
 </tr>
 <c:if test="${networkAgentFlag || cmmDmmAgentFlag}">
 <tr>
 <td align="right" class="listwhitetext"><fmt:message key='accountInfo.creditTerms' /></td>
 <td align="left">
 	<s:select cssClass="list-menu" name="contract.creditTerms" list="%{creditTermsList}" cssStyle="width:193px" tabindex="17" headerKey="" headerValue="" onchange="changeStatus();" />
 </td>
 <td align="right" class="listwhitetext">Credit&nbsp;Terms&nbsp;CMM&nbsp;Agent</td>
 <td align="left">
 	<s:select cssClass="list-menu" name="contract.creditTermsCmmAgent" list="%{creditTermsList}" cssStyle="width:193px" tabindex="17" headerKey="" headerValue="" onchange="changeStatus();" />
 </td>
 <td align="right" class="listwhitetext">Billing Moment</td>
 <td align="left" colspan="2">
 	<s:select cssClass="list-menu" name="contract.billingMoment" list="%{billingMomentList}" cssStyle="width:103px" tabindex="17" headerKey="" headerValue="" onchange="changeStatus();" />
 </td>
 </tr>
 </c:if>
</tbody>
</table>
	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
		<tbody>
			<tr>
				<td style="width:100%;">
			
				<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
				<tr>
				<td class="headtab_left">
				</td>
				<td class="headtab_center" >&nbsp;Discount Percents
				</td>
				<td width="28" valign="top" class="headtab_bg"></td>
				<td class="headtab_bg_center">&nbsp;
				</td>
				<td class="headtab_right">
				</td>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
			<td colspan="5">
			<table class="detailTabLabel" cellspacing="3" cellpadding="2" border="0">
			 <tbody>
			  <tr>
				<td align="right" class="listwhitetext"><fmt:message key='contract.trnsDisc'/></td>
				<td align="left" class="listwhitetext"><s:textfield name="contract.trnsDisc" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" required="true" size="4" maxlength="5" cssClass="input-text" tabindex="18"/> </td>
				<c:set var="ischecked" value="false"/>
				<c:if test="${contract.domesticSalesCommission}">
				<c:set var="ischecked" value="true"/>
				</c:if>
				<td align="left" class="listwhitetext" colspan="5">Domestic Sales Commission<s:checkbox key="contract.domesticSalesCommission" value="${ischecked}" fieldValue="true" tabindex="19" cssStyle="vertical-align:middle;"/></td>
			 </tr>
		     <tr>
               <td align="right" class="listwhitetext"><fmt:message key='contract.discount'/></td>
		       <td><s:textfield size="4" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="5" name="contract.discount"  cssClass="input-text" tabindex="20"/></td> 			 
               <td align="right" class="listwhitetext"><fmt:message key='contract.packDiscount'/></td>
		   	   <td><s:textfield size="4" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);"  onkeyup="valid(this,'special')" maxlength="5" name="contract.packDisc"  cssClass="input-text" tabindex="21"/>  </td> 			 
          	   <td></td><td align="right" class="listwhitetext"><fmt:message key='contract.minimumPay'/></td>
           	   <td><s:textfield size="4" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);"   onkeyup="valid(this,'special')" maxlength="5"  name="contract.minimumPay"  cssClass="input-text" tabindex="22"/>  </td> 			 
               <td align="right" class="listwhitetext"><fmt:message key='contract.tariff'/></td>
               <td><s:textfield size="4" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);"  onkeyup="valid(this,'special')" maxlength="5" name="contract.tariff"  cssClass="input-text" tabindex="23"/>  </td> 			 
               <td align="right" class="listwhitetext"><fmt:message key='contract.timeTo'/> <img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
               <td><s:textfield size="4" cssStyle="text-align:right"  maxlength="5"  name="contract.timeTo"  onkeydown="return onlyTimeFormatAllowed(event)"   onchange = "completeTimeString();" cssClass="input-text" tabindex="24"/>  </td> 			 
               <td align="right" class="listwhitetext"><fmt:message key='contract.timeFrom'/> <img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
               <td><s:textfield size="4" cssStyle="text-align:right"   onkeydown="return onlyNumsAllowed(event)"   maxlength="5" name="contract.timeFrom"   onchange = "completeTimeString();" cssClass="input-text" tabindex="25"/>  </td> 			            
		   </tr>
		   <tr>
	            <td align="right" class="listwhitetext"><fmt:message key='contract.sitDiscount'/></td>
	            <td><s:textfield cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="5" size="4" name="contract.sitDiscount"  cssClass="input-text" tabindex="26"/>  </td> 			 
	            <td align="right" class="listwhitetext"><fmt:message key='contract.otherDiscount'/></td>
			    <td><s:textfield cssStyle="text-align:right"  size="4" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="5"  name="contract.otherDiscount"  cssClass="input-text" tabindex="27"/></td>
			    <td colspan="4" align="right" class="listwhitetext"><fmt:message key='contract.section'/></td>			 
	            <td><s:textfield size="4" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="5"  name="contract.section"  cssClass="input-text" tabindex="28"/>  </td> 			 
		    </tr>
		    </tbody>
		    </table>
		    <tr>
		    <td>
		    <table class="tableNomarg">
		    <tbody>
		    <tr>
		      <td align="right" style="padding-left:30px;vertical-align:top;" class="listwhitetext"><fmt:message key='contract.details'/></td>
		     <td align="left" class="listwhitetext"><s:textarea  cssClass="textarea" name="contract.details" cssStyle="width:361px;height:80px;" onkeypress="return checkLength();" tabindex="29"/></td>
			</tr>
		     </tbody>
		     </table>
	<c:if test="${networkAgentFlag || cmmDmmAgentFlag}">
		<table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-bottom:1px;">
		<tbody>
		<tr>
		<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('insuranceSetup')" style="margin: 0px"  >
		<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
		<tr>
		<td class="headtab_left">
		</td>
		<td class="headtab_center" >&nbsp;Insurance&nbsp;Setup
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
		</table>
		</div>		
			<div id="insuranceSetup">
			<table class="tableNomarg" cellspacing="2" cellpadding="2" border="0">			
			<tr>
			<td align="right" class="listwhitetext" width="81">Detailed&nbsp;List</td>
			<td align="left"><s:checkbox name="contract.detailedList" cssClass="mc-0" tabindex="30" /></td>
			<td width="10"></td>
			<td align="right" class="listwhitetext" >No Insurance</td>
			<td align="left"><s:checkbox name="contract.noInsurance"  cssClass="mc-0" tabindex="31" /></td>
			<td width="32"></td>
			<td align="left"><s:checkbox name="contract.lumpSum"  cssClass="mc-0" tabindex="32" /></td>
			<td width="0"></td>
          	<td align="left" class="listwhitetext"><c:out value="${baseCurrencyValue}"/></td>
          	<td ><s:textfield name="contract.lumpSumAmount" cssClass="input-text" cssStyle="width:70px;" tabindex="33" /></td>
          	<td width="122"></td>
           	<td align="left" class="listwhitetext">Per</td>
           	<td width="67"><s:select cssClass="list-menu"   name="contract.lumpPerUnit" list="{'Kilo', 'CBM', 'CFT', 'LB'}" headerKey="" headerValue="" cssStyle="width:60px" tabindex="34" /></td>
            
            <td align="left" class="listwhitetext" width="85">Min&nbsp;Replacement&nbsp;value</td>
           	<td><s:textfield name="contract.minReplacementvalue" cssClass="input-text" cssStyle="width:94px;" tabindex="35" /></td>
           	
           	<td align="right" width="55" class="listwhitetext">VAT&nbsp;Desc</td>
			<td align="left"><s:select cssClass="list-menu" name="contract.defaultVat" list="%{euVatList}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:100px" tabindex="36" /></td>
			</tr>
			</table>
			<table class="tableNomarg" cellspacing="2" cellpadding="2" border="0">	
			<tr>
			<td align="right" width="81" class="listwhitetext" >Network</td>
			<td><s:textfield cssClass="input-text" key="contract.networkPartnerCode" cssStyle="width:55px;" tabindex="37" maxlength="8" onchange="valid(this,'special');checkNetworkPartnerName();"/></td>
			<td width=""><img align="left" class="openpopup" width="" height="20" onclick="javascript:winOpenPartnerNetwork();" id="openpopupnetwork.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left"><s:textfield name="contract.networkPartnerName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:195px;" tabindex="38" maxlength="255" /></td>			
			<td align="right" class="listwhitetext" width="145px">Vendor</td>					
			<td align="left" class="listwhitetext"><s:textfield key="contract.vendorCode" onchange="checkVendorName();" cssClass="input-text" readonly="false" cssStyle="width:55px;" tabindex="39" maxlength="8" /></td>
			<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenVendorCode()" id="openpopup5.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left"><s:textfield name="contract.vendorName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:195px;" tabindex="40" maxlength="255" /></td>
			<td width="15"></td>
			<td align="right" class="listwhitetext"><fmt:message key="partnerPrivate.source"/></td>
			<td><s:select cssClass="list-menu" name="contract.source" list="%{lead}" cssStyle="width:185px" tabindex="41" headerKey="" headerValue="" onchange="changeStatus();" /></td>
			</tr>
			</table>
			<table class="tableNomarg" cellspacing="2" cellpadding="2" border="0">	
			<tr>			
			<td align="right" class="listwhitetext"><fmt:message key="partnerPrivate.insuranceOptionCode"/></td>
			<td align="left"><s:select cssClass="list-menu" name="contract.insuranceOption" list="%{insopt}" cssStyle="width:286px" tabindex="42" onchange="" /></td>
			<td width="10"></td>
			<td align="left" class="listwhitetext">Account&nbsp;PO#/SAP&nbsp;Number</td>
			<td width=""><s:textfield cssClass="input-text" name="contract.billToAuthorization"  cssStyle="width:285px;" tabindex="43" maxlength="50" onblur="valid(this,'special')" /></td>
			
			</tr>		
		</table>
		</div>
		</td>
		</tr>
		</tbody>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;margin-bottom:1px;">
	<tbody>
	<tr>
	<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('accountingDefault')" style="margin: 0px"  >
		<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
		<tr>
		<td class="headtab_left">
		</td>
		<td class="headtab_center" >&nbsp;Accounting Default
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
		</table>
		</div>		
		<div id="accountingDefault">
		<table class="detailTabLabel" cellspacing="3" cellpadding="2" border="0">
			<tbody>
				<tr>								
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.storage' /></td>
				<td class="listwhitetext"><s:select cssClass="list-menu" name="contract.storageBillingGroup" list="%{billgrp}" cssStyle="width:262px" onchange="changeStatus();" tabindex="45" /></td>
				<td class="listwhitetext" width="143" align="right">Storage Email Address</td>
				<td><s:textfield cssClass="input-text" name="contract.storageEmail" maxlength="50" cssStyle="width:285px;" tabindex="46" onchange="checkEmail()" /></td>
				<td width="10"></td>
				<td align="right" class="listwhitetext">Default stationary</td>	
				<td><s:select cssClass="list-menu" name="contract.emailPrintOption" list="%{printOptions}" cssStyle="width:139px"  tabindex="47" ></s:select></td>
				</tr>
				
			</tbody>
		</table>
		</div>
		</td>
		</tr>
	</tbody>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-bottom:1px;">
	<tbody>
	<tr>
	<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('QualityMeasurement')" style="margin: 0px"  >
		<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
		<tr>
		<td class="headtab_left">
		</td>
		<td class="headtab_center" >&nbsp;Quality Measurement
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
		</table>
		</div>		
		<div id="QualityMeasurement">
		<table class="detailTabLabel" cellspacing="3" cellpadding="2" border="0">
			<tbody>
				<tr>								
				<td align="left" class="listwhitetext" width="150"><b><u>Customer&nbsp;Feedback</u></b></td>
				<td align="left" class="listwhitetext">
				<td><s:radio name="contract.customerFeedback" list="%{customerFeedbackList}" tabindex="48" /></td>
				</td>
				</tr>
				<tr><td height="5px"></td></tr>
				<tr>
				  <td colspan="10">
				  <fieldset class="bluefieldset" style="width:820px;">
				  <table class="detailTabLabel">
				  <tr>
				    <c:if test="${contract.noTransfereeEvaluation}">
					   <c:set var="ischeckedEvaluation" value="true" />
				    </c:if>
				    
				       <td align="right" class="listwhitetext" width="145px">No Transferee Evaluation</td>
				       <td class="listwhitetext" colspan="" width="10px"><s:checkbox key="contract.noTransfereeEvaluation" fieldValue="true" value="${ischeckedEvaluation}" onchange="changeStatus();" tabindex="49" /></td>
				      
				        <c:set var="ischeckeddddd" value="false" />
				        <c:if test="${contract.oneRequestPerCF}">
					       <c:set var="ischeckedCF" value="true" />
				       </c:if>
				       <td align="right" class="listwhitetext" width="145">One Request Per CF</td>
				       <td width="10px" align="left"><s:checkbox key="contract.oneRequestPerCF" onclick="changeStatus();" value="${ischeckedCF}" tabindex="50" /></td>
						<td width="45"></td>
						<td class="listwhitetext" >&nbsp;&nbsp;&nbsp;&nbsp;Rating Scale: </td><td><s:radio name="contract.ratingScale" list="%{ratingScaleList}" tabindex="51" /></td>
				 	</tr>
				  	</table>
				  	</fieldset>
				  	</td>
				  </tr>
			</tbody>
		</table>
		</div>
		</td>
		</tr>
	</tbody>
</table>
</c:if>
<table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
<tbody>
<tr>
	<td height="10" width="100%" align="left" >
	<div  onClick="javascript:animatedcollapse.toggle('valuation')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Default
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
	<div id="valuation">
	<table class="detailTabLabel" cellspacing="3" cellpadding="2" border="0">
	<tbody>
		<tr>
	<td align="right" class="listwhitetext"><fmt:message key='contract.insuranceHas'/></td>
	<td>
	  <s:select  name="contract.insuranceHas"   cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();" list= "{'Y','N'}"  required="true"  tabindex="52"/>
	</td>
	</tr>
	<tr>

	<td align="right" class="listwhitetext"><fmt:message key='contract.insvaluEntitled'/>&nbsp;${currencySign}</td>
	<td> <s:textfield  name="contract.insvaluEntitled" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="5" cssClass="input-text" tabindex="53"/>  </td> 			          
	</tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='contract.insoptCode'/></td>
	<td> <s:textfield name="contract.insoptCode" maxlength="10" cssClass="input-text" tabindex="54"/>  </td> 			          
	</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
 		  <table class="detailTabLabel" border="0" style="width:800px">
				<tbody>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<!--<tr>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.createdOn'/></b></td>
							<s:hidden name="contract.createdOn" />
							<td style="width:160px"><s:date name="contract.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.createdBy' /></b></td>
							<c:if test="${not empty contract.id}">
								<s:hidden name="contract.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{contract.createdBy}"/></td>
							</c:if>
							<c:if test="${empty contract.id}">
								<s:hidden name="contract.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.updatedOn'/></b></td>
							<s:hidden name="contract.updatedOn"/>
							<td style="width:160px"><s:date name="contract.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.updatedBy' /></b></td>
							<s:hidden name="contract.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:85px"><s:label name="contract.updatedBy" value="${pageContext.request.remoteUser}" /></td>
						</tr>
						
						--><tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.createdOn'/></b></td>
							<s:text id="contractcreatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="contract.createdOn" /></s:text>
							<td valign="top"><s:hidden name="contract.createdOn" value="%{contractcreatedOnFormattedValue}" /></td>
							<td style="width:150px"><fmt:formatDate value="${contract.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.createdBy' /></b></td>
							<c:if test="${not empty contract.id}">
								<s:hidden name="contract.createdBy"/>
								<td style="width:125px"><s:label name="createdBy" value="%{contract.createdBy}"/></td>
							</c:if>
							<c:if test="${empty contract.id}">
								<s:hidden name="contract.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:125px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.updatedOn'/></b></td>
							<s:text id="contractupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="contract.updatedOn" /></s:text>
							<td valign="top"><s:hidden name="contract.updatedOn" value="%{contractupdatedOnFormattedValue}" /></td>
							<td style="width:150px"><fmt:formatDate value="${contract.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.updatedBy' /></b></td>
							<c:if test="${not empty contract.id}">
								<s:hidden name="contract.updatedBy"/>
								<td ><s:label name="updatedBy" value="%{contract.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty contract.id}">
								<s:hidden name="contract.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table></td></tr>
        <s:submit cssClass="cssbutton" cssStyle="width:55px;" method="save" key="button.save" onmouseover="return chkSelect();" onclick="return validCurrency();return IsValidTime('save');return checkLengthOnSave();" tabindex="" />   
        <c:if test="${not empty contract.id}">    
        </c:if>   
       <s:reset cssClass="cssbutton" cssStyle="width:55px;" key="Reset" tabindex=""/>
	</div>
   </s:form> 
   
   <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('valuation', 'fade=1,hide=1')
	animatedcollapse.addDiv('insuranceSetup', 'fade=1,hide=1')
	animatedcollapse.addDiv('QualityMeasurement', 'fade=1,hide=1')
	animatedcollapse.addDiv('accountingDefault', 'fade=1,hide=1')
	animatedcollapse.init()
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

</script>  
   <script language="javascript" type="text/javascript">
   function checkEmail() {
		var status = false;     
		var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		     if (document.forms['contractForm'].elements['contract.storageEmail'].value.search(emailRegEx) == -1) {
		    	 document.forms['contractForm'].elements['contract.storageEmail'].value="";
		          alert("Please enter a valid email address.");
		     }
		     else {
		          status = true;
		     }
		     return status;
		}
   function checkNetworkPartnerName(){
	   var partnerCode = document.forms['contractForm'].elements['contract.networkPartnerCode'].value;
		if(partnerCode!=''){
			 new Ajax.Request('/redsky/networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode='+encodeURI(partnerCode),
					  {
					    method:'get',
					    onSuccess: function(transport){
					      var response = transport.responseText || "no response text";
					      var results = response.trim();
			                var res = results.split("#"); 
			                if(res.length>2){
			                	if(res[2] == 'Approved'){
			           				document.forms['contractForm'].elements['contract.networkPartnerName'].value = res[1];
				           		}else{
				           			alert("Network code is not approved" ); 
								    document.forms['contractForm'].elements['contract.networkPartnerName'].value="";
								    document.forms['contractForm'].elements['contract.networkPartnerCode'].value="";
				           		}
			               	}else {
			               	     alert("Network code is not valid" ); 
			                 	 document.forms['contractForm'].elements['contract.networkPartnerName'].value="";
			                 	 document.forms['contractForm'].elements['contract.networkPartnerCode'].value = "";
						   }
					    },
					    onFailure: function(){ 
						    }
					  });
		}else{
	    	document.forms['contractForm'].elements['contract.networkPartnerName'].value='';
	    }
	}
   function checkVendorName(){
	   var partnerCode = document.forms['contractForm'].elements['contract.vendorCode'].value;
		if(partnerCode!=''){
			 new Ajax.Request('/redsky/vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode='+encodeURI(partnerCode),
					  {
					    method:'get',
					    onSuccess: function(transport){
					      var response = transport.responseText || "no response text";
					      var results = response.trim();
			                var res = results.split("#"); 
			                if(res.length>2){
			                	if(res[2] == 'Approved'){
			           				document.forms['contractForm'].elements['contract.vendorName'].value = res[1];
				           		}else{
				           			alert("Vendor Code is not approved" ); 
								    document.forms['contractForm'].elements['contract.vendorName'].value="";
								    document.forms['contractForm'].elements['contract.vendorCode'].value="";
				           		}
			               	}else {
			               	     alert("Vendor code is not valid" ); 
			                 	 document.forms['contractForm'].elements['contract.vendorName'].value="";
			                 	 document.forms['contractForm'].elements['contract.vendorCode'].value = "";
						   }
					    },
					    onFailure: function(){ 
						    }
					  });
		}else{
	    	document.forms['contractForm'].elements['contract.vendorName'].value='';
	    }
	}
   function winOpenPartnerNetwork (){
		openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=contract.networkPartnerName&fld_code=contract.networkPartnerCode');
	}
   function winOpenVendorCode(){
   		openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=contract.vendorName&fld_code=contract.vendorCode');
   }
    function  checkLength(){
	var txt = document.forms['contractForm'].elements['contract.details'].value;
	if(txt.length >2000){
		document.forms['contractForm'].elements['contract.details'].value = txt.substring(0,2000);
		return false;
	}else{
		
		return true;
	}
}
function  checkLengthOnSave(){
	var txt = document.forms['contractForm'].elements['contract.details'].value;
	if(txt.length >2000){
		alert("Cannot enter more than 2000 characters in notes detail.");
		document.forms['contractForm'].elements['contract.details'].value = txt.substring(0,2000);
		return false;
	}else{
		return true;
	}
}   

function checkContract(clickType){
	if(checkJobValids('save'))
	{ 
   var date1 = document.forms['contractForm'].elements['contract.begin'].value;	 
   var date2 = document.forms['contractForm'].elements['contract.ending'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("End Date should be greater than  or equal to Begin Date.");
    document.forms['contractForm'].elements['contract.ending'].value='';
    return false;
  }
		
	if(document.forms['contractForm'].elements['contract.contract'].value==""){
	alert('Contract is required field.');
	return false;
	}
	if(document.forms['contractForm'].elements['contract.description'].value==""){
	alert('Description is required field.');
	return false;
	}
	if(document.forms['contractForm'].elements['contract.contractCurrency'].value==""){
	alert('Receivable Currency is required field.');
	return false;
	}
	if(document.forms['contractForm'].elements['contract.payableContractCurrency'].value==""){
		alert('Payable Currency is required field.');
		return false;
		}
	
	return IsValidTime(clickType);
	}
	}
	function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110) || (keyCode==67) || (keyCode==86); 
	}
	
   </script>
   
   
   <script language="JavaScript">	

	function onlyTimeFormatAllowed(evt)
		{
		
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
		}
	</script>
	
	
	<SCRIPT LANGUAGE="JavaScript">
	function completeTimeString() {
	
		stime1 = document.forms['contractForm'].elements['contract.timeTo'].value;
		stime2 = document.forms['contractForm'].elements['contract.timeFrom'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['contractForm'].elements['contract.timeTo'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['contractForm'].elements['contract.timeTo'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['contractForm'].elements['contract.timeTo'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['contractForm'].elements['contract.timeTo'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['contractForm'].elements['contract.timeTo'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['contractForm'].elements['contract.timeTo'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['contractForm'].elements['contract.timeFrom'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['contractForm'].elements['contract.timeFrom'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['contractForm'].elements['contract.timeFrom'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['contractForm'].elements['contract.timeFrom'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['contractForm'].elements['contract.timeFrom'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['contractForm'].elements['contract.timeFrom'].value = "0" + stime2;
			}
		}
	}
	
	
	
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function IsValidTime(clickType) {
if(document.forms['contractForm'].elements['contract.timeTo'].value!=""){
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['contractForm'].elements['contract.timeTo'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
//document.forms['contractForm'].elements['contract.timeTo'].value = '00:00';
document.forms['contractForm'].elements['contract.timeTo'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Begin Hours' time must between 0 to 23 (Hrs)");
//document.forms['contractForm'].elements['contract.timeTo'].value = '00:00';
document.forms['contractForm'].elements['contract.timeTo'].select();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Begin Hours' time must between 0 to 59 (Min)");
//document.forms['contractForm'].elements['contract.timeTo'].value = '00:00';
document.forms['contractForm'].elements['contract.timeTo'].select();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Begin Hours' time must between 0 to 59 (Sec)");
//document.forms['contractForm'].elements['contract.timeTo'].value = '00:00';
document.forms['contractForm'].elements['contract.timeTo'].select();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['contractForm'].elements['contract.timeFrom'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
//document.forms['contractForm'].elements['contract.timeFrom'].value = '00:00';
document.forms['contractForm'].elements['contract.timeFrom'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'End Hours' time must between 0 to 23 (Hrs)");
//document.forms['contractForm'].elements['contract.timeFrom'].value = '00:00';
document.forms['contractForm'].elements['contract.timeFrom'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'End Hours' time must between 0 to 59 (Min)");
//document.forms['contractForm'].elements['contract.timeFrom'].value = '00:00';
document.forms['contractForm'].elements['contract.timeFrom'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'End Hours' time must between 0 to 59 (Sec)");
//document.forms['contractForm'].elements['contract.timeFrom'].value = '00:00';
document.forms['contractForm'].elements['contract.timeFrom'].focus();
return false;
}
var date1 = document.forms['contractForm'].elements['contract.begin'].value;	 
   var date2 = document.forms['contractForm'].elements['contract.ending'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("End Date should be greater than  or equal to Begin Date.");
    document.forms['contractForm'].elements['contract.ending'].select();
    return false;
  }

}
return checkTime(clickType);
}
function checkTime(clickType)
{
	var tim1=document.forms['contractForm'].elements['contract.timeTo'].value;
	var tim2=document.forms['contractForm'].elements['contract.timeFrom'].value;
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	if(time1 > time2){
		alert("Begin hours cannot be less than end hours");
		//document.forms['contractForm'].elements['contract.timeFrom'].value = '00:00';
		document.forms['contractForm'].elements['contract.timeTo'].select();
		return false;
	}
	


	
	if(!(clickType == 'save')){
		if ('${autoSavePrompt}' == 'No'){
			var id1 = document.forms['contractForm'].elements['contract.id'].value;
			var contract = document.forms['contractForm'].elements['contract.contract'].value;
	
	if(document.forms['contractForm'].elements['formStatus'].value == '1'){
		document.forms['contractForm'].action = 'saveContract!saveOnTabChange.html';
		document.forms['contractForm'].submit();
	}else{
		if(id1 != ''){
		if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.contractList'){
				location.href = 'contracts.html';
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.chargeList'){
				location.href = 'charge23.html?id='+id1;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.accountList'){
				location.href = 'contractAccountList.html?id='+id1;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.materialList'){
				location.href = 'contractMaters.html?id='+id1+'&itemType=M&cont='+contract;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.equipmentList'){
				location.href = 'contractMaters.html?id='+id1+'&itemType=E&cont='+contract;
				}
		    if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.acctTemplateList'){
				location.href = 'acctTemplateList.html?id='+id1+'&itemType=E&contract='+contract;
				}			
		    if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.agentContractLists'){
				location.href = 'agentContractLists.html?id='+id1;
				}	
			}
		}
	}
	else{
	var id1 = document.forms['contractForm'].elements['contract.id'].value;
	var contract = document.forms['contractForm'].elements['contract.contract'].value;

	if (document.forms['contractForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='contractDetail.heading'/>");
		if(agree){
			document.forms['contractForm'].action = 'saveContract!saveOnTabChange.html';
			document.forms['contractForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.contractList'){
				location.href = 'contracts.html';
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.chargeList'){
				location.href = 'charge23.html?id='+id1;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.accountList'){
				location.href = 'contractAccountList.html?id='+id1;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.materialList'){
				location.href = 'contractMaters.html?id='+id1+'&itemType=M&cont='+contract;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.equipmentList'){
				location.href = 'contractMaters.html?id='+id1+'&itemType=E&cont='+contract;
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.acctTemplateList'){
				location.href = 'acctTemplateList.html?id='+id1+'&itemType=E&contract='+contract;
				}	
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.agentContractLists'){
				location.href = 'agentContractLists.html?id='+id1;
				}					
			}
		}
	}else{
		if(id1 != ''){
		if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.contractList'){
				location.href = 'contracts.html';
				}
			if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.chargeList'){
					location.href = 'charge23.html?id='+id1;
					}
				if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.accountList'){
					location.href = 'contractAccountList.html?id='+id1;
					}
				if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.materialList'){
					location.href = 'contractMaters.html?id='+id1+'&itemType=M&cont='+contract;
					}
				if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.equipmentList'){
					location.href = 'contractMaters.html?id='+id1+'&itemType=E&cont='+contract;
					}
				if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.acctTemplateList'){
				   location.href = 'acctTemplateList.html?id='+id1+'&itemType=E&contract='+contract;
				}	
				if(document.forms['contractForm'].elements['gotoPageString'].value == 'gototab.agentContractLists'){
					location.href = 'agentContractLists.html?id='+id1;
					}	
				}
			}
		}
	}
}
function isFloat(targetElement)
{   var i;
	var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        if (c == ".") {
        
        }else{
        alert("Only numbers are allowed here");
        //alert(targetElement.id);
        document.getElementById(targetElement.id).value='';
        document.getElementById(targetElement.id).select();
        return false;
        }
    }
    }
    return true;
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

function chkSelect()
	{ 
		if (checkFloat('contractForm','contract.trnsDisc','Invalid data in Transprtn') == false)
           {
              document.forms['contractForm'].elements['contract.trnsDisc'].focus();
              return false
           }
		if (checkFloat('contractForm','contract.discount','Invalid data in Discount') == false)
           {
              document.forms['contractForm'].elements['contract.discount'].focus();
              return false
           }
           
           if (checkFloat('contractForm','contract.packDisc','Invalid data in Pack/Unpack') == false)
           {
              document.forms['contractForm'].elements['contract.packDisc'].focus();
              return false
           }
           if (checkFloat('contractForm','contract.minimumPay','Invalid data in Min Bill Pay') == false)
           {
              document.forms['contractForm'].elements['contract.minimumPay'].focus();
              return false
           }
           if (checkFloat('contractForm','contract.tariff','Invalid data in Tariff') == false)
           {
              document.forms['contractForm'].elements['contract.tariff'].focus();
              return false
           }
           if (checkFloat('contractForm','contract.sitDiscount','Invalid data in Sit Discount') == false)
           {
              document.forms['contractForm'].elements['contract.sitDiscount'].focus();
              return false
           }
           if (checkFloat('contractForm','contract.otherDiscount','Invalid data in Other') == false)
           {
              document.forms['contractForm'].elements['contract.otherDiscount'].focus();
              return false
           }
           if (checkFloat('contractForm','contract.section','Invalid data in Section') == false)
           {
              document.forms['contractForm'].elements['contract.section'].focus();
              return false
           }
           if (checkFloat('contractForm','contract.insvaluEntitled','Invalid data in Entitled To $') == false)
           {
              document.forms['contractForm'].elements['contract.insvaluEntitled'].focus();
              return false
           }
	}
	
	function changeStatus(){
    document.forms['contractForm'].elements['formStatus'].value = '1';
}

	
function checkJobValidation(target){
var job=document.forms['contractForm'].elements['contract.jobType'].value;
job=job.trim();
if(job!=''){
var jobLength = job.length;
var reminder = ((jobLength-1)%6);
var startPoint = job.substring(0,2)
var endPoint = job.substring(jobLength-2,jobLength)
if(startPoint!="('")
{
    
	alert("Incorrect Job Type");
	document.forms['contractForm'].elements['contract.jobType'].select();
	return false;
}
else if(endPoint!="')")
{
	
	alert("Incorrect Job Type");
	document.forms['contractForm'].elements['contract.jobType'].select();
	return false;
}
else if(reminder > 0)
{
	alert("Incorrect Job Type");
	document.forms['contractForm'].elements['contract.jobType'].select();
	return false;
}
else if(target!='save')
{
	alert("Correct job type");
}
}
return true;
}

function checkJobValids(target){
var job=document.forms['contractForm'].elements['contract.jobType'].value;
job=job.trim();
if(job!=''){
var jobLength = job.length;
var reminder = ((jobLength-1)%6);
var startPoint = job.substring(0,2)
var endPoint = job.substring(jobLength-2,jobLength)
if(startPoint!="('")
{	
	
	alert("Incorrect Job Type");
	document.forms['contractForm'].elements['contract.jobType'].select();
	return false;
}
else if(endPoint!="')")
{
	
	alert("Incorrect Job Type");
	document.forms['contractForm'].elements['contract.jobType'].select();
	return false;
}
else if(reminder > 0)
{
	
	alert("Incorrect Job Type");
	document.forms['contractForm'].elements['contract.jobType'].select();
	return false;
}
else if(target!='save')
{
	alert("Correct job type");
}
}
return true;
}

function validCurrency(){
	var curr = document.forms['contractForm'].elements['contract.contractCurrency'].value;
	if(curr==''){
		alert('Please fill in the Receivable Currency to save the Contract.');
		return false;
	}
	if(document.forms['contractForm'].elements['contract.payableContractCurrency'].value==""){
		alert('Please fill in the Payable Currency to save the Contract.');
		return false;
		}
}

function checkReadOnly(){
    var chkCorpId= '${contract.owner}';
    var corpId='${contract.corpID}';
  
    if(corpId!=chkCorpId){
      var len = document.forms['contractForm'].length;
      var calLen = document.getElementsByTagName("img");
      for(var j=0; j<calLen.length; j++){
			if(calLen[j].getAttribute('id') == "begin_trigger" || calLen[j].getAttribute('id') == "ending_trigger"){
				calLen[j].id = "";
			}
      }
      for (var i=0 ;i<len ;i++){ 
    	  document.forms['contractForm'].elements[i].disabled = true;
      }
      var totalImages = document.images.length;
    	for (var i=0;i<totalImages;i++) {
    		if(document.images[i].src.indexOf('open-popup.gif')>0) {  
				var el = document.getElementById(document.images[i].id); 
				try{
				el.onclick = false;
				}catch(e){}
			    document.images[i].src = 'images/navarrow.gif';
		}
    	}
   }
   		document.forms['contractForm'].elements['contract.contract'].readOnly =true;
}

function chhkCmmDmm(){
	var chkPublished = document.forms['contractForm'].elements['contract.published'].value;
	if(chkPublished=='Y'){
		alert('This Contract has been Published.Cannot change the Contract type.');
		document.forms['contractForm'].elements['contract.contractType'].value='${contract.contractType}';
		return false;
		}else{
			if((document.forms['contractForm'].elements['contract.contractType'].value=='CMM' || document.forms['contractForm'].elements['contract.contractType'].value=='DMM') && document.forms['contractForm'].elements['contract.accountCode'].value !='' ){
				
				checkNetworkAccountName();
			}
		}
}
function checkNetworkAccountName(){ 
	var networkBillToCode = document.forms['contractForm'].elements['contract.accountCode'].value;
	networkBillToCode=networkBillToCode.trim();
    if(networkBillToCode==''){
      document.forms['contractForm'].elements['contract.accountName'].value="";
    }
	if(networkBillToCode!=''){
	    var url="findNetworkBillToName.html?ajax=1&decorator=simple&popup=true&networkPartnerCode=''&networkBillToCode=" + encodeURI(networkBillToCode);
	    dmmTypeContractHTTP.open("GET", url, true);
	    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseNetworkBillToName;
	    dmmTypeContractHTTP.send(null);
    }
}
function handleHttpResponseNetworkBillToName(){
	if (dmmTypeContractHTTP.readyState == 4){
        var results = dmmTypeContractHTTP.responseText
        results = results.trim();
        var res = results.split("#"); 
        if(res.length >= 2){ 
        		if(res[2] == 'Approved'){
        			document.forms['contractForm'].elements['contract.accountName'].value = res[1];
        		}else{
        			alert("Account code is not approved as per pricing contract" ); 
        			document.forms['contractForm'].elements['contract.accountCode'].value=""; 
        			document.forms['contractForm'].elements['contract.accountName'].value=""; 
        		}			
        }else{
            alert("Account code not valid as per pricing contract");
            document.forms['contractForm'].elements['contract.accountCode'].value="";
            document.forms['contractForm'].elements['contract.accountName'].value="";
		   }  } 
}
function findAccToName(){ 
   
	var billToCode = document.forms['contractForm'].elements['contract.accountCode'].value;
    if(billToCode==''){
    	document.forms['contractForm'].elements['contract.accountName'].value="";
    }
    if(billToCode!=''){
    if((document.forms['contractForm'].elements['contract.contractType'].value=='CMM' || document.forms['contractForm'].elements['contract.contractType'].value=='DMM') ){
    	checkNetworkAccountName();	
     }else{
     var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponseAcc;
     http2.send(null);
    	}
   }  
}

function handleHttpResponseAcc(){
       if (http2.readyState == 4){
          var results = http2.responseText
          results = results.trim();
          var res = results.split("#");  
          if(res.length>= 2){ 
           		if(res[2] == 'Approved'){ 
           				document.forms['contractForm'].elements['contract.accountName'].value = res[1];
           				document.forms['contractForm'].elements['contract.accountCode'].select();
		         
           		}else{
           			alert("Account code is not approved" ); 
				    document.forms['contractForm'].elements['contract.accountCode'].value="";
					document.forms['contractForm'].elements['contract.accountName'].value="";
					document.forms['contractForm'].elements['contract.accountCode'].select();
           		}
           					
          }else{
               alert("Account code not valid");    
			   document.forms['contractForm'].elements['contract.accountCode'].value="";
			   document.forms['contractForm'].elements['contract.accountName'].value="";
			   document.forms['contractForm'].elements['contract.accountCode'].select();
		   }   }  }
function assignBillToCode(){ 
	<c:if test="${checkUTSI==true || tenancyBillingContractType==true}">
	if(document.forms['contractForm'].elements['contract.contractType'].value=='CMM' || document.forms['contractForm'].elements['contract.contractType'].value=='DMM' ){
	javascript:openWindow('findNetworkBillToCodeList.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=contract.accountName&fld_code=contract.accountCode');
	}else{
	javascript:openWindow("partnersPopup.html?partnerType=AC&flag=0&firstName=&lastName=&phone=&email=&compDiv=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=contract.accountName&fld_code=contract.accountCode&fld_seventhDescription=seventhDescription");
	}
	</c:if>
	<c:if test="${!(checkUTSI==true || tenancyBillingContractType==true)}">
	javascript:openWindow("partnersPopup.html?partnerType=AC&flag=0&firstName=&lastName=&phone=&email=&compDiv=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=contract.accountName&fld_code=contract.accountCode&fld_seventhDescription=seventhDescription");
	</c:if>
}		   
function checkOldContract (target){	
	   var cCon = document.forms['contractForm'].elements['contract.contract'].value; 
	   if(cCon!='') { 
		   var url="findOldContract.html?ajax=1&decorator=simple&popup=true&cCon="+encodeURI(cCon);
		   http51.open("GET", url, true); 
		   http51.onreadystatechange = handleHttp90; 
		   http51.send(null); 
	   }
	 }
function handleHttp90(){
    if (http51.readyState == 4){
      var results = http51.responseText;
      results = results.trim();
      results=results.replace('[','');
      results=results.replace(']','');
      if(results != ''){    	  
        	alert('Contract with the same name already exist. Please select another name.');
      	document.forms['contractForm'].elements['contract.contract'].value='';
      }else{
    	  document.forms['contractForm'].elements['contract.contract'].selected=true;
      }
    }
} 
var dmmTypeContractHTTP = getHTTPObject();
var http2 = getHTTPObject();
var http51 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function regExMatch(element,evt){ 
	if(!element.readOnly){
		var key='';
		if (window.event)
			 key = window.event.keyCode;
		if (evt)
			  key = evt.which;
		var alphaExp = /^[a-zA-Z0-9-_\.'&'\\'&'\/'&'\"'&'\+'&'\,'&'\''\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\='\' ']+$/;
		if(element.value.match(alphaExp)){
			return true;
		}
		else if ((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27)||(key==20)||(key==16)||(key==17)||(key==32)||(key==18)||(key==27)||(key==35)||(key==36)||(key==45)||(key==39)||(key==37)||(key==38)||(key==40)||(key==116)){
			   return true;
		}
		else{ 
			return true;
		}
	}else{
		return false;
	}
}
function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
	
}
</script>
 
<script> 
   try{
	   <c:if test="${not empty contract.id}">    
	  	   checkReadOnly();
   </c:if>
   }catch(e){}
	 <c:if test ="${hitFlag=='1'}">
	    <c:redirect url="/editContract.html?id=${contract.id}"/>
	</c:if>
</script>
	
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>