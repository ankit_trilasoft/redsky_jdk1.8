<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
    <title><fmt:message key="dataExtracts.title"/></title>
    <meta name="heading" content="<fmt:message key='reportsDataExtact.heading'/>"/> 	
	<style>
.rounded-top {  -moz-border-radius-topleft: .8em;  -webkit-border-top-left-radius:.8em;  -moz-border-radius-topright: .8em;  -webkit-border-top-right-radius:.8em;
   border:1px solid #AFAFAF;border-bottom:none;  
   }
.rounded-bottom { -moz-border-radius-bottomleft: .8em; -webkit-border-bottom-left-radius:.8em; -moz-border-radius-bottomright: .8em; -webkit-border-bottom-right-radius:.8em;
   border:1px solid #AFAFAF; border-top:none;   
}
.subcontenttabChild{  padding: 2px 3px 1px 5px;}	
</style> 
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"djConfig="parseOnLoad:true, isDebug:false"></script>
<%--
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&visible=100&amp;key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRtKhpDtLX6R5MagO3ZOu0_GHeYthTZmolg4KuvVTJhWVj_M3YuGSUoeA" type="text/javascript"></script>  	
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
--%>
	
<script language="JavaScript">
	var geocoder = null;
	var map;
	var address;
window.onload = function() { 
		 //map = new GMap2(document.getElementById("map"));
		  //geocoder = new GClientGeocoder();
		  //getZipPoint();
	}
function progressBar(tar){
showOrHide(tar);
}

function initLoader() { 
  var script = document.createElement("script");    
  script.src = "http://maps.google.com/maps?file=api&v=2&key=ABQIAAAA70eRL8LPvVpGN9OFgwemRhR8QE5-94Soff2YIV4DvwlRcsG7-RR-oVgkG9dLgBnFoHS-x6EjDA11PQ&async=2&callback=loadMap";
  script.type = "text/javascript"; 
  document.getElementsByTagName("head")[0].appendChild(script); 
  //alert('hi');
}
function waitForMapToLoad(){
map = new GMap2(document.getElementById("map"));
geocoder = new GClientGeocoder();
//alert('hi');
}
function loadMap() { 
map = new GMap2(document.getElementById("map"));
geocoder = new GClientGeocoder();
}



function showOrHide(value) {
    if (value==0) { 
            document.getElementById("layerH").style.display="none";             
             }
   else if (value==1) {
   document.getElementById("layerH").style.display="block";
  }
}
function getZipOriPoint(){        
        showOrHide(1);		
		dojo.xhrPost({
	       form: "reportDataExtractForm",
	       url:"zipPortsPlacement.html",
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
  			reinitMarkers();
  			var i=0;
  			if(jsonData.count>0)
  			{
	           for(i=0; i < jsonData.ZipCode.length; i++){
	        	   console.log("lattitude: " + jsonData.ZipCode[i].latitude + ", longitude: " +  jsonData.ZipCode[i].longitude);
	        	   point = new GLatLng(jsonData.ZipCode[i].latitude, jsonData.ZipCode[i].longitude);
	        	   addZipCoordsToMap(point, jsonData.ZipCode[i]);
	        	   var delme = 0;
	        	  if(i==jsonData.ZipCode.length-1)
	        	  {
	        	  alert("Only 50 Zip Code locations are being displayed.");
	        	  showOrHide(0);
	        	  }
	        	   }
	        	   }
	        	   else{
	        	   alert("No Record Found.");
	        	   map = new GMap2(document.getElementById("map"));
		           geocoder = new GClientGeocoder();
	        	   showOrHide(0);
	        	   }
	        	}
	       	       
	   });	  
	}
	
	
	
	function getZipDestPoint(){
        showOrHide(1);		
		dojo.xhrPost({
	
	       form: "reportDataExtractForm",
	       url:"zipPortsDestPlacement.html",
	       
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
			  reinitMarkers();
			  if(jsonData.count>0)
			  {
	           for(var i=0; i < jsonData.ZipdestCode.length; i++){ 
	        	   console.log("lattitude: " + jsonData.ZipdestCode[i].latitude + ", longitude: " +  jsonData.ZipdestCode[i].longitude);
	        	   point = new GLatLng(jsonData.ZipdestCode[i].latitude, jsonData.ZipdestCode[i].longitude);
	        	   addZipDestCoordsToMap(point, jsonData.ZipdestCode[i]);
	        	   var delme = 0;
	        	   if(i==jsonData.ZipdestCode.length-1)
	        	  {
	        	  alert("Only 50 Zip Code locations are being displayed.");
	        	 showOrHide(0);
	        	  }
	        	}
	        	}
	        	else{
	        	   alert("No Record Found.");
	        	   map = new GMap2(document.getElementById("map"));
		           geocoder = new GClientGeocoder();
	        	   showOrHide(0);
	        	}
	       }	       
	   });
	   }
	
	
	
	
	function reinitMarkers(){

	   	  map.clearOverlays();
	   

   }
function addZipCoordsToMap(point, ZipDetails){
	      // Create a marker
	     // marker = new GMarker(point);
	      // Add the marker to map
	      //map.addOverlay(marker);
		// Create our "tiny" marker icon
		var yellowIcon = new GIcon(G_DEFAULT_ICON);
		//yellowIcon.image = "${pageContext.request.contextPath}/images/markerP-yellow.png";
		// Set up our GMarkerOptions object
		markerOptions = { icon:yellowIcon };
	    var marker = new GMarker(point, markerOptions);
	     //alert("After Marker");
		 map.addOverlay(marker, markerOptions);
		 map.setCenter(point, 7);
		  map.setUIToDefault();
		  map.disableScrollWheelZoom();
		  
		 //alert("2");
	     // GEvent.addListener(marker, "click", function()
		//{marker.openInfoWindowHtml(''+portDetails.portcode+', '+portDetails.name+'');});
		 var fn = markerClickFnPorts(point, ZipDetails);
	     GEvent.addListener(marker, "click", fn);
	  }	
	function markerClickFnPorts(point, ZipDetails) {
	   return function() {
		   if (!ZipDetails) return;
	       var originZip = ZipDetails.originZip;
	       var originZipCountNo = ZipDetails.originZipCountNo;
	       var infoHtml = '<div style="width:100%;"><h3>Zip Code: '+originZip+'<Br>Shipment Count: '+originZipCountNo+'</h3>'+
	       '<td></td>';
	            
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }
   
   function addZipDestCoordsToMap(point, ZipDestDetails){
	      // Create a marker
	     // marker = new GMarker(point);
	      // Add the marker to map
	      //map.addOverlay(marker);
		// Create our "tiny" marker icon
		
		var yellowIcon = new GIcon(G_DEFAULT_ICON);
		//yellowIcon.image = "${pageContext.request.contextPath}/images/markerP-yellow.png";
		// Set up our GMarkerOptions object
		markerOptions = { icon:yellowIcon };
	    var marker = new GMarker(point, markerOptions);
	     //alert("After Marker");
		 map.addOverlay(marker, markerOptions);
		 map.setCenter(point, 9);
		 map.setUIToDefault();
		 map.disableScrollWheelZoom();
		 
		 //alert("2");
	     // GEvent.addListener(marker, "click", function()
		//{marker.openInfoWindowHtml(''+portDetails.portcode+', '+portDetails.name+'');});
		 var fn = markerClickDestFnPorts(point, ZipDestDetails);
	     GEvent.addListener(marker, "click", fn);
	  }	
	function markerClickDestFnPorts(point, ZipDestDetails) {
	   return function() {
		   if (!ZipDestDetails) return;
	       var destZip = ZipDestDetails.destinationZip;
	       var destZipCountNo = ZipDestDetails.destZipCountNo;
	       var infoHtml = '<div style="width:100%;"><h3>Zip Code: '+destZip+'<Br>Shipment Count: '+destZipCountNo+'</h3>'+
	       '<td></td>';
	            
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);
	   }
   }
   
function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {  
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    } 
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{ 	
	  var keyCode = evt.which ? evt.which : evt.keyCode; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

	<script language="javascript" type="text/javascript">
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();  
    var http3 = getHTTPObject();  
    var http4 = getHTTPObject();  
    
function findBillToCode()
{
     
     var billToCode = document.forms['reportDataExtractForm'].elements['billToCodeType'].value; 
     billToCode=billToCode.trim();
     billToCode=billToCode.replace( ':' , '' ); 
     if(billToCode!=''){
     progressBarAutoSave('1');
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(billToCode); 
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpRespon;
     http2.send(null);
     }
}
function handleHttpRespon()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					
                 }
                 else
                 {
                     alert("Invalid  Bill To code, please select another");
                     document.forms['reportDataExtractForm'].elements['billToCodeType'].value = "";
					
                 }
             }
             progressBarAutoSave('0');
        }
        
        
function findAccountCode()
{
     
     var accountCode = document.forms['reportDataExtractForm'].elements['accountCodeType'].value; 
     accountCode=accountCode.trim();
     accountCode=accountCode.replace( ':' , '' ); 
     if(accountCode!=''){
     progressBarAutoSave('1');
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(accountCode); 
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     }
}
function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					
                 }
                 else
                 {
                     alert("Invalid Account code, please select another");
                     document.forms['reportDataExtractForm'].elements['accountCodeType'].value = "";
					
                 }
             }
             progressBarAutoSave('0');
        }
        
 function findBookingAgentCode(){
          
         var bookingAgentCode = document.forms['reportDataExtractForm'].elements['bookingCodeType'].value;
         bookingAgentCode=bookingAgentCode.trim();
         bookingAgentCode=bookingAgentCode.replace( ':' , '' );
         if(bookingAgentCode!=''){
         progressBarAutoSave('1');
         var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
         http2.open("GET", url, true);
         http2.onreadystatechange = handleHttpResponse4;
         http2.send(null);
         }
  }
  
  function createTextAreaSave(){ 
   var answer = prompt ("Enter Query Name",""); 
   return answer; 
  } 
  function createTextAreaSave1(){    
	  getIdList();
	document.forms['reportDataExtractForm'].elements['selectCondition'].value; 
	//alert(document.forms['reportDataExtractForm'].elements['selectCondition'].value)
  if(document.forms['reportDataExtractForm'].elements['id'].value==""){
  var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);
      		var check=formValidate('none');
      		if(check){
		  if (btn == null) 
		   {
	          alert("Please select any radio button."); 
	       	  return true;
	       }
	      else{
	        if(document.forms['reportDataExtractForm'].elements['selectCondition'].value=='')
              {
                alert("Please select columns to be extract.")
               return true;
              }
            else{ 
             openWindow('extractSupport.html?&decorator=popup&popup=true',500,300);
             return true;
           }
           }
  }
  }
  if(document.forms['reportDataExtractForm'].elements['id'].value!=""){ 
  if(document.forms['reportDataExtractForm'].elements['autoGenerator'].value=='true') {
if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value==""){
alert("Plese select scheduling criteria");
return false ;
}else{
if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="M"){
if(document.forms['reportDataExtractForm'].elements['perMonthScheduler'].value==""){
alert("Plese select monthly scheduler");
return false ;
} 
}
if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="W"){
if(document.forms['reportDataExtractForm'].elements['perWeekScheduler'].value==""){
alert("Plese select weekly scheduler");
return false ;
} 
}
}
if(document.forms['reportDataExtractForm'].elements['email'].value==""){
alert("Plese enter E-mail");
return false ;
}
}else{
document.forms['reportDataExtractForm'].elements['queryScheduler'].value="";
document.forms['reportDataExtractForm'].elements['perWeekScheduler'].value=""
document.forms['reportDataExtractForm'].elements['perMonthScheduler'].value=""
}
  checkQueryName();  
  }
  } 
  
  function handleHttpResponse4()
        { 
          if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                { 	
 				}
                 else
                 {
                     alert("Invalid Booking Agent code, please select another");
                     document.forms['reportDataExtractForm'].elements['bookingCodeType'].value="";
					 
                 }
             }
              progressBarAutoSave('0');
    } 
    
 function valButton(btn) {
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) {
        if (btn[i].checked) {cnt = i; i = -1;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null;
} 
function  showColumns(targetElement){  
	/* if(targetElement.id = 'RelocationAnalysis'){
		jobType = document.getElementById('reportDataExtractForm_jobTypes').value;
		alert(jobType)
		if(jobType != 'RLO'){
			alert("Please Select Job Type is Relocation Services");
			document.getElementById('RelocationAnalysis').checked = false;
			return false;
			
		}
	} */
			
	if(document.forms['reportDataExtractForm'].elements['id'].value!=""){
            alert("You cannot change report type")
            document.getElementById('${reportName}').checked="checked";
            }else{
            var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']); 
            document.forms['reportDataExtractForm'].elements['reportName'].value=targetElement.id; 
      		document.forms['reportDataExtractForm'].action ='getDataExtractColumn.html?columnButton=yes';
            document.forms['reportDataExtractForm'].submit(); 
            }  
}

function getIdList(){
	var len = document.forms['reportDataExtractForm'].elements['checkV'].length;
	var idList='';
	for (i = 0; i < len; i++){
		if(document.forms['reportDataExtractForm'].elements['checkV'][i].checked == true){
    		idList =  idList+'#'+document.forms['reportDataExtractForm'].elements['checkV'][i].value;		
        }	
	}
	 document.forms['reportDataExtractForm'].elements['selectCondition'].value = idList;
}

    function findPreviewBillingReport(){ 
    	 <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 		
  	  		document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=false;  			
  	  		document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=false;
	  		document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=false;  		
  	  		document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=false; 
  	  	    document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=false;
  	  	</sec-auth:authComponent>		
    		getIdList();
            document.forms['reportDataExtractForm'].elements['selectCondition'].value;              
      		var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);
      		var check=formValidate('none');
      		if(check){
      		if(document.forms['reportDataExtractForm'].elements['selectCondition'].value=='')
              {
                alert("Please select columns to be extract.")
               return false;
              }
		  if (btn == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else if(btn ==1)
	       {
           		  if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
           		    document.forms['reportDataExtractForm'].action ='dynamicActiveShip.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 
           	      var testArmstrong='${armstrongPortal}'
           	    	  if(testArmstrong=='true'){ 
           	    		document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;  		
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=true;
           	    	  } else{
           	    		  document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;  			
           	    		  document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
           	    	  }
           	  	  </sec-auth:authComponent>
           		    return true;
           		 } 
        	}
        	else if(btn ==4)
        	{
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='dynamicDataFinancialSummary.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 
           	         var testArmstrong='${armstrongPortal}'
           	    	  if(testArmstrong=='true'){ 
           	    		document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;  		
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=true;
           	    	  } else{
           	    		  document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;  			
           	    		  document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
           	    	  }
           	  	  </sec-auth:authComponent>
        	   	    return true; 
        	   	 }
        	}
        	else if(btn ==7)
        	{
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='dynamicDataFinancialDetails.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 
           	          var testArmstrong='${armstrongPortal}'
           	    	  if(testArmstrong=='true'){ 
           	    		document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;  		
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=true;
           	    	  } else{
           	    		  document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;  			
           	    		  document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
           	    	  }
           	  	  </sec-auth:authComponent>
        	   	    return true; 
        	   	 }
        	}  
        	else if(btn ==8)
        	{
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {    
        	        document.forms['reportDataExtractForm'].action ='dynamicDataDomesticAnalysis.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 
           	      var testArmstrong='${armstrongPortal}'
           	    	  if(testArmstrong=='true'){ 
           	    		document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;  		
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=true;
           	    	  } else{
           	    		  document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;  			
           	    		  document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
           	    	  }
           	  	  </sec-auth:authComponent>
        	   	    return true; 
        	   	 }
        	} 
        	
        	else if(btn ==9){
        	     if(check==false){
           		    return false;
           		 }else{     
        	        document.forms['reportDataExtractForm'].action ='dynamicDataStorageAnalysis.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 
           	      var testArmstrong='${armstrongPortal}'
           	    	  if(testArmstrong=='true'){ 
           	    		document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;  		
           	    			document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=true;
           	    	  } else{
           	    		  document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;  			
           	    		  document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
           	    	  }
           	  	  </sec-auth:authComponent>
        	   	    return true; 
        	   	 }
        	}
		  
		  
        	 else if(btn == 10)
             {
                    if(check==false)
                    {
                      return false;
                    }
                    else
                    {
                      document.forms['reportDataExtractForm'].action ='dynamicDataReloAnalysis.html';
                      document.forms['reportDataExtractForm'].submit();
                      <sec-auth:authComponent componentId="module.script.form.corpSalesScript"> 
                    var testArmstrong='${armstrongPortal}'
                        if(testArmstrong=='true'){ 
                          document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
                              document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;         
                              document.forms['reportDataExtractForm'].elements['bookingCodeType'].disabled=true;
                        } else{
                            document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;              
                            document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
                        }
                    </sec-auth:authComponent>
                      return true;
                   } 
              }
		  
        	else{
        	  alert("Under Processing");
        	  //document.getElementById("hid").style.visibility="collapse";
        	  return false;
        	}   	
	   	}
  	
   } 
 function findPreviewBillingReportNew(){ 
      		var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);  
      		var check=formValidate('none');
      		if(check){
		    if (btn == null) 
		      {
	            alert("Please select any radio button"); 
	       	    return false;
	           } 
	          else if(btn ==1)
	           { 
           		  if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {  
           		    document.forms['reportDataExtractForm'].action ='saveDataExtractQuery.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    return true;
           		 } 
        	}
        	else if(btn ==4)
        	{ 
        	 if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='saveDataExtractQuery.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}
        	else if(btn ==7)
        	{ 
        	 if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='saveDataExtractQuery.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}  
        	else if(btn ==8)
        	{ 
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {           		   
        	        document.forms['reportDataExtractForm'].action ='saveDataExtractQuery.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	} 
        	
        	else if(btn ==9){ 
        	     if(check==false){
        	      alert("Please enter Query Name");
           		    return false;
           		 }else{           		   
        	        document.forms['reportDataExtractForm'].action ='saveDataExtractQuery.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}
		    
        	else if(btn ==10){ 
                if(check==false){
                 alert("Please enter Query Name");
                   return false;
                }else{                    
                   document.forms['reportDataExtractForm'].action ='saveDataExtractQuery.html';
                   document.forms['reportDataExtractForm'].submit();
                   return true; 
                }
           }
		    
        	else{
        	  alert("Under Processing"); 
        	  return false;
        	}   	
	  } 	
   }
   
    function formValidate(clickType){    
    
    	
      <c:if test="${companyDivisionFlag=='Yes'}">
      
    	if(document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value != ""       
    	      || document.forms['reportDataExtractForm'].elements['billToCodeCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['routingCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['modeCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['packModeCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['commodityCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['coordinatorCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['salesmanCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['oACountryCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['dACountryCondition'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['actualWgtCondition'].value != ""   || document.forms['reportDataExtractForm'].elements['actualVolumeCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['jobTypes'].value != ""     
    	    || document.forms['reportDataExtractForm'].elements['billToCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['originAgentCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['destinationAgentCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != ""|| document.forms['reportDataExtractForm'].elements['routingTypes'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['modeType'].value != ""      || document.forms['reportDataExtractForm'].elements['packModeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['commodityType'].value != "" || document.forms['reportDataExtractForm'].elements['coordinatorType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['oACountryType'].value != "" || document.forms['reportDataExtractForm'].elements['salesmanType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['dACountryType'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "" || document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['companyCodeCondition'].value != "" || document.forms['reportDataExtractForm'].elements['companyCode'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['carrierCondition'].value != "" || document.forms['reportDataExtractForm'].elements['driverIdCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['carrier'].value != "" || document.forms['reportDataExtractForm'].elements['driverId'].value != "" 
    		|| document.forms['reportDataExtractForm'].elements['accountCodeCondition'].value != ""
    		|| document.forms['reportDataExtractForm'].elements['consultantCondition'].value != "")
    	{	    
    	    
    	if(document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value != "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value == ""){
    	
    		alert("Job type value is blank, Please select."); 
          	return false;
    		
    	}
    	else if(document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value == "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value != "")
    	{
    		    alert("Job type condition is blank, Please select."); 
          	    return false;
    		
    	}  
    	else if(document.forms['reportDataExtractForm'].elements['billToCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value == "")
    	{
    		alert("BillToCode type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['billToCodeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value != "")
    	{
    		alert("BillToCode type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['originAgentCondition'].value != "" && document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value == "")
    	{
    		alert("OriginAgentCode type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['originAgentCondition'].value == "" && document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value != "")
    	{
    		alert("OriginAgentCode type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['destinationAgentCondition'].value != "" && document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value == "")
    	{
    		alert("DestinationAgentCode type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['destinationAgentCondition'].value == "" && document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value != "")
    	{
    		alert("DestinationAgentCode type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value == "")
    	{
    		alert("BookingCode value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != "")
    	{
    		alert("BookingCode condition is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['routingCondition'].value != "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value == "")
    	{
    		alert("Routing type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['routingCondition'].value == "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value != "")
    	{
    		alert("Routing types condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['modeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['modeType'].value == "")
    	{
    		alert("Mode type value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['modeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['modeType'].value != "")
    	{
    		alert("Mode type condition is blank, Please select."); 
    		return false;
    	} 
    	else if(document.forms['reportDataExtractForm'].elements['companyCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['companyCode'].value == "")
    	{
    		alert("Company Code value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['companyCodeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['companyCode'].value != "")
    	{
    		alert("Company Code condition is blank, Please select."); 
    		return false;
    	}   
    	else if(document.forms['reportDataExtractForm'].elements['packModeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['packModeType'].value == "")
    	{
    		alert("PackMode value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['packModeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['packModeType'].value != "")
    	{
    		alert("PackMode condition is blank, Please select."); 
    		return false;
    	}
    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['commodityCondition'].value != "" && document.forms['reportDataExtractForm'].elements['commodityType'].value == "")
    	{
    		alert("Commodity  type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['commodityCondition'].value == "" && document.forms['reportDataExtractForm'].elements['commodityType'].value != "")
    	{
    		alert("Commodity type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['coordinatorCondition'].value != "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value == "")
    	{
    		alert("Coordinator type value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['coordinatorCondition'].value == "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value != "")
    	{
    		alert("Coordinator types condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['consultantCondition'].value != "" && document.forms['reportDataExtractForm'].elements['consultantType'].value == "")
    	{
    		alert("consultant type value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['consultantCondition'].value == "" && document.forms['reportDataExtractForm'].elements['consultantType'].value != "")
    	{
    		alert("consultant types condition is blank, Please select."); 
    		return false;
    	}  
    	else if(document.forms['reportDataExtractForm'].elements['salesmanCondition'].value != "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value == "")
    	{
    		alert("Salesman type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['salesmanCondition'].value == "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value != "")
    	{
    		alert("Salesman type condition is blank, Please select."); 
    		return false;
    	}
    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['oACountryCondition'].value != "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value == "")
    	{
    		alert("OACountry type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['oACountryCondition'].value == "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value != "")
    	{
    		alert("OACountry type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['dACountryCondition'].value != "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value == "")
    	{
    		alert("DACountry type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['dACountryCondition'].value == "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value != "")
    	{
    		alert("DACountry type condition is blank, Please select."); 
    		return false;
    	} 
    	else if(document.forms['reportDataExtractForm'].elements['actualWgtCondition'].value != "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value == "")
    	{
    		alert("ActualWgt type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['actualWgtCondition'].value == "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "")
    	{
    		alert("ActualWgt type condition is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['actualVolumeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value == "")
    	{
    		alert("ActualVolume type value is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['actualVolumeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "")
    	{
    		alert("ActualVolume type condition is blank, Please select."); 
    		return false;
    	}

    	else if(document.forms['reportDataExtractForm'].elements['carrierCondition'].value != "" && document.forms['reportDataExtractForm'].elements['carrier'].value == "")
    	{
    		alert("Truck type value is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['carrierCondition'].value == "" && document.forms['reportDataExtractForm'].elements['carrier'].value != "")
    	{
    		alert("Truck type condition is blank, Please select."); 
    		return false;
    	}
		
    	else if(document.forms['reportDataExtractForm'].elements['driverIdCondition'].value != "" && document.forms['reportDataExtractForm'].elements['driverId'].value == "")
    	{
    		alert("Driver type value is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['driverIdCondition'].value == "" && document.forms['reportDataExtractForm'].elements['driverId'].value != "")
    	{
    		alert("Driver type condition is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['accountCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['accountCodeType'].value == "")
    	{
    		alert("AccountCode value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['accountCodeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['accountCodeType'].value != "")
    	{
    		alert("AccountCode condition is blank, Please select."); 
    		return false;
    	}
    	else
    	{  
    	        animatedcollapse.hide('parameterDiv'); 
          		if(clickType == "none")
          		{
          		if((document.forms['reportDataExtractForm'].elements['loadTgtBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['loadTgtEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['beginDate'].value != "" || document.forms['reportDataExtractForm'].elements['endDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['deliveryTBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['deliveryTEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['deliveryActBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['deliveryActEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['invPostingBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['invPostingEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['payPostingBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['payPostingEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['revenueRecognitionBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['revenueRecognitionEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['invoicingBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['invoicingEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['storageBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['storageEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['createDate'].value != "" || document.forms['reportDataExtractForm'].elements['toCreateDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['updateDate'].value != "" || document.forms['reportDataExtractForm'].elements['toUpdateDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['postingPeriodBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['postingPeriodEndDate'].value != "")
			     ||(document.forms['reportDataExtractForm'].elements['receivedBeginDate'].value != "" || document.forms['reportDataExtractForm'].elements['receivedEndDate'].value != "")
          		||(document.forms['reportDataExtractForm'].elements['bookingDate'].value != "" || document.forms['reportDataExtractForm'].elements['toBookingDate'].value != "")){
                 
                         if(document.forms['reportDataExtractForm'].elements['loadTgtBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['loadTgtEndDate'].value == ""){
    	                   alert("Load Target Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['loadTgtBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['loadTgtEndDate'].value != "")
						 { 
						   alert("Load Target Date From is blank, Please enter."); 
						   return false; 
						 } 
						 else if(document.forms['reportDataExtractForm'].elements['beginDate'].value != "" && document.forms['reportDataExtractForm'].elements['endDate'].value == ""){
    	                   alert("Load Actual Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['beginDate'].value == "" && document.forms['reportDataExtractForm'].elements['endDate'].value != "")
						 { 
						   alert("Load Actual Date From is blank, Please enter."); 
						   return false; 
						 } 
						 else if(document.forms['reportDataExtractForm'].elements['deliveryTBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['deliveryTEndDate'].value == ""){
    	                   alert("Delivery Target Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['deliveryTBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['deliveryTEndDate'].value != "")
						 { 
						   alert("Delivery Target Date From is blank, Please enter."); 
						   return false; 
						 } 
						 else if(document.forms['reportDataExtractForm'].elements['deliveryActBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['deliveryActEndDate'].value == ""){
    	                   alert("Delivery Actual Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['deliveryActBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['deliveryActEndDate'].value != "")
						 { 
						   alert("Delivery Actual Date From is blank, Please enter."); 
						   return false; 
						 } 
						 else if(document.forms['reportDataExtractForm'].elements['invPostingBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['invPostingEndDate'].value == ""){
    	                   alert("Invoice Posting Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['invPostingBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['invPostingEndDate'].value != "")
						 { 
						   alert("Invoice Posting Date From is blank, Please enter."); 
						   return false; 
						 } 
						 else if(document.forms['reportDataExtractForm'].elements['payPostingBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['payPostingEndDate'].value == ""){
    	                   alert("Payable Posting Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['payPostingBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['payPostingEndDate'].value != "")
						 { 
						   alert("Payable Posting Date From is blank, Please enter."); 
						   return false; 
						 }
						 else if(document.forms['reportDataExtractForm'].elements['revenueRecognitionBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['revenueRecognitionEndDate'].value == ""){
    	                   alert("Revenue Recognition To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['revenueRecognitionBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['revenueRecognitionEndDate'].value != "")
						 { 
						   alert("Revenue Recognition From is blank, Please enter."); 
						   return false; 
						 }
						 else if(document.forms['reportDataExtractForm'].elements['invoicingBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['invoicingEndDate'].value == ""){
    	                   alert("Invoicing date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['invoicingBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['invoicingEndDate'].value != "")
						 { 
						   alert("Invoicing date From is blank, Please enter."); 
						   return false; 
						 }
						 else if(document.forms['reportDataExtractForm'].elements['storageBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['storageEndDate'].value == ""){
    	                   alert("Storage Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['storageBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['storageEndDate'].value != "")
						 { 
						   alert("Storage Date From is blank, Please enter."); 
						   return false; 
						 }
						 else if(document.forms['reportDataExtractForm'].elements['createDate'].value != "" && document.forms['reportDataExtractForm'].elements['toCreateDate'].value == ""){
    	                   alert("Create Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['createDate'].value == "" && document.forms['reportDataExtractForm'].elements['toCreateDate'].value != "")
						 { 
						   alert("Create Date From is blank, Please enter."); 
						   return false; 
						 } 
						 else if(document.forms['reportDataExtractForm'].elements['updateDate'].value != "" && document.forms['reportDataExtractForm'].elements['toUpdateDate'].value == ""){
	    	                   alert("Update Date To is blank, Please enter."); 
						       return false; 
							 }else if(document.forms['reportDataExtractForm'].elements['updateDate'].value == "" && document.forms['reportDataExtractForm'].elements['toUpdateDate'].value != "")
							 { 
							   alert("Update Date From is blank, Please enter."); 
							   return false; 
							 } 
						 else if(document.forms['reportDataExtractForm'].elements['postingPeriodBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['postingPeriodEndDate'].value == ""){
    	                   alert("Posting Period Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['postingPeriodBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['postingPeriodEndDate'].value != "")
						 { 
						   alert("Posting Period Date From is blank, Please enter."); 
						   return false; 
						 } else if(document.forms['reportDataExtractForm'].elements['receivedBeginDate'].value != "" && document.forms['reportDataExtractForm'].elements['receivedEndDate'].value == ""){
    	                   alert("Received Date To is blank, Please enter."); 
					       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['receivedBeginDate'].value == "" && document.forms['reportDataExtractForm'].elements['receivedEndDate'].value != "")
						 { 
						   alert("Received Date From is blank, Please enter."); 
						   return false; 
						 } else if(document.forms['reportDataExtractForm'].elements['bookingDate'].value != "" && document.forms['reportDataExtractForm'].elements['toBookingDate'].value == ""){
	    	                   alert("Booking Date To is blank, Please enter."); 
						       return false; 
						 }else if(document.forms['reportDataExtractForm'].elements['bookingDate'].value == "" && document.forms['reportDataExtractForm'].elements['toBookingDate'].value != "")
							 { 
							   alert("Booking Date From is blank, Please enter."); 
							   return false; 
						 }else{
						 animatedcollapse.hide('billing'); 
                         return true;
                         }
				    }else { 
				       alert("Please enter at least one date range for the Activity Dates"); 
				        return false; 
				    } 
          		}
          		else
          		{
          			return false;
          		}
    	}
    	
    }	
   	else
   	{
    	    alert("Please select at least one extract parameter before selecting report."); 
    		return false;
    	
   	}
    
 </c:if>
 <c:if test="${companyDivisionFlag!='Yes'}">
 if(document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value != "" || document.forms['reportDataExtractForm'].elements['billToCodeCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['routingCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['modeCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['packModeCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['commodityCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['coordinatorCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['salesmanCondition'].value != ""  || document.forms['reportDataExtractForm'].elements['oACountryCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['dACountryCondition'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['actualWgtCondition'].value != ""   || document.forms['reportDataExtractForm'].elements['actualVolumeCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['jobTypes'].value != ""      
    	    || document.forms['reportDataExtractForm'].elements['billToCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['originAgentCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['destinationAgentCondition'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != ""|| document.forms['reportDataExtractForm'].elements['routingTypes'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['modeType'].value != ""      || document.forms['reportDataExtractForm'].elements['packModeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['commodityType'].value != "" || document.forms['reportDataExtractForm'].elements['coordinatorType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['oACountryType'].value != "" || document.forms['reportDataExtractForm'].elements['salesmanType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['dACountryType'].value != "" 
            || document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "" || document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != ""
            || document.forms['reportDataExtractForm'].elements['carrierCondition'].value != "" || document.forms['reportDataExtractForm'].elements['driverIdCondition'].value != ""
            || document.forms['reportDataExtractForm'].elements['carrier'].value != "" || document.forms['reportDataExtractForm'].elements['driverId'].value != ""
            || document.forms['reportDataExtractForm'].elements['accountCodeCondition'].value != ""
            	|| document.forms['reportDataExtractForm'].elements['consultantCondition'].value != ""){ 
	 	
   		 if(document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value != "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value == ""){
    	
    		alert("Job type value is blank, Please select."); 
          	return false;
    		
    	}
    	else if(document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value == "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value != "")
    	{
    		    alert("Job type condition is blank, Please select."); 
          	    return false;
    		
    	}  
    	else if(document.forms['reportDataExtractForm'].elements['billToCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value == "")
    	{
    		alert("BillToCode type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['originAgentCondition'].value != "" && document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value == "")
    	{
    		alert("OriginAgentCode type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['destinationAgentCondition'].value != "" && document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value == "")
    	{
    		alert("DestinationAgentCode type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value == "")
    	{
    		alert("BookingCode value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != "")
    	{
    		alert("BookingCode condition is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['routingCondition'].value != "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value == "")
    	{
    		alert("Routing type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['routingCondition'].value == "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value != "")
    	{
    		alert("Routing types condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['modeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['modeType'].value == "")
    	{
    		alert("Mode type value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['modeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['modeType'].value != "")
    	{
    		alert("Mode type condition is blank, Please select."); 
    		return false;
    	} 
    	else if(document.forms['reportDataExtractForm'].elements['packModeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['packModeType'].value == "")
    	{
    		alert("PackMode value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['packModeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['packModeType'].value != "")
    	{
    		alert("PackMode condition is blank, Please select."); 
    		return false;
    	}    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['commodityCondition'].value != "" && document.forms['reportDataExtractForm'].elements['commodityType'].value == "")
    	{
    		alert("Commodity  type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['commodityCondition'].value == "" && document.forms['reportDataExtractForm'].elements['commodityType'].value != "")
    	{
    		alert("Commodity type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['coordinatorCondition'].value != "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value == "")
    	{
    		alert("Coordinator type value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['coordinatorCondition'].value == "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value != "")
    	{
    		alert("Coordinator types condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['consultantCondition'].value != "" && document.forms['reportDataExtractForm'].elements['consultantType'].value == "")
    	{
    		alert("consultant type value is Blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['consultantCondition'].value == "" && document.forms['reportDataExtractForm'].elements['consultantType'].value != "")
    	{
    		alert("consultant types condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['salesmanCondition'].value != "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value == "")
    	{
    		alert("Salesman type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['salesmanCondition'].value == "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value != "")
    	{
    		alert("Salesman type condition is blank, Please select."); 
    		return false;
    	}    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['oACountryCondition'].value != "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value == "")
    	{
    		alert("OACountry type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['oACountryCondition'].value == "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value != "")
    	{
    		alert("OACountry type condition is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['dACountryCondition'].value != "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value == "")
    	{
    		alert("DACountry type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['dACountryCondition'].value == "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value != "")
    	{
    		alert("DACountry type condition is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['actualWgtCondition'].value != "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value == "")
    	{
    		alert("ActualWgt type value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['actualWgtCondition'].value == "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "")
    	{
    		alert("ActualWgt type condition is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['actualVolumeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value == "")
    	{
    		alert("ActualVolume type value is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['actualVolumeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "")
    	{
    		alert("ActualVolume type condition is blank, Please select."); 
    		return false;
    	}

    	else if(document.forms['reportDataExtractForm'].elements['carrierCondition'].value != "" && document.forms['reportDataExtractForm'].elements['carrier'].value == "")
    	{
    		alert("Truck type value is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['carrierCondition'].value == "" && document.forms['reportDataExtractForm'].elements['carrier'].value != "")
    	{
    		alert("Truck type condition is blank, Please select."); 
    		return false;
    	}

    	else if(document.forms['reportDataExtractForm'].elements['driverIdCondition'].value != "" && document.forms['reportDataExtractForm'].elements['driverId'].value == "")
    	{
    		alert("Driver type value is blank, Please select."); 
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['driverIdCondition'].value == "" && document.forms['reportDataExtractForm'].elements['driverId'].value != "")
    	{
    		alert("Driver type condition is blank, Please select."); 
    		return false;
    	}
   		 
    	else if(document.forms['reportDataExtractForm'].elements['accountCodeCondition'].value != "" && document.forms['reportDataExtractForm'].elements['accountCodeType'].value == "")
    	{
    		alert("AccountCode value is blank, Please select."); 
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['accountCodeCondition'].value == "" && document.forms['reportDataExtractForm'].elements['accountCodeType'].value != "")
    	{
    		alert("AccountCode type condition is blank, Please select."); 
    		return false;
    	}
    	else
    	{
    	    //document.getElementById("hid").style.visibility="visible"; 
          		if(clickType == "none")
          		{  
          			return true;
          			
          		}
          		else
          		{
          			return false;
          		}
    	}
    }	
   	else
   	{
    	    alert("Please select at least one extract parameter before selecting report.");  
    		return false;
    	
   	} 
  </c:if>
 }
 
 
function buttonDisable()
  {   	
  		document.getElementById("hid1").style.visibility="collapse"; 
  }
  
  function progress()
{
		setTimeout("alert('hello')",1250);
		var tend = "</tr></table>";
		var tstrt = "<table><tr>";
		scell = scell + "<td style='width:10;height:10' bgcolor=red>";
		document.getElementById("cell1").innerHTML = sbase + tstrt +  scell + tend;     
		 if( i < 50) // total 50 cell will be created with red color.
		 {                         i = i + 1;
			     timerID = setTimeout("progress()",1000); // recursive call
		 }
		 else
		 {
		    if(timerID)
			  {
			    clearTimeout(timerID);
			  }
		}
} 
 function openPopWindow(){
		
		javascript:openWindow('extractPartnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billToCodeType');
}
 
 function openAccountPopWindow(){
		
		javascript:openWindow('extractPartnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=accountCodeType');
}
function openBookingAgentPopWindow(){
		
		javascript:openWindow('bookingAgentPopup.html?ajax=1&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=bookingCodeType');
} 

function openOriginAgentPopWindow(){
	
	javascript:openWindow('bookingAgentPopup.html?ajax=1&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=originAgentCodeType');
} 
function opendestinationAgentPopWindow(){
	
	javascript:openWindow('bookingAgentPopup.html?ajax=1&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=destinationAgentCodeType');
} 


function checkColumn(){
var userCheckStatus ="";
  userCheckStatus = document.forms['reportDataExtractForm'].elements['selectCondition'].value; 
if(userCheckStatus!=""){ 
document.forms['reportDataExtractForm'].elements['selectCondition'].value="";
var column = userCheckStatus.split("#");  
for(i = 0; i<column.length ; i++)
{
     var userCheckStatus = document.forms['reportDataExtractForm'].elements['selectCondition'].value;  
     var userCheckStatus=	document.forms['reportDataExtractForm'].elements['selectCondition'].value = userCheckStatus + '#' + column[i];   
     document.getElementById(column[i]).checked=true;
} 
}
if(userCheckStatus==""){

}
}

function checkAll(){
	var len = document.forms['reportDataExtractForm'].elements['checkV'].length;
	for (i = 0; i < len; i++){	
	document.forms['reportDataExtractForm'].elements['checkV'][i].checked = true ; 
	document.forms['reportDataExtractForm'].elements['Show_Reports'].disabled=false;
	document.forms['reportDataExtractForm'].elements['Save_Queries'].disabled=false;
	}
}

function unCheckAll(){
	var len = document.forms['reportDataExtractForm'].elements['checkV'].length;
	for (i = 0; i < len; i++){	
		document.forms['reportDataExtractForm'].elements['checkV'][i].checked = false ; 		
		document.forms['reportDataExtractForm'].elements['Show_Reports'].disabled=true;
		document.forms['reportDataExtractForm'].elements['Save_Queries'].disabled=true; 
	}
}

function selectColumn(targetElement){
	var len = document.forms['reportDataExtractForm'].elements['checkV'].length;
	var flag=true;
	for (i = 0; i < len; i++){
		if(document.forms['reportDataExtractForm'].elements['checkV'][i].checked == true){
			document.forms['reportDataExtractForm'].elements['Show_Reports'].disabled=false;
			document.forms['reportDataExtractForm'].elements['Save_Queries'].disabled=false;
			flag=false;
		}	
	}
	if(flag){
		document.forms['reportDataExtractForm'].elements['Show_Reports'].disabled=true;
		document.forms['reportDataExtractForm'].elements['Save_Queries'].disabled=true;
	}
}
  
  function onloadCheckButton() {
  var selectCondition= document.forms['reportDataExtractForm'].elements['selectCondition'].value;
  var reportName=document.forms['reportDataExtractForm'].elements['reportName'].value; 
  if(selectCondition.length<1){
  document.forms['reportDataExtractForm'].elements['Show_Reports'].disabled=true;
  document.forms['reportDataExtractForm'].elements['Save_Queries'].disabled=true;
  }
  else if(selectCondition.length>=1){
  document.forms['reportDataExtractForm'].elements['Show_Reports'].disabled=false;
  document.forms['reportDataExtractForm'].elements['Save_Queries'].disabled=false;
  }
  if(reportName=='domesticAnalysis' || reportName=='shipmentAnalysis'){
  document.getElementById("GIS1").style.display="hide";
  document.getElementById("GIS2").style.display="hide";
  document.getElementById("GIS").style.display="block";
  }
  else{
  document.getElementById("GIS1").style.display="none";
  document.getElementById("GIS2").style.display="none";
  document.getElementById("GIS").style.display="none";
  }
  }
  
  function checkScheduler(targetElement) { 
  if(document.forms['reportDataExtractForm'].elements['id'].value=="") {
  document.getElementById("autoGenerator").style.display="none";
  document.getElementById("Scheduler").style.display="none"; 
  document.getElementById("weeklyScheduler").style.display="none";
  document.getElementById("monthalyScheduler").style.display="none"; 
  document.getElementById("quoterlyScheduler").style.display="none"; 
  document.getElementById("emailDiv").style.display="none";   
  }else {
  document.getElementById("autoGenerator").style.display="block";  
  if(targetElement.checked)
     {
     document.forms['reportDataExtractForm'].elements['autoGenerator'].checked='checked'
     document.forms['reportDataExtractForm'].elements['autoGenerator'].value=true;
     document.getElementById("Scheduler").style.display="block";
     document.getElementById("autoGenerator").style.display="block";
     document.getElementById("emailDiv").style.display="block";   
     if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="W"){
     document.getElementById("weeklyScheduler").style.display="block";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
    else if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="M"){
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="block"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
    else if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="Q"){
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
     else if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="D"){
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
     }else { 
     document.getElementById("Scheduler").style.display="none"; 
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     //document.getElementById("autoGenerator").style.display="none";
     document.getElementById("autoGenerator").style.display="block";  
     document.forms['reportDataExtractForm'].elements['autoGenerator'].value=false;
     document.getElementById("emailDiv").style.display="none";  
     }
  }
}
function onloadCheckScheduler(targetElement) { 
if(document.forms['reportDataExtractForm'].elements['id'].value=="") {
document.getElementById("autoGenerator").style.display="none";
document.getElementById("Scheduler").style.display="none"; 
document.getElementById("weeklyScheduler").style.display="none";
document.getElementById("monthalyScheduler").style.display="none"; 
document.getElementById("quoterlyScheduler").style.display="none";
document.getElementById("emailDiv").style.display="none";    
}else {
document.getElementById("autoGenerator").style.display="block";  
  if(targetElement)
     { 
     document.forms['reportDataExtractForm'].elements['autoGenerator'].checked='checked'
     document.forms['reportDataExtractForm'].elements['autoGenerator'].value=true;
     document.getElementById("Scheduler").style.display="block";
     document.getElementById("autoGenerator").style.display="block"; 
     document.getElementById("emailDiv").style.display="block";    
     if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="W"){
     document.getElementById("weeklyScheduler").style.display="block";
     document.getElementById("autoGenerator").style.display="block";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
    else if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="M"){
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="block"; 
     document.getElementById("autoGenerator").style.display="block";
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
    else if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="Q"){
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     document.getElementById("autoGenerator").style.display="block";
     }
     else if(document.forms['reportDataExtractForm'].elements['queryScheduler'].value=="D"){
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
     }
     }else { 
     document.getElementById("Scheduler").style.display="none"; 
     document.getElementById("weeklyScheduler").style.display="none";
     document.getElementById("monthalyScheduler").style.display="none"; 
     document.getElementById("quoterlyScheduler").style.display="none"; 
      //document.getElementById("autoGenerator").style.display="none";
      document.getElementById("autoGenerator").style.display="block"; 
      document.forms['reportDataExtractForm'].elements['autoGenerator'].value=false;
      document.getElementById("emailDiv").style.display="none";  
     }
  }
  }
function selectReport(){
var check=formValidate('none'); 
      		if(check){
      		animatedcollapse.toggle('report') 
      		}else {
      		animatedcollapse.hide('report'); 
      		
      		}
}

function checkQueryName()
    { 
     var queryName = document.forms['reportDataExtractForm'].elements['queryName'].value; 
     var publicPrivateFlag = document.forms['reportDataExtractForm'].elements['publicPrivateFlag'].value;  
     if(queryName!=''){
     var url="checkQueryName.html?ajax=1&decorator=simple&popup=true&queryName="+ encodeURI(queryName)+ "&publicPrivateFlag=" +encodeURI(publicPrivateFlag); 
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null); 
    }
    if(queryName==''){
    alert('Please Enter Query Name');
     }
    }
function handleHttpResponse3()
        { 
          if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results<=1)
                {
 					findPreviewBillingReportNew();
                 }
                 else
                 {
                     alert("Please select another query name.");
                     var queryName = document.forms['reportDataExtractForm'].elements['queryName'].value='';  
                 }
             }
        }

function checkEmail() {
if(document.forms['reportDataExtractForm'].elements['email'].value!=''){
var emailFilter2=/^.+@.+\..{2,3}$/ 
var emailValue2=document.forms['reportDataExtractForm'].elements['email'].value;
if (!(emailFilter2.test(emailValue2))) {
alert("Invalid E-mail Address! Please re-enter.")
document.forms['reportDataExtractForm'].elements['email'].value='';
document.forms['reportDataExtractForm'].elements['email'].select();
return false;
}else{
return true;
}
}
}

function checkPeriodDate(temp,from,to,fieldName) { 
        var mydate=new Date();
		var year=mydate.getFullYear()
		
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var day1=mydate.getDay()
		var month=mydate.getMonth()+1
		var month1=mydate.getMonth()+1
		if(temp.value=='LastYear'){
	    year=year-1;
		month=1;
		month1=12;
		day=1;
		day1=31; 
		} 
		if(temp.value=='CurrentYear'){
	    year=year;
		month=1;
		month1=12;
		day=1;
		day1=31; 
		} 
		if(temp.value=='LastHalfYear'){
		if(month>6){
		year=year;
		month=1;
		month1=6;
		day=1;
		day1=30; 
		}else if(month<6|| month==6){
		year=year-1;
		month=7;
		month1=12;
		day=1;
		day1=31; 
		
		}
		}
		if(temp.value=='CurrentHalfYear'){
		if(month>6){
		year=year;
		month=7;
		month1=12;
		day=1;
		day1=31; 
		}else if(month<6|| month==6){
		year=year;
		month=1;
		month1=6;
		day=1;
		day1=30; 
		
		}
		}
		if(temp.value=='LastQuarter'){
		if(month<3 || month==3){
		year=year-1;
		month=10;
		month1=12;
		day=1;
		day1=31; 
		}else if((month>3 && month<6)||month==6){
		year=year;
		month=1;
		month1=3;
		day=1;
		day1=31;  
		} else if((month>6&& month<9)||month==9){
		year=year;
		month=4;
		month1=6;
		day=1;
		day1=30; 
		} else if((month>9)){
		year=year;
		month=7;
		month1=9;
		day=1;
		day1=30;  
		}
		}
		if(temp.value=='CurrentQuarter'){
		if(month<3 || month==3){
		year=year;
		month=1;
		month1=3;
		day=1;
		day1=31; 
		}else if((month>3 && month<6)||month==6){
		year=year;
		month=4;
		month1=6;
		day=1;
		day1=30;  
		} else if((month>6&& month<9)||month==9){
		year=year;
		month=7;
		month1=9;
		day=1;
		day1=30; 
		} else if((month>9)){
		year=year;
		month=10;
		month1=12;
		day=1;
		day1=31;  
		}
		}
		if(temp.value=='LastMonth'){
		year=year;
		if(month==1){
		month=12;
		month1=12;
		year=year-1;
		}else{
		month=month-1;
		month1=month1-1;
		}
		day=1; 
		var lastday = getLastDayOfMonth(month,year); 
		day1=lastday; 
		}
		if(temp.value=='CurrentMonth'){
		year=year;
		month=month;
		month1=month1;
		day=1; 
		var lastday = getLastDayOfMonth(month,year); 
		day1=lastday; 
		}
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		if(month1 == 1)month1="Jan";
		if(month1 == 2)month1="Feb";
		if(month1 == 3)month1="Mar";
		if(month1 == 4)month1="Apr";
		if(month1 == 5)month1="May";
		if(month1 == 6)month1="Jun";
		if(month1 == 7)month1="Jul";
		if(month1 == 8)month1="Aug";
		if(month1 == 9)month1="Sep";
		if(month1 == 10)month1="Oct";
		if(month1 == 11)month1="Nov";
		if(month1 == 12)month1="Dec";
		
		if (day<10){
			day="0"+day
		}
		var y=""+year; 
		var fromdate=day+"-"+month+"-"+y.substring(2,4);
		var todate=day1+"-"+month1+"-"+y.substring(2,4);
		if(temp.value!=''){
		document.forms['reportDataExtractForm'].elements[from].value=fromdate;
		document.forms['reportDataExtractForm'].elements[to].value=todate;
		} else if(temp.value==''){
		document.forms['reportDataExtractForm'].elements[from].value='';
		document.forms['reportDataExtractForm'].elements[to].value='';
		}
		checkDateChange(from,to,fieldName);	
}

  function getLastDayOfMonth(month,year)
{
    var day;
    switch(month)
    {
        case 1 :
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            day = 31;
            break;
        case 4 :
        case 6 :
        case 9 :
        case 11 :
               day = 30;
            break;
        case 2 :
            if( ( (year % 4 == 0) && ( year % 100 != 0) )
                           || (year % 400 == 0) )
                day = 29;
            else
                day = 28;
            break;
    }
    return day;
}
  function findFieldName(from,to,fieldName){

	  var fromFieldName;
	  var toFieldName;
	  var periodValFieldname;
	  if(fieldName=='DummyName'){
		  if(from == 'loadTgtBeginDate' || from == 'loadTgtEndDate'){			
			  	fromFieldName='loadTgtBeginDate';
			  	toFieldName='loadTgtEndDate';
			  	periodValFieldname='loadTargetDatePeriod';
		  }else if(from == 'deliveryTBeginDate' || from == 'deliveryTEndDate'){			  
			  	fromFieldName='deliveryTBeginDate';
			  	toFieldName='deliveryTEndDate';
			  	periodValFieldname='deliveryTargetDatePeriod';
		  }else if(from == 'invPostingBeginDate' || from == 'invPostingEndDate'){			  
			  	fromFieldName='invPostingBeginDate';
			  	toFieldName='invPostingEndDate';
			  	periodValFieldname='invoicePostingDatePeriod';
		  }else if(from == 'revenueRecognitionBeginDate' || from == 'revenueRecognitionEndDate'){			  
			  	fromFieldName='revenueRecognitionBeginDate';
			  	toFieldName='revenueRecognitionEndDate';
			  	periodValFieldname='revenueRecognitionPeriod';
		  }else if(from == 'storageBeginDate' || from == 'storageEndDate'){			  
			  	fromFieldName='storageBeginDate';
			  	toFieldName='storageEndDate';
			  	periodValFieldname='storageDatePeriod';
		  }else if(from == 'postingPeriodBeginDate' || from == 'postingPeriodEndDate'){			  
			  	fromFieldName='postingPeriodBeginDate';
			  	toFieldName='postingPeriodEndDate';
			  	periodValFieldname='PostingPeriodDatePeriod';
		  }else if(from == 'bookingDate' || from == 'toBookingDate'){			  
			  	fromFieldName='bookingDate';
			  	toFieldName='toBookingDate';
			  	periodValFieldname='bookingDatePeriod';
		  }else if(from == 'beginDate' || from == 'endDate'){			  
			  	fromFieldName='beginDate';
			  	toFieldName='endDate';
			  	periodValFieldname='loadActualDatePeriod';
		  }else if(from == 'deliveryActBeginDate' || from == 'deliveryActEndDate'){			  
			  	fromFieldName='deliveryActBeginDate';
			  	toFieldName='deliveryActEndDate';
			  	periodValFieldname='deliveryActualDatePeriod';
		  }else if(from == 'payPostingBeginDate' || from == 'payPostingEndDate'){			  
			  	fromFieldName='payPostingBeginDate';
			  	toFieldName='payPostingEndDate';
			  	periodValFieldname='payablePostingDatePeriod';
		  }else if(from == 'invoicingBeginDate' || from == 'invoicingEndDate'){			  
			  	fromFieldName='invoicingBeginDate';
			  	toFieldName='invoicingEndDate';
			  	periodValFieldname='InvoicingdatePeriod';
		  }else if(from == 'createDate' || from == 'toCreateDate'){			  
			  	fromFieldName='createDate';
			  	toFieldName='toCreateDate';
			  	periodValFieldname='createDatePeriod';
		  }else if(from == 'updateDate' || from == 'toUpdateDate'){			  
			  	fromFieldName='updateDate';
			  	toFieldName='toUpdateDate';
			  	periodValFieldname='updateDatePeriod';
		  }
		  else if(from == 'receivedBeginDate' || from == 'receivedEndDate'){			  
			  	fromFieldName='receivedBeginDate';
			  	toFieldName='receivedEndDate';
			  	periodValFieldname='receivedDatePeriod';
		  }
		  checkDateChange(fromFieldName,toFieldName,periodValFieldname);
	  }
  }
  
  function checkDateChange(from,to,fieldName){
	  
	  var beginDate;
	  var endDate;
	  var periodVal;
		beginDate = document.forms['reportDataExtractForm'].elements[from].value;
		endDate = document.forms['reportDataExtractForm'].elements[to].value;	  	   
	   
	   var mySplitResult1 = beginDate.split("-");
	   var formBeginDay = mySplitResult1[0];
	   var formBeginMonth = mySplitResult1[1];
	   var formBeginYear = mySplitResult1[2];
	   
	   var mySplitResult = endDate.split("-");
	   var formEndDay = mySplitResult[0];
	   var formEndMonth = mySplitResult[1];
	   var formEndYear = mySplitResult[2];
		    
	    var mydate=new Date();
		var year=mydate.getFullYear()		
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var day1=mydate.getDay()
		var month=mydate.getMonth()+1
		var month1=mydate.getMonth()+1
		var y=""+year;
		
		if(month == 'Jan')month="1";
		if(month == 'Feb')month="2";
		if(month == 'Mar')month="3";
		if(month == 'Apr')month="4";
		if(month == 'May')month="5";
		if(month == 'Jun')month="6";
		if(month == 'Jul')month="7";
		if(month == 'Aug')month="8";
		if(month == 'Sep')month="9";
		if(month == 'Oct')month="10";
		if(month == 'Nov')month="11";
		if(month == 'Dec')month="12";
		if(month1 == 'Jan')month1="1";
		if(month1 == 'Feb')month1="2";
		if(month1 == 'Mar')month1="3";
		if(month1 == 'Apr')month1="4";
		if(month1 == 'May')month1="5";
		if(month1 == 'Jun')month1="6";
		if(month1 == 'Jul')month1="7";
		if(month1 == 'Aug')month1="8";
		if(month1 == 'Sep')month1="9";
		if(month1 == 'Oct')month1="10";
		if(month1 == 'Nov')month1="11";
		if(month1 == 'Dec')month1="12";

		  periodVal = document.forms['reportDataExtractForm'].elements[fieldName].value;
		  if(periodVal=='CurrentYear'){
			  	year=year;
				month="Jan";
				month1="Dec";
				day=1;
				day1=31;			
		  	}
		  if(periodVal=='LastYear'){
			    year=year-1;
			    month="Jan";
				month1="Dec";
				day=1;
				day1=31;
		  }
		  if(periodVal=='LastHalfYear'){
			  if(month>6){
					year=year;
					month="Jan";
					month1="Jun";
					day=1;
					day1=30;				
					}else if(month<6|| month==6){
					year=year-1;
					month="Jul";
					month1="Dec";
					day=1;
					day1=31;
				}		  
		 }
		  if(periodVal=='CurrentHalfYear'){
			  if(month>6){
					year=year;
					month="Jul";
					month1="Dec";
					day=1;
					day1=31; 
					}else if(month<6|| month==6){
					year=year;
					month="Jan";
					month1="Jun";
					day=1;
					day1=30; 
				}			
		 }
		  if(periodVal=='LastQuarter'){
			  if(month<3 || month==3){
					year=year-1;
					month="Oct";
					month1="Dec";
					day=1;
					day1=31; 
					}else if((month>3 && month<6)||month==6){
					year=year;
					month="Jan";
					month1="Mar";
					day=1;
					day1=31;  
					} else if((month>6&& month<9)||month==9){
					year=year;
					month="Apr";
					month1="Jun";
					day=1;
					day1=30; 
					} else if((month>9)){
					year=year;
					month="Jul";
					month1="Sep";
					day=1;
					day1=30;  
				}				
		 }
		  if(periodVal=='CurrentQuarter'){
			  if(month<3 || month==3){
					year=year;
					month="Jan";
					month1="Mar";
					day=1;
					day1=31; 
					}else if((month>3 && month<6)||month==6){
					year=year;
					month="Apr";
					month1="Jun";
					day=1;
					day1=30;  
					} else if((month>6&& month<9)||month==9){
					year=year;
					month="Jul";
					month1="Sep";
					day=1;
					day1=30; 
					} else if((month>9)){
					year=year;
					month="Oct";
					month1="Dec";
					day=1;
					day1=31;  
				}				
		 }
		  if(periodVal=='LastMonth'){
			  year=year;
				if(month==1){
				month="Dec";
				month1="Dec";
				year=year-1;
				}else{
				month=month-1;
				month1=month1-1;
				}
				day=1; 
				var lastday = getLastDayOfMonth(month,year); 
				day1=lastday; 	
				
				if(month == 1)month="Jan";
				if(month == 2)month="Feb";
				if(month == 3)month="Mar";
				if(month == 4)month="Apr";
				if(month == 5)month="May";
				if(month == 6)month="Jun";
				if(month == 7)month="Jul";
				if(month == 8)month="Aug";
				if(month == 9)month="Sep";
				if(month == 10)month="Oct";
				if(month == 11)month="Nov";
				if(month == 12)month="Dec";
				if(month1 == 1)month1="Jan";
				if(month1 == 2)month1="Feb";
				if(month1 == 3)month1="Mar";
				if(month1 == 4)month1="Apr";
				if(month1 == 5)month1="May";
				if(month1 == 6)month1="Jun";
				if(month1 == 7)month1="Jul";
				if(month1 == 8)month1="Aug";
				if(month1 == 9)month1="Sep";
				if(month1 == 10)month1="Oct";
				if(month1 == 11)month1="Nov";
				if(month1 == 12)month1="Dec";
		  }
		  if(periodVal=='CurrentMonth'){
			  	year=year;
				month=month;
				month1=month1;
				day=1; 
				var lastday = getLastDayOfMonth(month,year); 
				day1=lastday;
				
				if(month == 1)month="Jan";
				if(month == 2)month="Feb";
				if(month == 3)month="Mar";
				if(month == 4)month="Apr";
				if(month == 5)month="May";
				if(month == 6)month="Jun";
				if(month == 7)month="Jul";
				if(month == 8)month="Aug";
				if(month == 9)month="Sep";
				if(month == 10)month="Oct";
				if(month == 11)month="Nov";
				if(month == 12)month="Dec";
				if(month1 == 1)month1="Jan";
				if(month1 == 2)month1="Feb";
				if(month1 == 3)month1="Mar";
				if(month1 == 4)month1="Apr";
				if(month1 == 5)month1="May";
				if(month1 == 6)month1="Jun";
				if(month1 == 7)month1="Jul";
				if(month1 == 8)month1="Aug";
				if(month1 == 9)month1="Sep";
				if(month1 == 10)month1="Oct";
				if(month1 == 11)month1="Nov";
				if(month1 == 12)month1="Dec";
		  }		  	
			if (day<10){
				day="0"+day
			}
			if(periodVal=='LastYear'){
				var year=mydate.getFullYear()		
				if (year < 1000)
				year+=1900
				year=year-1;
				y=""+year;
			}
			if(formBeginDay!=day || formBeginMonth!=month || formBeginYear!=y.substring(2,4)){
				document.forms['reportDataExtractForm'].elements[fieldName].value='';
			}			
			
			if(formEndDay!=day1 || formEndMonth!=month1 || formEndYear!=y.substring(2,4)){
				document.forms['reportDataExtractForm'].elements[fieldName].value='';
			}
	}

function selectdatePeriod(targetElement)
   {
   if(targetElement.value!=''){
   var rowId=targetElement.name; 
   var reviewerStatusCheck = document.forms['reportDataExtractForm'].elements['datePeriod'].value;
    if(reviewerStatusCheck == '')
     {
	document.forms['reportDataExtractForm'].elements['datePeriod'].value = rowId+'#'+targetElement.value;
     }
    else
     {
      if(reviewerStatusCheck.indexOf(rowId+'#') >-1)
       {
	    var splitReviewerStatusCheck = reviewerStatusCheck.split(',');
	    var reviewerStatusCheck = '';
	    for(i = 0; i<splitReviewerStatusCheck.length ; i++)
	     {
		    if(splitReviewerStatusCheck[i].indexOf(rowId+'#') > -1)
		     {
			   splitReviewerStatusCheck[i] = rowId+'#'+targetElement.value;
		     }
		    if(reviewerStatusCheck == '')
		     {
			reviewerStatusCheck = splitReviewerStatusCheck[i]
		     }
		    else
		     {
			reviewerStatusCheck = reviewerStatusCheck+','+splitReviewerStatusCheck[i];
		     }
	     }
	
      }
     else
      {
       reviewerStatusCheck=document.forms['reportDataExtractForm'].elements['datePeriod'].value = reviewerStatusCheck + ',' + rowId+'#'+targetElement.value;
      }
    document.forms['reportDataExtractForm'].elements['datePeriod'].value = reviewerStatusCheck.replace( ',,' , ',' );
    }

  } else{
  var rowId=targetElement.name; 
  var datePeriodValue = document.forms['reportDataExtractForm'].elements['datePeriod'].value;
  var splitReviewerStatusCheck = datePeriodValue.split(',');
	    var reviewerStatusCheck = '';
	    for(i = 0; i<splitReviewerStatusCheck.length ; i++)
	     {
		    if(splitReviewerStatusCheck[i].indexOf(rowId+'#') > -1)
		     {
		     reviewerStatusCheck=splitReviewerStatusCheck[i];
		      i=splitReviewerStatusCheck.length; 
		     }
		 } 
  var newDatePeriodValue= document.forms['reportDataExtractForm'].elements['datePeriod'].value=datePeriodValue.replace(reviewerStatusCheck,'');
  document.forms['reportDataExtractForm'].elements['datePeriod'].value = newDatePeriodValue.replace( ',,' , ',' );
  }
}

function findTruckNumber() {	
	 	  openWindow('findTruckNumber.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=carrier');
	 	   }

function findDriverFromPartner(){	
	 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=driverId');
	 }
</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>

</head>

<s:form cssClass="form_magn" id="reportDataExtractForm" name="reportDataExtractForm" action="dynamicDataExtracts" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="userName" value="${pageContext.request.remoteUser}" />
<s:hidden name="userName" value="${pageContext.request.remoteUser}"/> 
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="columnButton" value="<%=request.getParameter("columnButton")%>"/> 
<s:hidden name="selectCondition" />
<s:hidden name="id" value="<%=request.getParameter("id")%>"/> 
<s:hidden name="lastEmailSent" /> 
<s:hidden name="emailStatus" /> 
<s:hidden name="emailMessage" /> 
<s:hidden name="datePeriod" value="${datePeriod}" /> 
<c:if test="${not empty emailDateTime}">
	 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="emailDateTime" /></s:text>
	 <s:hidden  name="emailDateTimeString" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty emailDateTime}">
	 <s:hidden   name="emailDateTimeString"/> 
</c:if>
<s:hidden id="reportName" name="reportName" value="${reportName}"/> 
<s:hidden id="querycreatedBy" name="querycreatedBy" value="${querycreatedBy}"/>
 <c:if test="${not empty querycreatedOn}">
 
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="querycreatedOn" /></s:text>
			 <s:hidden  name="querycreatedOn1" value="%{customerFileSurveyFormattedValue}" /> 
	  </c:if>
	 <c:if test="${empty querycreatedOn}"> 
		 <s:hidden   name="querycreatedOn1"/> 
	 </c:if> 
<div id="Layer1" style="width:100%">
<s:hidden name="reports.id" value="%{reports.id}"/> 
<div id="newmnav" class="nav_tabs">
		  <ul>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Data Extracts<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<li><a id="findOldQueriesId" href="findOldQueries.html"><span>Find Old Queries</span></a></li>
		  	  </ul>
	</div><div class="spn">&nbsp;</div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">

<table class="" style="width:100%; margin-bottom:5px" cellspacing="0" cellpadding="0" border="0" >
<tr><td>
<table style="margin-bottom:0px">
<tr><td height="5px"></td></tr>	  
<tr>
<td  class="bgblue" >Extract Parameters</td></tr>
<tr><td height="2px"></td></tr>
</table>
</td></tr>
	
	 <tr><td  class="listwhitetext" colspan="13" style="text-align:left; margin: 0px;">
	  		
	  		 <div class="" onClick="javascript:animatedcollapse.toggle('parameterDiv')" style="cursor: pointer;">
	  		 		 
	  		  <table cellpadding="0" cellspacing="0" width="100%" border="0"  
style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;<a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;Please enter the Parameter for the query</a>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table> 	 
	  	</div> 
				<div id="parameterDiv" > 
	  	   <table border="0" width="100%" cellpadding="1" cellspacing="1" class="detailTabLabel" style="margin-bottom:0px;margin: 0px;">
	  		<tr><td height="0px"></td>	  		
	  		</tr>
	  	
	  		<tr>
	  			<td align="center" class="subcontenttabChild" >Field</td>
	  			<td class="subcontenttabChild"  width="5px"></td>
	  			<td  class="subcontenttabChild" >Condition</td> 
	  			<td  class="subcontenttabChild" >Value</td>
	  			<td class="subcontenttabChild"  width="5px"></td>
	  			<td align="center" class="subcontenttabChild" >Field</td>
	  			<td class="subcontenttabChild"  width="5px"></td>
	  			<td  class="subcontenttabChild" >Condition</td> 
	  			<td  class="subcontenttabChild" >Value</td>
	  			
	  	   </tr>
	  	  
	  	   <tr>
	  	   
	  	   <td width="105px" class="listwhitetext" align="right" height="">Job Type</td>
	  			<td  width="5px"></td>
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:110px" id="c1" name="jobTypesCondition" list="{'Equal to','Not Equal to'}" value="%{jobTypesCondition}"  headerKey="" headerValue="" /></td>
	  			
	  			<td width="130px" colspan="3"><s:select cssClass="list-menu" cssStyle="width:110px;height:50px"  name="jobTypes" list="%{jobtype}"   value="%{multiplejobType}" multiple="true"  /></td>
	  	    <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Job Type</b>
             </td>
	  	   
	  	   </tr>
	  	   <tr><td height="10px" width="100px"></td>
	  	   <td height="10px"></td>
	  	   <td height="10px"></td>
	  	   <td height="10px" width="212px"></td>
	  	   <td height="10px" rowspan="11" class="vertlinedata_vert"></td>	
	  	    <td height="10px"></td>
	  	   <td height="10px"></td>
	  	   </tr>
	  	   
	  		<tr>
		  		<c:if test="${companyDivisionFlag=='Yes'}"> 
		  		<td class="listwhitetext" align="right">Company Code</td>
		  			<td  width="5px"></td>
		  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="companyCodeCondition" list="{'Equal to','Not Equal to'}"  value="%{companyCodeCondition}" headerKey="" headerValue="" /></td>
		  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="companyCode" list="%{companyCodeList}"  value="%{companyCode}" headerKey="" headerValue="" /></td>
		  			
		  		</c:if>	 
		  		
		  			<td width="101px" class="listwhitetext" align="right" height="">Job Status</td>
					<td  width="5px"></td>
					<td align="left"><s:select cssClass="list-menu" cssStyle="width:110px" id="activeStatus" name="activeStatus" value="%{activeStatus}"  list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
				
	  		</tr>
	  		 
	  		 <tr>
	  			<td class="listwhitetext" align="right">Bill To Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c4" name="billToCodeCondition" list="{'Equal to','Not Equal to'}" value="%{billToCodeCondition}" headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" name="billToCodeType" value="${billToCodeType}" cssStyle="width:108px" maxlength="8" onchange="findBillToCode()" onkeydown="return onlyAlphaNumericAllowed(event)" />
	  			<img class="openpopup" width="17" align="top" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		
	  			<td class="listwhitetext" width="110" align="right">Booking Code</td>
	  			<td  width="5px"></td>
	  			<td width="140"><s:select cssClass="list-menu" cssStyle="width:110px" id="c5" name="bookingCodeCondition" value="%{bookingCodeCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield  cssClass="input-text" name="bookingCodeType" value="${bookingCodeType}" cssStyle="width:108px" maxlength="8" onchange="findBookingAgentCode()" onkeydown="return onlyAlphaNumericAllowed(event)" />
	  			<img class="openpopup" width="17" height="20" align="top" id="bookingCodeTypeId" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Account Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c4" name="accountCodeCondition" list="{'Equal to','Not Equal to'}" value="%{accountCodeCondition}" headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" name="accountCodeType" value="${accountCodeType}" cssStyle="width:108px" maxlength="8" onchange="findAccountCode()" onkeydown="return onlyAlphaNumericAllowed(event)" />
	  			<img class="openpopup" width="17" align="top" height="20" onclick="openAccountPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  			
	  			
	  			<td class="listwhitetext" align="right">Mode</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="modeCondition" value="%{modeCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="modeType" list="%{mode}"  value="%{modeType}" headerKey="" headerValue="" /></td>
	  	
	  			
	  		</tr>
	  		
	  		<tr>
	  			<td class="listwhitetext" align="right">Routing</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c6" name="routingCondition" value="%{routingCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		  
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="routingTypes" list="%{routing}" value="%{routingTypes}" headerKey="" headerValue="" /></td>
	  			
	  			
	  			
	  			<td class="listwhitetext" align="right">Commodity</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="c9" name="commodityCondition" value="%{commodityCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		   
	  			<td ><s:select cssClass="list-menu"  cssStyle="width:110px"  name="commodityType"  value="%{commodityType}"  list="%{commodit}"  headerKey="" headerValue="" /></td>
	  			
	  		
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Pack Mode</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c8" name="packModeCondition" value="%{packModeCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="packModeType"  list="%{pkmode}"  value="%{packModeType}" headerKey="" headerValue="" /></td>
	  			
	  			
	  			<td class="listwhitetext" align="right">Salesman</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c11" name="salesmanCondition" value="%{salesmanCondition}"  list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="salesmanType" value="%{salesmanType}"  list="%{sale}"  headerKey="" headerValue="" /></td>
	  			
	  		
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Coordinator</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c10" name="coordinatorCondition" value="%{coordinatorCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="coordinatorType"  value="%{coordinatorType}" list="%{coord}"  headerKey="" headerValue="" /></td>
	  			
	  			
	  			<td class="listwhitetext" align="right">DA Country</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c13"  name="dACountryCondition" value="%{dACountryCondition}"  list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="dACountryType" list="%{dcountry}" value="%{dACountryType}" headerKey="" headerValue="" /></td>
	  
	  		
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">OA Country</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c12"  name="oACountryCondition"  value="%{oACountryCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="oACountryType"  value="%{oACountryType}" list="%{ocountry}"  headerKey="" headerValue="" /></td>
	  			
	  			
	  			<td class="listwhitetext" align="right">Actual Volume</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="c16" name="actualVolumeCondition" value="%{actualVolumeCondition}" list="{'Equal to','Not Equal to','Greater than','Less than','Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" id="actualVolumeType" name="actualVolumeType" value="${actualVolumeType}" cssStyle="width:108px" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/>
	  			</td>
	  		
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Actual Wgt</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c15" name="actualWgtCondition" value="%{actualWgtCondition}" list="{'Equal to','Not Equal to','Greater than','Less than','Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" id="actualWgtType" name="actualWgtType" value="${actualWgtType}" cssStyle="width:108px" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td>
	  			
	  			<td class="listwhitetext" align="right">Driver&nbsp;#</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="" name="driverIdCondition" value="%{driverIdCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield  cssClass="input-text" name="driverId" value="${driverId}" cssStyle="width:108px" maxlength="6" onchange="" onkeydown="" />
	  			<img class="openpopup" width="17" height="20" align="top" onclick="findDriverFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Truck&nbsp;#</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="" name="carrierCondition" list="{'Equal to','Not Equal to'}" value="%{carrierCondition}" headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" name="carrier" value="${carrier}" cssStyle="width:108px" maxlength="6" onchange="" onkeydown="" />
	  			<img class="openpopup" width="17" align="top" height="20" onclick="findTruckNumber();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		    
	  		    <td class="listwhitetext" align="right">Consultant </td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c10" name="consultantCondition" value="%{consultantCondition}" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="consultantType"  value="%{consultantType}" list="%{sale}"  headerKey="" headerValue="" /></td>
	  			
	  		
	  			</tr>
	  			<tr>
	  			<td class="listwhitetext" align="right">Origin Agent Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c6" name="originAgentCondition" list="{'Equal to','Not Equal to'}" value="%{originAgentCondition}" headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" name="originAgentCodeType" value="${originAgentCodeType}" cssStyle="width:108px" maxlength="8" onchange="findOriginAgentCode(this)" onkeydown="return onlyAlphaNumericAllowed(event)" />
	  			<img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="openOriginAgentPopWindow()" src="<c:url value='/images/open-popup.gif'/>" /></td>
                
                <td class="listwhitetext" align="right">Destination Agent Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c5" name="destinationAgentCondition" list="{'Equal to','Not Equal to'}" value="%{destinationAgentCondition}" headerKey="" headerValue="" /></td>
	  			
	  			<td style="margin:0px;text-align:left;"><s:textfield cssClass="input-text" name="destinationAgentCodeType" value="${destinationAgentCodeType}" cssStyle="width:108px" maxlength="8" onchange="findDestiAgentCode(this)" onkeydown="return onlyAlphaNumericAllowed(event)" />
	  			<img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="opendestinationAgentPopWindow()" src="<c:url value='/images/open-popup.gif'/>" /></td>
                </tr>
	  		 </table>
	  		 
	  		 </div></td></tr>
	  		 <tr><td  height="13" class="listwhitetext" colspan="13" style="text-align:left; margin: 0px;">
	  		
	  		 <div class="" onClick="javascript:animatedcollapse.toggle('billing')" style="cursor: pointer;">
	  		 		 
	  		  <table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;<a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;Please enter the date ranges for the Activity Dates</a>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	  		 
	  	</div>
				
				<div class="dspcont" id="billing" > 
	  		<table class="detailTabLabel" cellspacing="1" cellpadding="1" border="0" width="99%" align="left" style="margin:0px; padding:0px;">
			<tbody>
			
			<tr>
			<td></td>
			<td></td>
			<td class="listwhitetext" align="left"><b>From</b></td>
			
			<td class="listwhitetext" align="left"><b>To</b></td>
			<td class="listwhitetext" align="left" width="148"><b>Period</b></td>
			<c:if test="${accountInterface=='Y'}"> 
			<td height="10px" rowspan="9" width="2px" class="vertlinedata_vert"></td>	
			</c:if>
			<c:if test="${accountInterface!='Y'}">
			<td height="10px" rowspan="9" width="2px" class="vertlinedata_vert"></td>	 
			</c:if>
			<td></td>
			
			<td></td>
			<td class="listwhitetext" align="left"><b>From</b></td>
			
			<td class="listwhitetext" align="left"><b>To</b></td>
			<td class="listwhitetext" align="left"><b>Period</b></td>
			</tr>
	  		<tr>
	  			
	  			<td class="listwhitetext" align="right" width="115px">Load Target Date</td>
	  			<td  width="0px"></td> 
	  			<s:hidden   id="loadTgtBeginDateCondition" name="loadTgtBeginDateCondition" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty loadTgtBeginDate}">
						<s:text id="loadTgtBeginDate" name="${FormDateValue}"> <s:param name="value" value="loadTgtBeginDate" /></s:text>
						<td width="94px"><s:textfield cssClass="input-text" id="loadTgtBeginDate" name="loadTgtBeginDate" value="%{loadTgtBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty loadTgtBeginDate}" >
						<td width="94px"><s:textfield cssClass="input-text" id="loadTgtBeginDate" name="loadTgtBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if> 
	  			<s:hidden id="loadTgtEndDateCondition" name="loadTgtEndDateCondition" value="Less than Equal to" /> 
	  		  <c:if test="${not empty loadTgtEndDate}">
						<s:text id="loadTgtEndDate" name="${FormDateValue}"> <s:param name="value" value="loadTgtEndDate" /></s:text>
						<td width="95px"><s:textfield cssClass="input-text" id="loadTgtEndDate" name="loadTgtEndDate" value="%{loadTgtEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty loadTgtEndDate}" >
						<td width="95px"><s:textfield cssClass="input-text" id="loadTgtEndDate" name="loadTgtEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td><s:select cssClass="list-menu" cssStyle="width:110px"  name="loadTargetDatePeriod"  list="%{periodScheduler}"  headerKey="" headerValue=""  onchange="checkPeriodDate(this,'loadTgtBeginDate','loadTgtEndDate','loadTargetDatePeriod');selectdatePeriod(this);" /></td>  
	  			
	  			<td class="listwhitetext" align="right" width="120px">Load Actual Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="beginDateCondition" name="beginDateCondition" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"> <s:param name="value" value="beginDate" /></s:text>
						<td width="95px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty beginDate}" >
						<td width="95px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden id="endDateCondition" name="endDateCondition" value="Less than Equal to"  /> 
	  		<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td width="96px"><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty endDate}" >
			  <td width="96px"><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="loadActualDatePeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'beginDate','endDate','loadActualDatePeriod');selectdatePeriod(this);" /></td>	
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Delivery Target Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="deliveryTBeginDateCondition" name="deliveryTBeginDateCondition" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty deliveryTBeginDate}">
						<s:text id="deliveryTBeginDate" name="${FormDateValue}"> <s:param name="value" value="deliveryTBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryTBeginDate" name="deliveryTBeginDate" value="%{deliveryTBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryTBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty deliveryTBeginDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryTBeginDate" name="deliveryTBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryTBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if> 
	  			<s:hidden id="deliveryTEndDateCondition" name="deliveryTEndDateCondition" value ="Less than Equal to"  /> 
	  		<c:if test="${not empty deliveryTEndDate}">
						<s:text id="deliveryTEndDate" name="${FormDateValue}"> <s:param name="value" value="deliveryTEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryTEndDate" name="deliveryTEndDate" value="%{deliveryTEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryTEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty deliveryTEndDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryTEndDate" name="deliveryTEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryTEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="deliveryTargetDatePeriod"  list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'deliveryTBeginDate','deliveryTEndDate','deliveryTargetDatePeriod');selectdatePeriod(this);"/></td>	 
	  			<td class="listwhitetext" align="right">Delivery Actual Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="deliveryActBeginDateCondition" name="deliveryActBeginDateCondition" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty deliveryActBeginDate}">
						<s:text id="deliveryActBeginDate" name="${FormDateValue}"> <s:param name="value" value="deliveryActBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryActBeginDate" name="deliveryActBeginDate" value="%{deliveryActBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryActBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty deliveryActBeginDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryActBeginDate" name="deliveryActBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryActBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden  id="deliveryActEndDateCondition" name="deliveryActEndDateCondition" value="Less than Equal to" /> 
	  		<c:if test="${not empty deliveryActEndDate}">
						<s:text id="deliveryActEndDate" name="${FormDateValue}"> <s:param name="value" value="deliveryActEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryActEndDate" name="deliveryActEndDate" value="%{deliveryActEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryActEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty deliveryActEndDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryActEndDate" name="deliveryActEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryActEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="deliveryActualDatePeriod"  list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'deliveryActBeginDate','deliveryActEndDate','deliveryActualDatePeriod');selectdatePeriod(this);"/></td>	
	  		</tr>
	  		<c:if test="${accountInterface=='Y'}">
	  		<tr>
	  			<td class="listwhitetext" align="right">Invoice Posting Date</td>
	  			<td  width="5px"></td>
	  			
	  			<s:hidden  id="invPostingBeginDateCondition" name="invPostingBeginDateCondition" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty invPostingBeginDate}">
						<s:text id="invPostingBeginDate" name="${FormDateValue}"> <s:param name="value" value="invPostingBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="invPostingBeginDate" name="invPostingBeginDate" value="%{invPostingBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invPostingBeginDate}" >
						<td><s:textfield cssClass="input-text" id="invPostingBeginDate" name="invPostingBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden  id="invPostingEndDateCondition" name="invPostingEndDateCondition" value="Less than Equal to" /> 
	  		<c:if test="${not empty invPostingEndDate}">
						<s:text id="invPostingEndDate" name="${FormDateValue}"> <s:param name="value" value="invPostingEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="invPostingEndDate" name="invPostingEndDate" value="%{invPostingEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty invPostingEndDate}" >
						<td><s:textfield cssClass="input-text" id="invPostingEndDate" name="invPostingEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="invoicePostingDatePeriod"  list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'invPostingBeginDate','invPostingEndDate','invoicePostingDatePeriod');selectdatePeriod(this);"/></td>	 
	  		</c:if>
	  		<c:if test="${accountInterface!='Y'}">
	  		<s:hidden id="invPostingBeginDateCondition" name="invPostingBeginDateCondition" />
	  		 <s:hidden id="invPostingBeginDate" name="invPostingBeginDate" /> 
	  		 <s:hidden id="invPostingEndDateCondition" name="invPostingEndDateCondition" />
	  		 <s:hidden id="invPostingEndDate" name="invPostingEndDate" /> 
	  		 <s:hidden id="invoicePostingDatePeriod" name="invoicePostingDatePeriod" /> 
	  		</c:if>
	  		<c:if test="${accountInterface=='Y'}"> 
	  			<td class="listwhitetext" align="right">Payable Posting Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="payPostingBeginDateCondition" name="payPostingBeginDateCondition" value="Greater than Equal to" /> 
	  			<c:if test="${not empty payPostingBeginDate}">
						<s:text id="payPostingBeginDate" name="${FormDateValue}"> <s:param name="value" value="payPostingBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="payPostingBeginDate" name="payPostingBeginDate" value="%{payPostingBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="payPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty payPostingBeginDate}" >
						<td><s:textfield cssClass="input-text" id="payPostingBeginDate" name="payPostingBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="payPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if> 
	  			<s:hidden  id="payPostingEndDateCondition" name="payPostingEndDateCondition" value="Less than Equal to"   /> 
	  		<c:if test="${not empty payPostingEndDate}">
						<s:text id="payPostingEndDate" name="${FormDateValue}"> <s:param name="value" value="payPostingEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="payPostingEndDate" name="payPostingEndDate" value="%{payPostingEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="payPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty payPostingEndDate}" >
						<td><s:textfield cssClass="input-text" id="payPostingEndDate" name="payPostingEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="payPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="payablePostingDatePeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'payPostingBeginDate','payPostingEndDate','payablePostingDatePeriod');selectdatePeriod(this);"/></td>		
	  		 </tr>
	  		</c:if>
	  		<c:if test="${accountInterface!='Y'}">
	  		<s:hidden id="payPostingBeginDateCondition" name="payPostingBeginDateCondition" />
	  		 <s:hidden id="payPostingBeginDate" name="payPostingBeginDate" /> 
	  		 <s:hidden id="payPostingEndDateCondition" name="payPostingEndDateCondition" />
	  		 <s:hidden id="payPostingEndDate" name="payPostingEndDate" /> 
	  		 <s:hidden id="payablePostingDatePeriod" name="payablePostingDatePeriod" /> 
	  		</c:if> 
	  			<td class="listwhitetext" align="right">Revenue Recognition</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="revRecgBeginDateCondition" name="revRecgBeginDateCondition" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty revenueRecognitionBeginDate}">
						<s:text id="revenueRecognitionBeginDate" name="${FormDateValue}"> <s:param name="value" value="revenueRecognitionBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="revenueRecognitionBeginDate" name="revenueRecognitionBeginDate" value="%{revenueRecognitionBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="revenueRecognitionBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty revenueRecognitionBeginDate}" >
						<td><s:textfield cssClass="input-text" id="revenueRecognitionBeginDate" name="revenueRecognitionBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="revenueRecognitionBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   	</c:if>  
	  			<s:hidden id="revRecgEndDateCondition" name="revRecgEndDateCondition" value="Less than Equal to"  /> 
		  		<c:if test="${not empty revenueRecognitionEndDate}">
							<s:text id="revenueRecognitionEndDate" name="${FormDateValue}"> <s:param name="value" value="revenueRecognitionEndDate" /></s:text>
							<td><s:textfield cssClass="input-text" id="revenueRecognitionEndDate" name="revenueRecognitionEndDate" value="%{revenueRecognitionEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="revenueRecognitionEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty revenueRecognitionEndDate}" >
						<td><s:textfield cssClass="input-text" id="revenueRecognitionEndDate" name="revenueRecognitionEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="revenueRecognitionEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="revenueRecognitionPeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'revenueRecognitionBeginDate','revenueRecognitionEndDate','revenueRecognitionPeriod');selectdatePeriod(this);" /></td>		 
	  			<td class="listwhitetext" align="right">Invoicing date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="invoicingBeginDateCondition" name="invoicingBeginDateCondition" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty invoicingBeginDate}">
						<s:text id="invoicingBeginDate" name="${FormDateValue}"> <s:param name="value" value="invoicingBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="invoicingBeginDate" name="invoicingBeginDate" value="%{invoicingBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invoicingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invoicingBeginDate}" >
						<td><s:textfield cssClass="input-text" id="invoicingBeginDate" name="invoicingBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invoicingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   	</c:if> 
	  			<s:hidden  id="invoicingEndDateCondition" name="invoicingEndDateCondition" value="Less than Equal to"   />
	  	
		  		<c:if test="${not empty invoicingEndDate}">
							<s:text id="invoicingEndDate" name="${FormDateValue}"> <s:param name="value" value="invoicingEndDate" /></s:text>
							<td><s:textfield cssClass="input-text" id="invoicingEndDate" name="invoicingEndDate" value="%{invoicingEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invoicingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invoicingEndDate}" >
						<td><s:textfield cssClass="input-text" id="invoicingEndDate" name="invoicingEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invoicingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="InvoicingdatePeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'invoicingBeginDate','invoicingEndDate','InvoicingdatePeriod');selectdatePeriod(this);"/></td>	
	  		</tr> 
	  		<tr>
	  			<td class="listwhitetext" align="right">Storage Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="storageBeginDateCondition" name="storageBeginDateCondition" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty storageBeginDate}">
						<s:text id="storageBeginDate" name="${FormDateValue}"> <s:param name="value" value="storageBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="storageBeginDate" name="storageBeginDate" value="%{storageBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="storageBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty storageBeginDate}" >
						<td><s:textfield cssClass="input-text" id="storageBeginDate" name="storageBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="storageBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   	</c:if> 
	  			<s:hidden id="storageEndDateCondition" name="storageEndDateCondition" value="Less than Equal to"   /> 
		  		<c:if test="${not empty storageEndDate}">
							<s:text id="storageEndDate" name="${FormDateValue}"> <s:param name="value" value="storageEndDate" /></s:text>
							<td><s:textfield cssClass="input-text" id="storageEndDate" name="storageEndDate" value="%{storageEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="storageEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty storageEndDate}" >
						<td><s:textfield cssClass="input-text" id="storageEndDate" name="storageEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="storageEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if> 
				<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="storageDatePeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'storageBeginDate','storageEndDate','storageDatePeriod');selectdatePeriod(this);"/></td>	
	  		   <td class="listwhitetext" align="right">Received Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="receivedBeginDateCondition" name="receivedBeginDateCondition" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty receivedBeginDate}">
						<s:text id="receivedBeginDate" name="${FormDateValue}"> <s:param name="value" value="receivedBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="receivedBeginDate" name="receivedBeginDate" value="%{receivedBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="receivedBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty receivedBeginDate}" >
						<td><s:textfield cssClass="input-text" id="receivedBeginDate" name="receivedBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="receivedBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden id="receivedEndDateCondition" name="receivedEndDateCondition" value="Less than Equal to"   /> 
	  		<c:if test="${not empty receivedEndDate}">
						<s:text id="receivedEndDate" name="${FormDateValue}"> <s:param name="value" value="receivedEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="receivedEndDate" name="receivedEndDate" value="%{receivedEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="receivedEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty receivedEndDate}" >
						<td><s:textfield cssClass="input-text" id="receivedEndDate" name="receivedEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="receivedEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="receivedDatePeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'receivedBeginDate','receivedEndDate','receivedDatePeriod');selectdatePeriod(this);"/></td>		
	  		</tr>
	  		<tr>
	  		 <c:if test="${accountInterface=='Y'}"> 
	  			<td class="listwhitetext" align="right">Posting Period Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="periodBeginDateCondition" name="periodBeginDateCondition" value="Greater than Equal to "/> 
	  			<c:if test="${not empty postingPeriodBeginDate}">
						<s:text id="postingPeriodBeginDate" name="${FormDateValue}"> <s:param name="value" value="postingPeriodBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="postingPeriodBeginDate" name="postingPeriodBeginDate" value="%{postingPeriodBeginDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="postingPeriodBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty postingPeriodBeginDate}" >
						<td><s:textfield cssClass="input-text" id="postingPeriodBeginDate" name="postingPeriodBeginDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="postingPeriodBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden  id="periodEndDateCondition" name="periodEndDateCondition" value="Less than Equal to"  /> 
	  		<c:if test="${not empty postingPeriodEndDate}">
						<s:text id="postingPeriodEndDate" name="${FormDateValue}"> <s:param name="value" value="postingPeriodEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="postingPeriodEndDate" name="postingPeriodEndDate" value="%{postingPeriodEndDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="postingPeriodEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty postingPeriodEndDate}" >
						<td><s:textfield cssClass="input-text" id="postingPeriodEndDate" name="postingPeriodEndDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="postingPeriodEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="PostingPeriodDatePeriod"   list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'postingPeriodBeginDate','postingPeriodEndDate','PostingPeriodDatePeriod');selectdatePeriod(this);"/></td>	 
	  		 </c:if>
	  		 <c:if test="${accountInterface!='Y'}">
	  		 <s:hidden id="periodBeginDateCondition" name="periodBeginDateCondition" />
	  		 <s:hidden id="postingPeriodBeginDate" name="postingPeriodBeginDate" /> 
	  		 <s:hidden id="periodEndDateCondition" name="periodEndDateCondition" />
	  		 <s:hidden id="postingPeriodEndDate" name="postingPeriodEndDate" /> 
	  		 <s:hidden id="PostingPeriodDatePeriod" name="PostingPeriodDatePeriod" /> 
	  		 </c:if> 
	  			<td class="listwhitetext" align="right">Create Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="createDateCondition" name="createDateCondition" value="Greater than Equal to"   />
	  			<c:if test="${not empty createDate}">
	  			<s:text id="createDate" name="${FormDateValue}"> <s:param name="value" value="createDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="createDate" name="createDate" value="%{createDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="createDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if>
	  		    <c:if test="${empty createDate}" >
	  		    <td><s:textfield cssClass="input-text" id="createDate" name="createDate" value="%{createDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="createDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if> 
	  			<s:hidden id="toCreateDateCondition" name="toCreateDateCondition" value="Less than Equal to"  />
	  			<c:if test="${not empty toCreateDate}">
	  			<s:text id="toCreateDate" name="${FormDateValue}"> <s:param name="value" value="toCreateDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="toCreateDate" name="toCreateDate" value="%{toCreateDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="toCreateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <c:if test="${empty toCreateDate}">
	  		   <td><s:textfield cssClass="input-text" id="toCreateDate" name="toCreateDate" value="%{toCreateDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="toCreateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="createDatePeriod" list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'createDate','toCreateDate','createDatePeriod');selectdatePeriod(this);"/></td>		  		   
	  		  </tr> 
	  		 <tr>
	  			<td class="listwhitetext" align="right">Booking&nbsp;Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="bookingDateCondition" name="bookingDateCondition" value="Greater than Equal to"   />
	  			<c:if test="${not empty bookingDate}">
	  			<s:text id="bookingDate" name="${FormDateValue}"> <s:param name="value" value="bookingDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="bookingDate" name="bookingDate" value="%{bookingDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="bookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if>
	  		    <c:if test="${empty bookingDate}" >
	  		    <td><s:textfield cssClass="input-text" id="bookingDate" name="bookingDate" value="%{bookingDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="bookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if> 
	  			<s:hidden id="toBookingDateCondition" name="toBookingDateCondition" value="Less than Equal to"  />
	  			<c:if test="${not empty toBookingDate}">
	  			<s:text id="toBookingDate" name="${FormDateValue}"> <s:param name="value" value="toBookingDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="toBookingDate" name="toBookingDate" value="%{toBookingDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="toBookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <c:if test="${empty toBookingDate}">
	  		   <td><s:textfield cssClass="input-text" id="toBookingDate" name="toBookingDate" value="%{toBookingDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="toBookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="bookingDatePeriod" list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'bookingDate','toBookingDate','bookingDatePeriod');selectdatePeriod(this);"/></td>		  		   
	  		   <td class="listwhitetext" align="right">Update Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="updateDateCondition" name="updateDateCondition" value="Greater than Equal to"   />
	  			<c:if test="${not empty updateDate}">
	  			<s:text id="updateDate" name="${FormDateValue}"> <s:param name="value" value="updateDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="updateDate" name="updateDate" value="%{updateDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="updateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if>
	  		    <c:if test="${empty updateDate}" >
	  		    <td><s:textfield cssClass="input-text" id="updateDate" name="updateDate" value="%{updateDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="updateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if> 
	  			<s:hidden id="toUpdateDateCondition" name="toUpdateDateCondition" value="Less than Equal to"  />
	  			<c:if test="${not empty toUpdateDate}">
	  			<s:text id="toUpdateDate" name="${FormDateValue}"> <s:param name="value" value="toUpdateDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="toUpdateDate" name="toUpdateDate" value="%{toUpdateDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="toUpdateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <c:if test="${empty toUpdateDate}">
	  		   <td><s:textfield cssClass="input-text" id="toUpdateDate" name="toUpdateDate" value="%{toUpdateDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="toUpdateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="updateDatePeriod" list="%{periodScheduler}"  headerKey="" headerValue="" onchange="checkPeriodDate(this,'updateDate','toUpdateDate','updateDatePeriod');selectdatePeriod(this);"/></td>		  		   
	  		  
	  		</tr>
	  		</tbody>
	  		</table>
	  		</div>	  		
	  		</td>
	  		</tr> 
	  		 <c:if test="${companyDivisionFlag!='Yes'}">
	  		  <s:hidden id="companyCodeCondition" name="companyCodeCondition" />
	  		 <s:hidden id="companyCode" name="companyCode" /> 
	  		 </c:if>
	  		 <tr>
	  			<td height="5px" colspan="4"></td>
	  		</tr>
	  		
	  		<tr>
   	           <td colspan="15" align="left" class="vertlinedata"></td>
   	        </tr>
   	        
   	        <tr><td height="5px"></td></tr>
   	        <tr>
   	        
   	          <s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:choose>
							<c:when test="${submitType == 'job'}">
							<td align="center" colspan="7"><s:reset type="button" cssClass="cssbutton" align="top" cssStyle="width:100px; height:25px" key="Reset" /></td>
							</c:when>							
	 					</c:choose>	 					
	 					<td colspan="10">
	 					<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0">	 					 
	 					<tr><td><div id="autoGenerator">
	 					 <table class="detailTabLabel" border="0">
	 					 <tr>
			<td  class="bgblue" colspan="9">Edit Query Type, Name and Scheduler</td>
			</tr>
			</table>
	  	   <table class="detailTabLabel" border="0"> 
	  	   <tr> 
	  	   <td>
	  	    <div id="autoGenerator">
	  	   <table class="detailTabLabel">	  	  
	  	   <tr>
	 		<td class="listwhitetext" align="right">Auto Generator Extract & Email</td>
	  	   <td align="left" ><s:checkbox name="autoGenerator"  fieldValue="true" disabled="disabled" onclick="checkScheduler(this);"/></td>
	  	   </tr></table></div>
	  	   </td>
	  	  <td>
	  	       <div id="emailDiv">
	  	       <table class="detailTabLabel">
	  	       <tr>
	  	       <td class="listwhitetext" align="right" width="60px">E-Mail<font color="red" size="2">*</font></td>
	  	       <td><s:textfield name="email"  key="email" required="true" value="${email}" cssClass="input-text" size="30"  onchange="checkEmail();"/></td>
	  	       <td class="listwhitetext" align="right"><b>&nbsp;&nbsp;For multiple emails, please use comma to separate addresses</b></td>
	  	       </tr>
	  	       </table></div></td>
	  	   </tr>
	  	   </table>
	  	   <table class="detailTabLabel" border="0">
	  	   <tr>   
	  	   <td>
	  	    <div id="Scheduler">
	  	   <table class="detailTabLabel">
	  	   <tr>
	  	   <td class="listwhitetext" align="right">Scheduling Criteria<font color="red" size="2">*</font></td>
	  	   <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="queryScheduler"  value="%{queryScheduler}" list="%{querySchedulerList}"  headerKey="" headerValue="" onchange="onloadCheckScheduler(document.forms['reportDataExtractForm'].elements['autoGenerator'].value);" /></td>
	  	   </tr></table>
	  	   </div></td>
	  	   <td>
	  	    <div id="weeklyScheduler">
	  	   <table class="detailTabLabel">
	  	   <tr>
	  	   <td class="listwhitetext" align="right" >Weekly Scheduler<font color="red" size="2">*</font></td>
	  	   <td ><s:select cssClass="list-menu" cssStyle="width:50px"  name="perWeekScheduler"  value="%{perWeekScheduler}" list="%{perWeekSchedulerList}"  headerKey="" headerValue="" /></td>
	  	   </tr></table></div></td>
	  	   <td>
	  	    <div id="monthalyScheduler">
	  	   <table class="detailTabLabel">
	  	   <tr>
	  	   <td class="listwhitetext" align="right" >Monthly Scheduler<font color="red" size="2">*</font></td>
	  	   <td ><s:select cssClass="list-menu" cssStyle="width:50px"  name="perMonthScheduler"  value="%{perMonthScheduler}" list="%{perMonthSchedulerList}"  headerKey="" headerValue="" /></td>
	  	   </tr></table></div></td>
	  	   <td>
	  	    <div id="quoterlyScheduler">
	  	   <table class="detailTabLabel">
	  	   <tr>
	  	   <td class="listwhitetext" align="right" >Quarterly Scheduler</td>
	  	   <td ><s:select cssClass="list-menu" cssStyle="width:50px"  name="perQuarterScheduler"  value="%{perQuarterScheduler}" list="%{perQuarterSchedulerList}"  headerKey="" headerValue="" /></td>
	  	   </tr>
	  	   </table>
	  	   </div>
	  	   </td> 
	  	   <c:if test="${not empty id}"> 
	  	   <td><table class="detailTabLabel"><tr> 
	  	    <td class="listwhitetext" align="left" >Query Type<font color="red" size="2">*</font></td>
	  	    <td><s:select list="{'private','public'}" name="publicPrivateFlag" cssClass="list-menu" cssStyle="width:111px" ></s:select></td>
	  	    <td class="listwhitetext" align="left">Query Name<font color="red" size="2">*</font></td>
	  	    <td><s:textfield name="queryName" title="queryName" key="queryName" required="true" value="${queryName}" cssClass="input-text" size="40" maxlength="30"  /></td>
	  	    </tr></table></td> 
	  	   </c:if> 
	  	   </tr></table></div>
	  	   <c:if test="${empty id}"> 
	  	    <s:hidden id="publicPrivateFlag" name="publicPrivateFlag"/>
            <s:hidden id="queryName" name="queryName" value="${queryName}"/>
	  	   </c:if> 
	  		<table>
	  		 <tr><td  height="13" class="listwhitetext" colspan="13" style="text-align:left; margin: 0px;">
	  		
	  		 <div class="" onClick="selectReport();" style="cursor: pointer;">
	  		 <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
			<tr>
			<td  class="bgblue" colspan="11">Select Data Extract</td>
			</tr>
            </table>
	  		</div>
			<div id="report" > 
	<table>	
	<tr>
	<td colspan="11">
	<table style="margin-bottom:0px;width:100%;" border="0">	  			  			  		
	  	   <tr>
	  	   <td height="" width="" class="listwhitetext" align="left" style="font-size: 10px;">
	  	   <label for="r1"><input  type="radio" name="radiobilling" id="shipmentAnalysis"  value="1" onclick="showColumns(this);"/><b>Shipment Analysis</b></label></td>
	  	   <td height="" width="" class="listwhitetext" align="right" valign="" style="vertical-align:bottom;"><a href="#"><img src="<c:url value='/images/info.png'/>" width="16" height="16" border="0" align="bottom" title="Extract Info" /></a></td><td> |&nbsp;&nbsp;</td>
   	       
   	     
	  	   <td height="" width="" class="listwhitetext" align="left" style="font-size: 10px;">
	  	   <label for="r1"><input style="" type="radio" name="radiobilling" id="domesticAnalysis" value="8" onclick="showColumns(this);"/><b>Domestic Analysis</b></label></td>
	  	   <td height="" width="" class="listwhitetext" align="right" valign="" style="vertical-align:bottom;"><a href="#"><img src="<c:url value='/images/info.png'/>" width="16" height="16" border="0" align="bottom" title="Extract Info"/></a></td><td> |&nbsp;&nbsp;</td>
   	       
   	      <td height="" width="" class="listwhitetext" style="font-size: 10px;">
	  	   <label for="r1"><input style="" type="radio" name="radiobilling" id="dynamicFinancialSummary" value="4" onclick="showColumns(this);"/><b>Dynamic Financial Summary</b></label></td>
    	   <td height="" width="" class="listwhitetext" valign="" align="right" style="vertical-align:bottom;">
    	   <a href="#" style="vertical-align:bottom;">
			<img onclick="window.open('resources/extracts/DynamicFinancialSummary_info.pdf?decorator=popup&popup=true','DynamicFinancialSummary_info','height=800,width=750,top=0, left=610, scrollbars=no,resizable=yes');" src="<c:url value='/images/info.png'/>" width="16" height="16" border="0" title="Extract Info" border="0" align="bottom" />
			</a></td><td> |&nbsp;&nbsp;</td>   
    	
    	  
	  	   <td height="" width="" class="listwhitetext" style="font-size: 10px;"><label for="r1">
	  	   <input style="" type="radio" name="radiobilling" id="dynamicFinancialDetails" value="7" onclick="showColumns(this);"/><b>Dynamic Financial Details</b></label></td>
 	   		<td height="" width="" class="listwhitetext" align="right" valign="" style="vertical-align:bottom;"><a href="#"><img src="<c:url value='/images/info.png'/>" width="16" align="bottom" height="16" border="0" title="Extract Info" /></a></td><td> |&nbsp;&nbsp;</td>
 	   		
	  	 
		  	    <td height="" width="" class="listwhitetext" style="font-size: 10px;">
		  	    <label for="r1"><input style="" type="radio" name="radiobilling" id="storageBillingAnalysis" value="9" onclick="showColumns(this);" /><b>Storage Billing Analysis</b></label></td>
	 	   		<td height="" width="" class="listwhitetext" align="right" valign="" style="vertical-align:bottom;"><a href="#"><img src="<c:url value='/images/info.png'/>" width="16" align="bottom" height="16" border="0" title="Extract Info" /></a></td><td> |&nbsp;&nbsp;</td>
	 	   		
	  	   <td height="" width="" class="listwhitetext" align="left" style="font-size: 10px;">
           <label for="r1"><input  type="radio" name="radiobilling" id="RelocationAnalysis"  value="10" onclick="showColumns(this);"/><b>Relocation Analysis</b></label></td>
           <td height="" width="" class="listwhitetext" align="right" valign="" style="vertical-align:bottom;"><a href="#"><img src="<c:url value='/images/info.png'/>" width="16" height="16" border="0" align="bottom" title="Extract Info" /></a></td><td> |&nbsp;&nbsp;</td>
	  	   	</tr>
	  	   	</table>
	  	   	</td>
	  	   	</tr>	  		
     </table>
     </div>    
     </td>     
     </tr>     
     </table>
     </td></tr>
     </table>
     </td>
     </tr>
     </table>    
     </div>     
	<div class="bottom-header"><span></span></div> 
</div>
</div>
     <!-- Arvind  -->	  	
	  	
	  	<div id="hid1">
	  	<table class="tablegrey" style="margin-bottom:0px;width:100%;" cellspacing="0" cellpadding="0" border="0">
    <tr><td height="10px"> </td></tr>
    <tr><td width="15px"> </td>
    <td  class="bgblue" >Select columns to be extracted   <input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong><input type="radio"  name="chk" onClick="unCheckAll()" /><strong>Uncheck All</strong></td></tr>
	<tr><td width="10px" style="margin:0px;"></td><td>
	<table style="margin-bottom:0px;margin:0px; ">
	<tr>
	<td valign="top">
	  	<table border="0" style="margin:0px;">
	  	<tr><td valign="top">
	  	<s:set name="columnList1" value="columnList1" scope="request"/>  
         <display:table name="columnList1" class="table" requestURI="" id="columnList1" style="width:100%;font-size:.90em "  pagesize="100" decorator="">   
 		 <display:column property="displayName" sortable="true" title="Column Name" style="width:70px" /> 
         <display:column title=""   style="width:10px"> 
	     <input type="checkbox" name="checkV" id="${columnList1.columnSequenceNumber}" value="${columnList1.columnSequenceNumber}" style="margin-left:10px;" onclick="selectColumn(this)" />
        </display:column> 
        </display:table> 
	  	</td>
	  	<td valign="top">
	  	<s:set name="columnList2" value="columnList2" scope="request"/>  
         <display:table name="columnList2" class="table" requestURI="" id="columnList2" style="width:100%;font-size:.90em "  pagesize="100" decorator="">   
 		 <display:column property="displayName" sortable="true" title="Column Name" style="width:70px" /> 
         <display:column title=""   style="width:10px"> 
	     <input type="checkbox" name="checkV" id="${columnList2.columnSequenceNumber}" value="${columnList2.columnSequenceNumber}" style="margin-left:10px;" onclick="selectColumn(this)" />
        </display:column></display:table>
        </td>
        <td valign="top">
	  	<s:set name="columnList3" value="columnList3" scope="request"/>  
         <display:table name="columnList3" class="table" requestURI="" id="columnList3" style="width:100%;font-size:.90em "  pagesize="100" decorator="">   
         <display:column property="displayName" sortable="true" title="Column Name" style="width:70px" /> 
         <display:column title=""   style="width:10px"> 
	     <input type="checkbox" name="checkV"  id="${columnList3.columnSequenceNumber}" value="${columnList3.columnSequenceNumber}" style="margin-left:10px;" onclick="selectColumn(this)" />
        </display:column></display:table>
        </td> 
        <td valign="top">
	  	<s:set name="columnList4" value="columnList4" scope="request"/>  
         <display:table name="columnList4" class="table" requestURI="" id="columnList4" style="width:100%;font-size:.90em "  pagesize="100" decorator="">   
         <display:column property="displayName" sortable="true" title="Column Name" style="width:70px" /> 
         <display:column title=""   style="width:10px"> 
	     <input type="checkbox" name="checkV"  id="${columnList4.columnSequenceNumber}" value="${columnList4.columnSequenceNumber}" style="margin-left:10px;" onclick="selectColumn(this)" />
        </display:column></display:table>
        </td> 
        <td valign="top">
	  	<s:set name="columnList5" value="columnList5" scope="request"/>  
         <display:table name="columnList5" class="table" requestURI="" id="columnList5" style="width:100%;font-size:.90em "  pagesize="100" decorator="">   
         <display:column property="displayName" sortable="true" title="Column Name" style="width:70px" /> 
         <display:column title=""   style="width:10px"> 
	     <input type="checkbox" name="checkV"  id="${columnList5.columnSequenceNumber}" value="${columnList5.columnSequenceNumber}" style="margin-left:10px;" onclick="selectColumn(this)" />
        </display:column></display:table>
        </td> 
        </tr>
	  	</table></div>
	  	</td> 
	  	</tr></table>
	  	
	  		</td></tr>
	  		
	  		</table>
</div>


<table style="margin:5px; padding:5px;" cellpadding="0" cellspacing="0"><tr>
   	         <td align="left" style="padding-left:18px;padding-top:10px;">
   	           <input type="button" class="cssbutton" style="width:110px; height:27px" align="top"  value="Extract Data" name="Show_Reports" onclick="return findPreviewBillingReport();"/>
   	         </td> 
   	         <td align="left"  style="padding-left:18px;padding-top:10px;"> 
   	           <input type="button" class="cssbutton" style="width:110px; height:27px" align="top"  value="Save Queries" name="Save_Queries" onclick="createTextAreaSave1();" />
   	         </td> 
   	         
             
   	    
</tr></table>

<div id="GIS">
<DIV ID="layerH" style="position:absolute; padding:180px 350px; z-index:999;">
<table cellspacing="0" cellpadding="3" align="center">			
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="200px">
	           <font color="#0F0F0F" face="Arial" size="3px" weight="bold">Processing...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="200px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
</DIV>



<table border="0" cellpadding="2" cellspacing="1" width="100%" style="margin:0px;padding: 0px;">
    <tbody>
<tr>
 
</tr>
</tbody>
</table>

</div>

</div>

<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" /> 
 
</s:form> 
<script type="text/javascript"> 
showOrHide(0);
try{
onloadCheckScheduler(${autoGenerator});
}
catch(e){}
try{
if(document.forms['reportDataExtractForm'].elements['columnButton'].value=='yes'){ 
document.getElementById("hid1").style.display="block"; 
animatedcollapse.addDiv('parameterDiv', 'fade=0,persist=0,hide=1,speed=90') 
animatedcollapse.addDiv('billing', 'fade=1,persist=0,hide=0,speed=90') 
animatedcollapse.addDiv('report', 'fade=1,persist=0,show=1,speed=90')  


animatedcollapse.init()
document.getElementById('${reportName}').checked="checked";
checkColumn(); 
}
}
catch(e){}
try{
onloadCheckButton(); 
}
catch(e){}
try{
if(document.forms['reportDataExtractForm'].elements['columnButton'].value!='yes'){
document.getElementById("hid1").style.display="none"; 
animatedcollapse.addDiv('parameterDiv', 'fade=0,persist=0,show=1,speed=90') 
animatedcollapse.addDiv('billing', 'fade=1,persist=0,hide=0,speed=90') 
animatedcollapse.addDiv('report', 'fade=1,persist=0,hide=1,speed=90')  


animatedcollapse.init()
}
}
catch(e){}  
     //buttonDisable();
    //Form.focusFirstElement($("reportForm")); 
</script>

<script type="text/javascript">
 function showHide1(obj) {
   var div = document.getElementById(obj);
     div.style.display = 'none';
   }
   function showHide2(obj) {
   var div = document.getElementById(obj);
     div.style.display = 'none';
   }
 
</script>

<script type="text/javascript">
    var myimage = document.getElementsByTagName("img");
   	for (var i = 0; i < myimage.length; i++) {
	   	var idinms=myimage[i].getAttribute('id');
	   	if(idinms != null && (idinms.split('_')[1]=="trigger")){
		   	var args1=idinms.split('_');
		   	RANGE_CAL_1 = new Calendar({
		           inputField: args1[0],
		           dateFormat: "%d-%b-%y",
		           trigger: idinms,
		           bottomBar: true,
		           animation:true,
		           onSelect: function() {
		        	   var dateField = this.inputField.id;
		        	   var dateVal = this.inputField.value;
		        	   findFieldName(dateField,dateVal,'DummyName');
		               this.hide();		              
		       }
		   });
	   	}
   	}

 <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
  	window.onload = function() {  
  		document.getElementById("findOldQueriesId").style.display = "none";	 	
  	  	var testArmstrong='${armstrongPortal}'	
  	  	  	if(testArmstrong=='true'){  	
  	  	  	
  	  	    document.forms['reportDataExtractForm'].elements['jobTypesCondition'].value='Equal to';
  	  		document.forms['reportDataExtractForm'].elements['jobTypesCondition'].disabled=true;
  			document.forms['reportDataExtractForm'].elements['jobTypesCondition'].className = 'input-textUpper';	
  			
  	  		document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].value='Equal to';
  	  		document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].disabled=true;
  			document.forms['reportDataExtractForm'].elements['bookingCodeCondition'].className = 'input-textUpper';	
  			document.forms['reportDataExtractForm'].elements['bookingCodeType'].value='${agentParentForPortal}';
  	  		document.forms['reportDataExtractForm'].elements['bookingCodeType'].readOnly =true;
  			document.forms['reportDataExtractForm'].elements['bookingCodeType'].className = 'input-textUpper';	
  			var el = document.getElementById("bookingCodeTypeId");
			try{
			el.onclick = false;
			document.images["bookingCodeTypeId"].src = 'images/navarrow.gif';
			}catch(e){}
			
  	  	  	}else{ 
  	  		document.forms['reportDataExtractForm'].elements['salesmanCondition'].value='Equal to';
  	  		document.forms['reportDataExtractForm'].elements['salesmanCondition'].disabled=true;
  			document.forms['reportDataExtractForm'].elements['salesmanCondition'].className = 'input-textUpper';	
  			document.forms['reportDataExtractForm'].elements['salesmanType'].value='${salesPerson}';
  	  		document.forms['reportDataExtractForm'].elements['salesmanType'].disabled=true;
  			document.forms['reportDataExtractForm'].elements['salesmanType'].className = 'input-textUpper';  	  	  	
  	  	  	}
  	}
  </sec-auth:authComponent>
  function findOriginAgentCode(temp){
	
    if(temp.value==''){
	    	document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value="";
	    }else
	   {
	    progressBarAutoSave('1');
	     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(temp.value);
	     http3.open("GET", url, true);
	     http3.onreadystatechange =handleHttpResponse44;
	     http3.send(null);
	    }}
  
  function handleHttpResponse44() {
       if (http3.readyState == 4)
       {
          var results = http3.responseText
          results = results.trim();
          if(results.length>=1)
          {
				
           }
           else 
           {
              alert("Invalid  Origin Agent Code Type, please select another");
               document.forms['reportDataExtractForm'].elements['originAgentCodeType'].value = "";
         } }
         progressBarAutoSave('0');
         }
  
      function findDestiAgentCode(temp){
      
	   if(temp.value==''){
	    	document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value="";
	    }
	    else
	    	{
	    	progressBarAutoSave('1');
	     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(temp.value);
	     http4.open("GET", url, true);
	     http4.onreadystatechange =handleHttpResponse444;
	     http4.send(null);
	    }}
      function handleHttpResponse444(){
        if (http4.readyState == 4)
        {
        var results = http4.responseText;
        results = results.trim();
        if(results.length>=1)
        	{
      		
         }
         else 
         {
             alert("Invalid  Destination Agent Code Type, please select another");
             document.forms['reportDataExtractForm'].elements['destinationAgentCodeType'].value = "";
          }} 
          progressBarAutoSave('0');
          }
  
</script>
