DELIMITER $$

 

DROP FUNCTION IF EXISTS `redsky`.`MatchCategory` $$

CREATE FUNCTION `MatchCategory`(v int(20), cat char(30)) RETURNS int(11)

BEGIN

  declare i int;

 

select if(count(*)=0,v,0) into i from accountline where serviceorderID=v and category=cat;

 

  return i;

END $$
 

DELIMITER ;

 