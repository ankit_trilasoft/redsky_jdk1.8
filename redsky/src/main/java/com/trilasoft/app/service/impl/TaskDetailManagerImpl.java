package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.TaskDetailDao;
import com.trilasoft.app.model.TaskDetail;
import com.trilasoft.app.service.TaskDetailManager;

public class TaskDetailManagerImpl extends GenericManagerImpl<TaskDetail, Long> implements TaskDetailManager{
	TaskDetailDao taskDetailDao;
	public TaskDetailManagerImpl(TaskDetailDao taskDetailDao) {
        super(taskDetailDao);
        this.taskDetailDao = taskDetailDao;
	}
	
public void saveTaskDetails(Long id, String fieldName, String fieldValue, String sessionCorpID, String selectedWO,String userType){
		taskDetailDao.saveTaskDetails(id, fieldName, fieldValue, sessionCorpID,selectedWO,userType);
	}

public List getTaskFromWorkOrder(String shipNumber, String selectedWO){
	return taskDetailDao.getTaskFromWorkOrder(shipNumber, selectedWO);
}

public String getValueFromTaskDetails(Long id, String shipNumber){
	return taskDetailDao.getValueFromTaskDetails(id, shipNumber);
}

public void updateTransferField(Long id, String shipNumber){
	taskDetailDao.updateTransferField(id, shipNumber);
}

}
