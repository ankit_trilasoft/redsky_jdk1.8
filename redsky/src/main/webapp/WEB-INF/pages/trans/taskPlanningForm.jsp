<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>
 
<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Sunil at 08-Nov-2019 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!-- Modification closed here -->
  



<title>Task&nbsp;Detail</title>
  <meta name="heading" content="Task Planning&nbsp;Detail" />
	
</head>

<s:form name="taskPlanForm" id="taskPlanForm" action="saveTaskPlanning" method="post" validate="true">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
    <s:hidden name="customerFile.id" />
    <s:hidden name="customerFile.email" />
	<s:hidden name="taskPlanning.id"/>
	<s:hidden name="taskPlanning.serviceOrderId"/>
	<s:hidden name="taskPlanning.customerFileId"/>
	<s:hidden name="taskPlanning.orgLocId"/>
	<s:hidden name="taskPlanning.destLocId"/>
	<s:hidden name="taskPlanning.taskTypeId"/>
	<s:hidden name="taskPlanning.taskStatus"/>
	<s:hidden name="uuidCompany"/>
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Task&nbsp;Details<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
              <li><a href="taskPlanningList.html?cid=${customerFile.id}"><span>Task Planning</span></a></li>
              <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
         </ul>
        </div><div class="spn">&nbsp;</div>
        
        </div>
        <div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
     <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">   
   
   <table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin: 0px;padding: 0px;" >
   <tr>
   <td valign="top" style="margin: 0px;padding: 0px;">
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	
	
	                    
				      	<tr>			
							<td class="listwhitetext"  >External?<font color="red">*</font></td>
							</tr>
	 					    <td align="left" ><s:select cssClass="list-menu" list="{'N','Y'}" id="taskVia" name="taskPlanning.taskVia" cssStyle="width:103px"  onchange="showhide(this.value);"/> </td>
					  	  	<tr>			
							
					  	    
	 					  <td width="95px" class="listwhitetext">Task Type</td>
	 					  <td></td>
	 					  <td class="listwhitetext"  >Duration of task in days</td>
					  		</tr> 
						<tr>
				      	<tr>			
							
	 					     <td  width="95px"><s:select cssClass="list-menu" list="{'Survey','VideoSurvey','Packing','Delivery'}" name="taskPlanning.taskType" cssStyle="width:256px;" headerKey="" headerValue="" onchange="disabledBtn();getAssigneeValues(this.value);"/></td>
					  	    		<td></td>
					  	    							<td   > <s:textfield name="taskPlanning.taskDuration" cssStyle="width:139px" size="15" maxlength="200" cssClass="input-text" /></td>
					  	    
					  		</tr> 
					  	
					  	<tr>
					  	   <td class="listwhitetext" width="95px" >Location(Origin)<font color="red">*</font></td>
					  	 
					  	   <td class="listwhitetext" colspan="2"  align="left"  >Task Date</td>
						   <!-- <td  class="listwhitetext" >To:</td> -->
						  <td></td>
						  <td></td>
						   <td class="listwhitetext" >Complete task before</td>
					  	</tr>
						<tr>
							<!-- <td class="listwhitetext"align="right"  >Location(Origin)<font color="red">*</font></td> -->
							<td align="left" width="95px"><s:select cssClass="list-menu" list="{'CustomerFile','ServiceOrder','WorkTicket'}" name="taskPlanning.taskLocation" cssStyle="width:256px;" onchange="disabledBtn()"/> </td>
						    <!-- <td class="listwhitetext"align="right"  >Duration of task in days</td> -->
						            <c:if test="${not empty taskPlanning.taskDate}">
							<s:text id="taskDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="taskPlanning.taskDate"/></s:text>
							<td width="95px"><s:textfield  cssClass="input-text" id="taskDate" name="taskPlanning.taskDate" value="%{taskDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onchange="disabledBtn()"/>
							<img id="taskDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<c:if test="${empty taskPlanning.taskDate}">
							<td width="95px"><s:textfield   cssClass="input-text" id="taskDate" name="taskPlanning.taskDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onchange="disabledBtn()" />
							<img id="taskDate_trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 							
							</c:if>
							
							<%-- <td><s:textfield  cssClass="input-text"   size="7" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"/>
							
							<img id="taskDates_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""/>
							</td> --%>
	
							
						<!-- 	<td class="listwhitetext"  align="right" ></td>  -->
							
							<td align="left" class="listwhitetext" style="width:120px;">Time<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" >	From:		
		                    <s:textfield id="taskTimeFrom" cssClass="input-text" name="taskPlanning.taskTimeFrom" size="3" maxlength="5" onchange="completeTimeString();" onkeydown="disabledBtn()"/></td>
		                    <td align="right" class="listwhitetext" style="width:20px;">To:</td> 				
		                    <td style="width:80px;"><s:textfield id="taskTimeTo" cssClass="input-text" name="taskPlanning.taskTimeTo" size="3" maxlength="5" onchange="completeTimeString();" onkeydown="disabledBtn()"/></td>
		
	   
	 
							<!-- <td class="listwhitetext" align="right" style="width:20px;" >Completeuu task before</td> -->
													
							<c:if test="${not empty taskPlanning.taskCompleteBefore}">
							<s:text id="taskPlanningTaskCompleteBeforeFormattedValue" name="${FormDateValue}"><s:param name="value" value="taskPlanning.taskCompleteBefore"/></s:text>
							<td><s:textfield cssClass="input-text" id="taskCompleteBefore" name="taskPlanning.taskCompleteBefore" value="%{taskPlanningTaskCompleteBeforeFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="onlyDel(event,this)" />
							<img id="taskCompleteBefore_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						
							</c:if>
							<c:if test="${empty taskPlanning.taskCompleteBefore}">
							<td><s:textfield cssClass="input-text" id="taskCompleteBefore" name="taskPlanning.taskCompleteBefore" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="onlyDel(event,this)" />
							<img id="taskCompleteBefore_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							
							</c:if>
							
							
							        
						</tr>
						<tr>
						<td class="listwhitetext taskCompany">Company</td>
	 					  
					  	 <td class="listwhitetext planner" align="left" class="listwhitetext" style="width:120px;">Company Planner</td>
	 					   
						  <td class="listwhitetext"   >Assignee</td>
						   
						</tr> 
						<tr>
							<!-- <td class="listwhitetext"  align="right" >Task Type</td> -->
							<td class="taskCompany"><s:select cssClass="list-menu taskCompany" list="%{extCompany}" name="taskPlanning.taskCompany" cssStyle="width:256px" onchange="getPlannerValues(this.value);getAssigneeValues(this.value);disabledBtn()"/> </td>
					  	    
	 					    <td class="planner"><s:select cssClass="list-menu"  list="%{planner}" cssStyle="width:139px" name="taskPlanning.taskPlanner" headerKey="" headerValue="" onchange="disabledBtn()"/> </td>
					  	   
							<td> <s:select cssClass="list-menu" list="%{estimatorList}" cssStyle="width:139px" name="taskPlanning.taskAssignee" onchange="disabledBtn()"/></td>
							
							<!-- <td class="listwhitetext"  align="right" >Task Date</td> -->
							
							
							
						</tr>
						<tr>
						
							<td class="listwhitetext"  align="left" style="    width: 270px;">Comment
							<s:textarea name="taskPlanning.taskComment" cols="39" cssClass="textarea" onkeyup="" onblur=""  rows="3" onchange="" tabindex=""/>
                            </td>
							
						</tr>
						<tr>
						<td align="left" class="listwhitetext" width="50">
						<c:if test="${moveCount > 0}">						
							
							<img class="openpopup" id="openpopup.img1" style="vertical-align:text-bottom;margin-left:17px; margin-top:1px;"  title="Move Cloud" src="<c:url value='/images/move4u.png'/>"  />
							<c:choose> 
			                <c:when test="${emailSetupValue=='Ready For Sending'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'Ready For Sending')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" title="${fn:replace(emailSetupValue,'Ready For Sending','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</c:if>
						 <c:if test="${not empty taskPlanning.id}">
						   <input type="button" class="cssbutton" name="SendtoMoveDashboard" value="Send to MoveDashboard" onclick="sendMailFromVoxme('MoveToCloud')" style="width:155px; height:28px;margin-top:10px;"/>
						   </c:if>
						 </td>
						
						</tr>
						</table>
						
						</td>
						</tr>
						</table>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
						

		   <s:hidden name="taskPlanning.corpID"/>				
		<table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="width:780px;">
		   <tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<td valign="top"></td>
						
						<td style="width:120px">
								<fmt:formatDate var="taskPlanningCreatedOnFormattedValue" value="${taskPlanning.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="taskPlanning.createdOn" value="${taskPlanningCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${taskPlanning.createdOn}" pattern="${displayDateTimeFormat}"/>
						</td>
						
						<td align="right" class="listwhitetext" width="70"><b><fmt:message key='customerFile.createdBy' /></b></td>
						
						<c:if test="${not empty taskPlanning.id}">
							<s:hidden name="taskPlanning.createdBy" />
							<td ><s:label name="createdBy" value="%{taskPlanning.createdBy}" /></td>
						</c:if>
						<c:if test="${empty taskPlanning.id}">
							<s:hidden name="taskPlanning.createdBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						<fmt:formatDate var="taskPlanningUpdatedOnFormattedValue" value="${taskPlanning.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<s:hidden name="taskPlanning.updatedOn"  value="${taskPlanningUpdatedOnFormattedValue}"/>
						<td style="width:130px"><fmt:formatDate value="${taskPlanning.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty taskPlanning.id}">
							<s:hidden name="taskPlanning.updatedBy" />
							<td ><s:label name="updatedBy" value="%{taskPlanning.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty taskPlanning.id}">
							<s:hidden name="taskPlanning.updatedBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
					</tr>
				</tbody>
			</table>	
	</div>
	<div style="padding-left:15px; padding-top:10px;">
	<s:submit cssClass="cssbuttonA" method="save" key="button.save"
		cssStyle="width:55px; height:25px" tabindex="23"/>
		</div>
</s:form>

<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
<c:if test="${redirectflag=='1'}">
<c:redirect url="/editTaskPlanning.html?id=${taskPlanning.id}&cid=${customerFile.id}"/>
</c:if>

function changeStatus() {	
}
function completeTimeString() {	
	stime1 = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value;
	stime2 = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value;
	stime3="";
	stime4="";
	if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
		if(stime1.length==1 || stime1.length==2){
			if(stime1.length==2){
				document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value = stime1 + ":00";
			}
			if(stime1.length==1){
				document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value = "0" + stime1 + ":00";
			}
		}else{
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value = stime1 + "00";
		}
	}else{
		if(stime1.indexOf(":") == -1 && stime1.length==3){
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
		}
		if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
		}
		if(stime1.indexOf(":") == 1){
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value = "0" + stime1;
		}
	}
	if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
		if(stime2.length==1 || stime2.length==2){
			if(stime2.length==2){
				document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value = stime2 + ":00";
			}
			if(stime2.length==1){
				document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value = "0" + stime2 + ":00";
			}
		}else{
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value = stime2 + "00";
		}
	}else{
		if(stime2.indexOf(":") == -1 && stime2.length==3){
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
		}
		if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
		}
		if(stime2.indexOf(":") == 1){
			document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value = "0" + stime2;
		} 
		} 
	}
	
	
function sendMailFromVoxme(target){
	   var estimator = document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'].value;
	   var taskPlaner = document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'].value;
	   var taskViaa = document.forms['taskPlanForm'].elements['taskPlanning.taskVia'].value;
        var survey = document.forms['taskPlanForm'].elements['taskPlanning.taskDate'].value;
        //var surveyDutch = document.forms['taskPlanForm'].elements['customerFile.survey'].value;
        var sendsurveyTime = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value;
        var surveyTime = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeFrom'].value;
       // var surveyDutchTime = document.forms['taskPlanForm'].elements['customerFile.surveyTime'].value;
        var sendsurveyTime2 = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value;
        var surveyTime2 = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value;
        var email = document.forms['taskPlanForm'].elements['customerFile.email'].value;
        var surveyType = document.forms['taskPlanForm'].elements['taskPlanning.taskType'].value;
        var voxmeInvTag = 'No';
        var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
        var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
        var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
        var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
        if(taskViaa=='Y') { 
         	  if(taskPlaner=='' && estimator==''){
               alert("Please select the Assignee to schedule the survey");
         	  }
            } 
              if(taskViaa=='N') { 
              if(estimator=='') { 
                  alert("Please select the Assignee to schedule the survey");
              }
              }
       if (hour<12){
        		surveyTime=hour+":"+min+"AM";
        } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
            surveyTime=(hour-12)+":"+min+"PM";
        }else{
            surveyTime=hour+":"+min+"PM";
        }
         if (hour2<12){
        		surveyTime2=hour2+":"+min2+"AM";
        } else if (hour2>12) {
            surveyTime2=(hour2-12)+":"+min2+"PM";
        }else{
            surveyTime2=hour2+":"+min2+"PM";
        }
        if(surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
        alert("Please select From and To time to schedule the survey");
        }
        else if(survey=='') { 
            alert("Please fill the Task Date to schedule the survey");
            }
        else if(surveyType=='') { 
            alert("Please fill the Task Type to schedule the survey");
            }
        else if((taskPlaner=='' && estimator=='') || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
            alert("Survey is not yet Scheduled - Cannot send Survey Email");
            }else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && (estimator !='' || taskPlaner!='') && surveyTime2 !='' && surveyTime2 !=''){
	        var systemDate = new Date();
			var mySplitResult = survey.split("-");
		   	var day = mySplitResult[0];
		   	var month = mySplitResult[1];
		   	var year = mySplitResult[2];
		 	if(month == 'Jan'){
		       month = "01";
		   	}else if(month == 'Feb'){
		       month = "02";
		   	}else if(month == 'Mar'){
		       month = "03"
		   	}else if(month == 'Apr'){
		       month = "04"
		   	}else if(month == 'May'){
		       month = "05"
		   	}else if(month == 'Jun'){
		       month = "06"
		   	}else if(month == 'Jul'){
		       month = "07"
		   	}else if(month == 'Aug'){
		       month = "08"
		   	}else if(month == 'Sep'){
		       month = "09"
		   	}else if(month == 'Oct'){
		       month = "10"
		   	}else if(month == 'Nov'){
		       month = "11"
		   	}else if(month == 'Dec'){
		       month = "12";
		   	}
		 	var finalDate = month+"/"+day+"/"+year;
		   	survey = finalDate.split("/");
		   	var enterDate = new Date(month+"/"+day+"/20"+year);
		  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
		   	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
		   	if(daysApart < 0){
		    	alert("The Task Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
		    	return false;
		  	}
		  	if(survey != ''){
	  		if(daysApart == 0){
	  			var time = document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].value;
				var hour = time.substring(0, time.indexOf(":"))
				var min = time.substring(time.indexOf(":")+1, time.length);
	  			if (hour < 0  || hour > 23) {
					document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].focus();
					alert('Please enter correct time.');
					return false;
				}
				if (min<0 || min > 59) {
					document.forms['taskPlanForm'].elements['taskPlanning.taskTimeTo'].focus();
					alert('Please enter correct time.');
					return false;
				}
				if(systemDate.getHours() > hour){
					alert("The Task Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
					alert("The Task Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				   }  }   }
		  	if(target=='MoveToCloud' && surveyType==''){
		  		alert("Please select Survey Type.");
				return false;	
		  	}
		  	var agree= confirm("Press OK to Send Survey, else press Cancel.");	  	   
         if(agree){
         		var url='sendMoveToCloud.html?ajax=1&cid=${customerFile.id}&id=${taskPlanning.id}&taskType='+surveyType+'&estimator='+estimator+'&surveyDates='+finalDate+'&surveyTime='+sendsurveyTime+'&surveyTime2='+sendsurveyTime2+'&voxmeInvTag='+voxmeInvTag+'&decorator=simple&popup=true';
         		httpTaskMgmt.open("GET", url, true);
         		httpTaskMgmt.onreadystatechange=httpTaskMgmtMailSender;
         		httpTaskMgmt.send(null);
         	}
    	else {             
	  	    }	  	   
         } else {
         alert("Task is not yet Scheduled.");
         }       
}
var planervalue;
function getPlannerValues(planervalue){
	var url="getTaskPlannerList.html?ajax=1&decorator=simple&popup=true&extCompanyId="+encodeURI(planervalue);
    httpServiceTaskComp.open("GET", url, true);
    httpServiceTaskComp.onreadystatechange = handleHttpResponseValues;
    httpServiceTaskComp.send(null);	
}
var assignee;
function getAssigneeValues(assignee)
{
var taskCompanyuiod = document.forms['taskPlanForm'].elements['taskPlanning.taskCompany'].value;
var uuidCompany = document.forms['taskPlanForm'].elements['uuidCompany'].value;
var taskType=document.forms['taskPlanForm'].elements['taskPlanning.taskType'].value;
var url="getAssigneeList.html?ajax=1&decorator=simple&popup=true&assignee="+encodeURI(assignee)+"&uuidCompany="+encodeURI(uuidCompany)+"&taskCompanyuuid="+encodeURI(taskCompanyuiod)+"&taskType="+encodeURI(taskType);
httpAssignee.open("GET", url, true);
httpAssignee.onreadystatechange = handleHttpResponseAssigneeValues;
httpAssignee.send(null);	
}
function handleHttpResponseAssigneeValues(){
	if (httpAssignee.readyState == 4){
		 var results = httpAssignee.responseText
	      results = results.trim();
		 results = results.trim();
		 //alert(results)
		 var res = results.split(","); 
		 targetElement = document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'];
		 targetElement.length = res.length;
		
		 var res2 ;
		 for(i=0;i<res.length;i++){
			  if(res[i] == ''){
				 
				document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'].options[i].text = '';
				document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'].options[i].value = '';
			  }else{
				 res2  = res[i].split("~");
			     document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'].options[i].text = res2[1];
				 document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'].options[i].value = res2[0];
			  }			
           } 
		
			document.forms['taskPlanForm'].elements['taskPlanning.taskAssignee'].value='${taskPlanning.taskAssignee}';       
	           }}
	           
function handleHttpResponseValues(){
	if (httpServiceTaskComp.readyState == 4){
		 var results = httpServiceTaskComp.responseText
	      results = results.trim();
		 results = results.trim();
		 //alert(results)
		 var res = results.split(","); 
		 targetElement = document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'];
		 targetElement.length = res.length;
		
		 var res2 ;
		 for(i=0;i<res.length;i++){
			  if(res[i] == ''){
				 
				document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'].options[i].text = '';
				document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'].options[i].value = '';
			  }else{
				 res2  = res[i].split("~");
			     document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'].options[i].text = res2[1];
				 document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'].options[i].value = res2[0];
			  }			
           } 
		
			document.forms['taskPlanForm'].elements['taskPlanning.taskPlanner'].value='${taskPlanning.taskPlanner}';       
	           }
			 }
var httpTaskMgmt = getHTTPObject();
var httpAssignee = getHTTPObject12();

var httpServiceTaskComp = getHTTPObject22();
function getHTTPObject22()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsofrstruts.cmlt.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function getHTTPObject12()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsofrstruts.cmlt.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
if (!xmlhttp)
{
    xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
}
}
return xmlhttp;
}	

function httpTaskMgmtMailSender(){
	if (httpTaskMgmt.readyState == 4){
		      var results = httpTaskMgmt.responseText
	           results = results.trim();
	           if(results=='sent'){
	        	}else{
	        	}
	           location.reload(); 
	           }
}

function disabledBtn() {
	try{
	document.forms['taskPlanForm'].SendtoMoveDashboard.disabled=true;
   }catch(e){}
}

</script>

<script>
$(document).ready(function(){
        var taskVia = document.forms['taskPlanForm'].elements['taskPlanning.taskVia'].value;

		if(taskVia == 'N')
			{
  
        $(".taskCompany").hide();
        $(".planner").hide();
        var uuidCompany = document.forms['taskPlanForm'].elements['uuidCompany'].value;
        getAssigneeValues(uuidCompany);
           }
		else
		{
		
		$(".taskCompany").show();
		$(".planner").show();

		
		}
  
});
var temp;
function showhide(temp)
{
	if(temp == 'N')
	{

$(".taskCompany").hide();
$(".planner").hide();
var uuidCompany = document.forms['taskPlanForm'].elements['uuidCompany'].value;
getAssigneeValues(uuidCompany);
}

else
	{
	
	$(".taskCompany").show();
	$(".planner").show();

	
	}}
	if(!document.forms['taskPlanForm'].elements['taskPlanning.taskCompany'].value=='')
		{
		var serviceTaskCompany = document.forms['taskPlanForm'].elements['taskPlanning.taskCompany'].value;
getPlannerValues(serviceTaskCompany);
getAssigneeValues(serviceTaskCompany);
		}
</script>
