package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.RoleAccess;

public interface RoleAccessDao extends GenericDao<RoleAccess, Long>{
	public List<RoleAccess> findByCorpID(String corpId);

}
