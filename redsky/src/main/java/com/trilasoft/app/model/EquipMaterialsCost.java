package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;

@Entity
@Table(name="equipmaterialscost")
public class EquipMaterialsCost implements Comparable<EquipMaterialsCost> {
	private Long id;
	private Long equipMaterialsId;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private Integer availableQty;
	private Double unitCost;
	private Double salestCost;
	private Double ownerOpCost;
	public int compareTo(final EquipMaterialsCost other) {
		return new CompareToBuilder().append(id, other.id)
				.append(equipMaterialsId, other.equipMaterialsId)
				.append(corpID, other.corpID)
				.append(createdBy, other.createdBy)
				.append(createdOn, other.createdOn)
				.append(updatedBy, other.updatedBy)
				.append(updatedOn, other.updatedOn)
				.append(availableQty, other.availableQty)
				.append(unitCost, other.unitCost)
				.append(salestCost, other.salestCost)
				.append(ownerOpCost, other.ownerOpCost).toComparison();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("equipMaterialsId", equipMaterialsId)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("availableQty", availableQty)
				.append("unitCost", unitCost).append("salestCost", salestCost)
				.append("ownerOpCost", ownerOpCost).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof EquipMaterialsCost))
			return false;
		EquipMaterialsCost castOther = (EquipMaterialsCost) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(equipMaterialsId, castOther.equipMaterialsId)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(availableQty, castOther.availableQty)
				.append(unitCost, castOther.unitCost)
				.append(salestCost, castOther.salestCost)
				.append(ownerOpCost, castOther.ownerOpCost).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(equipMaterialsId)
				.append(corpID).append(createdBy).append(createdOn)
				.append(updatedBy).append(updatedOn).append(availableQty)
				.append(unitCost).append(salestCost).append(ownerOpCost)
				.toHashCode();
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getEquipMaterialsId() {
		return equipMaterialsId;
	}
	public void setEquipMaterialsId(Long equipMaterialsId) {
		this.equipMaterialsId = equipMaterialsId;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Integer getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}
	@Column
	public Double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}
	@Column
	public Double getSalestCost() {
		return salestCost;
	}
	public void setSalestCost(Double salestCost) {
		this.salestCost = salestCost;
	}
	@Column
	public Double getOwnerOpCost() {
		return ownerOpCost;
	}
	public void setOwnerOpCost(Double ownerOpCost) {
		this.ownerOpCost = ownerOpCost;
	}
	
	
}
