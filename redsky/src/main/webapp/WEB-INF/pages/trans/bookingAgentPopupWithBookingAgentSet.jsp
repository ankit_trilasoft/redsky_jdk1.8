<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
</head> 
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-32.5px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style>
<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css"> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"bookingAgentPopupWithBookingAgentSet.html":"bookingAgentPopupWithBookingAgentSet.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="customerVendor" value="<%= request.getParameter("customerVendor")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>

<!-- Enhansment after 27-02-2008 -->
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="firstName" value="<%= request.getParameter("firstName")%>" />
<s:hidden name="firstName" value="<%= request.getParameter("firstName")%>" />
<c:set var="lastName" value="<%= request.getParameter("lastName")%>" />
<s:hidden name="lastName" value="<%= request.getParameter("lastName")%>" />
<!-- Enhansment after 27-02-2008 -->

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<div id="layer1" style="width:100%">
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">
<table class="table" style="width:99%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th>External Ref.</th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<c:if test="${partnerType == 'AG'}">  
<th>Vanline Code</th>
</c:if>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>
<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="5" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partner.extReference" size="14" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="15" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partner.aliasName" size="14" cssClass="input-text" />
			</td>
			<c:if test="${partnerType == 'AG'}">
			<td>
			    <s:textfield name="vanlineCodeSearch" size="14" cssClass="input-text" />
			</td>			
			</c:if>
			<td>
			    <s:textfield name="partner.billingCountryCode" size="7" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partner.billingCountry" size="16" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partner.billingState" size="14" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td width="130px" colspan="3" style="border-left: hidden;text-align:right;padding-right:5px;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<div id="newmnav">   
<ul>
  	   <li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAgent();"><span>Agents<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		 
</ul>
</div><div class="spnblk" style="width:800px">&nbsp;</div><br>
 
<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode"
		href="editPartnerAddForm.html" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if> 
    <display:column property="extReference" title="External Ref." sortable="true" />
    <display:column titleKey="partner.name"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column property="aliasName" sortable="true"  title="Alias Name"></display:column>
    <c:if test ="${(partnerType == 'AC')}">
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    </c:if>
    <c:if test="${(partnerType == 'AG' || partnerType == 'VN')}">  
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
    	<display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
    </c:if>
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:35px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:35px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:110px"/> 
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table> 
</div>

<c:set var="isTrue" value="false" scope="session"/> 
</s:form> 
<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="scripts/ajax.js"></script>
<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['partnerListForm'].elements['partner.lastName'].value = "";
	document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
	document.forms['partnerListForm'].elements['partner.extReference'].value = "";
	document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
	document.forms['partnerListForm'].elements['partner.billingState'].value = "";
	document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
	document.forms['partnerListForm'].elements['partner.aliasName'].value = "";
	 <c:if test="${partnerType == 'AG'}">  
	document.forms['partnerListForm'].elements['vanlineCodeSearch'].value = "";
	</c:if>
}

function setAccount(){
	document.forms['partnerListForm'].elements['partnerType'].value = "AC";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}
function setPrivate(){
	document.forms['partnerListForm'].elements['partnerType'].value = "PP";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}
function setAgent(){
	document.forms['partnerListForm'].elements['partnerType'].value = "AG";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}
function setVendor(){
	document.forms['partnerListForm'].elements['partnerType'].value = "VN";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}

function openOriginLocation(address1,address2,city,zip,state,country) {
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
}
</script>	
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }

</script>

<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">   
   //highlightTableRows("partnerList"); 
   //Form.focusFirstElement($("partnerListForm"));      
</script>