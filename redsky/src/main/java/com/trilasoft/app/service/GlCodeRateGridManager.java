package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.GlCodeRateGrid;

public interface GlCodeRateGridManager extends GenericManager<GlCodeRateGrid, Long>{
	public List getAllRateGridList(String sessionCorpID,String costId,String jobType,String routing1,String recGl,String payGl);
}
