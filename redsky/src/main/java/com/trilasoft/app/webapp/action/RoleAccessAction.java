package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.hibernate.Hibernate;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.model.RoleAccess;
import com.trilasoft.app.service.RoleAccessManager;

public class RoleAccessAction extends BaseAction {
	 	private List refMasters;
	 	private List roleAva;
	    private RefMasterManager refMasterManager;
	    private static Map<String, String>role_action;
	    private static Map<String, String>menu_s;
	    private static Map<String, String>role;
	    private String sessionCorpID;
	   

public RoleAccessAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
    
    private RoleAccessManager roleAccessManager;
    private RoleAccess roleAccess;

	public void setRoleAccessManager(RoleAccessManager roleAccessManager) {
		this.roleAccessManager = roleAccessManager;
	}

	public RoleAccess getRoleAccess() {
		return roleAccess;
	}

	public void setRoleAccess(RoleAccess roleAccess) {
		this.roleAccess = roleAccess;
	}
    
    public List roleAccesss;
	private Long id;
	private Long maxId;
	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}

	public List getRoleAccesss() {
		return roleAccesss;
	}

	public void setRoleAccesss(List roleAccesss) {
		this.roleAccesss = roleAccesss;
	}
	public Long getId() {
		return id;
  	}
	
 public void setId(Long id) {
		this.id = id;
  }
	public String RoleAccessList() { 
		roleAccesss=roleAccessManager.getAll();
			//System.out.println("\n\n roleAccess List are :\t"+roleAccesss);
	        return SUCCESS;   
	    }
	public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    
    public List getRefMasters(){
    	return refMasters;
    }

  public String findRole(String sessionCorpID) {
	//roleAva = refMasterManager.findRole(sessionCorpID);  
	return SUCCESS;
  }

    public String getComboList(String corpId){
    	role_action = refMasterManager.findByParameter(corpId, "ROLE_ACTION");
    	menu_s = refMasterManager.findByParameter(corpId, "MENU_S");
    	 return SUCCESS;
    }
    public  Map<String, String> getRole_action() {
 		return role_action;
 	}
    public  Map<String, String> getMenu_s() {
 		return menu_s;
 	}
public String edit() { 
	  getComboList(sessionCorpID);
    	if (id != null)
	     {
    		roleAccess = roleAccessManager.get(id);
    		roleAccess.setCorpID(sessionCorpID);
    	//	List roleAva = refMasterManager.findRole(sessionCorpID);
    		}
		else
		 {
			roleAccess= new RoleAccess();
		//	List roleAva = refMasterManager.findRole(sessionCorpID);
			roleAccess.setCreatedOn(new Date());
			roleAccess.setUpdatedOn(new Date());
			roleAccess.setCorpID(sessionCorpID);
			//System.out.println(roleAva);
		 }
    	
        //System.out.println(oldCreatedOn);
   	   	return SUCCESS;
		}

  public String save() throws Exception {   

     boolean isNew = (roleAccess.getId() == null); 
     if(isNew)
     {
    	 roleAccess.setCreatedOn(new Date());
     }
	
     roleAccess.setUpdatedOn(new Date());
     roleAccess=roleAccessManager.save(roleAccess);   
     roleAccess.setCorpID(sessionCorpID);
     String key = (isNew) ? "roleAccess.added" : "roleAccess.updated";   
     saveMessage(getText(key));   
     if (!isNew) {   
    	 return INPUT;
     } else {
     	return SUCCESS;
     }
 } 
}