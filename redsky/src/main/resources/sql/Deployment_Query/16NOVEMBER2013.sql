// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`notes` ADD COLUMN `memoUploadStatus` VARCHAR(20) AFTER `uvlSentStatus`, ADD COLUMN `memoLastUploadedDate` DATETIME AFTER `memoUploadStatus`;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `CAR_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `contactLastName`,  ADD COLUMN `COL_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `CAR_vendorCode`,  ADD COLUMN `TRG_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `COL_vendorCode`,
  ADD COLUMN `HOM_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`TRG_vendorCode`,
  ADD COLUMN `RNT_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`HOM_vendorCode`,
  ADD COLUMN `SET_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`RNT_vendorCode`,
  ADD COLUMN `LAN_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`SET_vendorCode`,
  ADD COLUMN `MMG_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`LAN_vendorCode`,
  ADD COLUMN `ONG_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`MMG_vendorCode`,
  ADD COLUMN `PRV_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`ONG_vendorCode`,
  ADD COLUMN `AIO_vendorCode` VARCHAR(25) DEFAULT NULL AFTER
`PRV_vendorCode`,
  ADD COLUMN `EXP_vendorCode` VARCHAR(25) DEFAULT NULL AFTER
`AIO_vendorCode`,
  ADD COLUMN `RPT_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`EXP_vendorCode`,
  ADD COLUMN `SCH_schoolSelected` VARCHAR(25) DEFAULT NULL AFTER 
`RPT_vendorCode`,
  ADD COLUMN `TAX_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`SCH_schoolSelected`,
  ADD COLUMN `TAC_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`TAX_vendorCode`,
  ADD COLUMN `TEN_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`TAC_vendorCode`,
  ADD COLUMN `VIS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER 
`TEN_vendorCode`;

ALTER TABLE `redsky`.`checklist` MODIFY COLUMN `partnerCode` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',  ADD COLUMN `billToExcludes` VARCHAR(255) DEFAULT '' AFTER `serviceOrderDate`;

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,bucket)values
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','CAR_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','COL_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','TRG_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','HOM_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','RNT_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','SET_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','LAN_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','MMG_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','ONG_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','PRV_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','AIO_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','EXP_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','RPT_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','SCH_schoolSelected'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','TAX_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','TAC_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','TEN_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 
'DATA_SECURITY_TABLE','en',now(),'subrat',now(),'subrat','VIS_vendorCode');

ALTER TABLE `redsky`.`integrationloginfo` MODIFY COLUMN `corpId` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `docCutOffTimeTo` VARCHAR(5) AFTER `reason`;

ALTER TABLE `redsky`.`integrationlog` ADD COLUMN `messageType` VARCHAR(45) DEFAULT NULL AFTER `filelastModified`;

ALTER TABLE `redsky`.`integrationlog` ADD COLUMN `messageEvent` VARCHAR(45) DEFAULT NULL AFTER `messageType`;

ALTER TABLE `redsky`.`integrationlog` ADD COLUMN `messageStatus` VARCHAR(5) DEFAULT NULL AFTER `messageEvent`;

ALTER TABLE `redsky`.`printdailypackage` ADD COLUMN `job` VARCHAR(45) NULL  AFTER `status` , ADD COLUMN  `billToCode` TEXT NULL DEFAULT NULL AFTER `job` ;
 
 ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `bookingAgentPhoneNumber` VARCHAR(25) DEFAULT NULL AFTER `docCutOffTimeTo`,
 ADD COLUMN `networkPhoneNumber` VARCHAR(25) DEFAULT NULL AFTER `bookingAgentPhoneNumber`,
 ADD COLUMN `originAgentPhoneNumber` VARCHAR(25) DEFAULT NULL AFTER `networkPhoneNumber`,
 ADD COLUMN `destinationPhoneNumber` VARCHAR(25) DEFAULT NULL AFTER `originAgentPhoneNumber`,
 ADD COLUMN `subOriginAgentPhoneNumber` VARCHAR(25) DEFAULT NULL AFTER `destinationPhoneNumber`,
 ADD COLUMN `subDestinationAgentPhoneNumber` VARCHAR(25) DEFAULT NULL AFTER `subOriginAgentPhoneNumber`;

ALTER TABLE `redsky`.`trackingstatus` CHANGE COLUMN `destinationPhoneNumber` `destinationAgentPhoneNumber` VARCHAR(25) DEFAULT NULL;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `defaultSortforFileCabinet` VARCHAR(45) DEFAULT NULL;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `sortOrder` VARCHAR(45) DEFAULT NULL AFTER `defaultSortforFileCabinet`;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','DEFAULTSORTFORFILECABINET',20,true,now(),'admintrila',now(),'admintrila',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', '2', 'Document Type', 40, 
'DEFAULTSORTFORFILECABINET','en',now(),'admintrila',now(),'admintrila'),
('TSFT', '3', 'Document Category', 40, 
'DEFAULTSORTFORFILECABINET','en',now(),'admintrila',now(),'admintrila'),
('TSFT', '4', 'Description', 40, 
'DEFAULTSORTFORFILECABINET','en',now(),'admintrila',now(),'admintrila'),
('TSFT', '6', 'Uploaded On', 40, 
'DEFAULTSORTFORFILECABINET','en',now(),'admintrila',now(),'admintrila'),
('TSFT', '7', 'Uploaded By', 40, 
'DEFAULTSORTFORFILECABINET','en',now(),'admintrila',now(),'admintrila');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('TrackingStatus','bookingAgentPhoneNumber' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','networkPhoneNumber' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','originAgentPhoneNumber' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','destinationAgentPhoneNumber' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','subOriginAgentPhoneNumber' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','subDestinationAgentPhoneNumber' ,'Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`app_user` MODIFY COLUMN `loginFailureReason` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci   DEFAULT NULL;

ALTER TABLE `redsky`.`printdailypackage` CHANGE COLUMN `job` `job` TEXT NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`refmaster` ADD COLUMN `flex5` VARCHAR(100),
ADD COLUMN `flex6` VARCHAR(100) AFTER `flex5`,
ADD COLUMN `label5` VARCHAR(50) DEFAULT '' AFTER `flex6`,
ADD COLUMN `label6` VARCHAR(50) DEFAULT '' AFTER `label5`;

DROP TABLE IF EXISTS `redsky`.`pertainingloss`;
CREATE TABLE  `redsky`.`pertainingloss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpID` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdBy` varchar(82) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `sequenceNumber` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipNumber` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `claimNumber` bigint(20) DEFAULT NULL,
  `lossNumber` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agentCode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chargeBackAmount` decimal(19,2) DEFAULT NULL,
  `damageByAction` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lossAction` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itmClmsItemSeqNbr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itmDnlRsnCode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itrPmntId` bigint(20) DEFAULT NULL,
  `itmAltItemDesc` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itrRespExcpCode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itrAssessdLiabilityAmt` decimal(10,2) DEFAULT NULL,
  `itrTotalLiabilityAmt` decimal(10,2) DEFAULT NULL,
  `partnerCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partnerName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44986 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
 
ALTER TABLE `redsky`.`systemdefault` MODIFY COLUMN `ownbillingBilltoCodes` VARCHAR(500) DEFAULT '';

insert into parametercontrol (corpid,parameter,comments,fieldlength,active,createdon,createdby,updatedon,updatedby,tsftflag,hybridflag,customtype)
values ('SSCW','INTEGRATIONTYPE','Integration Type',5,true,now(),'SSCW_SETUP',now(),'SSCW_SETUP',1,1,'')

insert into refmaster (code,description,parameter,corpid,createdby,createdon,updatedby,updatedon,language,fieldlength)
values('cl','Claims','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('dp','Dispatch Download','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('ds','Distribution ','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('me','Memo Download','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('rg','Registration','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('rt','Rating','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('st','Weekly Statement','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('20','Scanned Docs','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('tu','Trans Doc Upload','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('td','Trans Doc Download','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('mu','Memo Upload ','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('qt','Quotes-2-Go','INTEGRATIONTYPE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5);

ALTER TABLE `redsky`.`printdailypackage` MODIFY COLUMN `wtServiceType` VARCHAR(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;

insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'AATL', branch, 'AATL_SETUP', now(), 'AATL_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'ADAM', branch, 'ADAM_SETUP', now(), 'ADAM_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'BONG', branch, 'BONG_SETUP', now(), 'BONG_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'BOUR', branch, 'BOUR_SETUP', now(), 'BOUR_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'BRSH', branch, 'BRSH_SETUP', now(), 'BRSH_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'CWMS', branch, 'CWMS_SETUP', now(), 'CWMS_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'DECA', branch, 'DECA_SETUP', now(), 'DECA_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'GERS', branch, 'GERS_SETUP', now(), 'GERS_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'HIGH', branch, 'HIGH_SETUP', now(), 'HIGH_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'HSRG', branch, 'HSRG_SETUP', now(), 'HSRG_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'ICMG', branch, 'ICMG_SETUP', now(), 'ICMG_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'INTM', branch, 'INTM_SETUP', now(), 'INTM_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'JOHN', branch, 'JOHN_SETUP', now(), 'JOHN_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'STAR', branch, 'STAR_SETUP', now(), 'STAR_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGCA', branch, 'UGCA_SETUP', now(), 'UGCA_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGCN', branch, 'UGCN_SETUP', now(), 'UGCN_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGHK', branch, 'UGHK_SETUP', now(), 'UGHK_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGJP', branch, 'UGJP_SETUP', now(), 'UGJP_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGMY', branch, 'UGMY_SETUP', now(), 'UGMY_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGSG', branch, 'UGSG_SETUP', now(), 'UGSG_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';



insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'UGWW', branch, 'UGWW_SETUP', now(), 'UGWW_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';


insert into redsky.refmaster(state, number, description, parameter, corpID, branch, createdBy, createdOn, updatedBy, updatedOn, bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6)
select
state, number, description, parameter, 'VOER', branch, 'VOER_SETUP', now(), 'VOER_SETUP', now(), bucket, address1, address2, bucket2, city, zip, billCrew, code, fieldLength, lastReset, stopDate, flex1, flex2, flex3, flex4, label1, label2, label3, label4, language, flex5, flex6, label5, label6
 FROM refmaster where parameter='PENTITY_SERVICEORDER' and corpid='UTSI';


