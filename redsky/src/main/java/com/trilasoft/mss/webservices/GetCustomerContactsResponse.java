
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerContactsResult" type="{http://www.mss1.com/webservices/}ArrayOfMSSCustomerContact" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerContactsResult"
})
@XmlRootElement(name = "GetCustomerContactsResponse")
public class GetCustomerContactsResponse {

    @XmlElement(name = "GetCustomerContactsResult")
    protected ArrayOfMSSCustomerContact getCustomerContactsResult;

    /**
     * Gets the value of the getCustomerContactsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSCustomerContact }
     *     
     */
    public ArrayOfMSSCustomerContact getGetCustomerContactsResult() {
        return getCustomerContactsResult;
    }

    /**
     * Sets the value of the getCustomerContactsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSCustomerContact }
     *     
     */
    public void setGetCustomerContactsResult(ArrayOfMSSCustomerContact value) {
        this.getCustomerContactsResult = value;
    }

}
