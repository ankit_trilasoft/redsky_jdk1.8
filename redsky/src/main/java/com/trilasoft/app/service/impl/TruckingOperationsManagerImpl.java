package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.TruckingOperationsDao;
import com.trilasoft.app.model.TruckingOperations;
import com.trilasoft.app.service.TruckingOperationsManager;

public class TruckingOperationsManagerImpl extends GenericManagerImpl<TruckingOperations, Long> implements TruckingOperationsManager {
	TruckingOperationsDao truckingOperationsDao; 
    
	 
    public TruckingOperationsManagerImpl(TruckingOperationsDao truckingOperationsDao) { 
        super(truckingOperationsDao); 
        this.truckingOperationsDao = truckingOperationsDao; 
    }

    public List workTicketTruck(Long ticket,String corpID){
    	return truckingOperationsDao.workTicketTruck(ticket, corpID);
    }
    public List findWorkTicketTruck(String localTruckNumber,String corpID){
    	return truckingOperationsDao.findWorkTicketTruck(localTruckNumber,corpID);
    }
    public int updateDeleteStatus(Long id){
    	return truckingOperationsDao.updateDeleteStatus(id);
    }

	public void updateWorkTicketTrucks(Long ticket, int trucks,String TargetActual,boolean workticketQueue,String resourceValue,String sessionCorpID) {
		truckingOperationsDao.updateWorkTicketTrucks(ticket,trucks,TargetActual,workticketQueue,resourceValue,sessionCorpID);
		
	}
	public String findSurveyTool(String corpID, String job, String companyDivision){
		return truckingOperationsDao.findSurveyTool(corpID, job, companyDivision);
	}
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber){
		return truckingOperationsDao.findSendMoveCloudStatusCrew(sessionCorpID, fileNumber);
	}
	
}