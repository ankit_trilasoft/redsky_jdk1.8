package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ScrachCard;

public interface ScrachCardDao extends GenericDao<ScrachCard, Long> {
	
	public List getScrachCardByUser(String userName);
	public List getSchedulerScrachCardByUser(String userName ,Date workDate);
	public List getSchedulerScrachCardByDate(Date workDate);
}
