<%@ include file="/common/taglibs.jsp"%>
<title><fmt:message key="billingWorkTicketList.title" /></title>
<meta name="heading" content="<fmt:message key='billingWorkTicketList.heading'/>" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>

	</head>

<s:form id="billingWorkTicketList" name="billingWorkTicketList" action="updateWorkTicket.html?&btntype=yes&decorator=popup&popup=true" method="post">
<s:hidden name="userUncheck"/>
<s:hidden name="userCheck"/>
<s:hidden name="shipNumber" value="<%=request.getParameter("shipNumber")%>"/> 
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="btntype" value="<%=request.getParameter("btntype")%>"/> 

<div style=" width:100%; overflow-x:auto;">


	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Work Ticket List</span></a></li>
		  </ul>
		   <div class="key_workticket">&nbsp;</div>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="workTickets" value="workTickets" scope="request" /> 
<table><tr><td>
<display:table name="workTickets" class="table" requestURI="" id="workTicketList" export="false" defaultsort="4" pagesize="1000"  style="width:1000px;margin-top:-20px;!margin-top:0px;">
		<display:column style="width:5px" title="St"><c:if test="${workTicketList.targetActual == 'T'}"><img id="target" src="${pageContext.request.contextPath}/images/target.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'A'}"><img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'C'}"><img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if></display:column>
		<display:column  title="Cleared" style="width:5px">
		<c:if test="${workTicketList.reviewStatus=='Billed'}">
       	<input type="checkbox" style="margin-left:5px;" checked="checked" disabled="disabled" name="${workTicketList.id}" id="${workTicketList.id}" onclick="userStatusUncheck(${workTicketList.id},this)" />
	   	</c:if>
       	<c:if test="${workTicketList.reviewStatus=='UnBilled' || workTicketList.reviewStatus== null || workTicketList.reviewStatus== ''}">
       	<input type="checkbox"  style="margin-left:5px;" name="${workTicketList.id}" id="${workTicketList.id}" onclick="userStatusCheck(${workTicketList.id},this)" />
       	</c:if>
		
		</display:column> 
		<display:column property="reviewStatusDate" sortable="true" style="width:5px" title="Clr&nbsp;Date" format="{0,date,dd-MMM-yyyy}"/>
		<display:column sortable="true" titleKey="workTicket.ticket"><a href="editWorkTicketUpdate.html?id=${workTicketList.id}" target="_blank" ><c:out value="${workTicketList.ticket}"></c:out></a></display:column>   
		<display:column property="shipNumber" sortable="true" titleKey="workTicket.shipNumber"  />
		<display:column property="registrationNumber" sortable="true" style="width:110px" titleKey="workTicket.registrationNumber" />
		<display:column property="lastName" sortable="true" titleKey="workTicket.lastName" maxLength="12"/>
		<display:column property="firstName" sortable="true" titleKey="workTicket.firstName" />
		<display:column property="jobType" sortable="true" titleKey="workTicket.jobType" />
		<display:column property="jobMode" sortable="true" titleKey="workTicket.jobMode" />
		<%-- <display:column property="city" sortable="true" titleKey="workTicket.city" />
		<display:column property="destinationCity" sortable="true" titleKey="workTicket.destinationCity" />--%>
		<display:column property="instructions" sortable="true" titleKey="workTicket.instructions"  maxLength="11" style="width:110px"/>
		<display:column property="service" sortable="true" titleKey="workTicket.service" />
		<display:column property="date1" sortable="true" titleKey="workTicket.date1" style="width:70px" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="date2" sortable="true" titleKey="workTicket.date2" style="width:70px" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="warehouse" sortable="true" style="width:5px" titleKey="workTicket.warehouse" />
		
		<display:setProperty name="paging.banner.item_name" value="workticket" />
		<display:setProperty name="paging.banner.items_name" value="workTickets" />
		
		<display:setProperty name="export.excel.filename" value="WorkTicket List.xls" />
		<display:setProperty name="export.csv.filename" value="WorkTicket List.csv" />
		<display:setProperty name="export.pdf.filename" value="WorkTicket List.pdf" />
	</display:table>
	</td></tr>
	<tr><td>
	<s:submit cssClass="cssbutton1" type="button" value="Billed Ticket" name="BilledTicket" cssStyle="width:90px; height:25px" onclick="return check_all();" /> 
	<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:65px; height:25px" onclick="window.close();">
	</td></tr></table>
	 </div>
</s:form>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<SCRIPT LANGUAGE="JavaScript">
function userStatusCheck(rowId, targetElement) // 1 visible, 0 hidden
{
  	if(targetElement.checked)
    {
	      var userCheckStatus = document.forms['billingWorkTicketList'].elements['userCheck'].value;
	      if(userCheckStatus == '')
	      {
		  	  document.forms['billingWorkTicketList'].elements['userCheck'].value = rowId;
	      }
	      else
	      {
	     	  var userCheckStatus=	document.forms['billingWorkTicketList'].elements['userCheck'].value = userCheckStatus + ',' + rowId;
      		  document.forms['billingWorkTicketList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
	      }
    }
   if(targetElement.checked==false)
   {
     var userCheckStatus = document.forms['billingWorkTicketList'].elements['userCheck'].value;
     var userCheckStatus=document.forms['billingWorkTicketList'].elements['userCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['billingWorkTicketList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   }
     buttonCheck();
}

function buttonCheck(){
   var userCheck= document.forms['billingWorkTicketList'].elements['userCheck'].value
   if(userCheck=="" || userCheck==","){
     document.forms['billingWorkTicketList'].elements['BilledTicket'].disabled=true;
   }else if(userCheck!=""){
     document.forms['billingWorkTicketList'].elements['BilledTicket'].disabled=false; 
   }
}
    
function  userStatusUncheck(rowId, targetElement){
  document.getElementById(rowId).checked="checked";
}
    
function check_all() {
  this.form = document.getElementById('billingWorkTicketList');
  if (this.form)
  {
     this.checkboxNodes = this.form.getElementsByTagName('input');
     /*for (this.i = 0; this.i < this.checkboxNodes.length; this.i++)
     {
         if (this.checkboxNodes[i].getAttribute('type') == 'checkbox')
         {
            if(this.checkboxNodes[i].checked == false)
            {
                //alert("Please check all tickets in the Bill Cleared Column ");
                //return false;
            } 
         }
     }*/
  }
}
function pick() {  
	window.close();
} 
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
try{
if(document.forms['billingWorkTicketList'].elements['btntype'].value=='yes'){
pick();
}
}
catch(e){}
try{
buttonCheck();
}
catch(e){}
</script>