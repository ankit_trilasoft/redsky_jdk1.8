package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.EquipMaterialsCost;
import com.trilasoft.app.model.ItemsJEquip;

public interface EquipMaterialsCostDao extends GenericDao<EquipMaterialsCost, Long>{
	
	public  EquipMaterialsCost getCostById(long id);
	public  EquipMaterialsCost getByMaterailId(long id);

}
