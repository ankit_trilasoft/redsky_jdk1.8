package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.TruckDrivers;

public interface TruckDriversDao extends GenericDao<TruckDrivers, Long>{
public List getDriverValidation(String driverCode,String truckNumber,String partnerCode, String corpId);
public List findDriverList(String truckNumber, String corpId);
public List getPrimaryFlagStatus(String truckNumber, String corpId);
}
