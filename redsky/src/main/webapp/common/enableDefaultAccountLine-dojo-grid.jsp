<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!-- load the dojo toolkit base -->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"
    djConfig="parseOnLoad:true, isDebug:false"></script>

<style type="text/css">
  @import "<c:url value='/scripts/dojo/dijit/themes/nihilo/nihilo.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/grid/_grid/Grid.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/grid/_grid/nihiloGrid.css'/>";
  @import "<c:url value='/styles/${appConfig["csstheme"]}/dojo-layout.css'/>";

  
  #otabs{margin-left: 0px;}
#otabs ul{margin: 0;padding: 0;list-style: none;_z-index:-1000; !z-index:-1000}
#otabs li {float: left;margin: 0 3px 0 0;white-space: nowrap;background:#1666c9 url(images/owtabbg.gif);font:bold 12px arial,verdana;line-height:1.25em;padding: 0px 0px 0px 0px;}
#otabs a {float: left;padding: 0 0 0 9px;background:url(images/owtabl.gif) no-repeat 0% 0%;text-decoration: none;color: #000;}
#otabs a:hover{background-position: 0 -28px;background: url(images/owtabl.gif) no-repeat 0% 0%;color: #000;}
#otabs a.current {background-position: 0 -28px;background: url(images/1otab.gif);color: #fff;font:normal 12px arial,verdana; }
#otabs a span {float: left;background:url(images/owtabr.gif) no-repeat 100% 0;padding: 5px 9px 4px 0; }
#otabs a:hover span{background-position: 100% -28px;background:url(images/owtabr.gif) no-repeat 100% 0;cursor: pointer;}
#otabs a.current span {background-position: 100% -28px;background: url(images/2otab.gif) no-repeat 100% 0;cursor: pointer;}
#otabs a.current:hover span {cursor: default;}
.spnblk{clear:both; line-height:0px;}   

  
  
#otabs{margin-left: 0px;}
#otabs ul{margin: 0;padding: 0;list-style: none;_z-index:-1000; !z-index:-1000}
#otabs li {float: left;margin: 0 3px 0 0;white-space: nowrap;background:#1666c9 url(images/owtabbg.gif);font:bold 12px arial,verdana;line-height:1.25em;padding: 0px 0px 0px 0px;}
#otabs a {float: left;padding: 0 0 0 9px;background:url(images/owtabl.gif) no-repeat 0% 0%;text-decoration: none;color: #000;}
#otabs a:hover{background-position: 0 -28px;background: url(images/owtabl.gif) no-repeat 0% 0%;color: #000;}
#otabs a.current {background-position: 0 -28px;background: url(images/1otab.gif);color: #fff;font:normal 12px arial,verdana; }
#otabs a span {float: left;background:url(images/owtabr.gif) no-repeat 100% 0;padding: 5px 9px 4px 0; }
#otabs a:hover span{background-position: 100% -28px;background:url(images/owtabr.gif) no-repeat 100% 0;cursor: pointer;}
#otabs a.current span {background-position: 100% -28px;background: url(images/2otab.gif) no-repeat 100% 0;cursor: pointer;}
#otabs a.current:hover span {cursor: default;}
.spnblk{clear:both; line-height:0px;}

.dojoxGrid-master-header {
	position: relative;
	/*add 25/6/08*/
	background:#BCD2EF url(images/collapsebg.gif) top repeat-x;
	border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
	border-style:solid solid solid none;
	border-width:1px 1px 1px medium;
	border-top:1px solid #99BBE8;
	border-bottom:1px solid #99BBE8;
}
.dojoxGrid-header .dojoxGrid-cell { 
	border: 1px solid;
   /*border-color: #F6F4EB #ACA899 #ACA899 #F6F4EB;*/
   border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
   background:#BCD2EF url(images/collapsebg.gif) top repeat-x;
   /*background: url(images/grid_dx_gradient.gif) #E8E1CF top repeat-x;*/
   padding-bottom: 2px;
   font-family:arial,verdana;
   font-size:11px; 
   color:#15428B;
   border-left:none;
}

</style>

 <script type="text/javascript">
  dojo.require("dojox.grid.Grid");
  dojo.require("dojox.grid._data.model");
  dojo.require("dojox.grid._data.dijitEditors");
  dojo.require("dojo.parser");
  
</script>

<%@page import="com.trilasoft.app.webapp.util.GridDataProcessor"%>


<!-- ***********************move code from below once ready******* -->

<script type="text/javascript"><!--<!--

    // ==========================================================================
    // Create a data model
    // ==========================================================================
    <%
   String htmlCode = ((String[][])request.getAttribute("tableMetadata"))[0][2]; 
   String pattern = "listRow\\[[0-9a-zA-Z_]+\\]";
   %>
   data = [
           <c:forEach var="listRow" items="${request[tableMetadata[0][0]]}" varStatus="status">
           [
           <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
               <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'html'}">
                   "<%=GridDataProcessor.processHtml(htmlCode, pageContext.getAttribute("listRow"), pattern)%>"
                </c:if>                           <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int' || tableMetadata[2][fieldNameIteratorStatus.index] == 'long'
               || tableMetadata[2][fieldNameIteratorStatus.index] == 'float')}">
                    <c:out value="${listRow[fieldName]}"/>
                </c:if>
               <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'text'}">
                    '<c:out value="${listRow[fieldName]}"/>'                        </c:if>
                <c:if test="${!fieldNameIteratorStatus.last}">,</c:if>
           </c:forEach>
           ] <c:if test="${!status.last}">,</c:if>                       </c:forEach>
       ];
      
     job=[<c:forEach var="listRow" items="${job}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	 '<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];
      routing=[<c:forEach var="listRow" items="${routing}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];
      category=[<c:forEach var="listRow" items="${category}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];
      basis=[<c:forEach var="listRow" items="${basis}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];
      mode=[<c:forEach var="listRow" items="${mode}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow}"/>',
     </c:forEach>
      ];
      service=[<c:forEach var="listRow" items="${service}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];    
       model = new dojox.grid.data.Table(null, data);
       
      var formatQuantity1 = 0;
      var formatRate1 = 0;
      var formatAmount1 = 0;
      var formatbasis1 = '';
      
      
    formatQuantity =   function calcQuan(inDatum)
    {
    	formatQuantity1 = inDatum;
    	return  (inDatum);
	}

    formatRate =   function calcRate(inDatum){
    	formatRate1 = inDatum;
    	return  (inDatum);
    }
   
    formatAmount =   function calcAmount(inDatum)
    {
       if(formatbasis1=='cwt'){
          formatAmount1=(formatRate1*formatQuantity1)/100;
       }
       else
       {
         formatAmount1=(formatRate1*formatQuantity1);
       }
       return  (formatAmount1);  
    }
     formatbasis= function Abasis(inDatum)
    {
      formatbasis1=inDatum;
      return  (inDatum);  
    }
    
    // ==========================================================================
    // Tie UI to the data model
    // ==========================================================================
    model.observer(this);
   modelChange = function(){
     dojo.byId("rowCount").innerHTML = 'Row count: ' + model.count;
   }
    // ==========================================================================
    // Custom formatters 
    // ==========================================================================
    formatCurrency = function(inDatum){
     return isNaN(inDatum) ? '...' : dojo.currency.format(inDatum, this.constraint);
   }
   formatNumber = function(inDatum){
     return isNaN(inDatum) ? '' : inDatum;
   }
   formatDate = function(inDatum){
     return dojo.date.locale.format(new Date(inDatum), this.constraint);
   }
    
    // ==========================================================================
    // Grid structure
    // ==========================================================================

     gridLayout = [{
     type: 'dojox.GridRowView', width: '0px'
   },{
     defaultCell: { width: 4, styles: 'text-align: left;'  },
     rows: [[
           <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.Input, formatter: formatNumber, width: 5}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                        editor: dojox.grid.editors.Input, formatter: formatNumber, width: 5}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'route'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.ComboBox,
                         options: routing, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'route'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.ComboBox,
                         options: routing, width: 10}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'jobType'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.ComboBox,
                         options: job, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'jobType'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.ComboBox,
                         options: job, width: 10}
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'mode'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.ComboBox,
                         options: mode, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'mode'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.ComboBox,
                         options: mode, width: 10}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'serviceType'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.ComboBox,
                         options: service, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'serviceType'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                          editor: dojox.grid.editors.ComboBox,
                         options: service, width: 10}
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'categories'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                         editor: dojox.grid.editors.ComboBox,
                         options: category, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'categories'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.ComboBox,
                         options: category, width: 10}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'basis'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.ComboBox,formatter:formatbasis,
                         options: basis, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'basis'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.ComboBox,formatter:formatbasis,
                         options:basis, width: 10}
               </c:if>
               
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'quantity'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                          editor: dojox.grid.editors.Input, formatter: formatQuantity, width: 5}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'quantity'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.Input, formatter: formatQuantity, width: 5}
                         
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'rate'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.Input, formatter: formatRate, width: 5}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'rate'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.Input, formatter: formatRate, width: 5}
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'amount'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         //editor: dojox.grid.editors.Input, formatter: formatAmount, width: 5}
                         editor: dojox.grid.editors.Input, formatter: formatAmount, width: 5}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'amount'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                        editor: dojox.grid.editors.Input, formatter: formatAmount, width: 5}
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'vendorCode'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',

                         editor: dojox.grid.editors.Input, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'vendorCode'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',
                   
                         editor: dojox.grid.editors.Input, width: 10}
               </c:if>
              <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'html'}">
                           { name: '<fmt:message key="${fieldName}"/>'}
                </c:if> 
               <c:if test="${!fieldNameIteratorStatus.last}">,</c:if>
                
      </c:forEach>
     ]]
   }];
    // ==========================================================================
    // UI Action
    // ==========================================================================
     addRow = function(){

      grid2.addRow([
        '', '', '', '','','', 0,0,0,''
        ]);
      }

      saveListData = function(){
       grid2.edit.apply();
       var fields = "";
       var fieldTypes = "";
       var editability = "";
       var select = "";
       <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
           if (fields != "") fields += ',';
           fields += '<c:out value="${fieldName}"/>';
           if (editability != "") editability += ',';
           editability += '<c:out value="${tableMetadata[3][fieldNameIteratorStatus.index]}"/>';
           if (fieldTypes != "") fieldTypes += ',';
           fieldTypes += '<c:out value="${tableMetadata[2][fieldNameIteratorStatus.index]}"/>';
                  </c:forEach>
     document.forms['gridForm'].elements['listData'].value = dojo.toJson(grid2.model.data);
     document.forms['gridForm'].elements['listFieldNames'].value = fields;
     document.forms['gridForm'].elements['listFieldTypes'].value = fieldTypes;
     document.forms['gridForm'].elements['listFieldEditability'].value = editability;
     document.forms['gridForm'].submit();
   }
--></script>
  
	<div id="otabs">
         <ul>
           <li><a class="current"><span>Default Account Lines</span></a></li>
         </ul>
       </div>
       <div class="spnblk">&nbsp;</div>

<div style="!position:relative;z-index:-1;">

<div id="grid2" jsId="grid2" dojoType="dojox.Grid" model="model" structure="gridLayout"
        style="width:1100px;height:400px;border: 1px solid #99BBE8;">
 </div>

<br />

<div id="rowCount"style="padding-bottom:8px;"></div>

<!--<div id="controls">
   <button dojoType="dijit.form.Button" onclick="loadData()">Load Data</button>
   <button dojoType="dijit.form.Button" onclick="grid2.removeSelectedRows()">Remove</button>
 <button dojoType="dijit.form.Button" onclick="grid2.singleClickEdit = !grid2.singleClickEdit">Edit Cell</button>
 <button dojoType="dijit.form.Button" onclick="saveListData()">Save</button>
 <button dojoType="dijit.form.Button" onclick="addRow()">Add</button>
 <button dojoType="dijit.form.Button" onclick="grid2.refresh()">Reset</button>
 <s:submit cssClass="cssbutton1" key="button.delete" method="delete" onclick="grid2.removeSelectedRows()" />

</div>
<br />
-->

<div id="controls">
 <s:submit cssClass="cssbutton1" key="button.save" method="delete" onclick="saveListData()" />
 <s:submit cssClass="cssbutton1" key="button.add" method="delete" onclick="addRow()" />
 <s:submit cssClass="cssbutton1" key="button.reset" method="delete" onclick="grid2.refresh()" />
</div>
<br />
</div>
</div>
<br/>

