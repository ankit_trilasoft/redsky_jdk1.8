<%@ include file="/common/taglibs.jsp" %> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head> 
<title>LISA Log</title> 
<meta name="heading" content="LISA Log"/> 
 <style><%@ include file="/common/calenderStyle.css"%></style>
 <script src="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-15px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;
}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:52px; height:25px;"  method="lisaIntegrationLogSearch"  key="button.search" onclick="return goToSearchCustomerDetail();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px; height:25px;" onclick="clear_fields();"/> 
</c:set>   
<body>
<script type="text/javascript">
var r1={
		 'special1':/['\#'&'\$'&'\~'&'\!'&'\@'&'\+'&'\\'&'\/'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'('&'\)']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		
function validCheck(targetElement,w){
	 targetElement.value = targetElement.value.replace(r1[w],'');
	}
</script>
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['ugwwIntegrationErrorLog'].elements['integrationId'].value  = "";
		document.forms['ugwwIntegrationErrorLog'].elements['operation'].value  = "";
		document.forms['ugwwIntegrationErrorLog'].elements['orderComplete'].value  = "";
		document.forms['ugwwIntegrationErrorLog'].elements['statusCode'].value  = "";
		document.forms['ugwwIntegrationErrorLog'].elements['effectiveDate'].value  = "";
		document.forms['ugwwIntegrationErrorLog'].elements['registrationNumber'].value  = "";
}
function goToSearchCustomerDetail(){
	var soNumber=document.forms['ugwwIntegrationErrorLog'].elements['integrationId'].value;
	var action=document.forms['ugwwIntegrationErrorLog'].elements['operation'].value;
	var userName=document.forms['ugwwIntegrationErrorLog'].elements['orderComplete'].value;
	var actionTime=document.forms['ugwwIntegrationErrorLog'].elements['statusCode'].value;
	var trackCorpID=document.forms['ugwwIntegrationErrorLog'].elements['effectiveDate'].value;
	document.forms['ugwwIntegrationErrorLog'].action = 'lisaIntegrationLogSearch.html';
	document.forms['ugwwIntegrationErrorLog'].submit();
}
</script> 
 <s:form id="ugwwIntegrationErrorLog" name="ugwwIntegrationErrorLog" action="lisaIntegrationLogSearch" method="post" validate="true"> 
<!-- <p style="font-size:11px; font-weight:bold; margin-bottom:10px">Integration XML Summary</p> -->
<p style="font: bold 12px/2.5em arial,verdana; margin-bottom: 10px;" class="bgblue">Integration Centre</p>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:set name="integrationErrorList" value="%{integrationErrorList}" scope="session"/>  
<s:hidden name="id" />
<div id="newmnav">
		  <ul>
		<c:if test="${vanlineEnabled==true }">
		   <li><a href="integrationCenterList.html"><span>Vanline / Docs Downloads</span></a></li>
		     <li><a href="centreVanlineUpload.html"><span>Vanline / Docs Uploads</span></a></li>
		      <li><a href="centerIntegrationLogs.html"><span>Vanline Integration Log</span></a></li>
		       </c:if>
		       <c:if test="${enableMSS==true }">
		       <li><a href="mssLogList.html"><span>MSS Orders</span></a></li>
		       </c:if>
		             <li><a href="UGWWList.html"><span>UGWW</span></a></li>
		             <li><a href="centerintegrationLogList.html"><span>UGWW Log</span></a></li>
		       
		         <c:if test="${voxmeIntegration==true }">
		         <li><a href="voxmeEstimatorList.html"><span>Voxme&nbsp;Estimator</span></a></li>
		         </c:if>
		          <c:if test="${enablePricePoint==true}">
		          <li><a href="pricePointList.html"><span>Price&nbsp;Point</span></a></li>
		          </c:if>
		           <c:if test="${enableMoveForYou==true }">
		        <li><a href="moveForUList.html"><span>Move&nbsp;4&nbsp;u</span></a></li>
		        </c:if>
		        <li style="background:#FFF " id="newmnav1"><a class="current"><span>LISA&nbsp;Integration</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:16px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
    <div id="otabs" style="margin-bottom:24px;">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
<table class="table" style="width:100%"  >
<thead>
<tr>
<th>Crew Name</th>
<th>Crew Email</th>
<th>Ticket</th>
<th>Shipper</th>
<th>Status</th>
<th>Work Date</th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
		
			<td width="" align="left">
			    <s:textfield name="registrationNumber" id="registrationNumber" required="true" cssClass="input-text" cssStyle="width:100px" size="15" onchange="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			
			<td width="" align="left">
			    <s:textfield name="integrationId" id="integrationId" required="true" cssClass="input-text" cssStyle="width:90px" size="15" onchange="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
		
			<td width="" align="left">
			    <s:textfield name="operation" id= "operation" required="true" cssClass="input-text" size="16" onchange="valid(this,'special')"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="orderComplete" id ="orderComplete" required="true" cssClass="input-text" size="10"onchange="valid(this,'special')" />
			</td>
			<td width="" align="left">
			    <s:textfield name="statusCode" id ="statusCode" required="true" cssClass="input-text" size="10" onchange="valid(this,'special')" />
			</td>
			
			<td width="" align="left">
			         <c:if test="${not empty effectiveDate}">
			         <s:text id="effectDate" name="${FormDateValue}"><s:param name="value" value="effectiveDate"/></s:text>
					<s:textfield cssClass="input-text" id="effectiveDate" value="%{effectDate}" name="effectiveDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
					<img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['ugwwIntegrationErrorLog'].elements['effectiveDate'].focus();  return false;"/>
					</c:if>
					
					<c:if test="${empty effectiveDate}"> 
					   <s:textfield cssClass="input-text" id="effectiveDate" name="effectiveDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
					   <img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['ugwwIntegrationErrorLog'].elements['effectiveDate'].focus();  return false;"/>
		            </c:if>	
			</td>
			<td colspan="2" style="text-align:right;" >
			    <c:out value="${searchbuttons}" escapeXml="false" />
			</td>
			</tr>
			
			</table>  
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 
<c:out value="${searchresults}" escapeXml="false" />  
<div id="newmnav">
		  <ul>	    
		       <li><a class=""><span>LISA Log List</span></a></li>
		       
		  </ul>
		</div>
		<div class="spn" style="line-height:2px;!margin-bottom:3px;">&nbsp;</div>			
<display:table name="integrationErrorList" class="table" pagesize="25"requestURI="" id="customerFileList" export="true"  style="width:100%;" defaultsort="6" defaultorder="descending">
<display:column title="Crew Name" property="registrationNumber" sortable="true" />
<display:column title="Crew Email" property="integrationId" sortable="true" />
<display:column title="Ticket" property="operation" sortable="true" />
<display:column title="Shipper" property="orderComplete" sortable="true" />
<display:column title="Status" property="statusCode" sortable="true" maxLength="30"/>
<display:column title="Work Date" property="effectiveDate" format="{0,date,dd-MMM-yyyy}" sortable="true" />
</display:table>
</s:form>
 <script type="text/javascript">	
	setCalendarFunctionality();
</script>
</body>
