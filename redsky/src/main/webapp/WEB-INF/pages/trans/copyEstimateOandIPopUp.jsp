<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">  
<head>   
  <title>Estimate List</title>  
    <style type="text/css"> 
#overlay11 {
    filter:alpha(opacity=70);
    -moz-opacity:0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
    position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
    background:url(images/over-load.png);
}
</style> 
</head>

<s:form name="copyEstimateListForm" id="copyEstimateListForm">
<s:hidden name="oid" id="oid" />

<s:set name="copyEstimateList" value="copyEstimateList" scope="request"/>
	<display:table name="copyEstimateList" class="table" requestURI="" id="copyEstimateList" style="width:500px" pagesize="50" >   
			<display:column title="Work order" property="workorder" style="width:100px" />
			<display:column title="Ticket" property="ticket" style="width:100px" />
			<display:column title="Date" property="date" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
		    <display:column   title="" style="width:10px" >
	 			<input style="vertical-align:bottom;" type="checkbox" name="radioButton" id="radioButton${copyEstimateList.id}" value="${copyEstimateList.id}" onclick="setValues('${copyEstimateList.id}',this)"/>
	 		</display:column>
	</display:table>

	<table  cellspacing="0" cellpadding="0" border="0" style="width:510px;margin: 0px;padding: 0px;">
			<tbody>				
				<tr ><td class="listwhitetext" style="height:10px;"></td></tr>
				<c:if test="${copyEstimateList!='[]'}">
					<tr>
						<td align="center" class="listwhitetext" style="height:10px"></td>
						<td style="text-align:left;">
							<input type="button" id="save" class="cssbutton" method="" name="save" value="Copy To Revision"  style="width:110px; height:25px;margin:0px 10px 0px 10px;" onclick="validateFields();" />
						 	<input type="button" id="cancel" class="cssbutton" method="" name="cancel" value="Cancel" style="width:60px; height:25px;margin:0px 50px 0px 10px;" onclick="closeWindow();" />
						</td>
					</tr>
				</c:if>
				<c:if test="${copyEstimateList=='[]'}">
					<tr>
						<td align="center" class="listwhitetext" style="height:10px"></td>
						<td style="text-align:left;">
							<input type="button" id="save" class="cssbutton" value="Copy To Revision" disabled="true" style="width:110px; height:25px;margin:0px 10px 0px 10px;" />
						 	<input type="button" id="cancel" class="cssbutton" value="Cancel" style="width:60px; height:25px;margin:0px 50px 0px 10px;" onclick="closeWindow();" />
						</td>
					</tr>
				</c:if>				
			</tbody>
	</table>
<div id="overlay11">
            <table cellspacing="0" cellpadding="0" border="0" width="100%" >
            <tr>
            <td align="center">
            <table cellspacing="0" cellpadding="3" align="center">
            <tr>
            <td height="100px"></td>
            </tr>
            <tr>
           <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
               <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
           </td>
           </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>

</s:form>
</div></div></div>

<script type="text/javascript">
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay11"].visibility='show';
       	else
          document.getElementById("overlay11").style.visibility='visible';
   	}
}
function closeWindow() {
	window.close();
}
   
   function setValues(rowId,targetElement){
	   var userCheckStatus = document.getElementById("oid").value
	  if(targetElement.checked){
		  if(userCheckStatus == '' ){
			  document.getElementById("oid").value = rowId;
			}else{
				if(userCheckStatus.indexOf(rowId)>=0){
				}else{
					userCheckStatus = userCheckStatus + "~" +rowId;
				}
				document.getElementById("oid").value = userCheckStatus.replace('~ ~',"~");
			}
	   }else{
			if(userCheckStatus == ''){
				document.getElementById("oid").value = rowId;
		     }else{
		    	 var check = userCheckStatus.indexOf(rowId);
		 		if(check > -1){
		 			var values = userCheckStatus.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== rowId) {
		 		    	 values.splice(i, 1);
		 		    	userCheckStatus = values.join("~");
		 		    	document.getElementById("oid").value = userCheckStatus;
		 		      }
		 		   }
		 		}else{
		 			userCheckStatus = userCheckStatus + "~" +rowId;
		 			document.getElementById("oid").value = userCheckStatus.replace('~ ~',"~");
		 		}
		     }
	   }
  	}
   
   function validateFields(){
	   showOrHide(1);
		var oidBlank=document.forms['copyEstimateListForm'].elements['oid'].value;
		if(oidBlank!=''){
			new Ajax.Request('/redsky/copyToRevisionOandI.html?ajax=1&oid='+oidBlank+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      refreshParent();
				      showOrHide(0);
				    },
				    onFailure: function(){ 
					    }
				  });
		
		}else{
			alert('Please check any check box to continue.');
			showOrHide(0);
			return false;
		}
	}
   
   function refreshParent(){
	   parent.window.opener.document.location.reload();
	   window.close();
   }
   try{ 
	  showOrHide(0);
	}catch(e){}   
  </script>
 		  	