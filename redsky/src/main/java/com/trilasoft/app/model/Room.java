/**
 * 
 */
package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author GVerma
 *
 */
@Entity
@Table(name="room")
public class Room {
	private Long id;
	private String room;
	private String roomDesc;
	private Long imageId;
	private String corpId;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private Long locationId;
	private String shipNumber;
	private Boolean hasDamage = new Boolean(false);
	private String floor;
	private String comment;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("room", room)
				.append("roomDesc", roomDesc).append("imageId", imageId)
				.append("corpId", corpId).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy)
				.append("locationId", locationId)
				.append("shipNumber", shipNumber)
				.append("hasDamage", hasDamage).append("floor", floor)
				.append("comment", comment).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Room))
			return false;
		Room castOther = (Room) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(room, castOther.room)
				.append(roomDesc, castOther.roomDesc)
				.append(imageId, castOther.imageId)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(locationId, castOther.locationId)
				.append(shipNumber, castOther.shipNumber)
				.append(hasDamage, castOther.hasDamage)
				.append(floor, castOther.floor)
				.append(comment, castOther.comment).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(room).append(roomDesc)
				.append(imageId).append(corpId).append(createdOn)
				.append(createdBy).append(updatedOn).append(updatedBy)
				.append(locationId).append(shipNumber).append(hasDamage)
				.append(floor).append(comment).toHashCode();
	}
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the room
	 */
	@Column
	public String getRoom() {
		return room;
	}
	/**
	 * @param room the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
	}
	/**
	 * @return the roomDesc
	 */
	@Column
	public String getRoomDesc() {
		return roomDesc;
	}
	/**
	 * @param roomDesc the roomDesc to set
	 */
	public void setRoomDesc(String roomDesc) {
		this.roomDesc = roomDesc;
	}
	/**
	 * @return the imageId
	 */
	@Column
	public Long getImageId() {
		return imageId;
	}
	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}
	/**
	 * @return the corpId
	 */
	@Column
	public String getCorpId() {
		return corpId;
	}
	/**
	 * @param corpId the corpId to set
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the createdBy
	 */
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the updatedBy
	 */
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the locationId
	 */
	@Column
	public Long getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Boolean getHasDamage() {
		return hasDamage;
	}
	public void setHasDamage(Boolean hasDamage) {
		this.hasDamage = hasDamage;
	}
	@Column
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	@Column
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

}
