package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.FreightMarkUpControl;
import com.trilasoft.app.service.FreightMarkUpManager;
import com.trilasoft.app.service.PricingControlManager;
import com.trilasoft.app.service.RefMasterManager;

public class FreightMarkUpAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private FreightMarkUpControl freightMarkUpControl;
	private FreightMarkUpManager freightMarkUpManager;
	private PricingControlManager pricingControlManager;
	private List freightMarkUpList;
	private RefMasterManager refMasterManager;
	private Map<String, String> currencyList;
	private String hitFlag;
	private List contract;
	
	
	public FreightMarkUpAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId){
		 currencyList=refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		 contract=pricingControlManager.getContract(sessionCorpID);
		return SUCCESS;
	 }
	
	public void prepare() throws Exception {
		getComboList(sessionCorpID);
	}
	
	
	@SkipValidation
	public String list(){
		freightMarkUpList=freightMarkUpManager.getAll();
		return SUCCESS;
	}
	@SkipValidation
	public String edit(){
		//getComboList(sessionCorpID);
		
		if (id!= null)
		{
			freightMarkUpControl=freightMarkUpManager.get(id);
		}
		else
		{
			freightMarkUpControl=new FreightMarkUpControl();
			freightMarkUpControl.setContract("NO CONTRACT - INT");
			freightMarkUpControl.setCurrency("USD");
			freightMarkUpControl.setCreatedBy(getRequest().getRemoteUser());
			freightMarkUpControl.setCreatedOn(new Date());
			freightMarkUpControl.setUpdatedBy(getRequest().getRemoteUser());
			freightMarkUpControl.setUpdatedOn(new Date());
		}
		freightMarkUpControl.setCorpID(sessionCorpID);
		return SUCCESS;
	}
	
	public String save() throws Exception{
		//getComboList(sessionCorpID);
		boolean isNew = (freightMarkUpControl.getId() == null);
			if(isNew)
			{
				freightMarkUpControl.setCreatedBy(getRequest().getRemoteUser());
				freightMarkUpControl.setCreatedOn(new Date());
			}
		freightMarkUpControl.setUpdatedBy(getRequest().getRemoteUser());
		freightMarkUpControl.setUpdatedOn(new Date());
		freightMarkUpControl.setCorpID(sessionCorpID);	
		freightMarkUpControl=freightMarkUpManager.save(freightMarkUpControl);
		String key = (isNew) ? "FreightMarkUp has been added successfully" : "FreightMarkUp has been updated successfully";
		saveMessage(getText(key));
	
            return SUCCESS;
 	}
	
	public String delete(){
		try{
			freightMarkUpManager.remove(id);
			hitFlag="1";
			list();
			}catch(Exception ex)
			{
				list();
				ex.printStackTrace();
			}
		return SUCCESS;
	}
	public String search()
	{
				
			return SUCCESS;
	}

	public FreightMarkUpControl getFreightMarkUpControl() {
		return freightMarkUpControl;
	}

	public void setFreightMarkUpControl(
			FreightMarkUpControl freightMarkUpControl) {
		this.freightMarkUpControl = freightMarkUpControl;
	}

	public List getFreightMarkUpList() {
		return freightMarkUpList;
	}

	public void setFreightMarkUpList(List freightMarkUpList) {
		this.freightMarkUpList = freightMarkUpList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setFreightMarkUpManager(
			FreightMarkUpManager freightMarkUpManager) {
		this.freightMarkUpManager = freightMarkUpManager;
	}

	public Map<String, String> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(Map<String, String> currencyList) {
		this.currencyList = currencyList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public void setPricingControlManager(PricingControlManager pricingControlManager) {
		this.pricingControlManager = pricingControlManager;
	}

	public List getContract() {
		return contract;
	}

	public void setContract(List contract) {
		this.contract = contract;
	}
	

}
