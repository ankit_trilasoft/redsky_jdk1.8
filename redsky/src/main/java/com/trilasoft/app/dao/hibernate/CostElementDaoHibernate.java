package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.model.CostElement;
import com.trilasoft.app.model.RefJobDocumentType;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.dao.CostElementDao;
import com.trilasoft.app.dao.hibernate.dto.ReportEmailDTO;
public class CostElementDaoHibernate extends GenericDaoHibernate<CostElement, Long> implements CostElementDao {
    public CostElementDaoHibernate() {   
        super(CostElement.class);   
    }
	public List getCostListDataSummary(){
		return getHibernateTemplate().find("from CostElement");
	}
	public String costElDetailCode(String costEl){
		String costElement="";
		List cList= getHibernateTemplate().find("select concat(description,'~',recGl,'~',payGl) from CostElement where costElement = ?", costEl);
		if((cList!=null) && (!cList.isEmpty()))
		{
			costElement=cList.get(0).toString();
		}
		return costElement;
	}
	public String costDesDetailCode(String costDes){
		String costDescrption="";
		List cList= getHibernateTemplate().find("select concat(costElement,'~',recGl,'~',payGl) from CostElement where description = ?", costDes);
		if((cList!=null) && (!cList.isEmpty()))
		{
			costDescrption=cList.get(0).toString();
		}
		return costDescrption;
	}
	public List findCostElementList(String chargeCostElement, String agentCorpId,String jobType, String routing, String soCompanyDivision) {
		if(chargeCostElement== null){
			chargeCostElement="";
		}else{
			chargeCostElement =  chargeCostElement.replaceAll("'","''");
		}
		if((jobType == null || jobType.equals("") )&&(routing == null || routing.equals(""))){
		  	List agentGLList=this. getSession().createSQLQuery("select concat(recGl,'~',payGl,'~',reportingAlias) from costelement where corpId='"+agentCorpId+"' and costElement = '"+chargeCostElement+"'").list();
		  	return agentGLList  ;
		}else{
		
			List agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '"+routing+"' and cg.companyDivision='"+soCompanyDivision+"' and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list();
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = ''  and cg.companyDivision='"+soCompanyDivision+"'  and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list(); 
	        }
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"'  and cg.companyDivision='"+soCompanyDivision+"'  and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list(); 
	        }
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = ''  and cg.companyDivision='"+soCompanyDivision+"'  and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list(); 
	        }
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '"+routing+"'  and cg.companyDivision=''  and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list(); 
	        }
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = ''  and cg.companyDivision=''  and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list(); 
	        }
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias ) from glcoderategrid cg,costelement ce where cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"'  and cg.companyDivision=''  and cg.corpID='"+agentCorpId+"' and ce.corpID='"+agentCorpId+"' and ce.costElement = '"+chargeCostElement+"'").list(); 
	        }
	        if(agentGLList ==null || agentGLList.isEmpty()){
	        	agentGLList=this.getSession().createSQLQuery("select concat(recGl,'~',payGl,'~',reportingAlias) from costelement where corpId='"+agentCorpId+"' and costElement = '"+chargeCostElement+"'").list();
	        }
	        
			return agentGLList  ;
		}
	}
	public List findCostElementListTemplate(String chargeCode,String sessionCorpID, String jobType, String routing,String contract, String soCompanyDivision){
		List agentGLList = new ArrayList();
		String query = "";
		query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '"+routing+"' and cg.companyDivision='"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ;
		agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
		if(agentGLList ==null || agentGLList.isEmpty()){
        	query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where   ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.companyDivision='"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ; 
        	agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
		}
        if(agentGLList ==null || agentGLList.isEmpty()){
        	 query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where   ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.companyDivision='"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ;
        	 agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
        }
        if(agentGLList ==null || agentGLList.isEmpty()){
       	 query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where   ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '' and cg.companyDivision='"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ;
       	 agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
       }
        if(agentGLList ==null || agentGLList.isEmpty()){
          	 query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where   ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '"+routing+"' and cg.companyDivision='' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ;
          	 agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
          }
        if(agentGLList ==null || agentGLList.isEmpty()){
         	 query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where   ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.companyDivision='' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ;
         	 agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
         }
        if(agentGLList ==null || agentGLList.isEmpty()){
         	 query = "select concat((if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'~',(if(cg.gridPayGl is null or cg.gridPayGl = '',ce.payGl,cg.gridPayGl)),'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce , glcoderategrid cg  where   ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.companyDivision='' and cg.corpID='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1" ;
         	 agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
         }
        if(agentGLList ==null || agentGLList.isEmpty()){
        	 query = "select concat(ce.recGl,'~',ce.payGl,'~',ce.reportingAlias,'~',ch.description ) as valueText from charges ch, costelement ce  where  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and ch.contract ='"+contract+"' limit 1";
        	 agentGLList =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
        }
        
		return agentGLList  ;
	}
	public List findAgentCostElement(String costElement, String agentCorpId){
		if(costElement== null){
			costElement="";
		}else{
			costElement =  costElement.replaceAll("'","''");
		}
		return getSession().createSQLQuery("select id from costelement where corpID='"+agentCorpId+"' and costElement='"+costElement+"'").list();
	}
	public List getCharges(String costElement, String sessionCorpID){
		return getHibernateTemplate().find("select id from Charges where costElement='"+costElement+"' and corpID='"+sessionCorpID+"'");
	}
	public int updateAccountlineGLByCostElement(String charge, String contract, String sessionCorpID, String recGl, String payGl,Long costElementId){
		List al=this.getSession().createSQLQuery("select a.id,s.job,s.routing,if(a.recAccDate is null and a.recPostDate is null and a.payAccDate is null and a.payPostDate is null,'BOTH',if(a.recAccDate is null and a.recPostDate is null,'REC','PAY')),a.companyDivision from accountline a,serviceorder s where s.id=a.serviceOrderId and ((a.recAccDate is null and a.recPostDate is null) OR (a.payAccDate is null and a.payPostDate is null)) and s.corpId='"+sessionCorpID+"' and a.chargeCode='"+charge+"' and a.contract='"+contract+"'").list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
			String idList=covertToCommasSeperatedArray(al,sessionCorpID,costElementId);
			if(!idList.equalsIgnoreCase("")){
				String str[]=idList.split(",");
				int k=0;
				for(int l=0;l<str.length;l++){
					int c=0;
					String temp[]=str[l].split("~");
					if(temp[1].toString().equalsIgnoreCase("BOTH")){
						c=getHibernateTemplate().bulkUpdate("UPDATE AccountLine set recGl='"+recGl+"',payGl='"+payGl+"' where id = "+Long.parseLong(temp[0].toString())+"");
					}else if(temp[1].toString().equalsIgnoreCase("REC")){
						c=getHibernateTemplate().bulkUpdate("UPDATE AccountLine set recGl='"+recGl+"' where id = "+Long.parseLong(temp[0].toString())+"");
					}else{
						c=getHibernateTemplate().bulkUpdate("UPDATE AccountLine set payGl='"+payGl+"' where id = "+Long.parseLong(temp[0].toString())+"");
					}
					k=k+c;
				}
				return k;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	public String covertToCommasSeperatedArray(List list,String sessionCorpID,Long costElementId){
		  String a="";
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				List glCodeList=null;
				String tempQuery="";
				tempQuery="from GlCodeRateGrid where costElementId="+costElementId+" and corpID='"+sessionCorpID+"'";
				if((row[1]!=null)&&(!row[1].toString().equalsIgnoreCase(""))){
					tempQuery=tempQuery+" and (gridJob is null OR gridJob ='' OR gridJob like '%"+row[1]+"%')";
				}
				if((row[2]!=null)&&(!row[2].toString().equalsIgnoreCase(""))){
					tempQuery=tempQuery+" and (gridRouting is null OR gridRouting ='' OR gridRouting like '%"+row[2]+"%')";
				}
				if((row[4]!=null)&&(!row[4].toString().equalsIgnoreCase(""))){
					tempQuery=tempQuery+" and (companyDivision is null OR companyDivision ='' OR companyDivision like '%"+row[4]+"%')";
				}
				glCodeList = getHibernateTemplate().find(tempQuery);
	    		if((glCodeList==null)||(glCodeList.isEmpty())){ 
	    			if(a.equalsIgnoreCase("")){
		    			 a=row[0].toString()+"~"+row[3].toString();
	    			}else{
	    				a=a+","+row[0].toString()+"~"+row[3].toString();
	    			}
	    		}
		   }
		     return a;
	}	
	public String covertToCommasSeperated(List list){
			  String a="";
				Iterator it = list.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
		    			if(a.equalsIgnoreCase("")){
			    			 a=row[0].toString()+"~"+row[1].toString();
		    			}else{
		    				a=a+","+row[0].toString()+"~"+row[1].toString();
		    			}
			    }
			     return a;		     
	}
	public int updateAccountlineGLByGLRateGrid(String charge, String contract, String sessionCorpID, String job, String routing, String recGl, String payGl,String companyDivision){
		if(job==null){job="";}
		if(routing==null){routing="";}
		List al=this.getSession().createSQLQuery("select a.id,if(a.recAccDate is null and a.recPostDate is null and a.payAccDate is null and a.payPostDate is null,'BOTH',if(a.recAccDate is null and a.recPostDate is null,'REC','PAY')) from accountline a,serviceorder s where s.id=a.serviceOrderId and ((a.recAccDate is null and a.recPostDate is null) OR (a.payAccDate is null and a.payPostDate is null)) and s.corpId='"+sessionCorpID+"' and s.companyDivision like '%"+companyDivision+"%' and s.job like '%"+job+"%' and s.routing like '%"+routing+"%' and a.chargeCode='"+charge+"' and a.contract='"+contract+"'").list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
			String idList=covertToCommasSeperated(al);
			if(!idList.equalsIgnoreCase("")){
				String str[]=idList.split(",");
				int k=0;
				for(int l=0;l<str.length;l++){
					int c=0;
					String temp[]=str[l].split("~");
					if(temp[1].toString().equalsIgnoreCase("BOTH")){
						c=getHibernateTemplate().bulkUpdate("UPDATE AccountLine set recGl='"+recGl+"',payGl='"+payGl+"' where id = "+Long.parseLong(temp[0].toString())+"");
					}else if(temp[1].toString().equalsIgnoreCase("REC")){
						c=getHibernateTemplate().bulkUpdate("UPDATE AccountLine set recGl='"+recGl+"' where id = "+Long.parseLong(temp[0].toString())+"");
					}else{
						c=getHibernateTemplate().bulkUpdate("UPDATE AccountLine set payGl='"+payGl+"' where id = "+Long.parseLong(temp[0].toString())+"");
					}
					k=k+c;
				}
				return k;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	public int checkForCostEleUseInCharge(String costElement,String description, String recGl, String payGl, String sessionCorpID){
		int rec=0;
		List al = getHibernateTemplate().find("FROM Charges WHERE costElement=?",costElement);
		if((al!=null)&&(!al.isEmpty()))
		{
			rec=1;	
		}else{
			rec=-1;
		}
		return rec;
	}
	public List getCostElementValue(String tableN,String tableD){
		
		tableN=tableN.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");
		tableD=tableD.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");		
		String a = "%"+ tableN + "%";
		String b = "%"+tableD + "%";
		Criteria crit = this.getSession().createCriteria(CostElement.class);
		return crit.add(Restrictions.ilike("costElement", a)).add(Restrictions.ilike("description", b)).list();
	}
	
	public Map<String, String> costElementDataMap(String agentCorpID){
		Map<String,String> costElementData = new HashMap<String,String>();
		try {
			String query = "select costElement,if((recGl is null or recGl=''),'NODATA',recGl),if((payGl is null or payGl=''),'NODATA',payGl) from costelement where corpId='"+ agentCorpID +"' order by costElement";
		List costElementList  = this.getSession().createSQLQuery(query).list();
		Iterator itr=costElementList.iterator();
			while(itr.hasNext()){
				Object[] obj=(Object[])itr.next();
				costElementData.put(obj[0].toString(),obj[1].toString()+"~"+obj[2].toString());
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return costElementData;
	}
	
	public List findAgentCostElementData(String sessionCorpID,String agentCorpId){
		List costElementData = new ArrayList();
		try {
			String	query = "select id from costelement where corpId='"+ sessionCorpID +"' and trim(costElement) not in"
					+ " (select trim(costElement) from costelement where corpID='"+ agentCorpId +"' and costElement <>'' and costElement is not null )";
			 costElementData  = this.getSession().createSQLQuery(query).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return costElementData;
	}
}
