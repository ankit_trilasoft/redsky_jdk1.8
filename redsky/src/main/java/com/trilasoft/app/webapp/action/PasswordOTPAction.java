package com.trilasoft.app.webapp.action;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.Constants;
import org.appfuse.model.User; 
import org.appfuse.service.UserManager; 
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction; 

import javax.servlet.http.HttpServletRequest; 

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.PasswordOTP; 
import com.trilasoft.app.model.SystemDefault; 

import java.util.Calendar;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;   
import java.util.Properties;  

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart; 

import com.trilasoft.app.service.CompanyManager; 
import com.trilasoft.app.service.PasswordOTPManager;
import com.trilasoft.app.service.SystemDefaultManager; 

import org.appfuse.util.VerifyRecaptcha;
 
public class PasswordOTPAction extends BaseAction  {
    
    
    
     /** Model Objects */    
     
    private User userOTP;
    private PasswordOTP passwordOTP;
    private Company company;
    private SystemDefault systemDefault;
    
    
     /** Manager Objects*/
      
    private CompanyManager companyManager;
    private PasswordOTPManager passwordOTPManager;
    private UserManager userManager;
    private SystemDefaultManager systemDefaultManager;
    
    
     /** Variables */
      
    private String OTPusername;
    //private String email; 
    private String verificationCode;
    private String newPassword;
    private String rePassword; 
    private String sessionCorpID; 
    private String bypass_sessionfilter; 
    private String userType; 
    private List passwordOTPData;
    private List <User> userList; 
	static final Logger logger = Logger.getLogger(PasswordOTPAction.class); 
	Date currentdate = new Date();
    
	
     /** Method For Showing User*/
     
    public String forgotPasswordOTP() throws Exception {
    	String IPaddress=getRequest().getRemoteAddr();    
		 logger.warn("Request IP for forgotPasswordOTP : "+IPaddress+" method start");
    	return SUCCESS;   
    }
      
    /*
     * Method For Generate New OTP*/
     
    public PasswordOTP generateOTP(User userOTP, String  userIP){
    	logger.warn("USER AUDIT: "+currentdate+" generateOTP method start");
    	String IPaddress=getRequest().getRemoteAddr();    
		logger.warn("Request IP for generateOTP : "+IPaddress+" method");
    	int randomPin   =(int)(Math.random()*9000)+1000;
	    String otp  =String.valueOf(randomPin);  
			    passwordOTP = new PasswordOTP();
				passwordOTP.setCorpID("");
				passwordOTP.setCreatedBy(userOTP.getUsername());
				passwordOTP.setUpdatedBy(userOTP.getUsername());
				passwordOTP.setCreatedOn(new Date());
				passwordOTP.setUpdatedOn(new Date());
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.MINUTE, 15);
				passwordOTP.setExpiryDate(calendar.getTime());
				passwordOTP.setUserId(userOTP.getId()); 
				passwordOTP.setOtpCode(otp);
				passwordOTP=passwordOTPManager.save(passwordOTP); 
	    if(userOTP!=null && passwordOTP!=null && userOTP.getId() !=null && passwordOTP.getUserId() !=null &&   (userOTP.getId().toString().trim().equals(passwordOTP.getUserId().toString().trim()))){
	    sendOTPEmail(userOTP,passwordOTP, userIP);
	    }
	    logger.warn("USER AUDIT: "+currentdate+" generateOTP method end");	
	    return passwordOTP;
    } 
    
    
     /** Method For Send OTP Email*/
     
    public void sendOTPEmail(User userOTP,PasswordOTP passwordOTP,String userIP) {
    	try{
    		logger.warn("USER AUDIT: "+currentdate+" sendOTPEmail method start");
    		String IPaddress=getRequest().getRemoteAddr();    
    		logger.warn("Request IP for sendOTPEmail : "+IPaddress+" method");
    	   //email = userOTP.getEmail();
    		String ipAddress = "";
    		try {
    			ipAddress = "" + InetAddress.getLocalHost().getHostAddress();
    		} catch (UnknownHostException e) {
    			e.printStackTrace();
    		}
    		
    		//request.getRemoteAddr();
		   String host = "localhost";
		   String from = "support@redskymobility.com";
		   //String to[]= UserEmailId.split(",");
		   String tempEmailTo=userOTP.getEmail();
		   String tempRecipient="";
			String tempRecipientArr[]=tempEmailTo.split(",");
			for(String str1:tempRecipientArr){ 
					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
					tempRecipient += str1; 
			}
		   tempEmailTo=tempRecipient;
		   String to[] =  new String[]{tempEmailTo};
		   Properties props = System.getProperties();
		   props.put("mail.transport.protocol", "smtp");
		   props.put("mail.smtp.host", host);
		   Session session = Session.getInstance(props, null);
		   session.setDebug(true);
		   Message message = new MimeMessage(session);
		   message.setFrom(new InternetAddress(from));  
		   InternetAddress[] address =  { new InternetAddress(tempEmailTo) };
		   message.setRecipients(Message.RecipientType.TO, address); 
		   message.setSubject("Redsky password assistance");
		   BodyPart messageBodyPart = new MimeBodyPart();
		   SimpleDateFormat format1 = new SimpleDateFormat("MMM-dd-yyyy");
		   StringBuilder systemDate1 = new StringBuilder(format1.format(new Date()));
		   String msgText1="Dear "+userOTP.getUsername()+  ", <br><br>You recently requested to reset your password for your RedSky account. Please find below the verification pin to reset your password.  <br><br><b> Verification Pin: " +passwordOTP.getOtpCode()+"</b><br><br>If you did not request a password reset, please ignore this email or let us know.<br><br><b>Source # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) +"<br><br>Best regards,<br>RedSky Support Team";
		   messageBodyPart.setText(msgText1);
		   Multipart multipart = new MimeMultipart();
		   multipart.addBodyPart(messageBodyPart);
		   messageBodyPart = new MimeBodyPart(); 
		   MimeBodyPart mbp1 = new MimeBodyPart();
		   Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			message.setContent(mp);
			message.setSentDate(new Date());
			message.setContent(msgText1, "text/html; charset=ISO-8859-1"); 
		  try{
		       Transport.send(message);
		       logger.warn("mail send successfully to user : " + address[0]) ; 
		  }
		  catch(SendFailedException sfe)
		   {
			  logger.warn("Exception Occurred: " + sfe);			 	 
         }
		  catch(Exception e){
			 e.printStackTrace();
		  } 
    	}catch(Exception e){
			 e.printStackTrace();
		  }
    	logger.warn("USER AUDIT: "+currentdate+" sendOTPEmail method end");	
    }  
    
    
     /** Method For Get Verification Pin According User */
     
    
    public synchronized String getVerificationPin() { 
    	try{
    	logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method Start");	
    	HttpServletRequest request = ServletActionContext.getRequest();
    	String url = (request.getRequestURI()).substring(request.getRequestURI().lastIndexOf("/"), request.getRequestURI().length());
  	  
  	  if(url.indexOf("!")>-1){
  		  url=url.replaceAll(url.substring(url.indexOf("!"), url.indexOf(".")), "");
  	  }
   	if(url!=null && (!(url.trim().equals(""))) && ((url.indexOf("verifyCode.html"))>=0)){
  	
   		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
   		String userIP= request.getHeader("Forwarded");
  	
  	if(gRecaptchaResponse!=null){
  		boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse); 
  	if(verify){ 
 
    	userList = passwordOTPManager.loadUserByUsername(OTPusername); 
    	if( (userList!=null) && !(userList.isEmpty())){
    		userOTP = userList.get(0); 
    		 
    		if(userOTP.isEnabled())
			{
    			systemDefault = systemDefaultManager.findByCorpID(userOTP.getCorpID()).get(0);
				userType = userOTP.getUserType(); 
				passwordOTPData=passwordOTPManager.checkById(userOTP.getId()); 
				if(passwordOTPData !=null && (!(passwordOTPData.isEmpty())) ) {
				String key = "You can request for new pin after 15 minutes.";
				saveMessage(getText(key)); 
				logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method End");
				return INPUT;
				}else{
    			passwordOTP=generateOTP(userOTP, userIP);  
    			logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method End");
    	    	return SUCCESS;	
				}
    			  
			}else
			{
				OTPusername="";
				String key = "Your account is not enabled";
				saveMessage(getText(key));  
				return INPUT;
			}
    	}
    	else
		{ 
			String key = "Invalid Username.";
			saveMessage(getText(key)); 
			logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method End");
			return INPUT;
		}
  	 
  	}else
	{ 
		String key = "You missed the Captcha.";
		errorMessage(getText(key));
		logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method End");
		return INPUT;
	}
  	}else{
  		logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method End");
  		return INPUT;	
  	}
   	}
    }catch(Exception e){
    	
    	e.printStackTrace();
    }
    	logger.warn("USER AUDIT: "+currentdate+" getVerificationPin method End");
  		return INPUT;	
    }
    
    
     /** Method For Get Current Date With "yyyy-MM-dd" Date Format*/
     
    
    private java.util.Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
		}
		return currentDate; 
	} 
    
    
    /* * Method For Change Password According OTP*/
     
    public String changePassword() throws Exception {
    	logger.warn("USER AUDIT: "+currentdate+" changePassword method Start");		
    userList = passwordOTPManager.loadUserByUsername(OTPusername);
    if( (userList!=null) && !(userList.isEmpty())){
    	userOTP = userList.get(0);

   if(verificationCode!=null || (!(verificationCode.trim().equals(""))) ){
     passwordOTPData=passwordOTPManager.checkUserOTP(userOTP.getId(),verificationCode); 
    if(passwordOTPData !=null && (!(passwordOTPData.isEmpty())) && passwordOTPData.get(0) !=null ){
     passwordOTP = (PasswordOTP) passwordOTPData.get(0);
   	 if((!(verificationCode.equals(passwordOTP.getOtpCode())))){
   		String key = "Invalid verification pin. Please check your pin and try again.";
		saveMessage(getText(key)); 
		logger.warn("USER AUDIT: "+currentdate+" changePassword method end");	
  		return INPUT; 
   	 }
   	 if(newPassword==null || newPassword.trim().equals("")){
   		String key = "Invalid New Password.";
		saveMessage(getText(key)); 
		logger.warn("USER AUDIT: "+currentdate+" changePassword method end");	
 		return INPUT; 
   	 } 
   	if(rePassword==null || rePassword.trim().equals("")){
   		String key = "Invalid Confirm Password.";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: "+currentdate+" changePassword method end");	
 		return INPUT; 
   	 } 
   	if(!(newPassword.trim().equals(rePassword.trim()))){
   		String key = "Please enter the same password in the 'Confirm Password' field as in the 'New Password' field";
		saveMessage(getText(key)); 
		logger.warn("USER AUDIT: "+currentdate+" changePassword method end");		
 	    return INPUT; 
   	 } 
   	 else
   	 { 
   		Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
		if ("true".equals(getRequest().getParameter("encryptPass"))	&& (encrypt != null && encrypt)) {
			String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);
			if (algorithm == null) {
				log.debug("assuming testcase, setting algorithm to 'SHA'");
				algorithm = "SHA";
	         }
	        newPassword = StringUtil.encodePassword(newPassword, algorithm); 
	        userOTP.setConfirmPassword(newPassword);
	        userOTP.setPassword(newPassword);
	        userOTP.setAccountExpired(false);
	        userOTP.setAccountLocked(false);
	        userOTP.setCredentialsExpired(false);
	        userOTP.setPasswordReset(false);
		    company= companyManager.findByCorpID(userOTP.getCorpID()).get(0);
		    Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString())); 
   	        try{ 
   		       if(userOTP.getPwdexpiryDate()==null || compareWithDate.after(userOTP.getPwdexpiryDate())){
   		    	userOTP.setPwdexpiryDate(compareWithDate);
       	       }else{
       	    	userOTP.setPwdexpiryDate(userOTP.getPwdexpiryDate());
       	       }
	          }catch(Exception ex)
	          {
	        	  userOTP.setPwdexpiryDate(compareWithDate);
		        ex.printStackTrace();
	          }
   	        userOTP.setUpdatedBy(userOTP.getUsername());
   	        userOTP.setUpdatedOn(new Date());
	        userManager.saveUser(userOTP);
	        passwordOTP.setVerifiedOTPCode(verificationCode);
	        passwordOTP=passwordOTPManager.save(passwordOTP); 
	        
	        logger.warn("USER AUDIT: "+currentdate+" changePassword method end");	
			 return SUCCESS;
	    }
		logger.warn("USER AUDIT: "+currentdate+" changePassword method end");		
 	    return INPUT; 
     }//else
    }else{
    	String key = "Invalid or expired Pin. Please generate another.";
        saveMessage(getText(key)); 
        logger.warn("USER AUDIT: "+currentdate+" changePassword method end");		
        return INPUT; 
    }
    }
    }
    String key = "Invalid Username or Pin. Please check and try again.";
    saveMessage(getText(key)); 
    logger.warn("USER AUDIT: "+currentdate+" changePassword method end");		
    return INPUT; 
    
    }
    
    
     
     /** Setter and Getter methods of all variables  */
     
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}  
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}  
	
	public List getPasswordOTPData() {
		return passwordOTPData;
	}
	public void setPasswordOTPData(List passwordOTPData) {
		this.passwordOTPData = passwordOTPData;
	}
	
	public void setPasswordOTPManager(PasswordOTPManager passwordOTPManager) {
		this.passwordOTPManager = passwordOTPManager;
	}
	
	public PasswordOTP getPasswordOTP() {
		return passwordOTP;
	} 
	public void setPasswordOTP(PasswordOTP passwordOTP) {
		this.passwordOTP = passwordOTP;
	}
	
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String getRePassword() {
		return rePassword;
	}
	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	} 
	
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	} 
	
	public SystemDefault getSystemDefault() {
		return systemDefault;
	}
	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}
	
	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	} 
	
    public String getOTPusername() {
		return OTPusername;
	} 
	public void setOTPusername(String oTPusername) {
		OTPusername = oTPusername;
	} 
	
	public String getBypass_sessionfilter() {
		return bypass_sessionfilter;
	} 
	public void setBypass_sessionfilter(String bypass_sessionfilter) {
		this.bypass_sessionfilter = bypass_sessionfilter;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	 
}