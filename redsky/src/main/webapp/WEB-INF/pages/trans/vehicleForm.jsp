<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "Vehicles" in Redsky.
 * @File Name	vehicleForm
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        1-Dec-2008
 * --%>



<%@ include file="/common/taglibs.jsp"%>   
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="vehicleDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='vehicleDetail.heading'/>"/>
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>

<style type="text/css">
/* collapse */

</style>  

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
    
<!-- Modification closed here -->
<script language="JavaScript">
$('input,select,textfield :visible').each(function (i) {
	$(this).attr('tabindex', i + 1);

	});
var links = document.getElementsByTagName( 'a' );
for( var i = 0, j =  links.length; i < j; i++ ) {
    links[i].setAttribute( 'tabindex', '-1' );
}
  </script>
<script type="text/javascript"> 

<sec-auth:authComponent componentId="module.script.form.agentScript">

	window.onload = function() { 
	 trap();
		
		
		}
 </sec-auth:authComponent>
 
 function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		      	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		        	 

		      }
		    }
		  }
		  
		  function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		
		else return true;
		}
// function to check the form submittion.

var form_submitted = false;

function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}

// End of funtion.

</script>  
<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
		
	if(document.forms['vehicleForm'].elements['vehicle.title'].value == "null" ){
		document.forms['vehicleForm'].elements['vehicle.title'].value="";
	}
	if(document.forms['vehicleForm'].elements['vehicle.inventory'].value == "null" ){
		document.forms['vehicleForm'].elements['vehicle.inventory'].value="";
	}
	var f = document.getElementById('vehicleForm'); 
		f.setAttribute("autocomplete", "off");
}
 </script>      
</script>  
 <script>
 
 // function for auto save.
 
function ContainerAutoSave(clickType){
progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	    var id1 = document.forms['vehicleForm'].elements['serviceOrder.id'].value;
		if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                noSaveAction = 'accountLineList.html?sid='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    noSaveAction = 'pricingList.html?sid='+id1;
    }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                noSaveAction = 'containers.html?id='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.billing'){
                noSaveAction = 'editBilling.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                noSaveAction = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	noSaveAction = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
	noSaveAction =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                noSaveAction = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.claims'){
                noSaveAction = 'claims.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                noSaveAction = 'cartons.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                noSaveAction = 'vehicles.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                noSaveAction = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.containers'){
                noSaveAction = 'containers.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                noSaveAction = 'editCustomerFile.html?id='+cidVal;
                }
                processAutoSave(document.forms['vehicleForm'], 'saveVehicle!saveOnTabChange.html', noSaveAction);
        }
	
	else{

    if(!(clickType == 'save')){
    var id1 = document.forms['vehicleForm'].elements['serviceOrder.id'].value;
    var jobNumber = document.forms['vehicleForm'].elements['serviceOrder.shipNumber'].value;

    if (document.forms['vehicleForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='vehicleDetail.heading'/>");
        if(agree){
            document.forms['vehicleForm'].action = 'saveVehicle!saveOnTabChange.html';
            document.forms['vehicleForm'].submit();
        }else{
            if(id1 != ''){

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
        }
        }
    }else{
    if(id1 != ''){

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }


if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['vehicleForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
	var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
    }
    }
}
}
}

// End of funtion.

// funtion to check the status of page.

function changeStatus(){
    document.forms['vehicleForm'].elements['formStatus'].value = '1';
}

// End of funtion.

</script>
<script language="JavaScript">

// function for validations.


	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	
	
// 	End of funtion.

// function for validations during the note creation.

	function notExists(){
	alert("The Vehicle information has not been saved yet.");
}



function calculate(weightUnit,volumeUnit) {

    var weightUnitKgs = document.forms['vehicleForm'].elements['vehicle.unit2'].value;
    var volumeUnitCbm = document.forms['vehicleForm'].elements['vehicle.unit3'].value ;
	var density=0;
	var factor = 0;
	if(volumeUnitCbm == 'Inches')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 0.0005787;
		}
		else
		{
			factor = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 1;
		}
		else
		{
			factor = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 0.0000353;
		}
		else
		{
			factor = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 35.3147;
		}
		else
		{
			factor = 1;
		}
	}
	
    var density = (document.forms['vehicleForm'].elements['vehicle.width'].value * document.forms['vehicleForm'].elements['vehicle.height'].value * document.forms['vehicleForm'].elements['vehicle.length'].value );
		document.forms['vehicleForm'].elements['vehicle.volume'].value = Math.round(density*factor*1000)/1000;
		var volume = document.forms['vehicleForm'].elements['vehicle.volume'].value; 
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 	
	
	
	var http5 = getHTTPObject();

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	
	
	
	
	
	
	
	
	
	
	
	
	  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['vehicleForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['vehicleForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['vehicleForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['vehicleForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'vehicles.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['vehicleForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['vehicleForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrl(id)
	{
		location.href = "vehicles.html?id="+id;
	}
	
	
	
function goPrevChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['vehicleForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['vehicleForm'].elements['vehicle.id'].value;
	var url="vehiclePrev.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function goNextChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['vehicleForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['vehicleForm'].elements['vehicle.id'].value;
	var url="vehicleNext.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShipChild(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editVehicle.html?id='+results;
             }
       }     
function findCustomerOtherSOChild(position) {
 var sidNum=document.forms['vehicleForm'].elements['serviceOrder.id'].value;
 var soIdNum=document.forms['vehicleForm'].elements['vehicle.id'].value;
 var url="vehicleSO.html?ajax=1&decorator=simple&popup=true&sidNum=" + encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrlChild(id)
	{
		location.href = "editVehicle.html?id="+id;
	}
	
 
 
// 	End of funtion.	

</script>
</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
  <s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="vehicleForm" action="saveVehicle" onsubmit="return submit_form()" method="post" validate="true">   

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<s:hidden name="vehicle.id" value="%{vehicle.id}"/>   

<s:hidden name="id" value="<%=request.getParameter("id")%>"/>



<s:hidden name="vehicle.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="calOpener" value="notOPen" />
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
	<s:hidden name="vehicle.idNumber" /> 
	<s:hidden name="shipSize" />
    <s:hidden name="minShip" />
    <s:hidden name="countShip" />
    <s:hidden name="minChild" />
    <s:hidden name="maxChild" />
    <s:hidden name="countChild" />
	<s:hidden name="customerFile.id" />
	<s:hidden  name="vehicle.corpID" />
	<s:hidden name="vehicle.serviceOrderId" value="%{serviceOrder.id}"/>
	<s:hidden  name="vehicle.ugwIntId" />
	<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
	<s:hidden name="soId" value="%{serviceOrder.id}"/>
	<s:hidden id="countVehicleNotes" name="countVehicleNotes" value="<%=request.getParameter("countVehicleNotes") %>"/>
	<c:set var="countVehicleNotes" value="<%=request.getParameter("countVehicleNotes") %>" />
	
	<c:set var="from" value="<%=request.getParameter("from") %>"/>
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
	<s:hidden name="field" value="<%=request.getParameter("field") %>" />
	<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
	<c:set var="field1" value="<%=request.getParameter("field1") %>"/>


<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
    <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
    <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
    <c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
    <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
    <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
    			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
    <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
    <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.cartons' }">
    <c:redirect url="/cartons.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.vehicles' }">
    <c:redirect url="/vehicles.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.containers' }">
    <c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.servicepartners' }">
    <c:redirect url="/servicePartnerss.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
    <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>	
	<div id="Layer3" style="width:100%">
	<div id="newmnav" style="float:left;">     
         
            <ul>
            <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
              <li><a onclick="setReturnString('gototab.serviceorder');return ContainerAutoSave('none');"><span>S/O Details</span></a></li>
             </sec-auth:authComponent>
                <sec-auth:authComponent componentId="module.tab.container.billingTab">
              <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
              	<li><a onclick="setReturnString('gototab.billing');return ContainerAutoSave('none');" ><span>Billing</span></a></li>
              </sec-auth:authComponent>
              </sec-auth:authComponent>
                 <sec-auth:authComponent componentId="module.tab.container.accountingTab">
              <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			     <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		          <li><a onclick="setReturnString('gototab.accounting');return ContainerAutoSave('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose> 
		     </sec-auth:authComponent>
		     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		     <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		          <li><a onclick="setReturnString('gototab.newAccounting');return ContainerAutoSave('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose> 
		     </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
     		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>		     
		       <sec-auth:authComponent componentId="module.tab.container.forwardingTab"> 
              <li id="newmnav1" style="background:#FFF"><a class="current" onclick="setReturnString('gototab.containers'); return ContainerAutoSave('none');"><span>Forwarding</span></a></li>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.container.domesticTab">
              <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
              <li><a onclick="setReturnString('gototab.domestic');return  ContainerAutoSave('none');"><span>Domestic</span></a></li>
              </c:if>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	            <c:if test="${serviceOrder.job =='INT'}">
	             <li><a onclick="setReturnString('gototab.domestic');return  ContainerAutoSave('none');"><span>Domestic</span></a></li>
	            </c:if>
	           </sec-auth:authComponent> 
              <sec-auth:authComponent componentId="module.tab.container.statusTab">
              <li><a onclick="setReturnString('gototab.status');return  ContainerAutoSave('none');"><span>Status</span></a></li>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.container.ticketTab">
              <li><a onclick="setReturnString('gototab.ticket');return ContainerAutoSave('none');"><span>Ticket</span></a></li>
              </sec-auth:authComponent>
              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
               <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
              <li><a onclick="setReturnString('gototab.claims');return  ContainerAutoSave('none');"><span>Claims</span></a></li>
              </sec-auth:authComponent>
              </configByCorp:fieldVisibility>
               <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			      <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
              <li><a onclick="setReturnString('gototab.customerfile');return ContainerAutoSave('none');"><span>Customer File</span></a></li>
             </sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.container.reportTab">
              <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Billing&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
     		 <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
    		</sec-auth:authComponent>
    		<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
      			 <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
  			 </sec-auth:authComponent>
            </ul>
        </div>
        <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td> 
  		<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>
         <div class="spn">&nbsp;</div>
 
        </div>
        
 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
    <div id="Layer4" style="width:727px">
 <div id="newmnav">   
 <ul>
   <li><a onclick="setReturnString('gototab.containers');return ContainerAutoSave('none');"><span>SS Container</span></a></li>
  <li><a onclick="setReturnString('gototab.cartons');return ContainerAutoSave('none');"  ><span>Piece Count</span></a></li>
  <li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.vehicles');return ContainerAutoSave('none');"   class="current" ><span>Vehicle</span></a></li>
  <li><a onclick="setReturnString('gototab.servicepartners');return ContainerAutoSave('none');"  ><span>Routing</span></a></li>
   <sec-auth:authComponent componentId="module.tab.container.consigneeInstructionsTab">
  <li><a href="editConsignee.html?sid=${serviceOrder.id}"><span>Consignee Instructions</span></a></li>
 </sec-auth:authComponent>
   
   <c:if test="${countBondedGoods >= 0}" >
  <li><a href="customs.html?id=${serviceOrder.id}"><span>Customs</span></a></li>
  </c:if>
  
  </ul>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty vehicle.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${vehicle.id > minChild}" >
  		<a><img align="middle" onclick="goPrevChild();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${vehicle.id == minChild}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<td width="20px" align="left">
  		<c:if test="${vehicle.id < maxChild}" >
  		<a><img align="middle" onclick="goNextChild();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${vehicle.id == maxChild}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countChild != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSOChild(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="SO Vehicle List" title="SO Vehicle List" /></a> 
		</c:if>
		<c:if test="${countChild == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>

<div class="spn">&nbsp;</div>

</div>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%;">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="width:97%;">
				<tbody>	 
<tr>
<td align="right" class="listwhitetext">Status</td>
 <c:set var="isStatusFlag" value="false"/>
 <c:if test="${vehicle.status}">
 <c:set var="isStatusFlag" value="true"/>
 </c:if>
 <td align="left" width="20px" valign="bottom"><s:checkbox key="vehicle.status" value="${isStatusFlag}" onchange="statusValidation(this);"   fieldValue="true"  tabindex="3" /></td>
</tr> 					
            <tr>
 <td align="right" class="listwhitetext"><fmt:message key="vehicle.year"/></td>
<td colspan="2" align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="vehicleyear" name="vehicle.year" cssStyle="width:105px;"  maxlength="4" onchange="onlyNumeric(this); isNumeric(this);" tabindex="" /></td>
<td align="right" class="listwhitetext">Valuation</td>
<td colspan="1" align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.valuation" cssStyle="width:105px;"  maxlength="10"  tabindex="" /></td>
<td align="right" class="listwhitetext">Valuation&nbsp;Currency</td>
<td colspan="2" align="left" class="listwhitetext"><s:select key="vehicle.valuationCurrency"  headerKey="" headerValue=""  cssClass="list-menu"   list="%{valuationCurrencyList}"  cssStyle="width:110px" tabindex="" /></td>
<sec-auth:authComponent componentId="module.tab.container.customerFileTab">
		<c:if test="${empty vehicle.id}">
		<td width="9%" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
		</c:if>
		<c:if test="${not empty vehicle.id}">
		<c:choose>
			<c:when test="${countVehicleNotes == '0' || countVehicleNotes == '' || countVehicleNotes == null }">
			<td width="9%" align="right"><img id="countVehicleNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${vehicle.id }&notesId=${vehicle.id }&noteFor=Vehicle&subType=Vehicle&imageId=countVehicleNotesImage&fieldId=countVehicleNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${vehicle.id }&notesId=${vehicle.id }&noteFor=Vehicle&subType=Vehicle&imageId=countVehicleNotesImage&fieldId=countVehicleNotes&decorator=popup&popup=true',755,500);" ></a></td>
		</c:when>
		<c:otherwise>
			<td width="9%" align="right"><img id="countVehicleNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${vehicle.id }&notesId=${vehicle.id}&noteFor=Vehicle&subType=Vehicle&imageId=countVehicleNotesImage&fieldId=countVehicleNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${vehicle.id }&notesId=${vehicle.id }&noteFor=Vehicle&subType=Vehicle&imageId=countVehicleNotesImage&fieldId=countVehicleNotes&decorator=popup&popup=true',755,500);" ></a></td>
		</c:otherwise>
		</c:choose> 
	</c:if>
</sec-auth:authComponent>
</tr>

<tr>
<td align="right" class="listwhitetext">Vehicle Type</td>
<td colspan="2" align="left" class="listwhitetext"><s:select  id="vehicleType" cssClass="list-menu" key="vehicle.vehicleType" headerKey="" headerValue="" list="%{vehicleTypeList}"   cssStyle="width:110px" onchange="changeStatus();" tabindex="" /></td>

<td align="right" class="listwhitetext"><fmt:message key="vehicle.licNumber"/></td>
<td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-text" name="vehicle.licNumber" cssStyle="width:105px;"  maxlength="10" tabindex="" /></td>

<td width="" align="right" class="listwhitetext"><fmt:message key="container.containerNumber"/></td>
<td colspan="2" align="left" class="listwhitetext"><s:select  id="cntnrNumber" cssClass="list-menu" key="vehicle.cntnrNumber" headerKey="" headerValue="" list="%{containerNumberList}"   cssStyle="width:110px" onchange="changeStatus();" tabindex="" /></td>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="vehicle.make"/></td>
<td colspan="2" align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.make" cssStyle="width:105px;"  maxlength="10"  tabindex="" /></td>
<td align="right" class="listwhitetext"><fmt:message key="vehicle.serial"/></td>
<td colspan="2" align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.serial" cssStyle="width:105px;"  maxlength="20" tabindex="" /></td>
</tr>

<tr>
  <td align="right" class="listwhitetext"><fmt:message key="vehicle.model"/></td>
<td colspan="2" align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.model" cssStyle="width:105px;"   maxlength="20" tabindex="" /></td>
<td align="right" class="listwhitetext">Engine&nbsp;#</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.engineNumber" cssStyle="width:165px;"  maxlength="30" tabindex="" /></td>
 </tr>

<tr>
<td align="right" class="listwhitetext">Type</td>
<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-text" cssStyle="text-align:left;width:105px;" name="vehicle.type"   maxlength="45"  tabindex="" /></td>
<td align="right" class="listwhitetext"><fmt:message key="vehicle.cylinders"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:105px;" name="vehicle.cylinders" maxlength="3" onchange="onlyNumeric(this);" tabindex="" /></td>
</tr>

<tr>
<td width="" align="right" class="listwhitetext"><fmt:message key="vehicle.classEPA"/></td>
<td colspan="2" align="left" class="listwhitetext"><s:select  id="classEPA" cssClass="list-menu" key="vehicle.classEPA"  list="%{EPAClass}"  headerKey="" headerValue="" cssStyle="width:110px" onchange="changeStatus();" tabindex="" /></td>
<td align="right" class="listwhitetext"><fmt:message key="vehicle.proNumber"/></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" name="vehicle.proNumber" cssStyle="width:105px;"  maxlength="10" tabindex="" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="vehicle.color"/></td>
<td  colspan="2"align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.color" cssStyle="width:105px;"  maxlength="10" onkeydown="return onlyCharsAllowed(event)" tabindex="" /></td>
<td align="right" class="listwhitetext" width="14%"><fmt:message key="vehicle.titleNumber"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="vehicle.titleNumber" cssStyle="width:165px;"  maxlength="30" tabindex="" /></td>
 </tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="vehicle.doors"/></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:105px;" name="vehicle.doors" size="15"  maxlength="3" onchange="onlyNumeric(this); isNumeric(this);" tabindex="" /></td>
<td align="right" class="listwhitetext" ><fmt:message key="vehicle.title"/></td>
		<c:if test="${not empty vehicle.title}">
			<td colspan="2"><s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vehicle.title"/></s:text>
			<s:textfield cssClass="input-text" id="title" name="vehicle.title" value="%{customerFileMoveDateFormattedValue}" readonly="true" cssStyle="width:65px;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex=""/>
			
			<img id="title_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
			</td>
	</c:if>
	<c:if test="${empty vehicle.title}">
			<td colspan="2"><s:textfield cssClass="input-text" id="title" name="vehicle.title"  cssStyle="width:65px;" maxlength="11" onkeydown="onlyDel(event,this)" readonly="true" tabindex=""/>
			
			<img id="title_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
			</td>
	</c:if>
</tr>


</tbody>
</table>

<table width="100%" class="colored_bg" >
<tr><td colspan="8" class="vertlinedata"></td></tr>
<tr><td>
<table width="50%">
<tr><td width="17%" align="right" class="listwhitetextAcct"><fmt:message key="vehicle.weight"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:105px;" name="vehicle.weight" size="8" maxlength="10" onchange="onlyFloat(this);" tabindex="" /></td>
<td align="right" class="listwhitetextAcct" ><fmt:message key='labels.units'/></td>
<td width="" class="listwhitetextAcct" colspan="2"><s:radio name="vehicle.unit1" list="%{weightunits}" onchange="changeStatus();" tabindex="" /></td>

</tr>
<tr>
<td align="right" class="listwhitetextAcct"><fmt:message key="vehicle.length"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text"  cssStyle="text-align:right;width:105px;" name="vehicle.length" size="8" maxlength="10" onchange="onlyFloat(this);" tabindex="" /></td>
<td align="right" class="listwhitetextAcct" ><fmt:message key='labels.lenunit'/></td>
<td width="" class="listwhitetextAcct" colspan="2"><s:select cssClass="list-menu"   cssStyle="width:85px;" name="vehicle.unit3" list="%{lengthunits}" onchange="changeStatus();" tabindex="" /></td>
 
</tr>
<tr>
<td align="right" class="listwhitetextAcct"><fmt:message key="vehicle.width"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text"  cssStyle="text-align:right;width:105px;" name="vehicle.width" size="8" maxlength="10" onchange="onlyFloat(this);"  tabindex="" /></td>

</tr>
<tr>
<td align="right" class="listwhitetextAcct"><fmt:message key="vehicle.height"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text"  cssStyle="text-align:right;width:105px;" name="vehicle.height" size="8" maxlength="10" onchange="onlyFloat(this);" tabindex="" /></td>
 
</tr>
<tr>
<td align="right" class="listwhitetextAcct"><fmt:message key="vehicle.volume"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:105px;" name="vehicle.volume" size="8" maxlength="10" onchange="onlyFloat(this);" tabindex="" /></td>

<td width="50px" align="right" class="listwhitetextAcct"><fmt:message key='labels.volunit'/></td>
<td width="" class="listwhitetextAcct"><s:select cssClass="list-menu"  cssStyle="width:85px;" name="vehicle.unit2" list="%{volumeunits}" onchange="changeStatus();" tabindex="" /></td>
<td align="left">
  
 <input type="button" class="cssbutton" style="width:70px; height:24px"  name="calc" value="Calculate" onclick="calculate('vehicle.unit3','vehicle.unit2');" tabindex="" />

 </td> 
</tr>
<tr>

</tr>
<tr></tr>
<tr></tr>	
</table>
</td>
</tr>
</table>					
</tbody>
	</table>									
	</div>
	<div class="bottom-header"><span></span></div>
	</div>
	</div> 	
					<table>
					<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='vehicle.createdOn'/></td>
						<fmt:formatDate var="vehicleCreatedOnFormattedValue" value="${vehicle.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="vehicle.createdOn" value= "${vehicleCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${vehicle.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vehicle.createdBy' /></td>
						<c:if test="${not empty vehicle.id}">
								<s:hidden name="vehicle.createdBy"/>
								<td><s:label name="createdBy" value="%{vehicle.createdBy}"/></td>
							</c:if>
							<c:if test="${empty vehicle.id}">
								<s:hidden name="vehicle.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='vehicle.updatedOn'/></td>
						<fmt:formatDate var="vehicleUpdatedOnFormattedValue" value="${vehicle.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="vehicle.updatedOn"  value="${vehicleUpdatedOnFormattedValue}"/>
						<td><fmt:formatDate value="${vehicle.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty vehicle.id}">
							<s:hidden name="vehicle.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{vehicle.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty vehicle.id}">
							<s:hidden name="vehicle.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>					
			</div>	
					
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" />   
         <s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px" onmousemove="myDate();"/>

<c:if test="${not empty vehicle.id}">    
        
        </c:if>   
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>     
      </s:form>   
  
<script type="text/javascript">  
	/*<sec-auth:authComponent componentId="module.button.container.addButton">
		changeCalOpenarvalue();
	</sec-auth:authComponent> 
 
	 
	if(document.forms['vehicleForm'].elements['calOpener'].value=='notOPen')
	{
		
		for(i=0;i<=500;i++)
			{
					document.forms['vehicleForm'].elements[i].readOnly =true;
					document.forms['vehicleForm'].elements[i].className = 'input-textUpper';
						
			}
		
	}
  Form.focusFirstElement($("vehicleForm"));  */ 
</script>  
<script type="text/javascript">

function statusValidation(targetElementStatus){
	var massage="";
	if(targetElementStatus.checked==false){
		massage="Are you sure you wish to deactivate this row?"
	}else{
		massage="Are you sure you wish to activate this row?"
	}  
	var agree=confirm(massage);
	if (agree){
		
	}else{
		if(targetElementStatus.checked==false){
			document.forms['vehicleForm'].elements['vehicle.status'].checked=true;
			}else{
				document.forms['vehicleForm'].elements['vehicle.status'].checked=false;
			}
	}
}


	var fieldName = document.forms['vehicleForm'].elements['field'].value;

	var fieldName1 = document.forms['vehicleForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['vehicleForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['vehicleForm'].elements[fieldName1].className = 'rules-textUpper';
	}
	
</script>

<script type="text/javascript">
	RANGE_CAL_1 = new Calendar({
	    inputField: "title",
	    dateFormat: "%d-%b-%y",
	    trigger: "title_trigger",
	    bottomBar: true,
	    animation:true,
	    onSelect: function() {                             
	        this.hide();
		}
	});
</script>
<script type="text/javascript">
	try{  	
  		document.getElementById('vehicleyear').focus();
  	}catch(e){}
</script>