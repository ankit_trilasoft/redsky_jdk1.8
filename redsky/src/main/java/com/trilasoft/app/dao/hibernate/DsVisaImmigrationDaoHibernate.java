package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsVisaImmigrationDao;
import com.trilasoft.app.model.DsVisaImmigration;

public class DsVisaImmigrationDaoHibernate  extends GenericDaoHibernate<DsVisaImmigration, Long> implements DsVisaImmigrationDao {
	
	public DsVisaImmigrationDaoHibernate() {
		super(DsVisaImmigration.class);
		
	}

	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsVisaImmigration where id = "+id+" ");
	}
	
}
