<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title>Tracking FileCabinet</title>   
    <meta name="heading" content="Tracking FileCabinet"/>  
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<script language="javascript" type="text/javascript">
  
function submitForUpload(){
	
  	if(document.forms['fileUploadForm'].elements['openfromdate'].value==''){
    	alert("Please enter the from date"); 
 		return false;
  	}
  	if(document.forms['fileUploadForm'].elements['opentodate'].value==''){
    	alert("Please enter the to date"); 
 		return false;
  	}
  	if(document.forms['fileUploadForm'].elements['companyCorpID'].value==''){
    	alert("Please select the company"); 
 		return false;
  	}
  	else{
		document.forms['fileUploadForm'].action = "trackingDocFileProcessing.html";
		document.forms['fileUploadForm'].submit();
  		return true;
   	}
}
 
</script>

<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:800px;
}
</style>
</head> 
 
<s:form  id="fileUploadForm" name="fileUploadForm" enctype="multipart/form-data" method="post" validate="true"> 
 <div id="Layer3" style="width:100%; margin:0px; padding:0px">
 <div id="newmnav">
 <ul>
 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>File Upload<img src="images/navarrow.gif" align="absmiddle"/></span></a></li> 

 	</ul> 
</div>
		<div class="spn spnSF">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round">
        <div class="top" style="margin-top:0px;!margin-top:5px;"><span></span></div>
        <div class="center-content"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
			<tr>		  		
		  		<td align="right" width="100px" class="listwhitetext" style="padding-bottom:5px">From Date<font color="red" size="1">* &nbsp;</font></td> 
		  		<td><s:textfield cssClass="input-text" id="openfromdate" name="openfromdate" onkeydown="return onlyDel(event,this)" value="%{openfromdate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="openfromdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> 
				<td align="right" class="listwhitetext" style="padding-bottom:5px">To Date<font color="red" size="1">* &nbsp;</font></td> 
		  		<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:textfield cssClass="input-text" id="opentodate" onkeydown="return onlyDel(event,this)" name="opentodate" value="%{opentodate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="opentodate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> 
			    <td align="right"  class="listwhitetext" style="padding-bottom:5px">Company<font color="red" size="1">* &nbsp;</font></td> 
			    <td><s:select name='companyCorpID'cssStyle="width:100px" cssClass="list-menu" headerKey="" headerValue="" list="%{corpIDList}"/></td>  
	  		</tr>
			<tr><td height="20px"></td></tr>  
			<tr>
		    <td height="20px"></td> 
			 <td colspan="5">
             <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " align="top" value="Extract" onclick="return submitForUpload();" />	  	 				           	
			 <s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" /> 
			 </td>       
            </tr>  
            <tr><td height="20px"></td></tr>         
		</tbody>
		</table> 
	    </div>
       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       </div>  
  
</s:form>
<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality(); 
</script>

