// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`claim` CHANGE COLUMN `responsibleAgent` `responsibleAgentCode` VARCHAR(8),
ADD COLUMN `responsibleAgentName` VARCHAR(255) DEFAULT '' AFTER `remediationComment`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `isDefaultContactPerson` bit(1) DEFAULT b'0' ;

ALTER TABLE `redsky`.`defaultaccountline` ADD COLUMN `uploaddataflag` bit(1) DEFAULT '' AFTER `estExpVatPercent`;