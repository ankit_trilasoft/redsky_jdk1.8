package com.trilasoft.app.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;

import com.trilasoft.app.dao.CompanyDao;
import com.trilasoft.app.model.Company;

public class CompanyDaoHibernate extends GenericDaoHibernate<Company, Long> implements CompanyDao {

	Logger logger = Logger.getLogger(CompanyDaoHibernate.class);
	
	public CompanyDaoHibernate() {
		super(Company.class);
	}
	
	public List<Company> findByCorpID(String corpId) {
		return getSession().createQuery("from Company where corpID='"+corpId+"'").list();
	}

	public String findByCompanyDivis(String corpID){
		List results = null;
		try {
			Query query =  getSession().createSQLQuery("select companyDivisionFlag from company where corpID=:corpID").addScalar("companyDivisionFlag", Hibernate.STRING).setParameter("corpID", corpID, Hibernate.STRING) ;
			results = query.list();
			if(results!=null && results.size()>0)
				return (String) results.get(0);
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		// This should be handled properly
		return null;
	}

	public List getcorpID() {
		try {
			return getHibernateTemplate().find("select corpID from Company where status is true");
		} catch (DataAccessException e) {
			logger.error("Error executing query ",e);
		}
		return null;
	}

	public List getcompanyName(String corpID) {
		
		try {
			if(corpID.equals("TSFT")){
				return getHibernateTemplate().find("select companyName from Company order by 1");
			}else{ 
				return getHibernateTemplate().find("select companyName from Company where corpID='"+corpID+"'");
				}
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return null;
	}

	public List getDateDiffExp(String username, String sessionCorpID, Date passwordExp) {
		List getDiff = null;
		try {
			getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF(now(),'"+passwordExp+"')").list();
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return getDiff;
	}

	public List getDateDiffAlert(String username, String sessionCorpID, Date passwordExp, String pwdExpDate) {
		List getDiff = null;
		try {
			getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+passwordExp+"',date_format('"+pwdExpDate+"','%y-%m-%d'))").list();
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return getDiff;
	}

	public boolean findTracInternalUser(String sessionCorpID) {
		
		try {
			List list=getHibernateTemplate().find("select count(*) from Company where corpID='"+sessionCorpID+"' and tracInternalUser =true");
			
			String tracInternal= list.get(0).toString();
			if(tracInternal.equals("0")){
				return false;
			}else{
				return true;	
			}
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return false;
		
	}
	
	public String findAutomaticLinkup(String externalCorpID){
		String chk = "" ;
		try {
			List l1  = this.getSession().createSQLQuery("select if(automaticLinkup,'T','F') from company where corpid='"+externalCorpID+"' ").list();
			if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
				chk = l1.get(0).toString().trim();
			}
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return chk;
	}
	
	public List findCorpIdList(){
		List list = null;
		try {
			list = this.getSession().createSQLQuery("select concat(s.corpid,'#',if(s.companyCode is null or s.companyCode='','NA',s.companyCode)) from systemdefault s, company c, partnerpublic p where s.corpid=c.corpid and c.corpid=c.parentCorpId and s.companycode=p.partnercode and utsNumber='Y' ").list();
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return list;
	}
	
	public String defaultBillingRate(String tempCorpId){
		String dRate="";
		try {
			List list = this.getSession().createSQLQuery("select defaultBillingRate from company  where corpid='"+tempCorpId+"'").list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null){
				dRate =list.get(0).toString().trim();
			}
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return dRate;
		
	}

	public boolean getCompanySurveyFlag(String corpid) {
		boolean tempChk = false;
		List tempList = this.getSession().createSQLQuery("select surveyLinking from company  where corpid='"+corpid+"'").list();
		if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null   ){
			tempChk = (Boolean) tempList.get(0);
		}
		return tempChk;
	}

	public void updateStorageBillingStatus(String sessionCorpID, boolean storageBillingStatus) {
		getHibernateTemplate().bulkUpdate("update Company set storageBillingStatus =" + storageBillingStatus + "  where corpID=?", sessionCorpID);
		
	}
	
	public String getcorpIDName(String companyName) {
		String companyNam="";
		try {
			List list = this.getSession().createSQLQuery("select corpID from company where companyName='"+companyName+"'").list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null){
				companyNam =list.get(0).toString().trim();
			}
		} catch (Exception e) {
			logger.error("Error executing query ",e);
		}
		return companyNam;
	}
	
public List getcompanyListBycompanyView(String sessionCorpID,String companyName, String countryID,String billGroup, String status) {
	List companys = null;	
	try {
		if (status.equals("Active")) {
				companys = getHibernateTemplate().find(
						"from Company where companyName like '%"
								+ companyName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND countryID like '%"
								+ countryID.replaceAll("'", "''").replaceAll(
										":", "''")
							
										+"%' AND   rskyBillGroup like '%"
								+ billGroup.replaceAll("'", "''").replaceAll(
										":", "''")
								 + "%' AND status = true ");
			} else if(status.equals("Inactive")){
				companys = getHibernateTemplate().find(
						"from Company where companyName like '%"
								+ companyName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND countryID like '%"
								+ countryID.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND  rskyBillGroup like '%"
								+ billGroup.replaceAll("'", "''").replaceAll(
										":", "''")
								 + "%'  AND status = false");
			}
			else if(status.equals("All"))
			{
				
					companys = getHibernateTemplate().find(
							"from Company where companyName like '%"
									+ companyName.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND countryID like '%"
									+ countryID.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND  rskyBillGroup like '%"
									+ billGroup.replaceAll("'", "''").replaceAll(
											":", "''")
									 + "%' ");
				}
			else				
			{
				companys = getHibernateTemplate().find(
						"from Company where companyName like '%"
								+ companyName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND countryID like '%"
								+ countryID.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND  rskyBillGroup like '%"
								+ billGroup.replaceAll("'", "''").replaceAll(
										":", "''")
								 + "%' ");
			}
		 }
		catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}

		return companys;
	}

public List getcorpIDList() {
	try {
		return getHibernateTemplate().find("select corpID from Company where cmmdmmAgent is true");
	} catch (DataAccessException e) {
		logger.error("Error executing query ",e);
	}
	return null;
}
public List findCorpIdAgentList(){
	List list = null;
	try {
		list = this.getSession().createSQLQuery("select concat(s.corpid,'#',if(s.companyCode is null or s.companyCode='','NA',s.companyCode)) from systemdefault s, company c, partnerpublic p where s.corpid=c.corpid and c.corpid=c.parentCorpId and s.companycode=p.partnercode ").list();
	} catch (Exception e) {
		logger.error("Error executing query ",e);
	}
	return list;
}

public int updateVendorExtractDate(String sessionCorpID, String userName) {
	return getHibernateTemplate().bulkUpdate("update Company set vendorExtractDate = now(), updatedBy='"+userName+"',updatedOn = now()  where corpID=?", sessionCorpID);
}

public int updateBillToExtractDate(String sessionCorpID, String userName) {
	return getHibernateTemplate().bulkUpdate("update Company set billToExtractDate = now(), updatedBy='"+userName+"',updatedOn = now()  where corpID=?", sessionCorpID);
}
}
