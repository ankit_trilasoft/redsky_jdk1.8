package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DriverCommissionDao;
import com.trilasoft.app.model.DriverCommissionPlan;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.service.DriverCommissionManager;
import com.trilasoft.app.service.VanLineManager;

public class DriverCommissionManagerImpl extends GenericManagerImpl<DriverCommissionPlan, Long> implements DriverCommissionManager {
	DriverCommissionDao driverCommissionDao;
	public DriverCommissionManagerImpl(DriverCommissionDao driverCommissionDao){
		  super(driverCommissionDao);
		this.driverCommissionDao=driverCommissionDao;
	}
	
	public List getPlanList(String corpId){
		return driverCommissionDao.getPlanList(corpId);
	}
	public List getcontractList(String corpId){
		return driverCommissionDao.getcontractList(corpId);
	}
	public List findChargeList(String corpId){
		return driverCommissionDao.findChargeList(corpId);
	}
	public List getChargeList(String charge,String description,String corpId){
		return driverCommissionDao.getChargeList(charge,description,corpId);
		
	}
	public List getPlanListValue(String planValue,String corpId){
		return driverCommissionDao.getPlanListValue(planValue,corpId);
	}
	public List getChargeListValue(String planValue,String chargeValue,String corpId){
		return driverCommissionDao.getChargeListValue(planValue,chargeValue,corpId);
	}
	public List getCommissionPlanList(String corpId){
		return driverCommissionDao.getCommissionPlanList(corpId);
	}
	public List getChargeList(String corpId){
		return driverCommissionDao.getChargeList(corpId);
	}
	public List getCommissionSearchPlanList(String plan,String charge,String description,String contract,String corpId){
		return driverCommissionDao.getCommissionSearchPlanList( plan, charge, description, contract, corpId);
	}
	public void updateCommissionPercent(BigDecimal percent,String planAmount,String plan,String contract,String charge,String corpId){
		driverCommissionDao.updateCommissionPercent( percent,planAmount, plan,contract, charge, corpId);
	}
	 public int delDriverCommission(String delCommissionId){
		 return driverCommissionDao.delDriverCommission(delCommissionId);
		 
	 }
	 public int checkPlan(String plan,String charge,String contract,String corpId){
		 return driverCommissionDao.checkPlan(plan, charge,contract,corpId);
		 
	 }
	public List getChargedriverList(String charge,String corpId){
			return driverCommissionDao.getChargedriverList(charge,corpId);
			
		}
	
}
