package com.trilasoft.app.webapp.action;

import java.util.*;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsRelocationExpenseManagement;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsRelocationExpenseManagementManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
public class DsRelocationExpenseManagementAction extends BaseAction implements Preparable {
	

	private Long id;
	private String sessionCorpID;
	private DsRelocationExpenseManagement dsRelocationExpenseManagement;
	private DsRelocationExpenseManagementManager dsRelocationExpenseManagementManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countForDSRelocationExpenseManagementNotes;	
	private NotesManager notesManager;    
    private Map<String, String> paidStatus;
    private RefMasterManager refMasterManager;
	private String gotoPageString;
	private String validateFormNav;
    
		
	
	
	public DsRelocationExpenseManagementAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	
public void prepare() throws Exception { 
		
	}
private String shipSize;
private String minShip;
private String countShip;
private Map<String, String> relocationServices;
	
public String edit() { 
	if (id != null) {
		trackingStatus=trackingStatusManager.get(id);
		serviceOrder=serviceOrderManager.get(id);
    	customerFile = serviceOrder.getCustomerFile();
    	miscellaneous = miscellaneousManager.get(id); 
    	dsRelocationExpenseManagement = dsRelocationExpenseManagementManager.get(id);
	} else {
		dsRelocationExpenseManagement = new DsRelocationExpenseManagement(); 
		dsRelocationExpenseManagement.setCreatedOn(new Date());
		dsRelocationExpenseManagement.setUpdatedOn(new Date());
		dsRelocationExpenseManagement.setServiceOrderId(serviceOrderId);
	} 	
	shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
	relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");	
	paidStatus = refMasterManager.findByParameter(sessionCorpID, "PAIDSTATUS");	       		
	getNotesForIconChange();
	return SUCCESS;
}
	


public String save() throws Exception {
	
	boolean isNew = (dsRelocationExpenseManagement.getId() == null);  
			if (isNew) { 
				dsRelocationExpenseManagement.setCreatedOn(new Date());
	        }
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	dsRelocationExpenseManagement.setCorpID(sessionCorpID);
	    	dsRelocationExpenseManagement.setUpdatedOn(new Date());
	    	dsRelocationExpenseManagement.setUpdatedBy(getRequest().getRemoteUser()); 
	    	dsRelocationExpenseManagementManager.save(dsRelocationExpenseManagement);
	    	if(!(validateFormNav=="OK"))
	    	{
		    		    		
		    	    	String key = (isNew) ? "dsRelocationExpenseManagement.added" : "dsRelocationExpenseManagement.updated";
	    
		    saveMessage(getText(key));
	    	}
			paidStatus = refMasterManager.findByParameter(sessionCorpID, "PAIDSTATUS");			    
		    getNotesForIconChange();
			return SUCCESS;
			
		
}
@SkipValidation
public String saveOnTabChange() throws Exception {
	validateFormNav = "OK";
	String s = save();
	return SUCCESS;
}
@SkipValidation
public String getNotesForIconChange() {
	List noteDS = notesManager.countForDSRelocationExpenseManagementNotes(serviceOrder.getShipNumber());
	
	if (noteDS.isEmpty()) {
		countForDSRelocationExpenseManagementNotes = "0";
	} else {
		countForDSRelocationExpenseManagementNotes = ((noteDS).get(0)).toString();
	}
	paidStatus = refMasterManager.findByParameter(sessionCorpID, "PAIDSTATUS");		
	return SUCCESS;
			
}



   	/**
 * @return the countForDSRelocationExpenseManagementNotes
 */
public String getCountForDSRelocationExpenseManagementNotes() {
	return countForDSRelocationExpenseManagementNotes;
}

/**
 * @param countForDSRelocationExpenseManagementNotes the countForDSRelocationExpenseManagementNotes to set
 */
public void setCountForDSRelocationExpenseManagementNotes(
		String countForDSRelocationExpenseManagementNotes) {
	this.countForDSRelocationExpenseManagementNotes = countForDSRelocationExpenseManagementNotes;
}

/**
 * @return the customerFile
 */
public CustomerFile getCustomerFile() {
	return customerFile;
}

/**
 * @param customerFile the customerFile to set
 */
public void setCustomerFile(CustomerFile customerFile) {
	this.customerFile = customerFile;
}

/**
 * @return the dsRelocationExpenseManagement
 */
public DsRelocationExpenseManagement getDsRelocationExpenseManagement() {
	return dsRelocationExpenseManagement;
}

/**
 * @param dsRelocationExpenseManagement the dsRelocationExpenseManagement to set
 */
public void setDsRelocationExpenseManagement(
		DsRelocationExpenseManagement dsRelocationExpenseManagement) {
	this.dsRelocationExpenseManagement = dsRelocationExpenseManagement;
}

/**
 * @return the id
 */
public Long getId() {
	return id;
}

/**
 * @param id the id to set
 */
public void setId(Long id) {
	this.id = id;
}

/**
 * @return the miscellaneous
 */
public Miscellaneous getMiscellaneous() {
	return miscellaneous;
}

/**
 * @param miscellaneous the miscellaneous to set
 */
public void setMiscellaneous(Miscellaneous miscellaneous) {
	this.miscellaneous = miscellaneous;
}

/**
 * @return the paidStatus
 */
public Map<String, String> getPaidStatus() {
	return paidStatus;
}

/**
 * @param paidStatus the paidStatus to set
 */
public void setPaidStatus(Map<String, String> paidStatus) {
	this.paidStatus = paidStatus;
}

/**
 * @return the serviceOrder
 */
public ServiceOrder getServiceOrder() {
	return serviceOrder;
}

/**
 * @param serviceOrder the serviceOrder to set
 */
public void setServiceOrder(ServiceOrder serviceOrder) {
	this.serviceOrder = serviceOrder;
}

/**
 * @return the serviceOrderId
 */
public Long getServiceOrderId() {
	return serviceOrderId;
}

/**
 * @param serviceOrderId the serviceOrderId to set
 */
public void setServiceOrderId(Long serviceOrderId) {
	this.serviceOrderId = serviceOrderId;
}

/**
 * @return the sessionCorpID
 */
public String getSessionCorpID() {
	return sessionCorpID;
}

/**
 * @param sessionCorpID the sessionCorpID to set
 */
public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}

/**
 * @return the trackingStatus
 */
public TrackingStatus getTrackingStatus() {
	return trackingStatus;
}

/**
 * @param trackingStatus the trackingStatus to set
 */
public void setTrackingStatus(TrackingStatus trackingStatus) {
	this.trackingStatus = trackingStatus;
}

/**
 * @param customerFileManager the customerFileManager to set
 */
public void setCustomerFileManager(CustomerFileManager customerFileManager) {
	this.customerFileManager = customerFileManager;
}

/**
 * @param dsRelocationExpenseManagementManager the dsRelocationExpenseManagementManager to set
 */
public void setDsRelocationExpenseManagementManager(
		DsRelocationExpenseManagementManager dsRelocationExpenseManagementManager) {
	this.dsRelocationExpenseManagementManager = dsRelocationExpenseManagementManager;
}

/**
 * @param miscellaneousManager the miscellaneousManager to set
 */
public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
	this.miscellaneousManager = miscellaneousManager;
}

/**
 * @param notesManager the notesManager to set
 */
public void setNotesManager(NotesManager notesManager) {
	this.notesManager = notesManager;
}

/**
 * @param refMasterManager the refMasterManager to set
 */
public void setRefMasterManager(RefMasterManager refMasterManager) {
	this.refMasterManager = refMasterManager;
}

/**
 * @param serviceOrderManager the serviceOrderManager to set
 */
public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
	this.serviceOrderManager = serviceOrderManager;
}

/**
 * @param trackingStatusManager the trackingStatusManager to set
 */
public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
	this.trackingStatusManager = trackingStatusManager;
}

/**
 * @return the gotoPageString
 */
public String getGotoPageString() {
	return gotoPageString;
}

/**
 * @param gotoPageString the gotoPageString to set
 */
public void setGotoPageString(String gotoPageString) {
	this.gotoPageString = gotoPageString;
}

/**
 * @return the validateFormNav
 */
public String getValidateFormNav() {
	return validateFormNav;
}

/**
 * @param validateFormNav the validateFormNav to set
 */
public void setValidateFormNav(String validateFormNav) {
	this.validateFormNav = validateFormNav;
}

/**
 * @return the countShip
 */
public String getCountShip() {
	return countShip;
}

/**
 * @param countShip the countShip to set
 */
public void setCountShip(String countShip) {
	this.countShip = countShip;
}

/**
 * @return the minShip
 */
public String getMinShip() {
	return minShip;
}

/**
 * @param minShip the minShip to set
 */
public void setMinShip(String minShip) {
	this.minShip = minShip;
}

/**
 * @return the relocationServices
 */
public Map<String, String> getRelocationServices() {
	return relocationServices;
}

/**
 * @param relocationServices the relocationServices to set
 */
public void setRelocationServices(Map<String, String> relocationServices) {
	this.relocationServices = relocationServices;
}

/**
 * @return the shipSize
 */
public String getShipSize() {
	return shipSize;
}

/**
 * @param shipSize the shipSize to set
 */
public void setShipSize(String shipSize) {
	this.shipSize = shipSize;
}


}
