package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.model.WorkTicket;
  
public interface ItemsJbkEquipDao extends GenericDao<ItemsJbkEquip, Long> {   
    public List<ItemsJbkEquip> findByType(String type); 
    public List<ItemsJbkEquip> findById(Long id);
    public List<ItemsJbkEquip> findByDescript(String descript);
    public List<ItemsJbkEquip> findByDescript(String type, String descript);
    public List<ItemsJbkEquip> findSection(Long ticket, String itemType);
    public void genrateInvoiceNumber(Long id3);
    public int updateWorkTicketCartons(Long ticket);
    public List findByShipNum(String shipNumber,String corpId);
    public String updateItemsJbkResource(String resourceListServer,String categoryListServer, String qtyListServer, String returnedListServer, String actQtyListServer, String costListServer, String actualListServer,	String sessionCorpID, WorkTicket workTicket, String fromDate, String toDate, String hubId, String chkLimit,String estListServer,String comListServer, String updatedBy);
    public OperationsResourceLimits getOperationsResourceHub(String warehouse, String sessionCorpID);
    public List usedEstQuanitityList(String category, String resource,	String fromDate, String toDate, String hubId, String service, String sessionCorpID,Long ticket);
    public List getDailyResourceLimit(String category, String resource, String fromDate, String toDate, String hubId, String sessionCorpID);
    public String checkIsExist(Long itemsJbkId, String category, String resource, String sessionCorpID);
    public List<ItemsJbkEquip> checkResourceDuplicate(String shipNumber,String corpId, String category, String resource);
    
    public List getResourceAutoComplete(String corpId,String tempDescript,String tempCategory);   
    public int findDayTemplate(String shipNumber, String corpID);
    public void deleteAll(String shipNumber, String corpID, List day);
    public Map<String, List> findByShipNumAndDay(String shipNumber,String corpId,String dayFocus);
    public Map<String, Map> findByShipNumAndDayMap(String shipNumber,String corpId,String dayFocus);
    public void saveDateAjax(String shipNum,String day, String beginDt, String endDt);
    public List<ItemsJbkEquip> getResource(String corpId, String shipNum,String day);
    public void updateTicketTransferStatus(String shipNumber,Set set);
    public void updateAccountLineForCreatePricing(String shipNumber,String category,String resource,String day);
    public void deleteQtyBased(String shipNumber,String corpID);
    public String findByShipNumAndCategoryNull(String shipNumber,String corpID,String day);
    public List findByShipNumAndDay(String shipNumber, String day);
    public void deleteItemsJbkResource(ItemsJbkEquip jbkEquip,Boolean isDelete);
    public List findByShipNumAndTicket(String shipNumber,String ticket);
    public String findDateByShipNum(String shipNumber);
    public List findActualCost(String sessionCorpID, String shipnum, Long ticket, String itemType);
    public String getMaxSeqNumber(String sessionCorpID);
    public List findAllResourcesAjax(String shipNumber,String sessionCorpID);
    public Map<String, String> getItemsjequip(String sessionCorpID);
}