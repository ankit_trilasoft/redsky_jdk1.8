<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Partner Rate matrix - Contract Rates Details</title>
    <meta name="heading" content="Partner Rate matrix - Contract Rates Details"/>
    <style><%@ include file="/common/calenderStyle.css"%>
    div#errorMessages { background-color:#FDC8C1;border:2px solid red;text-align:left; color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;margin:10px auto;
		padding:3px;text-align:center;	vertical-align:bottom;width:800px;}
    </style>
   
<script language="javascript" type="text/javascript">
	var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->

<script>
function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
		return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	
		}
function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
var http3 = getHTTPObject();

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	
function findAgentTariffContract()
	{
	 var corpid = document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountCompany'].value;
	 var url="findAgentTariffContract.html?ajax=1&decorator=simple&popup=true&corpid="+encodeURI(corpid);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse49;
     http3.send(null);
     
}

function handleHttpResponse49()
        {
 	    if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                 res = results.replace("[",'');
                res = res.replace("]",''); 
                res = res.split("@");
                targetElement = document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.contract'];
				targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
				
					  if(res[i] == ''){
					  document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.contract'].options[i].text = '';
					  document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.contract'].options[i].value = '';
					}
					else
					{
					  document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.contract'].options[i].value=res[i].trim();
					  document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.contract'].options[i].text=res[i].trim();
					}
             }
       }
   } 
function validation(){ 
 if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountCompany'].value==''){
   alert("Discount Company is a required field ")
   return false; 
 } else if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.effectiveDate'].value==''){
   alert("Effective Date is a required field ")
   return false;  
 }else if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountBaseRate'].value==''){
  alert("Discount Base Rate is a required field ")
  return false; 
} else if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountAddlCharges'].value==''){
  alert("Discount Addl Charges is a required field ")
  return false; 
}else if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountHaulingCharges'].value==''){
  alert("Discount Hauling Charges is a required field ")
  return false; 
}else if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountAutoHandling'].value==''){
  alert("Discount Auto Handling is a required field ")
  return false; 
} else if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountSITCharges'].value==''){
  alert("Discount SIT Charges is a required field ")
  return false; 
}else{
return  calcDate();
}
} 
   
  function calcDate()
  {
 
   var date2 = document.forms['partnerRateGridContractsForm'].elements['partnerRateGrid.effectiveDate'].value; 
   var date1 = document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.effectiveDate'].value; 
   //var date3 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value;
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var month1 = 0;
   var month2 = month;
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
       month1 = 1;
   }
   else if(month == 'Feb')
   {
       month = "02";
       month1 = 2;
   }
   else if(month == 'Mar')
   {
       month = "03"
       month1 = 3;
   }
   else if(month == 'Apr')
   {
       month = "04"
       month1 = 4;
   }
   else if(month == 'May')
   {
       month = "05"
       month1 = 5;
   }
   else if(month == 'Jun')
   {
       month = "06"
       month1 = 6;
   }
   else if(month == 'Jul')
   {
       month = "07"
       month1 = 7;
   }
   else if(month == 'Aug')
   {
       month = "08"
       month1 = 8;
   }
   else if(month == 'Sep')
   {
       month = "09"
       month1 = 9;
   }
   else if(month == 'Oct')
   {
       month = "10"
       month1 = 10;
   }
   else if(month == 'Nov')
   {
       month = "11"
       month1 = 11;
   }
   else if(month == 'Dec')
   {
       month = "12";
       month1 = 12;
   }
   var year1 = '20'+year;
   //var day1 = getLastDayOfMonth(month1,year1); 
   //var date4 = day1+"-"+month2+"-"+year; 
   var finalDate = month+"-"+day+"-"+year; 
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2; 
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  //document.forms['accountLineForms'].elements['accountLine.storageDays'].value = daysApart; 
  var flag = 0;  
  if(daysApart<0)
  { 
   alert("You can not enter Effective Date less than Partner Rate matrix  tariff Effective Date."); 
   return false; 
  }else {
  <c:if test="${partnerRateGrid.publishTariff==true}"> 
  return true; 
  </c:if>
  <c:if test="${partnerRateGrid.publishTariff!=true}"> 
  return true; 
  </c:if>
  }
  
}
function calcMaxDate()
  { 
   var date1 = document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.effectiveDate'].value; 
   var date2=new Date(); 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1]; 
   var year = mySplitResult[2];
   if(month == 'Jan')
   {
       month = "01"; 
   }
   else if(month == 'Feb')
   {
       month = "02"; 
   }
   else if(month == 'Mar')
   {
       month = "03" 
   }
   else if(month == 'Apr')
   {
       month = "04" 
   }
   else if(month == 'May')
   {
       month = "05" 
   }
   else if(month == 'Jun')
   {
       month = "06" 
   }
   else if(month == 'Jul')
   {
       month = "07" 
   }
   else if(month == 'Aug')
   {
       month = "08" 
   }
   else if(month == 'Sep')
   {
       month = "09" 
   }
   else if(month == 'Oct')
   {
       month = "10" 
   }
   else if(month == 'Nov')
   {
       month = "11" 
   }
   else if(month == 'Dec')
   {
       month = "12"; 
   }
   var year1 = '20'+year;  
   var finalDate = month+"-"+day+"-"+year1; 
   date1 = finalDate.split("-"); 
   var eDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]); 
   var  sDate= new Date(); 
   sDate.setDate(sDate.getDate() + 60); 
   var daysApart = Math.round((eDate-sDate)/86400000); 
   var y =sDate.getFullYear();
   var d =sDate.getDate()+1;
   var m =""
   m=eval(sDate.getMonth())+1;
   m =""+m; 
   var m2 ="";
   if(m == "1")
   {
       m2 = "Jan"; 
   }
   else if(m == "2")
   {
       m2 = "Feb"; 
   }
   else if(m == "3")
   {
       m2 = "Mar" 
   }
   else if(m == "4")
   {
       m2 = "Apr" 
   }
   else if(m == "5")
   {
      m2= "May" 
   }
   else if(m == "6")
   {
       m2 = "Jun" 
   }
   else if(m == "7")
   {
      m2= "Jul" 
   }
   else if(m == "8")
   {
       m2 = "Aug" 
   }
   else if(m == "9")
   {
       m2 = "Sep" 
   }
   else if(m == "10")
   {
       m2= "Oct" 
   }
   else if(m == "11")
   {
       m2 = "Nov" 
   }
   else if(m == "12")
   {
       m2= "Dec"; 
   }
   var finalDate1 = d+"-"+m2 +"-"+y;  
   if(daysApart<0)
   {
    alert("The Earliest effective date you can choose is "+finalDate1+", Please correct the effective date before you can save the rate. ");  
    return false; 
   } else {
      return true; 
   }
}


 function checkFloat(temp,message)
   {
    var check='';  
    var i;
    var dotcheck=0;
	var s = temp.value; 
	var fieldName = temp.name; 
	var length=eval(s.length); 
	if(s.indexOf(".") == -1){
	dotcheck=eval(s.length)
	}else{
	dotcheck=eval(s.indexOf(".")-1)
	} 
	var count = 0;
	var countArth = 0;
   for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if(c == '-')
        {
        	countArth = countArth+1
        } 
        if((length==1)&&(c=='-'))
       	{
       		check='Invalid'; 
       	} 
        if((i!=0)&&(c=='-'))
       	{
       		check='Invalid'; 
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && ((c != '-') || (countArth>'1'))) 
        {  	
        	check='Invalid'; 
        }
    } 
    if(check=='Invalid'){
    document.forms['partnerRateGridContractsForm'].elements[fieldName].select(); 
    alert("Invalid data in Discount\\Premium " +message); 
    document.forms['partnerRateGridContractsForm'].elements[fieldName].value='0';
    } else{
    s=Math.round(s*100)/100; 
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['partnerRateGridContractsForm'].elements[fieldName].value=value;
    }
    var fieldValue=eval(value); 
    if(fieldValue<-100 || fieldValue>200){
	document.forms['partnerRateGridContractsForm'].elements[fieldName].select(); 
	alert("Discount\\Premium "+message+" should be between -100 to 200%")
	document.forms['partnerRateGridContractsForm'].elements[fieldName].value='0'; 
	} 
    return true;
}   	
</script> 	    
</head>

<s:form id="partnerRateGridContractsForm" action="savePartnerRateGridContracts.html?btntype=yes" method="post" validate="true">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<s:hidden name="partnerId" value="${partnerPublic.id}"/>
<s:hidden name="partnerRateGridID" value="${partnerRateGrid.id}"/>
<s:hidden name="partnerRateGridContracts.id" />
<s:hidden name="id" value="${partnerRateGridContracts.id}"/>
<s:hidden name="partnerRateGridContracts.partnerRateGridID" value="${partnerRateGrid.id}" /> 

<div id="layer1" >
<div id="newmnav"> 
       </div><div class="spn" >&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
   <table  cellspacing="1" cellpadding="1" border="0" >
					<tbody>
						<tr>
							<td width="" height="30px"><s:label label="  "/></td>
							<td class="listwhitetext"  >Partner Rate matrix</td>
							<td width="50px"><s:hidden name="partnerPublic.id" />
							    <s:textfield cssClass="input-textUpper" name="partnerPublic.partnerCode" required="true" size="7" readonly="true"/>
							</td>
							<td>
							    <s:textfield cssClass="input-textUpper" name="partnerPublic.lastName" required="true" size="35" readonly="true"/>
							</td>
							<td width="5px"></td> 
						    <td align="right" width="50px" class="listwhitetext">Metro City</td>
							<td align="left"><s:textfield cssClass="input-textUpper"  name="partnerRateGrid.metroCity" cssStyle="width:150px" maxlength="50"  readonly="true"/></td>
						    <td width="5px"></td>
						    <td align="right" class="listwhitetext">Effective&nbsp;Date</td>
							 <c:if test="${not empty partnerRateGrid.effectiveDate}"> 
					           <s:text id="partnerRateGridEffectiveDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGrid.effectiveDate"/></s:text>
				                 <td><s:textfield id="effectiveDate1" name="partnerRateGrid.effectiveDate" value="%{partnerRateGridEffectiveDate}" readonly="true" cssClass="input-textUpper" size="8" tabindex="4"/>
				                </td> 
				             </c:if>
				             <c:if test="${empty partnerRateGrid.effectiveDate}"> 
					             <td><s:textfield id="effectiveDate1" name="partnerRateGrid.effectiveDate"  readonly="true" cssClass="input-textUpper" size="8" tabindex="4"/>
					             </td>
					         </c:if>
					         <td width="5px"></td>
					         <td align="right" width="90px"  class="listwhitetext"><fmt:message key="partnerRateGrid.tariffApplicability"/></td>
							 <td><s:textfield cssClass="input-textUpper" name="partnerRateGrid.tariffApplicability" required="true" cssStyle="width:150px" readonly="true"/></td>
							
					  </tr>
					  <tr><td style="!height:25px;" ></td></tr>
					</tbody>
				</table>				
	</div><div class="bottom-header" style="margin-top:30px;"><span></span></div></div>
<div style="height:10px;"></div>
<div id="Layer1"  style="width:100%;">
		<div id="newmnav" style="float: left;"> 
		<ul> 
		 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Contract Rates Details</span></a></li>
		 <li ><a  href="partnerRateGridContractsList.html?partnerId=${partnerPublic.id}&partnerRateGridID=${partnerRateGrid.id}"><span>Contract Rates List</span></a></li> 
	    </ul>  
		</div>
 <div class="spn">&nbsp;</div>    
      </div>
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">	
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="180">Discount Company<font color="red" size="2">*</font></td>
<td align="left" ><s:select list="%{discountCompanyList}" cssClass="list-menu" cssStyle="width:72px" headerKey="" headerValue="" name="partnerRateGridContracts.discountCompany" onchange="findAgentTariffContract();"/></td>
 <td align="right" width="180px" class="listwhitetext">Discount Effective&nbsp;Date<font color="red" size="2">*</font></td>
 <c:if test="${not empty partnerRateGridContracts.effectiveDate}"> 
	 <s:text id="partnerRateGridEffectiveDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGridContracts.effectiveDate"/></s:text>
	 <td><s:textfield id="effectiveDate" name="partnerRateGridContracts.effectiveDate" value="%{partnerRateGridEffectiveDate}" readonly="true" cssClass="input-text" cssStyle="width:65px" />
	 <img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	 </td> 
 </c:if>
 <c:if test="${empty partnerRateGridContracts.effectiveDate}"> 
	 <td><s:textfield id="effectiveDate" name="partnerRateGridContracts.effectiveDate"  readonly="true" cssClass="input-text" cssStyle="width:65px" />
	 <img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
    </td>
 </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext">Contract</td>
<c:if test="${empty partnerRateGridContracts.id}">
<td align="left" ><s:select list="%{contractList}" value="%{multipleContractList}" cssClass="list-menu" cssStyle="width:200px;height:50px" headerKey="" headerValue="" name="partnerRateGridContracts.contract" multiple="true" /></td>
<td class="listwhitetext" width="240px" align="left" colspan="2">
   * Use Control + mouse to select multiple Contracts
</td> 
</c:if>
<c:if test="${not empty partnerRateGridContracts.id}">
<td align="left" ><s:select list="%{contractList}"  cssClass="list-menu" cssStyle="width:200px" headerKey="" headerValue="" name="partnerRateGridContracts.contract"  /></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext">Discount\Premium Base Rate<font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"   name="partnerRateGridContracts.discountBaseRate" maxlength="7" size="9" onkeydown="return onlyFloatNumsAllowed(event)" cssStyle="text-align:right" onchange="checkFloat(this,'Base Rate')"/>%</td>
<td align="right"  class="listwhitetext">Discount\Premium Addl Charges<font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"   name="partnerRateGridContracts.discountAddlCharges" maxlength="7" size="9" onkeydown="return onlyFloatNumsAllowed(event)" cssStyle="text-align:right" onchange="checkFloat(this,'Addl Charges')"/>%</td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Discount\Premium Hauling Charges<font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"   name="partnerRateGridContracts.discountHaulingCharges" maxlength="7" size="9" onkeydown="return onlyFloatNumsAllowed(event)" cssStyle="text-align:right" onchange="checkFloat(this,'Hauling Charges')"/>%</td>
<td align="right"  class="listwhitetext">Discount\Premium Auto Handling<font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"   name="partnerRateGridContracts.discountAutoHandling" maxlength="7" size="9" onkeydown="return onlyFloatNumsAllowed(event)" cssStyle="text-align:right"  onchange="checkFloat(this,'Auto Handling')"/>%</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Discount\Premium SIT Charges<font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"   name="partnerRateGridContracts.discountSITCharges" maxlength="7" size="9" onkeydown="return onlyFloatNumsAllowed(event)" cssStyle="text-align:right" onchange="checkFloat(this,'SIT Charges')"/>%</td>
</tr>
<tr><td height="20px"></td></tr>
</tbody></table>
<table  border="0" width="62%"><tr><td  align="center"  class="listwhitetext"><b>Note: Discounts are entered with a minus sign, premiums with no signs.</b></td></tr></table>
</div><div class="bottom-header" style="margin-top:50px;"><span></span></div></div></div></div>

<table style="">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${partnerRateGridContracts.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="partnerRateGridContracts.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${partnerRateGridContracts.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty partnerRateGridContracts.id}">
								<s:hidden name="partnerRateGridContracts.createdBy"/>
								<td ><s:label name="createdBy" value="%{partnerRateGridContracts.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partnerRateGridContracts.id}">
								<s:hidden name="partnerRateGridContracts.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${partnerRateGridContracts.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerRateGridContracts.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${partnerRateGridContracts.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty partnerRateGridContracts.id}">
							<s:hidden name="partnerRateGridContracts.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{partnerRateGridContracts.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partnerRateGridContracts.id}">
							<s:hidden name="partnerRateGridContracts.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
		
<table><tr><td>   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return validation();"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
	</td></tr></table>
</s:form>

<script type="text/javascript">
if(document.forms['partnerRateGridContractsForm'].elements['partnerRateGridContracts.discountCompany'].value!=''){
  findAgentTariffContract()
  }
</script>

<script type="text/javascript">
	RANGE_CAL_1 = new Calendar({
        inputField: "effectiveDate",
        dateFormat: "%d-%b-%y",
        trigger: "effectiveDate_trigger",
        bottomBar: true,
        animation:true,
        onSelect: function() {                             
            this.hide();
  		  }
	});
</script>
    