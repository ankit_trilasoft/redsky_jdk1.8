<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
       <title>
        <fmt:message key="BillingList.title"/></title> 
    	<meta name="heading" content="<fmt:message key='BillingList.heading'/>"/> 
 </head>
 
 <s:form id="billingList" action="" method="post" validate="true">
 	<display:table name="billingList" requestURI="" defaultsort="2" id="billingList" class="table" pagesize="50" style="width:100%;margin-top: 1px;" >  
    <display:column property="shipNumber" escapeXml="true" sortable="true" titleKey="billing.shipNumber" style="width: 10%; cursor: default;"/>
    <display:column property="firstName" escapeXml="true" sortable="true" titleKey="billing.firstName" style="width: 10%; cursor: default;"/>
    <display:column property="lastName" escapeXml="true" sortable="true" titleKey="billing.lastName" style="width: 10%; cursor: default;"/>
    <display:column property="createdBy" escapeXml="true" sortable="true" titleKey="billing.createdBy" style="width: 10%; cursor: default;"/>
    <display:column property="createdOn" escapeXml="true" sortable="true" titleKey="billing.createdOn" style="width: 10%; cursor: default;"/>
    </display:table>  
    </s:form>    
          