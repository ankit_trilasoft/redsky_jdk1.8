package com.trilasoft.app.webapp.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.SignUpService;
import org.appfuse.webapp.action.BaseAction;
import org.omg.PortableInterceptor.SUCCESSFUL;

import com.trilasoft.app.dao.hibernate.dto.AgentMasterDTO;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PricingControl;
import com.trilasoft.app.model.PricingControlDetails;
import com.trilasoft.app.model.PricingFreight;
import com.trilasoft.app.model.RefFreightRates;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PricingControlDetailsManager;
import com.trilasoft.app.service.PricingControlManager;
import com.trilasoft.app.service.PricingFreightManager;
import com.trilasoft.app.service.RefFreightRatesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class PricingControlAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private PricingControl pricingControl;
	private PricingControlManager  pricingControlManager;
	private RefMasterManager refMasterManager;
	private List pricingControlList;
	private Map<String, String> ocountry;
	private Map<String, String> ostates;
	private List mode;
	private Map<String, String> pkmode;
	private Map<String, String> EQUIP;
	private Long serviceOrderId;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private SignUpService signUpService;
	private String popup;
	private List addressList;
	private String tariffApplicability;
	 private String weightType;
	private CustomerFileManager customerFileManager;
	private String hitFlag;
	private List feedbackRating;
	private PricingControlDetailsManager pricingControlDetailsManager;
	private PricingControlDetails pricingControlDetails;
	private PricingControlDetails pricingControlDetailsOrigin;
	private PricingControlDetails pricingControlDetailsDestination;
	private PricingControlDetails pricingControlDetailsFreight;
	private PricingFreightManager pricingFreightManager;
	private PricingFreight pricingFreight;
	private Boolean costElementFlag;	
	private RefFreightRates refFreightRates;
	private List agentFreightList;
	private Partner originPartner;
	private Partner destinationPartner;
	
	private PartnerManager partnerManager;
	private String FIDI;
	private String OMNI;
	private String minimumFeedbackRating;
	private String FAIM;
	private String ISO9002;
	private String ISO27001;
	private String ISO1400;
	private String RIM;
	private String DOS;
	private String GSA;
	private String Military;
	
	private String originFIDI;
	private String originOMNI;
	private String originMinimumFeedbackRating;
	private String originFAIM;
	private String originISO9002;
	private String originISO27001;
	private String originISO1400;
	private String originRIM;
	private String originDOS;
	private String originGSA;
	private String originMilitary;
	private String originMarkup;
	private String DTHC;
	
	private String destinationFIDI;
	private String destinationOMNI;
	private String destinationMinimumFeedbackRating;
	private String destinationFAIM;
	private String destinationISO9002;
	private String destinationISO27001;
	private String destinationISO1400;
	private String destinationRIM;
	private String destinationDOS;
	private String destinationGSA;
	private String destinationMilitary;
	private String destinationMarkup;
	
	private String countryCode;
	private String latitude;
	private String longitude;
	private List contract;
	private  Map<String,String> currency;
	 private  Map<String,String> scope;
	private String parentAgent;
	private ChargesManager chargesManager;
	private String fromEmailAddress;
	private String emailMessage;
	private String userEmail;
	private Company company;
	private CompanyManager companyManager;
	private boolean accountLineAccountPortalFlag=false; 
	
	private List recentTariffsList;
private EmailSetupManager  emailSetupManager;
	
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	
	public PricingControlAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        userEmail=user.getEmail();
        parentAgent=user.getParentAgent();
        this.sessionCorpID = user.getCorpID();
        //company = companyManager.findByCorpID(sessionCorpID).get(0);
	}
	
	public String getComboList(String corpId){
		ocountry = signUpService.findCountry("COUNTRY");
		mode = refMasterManager.findByParameters(corpId, "MODE");
		pkmode = refMasterManager.findByParameter(corpId, "PKMODE");
		pkmode.remove("RORO");
		pkmode.put("LVCO", "Liftvans (FCL)");
		pkmode.put("BKBLK", "Liftvans (LCL)");
		pkmode.put("LOOSE", "Loose in container");
		EQUIP = refMasterManager.findByParameter(corpId, "EQUIP");
		feedbackRating= refMasterManager.findByParameters(corpId, "FEEDBACKRATING");
		contract=pricingControlManager.getContract(sessionCorpID);
		currency=refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
		scope=refMasterManager.findByParameter(corpId, "TARIFFSCOPE");
		costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
		return SUCCESS;
	 }
	
	private String modeName;
	public String getPackingMode(){
		if(modeName.equalsIgnoreCase("Air")){
			pkmode.remove("RORO");
			pkmode.remove("LVCO");
			pkmode.remove("BKBLK");
			pkmode.remove("LOOSE");
		}
		return SUCCESS;
	}

	@SkipValidation
	public String list(){
		getComboList(sessionCorpID);
		if(serviceOrderId!=null)
		{
			serviceOrder=serviceOrderManager.get(serviceOrderId);
			pricingControlList=pricingControlManager.getList(serviceOrder.getShipNumber());
		}else
		{
			pricingControlList=pricingControlManager.getAgentList(parentAgent, sessionCorpID);
		}
		return SUCCESS;
	}
	
	public String agentPricingAcceptedQuotes(){
		getComboList(sessionCorpID);
		pricingControlList=pricingControlManager.getAcceptedQuotes(sessionCorpID,parentAgent);
		hitFlag="2";
		return SUCCESS;
	}
	
	@SkipValidation
	public String agentPricing(){
		
		if(popup==null)
		{
			if (id != null)
			{
				pricingControl=pricingControlManager.get(id);
				ostates = customerFileManager.findDefaultStateList(pricingControl.getOriginCountry(), sessionCorpID);
			}else{
				pricingControl= new PricingControl();
				pricingControl.setContract("NO CONTRACT - INT");
				ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
			}
		}
		try{
		if(popup!=null)
		{
			if (id != null)
			{
				pricingControl=pricingControlManager.get(id);
				ostates = customerFileManager.findDefaultStateList(pricingControl.getOriginCountry(), sessionCorpID);
			}else{
				serviceOrder=serviceOrderManager.get(serviceOrderId);
				trackingStatus=trackingStatusManager.get(serviceOrderId);
				miscellaneous=miscellaneousManager.get(serviceOrderId);
				
				pricingControl= new PricingControl();
				ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
				pricingControl.setMode(serviceOrder.getMode());
				pricingControl.setPacking(serviceOrder.getPackingMode());
				pricingControl.setContainerSize(serviceOrder.getEquipment());
				pricingControl.setShipNumber(serviceOrder.getShipNumber());
				pricingControl.setWeight(serviceOrder.getActualNetWeight());
				pricingControl.setVolume(serviceOrder.getActualCubicFeet());
				if(trackingStatus.getLoadA()==null){
					pricingControl.setExpectedLoadDate(trackingStatus.getBeginLoad());
				}else{
					pricingControl.setExpectedLoadDate(trackingStatus.getLoadA());
				}
				pricingControl.setUnitType(miscellaneous.getUnit1());
			}
		}
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n\n exception---->"+ex);
		}
		getComboList(sessionCorpID);
		return SUCCESS;
	}
	private String navigation;
	public String saveAgentPricing(){
		String packing="";
		String tarrifApplicability="";
		String originPortsCode="";
		String destinationPortsCode="";
		getComboList(sessionCorpID);
		boolean isNew = (pricingControl.getId() == null);
		if(isNew)
		{
			String maxPricingId;
			try {
					List maxPricingIdCode = pricingControlManager.maxPricingIdCode(sessionCorpID);
					if(maxPricingIdCode.isEmpty()){
						maxPricingId = sessionCorpID+"1"+"P";
					}else{
						maxPricingId = sessionCorpID + (String.valueOf((Long.parseLong((maxPricingIdCode.get(0).toString()).substring(4, maxPricingIdCode.get(0).toString().length()-1)) + 1)))+"P";
					}
			} catch (Exception ex) {
				maxPricingId =sessionCorpID+"1"+"P";
			}
			pricingControl.setPricingID(maxPricingId);
			pricingControl.setCreatedBy(getRequest().getRemoteUser());
			pricingControl.setCreatedOn(new Date());
		}
		pricingControl.setParentAgent(parentAgent);
		pricingControl.setCorpID(sessionCorpID);
		pricingControl.setUpdatedBy(getRequest().getRemoteUser());
		pricingControl.setUpdatedOn(new Date());
		pricingControl.setFreightMarkUpControl(true);
		if(!pricingControl.getOriginLatitude().equalsIgnoreCase("")){
			pricingControl.setIsOrigin(true);
		}else{
			pricingControl.setIsOrigin(false);
		}
		if(!pricingControl.getDestinationLatitude().equalsIgnoreCase("")){
			pricingControl.setIsDestination(true);
		}else{
			pricingControl.setIsDestination(false);
		}
		if(!pricingControl.getOriginLatitude().equalsIgnoreCase("") && !pricingControl.getDestinationLatitude().equalsIgnoreCase("")){
			pricingControl.setIsFreight(true);
		}else{
			pricingControl.setIsFreight(false);
		}
		pricingControl=pricingControlManager.save(pricingControl);
		
		if(isNew)
		{
		if(pricingControl.getMode().equalsIgnoreCase("Air"))
		{
			packing="AIR";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("LOOSE") && pricingControl.getContainerSize().equalsIgnoreCase("20"))
		{
			packing="L20";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("LOOSE") && pricingControl.getContainerSize().equalsIgnoreCase("40"))
		{
			packing="L40";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("BKBLK") && pricingControl.getLclFclFlag().equalsIgnoreCase("lcl"))
		{
			packing="LCL";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("BKBLK") && pricingControl.getLclFclFlag().equalsIgnoreCase("fcl"))
		{
			packing="FCL";
		}
		try{
			boolean applyCompletemarkUp=true;
			if(pricingControl.getIsOrigin()==true && pricingControl.getIsDestination()==true){
				applyCompletemarkUp=pricingControlManager.checkMarkUp(pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(),pricingControl.getDestinationLongitude(),tarrifApplicability, pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(),packing, pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(),sessionCorpID,  pricingControl.getExpectedLoadDate(), pricingControl.getContract());
				//System.out.println("\n\n\n\n\n applyCompletemarkUp------>"+applyCompletemarkUp); 
			}
			try{
				if(pricingControl.getIsFreight()){
					tarrifApplicability="Freight";
					pricingControlManager.deletePricingFreightDetails(pricingControl.getId());
					agentFreightList=pricingControlDetailsManager.getFreightList(pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange(), pricingControl.getMode() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
					if(!agentFreightList.isEmpty()){
						Iterator  it =agentFreightList.iterator();
						while(it.hasNext()){
							Object[] row=(Object[])it.next();
							Long refFreightId=(Long.parseLong(row[0].toString()));
							String price=row[1].toString();
							PricingFreight pricingFreight=new PricingFreight();
							pricingFreight.setPricingControlID(pricingControl.getId());
							pricingFreight.setCorpID(sessionCorpID);
							pricingFreight.setReffreightId(refFreightId);
							pricingFreight.setFreightPrice(new BigDecimal(price));
							Double markup=0.0;
							List markUpList=pricingFreightManager.getMarkUpValue(new BigDecimal(price), pricingControl.getContract(),pricingControl.getMode());
							if(!markUpList.isEmpty()){
								String markupListString=markUpList.get(0).toString();
								String[] markupArray=markupListString.split("#");
								Double profitFlat=Double.parseDouble(markupArray[0]);
								Double profitPercent=Double.parseDouble(markupArray[1]);
								markup=Double.parseDouble(price)*(profitPercent/100.00) + profitFlat;
							}
							pricingFreight.setFreightMarkup(new BigDecimal(markup));
							pricingFreight.setFreightSelection(false);
							pricingFreight.setOriginPortCode(row[2].toString());
							pricingFreight.setDestinationPortCode(row[3].toString());
							pricingFreight.setCreatedBy(getRequest().getRemoteUser());
							pricingFreight.setCreatedOn(new Date());
							pricingFreight.setUpdatedBy(getRequest().getRemoteUser());
							pricingFreight.setUpdatedOn(new Date());
							pricingFreight=pricingFreightManager.save(pricingFreight);
						}
					}
				
				
				/*String originPortTemp=pricingControlDetailsManager.getMinimumFreightCode(pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getId(), pricingControl.getBaseCurrency(),pricingControl.getMode(), pricingControl.getContract(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
				String [] sArray=originPortTemp.split("#");
				String originPortCode=sArray[0];
				String destinationPortCode=sArray[1];*/
				
				String portsTempPerFreight=pricingControlDetailsManager.getPortsPerFreight(pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getId(), pricingControl.getBaseCurrency(),pricingControl.getMode(), pricingControl.getContract(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
				String [] s1Array=portsTempPerFreight.split("#");
				String originPortCodePerFreight=s1Array[0];
				String destinationPortCodePerFreight=s1Array[1];
				
				
				if(pricingControl.getIsOrigin())
				{
					tarrifApplicability="Origin";
					//	originPortCodePerFreight=originPortCodePerFreight.replaceAll(",", "','");
					List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), tarrifApplicability, pricingControl.getOriginCountry(), packing, originPortCodePerFreight, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(), pricingControl.getContract(),applyCompletemarkUp,"");
				}
				if(pricingControl.getIsDestination())
				{
					tarrifApplicability="Destination";
					//destinationPortCodePerFreight=destinationPortCodePerFreight.replaceAll(",", "','");
					List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), tarrifApplicability, pricingControl.getDestinationCountry(), packing, destinationPortCodePerFreight, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(), pricingControl.getContract(),applyCompletemarkUp,"");
				}
				hitFlag="1";
				return SUCCESS;
				}
			}catch(Exception ex){
				System.out.println("\n\n\n\n\n\n exception----->>>>"+ex);
				ex.printStackTrace();
				
			}	
				
			if(pricingControl.getIsOrigin())
			{
				tarrifApplicability="Origin";
				String originPortCodePerDistance="";
				if(pricingControl.getOriginPortRange()!=null){
					originPortCodePerDistance=pricingControlDetailsManager.getPortsCode(pricingControl.getOriginPortRange(), pricingControl.getMode(),pricingControl.getOriginCountry(),pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude());
				}
				else{
					originPortCodePerDistance=pricingControl.getOriginPOE();
				}
				List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), tarrifApplicability, pricingControl.getOriginCountry(), packing, originPortCodePerDistance, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(), pricingControl.getContract(),applyCompletemarkUp,"");
			}
			if(pricingControl.getIsDestination())
			{
				tarrifApplicability="Destination";
				String destinationPortCodePerDistance="";
				if(pricingControl.getDestinationPortRange()!=null){
					destinationPortCodePerDistance=pricingControlDetailsManager.getPortsCode(pricingControl.getDestinationPortRange(), pricingControl.getMode(),pricingControl.getDestinationCountry(),pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
				}
				else{
					destinationPortCodePerDistance=pricingControl.getDestinationPOE();
				}
				List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), tarrifApplicability, pricingControl.getDestinationCountry(), packing, destinationPortCodePerDistance, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(), pricingControl.getContract(),applyCompletemarkUp,"");
			}
		
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n\n exception----->>>>"+ex);
			ex.printStackTrace();
		}
		}else{
			
			if(pricingControl.getMode().equalsIgnoreCase("Air"))
			{
				packing="AIR";
			}
			if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("LOOSE") && pricingControl.getContainerSize().equalsIgnoreCase("20"))
			{
				packing="L20";
			}
			if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("LOOSE") && pricingControl.getContainerSize().equalsIgnoreCase("40"))
			{
				packing="L40";
			}
			if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("BKBLK") && pricingControl.getLclFclFlag().equalsIgnoreCase("lcl"))
			{
				packing="LCL";
			}
			if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("BKBLK") && pricingControl.getLclFclFlag().equalsIgnoreCase("fcl"))
			{
				packing="FCL";
			}
			try{
				
				boolean applyCompletemarkUp=true;
				if(pricingControl.getIsOrigin()==true && pricingControl.getIsDestination()==true){
					applyCompletemarkUp=pricingControlManager.checkMarkUp(pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(),pricingControl.getDestinationLongitude(),tarrifApplicability, pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(),packing, pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(),sessionCorpID,  pricingControl.getExpectedLoadDate(), pricingControl.getContract());
					//System.out.println("\n\n\n\n\n applyCompletemarkUp------>"+applyCompletemarkUp); 
				}
				
				try{
					if(pricingControl.getIsFreight()){
						tarrifApplicability="Freight";
						pricingControlManager.deletePricingFreightDetails(pricingControl.getId());
						agentFreightList=pricingControlDetailsManager.getFreightList(pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange(), pricingControl.getMode() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
						if(!agentFreightList.isEmpty()){
							Iterator  it =agentFreightList.iterator();
							while(it.hasNext()){
								Object[] row=(Object[])it.next();
								Long refFreightId=(Long.parseLong(row[0].toString()));
								String price=row[1].toString();
								PricingFreight pricingFreight=new PricingFreight();
								pricingFreight.setPricingControlID(pricingControl.getId());
								pricingFreight.setCorpID(sessionCorpID);
								pricingFreight.setReffreightId(refFreightId);
								pricingFreight.setFreightPrice(new BigDecimal(price));
								Double markup=0.0;
								List markUpList=pricingFreightManager.getMarkUpValue(new BigDecimal(price), pricingControl.getContract(),pricingControl.getMode());
								if(!markUpList.isEmpty()){
									String markupListString=markUpList.get(0).toString();
									String[] markupArray=markupListString.split("#");
									Double profitFlat=Double.parseDouble(markupArray[0]);
									Double profitPercent=Double.parseDouble(markupArray[1]);
									markup=Double.parseDouble(price)*(profitPercent/100.00) + profitFlat;
								}
								pricingFreight.setFreightMarkup(new BigDecimal(markup));
								pricingFreight.setFreightSelection(false);
								pricingFreight.setOriginPortCode(row[2].toString());
								pricingFreight.setDestinationPortCode(row[3].toString());
								pricingFreight.setCreatedBy(getRequest().getRemoteUser());
								pricingFreight.setCreatedOn(new Date());
								pricingFreight.setUpdatedBy(getRequest().getRemoteUser());
								pricingFreight.setUpdatedOn(new Date());
								pricingFreight=pricingFreightManager.save(pricingFreight);
							}
						}
						
						/*String originPortTemp=pricingControlDetailsManager.getMinimumFreightCode(pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getId(), pricingControl.getBaseCurrency(),pricingControl.getMode(), pricingControl.getContract(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
						String [] sArray=originPortTemp.split("#");
						String originPortCode=sArray[0];
						String destinationPortCode=sArray[1];*/
						
						String portsTempPerFreight=pricingControlDetailsManager.getPortsPerFreight(pricingControl.getOriginPOE(), pricingControl.getDestinationPOE(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getId(), pricingControl.getBaseCurrency(),pricingControl.getMode(), pricingControl.getContract(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
						String [] s1Array=portsTempPerFreight.split("#");
						String originPortCodePerFreight=s1Array[0];
						String destinationPortCodePerFreight=s1Array[1];
						
						if(pricingControl.getIsOrigin())
						{
							tarrifApplicability="Origin";
							pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
							//originPortCodePerFreight=originPortCodePerFreight.replaceAll(",", "','");
							List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), tarrifApplicability, pricingControl.getOriginCountry(), packing, originPortCodePerFreight, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp,"");
						}
						if(pricingControl.getIsDestination())
						{
							tarrifApplicability="Destination";
							pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
							//destinationPortCodePerFreight=destinationPortCodePerFreight.replaceAll(",", "','");
							List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), tarrifApplicability, pricingControl.getDestinationCountry(), packing, destinationPortCodePerFreight, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp,"");
						}
						hitFlag="1";
						return SUCCESS;
					}
				}catch(Exception ex){
					System.out.println("\n\n\n\n\n\n exception----->>>>"+ex);
					ex.printStackTrace();
				}	
	
				if(pricingControl.getIsOrigin())
				{
					tarrifApplicability="Origin";
					pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
					String originPortCodePerDistance="";
					if(pricingControl.getOriginPortRange()!=null){
						originPortCodePerDistance=pricingControlDetailsManager.getPortsCode(pricingControl.getOriginPortRange(), pricingControl.getMode(),pricingControl.getOriginCountry(),pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude());
					}
					else{
						originPortCodePerDistance=pricingControl.getOriginPOE();
					}
					//originPortCodePerFreight=originPortCodePerFreight.replaceAll(",", "','");
					List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), tarrifApplicability, pricingControl.getOriginCountry(), packing, originPortCodePerDistance, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp,"");
				}
				
				if(pricingControl.getIsDestination())
				{
					tarrifApplicability="Destination";
					pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
					String destinationPortCodePerDistance="";
					if(pricingControl.getDestinationPortRange()!=null){
						destinationPortCodePerDistance=pricingControlDetailsManager.getPortsCode(pricingControl.getDestinationPortRange(), pricingControl.getMode(),pricingControl.getDestinationCountry(),pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude());
					}
					else{
						destinationPortCodePerDistance=pricingControl.getDestinationPOE();
					}
					
					List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), tarrifApplicability, pricingControl.getDestinationCountry(), packing, destinationPortCodePerDistance, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp,"");
				}
			
			}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n\n exception----->>>>"+ex);
				ex.printStackTrace();
			}
		}
		
	
		if(pricingControl.getIsOrigin()){
			navigation="origin";
			
		}
		if(pricingControl.getIsDestination()){
			navigation="destination";
			
		}
		if(pricingControl.getIsOrigin() && pricingControl.getIsDestination()){
			navigation="origin";
			
		}
		hitFlag="1";
		if(popup==null)
		{
			//String key = (isNew) ? "Agent Pricing has been added successfully" : "Agent Pricing has been updated successfully";
			//saveMessage(getText(key));
		}
		return SUCCESS;
	}
	
	

	public String getAddressForPricing(){
		addressList=pricingControlManager.getAddress(serviceOrderId,tariffApplicability);
		return SUCCESS;
	}
	
	
	public String agentPricingSearch(){
		getComboList(sessionCorpID);
		boolean pricingID=(pricingControl.getPricingID() == null);
		boolean shipNumber = (pricingControl.getShipNumber() == null);
		//boolean tariffApplicability = (pricingControl.getTariffApplicability() == null);
		boolean packing = (pricingControl.getPacking() == null);
		boolean oCountry = (pricingControl.getOriginCountry() == null);
		boolean customerName = (pricingControl.getCustomerName() == null);
		
		pricingControlList=pricingControlManager.search(pricingControl.getPricingID(),pricingControl.getSequenceNumber(), "", pricingControl.getPacking(), "", pricingControl.getCustomerName(), parentAgent);
					
		return SUCCESS;
	}
	
	public String deleteAgentPricingList(){
		try{
		pricingControlManager.remove(id);
		hitFlag="1";
		list();
		}catch(Exception ex)
		{
			list();
		}
		return SUCCESS;
	}
	
	
	public String locatePartners() {
		return SUCCESS;
	}
	
	public String locatePartnersOrigin() {
		return SUCCESS;
	}
	
	public String locateFreight(){
		
		return SUCCESS;
	}
	
	public InputStream getLocateFreightDetails() {
		
		ByteArrayInputStream output = null;
		
		output= new ByteArrayInputStream(pricingControlManager.locateFreightDetails(pricingControl.getOriginPOE(),pricingControl.getDestinationPOE(),pricingControl.getContainerSize(), sessionCorpID).getBytes());
	
		return output;
	}
	
	public InputStream getPartnerList() {
		String packing="";
		String tarrifApplicability="";
		ByteArrayInputStream output = null;
		
				
		if(pricingControl.getIsDestination())
		{
			tarrifApplicability="Destination";
			//output= new ByteArrayInputStream(pricingControlManager.locateAgents(pricingControl.getWeight(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), tarrifApplicability, pricingControl.getDestinationCountry(), packing, pricingControl.getDestinationPOE(), FIDI,OMNI, minimumFeedbackRating,FAIM,ISO9002,ISO27001,ISO1400,RIM,DOS,GSA,Military).getBytes());
			if(minimumFeedbackRating==null || minimumFeedbackRating.equalsIgnoreCase(""))
			{
				minimumFeedbackRating="0.00";
			}
			output= new ByteArrayInputStream(pricingControlManager.locateAgents1(pricingControl.getId(),tarrifApplicability, FIDI,OMNI,Double.parseDouble(minimumFeedbackRating),FAIM,ISO9002,ISO27001,ISO1400,RIM,"","","").getBytes());
		}
		return output;
	}
	
	public InputStream getPartnerListOrigin() {
		String packing="";
		String tarrifApplicability="";
		ByteArrayInputStream output = null;
		
				
		if(pricingControl.getIsOrigin())
		{
			tarrifApplicability="Origin";
			if(minimumFeedbackRating==null || minimumFeedbackRating.equalsIgnoreCase(""))
			{
				minimumFeedbackRating="0.00";
			}
			output= new ByteArrayInputStream(pricingControlManager.locateAgents1(pricingControl.getId(),tarrifApplicability, FIDI,OMNI,Double.parseDouble(minimumFeedbackRating),FAIM,ISO9002,ISO27001,ISO1400,RIM,"","","").getBytes());
		}
		
		return output;
	}
	/* Changes By Kunal Sharma In process of Code Evaluation With Sandeep De */
	private String totalRevenue="";
	/* Changes End Here */
	public String getDeatilsCount(){
		int totalRevenue1=pricingControlManager.getCount(pricingControlID);
		totalRevenue=String.valueOf(totalRevenue1);
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String agentPricingBasicInfo(){
		getComboList(sessionCorpID);
		if (id != null)
		{
			pricingControl=pricingControlManager.get(id);
			ostates = customerFileManager.findDefaultStateList(pricingControl.getOriginCountry(), sessionCorpID);
		}else{
			pricingControl= new PricingControl();
			pricingControl.setContract("NO CONTRACT - INT");
			pricingControl.setBaseCurrency("USD");
			pricingControl.setScope("Commercial HHG");
			pricingControl.setCreatedBy(getRequest().getRemoteUser());
			pricingControl.setCreatedOn(new Date());
			pricingControl.setUpdatedBy(getRequest().getRemoteUser());
			pricingControl.setUpdatedOn(new Date());
			ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String agentPricingOriginInfo(){
		getComboList(sessionCorpID);
		ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		pricingControl=pricingControlManager.get(id);
		try{
		List portLocation=pricingControlManager.getPortLocation(pricingControl.getOriginPOE(), sessionCorpID);
		String[] sArray=portLocation.get(0).toString().split("#");
		latitude=sArray[0];
		longitude=sArray[1];
		}catch(Exception ex){
			
		}
		agentMasterList=getMasterAgentListForOrigin(pricingControl.getId());
		return SUCCESS;
	}
	
	@SkipValidation
	public String agentPricingDestinationInfo(){
		getComboList(sessionCorpID);
		ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		pricingControl=pricingControlManager.get(id);
		try{
		List portLocation=pricingControlManager.getPortLocation(pricingControl.getDestinationPOE(), sessionCorpID);
		String[] sArray=portLocation.get(0).toString().split("#");
		latitude=sArray[0];
		longitude=sArray[1];
		}catch(Exception ex){
			
		}
		agentMasterList=getMasterAgentListForDestination(pricingControl.getId());
		return SUCCESS;
	}
	private String selectedFreightId;
	private String from;
	@SkipValidation
	public String agentPricingFreightInfo(){
		pricingControl=pricingControlManager.get(id);
		agentFreightList=pricingControlDetailsManager.getMasterFreightList(pricingControl.getId(), sessionCorpID);
		try{
		List selectedFreight=pricingControlDetailsManager.getSelectedFreightFromDetails(pricingControl.getId(),sessionCorpID);
		selectedFreightId=selectedFreight.get(0).toString();
		}catch(Exception ex)
		{
			
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String agentPricingSummaryInfo(){
		
		if(from==null){from="";}
		if(from.equalsIgnoreCase("Accounting")){
			List pricingControlId=pricingControlManager.getPricingControId(shipnumber, sessionCorpID);
			if(!pricingControlId.isEmpty()){
				pricingControl=pricingControlManager.get(Long.parseLong(pricingControlId.get(0).toString()));
			try{
			pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID).get(0);
			originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
			}catch(Exception ex)
			{System.out.println("\n\n\n\n\n exception in Action Origin"); }
			try{
			pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID).get(0);
			destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
			}catch(Exception ex)
			{System.out.println("\n\n\n\n\n exception in Action destination");}
			try{
			refFreightRates=(RefFreightRates)pricingControlDetailsManager.getSelectedRefFreight(pricingControl.getId(), "Freight", sessionCorpID).get(0);
			pricingFreight=(PricingFreight)pricingFreightManager.getPricingFreightRecord(pricingControl.getId(), sessionCorpID).get(0);
			}catch(Exception ex)
			{System.out.println("\n\n\n\n\n exception in Action Freight"); }
			}else{
				
				getComboList(sessionCorpID);
			    serviceOrder=serviceOrderManager.get(Long.parseLong(shipnumber));
			    miscellaneous=miscellaneousManager.get(Long.parseLong(shipnumber));
			    customerFile=serviceOrder.getCustomerFile();
			    pricingControl= new PricingControl();
			    pricingControl.setServiceOrderId(serviceOrder.getId());
			    pricingControl.setContract("NO CONTRACT - INT");
			    pricingControl.setBaseCurrency("USD");
			    pricingControl.setScope("Commercial HHG");
			    pricingControl.setCreatedBy(getRequest().getRemoteUser());
			    pricingControl.setCreatedOn(new Date());
			    pricingControl.setUpdatedBy(getRequest().getRemoteUser());
			    pricingControl.setUpdatedOn(new Date());
			    pricingControl.setCustomerName(serviceOrder.getLastName());
			    pricingControl.setExpectedLoadDate(customerFile.getMoveDate());
			    pricingControl.setMode(serviceOrder.getMode());
			    pricingControl.setWeight(miscellaneous.getEstimatedNetWeight());
			    pricingControl.setPacking(serviceOrder.getPackingMode());
			    pricingControl.setOriginAddress1(serviceOrder.getOriginAddressLine1());
			    pricingControl.setOriginCity(serviceOrder.getOriginCityCode());
			    pricingControl.setOriginZip(serviceOrder.getOriginZip());
			    pricingControl.setOriginCountry(serviceOrder.getOriginCountryCode());
			    pricingControl.setOriginState(serviceOrder.getOriginState());
			    
			    pricingControl.setDestinationAddress1(serviceOrder.getDestinationAddressLine1());
			    pricingControl.setDestinationCity(serviceOrder.getDestinationCityCode());
			    pricingControl.setDestinationZip(serviceOrder.getDestinationZip());
			    pricingControl.setDestinationCountry(serviceOrder.getDestinationCountryCode());
			    pricingControl.setDestinationState(serviceOrder.getDestinationState());
			    ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
			    return INPUT;
			 
			}
		}else{
			pricingControl=pricingControlManager.get(id);
			
			
			
		try{
			if(from.equalsIgnoreCase("Pricing")){
				try{
				if(pricingControl.getIsFreight()){
					List agentMasterList=getMasterAgentList(pricingControl.getId());
					Map sortMap = new  TreeMap();
					Iterator it =agentMasterList.iterator();
					while(it.hasNext()){
						AgentMasterDTO agentMasterDTO=(AgentMasterDTO)it.next();
						Double netQuote=Double.parseDouble(agentMasterDTO.getNetCost().toString());
						String value=agentMasterDTO.getOriginAgentId().toString().concat("#").concat(agentMasterDTO.getFreightId().toString()).concat("#").concat(agentMasterDTO.getDestinationAgentId().toString());
						if(sortMap.containsKey(netQuote)){
						}else{
						sortMap.put(netQuote, value);
						}
					}
					String mapValue="";
					Iterator mapIterator1 = sortMap.entrySet().iterator();
					while(mapIterator1.hasNext()){
						Map.Entry entry = (Map.Entry) mapIterator1.next();
						Double mapKey = (Double) entry.getKey();
						mapValue = (String) entry.getValue();
						break;
					}
					String[] recordArray=mapValue.split("#");
					Long originAgentId=Long.parseLong(recordArray[0].toString());
					Long freightAgentId=Long.parseLong(recordArray[1].toString());
					Long destinationAgentId=Long.parseLong(recordArray[2].toString());
					pricingControlDetailsManager.updatePricingControlDetails(pricingControl.getId(),originAgentId, "Origin");
					pricingControlDetailsManager.updatePricingControlDetails(pricingControl.getId(),destinationAgentId, "Destination");
					pricingControlDetailsManager.updateFreightDetails(pricingControl.getId(),freightAgentId);
					
					
					pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID).get(0);
					originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
					
					pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID).get(0);
					destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
				
					refFreightRates=(RefFreightRates)pricingControlDetailsManager.getSelectedRefFreight(pricingControl.getId(), "Freight", sessionCorpID).get(0);
					pricingFreight=(PricingFreight)pricingFreightManager.getPricingFreightRecord(pricingControl.getId(), sessionCorpID).get(0);
					return SUCCESS;
				}
				
				}catch(Exception ex){
					try{
						if(pricingControl.getIsOrigin()){
							pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Origin", sessionCorpID, pricingControl.getIsFreight());
							pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID).get(0);
							originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
						}
						}catch(Exception ex1){
							
						}
						try{
						if(pricingControl.getIsDestination()){
							pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Destination", sessionCorpID,pricingControl.getIsFreight());
							pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID).get(0);
							destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
						}
						}catch(Exception ex1){
							
						}
				}
				try{
				if(pricingControl.getIsOrigin()){
					pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Origin", sessionCorpID, pricingControl.getIsFreight());
					pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID).get(0);
					originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
				}
				}catch(Exception ex){
					
				}
				try{
				if(pricingControl.getIsDestination()){
					pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Destination", sessionCorpID,pricingControl.getIsFreight());
					pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID).get(0);
					destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
				}
				}catch(Exception ex){
					
				}
			}else{
					try{
					pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID).get(0);
					originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
					}catch(Exception ex){
						
					}
					try{
					pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID).get(0);
					destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
					}catch(Exception ex){
						
					}
					try{
					refFreightRates=(RefFreightRates)pricingControlDetailsManager.getSelectedRefFreight(pricingControl.getId(), "Freight", sessionCorpID).get(0);
					pricingFreight=(PricingFreight)pricingFreightManager.getPricingFreightRecord(pricingControl.getId(), sessionCorpID).get(0);
					}catch(Exception ex){
						
					}
			}
		//pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID).get(0);
		//originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
		
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n exception in Action Origin"); 
		}
		/*try{
			if(from.equalsIgnoreCase("Pricing")){
				pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Destination", sessionCorpID,pricingControl.getIsFreight());
			}
		pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID).get(0);
		destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n exception in Action destination"); 
		}
		try{
			if(from.equalsIgnoreCase("Pricing")){
				//pricingControlManager.updateFreightResult(freightId, pricingControlID, "Freight", freightCurrency, pricingControl.getBaseCurrency());
				pricingControlDetailsManager.selectMinimumFreight(pricingControlDetailsOrigin.getOriginPortCode(), pricingControlDetailsDestination.getDestinationPortCode(), pricingControl.getContainerSize(), pricingControl.getExpectedLoadDate(), pricingControl.getId(), pricingControl.getBaseCurrency(),pricingControl.getMode(), pricingControl.getContract(), pricingControl.getOriginPortRange(), pricingControl.getDestinationPortRange() ,pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude() );
			}
		refFreightRates=(RefFreightRates)pricingControlDetailsManager.getSelectedRefFreight(pricingControl.getId(), "Freight", sessionCorpID).get(0);
		pricingFreight=(PricingFreight)pricingFreightManager.getPricingFreightRecord(pricingControl.getId(), sessionCorpID).get(0);
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n exception in Action Freight"); 
		}*/
		}
		
		
		return SUCCESS;
	}
	
	public String agentPricingAllPricingInfo(){
		pricingControl=pricingControlManager.get(id);
		if(pricingControl.getIsFreight()){
			agentMasterList=getMasterAgentList(pricingControl.getId());
			if(!agentMasterList.isEmpty()){
				return SUCCESS;
			}else{
				agentMasterList=getMasterAgentListForOrigin(pricingControl.getId());
				if(agentMasterList.isEmpty()){
					agentMasterList=getMasterAgentListForDestination(pricingControl.getId());
				}else{
					return SUCCESS;
				}
			}
			return SUCCESS;
		}
		if(pricingControl.getIsOrigin()){
			agentMasterList=getMasterAgentListForOrigin(pricingControl.getId());
		}
		if(pricingControl.getIsDestination()){
			agentMasterList=getMasterAgentListForDestination(pricingControl.getId());
		}
		return SUCCESS;
	}
	
	private List getMasterAgentListForDestination(Long pricingControlId) {
		List masList= new ArrayList();
		if(destinationMarkup==null){
			destinationMarkup="true";
		}
		if(DTHC==null){
			DTHC="true";
		}
		if(destinationMinimumFeedbackRating==null || destinationMinimumFeedbackRating.equalsIgnoreCase(",") || destinationMinimumFeedbackRating.equalsIgnoreCase(""))
		{
			destinationMinimumFeedbackRating="0.00";
		}
		List destinationRecordsList=pricingControlManager.getRecordList(pricingControlId,sessionCorpID, "Destination",destinationFIDI,destinationOMNI,Double.parseDouble(destinationMinimumFeedbackRating),destinationFAIM,destinationISO9002,destinationISO27001,destinationISO1400,destinationRIM,"","","");
		Iterator destinationListIterator=destinationRecordsList.iterator();
		while(destinationListIterator.hasNext()){
			PricingControlDetails pricingControlDetailsForDestination=(PricingControlDetails) destinationListIterator.next();
			AgentMasterDTO agentMasterDTO= new AgentMasterDTO();
			agentMasterDTO.setDestinationAgentId(pricingControlDetailsForDestination.getId());
			agentMasterDTO.setDestinationAgent(pricingControlDetailsForDestination.getPartnerName());
			agentMasterDTO.setDestinationPartnerId(pricingControlDetailsForDestination.getPartnerID());
			agentMasterDTO.setDestinationAgentActivity(pricingControlDetailsForDestination.getShipmentActivity());
			agentMasterDTO.setDestinationAgentFeedback(pricingControlDetailsForDestination.getFeedbackRating());
			agentMasterDTO.setDestinationPort(pricingControlDetailsForDestination.getDestinationPortCode());
			Double destinationCost=0.0;
			Double discountValue=0.0;
			Double netCost=0.0;
			
			if(destinationMarkup.equalsIgnoreCase("true") && DTHC.equalsIgnoreCase("true")){
				destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getRateMarkUp())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
			}
			if(destinationMarkup.equalsIgnoreCase("true") && DTHC.equalsIgnoreCase("false")){
				destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getQuoteMarkupWithoutTHC())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
			}
			if(destinationMarkup.equalsIgnoreCase("false") && DTHC.equalsIgnoreCase("true")){
				destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getRate())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
			}
			if(destinationMarkup.equalsIgnoreCase("false") && DTHC.equalsIgnoreCase("false")){
				destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getQuoteWithoutTHC())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
			}
			
			destinationCost=Math.round(destinationCost*100)/100.00;
			agentMasterDTO.setDestinationCost(destinationCost);
			Double totalCost= destinationCost;
			discountValue=pricingControlDetailsForDestination.getDiscountValue();
			if(DTHC.equalsIgnoreCase("true")){
				discountValue=pricingControlDetailsForDestination.getDiscountValue();
			}else{
				discountValue=pricingControlDetailsForDestination.getDiscountValueWithoutTHC();
			}
			netCost=totalCost +  discountValue;
			agentMasterDTO.setTotalCost(totalCost);
			agentMasterDTO.setNetCost(netCost);
			agentMasterDTO.setDiscountvalue(discountValue);
			agentMasterDTO.setHaulingDistance(pricingControlDetailsForDestination.getHaulingDistance());
			agentMasterDTO.setAddressDistance(pricingControlDetailsForDestination.getAddressDistance());
			if(pricingControlDetailsForDestination.getDestinationSelection()==true){
				agentMasterDTO.setOriginSelection(true);
			}
			masList.add(agentMasterDTO);
			
		}
		return masList;
	}

	private List getMasterAgentListForOrigin(Long pricingControlId) {
		List masList= new ArrayList();
		if(originMarkup==null){
			originMarkup="true";
		}
		
		
		if(originMinimumFeedbackRating==null || originMinimumFeedbackRating.equalsIgnoreCase(",") || originMinimumFeedbackRating.equalsIgnoreCase(""))
		{
			originMinimumFeedbackRating="0.00";
		}
		List originRecordsList=pricingControlManager.getRecordList(pricingControlId,sessionCorpID,"Origin",originFIDI,originOMNI,Double.parseDouble(originMinimumFeedbackRating),originFAIM,originISO9002,originISO27001,originISO1400,originRIM,"","","");
		
		Iterator originListIterator=originRecordsList.iterator();
		while(originListIterator.hasNext()){
			PricingControlDetails pricingControlDetailsForOrigin=(PricingControlDetails) originListIterator.next();
			AgentMasterDTO agentMasterDTO= new AgentMasterDTO();
			
			agentMasterDTO.setOriginAgentId(pricingControlDetailsForOrigin.getId());
			agentMasterDTO.setOriginAgent(pricingControlDetailsForOrigin.getPartnerName());
			agentMasterDTO.setOriginPartnerId(pricingControlDetailsForOrigin.getPartnerID());
			agentMasterDTO.setOriginAgentActivity(pricingControlDetailsForOrigin.getShipmentActivity());
			agentMasterDTO.setOriginAgentFeedback(pricingControlDetailsForOrigin.getFeedbackRating());
			agentMasterDTO.setOriginPort(pricingControlDetailsForOrigin.getOriginPortCode());
			Double originCost=0.0;
			Double discountValue=0.0;
			Double netCost=0.0;
			if(originMarkup.equalsIgnoreCase("true")){
				originCost=Double.parseDouble(pricingControlDetailsForOrigin.getRateMarkUp())*Double.parseDouble(pricingControlDetailsForOrigin.getExchangeRate().toString());
			}else{
				originCost=Double.parseDouble(pricingControlDetailsForOrigin.getRate())*Double.parseDouble(pricingControlDetailsForOrigin.getExchangeRate().toString());
			}
			
			
			originCost=Math.round(originCost*100)/100.00;
			agentMasterDTO.setOriginCost(originCost);
			Double totalCost= originCost;
			
			discountValue=pricingControlDetailsForOrigin.getDiscountValue();
			netCost=totalCost +  discountValue;
			
			agentMasterDTO.setTotalCost(totalCost);
			agentMasterDTO.setNetCost(netCost);
			agentMasterDTO.setDiscountvalue(discountValue);
			agentMasterDTO.setHaulingDistance(pricingControlDetailsForOrigin.getHaulingDistance());
			agentMasterDTO.setAddressDistance(pricingControlDetailsForOrigin.getAddressDistance());
			if(pricingControlDetailsForOrigin.getDestinationSelection()==true){
				agentMasterDTO.setOriginSelection(true);
			}
			masList.add(agentMasterDTO);
		
		}
		
		
		return masList;
	}

	private List agentMasterList;
	private RefFreightRatesManager refFreightRatesManager;
	
	private List getMasterAgentList(Long pricingControlId) { 
		pricingControl=pricingControlManager.get(pricingControlId); 
		List masList= new ArrayList();
		Map freightMap=getFreights(pricingControlId);
		if(originMarkup==null){
			originMarkup="true";
		}
		if(destinationMarkup==null){
			destinationMarkup="true";
		}
		if(DTHC==null){
			DTHC="true";
		}
		if(originMinimumFeedbackRating==null || originMinimumFeedbackRating.equalsIgnoreCase(",") || originMinimumFeedbackRating.equalsIgnoreCase(""))
		{
			originMinimumFeedbackRating="0.00";
		}
		if(destinationMinimumFeedbackRating==null || destinationMinimumFeedbackRating.equalsIgnoreCase(",") || destinationMinimumFeedbackRating.equalsIgnoreCase(""))
		{
			destinationMinimumFeedbackRating="0.00";
		}
		List originRecordsList=pricingControlManager.getRecordList(pricingControlId,sessionCorpID,"Origin",originFIDI,originOMNI,Double.parseDouble(originMinimumFeedbackRating),originFAIM,originISO9002,originISO27001,originISO1400,originRIM,"","","");
		List destinationRecordsList=pricingControlManager.getRecordList(pricingControlId,sessionCorpID, "Destination",destinationFIDI,destinationOMNI,Double.parseDouble(destinationMinimumFeedbackRating),destinationFAIM,destinationISO9002,destinationISO27001,destinationISO1400,destinationRIM,"","","");
		
		
		Iterator destinationListIterator=destinationRecordsList.iterator();
		
		/*while(destinationListIterator.hasNext()){
			pricingControlDetailsDestination=(PricingControlDetails) destinationListIterator.next();
			System.out.println("Dest Port Code---->"+pricingControlDetailsDestination.getDestinationPortCode());
		}
		while(originListIterator.hasNext()){
			pricingControlDetailsOrigin=(PricingControlDetails) originListIterator.next();
			System.out.println("Ori Port Code---->"+pricingControlDetailsOrigin.getOriginPortCode());
		}*/
		int count=0;
		
		
		
		while(destinationListIterator.hasNext()){
			PricingControlDetails pricingControlDetailsForDestination=(PricingControlDetails) destinationListIterator.next();
			Iterator originListIterator=originRecordsList.iterator();
			while(originListIterator.hasNext()){
				PricingControlDetails pricingControlDetailsForOrigin=(PricingControlDetails) originListIterator.next();
				String keyGenerated=pricingControlDetailsForOrigin.getOriginPortCode().concat(":").concat(pricingControlDetailsForDestination.getDestinationPortCode());
				
				Iterator mapIterator = freightMap.entrySet().iterator();
				while (mapIterator.hasNext()) {
					Map.Entry entry = (Map.Entry) mapIterator.next();
					String mapKey = (String) entry.getKey();
					String mapValue = (String) entry.getValue();
					String[] ids=mapValue.split(":");
					Long refFreightId=Long.parseLong(ids[0]);
					Long pricingFreightId=Long.parseLong(ids[1]);
					if(mapKey.contains(keyGenerated)){
						RefFreightRates refFreightRates=refFreightRatesManager.get(refFreightId);
						PricingFreight pricingFreight=pricingFreightManager.get(pricingFreightId);
						AgentMasterDTO agentMasterDTO= new AgentMasterDTO();
						
						agentMasterDTO.setOriginAgentId(pricingControlDetailsForOrigin.getId());
						agentMasterDTO.setOriginAgent(pricingControlDetailsForOrigin.getPartnerName());
						agentMasterDTO.setOriginPartnerId(pricingControlDetailsForOrigin.getPartnerID());
						agentMasterDTO.setOriginAgentActivity(pricingControlDetailsForOrigin.getShipmentActivity());
						agentMasterDTO.setOriginAgentFeedback(pricingControlDetailsForOrigin.getFeedbackRating());
						agentMasterDTO.setOriginPort(pricingControlDetailsForOrigin.getOriginPortCode());
						Double originCost=0.0;
						Double discountValue=0.0;
						Double netCost=0.0;
						
						if(originMarkup==null || originMarkup.equalsIgnoreCase("true")){
							originCost=Double.parseDouble(pricingControlDetailsForOrigin.getRateMarkUp())*Double.parseDouble(pricingControlDetailsForOrigin.getExchangeRate().toString());
						}else{
							originCost=Double.parseDouble(pricingControlDetailsForOrigin.getRate())*Double.parseDouble(pricingControlDetailsForOrigin.getExchangeRate().toString());
						}
						
						originCost=Math.round(originCost*100)/100.00;
						agentMasterDTO.setOriginCost(originCost);
						
						agentMasterDTO.setFreightCarrier(refFreightRates.getCarrier());
						BigDecimal freightCost=new BigDecimal(0.0);
						if(pricingControl.getFreightMarkUpControl()){
							freightCost=pricingFreight.getFreightPrice().add(pricingFreight.getFreightMarkup());
						}else{
							freightCost=pricingFreight.getFreightPrice();
						}
						agentMasterDTO.setFreightCost(freightCost);
						agentMasterDTO.setRefFreightId(refFreightId);
						agentMasterDTO.setFreightId(pricingFreight.getId());
						
						agentMasterDTO.setDestinationAgentId(pricingControlDetailsForDestination.getId());
						agentMasterDTO.setDestinationAgent(pricingControlDetailsForDestination.getPartnerName());
						agentMasterDTO.setDestinationPartnerId(pricingControlDetailsForDestination.getPartnerID());
						agentMasterDTO.setDestinationAgentActivity(pricingControlDetailsForDestination.getShipmentActivity());
						agentMasterDTO.setDestinationAgentFeedback(pricingControlDetailsForDestination.getFeedbackRating());
						agentMasterDTO.setDestinationPort(pricingControlDetailsForDestination.getDestinationPortCode());
						Double destinationCost=0.0;
						if(destinationMarkup.equalsIgnoreCase("true") && DTHC.equalsIgnoreCase("true")){
							destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getRateMarkUp())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
						}
						if(destinationMarkup.equalsIgnoreCase("true") && DTHC.equalsIgnoreCase("false")){
							destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getQuoteMarkupWithoutTHC())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
						}
						if(destinationMarkup.equalsIgnoreCase("false") && DTHC.equalsIgnoreCase("true")){
							destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getRate())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
						}
						if(destinationMarkup.equalsIgnoreCase("false") && DTHC.equalsIgnoreCase("false")){
							destinationCost=Double.parseDouble(pricingControlDetailsForDestination.getQuoteWithoutTHC())*Double.parseDouble(pricingControlDetailsForDestination.getExchangeRate().toString());
						}
												
						destinationCost=Math.round(destinationCost*100)/100.00;
						agentMasterDTO.setDestinationCost(destinationCost);
						Double totalCost= destinationCost + originCost + Double.parseDouble(freightCost.toString());
						totalCost=Math.round(totalCost*100)/100.00;
						if(DTHC.equalsIgnoreCase("true")){
							discountValue=pricingControlDetailsForOrigin.getDiscountValue() + pricingControlDetailsForDestination.getDiscountValue();
						}else{
							discountValue=pricingControlDetailsForOrigin.getDiscountValue() + pricingControlDetailsForDestination.getDiscountValueWithoutTHC();
						}
						
						netCost=totalCost +  discountValue;
						agentMasterDTO.setDiscountvalue(discountValue);
						agentMasterDTO.setTotalCost(totalCost);
						agentMasterDTO.setNetCost(Math.round(netCost*100)/100.00);
						if(pricingControlDetailsForOrigin.getDestinationSelection()==true && pricingControlDetailsForDestination.getDestinationSelection()==true && pricingFreight.getFreightSelection()==true){
							
							agentMasterDTO.setOriginSelection(true);
						}
						masList.add(agentMasterDTO);
						//System.out.println("\n\n\n\n\n\n masterlist----------->"+mapKey+" count--->"+count++);
						
					}
				}
				
				
				//System.out.println("\n ");
			}
			
		}
		
		
		return masList;
	}
	
	
	
	private Map getFreights(Long pricingControlId) { 
		List freightList=pricingFreightManager.getFreightList(pricingControlId);
		//System.out.println("freightList-->"+freightList);
		SortedMap freightMap = new TreeMap();
		String key ="";
		Iterator it=freightList.iterator();
		while(it.hasNext()){
			Object[] row=(Object[])it.next();
			String carrier=row[0].toString();
			String originPortCode=row[1].toString();
			String destinationPortCode=row[2].toString();
			String refFreightId=row[3].toString();
			String pricingFreightId=row[4].toString();
			
			String value=refFreightId.concat(":").concat(pricingFreightId);
			key=carrier.concat(":").concat(originPortCode).concat(":").concat(destinationPortCode);
			//System.out.println("\n\n\n Freight for ---> "+key);
			freightMap.put(key, value);
		}
		return freightMap;
	}

	private Long pricingDetailsId;
	private Long pricingControlID;
	private String tarrifApplicability;
	private Long freightId;
	private String freightCurrency;
	private String userCheck;
	private CustomerFile customerFile;
	private List maxSequenceNumber;
	private Long autoSequenceNumber;
	private String seqNum;
	private List checkSequenceNumberList;
	private List maxShip;
	private String ship;
	private Long autoShip;
	private Long prevShip;
	private String shipPrev;
	private String shipnumber;
	private Long tempID;
	private ServiceOrder serviceOrderPrev;
	private List<SystemDefault> sysDefaultDetail;
	private BillingManager billingManager;
	private Billing billing;
	private Map<String, String> dstates;
	private List maxLineNumber;
	private String accountLineNumber;
	private Long autoLineNumber;
	private AccountLineManager accountLineManager;
	private Boolean dthcFlag;
	private String sequenceNumber;
	private String pricingNotes;
	private Notes notes;
	private Long maxNotesId;
	private NotesManager notesManager;
	private String freightPrice;
	private Boolean freightMarkUpFlag;
	public String updatePricingDetail(){
		pricingControlManager.updateSelectedResult(pricingDetailsId,pricingControlID,tarrifApplicability);
		return SUCCESS;
	}
	public String updateFreightDetail(){
		pricingControl=pricingControlManager.get(pricingControlID);
		pricingControlManager.updateFreightResult(freightId, pricingControlID, "Freight", freightCurrency, pricingControl.getBaseCurrency(), pricingControl.getMode(),pricingControl.getContract(),freightPrice);
		return SUCCESS;
	}
	
	public String updateDTHCFlag(){
		
		pricingControlManager.updateDTHCFlag(pricingControlID,tarrifApplicability,dthcFlag);
		return SUCCESS;
	}
	
public String updateFreightMarkUpControl(){
		pricingControlManager.updateFreightMarkUpFlag(pricingControlID,"Freight",freightMarkUpFlag);
		return SUCCESS;
	}

	public String portsPoint(){
		return SUCCESS;
	}
	
	public InputStream getPortsPointMarkup()
	{
		ByteArrayInputStream output = null;
		if(tariffApplicability.equalsIgnoreCase("Origin"))
		{
			output=new ByteArrayInputStream(pricingControlManager.getPorts(pricingControl.getOriginCountry(),pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getMode()).getBytes());
		}
		if(tariffApplicability.equalsIgnoreCase("Destination"))
		{
			output=new ByteArrayInputStream(pricingControlManager.getPorts(pricingControl.getDestinationCountry(),pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), pricingControl.getMode()).getBytes());
		}
		
		return output;
	}
	
	public String portsPointPerOrigin(){
		return SUCCESS;
	}
	
	public InputStream getPortsPointMarkupPerOrigin()
	{
		ByteArrayInputStream output = null;
		
			output=new ByteArrayInputStream(pricingControlManager.getPortsPerSection(pricingControl.getId(), "Origin").getBytes());
		    return output;
	}
	
	public String portsPointPerDestination(){
		return SUCCESS;
	}
	
	public InputStream getPortsPointMarkupPerDestination()
	{
		ByteArrayInputStream output = null;
		
			output=new ByteArrayInputStream(pricingControlManager.getPortsPerSection(pricingControl.getId(), "Destination").getBytes());
		    return output;
	}
	
	public String pricingInformation(){
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		dstates = customerFileManager.findDefaultStateList("", sessionCorpID);
		if(userCheck.equals("")||userCheck.equals(","))
		  {
		  }
		 else
		  {
			userCheck=userCheck.trim();
		    if(userCheck.indexOf(",")==0)
			 {
			  userCheck=userCheck.substring(1);
			 }
			if(userCheck.lastIndexOf(",")==userCheck.length()-1)
			 {
			  userCheck=userCheck.substring(0, userCheck.length()-1);
			 }
		    String[] arrayId=userCheck.split(",");
		    int arrayLength = arrayId.length;
		    pricingControl=pricingControlManager.get(Long.parseLong(arrayId[0])); 
		    customerFile = new CustomerFile();
		    customerFile.setLastName(pricingControl.getCustomerName());
		    customerFile.setMoveDate(pricingControl.getExpectedLoadDate());
		    customerFile.setContract(pricingControl.getContract());
		    customerFile.setOriginAddress1(pricingControl.getOriginAddress1());
		    customerFile.setOriginAddress3("Port: "+pricingControl.getOriginPOE()+"("+pricingControl.getOriginPOEName()+")");
		    customerFile.setOriginCity(pricingControl.getOriginCity());
		    
		    try{
		    List  originCountryDescription=refMasterManager.getDescription(pricingControl.getOriginCountry(), "COUNTRY");
		    customerFile.setOriginCountry(originCountryDescription.get(0).toString());
		    }catch(Exception ex){
		    	
		    }
		    customerFile.setOriginState(pricingControl.getOriginState());
		    customerFile.setOriginZip(pricingControl.getOriginZip());
		    customerFile.setOriginCountryCode(pricingControl.getOriginCountry());
		    customerFile.setDestinationAddress1(pricingControl.getDestinationAddress1());
		    customerFile.setDestinationAddress3("Port: "+pricingControl.getDestinationPOE()+"("+pricingControl.getDestinationPOEName()+")");
		    customerFile.setDestinationCity(pricingControl.getDestinationCity());
		    try{
		    List  destinationCountryDescription=refMasterManager.getDescription(pricingControl.getDestinationCountry(), "COUNTRY");
		    customerFile.setDestinationCountry(destinationCountryDescription.get(0).toString());
		    }catch(Exception ex){
		    	
		    }
		    customerFile.setDestinationState(pricingControl.getDestinationState());
		    customerFile.setDestinationZip(pricingControl.getDestinationZip());
		    customerFile.setDestinationCountryCode(pricingControl.getDestinationCountry());
		  }
		
		
		
		return SUCCESS;
	}
	
	public String saveBasicInformation(){
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		//ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		//dstates = customerFileManager.findDefaultStateList("", sessionCorpID);
		boolean isNew = (customerFile.getId() == null);
		if(isNew){
			createCustomerFile(userCheck);
		}else{
			ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			customerFile=customerFileManager.save(customerFile);
		}
		String key="";
		if(isNew){
		key = "The Quotation File has been created successfully";
		}else{
		key = "The Quotation File has been updated successfully";
		}
		saveMessage(getText(key));
		hitFlag="55";
		return SUCCESS;
	}
	
	public String createCustomerFile(String userCheck){
				maxSequenceNumber = customerFileManager.findMaximum(sessionCorpID);
				if (maxSequenceNumber.get(0) == null) {
					customerFile.setSequenceNumber(sessionCorpID + "1000001");
				} else {
					autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
					seqNum = sessionCorpID + autoSequenceNumber.toString();
					customerFile.setSequenceNumber(seqNum);
				}
				checkSequenceNumberList = customerFileManager.findSequenceNumber(customerFile.getSequenceNumber());
				if (!(checkSequenceNumberList.isEmpty())) {
					autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
		
					seqNum = sessionCorpID + autoSequenceNumber.toString();
					customerFile.setSequenceNumber(seqNum);
				}
			customerFile.setCompanyDivision("SSC");
			customerFile.setStatus("NEW");
			customerFile.setQuotationStatus("New");
			customerFile.setStatusNumber(1);
			customerFile.setComptetive("Y");
			customerFile.setCorpID(sessionCorpID);
			customerFile.setSurveyTime("00:00");
			customerFile.setSurveyTime2("00:00");
			customerFile.setAssignmentType("None");
			customerFile.setControlFlag("Q");
			customerFile.setJob("INT");
			customerFile.setSource("PW");
			customerFile.setBookingAgentCode(parentAgent);
			customerFile.setAccountCode(parentAgent);
			List parentAgentName=customerFileManager.findByBillToCode(parentAgent, sessionCorpID);
			if(!parentAgentName.isEmpty()){
				customerFile.setBookingAgentName(parentAgentName.get(0).toString());
				customerFile.setAccountName(parentAgentName.get(0).toString());
				customerFile.setBillToName(parentAgentName.get(0).toString());
			}
			
			customerFile.setBillToCode(parentAgent);
			customerFile.setStatusDate(new Date());
			customerFile.setCreatedOn(new Date());
			customerFile.setUpdatedOn(new Date());
			customerFile.setCreatedBy(getRequest().getRemoteUser());
			customerFile.setUpdatedBy(getRequest().getRemoteUser());
			customerFile.setSystemDate(new Date());
			customerFile.setPrivileges(false);
			customerFile.setNoCharge(false);
			customerFile.setPortalIdActive(false);
			customerFile.setStatusDate(new Date());
			if(customerFile.getOriginState()==null){
				customerFile.setOriginState("");
			}
			if(customerFile.getDestinationState()==null){
				customerFile.setDestinationState("");
			}
			if(customerFile.getOriginAddress3().length()>30){
				customerFile.setOriginAddress3(customerFile.getOriginAddress3().substring(0, 29));
			}
			if(customerFile.getDestinationAddress3().length()>30){
				customerFile.setDestinationAddress3(customerFile.getDestinationAddress3().substring(0, 29));
			}
			customerFile=customerFileManager.save(customerFile);
			ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			createNotes(customerFile);
			createServiceOrder(customerFile ,userCheck);
			
		return SUCCESS;
	}
	
	private String createNotes(CustomerFile customerFile) {
		notes = new Notes();
		notes.setNoteStatus("NEW");
        notes.setCorpID(sessionCorpID);
        notes.setNotesId(customerFile.getSequenceNumber());
        notes.setCustomerNumber(customerFile.getSequenceNumber());
        notes.setNoteType("Customer File");
        notes.setRemindTime("00:00");
        notes.setCreatedOn(new Date());
        notes.setSystemDate(new Date());
        notes.setUpdatedOn(new Date());
        notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
        notes.setRemindInterval("15");
        notes.setNoteSubType("Customer");
        notes.setNotesKeyId(customerFile.getId());
       // maxNotesId = Long.parseLong(notesManager.findMaxId().get(0).toString());
    	//maxNotesId=maxNotesId+1;
    	//notes.setId(maxNotesId);
        notes.setNote(pricingNotes);
        notes.setSubject("Pricing Wizard Notes");
        notes.setCreatedBy(getRequest().getRemoteUser());
        notes.setUpdatedBy(getRequest().getRemoteUser());
        notes.setCorpID(sessionCorpID);
        notesManager.save(notes);
		return SUCCESS;
	}

	public String createServiceOrder(CustomerFile customerFile, String userCheck){
		String[] arrayId=userCheck.split(",");
		int arrayLength = arrayId.length;
	    for(int i=0;i<arrayLength;i++)
		 {
	    	boolean hasOrigin = false;
			boolean hasDestination = false;
			boolean hasFreight = false;
	    	pricingControl=pricingControlManager.get(Long.parseLong(arrayId[i]));
	    	
	    	try{
				List pricingControlDetailsOriginList=pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID);
	    		if(!pricingControlDetailsOriginList.isEmpty()){
	    			pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsOriginList.get(0);
	    			originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
	    			hasOrigin=true;
	    		}
	    	}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n exception in Action Origin"); 
				hasOrigin = false;
			}
			try{
				List  pricingControlDetailsDestinationList=pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID);
				if(!pricingControlDetailsDestinationList.isEmpty()){
					pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsDestinationList.get(0);
					destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
					hasDestination=true;
				}
				
			}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n exception in Action destination");
				hasDestination=false;
			}
			try{
				List pricingControlDetailsFreightList=pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Freight", sessionCorpID);
				if(!pricingControlDetailsFreightList.isEmpty()){
					pricingControlDetailsFreight=(PricingControlDetails)pricingControlDetailsFreightList.get(0);
					refFreightRates=(RefFreightRates)pricingControlDetailsManager.getSelectedRefFreight(pricingControl.getId(), "Freight", sessionCorpID).get(0);
					hasFreight=true;
				}
				
			}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n exception in Action Freight"); 
				hasFreight=true;
			}
	    	
	    	
	    	
	    	maxShip = customerFileManager.findMaximumShipCF(customerFile.getSequenceNumber(),"",customerFile.getCorpID());
			if (maxShip.get(0) == null) {
				ship = "01";

			} else {
				autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
				//System.out.println(autoShip);
				if ((autoShip.toString()).length() == 1) {
					ship = "0" + (autoShip.toString());
				} else {
					ship = autoShip.toString();
				}

				prevShip = Long.parseLong((maxShip).get(0).toString());
				//System.out.println(prevShip);
				if ((prevShip.toString()).length() == 1) {
					shipPrev = "0" + (prevShip.toString());
				} else {
					shipPrev = prevShip.toString();
				}
				if (!ship.equals("01")) {
					shipnumber = customerFile.getSequenceNumber() + shipPrev;
					tempID = Long.parseLong((serviceOrderManager.findIdByShipNumber(shipnumber)).get(0).toString());
					serviceOrderPrev = serviceOrderManager.get(tempID);
				}

			}
			
			shipnumber = customerFile.getSequenceNumber() + ship;
			serviceOrder = new ServiceOrder();
			miscellaneous = new Miscellaneous();
			miscellaneous.setShipNumber(shipnumber);
			miscellaneous.setEstimateTareWeight(new BigDecimal("0.00"));
			miscellaneous.setActualTareWeight(new BigDecimal("0.00"));
			miscellaneous.setRwghTare(new BigDecimal("0.00"));
			miscellaneous.setChargeableTareWeight(new BigDecimal("0.00"));
			miscellaneous.setEntitleTareWeight(new BigDecimal("0.00"));
			serviceOrder.setShip(ship);
			serviceOrder.setShipNumber(shipnumber);			
			serviceOrder.setSequenceNumber(customerFile.getSequenceNumber());
			//Bug 11839 - Possibility to modify Current status to Prior status
			serviceOrder.setStatus("NEW");
			serviceOrder.setStatusNumber(new Integer(1));
			serviceOrder.setQuoteStatus("PE");
			serviceOrder.setUpdatedOn(new Date());
			serviceOrder.setCreatedOn(new Date());
			serviceOrder.setCreatedBy(getRequest().getRemoteUser());
			serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
			
			serviceOrder.setCompanyDivision(customerFile.getCompanyDivision());
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
    		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
    			for (SystemDefault systemDefault : sysDefaultDetail) {
    				miscellaneous.setUnit1(systemDefault.getWeightUnit());
    				miscellaneous.setUnit2(systemDefault.getVolumeUnit());
    			}
    		}
			if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
    			weightType="lbscft";
    		}
    		else{
    			weightType="kgscbm";
    		}
			serviceOrder.setPrefix(customerFile.getPrefix());
			serviceOrder.setMi(customerFile.getMiddleInitial());
			serviceOrder.setFirstName(customerFile.getFirstName());
			serviceOrder.setLastName(customerFile.getLastName());
			serviceOrder.setJob(customerFile.getJob());
			serviceOrder.setOriginAddressLine1(customerFile.getOriginAddress1());
			serviceOrder.setOriginAddressLine2(customerFile.getOriginAddress2());
			serviceOrder.setOriginAddressLine3(customerFile.getOriginAddress3());
			serviceOrder.setOriginCity(customerFile.getOriginCity());
			serviceOrder.setOriginCityCode(customerFile.getOriginCityCode());
			serviceOrder.setOriginState(customerFile.getOriginState());
			serviceOrder.setOriginZip(customerFile.getOriginZip());
			serviceOrder.setOriginCountry(customerFile.getOriginCountry());
			serviceOrder.setOriginCountryCode(customerFile.getOriginCountryCode());
			serviceOrder.setOriginDayPhone(customerFile.getOriginDayPhone());
			serviceOrder.setOriginDayExtn(customerFile.getOriginDayPhoneExt());
			serviceOrder.setOriginHomePhone(customerFile.getOriginHomePhone());
			serviceOrder.setOriginFax(customerFile.getOriginFax());
			serviceOrder.setOriginContactName(customerFile.getContactName());
			serviceOrder.setOriginContactWork(customerFile.getContactPhone());
			serviceOrder.setEmail(customerFile.getEmail());
			serviceOrder.setEmail2(customerFile.getEmail2());
			serviceOrder.setDestinationCompany(customerFile.getDestinationCompany());
			serviceOrder.setDestinationAddressLine1(customerFile.getDestinationAddress1());
			serviceOrder.setDestinationAddressLine2(customerFile.getDestinationAddress2());
			serviceOrder.setDestinationAddressLine3(customerFile.getDestinationAddress3());
			serviceOrder.setDestinationCity(customerFile.getDestinationCity());
			serviceOrder.setDestinationCityCode(customerFile.getDestinationCityCode());
			serviceOrder.setDestinationZip(customerFile.getDestinationZip());
			serviceOrder.setDestinationCountryCode(customerFile.getDestinationCountryCode());
			serviceOrder.setDestinationCountry(customerFile.getDestinationCountry());
			serviceOrder.setDestinationDayPhone(customerFile.getDestinationDayPhone());
			serviceOrder.setDestinationDayExtn(customerFile.getDestinationDayPhoneExt());
			serviceOrder.setDestinationHomePhone(customerFile.getDestinationHomePhone());
			serviceOrder.setDestinationFax(customerFile.getDestinationFax());
			serviceOrder.setDestinationDayPhone(customerFile.getDestinationDayPhone());
			serviceOrder.setDestinationEmail(customerFile.getDestinationEmail());
			serviceOrder.setDestinationEmail2(customerFile.getDestinationEmail2());
			serviceOrder.setBillToName(customerFile.getBillToName());
			serviceOrder.setBillToCode(customerFile.getBillToCode());
			serviceOrder.setOriginMobile(customerFile.getOriginMobile());
			serviceOrder.setDestinationMobile(customerFile.getDestinationMobile());
			serviceOrder.setContactName(customerFile.getDestinationContactName());
			serviceOrder.setContactPhone(customerFile.getDestinationContactPhone());
			serviceOrder.setDestinationCountry(customerFile.getDestinationCountry());
			serviceOrder.setOriginCountry(customerFile.getOriginCountry());
			serviceOrder.setDestinationState(customerFile.getDestinationState());
			serviceOrder.setVip(customerFile.getVip());
			serviceOrder.setContract(customerFile.getContract());
			serviceOrder.setCorpID(sessionCorpID);
			serviceOrder.setBookingAgentCode(customerFile.getBookingAgentCode());
			serviceOrder.setBookingAgentName(customerFile.getBookingAgentName());
			serviceOrder.setLastName(customerFile.getLastName());
			serviceOrder.setControlFlag(customerFile.getControlFlag());
			serviceOrder.setStatusDate(new Date());
			serviceOrder.setCustomerFile(customerFile);
			company = companyManager.findByCorpID(sessionCorpID).get(0);
			try{
			if(pricingControl.getOriginCountry()!=null){
				if(pricingControl.getOriginCountry().toString().equalsIgnoreCase(company.getCountryID())){
					serviceOrder.setRouting("EXP");
				}
			}
			if(pricingControl.getDestinationCountry()!=null){
				if(pricingControl.getDestinationCountry().toString().equalsIgnoreCase(company.getCountryID())){
					serviceOrder.setRouting("IMP");
				}
			}
			}catch(Exception ex){
				
			}
			
			try{
				serviceOrder.setOriginAgentCode(originPartner.getPartnerCode());
			}catch(Exception ex){
				System.out.println("\n\n\n\n\n exception while setting partnerCode");
			}
			try{
				serviceOrder.setDestinationAgentCode(destinationPartner.getPartnerCode());
			}catch(Exception ex){
				System.out.println("\n\n\n\n\n exception while setting partnerCode");
			}
			serviceOrder.setPackingMode(pricingControl.getPacking());
			serviceOrder.setCommodity("HHG");
			serviceOrder.setMode(pricingControl.getMode());
			if(hasOrigin==true && hasDestination==true && hasFreight==true){
				serviceOrder.setServiceType("D/D");
			}
			if(hasOrigin==true && hasDestination==false && hasFreight==false){
				serviceOrder.setServiceType("D/OP");
			}
			if(hasOrigin==true && hasDestination==false && hasFreight==true){
				serviceOrder.setServiceType("D/DP");
			}
			if(hasOrigin==false && hasDestination==true && hasFreight==false){
				serviceOrder.setServiceType("DP/D");
			}
			
			serviceOrder=serviceOrderManager.save(serviceOrder);  
			
			
			
			
	          if(billingManager.getByShipNumber(serviceOrder.getShipNumber()).isEmpty()) 
	           {
	        	   billing = new Billing();
	           }
	           else
	           {
	            billing = billingManager.getByShipNumber(serviceOrder.getShipNumber()).get(0);
	           }
	          
	            
	            if(trackingStatusManager.checkById(serviceOrder.getId()).isEmpty())
	            {
	            	trackingStatus=new TrackingStatus();
	            }
	            
	            
	           
	            miscellaneous.setId(serviceOrder.getId());
	            miscellaneous.setShipNumber(serviceOrder.getShipNumber());
	            miscellaneous.setShip(serviceOrder.getShip());
	            miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
	            miscellaneous.setCorpID(serviceOrder.getCorpID());
	            miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
	            miscellaneous.setEstimateGrossWeight(pricingControl.getWeight());
	            miscellaneous.setEstimateCubicFeet(pricingControl.getVolume());
	            miscellaneous.setEstimateGrossWeightKilo(pricingControl.getWeight().multiply(new BigDecimal(0.4536)));
	            miscellaneous.setEstimateCubicMtr( pricingControl.getVolume().multiply(new BigDecimal(0.0283)));
	            miscellaneous.setUpdatedOn(new Date());
	            miscellaneous.setCreatedOn(new Date());
	            miscellaneous.setCreatedBy(getRequest().getRemoteUser());
	            miscellaneous.setUpdatedBy(getRequest().getRemoteUser());
	            miscellaneous=miscellaneousManager.save(miscellaneous);
	            
	            trackingStatus.setId(serviceOrder.getId());
	            trackingStatus.setCorpID(serviceOrder.getCorpID());
	            trackingStatus.setSitOriginDays(0);
	            trackingStatus.setSitDestinationDays(0);
	            trackingStatus.setFromWH(0);
	            trackingStatus.setDemurrageCostPerDay2(0);
	            trackingStatus.setUsdaCost(0);
	            trackingStatus.setInspectorCost(0);
	            trackingStatus.setXrayCost(0);
	            trackingStatus.setPerdiumCostPerDay(0);
	            trackingStatus.setDemurrageCostPerDay1(0);
	            trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
	        	trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
	        	trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
	            trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
	        	trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
	        	trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
	        	trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
	        	trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
	            trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
	        	trackingStatus.setServiceOrder(serviceOrder);
	            trackingStatus.setMiscellaneous(miscellaneous);
	            try{
	            trackingStatus.setOriginAgentCode(originPartner.getPartnerCode());
	            trackingStatus.setOriginAgent(originPartner.getLastName());
	            }catch(Exception ex){
	            }
	            try{
	            trackingStatus.setDestinationAgent(destinationPartner.getLastName());
	            trackingStatus.setDestinationAgentCode(destinationPartner.getPartnerCode());
	            }catch(Exception ex){
	            	
	            }
	            trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
	        	trackingStatus.setUpdatedOn(new Date());
	        	trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
	        	trackingStatus.setUpdatedBy(getRequest().getRemoteUser());
	        	
	            trackingStatusManager.save(trackingStatus);
	            
	            billing.setShipNumber(serviceOrder.getShipNumber());
    			billing.setSequenceNumber(serviceOrder.getSequenceNumber());
    			billing.setCorpID(sessionCorpID);
        		billing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
        		billing.setBillToName(serviceOrder.getCustomerFile().getBillToName());
        		billing.setNetworkBillToCode(serviceOrder.getCustomerFile().getAccountCode());
		    	billing.setNetworkBillToName(serviceOrder.getCustomerFile().getAccountName());
        		billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization());
        		billing.setBillingId(serviceOrder.getCustomerFile().getBillToCode());
        		billing.setBillName(serviceOrder.getCustomerFile().getBillToName());
        		billing.setBillTo1Point(serviceOrder.getCustomerFile().getBillPayMethod());
        		billing.setContract(serviceOrder.getCustomerFile().getContract());
        		billing.setBillTo2Code("");
        		billing.setPrivatePartyBillingCode("");
        		billing.setNoCharge(false);
        		List instructionList=billingManager.contractBillingInstCode(serviceOrder.getCustomerFile().getContract());
        		if(instructionList!=null && !instructionList.isEmpty() && instructionList.size()>1){
        			String[] instruction=instructionList.get(0).toString().split(":");
        			billing.setBillingInstructionCodeWithDesc(instructionList.get(0).toString());
        			billing.setBillingInstruction(instruction[1].toString());
        			billing.setSpecialInstruction(instruction[1].toString());
        		}
        		sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						billing.setCurrency(systemDefault.getBaseCurrency());
					}
				}
        		billing.setServiceOrder(serviceOrder);
        		billing.setId(serviceOrder.getId());
        		
        		billing.setCreatedOn(serviceOrder.getCreatedOn());
    	   		billing.setUpdatedOn(new Date());
    	   		billing.setCreatedBy(serviceOrder.getCreatedBy());
    	   		billing.setUpdatedBy(getRequest().getRemoteUser());
    	   		
        		billingManager.save(billing);
        		
        		if(pricingControlDetailsOrigin!=null){
        			if(Double.parseDouble((pricingControlDetailsOrigin.getRateMarkUp())) > 0.00){
        				
        				String quote=pricingControlDetailsOrigin.getChargeDetailMarkUp();
        				String individual[]=quote.split(",");
        				int individualArrayLength = individual.length;
        			    for(int j=0;j<individualArrayLength;j++){
        			    	if(!individual[j].trim().equals("")){
        			    		 
        			    		String str=individual[j];
        			    		String chargeCode = str.substring(0,str.indexOf(":")+1);
        			    		str=str.substring(str.indexOf(":")+1,str.length());
        			    		String price="";
        			    		String basis="";
        			    		if(str.contains(" ")){
        			    			price=str.substring(0,str.indexOf(" ")+1);
        			    			basis=str.substring(str.indexOf(" ")+1,str.length());
        			    			if(basis.trim().equals("Flat")){
        			    				basis="flat";
        			    			}
        			    		}else{
        			    			price = str.substring(0,str.length());
        			    			basis="cwt";
        			    		}
        			    		
        			    		
        					    //String st2 = st22.substring(0,st22.indexOf(" "));
        			    		//String st33 = st22.substring(st22.indexOf(" "),st22.length());
        			    		//String st3 = st33.substring(0,st33.length());
        			    		
        			    		
        			    		if(chargeCode.equals("Weight:")){
        			    			chargeCode="ORIGIN";
        			    		}
        			    		if(chargeCode.equals("Customs Clearance:")){
        			    			chargeCode="CLRANCE";
        			    		}
        			    		if(chargeCode.equals("THC:")){
        			    			chargeCode="THC";
        			    		}
        			    		if(chargeCode.equals("Forwarding:")){
        			    			chargeCode="MANFEE";
        			    		}
        			    		if(chargeCode.equals("Hauling:")){
        			    			chargeCode="INLFRT";
        			    		}
        			    		if(chargeCode.equals("Customs / Quarantine:")){
        			    			chargeCode="CUSTOMS";
        			    		}
        			    		//Weight:0
        			    		//THC:275 Flat
        			    		//Hauling:330
        			    		//Forwarding:250.0
        			    		//Customs Clearance:220 Flat
        			    		AccountLine accountLine= new AccountLine();
        			    		boolean activateAccPortal =true;
        			    		sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
        						if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
        							for (SystemDefault systemDefault : sysDefaultDetail) {
        								if(systemDefault.getAccountLineAccountPortalFlag()!=null){
        								accountLineAccountPortalFlag= systemDefault.getAccountLineAccountPortalFlag();
        								}
        							}
        						}
        			    		try{
        			    		if(accountLineAccountPortalFlag){	
        			    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
        			    		accountLine.setActivateAccPortal(activateAccPortal);
        			    		}
        			    		}catch(Exception e){
        			    			
        			    		}
        			    		maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
        			            if ( maxLineNumber.get(0) == null ) {          
        			             	accountLineNumber = "001";
        			             }else {
        			             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
        			                 if((autoLineNumber.toString()).length() == 2) {
        			                 	accountLineNumber = "0"+(autoLineNumber.toString());
        			                 }
        			                 else if((autoLineNumber.toString()).length() == 1) {
        			                 	accountLineNumber = "00"+(autoLineNumber.toString());
        			                 } 
        			                 else {
        			                 	accountLineNumber=autoLineNumber.toString();
        			                 }
        			             }
        			            accountLine.setCorpID(sessionCorpID);
        			            accountLine.setServiceOrderId(serviceOrder.getId());
        			            accountLine.setServiceOrder(serviceOrder);
        						accountLine.setAccountLineNumber(accountLineNumber);
        			    		accountLine.setCategory("Origin");
        			    		accountLine.setStatus(true);
        			    		
        			    		accountLine.setChargeCode(chargeCode);
        						try {
									String ss=chargesManager.findPopulateCostElementData(chargeCode, billing.getContract(),sessionCorpID);
									if(!ss.equalsIgnoreCase("")){
										accountLine.setAccountLineCostElement(ss.split("~")[0]);
										accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

        			    		accountLine.setBasis(basis);
        			    		
        			    		accountLine.setEstimateQuantity(pricingControl.getWeight());
        			    		BigDecimal p=new BigDecimal(price.trim());
        			    		if (basis.trim().equalsIgnoreCase("Flat") || basis.trim().equalsIgnoreCase("Each") ){
        			    			p = p; 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per CWT")){
        			    			p = p.multiply(pricingControl.getWeight().divide(new BigDecimal(100.00),4, RoundingMode.HALF_UP)); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per KG")){
        			    			p = p.multiply(pricingControl.getWeight().multiply(new BigDecimal(0.45359237))); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Included")){
        			    			p = new BigDecimal(0.0); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per CFT")){
        			    			p = p.multiply(pricingControl.getWeight().multiply((pricingControl.getVolume()))); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per CBM")){
        			    			p.multiply(pricingControl.getWeight().multiply((pricingControl.getVolume()))).multiply(new BigDecimal(0.028316)); 
        			    		}
        			    		BigDecimal excR=new BigDecimal(pricingControlDetailsOrigin.getExchangeRate().toString());
        			    		accountLine.setEstimateExpense(p.multiply(excR));
        			    		accountLine.setEstimateRate(new BigDecimal(price.trim()).multiply(new BigDecimal(pricingControlDetailsOrigin.getExchangeRate())).divide(pricingControl.getWeight(),4, RoundingMode.HALF_UP));
        			    		accountLine.setVendorCode(originPartner.getPartnerCode());
        			    		accountLine.setEstimateVendorName(originPartner.getLastName());
        			    		List actgCodeList=accountLineManager.getActgCode(originPartner.getPartnerCode(), "SSC" , sessionCorpID);
        			    		if(!actgCodeList.isEmpty()){
        			    			accountLine.setActgCode(actgCodeList.get(0).toString());
        			    		}
        			    		if(!costElementFlag){
	        			    		List glList=accountLineManager.getGlCode(pricingControl.getContract(), chargeCode, sessionCorpID);
	        			    		if(!glList.isEmpty()){
	        			    			accountLine.setRecGl(glList.get(0).toString());
	        			    		}
	        			    		
	        			    		List expGlList=accountLineManager.getExpGlCode(pricingControl.getContract(), chargeCode, sessionCorpID);
	        			    		if(!expGlList.isEmpty()){
	        			    			accountLine.setPayGl(expGlList.get(0).toString());
	        			    		}
        			    		}else{
        			    			String chargeStr="";
        							List glList = accountLineManager.findChargeDetailFromSO(pricingControl.getContract(),chargeCode,sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
        							if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
        								  chargeStr= glList.get(0).toString();
        							  }
        							   if(!chargeStr.equalsIgnoreCase("")){
        								  String [] chrageDetailArr = chargeStr.split("#");
        								  accountLine.setRecGl(chrageDetailArr[1]);
        								  accountLine.setPayGl(chrageDetailArr[2]);
        								}else{
        								  accountLine.setRecGl("");
        								  accountLine.setPayGl("");
        							  }
        			    		}
        			    		accountLine.setCreatedOn(new Date());
        			    		accountLine.setCreatedBy(getRequest().getRemoteUser());
        			    		accountLine.setUpdatedOn(new Date());
        			    		accountLine.setUpdatedBy(getRequest().getRemoteUser());
        			    		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
        			    		accountLine.setContract(billing.getContract());
        			    		accountLine=accountLineManager.save(accountLine);
        			    		updateAccountLine(serviceOrder);  
        			    		}
        			    		
        			    		//System.out.println("\n\n\n\n count---->"+individual[j]);
        			    	}
        					
        				}
        			}
        		
        		if(pricingControlDetailsDestination!=null){
        			if(Double.parseDouble(pricingControlDetailsDestination.getRateMarkUp()) > 0.00){
        				
        				String quote=pricingControlDetailsDestination.getChargeDetailMarkUp();
        				// Weight: 1950.0,THC:250.0 Flat ,Customs Clearance:0.0 Included ,,Hauling:0.0
        				String individual[]=quote.split(",");
        				int individualArrayLength = individual.length;
        			    for(int j=0;j<individualArrayLength;j++){
        			    	if(!individual[j].trim().equals("")){
        			    		 
        			    		String str=individual[j];
        			    		String chargeCode = str.substring(0,str.indexOf(":")+1);
        			    		str=str.substring(str.indexOf(":")+1,str.length());
        			    		String price="";
        			    		String basis="";
        			    		if(str.contains(" ")){
        			    			price=str.substring(0,str.indexOf(" ")+1);
        			    			basis=str.substring(str.indexOf(" ")+1,str.length());
        			    		}else{
        			    			price = str.substring(0,str.length());
        			    			basis="CWT";
        			    		}
        			    		Boolean createAccountLine=true;
        			    		
        			    		if(pricingControlDetailsDestination.getDthcFlag()==true && chargeCode.equals("THC:")){
        			    			createAccountLine=false;
        			    		}
        					    //String st2 = st22.substring(0,st22.indexOf(" "));
        			    		//String st33 = st22.substring(st22.indexOf(" "),st22.length());
        			    		//String st3 = st33.substring(0,st33.length());
        			    		
        			    		
        			    		if(chargeCode.equals("Weight:")){
        			    			chargeCode="DESTIN";
        			    		}
        			    		if(chargeCode.equals("Customs Clearance:")){
        			    			chargeCode="CLRANCE";
        			    		}
        			    		if(chargeCode.equals("THC:")){
        			    			chargeCode="THC";
        			    		}
        			    		if(chargeCode.equals("Forwarding:")){
        			    			chargeCode="MANFEE";
        			    		}
        			    		if(chargeCode.equals("Hauling:")){
        			    			chargeCode="INLFRT";
        			    		}  
        			    		
        			    		
        			    		//Weight:0
        			    		//THC:275 Flat
        			    		//Hauling:330
        			    		//Forwarding:250.0
        			    		//Customs Clearance:220 Flat
        			    		if(createAccountLine==true){
        			    		AccountLine accountLine= new AccountLine();
        			    		boolean activateAccPortal =true;
        			    		sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
        						if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
        							for (SystemDefault systemDefault : sysDefaultDetail) {
        								if(systemDefault.getAccountLineAccountPortalFlag()!=null){
        								accountLineAccountPortalFlag= systemDefault.getAccountLineAccountPortalFlag();
        								}
        							}
        						}
        			    		try{
        			    		if(accountLineAccountPortalFlag){	
        			    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
        			    		accountLine.setActivateAccPortal(activateAccPortal);
        			    		}
        			    		}catch(Exception e){
        			    			
        			    		}
        			    		maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
        			            if ( maxLineNumber.get(0) == null ) {          
        			             	accountLineNumber = "001";
        			             }else {
        			             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
        			                 if((autoLineNumber.toString()).length() == 2) {
        			                 	accountLineNumber = "0"+(autoLineNumber.toString());
        			                 }
        			                 else if((autoLineNumber.toString()).length() == 1) {
        			                 	accountLineNumber = "00"+(autoLineNumber.toString());
        			                 } 
        			                 else {
        			                 	accountLineNumber=autoLineNumber.toString();
        			                 }
        			             }
        			            accountLine.setCorpID(sessionCorpID);
        			            accountLine.setServiceOrderId(serviceOrder.getId());
        			            accountLine.setServiceOrder(serviceOrder);
        						accountLine.setAccountLineNumber(accountLineNumber);
        			    		accountLine.setCategory("Destin");
        			    		accountLine.setStatus(true);
        			    		
        			    		accountLine.setChargeCode(chargeCode);
        						try {
									String ss=chargesManager.findPopulateCostElementData(chargeCode, billing.getContract(),sessionCorpID);
									if(!ss.equalsIgnoreCase("")){
										accountLine.setAccountLineCostElement(ss.split("~")[0]);
										accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
        			    		
        			    		accountLine.setBasis(basis);
        			    		
        			    		accountLine.setEstimateQuantity(pricingControl.getWeight());
        			    		BigDecimal p=new BigDecimal(price.trim());
        			    		if (basis.trim().equalsIgnoreCase("Flat") || basis.trim().equalsIgnoreCase("Each") ){
        			    			p = p; 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per CWT")){
        			    			p = p.multiply(pricingControl.getWeight().divide(new BigDecimal(100.00),4, RoundingMode.HALF_UP)); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per KG")){
        			    			p = p.multiply(pricingControl.getWeight().multiply(new BigDecimal(0.45359237))); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Included")){
        			    			p = new BigDecimal(0.0); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per CFT")){
        			    			p = p.multiply(pricingControl.getWeight().multiply((pricingControl.getVolume()))); 
        			    		}
        			    		if (basis.trim().equalsIgnoreCase("Per CBM")){
        			    			p.multiply(pricingControl.getWeight().multiply((pricingControl.getVolume()))).multiply(new BigDecimal(0.028316)); 
        			    		}
        			    		BigDecimal excR=new BigDecimal(pricingControlDetailsDestination.getExchangeRate().toString());
        			    		accountLine.setEstimateExpense(p.multiply(excR));
        			    		accountLine.setEstimateRate(new BigDecimal(price.trim()).multiply(new BigDecimal(pricingControlDetailsDestination.getExchangeRate())).divide(pricingControl.getWeight(),4, RoundingMode.HALF_UP));
        			    		accountLine.setVendorCode(destinationPartner.getPartnerCode());
        			    		accountLine.setEstimateVendorName(destinationPartner.getLastName());
        			    		List actgCodeList=accountLineManager.getActgCode(destinationPartner.getPartnerCode(), "SSC" , sessionCorpID);
        			    		if(!actgCodeList.isEmpty()){
        			    			accountLine.setActgCode(actgCodeList.get(0).toString());
        			    		}
        			    		
        			    		List glList=accountLineManager.getGlCode(pricingControl.getContract(), chargeCode, sessionCorpID);
        			    		if(!glList.isEmpty()){
        			    			accountLine.setRecGl(glList.get(0).toString());
        			    		}
        			    		
        			    		List expGlList=accountLineManager.getExpGlCode(pricingControl.getContract(), chargeCode, sessionCorpID);
        			    		if(!expGlList.isEmpty()){
        			    			accountLine.setPayGl(expGlList.get(0).toString());
        			    		}
        			    		
        			    		
        			    		accountLine.setCreatedOn(new Date());
        			    		accountLine.setCreatedBy(getRequest().getRemoteUser());
        			    		accountLine.setUpdatedOn(new Date());
        			    		accountLine.setUpdatedBy(getRequest().getRemoteUser());
        			    		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
        			    		accountLine.setContract(billing.getContract());
        			    		accountLine=accountLineManager.save(accountLine);
        			    		updateAccountLine(serviceOrder);  
        			    		}
        			    	}
        			    		
        			    		//System.out.println("\n\n\n\n count---->"+individual[j]);
        			    	}
        					
        				}
        			}
        			pricingControlManager.updateCustomerFileNumber(pricingControl.getId(), customerFile.getSequenceNumber(),serviceOrder.getShipNumber(),serviceOrder.getId(), sessionCorpID);
        		}
	    	
        		
	   
		return SUCCESS;
	}
	
	public String updateAccountLine( ServiceOrder serviceOrder){ 
    	BigDecimal cuDivisormar=new BigDecimal(100);
    	
		serviceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setDistributedTotalAmount(new BigDecimal((accountLineManager.getdistributedAmountSum(serviceOrder.getShipNumber())).get(0).toString()));
		BigDecimal divisormarEst=new BigDecimal(((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString())); 
		if(serviceOrder.getEstimatedTotalRevenue()==null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000")) ){
			serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
		} 
		else 
		{
		serviceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEst,2));
		} 
		serviceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		serviceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		serviceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		BigDecimal divisormarEstPer=new BigDecimal(((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		if(serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))){
			serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
		}
		else{
		serviceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
		} 
		serviceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setActualExpense(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setActualRevenue(new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		BigDecimal divisormar=new BigDecimal(((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		if(serviceOrder.getActualRevenue()== null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))||serviceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getActualRevenue().equals(new BigDecimal("0.0000")) ){
			serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
		}
		else{
		   serviceOrder.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(divisormar,2));
		}
		/*# 7784 - Always show actual gross margin in accountline overview Start*/
		try{
		serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
		if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
			serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));
		}else{
			serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
		}
		}catch(Exception e){}
		/*# 7784 - Always show actual gross margin in accountline overview End*/		
		serviceOrderManager.updateFromAccountLine(serviceOrder);
    	
    		return SUCCESS;
    }
	public String emailPage(){
		pricingControl=pricingControlManager.get(id);
		fromEmailAddress=userEmail;
		return SUCCESS;
	}
	
	public String saveEmailPage(){
		pricingControl=pricingControlManager.get(id);
		try {
			String emailAdd = "";
			List rateDeptEmail = pricingControlManager.findRateDeptEmailAddress(sessionCorpID);
			try{
				List pricingControlDetailsOriginList=pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Origin", sessionCorpID);
	    		if(!pricingControlDetailsOriginList.isEmpty()){
	    			pricingControlDetailsOrigin=(PricingControlDetails)pricingControlDetailsOriginList.get(0);
	    			originPartner=partnerManager.get(Long.parseLong(pricingControlDetailsOrigin.getPartnerID()));
	    		}
	    	}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n exception in Action Origin"); 
			}
			try{
				List  pricingControlDetailsDestinationList=pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Destination", sessionCorpID);
				if(!pricingControlDetailsDestinationList.isEmpty()){
					pricingControlDetailsDestination=(PricingControlDetails)pricingControlDetailsDestinationList.get(0);
					destinationPartner=partnerManager.get(Long.parseLong(pricingControlDetailsDestination.getPartnerID()));
				}
				
			}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n exception in Action destination");
			}
			try{
				List pricingControlDetailsFreightList=pricingControlDetailsManager.getSelectedAgent(pricingControl.getId(), "Freight", sessionCorpID);
				if(!pricingControlDetailsFreightList.isEmpty()){
					pricingControlDetailsFreight=(PricingControlDetails)pricingControlDetailsFreightList.get(0);
					refFreightRates=(RefFreightRates)pricingControlDetailsManager.getSelectedRefFreight(pricingControl.getId(), "Freight", sessionCorpID).get(0);
				}
				
			}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n exception in Action Freight"); 
			}
			if (!(rateDeptEmail == null) && (!rateDeptEmail.isEmpty())) {
				emailAdd = rateDeptEmail.get(0).toString().trim();
			}
			
			String from = fromEmailAddress;
			String subject="";
			String tempRecipient="";
			String tempRecipientArr[]=emailAdd.split(",");
			for(String str1:tempRecipientArr){
				if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
					tempRecipient += str1;
				}
			}
			emailAdd=tempRecipient;
			subject="Pricing Wizard Query from '" + getRequest().getRemoteUser() + "'" +" for " + pricingControl.getPricingID();
			String messageText1 = "The basic informations are as below:";
			messageText1=messageText1+ "\n\nCustomer Name: "+pricingControl.getCustomerName() +",  Expected Load Date: "+pricingControl.getExpectedLoadDate();
			messageText1=messageText1+ "\nMode: "+pricingControl.getMode() + ", Packing Mode: "+pricingControl.getPacking();
			messageText1=messageText1+ "\nWeight: "+pricingControl.getWeight()+ ", Volume: "+pricingControl.getVolume();
			messageText1=messageText1+ "\nOrigin City: "+pricingControl.getOriginCity()+ ", Origin Country: "+pricingControl.getOriginCountry();
			messageText1=messageText1+ "\nOrigin State: "+pricingControl.getOriginState()+ ", Port of lading: "+pricingControl.getOriginPOE()+"("+pricingControl.getOriginPOEName()+")";
			messageText1=messageText1+ "\nDestination City: "+pricingControl.getDestinationCity()+ ", Destination Country: "+pricingControl.getDestinationCountry();
			messageText1=messageText1+ "\n\nDestination State: "+pricingControl.getDestinationState()+ ", Port of entry: "+pricingControl.getDestinationPOE()+"("+pricingControl.getDestinationPOEName()+")";
			try{
			messageText1=messageText1+ "\n\nOrigin Quote: " + Double.parseDouble(pricingControlDetailsOrigin.getRateMarkUp())*pricingControlDetailsOrigin.getExchangeRate();
			}catch(Exception ex){}
			
			try{
				messageText1=messageText1+ "\n\nDestination Quote: " + Double.parseDouble(pricingControlDetailsDestination.getRateMarkUp())*pricingControlDetailsDestination.getExchangeRate();
			}catch(Exception ex){}
			try{
				messageText1=messageText1+ "\n\nFreight Quote: " + (refFreightRates.getTotalPrice()).multiply(new BigDecimal(pricingControlDetailsFreight.getExchangeRate().toString()));
			}catch(Exception ex){}	
			messageText1=messageText1+ "\n\nPricing Wizard URL(for this record): " + "https://www.skyrelo.com/redsky/agentPricingBasicInfo.html?id="+pricingControl.getId();
			
			messageText1=messageText1+ "\n\n" + emailMessage;
			
			emailSetupManager.globalEmailSetupProcess(from, emailAdd, "", "", "", messageText1, subject, sessionCorpID,"","","");
		} catch (Exception ex) {

		}
		hitFlag="1";
		return SUCCESS;
	}
	
	public String checkControlFlag(){
		List checkControlFlag=pricingControlManager.checkControlFlag(sequenceNumber, sessionCorpID);
		/* Changes By Kunal Sharma In process of Code Evaluation With Sandeep De */
		if(!checkControlFlag.isEmpty() && checkControlFlag.size() > 0){
			totalRevenue=checkControlFlag.get(0).toString();
		}
		/* Changes Ends Here */
		return SUCCESS;
	}
	
	public String recentTariffs(){
		recentTariffsList=pricingControlManager.getRecentTariffs(sessionCorpID);
		return SUCCESS;
	}
	private String let;
	private String freightOriginPortCode;
	private String freightDestinationPortCode;
	private String freightOriginPortCodeName;
	private String freightDestinationPortCodeName;
	public String searchRecentTariffs(){
		recentTariffsList=pricingControlManager.searchRecentTariffs(sessionCorpID, let);
		return SUCCESS;
	}
	
	public String recalculatePricing(){
		pricingControl=pricingControlManager.get(id);
		pricingControlManager.updatePricingControlPorts(pricingControl.getId(),freightOriginPortCode,freightDestinationPortCode,sessionCorpID,freightOriginPortCodeName,freightDestinationPortCodeName);
		String packing="";
		String tarrifApplicability="";
		
		if(pricingControl.getMode().equalsIgnoreCase("Air"))
		{
			packing="AIR";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("LOOSE") && pricingControl.getContainerSize().equalsIgnoreCase("20"))
		{
			packing="L20";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("LOOSE") && pricingControl.getContainerSize().equalsIgnoreCase("40"))
		{
			packing="L40";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("BKBLK") && pricingControl.getLclFclFlag().equalsIgnoreCase("lcl"))
		{
			packing="LCL";
		}
		if(pricingControl.getMode().equalsIgnoreCase("Sea") && pricingControl.getPacking().equalsIgnoreCase("BKBLK") && pricingControl.getLclFclFlag().equalsIgnoreCase("fcl"))
		{
			packing="FCL";
		}
		try{
			
			boolean applyCompletemarkUp=true;
			if(pricingControl.getIsOrigin()==true && pricingControl.getIsDestination()==true){
				applyCompletemarkUp=pricingControlManager.checkMarkUp(pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), pricingControl.getDestinationLatitude(),pricingControl.getDestinationLongitude(),tarrifApplicability, pricingControl.getOriginCountry(), pricingControl.getDestinationCountry(),packing, freightOriginPortCode, freightDestinationPortCode,sessionCorpID,  pricingControl.getExpectedLoadDate(), pricingControl.getContract());
				//System.out.println("\n\n\n\n\n applyCompletemarkUp------>"+applyCompletemarkUp); 
			}
		
		if(pricingControl.getIsOrigin())
		{
			tarrifApplicability="Origin";
			pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
			//List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), tarrifApplicability, pricingControl.getOriginCountry(), packing, pricingControl.getOriginPOE(), sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp);
			List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getOriginLatitude(), pricingControl.getOriginLongitude(), tarrifApplicability, pricingControl.getOriginCountry(), packing, freightOriginPortCode, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp,freightOriginPortCode);
			pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Origin", sessionCorpID,pricingControl.getIsFreight());
		}
		if(pricingControl.getIsDestination())
		{
			tarrifApplicability="Destination";
			pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
			List masterListAgent=pricingControlManager.searchAgents(pricingControl.getWeight(), pricingControl.getDestinationLatitude(), pricingControl.getDestinationLongitude(), tarrifApplicability, pricingControl.getDestinationCountry(), packing, freightDestinationPortCode, sessionCorpID, pricingControl.getId(),pricingControl.getCreatedOn(), pricingControl.getCreatedBy(), pricingControl.getUpdatedBy(), pricingControl.getUpdatedOn(), pricingControl.getExpectedLoadDate(), pricingControl.getVolume(),pricingControl.getBaseCurrency(),pricingControl.getContract(),applyCompletemarkUp,freightDestinationPortCode);
			pricingControlDetailsManager.selectMinimumQuoteAgent(pricingControl.getId(), "Destination", sessionCorpID,pricingControl.getIsFreight());
		}
		
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n\n exception----->>>>"+ex);
			ex.printStackTrace();
		}
		
		if(pricingControl.getIsFreight()){
			tarrifApplicability="Freight";
			pricingControlManager.deletePricingDetails(pricingControl.getId(), tarrifApplicability);
			pricingControlDetails= new PricingControlDetails();
			pricingControlDetails.setCorpID(sessionCorpID);
			pricingControlDetails.setPricingControlID(pricingControl.getId().toString());
			pricingControlDetails.setTarrifApplicability(tarrifApplicability);
			pricingControlDetails.setCreatedBy(getRequest().getRemoteUser());
			pricingControlDetails.setUpdatedBy(getRequest().getRemoteUser());
			pricingControlDetails.setCreatedOn(new Date());
			pricingControlDetails.setUpdatedOn(new Date());
			pricingControlDetails.setBaseCurrency(pricingControl.getBaseCurrency());
			pricingControlDetails.setFreightMarkUpControl(true);
			pricingControlDetails=pricingControlDetailsManager.save(pricingControlDetails);
			pricingControlManager.updateFreightResult(freightId, pricingControl.getId(), "Freight", freightCurrency, pricingControl.getBaseCurrency(), pricingControl.getMode(),pricingControl.getContract(),freightPrice);
		}
		//agentPricingSummaryInfo();
		hitFlag="100";
		return SUCCESS;
	}
	private Long originAgentRecordId;
	private Long destinationAgentRecordId;
	private Long freightRecordId;
	
	public String updateRespectiveRecords(){
		pricingControlDetailsManager.updateRespectiveRecords(originAgentRecordId,destinationAgentRecordId, freightRecordId,pricingControlID);
		return SUCCESS;
	}
	
	public PricingControl getPricingControl() {
		return pricingControl;
	}


	public void setPricingControl(PricingControl pricingControl) {
		this.pricingControl = pricingControl;
	}


	public String getSessionCorpID() {
		return sessionCorpID;
	}


	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setPricingControlManager(PricingControlManager pricingControlManager) {
		this.pricingControlManager = pricingControlManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getPricingControlList() {
		return pricingControlList;
	}

	public void setPricingControlList(List pricingControlList) {
		this.pricingControlList = pricingControlList;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public void setPkmode(Map<String, String> pkmode) {
		this.pkmode = pkmode;
	}

	public Map<String, String> getEQUIP() {
		return EQUIP;
	}

	public void setEQUIP(Map<String, String> equip) {
		EQUIP = equip;
	}

	public Long getId() {
		return id;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public String getPopup() {
		return popup;
	}

	public void setPopup(String popup) {
		this.popup = popup;
	}

	public List getAddressList() {
		return addressList;
	}

	public void setAddressList(List addressList) {
		this.addressList = addressList;
	}

	public String getTariffApplicability() {
		return tariffApplicability;
	}

	public void setTariffApplicability(String tariffApplicability) {
		this.tariffApplicability = tariffApplicability;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public String getWeightType() {
		return weightType;
	}

	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public SignUpService getSignUpService() {
		return signUpService;
	}

	public void setSignUpService(SignUpService signUpService) {
		this.signUpService = signUpService;
	}

	public Map<String, String> getOstates() {
		return ostates;
	}

	public void setOstates(Map<String, String> ostates) {
		this.ostates = ostates;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getNavigation() {
		return navigation;
	}

	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}

	public String getFIDI() {
		return FIDI;
	}

	public void setFIDI(String fidi) {
		FIDI = fidi;
	}

	
	

	public String getDOS() {
		return DOS;
	}

	public void setDOS(String dos) {
		DOS = dos;
	}

	public String getFAIM() {
		return FAIM;
	}

	public void setFAIM(String faim) {
		FAIM = faim;
	}

	public String getGSA() {
		return GSA;
	}

	public void setGSA(String gsa) {
		GSA = gsa;
	}

	public String getISO1400() {
		return ISO1400;
	}

	public void setISO1400(String iso1400) {
		ISO1400 = iso1400;
	}

	public String getISO27001() {
		return ISO27001;
	}

	public void setISO27001(String iso27001) {
		ISO27001 = iso27001;
	}

	public String getISO9002() {
		return ISO9002;
	}

	public void setISO9002(String iso9002) {
		ISO9002 = iso9002;
	}

	public String getMilitary() {
		return Military;
	}

	public void setMilitary(String military) {
		Military = military;
	}

	public String getOMNI() {
		return OMNI;
	}

	public void setOMNI(String omni) {
		OMNI = omni;
	}

	public String getRIM() {
		return RIM;
	}

	public void setRIM(String rim) {
		RIM = rim;
	}

	public void setPricingControlDetailsManager(
			PricingControlDetailsManager pricingControlDetailsManager) {
		this.pricingControlDetailsManager = pricingControlDetailsManager;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public Long getPricingDetailsId() {
		return pricingDetailsId;
	}

	public void setPricingDetailsId(Long pricingDetailsId) {
		this.pricingDetailsId = pricingDetailsId;
	}

	public Long getPricingControlID() {
		return pricingControlID;
	}

	public void setPricingControlID(Long pricingControlID) {
		this.pricingControlID = pricingControlID;
	}

	public String getTarrifApplicability() {
		return tarrifApplicability;
	}

	public void setTarrifApplicability(String tarrifApplicability) {
		this.tarrifApplicability = tarrifApplicability;
	}

	

	public String getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public List getFeedbackRating() {
		return feedbackRating;
	}

	public void setFeedbackRating(List feedbackRating) {
		this.feedbackRating = feedbackRating;
	}

	public String getMinimumFeedbackRating() {
		return minimumFeedbackRating;
	}

	public void setMinimumFeedbackRating(String minimumFeedbackRating) {
		this.minimumFeedbackRating = minimumFeedbackRating;
	}

	public PricingControlDetails getPricingControlDetails() {
		return pricingControlDetails;
	}

	public void setPricingControlDetails(PricingControlDetails pricingControlDetails) {
		this.pricingControlDetails = pricingControlDetails;
	}

	public PricingControlDetails getPricingControlDetailsOrigin() {
		return pricingControlDetailsOrigin;
	}

	public void setPricingControlDetailsOrigin(
			PricingControlDetails pricingControlDetailsOrigin) {
		this.pricingControlDetailsOrigin = pricingControlDetailsOrigin;
	}

	public PricingControlDetails getPricingControlDetailsDestination() {
		return pricingControlDetailsDestination;
	}

	public void setPricingControlDetailsDestination(
			PricingControlDetails pricingControlDetailsDestination) {
		this.pricingControlDetailsDestination = pricingControlDetailsDestination;
	}

	public Partner getDestinationPartner() {
		return destinationPartner;
	}

	public void setDestinationPartner(Partner destinationPartner) {
		this.destinationPartner = destinationPartner;
	}

	public Partner getOriginPartner() {
		return originPartner;
	}

	public void setOriginPartner(Partner originPartner) {
		this.originPartner = originPartner;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public List getContract() {
		return contract;
	}

	public void setContract(List contract) {
		this.contract = contract;
	}

	public List getAgentFreightList() {
		return agentFreightList;
	}

	public void setAgentFreightList(List agentFreightList) {
		this.agentFreightList = agentFreightList;
	}

	public Long getFreightId() {
		return freightId;
	}

	public void setFreightId(Long freightId) {
		this.freightId = freightId;
	}

	public PricingControlDetails getPricingControlDetailsFreight() {
		return pricingControlDetailsFreight;
	}

	public void setPricingControlDetailsFreight(
			PricingControlDetails pricingControlDetailsFreight) {
		this.pricingControlDetailsFreight = pricingControlDetailsFreight;
	}

	public RefFreightRates getRefFreightRates() {
		return refFreightRates;
	}

	public void setRefFreightRates(RefFreightRates refFreightRates) {
		this.refFreightRates = refFreightRates;
	}

	public String getSelectedFreightId() {
		return selectedFreightId;
	}

	public void setSelectedFreightId(String selectedFreightId) {
		this.selectedFreightId = selectedFreightId;
	}

	public Map<String, String> getCurrency() {
		return currency;
	}

	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}

	
	public String getFreightCurrency() {
		return freightCurrency;
	}

	public void setFreightCurrency(String freightCurrency) {
		this.freightCurrency = freightCurrency;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Map<String, String> getScope() {
		return scope;
	}

	public void setScope(Map<String, String> scope) {
		this.scope = scope;
	}

	public String getUserCheck() {
		return userCheck;
	}

	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Map<String, String> getDstates() {
		return dstates;
	}

	public void setDstates(Map<String, String> dstates) {
		this.dstates = dstates;
	}

	public String getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public String getFromEmailAddress() {
		return fromEmailAddress;
	}

	public void setFromEmailAddress(String fromEmailAddress) {
		this.fromEmailAddress = fromEmailAddress;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public List getRecentTariffsList() {
		return recentTariffsList;
	}

	public void setRecentTariffsList(List recentTariffsList) {
		this.recentTariffsList = recentTariffsList;
	}

	public Boolean getDthcFlag() {
		return dthcFlag;
	}

	public void setDthcFlag(Boolean dthcFlag) {
		this.dthcFlag = dthcFlag;
	}

	public String getShipnumber() {
		return shipnumber;
	}

	public void setShipnumber(String shipnumber) {
		this.shipnumber = shipnumber;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getPricingNotes() {
		return pricingNotes;
	}

	public void setPricingNotes(String pricingNotes) {
		this.pricingNotes = pricingNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public Notes getNotes() {
		return notes;
	}

	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	public String getLet() {
		return let;
	}

	public void setLet(String let) {
		this.let = let;
	}

	public String getFreightPrice() {
		return freightPrice;
	}

	public void setFreightPrice(String freightPrice) {
		this.freightPrice = freightPrice;
	}

	public Boolean getFreightMarkUpFlag() {
		return freightMarkUpFlag;
	}

	public void setFreightMarkUpFlag(Boolean freightMarkUpFlag) {
		this.freightMarkUpFlag = freightMarkUpFlag;
	}

	public String getFreightDestinationPortCode() {
		return freightDestinationPortCode;
	}

	public void setFreightDestinationPortCode(String freightDestinationPortCode) {
		this.freightDestinationPortCode = freightDestinationPortCode;
	}

	public String getFreightOriginPortCode() {
		return freightOriginPortCode;
	}

	public void setFreightOriginPortCode(String freightOriginPortCode) {
		this.freightOriginPortCode = freightOriginPortCode;
	}

	public String getFreightDestinationPortCodeName() {
		return freightDestinationPortCodeName;
	}

	public void setFreightDestinationPortCodeName(
			String freightDestinationPortCodeName) {
		this.freightDestinationPortCodeName = freightDestinationPortCodeName;
	}

	public String getFreightOriginPortCodeName() {
		return freightOriginPortCodeName;
	}

	public void setFreightOriginPortCodeName(String freightOriginPortCodeName) {
		this.freightOriginPortCodeName = freightOriginPortCodeName;
	}

	public void setPricingFreightManager(PricingFreightManager pricingFreightManager) {
		this.pricingFreightManager = pricingFreightManager;
	}

	public List getAgentMasterList() {
		return agentMasterList;
	}

	public void setAgentMasterList(List agentMasterList) {
		this.agentMasterList = agentMasterList;
	}

	public void setRefFreightRatesManager(
			RefFreightRatesManager refFreightRatesManager) {
		this.refFreightRatesManager = refFreightRatesManager;
	}

	public Long getDestinationAgentRecordId() {
		return destinationAgentRecordId;
	}

	public void setDestinationAgentRecordId(Long destinationAgentRecordId) {
		this.destinationAgentRecordId = destinationAgentRecordId;
	}

	public Long getFreightRecordId() {
		return freightRecordId;
	}

	public void setFreightRecordId(Long freightRecordId) {
		this.freightRecordId = freightRecordId;
	}

	public Long getOriginAgentRecordId() {
		return originAgentRecordId;
	}

	public void setOriginAgentRecordId(Long originAgentRecordId) {
		this.originAgentRecordId = originAgentRecordId;
	}

	public String getDestinationDOS() {
		return destinationDOS;
	}

	public void setDestinationDOS(String destinationDOS) {
		this.destinationDOS = destinationDOS;
	}

	public String getDestinationFAIM() {
		return destinationFAIM;
	}

	public void setDestinationFAIM(String destinationFAIM) {
		this.destinationFAIM = destinationFAIM;
	}

	public String getDestinationFIDI() {
		return destinationFIDI;
	}

	public void setDestinationFIDI(String destinationFIDI) {
		this.destinationFIDI = destinationFIDI;
	}

	public String getDestinationGSA() {
		return destinationGSA;
	}

	public void setDestinationGSA(String destinationGSA) {
		this.destinationGSA = destinationGSA;
	}

	public String getDestinationISO1400() {
		return destinationISO1400;
	}

	public void setDestinationISO1400(String destinationISO1400) {
		this.destinationISO1400 = destinationISO1400;
	}

	public String getDestinationISO27001() {
		return destinationISO27001;
	}

	public void setDestinationISO27001(String destinationISO27001) {
		this.destinationISO27001 = destinationISO27001;
	}

	public String getDestinationISO9002() {
		return destinationISO9002;
	}

	public void setDestinationISO9002(String destinationISO9002) {
		this.destinationISO9002 = destinationISO9002;
	}

	public String getDestinationMilitary() {
		return destinationMilitary;
	}

	public void setDestinationMilitary(String destinationMilitary) {
		this.destinationMilitary = destinationMilitary;
	}

	public String getDestinationMinimumFeedbackRating() {
		return destinationMinimumFeedbackRating;
	}

	public void setDestinationMinimumFeedbackRating(
			String destinationMinimumFeedbackRating) {
		this.destinationMinimumFeedbackRating = destinationMinimumFeedbackRating;
	}

	public String getDestinationOMNI() {
		return destinationOMNI;
	}

	public void setDestinationOMNI(String destinationOMNI) {
		this.destinationOMNI = destinationOMNI;
	}

	public String getDestinationRIM() {
		return destinationRIM;
	}

	public void setDestinationRIM(String destinationRIM) {
		this.destinationRIM = destinationRIM;
	}

	public String getOriginDOS() {
		return originDOS;
	}

	public void setOriginDOS(String originDOS) {
		this.originDOS = originDOS;
	}

	public String getOriginFAIM() {
		return originFAIM;
	}

	public void setOriginFAIM(String originFAIM) {
		this.originFAIM = originFAIM;
	}

	public String getOriginFIDI() {
		return originFIDI;
	}

	public void setOriginFIDI(String originFIDI) {
		this.originFIDI = originFIDI;
	}

	public String getOriginGSA() {
		return originGSA;
	}

	public void setOriginGSA(String originGSA) {
		this.originGSA = originGSA;
	}

	public String getOriginISO1400() {
		return originISO1400;
	}

	public void setOriginISO1400(String originISO1400) {
		this.originISO1400 = originISO1400;
	}

	public String getOriginISO27001() {
		return originISO27001;
	}

	public void setOriginISO27001(String originISO27001) {
		this.originISO27001 = originISO27001;
	}

	public String getOriginISO9002() {
		return originISO9002;
	}

	public void setOriginISO9002(String originISO9002) {
		this.originISO9002 = originISO9002;
	}

	public String getOriginMilitary() {
		return originMilitary;
	}

	public void setOriginMilitary(String originMilitary) {
		this.originMilitary = originMilitary;
	}

	public String getOriginMinimumFeedbackRating() {
		return originMinimumFeedbackRating;
	}

	public void setOriginMinimumFeedbackRating(String originMinimumFeedbackRating) {
		this.originMinimumFeedbackRating = originMinimumFeedbackRating;
	}

	public String getOriginOMNI() {
		return originOMNI;
	}

	public void setOriginOMNI(String originOMNI) {
		this.originOMNI = originOMNI;
	}

	public String getOriginRIM() {
		return originRIM;
	}

	public void setOriginRIM(String originRIM) {
		this.originRIM = originRIM;
	}

	public String getDestinationMarkup() {
		return destinationMarkup;
	}

	public void setDestinationMarkup(String destinationMarkup) {
		this.destinationMarkup = destinationMarkup;
	}

	public String getOriginMarkup() {
		return originMarkup;
	}

	public void setOriginMarkup(String originMarkup) {
		this.originMarkup = originMarkup;
	}

	public PricingFreight getPricingFreight() {
		return pricingFreight;
	}

	public void setPricingFreight(PricingFreight pricingFreight) {
		this.pricingFreight = pricingFreight;
	}

	public String getDTHC() {
		return DTHC;
	}

	public void setDTHC(String dthc) {
		DTHC = dthc;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}

	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}
	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}
	
}
