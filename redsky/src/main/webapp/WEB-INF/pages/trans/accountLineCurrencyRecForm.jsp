<%@ include file="/common/taglibs.jsp"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = userRole+","+role.getName(); 
}
%> 
<title>Pricing Currency Form</title>
<meta name="heading" content="Pricing Currency Form" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
 
</script>


<s:form id="accountLineCurrencyRecForm" name="accountLineCurrencyRecForm" action="" onsubmit="" method="post">

<!-- Code Commented by Ankit for 13835 start-->
 <%if(userRole.contains("ROLE_EXECUTIVE")){ %>
<s:hidden name="userRoleExecutive" value="yes"/>
<%}else{%>
<s:hidden name="userRoleExecutive" value="no"/>
<%}%>
<!-- Code Commented by Ankit for 13835 End -->

<!-- Code Added by Ankit for 13835 start-->
<%-- <c:if test="${fn:contains(userRole, 'ROLE_EXECUTIVE')}">
<s:hidden name="userRoleExecutive" value="yes"/>
</c:if>
<c:if test="${not fn:contains(userRole, 'ROLE_EXECUTIVE')}">
<s:hidden name="userRoleExecutive" value="no"/>
</c:if> --%>
<!-- Code Added by Ankit for 13835 End-->

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="tempAccList"/>
 <s:hidden name="accountLine.actualRevenue"  /> 
 <s:hidden name="accountLine.recQuantity"  />  
 <s:hidden name="accountLine.basis"  />
 <s:hidden name="accountLine.actualDiscount"/> 
  <s:hidden name="accountLine.recRate"/> 
    <s:hidden name="accountLine.recInvoiceNumber"/>
 <s:hidden name="aid" value="${accountLine.id}" />  
<s:hidden name="formStatus"  />
<s:hidden name="billingCurrencyUpdateFlag" value="NO"/>
<div id="layer1" style="width:100%">
  
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Sell&nbsp;Rate&nbsp;Currency&nbsp;Data</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    

<table border="0" style="margin:0px;padding:0px;">
										<c:if test="${contractType}">																											 
										 <tr>
										 <td colspan="9">															 											
										 <table class="detailTabLabel" style="margin:0px;">
										 <tr>															
										 <td width="1px"></td>
										 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
										 <td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.contractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.contractExchangeRate');" headerValue="" tabindex="12"  /></td>
										 <td align="right" width="" class="listwhitetext">Value&nbsp;Date</td>

										 <c:if test="${not empty accountLine.contractValueDate}"> 
										  <s:text id="accountLineFormattedEstimateContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.contractValueDate"/></s:text>
										  <td ><s:textfield id="contractValueDate" name="accountLine.contractValueDate" value="%{accountLineFormattedEstimateContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
										  <img id="contractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										 </td>
										 </c:if>
										     <c:if test="${empty accountLine.contractValueDate}">
										   <td><s:textfield id="contractValueDate" name="accountLine.contractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/>
										   <img id="contractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										   </td>
										     </c:if>
                                            <td  align="right" class="listwhitetext" width="55px">Ex.&nbsp;Rate</td>
										 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.contractExchangeRate"  size="8" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.contractRate','','','');" /></td>   			
                                            <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.contractRate" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.contractRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" /></td>	 
                                            <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.contractRateAmmount" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true"  onblur=""/></td>	 
										 </tr></table>															
										 </td>															
										 </tr>
										</c:if> 
										
</table>
<table style="margin:0px 0px 20px 0px; padding:0px;">

<tr>

													 <c:choose>
													 <c:when test="${contractType}">
													 <td align="right" class="listwhitetext" width="102">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
													 </c:when>
													 <c:otherwise>
													 <td align="right" class="listwhitetext">Currency</td>
													 </c:otherwise>
													 </c:choose>
<td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.recRateCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.recRateExchange');" headerValue="" tabindex="18"  /></td>
<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
<c:if test="${not empty accountLine.racValueDate}"> 
<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.racValueDate"/></s:text>
<td width="110px" ><s:textfield id="racValueDate" name="accountLine.racValueDate" value="%{accountLineFormattedValue}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="racValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<c:if test="${empty accountLine.racValueDate}">
<td width="110px"><s:textfield id="racValueDate" name="accountLine.racValueDate" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="racValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<td  align="right" class="listwhitetext">Ex.&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.recRateExchange" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.contractRate','','','');"/></td>
<td align="right" class="listwhitetext" width="80px">Sell&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.recCurrencyRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.recCurrencyRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" onblur=""/></td>	 
<td align="right" class="listwhitetext" width="86px">Curr&nbsp;Amount</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.actualRevenueForeign" size="8" maxlength="12" readonly="true"/></td>	 
</tr> 
</table>

</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="calculateRecRate();"/></td>
<td><input type="button"  value="Cancel" name="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</s:form>
<script type="text/javascript">
</script>
<script type="text/javascript">
function findExchangeRateRateOnActualization(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
var daym;
var year=mydate.getFullYear()
var y=""+year;
if (year < 1000)
year+=1900
var day=mydate.getDay()
var month=mydate.getMonth()+1
if(month == 1)month="Jan";
if(month == 2)month="Feb";
if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function rateOnActualizationDate(field1,field2,field3){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
	<c:if test="${contractType}">
	var currency=""; 
		var currentDate=currentDateRateOnActualization();
		try{
		currency=document.forms['accountLineCurrencyRecForm'].elements[field1.trim().split('~')[0]].value;
		document.forms['accountLineCurrencyRecForm'].elements[field2.trim().split('~')[0]].value=currentDate;
		document.forms['accountLineCurrencyRecForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}
		try{
		currency=document.forms['accountLineCurrencyRecForm'].elements[field1.trim().split('~')[1]].value;
		document.forms['accountLineCurrencyRecForm'].elements[field2.trim().split('~')[1]].value=currentDate;
		document.forms['accountLineCurrencyRecForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}		
	</c:if>
	</c:if>
} 
<c:if test="${(((not empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber!='' && not empty accountLine.recInvoiceNumber))) || utsiRecAccDateFlag || utsiPayAccDateFlag  ) }"> 
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${ serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">


function calculateRecRate(){ 
	var invoiceNumber = document.forms['accountLineCurrencyRecForm'].elements['accountLine.recInvoiceNumber'].value; 
    var billingCurrencyUpdateFlag = document.forms['accountLineCurrencyRecForm'].elements['billingCurrencyUpdateFlag'].value;       
	var aid=document.forms['accountLineCurrencyRecForm'].elements['aid'].value;
	 window.opener.setFieldValue('actualRevenue'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenue'].value);
	 window.opener.setFieldValue('recRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value);
	 window.opener.setFieldValue('recQuantity'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.recQuantity'].value);
	 var contractTypeCurrency=false;
	 var billingTypeCurrency=false;
	 <c:if test="${contractType}">
	 if(window.opener.document.forms['serviceOrderForm'].elements['contractCurrency'+aid].value!=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractCurrency'].value){
		 contractTypeCurrency=true;	 
	 }
	 window.opener.setFieldValue('contractCurrency'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractCurrency'].value);
	 window.opener.setFieldValue('contractExchangeRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractExchangeRate'].value);
	 window.opener.setFieldValue('contractRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRate'].value);
	 window.opener.setFieldValue('contractRateAmmount'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRateAmmount'].value);
	 window.opener.setFieldValue('contractValueDate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value);
    </c:if> 
    if(window.opener.document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value!=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value){
		 billingTypeCurrency=true;	 
	 }
	 window.opener.setFieldValue('recRateCurrency'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value);
	 window.opener.setFieldValue('recRateExchange'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value);
	 window.opener.setFieldValue('recCurrencyRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.recCurrencyRate'].value);
	 window.opener.setFieldValue('actualRevenueForeign'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value);
	 window.opener.setFieldValue('racValueDate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value);
    if(document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value=='2'){
    window.opener.calculateActualRevenueFromFX(aid,'','FX','','','',billingTypeCurrency,contractTypeCurrency,'Rec');
    window.opener.changeStatus();
	    <c:if test="${multiCurrency=='Y'}">
		    if(billingCurrencyUpdateFlag=='YES'){
		    	invoiceNumber=invoiceNumber+"~"+aid;
		    	window.opener.appendFieldValue('oldRecRateCurrency',invoiceNumber);
		    }
		 </c:if>
    }
		window.close();
}




function changeStatus() {
	   document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value = '2'; 
	}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}	
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)||(keyCode==189)|| (keyCode==110); 
}
function findExchangeRateGlobal(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function updateExchangeRate(currency,field){
    var invoiceNumber = document.forms['accountLineCurrencyRecForm'].elements['accountLine.recInvoiceNumber'].value; 
    invoiceNumber=invoiceNumber.trim();
	if(field=='accountLine.contractExchangeRate'){
		document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value); 
		document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
		var recRateCurrency = document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value;
        if(recRateCurrency==currency.value){
        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractExchangeRate'].value;
        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value;
        }
		calculateRecCurrency('accountLine.contractRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');
	}else if(field=='accountLine.recRateExchange'){
		if(invoiceNumber!=''){	
		    if(document.forms['accountLineCurrencyRecForm'].elements['userRoleExecutive'].value=="no"){
			    alert("Invoice for this line has already been generated, You cannot change Billing Currency.");
			    document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
		    }else if (invoiceNumber!='' && (currency.value==null || currency.value==undefined || currency.value=='' )){
		    	alert("You cannot select blank Currency as Invoice for this line has already been generated. ")
			    document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
		    }else if (document.forms['accountLineCurrencyRecForm'].elements['userRoleExecutive'].value=="yes"){
			    var agree =confirm("Invoice for this line has already been generated and it will change all corresponding value for the invoice no "+invoiceNumber+".\n\n Are you sure you want to continue with the changes?");
			    if(agree) {
	      			document.forms['accountLineCurrencyRecForm'].elements['billingCurrencyUpdateFlag'].value='YES';
	    			document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value); 
	    			document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
	    			var contractCurrency = document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractCurrency'].value;
	    	        if(contractCurrency==currency.value){
	    	        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractExchangeRate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value;
	    	        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value;
	    	        }
	    			calculateRecCurrency('accountLine.contractRate','','','');	      			
			    }else{
				    document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
			    }
		    }
		}else{
			document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value);
			document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
			var contractCurrency = document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractCurrency'].value;
	        if(contractCurrency==currency.value){
	        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractExchangeRate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value;
	        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value;
	        }
			calculateRecCurrency('accountLine.contractRate','','','');
		}
	}else{
		
	}
}
function currentDateGlobal(){
	 var mydate=new Date();
    var daym;
    var year=mydate.getFullYear()
    var y=""+year;
    if (year < 1000)
    year+=1900
    var day=mydate.getDay()
    var month=mydate.getMonth()+1
    if(month == 1)month="Jan";
    if(month == 2)month="Feb";
    if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function calculateRecCurrency(target,field1,field2,field3) {
	 if(field1!='' && field2!='' & field3!=''){
		 rateOnActualizationDate(field1,field2,field3);
		 }	   
	 var basis=document.forms['accountLineCurrencyRecForm'].elements['accountLine.basis'].value;
	 var baseRateVal=1;
	var actualDiscount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualDiscount'].value;	
	var recQuantity=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recQuantity'].value;	
    var actualDiscount=0.00;
    var recRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value;
    try{
   	 actualDiscount=document.forms['serviceOrderForm'].elements['accountLine.actualDiscount'].value;
    }catch(e){}
	 if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
		 recQuantity = document.forms['accountLineCurrencyRecForm'].elements['accountLine.recQuantity'].value=1;
        } 
    if( basis=="cwt" || basis=="%age"){
      	 baseRateVal=100;
    }else if(basis=="per 1000"){
      	 baseRateVal=1000;
    } else {
      	 baseRateVal=1;  	
	 }		
<c:if test="${contractType}">
if(target=='accountLine.contractRate'){
	var contractCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractCurrency'].value;		
	var contractExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractExchangeRate'].value;
	var contractRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRate'].value;
	var contractRateAmmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRateAmmount'].value;
	var contractValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value;
	if(contractCurrency!=''){
		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
		contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRateAmmount'].value=contractRateAmmount;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value=contractRate/contractExchangeRate;
	}
	   var recRateCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value;		
	   var recRateExchange=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value;
	   var recCurrencyRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recCurrencyRate'].value;
	   var actualRevenueForeign=document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value;
	   var racValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value;
	   recRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value;
	   	if(recRateCurrency!=''){
	   		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
	   		recCurrencyRate=recRate*recRateExchange;
	   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.recCurrencyRate'].value=recCurrencyRate;
	   		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeign;   	 		
	}
}else if(target=='accountLine.recCurrencyRate'){
	   var recRateCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value;		
	   var recRateExchange=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value;
	   var recCurrencyRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recCurrencyRate'].value;
	   var actualRevenueForeign=document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value;
	   var racValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value;
	   	if(recRateCurrency!=''){
	   		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
	   		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeign; 
	   	document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value=recCurrencyRate/recRateExchange;	
	}	
	
		var contractCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractCurrency'].value;		
		var contractExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractExchangeRate'].value;
		var contractRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRate'].value;
		var contractRateAmmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRateAmmount'].value;
		var contractValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value;
		recRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value;
	if(contractCurrency!=''){
		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
		contractRate=recRate*contractExchangeRate;
   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRate'].value=contractRate;
   		contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.contractRateAmmount'].value=contractRateAmmount;
	}
}else{

}
</c:if>
<c:if test="${!contractType}">
	var recRateCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateCurrency'].value;		
	var recRateExchange=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRateExchange'].value;
	var recCurrencyRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.recCurrencyRate'].value;
	var actualRevenueForeign=document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value;
	var racValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value;
	if(recRateCurrency!=''){
		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
  		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
	    if(actualDiscount!=0.00){
	    	actualRevenueForeign=(actualRevenueForeign*actualDiscount)/100;
	    }
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeign;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.recRate'].value=recCurrencyRate/recRateExchange; 		
	}
	</c:if>
}
</script>
