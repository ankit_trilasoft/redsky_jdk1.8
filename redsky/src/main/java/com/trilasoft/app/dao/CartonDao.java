/**
 * @Class Name  Carton Dao
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Carton;

public interface CartonDao extends GenericDao<Carton, Long> {   
    public List<Carton> findByLastName(String lastName);  
    public List findMaxId();
    public List checkById(Long id);
    public List findMaximumIdNumber(String shipNum);
    public List getGross(String shipNumber);
    public List getTare(String shipNumber);
    public List getNet(String shipNumber);
    public List getVolume(String shipNumber);
    public List getPieces(String shipNumber);
    public List getWeightUnit(String shipNumber);
    public List getVolumeUnit(String shipNumber);
    public int updateMisc(String shipNumber,String actualGrossWeight,String actualNetWeight,String actualTareWeight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID);
    public List getContainerNumber(String shipNumber);
    public int updateMiscVolume(String shipNumber,String actualCubicFeet,String corpID,String updatedBy);
    public int updateDeleteStatus(Long ids);
    public List getGrossKilo(String shipNumber);
    public List getTareKilo(String shipNumber);
    public List getNetKilo(String shipNumber);
    public List getVolumeCbm(String shipNumber);
    public int updateMiscKilo(String shipNumber,String actualGrossWeightKilo,String actualNetWeightKilo,String actualTareWeightKilo,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID);
    public int updateMiscVolumeCbm(String shipNumber,String actualCubicMtr,String corpID,String updatedBy);
    public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List findMaximumChild(String shipNm);
    public List findMinimumChild(String shipNm);
    public List findCountChild(String shipNm);
    public List cartonList(Long serviceOrderId);
    public Long findRemoteCarton(String idNumber, Long serviceOrderId);
    public void deleteNetworkCarton(Long serviceOrderId, String idNumber, String cartonStatus);
    public List findCartonBySid(Long sofid, String sessionCorpID);
    public List getGrossNetwork(String shipNumber, String corpID);
	public List getNetNetwork(String shipNumber, String corpID);
	public List getVolumeNetwork(String shipNumber, String corpID);
	public List getPiecesNetwork(String shipNumber, String corpID);
	public List getTareNetwork(String shipNumber, String corpID);
	public List cartonListotherCorpId(Long sid);
	public List findCartonTypeValue(String sessionCorpID);
	public String findUnitValuesByCarton(String cartonVal,String sessionCorpID);
	public void updateCartonNetWeightDetails(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName,String sessionCorpID,String userName);
	public void updateCartonDetailsTareWt(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName,String sessionCorpID,String userName);
	public void updateCartonDetailsDensity(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String fieldTypeName,String fieldTypeVal,String tableName,String sessionCorpID,String userName);
	public void updateValueByCartonType(Long id,String fieldUnit1,String fieldUnit1Val,String fieldUnit2,String fieldUnit2Val,String fieldUnit3,String fieldUnit3Val,String fieldLength,String fieldLengthVal,String fieldWidth,String fieldWidthVal,String fieldHeight,String fieldHeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String fieldValueCartonVal ,String tableName,String sessionCorpID,String userName);
	public void updateStatus(Long id, String cartonStatus);
}

//End of Class.   