<%@ include file="/common/taglibs.jsp"%>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>
div.error, span.error, li.error, div.message {
width:450px;
}

 span.pagelinks {
display:block;
font-size:0.85em;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}


form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	


<head> 
<script language="javascript" type="text/javascript" SRC="/scripts/calendar.js"></script>
<script language="JavaScript" type="text/javascript" SRC="/scripts/masks.js"></script>
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
</script>
 <script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['existingContractsList'].elements['refFreightRates.serviceContractNo'].value = "";
			document.forms['existingContractsList'].elements['refFreightRates.carrier'].value = "";
			document.forms['existingContractsList'].elements['refFreightRates.originCountry'].value = "";
			document.forms['existingContractsList'].elements['refFreightRates.destinationCountry'].value = "";
			
} 
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Sea Freight Record");
	var did = targetElement;
		if (agree){
			location.href="deleteSeaFreight.html?id="+did;;
			
		}else{
			return false;
		}
}
</script> 
    <title>Sea Freight Data List</title>   
    <meta name="heading" content="Sea Freight Data List"/>   
</head>  
 <body> 
     
<s:form id="existingContractsList" name="existingContractsList" action="searchSeaFreightData" method="post">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<div id="Layer1" style="width: 100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">		
<table class="table" border="1" style="width:98%">
<thead>
<tr>
<th>service Contract No</th>
<th>Carrier</th>
<th>Origin Country</th>
<th>Destination Country</th>
</tr></thead> 
<tbody>
  <tr>
  <td><s:textfield name="refFreightRates.serviceContractNo" required="true" cssClass="input-text" size="18" /></td>
   <td><s:textfield name="refFreightRates.carrier" required="true" cssClass="input-text" size="18" /></td>
   <td><s:textfield name="refFreightRates.originCountry" required="true" cssClass="input-text" size="18" /></td> 
   <td><s:textfield name="refFreightRates.destinationCountry" required="true" cssClass="input-text" size="18" /></td> 
   </tr>
   <tr>
   <td colspan="3"></td> 
   <td style="border-left: hidden;">
       <s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;" key="button.search"/>  
       <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
   </td> 
  </tr>
  </tbody>
 </table>
 </div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>	
</div>	
</s:form> 
<div id="newmnav">
		  <ul>
		  <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Sea Freight Data List<img src="images/navarrow.gif" align="absmiddle"/></span></a></li> 
		  <li><a href="seaFreightUpdate.html"><span>Sea Freight Update</span></a></li> 
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>		

   <display:table name="userlogFileExt" class="table" requestURI="" id="existingContractsList"  export="true" pagesize="10" style="!margin-top:-15px;">   
   <display:column sortable="true"  title="Service Contract #"  sortProperty="serviceContractNo" style="width: 150px">
       <a href="editSeaFreightData.html?id=${existingContractsList.id}"> <c:out value="${existingContractsList.serviceContractNo}" /></a> 
   </display:column> 
   <display:column property="carrier" sortable="true" title="Carrier"  />
   <display:column property="originCountry" sortable="true" title="Origin Country"  />
   <display:column property="destinationCountry" sortable="true" title="Destination Country"  />
   <display:column property="totalPrice" sortable="true" title="Total Price"  />
	 <display:column title="Remove" style="width:50px">
			<a><img title="Remove" align="middle" onclick="confirmSubmit(${existingContractsList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column> 
	<display:setProperty name="export.excel.filename" value="Sea Freight Data List.xls"/>
	<display:setProperty name="export.csv.filename" value="Sea Freight Data List.csv"/>
</display:table>   
  <table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>		
  <tr>
  <td>
  <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editSeaFreightData.html"/>'"  
        value="<fmt:message key="button.add"/>"/> </td>  
  </tr>
  </tbody></table>
 
 <script type="text/javascript">  
</script>  
</body>
