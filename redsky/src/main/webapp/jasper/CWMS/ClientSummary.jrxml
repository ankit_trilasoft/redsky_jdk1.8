<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ClientSummary" pageWidth="1554" pageHeight="2380" whenNoDataType="AllSectionsNoDetail" columnWidth="1494" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="8782feec-bd45-4705-89fd-2c53d8184a5d">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Invoice Begin Date" class="java.util.Date"/>
	<parameter name="Invoice End Date" class="java.util.Date"/>
	<parameter name="Sales Person / Consultant" class="java.lang.String"/>
	<parameter name="Bill To" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT "ClientSummary-R-xls-CWMS.jrxml",
     concat(if(serviceorder.`firstName` is null,"",serviceorder.`firstName`)," ",if(serviceorder.`lastName` is null,"",serviceorder.`lastName`)) AS 'Shipper Name',
     date(trackingstatus.`loadA`) AS 'Load Date',
     serviceorder.`originCity` AS 'Origin City',
     serviceorder.`originState` AS 'Origin State',
     serviceorder.`destinationCity` AS 'Destination City',
     serviceorder.`destinationState` AS 'Destination State',
     serviceorder.`mode` AS Mode,
     round(sum(if(accountline.`actualRevenue` is null,0.00,round(accountline.`actualRevenue`,2))),2) AS 'Total Invoice amount',
     date(claim.`formRecived`) AS 'Claim Date',
     date(claim.`closeDate`) AS 'Close Date',
     serviceorder.OAEvaluation AS 'OA Quality Score',
     serviceorder.DAEvaluation AS 'DA Quality Score',
     serviceorder.`clientOverallResponse` AS 'Overall Score',
     serviceorder.surveyorEvaluation AS 'Surveyor Evaluation',
     serviceorder.coordinatorEvaluation AS 'Coordinator Evaluation',
     accountline.`billToname` AS accountline_billToname,
     round((if(claim.reciveReimbursement is null,0.00,claim.reciveReimbursement)),2) AS 'Settlement Amount',
     serviceorder.shipnumber as SO,
     accountline.recinvoicenumber as InvoiceNum,
     claim.claimnumber as ClaimNum
FROM
     `serviceorder` serviceorder inner join `trackingstatus` trackingstatus on serviceorder.id=trackingstatus.id and trackingstatus.corpid=$P{Corporate ID}
     inner join `accountline` accountline on serviceorder.id=accountline.serviceorderid and accountline.corpid=$P{Corporate ID}
     left outer join `claim` claim on serviceorder.id=claim.serviceorderid and claim.corpid=$P{Corporate ID}
     left outer join `app_user` app_user on app_user.username=$P{Sales Person / Consultant} and app_user.corpid=$P{Corporate ID}
WHERE serviceorder.corpid=$P{Corporate ID}
and date(accountline.`receivedInvoiceDate`) between $P{Invoice Begin Date} and $P{Invoice End Date}
and accountline.`billToCode` like $P{Bill To}
and serviceorder.`job` in ('INT','STO')
and if(app_user.salesPortalAccess is true,serviceorder.`bookingAgentCode` like app_user.parentAgent,serviceorder.corpid=$P{Corporate ID})
group by serviceorder.`shipNumber`,claim.id
order by serviceorder.`shipNumber`,claim.id]]>
	</queryString>
	<field name="ClientSummary-R-xls-CWMS.jrxml" class="java.lang.String"/>
	<field name="Shipper Name" class="java.lang.String"/>
	<field name="Load Date" class="java.sql.Date"/>
	<field name="Origin City" class="java.lang.String"/>
	<field name="Origin State" class="java.lang.String"/>
	<field name="Destination City" class="java.lang.String"/>
	<field name="Destination State" class="java.lang.String"/>
	<field name="Mode" class="java.lang.String"/>
	<field name="Total Invoice amount" class="java.math.BigDecimal"/>
	<field name="Claim Date" class="java.sql.Date"/>
	<field name="Close Date" class="java.sql.Date"/>
	<field name="OA Quality Score" class="java.lang.String"/>
	<field name="DA Quality Score" class="java.lang.String"/>
	<field name="Overall Score" class="java.lang.String"/>
	<field name="Surveyor Evaluation" class="java.lang.String"/>
	<field name="Coordinator Evaluation" class="java.lang.String"/>
	<field name="accountline_billToname" class="java.lang.String"/>
	<field name="Settlement Amount" class="java.math.BigDecimal"/>
	<field name="SO" class="java.lang.String"/>
	<field name="InvoiceNum" class="java.lang.String"/>
	<field name="ClaimNum" class="java.lang.Long"/>
	<variable name="ClaimCount" class="java.lang.Integer" resetType="Group" resetGroup="SO" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{ClaimNum}]]></variableExpression>
	</variable>
	<variable name="InvoiceAmountSO" class="java.math.BigDecimal" resetType="Group" resetGroup="SO" incrementType="Group" incrementGroup="ClaimNum" calculation="Sum">
		<variableExpression><![CDATA[$V{ClaimCount}.intValue()==0 ? $F{Total Invoice amount} : $F{Total Invoice amount}.divide(new BigDecimal($V{ClaimCount}.intValue()),2)]]></variableExpression>
	</variable>
	<variable name="SettlementAmountSO" class="java.math.BigDecimal" resetType="Group" resetGroup="SO" incrementType="Group" incrementGroup="ClaimNum" calculation="Sum">
		<variableExpression><![CDATA[$F{Settlement Amount}==null ? new BigDecimal(0.00) : $F{Settlement Amount}]]></variableExpression>
	</variable>
	<variable name="TotalInvAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="SO" calculation="Sum">
		<variableExpression><![CDATA[$F{Total Invoice amount}]]></variableExpression>
	</variable>
	<variable name="TotalSettlementAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="SO" calculation="Sum">
		<variableExpression><![CDATA[$V{SettlementAmountSO}]]></variableExpression>
	</variable>
	<variable name="TotalInvoiceAmount" class="java.math.BigDecimal" resetType="Group" resetGroup="SO" incrementType="Group" incrementGroup="ClaimNum" calculation="Sum">
		<variableExpression><![CDATA[$F{Total Invoice amount}]]></variableExpression>
	</variable>
	<group name="SO">
		<groupExpression><![CDATA[$F{SO}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="28" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-6" stretchType="RelativeToTallestObject" x="0" y="0" width="239" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="62d85a82-1678-403d-b881-2026deac440c"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Shipper Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yyyy" isBlankWhenNull="true">
					<reportElement key="textField-7" stretchType="RelativeToTallestObject" x="239" y="0" width="60" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="d53afdee-5f91-4920-b28d-a3e7ae0ae252"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Load Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-8" stretchType="RelativeToTallestObject" x="299" y="0" width="165" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="6f5a5d2e-42e9-4080-bd29-bff6db931122"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Origin City}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-9" stretchType="RelativeToTallestObject" x="464" y="0" width="65" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="54d0f21d-a27a-411d-9b27-db7cdb885402"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Origin State}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-10" stretchType="RelativeToTallestObject" x="529" y="0" width="165" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="ff8b4dc5-7fb2-419d-b432-8394245dfc6c"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Destination City}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-11" stretchType="RelativeToTallestObject" x="694" y="0" width="65" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="4db2f0d7-577b-4cb3-ac4f-3c7f238a1e8b"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Destination State}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-12" stretchType="RelativeToTallestObject" x="759" y="0" width="34" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="713a572b-d25b-4157-a133-02eebb284e46"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Mode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-13" stretchType="RelativeToTallestObject" x="793" y="0" width="90" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="04c87827-5d7c-4e11-8edc-7175852417ea"/>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[$F{Total Invoice amount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yyyy" isBlankWhenNull="true">
					<reportElement key="textField-14" stretchType="RelativeToTallestObject" x="948" y="0" width="65" height="28" isPrintWhenDetailOverflows="true" forecolor="#333333" uuid="ee029f44-88c3-4a7c-b68f-4e7666897b14"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Close Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yyyy" isBlankWhenNull="true">
					<reportElement key="textField-15" stretchType="RelativeToTallestObject" x="883" y="0" width="65" height="28" isPrintWhenDetailOverflows="true" forecolor="#333333" uuid="a5565688-46da-412b-beb8-8d7eda27c1a0"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Claim Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-16" stretchType="RelativeToTallestObject" x="1216" y="0" width="55" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="c3786a1f-2037-4817-aade-4cfe037116e9"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Overall Score}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-17" stretchType="RelativeToTallestObject" x="1076" y="0" width="70" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="33fbcc64-009f-477e-8937-1ee76104cb18"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{OA Quality Score}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-18" stretchType="RelativeToTallestObject" x="1146" y="0" width="70" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="b426c0d3-0e17-4196-aa4a-6751b925be76"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{DA Quality Score}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-19" stretchType="RelativeToTallestObject" x="1385" y="0" width="109" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="4d4d2d5f-39ca-40ec-a9f1-b1c411fe4d3a"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Coordinator Evaluation}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-20" stretchType="RelativeToTallestObject" x="1271" y="0" width="114" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="8aff6f35-99a3-4206-9485-2fbc9c113bcc"/>
					<box rightPadding="7"/>
					<textElement textAlignment="Left"/>
					<textFieldExpression><![CDATA[$F{Surveyor Evaluation}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-21" stretchType="RelativeToTallestObject" x="1013" y="0" width="63" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="43711c11-c922-4da6-883b-0ba988f6d7f4"/>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[$V{SettlementAmountSO}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="ClaimNum">
		<groupExpression><![CDATA[$F{ClaimNum}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="74" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="0" width="239" height="46" uuid="fecdef57-1007-4cd8-8e3e-185427f7bace"/>
				<box leftPadding="3"/>
				<textElement>
					<font size="24" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Client Summary]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="0" y="46" width="239" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="eb3c0f90-af7b-4553-b8f5-581a90360eb8"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="759" y="46" width="34" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="06efac78-bfe5-43ec-bc11-4a1faf0d94fe"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Mode]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="793" y="46" width="90" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="afe3e542-6a68-4c18-be95-10e35d4a531e"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Total Invoice
Amount]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Opaque" x="239" y="46" width="60" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="07c99547-dd97-4814-a63d-c3e3ba761e33"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Load Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" mode="Opaque" x="694" y="46" width="65" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="1177503c-ab25-4808-b99e-74d82c20fa3e"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Destination State]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" mode="Opaque" x="883" y="46" width="65" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="fab4275f-0b1e-4f85-858f-5f06f7611792"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Claim Received]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" mode="Opaque" x="464" y="46" width="65" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="23a6947f-2dfd-4ee3-ba5b-d497a20355d8"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Origin State]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" mode="Opaque" x="529" y="46" width="165" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="8c5322aa-dd70-4404-b989-3ce72df8d8ff"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Destination City]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="299" y="46" width="165" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="90923026-9b2e-4316-a786-720af15c0d83"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Origin City]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" mode="Opaque" x="1216" y="46" width="55" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="786e58ff-cf65-4460-bb2d-1532960a4e20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Overall Score]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" mode="Opaque" x="1076" y="46" width="70" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="bf86d1f5-5584-402b-8549-ab99aed38c30"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[OA Quality Score]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" mode="Opaque" x="1146" y="46" width="70" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="24613991-2992-4bd6-9108-c05b54564cda"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[DA Quality Score]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-17" mode="Opaque" x="948" y="46" width="65" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="050e7067-d3d3-48c3-bf09-652dfc2943c8"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Claim Closed]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" x="1271" y="0" width="223" height="46" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="7f6da9de-3e2f-4c36-938f-95a5aadebf94"/>
				<box rightPadding="7"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["From: "+(new java.text.SimpleDateFormat("dd-MMM-yyyy")).format($P{Invoice Begin Date})+" TO: "+(new java.text.SimpleDateFormat("dd-MMM-yyyy")).format($P{Invoice End Date})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" x="239" y="0" width="290" height="46" uuid="eb28fe6f-1f82-48dc-b686-0961e50684a7"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{accountline_billToname}==null ? "" : $F{accountline_billToname})+" - "+($P{Bill To}==null ? "" : $P{Bill To})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-19" mode="Opaque" x="1385" y="46" width="109" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="1051b83b-03f6-417f-be91-54f9ab2876ed"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Coordinator Evaluation]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-20" mode="Opaque" x="1271" y="46" width="114" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="25637233-37be-4e73-a89f-7afb608bd9e6"/>
				<box leftPadding="2" rightPadding="7"/>
				<textElement textAlignment="Left">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Surveyor Evaluation]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" mode="Opaque" x="1013" y="46" width="63" height="28" forecolor="#FFFFFF" backcolor="#003366" uuid="a09ce198-b9bc-4bf3-b3a9-f931136f8207"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Settlement
Amount]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="57" splitType="Stretch">
			<textField isStretchWithOverflow="true" evaluationTime="Report" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-22" stretchType="RelativeToTallestObject" x="793" y="29" width="90" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="eeefbb7f-fa16-49c7-868b-91e9edb734f8"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalInvAmt}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-23" stretchType="RelativeToTallestObject" x="1013" y="29" width="63" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="b7b6b2de-4a5e-44cf-a8bb-d71902796868"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalSettlementAmt}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-24" stretchType="RelativeToTallestObject" x="0" y="29" width="239" height="28" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="8ce6da16-ad30-4c95-be4c-f9d576b74a5f"/>
				<textElement textAlignment="Left">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["Total"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-25" stretchType="RelativeToTallestObject" x="0" y="0" width="1494" height="29" isPrintWhenDetailOverflows="true" printWhenGroupChanges="SO" forecolor="#333333" uuid="9fd8fa0f-ce74-4990-8559-baa1b4f441f3"/>
				<textElement textAlignment="Left">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["        "]]></textFieldExpression>
			</textField>
		</band>
	</lastPageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
