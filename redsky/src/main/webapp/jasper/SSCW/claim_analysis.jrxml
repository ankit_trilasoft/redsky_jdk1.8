<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="claim_analysis"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="609"
		 pageHeight="842"
		 columnWidth="549"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Date_From" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Date_To" isForPrompting="true" class="java.util.Date"/>
	<queryString><![CDATA[select "claim_analysis.jrxml",c.claimnumber, l.idnumber1 'LOSS #' ,
ct.ticket ,if (count(t.crewname) > 0, round(( ct.percent/100* (ifnull(l.paidcompensationcustomer,0.00)+ ifnull(l.paidCompensation3Party, 0.0)) / (select count(distinct ts.crewname) from timesheet ts where ts.ticket = ct.ticket)),2),0) 'CHARGE per CREW',
t.crewname 'CREW NAME', cw.warehouse, ((DATEDIFF(now(),hired))/365.25) 'Tenure' 
from claim c 
inner join loss l on c.claimnumber = l.claimnumber 
left outer join claimlossticket ct on ct.lossnumber = l.idnumber1 and l.corpid='SSCW'
left outer join timesheet t on t.ticket = ct.ticket and t.corpid='SSCW'
inner join crew cw on  cw.username = t.username and cw.corpid='SSCW'
where
l.lossaction not in ('C','D') and c.closedate between $P{Date_From} and $P{Date_To} and c.corpid='SSCW'
and round(( ct.percent/100* (ifnull(l.paidcompensationcustomer,0.00)+ ifnull(l.paidCompensation3Party, 0.0)) / (select count(distinct ts.crewname) from timesheet ts where ts.ticket = ct.ticket)),2)>0
group by c.claimnumber, l.idnumber1, ct.ticket, t.crewname
ORDER BY cw.warehouse,t.crewname]]></queryString>

	<field name="claim_analysis.jrxml" class="java.lang.String"/>
	<field name="claimnumber" class="java.lang.Long"/>
	<field name="LOSS #" class="java.lang.Integer"/>
	<field name="ticket" class="java.lang.Long"/>
	<field name="CHARGE per CREW" class="java.lang.Double"/>
	<field name="CREW NAME" class="java.lang.String"/>
	<field name="warehouse" class="java.lang.String"/>
	<field name="Tenure" class="java.math.BigDecimal"/>

	<variable name="sum" class="java.lang.Double" resetType="Group" resetGroup="CrewName" calculation="Sum">
		<variableExpression><![CDATA[$F{CHARGE per CREW}]]></variableExpression>
	</variable>
	<variable name="sum1" class="java.lang.Double" resetType="Group" resetGroup="Warehouse" calculation="Sum">
		<variableExpression><![CDATA[$F{CHARGE per CREW}]]></variableExpression>
	</variable>
	<variable name="count" class="java.lang.Integer" resetType="Group" resetGroup="Warehouse" calculation="Count">
		<variableExpression><![CDATA[$F{CREW NAME}]]></variableExpression>
	</variable>
	<variable name="count1" class="java.lang.Integer" resetType="Group" resetGroup="CrewName" calculation="Count">
		<variableExpression><![CDATA[$F{CREW NAME}]]></variableExpression>
	</variable>
	<variable name="lebel" class="java.lang.Double" resetType="Group" resetGroup="Warehouse" calculation="Average">
		<variableExpression><![CDATA[$F{CHARGE per CREW}]]></variableExpression>
	</variable>
	<variable name="lebel1" class="java.lang.Double" resetType="Group" resetGroup="CrewName" calculation="Average">
		<variableExpression><![CDATA[$F{CHARGE per CREW}]]></variableExpression>
	</variable>

		<group  name="CrewName" >
			<groupExpression><![CDATA[$F{warehouse}]]></groupExpression>
			<groupHeader>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="71"
						y="1"
						width="91"
						height="18"
						forecolor="#3333FF"
						key="textField"
						isPrintRepeatedValues="false"/>
					<box topPadding="1"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{warehouse}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="71"
						height="18"
						forecolor="#3333FF"
						key="staticText-11"/>
					<box leftPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="11"/>
					</textElement>
				<text><![CDATA[Warehouse :]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="247"
						y="0"
						width="106"
						height="15"
						forecolor="#3333FF"
						key="textField"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$V{sum}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="1"
						y="0"
						width="161"
						height="15"
						forecolor="#3333FF"
						key="staticText-14"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="11"/>
					</textElement>
				<text><![CDATA[Total :   ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="353"
						y="0"
						width="95"
						height="15"
						forecolor="#3333FF"
						key="textField"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{count1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="###0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="448"
						y="0"
						width="101"
						height="15"
						forecolor="#3333FF"
						key="textField-4"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$V{lebel1}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="Warehouse" >
			<groupExpression><![CDATA[$F{CREW NAME}]]></groupExpression>
			<groupHeader>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="1"
						y="1"
						width="161"
						height="15"
						key="textField"
						stretchType="RelativeToTallestObject"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="CrewName"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="22"></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{CREW NAME}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="Warehouse"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="247"
						y="1"
						width="106"
						height="15"
						key="textField"
						stretchType="RelativeToTallestObject"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="CrewName"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$V{sum1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="Warehouse"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="353"
						y="1"
						width="95"
						height="15"
						key="textField"
						stretchType="RelativeToTallestObject"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="CrewName"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{count}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="Warehouse"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="448"
						y="1"
						width="101"
						height="15"
						key="textField"
						stretchType="RelativeToTallestObject"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="CrewName"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$V{lebel}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="163"
						y="1"
						width="84"
						height="15"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Tenure}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="48"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="549"
						height="25"
						forecolor="#666666"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<text><![CDATA[Claim Analysis Report by Warehouse and Crew Name]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="46"
						width="549"
						height="1"
						forecolor="#666666"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="44"
						y="26"
						width="223"
						height="20"
						forecolor="#666666"
						key="staticText-12"/>
					<box rightPadding="9"></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Claims Closed Between:  ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="267"
						y="26"
						width="78"
						height="20"
						forecolor="#666666"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{Date_From}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="345"
						y="26"
						width="18"
						height="20"
						forecolor="#666666"
						key="staticText-13"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[to]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="363"
						y="26"
						width="100"
						height="20"
						forecolor="#666666"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{Date_To}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="32"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="246"
						y="15"
						width="106"
						height="16"
						key="staticText-4"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Total Charges]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="1"
						y="0"
						width="161"
						height="15"
						forecolor="#3333FF"
						key="staticText-6"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Warehouse 
]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="31"
						width="549"
						height="0"
						forecolor="#666666"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="352"
						y="15"
						width="96"
						height="16"
						key="staticText-9"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[# of losses]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="448"
						y="15"
						width="101"
						height="16"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Charges per Loss]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="15"
						width="161"
						height="16"
						backcolor="#999999"
						key="staticText-15"/>
					<box leftPadding="22"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Crew Name
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="162"
						y="15"
						width="84"
						height="16"
						key="staticText-16"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Tenure
]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="162"
						y="2"
						width="189"
						height="15"
						key="staticText-8"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="9"/>
					</textElement>
				<text><![CDATA[claim_analysis.jrxml]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="549"
						height="0"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="2"
						width="161"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="352"
						y="2"
						width="153"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="505"
						y="2"
						width="43"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
