


delimiter $$
CREATE  trigger redsky.trigger_add_history_myfile_update
BEFORE UPDATE on redsky.myfile
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



   IF (OLD.id <> NEW.id ) THEN
      select CONCAT(fieldNameList,'myfile.id~') into fieldNameList;
      select CONCAT(oldValueList,OLD.id,'~') into oldValueList;
      select CONCAT(newValueList,NEW.id,'~') into newValueList;
  END IF;

   IF (OLD.location <> NEW.location ) THEN
      select CONCAT(fieldNameList,'myfile.location~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location,'~') into newValueList;
  END IF;






   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'myfile.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;






   IF (OLD.createdBy <> NEW.createdBy ) THEN
      select CONCAT(fieldNameList,'myfile.createdBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.createdBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.createdBy,'~') into newValueList;
  END IF;

   IF (OLD.updatedBy <> NEW.updatedBy ) THEN
      select CONCAT(fieldNameList,'myfile.updatedBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.updatedBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.updatedBy,'~') into newValueList;
  END IF;

   IF (OLD.customerNumber <> NEW.customerNumber ) THEN
      select CONCAT(fieldNameList,'myfile.customerNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.customerNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.customerNumber,'~') into newValueList;
  END IF;






   IF (OLD.fileContentType <> NEW.fileContentType ) THEN
      select CONCAT(fieldNameList,'myfile.fileContentType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileContentType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileContentType,'~') into newValueList;
  END IF;






   IF (OLD.fileFileName <> NEW.fileFileName ) THEN
      select CONCAT(fieldNameList,'myfile.fileFileName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileFileName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileFileName,'~') into newValueList;
  END IF;






   IF (OLD.fileType <> NEW.fileType ) THEN
      select CONCAT(fieldNameList,'myfile.fileType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileType,'~') into newValueList;
  END IF;






   IF (OLD.fileId <> NEW.fileId ) THEN
      select CONCAT(fieldNameList,'myfile.fileId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileId,'~') into newValueList;
  END IF;






   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'myfile.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
      IF (OLD.docSent is not null) THEN 
       set NEW.docSent = null;
       END IF;
  END IF;




   IF (OLD.mapFolder <> NEW.mapFolder ) THEN
      select CONCAT(fieldNameList,'myfile.mapFolder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mapFolder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mapFolder,'~') into newValueList;
  END IF;


   IF (OLD.fileSize <> NEW.fileSize ) THEN
      select CONCAT(fieldNameList,'myfile.fileSize~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileSize,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileSize,'~') into newValueList;
  END IF;


   IF (OLD.networkLinkId <> NEW.networkLinkId ) THEN
      select CONCAT(fieldNameList,'myfile.networkLinkId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkLinkId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkLinkId,'~') into newValueList;
  END IF;


   IF (OLD.transDocStatus <> NEW.transDocStatus ) THEN
      select CONCAT(fieldNameList,'myfile.transDocStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.transDocStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.transDocStatus,'~') into newValueList;
  END IF;


   IF (OLD.transferredBy <> NEW.transferredBy ) THEN
      select CONCAT(fieldNameList,'myfile.transferredBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.transferredBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.transferredBy,'~') into newValueList;
  END IF;


   IF (OLD.emailStatus <> NEW.emailStatus ) THEN
      select CONCAT(fieldNameList,'myfile.emailStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailStatus,'~') into newValueList;
  END IF;



   IF (OLD.documentCategory <> NEW.documentCategory ) THEN
      select CONCAT(fieldNameList,'myfile.documentCategory~') into fieldNameList;
      select CONCAT(oldValueList,OLD.documentCategory,'~') into oldValueList;
      select CONCAT(newValueList,NEW.documentCategory,'~') into newValueList;
  END IF;



  
    IF ((OLD.updatedOn <> NEW.updatedOn) or (OLD.updatedOn is null and NEW.updatedOn is not null)
      or (OLD.updatedOn is not null and NEW.updatedOn is null)) THEN

       select CONCAT(fieldNameList,'myfile.updatedOn~') into fieldNameList;
       IF(OLD.updatedOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.updatedOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.updatedOn is not null) THEN
        	select CONCAT(oldValueList,OLD.updatedOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.updatedOn is not null) THEN
        	select CONCAT(newValueList,NEW.updatedOn,'~') into newValueList;
      	END IF;

   END IF;

    IF ((OLD.createdOn <> NEW.createdOn) or (OLD.createdOn is null and NEW.createdOn is not null)
      or (OLD.createdOn is not null and NEW.createdOn is null)) THEN

       select CONCAT(fieldNameList,'myfile.createdOn~') into fieldNameList;
       IF(OLD.createdOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.createdOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.createdOn is not null) THEN
        	select CONCAT(oldValueList,OLD.createdOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.createdOn is not null) THEN
        	select CONCAT(newValueList,NEW.createdOn,'~') into newValueList;
      	END IF;

   END IF;




    IF ((OLD.docSent <> NEW.docSent) or (OLD.docSent is null and NEW.docSent is not null)
      or (OLD.docSent is not null and NEW.docSent is null)) THEN

       select CONCAT(fieldNameList,'myfile.docSent~') into fieldNameList;
       IF(OLD.docSent is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.docSent is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.docSent is not null) THEN
        	select CONCAT(oldValueList,OLD.docSent,'~') into oldValueList;
     	END IF;
      	IF(NEW.docSent is not null) THEN
        	select CONCAT(newValueList,NEW.docSent,'~') into newValueList;
      	END IF;

   END IF;





    IF (OLD.isCportal <> NEW.isCportal ) THEN
       select CONCAT(fieldNameList,'myfile.isCportal~') into fieldNameList;
       IF(OLD.isCportal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isCportal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isAccportal <> NEW.isAccportal ) THEN
       select CONCAT(fieldNameList,'myfile.isAccportal~') into fieldNameList;
       IF(OLD.isAccportal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isAccportal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isAccportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isAccportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






    IF (OLD.isPartnerPortal <> NEW.isPartnerPortal ) THEN
       select CONCAT(fieldNameList,'myfile.isPartnerPortal~') into fieldNameList;
       IF(OLD.isPartnerPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isPartnerPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isPartnerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isPartnerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.coverPageStripped <> NEW.coverPageStripped ) THEN
       select CONCAT(fieldNameList,'myfile.coverPageStripped~') into fieldNameList;
       IF(OLD.coverPageStripped = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.coverPageStripped = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.coverPageStripped = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.coverPageStripped = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.active <> NEW.active ) THEN
       select CONCAT(fieldNameList,'myfile.active~') into fieldNameList;
       IF(OLD.active = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.active = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.active = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.active = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.isSecure <> NEW.isSecure ) THEN
       select CONCAT(fieldNameList,'myfile.isSecure~') into fieldNameList;
       IF(OLD.isSecure = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSecure = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSecure = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSecure = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






    IF (OLD.isBookingAgent <> NEW.isBookingAgent ) THEN
       select CONCAT(fieldNameList,'myfile.isBookingAgent~') into fieldNameList;
       IF(OLD.isBookingAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isBookingAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isBookingAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isBookingAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






    IF (OLD.isNetworkAgent <> NEW.isNetworkAgent ) THEN
       select CONCAT(fieldNameList,'myfile.isNetworkAgent~') into fieldNameList;
       IF(OLD.isNetworkAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isNetworkAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isNetworkAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isNetworkAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.isOriginAgent <> NEW.isOriginAgent ) THEN
       select CONCAT(fieldNameList,'myfile.isOriginAgent~') into fieldNameList;
       IF(OLD.isOriginAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isOriginAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isSubOriginAgent <> NEW.isSubOriginAgent ) THEN
       select CONCAT(fieldNameList,'myfile.isSubOriginAgent~') into fieldNameList;
       IF(OLD.isSubOriginAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSubOriginAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSubOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSubOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.isDestAgent <> NEW.isDestAgent ) THEN
       select CONCAT(fieldNameList,'myfile.isDestAgent~') into fieldNameList;
       IF(OLD.isDestAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isDestAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isSubDestAgent <> NEW.isSubDestAgent ) THEN
       select CONCAT(fieldNameList,'myfile.isSubDestAgent~') into fieldNameList;
       IF(OLD.isSubDestAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSubDestAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSubDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSubDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

    IF (NEW.invoiceAttachment <>'' or NEW.invoiceAttachment is not null) THEN

       select CONCAT(fieldNameList,'myfile.invoiceAttachment~') into fieldNameList;

	    IF(NEW.invoiceAttachment = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.invoiceAttachment = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
CALL add_tblHistory (OLD.id,"myfile", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());


END $$


delimiter;