package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.VatIdentification;

public interface VatIdentificationDao extends GenericDao<VatIdentification,Long>{
	public List findAllVat();
	public List isCountryExisted(String Country ,String corpid);
	public List searchAll(String vatCountryCode);
	public String getIdVatCode(String vatCountryCode);
}
