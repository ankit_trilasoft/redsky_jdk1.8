package com.trilasoft.app.webapp.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.acegisecurity.Authentication;
import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.module.sitemesh.filter.Buffer;
import com.trilasoft.app.service.RoleBasedComponentPermissionManager;

public class RoleBasedComponentPermissionUtil {
	
	public final static String EDIT_ACCESS = "edit";
	
	public final static String ADD_ACCESS = "add";

	private RoleBasedComponentPermissionManager RoleBasedComponentPermissionManager;
	
	protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
	
	protected HttpServletResponse getResponse() {
        return ServletActionContext.getResponse();
    }
	
	
	public void setRoleBasedComponentPermissionManager(RoleBasedComponentPermissionManager RoleBasedComponentPermissionManager) {
		this.RoleBasedComponentPermissionManager = RoleBasedComponentPermissionManager;
	}

	public boolean isAccessAllowed(String componentId, String roles) {
		return RoleBasedComponentPermissionManager.isAccessAllowed(componentId, roles);
	}

	public int getPermission(String componentId, String roles) {
		return RoleBasedComponentPermissionManager.getPermission(componentId, roles);
	}
	
	@SkipValidation
	public boolean getComponentAccessAttrbute(String componentId) {
		Authentication currentUser = SecurityContextHolder.getContext()
				.getAuthentication();

		if (null == currentUser) {
			return false;
		}

		if ((null == currentUser.getAuthorities())
				|| (currentUser.getAuthorities().length < 1)) {
			return false;
		}
		
		String roles = getAuthoritiesAsString(currentUser.getAuthorities());

		User user = (User) currentUser.getPrincipal();
		
		boolean accessAllowed = false;
		accessAllowed = isAccessAllowed(componentId, roles);

		return accessAllowed;
	}	
	
	@SkipValidation
	private String getAuthoritiesAsString(GrantedAuthority[] authorities) {
		String roles = "";
		for (GrantedAuthority authority:authorities){
			if (!roles.equals("")) roles += ",";
			roles += "'" + authority.getAuthority() + "'";
		}
		return roles;
	}

	@SkipValidation
	public int getComponentPermission(String componentId ) {
		Authentication currentUser = SecurityContextHolder.getContext()
		.getAuthentication();

		if (null == currentUser) {
			return 0;
		}
		
		if ((null == currentUser.getAuthorities())
				|| (currentUser.getAuthorities().length < 1)) {
			return 0;
		}
		String roles = getAuthoritiesAsString(currentUser.getAuthorities());
		String allRoles="";
		String eRoles="";
		User user = (User) currentUser.getPrincipal();
		if(roles.contains("ROLE_AGENT"))
		{
			//System.err.println("2--->"+getRequest().getAttribute("extraRoles"));
			eRoles =(String) getRequest().getAttribute("extraRoles");
		}
		
		
		if(eRoles!=null && !eRoles.equals(""))
		{
			allRoles = new StringBuilder(roles).append(",").append(eRoles).toString();
		}else{
			allRoles =roles;
		}
		
		
		//return getPermission(componentId, roles);
		return getPermission(componentId, allRoles);
		
	}
	
	public String getUserRoleInTransaction(String transId, String partnerIdList) {
		return RoleBasedComponentPermissionManager.getUserRoleInTransaction(transId, partnerIdList);
		
	}

	@SkipValidation
	public List getModulePermissions(String[] tableList, String additionalRoles) {
		Authentication currentUser = SecurityContextHolder.getContext()
		.getAuthentication();

		if (null == currentUser) {
			return null;
		}
		
		if ((null == currentUser.getAuthorities())
				|| (currentUser.getAuthorities().length < 1)) {
			return null;
		}
		
		String roles = getAuthoritiesAsString(currentUser.getAuthorities()) + (additionalRoles==null?"":","+additionalRoles);
		
		User user = (User) currentUser.getPrincipal();
		
		return RoleBasedComponentPermissionManager.getModulePermissions(tableList, roles);

	}

	@SkipValidation
	public String getPartnerIdList() {
		Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

		if (null == currentUser) {
			return null;
		}
		
		return RoleBasedComponentPermissionManager.getPartnerIdList(currentUser.getName());
	}
	

}
