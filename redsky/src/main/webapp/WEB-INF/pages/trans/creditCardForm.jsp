<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 



<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_ACCOUNTING")|| userRole.equalsIgnoreCase("ROLE_ADMIN") || userRole.equalsIgnoreCase("ROLE_FINANCE") || userRole.equalsIgnoreCase("ROLE_COORD") || userRole.equalsIgnoreCase("ROLE_SALE")){
		userRole=role.getName();
		break;
	}
	
}
%>

<head>   
    <title><fmt:message key="creditCardDetail.title"/></title>
 <style>
 .cssbuttonCancel {color:#000000; font-family:arial; font-size:12px; font-size-adjust:none; font-stretch:normal;
font-style:normal; font-variant:normal; font-weight:bold; height:25px; line-height:normal; padding:3px; width:55px;
}
    </style>
    <meta name="heading" content="<fmt:message key='creditCardDetail.heading'/>"/>   
</head>
 <s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />  
<s:form id="creditCardForm" name="creditCardForm" action="saveCreditCard" method="post" validate="true"> 
<s:hidden name="creditCard.id" value="%{creditCard.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="creditCard.shipNumber"/>
<s:hidden name="primaryFlagId"   value="${primaryFlagId}"/>
<c:set var="primaryFlagId"  value="${primaryFlagId}"/>
<s:hidden name="primaryFlags" value="${primaryFlags}"/>
<!-- For New Record -->
<!-- 
<s:hidden name="billing.shipNumber" value="%{billing.shipNumber}"/>
<s:hidden name="billing.id" value="%{billing.id}"/>
 -->
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="billing.shipNumber" value="%{billing.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{sid}"/> 
<s:hidden name="serviceOrder.sequenceNumber"/>
 <c:set var="serviceOrder.sequenceNumber" value="${serviceOrder.sequenceNumber}" />
<s:hidden name="serviceOrder.serviceType"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden id="countCreditCardNotes" name="countCreditCardNotes" value="<%=request.getParameter("countCreditCardNotes") %>"/>
<c:set var="countCreditCardNotes" value="<%=request.getParameter("countCreditCardNotes") %>" />
<s:hidden name="customerFile.id"/>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>


<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
	<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
	<c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
	<c:if test="${forwardingTabVal!='Y'}"> 
		<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${forwardingTabVal=='Y'}">
		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
	<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
	<s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
	        <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
	        <c:set var="relocationServicesKey" value="" />
	        <c:set var="relocationServicesValue" value="" /> 
	        <c:forEach var="entry" items="${relocationServices}">
                <c:if test="${relocationServicesKey==''}">
                <c:if test="${entry.key==serviceOrder.serviceType}">
	               <c:set var="relocationServicesKey" value="${entry.key}" />
	               <c:set var="relocationServicesValue" value="${entry.value}" /> 
                </c:if>
                </c:if> 
            </c:forEach>
            <c:if test="${serviceOrder.job=='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
			<c:if test="${serviceOrder.job!='RLO'}"> 
	       <c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}"/>
	       </c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
	<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
	<c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
	<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.creditcard' }">
		<c:redirect url="/creditCards.html?id=${serviceOrder.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="Layer3" style="width:100%;">
<div id="newmnav"> 
		    <ul>
		    <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
            <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
            <c:set var="relocationServicesKey" value="" />
            <c:set var="relocationServicesValue" value="" /> 
            <c:forEach var="entry" items="${relocationServices}">
            <c:if test="${relocationServicesKey==''}">
            <c:if test="${entry.key==serviceOrder.serviceType}">
            <c:set var="relocationServicesKey" value="${entry.key}" />
            <c:set var="relocationServicesValue" value="${entry.value}" /> 
            </c:if>
           </c:if> 
           </c:forEach>
		      <!-- <li><a onclick="setReturnString('gototab.serviceorder');return checkdate('none');"><span>S/O Details</span></a></li> -->
		      <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
            <li><a onclick="setReturnString('gototab.serviceorder');return checkdate('none');"><span>S/O Details</span></a></li>
             </c:if>
             <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
            <li><a onclick="setReturnString('gototab.serviceorder');return checkdate('none');"><span>Quotes</span></a></li>
              </c:if>
			  
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">	
			  	<li  id="newmnav1" style="background:#FFF"><a class="current"><span>Billing<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  </sec-auth:authComponent>
			  
			  <%-- <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
	             <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
		            <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	            </c:when>
	            <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
	            <c:otherwise> 
		            <li><a onclick="setReturnString('gototab.accounting');return checkdate('none');"><span>Accounting</span></a></li>
                </c:otherwise>
               </c:choose>  
               </sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
               <c:choose> 
	            <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
	            <c:otherwise> 
		            <li><a onclick="setReturnString('gototab.newAccounting');return checkdate('none');"><span>Accounting</span></a></li>
                </c:otherwise>
               </c:choose>
               </sec-auth:authComponent> --%>
               
               <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
              <c:choose>
               <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
                  <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li> 
               </c:when>
               <c:otherwise> 
               <li><a  onclick="setReturnString('gototab.accounting');return checkdate('none');"><span>Accounting</span></a></li>
              </c:otherwise>
             </c:choose>
             </sec-auth:authComponent> 
             <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
             <c:choose> 
               <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
                  <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li> 
               </c:when>
               <c:otherwise> 
               <li><a  onclick="setReturnString('gototab.newAccounting');return checkdate('none');"><span>Accounting</span></a></li>
              </c:otherwise>
             </c:choose>    
             </sec-auth:authComponent>
             </c:if>
             
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
	        
	         <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
              <c:if test="${serviceOrder.job!='RLO'}">    
              <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)== -1 && serviceOrder.corpID=='CWMS')}">       
			  			<li><a onclick="setReturnString('gototab.forwarding');return checkdate('none');"><span>Forwarding</span></a></li>	
			  			</c:if>		  		
			  </c:if>
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			 <c:if test="${serviceOrder.job!='RLO'}"> 
			  <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)== -1  && serviceOrder.corpID=='CWMS')}">
			  <li><a onclick="setReturnString('gototab.domestic');return checkdate('none');"><span>Domestic</span></a></li>
			  </c:if>
			  </c:if>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a onclick="setReturnString('gototab.domestic');return checkdate('none');"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  </c:if>
			   <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)== -1 && serviceOrder.corpID=='CWMS')}">
			  <li><a onclick="setReturnString('gototab.status');return checkdate('none');"><span>Status</span></a></li>
			  </c:if>
			  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
			  <c:if test="${serviceOrder.job!='RLO'}"> 
			  <li><a onclick="setReturnString('gototab.ticket');return checkdate('none');"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a onclick="setReturnString('gototab.claims');return checkdate('none');"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  </c:if>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a onclick="setReturnString('gototab.customerfile');return checkdate('none');"><span>Customer File</span></a></li>
				</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>	
		
		</div>
<!--<div id="newmnav">
  <ul >
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
  <li id="newmnav1"  style="background:#FFF"><a class="current"><span>Billing<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
  
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
</ul>
		</div><div class="spn">&nbsp;</div><br>


 -->
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
 <div id="Layer3" style="width:100%;"> 
<div id="newmnav">   
 <ul>
		  <!--<li class="current"><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing Info</span></a></li>
		  <li id="newmnav1" style="background:#FFF"><a class="current" href="creditCards.html?id=${serviceOrder.id}"><span>Credit Card<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  --><li><a onclick="setReturnString('gototab.billing');return checkdate('none');"><span>Billing Info</span></a></li>
		  <li id="newmnav1" style="background:#FFF"><a class="current" onclick="setReturnString('gototab.creditcard');return checkdate('none');"><span>Credit Card<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  <c:if test="${not empty creditCard.id}">
		  	<li><a onclick="openWindow('auditList.html?id=${creditCard.id}&tableName=creditcard&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>
		  </c:if>
		  <c:if test="${empty creditCard.id}">
		  	<li><a><span>Audit</span></a></li>
		  </c:if>
		</ul>
		</div><div class="spn">&nbsp;</div>
			
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;" >
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="0" border="0" style="width:717px;">
	<tbody>
	<tr>
	<td height="10" align="left" class="listwhitetext">
		<table  class="detailTabLabel" border="0" >
 		  <tbody>
 	
 			<tr><td align="left" height="5px"></td></tr>
			<tr>
			
				<td align="right" style="width:100px"><fmt:message key="creditCard.ccName"/><font color="red" size="2">*</font></td>
				<td align="left" colspan="2"><s:textfield name="creditCard.ccName" onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:170px;" maxlength="45"/></td>
			<c:if test="${primaryFlagId==creditCard.id && primaryFlags=='true'}">
				<c:set var="ischeckedddd" value="false"/>
				<c:if test="${creditCard.primaryFlag}">
					<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" style="width:100px" colspan="2">Primary Flag</td>
				<td align="left" colspan="2"><s:checkbox key="creditCard.primaryFlag" onclick="changeStatus();"  cssStyle="margin:0px;"  value="${ischeckedddd}" fieldValue="true" tabindex="18"/></td>
		    </c:if>
		    
		    <c:if test="${primaryFlags=='show'}">
				<c:set var="ischeckeddddsss" value="false"/>
				<c:if test="${creditCard.primaryFlag}">
					<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" style="width:250px" colspan="2">Primary Flag</td>
				<td align="left" colspan="2"><s:checkbox key="creditCard.primaryFlag" onclick="changeStatus();"  cssStyle="margin:0px;" value="${ischeckeddddsss}" fieldValue="true" tabindex="18"/></td>
		    </c:if>
		    </tr>
		    <tr>	
		    	
		    	<td align="right"><fmt:message key="creditCard.ccType"/><font color="red" size="2">*</font></td>
		    	<td align="left" colspan="5"><s:select name="creditCard.ccType" list="%{paytype}"  cssStyle="width:174px;" cssClass="list-menu" headerKey="" headerValue="" onchange="return lengthValidationCC();"/></td>

		   	</tr>
		    <tr>
		  
				<td align="right" style="width:100px"><fmt:message key="creditCard.ccNumber"/><font color="red" size="2">*</font></td>
				<td align="left" colspan="2">
				<c:if test="${empty creditCard.id}"> 
<sec-auth:authComponent componentId="module.creditCard.section.creditCardNumberCOORD.edit">
	    <%;
	        String permitionCreditCardNumber="true";
	        int permissionCreditCard  = (Integer)request.getAttribute("module.creditCard.section.creditCardNumberCOORD.edit" + "Permission");
 	        if (permissionCreditCard > 2 ){
 	        	permitionCreditCardNumber = "false"; 
 	       }
 	    %>
				<%if(permitionCreditCardNumber.equalsIgnoreCase("false")){ %>
				
					<s:hidden name="creditCard.ccNumber1"></s:hidden>
					<s:textfield name="creditCard.ccNumber" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special');" required="true" cssClass="input-text" cssStyle="width:170px;" maxlength="19" readonly="<%=permitionCreditCardNumber%>" onchange="return lengthValidation();"/>
 				<%}else{%>
 			 			<s:hidden name="creditCard.ccNumber"></s:hidden>
 						<input type="text" name="creditCard.ccNumber1" value="${creditCard.ccNumber1 }" onkeydown="return onlyNumsAllowed(event)"  onchange="copyCCNumber();" required="true" readonly="readonly" class="input-text" cssStyle="width:170px;" maxlength="19"/>
				<%}%>
</sec-auth:authComponent>
				</c:if>
				<c:if test="${not empty creditCard.id}">
<sec-auth:authComponent componentId="module.creditCard.section.creditCardNumberAdmin.edit">
	    <%;
	        String permitionCreditCardAdmin="true";
	        int permissionadmin  = (Integer)request.getAttribute("module.creditCard.section.creditCardNumberAdmin.edit" + "Permission");
 	        if (permissionadmin > 2 ){
 	        	permitionCreditCardAdmin = "false"; 
 	       } 
  %>
				<%if(permitionCreditCardAdmin.equalsIgnoreCase("false")){ %>
					<s:hidden name="creditCard.ccNumber1"></s:hidden>
					<s:textfield name="creditCard.ccNumber" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special');" required="true" cssClass="input-text" cssStyle="width:170px;" maxlength="19" readonly="<%=permitionCreditCardAdmin%>" onchange="return lengthValidation();"/>
 				<%}else{%>
 			 			<s:hidden name="creditCard.ccNumber"></s:hidden>
 						<input type="text" name="creditCard.ccNumber1" value="${creditCard.ccNumber1 }" onkeydown="return onlyNumsAllowed(event)"  onchange="copyCCNumber();" required="true" readonly="readonly" class="input-text" cssStyle="width:170px;" maxlength="19"/>
				<%}%>
</sec-auth:authComponent>
				</c:if>
				
		   </td>
		
		   <td align="right" style="width:140px" colspan="2">Expires<font color="red" size="2">*</font></td>
		  		<td align="left" style="width:50px" >
		  		<s:select cssClass="list-menu" name="creditCard.expireMonth"  list="{'','01','02','03','04','05','06','07','08','09','10','11','12'}" cssStyle="width:58px" onchange="changeStatus();"/>		  				  	   
		  		</td><td>
		  		<s:select cssClass="list-menu" name="creditCard.expireYear" onkeydown="return onlyNumsAllowed(event)"  required="true" list="{'','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020','2021','2022','2023','2024','2025'}"  cssStyle="width:60px" onchange="changeStatus();"/>
		  		</td> 
		  	</tr>
		
		    <tr>
		    	<td align="right"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName= creditCard.ccAuthorizationBillNumber',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key="creditCard.ccAuthorizationBillNumber"/></a></td>
		    	<td align="left" colspan="2"><s:textfield name="creditCard.ccAuthorizationBillNumber"onkeydown="return onlyAlphaNumericAllowedrequired(event)"  required="true" cssClass="input-text" cssStyle="width:170px;" maxlength="20"/></td>
		        <td align="right"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName= creditCard.securityCodeNo',this);return false" onmouseout="ajax_hideTooltip()">
		        <td align="right">Authorization&nbsp;Date</td>
		   		<c:if test="${not empty creditCard.authorizationDate}">
					 <td colspan="2"><s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="creditCard.authorizationDate"/></s:text><s:textfield id="authorizationDate" name="creditCard.authorizationDate" value="%{FormatedInvoiceDate}" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7"/>
				     <img id="authorizationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty creditCard.authorizationDate}">
					<td colspan="2"><s:textfield id="authorizationDate" name="creditCard.authorizationDate" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7"/>
					<img id="authorizationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                </c:if>
		        
     	    </tr>
		    <tr>
		    	<td align="right">Approved&nbsp;Amount</td>
		    	<td align="left"  colspan="2"><s:textfield name="creditCard.ccApprovedBillAmount" onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" size="7" maxlength="11" onchange="autoPopulate_creditCard_statusDate(event);"/></td>
		    	<td align="right" style="width:100px" colspan="2">Approved&nbsp;On</td>
		    	<td align="left" >
                <c:if test="${not empty creditCard.ccApproved}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="creditCard.ccApproved"/></s:text>
				    <s:textfield id="ccApproved" name="creditCard.ccApproved" value="%{FormatedInvoiceDate}" readonly="true"  onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7" />
				    <td>
				    	<img id="ccApproved_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				    </td>
				</c:if>
				<c:if test="${empty creditCard.ccApproved}">
					<s:textfield id="ccApproved" name="creditCard.ccApproved" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7"/>
				<td>
					<img id="ccApproved_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				<td align="right" style="width:186px"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName= creditCard.ccVlTransDate',this);return false" onmouseout="ajax_hideTooltip()"></a></td>
		    	<td  align="left">
		    	</c:if>
		    	</tr>
		    	<tr>
		    	<td align="right">VL&nbsp;Transmittal&nbsp;Date</td>
		   		<c:if test="${not empty creditCard.ccVlTransDate}">
					 <td colspan="3"><s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="creditCard.ccVlTransDate"/></s:text><s:textfield id="ccVlTransDate" name="creditCard.ccVlTransDate" value="%{FormatedInvoiceDate}" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7"/>
				     <img id="ccVlTransDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty creditCard.ccVlTransDate}">
					<td colspan="3"><s:textfield id="ccVlTransDate" name="creditCard.ccVlTransDate" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7"/>
					<img id="ccVlTransDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                </c:if>
                <td align="right"><fmt:message key="creditCard.securityCodeNo"/></a></td>
		    	<td align="left" colspan="2"><s:textfield name="creditCard.securityCodeNo"onkeydown="return onlyAlphaNumericAllowedrequired(event)"  required="true" cssClass="input-text" size="24" maxlength="20"/></td>
                <td  align="right" colspan="5" ></td>
                <c:if test="${empty creditCard.id}">
							<td width="190px" style="!width:170px;" align="right" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
				</c:if>
				
			 	 <c:if test="${not empty creditCard.id}">
										
					<c:choose>
						<c:when test="${countCreditCardNotes == '0' || countCreditCardNotes == '' || countCreditCardNotes == null}">
						<td width="60px" align="right"><img id="countCreditCardNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 align="top" onclick="javascript:openWindow('notess.html?id=${creditCard.id }&notesId=${creditCard.id }&noteFor=CreditCard&subType=CreditCard&imageId=countCreditCardNotesImage&fieldId=countCreditCardNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${creditCard.id }&notesId=${creditCard.id}&noteFor=CreditCard&subType=CreditCard&decorator=popup&popup=true',755,500);" ></a></td>
						</c:when>
						<c:otherwise>
						<td width="60px" align="right"><img id="countCreditCardNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg"  HEIGHT=17 WIDTH=50 align="top" onclick="javascript:openWindow('notess.html?id=${creditCard.id }&notesId=${creditCard.id }&noteFor=CreditCard&subType=CreditCard&imageId=countCreditCardNotesImage&fieldId=countCreditCardNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${creditCard.id }&notesId=${creditCard.id}&noteFor=CreditCard&subType=CreditCard&decorator=popup&popup=true',755,500);" ></a></td>
						</c:otherwise>
				 </c:choose>
			</c:if>
            </tr>
              
            <tr><td align="left" height="5px"></td></tr>
 	
   </tbody>
  </table>
  </td></tr>
</tbody></table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
 		  <table width="730px">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:50px"><b><fmt:message key='creditCard.createdOn'/></td>
						<fmt:formatDate var="credidCardCreatedOn" value="${creditCard.createdOn}" pattern="${displayDateTimeFormat}"/>
						<s:hidden name="creditCard.createdOn" value="${credidCardCreatedOn}"/>
						<td><fmt:formatDate value="${creditCard.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='creditCard.createdBy' /></td>
						
						<td>
						<c:if test="${not empty creditCard.id}">
							<s:label name="createdBy" value="%{creditCard.createdBy}"/>
							<s:hidden name="creditCard.createdBy"/>
						</c:if>
						<c:if test="${empty creditCard.id}">
							<s:label name="createdBy" value="${pageContext.request.remoteUser}"/>
							<s:hidden name="creditCard.createdBy" value="${pageContext.request.remoteUser}" />
						</c:if>
						</td>
						
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='creditCard.updatedOn'/></td>
						<fmt:formatDate var="credidCardUpdatedOn" value="${creditCard.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<s:hidden name="creditCard.updatedOn" value="${credidCardUpdatedOn}"/>
						<td><fmt:formatDate value="${creditCard.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='creditCard.updatedBy' /></td>
						<td>
						<c:if test="${not empty creditCard.id}">
							<s:label name="updatedBy" value="%{creditCard.updatedBy}"/>
							<s:hidden name="creditCard.updatedBy"/>
						</c:if>
						<c:if test="${empty creditCard.id}">
							<s:label name="updatedBy" value="${pageContext.request.remoteUser}"/>
							<s:hidden name="creditCard.updatedBy" value="${pageContext.request.remoteUser}" />
						</c:if>
						</td>
					</tr>
				</tbody>
			</table>   
        <s:submit cssClass="cssbuttonA" cssStyle="width:55px; height:25px" method="save" key="button.save" onclick="setFlagValue();" ></s:submit>
        
        <c:if test="${not empty creditCard.id}">
         	<input type="button" class="cssbutton1" style="width:55px; height:25px" value="Add" style="width:87px; height:26px" onclick="location.href='<c:url value="/editCreditCard.html?sid=${serviceOrder.id}"/>'" />
       	</c:if>
       	<c:if test="${empty creditCard.id}">
        <td></td>
    	<s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset"/>
		</c:if>
		<input type="button" class="cssbutton1" style="width:55px; height:25px" value="Cancel" style="width:87px; height:26px" onclick="location.href='<c:url value="/cancelCreditCard.html?id=${serviceOrder.id}"/>'" />
        
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<s:hidden name="creditCard.corpID"/>
<c:if test="${not empty creditCard.expiryDate}">
	 <s:text id="creditCardExpiryDate" name="${FormDateValue}"> <s:param name="value" value="creditCard.expiryDate" /></s:text>
	 <s:hidden  name="creditCard.expiryDate" value="%{creditCardExpiryDate}" /> 
</c:if>
<c:if test="${empty creditCard.expiryDate}">
	 <s:hidden name="creditCard.expiryDate"/> 
</c:if>
<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
<s:hidden name="hitFlag" />
<c:if test="${validate == 'OK'}">
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/creditCards.html?id=${serviceOrder.id}"  />
		</c:if>
</c:if>

</div>
</s:form>   

 
<script type="text/javascript">
	function copyCCNumber()
	{
		document.forms['creditCardForm'].elements['creditCard.ccNumber'].value=document.forms['creditCardForm'].elements['creditCard.ccNumber1'].value;
	}
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->

<script type="text/javascript">
function assignApprovedDate()
	{
		var d = new Date();
		// var sd=new SimpleDateFormat("yyyy-MM-dd" );
		alert(d.getDate());
		var tempdate;
		tempdate=d.getMonth();
		alert("getMonth"+tempdate);
		
		// alert("After Add getMonth"+tempdate);
		// tempdate=Number(tempdate)+1;
		// tempdate=tempdate+"-"+d.getDate();
		// tempdate=tempdate+"-"+d.getFullYear();
		document.forms['creditCardForm'].elements['creditCard.ccApproved'].value=tempdate;
	}
//

</script>
<script type="text/javascript">
	function autoPopulate_creditCard_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['creditCardForm'].elements['creditCard.ccApproved'].value=datam;
		
	}
</script>	
<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	//alert(evt.keyCode);
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==189)|| (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==67) || (keyCode==86); 
	}
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36) ||( keyCode==110) || (keyCode==67) || (keyCode==86); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>

<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please enter a valid phone number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }
</script>
<script type="text/javascript">
<!--
 function checkData()
  {
	 var f = document.getElementById('creditCardForm');
   		 f.setAttribute("autocomplete", "off");
  }
  
  function notExists(){
	alert("The Credit Card  is not saved yet");
}

 function setFlagValue(){
	document.forms['creditCardForm'].elements['hitFlag'].value = '1';
}

//-->
</script>
<script>
function checkdate(clickType){
    chkSelect();
	if(!(clickType == 'save')){
	if ('${autoSavePrompt}' == 'No'){	
		document.forms['creditCardForm'].action = 'saveCreditCard!saveOnTabChange.html';
		document.forms['creditCardForm'].submit();
	}else{	
	var id1 = document.forms['creditCardForm'].elements['serviceOrder.id'].value;
	var jobNumber = document.forms['creditCardForm'].elements['serviceOrder.shipNumber'].value;
	var idc = document.forms['creditCardForm'].elements['customerFile.id'].value;
	
	if (document.forms['creditCardForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Do you want to save the billing and continue? press OK to continue with save OR press CANCEL");
		if(agree){
			document.forms['creditCardForm'].action = 'saveCreditCard!saveOnTabChange.html';
			document.forms['creditCardForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'editServiceOrderUpdate.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				location.href = 'pricingList.html?sid='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				<c:if test="${forwardingTabVal!='Y'}"> 
					location.href = 'containers.html?id='+id1;
				</c:if>
				  <c:if test="${forwardingTabVal=='Y'}">
				  	location.href='containersAjaxList.html?id='+id1;
				  </c:if>
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.status'){
                <c:if test="${serviceOrder.job =='RLO'}">
                    location.href = 'editDspDetails.html?id='+id1; 
      			</c:if>
                <c:if test="${serviceOrder.job !='RLO'}">
                    location.href =  'editTrackingStatus.html?id='+id1; 
      			</c:if>    				
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.ticket'){
				location.href = 'customerWorkTickets.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				location.href = 'editCustomerFile.html?id='+idc;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					location.href = 'creditCards.html?id='+id1;
				}
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'editServiceOrderUpdate.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				location.href = 'pricingList.html?sid='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				<c:if test="${forwardingTabVal!='Y'}"> 
					location.href = 'containers.html?id='+id1;
				</c:if>
			  <c:if test="${forwardingTabVal=='Y'}">
			  		location.href='containersAjaxList.html?id='+id1;
			  </c:if>
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.status'){
				
                <c:if test="${serviceOrder.job =='RLO'}">
                location.href = 'editDspDetails.html?id='+id1; 
  			</c:if>
            <c:if test="${serviceOrder.job !='RLO'}">
                location.href =  'editTrackingStatus.html?id='+id1; 
  			</c:if> 
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.ticket'){
				location.href = 'customerWorkTickets.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				location.href = 'editCustomerFile.html?id='+idc;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
				}
			if(document.forms['creditCardForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					location.href = 'creditCards.html?id='+id1;
				}
	}
}
}
}
}

function changeStatus(){
	document.forms['creditCardForm'].elements['formStatus'].value = '1';
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'\_'&'\-']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}
function chkSelect()
   {    
 var i;
 var s = document.forms['creditCardForm'].elements['creditCard.ccNumber'].value;
 for (i = 0; i < s.length; i++)
 {
 var c = s.charAt(i);
 if ((((c < "0") || (c > "9")) && (c != " "))) {
 alert("Enter valid Number");
 document.forms['creditCardForm'].elements['creditCard.ccNumber'].value="";
 return false;
 }
 }   
 }

function lengthValidation(){
	var ccNum=document.forms['creditCardForm'].elements['creditCard.ccNumber'].value;
	var ccType = document.forms['creditCardForm'].elements['creditCard.ccType'].value;
	ccNum = ccNum.replace(/ /g, "");
	ccNum1 = ccNum.length;
	var i;
	if(ccType == 'AE'){
		if(ccNum1 < 15){
		alert('Credit card number should contain 15 digits.');
		document.forms['creditCardForm'].elements['creditCard.ccNumber'].value='';
		return false;
		}
	}else{
		if(ccNum1 < 16){
			alert('Credit card number should contain 16 digits.');
			document.forms['creditCardForm'].elements['creditCard.ccNumber'].value='';
			return false;
		}
	}
	for (i = 0; i < ccNum.length; i++)
{
	 var c = ccNum.charAt(i);
	 if ((((c < "0") || (c > "9")) && (c != " "))) {
	 alert("Enter valid Number");
	 document.forms['creditCardForm'].elements['creditCard.ccNumber'].value="";
	 return false;
 }
 }  
}

function lengthValidationCC(){
	var ccNum=document.forms['creditCardForm'].elements['creditCard.ccNumber'].value;
	if(ccNum != ''){
		var ccType = document.forms['creditCardForm'].elements['creditCard.ccType'].value;
		ccNum = ccNum.replace(/ /g, "");
		ccNum1 = ccNum.length;
		if(ccType == 'AE'){
			if(ccNum1 < 15){
			alert('Credit card number should contain 15 digits.');
			document.forms['creditCardForm'].elements['creditCard.ccNumber'].value='';
			return false;
			}
		}else{
			if(ccNum1 < 16){
				alert('Credit card number should contain 16 digits.');
				document.forms['creditCardForm'].elements['creditCard.ccNumber'].value='';
				return false;
			}
		}
	}	
}
</script>

<script type="text/javascript">   
   //// Form.focusFirstElement($("creditCardForm"));   
 
</script>  
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>