delimiter $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_claim` $$
CREATE trigger trigger_add_history_claim
before update on claim
for each row
BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;
   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
      IF (OLD.state <> NEW.state ) THEN
       select CONCAT(fieldNameList,'claim.state~') into fieldNameList;
       select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
       select CONCAT(newValueList,NEW.state,'~') into newValueList;
   END IF;

 IF (OLD.country <> NEW.country ) THEN
       select CONCAT(fieldNameList,'claim.country~') into fieldNameList;
       select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
       select CONCAT(newValueList,NEW.country,'~') into newValueList;
   END IF;

 IF (OLD.email <> NEW.email ) THEN
       select CONCAT(fieldNameList,'claim.email~') into fieldNameList;
       select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
       select CONCAT(newValueList,NEW.email,'~') into newValueList;
   END IF;

 IF (OLD.firstName <> NEW.firstName ) THEN
       select CONCAT(fieldNameList,'claim.firstName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
   END IF;

 IF (OLD.lastName <> NEW.lastName ) THEN
       select CONCAT(fieldNameList,'claim.lastName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
   END IF;

 IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'claim.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;

IF (OLD.city <> NEW.city ) THEN
       select CONCAT(fieldNameList,'claim.city~') into fieldNameList;
       select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
       select CONCAT(newValueList,NEW.city,'~') into newValueList;
   END IF;

IF (OLD.fax <> NEW.fax ) THEN
       select CONCAT(fieldNameList,'claim.fax~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fax,'~') into oldValueList;
       select CONCAT(newValueList,NEW.fax,'~') into newValueList;
   END IF;

IF (OLD.phone <> NEW.phone ) THEN
       select CONCAT(fieldNameList,'claim.phone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.phone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.phone,'~') into newValueList;
   END IF;


IF (OLD.zip <> NEW.zip ) THEN
       select CONCAT(fieldNameList,'claim.zip~') into fieldNameList;
       select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
       select CONCAT(newValueList,NEW.zip,'~') into newValueList;
   END IF;

   IF (OLD.email2 <> NEW.email2 ) THEN
       select CONCAT(fieldNameList,'claim.email2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.email2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.email2,'~') into newValueList;
   END IF;

  IF (OLD.addressLine1 <> NEW.addressLine1 ) THEN
       select CONCAT(fieldNameList,'claim.addressLine1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.addressLine1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.addressLine1,'~') into newValueList;
   END IF;

IF (OLD.addressLine2 <> NEW.addressLine2 ) THEN
       select CONCAT(fieldNameList,'claim.addressLine2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.addressLine2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.addressLine2,'~') into newValueList;
   END IF;


IF (OLD.addressLine3 <> NEW.addressLine3 ) THEN
       select CONCAT(fieldNameList,'claim.addressLine3~') into fieldNameList;
       select CONCAT(oldValueList,OLD.addressLine3,'~') into oldValueList;
       select CONCAT(newValueList,NEW.addressLine3,'~') into newValueList;
   END IF;

IF (OLD.cancelled <> NEW.cancelled ) THEN
       select CONCAT(fieldNameList,'claim.cancelled~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cancelled,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cancelled,'~') into newValueList;
   END IF;
   
IF (OLD.claimEmailSent <> NEW.claimEmailSent ) THEN
       select CONCAT(fieldNameList,'claim.claimEmailSent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimEmailSent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimEmailSent,'~') into newValueList;
   END IF;

IF (OLD.claimCertificateNumber <> NEW.claimCertificateNumber ) THEN
       select CONCAT(fieldNameList,'claim.claimCertificateNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimCertificateNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimCertificateNumber,'~') into newValueList;
   END IF;
   
   IF (OLD.responsibleAgentCode <> NEW.responsibleAgentCode ) THEN
       select CONCAT(fieldNameList,'claim.responsibleAgentCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.responsibleAgentCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.responsibleAgentCode,'~') into newValueList;
   END IF;
   
   IF (OLD.factor <> NEW.factor ) THEN
       select CONCAT(fieldNameList,'claim.factor~') into fieldNameList;
       select CONCAT(oldValueList,OLD.factor,'~') into oldValueList;
       select CONCAT(newValueList,NEW.factor,'~') into newValueList;
   END IF;
   
   IF (OLD.occurence <> NEW.occurence ) THEN
       select CONCAT(fieldNameList,'claim.occurence~') into fieldNameList;
       select CONCAT(oldValueList,OLD.occurence,'~') into oldValueList;
       select CONCAT(newValueList,NEW.occurence,'~') into newValueList;
   END IF;
   
   IF (OLD.avoidable <> NEW.avoidable ) THEN
       select CONCAT(fieldNameList,'claim.avoidable~') into fieldNameList;
       select CONCAT(oldValueList,OLD.avoidable,'~') into oldValueList;
       select CONCAT(newValueList,NEW.avoidable,'~') into newValueList;
   END IF;
   
   IF (OLD.culpable <> NEW.culpable ) THEN
       select CONCAT(fieldNameList,'claim.culpable~') into fieldNameList;
       select CONCAT(oldValueList,OLD.culpable,'~') into oldValueList;
       select CONCAT(newValueList,NEW.culpable,'~') into newValueList;
   END IF;
   
      
   IF (OLD.responsibleAgentName <> NEW.responsibleAgentName ) THEN
       select CONCAT(fieldNameList,'claim.responsibleAgentName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.responsibleAgentName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.responsibleAgentName,'~') into newValueList;
   END IF;
   
      
   IF (OLD.actiontoPreventRecurrence <> NEW.actiontoPreventRecurrence ) THEN
       select CONCAT(fieldNameList,'claim.actiontoPreventRecurrence~') into fieldNameList;
       select CONCAT(oldValueList,OLD.actiontoPreventRecurrence,'~') into oldValueList;
       select CONCAT(newValueList,NEW.actiontoPreventRecurrence,'~') into newValueList;
   END IF;
   
   IF (OLD.remediationComment <> NEW.remediationComment ) THEN
       select CONCAT(fieldNameList,'claim.remediationComment~') into fieldNameList;
       select CONCAT(oldValueList,OLD.remediationComment,'~') into oldValueList;
       select CONCAT(newValueList,NEW.remediationComment,'~') into newValueList;
   END IF;
   
   IF (OLD.location <> NEW.location ) THEN
       select CONCAT(fieldNameList,'claim.location~') into fieldNameList;
       select CONCAT(oldValueList,OLD.location,'~') into oldValueList;
       select CONCAT(newValueList,NEW.location,'~') into newValueList;
   END IF;

IF (OLD.closeDate <> NEW.closeDate ) THEN
       select CONCAT(fieldNameList,'claim.closeDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.closeDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.closeDate,'~') into newValueList;
   END IF;
   
   IF (OLD.remediationClosingDate <> NEW.remediationClosingDate ) THEN
       select CONCAT(fieldNameList,'claim.remediationClosingDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.remediationClosingDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.remediationClosingDate,'~') into newValueList;
   END IF;


IF (OLD.formRecived <> NEW.formRecived ) THEN
       select CONCAT(fieldNameList,'claim.formRecived~') into fieldNameList;
       select CONCAT(oldValueList,OLD.formRecived,'~') into oldValueList;
       select CONCAT(newValueList,NEW.formRecived,'~') into newValueList;
   END IF;

IF (OLD.formSent <> NEW.formSent ) THEN
       select CONCAT(fieldNameList,'claim.formSent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.formSent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.formSent,'~') into newValueList;
   END IF;

IF (OLD.inspecteDate <> NEW.inspecteDate ) THEN
       select CONCAT(fieldNameList,'claim.inspecteDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.inspecteDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.inspecteDate,'~') into newValueList;
   END IF;

IF (OLD.insurer <> NEW.insurer ) THEN
       select CONCAT(fieldNameList,'claim.insurer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insurer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insurer,'~') into newValueList;
   END IF;


   IF (OLD.insurerAcknowledgement <> NEW.insurerAcknowledgement ) THEN
       select CONCAT(fieldNameList,'claim.insurerAcknowledgement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insurerAcknowledgement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insurerAcknowledgement,'~') into newValueList;
   END IF;


  IF (OLD.insurerCode <> NEW.insurerCode ) THEN
       select CONCAT(fieldNameList,'claim.insurerCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insurerCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insurerCode,'~') into newValueList;
   END IF;


  IF (OLD.insurerNotification <> NEW.insurerNotification ) THEN
       select CONCAT(fieldNameList,'claim.insurerNotification~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insurerNotification,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insurerNotification,'~') into newValueList;
   END IF;


 IF (OLD.noProcess <> NEW.noProcess ) THEN
       select CONCAT(fieldNameList,'claim.noProcess~') into fieldNameList;
       select CONCAT(oldValueList,OLD.noProcess,'~') into oldValueList;
       select CONCAT(newValueList,NEW.noProcess,'~') into newValueList;
   END IF;


 IF (OLD.notifyShipper <> NEW.notifyShipper ) THEN
       select CONCAT(fieldNameList,'claim.notifyShipper~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notifyShipper,'~') into oldValueList;
       select CONCAT(newValueList,NEW.notifyShipper,'~') into newValueList;
   END IF;


 IF (OLD.paidByName <> NEW.paidByName ) THEN
       select CONCAT(fieldNameList,'claim.paidByName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.paidByName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.paidByName,'~') into newValueList;
   END IF;

 IF (OLD.premium <> NEW.premium ) THEN
       select CONCAT(fieldNameList,'claim.premium~') into fieldNameList;
       select CONCAT(oldValueList,OLD.premium,'~') into oldValueList;
       select CONCAT(newValueList,NEW.premium,'~') into newValueList;
   END IF;
   
   IF (OLD.propertyDamageAmount <> NEW.propertyDamageAmount ) THEN
       select CONCAT(fieldNameList,'claim.propertyDamageAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.propertyDamageAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.propertyDamageAmount,'~') into newValueList;
   END IF;
   
   IF (OLD.lostDamageAmount <> NEW.lostDamageAmount ) THEN
       select CONCAT(fieldNameList,'claim.lostDamageAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lostDamageAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lostDamageAmount,'~') into newValueList;
   END IF;
   
   IF (OLD.totalPaidToCustomerAmount <> NEW.totalPaidToCustomerAmount ) THEN
       select CONCAT(fieldNameList,'claim.totalPaidToCustomerAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalPaidToCustomerAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.totalPaidToCustomerAmount,'~') into newValueList;
   END IF;
   
   IF (OLD.customerSvcAmount <> NEW.customerSvcAmount ) THEN
       select CONCAT(fieldNameList,'claim.customerSvcAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.customerSvcAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.customerSvcAmount,'~') into newValueList;
   END IF;


 IF (OLD.reciveReimbursement <> NEW.reciveReimbursement ) THEN
       select CONCAT(fieldNameList,'claim.reciveReimbursement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reciveReimbursement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reciveReimbursement,'~') into newValueList;
   END IF;

 IF (OLD.reimbursement <> NEW.reimbursement ) THEN
       select CONCAT(fieldNameList,'claim.reimbursement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reimbursement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reimbursement,'~') into newValueList;
   END IF;
   
   IF (OLD.goodWillPayment <> NEW.goodWillPayment ) THEN
       select CONCAT(fieldNameList,'claim.goodWillPayment~') into fieldNameList;
       select CONCAT(oldValueList,OLD.goodWillPayment,'~') into oldValueList;
       select CONCAT(newValueList,NEW.goodWillPayment,'~') into newValueList;
   END IF;

 IF (OLD.requestForm <> NEW.requestForm ) THEN
       select CONCAT(fieldNameList,'claim.requestForm~') into fieldNameList;
       select CONCAT(oldValueList,OLD.requestForm,'~') into oldValueList;
       select CONCAT(newValueList,NEW.requestForm,'~') into newValueList;
   END IF;

 IF (OLD.requestReimbursement <> NEW.requestReimbursement ) THEN
       select CONCAT(fieldNameList,'claim.requestReimbursement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.requestReimbursement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.requestReimbursement,'~') into newValueList;
   END IF;


 IF (OLD.shipperWorkPhone <> NEW.shipperWorkPhone ) THEN
       select CONCAT(fieldNameList,'claim.shipperWorkPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipperWorkPhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipperWorkPhone,'~') into newValueList;
   END IF;

 IF (OLD.toVendor <> NEW.toVendor ) THEN
       select CONCAT(fieldNameList,'claim.toVendor~') into fieldNameList;
       select CONCAT(oldValueList,OLD.toVendor,'~') into oldValueList;
       select CONCAT(newValueList,NEW.toVendor,'~') into newValueList;
   END IF;


 IF (OLD.uvlAmount <> NEW.uvlAmount ) THEN
       select CONCAT(fieldNameList,'claim.uvlAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.uvlAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.uvlAmount,'~') into newValueList;
   END IF;


 IF (OLD.uvlRecived <> NEW.uvlRecived ) THEN
       select CONCAT(fieldNameList,'claim.uvlRecived~') into fieldNameList;
       select CONCAT(oldValueList,OLD.uvlRecived,'~') into oldValueList;
       select CONCAT(newValueList,NEW.uvlRecived,'~') into newValueList;
   END IF;


 IF (OLD.countryCode <> NEW.countryCode ) THEN
       select CONCAT(fieldNameList,'claim.countryCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.countryCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.countryCode,'~') into newValueList;
   END IF;


 IF (OLD.cityCode <> NEW.cityCode ) THEN
       select CONCAT(fieldNameList,'claim.cityCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cityCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cityCode,'~') into newValueList;
   END IF;

 IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'claim.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;



 IF (OLD.dateOfInspection <> NEW.dateOfInspection ) THEN
       select CONCAT(fieldNameList,'claim.dateOfInspection~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dateOfInspection,'~') into oldValueList;
       select CONCAT(newValueList,NEW.dateOfInspection,'~') into newValueList;
   END IF;



 IF (OLD.insuranceCompanyPaid <> NEW.insuranceCompanyPaid ) THEN
       select CONCAT(fieldNameList,'claim.insuranceCompanyPaid~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insuranceCompanyPaid,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insuranceCompanyPaid,'~') into newValueList;
   END IF;

   
 IF (OLD.cportalAccess <> NEW.cportalAccess ) THEN
       select CONCAT(fieldNameList,'claim.cportalAccess~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cportalAccess,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cportalAccess,'~') into newValueList;
   END IF;
   
 IF (OLD.signature <> NEW.signature ) THEN
       select CONCAT(fieldNameList,'claim.signature~') into fieldNameList;
       select CONCAT(oldValueList,OLD.signature,'~') into oldValueList;
       select CONCAT(newValueList,NEW.signature,'~') into newValueList;
   END IF;


 IF (OLD.signatutreDate <> NEW.signatutreDate ) THEN
       select CONCAT(fieldNameList,'claim.signatutreDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.signatutreDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.signatutreDate,'~') into newValueList;
   END IF;

 IF (OLD.remarks <> NEW.remarks ) THEN
       select CONCAT(fieldNameList,'claim.remarks~') into fieldNameList;
       select CONCAT(oldValueList,OLD.remarks,'~') into oldValueList;
       select CONCAT(newValueList,NEW.remarks,'~') into newValueList;
   END IF;


 IF (OLD.fullValueProtection <> NEW.fullValueProtection ) THEN
       select CONCAT(fieldNameList,'claim.fullValueProtection~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fullValueProtection,'~') into oldValueList;
       select CONCAT(newValueList,NEW.fullValueProtection,'~') into newValueList;
   END IF;



 IF (OLD.submitFlag <> NEW.submitFlag ) THEN
       select CONCAT(fieldNameList,'claim.submitFlag~') into fieldNameList;
       select CONCAT(oldValueList,OLD.submitFlag,'~') into oldValueList;
       select CONCAT(newValueList,NEW.submitFlag,'~') into newValueList;
   END IF;



 IF (OLD.status <> NEW.status ) THEN
       select CONCAT(fieldNameList,'claim.status~') into fieldNameList;
       select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
       select CONCAT(newValueList,NEW.status,'~') into newValueList;
   END IF;



IF (OLD.submitDate <> NEW.submitDate ) THEN
       select CONCAT(fieldNameList,'claim.submitDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.submitDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.submitDate,'~') into newValueList;
   END IF;

IF (OLD.confirmedDeliveryDateByCustomer <> NEW.confirmedDeliveryDateByCustomer ) THEN
       select CONCAT(fieldNameList,'claim.confirmedDeliveryDateByCustomer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.confirmedDeliveryDateByCustomer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.confirmedDeliveryDateByCustomer,'~') into newValueList;
   END IF;

IF (OLD.deductible <> NEW.deductible ) THEN
       select CONCAT(fieldNameList,'claim.deductible~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deductible,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deductible,'~') into newValueList;
   END IF;

IF (OLD.bankName <> NEW.bankName ) THEN
       select CONCAT(fieldNameList,'claim.bankName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bankName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.bankName,'~') into newValueList;
   END IF;

IF (OLD.numberIBAN <> NEW.numberIBAN ) THEN
       select CONCAT(fieldNameList,'claim.numberIBAN~') into fieldNameList;
       select CONCAT(oldValueList,OLD.numberIBAN,'~') into oldValueList;
       select CONCAT(newValueList,NEW.numberIBAN,'~') into newValueList;
   END IF;

IF (OLD.bankAddress <> NEW.bankAddress ) THEN
       select CONCAT(fieldNameList,'claim.bankAddress~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bankAddress,'~') into oldValueList;
       select CONCAT(newValueList,NEW.bankAddress,'~') into newValueList;
   END IF;

IF (OLD.bankAccountNo <> NEW.bankAccountNo ) THEN
       select CONCAT(fieldNameList,'claim.bankAccountNo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bankAccountNo,'~') into oldValueList;
       select CONCAT(newValueList,NEW.bankAccountNo,'~') into newValueList;
   END IF;

IF (OLD.swiftCode <> NEW.swiftCode ) THEN
       select CONCAT(fieldNameList,'claim.swiftCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.swiftCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.swiftCode,'~') into newValueList;
   END IF;
  
IF (OLD.docCompleted <> NEW.docCompleted ) THEN
       select CONCAT(fieldNameList,'claim.docCompleted~') into fieldNameList;
       select CONCAT(oldValueList,OLD.docCompleted,'~') into oldValueList;
       select CONCAT(newValueList,NEW.docCompleted,'~') into newValueList;
   END IF;


IF (OLD.acceptanceSent <> NEW.acceptanceSent ) THEN
       select CONCAT(fieldNameList,'claim.acceptanceSent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.acceptanceSent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.acceptanceSent,'~') into newValueList;
   END IF;

IF (OLD.acceptanceRcvd <> NEW.acceptanceRcvd ) THEN
       select CONCAT(fieldNameList,'claim.acceptanceRcvd~') into fieldNameList;
       select CONCAT(oldValueList,OLD.acceptanceRcvd,'~') into oldValueList;
       select CONCAT(newValueList,NEW.acceptanceRcvd,'~') into newValueList;
   END IF;

IF (OLD.settlementForm <> NEW.settlementForm ) THEN
       select CONCAT(fieldNameList,'claim.settlementForm~') into fieldNameList;
       select CONCAT(oldValueList,OLD.settlementForm,'~') into oldValueList;
       select CONCAT(newValueList,NEW.settlementForm,'~') into newValueList;
   END IF;


IF (OLD.supplier1pay <> NEW.supplier1pay ) THEN
       select CONCAT(fieldNameList,'claim.supplier1pay~') into fieldNameList;
       select CONCAT(oldValueList,OLD.supplier1pay,'~') into oldValueList;
       select CONCAT(newValueList,NEW.supplier1pay,'~') into newValueList;
   END IF;
  
IF (OLD.supplier2pay <> NEW.supplier2pay ) THEN
       select CONCAT(fieldNameList,'claim.supplier2pay~') into fieldNameList;
       select CONCAT(oldValueList,OLD.supplier2pay,'~') into oldValueList;
       select CONCAT(newValueList,NEW.supplier2pay,'~') into newValueList;
   END IF;

IF (OLD.deductibleInsurer <> NEW.deductibleInsurer ) THEN
       select CONCAT(fieldNameList,'claim.deductibleInsurer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deductibleInsurer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deductibleInsurer,'~') into newValueList;
   END IF;

IF (OLD.valuedForCurrency <> NEW.valuedForCurrency ) THEN
       select CONCAT(fieldNameList,'claim.valuedForCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.valuedForCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.valuedForCurrency,'~') into newValueList;
   END IF;

IF (OLD.deductibleAmtCurrency <> NEW.deductibleAmtCurrency ) THEN
       select CONCAT(fieldNameList,'claim.deductibleAmtCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deductibleAmtCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deductibleAmtCurrency,'~') into newValueList;
   END IF;

IF (OLD.deductibleInsurerCurrency <> NEW.deductibleInsurerCurrency ) THEN
       select CONCAT(fieldNameList,'claim.deductibleInsurerCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deductibleInsurerCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deductibleInsurerCurrency,'~') into newValueList;
   END IF;

IF (OLD.reimbursementCurrency <> NEW.reimbursementCurrency ) THEN
       select CONCAT(fieldNameList,'claim.reimbursementCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reimbursementCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reimbursementCurrency,'~') into newValueList;
   END IF;

IF (OLD.reciveReimbursementCurrency <> NEW.reciveReimbursementCurrency ) THEN
       select CONCAT(fieldNameList,'claim.reciveReimbursementCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reciveReimbursementCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reciveReimbursementCurrency,'~') into newValueList;
   END IF;

IF (OLD.insuranceUser <> NEW.insuranceUser ) THEN
       select CONCAT(fieldNameList,'claim.insuranceUser~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insuranceUser,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insuranceUser,'~') into newValueList;
   END IF;

IF (OLD.insuranceUser <> NEW.insuranceUser ) THEN
       select CONCAT(fieldNameList,'claim.insuranceUser~') into fieldNameList;
       select CONCAT(oldValueList,OLD.insuranceUser,'~') into oldValueList;
       select CONCAT(newValueList,NEW.insuranceUser,'~') into newValueList;
   END IF;

      IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'claim.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;
 
   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'claim.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;
   IF ((OLD.claimNumber <> NEW.claimNumber) or (OLD.claimNumber is null and NEW.claimNumber is not null)
  or (OLD.claimNumber is not null and NEW.claimNumber is null)) THEN
select CONCAT(fieldNameList,'claim.claimNumber~') into fieldNameList;
IF(OLD.claimNumber is null) THEN
select CONCAT(oldValueList,"0",'~') into oldValueList;
END IF;
IF(NEW.claimNumber is null) THEN
select CONCAT(newValueList,"0",'~') into newValueList;
END IF;
IF(OLD.claimNumber is not null) then
select CONCAT(oldValueList,OLD.claimNumber,'~') into oldValueList;
END IF;
IF(NEW.claimNumber is not null) then
select CONCAT(newValueList,NEW.claimNumber,'~') into newValueList;
END IF;

END IF;
IF (OLD.registrationNumber <> NEW.registrationNumber ) THEN
       select CONCAT(fieldNameList,'claim.registrationNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.registrationNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.registrationNumber,'~') into newValueList;
   END IF;
IF ((OLD.claimNumber1 <> NEW.claimNumber1) or (OLD.claimNumber1 is null and NEW.claimNumber1 is not null)
  or (OLD.claimNumber1 is not null and NEW.claimNumber1 is null)) THEN
select CONCAT(fieldNameList,'claim.claimNumber1~') into fieldNameList;
IF(OLD.claimNumber1 is null) THEN
select CONCAT(oldValueList,"0",'~') into oldValueList;
END IF;
IF(NEW.claimNumber1 is null) THEN
select CONCAT(newValueList,"0",'~') into newValueList;
END IF;
IF(OLD.claimNumber1 is not null) then
select CONCAT(oldValueList,OLD.claimNumber1,'~') into oldValueList;
END IF;
IF(NEW.claimNumber1 is not null) then
select CONCAT(newValueList,NEW.claimNumber1,'~') into newValueList;
END IF;
END IF;

IF (OLD.crossReference <> NEW.crossReference ) THEN
       select CONCAT(fieldNameList,'claim.crossReference~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crossReference,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crossReference,'~') into newValueList;
   END IF;

IF (OLD.claimPerson <> NEW.claimPerson ) THEN
       select CONCAT(fieldNameList,'claim.claimPerson~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimPerson,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimPerson,'~') into newValueList;
   END IF;
   
    IF (OLD.claimType <> NEW.claimType ) THEN
       select CONCAT(fieldNameList,'claim.claimType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimType,'~') into newValueList;
   END IF;
    IF (OLD.branchandsortcode <> NEW.branchandsortcode ) THEN
       select CONCAT(fieldNameList,'claim.branchandsortcode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.branchandsortcode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.branchandsortcode,'~') into newValueList;
   END IF;
    IF (OLD.accountname <> NEW.accountname ) THEN
       select CONCAT(fieldNameList,'claim.accountname~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accountname,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accountname,'~') into newValueList;
   END IF;
   
   IF ((OLD.claimBy <> NEW.claimBy) or (OLD.claimBy is null and NEW.claimBy is not null)
  or (OLD.claimBy is not null and NEW.claimBy is null)) THEN
select CONCAT(fieldNameList,'claim.claimBy~') into fieldNameList;
IF(OLD.claimBy is null) THEN
select CONCAT(oldValueList,"0",'~') into oldValueList;
END IF;
IF(NEW.claimBy is null) THEN
select CONCAT(newValueList,"0",'~') into newValueList;
END IF;
IF(OLD.claimBy is not null) then
select CONCAT(oldValueList,OLD.claimBy,'~') into oldValueList;
END IF;
IF(NEW.claimBy is not null) then
select CONCAT(newValueList,NEW.claimBy,'~') into newValueList;
END IF;
END IF;

IF ((OLD.noOfDaysForSettelment <> NEW.noOfDaysForSettelment) or (OLD.noOfDaysForSettelment is null and NEW.noOfDaysForSettelment is not null)
  or (OLD.noOfDaysForSettelment is not null and NEW.noOfDaysForSettelment is null)) THEN
select CONCAT(fieldNameList,'claim.noOfDaysForSettelment~') into fieldNameList;
IF(OLD.noOfDaysForSettelment is null) THEN
select CONCAT(oldValueList,"0",'~') into oldValueList;
END IF;
IF(NEW.noOfDaysForSettelment is null) THEN
select CONCAT(newValueList,"0",'~') into newValueList;
END IF;
IF(OLD.noOfDaysForSettelment is not null) then
select CONCAT(oldValueList,OLD.noOfDaysForSettelment,'~') into oldValueList;
END IF;
IF(NEW.noOfDaysForSettelment is not null) then
select CONCAT(newValueList,NEW.noOfDaysForSettelment,'~') into newValueList;
END IF;
END IF;

IF ((OLD.clmsClaimNbr <> NEW.clmsClaimNbr) or (OLD.clmsClaimNbr is null and NEW.clmsClaimNbr is not null)
  or (OLD.clmsClaimNbr is not null and NEW.clmsClaimNbr is null)) THEN
select CONCAT(fieldNameList,'claim.clmsClaimNbr~') into fieldNameList;
IF(OLD.clmsClaimNbr is null) THEN
select CONCAT(oldValueList,"0",'~') into oldValueList;
END IF;
IF(NEW.clmsClaimNbr is null) THEN
select CONCAT(newValueList,"0",'~') into newValueList;
END IF;
IF(OLD.clmsClaimNbr is not null) then
select CONCAT(oldValueList,OLD.clmsClaimNbr,'~') into oldValueList;
END IF;
IF(NEW.clmsClaimNbr is not null) then
select CONCAT(newValueList,NEW.clmsClaimNbr,'~') into newValueList;
END IF;
END IF;

 IF (OLD.assistanceRequired <> NEW.assistanceRequired ) THEN
       select CONCAT(fieldNameList,'claim.assistanceRequired~') into fieldNameList;
       select CONCAT(oldValueList,OLD.assistanceRequired,'~') into oldValueList;
       select CONCAT(newValueList,NEW.assistanceRequired,'~') into newValueList;
   END IF;

   IF (OLD.origClmsAmt <> NEW.origClmsAmt ) THEN
       select CONCAT(fieldNameList,'claim.origClmsAmt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.origClmsAmt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.origClmsAmt,'~') into newValueList;
   END IF;
   
   IF (OLD.pmntAmt <> NEW.pmntAmt ) THEN
       select CONCAT(fieldNameList,'claim.pmntAmt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pmntAmt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.pmntAmt,'~') into newValueList;
   END IF;

 IF (OLD.claimantCountry <> NEW.claimantCountry ) THEN
       select CONCAT(fieldNameList,'claim.claimantCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimantCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimantCountry,'~') into newValueList;
   END IF;

IF (OLD.claimantCity <> NEW.claimantCity ) THEN
       select CONCAT(fieldNameList,'claim.claimantCity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimantCity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimantCity,'~') into newValueList;
   END IF;
     
  update sodashboard set claims=NEW.claimNumber where serviceorderid=NEW.serviceorderid;
		call ServiceOrder_UpdateStatus(NEW.shipnumber,NEW.updatedBy);
   CALL add_tblHistory (OLD.id,"claim", fieldNameList, oldValueList, newValueList, NEW.updatedBy, OLD.corpID, now());
END
$$


delimiter;