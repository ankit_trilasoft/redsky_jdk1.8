package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.springframework.dao.DataAccessException;

import com.trilasoft.app.dao.DspDetailsDao;
import com.trilasoft.app.dao.hibernate.CustomerFileDaoHibernate.partnerDetailsList;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.RevenueTrackerDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.model.DspDetails;
import com.trilasoft.app.model.ExchangeRate;
import com.trilasoft.app.model.TenancyUtilityService;
import com.trilasoft.app.service.ExchangeRateManager;

public class DspDetailsDaoHibernate extends GenericDaoHibernate<DspDetails, Long> implements DspDetailsDao {
	private HibernateUtil hibernateUtil;
	public DspDetailsDaoHibernate() {
		super(DspDetails.class);
		
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DspDetails where id = "+id+" ");
	}
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from DspDetails where id=?",id);
    }
    public String getListRecord(String job,String corpId){
    	String rec="";
    	List vendorCode=getHibernateTemplate().find("select bookingAgentCode from CompanyDivision where vanlinedefaultJobtype='"+job+"' and corpID=?", corpId);
    	if(!vendorCode.isEmpty()&&vendorCode.get(0)!=null)
    	{
    	List reloRec=this.getSession().createSQLQuery("select concat(if(partnerCode=null or partnerCode='',' ',partnerCode),'~',if(lastName=null or lastName='',' ',lastName),'~',if(reloContactEmail=null or reloContactEmail='',' ',reloContactEmail),'~',if(reloContact=null or reloContact='',' ',reloContact)) from partnerpublic where   status='Approved'  and partnerCode='"+vendorCode.get(0).toString()+"' AND corpID in ('"+corpId+"','TSFT')").list();
    		if(!reloRec.isEmpty()&&reloRec.get(0)!=null)
    		{
    			rec=reloRec.get(0).toString();
    		}
    	}
    	return rec;
    }
    public void updateServiceOrderStatus(Long id,String shipNumber,String serviceOrderStatus,String corpId,String userName ,int StatusNumber){
    	getSession().createSQLQuery("update serviceorder set status='"+serviceOrderStatus.trim()+"',updatedBy='"+userName+"',updatedOn=now(),statusNumber="+StatusNumber+",statusDate=now() where shipNumber='"+shipNumber+"' and id='"+id+"' ").executeUpdate();
    
    }
    public void updateServiceCompleteDate(Long id,String shipNumber,String latestDate,String corpId ){
    	//String abc="update dspdetails set serviceCompleteDate=DATE_ADD('"+latestDate+"', INTERVAL 31 DAY) where shipNumber='"+shipNumber+"' and id='"+id+"' and corpID='"+corpId+"'";
    	getSession().createSQLQuery("update dspdetails set serviceCompleteDate='"+latestDate+"' where shipNumber='"+shipNumber+"' and id='"+id+"' and corpID='"+corpId+"'").executeUpdate();
    }
    public String getRloVenderCode(Long id,String corpId){
    	String partnerSetCode="";
    	List setList = new ArrayList();
	    setList = this.getSession().createSQLQuery("select tableName,filterValues,fieldName, dss.name from userdatasecurity uds , " +
				"datasecuritypermission dsp , datasecurityfilter dsf , datasecurityset dss where uds.userdatasecurity_id = dsp.datasecurityset_id " +
				"and dsp.datasecurityset_id = dss.id and uds.user_id="+id+" and dsf.corpID in('"+corpId+"','TSFT') and dsf.id=dsp.datasecurityfilter_id").list();
	    if(!setList.isEmpty()&& setList!=null ){
	    	Iterator it = setList.iterator();
			List list1 = new ArrayList();
			while(it.hasNext())
			{
				Object[] object = (Object[])it.next();
				String dataSetName = object[3].toString();
				if(dataSetName.startsWith("DATA_SECURITY_SET_SO_RLOVENDORCODE_"))
				{
					list1.add("" + object[1].toString().trim()+ "");
					/*List tempChildCode = customerFileManager.getChildAgentCode( object[1].toString(),sessionCorpID);
					if(tempChildCode!=null && !tempChildCode.isEmpty()){
						for(int i=0 ; i<tempChildCode.size() ; i++){
							list1.add("'" + tempChildCode.get(i).toString() + "'");
						}
					}*/
				}
			/*	if(dataSetName.startsWith("DATA_SECURITY_SET_SO_COMPANYDIVISION_"))
				{
					list2.add("'" + object[1].toString() + "'");
				}*/
			}
		partnerSetCode= list1.toString().replace("[","").replace("]", "").trim();
		//companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
	    }
	    return partnerSetCode;
    }
    /**
     * Convenience method to get the request
     * @return current request
     */
    protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
    public List findMaximumShip(String seqNum, String job,String CorpID) {
    	List userList=new ArrayList();
    	try {
			String username = getRequest().getRemoteUser();
			String userId = "";
			String companydivisionSetCode="";
			List users = this
					.getSession()
					.createSQLQuery(
							"select id from app_user where username='"
									+ username + "'").list();
			if (users != null && !users.isEmpty()) {
				userId = users.get(0).toString();
			}
			List list = new ArrayList();
			list = this
					.getSession()
					.createSQLQuery(
							"select tableName,filterValues,fieldName, dss.name from userdatasecurity uds , "
									+ "datasecuritypermission dsp , datasecurityfilter dsf , datasecurityset dss where uds.userdatasecurity_id = dsp.datasecurityset_id "
									+ "and dsp.datasecurityset_id = dss.id and uds.user_id="
									+ userId
									+ " and dsf.corpID in('"
									+ CorpID
									+ "','TSFT') and dsf.id=dsp.datasecurityfilter_id")
					.list();
			if (!list.isEmpty() && list != null) {
				Iterator it = list.iterator();
				List list1 = new ArrayList();
				List list2 = new ArrayList();
				while (it.hasNext()) {
					Object[] object = (Object[]) it.next();
					String dataSetName = object[3].toString();
				/*	if (dataSetName
							.startsWith("DATA_SECURITY_SET_SO_BILLTOCODE_")) {
						list1.add("'" + object[1].toString() + "'");
						List tempChildCode = getChildAgentCode( object[1].toString(),CorpID);
						if(tempChildCode!=null && !tempChildCode.isEmpty()){
							for(int i=0 ; i<tempChildCode.size() ; i++){
								list1.add("'" + tempChildCode.get(i).toString() + "'");
							}
						}
					}*/
					if(dataSetName.startsWith("DATA_SECURITY_SET_SO_COMPANYDIVISION_"))
					{
						list2.add("'" + object[1].toString() + "'");
					}
					if(dataSetName.startsWith("DATA_SECURITY_SET_SO_RLOVENDORCODE_"))
					{
						list1.add("'" + object[1].toString() + "'");
					}
				}
				String partnerCodePopup = list1.toString().replace("[", "")
						.replace("]", "");
				companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
				if (partnerCodePopup != null && !partnerCodePopup.equals("") &&  companydivisionSetCode != null && !companydivisionSetCode.equals("") ) {
					userList = this
							.getSession()
							.createSQLQuery(
									"select max(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and sequenceNumber='"
											+ seqNum + "' and ( CAR_vendorCode in("+ partnerCodePopup+ ") or COL_vendorCode in("+ partnerCodePopup+ ") or TRG_vendorCode in("+ partnerCodePopup+ ") or HOM_vendorCode in("+ partnerCodePopup+ ") or RNT_vendorCode in("+ partnerCodePopup+ ") or SET_vendorCode in("+ partnerCodePopup+ ") or LAN_vendorCode in("+ partnerCodePopup+ ") or MMG_vendorCode in("+ partnerCodePopup+ ") or ONG_vendorCode in("+ partnerCodePopup+ ") or PRV_vendorCode in("+ partnerCodePopup+ ") or AIO_vendorCode in("+ partnerCodePopup+ ") or EXP_vendorCode in("+ partnerCodePopup+ ") or RPT_vendorCode in("+ partnerCodePopup+ ") or SCH_schoolSelected in("+ partnerCodePopup+ ") or TAX_vendorCode in("+ partnerCodePopup+ ") or TAC_vendorCode in("+ partnerCodePopup+ ") or TEN_vendorCode in("+ partnerCodePopup+ ") or VIS_vendorCode in("+ partnerCodePopup+ ") or WOP_vendorCode in("+ partnerCodePopup+ ") or REP_vendorCode in("+ partnerCodePopup+ ") or RLS_vendorCode in("+ partnerCodePopup+ ")  or CAT_vendorCode in("+ partnerCodePopup+ ")  or CLS_vendorCode in("+ partnerCodePopup+ ")  or CHS_vendorCode in("+ partnerCodePopup+ ")  or DPS_vendorCode in("+ partnerCodePopup+ ")  or HSM_vendorCode in("+ partnerCodePopup+ ")  or PDT_vendorCode in("+ partnerCodePopup+ ")  or RCP_vendorCode in("+ partnerCodePopup+ ")  or SPA_vendorCode in("+ partnerCodePopup+ ")  or TCS_vendorCode in("+ partnerCodePopup+ ") or MTS_vendorCode in("+ partnerCodePopup+ ") or DSS_vendorCode in("+ partnerCodePopup+ ") or HOB_vendorCode in("+ partnerCodePopup+ ") or FLB_vendorCode in("+ partnerCodePopup+ ") or FRL_vendorCode in("+ partnerCodePopup+ ") or APU_vendorCode in("+ partnerCodePopup+ ") or INS_vendorCode in ("+ partnerCodePopup+ ") or INP_vendorCode in ("+ partnerCodePopup+ ") or EDA_vendorCode in ("+ partnerCodePopup+ ") or TAS_vendorCode in ("+ partnerCodePopup+ ")) and companyDivision in("
							                + companydivisionSetCode
							                + ") and corpID='" + CorpID + "'")
							                .list();
				}else if (partnerCodePopup != null && !partnerCodePopup.equals("") ) {
					/*String ans="select max(ship) from serviceorder where job like '"
									+ job.replaceAll("'", "''")
									+ "%' and sequenceNumber='"
									+ seqNum + "' and  ( CAR_vendorCode in("+ partnerCodePopup+ ") or COL_vendorCode in("+ partnerCodePopup+ ") or TRG_vendorCode in("+ partnerCodePopup+ ") or HOM_vendorCode in("+ partnerCodePopup+ ") or RNT_vendorCode in("+ partnerCodePopup+ ") or SET_vendorCode in("+ partnerCodePopup+ ") or LAN_vendorCode in("+ partnerCodePopup+ ") or MMG_vendorCode in("+ partnerCodePopup+ ") or ONG_vendorCode in("+ partnerCodePopup+ ") or PRV_vendorCode in("+ partnerCodePopup+ ") or AIO_vendorCode in("+ partnerCodePopup+ ") or EXP_vendorCode in("+ partnerCodePopup+ ") or RPT_vendorCode in("+ partnerCodePopup+ ") or SCH_schoolSelected in("+ partnerCodePopup+ ") or TAX_vendorCode in("+ partnerCodePopup+ ") or TAC_vendorCode in("+ partnerCodePopup+ ") or TEN_vendorCode in("+ partnerCodePopup+ ") or VIS_vendorCode in("+ partnerCodePopup+ ")) and corpID='" + CorpID + "'";*/
					userList = this
					.getSession()
					.createSQLQuery(
							"select max(ship) from serviceorder where job like '"
									+ job.replaceAll("'", "''")
									+ "%' and sequenceNumber='"
									+ seqNum + "' and  ( CAR_vendorCode in("+ partnerCodePopup+ ") or COL_vendorCode in("+ partnerCodePopup+ ") or TRG_vendorCode in("+ partnerCodePopup+ ") or HOM_vendorCode in("+ partnerCodePopup+ ") or RNT_vendorCode in("+ partnerCodePopup+ ") or SET_vendorCode in("+ partnerCodePopup+ ") or LAN_vendorCode in("+ partnerCodePopup+ ") or MMG_vendorCode in("+ partnerCodePopup+ ") or ONG_vendorCode in("+ partnerCodePopup+ ") or PRV_vendorCode in("+ partnerCodePopup+ ") or AIO_vendorCode in("+ partnerCodePopup+ ") or EXP_vendorCode in("+ partnerCodePopup+ ") or RPT_vendorCode in("+ partnerCodePopup+ ") or SCH_schoolSelected in("+ partnerCodePopup+ ") or TAX_vendorCode in("+ partnerCodePopup+ ") or TAC_vendorCode in("+ partnerCodePopup+ ") or TEN_vendorCode in("+ partnerCodePopup+ ") or VIS_vendorCode in("+ partnerCodePopup+ ") or WOP_vendorCode in("+ partnerCodePopup+ ") or  REP_vendorCode in("+ partnerCodePopup+ ") or RLS_vendorCode in("+ partnerCodePopup+ ")  or CAT_vendorCode in("+ partnerCodePopup+ ")  or CLS_vendorCode in("+ partnerCodePopup+ ")  or CHS_vendorCode in("+ partnerCodePopup+ ")  or DPS_vendorCode in("+ partnerCodePopup+ ")  or HSM_vendorCode in("+ partnerCodePopup+ ")  or PDT_vendorCode in("+ partnerCodePopup+ ")  or RCP_vendorCode in("+ partnerCodePopup+ ")  or SPA_vendorCode in("+ partnerCodePopup+ ")  or TCS_vendorCode in("+ partnerCodePopup+ ") or MTS_vendorCode in("+ partnerCodePopup+ ") or DSS_vendorCode in("+ partnerCodePopup+ ") or HOB_vendorCode in("+ partnerCodePopup+ ") or FLB_vendorCode in("+ partnerCodePopup+ ") or FRL_vendorCode in("+ partnerCodePopup+ ") or APU_vendorCode in("+ partnerCodePopup+ ") or INS_vendorCode in ("+ partnerCodePopup+ ") or INP_vendorCode in ("+ partnerCodePopup+ ") or EDA_vendorCode in ("+ partnerCodePopup+ ") or TAS_vendorCode in ("+ partnerCodePopup+ ")) and corpID='" + CorpID + "'")
					.list();
		   }else if (companydivisionSetCode != null && !companydivisionSetCode.equals("") ) {
			  userList = this
			 .getSession()
			 .createSQLQuery(
					"select max(ship) from serviceorder where job like '"
							+ job.replaceAll("'", "''")
							+ "%' and sequenceNumber='"
							+ seqNum + "' and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'")
			.list();
         } else {
					userList = this
							.getSession()
							.createSQLQuery(
									"select max(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and sequenceNumber='"
											+ seqNum + "' and corpID='"
											+ CorpID + "'").list();
				}
			} else {
				userList = this
						.getSession()
						.createSQLQuery(
								"select max(ship) from serviceorder where job like '"
										+ job.replaceAll("'", "''")
										+ "%' and sequenceNumber='" + seqNum
										+ "' and corpID='" + CorpID + "'")
						.list();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		//return getHibernateTemplate().find("select max(ship) from ServiceOrder where job like '" +job.replaceAll("'", "''")+ "%' and sequenceNumber=?", seqNum);
    	return userList;
    }
    public List findMinimumShip(String seqNum, String job,String CorpID) {
    	List userList=new ArrayList();
    	try {
			String username = getRequest().getRemoteUser();
			String userId = "";
			String companydivisionSetCode="";
			List users = this
					.getSession()
					.createSQLQuery(
							"select id from app_user where username='"
									+ username + "'").list();
			if (users != null && !users.isEmpty()) {
				userId = users.get(0).toString();
			}
			List list = new ArrayList();
			List list2 = new ArrayList();
			list = this
					.getSession()
					.createSQLQuery(
							"select tableName,filterValues,fieldName, dss.name from userdatasecurity uds , "
									+ "datasecuritypermission dsp , datasecurityfilter dsf , datasecurityset dss where uds.userdatasecurity_id = dsp.datasecurityset_id "
									+ "and dsp.datasecurityset_id = dss.id and uds.user_id="
									+ userId
									+ " and dsf.corpID in('"
									+ CorpID
									+ "','TSFT') and dsf.id=dsp.datasecurityfilter_id")
					.list();
			if (!list.isEmpty() && list != null) {
				Iterator it = list.iterator();
				List list1 = new ArrayList();
				while (it.hasNext()) {
					Object[] object = (Object[]) it.next();
					String dataSetName = object[3].toString();
					/*if (dataSetName
							.startsWith("DATA_SECURITY_SET_SO_BILLTOCODE_")) {
						list1.add("'" + object[1].toString() + "'");
						List tempChildCode = getChildAgentCode( object[1].toString(),CorpID);
						if(tempChildCode!=null && !tempChildCode.isEmpty()){
							for(int i=0 ; i<tempChildCode.size() ; i++){
								list1.add("'" + tempChildCode.get(i).toString() + "'");
							}
						}
					}*/
					if(dataSetName.startsWith("DATA_SECURITY_SET_SO_COMPANYDIVISION_"))
					{
						list2.add("'" + object[1].toString() + "'");
					}
					if(dataSetName.startsWith("DATA_SECURITY_SET_SO_RLOVENDORCODE_"))
					{
						list1.add("'" + object[1].toString() + "'");
					}
				}
				String partnerCodePopup = list1.toString().replace("[", "")
						.replace("]", "");
				companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
				if (companydivisionSetCode != null  && !companydivisionSetCode.equals("") &&   partnerCodePopup != null && !partnerCodePopup.equals("") ) {
					userList = this
							.getSession()
							.createSQLQuery(
									"select min(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and  sequenceNumber='"
											+ seqNum + "' and ( CAR_vendorCode in("+ partnerCodePopup+ ") or COL_vendorCode in("+ partnerCodePopup+ ") or TRG_vendorCode in("+ partnerCodePopup+ ") or HOM_vendorCode in("+ partnerCodePopup+ ") or RNT_vendorCode in("+ partnerCodePopup+ ") or SET_vendorCode in("+ partnerCodePopup+ ") or LAN_vendorCode in("+ partnerCodePopup+ ") or MMG_vendorCode in("+ partnerCodePopup+ ") or ONG_vendorCode in("+ partnerCodePopup+ ") or PRV_vendorCode in("+ partnerCodePopup+ ") or AIO_vendorCode in("+ partnerCodePopup+ ") or EXP_vendorCode in("+ partnerCodePopup+ ") or RPT_vendorCode in("+ partnerCodePopup+ ") or SCH_schoolSelected in("+ partnerCodePopup+ ") or TAX_vendorCode in("+ partnerCodePopup+ ") or TAC_vendorCode in("+ partnerCodePopup+ ") or TEN_vendorCode in("+ partnerCodePopup+ ") or VIS_vendorCode in("+ partnerCodePopup+ ") or WOP_vendorCode in("+ partnerCodePopup+ ") or REP_vendorCode in("+ partnerCodePopup+ ") or RLS_vendorCode in("+ partnerCodePopup+ ")  or CAT_vendorCode in("+ partnerCodePopup+ ")  or CLS_vendorCode in("+ partnerCodePopup+ ")  or CHS_vendorCode in("+ partnerCodePopup+ ")  or DPS_vendorCode in("+ partnerCodePopup+ ")  or HSM_vendorCode in("+ partnerCodePopup+ ")  or PDT_vendorCode in("+ partnerCodePopup+ ")  or RCP_vendorCode in("+ partnerCodePopup+ ")  or SPA_vendorCode in("+ partnerCodePopup+ ")  or TCS_vendorCode in("+ partnerCodePopup+ ") or MTS_vendorCode in("+ partnerCodePopup+ ") or DSS_vendorCode in("+ partnerCodePopup+ ") or HOB_vendorCode in("+ partnerCodePopup+ ") or FLB_vendorCode in("+ partnerCodePopup+ ") or FRL_vendorCode in("+ partnerCodePopup+ ") or APU_vendorCode in("+ partnerCodePopup+ ")or INS_vendorCode in ("+ partnerCodePopup+ ") or INP_vendorCode in ("+ partnerCodePopup+ ") or EDA_vendorCode in ("+ partnerCodePopup+ ") or TAS_vendorCode in ("+ partnerCodePopup+ ")) and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'")
							.list();
				}else if (partnerCodePopup != null && !partnerCodePopup.equals("")   ) {
					userList = this
					.getSession()
					.createSQLQuery(
							"select min(ship) from serviceorder where job like '"
									+ job.replaceAll("'", "''")
									+ "%' and  sequenceNumber='"
									+ seqNum + "' and  ( CAR_vendorCode in("+ partnerCodePopup+ ") or COL_vendorCode in("+ partnerCodePopup+ ") or TRG_vendorCode in("+ partnerCodePopup+ ") or HOM_vendorCode in("+ partnerCodePopup+ ") or RNT_vendorCode in("+ partnerCodePopup+ ") or SET_vendorCode in("+ partnerCodePopup+ ") or LAN_vendorCode in("+ partnerCodePopup+ ") or MMG_vendorCode in("+ partnerCodePopup+ ") or ONG_vendorCode in("+ partnerCodePopup+ ") or PRV_vendorCode in("+ partnerCodePopup+ ") or AIO_vendorCode in("+ partnerCodePopup+ ") or EXP_vendorCode in("+ partnerCodePopup+ ") or RPT_vendorCode in("+ partnerCodePopup+ ") or SCH_schoolSelected in("+ partnerCodePopup+ ") or TAX_vendorCode in("+ partnerCodePopup+ ") or TAC_vendorCode in("+ partnerCodePopup+ ") or TEN_vendorCode in("+ partnerCodePopup+ ") or VIS_vendorCode in("+ partnerCodePopup+ ") or WOP_vendorCode in("+ partnerCodePopup+ ") or REP_vendorCode in("+ partnerCodePopup+ ") or RLS_vendorCode in("+ partnerCodePopup+ ")  or CAT_vendorCode in("+ partnerCodePopup+ ")  or CLS_vendorCode in("+ partnerCodePopup+ ")  or CHS_vendorCode in("+ partnerCodePopup+ ")  or DPS_vendorCode in("+ partnerCodePopup+ ")  or HSM_vendorCode in("+ partnerCodePopup+ ")  or PDT_vendorCode in("+ partnerCodePopup+ ")  or RCP_vendorCode in("+ partnerCodePopup+ ")  or SPA_vendorCode in("+ partnerCodePopup+ ")  or TCS_vendorCode in("+ partnerCodePopup+ ") or MTS_vendorCode in("+ partnerCodePopup+ ") or DSS_vendorCode in("+ partnerCodePopup+ ") or HOB_vendorCode in("+ partnerCodePopup+ ") or FLB_vendorCode in("+ partnerCodePopup+ ") or FRL_vendorCode in("+ partnerCodePopup+ ") or APU_vendorCode in("+ partnerCodePopup+ ")or INS_vendorCode in ("+ partnerCodePopup+ ") or INP_vendorCode in ("+ partnerCodePopup+ ") or EDA_vendorCode in ("+ partnerCodePopup+ ") or TAS_vendorCode in ("+ partnerCodePopup+ ")) and corpID='" + CorpID + "'")
					.list();
		}else if (companydivisionSetCode != null && !companydivisionSetCode.equals("")  ) {
			userList = this
			.getSession()
			.createSQLQuery(
					"select min(ship) from serviceorder where job like '"
							+ job.replaceAll("'", "''")
							+ "%' and  sequenceNumber='"
							+ seqNum + "' and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'")
			.list();
} else {
					userList = this
							.getSession()
							.createSQLQuery(
									"select min(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and  sequenceNumber='"
											+ seqNum + "'and corpID='" + CorpID
											+ "'").list();
				}
			} else {
				userList = this
						.getSession()
						.createSQLQuery(
								"select min(ship) from serviceorder where job like '"
										+ job.replaceAll("'", "''")
										+ "%' and  sequenceNumber='" + seqNum
										+ "'and corpID='" + CorpID + "'")
						.list();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		//return getHibernateTemplate().find("select min(ship) from ServiceOrder where job like '" +job.replaceAll("'", "''")+ "%' and  sequenceNumber=?", seqNum);
    	return userList;
    }
    
    public String checkNetworkPartnerServiceType(String sessionCorpID,String networkVendorCode,String preFixCode,String moveTypeCheck){
    	String serviceTypeCheck="N";
    	String quotesMoveTypeCheck="N";
		List list=this.getSession().createSQLQuery("select agentCorpId from networkpartners where agentPartnerCode = '"+networkVendorCode+"' and corpId = '"+sessionCorpID+"' ").list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
			String corpID=list.get(0).toString();
			list=this.getSession().createSQLQuery("select code from refmaster where parameter='SERVICE' and flex1='Relo' and corpId in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and code='"+preFixCode+"'").list();
			if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
				serviceTypeCheck="Y";
			}
			if(moveTypeCheck.equalsIgnoreCase("Y")){
				list=this.getSession().createSQLQuery("select if(accessQuotationFromCustomerFile=null,'F',if(accessQuotationFromCustomerFile,'T','F')) from company where corpid='"+corpID+"'").list();
				if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)&&(list.get(0).toString().equalsIgnoreCase("T"))){
					quotesMoveTypeCheck="Y";
				}
			}else{
				quotesMoveTypeCheck="Y";
			}
		}
		return serviceTypeCheck+"~"+quotesMoveTypeCheck;
    }
    public String checkMoveTypeAgent(String sessionCorpID,String networkVendorCode){
    	String quotesMoveTypeCheck="N";
		List list=this.getSession().createSQLQuery("select agentCorpId from networkpartners where agentPartnerCode = '"+networkVendorCode+"' and corpId = '"+sessionCorpID+"' ").list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
			String corpID=list.get(0).toString();
				list=this.getSession().createSQLQuery("select if(accessQuotationFromCustomerFile=null,'F',if(accessQuotationFromCustomerFile,'T','F')) from company where corpid='"+corpID+"'").list();
				if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)&&(list.get(0).toString().equalsIgnoreCase("T"))){
					quotesMoveTypeCheck="Y";
				}
		}
		return quotesMoveTypeCheck;    	
    }
    public List findAllServicesForRelo(String corpId){
    	List al = new ArrayList();
    	List list=this.getSession().createSQLQuery("select code from refmaster where parameter='SERVICE' and flex1='Relo' and corpId in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')").list();
    	if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
	    	Iterator itr=list.iterator();
	    	while(itr.hasNext()){
	    		al.add((String)itr.next());
	    	}
    	}
    	return al;
    }
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	 public String getCustomerSurveyDetails(String ShipNumber,Long soId,String corpId){
		 String serviceDetails="";
		 List customerSurvey=this.getSession().createSQLQuery("select serviceName from  customersurveyfeedback where shipNumber='"+ShipNumber+"' and serviceOrderId='"+soId+"' and corpId='"+corpId+"' ").list();
		for(Object object : customerSurvey) {
			if(serviceDetails.equalsIgnoreCase("")){
				serviceDetails=object.toString();
			}else{
				serviceDetails=serviceDetails+","+object.toString();
			}
		}
		 return serviceDetails;
	 }
	 public List getFeebackDetails(String shipnumber,String DSserviceOrderId,String serviceName,String sessionCorpID){
    List customerDetails = new ArrayList();
	List customerSurvey=null;
	if(!serviceName.equalsIgnoreCase("NET")){
		customerSurvey=this.getSession().createSQLQuery("select customerRating,feedback,serviceDetails from  customersurveyfeedback where shipNumber='"+shipnumber+"' and serviceOrderId='"+DSserviceOrderId+"' and serviceName='"+serviceName+"' and corpId='"+sessionCorpID+"'  ").list();
	}else{
		customerSurvey=this.getSession().createSQLQuery("select customerRating,feedback,'' from  serviceorder where shipNumber='"+shipnumber+"' and id='"+DSserviceOrderId+"' and corpId='"+sessionCorpID+"'  ").list();
	}
	FeebackDetail dto=null;
	Iterator it = customerSurvey.iterator();
	while (it.hasNext()){
		Object []row= (Object [])it.next();
		dto = new FeebackDetail();
		dto.setCustomerRating(row[0]);
		dto.setFeedback(row[1]);
		dto.setServiceDetails(row[2]);
		customerDetails.add(dto);
	}
	return customerDetails;
	 }
	 public class FeebackDetail{
		private Object customerRating; 
		private Object feedback;
		private Object serviceDetails;
		
		public Object getCustomerRating() {
			return customerRating;
		}
		public void setCustomerRating(Object customerRating) {
			this.customerRating = customerRating;
		}
		public Object getFeedback() {
			return feedback;
		}
		public void setFeedback(Object feedback) {
			this.feedback = feedback;
		}
		public Object getServiceDetails() {
			return serviceDetails;
		}
		public void setServiceDetails(Object serviceDetails) {
			this.serviceDetails = serviceDetails;
		} 
	 }
	 public void resendEmailUpdateAjax(String updateQuery,String sessionCorpID,Long soId){
				 getSession().createSQLQuery("update dspdetails "+updateQuery+" where id="+soId+" and corpID='"+sessionCorpID+"'").executeUpdate();
	 }
	 
	 public void saveTenancyMgmtUtilities(String sessionCorpID,Long serviceOrderId, String fieldName, boolean fieldValue,String updatedBy){
		 this.getSession().createSQLQuery("update dspdetails set "+fieldName+" = "+fieldValue+",updatedOn = now(), updatedBy='"+updatedBy+"' where id="+serviceOrderId+" and corpID='"+sessionCorpID+"'").executeUpdate();
	 }
	 
	 public List<TenancyUtilityService> findUtilityServiceDetail(String sessionCorpID,Long serviceOrderId,String fieldName){
		 String query="";
		 if(fieldName!=null && !fieldName.equalsIgnoreCase("")){
		 	query="from TenancyUtilityService where parentId='"+serviceOrderId+"' and parentFieldName='"+fieldName+"'" ;
		 }else{
			 query="from TenancyUtilityService where parentId='"+serviceOrderId+"' and userUpdatedRow is true";
		 }
		return getHibernateTemplate().find(query);
	 }
	 
	 public void updateUtilityService(String updateQuery){
		 getHibernateTemplate().bulkUpdate(updateQuery);
	 }
	 
	 public String findUtilitiesCheckBoxValues(String sessionCorpID,Long serviceOrderId){
		 String checkBoxValues="";
		 List al = new ArrayList();
		 String query = " select concat(if(TEN_Gas_Water is true,1,0),'~',if(TEN_Gas is true,1,0),'~',if(TEN_Electricity is true,1,0),'~',if(TEN_TV_Internet_Phone is true,1,0),'~',if(TEN_mobilePhone is true,1,0),"
		 				+ " '~',if(TEN_furnitureRental is true,1,0),'~',if(TEN_cleaningServices is true,1,0),'~',if(TEN_parkingPermit is true,1,0),'~',if(TEN_communityTax is true,1,0), "
		 				+ " '~',if(TEN_insurance is true,1,0),'~',if(TEN_gardenMaintenance is true,1,0),'~',if(TEN_Miscellaneous is true,1,0),'~',if(TEN_Utility_Gas_Water is true,1,0),'~',if(TEN_Utility_Gas is true,1,0), "
		 				+ " '~',if(TEN_Utility_Electricity is true,1,0),'~',if(TEN_Utility_TV_Internet_Phone is true,1,0),'~',if(TEN_Utility_mobilePhone is true,1,0),'~',if(TEN_Utility_furnitureRental is true,1,0),'~',if(TEN_Utility_cleaningServices is true,1,0), "
		 				+ " '~',if(TEN_Utility_parkingPermit is true,1,0),'~',if(TEN_Utility_communityTax is true,1,0),'~',if(TEN_Utility_insurance is true,1,0),"
		 				+ " '~',if(TEN_Utility_gardenMaintenance is true,1,0),'~',if(TEN_Utility_Miscellaneous is true,1,0),'~',if(TEN_utilitiesIncluded  is null or TEN_utilitiesIncluded='','NA',TEN_utilitiesIncluded),"
		 				+ " '~',if(TEN_Gas_Electric is true,1,0),'~',if(TEN_Utility_Gas_Electric is true,1,0)) from dspdetails where id='"+serviceOrderId+"' and corpID='"+sessionCorpID+"'";
	    	List list=this.getSession().createSQLQuery(query).list();
	    	return checkBoxValues = list.get(0).toString();
	 }
	 
	 public List tenancyBillingPreviewList(Date billThroughFrom, Date billThroughTo,String billToCode, String sessionCorpID, String tenancyBillingGrpList){
		 try {
			 String billThroughFrom1 = "";
				if (!(billThroughFrom == null)) {
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(billThroughFrom));
					billThroughFrom1 = nowYYYYMMDD1.toString();
				}
				String billThroughTo1 = "";
				if (!(billThroughTo == null)) {
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(billThroughTo));
					billThroughTo1 = nowYYYYMMDD1.toString();
				}
			if(tenancyBillingGrpList!=null && !tenancyBillingGrpList.equalsIgnoreCase("")){
				String tempBillingGroup = "";
				String[] arrayTenancyBillingGrp = tenancyBillingGrpList.split(",");
							int arrayLength = arrayTenancyBillingGrp.length;
							for (int i = 0; i < arrayLength; i++) {
								if(tempBillingGroup.equalsIgnoreCase("")){
									tempBillingGroup = "'"+arrayTenancyBillingGrp[i].trim()+"'";
								}else{
									tempBillingGroup = tempBillingGroup+","+"'"+arrayTenancyBillingGrp[i].trim()+"'";
								}
							}
							
				String query = " select partnercode from partnerprivate where corpid='"+sessionCorpID+"' and storageBillingGroup in ("+tempBillingGroup+")";
				List billToCodeAllList =this.getSession().createSQLQuery(query).list();
				if(billToCodeAllList!=null && !billToCodeAllList.isEmpty()){
					String tempBillToCode = "";
					String[] arraybillToCode = billToCodeAllList.toString().replace("[", "").replace("]", "").split(",");
					int billToCodeLength = arraybillToCode.length;
					for (int i = 0; i < billToCodeLength; i++) {
						if(tempBillToCode.equalsIgnoreCase("")){
							tempBillToCode = "'"+arraybillToCode[i].trim()+"'";
						}else{
							tempBillToCode = tempBillToCode+","+"'"+arraybillToCode[i].trim()+"'";
						}
					}
					
					billToCode = tempBillToCode;
				}
			}else{
				billToCode =  "'"+billToCode+"'";
			}
			 List previewList = new ArrayList();
			 String previewQuery = "";
			 if(billToCode!=null && !billToCode.equalsIgnoreCase("") && !billToCode.equalsIgnoreCase("''")){
					previewQuery = " select s.shipnumber,concat(s.lastName,' ',s.firstName),b.billtocode,b.billtoname,'1' as billingCycle, "
			 				+ "	b.contract,d.id,d.TEN_vendorCode,d.TEN_vendorName,d.TEN_rentAmount,d.TEN_rentCurrency,d.TEN_rentAllowance,d.TEN_allowanceCurrency,d.TEN_housingRentVatPercent,"
			 				+ " b.billingCurrency, d.TEN_housingRentVatDesc,d.TEN_leaseExpireDate,b.billToAuthority,d.TEN_billThroughDate, "
			 				+ " LAST_DAY(DATE_ADD(d.TEN_billThroughDate , interval '1' MONTH)) as toDates,"
			 				+ "	DATEDIFF((DATE_ADD(d.TEN_billThroughDate , interval '1' MONTH)),d.TEN_billThroughDate )+1 as dateDiff "
					 		+ "	from serviceorder s left outer join billing b on s.id=b.id left outer join dspdetails d on s.id=d.id "
					 		+ " where b.billtocode in ("+billToCode+") and s.corpid='"+sessionCorpID+"' and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD')"
					 		+ " and d.TEN_billThroughDate >= ('"+billThroughFrom1+"') AND d.TEN_billThroughDate <= ('"+billThroughTo1+"') "
					 		+ " and d.TEN_leaseExpireDate > d.TEN_billThroughDate" ;
			 }else{
				 previewQuery = " select s.shipnumber,concat(s.lastName,' ',s.firstName),b.billtocode,b.billtoname,'1' as billingCycle, "
			 				+ "	b.contract,d.id,d.TEN_vendorCode,d.TEN_vendorName,d.TEN_rentAmount,d.TEN_rentCurrency,d.TEN_rentAllowance,d.TEN_allowanceCurrency,d.TEN_housingRentVatPercent,"
			 				+ " b.billingCurrency, d.TEN_housingRentVatDesc,d.TEN_leaseExpireDate,b.billToAuthority,d.TEN_billThroughDate, "
			 				+ " LAST_DAY(DATE_ADD(d.TEN_billThroughDate , interval '1' MONTH)) as toDates,"
			 				+ "	DATEDIFF((DATE_ADD(d.TEN_billThroughDate , interval '1' MONTH)),d.TEN_billThroughDate )+1 as dateDiff "
					 		+ "	from serviceorder s left outer join billing b on s.id=b.id left outer join dspdetails d on s.id=d.id "
					 		+ " where s.corpid='"+sessionCorpID+"' and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD')"
					 		+ " and d.TEN_billThroughDate >= ('"+billThroughFrom1+"') AND d.TEN_billThroughDate <= ('"+billThroughTo1+"') "
					 		+ " and d.TEN_leaseExpireDate > d.TEN_billThroughDate" ;
			 }
			 List previewAllList =this.getSession().createSQLQuery(previewQuery).list();
			 
				Iterator it = previewAllList.iterator();
				while (it.hasNext()){
					Object []row= (Object [])it.next();
					String chrgCode="";
					String chrgDesc="";
					String costElementVal="";
					String costElementDescVal="";
					BigDecimal contractRateVal = new BigDecimal(0);
					BigDecimal billingCycleVal = new BigDecimal(1);
					billingCycleVal = new BigDecimal(row[4].toString());
					String contractCurrencyVal = "";
					String billingCurrencyVal = "";
					if(row[14]!=null && !row[14].equals("")){
						billingCurrencyVal = row[14].toString();
					}
					BigDecimal vatPercentVal = new BigDecimal(0);
					if(row[13]!=null && !row[13].equals("")){
						vatPercentVal = new BigDecimal(row[13].toString());
					}
					for(int x=0;x<3;x++){
						TenancyBillingDTO dto = new TenancyBillingDTO();
						dto.setShipNumber(row[0]);
						dto.setShipperName(row[1]);
						dto.setBillToCode(row[2]);
						dto.setBillToName(row[3]);
						dto.setRentBillingCycle(new Double(row[4].toString()));
						dto.setRentCycle(row[4].toString());
						String query="";
						if(row[5]!=null && !row[5].equals("")){
							if(x==0){
								query="from Charges where contract='"+row[5].toString()+"' and corpid='"+sessionCorpID+"' and bucket='RENT'";
							}else if(x==1){
								query="from Charges where contract='"+row[5].toString()+"' and corpid='"+sessionCorpID+"' and bucket='TEN'";
							}else{
								query="from Charges where contract='"+row[5].toString()+"' and corpid='"+sessionCorpID+"' and bucket='EMG'";
							}
							List<Charges> chargeList = getHibernateTemplate().find(query);
							for(Charges crlist:chargeList){
								chrgCode=crlist.getCharge();
								chrgDesc = crlist.getDescription();
								dto.setVatExclude(crlist.getVATExclude());
								dto.setRecGl(crlist.getGl());
								dto.setPayGl(crlist.getExpGl());
								if((crlist.getBucket()!=null && !crlist.getBucket().equalsIgnoreCase("")) && crlist.getBucket().equalsIgnoreCase("TEN")){
									dto.setContractRate(crlist.getQuantity2preset().doubleValue());
									dto.setRentAmount(crlist.getQuantity2preset());
									contractCurrencyVal = crlist.getContractCurrency();
									dto.setContractCurrency(crlist.getContractCurrency());
									if(crlist.getCostElement()!=null && !crlist.getCostElement().equalsIgnoreCase("")){
										costElementVal = crlist.getCostElement();
									}
									if(crlist.getScostElementDescription()!=null && !crlist.getScostElementDescription().equalsIgnoreCase("")){
										costElementDescVal = crlist.getScostElementDescription();
									}
									dto.setCostElement(costElementVal);
									dto.setCostElementDescription(costElementDescVal);
									String amountAndVal = findAmountAndVatByExchangeRate(crlist.getContractCurrency(),crlist.getQuantity2preset(),billingCurrencyVal,new BigDecimal(0), sessionCorpID,billingCycleVal);
									if(!amountAndVal.equalsIgnoreCase("")){
										String [] amountAndValArr = amountAndVal.split("~");
										dto.setBaseRate(new Double(amountAndValArr[0]));
										dto.setBillingRate(new Double(amountAndValArr[1]));
										dto.setContractExRate(amountAndValArr[2].toString());
										dto.setBillingExRate(amountAndValArr[3].toString());
										dto.setRentVat(amountAndValArr[4].toString());
									}
								}
								if((crlist.getBucket()!=null && !crlist.getBucket().equalsIgnoreCase("")) && crlist.getBucket().equalsIgnoreCase("EMG")){
									dto.setContractRate(crlist.getQuantity2preset().doubleValue());
									dto.setRentAmount(crlist.getQuantity2preset());
									contractCurrencyVal = crlist.getContractCurrency();
									dto.setContractCurrency(crlist.getContractCurrency());
									if(crlist.getCostElement()!=null && !crlist.getCostElement().equalsIgnoreCase("")){
										costElementVal = crlist.getCostElement();
									}
									if(crlist.getScostElementDescription()!=null && !crlist.getScostElementDescription().equalsIgnoreCase("")){
										costElementDescVal = crlist.getScostElementDescription();
									}
									dto.setCostElement(costElementVal);
									dto.setCostElementDescription(costElementDescVal);
									String amountAndVal = findAmountAndVatByExchangeRate(crlist.getContractCurrency(),crlist.getQuantity2preset(),billingCurrencyVal,new BigDecimal(0), sessionCorpID,billingCycleVal);
									if(!amountAndVal.equalsIgnoreCase("")){
										String [] amountAndValArr = amountAndVal.split("~");
										dto.setBaseRate(new Double(amountAndValArr[0]));
										dto.setBillingRate(new Double(amountAndValArr[1]));
										dto.setContractExRate(amountAndValArr[2].toString());
										dto.setBillingExRate(amountAndValArr[3].toString());
										dto.setRentVat(amountAndValArr[4].toString());
									}
								}
								if((crlist.getBucket()!=null && !crlist.getBucket().equalsIgnoreCase("")) && crlist.getBucket().equalsIgnoreCase("RENT")){
									if(crlist.getCostElement()!=null && !crlist.getCostElement().equalsIgnoreCase("")){
										costElementVal = crlist.getCostElement();
									}
									if(crlist.getScostElementDescription()!=null && !crlist.getScostElementDescription().equalsIgnoreCase("")){
										costElementDescVal = crlist.getScostElementDescription();
									}
									dto.setCostElement(costElementVal);
									dto.setCostElementDescription(costElementDescVal);
								}
							}
							dto.setContractExRate(1.000);
							dto.setBillingExRate(1.000);
						}
						
						if(x==0){
							BigDecimal  rentAmountVal =new BigDecimal(row[9].toString());
							String rentCurrencyVal = row[10].toString();
							BigDecimal  rentAllowanceVal =new BigDecimal(row[11].toString());
							String rentAllowanceCurrencyVal = row[12].toString();
							int res;
							res = rentAmountVal.compareTo(rentAllowanceVal);
							
							if( res == 0 ){
								contractRateVal = rentAmountVal;
								contractCurrencyVal = rentCurrencyVal;
							}else if( res == 1 ){
								contractRateVal = rentAllowanceVal;
								contractCurrencyVal = rentAllowanceCurrencyVal;
							}else if( res == -1 ){
								contractRateVal = rentAmountVal;
								contractCurrencyVal = rentCurrencyVal;
							}
							dto.setContractRate(contractRateVal.doubleValue());
							dto.setContractCurrency(contractCurrencyVal);
							dto.setRentAmount(contractRateVal);
							String amountAndVal = findAmountAndVatByExchangeRate(contractCurrencyVal,contractRateVal,billingCurrencyVal,vatPercentVal, sessionCorpID,billingCycleVal);
							if(!amountAndVal.equalsIgnoreCase("")){
								String [] amountAndValArr = amountAndVal.split("~");
								dto.setBaseRate(new Double(amountAndValArr[0]));
								dto.setBillingRate(new Double(amountAndValArr[1]));
								dto.setContractExRate(amountAndValArr[2].toString());
								dto.setBillingExRate(amountAndValArr[3].toString());
								dto.setRentVat(amountAndValArr[4].toString());
							}
							dto.setVatDesc(row[15]);
							dto.setVatPercent(row[13]);
						}else{
							dto.setVatDesc("2");
							dto.setVatPercent("0");
						}
						dto.setChargeCode(chrgCode);
						dto.setChargeDescription(chrgDesc);
						dto.setBillingCurrency(billingCurrencyVal);
						dto.setBillingContract(row[5]);
						dto.setBillToAuthority(row[17]);
						dto.setDspId(Long.parseLong(row[6].toString()));
						dto.setDspBillThroughDate(row[18]);
						dto.setToDate(row[19]);
						dto.setDateDifference(row[20]);
						
						if(billingCurrencyVal.equalsIgnoreCase("")){
							dto.setNotFound("Billing Currency Missing");
						}else if(chrgCode.equalsIgnoreCase("")){
							dto.setNotFound("Charge Missing");
						}else if(row[15]==null || row[15].equals("")){
							dto.setNotFound("VAT desc Missing");
						}else if(contractCurrencyVal.equalsIgnoreCase("")){
							dto.setNotFound("Contract Currency Missing");
						}
						

						previewList.add(dto);
					}
				if(row[6]!=null){
					List utilityList = getFromTenancyUtility(sessionCorpID,Long.parseLong(row[6].toString()),billThroughFrom1, billThroughTo1);
					 
					Iterator itr = utilityList.iterator();
					while (itr.hasNext()){
						Object []row1= (Object [])itr.next();
						TenancyBillingDTO dto1 = new TenancyBillingDTO();
					
						dto1 = new TenancyBillingDTO();
						dto1.setShipNumber(row[0]);
						dto1.setShipperName(row[1]);
						dto1.setBillToCode(row[2]);
						dto1.setBillToName(row[3]);
						Double  rentBillingCycleVal =new Double(0);
						if(row1[6]!=null && !row1[6].equals("")){
							rentBillingCycleVal = new Double(row1[6].toString());
						}
						dto1.setRentBillingCycle(rentBillingCycleVal);
						if(row1[6]!=null && !row1[6].equals("")){
							dto1.setRentCycle(row1[6].toString());
						}else{
							dto1.setRentCycle("");
						}
						
						BigDecimal  revAmountVal =new BigDecimal(0);
						if(row1[4]!=null && !row1[4].equals("")){
							revAmountVal = new BigDecimal(row1[4].toString());
						}
						BigDecimal utilityBillCycle = new BigDecimal(0);
						if(row1[6]!=null && !row1[6].equals("")){
							utilityBillCycle = new BigDecimal(row1[6].toString());
						}
						dto1.setRentAmount(revAmountVal.multiply(utilityBillCycle));
						dto1.setContractRate(revAmountVal.doubleValue());
						BigDecimal divideVal = new BigDecimal(100);
						BigDecimal utilityVatPercent = new BigDecimal(0);
						if(row1[8]!=null && !row1[8].equals("")){
							utilityVatPercent = new BigDecimal(row1[8].toString());
						}
						String chrgCodeBucket="";
						Map<String, String> utilityFieldNameMap =findTenancyUtilityFieldName();
						for(Map.Entry<String, String> rec:utilityFieldNameMap.entrySet()){
							if(rec.getValue().trim().equalsIgnoreCase(row1[1].toString().trim()) ){
								chrgCodeBucket=rec.getKey();
							
							if(chrgCodeBucket!=null && !chrgCodeBucket.equalsIgnoreCase("")){
								String query="from Charges where contract='"+row[5].toString()+"' and corpid='"+sessionCorpID+"' and bucket='"+chrgCodeBucket+"'";
								List<Charges> chargeList = getHibernateTemplate().find(query);
								for(Charges crlist:chargeList){
									chrgCode=crlist.getCharge();
									chrgDesc = crlist.getDescription();
									dto1.setChargeCode(chrgCode);
									dto1.setChargeDescription(chrgDesc);
									dto1.setVatExclude(crlist.getVATExclude());
									dto1.setRecGl(crlist.getGl());
									dto1.setPayGl(crlist.getExpGl());
									if(crlist.getCostElement()!=null && !crlist.getCostElement().equalsIgnoreCase("")){
										costElementVal = crlist.getCostElement();
									}
									if(crlist.getScostElementDescription()!=null && !crlist.getScostElementDescription().equalsIgnoreCase("")){
										costElementDescVal = crlist.getScostElementDescription();
									}
									dto1.setCostElement(costElementVal);
									dto1.setCostElementDescription(costElementDescVal);
								}
							}
						
							}
						}
						
						if(row1[5]!=null && !row1[5].equals("")){
							contractCurrencyVal = row1[5].toString();
						}
						
						String amountAndVal = findAmountAndVatByExchangeRate(contractCurrencyVal,revAmountVal,billingCurrencyVal,utilityVatPercent, sessionCorpID,utilityBillCycle);
						if(!amountAndVal.equalsIgnoreCase("")){
							String [] amountAndValArr = amountAndVal.split("~");
							dto1.setBaseRate(new Double(amountAndValArr[0]));
							dto1.setBillingRate(new Double(amountAndValArr[1]));
							dto1.setContractExRate(amountAndValArr[2]);
							dto1.setBillingExRate(amountAndValArr[3]);
							dto1.setRentVat(amountAndValArr[4]);
						}
						
						String vendorCode="";
						String vendorName="";
						if(row1[2]!=null && !row1[2].equals("")){
							vendorCode = row1[2].toString();
						}
						if(row1[3]!=null && !row1[3].equals("")){
							vendorName = row1[3].toString();
						}
						dto1.setVendorCode(vendorCode);
						dto1.setVendorName(vendorName);
						
						dto1.setBillingCurrency(billingCurrencyVal);
						String vatDesc="";
						if(row1[7]!=null && !row1[7].equals("")){
							vatDesc = row1[7].toString();
						}
						dto1.setVatDesc(vatDesc);
						dto1.setVatPercent(utilityVatPercent);
						dto1.setContractCurrency(contractCurrencyVal);
						
						if(dto1.getChargeCode()==null || dto1.getChargeCode().equals("")){
							dto1.setNotFound("Charge Missing");
						}else if(vatDesc==null || vatDesc.equalsIgnoreCase("")){
							dto1.setNotFound("VAT desc Missing");
						}else if(row1[6]==null || row1[6].equals("")){
							dto1.setNotFound("Billing Cycle Missing");
						}else if(row[16]!=null && row1[10]==null){
							dto1.setNotFound("Utility End Date Missing");
						}else if(contractCurrencyVal.equalsIgnoreCase("")){
							dto1.setNotFound("Contract Currency Missing");
						}
						dto1.setBillingContract(row[5]);
						dto1.setBillToAuthority(row[17]);
						dto1.setUtilityId(Long.parseLong(row1[0].toString()));
						String utilityBillThrough="";
						if(row1[9]!=null && !row1[9].equals("")){
							utilityBillThrough = row1[9].toString();
						}
						dto1.setUtilityBillThroughDate(utilityBillThrough);
						String toDateVal="";
						if(row1[11]!=null && !row1[11].equals("")){
							toDateVal = row1[11].toString();
						}
						dto1.setToDate(toDateVal);
						String dateDiffVal="";
						if(row1[12]!=null && !row1[12].equals("")){
							dateDiffVal = row1[12].toString();
						}
						dto1.setDateDifference(dateDiffVal);
						
						previewList.add(dto1);
				
					}
				
				}
					
			}
			 return previewList;
		 } catch (Exception e) {
				logger.error("Error executing query"+ e,e);
		    	e.printStackTrace();
		}
		 return null;
	 }
	 private Map<String, String> currencyExchangeRate;
	 DecimalFormat decimalFormat = new DecimalFormat("#.####");
	 public String findAmountAndVatByExchangeRate(String contractCurrencyVal,BigDecimal contractRateVal,String billingCurrencyVal,BigDecimal vatPercentVal, String sessionCorpID,BigDecimal billingCycleVal){
		 currencyExchangeRate=getExchangeRateWithCurrency(sessionCorpID);
		 Double contractExRate = 1.000;
		 Double billingExRate = 1.000;
		 BigDecimal baseRateVal = new BigDecimal(0);
		 BigDecimal billingRateVal = new BigDecimal(0);
		 BigDecimal billingAmountVal = new BigDecimal(0);
		 BigDecimal vatPercentValue = new BigDecimal(0);
		 String returnVal = "";
		 if(currencyExchangeRate.containsKey(contractCurrencyVal)){
			 contractExRate=Double.parseDouble(currencyExchangeRate.get(contractCurrencyVal));
			 baseRateVal = new BigDecimal(decimalFormat.format(contractRateVal.doubleValue()/contractExRate.doubleValue()));
		 }
		 if(currencyExchangeRate.containsKey(billingCurrencyVal)){
			 billingExRate=Double.parseDouble(currencyExchangeRate.get(billingCurrencyVal));
			 billingRateVal = new BigDecimal(decimalFormat.format((baseRateVal.doubleValue())*(billingExRate)));
			 billingAmountVal = new BigDecimal(decimalFormat.format((billingRateVal.doubleValue())*(billingCycleVal.doubleValue())));
			 Double vatAmountTemp = (billingAmountVal.doubleValue()*vatPercentVal.doubleValue())/100;
			 vatPercentValue = new BigDecimal(decimalFormat.format(vatAmountTemp));
		 }
		 
		 return returnVal = baseRateVal+"~"+billingRateVal+"~"+contractExRate+"~"+billingExRate+"~"+vatPercentValue;
	 }
	 public Map<String, String> getExchangeRateWithCurrency(String corpID){
			List<ExchangeRate> list;
			list = getHibernateTemplate().find("from ExchangeRate order by valueDate"); 
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
				for (ExchangeRate exchangeRate : list) {
					parameterMap.put(exchangeRate.getCurrency(), exchangeRate.getBaseCurrencyRate()+"");
				} 
			return parameterMap;
		}
	 public Map<String, String> findTenancyUtilityFieldName(){
		 Map<String, String> utilityFieldNameMap= new LinkedHashMap<String, String>();
		 utilityFieldNameMap.put("WTR", "TEN_Utility_Gas_Water");
		 utilityFieldNameMap.put("GAS", "TEN_Utility_Gas");
		 utilityFieldNameMap.put("INS", "TEN_Utility_insurance");
		 utilityFieldNameMap.put("ELC", "TEN_Utility_Electricity");
		 utilityFieldNameMap.put("GSM", "TEN_Utility_mobilePhone");
		 utilityFieldNameMap.put("TVINT", "TEN_Utility_TV_Internet_Phone");
		 utilityFieldNameMap.put("GAEL", "TEN_Utility_Gas_Electric");
		 utilityFieldNameMap.put("CLEAN", "TEN_Utility_cleaningServices");
		 utilityFieldNameMap.put("PARK", "TEN_Utility_parkingPermit");
		 utilityFieldNameMap.put("GARD", "TEN_Utility_gardenMaintenance");
		 utilityFieldNameMap.put("FRENT", "TEN_Utility_furnitureRental");
		 utilityFieldNameMap.put("CTAX", "TEN_Utility_communityTax");
		 utilityFieldNameMap.put("MISC", "TEN_Utility_Miscellaneous");
		 
		 return utilityFieldNameMap;
	 }
	 
	 public List getFromTenancyUtility(String sessionCorpID,Long dspId,String billThroughFrom1,String billThroughTo1){
		 List list = new ArrayList();
		 String query = " select id,parentFieldName,TEN_utilityVendorCode,TEN_utilityVendorName,TEN_utilityAllowanceAmount,"
		 				+ " TEN_utilityAllowanceCurrency,TEN_billingCycleUtility,TEN_utilityVatDesc,TEN_utilityVatPercent,"
		 				+ " TEN_utilityBillThrough,TEN_utilityEndDate,"
		 				+ " LAST_DAY(DATE_ADD(TEN_utilityBillThrough , interval TEN_billingCycleUtility MONTH)) as toDates,"
		 				+ " DATEDIFF((DATE_ADD(TEN_utilityBillThrough , interval TEN_billingCycleUtility MONTH)),TEN_utilityBillThrough )+1 as dateDiff"
		 				+ " from tenancyutilityservice where parentId='"+dspId+"' and corpid='"+sessionCorpID+"' and userUpdatedRow is true "
		 				+ " and TEN_utilityBillThrough >= ('"+billThroughFrom1+"') AND TEN_utilityBillThrough <= ('"+billThroughTo1+"') "
		 				+ " and TEN_utilityEndDate > TEN_utilityBillThrough" ;
		 list =this.getSession().createSQLQuery(query).list();
		 return list;
	 }
	 
	 public void updateBillThroughDate(String tableName,String fieldName,String billThroughDate, Long id, String userName){
		 try {
				getHibernateTemplate().bulkUpdate("update "+tableName+" set "+fieldName+"='"+billThroughDate+"', updatedBy='"+userName+"', updatedOn=now() where id='"+id+"'");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	 e.printStackTrace();
			}
	 }
	 
	 public class TenancyBillingDTO{
		 private Object shipNumber;
		 private Object shipperName;
		 private Object billToCode;
		 private Object billToName;
		 private Object rentAmount;
		 private Object rentVat;
		 private Double rentBillingCycle;
		 private Object chargeCode;
		 private Object chargeDescription;
		 private Object vendorCode;
		 private Object vendorName;
		 private Object rentCycle;
		 private Object notFound;
		 private Object billingContract;
		 private Object vatExclude;
		 private Object recGl;
		 private Object payGl;
		 private Object billToAuthority;
		 private Object contractCurrency;
		 private Object billingCurrency; 
		 private Object contractExRate;
		 private Object billingExRate;
		 private Object vatDesc;
		 private Object vatPercent;
		 private Double baseRate;
		 private Double contractRate;
		 private Double billingRate;
		 private Long dspId;
		 private Object dspBillThroughDate;
		 private Long utilityId;
		 private Object utilityBillThroughDate;
		 private Object dateDifference;
		 private Object toDate;
		 private Object costElement;
		 private Object costElementDescription;
		 
		 
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getBillToCode() {
			return billToCode;
		}
		public void setBillToCode(Object billToCode) {
			this.billToCode = billToCode;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getShipperName() {
			return shipperName;
		}
		public void setShipperName(Object shipperName) {
			this.shipperName = shipperName;
		}
		public Double getRentBillingCycle() {
			return rentBillingCycle;
		}
		public void setRentBillingCycle(Double rentBillingCycle) {
			this.rentBillingCycle = rentBillingCycle;
		}
		public Object getChargeCode() {
			return chargeCode;
		}
		public void setChargeCode(Object chargeCode) {
			this.chargeCode = chargeCode;
		}
		public Object getChargeDescription() {
			return chargeDescription;
		}
		public void setChargeDescription(Object chargeDescription) {
			this.chargeDescription = chargeDescription;
		}
		public Object getVendorCode() {
			return vendorCode;
		}
		public void setVendorCode(Object vendorCode) {
			this.vendorCode = vendorCode;
		}
		public Object getVendorName() {
			return vendorName;
		}
		public void setVendorName(Object vendorName) {
			this.vendorName = vendorName;
		}
		public Object getNotFound() {
			return notFound;
		}
		public void setNotFound(Object notFound) {
			this.notFound = notFound;
		}
		public Object getBillingContract() {
			return billingContract;
		}
		public void setBillingContract(Object billingContract) {
			this.billingContract = billingContract;
		}
		public Object getVatExclude() {
			return vatExclude;
		}
		public void setVatExclude(Object vatExclude) {
			this.vatExclude = vatExclude;
		}
		public Object getRecGl() {
			return recGl;
		}
		public void setRecGl(Object recGl) {
			this.recGl = recGl;
		}
		public Object getPayGl() {
			return payGl;
		}
		public void setPayGl(Object payGl) {
			this.payGl = payGl;
		}
		public Object getBillToAuthority() {
			return billToAuthority;
		}
		public void setBillToAuthority(Object billToAuthority) {
			this.billToAuthority = billToAuthority;
		}
		public Object getContractCurrency() {
			return contractCurrency;
		}
		public void setContractCurrency(Object contractCurrency) {
			this.contractCurrency = contractCurrency;
		}
		public Object getBillingCurrency() {
			return billingCurrency;
		}
		public void setBillingCurrency(Object billingCurrency) {
			this.billingCurrency = billingCurrency;
		}
		public Object getContractExRate() {
			return contractExRate;
		}
		public void setContractExRate(Object contractExRate) {
			this.contractExRate = contractExRate;
		}
		public Object getBillingExRate() {
			return billingExRate;
		}
		public void setBillingExRate(Object billingExRate) {
			this.billingExRate = billingExRate;
		}
		public Object getVatDesc() {
			return vatDesc;
		}
		public void setVatDesc(Object vatDesc) {
			this.vatDesc = vatDesc;
		}
		public Object getVatPercent() {
			return vatPercent;
		}
		public void setVatPercent(Object vatPercent) {
			this.vatPercent = vatPercent;
		}
		public Double getContractRate() {
			return contractRate;
		}
		public void setContractRate(Double contractRate) {
			this.contractRate = contractRate;
		}
		public Object getRentAmount() {
			return rentAmount;
		}
		public void setRentAmount(Object rentAmount) {
			this.rentAmount = rentAmount;
		}
		public Object getRentVat() {
			return rentVat;
		}
		public void setRentVat(Object rentVat) {
			this.rentVat = rentVat;
		}
		public Double getBaseRate() {
			return baseRate;
		}
		public void setBaseRate(Double baseRate) {
			this.baseRate = baseRate;
		}
		public Double getBillingRate() {
			return billingRate;
		}
		public void setBillingRate(Double billingRate) {
			this.billingRate = billingRate;
		}
		public Object getRentCycle() {
			return rentCycle;
		}
		public void setRentCycle(Object rentCycle) {
			this.rentCycle = rentCycle;
		}
		public Long getDspId() {
			return dspId;
		}
		public void setDspId(Long dspId) {
			this.dspId = dspId;
		}
		public Long getUtilityId() {
			return utilityId;
		}
		public void setUtilityId(Long utilityId) {
			this.utilityId = utilityId;
		}
		public Object getDspBillThroughDate() {
			return dspBillThroughDate;
		}
		public void setDspBillThroughDate(Object dspBillThroughDate) {
			this.dspBillThroughDate = dspBillThroughDate;
		}
		public Object getUtilityBillThroughDate() {
			return utilityBillThroughDate;
		}
		public void setUtilityBillThroughDate(Object utilityBillThroughDate) {
			this.utilityBillThroughDate = utilityBillThroughDate;
		}
		public Object getDateDifference() {
			return dateDifference;
		}
		public void setDateDifference(Object dateDifference) {
			this.dateDifference = dateDifference;
		}
		public Object getToDate() {
			return toDate;
		}
		public void setToDate(Object toDate) {
			this.toDate = toDate;
		}
		public Object getCostElement() {
			return costElement;
		}
		public void setCostElement(Object costElement) {
			this.costElement = costElement;
		}
		public Object getCostElementDescription() {
			return costElementDescription;
		}
		public void setCostElementDescription(Object costElementDescription) {
			this.costElementDescription = costElementDescription;
		}
	 }
	public Map<String, String> getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}
	public void setCurrencyExchangeRate(Map<String, String> currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}
	public DecimalFormat getDecimalFormat() {
		return decimalFormat;
	}
	public void setDecimalFormat(DecimalFormat decimalFormat) {
		this.decimalFormat = decimalFormat;
	}
	 
}
