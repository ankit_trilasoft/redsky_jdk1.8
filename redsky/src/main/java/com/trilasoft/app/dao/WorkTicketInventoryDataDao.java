package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.WorkTicketInventoryData;


public interface WorkTicketInventoryDataDao  extends GenericDao<WorkTicketInventoryData, Long> {
	public List checkInventoryData(String corpID, String barcode,String shipNumber);
	public List getAllInventory(String sessionCorpID, String shipno);
}
