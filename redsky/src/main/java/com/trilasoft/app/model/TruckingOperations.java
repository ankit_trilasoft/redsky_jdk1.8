package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="truckingoperations")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class TruckingOperations extends BaseObject{
	private String corpID;
	private Long id;
	private Long ticket;
	private String localTruckNumber;
	private String description;
	private Boolean isTruckUsed;
	private Date workDate;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String driver;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("ticket", ticket)
				.append("localTruckNumber", localTruckNumber)
				.append("description", description)
				.append("isTruckUsed", isTruckUsed)
				.append("workDate", workDate).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("driver", driver).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TruckingOperations))
			return false;
		TruckingOperations castOther = (TruckingOperations) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id).append(ticket, castOther.ticket)
				.append(localTruckNumber, castOther.localTruckNumber)
				.append(description, castOther.description)
				.append(isTruckUsed, castOther.isTruckUsed)
				.append(workDate, castOther.workDate)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(driver, castOther.driver).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(ticket)
				.append(localTruckNumber).append(description)
				.append(isTruckUsed).append(workDate).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn).append(driver)
				.toHashCode();
	}
	/**
	 * @return the corpID
	 */
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	/**
	 * @param corpID the corpID to set
	 */
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	/**
	 * @return the createdBy
	 */
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the description
	 */
	@Column(length=28)
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the localTruckNumber
	 */
	@Column(length=20)
	public String getLocalTruckNumber() {
		return localTruckNumber;
	}
	/**
	 * @param localTruckNumber the localTruckNumber to set
	 */
	public void setLocalTruckNumber(String localTruckNumber) {
		this.localTruckNumber = localTruckNumber;
	}
	/**
	 * @return the ticket
	 */
	@Column
	public Long getTicket() {
		return ticket;
	}
	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	/**
	 * @return the updatedBy
	 */
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Boolean getIsTruckUsed() {
		return isTruckUsed;
	}
	public void setIsTruckUsed(Boolean isTruckUsed) {
		this.isTruckUsed = isTruckUsed;
	}
	@Column
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	@Column
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
}