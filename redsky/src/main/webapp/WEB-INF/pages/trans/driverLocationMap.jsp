<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>   
    <title>Driver Map</title>   
    <META HTTP-EQUIV='Pragma' CONTENT='no-cache'">
	<META HTTP-EQUIV="Expires" CONTENT="-1"> 
    <meta name="heading" content="Driver Location Map"/>   
    <script language="javascript" type="text/javascript">
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
form {
margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
.listheadred {    
    color: red;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;    
    text-decoration: none;
}
#content {
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
}


#content-containertable.override{
width:100%;
}

#content-containertable.override div{
height:460px;
overflow:scroll;
}
.price_tleft{ !margin-top:2px; }
</style> 
<!--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>-->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>" djConfig="parseOnLoad:true, isDebug:false"></script>

<script>

function initLoader() { 
	 var script = document.createElement("script");
     script.type = "text/javascript";
  	 script.src = "https://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadMap";
     document.body.appendChild(script);
     setTimeout('',6000);
     }
var geocoder;
var map;
var letter1 = "";
var letter = "";
var letter3 = "";
var flag1="0";
var count1=-1;
var driverDataMap = new Object();
var letterMap = new Object();
function loadMap() { 
	var address1="";
	var city="";
	var zip="";
	var state="";
	var country = "";
    map = new GMap2(document.getElementById('map'));
    geocoder = new GClientGeocoder();

		dojo.xhrPost({
	       form: "driverLocationForm",
	       url:"driverLocationPoint.html",
	        timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	       	var d=document.getElementById("driverList");	        		
	        d.innerHTML='';	  
	       			var j = 0;
	       			var oldAddress="";	       			
	      for(var i=0; i < jsonData.ports.length; i++)
		  {
	    	  var address = jsonData.ports[i].vanLastLocation;
			  if(address!='null' && address!=''){
	           letter = String.fromCharCode("A".charCodeAt(0) + j);
	           j++;
		       var tempLetter = letter;	             	   
	 	   		   if(flag1=="1")
	 	   		   {
		 	   		   var temp=String.fromCharCode("A".charCodeAt(0) + count1);
		 	   		   letter = temp+letter;
	 	   		   }
	 	   		   if(jsonData.ports[i].currentVanID!='null' && jsonData.ports[i].currentVanID!=''){
	 	   		 d.innerHTML+='<table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" ><span onmouseover="findTotalAmount(\''+jsonData.ports[i].partnerCode+'\',this);" onmouseout="ajax_hideTooltip()"><a href="javascript:getDriverInvolvement(\''+jsonData.ports[i].partnerCode+'\',\''+jsonData.ports[i].name.replace("'","")+'\',\''+jsonData.ports[i].vanLastLocation+'\',\''+jsonData.ports[i].validNationalCode+'\')"><u>'+jsonData.ports[i].name+' / '+jsonData.ports[i].currentVanID+' </u></a><b class="listheadred">('+letter+')</b></span> </td></tr></table>';
			    }else{
			    d.innerHTML+='<table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext"  ><span onmouseover="findTotalAmount(\''+jsonData.ports[i].partnerCode+'\',this);" onmouseout="ajax_hideTooltip()"><a href="javascript:getDriverInvolvement(\''+jsonData.ports[i].partnerCode+'\',\''+jsonData.ports[i].name.replace("'","")+'\',\''+jsonData.ports[i].vanLastLocation+'\',\''+jsonData.ports[i].validNationalCode+'\')"><u>'+jsonData.ports[i].name+'</u></a><b class="listheadred">('+letter+')</b></span> </td></tr></table>';
			    }
			    }else{	
				   d.innerHTML+='<table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext"  ><span onmouseover="findTotalAmount(\''+jsonData.ports[i].partnerCode+'\',this);" onmouseout="ajax_hideTooltip()"><a href="javascript:getDriverInvolvement(\''+jsonData.ports[i].partnerCode+'\',\''+jsonData.ports[i].name.replace("'","")+'\',\''+jsonData.ports[i].vanLastLocation+'\',\''+jsonData.ports[i].validNationalCode+'\')"><u>'+jsonData.ports[i].name+'</u></a></span></td></tr></table>';	               	           
			    }
	        	   console.log("address: " + address);
	        	   if(oldAddress.indexOf(address)  < 0 ){
		        	   driverDataMap[jsonData.ports[i].vanLastLocation] = jsonData.ports[i];				   
		        	   letterMap[jsonData.ports[i].vanLastLocation] = letter;
		               if(address!='null' && address!=''){
		               geocoder.getLocations(address, addDriverToMap); 
		               }
	               }
                   oldAddress= oldAddress+"~"+jsonData.ports[i].vanLastLocation;
					if(tempLetter=="Z" && address!=null && address!='')
					{
					flag1="1";
					j=0;
					count1++;
					}				  
	        	}
	       }	       
	   });
}
     var j = 0; 
   function addDriverToMap(response)
   {
         if(!response.Placemark) return;
   			   place = response.Placemark[0];
		      point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);	          
	     	  var baseIcon = new GIcon(G_DEFAULT_ICON);
  	          var letteredIconR = new GIcon(baseIcon);
              letter = String.fromCharCode("A".charCodeAt(0) + j);
	         	   j++;
		       var tempLetter = letter;	             	   
		       if(flag1=="1")
			   {
		   		   var temp=String.fromCharCode("A".charCodeAt(0) + count1);
		   		   letter = temp+letter;
			   }	
 	    
				    letteredIconR.image = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=4.5|0|FF0000|100|b|"+letterMap[response.name];			  			   						    
				    markerOptions = { icon:letteredIconR };
				    marker = new GMarker(point, markerOptions); 
					map.addOverlay(marker, markerOptions);
					   		if(tempLetter=="Z")
								{
									flag1="1";
									j=0;
									count1++;
								}
								map.setCenter(point, 8);
	          map.setZoom(4);	          
			map.setUIToDefault();								
	  	   map.disableScrollWheelZoom();
			
			var fn = markerClickFnPorts(point,driverDataMap[response.name]);
				GEvent.addListener(marker, "mouseover", fn);
				GEvent.addListener(marker, "mouseout", onMouseOutFunc);
   }
 function onMouseOutFunc() {
                      var point = this;                     
                      setTimeout("map.closeInfoWindow()",2000);
                    }
  
   
   	  function getDriverInvolvement(partnerCode,driverName,vLocation,driverVanId) {
   	  		if(partnerCode!='')
   	  			{	
   	  			  	window.open("driverInvolvementMap.html?driverId="+partnerCode+"&driverName="+driverName+"&vLocation="+vLocation+"&driverVanId="+driverVanId+"&decorator=popup&popup=true","forms",',type=fullWindow,fullscreen,scrollbars=yes,resizable=yes')					
				}
	  }
   

	  function markerClickFnPorts(point, portDetails) {
	   return function() {
		   if (!portDetails) return;
	       var name = portDetails.name;
	       var agency = portDetails.driverAgency;
		   var location = portDetails.vanLastLocation;
		   var rTime = portDetails.reportTime;
		   var aCube = portDetails.vanAvailCube;
		   var vanNum = portDetails.currentVanID;
		   if(aCube=='null')
		   {
		   aCube='';
		   }
		   if(vanNum=='null'){
		   vanNum='';
		   }
		   if((location!='null')&&(location!=''))
		   {
				   var loc = location.split(",");
				   location=loc[0]+', '+loc[1];
		   }		   
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;">('+agency+') '+name+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;">'+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;">'+rTime+'</h4><br style="line-height:11px;"><h4 style="line-height:0px;">Cube Available: '+aCube+'</h4><br style="line-height:11px;"><h4 style="line-height:0px;">Van#: '+vanNum+' </h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);
	   }
   }
	  
	  function findTotalAmount(partnerCode,position){
			var url="findAmount.html?partnerCode="+partnerCode+"&decorator=simple&popup=true";
			ajax_showTooltip(url,position);		
	  }
		
	  function refreshDriverType(){
		  var driverTypeValue = document.forms['driverLocationForm'].elements['driverTypeValue'].value;
		  location.href="driverLocationMap.html?driverTypeValue="+driverTypeValue;		
	  }
</script>  
</head>
<s:form id="driverLocationForm" action="" method="post" validate="true" >
<s:hidden name="geoLatitude"/>
<s:hidden name="geoLongitude"/>

<div id="layer1" style="width:100%; margin-top:25px;"><!--sandeep-->
<div id="newmnav">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Driver Map</span></a></li>
	  <li><a href="driverLocationList.html" ><span>Driver List</span></a></li>					  	
	  </ul>
</div>
<div class="spnblk" style="!margin-bottom:-10px;">&nbsp;</div> 	

  <table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0"> 
  <tr>
      <td align="left" style="width:21%; !width:25%;background-color:#e1ebef;" valign="top">
       <div id="priceinfotab_bg" style="width:99%;">
	       <div class="price_tleft" style="!padding-top:18px;padding-top:2px;font-size:12px;">&nbsp;Driver&nbsp;Type&nbsp;:&nbsp;
	       <s:select name="driverTypeValue" cssClass="list-menu" list="%{driverType}" cssStyle="width:110px" headerKey="" headerValue="" onchange="refreshDriverType();"/></div>
       </div>
       
        <div id="content-containertable" class="override" style="!margin-top:15px;">			
      <div class="price_tleft" id="driverList" style="width:97%;border:1px dotted #219DD1;border-right:none;min-height:520px;"></div>
      </div>  
      </td>
      
      <td align="left" width="79%" valign="top">
      <div id="map" style="width:100%;border:1px dotted #219DD1; height: 550px"></div>

      </td>
      <B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
  </tr>
  </table>
</s:form>
<script type="text/javascript">
try{
initLoader();
}
catch(e){}
</script>