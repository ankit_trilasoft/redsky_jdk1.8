<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="AA_Scan_Cover_WT"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="486"
		 columnSpacing="0"
		 leftMargin="90"
		 rightMargin="36"
		 topMargin="72"
		 bottomMargin="18"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Work Ticket Number" isForPrompting="true" class="java.lang.String"/>
	<parameter name="User Name" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
     'AA_scan_cover_WT',
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`billToCode` AS serviceorder_billToCode,
     serviceorder.`registrationNumber` AS serviceorder_registrationNumber,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`job` AS serviceorder_job,
     serviceorder.`sequenceNumber` AS serviceorder_sequenceNumber,
     workticket.`shipNumber` AS workticket_shipNumber,
     workticket.`warehouse` AS workticket_warehouse,
     workticket.`service` AS workticket_service,
     workticket.`date1` AS workticket_date1,
     workticket.`date2` AS workticket_date2,
     workticket.`scanned` AS workticket_scanned,
     workticket.`ticket` AS workticket_ticket,
     workticket.`corpID` AS workticket_corpID,
     workticket.`serviceOrderId` AS workticket_serviceOrderId,
     serviceorder.`id` AS serviceorder_id,
     billing.`billToCode` AS billing_billToCode,
     billing.`billtoname` AS billing_billtoname,
     IF(serviceorder.`registrationNumber`<>"",CONCAT("*",serviceorder.`registrationNumber`,"*")," ")AS REGBARCODE,
     CONCAT("*",serviceorder.`job`,"*")AS JOBBARCODE,
     CONCAT("*",IF(serviceorder.`mode`="SEA","SUR",serviceorder.`mode`),"*")AS MODEBARCODE,
     CONCAT("*",serviceorder.`shipnumber`,"*")AS SOBARCODE,
     CONCAT("*",billing.`billToCode`,"*")AS BILLTOBARCODE,
     CONCAT("*",serviceorder.`sequenceNumber`,"*")AS SEQBARCODE,
     CONCAT("*","COVERPAGE","*") AS COVERPAGE
FROM
     `serviceorder` serviceorder INNER JOIN `workticket` workticket ON serviceorder.`id` = workticket.`serviceOrderId`
     LEFT OUTER JOIN `billing` billing ON serviceorder.`id` = billing.`id`
WHERE
 workticket.`corpID` = "SSCW"
 AND workticket.`ticket` like $P{Work Ticket Number}]]></queryString>

	<field name="AA_scan_cover_WT" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_billToCode" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="serviceorder_sequenceNumber" class="java.lang.String"/>
	<field name="workticket_shipNumber" class="java.lang.String"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_date1" class="java.sql.Timestamp"/>
	<field name="workticket_date2" class="java.sql.Timestamp"/>
	<field name="workticket_scanned" class="java.sql.Timestamp"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_corpID" class="java.lang.String"/>
	<field name="workticket_serviceOrderId" class="java.lang.Long"/>
	<field name="serviceorder_id" class="java.lang.Long"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="REGBARCODE" class="java.lang.String"/>
	<field name="JOBBARCODE" class="java.lang.String"/>
	<field name="MODEBARCODE" class="java.lang.String"/>
	<field name="SOBARCODE" class="java.lang.String"/>
	<field name="BILLTOBARCODE" class="java.lang.String"/>
	<field name="SEQBARCODE" class="java.lang.String"/>
	<field name="COVERPAGE" class="java.lang.String"/>

	<variable name="jobbar" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA["*"+$F{serviceorder_job}+"*"]]></variableExpression>
	</variable>
	<variable name="billtobar" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA["*"+$F{billing_billToCode}+"*"]]></variableExpression>
	</variable>
	<variable name="coverpage" class="java.lang.String" resetType="None" calculation="Nothing">
		<variableExpression><![CDATA["*COVERPAGE*"]]></variableExpression>
		<initialValueExpression><![CDATA["*COVERPAGE*"]]></initialValueExpression>
	</variable>
	<variable name="shipper" class="java.lang.String" resetType="None" calculation="Nothing">
		<variableExpression><![CDATA[$F{serviceorder_lastName}+" , "+$F{serviceorder_firstName}]]></variableExpression>
	</variable>
	<variable name="cfbar" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA["*"+$F{serviceorder_sequenceNumber}+"*"]]></variableExpression>
	</variable>
	<variable name="sobar" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA["*"+$F{serviceorder_shipNumber}+"*"]]></variableExpression>
	</variable>
	<variable name="regbar" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA["*"+$F{serviceorder_registrationNumber}+"*"]]></variableExpression>
	</variable>
	<variable name="modebar" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA["*"+$F{serviceorder_mode}+"*"]]></variableExpression>
	</variable>

		<group  name="so" >
			<groupExpression><![CDATA[$F{serviceorder_shipNumber}]]></groupExpression>
			<groupHeader>
			<band height="471"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="90"
						y="9"
						width="301"
						height="23"
						forecolor="#000000"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{shipper}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="32"
						y="301"
						width="116"
						height="17"
						key="staticText-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Addendum Order For Service]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="431"
						width="19"
						height="15"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="395"
						width="19"
						height="15"
						key="rectangle-4"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="340"
						width="19"
						height="15"
						key="rectangle-5"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="359"
						width="19"
						height="15"
						key="rectangle-6"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="302"
						width="19"
						height="15"
						key="rectangle-9"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="413"
						width="19"
						height="15"
						key="rectangle-10"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="322"
						width="19"
						height="15"
						key="rectangle-11"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="32"
						y="321"
						width="116"
						height="15"
						key="staticText-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[AR 23 Form]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="340"
						width="116"
						height="15"
						key="staticText-11"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Bill of Lading]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="358"
						width="116"
						height="16"
						key="staticText-12"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Bingo Sheet]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="377"
						width="116"
						height="15"
						key="staticText-13"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Claim Form]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="395"
						width="116"
						height="15"
						key="staticText-14"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Correspomdence]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="32"
						y="413"
						width="116"
						height="15"
						key="staticText-15"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Customs Forms]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="6"
						y="377"
						width="19"
						height="15"
						key="rectangle-20"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="182"
						y="301"
						width="118"
						height="17"
						key="staticText-16"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[DD Forms]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="431"
						width="19"
						height="15"
						key="rectangle-21"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="395"
						width="19"
						height="15"
						key="rectangle-22"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="340"
						width="19"
						height="15"
						key="rectangle-23"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="359"
						width="19"
						height="15"
						key="rectangle-24"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="302"
						width="19"
						height="15"
						key="rectangle-25"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="413"
						width="19"
						height="15"
						key="rectangle-26"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="322"
						width="19"
						height="15"
						key="rectangle-27"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="182"
						y="321"
						width="118"
						height="15"
						key="staticText-17"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Driver's Receipt]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="182"
						y="340"
						width="136"
						height="15"
						key="staticText-18"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Estimate/Cube/Order for Service]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="182"
						y="358"
						width="118"
						height="16"
						key="staticText-19"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Forwarder Instructions]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="182"
						y="377"
						width="118"
						height="15"
						key="staticText-20"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Insurance Application]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="182"
						y="395"
						width="118"
						height="15"
						key="staticText-21"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Invoice - Client]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="182"
						y="413"
						width="118"
						height="15"
						key="staticText-22"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Invoice - Vendor]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="377"
						width="19"
						height="15"
						key="rectangle-28"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="357"
						y="301"
						width="109"
						height="17"
						key="staticText-23"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Inventory Origin]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="395"
						width="19"
						height="15"
						key="rectangle-30"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="340"
						width="19"
						height="15"
						key="rectangle-31"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="359"
						width="19"
						height="15"
						key="rectangle-32"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="302"
						width="19"
						height="15"
						key="rectangle-33"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="413"
						width="19"
						height="15"
						key="rectangle-34"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="322"
						width="19"
						height="15"
						key="rectangle-35"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="357"
						y="321"
						width="109"
						height="15"
						key="staticText-24"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Inventory Destination]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="357"
						y="340"
						width="109"
						height="15"
						key="staticText-25"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Move Authorization/Contract]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="357"
						y="358"
						width="109"
						height="16"
						key="staticText-26"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Notification Letter]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="357"
						y="377"
						width="109"
						height="15"
						key="staticText-27"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Operational Documents]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="357"
						y="395"
						width="109"
						height="15"
						key="staticText-28"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Pack Material labor Record]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="357"
						y="413"
						width="109"
						height="15"
						key="staticText-29"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Rate Quotation]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="331"
						y="377"
						width="19"
						height="15"
						key="rectangle-36"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="357"
						y="431"
						width="66"
						height="17"
						key="staticText-30"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[WH Receipt]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="5"
						y="453"
						width="19"
						height="15"
						key="rectangle-38"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="156"
						y="449"
						width="19"
						height="15"
						key="rectangle-39"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="330"
						y="432"
						width="19"
						height="15"
						key="rectangle-41"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="330"
						y="450"
						width="19"
						height="15"
						key="rectangle-43"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="357"
						y="452"
						width="55"
						height="15"
						key="staticText-31"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Weight Ticket]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="182"
						y="449"
						width="55"
						height="15"
						key="staticText-32"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Workticket]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="33"
						y="468"
						width="114"
						height="0"
						key="line-4"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="32"
						y="446"
						width="118"
						height="0"
						key="line-6"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="181"
						y="431"
						width="118"
						height="15"
						key="staticText-33"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Invoice - Credit Memo]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="115"
						y="280"
						width="180"
						height="17"
						key="staticText-34"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Document Types ( Check the Box )]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="237"
						y="116"
						width="200"
						height="23"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="6"
						y="116"
						width="208"
						height="23"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_sequenceNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="13"
						y="198"
						width="201"
						height="23"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_registrationNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="300"
						y="198"
						width="163"
						height="25"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="6"
						y="64"
						width="175"
						height="44"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" pdfFontName="Helvetica" size="24" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{JOBBARCODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="237"
						y="64"
						width="195"
						height="44"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" size="24"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{MODEBARCODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="295"
						y="225"
						width="186"
						height="46"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" size="24"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{BILLTOBARCODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="13"
						y="221"
						width="265"
						height="50"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" size="24"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{REGBARCODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="225"
						y="145"
						width="248"
						height="46"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" size="24"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{SOBARCODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="6"
						y="145"
						width="208"
						height="46"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" size="24"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{SEQBARCODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="6"
						y="41"
						width="120"
						height="23"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="237"
						y="41"
						width="130"
						height="23"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="381"
						y="-60"
						width="100"
						height="18"
						key="textField-3"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{User Name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="380"
						y="-77"
						width="96"
						height="15"
						key="staticText-36"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Printed By:]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="6"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="31"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="25"
						y="0"
						width="360"
						height="24"
						forecolor="#663300"
						key="staticText-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<text><![CDATA[Scanning Cover Sheet  - Workticket]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="44"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="156"
						y="0"
						width="201"
						height="35"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="SansSerif" size="24"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{COVERPAGE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="381"
						y="22"
						width="100"
						height="18"
						key="textField-2"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{User Name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="380"
						y="5"
						width="96"
						height="15"
						key="staticText-35"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Printed By:]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
