package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccessInfoDao;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.service.AccessInfoManager;

public class AccessInfoManagerImpl extends GenericManagerImpl<AccessInfo, Long> implements AccessInfoManager{

	AccessInfoDao accessInfoDao;
	public AccessInfoManagerImpl(AccessInfoDao accessInfoDao) {
		super(accessInfoDao);
		this.accessInfoDao =accessInfoDao;
	}
	public List getAccessInfo(Long cid){
		  return  accessInfoDao.getAccessInfo(cid);
	  }
	public List getAccessInfoForSO(Long cid){
		  return  accessInfoDao.getAccessInfoForSO(cid);
	  }
	public List getIdBySoID(Long cid){
		  return  accessInfoDao.getIdBySoID(cid);
	  }
}
