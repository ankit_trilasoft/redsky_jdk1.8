package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.DataSecuritySet;

public interface DataSecuritySetManager extends GenericManager<DataSecuritySet, Long> {
	public List findById(Long id);

	public List findMaximumId();

	public List getDataSecurityFilterList();

	public List getPermission(String permissionName);

	public List getPermissionList();

	public List getSetList();

	public void saveDataSecuritySet(DataSecuritySet dataSecuritySet);

	public void deleteFilters(String prefixDataSet, String sessionCorpID);

	public List getPartnerUsersList(String prefixDataSet, String sessionCorpID);

	public List getUnassignedPartnerUsersList(String prefixDataSet, String sessionCorpID);

	public List getDataSecuritySet(String prefixDataSet, String sessionCorpID);

	public List getAssociatedAgents(String parentAgentCode, String sessionCorpID);

	public List search(String name, String description);

	public List validateUserName(String userName, String sessionCorpID);

	public List getUsers(String roleName, String userName, String sessionCorpID);

	public List getRoleList(String sessionCorpID);

	public List getUserList(String sessionCorpID, String name);

	public List getUserRoleSet(String userName, String sessionCorpID);

	public List getRequestedPartner(String partnerCode, String sessionCorpID);

	public List getAgentsUsersList(String partnerCode, String sessionCorpID,String checkOption);

	public Map<String, List> getChildAgentCodeMap();
}
