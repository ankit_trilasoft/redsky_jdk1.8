<%@ include file="/common/taglibs.jsp"%>  

<head>
<title><fmt:message key="portalReference.title"/></title> 
<meta name="heading" content="<fmt:message key='portalReference.heading'/>"/> 
</head> 
<s:form id="portalReference" name="portalReference" method="post" validate="true"> 
<c:set var="quoteID" value="<%=request.getParameter("id")%>" scope="session"/>
<c:set var="customeFileID" value="${customerFile.id}" scope="session"/>
<div id="Layer1" style="width:90%">
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style=""><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
	<tr><td align="left" class="listwhitetext" style="width:100%">
		<table class="detailTabLabel" border="0" style="width:100%">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr>
		  		<td align="right"><fmt:message key="portalReference.agent"/></td>
		  		<td align="left"><s:textfield name="partnerQuote.vendorName"  value="%{partnerQuote.vendorName}" size="25"  cssClass="input-textUpper" readonly="true"/></td>
		  		<td align="right"><fmt:message key="partnerQuote.shipNumber"/></td>
		  		<td align="left" ><s:textfield name="serviceOrder.lastName"  value="%{partnerQuote.quote}"cssClass="input-textUpper" size="16" readonly="true"/></td>
		  		<td align="right"><fmt:message key="portal.quoteStatus"/></td>
		  		<td align="left"><s:textfield name="customerFile.status"  value="%{partnerQuote.quoteStatus}"cssClass="input-textUpper" size="16" readonly="true"/></td>
		  		<td align="right"><fmt:message key="partnerQuote.Requestor"/></td>
		  		<td align="left"><s:textfield name="customerFile.corpID" value="%{customerFile.corpID}"cssClass="input-textUpper" size="16" readonly="true"/>
		  	</tr>
		  	<tr>
		  		<td align="right"><fmt:message key="portalReference.city"/></td>
		  		<td align="left"><s:textfield name="customerFile.originCity" value="%{customerFile.originCity}" cssClass="input-textUpper" size="16" readonly="true"/></td>
		  		<td align="right"><fmt:message key="portalReference.country"/></td>
		  		<td align="left" ><s:textfield name="customerFile.destinationCity" value="%{customerFile.originCountry}"cssClass="input-textUpper"  size="16" readonly="true"/></td>
		  		<td align="right"><fmt:message key="portalReference.service"/></td>
		  		<td align="left"><s:textfield name="customerFile.destinationCity" value="%{partnerQuote.quoteType}"cssClass="input-textUpper"  size="16" readonly="true"/></td>
		  		<td align="right"><fmt:message key="portalReference.accountName"/></td>
		  		<td align="left"><s:textfield name="serviceOrder.mode" value="%{customerFile.billToName}"cssClass="input-textUpper" size="16" readonly="true"/></td>
		  	</tr>
		  	<tr><td align="left" height="5px"></td></tr>
		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>   
  </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
 <s:set name="partnerQuotess" value="partnerQuotess" scope="partnerQuotess"/> 
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Service Orders</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<display:table name="serviceOrders" class="table" requestURI="" id="portalReference"  defaultsort="1" export="true" pagesize="10"> 
<display:column property="shipNumber" sortable="true" href="portalReferenceDetails.html?cid=${customeFileID}&sid=&quoteID=${quoteID}" 
titleKey="portalReference.id"/>
<display:column property="commodity" sortable="true" titleKey="portalReference.Commodity"/> 
<display:column headerClass="containeralign" style="text-align: right;" property="estimateGrossWeight" sortable="true" titleKey="portalReference.Weight1"/> 
<display:column property="mode" sortable="true" titleKey="portalReference.Mode"/> 
<display:column property="equipment" headerClass="containeralign" style="text-align: right;" sortable="true" titleKey="portalReference.Equipment"/>
<display:column headerClass="containeralign" style="text-align: right;" property="estimateCubicFeet" sortable="true" titleKey="portalReference.Volume1"/> 
<display:setProperty name="export.excel.filename" value="Service Orders.xls"/>
<display:setProperty name="export.csv.filename" value="Service Orders.csv"/>
</display:table> 

</div>


<c:out value="${buttons}" escapeXml="false" /> 



</s:form>
<script type="text/javascript"> 

//highlightTableRows("portalReferenceList"); 

</script> 
