<script type="text/javascript">
var resId,resDay;
var httpHub = getHTTPObject();
function validateHubLimit(id,day,fieldName,workTicketID){
	resId=id;resDay=day;
	var category = document.getElementById('category'+id).value;
	var descript = document.getElementById('descript'+id).value;
	var cost = document.getElementById('cost'+id).value;
	if(workTicketID != null && workTicketID != ''){
		showOrHide(1);
		var tickets = workTicketID+'-'+category+'-'+descript+'-'+cost;
		var url = "checkHubLimitForResourceSOAjax.html?ajax=1&decorator=simple&popup=true&tickets="+tickets;
		httpHub.open("GET", url, true);
		httpHub.onreadystatechange = function(){ handleHttpHubResponse(workTicketID);};
		httpHub.send(null);
	}else{
		autoSaveAjax(id,day,fieldName,'','');
	}
}

function handleHttpHubResponse(workTicketID){
    if(httpHub.readyState == 4){
      var results = httpHub.responseText;
      results = results.trim();
     	if(results == 'Y'){
     		showOrHide(0);
          	var agree = confirm("Exceeding the limit & will be sent in Dispatch Queue for Approval");
          	if(agree){
          		autoSaveAjax(resId,resDay,'','PENDING',workTicketID);
          	}
     	 }else{
     		autoSaveAjax(resId,resDay,'','',workTicketID);
     	 }
      }
}

function AutoCompleterAjaxCall(descriptId,categoryId,divId){
	var descriptVal = document.getElementById(descriptId).value;
	var type = document.getElementById(categoryId).value;
	
	document.getElementById('serviceOrderForm').setAttribute("AutoComplete","off");
	 document.forms['serviceOrderForm'].elements['tempId1'].value=descriptId;
	 document.forms['serviceOrderForm'].elements['tempId2'].value=divId;
	 new Ajax.Request('/redsky/resourceAutocomplete.html?ajax=1&tempCategory='+type+'&tempresource='+encodeURIComponent(descriptVal)+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
		    
			      var response = transport.responseText || "no response text";
//			      alert(response);
//					alert(divId);
					
                   var mydiv = document.getElementById(divId);
                   document.getElementById(divId).style.display = "block";
                 
                   mydiv.innerHTML = response;
                   mydiv.show(); 
			    },
			    onFailure: function(){ 
				//    alert('Something went wrong...');
				    }
			  });
}

function findResource(type,descriptId){
	if(type.length > 0){
		document.getElementById(descriptId).value='';
	}
}

function checkChar(descript,descriptId,categoryId,divId) {
	try {
		 	AutoCompleterAjaxCall(descriptId,categoryId,divId);
	}catch(e){
		alert(e);
		}
	}
</script>