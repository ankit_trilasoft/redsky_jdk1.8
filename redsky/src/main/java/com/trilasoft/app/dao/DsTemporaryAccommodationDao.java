package com.trilasoft.app.dao;
import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsTemporaryAccommodation;

public interface DsTemporaryAccommodationDao extends GenericDao<DsTemporaryAccommodation, Long>{
	List getListById(Long id);
}
