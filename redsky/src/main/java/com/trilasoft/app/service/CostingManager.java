package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.Costing;

public interface CostingManager extends GenericManager<Costing, Long> {
    public List findMaxId();
    public List checkById(Long id);
    public List findInvoiceNumber();
    public int genrateInvoiceNumber(Long newInvoiceNumber,String shipNumber);
}
