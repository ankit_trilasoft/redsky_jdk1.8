1)    Bugzilla ticket 13935

 

// corpid=STAR

 update customerfile set moveType='Quote' where controlFlag='Q' and corpid='STAR' and (moveType='' || moveType is null);

 update customerfile set moveType='BookedMove' where controlFlag='C' and corpid='STAR' and (moveType='' || moveType is null);

 update serviceorder set moveType='Quote' where controlFlag='Q' and corpid='STAR' and (moveType='' || moveType is null);

 update serviceorder set moveType='BookedMove' where controlFlag='C' and corpid='STAR' and (moveType='' || moveType is null);

 update customerfile set controlFlag='C' where controlFlag='Q' and corpid='STAR';

 update serviceorder set controlFlag='C' where controlFlag='Q' and corpid='STAR';

 

// corpid=BONG

update customerfile set moveType='Quote' where controlFlag='Q' and corpid='BONG' and (moveType='' || moveType is null);

update customerfile set moveType='BookedMove' where controlFlag='C' and corpid='BONG' and (moveType='' || moveType is null);

update serviceorder set moveType='Quote' where controlFlag='Q' and corpid='BONG' and (moveType='' || moveType is null);

update serviceorder set moveType='BookedMove' where controlFlag='C' and corpid='BONG' and (moveType='' || moveType is null);

update customerfile set controlFlag='C' where controlFlag='Q' and corpid='BONG';

update serviceorder set controlFlag='C' where controlFlag='Q' and corpid='BONG';

 

// corpid=ICMG

update customerfile set moveType='Quote' where controlFlag='Q' and corpid='ICMG' and (moveType='' || moveType is null);

update customerfile set moveType='BookedMove' where controlFlag='C' and corpid='ICMG' and (moveType='' || moveType is null);

update serviceorder set moveType='Quote' where controlFlag='Q' and corpid='ICMG' and (moveType='' || moveType is null);

update serviceorder set moveType='BookedMove' where controlFlag='C' and corpid='ICMG' and (moveType='' || moveType is null);

update customerfile set controlFlag='C' where controlFlag='Q' and corpid='ICMG';

update serviceorder set controlFlag='C' where controlFlag='Q' and corpid='ICMG';

 

// corpid=HIGH

update customerfile set moveType='Quote' where controlFlag='Q' and corpid='HIGH' and (moveType='' || moveType is null);

update customerfile set moveType='BookedMove' where controlFlag='C' and corpid='HIGH' and (moveType='' || moveType is null);

update serviceorder set moveType='Quote' where controlFlag='Q' and corpid='HIGH' and (moveType='' || moveType is null);

update serviceorder set moveType='BookedMove' where controlFlag='C' and corpid='HIGH' and (moveType='' || moveType is null);

update customerfile set controlFlag='C' where controlFlag='Q' and corpid='HIGH';

update serviceorder set controlFlag='C' where controlFlag='Q' and corpid='HIGH';

 

======================================================================================================================

// Query to modify for control flag in STAR.

 

select id from sqlextract where corpid='STAR' and sqltext like '%controlflag%';

 

select id from todorule where corpid='STAR' and expression like '%controlflag%';

 

update todorule set expression=replace(expression,"serviceorder.controlFlag = 'C'","(serviceorder.controlFlag='C' AND serviceorder.movetype='BookedMove')")

where corpid='STAR' and id in(151,156,158,199);

 

update todorule set expression=replace(expression,"customerfile.controlFlag = 'C'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='STAR' and id in(160);

 

====================================================================================================================================

 

// Query to modify for control flag in BONG.

 

select rulenumber from todorule where corpid='BONG' and expression like '%controlflag%';

 

select rulenumber from todorule where corpid='BONG' and expression like '%controlFlag%';

select rulenumber from todorule where corpid='BONG' and expression like '%customerfile.controlFlag = \'C\'%';

 

select rulenumber from todorule where corpid='BONG' and expression like '%customerfile.controlFlag = \'C\'%' and expression not like '%(customerfile.controlFlag=\'C\' AND customerfile.movetype=\'BookedMove\')%';

 

 

update todorule set expression=replace(expression,"customerfile.controlFlag = 'C'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='BONG' and rulenumber in (1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20,24,26,27,31,32,33,34,40,41,42,45,46,47,52,53,55,56,57,49,58,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,96,99,100,101,102,103,104);

 

update todorule set expression=replace(expression,"customerfile.controlflag = 'c'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='BONG' and rulenumber in (57);

 

==============================================================================================================================

// Query to modify for control flag in ICMG.

 

select * from todorule where corpid='ICMG' and expression like '%controlflag%';

 

select * from todorule where corpid='ICMG' and expression like '%controlFlag%';

select * from todorule where corpid='ICMG' and expression like '%customerfile.controlFlag = \'C\'%';

 

select * from todorule where corpid='ICMG' and expression like '%customerfile.controlFlag = \'C\'%' and expression not like '%(customerfile.controlFlag=\'C\' AND customerfile.movetype=\'BookedMove\')%';

 

 

update todorule set expression=replace(expression,"customerfile.controlFlag = 'C'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='ICMG' and rulenumber in (1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,24,25,26,27,31,32,33,34,40,41,42,45,46,47,48,49,50,51,52,53,54,55,56,57,58);

 

update todorule set expression=replace(expression,"customerfile.controlflag = 'c'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='ICMG' and rulenumber in (57);

===============================================================================================================================

 

// Query to modify for control flag in HIGH.

 

select * from todorule where corpid='HIGH' and expression like '%controlflag%';

 

select * from todorule where corpid='HIGH' and expression like '%controlFlag%';

select * from todorule where corpid='HIGH' and expression like '%customerfile.controlFlag = \'C\'%';

 

select * from todorule where corpid='HIGH' and expression like '%customerfile.controlFlag = \'C\'%' and expression not like '%(customerfile.controlFlag=\'C\' AND customerfile.movetype=\'BookedMove\')%';

 

 

update todorule set expression=replace(expression,"customerfile.controlFlag = 'C'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='HIGH' and rulenumber in (1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,24,25,26,27,31,32,33,34,40,41,42,45,46,47,48,49,50,51,52,53,54,55,56,57,58,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,106,107,108,109,111,128,129,130,134,135,136);

 

update todorule set expression=replace(expression,"customerfile.controlflag = 'c'","(customerfile.controlFlag='C' AND customerfile.movetype='BookedMove')")

where corpid='HIGH' and rulenumber in (57);

===============================================================================================================================================================

1)    Bugzilla ticket 13887

-- Display

        select recVatDescr,payVatDescr from accountline where corpid='UTSI' and id=8837896;

-- Update

update accountline set recVatDescr='VOER_2',payVatDescr='VOER_2',updatedby='BackendUpd_13887',updatedon=now() where corpid='UTSI' and id=8837896;

 

-- Display

select recVatDescr,payVatDescr from accountline where corpid='UTSI' and id=8830463;

-- Update

update accountline set recVatDescr='VOER_4',payVatDescr='',updatedby='BackendUpd_13887',updatedon=now() where corpid='UTSI' and id=8830463; 

================================================================================================================================================================

1)    Bugzilla ticket 13887

-- Display

        select recVatDescr,payVatDescr from accountline where corpid='UTSI' and id=8837896;

-- Update

update accountline set recVatDescr='VOER_2',payVatDescr='VOER_2',updatedby='BackendUpd_13887',updatedon=now() where corpid='UTSI' and id=8837896;

 

-- Display

select recVatDescr,payVatDescr from accountline where corpid='UTSI' and id=8830463;

-- Update

update accountline set recVatDescr='VOER_4',payVatDescr='',updatedby='BackendUpd_13887',updatedon=now() where corpid='UTSI' and id=8830463; 

=======================================================================================================================================================================

update partnerpublic set mailingcountrycode='GBR',billingcountrycode='GBR',updatedBy='BackendUpd_13904',updatedOn=now() where partnercode in('T182354'); 

===========================================================================================================================================================================

  Bugzilla ticket 13425

 

// To find id partnerprivate id

select  ppvt.id

FROM    partnerpublic ppbl

        INNER JOIN partnerprivate ppvt        ON ppbl.id = ppvt.partnerPublicId

WHERE ppbl.status='Inactive'

AND ppvt.status<>'Inactive';

 

// to update according to partnerprivate  id which is getting from 1st query.

UPDATE  

        partnerprivate  

SET  

          status = 'Inactive'  

        , updatedOn = NOW()  

        , UpdatedBy = 'BackendUpdate_13425'   

WHERE  

        id IN (select  ppvt.id

FROM    partnerpublic ppbl

        INNER JOIN partnerprivate ppvt        ON ppbl.id = ppvt.partnerPublicId

WHERE ppbl.status='Inactive'

AND ppvt.status<>'Inactive');  
        
        =======================================================================================================================================
        
        update partnerpublic set mailingcountrycode='GBR',billingcountrycode='GBR',updatedBy='BackendUpd_13904',updatedOn=now() where partnercode in('T182354');

  
