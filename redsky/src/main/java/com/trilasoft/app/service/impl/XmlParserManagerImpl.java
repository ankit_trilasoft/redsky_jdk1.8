package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.XmlParserDao;
import com.trilasoft.app.model.XmlParser;
import com.trilasoft.app.service.XmlParserManager;


public class XmlParserManagerImpl extends GenericManagerImpl<XmlParser,Long> implements XmlParserManager{

	private XmlParserDao xmlParserDao;
	public XmlParserManagerImpl(XmlParserDao xmlParserDao) {
		super(xmlParserDao);
		this.xmlParserDao = xmlParserDao;
	}

	public String saveData(String sessionCorpID, String file, String userName) {
		
		return xmlParserDao.saveData(sessionCorpID, file, userName);
	}

	public String saveNokRate(String sessionCorpID, String file, String userName) {
		return xmlParserDao.saveNokRate(sessionCorpID, file, userName);
	}
	/*public String saveUsdRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveUsdRate(sessionCorpID,file,userName);
	}*/
	/*public String saveGBPRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveGBPRate(sessionCorpID,file,userName);
	}*/
	/*public String saveCNYRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveCNYRate(sessionCorpID,file,userName);	
	}*/
	/*public String saveINRRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveINRRate(sessionCorpID, file, userName);
	}*/
	/*public String saveBRLRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveBRLRate(sessionCorpID, file, userName);
	}*/
	/*public String saveKRWRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveKRWRate(sessionCorpID, file, userName);
	}
	*/public String saveCADRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveCADRate(sessionCorpID, file, userName);
	}
	/*public String saveSGDRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveSGDRate(sessionCorpID, file, userName);
	}*/
	/*public String saveJODRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveJODRate(sessionCorpID, file, userName);
	}*/
	/*public String saveIDRRate(String sessionCorpID, String file, String userName){
		return xmlParserDao.saveIDRRate(sessionCorpID, file, userName);
	}*/
	public String saveDynamicRate(String sessionCorpID, String file, String userName,String baseCurrency){
		return xmlParserDao.saveDynamicRate(sessionCorpID, file, userName, baseCurrency);
	}
}
