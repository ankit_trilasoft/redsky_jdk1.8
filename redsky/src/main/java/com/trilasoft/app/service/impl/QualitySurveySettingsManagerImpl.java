package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.QualitySurveySettingsDao;
import com.trilasoft.app.model.QualitySurveySettings;
import com.trilasoft.app.service.QualitySurveySettingsManager;

public class QualitySurveySettingsManagerImpl extends GenericManagerImpl<QualitySurveySettings, Long> implements QualitySurveySettingsManager{
	QualitySurveySettingsDao qualitySurveySettingsDao;
public QualitySurveySettingsManagerImpl(QualitySurveySettingsDao qualitySurveySettingsDao)
{
	super(qualitySurveySettingsDao);
	this.qualitySurveySettingsDao = qualitySurveySettingsDao;
}
public List getQualitySurveySettingsDate(String accountId, String sessionCorpID,String mode)
{
	return qualitySurveySettingsDao.getQualitySurveySettingsDate(accountId, sessionCorpID,mode);
}
public List getEmailConfigrationSoList(String sessionCorpID,String partnerCode,String partnerName){
	return qualitySurveySettingsDao.getEmailConfigrationSoList(sessionCorpID,partnerCode,partnerName);
}
public Boolean getExcludeFPU(String partnerCode,String corpId){
	return qualitySurveySettingsDao.getExcludeFPU(partnerCode, corpId);
}
public List getQualityEmailListByPartnerCode(String partnerCode, String corpID){
	return qualitySurveySettingsDao.getQualityEmailListByPartnerCode(partnerCode, corpID);
}
public int getQualityListReference(String partnerCode, String corpID,String mode){
	return qualitySurveySettingsDao.getQualityListReference(partnerCode, corpID, mode);
}
}
