<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="IntBillOverDue" pageWidth="849" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="789" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="ffa2a2a8-4736-47f3-90c8-cbb524b83ddf">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[select
	"IntBillOverDue-ICMG.jrxml",
	serviceorder.shipnumber AS serviceorder_shipnumber,
	serviceorder.companyDivision AS serviceorder_companyDivision,
	concat(serviceorder.lastname,', ',serviceorder.firstname) as 'Shipper', 
	billing.billtocode AS billing_billtocode,
	serviceorder.status AS serviceorder_status,
	serviceorder.routing AS serviceorder_routing,
	date_format(trackingstatus.loadA,'%d/%m/%Y') AS trackingstatus_loadA,
	date_format(trackingstatus.deliveryA,'%d/%m/%Y')AS trackingstatus_deliveryA,
	lower(billing.personbilling) AS billing_personbilling,
	ifnull(notes.subject,'') As notes_subject,
        date_format(now(),'%d/%m/%Y') as currentdate
FROM serviceorder serviceorder
	INNER JOIN trackingstatus trackingstatus on serviceorder.id = trackingstatus.id	AND trackingstatus.corpid = $P{Corporate ID}
	INNER JOIN billing billing on serviceorder.id = billing.id AND billing.corpid = $P{Corporate ID}
	LEFT OUTER JOIN notes notes ON serviceorder.id = notes.noteskeyId AND notes.noteSubType ='Accounting Issue' AND notes.notestatus <> 'CMP' and notetype = 'Service Order' AND notes.corpid = $P{Corporate ID}
WHERE  serviceorder.job = 'INT'
	AND billing.billComplete is null
	AND serviceorder.status not in ('CNCL','CLSD','DWNLD','OSIT')
	AND serviceorder.corpid = $P{Corporate ID}
	AND trackingstatus.loadA is not null
order by serviceorder.companyDivision,billing.billtocode, trackingstatus.loadA, trackingstatus.deliveryA]]>
	</queryString>
	<field name="IntBillOverDue-ICMG.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipnumber" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="billing_billtocode" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="trackingstatus_loadA" class="java.lang.String"/>
	<field name="trackingstatus_deliveryA" class="java.lang.String"/>
	<field name="billing_personbilling" class="java.lang.String"/>
	<field name="notes_subject" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="20" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="0" y="0" width="398" height="18" uuid="6456079e-4316-498d-bb21-88cba0be95b7"/>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["Company Division: "+($F{serviceorder_companyDivision}==null ? "" : $F{serviceorder_companyDivision})]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="31" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" mode="Transparent" x="0" y="0" width="789" height="30" uuid="b6812640-e757-4900-9e57-60f670f93828"/>
				<box topPadding="5"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="14" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[International Billing Overdue Report]]></text>
			</staticText>
			<line>
				<reportElement key="line-3" x="0" y="30" width="789" height="1" forecolor="#999999" uuid="23648989-c0c3-48c0-a138-c1e4642c791c"/>
			</line>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="19" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="0" y="0" width="95" height="18" backcolor="#D2CCCC" uuid="a422bcfd-ba2e-44a7-bc3e-63c8bd98f0b9"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[S/O #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="95" y="0" width="124" height="18" backcolor="#D2CCCC" uuid="b5473041-38e8-4912-9b4e-69e04f9461b3"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="219" y="0" width="81" height="18" backcolor="#D2CCCC" uuid="68b7d46b-b4f3-4977-bcc8-a1eca8d53272"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Opaque" x="300" y="0" width="50" height="18" backcolor="#D2CCCC" uuid="fd7e7d5e-55c1-4d4e-b99e-660c07de89f1"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" mode="Opaque" x="350" y="0" width="58" height="18" backcolor="#D2CCCC" uuid="c6f6d001-4a57-46ea-9b36-2d7ec66158cc"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Routing]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" mode="Opaque" x="408" y="0" width="65" height="18" backcolor="#D2CCCC" uuid="5172e340-1539-4bcb-b348-709f143d4192"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Loading]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" mode="Opaque" x="473" y="0" width="65" height="18" backcolor="#D2CCCC" uuid="a9be7759-2019-4a3a-a1c2-df30c40f735e"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Delivery]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" mode="Opaque" x="538" y="0" width="85" height="18" backcolor="#D2CCCC" uuid="20bba1dd-ba67-4af9-a727-92edc9736e77"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Billing Person]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="623" y="0" width="166" height="18" backcolor="#D2CCCC" uuid="37609efe-b776-45d8-a584-1db2b29e2956"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Note]]></text>
			</staticText>
			<line>
				<reportElement key="line-2" x="0" y="18" width="789" height="1" forecolor="#999999" backcolor="#999999" uuid="a2759e08-8dcf-473e-a015-a5c82289069f"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="0" y="0" width="95" height="18" backcolor="#E2E1E1" uuid="2edde373-6a3b-4432-8351-b9a4b6b6aa2f"/>
				<box topPadding="5" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipnumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="95" y="0" width="124" height="18" uuid="8a4495aa-d76c-428f-9209-95a3cf3626d4"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="219" y="0" width="81" height="18" backcolor="#E2E1E1" uuid="05c45540-3cee-4734-9620-ae51a039e01f"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billtocode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="300" y="0" width="50" height="18" uuid="bc4dc91c-8b5f-4791-8179-6591a50236e7"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_status}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="350" y="0" width="58" height="18" backcolor="#E2E1E1" uuid="e846190f-3d00-4573-ae9c-e237b7565f55"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_routing}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="408" y="0" width="65" height="18" uuid="91e5a8b1-f9da-4297-83eb-95639901298e"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_loadA}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="473" y="0" width="65" height="18" backcolor="#E2E1E1" uuid="1fb10378-dde8-42f1-946e-2e657ced2456"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_deliveryA}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="538" y="0" width="85" height="18" uuid="3e0b3617-4508-4270-a515-3e540b93dda4"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_personbilling}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="623" y="0" width="166" height="18" backcolor="#E2E1E1" uuid="37f86199-ad2b-474e-9e6c-7c26167b2298"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{notes_subject}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="16" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="647" y="2" width="85" height="14" forecolor="#666666" uuid="69efa21c-b196-4e25-8b4e-4e97730d622f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="730" y="2" width="59" height="14" forecolor="#666666" uuid="a13c8a63-9e19-4a0c-b0ae-f6a6cbd32e2e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="9" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-11" x="236" y="2" width="317" height="14" forecolor="#666666" uuid="4e0a6a3d-875d-4fb4-9207-aee7cf0d3b2e"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<text><![CDATA[IntBillOverDue.jrxml]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-3" x="5" y="2" width="150" height="14" uuid="990240a9-abbc-483b-a5ba-68e7914d2cce"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Times-Roman" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-5" x="0" y="1" width="788" height="1" uuid="0d31461d-5263-46f5-85c1-dd8fed8e7184"/>
				<graphicElement fill="Solid"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
