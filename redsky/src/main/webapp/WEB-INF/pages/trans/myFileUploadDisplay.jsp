<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="display.title"/></title>
    <meta name="heading" content="<fmt:message key='display.heading'/>"/>
	<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
</head>
<s:form id="myFileForm" action="searchMyFiles" method="post" >  
<s:hidden name="myFile.fileId" />
<s:hidden name="myFile.fileType" />
<s:hidden name="customerFile.id" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="myFile.customerNumber" />
<s:hidden name="myFileFor" />

<!--<c:choose>
	
	<c:when test="${not empty serviceOrder.id}">
		<div id="newmnav">
		  <ul>
		  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id} "><span>S/O Details</span></a></li>
		  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
		   <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			<li><a href="containers.html?id=${serviceOrder.id}" ><span>Forwarding</span></a></li>
		  <c:if test="${serviceOrder.job !='INT'}">
		  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		  </c:if>
		  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
		  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		  <li><a href="sortServiceOrderReportss.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}"><span>Forms</span></a></li>
		  <li id="newmnav1" style="background:#FFF "><a href="myFiles.html?id=${serviceOrder.id }&myFileFor=SO"  class="current"><span>Docs<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		</ul>
		</div><div class="spn">&nbsp;</div><br>
	</c:when>
	<c:otherwise>
		<div id="newmnav">
		<ul>
		    <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
		    <li><a href="customerServiceOrders.html?id=${customerFile.id} "><span>Service Orders</span></a></li>
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <li><a href="surveysList.html?id1=${customerFile.id} "><span>Surveys</span></a></li>
		    <li><a href="sortCustomerReportss.html"><span>Forms</span></a></li>  
			<li id="newmnav1" style="background:#FFF "><a href="myFiles.html?id=${customerFile.id }&myFileFor=CF"  class="current"><span>Docs<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div><br>
	</c:otherwise>
</c:choose>
<c:choose>
	
	<c:when test="${not empty serviceOrder.id}">
		<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0" style="width:800px">
			<tbody>
			<tr><td align="center" class="listwhitebox">
				<table class="detailTabLabel" border="0">
				  <tbody>  	
				  	<tr><td align="left" height="5px"></td></tr>
				  	<tr><td align="right"><fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"  size="13"  cssClass="input-textUpper" readonly="true"  onfocus="checkDate();"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right"><fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="15" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right"><fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right"><fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right"><fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
				  	<tr><td align="right"><fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="8" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right"><fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="15" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right"><fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right"><fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="18" readonly="true"/></td></tr>
				  	<tr><td align="left" height="5px"></td></tr>
		   		  </tbody>
			  </table>
			  </td></tr>
			</tbody>
		 </table> 
	</c:when>
	<c:otherwise>
		<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0" style="width:800px">
			<tbody>
			<tr><td align="center" class="listwhitebox">
				<table class="detailTabLabel" border="0">
				  <tbody>  	
				  	<tr>
						<td>Shipper</td><td><s:textfield cssClass="input-textUpper"name="customerFile.firstName" required="true" size="7" readonly="true"/></td><td><s:textfield cssClass="input-textUpper"name="customerFile.lastName" required="true" size="15" readonly="true"/></td>
						<td>Origin</td><td><s:textfield cssClass="input-textUpper"name="customerFile.originCityCode" required="true" size="10" readonly="true"/></td><td><s:textfield cssClass="input-textUpper"name="customerFile.originCountryCode" required="true" size="2" readonly="true"/></td>
						<td>Type</td><td><s:textfield cssClass="input-textUpper"name="customerFile.job" required="true" size="2" readonly="true"/></td>
						<td>Destination</td><td><s:textfield cssClass="input-textUpper"name="customerFile.destinationCityCode" required="true" size="10" readonly="true"/></td><td><s:textfield cssClass="input-textUpper"name="customerFile.destinationCountryCode" required="true" size="2" readonly="true"/></td>
					</tr>
				  </tbody>
				 </table>
				</td>
			</tr>
			</tbody>
		</table>
	</c:otherwise>
</c:choose> 


-->

<div class="separator"></div>
<div id="Layer1">
<div id="newmnav">
				  <ul>
				    <c:choose>
					<c:when test="${not empty serviceOrder.id}">
					   <li id="newmnav1" style="background:#FFF "> <a  class="current" ><span>Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					   <li> <a  href="uploadMyFile!start.html?id=${serviceOrder.id}&myFileFor=SO&decorator=popup&popup=true"><span>Upload</span></a></li>
					    <li><a href="myFiles.html?id=${serviceOrder.id }&myFileFor=SO&decorator=popup&popup=true"><span>Document List</span></a></li>
				    	<li><a href="relatedFiles.html?id=${serviceOrder.id }&myFileFrom=SO&decorator=popup&popup=true"><span>Related Docs</span></a></li>
				    </c:when>
				    <c:otherwise>
				    <li id="newmnav1" style="background:#FFF "> <a  class="current" ><span>Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				     <li> <a  href="uploadMyFile!start.html?id=${customerFile.id}&myFileFor=CF&decorator=popup&popup=true"><span>Upload</span></a></li>
				    	<li><a href="myFiles.html?id=${customerFile.id }&myFileFor=CF&decorator=popup&popup=true"><span>Document List</span></a></li>
				    	<li><a href="relatedFiles.html?id=${customerFile.id }&myFileFrom=CF&decorator=popup&popup=true"><span>Related Docs</span></a></li>
				    </c:otherwise>
				   </c:choose>
				  </ul>
		</div><div class="spn">&nbsp;</div><br>
    
<table class="mainDetailTable" style="width:690px">
<tbody>
<tr>
<td align="center" >
<table cellpadding="5">
<tr><p>Below is a list of attributes that were gathered for file.</p></tr>
    <tr>
        <td align="right" style="width:300px"><b>Filename:</b></td>
        <td><s:property value="fileFileName"/></td>
    </tr>
    <tr>
        <td align="right"><b>File Type:</b></td>
        <td>${myFile.fileType}</td>
    </tr>
    <tr>
        <td align="right"><b>File Content Type:</b></td>
        <td><s:property value="fileContentType"/></td>
    </tr>
    <tr>
        <td align="right"><b>File Size:</b></td>
        <td><s:property value="file.length()"/> bytes</td>
    </tr>
    <tr>
        <td align="right"><b>File Description:</b></td>
        <td>${myFile.description}</td>
    </tr>
    <tr>
        <td align="right" class="tallCell"><b>File Location:</b></td>
        <td colspan="2">The file has been Uploaded Successfully: 
            <a onclick="javascript:openWindow('<c:out value="${link}"/>',900,600);"><font color="blue">
            <u>Click here to view</u></font></a>
        </td>
        <!-- td colspan="2">The file has been written to: <br />
            <a href="<c:out value="${link}"/>">
            <c:out value="${location}" escapeXml="false"/></a>
        </td-->
    </tr>
    <tr>
        <c:choose>
			<c:when test="${not empty serviceOrder.id}">
			<td align="right">
			<input class="cssbutton" type="button" style="width: 120px" value="Go To List"
                onclick="location.href='myFiles.html?id=${serviceOrder.id }&myFileFor=SO&decorator=popup&popup=true'" />
            </td>
            <td>
            <input class="cssbutton" type="button" style="width: 120px" value="Upload Another"
                onclick="location.href='uploadMyFile!start.html?id=${serviceOrder.id}&myFileFor=SO&decorator=popup&popup=true'" />
            </td>
			</c:when>
			<c:otherwise>
			<td align="right">
			<input class="cssbutton" type="button" style="width: 120px" value="Go To List"
                onclick="location.href='myFiles.html?id=${customerFile.id }&myFileFor=CF&decorator=popup&popup=true'" />
            </td>
            <td>
            <input class="cssbutton" type="button" style="width: 120px" value="Upload Another"
                onclick="location.href='uploadMyFile!start.html?id=${customerFile.id}&myFileFor=CF&decorator=popup&popup=true'" />
            </td>
			</c:otherwise>
			</c:choose>
    </tr>
</table>
</td>
</tr>
</tbody>
</table>
</s:form>


