<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="Tenure_Crew"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="576"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isFloatColumnFooter="true"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
     "Tenure_Crew",
     crew.`warehouse` AS crew_warehouse,
     crew.`typeofWork` AS crew_typeofWork,
     crew.`firstName` AS crew_firstName,
     crew.`lastName` AS crew_lastName,
     crew.`classificationEffective` AS crew_classificationEffective,
     crew.`hired` AS crew_hired,
     crew.`terminatedDate` AS crew_terminatedDate,
     crew.`corpID` AS workticket_corpID,
     CONCAT(crew.`lastName`,if(CHAR_LENGTH(crew.`firstName`)>1,CONCAT(', ','',crew.`firstName`,''),''),'')AS empName,
     YEAR(curDate())-YEAR(crew.`classificationEffective`)AS Tenure,
     DATEDIFF(curDate(),crew.`classificationEffective`)AS TenureD,
     crew.`firstName` AS crew_firstName,
     crew.`lastName` AS crew_lastName
FROM
     `crew` crew
WHERE
     crew.`corpID`=$P{Corporate ID} 
AND crew.`typeofWork` IN ('DR','HE','PK','SU','WH') 
AND crew.`terminatedDate` IS NULL 
AND crew.`hired` IS NOT NULL
and crew.`unionName` is not null

ORDER BY 
     crew.`warehouse`,crew.`typeofWork`,crew.`classificationEffective`]]></queryString>

	<field name="Tenure_Crew" class="java.lang.String"/>
	<field name="crew_warehouse" class="java.lang.String"/>
	<field name="crew_typeofWork" class="java.lang.String"/>
	<field name="crew_firstName" class="java.lang.String"/>
	<field name="crew_lastName" class="java.lang.String"/>
	<field name="crew_classificationEffective" class="java.sql.Timestamp"/>
	<field name="crew_hired" class="java.sql.Timestamp"/>
	<field name="crew_terminatedDate" class="java.sql.Timestamp"/>
	<field name="workticket_corpID" class="java.lang.String"/>
	<field name="empName" class="java.lang.String"/>
	<field name="Tenure" class="java.lang.String"/>
	<field name="TenureD" class="java.lang.String"/>

	<variable name="crewCount" class="java.lang.Integer" resetType="Group" resetGroup="warehouse" calculation="Count">
		<variableExpression><![CDATA[$F{empName}]]></variableExpression>
	</variable>
	<variable name="Grand" class="java.lang.Integer" resetType="Group" resetGroup="warehouse" calculation="Count">
		<variableExpression><![CDATA[$F{empName}]]></variableExpression>
	</variable>

		<group  name="warehouse" isStartNewPage="true" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{crew_warehouse}]]></groupExpression>
			<groupHeader>
			<band height="26"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="2"
						y="8"
						width="90"
						height="18"
						forecolor="#0000FF"
						key="staticText-8"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Warehouse#]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="92"
						y="8"
						width="484"
						height="18"
						forecolor="#0000FF"
						key="textField-4"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{crew_warehouse}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="35"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="20"
						y="13"
						width="218"
						height="15"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="10" isBold="true" isItalic="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Total Crew Member for Warehouse #]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="247"
						y="13"
						width="51"
						height="15"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{crew_warehouse}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="298"
						y="13"
						width="31"
						height="15"
						key="staticText-11"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[--------]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="329"
						y="13"
						width="247"
						height="15"
						forecolor="#0000FF"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{crewCount}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="CrewType" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{crew_typeofWork}]]></groupExpression>
			<groupHeader>
			<band height="38"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="20"
						y="12"
						width="70"
						height="18"
						forecolor="#810541"
						key="staticText-9"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="11" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Crew Type]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="90"
						y="12"
						width="486"
						height="18"
						forecolor="#810541"
						key="textField-5"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="11" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{crew_typeofWork}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="56"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="70"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="477"
						y="1"
						width="59"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="536"
						y="1"
						width="40"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="20"
						y="40"
						width="70"
						height="15"
						key="staticText-2"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="224"
						y="40"
						width="91"
						height="15"
						key="staticText-3"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Classification]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="315"
						y="40"
						width="52"
						height="15"
						key="staticText-4"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Tenure]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="367"
						y="40"
						width="81"
						height="15"
						key="staticText-5"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Tenure Days]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="454"
						y="40"
						width="39"
						height="15"
						key="staticText-6"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Age]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="493"
						y="40"
						width="83"
						height="15"
						key="staticText-7"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[DOB]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Transparent"
						x="208"
						y="1"
						width="159"
						height="31"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<staticText>
					<reportElement
						x="224"
						y="7"
						width="130"
						height="20"
						key="staticText-15"/>
					<box>					<pen lineWidth="0.0" lineStyle="Solid"/>
					<topPen lineWidth="0.0" lineStyle="Solid"/>
					<leftPen lineWidth="0.0" lineStyle="Solid"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid"/>
					<rightPen lineWidth="0.0" lineStyle="Solid"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<text><![CDATA[Tenure Report]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="224"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{empName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="224"
						y="0"
						width="91"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{crew_classificationEffective}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="315"
						y="0"
						width="52"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Tenure}==null ? "*****" : $F{Tenure}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="367"
						y="0"
						width="81"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{TenureD}==null ? "***********" : $F{TenureD}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="454"
						y="0"
						width="39"
						height="18"
						key="staticText-13"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[--------]]></text>
				</staticText>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="20"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="2"
						y="5"
						width="574"
						height="15"
						key="staticText-14"/>
					<box></box>
					<textElement textAlignment="Center">
						<font size="8"/>
					</textElement>
				<text><![CDATA[Tenure_Crew]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="30"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="20"
						y="10"
						width="218"
						height="15"
						key="staticText-12"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Grand Total of Crew Members]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="247"
						y="10"
						width="100"
						height="15"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{Grand}]]></textFieldExpression>
				</textField>
			</band>
		</summary>
</jasperReport>
