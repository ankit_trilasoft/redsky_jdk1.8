<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head>   
    <title>NonLinked AccountLine Upload</title>   
    <meta name="heading" content="NonLinked AccountLine Upload"/>  
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script>
function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
function payablesValidateUpload(){
	var fileValue = document.forms['nonLinkedAccountLineUploadForm'].elements['file'].value; 
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	}
			
	else{ 
		var extension=fileValue.substring(fileValue.indexOf('.')+1, fileValue.length);
			document.forms['nonLinkedAccountLineUploadForm'].elements['fileExt'].value = extension;
			if(extension=='xls' || extension=='csv'){
				submitForUpload();
				progressBar('1');
				return true;
       		}else{
         		alert( "Please select a valid file format.xls and .csv are only allowed" ) ;
         		return false;
         	}
	}
}

function fieldValidate(){
 	if(document.forms['nonLinkedAccountLineForm'].elements['beginDate'].value==''){
 		alert("Please enter the begin date"); 
 		return false;
 	}
 	if(document.forms['nonLinkedAccountLineForm'].elements['endDate'].value==''){
 		alert("Please enter the end date "); 
 		return false;
 	}
 	 
 	else{
 		document.forms['nonLinkedAccountLineForm'].action = 'findNonLinkedAccountLine.html?ajax=1';
    	document.forms['nonLinkedAccountLineForm'].submit();
 		return true;
 	}
}

</script> 
<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:800px;
}
</style>

</head> 
 <DIV ID="layerH" style="position:top;width:350px;height:10px;left:250px;top:320px; background-color:white;z-index:150;">
<td align="justify"  class="listwhitetext" valign="middle"><font size="4" color="#1666C9"><b><blink>Uploading...</blink></b></font></td>
</DIV> 
<table border="0"><tr><td>
<s:form id="nonLinkedAccountLineUploadForm" action="processNonLinkedAccountLineUpload.html" enctype="multipart/form-data" method="post" validate="true">   
 <s:hidden name="fileExt" />

 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>NonLinked AccountLine Upload</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4"> <s:file name="file" size="25"/> </td>
						</tr> 
						<tr><td height="20px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>
						<td></td>
						<td>
						<s:submit cssClass="cssbutton1" name="payables" value="Process" cssStyle="width:65px; height:25px" onclick="return payablesValidateUpload();"/>  
                        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset"/></td>
                        </tr>
                        <tr><td height="10px"></td></tr>  
					</tbody>
				</table> 
	    </div>

       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       
</s:form></td>

<td valign="top">
 <s:form id="nonLinkedAccountLineForm" action="findNonLinkedAccountLine.html?ajax=1" enctype="multipart/form-data" method="post" validate="true">   
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Find NonLinked AccountLine</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
 <tbody>  
  
	  		<tr>
	  			<td height="1px"></td>
	  			
	  		</tr>
  			<tr> 
  			<td width=""></td>
				<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="10" maxlength="11" readonly="true" /> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				      	
				<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="10" maxlength="11" readonly="true" /> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	 
			</tr> 
			  <tr><td height="20px"></td></tr>  
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return fieldValidate();"/></td>
	    	</tr>	
	    	 <tr><td height="10px"></td></tr>  
	    	</tbody></table></div>
	    	<div class="bottom-header"><span></span></div>
	    	 </div>
	    	</div></s:form>
	    	
	    	</td>
	    	
</tr>
	    	 				
	
 </table>

<script type="text/javascript">  
 showOrHide(0);
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>