package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.PartnerRefHaulingGridDao;
import com.trilasoft.app.model.PartnerRefHaulingGrid;

public class PartnerRefHaulingGridDaoHibernate extends GenericDaoHibernate<PartnerRefHaulingGrid, Long> implements PartnerRefHaulingGridDao{
	
	public PartnerRefHaulingGridDaoHibernate() {
		super(PartnerRefHaulingGrid.class);
	}

	public List getHaulingGridList(String lowDistance, String highDistance, String rateMile, String rateFlat, String grid, String unit, String countryCode,String corpID) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from PartnerRefHaulingGrid where lowDistance like '%"+lowDistance.replaceAll("'", "''").replaceAll(":", "''")+"%'  and highDistance like '%"+highDistance.replaceAll("'", "''").replaceAll(":", "''")+"%' and  rateMile like '%"+rateMile.replaceAll("'", "''").replaceAll(":", "''")+"%'  and rateFlat like '%"+rateFlat.replaceAll("'", "''").replaceAll(":", "''")+"%'  and grid like '%"+grid.replaceAll("'", "''").replaceAll(":", "''")+"%'  and unit like '%"+unit.replaceAll("'", "''").replaceAll(":", "''")+"%' and countryCode like '%"+countryCode.replaceAll("'", "''").replaceAll(":", "''")+"%' and corpID='"+corpID+"'");
	
	}
	
	
}
