package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "vanlinecommissiontype")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class VanLineCommissionType extends BaseObject implements ListLinkData {

	private String corpID;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	private Long id;

	private String glCode;

	private String code;

	private String type;

	private String description;

	private BigDecimal pc;

	private String calculation;

	private String realCalculation;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("createdBy", createdBy).append("updatedBy", updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("id", id).append("glCode", glCode).append("code", code).append("type", type).append("description", description).append("pc", pc).append(
						"calculation", calculation).append("realCalculation", realCalculation).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof VanLineCommissionType))
			return false;
		VanLineCommissionType castOther = (VanLineCommissionType) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(createdBy, castOther.createdBy).append(updatedBy, castOther.updatedBy).append(createdOn,
				castOther.createdOn).append(updatedOn, castOther.updatedOn).append(id, castOther.id).append(glCode, castOther.glCode).append(code, castOther.code).append(type,
						castOther.type).append(description, castOther.description).append(pc, castOther.pc).append(calculation, castOther.calculation).append(realCalculation,
								castOther.realCalculation).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(createdBy).append(updatedBy).append(createdOn).append(updatedOn).append(id).append(glCode).append(code).append(type)
		.append(description).append(pc).append(calculation).append(realCalculation).toHashCode();
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 15)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(length = 255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length = 20)
	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}

	@Column(precision = 5, scale = 2)
	public BigDecimal getPc() {
		return pc;
	}

	public void setPc(BigDecimal pc) {
		this.pc = pc;
	}

	public String getCalculation() {
		return calculation;
	}

	public void setCalculation(String calculation) {
		this.calculation = calculation;
	}

	public String getRealCalculation() {
		return realCalculation;
	}

	public void setRealCalculation(String realCalculation) {
		this.realCalculation = realCalculation;
	}
	@Transient
	public String getListCode() {
		
		return code;
	}
	@Transient
	public String getListDescription() {
	
		return "";
	}
	@Transient
	public String getListEigthDescription() {
	
		return "";
	}
	@Transient
	public String getListFifthDescription() {
		
		return "";
	}
	@Transient
	public String getListFourthDescription() {
		
		return "";
	}
	@Transient
	public String getListNinthDescription() {
		
		return "";
	}
	@Transient
	public String getListSecondDescription() {
	
		return "";
	}
	@Transient
	public String getListSeventhDescription() {
	
		return "";
	}
	@Transient
	public String getListSixthDescription() {
		
		return "";
	}
	@Transient
	public String getListTenthDescription() {
		
		return "";
	}
	@Transient
	public String getListThirdDescription() {
		
		return "";
	}

}
