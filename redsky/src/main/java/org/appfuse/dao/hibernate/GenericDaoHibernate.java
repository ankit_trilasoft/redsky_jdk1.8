package org.appfuse.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.dao.GenericDao;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.impl.SessionImpl;
import org.hibernate.loader.OuterJoinLoader;
import org.hibernate.loader.criteria.CriteriaLoader;
import org.hibernate.loader.criteria.CriteriaQueryTranslator;
import org.hibernate.persister.entity.OuterJoinLoadable;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * This class serves as the Base class for all other DAOs - namely to hold
 * common CRUD methods that they might all use. You should only need to extend
 * this class when your require custom CRUD logic.
 *
 * <p>To register this class in your Spring context file, use the following XML.
 * <pre>
 *      &lt;bean id="fooDao" class="org.appfuse.dao.hibernate.GenericDaoHibernate"&gt;
 *          &lt;constructor-arg value="org.appfuse.model.Foo"/&gt;
 *          &lt;property name="sessionFactory" ref="sessionFactory"/&gt;
 *      &lt;/bean&gt;
 * </pre>
 *
 * @author <a href="mailto:bwnoll@gmail.com">Bryan Noll</a>
 */
public class GenericDaoHibernate<T, PK extends Serializable> extends HibernateDaoSupport implements GenericDao<T, PK> {
    protected final Log log = LogFactory.getLog(getClass());
    private Class<T> persistentClass;

    public GenericDaoHibernate(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    @SuppressWarnings("unchecked")
	public List<T> getAll() {
        return super.getHibernateTemplate().loadAll(this.persistentClass);
    }

    @SuppressWarnings("unchecked")
	public T get(PK id) {
    	
    	T entity = null;
    	
    	try{
    		Session currentSession = super.getHibernateTemplate().getSessionFactory().getCurrentSession();
    		entity = (T) (currentSession.createCriteria(persistentClass).add(Restrictions.eq("id", id))).uniqueResult();
    	}catch(Exception ex){
            log.warn("Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...");
            throw new ObjectRetrievalFailureException(this.persistentClass, id);
    	}
    	
        if (entity == null) {
            log.warn("Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...");
            throw new ObjectRetrievalFailureException(this.persistentClass, id);
        }

        return entity;
    }
    
    @SuppressWarnings("unchecked")
	public T getByCorpid(String corpID) {
    	
    	T entity = null;
    	
    	try{
    		log.warn("Call from UserLogin, '" + this.persistentClass + "' object with corpID '" + corpID + "'" );
    		Session currentSession = super.getHibernateTemplate().getSessionFactory().getCurrentSession();
    		entity = (T) (currentSession.createCriteria(persistentClass).add(Restrictions.eq("corpID", corpID))).uniqueResult();
    	}catch(Exception ex){
    		toSql(corpID);
            log.warn("Uh oh, '" + this.persistentClass + "' object with CorpID '" + corpID + "' not found...");
            throw new ObjectRetrievalFailureException(this.persistentClass, corpID);
    	}
    	
        if (entity == null) {
        	toSql(corpID);
            log.warn("Uh oh, '" + this.persistentClass + "' object(entity is null) with CorpID '" + corpID + "' not found...");
            throw new ObjectRetrievalFailureException(this.persistentClass, corpID);
        }

        return entity;
    }
    
    public void toSql(String corpID){
        String sql="";
        Object[] parameters = null;
        Object[] filterParameters = null;
        try{
            Session currentSession = super.getHibernateTemplate().getSessionFactory().getCurrentSession(); 
    	    Criteria criteria = currentSession.createCriteria(persistentClass).add(Restrictions.eq("corpID", corpID)); 
            CriteriaImpl c = (CriteriaImpl) criteria;
            SessionImpl s = (SessionImpl)c.getSession();
            SessionFactoryImplementor factory = (SessionFactoryImplementor)s.getSessionFactory();
            String[] implementors = factory.getImplementors( c.getEntityOrClassName() );
            CriteriaLoader loader = new CriteriaLoader((OuterJoinLoadable)factory.getEntityPersister(implementors[0]), factory, c, implementors[0], s.getEnabledFilters());
            Field f = OuterJoinLoader.class.getDeclaredField("sql");
            f.setAccessible(true);
            sql = (String)f.get(loader);
            System.out.println("\n HQL Query for find companysystemdefault : "+sql); 
            Field fp = CriteriaLoader.class.getDeclaredField("translator");
            fp.setAccessible(true);
            CriteriaQueryTranslator translator = (CriteriaQueryTranslator) fp.get(loader);
            parameters = translator.getQueryParameters().getPositionalParameterValues(); 
            System.out.println("\n SQL filterParameters : "+translator.getRootCriteria()); 
            System.out.println("\n SQL parameters : "+parameters); 
        }
        catch(Exception e){
            throw new RuntimeException(e);
        }
        if (sql !=null){
            int fromPosition = sql.indexOf(" from ");
            sql = "SELECT * "+ sql.substring(fromPosition);

            if (parameters!=null && parameters.length>0){
                for (Object val : parameters) {
                    String value="%";
                    if(val instanceof Boolean){
                        value = ((Boolean)val)?"1":"0";
                    }else if (val instanceof String){
                        value = "'"+val+"'";
                    }
                    sql = sql.replaceFirst("\\?", value);
                }
            }
        } 
        System.out.println("\n SQL Query for find companysystemdefault : "+sql); 
        }
    
    @SuppressWarnings("unchecked")
	public T getForOtherCorpid(PK id) {
        T entity = (T) super.getHibernateTemplate().get(this.persistentClass, id);

        if (entity == null) {
            log.warn("Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...");
            throw new ObjectRetrievalFailureException(this.persistentClass, id);
        }

        return entity;
    }
    
    @SuppressWarnings("unchecked")
	public boolean exists(PK id) {
        T entity = (T) super.getHibernateTemplate().get(this.persistentClass, id);
        if (entity == null) {
            return false;
        } else {
            return true;
        }
    }

    @SuppressWarnings("unchecked")
	public T save(T object) {
        return (T) super.getHibernateTemplate().merge(object);
    }

    public void remove(PK id) {
        super.getHibernateTemplate().delete(this.get(id));
    }
    public void removeForOtherCorpid(PK id) {
        super.getHibernateTemplate().delete(this.getForOtherCorpid(id));
    }
}
