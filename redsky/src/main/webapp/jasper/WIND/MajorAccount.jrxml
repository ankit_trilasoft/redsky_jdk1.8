<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MajorAccount" pageWidth="1153" pageHeight="933" orientation="Landscape" columnWidth="1153" leftMargin="0" rightMargin="0" topMargin="20" bottomMargin="20" uuid="48eb11a8-c457-4d65-996c-b4cba0e1b193">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="449"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT "MajorAccount-R-WIND.jrxml",
	partner.abbreviation AS 'Major Account',
	year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))  AS 'Year',
	concat(DATE_FORMAT(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA),'%m'),'(',DATE_FORMAT(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA),'%b'),')')  AS 'Month',
	round(sum(IF (accountline.actualrevenue <> 0, accountline.actualrevenue, accountline.revisionrevenueamount)),0)  AS 'Revenue',
	round(ifnull(sum(if(mode='Air', miscellaneous.actualgrossweight, miscellaneous.actualnetweight)/(select count(*) from accountline ax where ax.serviceorderid = serviceorder.id)),0),0)  AS 'Tonnage',
	count(distinct serviceorder.shipnumber)  AS '# of Shipment',
	if(year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))=concat(year(sysdate())-1,'-01-01'),round(sum(IF (accountline.actualrevenue <> 0, accountline.actualrevenue, accountline.revisionrevenueamount)),0),0)  AS 'Revenue Previous Year',
	if(year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))=concat(year(sysdate())-1,'-01-01'),round(ifnull(sum(if(mode='Air', miscellaneous.actualgrossweight, miscellaneous.actualnetweight)/(select count(*) from accountline ax where ax.serviceorderid = serviceorder.id)),0),0),0)  AS 'Tonage Previous Year',
	if(year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))=concat(year(sysdate())-1,'-01-01'),count(distinct serviceorder.shipnumber),0)  AS 'Shipment Previous Year',
	if(year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))=concat(year(sysdate()),'-01-01'),round(sum(IF (accountline.actualrevenue <> 0, accountline.actualrevenue, accountline.revisionrevenueamount)),0),0) AS 'Revenue Current Year',
	if(year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))=concat(year(sysdate()),'-01-01'),round(ifnull(sum(if(mode='Air', miscellaneous.actualgrossweight, miscellaneous.actualnetweight)/(select count(*) from accountline ax where ax.serviceorderid = serviceorder.id)),0),0),0) AS 'Tonage Current Year',
	if(year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))=concat(year(sysdate()),'-01-01'),count(distinct serviceorder.shipnumber),0) AS 'Shipment Current Year'
FROM serviceorder serviceorder
	INNER JOIN billing billing on  serviceorder.id = billing.id AND billing.corpid =  $P{Corporate ID}
	INNER JOIN miscellaneous miscellaneous on serviceorder.id = miscellaneous.id AND miscellaneous.corpid =  $P{Corporate ID}
	INNER JOIN trackingstatus trackingstatus on serviceorder.id = trackingstatus.id AND trackingstatus.corpid =  $P{Corporate ID}
	INNER JOIN partner partner on partner.partnercode = billing.billtocode and partner.corpid = $P{Corporate ID}
	LEFT OUTER JOIN accountprofile accountprofile on accountprofile.partnercode = partner.partnercode AND accountprofile.corpid =  $P{Corporate ID}
	LEFT OUTER JOIN accountline accountline on accountline.serviceorderid = serviceorder.id AND accountline.corpid = $P{Corporate ID} and accountline.status is true
WHERE
serviceorder.status not in ('CNCL','DWNLD')
	AND serviceorder.corpid =  $P{Corporate ID}
	AND if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA) >= concat(year(sysdate())-1,'-01-01')
	AND partner.abbreviation <> 'MPRI'
	AND accountprofile.stage = 'Major'
GROUP BY partner.abbreviation, year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA)),month(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA))
ORDER BY partner.abbreviation, year(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA)),month(if(trackingstatus.loadA is null || trackingstatus.loadA='', trackingstatus.beginload, trackingstatus.loadA));]]>
	</queryString>
	<field name="MajorAccount-R-WIND.jrxml" class="java.lang.String"/>
	<field name="Major Account" class="java.lang.String"/>
	<field name="Year" class="java.lang.Integer"/>
	<field name="Month" class="java.lang.String"/>
	<field name="Revenue" class="java.math.BigDecimal"/>
	<field name="Tonnage" class="java.math.BigDecimal"/>
	<field name="# of Shipment" class="java.lang.Long"/>
	<field name="Revenue Previous Year" class="java.math.BigDecimal"/>
	<field name="Tonage Previous Year" class="java.math.BigDecimal"/>
	<field name="Shipment Previous Year" class="java.lang.Long"/>
	<field name="Revenue Current Year" class="java.math.BigDecimal"/>
	<field name="Tonage Current Year" class="java.math.BigDecimal"/>
	<field name="Shipment Current Year" class="java.lang.Long"/>
	<group name="year">
		<groupExpression><![CDATA[$F{Year}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="63" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="20" width="1110" height="20" uuid="8c59aa83-94e5-4328-8007-3651ff810e10"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="13" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Major Account Bookings and Tonnage]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="0" y="0" width="1110" height="20" uuid="f9b9928b-1b2a-4ec0-8003-99222f83ffc9"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="13" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Management Report]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="30" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="913" y="14" width="130" height="15" uuid="44718313-2371-4843-a88e-ebdce824eb06"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="1045" y="14" width="62" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="92ad35a9-0b07-4f22-bf6d-f572dbd88c0b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" x="21" y="14" width="142" height="15" uuid="e093b84c-7055-442b-83ec-f91e4ff04568"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-7" x="474" y="15" width="155" height="14" uuid="57b43bfc-1a38-4231-b70a-61eebcfde6ec"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[MajorAccount.jrxml]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band height="129" splitType="Stretch">
			<crosstab>
				<reportElement key="crosstab-1" x="5" y="5" width="1110" height="117" uuid="a9b39c4d-8b52-4125-84fb-63d7ff580ffc"/>
				<crosstabHeaderCell>
					<cellContents mode="Transparent">
						<staticText>
							<reportElement key="staticText-8" x="70" y="0" width="55" height="12" forecolor="#0000CC" uuid="02a7b1b9-4107-4df6-b7d2-64bba7c7b8b6"/>
							<textElement textAlignment="Left" verticalAlignment="Top">
								<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<text><![CDATA[Month/
]]></text>
						</staticText>
						<staticText>
							<reportElement key="staticText-8" x="0" y="0" width="70" height="25" forecolor="#0000CC" uuid="40f2d9d2-f5ee-4c6e-a974-9b2a31a74a98"/>
							<textElement textAlignment="Left" verticalAlignment="Top">
								<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<text><![CDATA[Account]]></text>
						</staticText>
						<staticText>
							<reportElement key="staticText-8" x="70" y="13" width="55" height="12" forecolor="#0000CC" uuid="1494e6bc-d7c7-45fa-adb5-45fdd4bd710b"/>
							<textElement textAlignment="Left" verticalAlignment="Top">
								<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<text><![CDATA[Year
]]></text>
						</staticText>
					</cellContents>
				</crosstabHeaderCell>
				<rowGroup name="Major Account" width="70" totalPosition="End">
					<bucket class="java.lang.String">
						<bucketExpression><![CDATA[$F{Major Account}]]></bucketExpression>
					</bucket>
					<crosstabRowHeader>
						<cellContents mode="Transparent">
							<textField isBlankWhenNull="true">
								<reportElement key="textField" x="0" y="0" width="70" height="25" uuid="17ce48c7-0155-4b64-a571-b4fd03e14b35"/>
								<textElement textAlignment="Left" verticalAlignment="Top">
									<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA[$V{Major Account}]]></textFieldExpression>
							</textField>
						</cellContents>
					</crosstabRowHeader>
					<crosstabTotalRowHeader>
						<cellContents mode="Transparent">
							<textField isBlankWhenNull="true">
								<reportElement key="textField" x="0" y="25" width="70" height="25" uuid="4c8d9830-a62f-4a2b-9f54-158102cb8f61"/>
								<box topPadding="1"/>
								<textElement textAlignment="Left" verticalAlignment="Top">
									<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA["Major Account         Last Year"]]></textFieldExpression>
							</textField>
							<staticText>
								<reportElement key="staticText-8" x="70" y="75" width="55" height="25" forecolor="#0000CC" uuid="a3e5ba58-7084-42f2-872f-1816b27b584a"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Revenue]]></text>
							</staticText>
							<staticText>
								<reportElement key="staticText-8" x="70" y="50" width="55" height="25" forecolor="#0000CC" uuid="6611c60c-c21f-4b0d-be74-ace81922acfc"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Tonnage]]></text>
							</staticText>
							<staticText>
								<reportElement key="staticText-8" x="70" y="25" width="55" height="25" forecolor="#0000CC" uuid="df7a2656-19ac-40b2-8ede-b8284fa6ef1a"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Shipment]]></text>
							</staticText>
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement key="textField-4" x="0" y="100" width="70" height="25" uuid="bace0b92-3e7d-45b7-9823-3e100cb82c9c"/>
								<textElement textAlignment="Left" verticalAlignment="Top">
									<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA["Major Account      Current Year"]]></textFieldExpression>
							</textField>
							<staticText>
								<reportElement key="staticText-8" x="70" y="150" width="55" height="25" forecolor="#0000CC" uuid="f4fafc5e-df3e-4081-8aa2-dc0e05988362"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Revenue]]></text>
							</staticText>
							<staticText>
								<reportElement key="staticText-8" x="70" y="125" width="55" height="25" forecolor="#0000CC" uuid="f5c2b26a-961d-4a1c-8b91-69009f9382a3"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Tonnage]]></text>
							</staticText>
							<staticText>
								<reportElement key="staticText-8" x="70" y="100" width="55" height="25" forecolor="#0000CC" uuid="cc6c243f-597f-4ec9-8e7d-b948d3ae738d"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Shipment]]></text>
							</staticText>
							<textField isBlankWhenNull="true">
								<reportElement key="textField-4" x="1" y="0" width="70" height="25" uuid="175c5979-5c06-42f0-944f-0c71bfa5854d"/>
								<box topPadding="1"/>
								<textElement textAlignment="Left" verticalAlignment="Top">
									<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA["Total"]]></textFieldExpression>
							</textField>
						</cellContents>
					</crosstabTotalRowHeader>
				</rowGroup>
				<rowGroup name="Year" width="55" totalPosition="End">
					<bucket class="java.lang.Integer">
						<bucketExpression><![CDATA[$F{Year}]]></bucketExpression>
					</bucket>
					<crosstabRowHeader>
						<cellContents mode="Transparent">
							<textField isBlankWhenNull="false">
								<reportElement key="textField" x="0" y="0" width="55" height="25" uuid="f5d50f35-d15d-4554-ad8e-9d2070cc746e"/>
								<textElement textAlignment="Left" verticalAlignment="Top">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA[$V{Year}]]></textFieldExpression>
							</textField>
							<staticText>
								<reportElement key="staticText-8" x="0" y="25" width="55" height="25" forecolor="#0000CC" uuid="243dac99-8b44-4a86-bd98-6dac81664885"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Shipment]]></text>
							</staticText>
							<staticText>
								<reportElement key="staticText-8" x="0" y="50" width="55" height="25" forecolor="#0000CC" uuid="ee352272-1cc0-4e9a-8db8-042be40273b0"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Tonnage]]></text>
							</staticText>
							<staticText>
								<reportElement key="staticText-8" x="0" y="75" width="55" height="25" forecolor="#0000CC" uuid="f7cee3c0-1d09-4c3c-bba1-2ba89acb0161"/>
								<textElement textAlignment="Left">
									<font fontName="Arial" size="11" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<text><![CDATA[Revenue]]></text>
							</staticText>
						</cellContents>
					</crosstabRowHeader>
					<crosstabTotalRowHeader>
						<cellContents/>
					</crosstabTotalRowHeader>
				</rowGroup>
				<columnGroup name="Month" height="25" totalPosition="End" headerPosition="Center">
					<bucket class="java.lang.String">
						<bucketExpression><![CDATA[$F{Month}]]></bucketExpression>
					</bucket>
					<crosstabColumnHeader>
						<cellContents mode="Transparent">
							<textField isBlankWhenNull="true">
								<reportElement key="textField" x="0" y="0" width="75" height="25" forecolor="#0000CC" uuid="8b3f691c-ca96-4632-b790-e53cc73a3b08"/>
								<box bottomPadding="10"/>
								<textElement textAlignment="Right" verticalAlignment="Middle">
									<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA[$V{Month}]]></textFieldExpression>
							</textField>
						</cellContents>
					</crosstabColumnHeader>
					<crosstabTotalColumnHeader>
						<cellContents mode="Transparent">
							<textField isBlankWhenNull="true">
								<reportElement key="textField" x="0" y="0" width="80" height="25" forecolor="#0000CC" uuid="d03e3654-c7a1-415e-ba77-ea8105d3e31f"/>
								<box bottomPadding="10"/>
								<textElement textAlignment="Right" verticalAlignment="Middle">
									<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
								</textElement>
								<textFieldExpression><![CDATA["Total"]]></textFieldExpression>
							</textField>
						</cellContents>
					</crosstabTotalColumnHeader>
				</columnGroup>
				<measure name="# of Shipment_Sum" class="java.lang.Long" calculation="Sum">
					<measureExpression><![CDATA[$F{# of Shipment}]]></measureExpression>
				</measure>
				<measure name="Tonage" class="java.math.BigDecimal" calculation="Sum">
					<measureExpression><![CDATA[$F{Tonnage}]]></measureExpression>
				</measure>
				<measure name="Revenue" class="java.math.BigDecimal" calculation="Sum">
					<measureExpression><![CDATA[$F{Revenue}]]></measureExpression>
				</measure>
				<measure name="ship2008" class="java.lang.Long" calculation="Sum">
					<measureExpression><![CDATA[$F{Shipment Previous Year}]]></measureExpression>
				</measure>
				<measure name="tonage2008" class="java.math.BigDecimal" calculation="Sum">
					<measureExpression><![CDATA[$F{Tonage Previous Year}]]></measureExpression>
				</measure>
				<measure name="rev2008" class="java.math.BigDecimal" calculation="Sum">
					<measureExpression><![CDATA[$F{Revenue Previous Year}]]></measureExpression>
				</measure>
				<measure name="shipment2009" class="java.lang.Long" calculation="Sum">
					<measureExpression><![CDATA[$F{Shipment Current Year}]]></measureExpression>
				</measure>
				<measure name="tonage2009" class="java.math.BigDecimal" calculation="Sum">
					<measureExpression><![CDATA[$F{Tonage Current Year}]]></measureExpression>
				</measure>
				<measure name="revenue2009" class="java.math.BigDecimal" calculation="Sum">
					<measureExpression><![CDATA[$F{Revenue Current Year}]]></measureExpression>
				</measure>
				<crosstabCell width="75" height="100">
					<cellContents mode="Transparent">
						<textField isBlankWhenNull="true">
							<reportElement key="textField" x="0" y="25" width="75" height="25" uuid="b1bbebbc-d4e8-4b64-a02b-a08463bb1601"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{# of Shipment_Sum}]]></textFieldExpression>
						</textField>
						<textField pattern="#,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="50" width="75" height="25" uuid="6698a37a-0c05-4d47-9725-6f1edd5c048d"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{Tonage}]]></textFieldExpression>
						</textField>
						<textField pattern="¤ #,##0" isBlankWhenNull="false">
							<reportElement key="textField-4" x="0" y="75" width="75" height="25" uuid="ba9b467a-783e-42c7-8a6b-723610fcbc0c"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{Revenue}]]></textFieldExpression>
						</textField>
					</cellContents>
				</crosstabCell>
				<crosstabCell width="80" height="100" columnTotalGroup="Month">
					<cellContents mode="Transparent">
						<textField isBlankWhenNull="false">
							<reportElement key="textField" x="0" y="25" width="80" height="25" uuid="c1324d25-197d-4c02-b90c-2d17af2f611b"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{# of Shipment_Sum}]]></textFieldExpression>
						</textField>
						<textField pattern="#,##0" isBlankWhenNull="false">
							<reportElement key="textField-4" x="0" y="50" width="80" height="25" uuid="d27ee582-83f9-4716-9c97-07832827033d"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{Tonage}]]></textFieldExpression>
						</textField>
						<textField pattern="¤ #,##0" isBlankWhenNull="false">
							<reportElement key="textField-4" x="0" y="75" width="80" height="25" uuid="c2dd29ec-822c-4b27-b995-aa3a1a3fe07b"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{Revenue}]]></textFieldExpression>
						</textField>
					</cellContents>
				</crosstabCell>
				<crosstabCell width="75" height="0" rowTotalGroup="Year">
					<cellContents mode="Transparent"/>
				</crosstabCell>
				<crosstabCell width="80" height="0" rowTotalGroup="Year" columnTotalGroup="Month">
					<cellContents mode="Transparent"/>
				</crosstabCell>
				<crosstabCell width="75" height="180" rowTotalGroup="Major Account">
					<cellContents mode="Transparent">
						<textField isStretchWithOverflow="true" isBlankWhenNull="true">
							<reportElement key="textField" x="0" y="25" width="75" height="25" uuid="bbdcca05-be3e-4eee-bd0f-9b6e26cedde0"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{ship2008}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" pattern="#,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="50" width="75" height="25" uuid="ae4a7b28-1761-4e2c-9e74-1c696a05d66c"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{tonage2008}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" pattern="¤ #,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="75" width="75" height="25" uuid="6881704a-7bc8-4bc1-ac49-3f5540d61610"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{rev2008}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="100" width="75" height="25" uuid="a5a66761-679e-4731-8524-49b8f7306cd1"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{shipment2009}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" pattern="#,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="125" width="75" height="25" uuid="1b20ac6d-d9df-4181-a2ae-3d090df4af4c"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{tonage2009}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" pattern="¤ #,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="150" width="75" height="25" uuid="bdcd91c1-1be8-4e2e-8f21-7a8a3155ef8d"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{revenue2009}]]></textFieldExpression>
						</textField>
					</cellContents>
				</crosstabCell>
				<crosstabCell width="80" height="180" rowTotalGroup="Major Account" columnTotalGroup="Month">
					<cellContents mode="Transparent">
						<textField isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="25" width="80" height="25" uuid="ae715260-7132-43b6-b107-a44600938ab5"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{ship2008}]]></textFieldExpression>
						</textField>
						<textField pattern="#,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="50" width="80" height="25" uuid="e4f46f2d-fa46-406f-9705-e0fe01d3d9da"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{tonage2008}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" pattern="¤ #,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="75" width="80" height="25" uuid="f9f8d6b5-ed49-4966-859e-2f921ddfb566"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{rev2008}]]></textFieldExpression>
						</textField>
						<textField isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="100" width="80" height="25" uuid="280f2ea4-f51c-43b1-962a-792930803ef0"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{shipment2009}]]></textFieldExpression>
						</textField>
						<textField pattern="#,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="125" width="80" height="25" uuid="d6370501-4742-4f66-9371-fb4b19372170"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{tonage2009}]]></textFieldExpression>
						</textField>
						<textField isStretchWithOverflow="true" pattern="¤ #,##0" isBlankWhenNull="true">
							<reportElement key="textField-4" x="0" y="150" width="80" height="25" uuid="f32614c0-7f54-4ebd-9ded-b9ddadf1bca7"/>
							<box bottomPadding="10"/>
							<textElement textAlignment="Right" verticalAlignment="Middle">
								<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
							</textElement>
							<textFieldExpression><![CDATA[$V{revenue2009}]]></textFieldExpression>
						</textField>
					</cellContents>
				</crosstabCell>
				<whenNoDataCell>
					<cellContents mode="Transparent"/>
				</whenNoDataCell>
			</crosstab>
		</band>
	</summary>
</jasperReport>
