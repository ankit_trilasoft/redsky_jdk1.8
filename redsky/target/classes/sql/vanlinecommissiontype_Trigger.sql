delimiter $$

CREATE trigger redsky.trigger_add_history_vanlinecommissiontype
BEFORE UPDATE on redsky.vanlinecommissiontype
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;


   IF (OLD.code <> NEW.code ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.code~') into fieldNameList;
      select CONCAT(oldValueList,OLD.code,'~') into oldValueList;
      select CONCAT(newValueList,NEW.code,'~') into newValueList;
  END IF;


   IF (OLD.type <> NEW.type ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.type~') into fieldNameList;
      select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
      select CONCAT(newValueList,NEW.type,'~') into newValueList;
  END IF;


   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;


   IF (OLD.calculation <> NEW.calculation ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.calculation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.calculation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.calculation,'~') into newValueList;
  END IF;


   IF (OLD.glCode <> NEW.glCode ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.glCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glCode,'~') into newValueList;
  END IF;


   IF (OLD.realCalculation <> NEW.realCalculation ) THEN
      select CONCAT(fieldNameList,'vanlinecommissiontype.realCalculation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.realCalculation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.realCalculation,'~') into newValueList;
  END IF;




    IF ((OLD.pc <> NEW.pc) or (OLD.pc is null and NEW.pc is not null)
      or (OLD.pc is not null and NEW.pc is null)) THEN
        select CONCAT(fieldNameList,'vanlinecommissiontype.pc~') into fieldNameList;
        IF(OLD.pc is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pc is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pc is not null) then
            select CONCAT(oldValueList,OLD.pc,'~') into oldValueList;
        END IF;
        IF(NEW.pc is not null) then
            select CONCAT(newValueList,NEW.pc,'~') into newValueList;
        END IF;
   END IF;     


CALL add_tblHistory (OLD.id,"vanlinecommissiontype", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;