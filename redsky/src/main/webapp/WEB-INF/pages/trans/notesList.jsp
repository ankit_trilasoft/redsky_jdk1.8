<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>   
    <title><fmt:message key="notesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='notesList.heading'/>"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
   <script language="javascript" type="text/javascript">
	function clear_fields(){
		document.forms['notesListForm'].elements['notes.noteSubType'].value = "";
		document.forms['notesListForm'].elements['notes.noteStatus'].value = "";
	}
	
	</script> 
 <script language="javascript" type="text/javascript">
	function findToolTipSubjectForNotes(id,position){		
	var url="findToolTipSubjectNotes.html?ajax=1&decorator=simple&popup=true&id="+id;
	ajax_showTooltip(url,position);	
	return false;
	}
	function findToolTipNoteForNotes(id,position){		
		var url="findToolTipNote.html?ajax=1&decorator=simple&popup=true&id="+id;
		ajax_showTooltip(url,position);	
		return false;
		}
</script> 
<script language="javascript" type="text/javascript">
         <c:set var="OIFlag" value="true" />
         <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
         <c:set var="OIFlag" value="false" />
         </sec-auth:authComponent>
         </script>
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:5px;margin-top:-20.5px;padding:2px 0px;text-align:right;width:90%;}
#otabs{margin-left: 40px; position:relative;margin-bottom: 0px;}
.buttonSht {margin-right: 5px;height: 25px;width:90px; font-size: 15}
div#main p{line-height:20px !important;margin-top:0px !important;}
</style>
	
</head>
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
<c:set var="notesKey" value="<%=request.getParameter("id") %>"/>
<s:hidden name="notesKey" value="<%=request.getParameter("id") %>" />
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<s:hidden name="from"  id="from" value="<%=request.getParameter("from") %>" />
<c:set var="idOfTasks" value="" scope="session"/>
<s:hidden name="PPID" id="PPID" value="<%=request.getParameter("PPID") %>" />
<c:set var="PPID" value="<%=request.getParameter("PPID") %>"/>
<c:if test="${not empty notesKey && noteFor=='agentQuotes'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="CF"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
		<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${not empty notesKey && noteFor=='CustomerFile'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="CF"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
		<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${not empty notesKey && noteFor=='ServiceOrder'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="SO"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
		<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${not empty notesKey && noteFor=='agentQuotesSO'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="SO"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
		<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${not empty notesKey && noteFor=='Partner'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="PO"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
		<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
        <c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
        <s:hidden name="ppCode"  id= "ppCode" value=""/>
</c:if>
<s:form id="notesListForm" action="searchNotes.html?noteFor=${noteFor}" method="post" >  
<c:set var="notesId" value="<%=request.getParameter("notesId") %>"/>
<s:hidden name="flagAcc" value="<%=request.getParameter("flagAcc") %>" />
<c:set var="flagAcc" value="<%=request.getParameter("flagAcc") %>"/>
<s:hidden name="notesId" value="<%=request.getParameter("notesId") %>" />
<s:hidden name="accountNotesFor" value="<%=request.getParameter("accountNotesFor") %>"/>
<c:set var="accountNotesFor" value="<%=request.getParameter("accountNotesFor") %>"/>
<c:set var="notess" value="${notess}" />
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>" scope="request"/>
<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
<c:set var="notesKey" value="<%=request.getParameter("id") %>"/>
<s:hidden name="notesKey" value="<%=request.getParameter("id") %>" />
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id && empty serviceOrder.id}">
		<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id= "fileNameFor" value=""/>
	<s:hidden name="fileID" id ="fileID" value="" />
	<s:hidden name="ppType" id ="ppType" value="" />
    <c:set var="ppType" value=""/>
	</c:if>
	<c:if test="${empty serviceOrder.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty serviceOrder.id && not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id= "fileNameFor" value=""/>
		<s:hidden name="fileID" id ="fileID" value="" />
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>


<c:if test="${param.popup}"> 
	<c:set var="imageId" value="<%=request.getParameter("imageId") %>" />
	<s:hidden name="imageId" value="<%=request.getParameter("imageId") %>" />
	<c:set var="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<s:hidden name="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<c:set var="subType" value="<%=request.getParameter("subType") %>" />
	<s:hidden name="subType" value="<%=request.getParameter("subType") %>" />
</c:if>
<%
    String value=request.getParameter("notesId"); 
    Cookie cookie = new Cookie("TempnotesId",value);
    cookie.setMaxAge(3600);
    response.addCookie(cookie);
%>

<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="idOfWhom" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.id" />	
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.moveType" />
<s:hidden name="workTicket.id" />
<s:hidden name="claim.id" />
<s:hidden name="container.id" />
<s:hidden name="carton.id" />
<s:hidden name="creditCard.id" />
<s:hidden name="vehicle.id" />
<s:hidden name="accountLine.id" />
<s:hidden name="servicePartner.id" />
<s:hidden name="customerFile.sequenceNumber" />	
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="workTicket.ticket" />
<s:hidden name="claim.claimNumber" />
<s:hidden name="payroll.id"/>
<s:hidden name="partnerPrivate.id"/>
<s:hidden name="mss.id" />
<s:hidden name="dsFamilyDetails.id" />
<c:set var="ischecked" value="false"/>
<configByCorp:fieldVisibility componentId="component.field.partnerUser.showEditor">
<c:set var="ischecked" value="true"/>
</configByCorp:fieldVisibility>
<c:set var="idOfWhom" value="<%=request.getParameter("id")%>" scope="session"/>
<s:hidden name="notes.customerNumber" value="%{customerFile.sequenceNumber}" />
<c:set var="subType" value="<%=request.getParameter("subType") %>"/>
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<c:choose>
	<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
		<s:hidden name="notes.notesId" value="%{workTicket.ticket}" />	
	</c:when>
	<c:when test="${not empty serviceOrder.id && not empty claim.id}">
		<s:hidden name="notes.notesId" value="%{claim.claimNumber}" />	
	</c:when>
	<c:when test="${not empty serviceOrder.id && not empty creditCard.id}">
		<s:hidden name="notes.notesId" value="%{creditCard.id}" />	
	</c:when>
	<c:when test="${not empty serviceOrder.id && empty workTicket.id && empty claim.id}">
		<s:hidden name="notes.notesId" value="%{serviceOrder.shipNumber}" />	
	</c:when>
	<c:when test="${not empty payroll.id }">
		<s:hidden name="notes.notesId" value="%{payroll.id}" />	
	</c:when>
	<c:when test="${not empty partnerPrivate.id}">
		<s:hidden name="notes.notesId" value="%{notesId}" />
	</c:when>
	<c:otherwise>
		<s:hidden name="notes.notesId" value="%{customerFile.sequenceNumber}" />	
	</c:otherwise>
</c:choose>
<c:set var="buttons"> 
	<c:choose>
		<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForWorkTicket.html?id=${workTicket.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty claim.id}">
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNoteForClaim.html?id=${claim.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		
		<c:when test="${not empty serviceOrder.id && not empty claim.id}">
            <input type="button" class="cssbutton buttonSht" 
            onclick="location.href='<c:url value="/editNewNoteForClaim.html?id=${claim.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
            value="<fmt:message key="button.addNewNote"/>"/> 
        </c:when>
		
		<c:when test="${not empty serviceOrder.id && not empty creditCard.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForCreditCard.html?id=${creditCard.id}&notesId=${notesId}&noteFor=${noteFor}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty servicePartner.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForServicePartner.html?id=${servicePartner.id }&notesId=${notesId}&noteFor=${noteFor}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty accountLine.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForAccountLine.html?id=${accountLine.id }&notesId=${notesId}&noteFor=${noteFor}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty carton.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForCarton.html?id=${carton.id }&notesId=${notesId}&noteFor=${noteFor}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty container.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForContainer.html?id=${container.id }&notesId=${notesId}&noteFor=${noteFor}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty vehicle.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForVehicle.html?id=${vehicle.id }&notesId=${notesId}&noteFor=${noteFor}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && empty workTicket.id && empty claim.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForServiceOrder.html?id=${serviceOrder.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty partnerPrivate.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForPartnerPrivate.html?id=${partnerPrivate.id }&notesId=${notesId}&noteFor=${noteFor}&ppType=${ppType}&PPID=${PPID}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
			<c:when test="${not empty payroll.id}">
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNoteForPayroll.html?id=${payroll.id}&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty mss.id}"> 
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForMss.html?id=${mss.id}&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty dsFamilyDetails.id}"> 
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForFamilyDetails.html?id=${dsFamilyDetails.id}&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:otherwise>
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNote.html?id1=${customerFile.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:otherwise>
	 </c:choose> 
      
</c:set>  
<c:set var="popupbuttons">
	<c:choose>
		<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForWorkTicket.html?id=${workTicket.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty claim.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForClaim.html?id=${claim.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty creditCard.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForCreditCard.html?id=${creditCard.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty servicePartner.id}">
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNoteForServicePartner.html?id=${servicePartner.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty accountLine.id && empty flagAcc}">
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNoteForAccountLine.html?id=${accountLine.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
	    <c:when test="${not empty serviceOrder.id && not empty accountLine.id && not empty flagAcc}">
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNoteForAccountLine.html?id=${accountLine.id }&decorator=popup&popup=true&subType=${subType}&flagAcc=true&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty carton.id}">
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNoteForCarton.html?id=${carton.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty container.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForContainer.html?id=${container.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty vehicle.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForVehicle.html?id=${vehicle.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && empty workTicket.id && empty claim.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForServiceOrder.html?id=${serviceOrder.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&notesList=YES&NetwokAccess=${param.NetwokAccess} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty partnerPrivate.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForPartnerPrivate.html?id=${partnerPrivate.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty payroll.id}">		
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForPayroll.html?id=${payroll.id}&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:when test="${not empty mss.id}">
			<input type="button" class="cssbutton buttonSht"   
        	onclick="location.href='<c:url value="/editNewNoteForMss.html?id=${mss.id}&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
	<c:when test="${not empty dsFamilyDetails.id}">
			<input type="button" class="cssbutton buttonSht"  
        	onclick="location.href='<c:url value="/editNewNoteForFamilyDetails.html?id=${dsFamilyDetails.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when> 
		<c:otherwise>
			<input type="button" class="cssbutton buttonSht" 
        	onclick="location.href='<c:url value="/editNewNote.html?id1=${customerFile.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:otherwise>
	 </c:choose> 
	 
</c:set>
<c:if test="${param.popup && notess == '[]'}">
<c:choose>
		<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
			<c:redirect url="/editNewNoteForWorkTicket.html?id=${workTicket.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty claim.id}">
			<c:redirect url="/editNewNoteForClaim.html?id=${claim.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty creditCard.id}">
			<c:redirect url="/editNewNoteForCreditCard.html?id=${creditCard.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty servicePartner.id}">
			<c:redirect url="/editNewNoteForServicePartner.html?id=${servicePartner.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty accountLine.id &&  empty flagAcc}">
			<c:redirect url="/editNewNoteForAccountLine.html?id=${accountLine.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty accountLine.id &&  not empty flagAcc}">
			<c:redirect url="/editNewNoteForAccountLine.html?id=${accountLine.id }&decorator=popup&popup=true&subType=${subType}&flagAcc=true&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty carton.id}">
			<c:redirect url="/editNewNoteForCarton.html?id=${carton.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty container.id}">
			<c:redirect url="/editNewNoteForContainer.html?id=${container.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty vehicle.id}">
			<c:redirect url="/editNewNoteForVehicle.html?id=${vehicle.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty serviceOrder.id && empty workTicket.id && empty claim.id}">
			<c:redirect url="/editNewNoteForServiceOrder.html?id=${serviceOrder.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&notesList=YES&NetwokAccess=${NetwokAccess} "/>
		</c:when>
		<c:when test="${not empty partnerPrivate.id}">
			<c:redirect url="/editNewNoteForPartnerPrivate.html?id=${partnerPrivate.id}&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty payroll.id}">
			<c:redirect url="/editNewNoteForPayroll.html?id=${payroll.id}&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when>
		<c:when test="${not empty mss.id}">
        	<c:redirect url="/editNewNoteForMss.html?id=${mss.id}&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId} "/>  
		</c:when>
	<c:when test="${not empty dsFamilyDetails.id}">
			<c:redirect url="/editNewNoteForFamilyDetails.html?id=${dsFamilyDetails.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:when> 
		<c:otherwise>
		<c:redirect url="/editNewNote.html?id1=${customerFile.id }&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}"/>
		</c:otherwise>
	 </c:choose>
</c:if>
	
<c:if test="${!param.popup}"> 
<c:choose>
	<c:when test="${noteFor=='agentQuotesSO'}">
		<div id="newmnav">
		  	<ul>
			  	<sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
			  		    <li ><a href="QuotationFileForm.html?id=${customerFile.id}"><span>Quotation File</span></a></li>
			   	</sec-auth:authComponent>
			   	<sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
			  			<li id="newmnav1" style="background:#FFF "><a href="quotationServiceOrders.html?id=${customerFile.id}" class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			   	</sec-auth:authComponent> 
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	             <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	             <li><a href="operationResource.html?id=${serviceOrder.id}&quoteFlag=y"><span>O&I</span></a></li>
	             </sec-auth:authComponent>
	              </c:if> 			   	
			   	<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}&Quote=y"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}&Quote=y"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}&Quote=y"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		     <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
		  	</ul>
	   	</div><div style="width: 770px"><div class="spn spnSF">&nbsp;</div></div>
  	</c:when>
	
	<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
	<c:if test="${noteFor=='WorkTicket' }">
		<div id="newmnav">
		  <ul>
			  <sec-auth:authComponent componentId="module.tab.notes.ticketTab">
			  <c:if test="${serviceOrder.job !='RLO'}"> 
	    		   	<li id="newmnav1" style="background:#FFF "><a href="editWorkTicketUpdate.html?id=${workTicket.id}" class="current"><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	    		   	</c:if>
			  </sec-auth:authComponent>
			  
	    	  <sec-auth:authComponent componentId="module.tab.notes.serviceOrderDetailsTab">
	    			<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	    			<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
	    			</c:if>
	    			<c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	    			<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
	    			</c:if>
	    	  </sec-auth:authComponent>
		  </ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div style="width: 770px"><div class="spn spnSF">&nbsp;</div>
		<!--<div style="padding-bottom:3px;"></div>
		--></div>
		</c:if>
	</c:when>
	<c:when test="${not empty serviceOrder.id && empty workTicket.id}">
	
	<c:if test="${noteFor=='Claim'}">
		<div id="newmnav">
		  <ul>			  
	    	 <sec-auth:authComponent componentId="module.tab.notes.serviceOrderDetailsTab">
			  	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  	
			    	<li ><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" ><span>S/O Details<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
			 	</c:if>
			 	<c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
			    	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" ><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
			 	</c:if>
			 	</sec-auth:authComponent>
				<sec-auth:authComponent componentId="module.tab.notes.billingTab">
				  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
				  		<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
				  	</sec-auth:authComponent>
			 	</sec-auth:authComponent>
			 	<sec-auth:authComponent componentId="module.tab.notes.accountingTab">			 	
			  	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			 	</c:if>
			 	</sec-auth:authComponent>
			 	<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			 	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			 	</c:if>
			 	</sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
  	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>
			 	<sec-auth:authComponent componentId="module.tab.notes.forwardingTab">
			 	<c:if test="${OIFlag == true}"> 
   <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
			 	 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			 	<c:if test="${serviceOrder.job !='RLO'}"> 
			  		<c:if test="${forwardingTabVal!='Y'}"> 
	   					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  				</c:if>
	  				<c:if test="${forwardingTabVal=='Y'}">
	  					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  				</c:if>
			  	</c:if>
			  	</c:if>
			  	</c:if></c:if>
			 	</sec-auth:authComponent>
			 	<sec-auth:authComponent componentId="module.tab.notes.domesticTab">
			  		<c:if test="${serviceOrder.job !='INT'}">
			  		<c:if test="${serviceOrder.job !='RLO'}"> 
			   			<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			   			</c:if>
			  		</c:if>
			  	</sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.notes.statusTab">
			  		<c:if test="${serviceOrder.job =='RLO'}"> 
	 				<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
					</c:if>
					<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
					</c:if>				  		
			  	</sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.notes.ticketTab">
			  	 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  	<c:if test="${serviceOrder.job !='RLO'}"> 
			  		<li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
			  		</c:if>
			  		</c:if>
			  	</sec-auth:authComponent>
			  	
			  	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  	 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  	 
			  	<c:if test="${serviceOrder.job !='RLO'}"> 
			  
			  		<li id="newmnav1" style="background:#FFF "><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  		</c:if>
			  		</c:if>
			  	</sec-auth:authComponent>
			  	
			  	
			  		<configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  	</configByCorp:fieldVisibility>
			  	<sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
			  		<li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
			  	</sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.notes.reportTab">
			  		<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Notes&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  	</sec-auth:authComponent>		
			   	<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	          	</sec-auth:authComponent>
	          	<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		     <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
	          	<sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
	           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
	          	</sec-auth:authComponent>	          	
		  </ul>
		</div><div style="width: 770px"><div class="spn spnSF">&nbsp;</div>
		<!--<div style="padding-bottom:3px;"></div>
		--></div>
		</c:if>
	
	<c:if test="${noteFor=='ServiceOrder'}">
		<div id="newmnav">
		  	<ul>
			  	<sec-auth:authComponent componentId="module.tab.notes.serviceOrderDetailsTab">
			  	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			    	<li id="newmnav1" style="background:#FFF "><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" class="current"><span>S/O Details<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
			 	</c:if>
			 	<c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
			    	<li id="newmnav1" style="background:#FFF "><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
			 	</c:if>
			 	</sec-auth:authComponent>
			 	
			 	 <c:if test="${userType =='ACCOUNT'}">
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
                       <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
                          <li><a href="operationResourceFromAcPortal.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
                     </sec-auth:authComponent>
                 </c:if>
                </c:if> 
			 	
				<sec-auth:authComponent componentId="module.tab.notes.billingTab">
				  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
				  		<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
				  	</sec-auth:authComponent>
			 	</sec-auth:authComponent>
			 	<sec-auth:authComponent componentId="module.tab.notes.accountingTab">			 	
			  	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			 	</c:if>
			 	</sec-auth:authComponent>
			 	<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			 	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			 	</c:if>
			 	</sec-auth:authComponent>
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
  	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>
			 	<sec-auth:authComponent componentId="module.tab.notes.forwardingTab">
			 	 <c:if test="${OIFlag == true}"> 
  <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
			 	 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			 	<c:if test="${serviceOrder.job !='RLO'}"> 
			  		<c:if test="${forwardingTabVal!='Y'}"> 
	   					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  				</c:if>
	  				<c:if test="${forwardingTabVal=='Y'}">
	  					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  				</c:if>
			  	</c:if>
			  	</c:if>
			  	</c:if>
			  	</c:if>
			 	</sec-auth:authComponent>
			 	<sec-auth:authComponent componentId="module.tab.notes.domesticTab">
			  		 <c:if test="${OIFlag == true}"> 
			  		<c:if test="${serviceOrder.job !='INT'}">
			  		<c:if test="${serviceOrder.job !='RLO'}"> 
			   			<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			   			</c:if>
			  		</c:if>
			  		</c:if>
			  	</sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.notes.statusTab">
			  		 <c:if test="${OIFlag == true}"> 
			  		<c:if test="${serviceOrder.job =='RLO'}"> 
	 				<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
					</c:if>
					<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
					</c:if>	
					</c:if>			  		
			  	</sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.notes.ticketTab">
			  	 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  	<c:if test="${serviceOrder.job !='RLO'}"> 
			  		<li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
			  		</c:if>
			  		</c:if>
			  	</sec-auth:authComponent>
			  		<configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  	  <c:if test="${OIFlag == true}"> 
			  	 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  	<c:if test="${serviceOrder.job !='RLO'}"> 
			  		<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  		</c:if>
			  		</c:if>
			  		</c:if>
			  	</sec-auth:authComponent>
			  	</configByCorp:fieldVisibility>
			  	<sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
			  		<li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
			  	</sec-auth:authComponent>
			  	<sec-auth:authComponent componentId="module.tab.notes.reportTab">
			  		<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Notes&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  	</sec-auth:authComponent>		
			   	<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	          	</sec-auth:authComponent>
	          	<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
	          	<sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
	           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
	          	</sec-auth:authComponent>
	          	 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>	          	
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 2px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div style="width: 780px"><div class="spn spnSF">&nbsp;</div>
	
		</div>
		</c:if>
	</c:when>
	<c:otherwise>
		<div id="newmnav">
			<c:if test="${noteFor=='CustomerFile' }">
			  	<ul>
			  		<sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
		 				<li id="newmnav1" style="background:#FFF "><a href="editCustomerFile.html?id=${customerFile.id}"  class="current"><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		 		  	</sec-auth:authComponent>
		 		  	<sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
		 		  	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">		    
				    <li><a href="customerServiceOrders.html?id=${customerFile.id} "><span>Service Orders</span></a></li>
					</c:if>
					<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    		<li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		   			 </c:if>
		 		  	</sec-auth:authComponent>
		 		  	<sec-auth:authComponent componentId="module.tab.notes.rateRequestTab">
		 				<li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		 		 	 </sec-auth:authComponent>
			 		<%--
				     <li><a href="sortCustomerReportss.html?id=${customerFile.id}"><span>Forms</span></a></li>  
				  	--%>
			  		<sec-auth:authComponent componentId="module.tab.notes.reportTab">
		 				<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		 			</sec-auth:authComponent>
		 			<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		      <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
				</ul>
			</c:if>
		 	<c:if test="${noteFor=='agentQuotes'}">
		  		<ul>
					<sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
			  				<li id="newmnav1" style="background:#FFF "><a href="QuotationFileForm.html?id=${customerFile.id}"  class="current"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		</sec-auth:authComponent>
			  		<sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
			  				<li><a href="quotationServiceOrders.html?id=${customerFile.id} "><span>Quotes</span></a></li>
			  		</sec-auth:authComponent>
			  		<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}&Quote=y"><span>Survey Details</span></a></li>
		    	</c:if>
		 		</ul>
			</c:if>
		</div>
		<div style="width: 770px"><div class="spn spnSF">&nbsp;</div>

		</div>
	</c:otherwise>
</c:choose> 
<c:choose>
	<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
	<div id="content" align="center">
	<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
			<tbody>
			<tr>
			<td align="left" class="listwhitebox" style="width:80%">
				<table class="" border="0" style="width:100%">
				  <tbody>  	
				  	<tr><td align="left" height="5px"></td></tr>
				  	<tr><td align="right"><fmt:message key="billing.shipper"/></td>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"  size="17"  cssClass="input-textUpper" readonly="true"  onfocus="checkDate();"/>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.originCountry"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="15" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.Type"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="5" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.commodity"/></td>
				  	<td align="left" width="50"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.routing"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="8" readonly="true"/></td>
				  	</tr>
				  	<tr>
				  	<td align="right"><fmt:message key="billing.jobNo"/></td>
				  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="17" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.destination"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="15" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.mode"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="5" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.AccName"/></td>
				  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="39" readonly="true"/></td></tr>
				  	<tr><td align="left" height="5px"></td></tr>
		   		  </tbody>
			  </table>
			  </td></tr>
			</tbody>
		 </table> 
		 		</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	</c:when>
	<c:when test="${not empty serviceOrder.id && empty workTicket.id}">
	<div id="content" align="center">
	<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width:80%">
			<tbody>
			<tr><td align="left" class="listwhitebox" style="width:80%">
				<table class="" border="0" style="width:100%">
				  <tbody>  	
				  	<tr><td align="left" height="5px"></td></tr>
				  	<tr><td align="right"><fmt:message key="billing.shipper"/></td>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"  size="17"  cssClass="input-textUpper" readonly="true"  onfocus="checkDate();"/>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.originCountry"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="15" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.Type"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="5" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.commodity"/></td>
				  	<td align="left" width="50"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.routing"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="8" readonly="true"/></td>
				  	</tr>
				  	<tr><td align="right"><fmt:message key="billing.jobNo"/></td>
				  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="17" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.destination"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="15" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.mode"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="5" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.AccName"/></td>
				  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="33" readonly="true"/></td></tr>
				  	<tr><td align="left" height="5px"></td></tr>
		   		  </tbody>
			  </table>
			  </td></tr>
			</tbody>
		 </table> 
		 		</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:when>
<c:otherwise>
<c:if test="${empty payroll.id && empty partnerPrivate.id && noteFor!='Partner'}">
<div id="content" align="center">
 <div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width:95%">
			<tbody>
			<tr><td align="left" class="listwhitebox" style="width:100%">
				<table class="" border="0" style="width:80%">
				  <tbody>  	
				  	<tr>
						<td align="right">Shipper</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.firstName" required="true" size="20" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.lastName" required="true" size="20" readonly="true"/></td>
						<td align="right">Origin</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.originCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.originCountryCode" required="true" size="3" readonly="true"/></td>
						<td align="right">Type</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.job" required="true" size="3" readonly="true"/></td>
						<td align="right">Destination</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.destinationCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.destinationCountryCode" required="true" size="3" readonly="true"/></td>
					</tr>
				  </tbody>
				 </table>
				</td>
			</tr>
			</tbody>
		</table>
				</div>
<div class="bottom-header" style="margin-top:0px;"><span></span></div>
</div>
</div>
</c:if>
</c:otherwise>
</c:choose>
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div style="width: 765px;" >
				<div class="spn">&nbsp;</div>				
				</div>
				<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
				<table class="table" style="width:100%;" >
				<thead>
				<tr>
				<th><fmt:message key="notes.noteSubType"/></th>
				<th><fmt:message key="notes.forwardStatus"/></th>
				<th></th>				
				</tr></thead>	
						<tbody>
						<tr>
							<td>
								<s:select cssClass="list-menu"   name="notes.noteSubType" list="%{notesubtype}" headerKey="" headerValue="" cssStyle="width:200px"/>
							    
							</td>
							<td>
							
								<s:select cssClass="list-menu"   name="notes.noteStatus" list="%{notestatus}" headerKey="" headerValue="" cssStyle="width:200px"/>
							    
							</td>
							<td >
							    <c:out value="${searchbuttons}" escapeXml="false" />   
							</td>
							</tr>
							
						</tbody>
					</table>
							</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
								
</c:if>
<c:if test="${!param.popup}">
	<div id="newmnav">
		<ul>
		          <c:if test="${not empty partnerPrivate.id && empty payroll.id && noteFor=='Partner'}">
				  	<li id="newmnav1" style="background:#FFF; "><a class="current" href="notess.html?id=${idOfWhom }&notesId=${notesId}&noteFor=${noteFor }&ppType=${ppType}"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  	<li><a href="allNotess.html?id=${idOfWhom }&notesId=${notesId}&noteFor=${noteFor }&ppType=${ppType}"><span>ALL List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<c:if test="${from != 'View'}">
						<c:if test="${!param.popup && ppType=='AG'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Agent Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='AC'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='PP'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='CR'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='VN'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='OO'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
					</c:if>
					</c:if>
						<c:if test="${from == 'View'}">
						<c:if test="${!param.popup && ppType=='AG'}"> 
							<li><a href="findPartnerProfileList.html?from=view&code=${notesId}&partnerType=${ppType}&id=${idOfWhom}"><span>Agent Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='AC'}"> 
							<li><a href="viewPartner.html?id=${idOfWhom}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='PP'}"> 
							<li><a href="viewPartner.html?id=${idOfWhom}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='CR'}"> 
							<li><a href="viewPartner.html?id=${idOfWhom}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='VN'}"> 
							<li><a href="viewPartner.html?id=${idOfWhom}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='OO'}"> 
							<li><a href="viewPartner.html?id=${idOfWhom}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
						</c:if>
					</c:if>
					</c:if>
					<c:if test="${empty partnerPrivate.id && empty payroll.id && noteFor!='Partner'}">
				  	<li id="newmnav1" style="background:#FFF; "><a class="current" href="notess.html?id=${idOfWhom }&notesId=${notesId}&noteFor=${noteFor }"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					</c:if>
					<c:if test="${empty partnerPrivate.id && empty payroll.id && noteFor!='Partner'}">
				  	<li><a href="allNotess.html?id=${idOfWhom }&notesId=${notesId}&noteFor=${noteFor }"><span>ALL List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					</c:if>
					<c:if test="${empty partnerPrivate.id && empty payroll.id && noteFor!='Partner'}">
					  <c:if test="${empty  dsFamilyDetails.id}">
					<li><a href="raleventNotess.html?id=${idOfWhom}&customerNumber=${notesId}&noteFor=${noteFor}&noteFrom=${noteFor}"><span>Related Notes</span></a></li>
							<li><a href="#" onclick="javascript:openWindow('partnerNotess.html?id=${idOfWhom }&customerNumber=${notesId}&noteFor=${noteFor}&noteFrom=${noteFor}&decorator=popup&popup=true',800,600)"><span>Partner Notes</span></a></li>
						</c:if>
						</c:if>
		</ul>
	</div><div style="width: 770px"><div class="spn spnSF">&nbsp;</div>
	</div>
</c:if>
<c:if test="${param.popup}"> 
	<div id="newmnav">	
		<ul>
			
			<li id="newmnav1" style="background:#FFF;margin-bottom:2px; "><a class="current" href="notess.html?id=${idOfWhom}&notesId=${notesId}&noteFor=${noteFor }&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li ><a href="allNotess.html?id=${idOfWhom}&notesId=${notesId}&noteFor=${noteFor }&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true"><span>ALL List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						<c:if test="${userType!='DRIVER'}">
						<c:if test="${empty partnerPrivate.id}">
						  <c:if test="${empty  dsFamilyDetails.id}">
						<li><a href="raleventNotess.html?id=${idOfWhom }&customerNumber=${notesId}&noteFor=${noteFor}&noteFrom=${noteFor}&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true"><span>Related Notes</span></a></li>
				<li><a href="partnerNotess.html?id=${idOfWhom }&customerNumber=${notesId}&noteFor=${noteFor}&noteFrom=${noteFor}&decorator=popup&popup=true"><span>Partner Notes</span></a></li>
			</c:if>
			</c:if>
			</c:if>
		</ul>
	</div><div style="width: 700px"><div class="spn spnSF">&nbsp;</div>	
	</div>
</c:if>
<div id="Layer1" style="width:100%">				
<s:set name="notess" value="notess" scope="request"/> 
<c:if test="${!param.popup}"> 
	<display:table name="notess" class="table" requestURI="" id="notesList" export="false"  pagesize="10" style="width:100%">   
		<c:choose>
			<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForWorkTicket.html?from=list&id1=${workTicket.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty claim.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForClaim.html?from=list&id1=${claim.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty accountLine.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForAccountLine.html?from=list&id1=${accountLine.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty container.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForContainer.html?from=list&id1=${container.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty vehicle.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForVehicle.html?from=list&id1=${vehicle.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty carton.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForCarton.html?from=list&id1=${carton.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty servicePartner.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForServicePartner.html?from=list&id1=${servicePartner.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && empty workTicket.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForServiceOrder.html?from=list&id1=${serviceOrder.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty partnerPrivate.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartnerPrivate.html?id1=${partnerPrivate.id }&notesId=${notesId}&noteFor=${noteFor}&from=${from}&PPID=${PPID}&ppType=${ppType}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty payroll.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPayroll.html?from=list&id1=${payroll.id}&imageId=${imageId}&fieldId=${fieldId}" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty mss.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForMss.html?from=list&id1=${mss.id}&imageId=${imageId}&fieldId=${fieldId}" paramId="id" paramProperty="id"/>
			</c:when>
					<c:when test="${not empty dsFamilyDetails.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForFamilyDetails.html?from=list&id1=${dsFamilyDetails.id}&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:otherwise>
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNote.html?from=list&id1=${customerFile.id }&notesId=${notesId}&noteFor=${noteFor}" paramId="id" paramProperty="id"/> 
			</c:otherwise>
		 </c:choose> 
	
	    <display:column property="noteType" sortable="true" titleKey="notes.noteType"/>
	    <display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType"/>
	    <display:column sortable="true" titleKey="notes.subject" >
	    <c:if test="${notesList.subject != ''}" >
		<c:if test="${fn:length(notesList.subject) > 20}" >
		<c:set var="abc" value="${fn:substring(notesList.subject, 0, 20)}" />
	    <div align="left" onmouseover="findToolTipSubjectForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
		</c:if>
		<c:if test="${fn:length(notesList.subject) <= 20}" >
		<c:set var="abc" value="${fn:substring(notesList.subject, 0, 20)}" />
	    <div align="left" ><c:out value="${abc}" /></div>
		</c:if>
		</c:if>
		</display:column>
		<display:column sortable="true" title="Note" >
	    <c:if test="${notesList.note != ''}" >
	    <c:if test="${ischecked =='false'}">
		<c:if test="${fn:length(notesList.note) > 20}" >
		<c:set var="abc" value="${fn:substring(notesList.note, 0, 20)}" />
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" escapeXml="false"/>&nbsp;.....</div>
	    </c:if>
	    <c:if test="${fn:length(notesList.note) <= 20}" >
		<c:set var="abc" value="${fn:substring(notesList.note, 0, 20)}" />
	    <div align="left" ><c:out value="${abc}" escapeXml="false"/></div>
		</c:if>
	    </c:if>
	    <c:if test="${ischecked =='true'}">
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" style="height: 18px; width: 182px; overflow: hidden;"><c:out value="${notesList.note}" escapeXml="false"/></div>&nbsp;.....
	    </c:if>
		</c:if>
		</display:column>
		<display:column  sortable="true" title="Follow&nbsp;Up&nbsp;Date" >
		<fmt:timeZone value="${sessionTimeZone}" >
		<fmt:formatDate value="${notesList.forwardDate}" pattern="dd-MMM-yyyy"/>
		</fmt:timeZone>
		</display:column>
	    <display:column property="updatedOn" sortable="true" titleKey="notes.updatedOn" format="{0,date,dd-MMM-yyyy}" />
	    <display:column property="noteStatus" sortable="true" titleKey="notes.noteStatus"/>
	    <display:column property="updatedBy" sortable="true" title="Modified&nbsp;By"/>
	    <display:column property="noteStatus" sortable="true" titleKey="notes.noteStatus"/>
	     <display:column  sortable="true" title="Active"> <i class="fa fa-archive" aria-hidden="true"></i> </display:column>
	     
	    <display:setProperty name="paging.banner.item_name" value="customerfilenotes"/>   
	    <display:setProperty name="paging.banner.items_name" value="notes"/>   
	
	    <display:setProperty name="export.excel.filename" value="CustomerFileNotes List.xls"/>   
	    <display:setProperty name="export.csv.filename" value="CustomerFileNotes List.csv"/>   
	    <display:setProperty name="export.pdf.filename" value="CustomerFileNotes List.pdf"/>   
	</display:table>  
</c:if>
<c:if test="${param.popup}"> 
	<display:table name="notess" class="table" requestURI="" id="notesList" export="false"  pagesize="10" style="width:100%;margin-top:2px;">   
		
		<c:choose>
			<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForWorkTicket.html?from=list&id1=${workTicket.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>			
			<c:when test="${not empty serviceOrder.id && not empty claim.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForClaim.html?from=list&id1=${claim.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty accountLine.id &&  empty flagAcc}">

				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForAccountLine.html?from=list&id1=${accountLine.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty accountLine.id && not empty flagAcc}">

				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForAccountLine.html?from=list&id1=${accountLine.id }&flagAcc=true&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty container.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForContainer.html?from=list&id1=${container.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty vehicle.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForVehicle.html?from=list&id1=${vehicle.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && not empty carton.id}">
				<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForCarton.html?from=list&id1=${carton.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty serviceOrder.id && empty workTicket.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForServiceOrder.html?from=list&id1=${serviceOrder.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty partnerPrivate.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartnerPrivate.html?from=list&id1=${partnerPrivate.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty payroll.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPayroll.html?from=list&id1=${payroll.id}&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:when test="${not empty mss.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForMss.html?from=list&id1=${mss.id}&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
				<c:when test="${not empty dsFamilyDetails.id}">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForFamilyDetails.html?from=list&id1=${dsFamilyDetails.id}&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
			</c:when>
			<c:otherwise>
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNote.html?from=list&id1=${customerFile.id }&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true" paramId="id" paramProperty="id"/> 
			</c:otherwise>
		 </c:choose> 
	
	    <display:column property="noteType" sortable="true" titleKey="notes.noteType" style="width:100px"/>
	    <display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType" style="width:80px"/>
	    <display:column sortable="true" titleKey="notes.subject" >
	    <c:if test="${notesList.subject != ''}" >
		<c:if test="${fn:length(notesList.subject) > 20}" >
		<c:set var="abc" value="${fn:substring(notesList.subject, 0, 20)}" />
	    <div align="left" onmouseover="findToolTipSubjectForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
		</c:if>
		<c:if test="${fn:length(notesList.subject) <= 20}" >
		<c:set var="abc" value="${fn:substring(notesList.subject, 0, 20)}" />
	    <div align="left" ><c:out value="${abc}" /></div>
		</c:if>
		</c:if>
		</display:column>
	    <display:column sortable="true" title="Note" >
	    <c:if test="${notesList.note != ''}" >
	   	<c:if test="${ischecked =='false'}"> 
		<c:if test="${fn:length(notesList.note) > 20}" >
		<c:set var="abc" value="${fn:substring(notesList.note, 0, 20)}" />
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" escapeXml="false"/>&nbsp;.....</div>
	    </c:if>
	    <c:if test="${fn:length(notesList.note) <= 20}" >
		<c:set var="abc" value="${fn:substring(notesList.note, 0, 20)}" />
	    <div align="left" ><c:out value="${abc}" escapeXml="false"/></div>
		</c:if>
	    </c:if>
	    <c:if test="${ischecked =='true'}">
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" style="height: 18px; width: 182px; overflow: hidden;"><c:out value="${notesList.note}" escapeXml="false"/></div>&nbsp;.....
	    </c:if>
		
		</c:if>
		</display:column>
		<display:column  sortable="true" title="Follow&nbsp;Up&nbsp;Date" >
		<fmt:timeZone value="${sessionTimeZone}" >
		<fmt:formatDate value="${notesList.forwardDate}" pattern="dd-MMM-yyyy"/>
		</fmt:timeZone>
		</display:column>
		<display:column property="updatedOn" sortable="true" titleKey="notes.updatedOn" style="width:100px" format="{0,date,dd-MMM-yyyy}" />
	    <display:column property="updatedBy" sortable="true" title="Modified&nbsp;By"  style="width:45px"/>
	    <display:column property="noteStatus" sortable="true" titleKey="notes.noteStatus"/>
	    <display:setProperty name="paging.banner.item_name" value="customerfilenotes"/>   
	    <display:setProperty name="paging.banner.items_name" value="notes"/>   
	
	    <display:setProperty name="export.excel.filename" value="CustomerFileNotes List.xls"/>   
	    <display:setProperty name="export.csv.filename" value="CustomerFileNotes List.csv"/>   
	    <display:setProperty name="export.pdf.filename" value="CustomerFileNotes List.pdf"/>   
	</display:table>  
</c:if>
</div>   
<c:if test="${!param.popup && from != 'View'}"> 
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<c:if test="${param.popup}"> 
<c:out value="${popupbuttons}" escapeXml="false" />&nbsp;&nbsp;&nbsp;<input type="button" class="cssbutton buttonSht"  value="Close" onclick="window.close();"/>
</c:if>
<c:set var="noteFrom" value="${noteFor}" scope="session"/>
</s:form>
<script type="text/javascript">   
    highlightTableRows("notesList");   
    Form.focusFirstElement($("notesListForm")); 
</script> 