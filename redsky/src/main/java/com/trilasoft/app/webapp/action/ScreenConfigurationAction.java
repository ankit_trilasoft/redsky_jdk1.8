package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.*;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.BaseObject;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.ScreenConfiguration;
import com.trilasoft.app.model.SystemConfiguration;
import com.trilasoft.app.service.ScreenConfigurationManager;
import com.trilasoft.app.service.SystemConfigurationManager;

public class ScreenConfigurationAction extends BaseAction{
	
	private String sessionCorpID;
	private Long id;
	private Long systemId;
	private  static Date createdon;
	private List screenConfigurations;
    private ScreenConfiguration screenConfiguration;
    private ScreenConfigurationManager screenConfigurationManager;
	private SystemConfiguration systemConfiguration;
	private SystemConfigurationManager systemConfigurationManager;
	public ScreenConfigurationAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public String list()
	{
		screenConfigurations=screenConfigurationManager.getAll();
		return SUCCESS;
	}
	public String edit()
	{
		if (id != null) { 
			screenConfiguration = screenConfigurationManager.get(id); 
			systemConfiguration = screenConfiguration.getSystemConfiguration();
			createdon=screenConfiguration.getCreatedOn();
			screenConfiguration.setCreatedOn(createdon);
			screenConfiguration.setUpdatedOn(new Date());
	
		} else { 
			screenConfiguration = new ScreenConfiguration();
			screenConfiguration.setCreatedOn(new Date());
			screenConfiguration.setUpdatedOn(new Date());
	
		}
		return SUCCESS;
	}
	public String delete() { 
    	
		screenConfigurationManager.remove(screenConfiguration.getId()); 
    
	   
	     screenConfiguration.setCreatedOn(createdon);
	     screenConfiguration.setUpdatedOn(new Date());

	     return SUCCESS;
 
} 
	public String save()
	{
		if (cancel != null) {
    		
            return "cancel"; 
        } 
    	
        if (delete != null) { 
        	
            return delete(); 
        } 
        
        boolean isNew = (screenConfiguration.getId() == null); 
        String key = (isNew) ? "screenConfiguration added" : "screenConfiguration updated"; 
        saveMessage(getText(key)); 
        screenConfiguration.setSystemConfiguration(systemConfiguration);
        screenConfigurationManager.save(screenConfiguration);
        if (!isNew) { 
        	screenConfiguration.setCreatedOn(createdon);
        	screenConfiguration.setUpdatedOn(new Date());
            return INPUT; 
        } else {
        	screenConfiguration.setCreatedOn(new Date());
        	screenConfiguration.setUpdatedOn(new Date());
		return SUCCESS;
        }
	}
    public  String search()
    {
    	screenConfigurations=screenConfigurationManager.searchByCorpIdTableAndField(screenConfiguration.getCorpID(), screenConfiguration.getTableName(), screenConfiguration.getFieldName());
    	return SUCCESS;
    }
	public static Date getCreatedon() {
		return createdon;
	}

	public static void setCreatedon(Date createdon) {
		ScreenConfigurationAction.createdon = createdon;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ScreenConfiguration getScreenConfiguration() {
		return screenConfiguration;
	}

	public void setScreenConfiguration(ScreenConfiguration screenConfiguration) {
		this.screenConfiguration = screenConfiguration;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getSystemId() {
		return systemId;
	}

	public void setSystemId(Long systemId) {
		this.systemId = systemId;
	}

	public void setScreenConfigurationManager(
			ScreenConfigurationManager screenConfigurationManager) {
		this.screenConfigurationManager = screenConfigurationManager;
	}

	public List getScreenConfigurations() {
		return screenConfigurations;
	}

	public void setScreenConfigurations(List screenConfigurations) {
		this.screenConfigurations = screenConfigurations;
	}

	public SystemConfiguration getSystemConfiguration() {
		return systemConfiguration;
	}

	public void setSystemConfiguration(SystemConfiguration systemConfiguration) {
		this.systemConfiguration = systemConfiguration;
	}

	public void setSystemConfigurationManager(
			SystemConfigurationManager systemConfigurationManager) {
		this.systemConfigurationManager = systemConfigurationManager;
	}
}