<%@ include file="/common/taglibs.jsp"%>   
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>


<head>   
    <title><fmt:message key="costingDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='costingDetail.heading'/>"/> 
    <style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>  
</head>
   
<s:form id="costingForm" name="costingForm" action="saveCosting" method="post" validate="true"> 

<s:hidden name="costing.id" value="%{costing.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="costing.shipNumber"/>

<!-- For New Record -->
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="customerFile.id"/>
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>


<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<!-- For Partner Type -->

<div id="newmnav">


  <ul>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
  </sec-auth:authComponent>
  <li><a href="servicePartners.html?id=${serviceOrder.id}"><span>Partner</span></a></li>
  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
  </sec-auth:authComponent>
  <li><a href="containers.html?id=${serviceOrder.id}"><span>Container</span></a></li>
  
  <c:if test="${serviceOrder.job !='INT'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
     <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
    </c:if>
    </sec-auth:authComponent>
   <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
   <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Invoice<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
</ul>
		</div><div class="spn">&nbsp;</div><br>



 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" onfocus="checkData();"  size="15"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountry" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountry" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

 
 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  <li><a href="costings.html?id=${serviceOrder.id}"><span>Invoice Line</span></a></li>
  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Invoice Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  <li><a><span>Distribution</span></a></li>
  <li><a><span>Job Costing</span></a></li>
</ul>
</td></tr>
</tbody></table> 
 
 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	 
	<tr><td align="left" class="listwhitetext">
		<table class="detailTabLabel" border="0">
		  <tbody>
		  	<tr><td align="left" height="5px"></td></tr>  	
		  	<tr>
		  		<c:set var="billingContract" value="${costing.contract}"/>
			  	<td align="right"><fmt:message key="costing.contract"/></td>
			  	<td align="left"><s:textfield name="costing.contract" cssClass="input-textUpper" readonly="true" size="28" /></td>
			  	<td align="right" width="100px"><fmt:message key="costing.charge"/><font color="red" size="2">*</font></td>
			  	<td align="left"><s:textfield name="costing.type" size="10" cssClass="input-text" readonly="true"/></td><td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('chargess.html?contract=${billingContract}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=costing.expGl&fld_secondDescription=costing.gl&fld_description=costing.description&fld_code=costing.type');" src="<c:url value='/images/open-popup.gif'/>" /></td>
			  	<td align="left"><fmt:message key="costing.ticketNumber"/></td><td align="left"><s:textfield name="costing.ticketNumber" required="true" cssClass="input-textUpper" size="10" readonly="true"/></td>
			  	<td align="right"><fmt:message key="costing.line"/></td><td align="left"><s:textfield name="costing.line" readonly="true" required="true" cssClass="input-textUpper" size="7" value="${costing.id}"/></td>
		  	</tr>
		  	<tr>
		  		<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="costing.poNumber"/></td>
		  		<td align="left"><s:textfield name="costing.poNumber" cssClass="input-text" maxlength="12" size="28"/></td>
		  		<td align="right" width="100px"><fmt:message key="costing.description"/></td>
		  		<td align="left" colspan="6"><s:textfield name="costing.description" readonly="true" required="true" cssClass="input-textUpper" size="60"/></td>
		  	</tr>
		  	<tr><td align="left" height="5px"></td></tr>
		  </tbody>
		 </table>
 	  </td>
 	</tr>
 	
   </tbody>
  </table>
 	<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	
	
	<tr><td align="left" class="listwhitetext" colspan="3" >
		<table  class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
		  <tbody>  	
		  <!--  Income and Expense -->
		  		<tr>
			  		<td class="subcontent-tab" width="350px" align="center" colspan="5"><fmt:message key="costing.Income"/></td>
			  		<td></td>
			  		<td class="subcontent-tab" width="350px" align="center" colspan="5"><fmt:message key="costing.Expense"/></td>
			  	</tr>
		  <!--  Income and Expense -->
				<tr>
					<td align="right" width="67px"><fmt:message key="costing.billToCode"/>&nbsp;</td>
					<td align="left"><s:textfield name="costing.billToCode" size="6" required="true" onkeydown="return onlyAlphaNumericAllowed(event)" cssClass="input-text" maxlength="8" readonly="true"/></td>
					<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=costing.billToName&fld_code=costing.billToCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
					<td align="left"><s:textfield name="costing.billToName" onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" maxlength="30" size="32" readonly="true"/></td>
					<td></td>
					
					<td valign="top"><img height="45px" width="2px" src="<c:url value='/images/vertline.jpg'/>"/></td>
					
					<td align="right" width="45px"><fmt:message key="costing.Vendor"/>&nbsp;</td>
				    <td align="left"><s:textfield key="costing.expPayeeType" required="true" cssClass="input-text" maxlength="2" readonly="true" size="1"/></td>
				    <td align="left"><s:textfield key="costing.expPayeeCode" onkeydown="return onlyNumsAllowed(event)" required="true" cssClass="input-text" maxlength="8" readonly="true" size="5"/></td>
				    
				    <c:set var="partnerTypeVal" value="${costing.expPayeeType}"/>
				    <c:choose>
						<c:when test="${partnerTypeVal=='BR' || partnerTypeVal=='FW' }">
							<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('brokerServicePartners.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=costing.expPayeeType&fld_description=costing.expPayeeName&fld_code=costing.expPayeeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						</c:when>
						<c:when test="${partnerTypeVal=='CR' }">
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('carrierServicePartners.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=costing.expPayeeType&fld_description=costing.expPayeeName&fld_code=costing.expPayeeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						</c:when>
						<c:when test="${partnerTypeVal=='BK' || partnerTypeVal=='DA' || partnerTypeVal=='OA' || partnerTypeVal=='DL' || partnerTypeVal=='PK' || partnerTypeVal=='SA'}">
						<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('agentServicePartners.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=costing.expPayeeType&fld_description=costing.expPayeeName&fld_code=costing.expPayeeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						</c:when>
				    	<c:otherwise>
							<td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('findServicePartners.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=costing.expPayeeType&fld_description=costing.expPayeeName&fld_code=costing.expPayeeCode');" src="<c:url value='/images/open-popup.gif'/>"/></td>
						</c:otherwise>
					</c:choose>
				    
				    <td align="left"><s:textfield key="costing.expPayeeName" onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" maxlength="30" readonly="true" size="30"/></td>				
				</tr>
		   </tbody>
		 </table>
 	</td>
   </tr>
 	
    <tr><td align="left" class="subcontent-tab" colspan="3">Actuals</td></tr>
 	<tr><td align="left" class="listwhitetext" width="50%">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  		<tr><td align="left" height="5px"></td></tr>
				<tr><td align="right" width="60px"><fmt:message key="costing.invoiceNumber"/></td><td align="left"><s:textfield name="costing.invoiceNumber" size="7" readonly="true" required="true" cssClass="input-textUpper"/></td>
				<td align="right"><fmt:message key="costing.dated"/></td>
			
				<c:if test="${not empty costing.invoiceDate}">
					<td align="left" colspan="2"><s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.invoiceDate"/></s:text>
				    <s:textfield name="costing.invoiceDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-textUpper" size="7"/></td>
				</c:if>
				<c:if test="${empty costing.invoiceDate}">
					<td align="left" colspan="2"><s:textfield name="costing.invoiceDate" readonly="true" required="true" cssClass="input-textUpper" size="7"/></td>
				</c:if>
				
				
				
				</tr>
				<tr><td align="left"></td><td align="left" colspan="3"><s:submit cssClass="cssbuttonA" cssStyle="width:110px; height:25px" method="generateInvoiceNumber" key="button.generateInvoice"/></td></tr>
				
				<tr>
					<td align="right" width="60px"><fmt:message key="costing.quantity"/></td><td align="left"><s:textfield name="costing.quantity" maxlength="17" onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" size="7"/></td>
					<td align="right"><fmt:message key="costing.item"/></td><td align="left"><s:textfield name="costing.item" maxlength="15" cssClass="input-text" size="7"/></td>
					
					
				</tr>
				<tr>
					<td align="right" width="60px"><fmt:message key="costing.price"/></td><td align="left"><s:textfield name="costing.rate" maxlength="17" onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" size="7" onchange="assignDefaultValue();"/></td>
					<td align="right"><fmt:message key="costing.discount"/></td><td align="left"><s:textfield name="costing.discount" maxlength="17" onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" size="7"/></td>
					
					<td width="82px"></td>
				
				</tr>
				<tr>
					<td align="right" width="60px"><fmt:message key="costing.divMult"/></td><td align="left"><s:select size="1px" name="costing.divMult"  list="{'Divison','Multiply'}" cssStyle="width:66px" cssClass="list-menu"/></td>
					<td align="left"><s:textfield name="costing.per" maxlength="17" onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" size="3"/></td>
					<td align="left" width="70px"><fmt:message key="costing.perItem"/></td><td align="left"><s:textfield name="costing.perItem" maxlength="15" cssClass="input-text" size="7"/></td>
				</tr>
				<tr>
					 
					<td align="right" width="60px"><fmt:message key="costing.amount"/></td><td align="left" colspan="2"><s:textfield name="costing.amount" maxlength="17" onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" size="17"/></td>
					<td align="left"><input type="button" style="width:55px; height:25px" class="cssbuttonA" name="Calc" value="Calc" onclick="calcFunction();"/></td>
				</tr>
 		   </tbody>
		 </table>
		</td>
	 	
	 	<td valign="top"><img height="165px" width="2px" src="<c:url value='/images/vertline.jpg'/>"/></td> 
 		
 		<td align="left" class="listwhitetext" width="50%">
		<table class="detailTabLabel" border="0">
		 <tbody>  	
			  <tr><td align="left" class="listwhitetext">
			  <table class="detailTabLabel" border="0">
			  <tbody>
			  	<tr>
						<td align="right" width="66px"><fmt:message key="costing.invoiceNumber"/></td>
						<td align="left"><s:textfield name="costing.expInvoiceNumber" onkeydown="return onlyAlphaNumericAllowed(event)" required="true" cssClass="input-text" maxlength="15" size="7"/></td>
						 
						<td align="right"><fmt:message key="costing.expValInvoiceDate"/></td>
		              	<td align="left">				   						
							<c:if test="${not empty costing.expValInvoiceDate}">
									<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.expValInvoiceDate"/></s:text>
								    <s:textfield id="expValInvoiceDate" name="costing.expValInvoiceDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.expValInvoiceDate}">
									<s:textfield id="expValInvoiceDate" name="costing.expValInvoiceDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>
								<td><img id="expValInvoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						
					</tr>
				  </tbody>
				  </table>
				  <table class="detailTabLabel" border="0">
				  <tbody>
				   <tr>
						<td align="right" width="66px"><fmt:message key="costing.valInvoiceDate"/></td>
						<td align="left">
						<c:if test="${not empty costing.expInvoiceDate}">
							<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.expInvoiceDate"/></s:text>
						    <s:textfield id="expInvoiceDate" name="costing.expInvoiceDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.expInvoiceDate}">
							<s:textfield id="expInvoiceDate" name="costing.expInvoiceDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<td><img id="expInvoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	                </tr>
	                
	                <tr>
	                	<td align="right"><fmt:message key="costing.amountFx"/></td>
	                	<td align="left"><s:textfield name="costing.expAmountFx" required="true" onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" maxlength="15" size="7"/></td>
	                	<td align="right" colspan="2"><fmt:message key="costing.exchangeRate"/></td>
	                	<td align="left"><s:textfield name="costing.expExchangeRate" onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" maxlength="15" size="10"/></td>
	                </tr>
			    	<tr>
			    		<td align="right"><fmt:message key="costing.valueDate"/></td>
			    		<td align="left">
						<c:if test="${not empty costing.valueDate}">
							<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.valueDate"/></s:text>
						    <s:textfield id="valueDate" name="costing.valueDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.valueDate}">
							<s:textfield id="valueDate" name="costing.valueDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<td><img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	                
			    		<td align="right"><fmt:message key="costing.currencyCode"/></td>
			    	    <td><s:select name="costing.expCurrencyCode" list="%{countrycode}" cssStyle="width:115px" cssClass="list-menu"/></td>
			    	</tr>
			    	    
			  </tbody>
			 </table>
			 <table class="detailTabLabel" border="0">
			  <tbody>
						<tr>
			    	    	<td align="right" width="66px"><fmt:message key="costing.totalInvoice"/></td>
			    	    	<td align="left"><s:textfield name="costing.expTotalInvoice" onkeydown="return onlyNumsAllowed(event)" cssClass="input-text" maxlength="17" size="17"/></td>
		      				<td align="left"><input type="button" style="width:55px; height:25px" class="cssbuttonA" name="Calc" value="Calc" onclick="expenseCalc()"/></td>
		      			</tr>
		      			<tr>
		      				<td align="right"><fmt:message key="costing.action"/></td>
		      				<td align="left"><s:select name="costing.expAction" list="%{action}" cssStyle="width:114px" cssClass="list-menu" /></td>
		      			</tr>
				  </tbody>
				  </table>
			 </td></tr>
 			</tbody>
		 </table>
 		</td>
 	</tr>
 	
 	<tr>
	<td height="10" align="left" colspan="3" class="listwhitetext">
		<div class="subcontent-tab"><a href="javascript:void(0)"
	class="dsphead" onclick="dsp(this)">Estimation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
							class="dspchar2"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span></a></div>
  
  		<div class="dspcont" id="Layer4">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  
 		  <tr><td align="left" class="listwhitetext" width="50%">
				<table class="detailTabLabel" border="0">
				  <tbody>
				  <tr><td align="left" height="5px"></td></tr>
				  	<tr>
				  		<td align="right" width="90px"><fmt:message key="costing.entitledAmount"/></td>
						<td align="left"><s:textfield name="costing.entitledAmount" onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" size="7" onchange="assignEntldDate();"/></td>
						<td align="left">
						
							<c:if test="${not empty costing.entitledDate}">
								<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.entitledDate"/></s:text>
							    <s:textfield id="entitledDate" name="costing.entitledDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.entitledDate}">
								<s:textfield id="entitledDate" name="costing.entitledDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<td><img id="entitledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                
							</td>

					</tr>
				   	
					<tr>
						<td align="right" width="90px"><fmt:message key="costing.estimate"/></td>
						<td align="left"><s:textfield name="costing.estimate" onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" onchange="assignEstDate();" size="7"/></td>
						<td align="left">
							<c:if test="${not empty costing.estimateDate}">
								<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.estimateDate"/></s:text>
							    <s:textfield id="estimateDate" name="costing.estimateDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.estimateDate}">
								<s:textfield id="estimateDate" name="costing.estimateDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<td><img id="estimateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
             		</tr>
						<tr>         
			            <td align="right"><fmt:message key="costing.estimateSent"/></td>
			            <td align="left">
		            		<c:if test="${not empty costing.estimateSent}">
								<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.estimateSent"/></s:text>
							    <s:textfield id="estimateSent" name="costing.estimateSent" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.estimateSent}">
								<s:textfield id="estimateSent" name="costing.estimateSent" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<td><img id="estimateSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	
						
		          </tr>
		   		  <tr>
		   		  		<td align="right" width="90px"><fmt:message key="costing.estimateRevisedAmount"/></td>
		   		  		<td align="left"><s:textfield name="costing.estimateRevisedAmount" onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" onchange="assignRevDate();" size="7"/></td>
		   				<td align="left">
			
							<c:if test="${not empty costing.estimateRevisedDate}">
								<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.estimateRevisedDate"/></s:text>
							    <s:textfield id="estimateRevisedDate" name="costing.estimateRevisedDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.estimateRevisedDate}">
								<s:textfield id="estimateRevisedDate" name="costing.estimateRevisedDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<td><img id="estimateRevisedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                			
						</td>
				   </tr>
				   </tbody>
			 </table>
		 	</td>
		 	
		 	<td valign="top" width="1px"><img height="110px" width="2px" src="<c:url value='/images/vertline.jpg'/>"/></td>
		 	<td align="left" class="listwhitetext" width="50%" valign="top">
				<table class="detailTabLabel" border="0">
				  <tbody>
				  <tr><td align="left" height="5px"></td></tr>
				<tr>
						<!-- Expense -->
						<td align="right" width="90px"><fmt:message key="costing.expEstimateExpenses"/></td>
						<td align="left"><s:textfield name="costing.expEstimateExpenses"  onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" maxlength="15" size="7" onchange="assignExpRevDate();"/></td>
						<td align="left">
						<c:if test="${not empty costing.expEstimateDate}">
								<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.expEstimateDate"/></s:text>
							    <s:textfield id="expEstimateDate" name="costing.expEstimateDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.expEstimateDate}">
								<s:textfield id="expEstimateDate" name="costing.expEstimateDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
							<td><img id="expEstimateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                			
						
						<!-- Expense -->
				</tr>
				<tr>
					<!-- Expense -->
		            <td align="right" width="90px"><fmt:message key="costing.expRevisedAmount"/></td>
		            <td align="left"><s:textfield name="costing.expRevisedAmount" onkeydown="return onlyFloatNumsAllowed(event)" required="true" cssClass="input-text" maxlength="15" size="7"onchange="assignExpEntldDate();"/></td>
					<td align="left">				   						
						<c:if test="${not empty costing.expRevisedDate}">
								<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.expRevisedDate"/></s:text>
							    <s:textfield id="expRevisedDate" name="costing.expRevisedDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.expRevisedDate}">
								<s:textfield id="expRevisedDate" name="costing.expRevisedDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
							<td><img id="expRevisedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                			
						
		            <!-- Expense -->
				</tr>
				
			</tbody>
 		</table>
 		</td></tr>
 	   </tbody>
 	</table></div></td>
 		  <td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr>  

 	<tr>
	<td height="10" align="left" colspan="3">
		<div class="subcontent-tab"><a href="javascript:void(0)"
	class="dsphead" onclick="dsp(this)">Accounting&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
							class="dspchar2"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span></a></div>
  
  		<div class="dspcont" id="Layer5">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" class="listwhitetext" width="50%" valign="top">
				<table class="detailTabLabel" border="0">
				  <tbody>
				  	<tr><td align="left" height="5px"></td></tr> 
					 <tr>
						 <td align="right" width="60px"><fmt:message key="costing.gl"/></td>
						 <td align="left" colspan="2"><s:textfield name="costing.gl" required="true" readonly="true" cssClass="input-text" size="7"/></td>
						 <td align="right"><fmt:message key="costing.postDate"/></td>
			             
							<c:if test="${not empty costing.postDate}">
							<td align="left"><s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.postDate"/></s:text>
							            <s:textfield name="costing.postDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.postDate}">
							<td align="left"><s:textfield name="costing.postDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>  
					 
		             </tr>
		             <tr>
		             	<td align="right"><fmt:message key="costing.sentAccount"/></td>
						<td align="left" colspan="2">
			            <s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.sentAccount"/></s:text>
                		<s:textfield name="costing.sentAccount" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
                		
					
		                <td align="right"><fmt:message key="costing.sentFile"/></td>
		                <td align="left"><s:textfield name="costing.sentFile" required="true" cssClass="input-text" readonly="true" size="7"/></td>
		             </tr>
		   			<tr>
		   				<td align="right" width="60px"><fmt:message key="costing.sentWb"/></td>
				   		<td align="left">
						<c:if test="${not empty costing.sentWb}">
							<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.sentWb"/></s:text>
						    <s:textfield id="sentWb" name="costing.sentWb" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.sentWb}">
							<s:textfield id="sentWb" name="costing.sentWb" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<td><img id="sentWb_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                
			            <td align="right"><fmt:message key="costing.doNotSent"/></td>
			            <td align="left">
						<c:if test="${not empty costing.doNotSent}">
							<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.doNotSent"/></s:text>
						    <s:textfield id="doNotSent" name="costing.doNotSent" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.doNotSent}">
							<s:textfield id="doNotSent" name="costing.doNotSent" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<td><img id="doNotSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                
	               </tr>
	            </tbody>
			 </table>
		 	</td>
		 	
		 	<td valign="top" width="1px"><img height="85px" width="2px" src="<c:url value='/images/vertline.jpg'/>"/></td>
		 	<td align="left" class="listwhitetext" width="50%" valign="top">
				<table class="detailTabLabel" border="0">
				  <tbody>  	
				  <tr><td align="left" height="5px"></td></tr>
					<tr>
						<td align="right" width="60px"><fmt:message key="costing.expGl"/></td>
						<td align="left"><s:textfield name="costing.expGl" required="true" readonly="true" cssClass="input-text" size="7"/></td>
						<td align="right"><fmt:message key="costing.postDate"/></td>
							<c:if test="${not empty costing.expPostDate}">
								<td align="left"><s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.expPostDate"/></s:text>
							    <s:textfield name="costing.expPostDate" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
							</c:if>
							<c:if test="${empty costing.expPostDate}">
								<td align="left"><s:textfield name="costing.expPostDate" readonly="true" required="true" cssClass="input-text" size="7"/></td>
							</c:if>
                	</tr>
   		    		<tr>
   		    			<td align="right" width="60px"><fmt:message key="costing.sentAccount"/></td>
   		    			
		   		    	<td align="left"><s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.expSentAccount"/></s:text>
                		<s:textfield name="costing.expSentAccount" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
                		
                		<td align="right"><fmt:message key="costing.sentFile"/></td>
                		<td align="left"><s:textfield name="costing.expSentFile" onkeydown="return onlyCharsAllowed(event)" readonly="true" required="true" cssClass="input-text" maxlength="20" size="7"/></td>
                	</tr>
				 </table>
		 	</td>
		 	</tr>
		 	</tbody>
		 	</table></div></td>
 		  <td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr>  
 	
 	
 	
 	
	<tr><td height="10" align="left" colspan="3" class="listwhitetext">
		<div class="subcontent-tab"><a href="javascript:void(0)"
	class="dsphead" onclick="dsp(this)">Storage&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
							class="dspchar2"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span></a></div>
  
  		<div class="dspcont" id="Layer6">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" class="listwhitetext" width="50%" valign="top">
				<table class="detailTabLabel" border="0">
				  <tbody>
				<tr><td align="left" height="5px"></td></tr>
				<tr><td align="right" width="50px"><fmt:message key="costing.storageBegin"/></td>
		
				<td align="left">
						<c:if test="${not empty costing.storageBegin}">
							<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.storageBegin"/></s:text>
						    <s:textfield id="storageBegin" name="costing.storageBegin" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.storageBegin}">
							<s:textfield id="storageBegin" name="costing.storageBegin" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<td><img id="storageBegin_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                
                <td align="right"><fmt:message key="costing.storageEnd"/></td>
				<td align="left">
						<c:if test="${not empty costing.storageEnd}">
							<s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="costing.storageEnd"/></s:text>
						    <s:textfield id="storageEnd" name="costing.storageEnd" value="%{FormatedInvoiceDate}" readonly="true"  required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<c:if test="${empty costing.storageEnd}">
							<s:textfield id="storageEnd" name="costing.storageEnd" readonly="true" required="true" cssClass="input-text" size="7"/></td>
						</c:if>
						<td><img id="storageEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
                
                
                <td align="left"><s:textfield name="costing.days" required="true" cssClass="input-text"  cssStyle="width:10px" maxlength="1"/></td><td align="left"><fmt:message key="costing.days"/></td>
             </tr>
             <tr>
             <td align="right"><fmt:message key="costing.warehouse"/></td>
             <td align="left" colspan="7"><s:select name="costing.warehouse" list="%{house}" headerKey=""  headerValue="" cssStyle="width:240px" cssClass="list-menu" /></td>
             </tr>
             <tr>
             	<td align="right"><fmt:message key="costing.commodity"/></td>
                <td align="left" colspan="7"><s:select name="costing.commodity" list="%{commodit}" cssStyle="width:240px" cssClass="list-menu"  headerKey="" headerValue=""/></td>
             </tr>
             
             
	            </tbody>
			 </table>
		 	</td>
		 	
		 	<td valign="top" width="1px"><img height="90px" width="2px" src="<c:url value='/images/vertline.jpg'/>"/></td>
		 	<td align="left" class="listwhitetext" width="50%" valign="top">
				<table class="detailTabLabel" border="0">
				  	<tbody>  	

  					</tbody>
				 </table>
		 	</td>
		 	</tr>
		 	</tbody>
		 	</table></div></td>
 		  <td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 
 		  
 		  <tr><td align="left" valign="top" height="5px" class="subcontent-tab" width="650px" colspan="17"></td></tr>
 	
 	
		<tr><td colspan="3">
 		  <table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='costing.createdOn'/></td>
						<s:hidden name="costing.createdOn" />
						<td><s:date name="costing.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='costing.createdBy' /></td>
						
						<td>
						<c:if test="${not empty costing.id}">
							<s:label name="createdBy" value="%{costing.createdBy}"/>
							<s:hidden name="costing.createdBy"/>
						</c:if>
						<c:if test="${empty costing.id}">
							<s:label name="createdBy" value="${pageContext.request.remoteUser}"/>
							<s:hidden name="costing.createdBy" value="${pageContext.request.remoteUser}" />
						</c:if>
						</td>
						
						<td align="right" class="listwhitetext"><fmt:message key='costing.updatedOn'/></td>
						<s:hidden name="costing.updatedOn"/>
						<td><s:date name="costing.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='costing.updatedBy' /></td>
						<s:hidden name="costing.updatedBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="costing.updatedBy"/></td>
					</tr>
				</tbody>
			</table></td></tr>

</tbody></table>
          <s:submit cssClass="cssbuttonA" cssStyle="width:55px; height:25px" method="save" key="button.save"/>   
        
        <input type="button" class="cssbutton1" style="width:55px; height:25px" value="<fmt:message key='button.add'/>" onclick="location.href='<c:url value="/editCosting.html?sid=${ServiceOrderID}"/>'" />
   <td></td>
    <s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="button.reset"/>

<s:hidden name="secondDescription"/>
<s:hidden name="thirdDescription"/>
<s:hidden name="fourthDescription"/>
<s:hidden name="fifthDescription"/>
<s:hidden name="sixthDescription"/>
<s:hidden name="costing.corpID"/>
</s:form>   

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script type="text/javascript">
<!--
function assignEstDate()
	{
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['costingForm'].elements['costing.estimateDate'].value=tempdate;
		
	}
  function assignRevDate()
		{
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['costingForm'].elements['costing.estimateRevisedDate'].value=tempdate;
	}
	
  function assignEntldDate()
		{
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['costingForm'].elements['costing.entitledDate'].value=tempdate;
	}

// for Expenses

 function assignExpRevDate()
		{
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['costingForm'].elements['costing.expEstimateDate'].value=tempdate;
	}
	
  function assignExpEntldDate()
		{
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['costingForm'].elements['costing.expRevisedDate'].value=tempdate;
	}
function assignDefaultValue()
	{
		document.forms['costingForm'].elements['costing.per'].value=100;
		document.forms['costingForm'].elements['costing.perItem'].value='cwt.';
	}
//-->
</script>


<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==110); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>
<script language="javascript" type="text/javascript">
function checkDate() {
/*
var i;
for(i=0;i<=70;i++)
{
	
	if(document.forms['costingForm'].elements[i].value == "null" ){
		document.forms['costingForm'].elements[i].value="";
	}
}
*/
}
 function checkData()
  {
	 var f = document.getElementById('costingForm');
   		 f.setAttribute("autocomplete", "off");
  }
</script>

<script type="text/javascript">
	function autoFilter_PartnerType(targetElement) {
		var partnerType=targetElement.options[targetElement.selectedIndex].value;
		//alert(partnerType);
		document.forms['costingForm'].submit();
		//targetElement.form.elements['customerFile.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
	}
</script>
 <script type="text/javascript" language="javascript">
    
     function actualCal()
     {
     	if(!(document.forms['costingForm'].elements['costing.quantity'].value > 0))
 	 			{
 	 				alert("Please enter quantity");
 	 				document.forms['costingForm'].elements['costing.quantity'].focus();
 	 				
 	 			}
 	 		else if(!(document.forms['costingForm'].elements['costing.rate'].value > 0))
 	 			{
 	 				alert("Please enter price");
 	 				document.forms['costingForm'].elements['costing.rate'].focus();
 	 				
 	 			}
			else if((document.forms['costingForm'].elements['costing.discount'].value > 100) || (document.forms['costingForm'].elements['costing.discount'].value < 0) )
 	 			{
 	 				alert("Please enter discount b/w 0 to 100");
 	 				document.forms['costingForm'].elements['costing.discount'].focus();
 	 				
 	 			}
 	 		else
 	 		 { 
		 	 		var temp;
		 	 		tempAmount=document.forms['costingForm'].elements['costing.quantity'].value*document.forms['costingForm'].elements['costing.rate'].value*(100-document.forms [ 'costingForm'].elements['costing.discount'].value)/100;
		 	 		//alert(tempAmount);
		 	 		document.forms['costingForm'].elements['costing.amount'].value=tempAmount;
	    			return true;
			 }
    	
     }
    function  estimateCal ()
     {
     
     	     	if(!(document.forms['costingForm'].elements['costing.quantity'].value > 0))
 	 			{
 	 				alert("Please enter quantity");
 	 				document.forms['costingForm'].elements['costing.quantity'].focus();
 	 				
 	 			}
 	 		else if(!(document.forms['costingForm'].elements['costing.rate'].value > 0))
 	 			{
 	 				alert("Please enter price");
 	 				document.forms['costingForm'].elements['costing.rate'].focus();
 	 				
 	 			}
 	 		else
 	 		 { 
		 	 			    document.forms['costingForm'].elements['costing.estimate'].value=document.forms[ 'costingForm'].elements['costing.quantity'].value*document.forms[ 'costingForm'] .elements['costing.rate'].value;
	    					return true;
			 }

     }
     
    function entitledCal()
     {
     
           	if(!(document.forms['costingForm'].elements['costing.quantity'].value > 0))
 	 			{
 	 				alert("Please enter quantity");
 	 				document.forms['costingForm'].elements['costing.quantity'].focus();
 	 				
 	 			}
 	 		else if(!(document.forms['costingForm'].elements['costing.rate'].value > 0))
 	 			{
 	 				alert("Please enter price");
 	 				document.forms['costingForm'].elements['costing.rate'].focus();
 	 				
 	 			}
 	 		else
 	 		 { 
	   			 	document.forms['costingForm'].elements['costing.entitledAmount'].value=document.forms[ 'costingForm' ].elements['costing.quantity'].value*document.forms[ 'costingForm'] .elements['costing.rate'].value;
	    			return true;
			 }

     }
     
     
     
   function calcFunction()
 	 	{
 	 		if(!(document.forms['costingForm'].elements['costing.quantity'].value > 0))
 	 			{
 	 				alert("Please enter quantity");
 	 				document.forms['costingForm'].elements['costing.quantity'].focus();
 	 				
 	 			}

 	 		else if(!(document.forms['costingForm'].elements['costing.rate'].value > 0))
 	 			{
 	 				alert("Please enter price");
 	 				document.forms['costingForm'].elements['costing.rate'].focus();
 	 				
 	 			}
		   
 	 		else if((document.forms['costingForm'].elements['costing.discount'].value > 100) || (document.forms['costingForm'].elements['costing.discount'].value < 0) )
 	 			{
 	 				alert("Please enter discount b/w 0 to 100");
 	 				document.forms['costingForm'].elements['costing.discount'].focus();
 	 				
 	 			}
 	 	    else if(!(document.forms['costingForm'].elements['costing.per'].value > 0))
 	 			{
 	 				alert("Please enter Value for / or X");
 	 				document.forms['costingForm'].elements['costing.per'].focus();
 	 				
 	 			}
 	 		
 	 		else
 	 		 { 
		 	 		var d =((document.forms['costingForm'].elements['costing.quantity'].value*document.forms[ 'costingForm'].elements['costing.rate'].value)*(100-document.forms[ 'costingForm'].elements['costing.discount'].value))/100;
		 	 		var tempAmount;
		 	 		//document.forms['costingForm'].elements['costing.amount'].value= d/document.forms['costingForm'].elements['costing.per'].value;
		 			if(document.forms['costingForm'].elements['costing.divMult'].value=='Divison')
		 	 			{ 
		 					
		 	 				tempAmount=d/document.forms['costingForm'].elements['costing.per'].value;
		 	 				//alert(tempAmount);
		 	 				if(tempAmount < 999999999999999)
		 	 				{
		 					  document.forms['costingForm'].elements['costing.amount'].value=tempAmount;
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['costingForm'].elements['costing.amount'].value=0;
		 					  document.forms['costingForm'].elements['costing.amount'].focus();
		 					}
		 				}
				    else
						{
						
						tempAmount=d*document.forms['costingForm'].elements['costing.per'].value;
						if(tempAmount < 999999999999999)
		 	 				{
		 	 				document.forms ['costingForm'].elements['costing.amount'].value=tempAmount;
		 					}else
		 					{
		 					  alert("Please provide valid data");
		 					  document.forms['costingForm'].elements['costing.amount'].value=0;
		 					  document.forms['costingForm'].elements['costing.amount'].focus();
		 					}
							
						}
						
			 }
 		}
     
 </script>
 
 
 <script type="text/javascript">
<!--
function expenseCalc()
  {
  
  	tempAmount=document.costingForm.elements['costing.expExchangeRate'].value*document.costingForm.elements['costing.expAmountFx'].value;
	if(tempAmount < 999999999999999)
	 {
		document.costingForm.elements['costing.expTotalInvoice'].value=tempAmount;
	}else
	 {
	   	alert("Please provide valid data");
		document.forms['costingForm'].elements['costing.expTotalInvoice'].value=0;
		document.forms['costingForm'].elements['costing.expTotalInvoice'].focus();
	}
  	
    return false;
  }
 //-->
</script> 
<script type="text/javascript">   
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
    Form.focusFirstElement($("costingForm"));   
</script>  
