<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Address:</b></td>
	<td align="right"  style="width:88px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<c:if test="${not empty billingAddress1}">
${billingAddress1}
</c:if>
<c:if test="${not empty billingAddress2}">
${billingAddress2}
</c:if>
<c:if test="${not empty billingAddress3}">
${billingAddress3}
</c:if>
<c:if test="${not empty billingAddress4}">
${billingAddress4}
</c:if>
<c:if test="${not empty billingCity}">
${billingCity}
</c:if>
<c:if test="${not empty billingState}">
${billingState}
</c:if>
<c:if test="${not empty billingZip}">
${billingZip}
</c:if>
<c:if test="${not empty billingCountry}">
<br>${billingCountry}
</c:if>
<c:if test="${not empty billingPhone}">
<br><b>Phone:</b>${billingPhone}
</c:if>
<c:if test="${not empty billingTelex}">
<br><b>Telex:</b>${billingTelex}
</c:if>
<c:if test="${not empty billingEmail}">
<br><b>E-mail:</b>${billingEmail}
</c:if>
<c:if test="${not empty billingFax}">
<br><b>Fax:</b>${billingFax}
</c:if>

 <c:if test="${flagForPartnerAddress}">
 <c:if test="${not empty vatBillingGroup}">
<br><b>Vat Billing Group:</b>${vatBillingGroup}</c:if>
<c:if test="${not empty creditTerms}">
<br><b>Credit terms:</b>${creditTerms}
</c:if>
<c:if test="${not empty paymentMethod}">
<br><b>Payments method:</b>${paymentMethod}
</c:if>
</c:if> 
<c:if test="${not empty extReference}">
<br><b>ExtReference:</b>${extReference}
</c:if>
<c:if test="${not empty agentParent}">
<br><b>AgentParentCode:</b>${agentParent}
</c:if>
<c:if test="${not empty agentParentName}">
<br><b>AgentParentName:</b>${agentParentName}
</c:if>
<c:if test="${not empty vatNumber}">
<br><b>VAT #:</b>${vatNumber}
</c:if> 
