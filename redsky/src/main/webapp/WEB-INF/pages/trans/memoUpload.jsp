<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title><fmt:message key="integrationLogList.title"/></title>
    <meta name="heading" content="<fmt:message key='integrationLogList.heading'/>"/>
   
   <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
   </script>
   <!-- Modification closed here -->
    
    <script language="javascript" type="text/javascript">
		function clear_fields(){
			var i;
					for(i=0;i<=4;i++)
					{
						document.forms['memoUplaodForm'].elements['memoUplod.so'].value = "";
						document.forms['memoUplaodForm'].elements['memoUplod.noteID'].value = "";
						document.forms['memoUplaodForm'].elements['memoUplod.uploadStatus'].value = "";
						document.forms['memoUplaodForm'].elements['memoUplod.uploadDate'].value = "";
						//document.forms['integrationLogForm'].elements['integrationLog.transactionID'].value = "";
						//document.forms['integrationLogForm'].elements[i].value = "";
					}
		}
function findToolTipNoteForNotes(id,position){		
			var url="findToolTipNote.html?ajax=1&decorator=simple&popup=true&id="+id;
			ajax_showTooltip(url,position);	
			return false;
			}
		
</script>
   
   <style> 
   span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
!margin-bottom:2px;
margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
   
   </style>
</head>


<s:form id="memoUplaodForm" action="searchMemoUpload" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<!-- <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div> -->
		<div class="spnblk">&nbsp;</div>
<%-- <table class="table" width="100%" >
<thead>
<tr>

<th>SO#</th>
<th>Note Id</th>
<th>Upload Status</th>
<th>Upload Date</th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td><s:textfield name="memoUplod.so" required="true" size="30" cssClass="text medium" onkeypress="return checkIt(event)" onfocus="onFormLoad();" /></td>
			<!--<td><s:textfield name="integrationLog.processedOn"  required="true" cssClass="text medium"/></td>-->
			<td><s:textfield name="memoUplod.note" size="30" required="true" cssClass="text medium" /></td>
			<td><s:textfield name="memoUplod.uploadStatus" size="30" required="true" cssClass="text medium" /></td>
			<c:if test="${not empty memoUplod.uploadDate}">
				<s:text id="integrationLogprocessedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="memoUplod.uploadDate" /></s:text>
				<td align="left"><s:textfield cssClass="input-text" id="processedOn" name="memoUplod.uploadDate" value="%{integrationLogprocessedOnFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/><img id="processedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty memoUplod.uploadDate}">
				<td align="left"><s:textfield cssClass="input-text" id="processedOn" name="memoUplod.uploadDate" required="true" cssStyle="width:60px" maxlength="11" readonly="true"   onkeydown="return onlyDel(event,this)" /><img id="processedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>		
			</tr>
			<tr>
			<td colspan="4"></td>
			<td width="" style="border-left: hidden;">
			    <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px" key="button.search"  />
			    <input type="button" class="cssbutton" value="Clear" style="width:70px; height:25px" onclick="clear_fields();"/>   
			</td>
		</tr>
		</tbody>
	</table> --%>

		<div id="newmnav">
		  <ul>
		  	<li><a href="weeklyXML.html"><span>Weekly XML Summary</span></a></li>
		  	<li><a href="integrationLogs.html"><span>Integration Log List</span></a></li>
		    <li  id="newmnav1" style="background:#FFF "><a class="current"><span>Memo Upload List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div>
		<div class="spn" style="line-height:0px;!margin-bottom:3px;">&nbsp;</div>		
<s:set name="memoUploadLogs" value="memoUploadLogs" scope="request"/>
<display:table name="memoUploadLogs" class="table" requestURI="" id="memoUploadList" export="false" pagesize="50" defaultsort="5" defaultorder="descending" style="width:60% ;">
    <display:column property="so" sortable="true"  style="width:10px; text-align:left;" title="SO#"/>
    <display:column  sortable="true" title="Memo" style="width:15px; text-align:left;">
    <c:if test="${memoUploadList.note != ''}" >
  <c:if test="${fn:length(memoUploadList.note) > 20}" >
  <c:set var="abc" value="${fn:substring(memoUploadList.note, 0, 20)}" />
     <div align="left" onmouseover="findToolTipNoteForNotes('${memoUploadList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
  </c:if>
  <c:if test="${fn:length(memoUploadList.note) <= 20}" >
  <c:set var="abc" value="${fn:substring(memoUploadList.note, 0, 20)}" />
     <div align="left" ><c:out value="${abc}" /></div>
  </c:if>
  </c:if>
    </display:column>
    <display:column property="uploadStatus" sortable="true" title="Upload&nbsp;Status"  style="width:10px; text-align:left;"/> 	
    <display:column property="uploadDate" sortable="true" title="Upload&nbsp;Date" style="width:10px; text-align:left;"/>    	
    
</display:table>
</s:form>
<script type="text/javascript">
    //highlightTableRows("integrationLogList");
    //Form.focusFirstElement($("integrationLogForm")); 
   	
   	RANGE_CAL_1 = new Calendar({
           inputField: "processedOn",
           dateFormat: "%d-%b-%y",
           trigger: "processedOn_trigger",
           bottomBar: true,
           animation:true,
           onSelect: function() {                             
               this.hide();
       }
        
   });
  
</script>