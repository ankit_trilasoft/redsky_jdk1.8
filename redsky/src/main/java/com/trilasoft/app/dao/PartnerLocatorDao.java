package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PartnerLocation;


public interface PartnerLocatorDao  extends GenericDao<PartnerLocation, Long>
{

	String locatePartners(double inputLatitute, double inputLongitude, String countryCode, int withinMiles);

	String locateAgents(double weight, double latitude, double longitude,
			String tariffApplicability, String countryCode, String packingMode, String poe);   
 
}