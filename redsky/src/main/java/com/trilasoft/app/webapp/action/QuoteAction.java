package com.trilasoft.app.webapp.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.CustomerInformation;
import com.trilasoft.app.model.Quote;
import com.trilasoft.app.model.QuoteItem;
import com.trilasoft.app.model.QuoteRate;
import com.trilasoft.app.model.SecorQuote;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.QuoteManager;
import com.trilasoft.app.service.RefMasterManager;

public class QuoteAction extends BaseAction {   

	private QuoteManager quoteManager;
    private List quotes;   
    private Quote quote;   
    private Long id;  
    private CustomerFile customerFile;
    private CustomerFileManager customerFileManager;
	private List customerFiles;
	private List quoteDetailslist;
	private String firstName;
	private String lastName;
	private Date createdOn;
	private String originCity;
	private String originCountry;
	private String destinationCity;
	private String destinationCountry;
	private String sessionCorpID;
	private Map<String, String> ocountry;
	private Map<String, String> dcountry;
    private RefMasterManager refMasterManager;
    private Long quoteId;
    private List quoteItemList;
    private SecorQuote secorQuote;
    private CustomerInformation customerInformation;
    private QuoteRate quoteRate;
    private QuoteItem quoteItem;
    private Double totalCount=0.0;
	private Double totalVolume=0.0;
	private Double totalActualWeight=0.0;
	private Double totalWeight=0.0;
	
	public QuoteAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	
    public void setQuoteManager(QuoteManager quoteManager) {
        this.quoteManager = quoteManager;
    }
	
    public void setCustomerFileManager(CustomerFileManager customerFileManager) {
        this.customerFileManager = customerFileManager;
    }
 
    public List getQuotes() {   
        return quotes;   
    } 
    
    public void setId(Long id) {   
        this.id = id;   
    }   
 
    public List getCustomerFiles() {   
        return customerFiles;   
    } 
    
    public CustomerFile getCustomerFile() {   
        return customerFile;   
    }   
      
    public void setCustomerFile(CustomerFile customerFile) {   
        this.customerFile = customerFile;   
    }   
    
    public Quote getQuote() {   
        return quote;   
    }   
      
    public void setQuote(Quote quote) {   
        this.quote = quote;   
    }   
    
    
    public String delete() {   
        quoteManager.remove(quote.getId());   
        saveMessage(getText("quote.deleted"));   
      
        return listQuotes();   
    }   
    public String edit() {   
        if (id != null) {   
            quote = quoteManager.get(id);
            customerFile = quote.getCustomerFile();
        } else {   
            quote = new Quote();   
            
        }   
      
        return SUCCESS;   
    }   
      
    public String save() throws Exception {   

    	quote.setCustomerFile(customerFile);
        boolean isNew = (quote.getId() == null);  
        quoteManager.save(quote);   
      
        String key = (isNew) ? "quote.added" : "quote.updated";   
        saveMessage(getText(key));   
        if (!isNew) {   
            return INPUT;   
        } else {   
            return SUCCESS;
        } 
    }  
    
    public String listQuotes() {  
    	quotes = quoteManager.getAll();
    	return SUCCESS;   
    }
    
    public String quoteDetailsList(){  
    	ocountry = new TreeMap();
		ocountry.put("USA", "United States");
	 	dcountry = refMasterManager.findCountryForPartner(sessionCorpID, "COUNTRY");
    	return SUCCESS; 
    }
    
    public String quoteDetailSearch(){
    	ocountry = new TreeMap();
		ocountry.put("USA", "United States");
	 	dcountry = refMasterManager.findCountryForPartner(sessionCorpID, "COUNTRY");
	    quoteDetailslist=quoteManager.searchQuoteDetailList(firstName,lastName,createdOn,originCity,originCountry,destinationCity,destinationCountry,sessionCorpID);
	  	return SUCCESS; 
    }
    
    public String editQuoteDetails(){
    	if(quoteId!=null){
    	Double tolTalValueTemp=0.0;
    	Double actualWeight=0.0;
    	secorQuote = quoteManager.findSecorQuoteByQuoteId(sessionCorpID,quoteId).get(0);
    	customerInformation =  quoteManager.findCustomerInformationByQuoteId(sessionCorpID,quoteId).get(0);
    	quoteItemList = quoteManager.findQuoteItemByQuoteId(sessionCorpID,quoteId);
    	quoteRate=new QuoteRate();
    	if(quoteManager.findQuoteRateByQuoteId(sessionCorpID,quoteId)!=null && (!(quoteManager.findQuoteRateByQuoteId(sessionCorpID,quoteId).isEmpty())))
    	   {
    	     quoteRate = quoteManager.findQuoteRateByQuoteId(sessionCorpID,quoteId).get(0);
    	   }
    	Iterator<QuoteItem> it=quoteItemList.iterator();
		while(it.hasNext()){
			QuoteItem quoteItem =it.next();
			totalCount=totalCount+quoteItem.getNoOfItems();
			tolTalValueTemp +=quoteItem.getVolume();
			totalVolume=Math.round(tolTalValueTemp*100.00)/100.00;
			actualWeight=quoteItem.getActualWeight();
			if(actualWeight==null){
				actualWeight=0.0;
			}
			totalActualWeight +=actualWeight;
		}
		totalWeight=Math.round(totalVolume*10.4*100.00)/100.00;
    	}    
	  	return SUCCESS; 
    }

	public List getQuoteDetailslist() {
		return quoteDetailslist;
	}

	public void setQuoteDetailslist(List quoteDetailslist) {
		this.quoteDetailslist = quoteDetailslist;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}


	public String getSessionCorpID() {
		return sessionCorpID;
	}


	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public Map<String, String> getOcountry() {
		return ocountry;
	}


	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}


	public Map<String, String> getDcountry() {
		return dcountry;
	}


	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public Long getQuoteId() {
		return quoteId;
	}


	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}


	public SecorQuote getSecorQuote() {
		return secorQuote;
	}


	public void setSecorQuote(SecorQuote secorQuote) {
		this.secorQuote = secorQuote;
	}


	public QuoteItem getQuoteItem() {
		return quoteItem;
	}


	public void setQuoteItem(QuoteItem quoteItem) {
		this.quoteItem = quoteItem;
	}


	public CustomerInformation getCustomerInformation() {
		return customerInformation;
	}


	public void setCustomerInformation(CustomerInformation customerInformation) {
		this.customerInformation = customerInformation;
	}


	public List getQuoteItemList() {
		return quoteItemList;
	}


	public void setQuoteItemList(List quoteItemList) {
		this.quoteItemList = quoteItemList;
	}


	public Double getTotalCount() {
		return totalCount;
	}


	public void setTotalCount(Double totalCount) {
		this.totalCount = totalCount;
	}


	public Double getTotalVolume() {
		return totalVolume;
	}


	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}


	public Double getTotalActualWeight() {
		return totalActualWeight;
	}


	public void setTotalActualWeight(Double totalActualWeight) {
		this.totalActualWeight = totalActualWeight;
	}


	public Double getTotalWeight() {
		return totalWeight;
	}


	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}


	public QuoteRate getQuoteRate() {
		return quoteRate;
	}


	public void setQuoteRate(QuoteRate quoteRate) {
		this.quoteRate = quoteRate;
	}
    
}  

