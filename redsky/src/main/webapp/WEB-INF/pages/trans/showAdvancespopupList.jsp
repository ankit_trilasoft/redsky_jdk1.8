<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Advance Details</title>   
    <meta name="heading" content="Show Advances"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">
	</script>

<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}
</style>
</head>

</div>

<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr> 
 <td align="left"><b>Advances per Service Order</b></td>
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table> 
<br>
<s:set name="showAdvList" value="showAdvList" scope="request"/>  
<display:table name="showAdvList" class="table" requestURI="" id="showAdvList" defaultsort="2" pagesize="10" style="width:100%;margin-top:-10px;" >   
        <display:column property="description"  title="Description" style="width:65px"/>
        <display:column property="amount"  title="Amount" style="width:65px"/>
        <display:column property="advanceDate" title="Advance&nbsp;Date" format="{0,date,dd-MMM-yyyy}" style="width:65px"/>
</display:table> 
 
</div>


