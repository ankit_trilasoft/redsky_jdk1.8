/**
 * ImfEntitlements Data Access Object (Dao) interface.
 * Gets ImfEntitlements information.
 * @param 			  
 * This interface represents the basic actions on "ImfEntitlements" object in Redsky .
 * @Interface Name	  ImfEntitlementsDao
 * @Author            Ravi Mishra
 * @Version           V01.0
 * @Since             1.0
 * @Date              16-Dec-2008
 */

package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.ImfEntitlements;

public interface ImfEntitlementsDao extends GenericDao<ImfEntitlements,Long>{
	
	public List findById(Long id);
	public List findMaximumId(); 
	public List getImfEntitlements(String sequenceNumber);
		
	public List getImfEstimate();
	public List getImfEstimateList();
	public List getImfEstimateActual();	
	public List getImfEstimateAgent();	 
	public List getImfEstimateDown();
	public void getImfEstimateListUpdate(String sequenceNumber);
	public void getImfEstimateUpdate(String shipNumber);
	public void getImfEstimateActualUpdate(String shipNumber);
	public List getImfEstimateSscar();
	public List getImfEstimateTsto();
	public List getImfEstimateTbkstg();
	public List getImfEstimateSshist();
	public List checkById(String seqNum);
	public List getImfEstimateTstghist();
	public List officeDocMove(String corpID);
	public List docExtract(String corpID);
	public List findDosSCAC(String sessionCorpID);
	public List officeDocMoveNew(String corpID);
 }
