package com.trilasoft.app.service.impl;


import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerLocatorDao;
import com.trilasoft.app.model.PartnerLocation;
import com.trilasoft.app.service.PartnerLocatorManager;


public class PartnerLocatorManagerImpl extends GenericManagerImpl<PartnerLocation, Long> implements PartnerLocatorManager { 
	PartnerLocatorDao partnerLocatorDao; 
    
 
    public PartnerLocatorManagerImpl(PartnerLocatorDao partnerLocatorDao) { 
        super(partnerLocatorDao); 
        this.partnerLocatorDao = partnerLocatorDao; 
    }


	public String locatePartners(double inputLatitute, double inputLongitude, String countryCode, int withinMiles) {
		return partnerLocatorDao.locatePartners(inputLatitute, inputLongitude, countryCode, withinMiles);
	}
	
	public String locateAgents(double weight, double latitude, double longitude, String tariffApplicability, String countryCode, String packingMode, String poe ){
		return partnerLocatorDao.locateAgents(weight, latitude, longitude, tariffApplicability, countryCode, packingMode, poe);
	}



}