package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.OperationsResourceLimitsDao;
import com.trilasoft.app.model.OperationsResourceLimits;

public class OperationsResourceLimitsDaoHibernate extends GenericDaoHibernate<OperationsResourceLimits, Long> implements OperationsResourceLimitsDao{
	
	public OperationsResourceLimitsDaoHibernate() {
		super(OperationsResourceLimits.class);
	}
	
	String workLimitDate="";
	public List isExisted(Date workDate, String hub, String sessionCorpID){
		if (!(workDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
			workLimitDate = nowYYYYMMDD1.toString();
		}
		return this.getSession().createSQLQuery("select * from operationsResourcelimits where workDate = '"+workLimitDate+"' AND hubID = '"+hub+"' AND corpID = '"+sessionCorpID+"'").list();
	}
	
	public List getResourceLimitsByHub(String hub, String category) {
		List list = new ArrayList();
		if(category==null) category="";
		if(category.equalsIgnoreCase("NA") || category.equalsIgnoreCase("")){
			list = getHibernateTemplate().find("from OperationsResourceLimits where hubId = '"+hub+"' ");		
		}else{
			list = getHibernateTemplate().find("from OperationsResourceLimits where hubId = '"+hub+"' and category='"+category+"'");
		}
		return list;
	}
	
	public List getItemListByCategory(String category, String resource, String sessionCorpID){
		if(resource==null){
			resource="";
		}
		category=category.trim();
		return getHibernateTemplate().find("from ItemsJEquip where type ='"+category+"' and descript like '%"+resource+"%' and controlled is true ");
	}
	public List checkResource(String resource, String sessionCorpID){			
		List check = this.getSession().createSQLQuery("select resource from operationsresourcelimits where resource='"+resource+"' AND corpID='" +sessionCorpID+ "'").list(); 
		return check;
	}
}
