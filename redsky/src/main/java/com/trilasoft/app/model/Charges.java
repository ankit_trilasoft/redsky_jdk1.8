package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "charges")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class Charges extends BaseObject implements ListLinkData {
	private Long id;

	private String contract;

	private String corpID;

	private String charge;

	private String description;

	private String wording;

	private String quantityType;

	private BigDecimal quantityPreset= new BigDecimal(0);

	private String quantitySource;

	private String priceType;

	private double pricePre;

	private String priceFormula;

	private String priceSource;

	private String perItem;

	private BigDecimal perValue= new BigDecimal(0);

	private BigDecimal minimum= new BigDecimal(0);

	private String bucket;

	private String estimateActual;

	private String gl;

	private String item;

	private String divideMultiply;

	private String realPrice;

	private String realWords;

	private double minimumWeight;

	private String tariff;

	private String useDiscount;

	private boolean service;

	private String otherCost;

	private String item400n;

	private String subItem;

	private String sortGroup;

	private String systemFunction;

	private String quantity2Type;

	private BigDecimal quantity2preset= new BigDecimal(0);

	private String quantity2source;

	private String item2;

	private String expGl;

	private String quantity1;

	private String price1;

	private String extra1;

	private boolean useOptionalWording;
	
	private Boolean includeLHF;
	//added By Dilip
	private Boolean VATExclude;
	
	private Boolean printOnInvoice;
	// added By Rajesh
	private String quantityItemEstimate;

	private String quantityItemRevised;

	private String quantityEstimate;

	private String quantityRevised;

	private BigDecimal quantityEstimatePreset= new BigDecimal(0); 

	private String quantityEstimateSource;

	private BigDecimal quantityRevisedPreset= new BigDecimal(0);

	private String quantityRevisedSource;

	private String extraItemEstimate;

	private String extraItemRevised;

	private String extraEstimate;

	private String extraRevised;

	private double extraEstimatePreset;

	private String extraEstimateSource;

	private double extraRevisedPreset;

	private String extraRevisedSource;

	private Date createdOn;

	private String createdBy;

	private Date updatedOn;

	private String updatedBy;

	private Boolean checkBreakPoints;

	private String basis;

	private String commission;

	private Boolean storageFlag;

	private String storageType;

	// added By Sandeep
	private String frequency;

	private Boolean week1;

	private Boolean week2;

	private Boolean week3;

	private Boolean week4;

	private Boolean week5;
	
	private Boolean week6;

	private Boolean jan;

	private Boolean feb;

	private Boolean mar;

	private Boolean apr;

	private Boolean may;

	private Boolean jun;

	private Boolean jul;

	private Boolean aug;

	private Boolean sep;

	private Boolean oct;

	private Boolean nov;
	
	private Boolean decem;
	
	private String deductionType;
	
	private String workDay;
	
    private String originCountry;
	
	private String destinationCountry;
	
	private String mode;
	
	private String twoDGridUnit;
	private String multquantity;

	private String costElement;
	private String scostElementDescription;
	private String deviation;
	
	private String buyDependSell;
	private String 	contractCurrency;
	private String 	payableContractCurrency;
	private BigDecimal payablePreset= new BigDecimal(0);
	private Boolean commissionable;
	private Boolean grossMargin;
	
	private String payablePriceType;
	private String expensePrice;
	private Boolean countryFlexibility;
	private String earnout;
	private Boolean rollUpInInvoice;
	private Boolean status=true;
	private Boolean excludeSalesAdditionalCommission=new Boolean(true);
	private String servicedetail;
	private Boolean compensation;
	private BigDecimal incrementalStep= new BigDecimal(0);
	private String GSTHSNCode;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id) 
				.append("payablePriceType",payablePriceType).append("expensePrice",expensePrice)
				.append("contract", contract).append("corpID", corpID)
				.append("charge", charge).append("description", description)
				.append("wording", wording)
				.append("quantityType", quantityType)
				.append("quantityPreset", quantityPreset)
				.append("quantitySource", quantitySource)
				.append("priceType", priceType).append("pricePre", pricePre)
				.append("priceFormula", priceFormula)
				.append("priceSource", priceSource).append("perItem", perItem)
				.append("perValue", perValue).append("minimum", minimum)
				.append("bucket", bucket)
				.append("estimateActual", estimateActual).append("gl", gl)
				.append("item", item).append("divideMultiply", divideMultiply)
				.append("realPrice", realPrice).append("realWords", realWords)
				.append("minimumWeight", minimumWeight)
				.append("tariff", tariff).append("useDiscount", useDiscount)
				.append("service", service).append("otherCost", otherCost)
				.append("item400n", item400n).append("subItem", subItem)
				.append("sortGroup", sortGroup)
				.append("systemFunction", systemFunction)
				.append("quantity2Type", quantity2Type)
				.append("quantity2preset", quantity2preset)
				.append("quantity2source", quantity2source)
				.append("item2", item2).append("expGl", expGl)
				.append("quantity1", quantity1).append("price1", price1)
				.append("extra1", extra1)
				.append("useOptionalWording", useOptionalWording)
				.append("includeLHF", includeLHF)
				.append("VATExclude", VATExclude)
				.append("printOnInvoice", printOnInvoice)
				.append("quantityItemEstimate", quantityItemEstimate)
				.append("quantityItemRevised", quantityItemRevised)
				.append("quantityEstimate", quantityEstimate)
				.append("quantityRevised", quantityRevised)
				.append("quantityEstimatePreset", quantityEstimatePreset)
				.append("quantityEstimateSource", quantityEstimateSource)
				.append("quantityRevisedPreset", quantityRevisedPreset)
				.append("quantityRevisedSource", quantityRevisedSource)
				.append("extraItemEstimate", extraItemEstimate)
				.append("extraItemRevised", extraItemRevised)
				.append("extraEstimate", extraEstimate)
				.append("extraRevised", extraRevised)
				.append("extraEstimatePreset", extraEstimatePreset)
				.append("extraEstimateSource", extraEstimateSource)
				.append("extraRevisedPreset", extraRevisedPreset)
				.append("extraRevisedSource", extraRevisedSource)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("checkBreakPoints", checkBreakPoints)
				.append("basis", basis).append("commission", commission)
				.append("storageFlag", storageFlag)
				.append("storageType", storageType)
				.append("frequency", frequency).append("week1", week1)
				.append("week2", week2).append("week3", week3)
				.append("week4", week4).append("week5", week5)
				.append("week6", week6).append("jan", jan).append("feb", feb)
				.append("mar", mar).append("apr", apr).append("may", may)
				.append("jun", jun).append("jul", jul).append("aug", aug)
				.append("sep", sep).append("oct", oct).append("nov", nov)
				.append("decem", decem).append("deductionType", deductionType)
				.append("workDay", workDay)
				.append("originCountry", originCountry)
				.append("destinationCountry", destinationCountry)
				.append("mode", mode).append("twoDGridUnit", twoDGridUnit)
				.append("multquantity", multquantity)
				.append("costElement", costElement)
				.append("scostElementDescription", scostElementDescription)
				.append("deviation", deviation)
				.append("buyDependSell", buyDependSell)
				.append("contractCurrency", contractCurrency)
				.append("payableContractCurrency", payableContractCurrency)
				.append("payablePreset", payablePreset)
				.append("commissionable", commissionable).append("grossMargin",grossMargin)
				.append("countryFlexibility",countryFlexibility).append("earnout", earnout)
				.append("rollUpInInvoice", rollUpInInvoice)
				.append("status", status).append("excludeSalesAdditionalCommission",excludeSalesAdditionalCommission)
				.append("servicedetail",servicedetail)
				.append("compensation",compensation)
				.append("incrementalStep",incrementalStep)
				.append("GSTHSNCode",GSTHSNCode)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Charges))
			return false;
		Charges castOther = (Charges) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(contract, castOther.contract)
				.append(corpID, castOther.corpID)
				.append(charge, castOther.charge)
				.append(description, castOther.description)  
				.append(payablePriceType, castOther.payablePriceType).append(expensePrice,castOther.expensePrice)
				.append(wording, castOther.wording)
				.append(quantityType, castOther.quantityType)
				.append(quantityPreset, castOther.quantityPreset)
				.append(quantitySource, castOther.quantitySource)
				.append(priceType, castOther.priceType)
				.append(pricePre, castOther.pricePre)
				.append(priceFormula, castOther.priceFormula)
				.append(priceSource, castOther.priceSource)
				.append(perItem, castOther.perItem)
				.append(perValue, castOther.perValue)
				.append(minimum, castOther.minimum)
				.append(bucket, castOther.bucket)
				.append(estimateActual, castOther.estimateActual)
				.append(gl, castOther.gl)
				.append(item, castOther.item)
				.append(divideMultiply, castOther.divideMultiply)
				.append(realPrice, castOther.realPrice)
				.append(realWords, castOther.realWords)
				.append(minimumWeight, castOther.minimumWeight)
				.append(tariff, castOther.tariff)
				.append(useDiscount, castOther.useDiscount)
				.append(service, castOther.service)
				.append(otherCost, castOther.otherCost)
				.append(item400n, castOther.item400n)
				.append(subItem, castOther.subItem)
				.append(sortGroup, castOther.sortGroup)
				.append(systemFunction, castOther.systemFunction)
				.append(quantity2Type, castOther.quantity2Type)
				.append(quantity2preset, castOther.quantity2preset)
				.append(quantity2source, castOther.quantity2source)
				.append(item2, castOther.item2)
				.append(expGl, castOther.expGl)
				.append(quantity1, castOther.quantity1)
				.append(price1, castOther.price1)
				.append(extra1, castOther.extra1)
				.append(useOptionalWording, castOther.useOptionalWording)
				.append(includeLHF, castOther.includeLHF)
				.append(VATExclude, castOther.VATExclude)
				.append(printOnInvoice, castOther.printOnInvoice)
				.append(quantityItemEstimate, castOther.quantityItemEstimate)
				.append(quantityItemRevised, castOther.quantityItemRevised)
				.append(quantityEstimate, castOther.quantityEstimate)
				.append(quantityRevised, castOther.quantityRevised)
				.append(quantityEstimatePreset,
						castOther.quantityEstimatePreset)
				.append(quantityEstimateSource,
						castOther.quantityEstimateSource)
				.append(quantityRevisedPreset, castOther.quantityRevisedPreset)
				.append(quantityRevisedSource, castOther.quantityRevisedSource)
				.append(extraItemEstimate, castOther.extraItemEstimate)
				.append(extraItemRevised, castOther.extraItemRevised)
				.append(extraEstimate, castOther.extraEstimate)
				.append(extraRevised, castOther.extraRevised)
				.append(extraEstimatePreset, castOther.extraEstimatePreset)
				.append(extraEstimateSource, castOther.extraEstimateSource)
				.append(extraRevisedPreset, castOther.extraRevisedPreset)
				.append(extraRevisedSource, castOther.extraRevisedSource)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(checkBreakPoints, castOther.checkBreakPoints)
				.append(basis, castOther.basis)
				.append(commission, castOther.commission)
				.append(storageFlag, castOther.storageFlag)
				.append(storageType, castOther.storageType)
				.append(frequency, castOther.frequency)
				.append(week1, castOther.week1)
				.append(week2, castOther.week2)
				.append(week3, castOther.week3)
				.append(week4, castOther.week4)
				.append(week5, castOther.week5)
				.append(week6, castOther.week6)
				.append(jan, castOther.jan)
				.append(feb, castOther.feb)
				.append(mar, castOther.mar)
				.append(apr, castOther.apr)
				.append(may, castOther.may)
				.append(jun, castOther.jun)
				.append(jul, castOther.jul)
				.append(aug, castOther.aug)
				.append(sep, castOther.sep)
				.append(oct, castOther.oct)
				.append(nov, castOther.nov)
				.append(decem, castOther.decem)
				.append(deductionType, castOther.deductionType)
				.append(workDay, castOther.workDay)
				.append(originCountry, castOther.originCountry)
				.append(destinationCountry, castOther.destinationCountry)
				.append(mode, castOther.mode)
				.append(twoDGridUnit, castOther.twoDGridUnit)
				.append(multquantity, castOther.multquantity)
				.append(costElement, castOther.costElement)
				.append(scostElementDescription,
						castOther.scostElementDescription)
				.append(deviation, castOther.deviation)
				.append(buyDependSell, castOther.buyDependSell)
				.append(contractCurrency, castOther.contractCurrency)
				.append(payableContractCurrency,
						castOther.payableContractCurrency)
				.append(payablePreset, castOther.payablePreset)
				.append(commissionable, castOther.commissionable).append(grossMargin,castOther.grossMargin)
				.append(countryFlexibility, castOther.countryFlexibility)
				.append(earnout, castOther.earnout).append(rollUpInInvoice, castOther.rollUpInInvoice)
				.append(status, castOther.status)
				.append(excludeSalesAdditionalCommission, castOther.excludeSalesAdditionalCommission)
				.append(servicedetail,castOther.servicedetail)
				.append(compensation,castOther.compensation)
				.append(incrementalStep,castOther.incrementalStep)
				.append(GSTHSNCode,castOther.GSTHSNCode)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(contract).append(corpID)
				.append(payablePriceType).append(expensePrice)
				.append(charge).append(description).append(wording)
				.append(quantityType).append(quantityPreset)
				.append(quantitySource).append(priceType).append(pricePre)
				.append(priceFormula).append(priceSource).append(perItem)
				.append(perValue).append(minimum).append(bucket)
				.append(estimateActual).append(gl).append(item)
				.append(divideMultiply).append(realPrice).append(realWords)
				.append(minimumWeight).append(tariff).append(useDiscount)
				.append(service).append(otherCost).append(item400n)
				.append(subItem).append(sortGroup).append(systemFunction)
				.append(quantity2Type).append(quantity2preset)
				.append(quantity2source).append(item2).append(expGl)
				.append(quantity1).append(price1).append(extra1)
				.append(useOptionalWording).append(includeLHF)
				.append(VATExclude).append(printOnInvoice)
				.append(quantityItemEstimate).append(quantityItemRevised)
				.append(quantityEstimate).append(quantityRevised)
				.append(quantityEstimatePreset).append(quantityEstimateSource)
				.append(quantityRevisedPreset).append(quantityRevisedSource)
				.append(extraItemEstimate).append(extraItemRevised)
				.append(extraEstimate).append(extraRevised)
				.append(extraEstimatePreset).append(extraEstimateSource)
				.append(extraRevisedPreset).append(extraRevisedSource)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(checkBreakPoints).append(basis)
				.append(commission).append(storageFlag).append(storageType)
				.append(frequency).append(week1).append(week2).append(week3)
				.append(week4).append(week5).append(week6).append(jan)
				.append(feb).append(mar).append(apr).append(may).append(jun)
				.append(jul).append(aug).append(sep).append(oct).append(nov)
				.append(decem).append(deductionType).append(workDay)
				.append(originCountry).append(destinationCountry).append(mode)
				.append(twoDGridUnit).append(multquantity).append(costElement)
				.append(scostElementDescription).append(deviation)
				.append(buyDependSell).append(contractCurrency)
				.append(payableContractCurrency).append(payablePreset)
				.append(commissionable).append(grossMargin)
				.append(countryFlexibility).append(earnout).append(rollUpInInvoice).append(status).append(excludeSalesAdditionalCommission)
				.append(servicedetail)
				.append(compensation)
				.append(incrementalStep)
				.append(GSTHSNCode)
				.toHashCode();
	}

	@Column(length = 7)
	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	@Column(length = 25)
	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	@Column(length = 100)
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 45)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length = 10)
	public String getDivideMultiply() {
		return divideMultiply;
	}

	public void setDivideMultiply(String divideMultiply) {
		this.divideMultiply = divideMultiply;
	}

	@Column(length = 1)
	public String getEstimateActual() {
		return estimateActual;
	}

	public void setEstimateActual(String estimateActual) {
		this.estimateActual = estimateActual;
	}

	@Column(length = 20)
	public String getExpGl() {
		return expGl;
	}

	public void setExpGl(String expGl) {
		this.expGl = expGl;
	}

	@Column(length = 20)
	public String getGl() {
		return gl;
	}

	public void setGl(String gl) {
		this.gl = gl;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 45)
	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@Column(length = 45)
	public String getItem2() {
		return item2;
	}

	public void setItem2(String item2) {
		this.item2 = item2;
	}

	@Column(length = 10)
	public String getItem400n() {
		return item400n;
	}

	public void setItem400n(String item400n) {
		this.item400n = item400n;
	}
	
	@Column(length = 20)
	public double getMinimumWeight() {
		return minimumWeight;
	}

	public void setMinimumWeight(double minimumWeight) {
		this.minimumWeight = minimumWeight;
	}

	@Column(length = 1)
	public String getOtherCost() {
		return otherCost;
	}

	public void setOtherCost(String otherCost) {
		this.otherCost = otherCost;
	}

	@Column(length = 500)
	public String getPerItem() {
		return perItem;
	}

	public void setPerItem(String perItem) {
		this.perItem = perItem;
	}

	
	@Column(length = 500)
	public String getPriceFormula() {
		return priceFormula;
	}

	public void setPriceFormula(String priceFormula) {
		this.priceFormula = priceFormula;
	}

	public double getPricePre() {
		return pricePre;
	}

	public void setPricePre(double pricePre) {
		this.pricePre = pricePre;
	}

	@Column(length = 500)
	public String getPriceSource() {
		return priceSource;
	}

	public void setPriceSource(String priceSource) {
		this.priceSource = priceSource;
	}

	@Column(length = 20)
	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}	

	@Column
	public String getQuantity2source() {
		return quantity2source;
	}

	public void setQuantity2source(String quantity2source) {
		this.quantity2source = quantity2source;
	}

	@Column(length = 20)
	public String getQuantity2Type() {
		return quantity2Type;
	}

	public void setQuantity2Type(String quantity2Type) {
		this.quantity2Type = quantity2Type;
	}

	@Column
	public String getQuantitySource() {
		return quantitySource;
	}

	public void setQuantitySource(String quantitySource) {
		this.quantitySource = quantitySource;
	}

	@Column(length = 20)
	public String getQuantityType() {
		return quantityType;
	}

	public void setQuantityType(String quantityType) {
		this.quantityType = quantityType;
	}

	@Column(length = 500)
	public String getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(String realPrice) {
		this.realPrice = realPrice;
	}

	@Column(length = 500)
	public String getRealWords() {
		return realWords;
	}

	public void setRealWords(String realWords) {
		this.realWords = realWords;
	}

	@Column(length = 2)
	public String getSortGroup() {
		return sortGroup;
	}

	public void setSortGroup(String sortGroup) {
		this.sortGroup = sortGroup;
	}

	@Column(length = 10)
	public String getSubItem() {
		return subItem;
	}

	public void setSubItem(String subItem) {
		this.subItem = subItem;
	}

	@Column(length = 20)
	public String getSystemFunction() {
		return systemFunction;
	}

	public void setSystemFunction(String systemFunction) {
		this.systemFunction = systemFunction;
	}

	@Column(length = 10)
	public String getTariff() {
		return tariff;
	}

	public void setTariff(String tariff) {
		this.tariff = tariff;
	}

	@Column(length = 1)
	public String getUseDiscount() {
		return useDiscount;
	}

	public void setUseDiscount(String useDiscount) {
		this.useDiscount = useDiscount;
	}

	@Column(length = 500)
	public String getWording() {
		return wording;
	}

	public void setWording(String wording) {
		this.wording = wording;
	}

	// For look up Return values

	@Transient
	public String getListCode() {
		return charge; // return the code that needs to be set on the parent
		// page
	}

	@Transient
	public String getListDescription() {
		return this.description; // return the description that needs to be
		// set on the parent page
	}

	@Transient
	public String getListSecondDescription() {
		return this.gl;
	}

	@Transient
	public String getListThirdDescription() {
		return this.expGl;
	}

	@Transient
	public String getListFourthDescription() {
		return this.payableContractCurrency;
	}

	@Transient
	public String getListFifthDescription() {
		if(includeLHF!=null){
		return this.includeLHF.toString();
		}else{
			return this.includeLHF.FALSE.toString();	
		}
	}

	@Transient
	public String getListSixthDescription() {
		if(printOnInvoice!=null){
			return this.printOnInvoice.toString();
			}else{
				return this.printOnInvoice.FALSE.toString();	
			}
	}

	@Column(length = 20)
	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	@Column(length = 20)
	public String getPrice1() {
		return price1;
	}

	public void setPrice1(String price1) {
		this.price1 = price1;
	}

	@Column(length = 20)
	public String getQuantity1() {
		return quantity1;
	}

	public void setQuantity1(String quantity1) {
		this.quantity1 = quantity1;
	}

	@Column(length = 1)
	public boolean isUseOptionalWording() {
		return useOptionalWording;
	}

	public void setUseOptionalWording(boolean useOptionalWording) {
		this.useOptionalWording = useOptionalWording;
	}

	@Column
	public String getExtraEstimate() {
		return extraEstimate;
	}

	public void setExtraEstimate(String extraEstimate) {
		this.extraEstimate = extraEstimate;
	}

	@Column
	public double getExtraEstimatePreset() {
		return extraEstimatePreset;
	}

	public void setExtraEstimatePreset(double extraEstimatePreset) {
		this.extraEstimatePreset = extraEstimatePreset;
	}

	@Column
	public String getExtraEstimateSource() {
		return extraEstimateSource;
	}

	public void setExtraEstimateSource(String extraEstimateSource) {
		this.extraEstimateSource = extraEstimateSource;
	}

	@Column
	public String getExtraItemEstimate() {
		return extraItemEstimate;
	}

	public void setExtraItemEstimate(String extraItemEstimate) {
		this.extraItemEstimate = extraItemEstimate;
	}

	@Column
	public String getExtraItemRevised() {
		return extraItemRevised;
	}

	public void setExtraItemRevised(String extraItemRevised) {
		this.extraItemRevised = extraItemRevised;
	}

	@Column
	public String getExtraRevised() {
		return extraRevised;
	}

	public void setExtraRevised(String extraRevised) {
		this.extraRevised = extraRevised;
	}

	@Column
	public double getExtraRevisedPreset() {
		return extraRevisedPreset;
	}

	public void setExtraRevisedPreset(double extraRevisedPreset) {
		this.extraRevisedPreset = extraRevisedPreset;
	}

	@Column
	public String getExtraRevisedSource() {
		return extraRevisedSource;
	}

	public void setExtraRevisedSource(String extraRevisedSource) {
		this.extraRevisedSource = extraRevisedSource;
	}

	@Column
	public String getQuantityEstimate() {
		return quantityEstimate;
	}

	public void setQuantityEstimate(String quantityEstimate) {
		this.quantityEstimate = quantityEstimate;
	}


	@Column
	public String getQuantityEstimateSource() {
		return quantityEstimateSource;
	}

	public void setQuantityEstimateSource(String quantityEstimateSource) {
		this.quantityEstimateSource = quantityEstimateSource;
	}

	@Column
	public String getQuantityItemEstimate() {
		return quantityItemEstimate;
	}

	public void setQuantityItemEstimate(String quantityItemEstimate) {
		this.quantityItemEstimate = quantityItemEstimate;
	}

	@Column
	public String getQuantityItemRevised() {
		return quantityItemRevised;
	}

	public void setQuantityItemRevised(String quantityItemRevised) {
		this.quantityItemRevised = quantityItemRevised;
	}

	@Column
	public String getQuantityRevised() {
		return quantityRevised;
	}

	public void setQuantityRevised(String quantityRevised) {
		this.quantityRevised = quantityRevised;
	}

	
	@Column
	public String getQuantityRevisedSource() {
		return quantityRevisedSource;
	}

	public void setQuantityRevisedSource(String quantityRevisedSource) {
		this.quantityRevisedSource = quantityRevisedSource;
	}

	public Boolean getCheckBreakPoints() {
		return checkBreakPoints;
	}

	public void setCheckBreakPoints(Boolean checkBreakPoints) {
		this.checkBreakPoints = checkBreakPoints;
	}

	public boolean isService() {
		return service;
	}

	public void setService(boolean service) {
		this.service = service;
	}

	@Transient
	public String getListSeventhDescription() {
		return this.costElement;
	}

	@Column(length = 25)
	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 6)
	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	@Transient
	public String getListEigthDescription() {
		return this.buyDependSell;
	}

	@Transient
	public String getListNinthDescription() {
		if(VATExclude!=null){
			return this.VATExclude.toString();
			}else{
				return this.VATExclude.FALSE.toString();	
			}
	}

	@Transient
	public String getListTenthDescription() {
		return contractCurrency;
	}

	public Boolean getStorageFlag() {
		return storageFlag;
	}

	public void setStorageFlag(Boolean storageFlag) {
		this.storageFlag = storageFlag;
	}

	@Column(length = 25)
	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public Boolean getApr() {
		return apr;
	}

	public void setApr(Boolean apr) {
		this.apr = apr;
	}

	public Boolean getAug() {
		return aug;
	}

	public void setAug(Boolean aug) {
		this.aug = aug;
	}

	public Boolean getFeb() {
		return feb;
	}

	public void setFeb(Boolean feb) {
		this.feb = feb;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Boolean getJan() {
		return jan;
	}

	public void setJan(Boolean jan) {
		this.jan = jan;
	}

	public Boolean getJul() {
		return jul;
	}

	public void setJul(Boolean jul) {
		this.jul = jul;
	}

	public Boolean getJun() {
		return jun;
	}

	public void setJun(Boolean jun) {
		this.jun = jun;
	}

	public Boolean getMar() {
		return mar;
	}

	public void setMar(Boolean mar) {
		this.mar = mar;
	}

	public Boolean getMay() {
		return may;
	}

	public void setMay(Boolean may) {
		this.may = may;
	}

	public Boolean getNov() {
		return nov;
	}

	public void setNov(Boolean nov) {
		this.nov = nov;
	}

	public Boolean getOct() {
		return oct;
	}

	public void setOct(Boolean oct) {
		this.oct = oct;
	}

	public Boolean getSep() {
		return sep;
	}

	public void setSep(Boolean sep) {
		this.sep = sep;
	}

	public Boolean getWeek1() {
		return week1;
	}

	public void setWeek1(Boolean week1) {
		this.week1 = week1;
	}

	public Boolean getWeek2() {
		return week2;
	}

	public void setWeek2(Boolean week2) {
		this.week2 = week2;
	}

	public Boolean getWeek3() {
		return week3;
	}

	public void setWeek3(Boolean week3) {
		this.week3 = week3;
	}

	public Boolean getWeek4() {
		return week4;
	}

	public void setWeek4(Boolean week4) {
		this.week4 = week4;
	}

	public Boolean getWeek5() {
		return week5;
	}

	public void setWeek5(Boolean week5) {
		this.week5 = week5;
	}

	public String getDeductionType() {
		return deductionType;
	}

	public void setDeductionType(String deductionType) {
		this.deductionType = deductionType;
	}

	public Boolean getDecem() {
		return decem;
	}

	public void setDecem(Boolean decem) {
		this.decem = decem;
	}

	public Boolean getWeek6() {
		return week6;
	}

	public void setWeek6(Boolean week6) {
		this.week6 = week6;
	}

	public String getWorkDay() {
		return workDay;
	}

	public void setWorkDay(String workDay) {
		this.workDay = workDay;
	}
	
  
	public Boolean getIncludeLHF() {
		return includeLHF;
	}

	public void setIncludeLHF(Boolean includeLHF) {
		this.includeLHF = includeLHF;
	}
	public Boolean getVATExclude() {
		return VATExclude;
	}

	public void setVATExclude(Boolean vATExclude) {
		VATExclude = vATExclude;
	}
	public Boolean getPrintOnInvoice() {
		return printOnInvoice;
	}

	public void setPrintOnInvoice(Boolean printOnInvoice) {
		this.printOnInvoice = printOnInvoice;
	}
	
	@Column
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	@Column
	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	@Column
	public String getTwoDGridUnit() {
		return twoDGridUnit;
	}

	public void setTwoDGridUnit(String twoDGridUnit) {
		this.twoDGridUnit = twoDGridUnit;
	}
	
	@Column
	public String getMultquantity() {
		return multquantity;
	}

	public void setMultquantity(String multquantity) {
		this.multquantity = multquantity;
	}
	@Column(length = 7)
	public String getCostElement() {
		return costElement;
	}

	public void setCostElement(String costElement) {
		this.costElement = costElement;
	}
	@Column(length = 30)
	public String getScostElementDescription() {
		return scostElementDescription;
	}

	public void setScostElementDescription(String scostElementDescription) {
		this.scostElementDescription = scostElementDescription;
	}
	
	@Column
	public String getDeviation() {
		return deviation;
	}

	public void setDeviation(String deviation) {
		this.deviation = deviation;
	}

	@Column
	public String getBuyDependSell() {
		return buyDependSell;
	}

	public void setBuyDependSell(String buyDependSell) {
		this.buyDependSell = buyDependSell;
	}
	
	@Column
	public String getContractCurrency() {
		return contractCurrency;
	}

	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	@Column
	public String getPayableContractCurrency() {
		return payableContractCurrency;
	}

	public void setPayableContractCurrency(String payableContractCurrency) {
		this.payableContractCurrency = payableContractCurrency;
	}

	@Column
	public Boolean getCommissionable() {
		return commissionable;
	}

	public void setCommissionable(Boolean commissionable) {
		this.commissionable = commissionable;
	}

	@Column(length=20)
	public BigDecimal getQuantity2preset() {
		return quantity2preset;
	}

	public void setQuantity2preset(BigDecimal quantity2preset) {
		this.quantity2preset = quantity2preset;
	}

	@Column(length=20)
	public BigDecimal getQuantityPreset() {
		return quantityPreset;
	}

	public void setQuantityPreset(BigDecimal quantityPreset) {
		this.quantityPreset = quantityPreset;
	}
	
	@Column(length=20)
	public BigDecimal getPerValue() {
		return perValue;
	}

	public void setPerValue(BigDecimal perValue) {
		this.perValue = perValue;
	}
	
	@Column(length=20)
	public BigDecimal getMinimum() {
		return minimum;
	}

	public void setMinimum(BigDecimal minimum) {
		this.minimum = minimum;
	}
	
	@Column(length=20)
	public BigDecimal getQuantityEstimatePreset() {
		return quantityEstimatePreset;
	}

	public void setQuantityEstimatePreset(BigDecimal quantityEstimatePreset) {
		this.quantityEstimatePreset = quantityEstimatePreset;
	}
	
	@Column(length=20)
	public BigDecimal getQuantityRevisedPreset() {
		return quantityRevisedPreset;
	}

	public void setQuantityRevisedPreset(BigDecimal quantityRevisedPreset) {
		this.quantityRevisedPreset = quantityRevisedPreset;
	}
	
	@Column(length=20)
	public BigDecimal getPayablePreset() {
		return payablePreset;
	}

	public void setPayablePreset(BigDecimal payablePreset) {
		this.payablePreset = payablePreset;
	}
	@Column
	public Boolean getGrossMargin() {
		return grossMargin;
	}

	public void setGrossMargin(Boolean grossMargin) {
		this.grossMargin = grossMargin;
	}
	@Column
	public String getPayablePriceType() {
		return payablePriceType;
	}

	public void setPayablePriceType(String payablePriceType) {
		this.payablePriceType = payablePriceType;
	}
	@Column
	public String getExpensePrice() {
		return expensePrice;
	}

	public void setExpensePrice(String expensePrice) {
		this.expensePrice = expensePrice;
	}
	@Column
	public Boolean getCountryFlexibility() {
		return countryFlexibility;
	}

	public void setCountryFlexibility(Boolean countryFlexibility) {
		this.countryFlexibility = countryFlexibility;
	}

	public String getEarnout() {
		return earnout;
	}

	public void setEarnout(String earnout) {
		this.earnout = earnout;
	}

	public Boolean getRollUpInInvoice() {
		return rollUpInInvoice;
	}

	public void setRollUpInInvoice(Boolean rollUpInInvoice) {
		this.rollUpInInvoice = rollUpInInvoice;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Column
	public Boolean getExcludeSalesAdditionalCommission() {
		return excludeSalesAdditionalCommission;
	}

	public void setExcludeSalesAdditionalCommission(
			Boolean excludeSalesAdditionalCommission) {
		this.excludeSalesAdditionalCommission = excludeSalesAdditionalCommission;
	}
	@Column
	public String getServicedetail() {
		return servicedetail;
	}

	public void setServicedetail(String servicedetail) {
		this.servicedetail = servicedetail;
	}

	@Column
	public Boolean getCompensation() {
		return compensation;
	}

	public void setCompensation(Boolean compensation) {
		this.compensation = compensation;
	}

	@Column
	public BigDecimal getIncrementalStep() {
		return incrementalStep;
	}

	public void setIncrementalStep(BigDecimal incrementalStep) {
		this.incrementalStep = incrementalStep;
	}

	@Column
	public String getGSTHSNCode() {
		return GSTHSNCode;
	}

	public void setGSTHSNCode(String gSTHSNCode) {
		GSTHSNCode = gSTHSNCode;
	}

}
