// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`cportalresourcemgmt` MODIFY COLUMN 
`childBillToCode` VARCHAR(1000) CHARACTER SET utf8 COLLATE 
utf8_unicode_ci DEFAULT NULL;

ALTER TABLE serviceorder ADD COLUMN projectedGrossMarginPercentage DECIMAL(19,2) DEFAULT '0.00' AFTER noOfEmailsSent,ADD COLUMN projectedGrossMargin DECIMAL(19,2) DEFAULT '0.00' AFTER projectedGrossMarginPercentage;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `recordToDel` VARCHAR(5) DEFAULT 0 AFTER `billToName`;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `serviceCompleteDate` DATETIME DEFAULT NULL AFTER `VIS_comment`;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `mailStauts` VARCHAR(10) AFTER `recordToDel`;

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `signaturePart` TEXT DEFAULT NULL;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `missedRDDNotification` DATETIME DEFAULT NULL AFTER `podToBooker`, ADD COLUMN `reason` VARCHAR(45) DEFAULT NULL AFTER `missedRDDNotification`;
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','RDDREASON',20,true,now(),'adminsscw',now(),'adminsscw',1,1,'');
 
Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('SSCW', 'ABC', 'ABC', 40, 'RDDREASON','en',now(),'adminsscw',now(),'adminsscw');

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `lastUpdatedLeave` DATETIME DEFAULT NULL AFTER `mssPassword`;
