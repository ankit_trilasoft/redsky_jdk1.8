<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title>Purchase Invoice Extract</title>
<meta name="heading" content="Purchase Invoice Extract" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>

<script language="javascript" type="text/javascript"> 
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript">
function checkCompanyCode()
{  
 var companyCode = document.forms['payableCompanyCodeForm'].elements['payCompanyCode'].value; 
 companyCode=companyCode.trim(); 
 if(companyCode=='')
 {
  document.forms['payableCompanyCodeForm'].elements['Extract'].disabled=true;
  
 }
 
else if(companyCode!='')
{
document.forms['payableCompanyCodeForm'].elements['Extract'].disabled=false;

}
}


function fillReverseInvoice(){ 
       var postDate = valButton(document.forms['payableCompanyCodeForm'].elements['radiobilling']); 
	    if (postDate == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       { 
	        var companyCode = document.forms['payableCompanyCodeForm'].elements['payCompanyCode'].value;  
            companyCode=companyCode.trim(); 
            if(companyCode!='' )
            { 
  	        var agree = confirm("Invoice data for company " +companyCode+" is under processing.");
            if(agree)
             {
	          document.forms['payableCompanyCodeForm'].action = 'payableNavisionExtractVoru.html';
              document.forms['payableCompanyCodeForm'].submit();
             }
             else {
             return false; 
             } 
           }  
      }
      }
      
  function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['payableCompanyCodeForm'].elements['radiobilling'].value; 
    } 
  }
  
 function refreshPostDate()
 {
 document.forms['payableCompanyCodeForm'].action = 'payableNavisionCompanyCodeListVoru.html';
 document.forms['payableCompanyCodeForm'].submit();
 }
 
 function setCompanydiv(companydivision)
  {
  document.forms['payableCompanyCodeForm'].elements['payCompanyCode'].value= companydivision
  checkCompanyCode();
  }
</script>
</head>
<s:form id="payableCompanyCodeForm" name="payableCompanyCodeForm" action="payableNavisionExtract" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<div id="Layer1" style="width:85%;">
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;"><span></span></div>
   <div class="center-content">
<table border="0">
<tr><td width="5px"></td><td class="bgblue" colspan="2">Purchase Invoice Extract </td>
</tr>
 <tr><td height="5"></td></tr>
	<tr><td colspan="3">
	<table border="0" style="margin-bottom: 3px">
	<tr> 
	    <%-- <td width="5px"></td>
		<td align="right"  class="listwhitebox" width="150px">Company Group Code</td> 
		<td align="left" ><s:select list="%{payCompanyCodeList}" cssClass="list-menu"  cssStyle="width:100px" headerKey=" " headerValue=" " id="payCompanyCode"  name="payCompanyCode"   onchange="checkCompanyCode();"/></td>
		--%>
		<td width="5px"></td>
		<s:hidden id="payCompanyCode"  name="payCompanyCode"  />
	    <td align="left"><input type="button"  class="cssbutton1"  value="Refresh Date List for All Companies" name="refresh"  style="width:230px;" onclick="refreshPostDate();" />
		</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td></tr></table>
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>


<table class="detailTabLabel" cellpadding="0" cellspacing="2">
<c:if test="${payablePostDateList!='[]'}"> 
<tr>
	<td align="right" class="listwhitetext"><b>The following posting dates are available, please select one to process</b></td> 
	</tr>
	</c:if>
	<tr>

		<s:set name="payablePostDateList" value="payablePostDateList" scope="request"/> 
        <display:table name="payablePostDateList" class="table" requestURI="" id="payablePostDateList" style="width:100%" defaultsort="1" pagesize="100" >   
 		<display:column property="companyDivision" sortable="true" title="Company Division" style="width:70px"/> 
		<display:column property="payPostDateCount" sortable="true" title="# Payables" maxLength="15"  style="width:120px"/> 
	   <display:column   title="Select" style="width:10px" >
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${payablePostDateList.companyDivision} value=${payablePostDateList.companyDivision} onclick="setCompanydiv('${payablePostDateList.companyDivision}');"/>
 		<s:hidden  id="payCompanyCode1"  name="payCompanyCode1"  value="${payablePostDateList.companyDivision}"/>
 		</display:column>
	     
</display:table> 
</tr>
</table>
		<table><tbody> 
		<tr>
		<td width="5px"></td>
		<td></td> 
        <td align="left"><input type="button"  class="cssbutton1"  value="Extract" name="Extract"  style="width:70px;" onclick="return fillReverseInvoice();" />  
        </td>
        </tr>
        <tr>
        <td height="10"></td></tr> 
	</tbody></table>
	
</div>
</s:form>
<script type="text/javascript"> 

try{
document.forms['payableCompanyCodeForm'].elements['radiobilling'].checked=true;
document.forms['payableCompanyCodeForm'].elements['payCompanyCode'].value= document.forms['payableCompanyCodeForm'].elements['payCompanyCode1'].value;
}
catch(e){}
try{
checkCompanyCode(); 
}
catch(e){}
</script>