<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<head> 
<title>Crew Capacity</title> 
<meta name="heading" content="Crew Capacity"/>
<style type="text/css">

</style>

<script type="text/javascript">
var r={
		 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}
function isFloat(targetElement){   
	var i;
var s = targetElement.value;
if(s=='.'){
	 alert("Only numbers are allowed here");
        document.getElementById(targetElement.id).value='';
        document.getElementById(targetElement.id).select();
        return false;
	}else{
for (i = 0; i < s.length; i++){   
    var c = s.charAt(i);
    if (((c < "0") || (c > "9"))) {
    if (c == ".") {
    
    }else{
    alert("Only numbers are allowed here");
    //alert(targetElement.id);
    document.getElementById(targetElement.id).value='';
    document.getElementById(targetElement.id).select();
    return false;
    }
}
}
}
return true;
}


function onlyNumsAllowed(evt, strList, bAllow)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110) || (keyCode==67) || (keyCode==86); 
}

function addNewGroup(){
	window.open("openPopUpCrewCapacity.html?decorator=popup&popup=true","mywindow","menubar=0,resizable=1,top=100,left=380,scrollbars=1,width=1000,height=630")
}

function checkJobType(){
	var job=document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value;
	var crewGroup=document.forms['crewCapacityForm'].elements['crewCapacity.crewGroup'].value;
    var notFound=true;	
	<c:forEach var="entry" items="${crewGroupAndJobMap}">
		var valKey="${entry.key}";
		var valValue="${entry.value}";
		if(valKey!=crewGroup){
			job=job.replace("(","");
			job=job.replace(")","");
			var rec=job.split(",");
			for(var x=0;x<rec.length&&notFound;x++){
				if(valValue.indexOf(rec[x])>-1){
					notFound=false;	
				}
			}
		}
	</c:forEach>
	return notFound;
}
function checkJobTypeCurrent(){
	var job=document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value;
    var notFound=true;	
	job=job.replace("(",'');
	job=job.replace(")",'');
	job=job.replace(/\'/g,'');
	var rec=job.split(',');
	for(var x=0;x<rec.length&&notFound;x++){
		var cont=countString(job,rec[x])
		if(cont>1){
			notFound=false;	
		}
	}
	return notFound;
}
function countString(str, search){
    var count=0;
    var index=str.indexOf(search);
    while(index!=-1){
        count++;
        index=str.indexOf(search,index+1);
    }
    return count;
}
function checkJobValidation(){
	var job=document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value;
	job=job.trim();
	if(job!=''){
		var jobLength = job.length;
		var reminder = ((jobLength-1)%6);
		var startPoint = job.substring(0,2)
		var endPoint = job.substring(jobLength-2,jobLength)
		if(startPoint!="('"){		
			alert("Incorrect Job Format");
			document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value="${crewCapacity.jobType}";
			return false;
		}else if(endPoint!="')"){		
			alert("Incorrect Job Format");
			document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value="${crewCapacity.jobType}";
			return false;
		}else if(reminder > 0){		
			alert("Incorrect Job Format");
			document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value="${crewCapacity.jobType}";
			return false;
		}else if(!checkJobType()){
			alert("Same Job type already exist. Please fill another job.");
			document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value="${crewCapacity.jobType}";
			
		}else if(!checkJobTypeCurrent()){
			alert("Same Job type already exist. Please fill another job.");
			document.forms['crewCapacityForm'].elements['crewCapacity.jobType'].value="${crewCapacity.jobType}";
		}else{
			return true;
		}	
	}else{
		return true;
	}
}
	function findAllRecordsOfGroup(target){		
				document.forms['crewCapacityForm'].action = 'searchRecordsByGroupName.html';
		    	document.forms['crewCapacityForm'].submit();
		  }
	
	function checkMandatoryField(rowCount){
		for(var i=1; i<26&&i!=rowCount;i++){
			 var minQty=document.getElementById('minQty'+i).value;
			 var maxQty=document.getElementById('maxQty'+i).value;																
			 var averageWt=document.getElementById('avgWet'+i).value;
			 var rangeMode=document.getElementById('rngMod'+i).value;
		      if(minQty!="" && maxQty!="" && averageWt!="" && rangeMode==""){
			    	 alert("Please select Mode for line # "+i);
			    	 return false;
		       }		 	
		}
	}

	function checkDuplicateMode(rowCount){
		for(var i=1; i<8;i++){
			var serviceTypeOld=document.getElementById('srvTyp'+i).value;
			var asmpModOld=document.getElementById('ampMod'+i).value;
			var serviceTypeCurrent=document.getElementById('srvTyp'+rowCount).value;
		 	var asmpModCurrent=document.getElementById('ampMod'+rowCount).value;
			 if(rowCount!=i){
				 if((serviceTypeOld==serviceTypeCurrent) && (((asmpModOld=='All') && (asmpModCurrent=='Sea' || asmpModCurrent=='Air' || asmpModCurrent=='All')) || (asmpModOld==asmpModCurrent))){
					alert("This Service Type and Mode already exists,Kindly select another value.");	
					document.getElementById('ampMod'+rowCount).value='';
					return false;
				 }
			 }
		}
	}
	function checkDuplicatePhyCondition(rowCount){
		for(var i=1; i<7;i++){
			var phyConOld=document.getElementById('phyCon'+i).value;
			var phyConCurrent=document.getElementById('phyCon'+rowCount).value;
			 if(rowCount!=i){
				 if(phyConOld==phyConCurrent){
					alert("This Physical condition already exists,Kindly select another value.");	
					document.getElementById('phyCon'+rowCount).value='';
					return false;
				 }
			 }
		}
	}
function deleteRecord(targetElement){
	var id = targetElement.value;
	if(id!=''){
		var agree=confirm("Are you sure you wish to remove this Group Name?");
		if (agree){			
			document.forms['crewCapacityForm'].action = 'deleteGroupNameCrewCapacity.html';
	    	document.forms['crewCapacityForm'].submit();
		 }else{
			return false;
		}
	}
}
function checkMandatoryField1(){
	for(var i=1; i<26;i++){
		 var minQty=document.getElementById('minQty'+i).value;
		 var maxQty=document.getElementById('maxQty'+i).value;																
		 var averageWt=document.getElementById('avgWet'+i).value;
		 var rangeMode=document.getElementById('rngMod'+i).value;
	      if(minQty!="" && maxQty!="" && averageWt!="" && rangeMode==""){
		    	 alert("Please select Mode for line # "+i);
		    	 return false;
	       }		 	
	}
}
</script>

</head>
<s:form id="crewCapacityForm" name="crewCapacityForm" action="saveCrewCapacity.html" method="post" onsubmit="return checkMandatoryField1();" >
<s:hidden name="CrewCapacityChildFormFlag" value="N"/>
<s:hidden name="id" value="${crewCapacity.id}"/>
<s:hidden name="crewCapacity.id" />
<div id="newmnav">
		  <ul>		  	
		  		<li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>		  		
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Crew Capacity<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>		  		
		  </ul>
</div>
<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="detailTabLabel" cellspacing="1" cellpadding="1" border="0">
	  <tbody>
		  <tr>
		  	<td align="right" class="listwhitetext"><b>Select Group</b>
		  		<s:select cssClass="list-menu" name="crewCapacity.crewGroup" list="%{crewGroupList}" onchange="findAllRecordsOfGroup(this);" cssStyle="width:150px"/>
		  	</td>
		  	<td align="right" class="listwhitetext" width="680">Job Type
		  		<s:textfield name="crewCapacity.jobType" onchange="checkJobValidation();" maxlength="100" size="100" cssClass="input-text" cssStyle="text-align:left" onkeydown="" onkeyup=""/>
		  	</td>
		    </tr>
		      	<tr>
					<td align="left" height="13px"></td>
				</tr>		  	
		 </tbody>
	</table>
	
	 	<table class="detailTabLabel" border="0" cellpadding="2" style="margin-bottom:20px;">
		  <tbody>  	
		  	<tr>
		  	<!-- start left table -->	
		  		<td align="left">
		  	 <div style="margin-bottom:0px;" id="otabs">
				  <ul>
				    <li><a class="current"><span>Weight Range</span></a></li>
				  </ul>
			</div>
		<div style="clear:both;"> </div>
		<table border="0" class="table" style="margin:0px">		
		   <thead>	 	
		  <tr>
		  	<th></th>
			  <th>Min.Qty</th>
			  <th>Max.Qty</th>
			  <th>Average Weight</th>
			  <th>Mode</th>
		  </tr>
		  </thead>
		   <tbody>
		   <c:forEach var="i" begin="1" end="25">		   
		  	<tr>
		  	<td align="left" class="listwhitetext">${i}</td>
		  		<td align="left" class="listwhitetext">
		  			<s:textfield name="crewCapacity.weightRangeMinQty${i}" id="minQty${i}" onchange="checkMandatoryField('${i}');onlyFloat(this);" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" />
		  		</td>
		  		<td align="left" class="listwhitetext">
		  			<s:textfield name="crewCapacity.weightRangeMaxQty${i}" id="maxQty${i}" onchange="checkMandatoryField('${i}');onlyFloat(this);" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" />
		  		</td>
		  		<td align="left" class="listwhitetext">
		  			<s:textfield name="crewCapacity.weightRangeAverageWeight${i}" id="avgWet${i}" onchange="checkMandatoryField('${i}');onlyFloat(this);" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" />
		  		</td>
		  		<td align="right" class="listwhitetext">
		  			<s:select cssClass="list-menu" name="crewCapacity.weightRangeMode${i}" id="rngMod${i}" onchange="checkMandatoryField('${i}');" list="%{crewCapacityMode}" headerKey=""  headerValue="" cssStyle="width:100px"/>
		  		</td>
		  	</tr>		  	
		  	</c:forEach>
		  	</tbody>
		  	</table>
		  		<td align="left" width="50"></td>
		  	<!-- start right first table -->	
		  		<td align="left" valign="top">
		  		
		  		
		  		<table class="detailTabLabel" border="0" cellpadding="2" >
		 	 <tbody>  	
		  	<tr>
		  	<td>
		  	<div style="margin-bottom:0px;" id="otabs">
				  <ul>
				    <li><a class="current"><span>Assumptions</span></a></li>
				  </ul>
			</div>
		<div style="clear:both;"> </div>
		  		<table class="table" border="0" cellpadding="2" >
		  <thead>  	
		  <tr>
		  <th></th>
			  <th>Service Type</th>
			  <th>Mode</th>
			  <th>Average Weight</th>
			  <th>Crew</th>
		  </tr>
		  </thead>
		  <tbody>
		  <c:forEach var="i" begin="1" end="7">
		  	<tr>
		  	<td align="left" class="listwhitetext">${i}</td>
		  		<td align="left" class="listwhitetext">
		  			<s:select cssClass="list-menu" name="crewCapacity.assumptionServiceType${i}" list="%{serviceType}" id="srvTyp${i}" onchange="checkDuplicateMode('${i}');" headerKey=""  headerValue="" cssStyle="width:100px"/>
		  		</td>
		  		<td align="left" class="listwhitetext">
		  			<s:select cssClass="list-menu" name="crewCapacity.assumptionMode${i}" list="%{crewCapacityMode}" id="ampMod${i}" onchange="checkDuplicateMode('${i}');" headerKey=""  headerValue="" cssStyle="width:100px"/>
		  		</td>
		  		<td align="left" class="listwhitetext">
		  			<s:textfield name="crewCapacity.assumptionAverageWeight${i}" onchange="onlyFloat(this);" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="" onkeyup=""/>
		  		</td>
		  		<td align="right" class="listwhitetext">
		  			<s:textfield name="crewCapacity.assumptionCrew${i}" onchange="onlyNumeric(this);" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="" onkeyup=""/>
		  		</td>
		  	</tr>
		  	</c:forEach>
		  	</tbody>
		  	</table>
		  	</td>
		  	</tr>
		  		<!-- start right second table -->	
		  	<tr>
		  	<td>
		  	<table class="detailTabLabel" border="0" cellpadding="2" >
		 	 <tbody>  	
		  	<tr>
		  	<div style="margin-bottom:0px;" id="otabs">
				  <ul>
				    <li><a class="current"><span>Adjustment</span></a></li>
				  </ul>
			</div>
		<div style="clear:both;"> </div>
		  		<table class="table" border="0" cellpadding="2" >
		  <thead>  	
		  <tr>
		   <th></th>
			  <th>Physical Conditions</th>
			  <th>% Adjust</th>
			  
		  </tr>
		  </thead>
		  <tbody>
		  <c:forEach var="i" begin="1" end="6">
		  	<tr>
		  	<td align="left" class="listwhitetext">${i}</td>
		  		<td align="left" class="listwhitetext">
		  			<s:select cssClass="list-menu" name="crewCapacity.adjustmentPhysicalCondition${i}" list="%{physicalConditionList}" id="phyCon${i}" onchange="checkDuplicatePhyCondition('${i}');" headerKey=""  headerValue="" cssStyle="width:150px"/>
		  		</td>
		  		<td align="left" class="listwhitetext">
		  			<s:textfield name="crewCapacity.adjustmentPercentage${i}" onblur="return isFloat(this);" onkeyup="valid(this,'special')" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="15" cssClass="input-text" cssStyle="text-align:right" />
		  		</td>		  		
		  	</tr>
		 </c:forEach>
		  	</tbody>
		  	</table>
		  	
		  	</tr>
		  	
		  	<tr>
		  	<td>
		  	
		  	</td>
		  	</tr>
		  	</tbody>
		  	</table>
		  		<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
				<td align="right" class="listwhitetext"><input type="button" class="cssbutton1" style="width:110px; height:25px;float:left;margin-left:5px;" 
				 onclick="addNewGroup();"  value="Add New Group" /></td>
				<td align="left"><s:submit cssClass="cssbutton1" type="button" key="button.save" onclick=""/></td>
		       	<td align="right" class="listwhitetext"><input type="button" class="cssbutton1" onclick="deleteRecord('${crewCapacity.id}');" style="width:100px; height:25px;float:left;margin-left:5px;" value="Delete Group" /></td>
			</tr>	
				<tr>
					<td align="left" height="20px"></td>
				</tr>		  	
		</tbody>
	</table>
		  	</td>
		  	</tr>
		  	</tbody>
		  	</table>
		  	
		  		</td>
		  		</tr>
		  		
		</tbody>
	</table>

	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<table border="0" width="800px;">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${crewCapacity.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="crewCapacity.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${crewCapacity.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty crewCapacity.id}">
								<s:hidden name="crewCapacity.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{crewCapacity.createdBy}"/></td>
							</c:if>
							<c:if test="${empty crewCapacity.id}">
								<s:hidden name="crewCapacity.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${crewCapacity.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="crewCapacity.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${crewCapacity.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty crewCapacity.id}">
								<s:hidden name="crewCapacity.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{crewCapacity.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty crewCapacity.id}">
								<s:hidden name="crewCapacity.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>			
</s:form>
<script type="text/javascript">	

</script>
