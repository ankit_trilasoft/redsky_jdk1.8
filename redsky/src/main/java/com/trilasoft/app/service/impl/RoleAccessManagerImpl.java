package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RoleAccessDao;
import com.trilasoft.app.model.RoleAccess;
import com.trilasoft.app.service.RoleAccessManager;

public class RoleAccessManagerImpl extends GenericManagerImpl<RoleAccess, Long> implements RoleAccessManager {
	RoleAccessDao roleAccessDao; 
    
	 
    public RoleAccessManagerImpl(RoleAccessDao roleAccessDao) { 
        super(roleAccessDao); 
        this.roleAccessDao = roleAccessDao; 
    }

    public List<RoleAccess> findByCorpID(String corpId){
    	return roleAccessDao.findByCorpID(corpId);
    }
}
