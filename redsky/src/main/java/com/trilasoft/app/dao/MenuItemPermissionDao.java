package com.trilasoft.app.dao;

import java.util.List;
import java.util.Set;


import org.appfuse.dao.GenericDao;
import org.appfuse.model.Role;
import org.appfuse.model.User;


import com.trilasoft.app.model.MenuItemPermission;


public interface MenuItemPermissionDao extends GenericDao<MenuItemPermission, Long>{
	public List<MenuItemPermission> findByCorpID(String corpId);
	public List checkById(Long id);
	public List<MenuItemPermission> findByUser(User user, Set<Role> roles);
	public List<User> findByUserPermission(String userName);
	public List<Long> findMenuIdsByUser(User user, Set<Role> roles);
}
