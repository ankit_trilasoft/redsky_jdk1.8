package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="losspicture")
public class LossPicture extends BaseObject
{
	 private String corpID;
	 private Long id;
	 private Long lossId;
	 private Long claimId;
	 private String createdBy;
	 private Date createdOn;
	 private String updatedBy;
	 private Date updatedOn;
	 private String photoId;
	 private String photoUri;
	 private String fileName;
	 
	 @Override
		public String toString() {
			return new ToStringBuilder(this)
					.append("corpID", corpID)
					.append("id", id)
					.append("lossId", lossId)
					.append("claimId", claimId)
					.append("createdBy", createdBy)
					.append("createdOn", createdOn)
					.append("updatedBy", updatedBy)
					.append("updatedOn", updatedOn)
					.append("photoId", photoId)
					.append("photoUri", photoUri)
					.append("fileName", fileName)
					.toString();
		}
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(corpID).append(id)
					.append(lossId).append(claimId)
					.append(createdBy).append(createdOn).append(updatedBy)
					.append(updatedOn).append(photoId).append(photoId)
					.append(photoUri).append(fileName)
					.toHashCode();
		}
		@Override
		public boolean equals(final Object other) {
			if (!(other instanceof LossPicture))
				return false;
			LossPicture castOther = (LossPicture) other;
			return new EqualsBuilder()
					.append(corpID, castOther.corpID)
					.append(id, castOther.id)
					.append(lossId, castOther.lossId)
					.append(claimId, castOther.claimId)
					.append(createdBy, castOther.createdBy)
					.append(createdOn, castOther.createdOn)
					.append(updatedBy, castOther.updatedBy)
					.append(updatedOn, castOther.updatedOn)
					.append(photoId, castOther.photoId)
					.append(photoUri, castOther.photoUri)
					.append(fileName, castOther.fileName)
					.isEquals();
		}
		
	@Column 
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getLossId() {
		return lossId;
	}
	public void setLossId(Long lossId) {
		this.lossId = lossId;
	}
	@Column
	public Long getClaimId() {
		return claimId;
	}
	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getPhotoId() {
		return photoId;
	}
	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}
	@Column
	public String getPhotoUri() {
		return photoUri;
	}
	public void setPhotoUri(String photoUri) {
		this.photoUri = photoUri;
	}
	@Column
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}