
package com.trilasoft.mss.webservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMSSPricedCrate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMSSPricedCrate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSSPricedCrate" type="{http://www.mss1.com/webservices/}MSSPricedCrate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMSSPricedCrate", propOrder = {
    "mssPricedCrate"
})
public class ArrayOfMSSPricedCrate {

    @XmlElement(name = "MSSPricedCrate")
    protected List<MSSPricedCrate> mssPricedCrate;

    /**
     * Gets the value of the mssPricedCrate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mssPricedCrate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMSSPricedCrate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MSSPricedCrate }
     * 
     * 
     */
    public List<MSSPricedCrate> getMSSPricedCrate() {
        if (mssPricedCrate == null) {
            mssPricedCrate = new ArrayList<MSSPricedCrate>();
        }
        return this.mssPricedCrate;
    }

}
