package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.TruckDriversDao;
import com.trilasoft.app.model.TruckDrivers;

public class TruckDriversDaoHibernate extends GenericDaoHibernate<TruckDrivers, Long> implements TruckDriversDao{
public TruckDriversDaoHibernate(){
	super(TruckDrivers.class);
}
public List getDriverValidation(String driverCode,String truckNumber,String partnerCode, String corpId)
{
	return getHibernateTemplate().find("select count(*) from TruckDrivers where driverCode='"+driverCode+"' and corpID='"+corpId+"' and truckNumber=?",truckNumber);
}
public List findDriverList(String truckNumber, String corpId){
	return getHibernateTemplate().find("from TruckDrivers where corpID='"+corpId+"' and truckNumber like '%"+truckNumber+"%' and driverName is not null and driverCode is not null and driverName !='' and driverCode !='' ");
}
public List getPrimaryFlagStatus(String truckNumber, String corpId){
	return getHibernateTemplate().find("select id,primaryDriver from TruckDrivers where primaryDriver=true and corpID='"+corpId+"' and truckNumber=?",truckNumber);
}
}
