package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.BrokerExamDetailsDao;
import com.trilasoft.app.model.BrokerExamDetails;
import com.trilasoft.app.service.BrokerExamDetailsManager;

public class BrokerExamDetailsManagerImpl extends GenericManagerImpl<BrokerExamDetails, Long> implements BrokerExamDetailsManager {
	BrokerExamDetailsDao brokerExamDetailsDao;
	public BrokerExamDetailsManagerImpl(BrokerExamDetailsDao brokerExamDetailsDao) {
		super(brokerExamDetailsDao);
		this.brokerExamDetailsDao=brokerExamDetailsDao;
		// TODO Auto-generated constructor stub
	}
	
	public List findByBrokerExamDetailsList(String shipNumber) {
		return brokerExamDetailsDao.findByBrokerExamDetailsList(shipNumber);
	}
	public void updateBrokerDetailsAjaxFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType){
		brokerExamDetailsDao.updateBrokerDetailsAjaxFromList(id,fieldName,fieldValue,sessionCorpID,shipNumber, userName,fieldType);
	}

}
