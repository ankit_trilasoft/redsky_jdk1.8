<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="discountList.title"/></title>   
    <meta name="heading" content="<fmt:message key='discountList.heading'/>"/> 
</head>  

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px;"  
        onclick="location.href='<c:url value="editDiscount.html?contract=${contract}&chargeCode=${chargeCode}&chargeId=${chargeId}&contractId=${contractId}"/>'" 
        value="<fmt:message key="button.add"/>" />   
</c:set>  
<s:form id="searchForm" action="discountList" method="post" validate="true" >  
<s:hidden name="chargeId" />
<s:hidden name="contractId" />
<div id="Layer1" style="width:100%" >
<div id="newmnav">
		  <ul>
		  	<li><a href="contracts.html"><span>Contract List</span></a></li>
		  	<li><a href="editCharges.html?id=${chargeId}&contractId=${contractId}"><span>Charge&nbsp;Detail</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Discount<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  		</ul>
		</div><div class="spn">&nbsp;</div>


 
<s:set name="discountList" value="discountList" scope="request"/>


<display:table name="discountList" class="table" requestURI="" id="discountList" style="width:80%;margin-top:2px;!margin-top:5px; " defaultsort="1" pagesize="10"  >
		 <display:column  sortable="true" style="width:15%" title="Discount" sortProperty="discount">
		 <a onclick="editDiscountForm('${discountList.id}')"><c:out value="${discountList.discount}"></c:out></a>		 
		  </display:column>
		 <display:column property="description" sortable="true" title="Discount&nbsp;Description"   style=""/> 
		 <display:column title="Remove" style="width:5%"> 
		<a><img align="middle" onclick="confirmSubmit(${discountList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>
</display:table>
</div>
</s:form>
<c:out value="${buttons}" escapeXml="false" /> 


<script type="text/javascript">
function editDiscountForm(id){
	chargeId=document.forms['searchForm'].elements['chargeId'].value;
	contractId=document.forms['searchForm'].elements['contractId'].value;
	location.href="editDiscount.html?id="+id+"&chargeId="+chargeId+"&contractId="+contractId;
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure to remove this row? ");
	var did = targetElement;
	if (agree){
		location.href="deleteDiscount.html?id="+did+"&chargeCode=${chargeCode}&contract=${contract}";
		}
	else{
		return false;
	}
}
</script>
		  	