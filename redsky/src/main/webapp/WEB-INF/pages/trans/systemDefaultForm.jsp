<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<style type="text/css">
.upper-case{
    text-transform:capitalize;
}
</style> 

<script language="JavaScript">
	function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==109); 
	}
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	function isNumeric()
	{   
		var i;
	    var s = document.forms['systemDefaultForm'].elements['systemDefault.maxAttempt'].value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Max LogIn Attempts should be integer");
	        document.forms['systemDefaultForm'].elements['systemDefault.maxAttempt'].value="0";
	        
	        return false;
	        }
	    }
	    return true;
	    
	}
	function isNumericNew()
	{   
		var i;
	    var s = document.forms['systemDefaultForm'].elements['systemDefault.daystoManageAlert'].value;
	    
	    if (isNaN(s)) 
  		{
	    	alert("Days To Manage Alert should be Numeric");
	    	document.forms['systemDefaultForm'].elements['systemDefault.daystoManageAlert'].value="0";
    		return false;
  		}
	    /* for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Days To Manage Alert should be integer");
	        document.forms['systemDefaultForm'].elements['systemDefault.daystoManageAlert'].value="0";
	        
	        return false;
	        }
	    } */
	    return true;
	    
	}
	

	function numbOnly(target)
	{   
		var i;
	    var s = target.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Please enter number only.");
	        target.value="";
	        
	        return false;
	        }
	    }
	    return true;
	    
	}
	
function isURL()
	 {
	 var urlStr=document.forms['systemDefaultForm'].elements['systemDefault.website'];
	 if (urlStr.value.indexOf(" ") != -1) {
	 alert("Spaces are not allowed in a URL");
	 document.forms['systemDefaultForm'].elements['systemDefault.website'].value="";
	 return false;
	}

	if (urlStr.value == "" || urlStr.value == null) {
	return true;
	}

  	urlStr=urlStr.value.toLowerCase();

	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var atom=validChars + '+';
	var urlPat=/^http:\/\/(\w*)\.([\-\+a-z0-9]*)\.(\w*)/;
	var matchArray=urlStr.match(urlPat);

	if (matchArray==null) {
	alert("The URL seems incorrect check it begins with http:// and it has 2 .'s");
	document.forms['systemDefaultForm'].elements['systemDefault.website'].value="";
	return false;
	}

	var user=matchArray[2];
	var domain=matchArray[3];

	for (i=0; i<user.length; i++) {
	if (user.charCodeAt(i)>127) {
	alert("This domain contains invalid characters.");
	document.forms['systemDefaultForm'].elements['systemDefault.website'].value="";
	return false;
	}
	}

	for (i=0; i<domain.length; i++) {
	if (domain.charCodeAt(i) > 127) {
	alert("This domain name contains invalid characters.");
	document.forms['systemDefaultForm'].elements['systemDefault.website'].value="";
	return false;
	}
	}

	var atomPat=new RegExp("^" + atom + "$");
	var domArr=domain.split(".");
	var len=domArr.length;

	for (i=0;i<len;i++) {
	if (domArr[i].search(atomPat) == -1) {
	alert("The domain name does not seem to be valid.");
	document.forms['systemDefaultForm'].elements['systemDefault.website'].value="";
	return false;
	}
	}
	
return true;
}

function validate_email(targetElement)
{
with (targetElement)
{
var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/
	 if (reg.test(targetElement)){ 
{
  
  return true;
  }
else {
	alert("Not a valid e-mail address!");
	  document.getElementById(targetElement.id).value='';
	  document.getElementById(targetElement.id).select();
	return false}
}
}
}

function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value=0;
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}

function autoPopulate_country(targetElement) {		
		var oriCountry = targetElement.value;
		var enbState = '${enbState}';
		  var index = (enbState.indexOf(oriCountry)> -1);
		  if(index != ''){
			document.forms['systemDefaultForm'].elements['systemDefault.state'].disabled = false;
		}else{
			document.forms['systemDefaultForm'].elements['systemDefault.state'].disabled = true;
			document.forms['systemDefaultForm'].elements['systemDefault.state'].value = '';
		}
     }

function getState(targetElement) {
	 var country = targetElement.value;
	 var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">	
		if(country=='${entry.value}'){
			countryCode='${entry.key}';		
		}
		</c:forEach>
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(country);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['systemDefaultForm'].elements['systemDefault.state'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['systemDefaultForm'].elements['systemDefault.state'].options[i].text = '';
					document.forms['systemDefaultForm'].elements['systemDefault.state'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['systemDefaultForm'].elements['systemDefault.state'].options[i].text = stateVal[1];
					document.forms['systemDefaultForm'].elements['systemDefault.state'].options[i].value = stateVal[0];
					
				if (document.getElementById("sState").options[i].value == '${systemDefault.state}'){
					   document.getElementById("sState").options[i].defaultSelected = true;					  					   
					}
					}
					}
 				if ('${stateshitFlag}'=="1"){
					document.getElementById("sState").value = '';
					}
					else{ 
					document.getElementById("sState").value = '${systemDefault.state}';
             }
 				autoPopulate_country(document.forms['systemDefaultForm'].elements['systemDefault.country']);
        }
 } 
var httpState = getHTTPObjectState();
 function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	
 function validation(){
	 var minRec = document.forms['systemDefaultForm'].elements['systemDefault.minRec'].value;
	 var maxRec = document.forms['systemDefaultForm'].elements['systemDefault.maxRec'].value;
	 var minPay = document.forms['systemDefaultForm'].elements['systemDefault.minPay'].value;
	 var maxPay = document.forms['systemDefaultForm'].elements['systemDefault.maxPay'].value;
	 if(minRec == '' || maxRec=='' || minPay=='' || maxPay==''){
		 alert('Please fill the Low and High limit value for payable and Receivable in the Accounting section.');
		 return false;
	 }
	}

 function chkAutoNumber(){
	 var minRec = document.forms['systemDefaultForm'].elements['systemDefault.minRec'].value;
	 var maxRec = document.forms['systemDefaultForm'].elements['systemDefault.maxRec'].value;
	 var currRec = document.forms['systemDefaultForm'].elements['systemDefault.currentRec'].value;
	 var actualMinRec = '${systemDefault.minRec}';
	 var actualMaxRec = '${systemDefault.maxRec}';
	 var minPay = document.forms['systemDefaultForm'].elements['systemDefault.minPay'].value;
	 var maxPay = document.forms['systemDefaultForm'].elements['systemDefault.maxPay'].value;
	 var currPay = document.forms['systemDefaultForm'].elements['systemDefault.currentPay'].value;
	 var actualMinPay = '${systemDefault.minPay}';
	 var actualMaxPay = '${systemDefault.maxPay}';
	 if(eval(actualMinRec) > eval(minRec) && minRec!='' ){
		 alert('Entered value is less than previously entered number. ');
		 document.forms['systemDefaultForm'].elements['systemDefault.minRec'].value='';
		 return false;
	 }
	if( eval(actualMinPay) > eval(minPay) && minPay!='' ){
		 alert('Entered value is less than previously entered number. ');
		 document.forms['systemDefaultForm'].elements['systemDefault.minPay'].value='';
		 return false;
	 }
	 if(eval(minRec) > eval(maxRec)){
		 alert('Low limit value is greater than high limit value.');
		 document.forms['systemDefaultForm'].elements['systemDefault.maxRec'].value='';
		 return false;
	 }
	 if( eval(minPay) > eval(maxPay)){
		 alert('Low limit value is greater than high limit value.');
		 document.forms['systemDefaultForm'].elements['systemDefault.maxPay'].value='';
		 return false;
	 }
 }
 function enableStateListOrigin(){ 
		var oriCon=document.forms['systemDefaultForm'].elements['systemDefault.country'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(oriCon)> -1);
		  window.setTimeout(function() { enableStateListOriginSec(index); }, 250);
	}
	function enableStateListOriginSec(index)
	{
		  if(index != ''){
		  		document.forms['systemDefaultForm'].elements['systemDefault.state'].disabled = false;
		  }else{
			  document.forms['systemDefaultForm'].elements['systemDefault.state'].disabled = true;
		  }	        
	}
	function onlyVanline(targetElement)
	{   var i;
		var s = targetElement.value;
		var count = 0;
		var countArth = 0;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        
	        if(c == '.')
	        {
	        	count = count+1
	        }
	        if(c == '-')
	        {
	        	countArth = countArth+1
	        }
	        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1)) )
	       	{
	        	alert("Enter a valid vanline minimum amount.");
		        document.getElementById(targetElement.id).value=0;
	            document.getElementById(targetElement.id).select();
		        return false;
	       	}
	        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && ((c != '-') || (countArth>'1'))) 
	        {
	        	alert("Enter a valid vanline minimum amount.");
		        document.getElementById(targetElement.id).value=0;
	            document.getElementById(targetElement.id).select();
		        return false;
	        }
	    }
	    return true;
	}
	function onlyVanlineMax(targetElement)
	{   var i;
		var s = targetElement.value;
		var count = 0;
		var countArth = 0;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        
	        if(c == '.')
	        {
	        	count = count+1
	        }
	        if(c == '-')
	        {
	        	countArth = countArth+1
	        }
	        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1)) )
	       	{
	        	alert("Enter a valid vanline maximum amount.");
		        document.getElementById(targetElement.id).value=0;
	            document.getElementById(targetElement.id).select();
		        return false;
	       	}
	        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && ((c != '-') || (countArth>'1'))) 
	        {
	        	alert("Enter a valid vanline maximum amount.");
		        document.getElementById(targetElement.id).value=0;
	            document.getElementById(targetElement.id).select();
		        return false;
	        }
	    }
	    return true;
	}
	function desableInsurance(){
		<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
		var Insurance=document.forms['systemDefaultForm'].elements['systemDefault.noInsurance'].checked;
		if(Insurance){
			document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].readOnly = true;
			document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].className = 'input-textUpper';
			document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].readOnly = true;
			document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].className = 'input-textUpper';
			document.forms['systemDefaultForm'].elements['systemDefault.unitForInsurance'].disabled = true;
			document.forms['systemDefaultForm'].elements['systemDefault.minInsurancePerUnit'].readOnly = true;
			document.forms['systemDefaultForm'].elements['systemDefault.minInsurancePerUnit'].className = 'input-textUpper';
			document.forms['systemDefaultForm'].elements['systemDefault.lumpSum'].disabled = true;
			document.forms['systemDefaultForm'].elements['systemDefault.detailedList'].disabled = true;
			document.forms['systemDefaultForm'].elements['systemDefault.noInsuranceCportal'].disabled = true;
			
			//.className = 'input-text';
		}else{
			document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].readOnly = false;
			document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].className = 'input-text';
			document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].readOnly = false;
			document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].className = 'input-text';
			document.forms['systemDefaultForm'].elements['systemDefault.unitForInsurance'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.minInsurancePerUnit'].readOnly = false;
			document.forms['systemDefaultForm'].elements['systemDefault.minInsurancePerUnit'].className = 'input-text';
			document.forms['systemDefaultForm'].elements['systemDefault.lumpSum'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.detailedList'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.noInsuranceCportal'].disabled = false;
		}
		
		</configByCorp:fieldVisibility>
	}
	function checkInsurance(){
		<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
		var Insurance=document.forms['systemDefaultForm'].elements['systemDefault.noInsurance'].checked;
		if(!Insurance){		
		var Insurance=document.forms['systemDefaultForm'].elements['systemDefault.lumpSum'].checked;
		var Unit=document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].value;
		var Replacementvalue=document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].value
		var unitForInsurance=document.forms['systemDefaultForm'].elements['systemDefault.unitForInsurance'].value;
		var InsurancePerUnit=document.forms['systemDefaultForm'].elements['systemDefault.minInsurancePerUnit'].value;
		if(Insurance){
			if(Unit=='' || Replacementvalue==''|| unitForInsurance==''|| InsurancePerUnit==''){
				alert("Please enter Lump Sum Insurance option details");
				return false;	
			}
			document.forms['systemDefaultForm'].elements['systemDefault.lumpSum'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.detailedList'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.noInsuranceCportal'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.unitForInsurance'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].disabled = false;
			document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].disabled = false;
			 return true;
		}
		}
		document.forms['systemDefaultForm'].elements['systemDefault.lumpSum'].disabled = false;
		document.forms['systemDefaultForm'].elements['systemDefault.detailedList'].disabled = false;
		document.forms['systemDefaultForm'].elements['systemDefault.noInsuranceCportal'].disabled = false;
		document.forms['systemDefaultForm'].elements['systemDefault.unitForInsurance'].disabled = false;
		document.forms['systemDefaultForm'].elements['systemDefault.insurancePerUnit'].disabled = false;
		document.forms['systemDefaultForm'].elements['systemDefault.minReplacementvalue'].disabled = false;		
		 return true;
		 </configByCorp:fieldVisibility>
	}
	function showVlidField(){
		<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
		var Insurance=document.forms['systemDefaultForm'].elements['systemDefault.noInsurance'].checked;
		if(!Insurance){
			var lumpSum=document.forms['systemDefaultForm'].elements['systemDefault.lumpSum'];
			 var e1 = document.getElementById('Currency11');
			var e2 = document.getElementById('Currency12');
			var e3 = document.getElementById('Currency13');
			var e4 = document.getElementById('Currency14');
			if(lumpSum.checked){
				e1.style.display = 'block';
				e2.style.display = 'block';
				e3.style.display = 'block';
				e4.style.display = 'block';
			}else{
				e1.style.display = 'none';
				e2.style.display = 'none';
				e3.style.display = 'none';
				e4.style.display = 'none';
			}
		}
		</configByCorp:fieldVisibility>	
	}
</script> 

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('communications', 'fade=1,hide=1')
animatedcollapse.addDiv('accounting', 'fade=1,hide=1')
animatedcollapse.addDiv('localops', 'fade=1,hide=1')
animatedcollapse.addDiv('def', 'fade=1,hide=1')
animatedcollapse.addDiv('insur', 'fade=1,hide=1')
 <c:if test="${company.vanlineEnabled==true}">
animatedcollapse.addDiv('vanline', 'fade=1,hide=1')
</c:if>
animatedcollapse.addDiv('quotforms', 'fade=1,hide=1')
animatedcollapse.addDiv('claim', 'fade=1,hide=1')
animatedcollapse.addDiv('networkConfiguration', 'fade=1,hide=1') 
animatedcollapse.addDiv('pensionDefault', 'fade=1,hide=1') 
animatedcollapse.addDiv('officeMove', 'fade=1,hide=1') 
animatedcollapse.init()

</script>
   
<style type="text/css">
/* collapse */
input[type=checkbox]{margin:0px;}
</style>
<head>   
    <title><fmt:message key="systemDefaultDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='systemDefaultDetail.heading'/>"/>
    <style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	
<script type="text/javascript">
	function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['systemDefaultForm'].elements['systemDefault.weightUnit'].value='Lbs';
  document.forms['systemDefaultForm'].elements['systemDefault.volumeUnit'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['systemDefaultForm'].elements['systemDefault.weightUnit'].value='Kgs';
  document.forms['systemDefaultForm'].elements['systemDefault.volumeUnit'].value='Cbm';
  }
} 
</script>
</head> 
<!-- Edited -->

<body>

<s:form id="systemDefaultForm" name="systemDefaultForm" action="saveSystemDefault" method="post"  onsubmit = "return validation();" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy hh:mm:ss}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="systemDefault.id"/>
<s:hidden name="systemDefault.corpID"/>
<s:hidden name="systemDefault.dailyOpLimitPC"/>
<s:hidden name="systemDefault.dailyOperationalLimit"/>
<s:hidden name="systemDefault.minServiceDay"/>
<s:hidden name="systemDefault.weightUnit"/>
<s:hidden name="systemDefault.volumeUnit"/>
<s:hidden name="systemDefault.maxupdatedon" />
<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
<s:hidden name="cportalActivation" value="YES"/>
</configByCorp:fieldVisibility>
<div id="Layer1" style="width:100%;">
<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>System Default<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${systemDefault.id}&tableName=systemdefault&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		</ul>
		</div>

		<div class="spnblk" style="padding-top:10px;">&nbsp;</div>

     <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top:-12px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>
	<tr>
	<td colspan="2">
	<!--<div class="subcontent-tab">Basic Setup</div>-->
	<table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
		<tr>
			<td class="headtab_left">
		</td>
			<td NOWRAP class="headtab_center">&nbsp;Basic Setup
		</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
		</td>
			<td class="headtab_right">
		</td>
		</tr>
	</table>	
	</td>
	</tr>
   <tr>
	<td height="10" align="left" class="listwhitetext">
  	  <table width="100%" class="detailTabLabel" border="0" >
 		  <tbody>
			<tr><td align="left" height="2px"></td></tr>
			<tr>		
				<td height="10" align="left" class="listwhitetext">
		 			<table  class="detailTabLabel" border="0">
			 		  	<tbody>
			 		  	    <tr>
			 		  			<td align="right" width="136px"><fmt:message key="systemDefault.companyCode"/></td>
			 		  			<td align="left" colspan="5"><s:textfield name="systemDefault.companyCode" required="true" maxlength="25" cssClass="input-text"  readonly="true" size="30"/></td>
			 		  		</tr>
			 		  		<tr>
			 		  			<td align="right" width=""><fmt:message key="systemDefault.company"/></td>
			 		  			<td align="left" colspan="5"><s:textfield name="systemDefault.company" required="true" cssClass="input-text" onkeydown="return onlyCharsAllowed(event)" size="30"/></td>
			 		  			<td align="right" width="95px"><fmt:message key="systemDefault.CMPYABBRV"/></td>
			 		  			<td align="left"><s:textfield name="systemDefault.CMPYABBRV" required="true" cssClass="input-text" size="32"/></td>
			 		  		</tr>
			 		  		<tr>
			 		  			<td align="right"><fmt:message key="systemDefault.address"/></td>
			 		  			<td align="left" colspan="5"><s:textfield name="systemDefault.address1" required="true" maxlength="35"cssClass="input-text upper-case" size="30" onblur="titleCase(this)"/></td>
			 		  			<td align="right"><fmt:message key="systemDefault.fax"/></td>
			 		  			<td align="left"><s:textfield name="systemDefault.fax" required="true" maxlength="20" cssClass="input-text" size="32"/></td>
			 		  		</tr>
							<tr>
								<td align="right"></td>
								<td align="left" colspan="5"><s:textfield name="systemDefault.address2" required="true" maxlength="35" cssClass="input-text upper-case" size="30" onblur="titleCase(this)" /></td>
								<td align="right"><fmt:message key="systemDefault.phone"/></td>
								<td align="left"><s:textfield name="systemDefault.phone" required="true" maxlength="20" cssClass="input-text" size="32"/></td>
							</tr>
							<tr>
								<td align="right"></td>
								<td align="left" colspan="5"><s:textfield name="systemDefault.address3" required="true" maxlength="35" cssClass="input-text upper-case" size="30" onblur="titleCase(this)"/></td>
								<td align="right"><fmt:message key="systemDefault.tin"/></td>
								<td align="left"><s:textfield name="systemDefault.tin" required="true" cssClass="input-text" size="32"/></td>
							</tr>
			 		  		<tr>
			 		  			<td align="right"><fmt:message key="systemDefault.city"/></td>
			 		  			<td align="left"><s:textfield name="systemDefault.city" required="true" cssClass="input-text upper-case" onkeydown="return onlyCharsAllowed(event)" onblur="titleCase(this)" size="30"/></td>
			 		  			
			 		  			<td align="right" colspan="5"><fmt:message key="systemDefault.zip"/></td>
			 		  			<td align="left"><s:textfield name="systemDefault.zip" required="true" maxlength="11" cssClass="input-text" size="32"/></td>
			 		  		</tr>
			 		  		<tr>
			 		  			<td align="right">Country</td>
			 		  			<td align="left" colspan="5"><s:select cssClass="list-menu"   name="systemDefault.country" list="%{country}" onchange="autoPopulate_country(this);getState(this);enableStateListOrigin();"  headerKey="" headerValue="" cssStyle="width:208px"/></td>
			 		  			<td align="right" ><fmt:message key="systemDefault.state"/></td>
			 		  			<td align="left"><s:select cssClass="list-menu" id="sState"   name="systemDefault.state" list="%{state}" headerKey="" headerValue="" cssStyle="width:220px"/></td>
							</tr>
			 		  		<tr>
			 		  			<td align="right"><fmt:message key="systemDefault.email"/></td>
			 		  			<td align="left" colspan="5"><s:textfield name="systemDefault.email" required="true" cssClass="input-text" maxlength="65"  size="30"/></td>
			 		  			
			 		 		</tr>
			 		  		<tr>
								<td align="right" class="listwhitetext">Rule Execution Time</td>
								<td align="left" colspan="5"><s:textfield name="systemDefault.toDoRuleExecutionTime" required="true" maxlength="15" cssClass="input-text" size="12"/>
								&nbsp;<s:textfield name="systemDefault.toDoRuleExecutionTime2" required="true" maxlength="15" cssClass="input-text" size="12" cssStyle="width:100px"/>
								</td>
								
								<td align="right" class="listwhitetext"><b>Last Run Date</b></td>
								<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${systemDefault.lastRunDate}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="systemDefault.lastRunDate" value="${customerFileCreatedOnFormattedValue}"/>
								<td align="left"><fmt:formatDate value="${systemDefault.lastRunDate}" pattern="${displayDateTimeFormat}"/></td>
					
					</tr>
					 	<tr>
			 		  		<td align="right">Website</td>
			 		  		<td align="left" colspan="5"><s:textfield name="systemDefault.website" required="true" cssClass="input-text" onchange="isURL();" maxlength="100"  size="30"/></td>
			 		  		<td align="right">Max&nbsp;LogIn&nbsp;Attempts</td>
			 		  		<td align="left" colspan="5">
			 		  		<table cellspacing="0" cellpadding="0" style="margin:0px;">
			 		  		<tr>
			 		  		<td align="left"><s:textfield name="systemDefault.maxAttempt" required="true" cssClass="input-text" maxlength="2" onkeydown="return isNumeric()" size="2"/></td>
			 		  		<td align="right" class="listwhitetext">&nbsp;&nbsp;Days&nbsp;To&nbsp;Manage&nbsp;Alerts&nbsp;</td>
			 		  		<td align="left"><s:textfield name="systemDefault.daystoManageAlert" required="true" cssClass="input-text" maxlength="4" onkeydown="return onlyNumsAllowed(event);" onchange="return numbOnly(this);" size="2" cssStyle="width:55px" /></td>
			 		  		</tr>
			 		  		</table>
			 		  		</td>
			 		  		</tr>
			 		  			
			 		  		<tr>
			 		  		<td align="right">Exclude IPs</td>
			 		  		<td align="left" colspan="5"><s:textfield name="systemDefault.excludeIPs" required="true" cssClass="input-text"  maxlength="100"  size="30"/></td>
			 		  		<td align="right">Survey External IPs</td>
			 		  		<td align="left" colspan="3"><s:textfield name="systemDefault.surveyExternalIPs" required="true" cssClass="input-text"  maxlength="200"  size="33" cssStyle="width:218px"/></td>			 		  		
			 		  		</tr>
			 		  		 		  		
					   </tbody>
			 	  	</table>
	 			</td>
	 		</tr> 		
	</tbody>
 		</table></td>
 		</tr>  
 		<tr>
 		<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('communications')" style="margin: 0px;">
    		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Communications
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>											
  		<div class="dspcont" id="communications">
		<table class="detailTabLabel" border="0">
 		  <tbody> 		  
 	
   	<tr>
   	   <td class="listwhitetext" align="right">Agent ID Registration Distribution List</td>
   	   <td align="left"><s:textfield name="systemDefault.agentIdRegistrationDistribution" onchange="validate_email(this);" cssClass="input-text" size="50" maxlength="65" /></td>
   	</tr>
   	<tr>
   	   <td class="listwhitetext" align="right">Rate Dept Email</td>
       <td align="left"><s:textfield name="systemDefault.rateDeptEmail" onchange="validate_email(this);" cssClass="input-text" size="50" maxlength="65"/></td>
   	</tr>	
   	<tr>
   	   <td class="listwhitetext" align="right">Storage Invoice Email</td>
       <td align="left"><s:textfield name="systemDefault.storageEmail" onchange="validate_email(this);" cssClass="input-text" size="50" maxlength="65"/></td>
   	</tr>  
   		<tr>
   	   <td class="listwhitetext" align="right">Customer Survey Email</td>
       <td align="left"><s:textfield name="systemDefault.customerSurveyEmail" onchange="validate_email(this);" cssClass="input-text" size="50" maxlength="65"/></td>
   	</tr>	
  </tbody>
</table>
</div>
</td>
</tr> 
	<tr>
 		<td height="10" width="100%" align="left" >
	
		<div  onClick="javascript:animatedcollapse.toggle('accounting')" style="margin: 0px;">
    		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Accounting
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>											
  		<div class="dspcont" id="accounting">
		<c:set var="isVatCalculation" value="false"/>
        <c:if test="${systemDefault.vatCalculation}">
	    <c:set var="isVatCalculation" value="true"/>
	    </c:if>
		<c:set var="isCostElement" value="false"/>
        <c:if test="${systemDefault.costElement}">
	    <c:set var="isCostElement" value="true"/>
	    </c:if>	 
		<c:set var="isAutoPayablePosting" value="false"/>
        <c:if test="${systemDefault.autoPayablePosting}">
	    <c:set var="isAutoPayablePosting" value="true"/>
	    </c:if>	 
	    <c:set var="isCostingDetailShow" value="false"/>
        <c:if test="${systemDefault.costingDetailShow}">
	    <c:set var="isCostingDetailShow" value="true"/>
	    </c:if>
		<table class="detailTabLabel" border="0">
 		  <tbody> 	
			<tr>                
				<td align="right" class="listwhitetext" width="139px"><fmt:message key="systemDefault.baseCurrency"/></td>
		        <td align="left"><s:select cssClass="list-menu"   name="systemDefault.baseCurrency" list="%{baseCurrency}" headerKey="" headerValue="" cssStyle="width:91px"/></td> 
		         <td align="right" class="listwhitetext">Currency Margin</td>				
				 <td align="left"><s:textfield name="systemDefault.currencyMargin" required="true" cssClass="input-text" cssStyle="width:89px" maxlength="9" onkeydown="return onlyNumsAllowed(event)"/><b>%</b></td>	
			     <td align="right" class="listwhitetext">Vat Calculation</td>
			     <td align="left"><s:checkbox key="systemDefault.vatCalculation" value="${isVatCalculation}"  fieldValue="true" />
			     </td>
			</tr>
			<tr>                
				<td align="right" class="listwhitetext" width="139px">Currency Pull Scheduling</td>
			    <td align="left"><s:textfield name="systemDefault.currencyPullScheduling" required="true" maxlength="15" cssClass="input-text" size="14" cssStyle="width:89px;" /></td>

				<td align="right" class="listwhitetext" width="180px">Company Division AcctgCode Unique</td>
			    <td align="left"><s:select cssClass="list-menu" cssStyle="min-width: 91px;" name="systemDefault.companyDivisionAcctgCodeUnique" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			    <td align="right" class="listwhitetext">Cost Element</td>
			    <td align="left"><s:checkbox key="systemDefault.costElement" value="${isCostElement}"  fieldValue="true" />
  		       </td>
								 
			</tr>
			<tr>						
				<td align="right"  class="listwhitetext"><fmt:message key="systemDefault.postDate1"/></td>
				<c:if test="${not empty systemDefault.postDate1}">
				<s:text id="postDate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.postDate1"/></s:text>
		  		<td align="left" ><s:textfield id="postDate1" name="systemDefault.postDate1" value="%{postDate1FormattedValue}"  maxlength="10" size="9"  cssStyle="width:89px;" cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="postDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.postDate1}">
				<s:text id="systemDefault.postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.postDate1"/></s:text>
		  		<td align="left" ><s:textfield id="postDate1" name="systemDefault.postDate1"  maxlength="10" size="9"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="postDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		
		  		<td align="right" class="listwhitetext" width="180px"><fmt:message key="systemDefault.postDate"/></td>
				<c:if test="${not empty systemDefault.postDate}">
				<s:text id="postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.postDate"/></s:text>
		  		<td align="left"><s:textfield id="postDate" name="systemDefault.postDate" value="%{postDateFormattedValue}"  maxlength="10" size="9"  cssStyle="width:89px;" cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="postDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.postDate}">
				<s:text id="systemDefault.postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.postDate"/></s:text>
		  		<td align="left"><s:textfield id="postDate" name="systemDefault.postDate"  maxlength="10" size="9"  cssStyle="width:89px;" cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="postDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>	
			    <td align="right" class="listwhitetext">Auto Payable Posting</td>
			    <td align="left"><s:checkbox key="systemDefault.autoPayablePosting" value="${isAutoPayablePosting}"  fieldValue="true" />
			</tr>     		
   
   	<tr>
 	 <td align="right" class="listwhitetext"><fmt:message key="systemDefault.accountingInterface"/></td> 
 	 <td align="left"><s:textfield cssClass="input-textUpper"   name="systemDefault.accountingInterface"  cssStyle="width:89px" readonly="true" /></td> 				
 	 <td align="right" class="listwhitetext"><fmt:message key="systemDefault.accountingSystem"/></td> 
 	 <td align="left"><s:textfield cssClass="input-textUpper"   name="systemDefault.accountingSystem" cssStyle="width:89px" readonly="true" /></td> 
 	 <td align="right" class="listwhitetext">Costing Detail Expand</td>
	  <td align="left"><s:checkbox key="systemDefault.costingDetailShow" value="${isCostingDetailShow}"  fieldValue="true" />				
 	</tr>
 	
   	<tr>
   	   <td align="right" class="listwhitetext"><fmt:message key="systemDefault.salesCommisionRate"/></td>
       <td align="left"><s:textfield name="systemDefault.salesCommisionRate" required="true" cssClass="input-text" size="5" maxlength="9" onkeydown="return onlyNumsAllowed(event)" cssStyle="width:89px;"/><b>%</b></td>
       <td align="right" class="listwhitetext"><fmt:message key="systemDefault.grossMarginThreshold"/></td>
       <td align="left"><s:textfield name="systemDefault.grossMarginThreshold" required="true" cssClass="input-text" size="5" maxlength="9" onkeydown="return onlyNumsAllowed(event)" cssStyle="width:89px;"/><b>%</b></td>
   <td align="right" class="listwhitetext">Driver Commission SetUp</td>
       <td align="left"><s:checkbox name="systemDefault.driverCommissionSetUp" /></td>
   	</tr>	
   	<tr>
   		
   	   <td align="right" class="listwhitetext">Invoice Extract Sequence</td>
       <td align="left"><s:textfield name="systemDefault.invExtractSeq" required="true" cssClass="input-text" size="5" maxlength="9" readonly="true" cssStyle="width:88px;"/></td>
       <td align="right" class="listwhitetext">Payable Extract Sequence</td>
       <td align="left"><s:textfield name="systemDefault.payExtractSeq" required="true" cssClass="input-text" size="5" maxlength="9" readonly="true" cssStyle="width:88px;"/></td>
       <td align="right" class="listwhitetext">Payment Application Extract Sequence</td>
       <td align="left"><s:textfield name="systemDefault.paymentApplicationExtractSeq" required="true" cssClass="input-text" size="3" maxlength="9" readonly="true" cssStyle="width:145px;"/></td>
   	</tr>
   	<tr>
   	   <td align="right" class="listwhitetext">Partner Extract Sequence</td>
       <td align="left"><s:textfield name="systemDefault.prtExtractSeq" required="true" cssClass="input-text" size="5" maxlength="9" readonly="true" cssStyle="width:88px;"/></td>
       <td align="right" class="listwhitetext">SubContract Extract Sequence</td>
       <td align="left"><s:textfield name="systemDefault.subContcExtractSeq" required="true" cssClass="input-text" size="5" maxlength="9" readonly="true" cssStyle="width:88px;"/></td>
    <configByCorp:fieldVisibility componentId="component.soExtSeqNumber.soExtSeqXml">
    <td align="right" class="listwhitetext">S/O&nbsp;Extract&nbsp;Sequence</td>
       <td align="left"><s:textfield name="systemDefault.soExtractSeq" required="true" cssClass="input-text" size="3" maxlength="9" readonly="true"/></td>
   	</configByCorp:fieldVisibility>
   	<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
   	<td align="right" class="listwhitetext">AccountLine&nbsp;Acc&nbsp;Portal&nbsp;SetUp</td>
       <td align="left"><s:checkbox name="systemDefault.accountLineAccountPortalFlag" value="${systemDefault.accountLineAccountPortalFlag}" /></td>
       </configByCorp:fieldVisibility>
   	</tr>
   	<configByCorp:fieldVisibility componentId="component.seqNumber.journalingXml">
	   	<tr>
	   	   <td align="right" class="listwhitetext">Journal Validation Field</td>
	       <td align="left"><s:textfield name="systemDefault.validation" required="true" cssClass="input-text" size="10" maxlength="15" readonly="true"/></td>
	    </tr>
    </configByCorp:fieldVisibility>
   	<tr>
   	<td align="right" class="listwhitetext">Commission Job</td>
	<td align="left" colspan="3"><s:textfield name="systemDefault.commissionJob" required="true" maxlength="55" cssClass="input-text" size="73" cssStyle="width:390px;"/></td>
   	
   	 <td align="right" class="listwhitetext">Contract&nbsp;and&nbsp;Charges&nbsp;Mandatory</td>
     <td align="left">
     <s:select cssClass="list-menu" name="systemDefault.contractChargesMandatory" list="#{'0':'No Validation', '1':'Vigorously Validation', '2':'Sympathetic Validation'}"  />
     </td>
   	</tr>
   	</tr>
   	<configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
   	<tr>
   	  	<td align="right" class="listwhitetext">Commissionable</td>
	<td align="left" colspan="3"><s:textfield name="systemDefault.commissionable" required="true" maxlength="110" cssClass="input-text" size="82"/></td>
	<td align="right" class="listwhitetext">Employee Expenses Job</td>
	<td align="left" colspan="3"><s:textfield name="systemDefault.employeeExpensesJob" required="true" maxlength="110" cssClass="input-text" size="40"/></td>
   	</tr>	
   	</configByCorp:fieldVisibility> 
   	<configByCorp:fieldVisibility componentId="component.button.commission.automation">
   	<tr>
   	  	<td align="right" class="listwhitetext">Commissionable</td>
	<td align="left" colspan="3"><s:textfield name="systemDefault.commissionable" required="true" maxlength="55" cssClass="input-text" size="75"/></td>
   	</tr>	
   	</configByCorp:fieldVisibility>
   	<tr>
   	<td align="right" class="listwhitetext">Receivable Vat</td>
   	<td align="left" class="listwhitetext" ><s:select  cssClass="list-menu" key="systemDefault.receivableVat" id="accountLine.receivableVat" cssStyle="width:89px" list="%{recVatList}" headerKey="" headerValue=""/>
   	</td>
	<td align="right" class="listwhitetext">Payable&nbsp;Vat</td>
     <td align="left" class="listwhitetext" ><s:select  cssClass="list-menu" key="systemDefault.payableVat" id="accountLine.PayableVat" cssStyle="width:89px" list="%{payVatList}" headerKey="" headerValue=""/></td>
   	 <td align="right" class="listwhitetext">Current&nbsp;Open&nbsp;Period</td>
     <td align="left"><s:checkbox name="systemDefault.currentOpenPeriod" value="${systemDefault.currentOpenPeriod}" /></td>	
   </tr> 
   <tr>
   <td align="right" class="listwhitetext">Vat Sum Threshhold</td>
   <td align="left" colspan="1"><s:textfield name="systemDefault.vatSumThreshhold" required="true" cssClass="input-text" size="5" maxlength="9"  cssStyle="width:88px;"/></td>
   <td align="right"  class="listwhitetext">Commission Close Date</td>
				<c:if test="${not empty systemDefault.commissionCloseDate}">
				<s:text id="startFlatRevenueCommissionFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.commissionCloseDate"/></s:text>
		  		<td align="left" ><s:textfield id="commissionCloseDate" name="systemDefault.commissionCloseDate" value="%{startFlatRevenueCommissionFormattedValue}"  maxlength="10" size="9"  cssStyle="width:89px;" cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="commissionCloseDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.commissionCloseDate}">
				<s:text id="startFlatRevenueCommissionFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.commissionCloseDate"/></s:text>
		  		<td align="left" ><s:textfield id="commissionCloseDate" name="systemDefault.commissionCloseDate"  maxlength="10" size="9"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="commissionCloseDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
   </tr>	
   	 <configByCorp:fieldVisibility componentId="component.systemDefault.accounting.accountingControls">
        <td colspan="10">
            <fieldset style="margin-bottom: 2px; margin-left: 20px; width: 600px;">
            <legend>Accounting Controls</legend>
            <table style="margin:0px">  
            <tr>
             <td class="listwhitetext" align="right" class="listwhitetext" width="250px">Accounts&nbsp;Payable-Actual</font></div></td>
             <td><s:textfield name="systemDefault.payableActual" maxlength="100" cssClass="input-text" size="25"/></td>
            <td align="right" class="listwhitetext"width="250px">Accounts Receivable - Actual</td>
            <td align="left"><s:textfield name="systemDefault.receivableActual" maxlength="100" cssClass="input-text" size="25"/></td> 
            </tr>
            <tr>
             <td class="listwhitetext" align="right" class="listwhitetext" width="250px">Accounts&nbsp;Payable-Accrued</td>
             <td><s:textfield name="systemDefault.payableAccrued" maxlength="100" cssClass="input-text" size="25"/></td>
             <td class="listwhitetext" align="right" class="listwhitetext" width="250px">Accounts&nbsp;Receivable-Accrued</td>
             <td><s:textfield name="systemDefault.receivableAccrued" maxlength="100" cssClass="input-text" size="25"/></td>
            </tr>
           </table>
           </fieldset>
          </td>
   	</configByCorp:fieldVisibility>
  </tbody>
</table>
<c:if test="${company.autoGenerateAccRef == 'Y'}">
<table class="detailTabLabel" width="100%">
<tr><td class="subcontenttabChild"><b>Accounting Reference Number</b></td></tr>
<tr>
<td>
	<table class="detailTabLabel">
	<tr>
	<td width="137px"></td>
<td class="listwhitetext" width="65px">Low Limit<font color="red" size="2">*</font></td>
	<td class="listwhitetext" width="65px">High Limit<font color="red" size="2">*</font></td>
	<td class="listwhitetext">Last#</td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext">Receivable #</td>
		<td align="left" ><s:textfield name="systemDefault.minRec" required="true" maxlength="6" cssClass="input-text" size="5" onchange="return numbOnly(this),chkAutoNumber();"/></td>
   		<td align="left" ><s:textfield name="systemDefault.maxRec" required="true" maxlength="6" cssClass="input-text" size="5" onchange="return numbOnly(this),chkAutoNumber();"/></td>
   		<td align="left" ><s:textfield name="systemDefault.currentRec" required="true" maxlength="6" cssClass="input-textUpper" size="5" readonly="true" /></td>
   	</tr>
   	<tr>
		<td align="right" class="listwhitetext">Payable #</td>
		<td align="left" ><s:textfield name="systemDefault.minPay" required="true" maxlength="6" cssClass="input-text" size="5" onchange="return numbOnly(this),chkAutoNumber();"/></td>
   		<td align="left" ><s:textfield name="systemDefault.maxPay" required="true" maxlength="6" cssClass="input-text" size="5" onchange="return numbOnly(this),chkAutoNumber();"/></td>
   		<td align="left" ><s:textfield name="systemDefault.currentPay" required="true" maxlength="6" cssClass="input-textUpper" size="5" readonly="true" /></td>
   	</tr>
   	<tr>
   		<td align="right" class="listwhitetext">Suspense Default #</td>
   		<td align="left" ><s:textfield name="systemDefault.suspenseDefault" required="true" maxlength="6" cssClass="input-text" size="5" onchange="valid(this,'special')" /></td>
   		
   	</tr>
	</table>
</td>
</tr>
</table>
</c:if>
</div>
</td>
</tr> 	
 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
<tr>
 		<td height="10" width="100%" align="left" >
	
		<div  onClick="javascript:animatedcollapse.toggle('claim')" style="margin: 0px;">
    		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Claim
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>											
  		<div class="dspcont" id="claim">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr>
 		     <td align="right" class="listwhitetext">Contact&nbsp;Person</td>
 		     <td align="left" width="150"><s:select name="systemDefault.claimContactPerson" list="%{claimsPerson}" cssClass="list-menu" cssStyle="width:208px" headerKey="" headerValue=""/>
 		     <td align="right" width="110" class="listwhitetext">Claim&nbsp;Show&nbsp;To&nbsp;Customer&nbsp;Portal</td>
		     <td align="left"><s:checkbox key="systemDefault.claimShowCportal" fieldValue="true"/></td> 		  
		   </tr>
			<tr>                
				<td align="right" class="listwhitetext" width="139px">Claim&nbsp;Email</td>
		        <td align="left"><s:textfield cssClass="input-text"   name="systemDefault.claimEmail" size="30" maxlength="65"/></td> 				
				<td align="right" class="listwhitetext">Claim&nbsp;Fax</td>
		       <td align="left"><s:textfield name="systemDefault.claimFax" required="true" cssClass="input-text" size="30" maxlength="20"/></td>
		       <td align="right" class="listwhitetext">Claim&nbsp;Phone</td>
		       <td align="left"><s:textfield name="systemDefault.claimPhone" required="true" cssClass="input-text" size="24" maxlength="20"/></td>
		   		  		
			</tr>
			<tr>						
				<td align="right" class="listwhitetext">Valuation Details 1</td>
		       <td align="left"><s:textfield name="systemDefault.valuationDetails1" required="true" cssClass="input-text" size="30" maxlength="50"/></td>
		       <td align="right" class="listwhitetext">Valuation Details 2</td>
		       <td align="left"><s:textfield name="systemDefault.valuationDetails2" required="true" cssClass="input-text" size="30" maxlength="50"/></td>
		   		
			</tr>   
			
			<tr>						
				<td align="right" class="listwhitetext">Valuation Details 3</td>
		       <td align="left"><s:textfield name="systemDefault.valuationDetails3" required="true" cssClass="input-text" size="30" maxlength="50"/></td>
		       <td align="right" class="listwhitetext">Valuation Details 4</td>
		       <td align="left"><s:textfield name="systemDefault.valuationDetails4" required="true" cssClass="input-text" size="30" maxlength="50"/></td>
		   	</tr>   
			
			<tr>						
				<td align="right" class="listwhitetext">Contact Address</td>
		       <td align="left" colspan="6"><s:textfield name="systemDefault.valuationDetails5" required="true" cssClass="input-text" size="140" maxlength="100" cssStyle="width:578px;"/></td>
		      	
			</tr>   	
   	<tr>
   	   	</tr>	  	
  </tbody>
</table>
</div>
</td>
</tr> 
</configByCorp:fieldVisibility>	
 		<tr>
 		<td height="10" width="100%" align="left" >
 		
		 <div  onClick="javascript:animatedcollapse.toggle('localops')" style="margin: 0px;">    		
		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Operations
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>		
		 <div class="dspcont" id="localops">
		<table class="detailTabLabel" border="0">
 		  <tbody>  		  
 		  <tr>
	 		  <td align="right" class="listwhitetext"><fmt:message key="systemDefault.lunch1"/></td>
		      <td align="left"><s:textfield name="systemDefault.lunch1" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		 <td></td><td align="right" class="listwhitetext" width="176"><fmt:message key="systemDefault.lunch2"/></td>
		      <td align="left"><s:textfield name="systemDefault.lunch2" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		 
 		  </tr>
 		  <tr>
 		     <td align="right" class="listwhitetext"><fmt:message key="systemDefault.weekdays1"/></td>
		     <td align="left"><s:textfield name="systemDefault.weekdays1" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		   		     
 		  <td></td>
 		     <td align="right" class="listwhitetext"><fmt:message key="systemDefault.weekdays2"/></td>
		     <td align="left"><s:textfield name="systemDefault.weekdays2" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		   		     
 		  </tr>
 		  
 		  <tr>
 		     <td align="right" class="listwhitetext"><fmt:message key="systemDefault.dayAt1"/></td>
		     <td align="left"><s:textfield name="systemDefault.dayAt1" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		  <td></td>
 		     <td align="right" class="listwhitetext"><fmt:message key="systemDefault.dayAt2"/></td>
		     <td align="left"><s:textfield name="systemDefault.dayAt2" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		   		     
 		  </tr>
 		  
 		  <tr>
 		     <td align="right" class="listwhitetext"><fmt:message key="systemDefault.workDay"/></td>
		     <td align="left"><s:textfield name="systemDefault.workDay" required="true" maxlength="25" cssClass="input-text" size="10"/></td>
 		   <td></td>
 		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.lateEnd"/></td>
 		    <td align="left"><s:textfield name="systemDefault.lateEnd" required="true" cssClass="input-text" size="10"/></td>
 		  </tr>
 		  <tr><td align="right" class="listwhitetext"><fmt:message key="systemDefault.lateBegin"/></td>
 		     <td align="left"><s:textfield name="systemDefault.lateBegin" required="true" cssClass="input-text" size="10"/></td>
 		 
 		       <td></td>  
 		      <td align="right" class="listwhitetext"><fmt:message key="systemDefault.minPayCode"/></td>
			  <td align="left"><s:textfield name="systemDefault.minPayCode" required="true" maxlength="7" cssClass="input-text" size="10"/></td>
 		  </tr>
 		  <tr>
	 		 
 		      <td align="right" class="listwhitetext"><fmt:message key="systemDefault.minimumHoursGurantee"/></td>
			  <td align="left"><s:textfield name="systemDefault.minimumHoursGurantee"  maxlength="10" cssClass="input-text" size="10"/></td>
			  <td></td>
			  <td align="right" class="listwhitetext">Health & Welfare Rate</td>
			  <td align="left"><s:textfield name="systemDefault.healthWelfareRate" required="true" maxlength="12" cssClass="input-text" size="10"/></td>
 		      <td></td> 		      
 		  </tr>  
 		  <tr>
 		  <td align="right" class="listwhitetext" width="">Flexible&nbsp;Weight&nbsp;Unit </td>
		 <td align="left"><s:select cssClass="list-menu" name="systemDefault.unitVariable" list="%{unitVariable}" headerKey="" headerValue="" cssStyle="width:88px"/></td> 	
      	<td colspan="1">		   
 	     <td align="right" class="listwhitetext" width="">Truck&nbsp;required&nbsp;for&nbsp;scheduling </td>
		 <td align="left"><s:select cssClass="list-menu" cssStyle="width:88px" name="systemDefault.truckRequired" list="{'Y','N'}"  /></td>
		<c:set var="isBillingCheck" value="false"/>
		<c:if test="${systemDefault.billingCheck}">
			<c:set var="isBillingCheck" value="true"/>
		</c:if>
		<td align="right" class="listwhitetext" width="96"><fmt:message key="systemDefault.billingCheck"/></td>
	 	<td align="left" class="listwhitetext" width="20"><s:checkbox key="systemDefault.billingCheck" value="${isBillingCheck}" fieldValue="true" onclick="changeStatus();"/></td>  	
  	    <td align="right" class="listwhitetext" width="82">Password&nbsp;Policy</td>
	 	<td align="left" class="listwhitetext"><s:checkbox key="systemDefault.passwordPolicy"  fieldValue="true" onclick="changeStatus();"/></td>  
	 	
	 	 <td align="right" class="listwhitetext" width="82">&nbsp;&nbsp;&nbsp;Password&nbsp;Policy&nbsp;For&nbsp;External&nbsp;User</td>
        <td align="left" class="listwhitetext"><s:checkbox key="systemDefault.passwordPolicyForExternalUser"  fieldValue="true" onclick="changeStatus();"/></td>  	
  	 </tr>
  	 <tr>
   <td align="right" class="listwhitetext" style="vertical-align:inherit;">Default Distance Calc:</td>
    <td colspan="2">
    <table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">    
    <tr>    
     <td align="left" class="listwhitetext" width="45">
    <c:if test="${systemDefault.defaultDistanceCalc == 'KM'}">			
						&nbsp;KM<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.defaultDistanceCalc" checked="checked" value="KM">
						</c:if>
						<c:if test="${systemDefault.defaultDistanceCalc != 'KM'}">			
						&nbsp;KM<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.defaultDistanceCalc" value="KM" >
						</c:if>
               </td>
	           <td align="right" class="listwhitetext"><c:if test="${systemDefault.defaultDistanceCalc == 'Mile'}">
                    &nbsp;Mile<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.defaultDistanceCalc" value="Mile" checked="checked" >
                        </c:if>
						<c:if test="${systemDefault.defaultDistanceCalc != 'Mile'}">
                  &nbsp;Mile<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.defaultDistanceCalc" value="Mile" >
                   </c:if></td>
           </tr>
           </table>        
           </td>       
           
                   	<td align="right" class="listwhitetext" style="vertical-align:bottom;!vertical-align:top;">Weights & Volume Units:</td>
 			        <td colspan="4">
 		<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
 			<tr> 	
     		<td align="right" class="listwhitetext">
     		<c:if test="${weightType == 'lbscft'}">			
						&nbsp;&nbsp;(Pounds & Cft)&nbsp;&nbsp;<INPUT type="radio" style="vertical-align:bottom;" name="weightType" checked="checked" value="lbscft" onclick="check(this);">
						</c:if>
						<c:if test="${weightType != 'lbscft'}">			
						&nbsp;&nbsp;(Pounds & Cft)&nbsp;&nbsp;<INPUT type="radio" style="vertical-align:bottom;" name="weightType" value="lbscft" onclick="check(this);">
						</c:if>
			 </td>
			<td align="right" class="listwhitetext"><c:if test="${weightType == 'kgscbm'}">
 			&nbsp;&nbsp;(Metric Units)<INPUT type="radio" style="vertical-align:bottom;" name="weightType" value="kgscbm" checked="checked" onclick="check(this);">
                        </c:if>
						<c:if test="${weightType != 'kgscbm'}">
 			&nbsp;&nbsp;(Metric Units)<INPUT type="radio" style="vertical-align:bottom;" name="weightType" value="kgscbm"  onclick="check(this);">
                        </c:if></td>
 		</tr> 		
 		</table> 
 		</td>
 		<tr>
 		<td align="right" class="listwhitetext" width="">Operation&nbsp;Mgmt</td>
		 <td align="left"><s:select cssClass="list-menu" name="systemDefault.operationMgmtBy" list="%{operationMgmtBy}" cssStyle="width:88px"/></td> 
		 </tr>
      	<tr>
      	   <td align="right" class="listwhitetext" style="vertical-align:bottom;">Hub / Warehouse Operational limits :</td>
     			<td align="left" class="listwhitetext" width="">
    				<c:if test="${systemDefault.hubWarehouseOperationalLimit == 'Crew'}">			
						&nbsp;Crew Capacity<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.hubWarehouseOperationalLimit" checked="checked" value="Crew">
						</c:if>
						<c:if test="${systemDefault.hubWarehouseOperationalLimit != 'Crew'}">			
						&nbsp;Crew Capacity<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.hubWarehouseOperationalLimit" value="Crew" >
						</c:if>
               </td>
	           <td align="left" class="listwhitetext" colspan="5"><c:if test="${systemDefault.hubWarehouseOperationalLimit == 'Ticket'}">
                    &nbsp;Weight / Volume / # of tickets<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.hubWarehouseOperationalLimit" value="Ticket" checked="checked" >
                        </c:if>
						<c:if test="${systemDefault.hubWarehouseOperationalLimit != 'Ticket'}">
                  &nbsp;Weight / Volume / # of tickets<INPUT type="radio" style="vertical-align:bottom;" name="systemDefault.hubWarehouseOperationalLimit" value="Ticket" >
                   </c:if></td>
                   
             
      	</tr>
   </tr> 	
 	</table>
 	
</div></td>
</tr> 		   
  
<tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('def')" style="margin: 0px;">
    		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Default
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>									
  		<div class="dspcont" id="def">
		<table class="detailTabLabel" border="0">
 		  <tbody>
   			<tr>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.storage"/></td>
			<td align="left"><s:textfield name="systemDefault.storage" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
			<td align="right" class="listwhitetext">Storage&nbsp;Billing&nbsp;Job</td>
			<td align="left"><s:textfield name="systemDefault.storageBilling" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
			</tr>
			<tr>
			<td align="right" class="listwhitetext">SIT&nbsp;Billing&nbsp;Job</td>
			<td align="left"><s:textfield name="systemDefault.SITBilling" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
			<td align="right" class="listwhitetext">TPS&nbsp;Billing&nbsp;Job</td>
			<td align="left"><s:textfield name="systemDefault.TPSBilling" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
			</tr>
			<tr>
			<td align="right" class="listwhitetext">SCS&nbsp;Billing&nbsp;Job</td>
			<td align="left"><s:textfield name="systemDefault.SCSBilling" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
			<td class="listwhitetext" align="right" width="135px">Filter&nbsp;Coordinator:&nbsp;</td>
			<td class="listwhitetext"><s:radio name="systemDefault.filterCoordinator" cssStyle="margin-left:0px;vertical-align:bottom;" list="%{filterCoordinator}"/></td>
			<tr>
			</tr>
			<tr>
				<td align="right" class="listwhitetext">DoS Partner</td>
			    <td align="left" colspan=""><s:textfield name="systemDefault.dosPartnerCode" required="true" maxlength="150" cssClass="input-text" size="40"/></td>
				<td align="right" class="listwhitetext">DoS SCAC</td>
			    <td align="left" colspan="" ><s:textfield name="systemDefault.dosscac" required="true" maxlength="4" cssClass="input-text" size="20" cssStyle="width:266px;"/></td>
			</tr>
			
			<tr>
				<td class="listwhitetext" align="right" >Standard Country Hauling</td>
				<td><s:textfield name="systemDefault.standardCountryHauling" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
				<td class="listwhitetext" align="right" >Standard Country Charges</td>
				<td colspan="5"><s:textfield name="systemDefault.standardCountryCharges" required="true" maxlength="100" cssClass="input-text" size="40"/></td>
			</tr>	
			<tr>
				<td class="listwhitetext" align="right" >Local Job Types</td>
				<td><s:textfield name="systemDefault.localJobs" required="true" maxlength="500" cssClass="input-text" size="40"/></td>
				<configByCorp:fieldVisibility componentId="component.standard.registrationNumber.logic">
				<td class="listwhitetext" align="right" >Prefix </td>
				<td><s:textfield name="systemDefault.prefix" required="true" maxlength="4" cssClass="input-text" size="4"/></td>
				<td class="listwhitetext" align="right" >Seq </td>
				<td><s:textfield name="systemDefault.sequence" onchange="return numbOnly(this);" onkeydown="return onlyNumsAllowed(event);" required="true" maxlength="4" cssClass="input-text" size="4"/></td>
				</configByCorp:fieldVisibility>
				<td class="listwhitetext" align="right" >Reciprocity Job Types</td>
				<td><s:textfield name="systemDefault.reciprocityjobtype" required="true" maxlength="500" cssClass="input-text" size="40"/></td>
			</tr>
			<tr>
				<td class="listwhitetext" align="right" >Agent Search Validation</td>
				<td align="left"><s:checkbox name="systemDefault.agentSearchValidation" value="${systemDefault.agentSearchValidation}" /></td>	
				<td class="listwhitetext" align="right" >US GovJob Types</td>
			    <td><s:textfield name="systemDefault.usGovJobs" required="true" maxlength="500" cssClass="input-text" size="40"/></td>
			</tr>
			<tr>
			<td class="listwhitetext" align="right" >Account Search Validation</td>
			<td align="left"><s:checkbox name="systemDefault.accountSearchValidation" value="${systemDefault.accountSearchValidation}" /></td>	
			<td align="right" class="listwhitetext">EDI Partner</td>
			<td align="left" colspan=""><s:textfield name="systemDefault.ediBillToCode" required="true" maxlength="150" cssClass="input-text" size="40"/></td>
			</tr>
		
			
</tbody>
</table>
</div></td>
</tr>
<!-- Start Insurance Section  -->
<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">	
 <tr>
 	<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('insur');showVlidField();" style="margin: 0px;">
    		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Insurance
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>		
		<div class="dspcont" id="insur">
		<table class="detailTabLabel" border="0">
 		  <tbody>
			<tr>
			 <td class="listwhitetext" align="left" width=""  class="listwhitetext">No Insurance on Cportal
         	<s:checkbox name="systemDefault.noInsurance" cssStyle="vertical-align:sub;!vertical-align:top;" onclick="desableInsurance();" /></td>
			</tr>			
			<tr>
			<td colspan="10">
			<fieldset style="margin-bottom: 2px; margin-left: 20px; width: 525px;">
		    <legend>Insurance Options:</legend>
		    <table style="margin:0px">	
		    <tr>
			 <td class="listwhitetext" align="right" class="listwhitetext">Lump Sum</td>
			<td class="listwhitetext" ><s:checkbox name="systemDefault.lumpSum" onclick="showVlidField();" /></td>
			</tr>		
			<tr>
			 <td class="listwhitetext" align="right" class="listwhitetext">Insurance Per Unit <div id="Currency11" style="float:right;"><font color="red" size="2">*</font></div></td>
             <td><s:textfield name="systemDefault.insurancePerUnit" required="true" maxlength="100" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext" width="139px">Unit for Insurance<div id="Currency12" style="float:right;"><font color="red" size="2">*</font></div></td>
		    <td align="left"><s:select cssClass="list-menu"   name="systemDefault.unitForInsurance" list="{'Kilo', 'CBM', 'CFT', 'LB'}" headerKey="" headerValue="" cssStyle="width:124px"/></td> 
			</tr>
			<tr>
			 <td class="listwhitetext" align="right" class="listwhitetext" width="115px">Min Replacement value<div id="Currency13" style="float:right;"><font color="red" size="2">*</font></div></td>
             <td><s:textfield name="systemDefault.minReplacementvalue" required="true" maxlength="100" cssClass="input-text" size="20"/></td>
             <td class="listwhitetext" align="right" class="listwhitetext">Min Insur value per unit<div id="Currency14" style="float:right;"><font color="red" size="2">*</font></div></td>
             <td><s:textfield name="systemDefault.minInsurancePerUnit" required="true" maxlength="100" cssClass="input-text" size="16"/></td>
			</tr>
				<tr>
			 <td  align="right" class="listwhitetext" >Detailed List</td>
			<td><s:checkbox name="systemDefault.detailedList" /></td>
		   </tr>
		   <tr>
			 <td  align="right" class="listwhitetext" >No Insurance</td>
			<td><s:checkbox name="systemDefault.noInsuranceCportal"  /></td>
		   </tr>
		   </table>
		   </fieldset>
		   </td>
		  </tbody>
		</table>
		</div>
	</td>
</tr> 
</configByCorp:fieldVisibility>
<!-- End Insurance Section  -->
			   
 <c:if test="${company.vanlineEnabled==true}"> 
<tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('vanline')" style="margin: 0px;">
    		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Van Line
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>		
										
  		<div class="dspcont" id="vanline">
		<table class="detailTabLabel" border="0" width="">
 		  <tbody>
 		  
 		  <tr>
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.miles"/></td>
		    <td align="left" colspan="3"><s:textfield name="systemDefault.miles" required="true" cssClass="input-text" size="78"/></td>
		    <td align="right" class="listwhitetext">Vanline Minimum Amount</td>
		    <td align="left" ><s:textfield name="systemDefault.vanlineMinimumAmount"  onchange="onlyVanline(this);" required="true" cssClass="input-text" maxlength="12" size="10" cssStyle="width:160px;"/></td>
		    <td align="right" class="listwhitetext">Vanline Maximum Amount</td>
		    <td align="left" ><s:textfield name="systemDefault.vanlineMaximumAmount"  onchange="onlyVanlineMax(this);" required="true" cssClass="input-text" maxlength="12" size="10"/></td>
		 </tr>
		<tr>
		  		 
			<td align="right" class="listwhitetext" style="width:139px"><fmt:message key="systemDefault.coordinator"/></td>
			<td align="left" colspan="3"><s:textfield name="systemDefault.coordinator" required="true" cssClass="input-text" size="78"/></td>
		    
		    <c:set var="ischecked1" value="false"/>
				<c:if test="${systemDefault.automaticReconcile}">
					<c:set var="ischecked1" value="true"/>
				</c:if>
	 		
	 		<td align="right" class="listwhitetext" width="150">Automatic Reconcile</td>
	 		<td align="left" class="listwhitetext"><s:checkbox key="systemDefault.automaticReconcile" value="${ischecked1}" fieldValue="true" cssStyle="margin:0px;" /></td>
		    <td align="right" class="listwhitetext">Exception Charge Code</td>
		    <td align="left" ><s:textfield name="systemDefault.vanlineExceptionChargeCode"   required="true" cssClass="input-text" maxlength="225" size="10"/></td>
		</tr>
		<tr>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.daCode"/></td>
			<td align="left" colspan="3"><s:textfield name="systemDefault.daCode" required="true" cssClass="input-text" size="78"/></td>
		        <c:set var="ischecked3" value="false"/>
				<c:if test="${systemDefault.POS}">
					<c:set var="ischecked3" value="true"/>
				</c:if>
	 		
	 		<td align="right" class="listwhitetext" width="150">POS</td>
	 		<td align="left" class="listwhitetext"><s:checkbox key="systemDefault.POS" value="${ischecked3}" fieldValue="true" cssStyle="margin:0px;" /></td>
		    <c:set var="isownbillingVanline" value="false"/>
				<c:if test="${systemDefault.ownbillingVanline}">
					<c:set var="isownbillingVanline" value="true"/>
				</c:if>
			<td align="right" class="listwhitetext" width="150">Own billing Vanline</td>
	 		<td align="left" class="listwhitetext"><s:checkbox key="systemDefault.ownbillingVanline" value="${isownbillingVanline}" fieldValue="true" cssStyle="margin:0px;" /></td>
		
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="150">Own Billing Bill To Codes</td>
	 	<td align="left" colspan="3"><s:textfield name="systemDefault.ownbillingBilltoCodes" required="true" maxlength="500" cssClass="input-text" size="78"/></td>
		</tr>
		<tr>    
		  <td align="right" class="listwhitetext"><fmt:message key="systemDefault.longDtypes"/></td>
		  <td align="left" colspan="3"><s:textfield name="systemDefault.longDtypes" required="true" maxlength="55" cssClass="input-text" size="78"/></td>
		<td align="right" class="listwhitetext">Miscellaneous Driver</td>
		  <td align="left" colspan="3"><s:textfield name="systemDefault.miscellaneousdefaultdriver" required="true" maxlength="8" cssClass="input-text" size="10" cssStyle="width:160px;"/></td>
		
		</tr>
		<tr>	
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.miscVl"/></td>
			<td align="left" colspan="3"><s:textfield name="systemDefault.miscVl" required="true" maxlength="55" cssClass="input-text" size="78"/></td>
			<td align="right" class="listwhitetext" width="150">Vanline Settle Colour Status</td>
	 		<td align="left" class="listwhitetext"><s:checkbox key="systemDefault.vanlineSettleColourStatus" value="${systemDefault.vanlineSettleColourStatus}" fieldValue="true" cssStyle="margin:0px;" /></td>
		
        </tr>
        
        <tr> 
             
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.vanLine"/></td>
			<td align="left" width="50px"><s:textfield name="systemDefault.vanLine" required="true" maxlength="25" cssClass="input-text" size="20" cssStyle="width:160px;"/></td>
			
			<td width="112px" align="right" class="listwhitetext"><fmt:message key="systemDefault.cmpy"/></td>
			<td align="left"><s:textfield name="systemDefault.cmpy" required="true" maxlength="45" cssClass="input-text" size="28" cssStyle="width:126px;"/></td>
		
			<td align="right" class="listwhitetext" width="150">Rating</td>
	 		<td align="left" class="listwhitetext" width="20"><s:checkbox key="systemDefault.rating" value="${systemDefault.rating}" fieldValue="true" cssStyle="margin:0px;" />
			<td align="left" class="listwhitetext" width="">Distribution <s:checkbox key="systemDefault.distribution" value="${systemDefault.distribution}" fieldValue="true" cssStyle="margin:0px;vertical-align:top;" /></td>
		
		</tr>
		
		 <tr>
		 	
		 	<c:set var="ischecked" value="false"/>
				<c:if test="${systemDefault.isUnigroup}">
					<c:set var="ischecked" value="true"/>
				</c:if>
	 		<td align="right" valign="top" class="listwhitetext"><s:checkbox key="systemDefault.isUnigroup" value="${ischecked}" fieldValue="true" onclick="changeStatus();"/></td>
	 		<td align="left" class="listwhitetext" width="130"><fmt:message key="systemDefault.isUnigroup"/></td>
 		   
 		   
 		   <td width="112px" align="right" class="listwhitetext">Company Division</td>
			<td align="left"><s:textfield name="systemDefault.vanLineCompanyDivision" required="true" maxlength="10" cssClass="input-text" size="28" cssStyle="width:126px;"/></td>
 		   <td width="" align="right" class="listwhitetext">Driver Crew Type</td>
			<td align="left" colspan="3"><s:textfield name="systemDefault.driverCrewType" required="true" maxlength="100" cssClass="input-text" size="25" cssStyle="width:160px;"/></td>
 		 
 		 </tr> 
 		 <tr>
		 	
	 		<td align="right" class="listwhitetext"><fmt:message key="systemDefault.uvl"/></td>
			<td align="left"><s:textfield name="systemDefault.uvl" required="true" maxlength="21" cssClass="input-text" size="20" cssStyle="width:160px;"/></td>
 		   	<td align="right" class="listwhitetext"><fmt:message key="systemDefault.mvl"/></td>
			<td align="left"><s:textfield name="systemDefault.mvl" required="true" maxlength="21" cssClass="input-text" size="28" cssStyle="width:126px;"/></td>
		</tr>
		<tr>
			<td align="right" class="listwhitetext">UVL Contract</td>
			<td align="left"><s:textfield name="systemDefault.uvlContract" required="true" maxlength="65" cssClass="input-text" size="28"/></td>
			<td align="right" class="listwhitetext">UVL Bill To Code</td>
			<td align="left"><s:textfield name="systemDefault.uvlBillToCode" required="true" maxlength="8" cssClass="input-text" size="20" cssStyle="width:126px;"/></td>
			<td align="right" class="listwhitetext">UVL Bill To Name</td>
			<td align="left"><s:textfield name="systemDefault.uvlBillToName" required="true" maxlength="225" cssClass="input-text" size="28"/></td>
		</tr>
		<tr>
 		   	<td align="right" class="listwhitetext">MVL Contract</td>
			<td align="left"><s:textfield name="systemDefault.mvlContract" required="true" maxlength="65" cssClass="input-text" size="28"/></td>
 		   	<td align="right" class="listwhitetext">MVL Bill To Code</td>
			<td align="left"><s:textfield name="systemDefault.mvlBillToCode" required="true" maxlength="8" cssClass="input-text" size="20" cssStyle="width:126px;"/></td>
			<td align="right" class="listwhitetext">MVL Bill To Name</td>
			<td align="left"><s:textfield name="systemDefault.mvlBillToName" required="true" maxlength="225" cssClass="input-text" size="28"/></td>
		</tr>	
		<tr>
            <td align="right" class="listwhitetext">INT Contract</td>
			<td align="left"><s:textfield name="systemDefault.intContract" required="true" maxlength="65" cssClass="input-text" size="28"/></td>
			<td align="right" class="listwhitetext">INT Bill To Code</td>
			<td align="left"><s:textfield name="systemDefault.intBillToCode" required="true" maxlength="8" cssClass="input-text" size="20" cssStyle="width:126px;"/></td>
			<td align="right" class="listwhitetext">INT Bill To Name</td>
			<td align="left"><s:textfield name="systemDefault.intBillToName" required="true" maxlength="225" cssClass="input-text" size="28"/></td> 		   	
 		 </tr> 
 		 <tr>
 		 <td align="right" class="listwhitetext">TransDoc</td>
			<td align="left" colspan="3"><s:textfield name="systemDefault.transDoc" required="true" maxlength="125" cssClass="input-text" size="78"/></td>
			<td align="right" class="listwhitetext">Default Set Of Charge</td>
			<td align="left" colspan="3"><s:textfield name="systemDefault.defaultDivisionCharges" required="true" maxlength="225" cssClass="input-text" size="78"/></td>
 		 </tr>

</tbody>
</table>
</div></td>
</tr>
</c:if>
 <c:if test="${company.vanlineEnabled!=true}"> 
 <s:hidden name="systemDefault.miles" value="${systemDefault.miles}" />
 <s:hidden name="systemDefault.vanlineMinimumAmount" value="${systemDefault.vanlineMinimumAmount}"/>
 <s:hidden name="systemDefault.vanlineMaximumAmount" value="${systemDefault.vanlineMaximumAmount}"/>
 <s:hidden name="systemDefault.vanlineExceptionChargeCode" />
 <s:hidden name="systemDefault.ownbillingVanline" /> 
 <s:hidden name="systemDefault.coordinator" />
 <s:hidden name="systemDefault.automaticReconcile"  />
 <s:hidden name="systemDefault.daCode" />
 <s:hidden name="systemDefault.POS" />
 <s:hidden name="systemDefault.longDtypes" />
 <s:hidden name="systemDefault.miscellaneousdefaultdriver" />
 <s:hidden name="systemDefault.miscVl" />
 <s:hidden name="systemDefault.vanLine" />
 <s:hidden name="systemDefault.cmpy" />
 <s:hidden name="systemDefault.isUnigroup" />
 <s:hidden name="systemDefault.vanLineCompanyDivision" />
 <s:hidden name="systemDefault.uvl" />
 <s:hidden name="systemDefault.mvl" />
 <s:hidden name="systemDefault.uvlContract" />
 <s:hidden name="systemDefault.mvlContract" />
 <s:hidden name="systemDefault.uvlBillToCode" />
 <s:hidden name="systemDefault.mvlBillToCode" />
 <s:hidden name="systemDefault.uvlBillToName" />
 <s:hidden name="systemDefault.mvlBillToName" />
 <s:hidden name="systemDefault.transDoc" />
 <s:hidden name="systemDefault.vanlineSettleColourStatus" />
 </c:if>
<!-- 	

<tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('notyet')" style="margin: 0px;">    		
		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Not Yet Characterized
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>										
  		<div class="dspcont" id="notyet">
		<table class="detailTabLabel" border="0">
 		  <tbody>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.lastSequence"/></td>
			<td align="left"><s:textfield name="systemDefault.lastSequence" required="true" cssClass="input-text" size="20"/></td>
			<td width="151px" align="right" class="listwhitetext"><fmt:message key="systemDefault.persionalPath"/></td>
			<td align="left"><s:textfield name="systemDefault.persionalPath" required="true" maxlength="60" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.localTemp"/></td>
			<td align="left"><s:textfield name="systemDefault.localTemp" required="true" maxlength="60" cssClass="input-text" size="20"/></td>
		
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.export"/></td>
		    <td align="left"><s:textfield name="systemDefault.export" required="true" cssClass="input-text" size="20"/></td>
            
         </tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.IMPORT"/></td>
			<td align="left"><s:textfield name="systemDefault.IMPORT" required="true" cssClass="input-text" size="20"/></td>
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.sCloseCSO"/></td>
		    <td align="left"><s:textfield name="systemDefault.sCloseCSO" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.domestic"/></td>
			<td align="left"><s:textfield name="systemDefault.domestic" required="true" cssClass="input-text" size="20"/></td>
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.sTestDate"/></td>
		    <td align="left"><s:textfield name="systemDefault.sTestDate" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr>
			 <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.other"/></td>
			<td align="left"><s:textfield name="systemDefault.other" required="true" maxlength="20" cssClass="input-text" size="20"/></td>
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.oInterval"/></td>
		    <td align="left"><s:textfield name="systemDefault.oInterval" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.byClient"/></td>
			<td align="left"><s:textfield name="systemDefault.byClient" required="true" cssClass="input-text" size="20"/></td>
	        <td align="right" class="listwhitetext"><fmt:message key="systemDefault.userPath"/></td>
	        <td align="left"><s:textfield name="systemDefault.userPath" required="true" cssClass="input-text" size="20"/></td>
	   </tr>
		
		<tr> 
			<td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.lastStars"/></td>
			<td align="left"><s:textfield name="systemDefault.lastStars" required="true" cssClass="input-text" size="20"/></td>
			
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.spCode"/></td>
			<td align="left"><s:textfield name="systemDefault.spCode" required="true" maxlength="21" cssClass="input-text" size="20"/></td>
		</tr>
				
		   <tr> 
		   <td></td>
			
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.vlContract"/></td>
			<td align="left"><s:textfield name="systemDefault.vlContract" required="true" maxlength="20" cssClass="input-text" size="20"/></td>
		
			<td align="right" class="listwhitetext" width="145px"><fmt:message key="systemDefault.doRegistrationNumber"/></td>
			<td align="left"><s:textfield name="systemDefault.doRegistrationNumber" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.dorecncl"/></td>
			<td align="left"><s:textfield name="systemDefault.dorecncl" required="true" cssClass="input-text" size="20"/></td>
		
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.trackPath"/></td>
		    <td align="left"><s:textfield name="systemDefault.trackPath" required="true" maxlength="60" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.backup"/></td>
			<td align="left"><s:textfield name="systemDefault.backup" required="true" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.scoreForm"/></td>
			<td align="left"><s:textfield name="systemDefault.scoreForm" required="true" maxlength="20" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.beginStars"/></td>
		    <td align="left"><s:textfield name="systemDefault.beginStars" required="true" maxlength="5" cssClass="input-text" size="20"/></td>
		    <td align="right" class="listwhitetext"><fmt:message key="systemDefault.srunDate"/></td>
		    <td align="left"><s:textfield name="systemDefault.srunDate" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.dispatch"/></td>
			<td align="left"><s:textfield name="systemDefault.dispatch" required="true" maxlength="6" cssClass="input-text" size="20"/></td>
		
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.LASTEQUIP"/></td>
			<td align="left"><s:textfield name="systemDefault.LASTEQUIP" required="true" cssClass="input-text" size="20"/></td>
			
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.thirdParty"/></td>
			<td align="left"><s:textfield name="systemDefault.thirdParty" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.lastMaster"/></td>
			<td align="left"><s:textfield name="systemDefault.lastMaster" required="true" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.numDays"/></td>
			<td align="left"><s:textfield name="systemDefault.numDays" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.doagntcde"/></td>
			<td align="left"><s:textfield name="systemDefault.doagntcde" required="true" cssClass="input-text" size="20"/></td>
		
			
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.special2"/></td>
			<td align="left"><s:textfield name="systemDefault.special2" required="true" maxlength="20" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.loadAtLast"/></td>
			<td align="left"><s:textfield name="systemDefault.loadAtLast" required="true" maxlength="8" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.special1"/></td>
			<td align="left"><s:textfield name="systemDefault.special1" required="true" maxlength="20"  cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.lastLoss"/></td>
			<td align="left"><s:textfield name="systemDefault.lastLoss" required="true" cssClass="input-text" size="20"/></td>
		
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.otherPcPay"/></td>
			<td align="left"><s:textfield name="systemDefault.otherPcPay" required="true" cssClass="input-text" size="20"/></td>
			
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.packScreen"/></td>
			<td align="left"><s:textfield name="systemDefault.packScreen" required="true" maxlength="258" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.mRegistrationAgent"/></td><td align="left"><s:textfield name="systemDefault.mRegistrationAgent" required="true" maxlength="10" cssClass="input-text" size="15"/></td>
		<td align="right" class="listwhitetext"><fmt:message key="systemDefault.doIndex"/></td>
		<td align="left"><s:textfield name="systemDefault.doIndex" required="true" maxlength="10" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.willAdvis"/></td>
			<td align="left"><s:textfield name="systemDefault.willAdvis" required="true" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.pdfprinter"/></td>
			<td align="left"><s:textfield name="systemDefault.pdfprinter" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr> <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.workDymension"/></td>
			<td align="left"><s:textfield name="systemDefault.workDymension" required="true" cssClass="input-text" size="20"/></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.runReports"/></td>
			<td align="left"><s:textfield name="systemDefault.runReports" required="true" maxlength="45" cssClass="input-text" size="20"/></td>
		</tr>
		

</tbody>
</table>
</div></td>

--><tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('quotforms')" style="margin: 0px;">    		
		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Quotation Forms Text
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>									
  		<div class="dspcont" id="quotforms">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  
 		 <tr>
 		    <td width="50px"></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.serviceInclude"/></td>
			<td align="left" ><s:textarea name="systemDefault.serviceInclude" cols="75" rows="5" cssClass="textarea"/></td>
		</tr>
		<tr><td height="10px"></td></tr>
		<tr>
		    <td></td>
			<td align="right" class="listwhitetext"><fmt:message key="systemDefault.serviceExclude"/></td>
			<td align="left" ><s:textarea name="systemDefault.serviceExclude" cols="75" rows="7" cssClass="textarea"/></td>
		</tr>

</tbody>
</table>
</div>
</td>
	</tr>
	<tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('networkConfiguration')" style="margin: 0px;">    		
		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Network Configuration
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>									
  		<div class="dspcont" id="networkConfiguration">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  
 		 <tr>
 		    <td width="40px"></td>
			<td align="right" class="listwhitetext">Default Coordinator</td>
			<td align="left"><s:select cssClass="list-menu"  name="systemDefault.networkCoordinator" list="%{coordinatorList}" headerKey="" headerValue="" cssStyle="width:180px"/></td>
		 </tr>
		<tr><td height="5px"></td></tr> 
</tbody>
</table>
</div>
</td>
	</tr>
	<configByCorp:fieldVisibility componentId="component.section.systemDefault.pension">	
		<tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('pensionDefault')" style="margin: 0px;">    		
		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Pension Default
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>									
  		<div class="dspcont" id="pensionDefault">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  
 		 <tr>
 		    <td width="60px"></td>
			<td align="right" class="listwhitetext">Effective Date1</td>
				<c:if test="${not empty systemDefault.pensionEffectiveDate1}">
				<s:text id="pensionEffectiveDate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate1"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate1" name="systemDefault.pensionEffectiveDate1" value="%{pensionEffectiveDate1FormattedValue}"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.pensionEffectiveDate1}">
				<s:text id="systemDefault.postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate1"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate1" name="systemDefault.pensionEffectiveDate1"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<td width="50px"></td>
		 	<td align="right" class="listwhitetext">Rate1</td>
			<td align="left"><s:textfield name="systemDefault.pensionRate1" required="true" cssClass="input-text" size="5" maxlength="9" onchange="onlyFloat(this);"/></td>	
			     
		 </tr>
		 <tr>
 		    <td width="50px"></td>
			<td align="right" class="listwhitetext">Effective Date2</td>
				<c:if test="${not empty systemDefault.pensionEffectiveDate2}">
				<s:text id="pensionEffectiveDate2FormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate2"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate2" name="systemDefault.pensionEffectiveDate2" value="%{pensionEffectiveDate2FormattedValue}"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.pensionEffectiveDate2}">
				<s:text id="systemDefault.postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate2"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate2" name="systemDefault.pensionEffectiveDate2"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<td width="50px"></td>
		 	<td align="right" class="listwhitetext">Rate2</td>
			<td align="left"><s:textfield name="systemDefault.pensionRate2" required="true" cssClass="input-text" size="5" maxlength="9" onchange="onlyFloat(this);"/></td>	
			     
		 </tr>
		 <tr>
 		    <td width="50px"></td>
			<td align="right" class="listwhitetext">Effective Date3</td>
				<c:if test="${not empty systemDefault.pensionEffectiveDate3}">
				<s:text id="pensionEffectiveDate3FormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate3"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate3" name="systemDefault.pensionEffectiveDate3" value="%{pensionEffectiveDate3FormattedValue}"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate3_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.pensionEffectiveDate3}">
				<s:text id="systemDefault.postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate3"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate3" name="systemDefault.pensionEffectiveDate3"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate3_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<td width="50px"></td>
		 	<td align="right" class="listwhitetext">Rate3</td>
			<td align="left"><s:textfield name="systemDefault.pensionRate3" required="true" cssClass="input-text" size="5" maxlength="9" onchange="onlyFloat(this);"/></td>	
			     
		 </tr>
		 <tr>
 		    <td width="50px"></td>
			<td align="right" class="listwhitetext">Effective Date4</td>
				<c:if test="${not empty systemDefault.pensionEffectiveDate4}">
				<s:text id="pensionEffectiveDate4FormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate4"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate4" name="systemDefault.pensionEffectiveDate4" value="%{pensionEffectiveDate4FormattedValue}"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<c:if test="${empty systemDefault.pensionEffectiveDate4}">
				<s:text id="systemDefault.postDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="systemDefault.pensionEffectiveDate4"/></s:text>
		  		<td align="left" ><s:textfield id="pensionEffectiveDate4" name="systemDefault.pensionEffectiveDate4"  maxlength="10" size="8"  cssClass="input-text" onkeydown="return onlyDel(event,this)"/><img id="pensionEffectiveDate4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		</c:if>
		  		<td width="50px"></td>
		 	<td align="right" class="listwhitetext">Rate4</td>
			<td align="left"><s:textfield name="systemDefault.pensionRate4" required="true" cssClass="input-text" size="5" maxlength="9" onchange="onlyFloat(this);"/></td>	
			     
		 </tr>
		<tr><td height="10px"></td></tr> 
</tbody>
</table>
</div>
</td>
	</tr>
	
</configByCorp:fieldVisibility>		
	  <configByCorp:fieldVisibility componentId="component.systemDefault.officeMove">
	<tr>
 		<td height="10" width="100%" align="left" >
 		<div  onClick="javascript:animatedcollapse.toggle('officeMove')" style="margin: 0px;">    		
		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
				<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Office Move
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
		</tr>
		</table>
		</div>									
  		<div class="dspcont" id="officeMove">
		  <table class="detailTabLabel123" border="0">
 		  <tbody>
 		  <tr> 		   
			<td align="right" class="listwhitetext" width="132">Service</td>
			<td align="left"><s:select cssClass="list-menu"  name="systemDefault.service" list="%{tcktservc}" cssStyle="width:180px"/></td>
		  <td width="30px"></td>
		 <td align="right" class="listwhitetext">Warehouse</td>
			<td align="left"><s:select cssClass="list-menu"  name="systemDefault.warehouse" list="%{house}" cssStyle="width:180px"/></td>
		 </tr>
		<tr><td height="5px"></td></tr> 
 		  </tbody>
 		  </table>
		</div>
		</td>
		</tr>
	</configByCorp:fieldVisibility>
	<tr><td height="10px"></td></tr>	
  </tbody>
  </table> 
 </div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
</div>        

<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td> 			     
 		  
   <tr><td>
 		 <table class="detailTabLabel" border="0" style="width:700px">
				<tbody>
					<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
						
						
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='systemDefault.createdOn'/></td>
							<fmt:formatDate var="systemDefaultCreatedOnFormattedValue" value="${systemDefault.createdOn}" 
								pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="container.createdOn" value="${systemDefaultCreatedOnFormattedValue}" />
							<td style="width:140px"><fmt:formatDate value="${systemDefault.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<!--<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.createdOn'/></b></td>
							<s:hidden name="systemDefault.createdOn" />
							<td style="width:140px"><fmt:formatDate value="${systemDefault.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							--><td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.createdBy' /></b></td>
							<c:if test="${not empty systemDefault.id}">
								<s:hidden name="systemDefault.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{systemDefault.createdBy}"/></td>
							</c:if>
							
							<c:if test="${empty systemDefault.id}">
								<s:hidden name="systemDefault.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.updatedOn'/></b></td>
							<s:hidden name="systemDefault.updatedOn"/>
							<td style="width:140px"><fmt:formatDate value="${systemDefault.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.updatedBy' /></b></td>
							
														
							<c:if test="${not empty systemDefault.id}">

									<s:hidden name="customerFile.updatedBy"/>
									
									<td style="width:85px"><s:label name="updatedBy" value="%{systemDefault.updatedBy}"/></td>
								
							</c:if>
							
							<c:if test="${empty systemDefault.id}">
								<s:hidden name="systemDefault.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table></td></tr>


            <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save" onclick="return isNumeric(),isURL(),checkInsurance();"/>
             <td></td>   
            <input class="cssbutton1" type="button" value="Reset" style="width:55px;" onclick="this.form.reset();getState(document.forms['systemDefaultForm'].elements['systemDefault.country']);"/>

</s:form>   
  
<script type="text/javascript">   
    Form.focusFirstElement($("systemDefaultForm")); 
  try{
     autoPopulate_country(document.forms['systemDefaultForm'].elements['systemDefault.country']);
     }
   catch(e){}  
 try{
    if('${stateshitFlag}'!= "1"){ 
  getState(document.forms['systemDefaultForm'].elements['systemDefault.country']);
   }
   }
  catch(e){}
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	
	function titleCase(element){ 
        var txt=element.value; var spl=txt.split(" "); 
        var upstring=""; for(var i=0;i<spl.length;i++){ 
        try{ 
        upstring+=spl[i].charAt(0).toUpperCase(); 
        }catch(err){} 
        upstring+=spl[i].substring(1, spl[i].length); 
        upstring+=" ";   
        } 
        element.value=upstring.substring(0,upstring.length-1); 
        }
</script>
<script type="text/javascript">
try{
	desableInsurance();
	showVlidField();
}catch(e){
	
}
</script>
