ALTER TABLE `redsky`.`billing` ADD COLUMN `contractCurrency` VARCHAR(3) DEFAULT NULL AFTER `vendorBillingCurency`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `payableContractCurrency` VARCHAR(3) DEFAULT NULL AFTER `contractCurrency`;

ALTER TABLE `redsky`.`company` ADD COLUMN `postingDateFlexibility` BIT(1) DEFAULT b'0' AFTER `ftpDir`;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('AccountLine','storageDateRangeFrom' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','storageDateRangeFrom' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','storageDateRangeTo' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','storageDateRangeTo' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','storageDays' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','storageDays' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','onHand' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('AccountLine','onHand' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM');

DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`FindsumofActualRevenue_Customerfile_Initiation` $$
CREATE DEFINER=`root`@`%` FUNCTION `FindsumofActualRevenue_Customerfile_Initiation`(v1 VARCHAR(50),v2 varchar(50),billcode varchar(20),fromdate DATETIME,todate DATETIME) RETURNS double
    DETERMINISTIC
BEGIN
    DECLARE avg double;
    DECLARE i1 double;
    DECLARE i2 double;
    DECLARE i3 double;
select concat('\'',replace(billcode, ',','\',\''),'\'') into i2;
 set i3=i2;
         select sum(a.actualrevenue) into i1 from customerfile c Inner Join serviceorder s on c.id=s.customerfileid and s.corpid='UTSI'
Inner Join billing b on s.id=b.id and b.corpid='UTSI'
Left Outer join accountline a on s.id=a.serviceorderid and a.corpid='UTSI'
and a.status is true and a.activateaccportal is true and a.actualrevenue!='0.00'
Inner Join contract co on a.contract=co.contract and co.corpid='UTSI'
Inner Join charges ch on ch.charge=a.chargecode and ch.contract=co.contract and ch.corpid='UTSI'
Inner Join costelement ce on ch.costelement=ce.costelement and ce.corpid='UTSI'
where  ce.reportingalias in ('Accessorials','broker')
 and a.category in ('Accessorials','broker')
and (a.recinvoicenumber is not null and a.recinvoicenumber!='')
and c.corpid='UTSI' and c.sequencenumber=v1 and s.shipnumber=v2 and a.chargecode not in('Prepaid services')
and  b.billtocode in (i3)
and c.billtocode in (i3)
and a.billtocode in (i3)
and s.billtocode in (i3)
and c.initialContactDate between  fromdate and todate;

    SET avg = i1;

      RETURN avg;
   END $$

DELIMITER ;


DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`FindsumofActualRevenueofallserviceorder` $$
CREATE DEFINER=`root`@`%` FUNCTION `FindsumofActualRevenueofallserviceorder`(v1 VARCHAR(50),v2 varchar(50),billcode varchar(20),fromdate DATETIME,todate DATETIME) RETURNS double
    DETERMINISTIC
BEGIN
    DECLARE avg double;
    DECLARE i1 double;
    DECLARE i2 double;
    DECLARE i3 double;
select concat('\'',replace(billcode, ',','\',\''),'\'') into i2;
 set i3=i2;
         select sum(a.actualrevenue) into i1 from customerfile c Inner Join serviceorder s on c.id=s.customerfileid and s.corpid='UTSI'
Inner Join billing b on s.id=b.id and b.corpid='UTSI'
Left Outer join accountline a on s.id=a.serviceorderid and a.corpid='UTSI'
and a.status is true and a.activateaccportal is true and a.actualrevenue!='0.00'
Inner Join contract co on a.contract=co.contract and co.corpid='UTSI'
Inner Join charges ch on ch.charge=a.chargecode and ch.contract=co.contract and ch.corpid='UTSI'
Inner Join costelement ce on ch.costelement=ce.costelement and ce.corpid='UTSI'
where  ce.reportingalias in ('Accessorials','broker')
 and a.category in ('Accessorials','broker')
and (a.recinvoicenumber is not null and a.recinvoicenumber!='')
and c.corpid='UTSI' and c.sequencenumber=v1 and s.shipnumber=v2 and a.chargecode not in('Prepaid services')
and  b.billtocode in (i3)
and c.billtocode in (i3)
and a.billtocode in (i3)
and s.billtocode in (i3)
and a.receivedinvoicedate between  fromdate and todate;

    SET avg = i1;

      RETURN avg;
   END $$

DELIMITER ;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('Billing','storageVatDescr' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'), ('Billing','storageVatDescr' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'), ('Billing','storageVatPercent' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'), ('Billing','storageVatPercent' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('Billing','billingCurrency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'), ('Billing','billingCurrency' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('Billing','contractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('Billing','contractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`commission` ADD COLUMN `companyDivision` VARCHAR(10) DEFAULT NULL AFTER `chargeCode`;



