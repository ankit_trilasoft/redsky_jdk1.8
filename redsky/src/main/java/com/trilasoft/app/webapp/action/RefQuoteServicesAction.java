package com.trilasoft.app.webapp.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.RefQuoteServices;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RefQuoteServicesManager;
import com.trilasoft.app.webapp.util.GetSortedMap;

public class RefQuoteServicesAction extends BaseAction implements Preparable {
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String langauge;
	private String mode;
	private Date jobType;
	private String route;
	/*private String code;*/
	private String description;
	private String checkIncludeExclude ;
	private Boolean refQuoteServicesDefault;
	private RefQuoteServicesManager refQuoteServicesManager;
	private RefMasterManager refMasterManager;
	private List refQuoteServiceList;
    private RefQuoteServices refQuoteServices;
    private String sessionCorpID;
    private String userName;
    private Map<String,String> localParameter;
    private Map<String,String> modeMap;
    private Map<String,String> job;
    private Map<String,String> routing;
    private Map<String,String> lang;
    private String uniqueCodeLi;
    private String validateFormNav;
    private Company company;
    private CompanyManager companyManager;
    private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>();
    Map<String, String> checkIncludeExcludeList = new HashMap<String, String>();
    private String refQuoteServicesDefaultCheck;
    static final Logger logger = Logger.getLogger(RefQuoteServicesAction.class);
    Date currentdate = new Date();
    private String userQuoteServices;
    private Map<String, String> pkmode;
    private Map<String, String> commodits;
    private Map<String, String> ocountry;
    private List companyDivis=new ArrayList();
    private CustomerFileManager customerFileManager;
    private Map<String, String> contract;
    Map<String, String> checkIncludeExcludeDocumentList = new HashMap<String, String>();
	private  Map<String, String> service;
    public RefQuoteServicesAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        this.userName = user.getUsername();
    }
	public void prepare() throws Exception {
		if(getRequest().getParameter("ajax") == null){ 
			getComboList(sessionCorpID);
			 company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
				 if((company.getQuoteServices()!=null)&&(!company.getQuoteServices().equalsIgnoreCase(""))){
					 	userQuoteServices=company.getQuoteServices();
					 }else{
						 userQuoteServices="N/A";
					 }				 
			 }
		}
		checkIncludeExcludeList.put("N", "Exclude");
		checkIncludeExcludeList.put("Y", "Include");
		
		checkIncludeExcludeDocumentList.put("N", "Exclude");
		checkIncludeExcludeDocumentList.put("Y", "Include");
		checkIncludeExcludeDocumentList.put("D", "Document");
	}
	
	
	 public String getComboList(String corpId){
	Long StartTime = System.currentTimeMillis();
    
	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	     try {
    	    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Getting parameter values from combolist");
    	    	 String parameters=" 'JOB','ROUTING','MODE','LANGUAGE','PKMODE','COMMODIT'";
    	    	 logger.warn("Parameters: "+parameters);
    	    	 LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
    	    	 modeMap = new TreeMap<String, String>();
    	    	 routing = new TreeMap<String, String>();
    			 job = new TreeMap<String, String>();
    			 lang=new TreeMap<String, String>();
    			 pkmode = new TreeMap<String, String>();
    			 commodits = new TreeMap<String, String>();
    			 contract = new TreeMap<String, String>();
    			  List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
    	    	 for (RefMasterDTO refObj : allParamValue) {
    	    		 if(refObj.getParameter().trim().equals("LANGUAGE")){
    	    			 if(refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") ){
    	    				 lang.put(refObj.getFlex1().trim(), refObj.getDescription().trim());
    	    			 }
	    	    	}else if(refObj.getParameter().trim().equals("MODE")){
	    	    		modeMap.put( refObj.getDescription().trim(),  refObj.getDescription().trim());
	    	    	}else{
	    	    		 if(parameterMap.containsKey(refObj.getParameter().trim())){
		    		 			tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
		    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		    		 		}else{
		    		 			tempParemeterMap = new LinkedHashMap<String, String>();
		    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		    		 		}
	    	    	}
    	    	 }
    	    	 
    	    	 	pkmode = findByParameterLocal("PKMODE");
					pkmode = GetSortedMap.sortByValues(pkmode);
					commodits = findByParameterLocal("COMMODIT");
					service = refMasterManager.findByParameter(corpId, "SERVICE");
					contract=refQuoteServicesManager.findForContract(corpId);
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: pkmode "+pkmode);
    	    	 routing = findByParameterLocal("ROUTING");
    	    	 job = findByParameterLocal("JOB");
    	    	 ocountry = refMasterManager.findCountry(corpId, "COUNTRY");
    			 companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,"");
    	    	 
    	    	 
    	     }catch (Exception e) {
    	    	 logger.warn("Exception in getting combo list "+e.getStackTrace()[0]);
			}
    	     return SUCCESS;
	 }
	 
	//  Method for getting the values for all comboboxes in the dropdown list.
	 @SkipValidation
	 public Map<String,String> findByParameterLocal(String parameter){ 
	 	localParameter = new HashMap<String, String>();
	 	if(parameterMap.containsKey(parameter)){
	 		localParameter = parameterMap.get(parameter);
	 	}
	 	return localParameter;
	 }
	
		public String list(){
			refQuoteServices = new RefQuoteServices();
			refQuoteServiceList = refQuoteServicesManager.searchList("","","","",sessionCorpID,"","","","","","","","",""); 
		   		return SUCCESS;
		}
		@SkipValidation
		public String inclusionExclusioReport(){
			List <RefQuoteServices> refQuoteServiceList = refQuoteServicesManager.searchList(
					refQuoteServices.getJob() == null ? "" : refQuoteServices.getJob(),
					refQuoteServices.getMode() == null ? "" : refQuoteServices.getMode(),
					refQuoteServices.getRouting() == null ? "" : refQuoteServices.getRouting(),
					refQuoteServices.getLangauge() == null ? "" : refQuoteServices.getLangauge(),
					sessionCorpID, 
					refQuoteServices.getDescription() == null ? "": refQuoteServices.getDescription(),
					refQuoteServices.getCheckIncludeExclude() == null ? "": refQuoteServices.getCheckIncludeExclude(),
					refQuoteServices.getCompanyDivision() == null ? "": refQuoteServices.getCompanyDivision(),						
					refQuoteServices.getCommodity() == null ? "": refQuoteServices.getCommodity(),		
					refQuoteServices.getPackingMode() == null ? "": refQuoteServices.getPackingMode(),		
					refQuoteServices.getOriginCountry() == null ? "": refQuoteServices.getOriginCountry(),	
					refQuoteServices.getDestinationCountry() == null ? "": refQuoteServices.getDestinationCountry(),
					refQuoteServices.getServiceType() == null ? "" : refQuoteServices.getServiceType(),
					refQuoteServices.getContract() == null ? "" : refQuoteServices.getContract()); 
			try{
			 HttpServletResponse response = getResponse();
			 response.setContentType("application/vnd.ms-excel");
				ServletOutputStream outputStream = response.getOutputStream();
				File file = new File("InclusioExclusion");
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write("Job Type\t".getBytes()); 
				outputStream.write("Mode\t".getBytes());
				outputStream.write("Route\t".getBytes());
				outputStream.write("Description\t".getBytes());
				outputStream.write("Order By\t".getBytes());
				outputStream.write("Incl/Excl\t".getBytes());
				outputStream.write("Pack Mode\t".getBytes());
				outputStream.write("Commodity\t".getBytes());
				outputStream.write("Origin Country\t".getBytes());	
				outputStream.write("Destination Country\t".getBytes());	
				outputStream.write("Company Division\t".getBytes());
				outputStream.write("Serv. Default\t".getBytes());
				outputStream.write("Service Type\t".getBytes());
				
				outputStream.write("\r\n".getBytes());
				if(!(refQuoteServiceList.isEmpty())){
					for(RefQuoteServices ref:refQuoteServiceList){
						if (ref.getJob() != null) {
							outputStream.write(("" + ref.getJob().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}

						if (ref.getMode() != null) {
							outputStream.write(("" + ref.getMode().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getRouting() != null) {
							outputStream.write(("" + ref.getRouting().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getDescription() != null) {
							outputStream.write(("" + ref.getDescription().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getDisplayOrder() != null) {
							outputStream.write(("" + ref.getDisplayOrder().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if(ref.getCheckIncludeExclude() != null && ref.getCheckIncludeExclude().equalsIgnoreCase("Y")) {
							outputStream.write(("Include\t").getBytes());
						}else if (ref.getCheckIncludeExclude() != null && ref.getCheckIncludeExclude().equalsIgnoreCase("N")) {
							outputStream.write(("Exclude\t").getBytes());
						}else{ 
							outputStream.write("\t".getBytes());
						}
						if (ref.getPackingMode() != null) {
							outputStream.write(("" + ref.getPackingMode().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getCommodity() != null) {
							outputStream.write(("" + ref.getCommodity().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getOriginCountry() != null) {
							outputStream.write(("" + ref.getOriginCountry().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getDestinationCountry() != null) {
							outputStream.write(("" + ref.getDestinationCountry().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getCompanyDivision() != null) {
							outputStream.write(("" + ref.getCompanyDivision().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						if (ref.getRefQuoteServicesDefault() != null && ref.getRefQuoteServicesDefault()) {
							outputStream.write(("TRUE\t").getBytes());
						} else { 
							outputStream.write("FALSE\t".getBytes());
						}
						if (ref.getServiceType() != null) {
							outputStream.write(("" + ref.getServiceType().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						}
						outputStream.write("\r\n".getBytes());
					}
				}else{
					outputStream.write(("" +"No Data found" + "\t").getBytes());
				}
				outputStream.flush();
				outputStream.close();
			}catch(Exception e){
		    	 e.printStackTrace();
			}
			return SUCCESS;
		}
		public String searchList(){
		refQuoteServiceList = refQuoteServicesManager.searchList(
				refQuoteServices.getJob() == null ? "" : refQuoteServices.getJob(),
				refQuoteServices.getMode() == null ? "" : refQuoteServices.getMode(),
				refQuoteServices.getRouting() == null ? "" : refQuoteServices.getRouting(),
				refQuoteServices.getLangauge() == null ? "" : refQuoteServices.getLangauge(),
				sessionCorpID, 
				refQuoteServices.getDescription() == null ? "": refQuoteServices.getDescription(),
				refQuoteServices.getCheckIncludeExclude() == null ? "": refQuoteServices.getCheckIncludeExclude(),
				refQuoteServices.getCompanyDivision() == null ? "": refQuoteServices.getCompanyDivision(),						
				refQuoteServices.getCommodity() == null ? "": refQuoteServices.getCommodity(),		
				refQuoteServices.getPackingMode() == null ? "": refQuoteServices.getPackingMode(),		
				refQuoteServices.getOriginCountry() == null ? "": refQuoteServices.getOriginCountry(),	
				refQuoteServices.getDestinationCountry() == null ? "": refQuoteServices.getDestinationCountry(),
				refQuoteServices.getServiceType() == null ? "" : refQuoteServices.getServiceType(),
				refQuoteServices.getContract() == null ? "" : refQuoteServices.getContract()); 
		
			 return SUCCESS;
			}
		
		public String edit() {
			 if (id != null) {
				 	refQuoteServices = refQuoteServicesManager.get(id);
		        }else{
		        	refQuoteServices = new RefQuoteServices();
		        }
	        return SUCCESS;
	    }
		public String deleteQuoteServices() {
			 refQuoteServicesManager.remove(id);
		    	saveMessage(getText("RefQuoteService is deleted."));
		    	   list();
		    	 return SUCCESS;
		    	}
		 @SkipValidation	
		 public String save()  {
			   boolean isNew = (id == null);	
			   
				 
					   if(isNew){
						   refQuoteServices.setCorpID(sessionCorpID);
						   refQuoteServices.setCreatedBy(userName);
						 refQuoteServices.setCreatedOn(new Date());
					   }
						 refQuoteServices.setUpdatedBy(userName);
						   refQuoteServices.setUpdatedOn(new Date());
						   refQuoteServices.setCreatedOn(refQuoteServices.getCreatedOn());
						   if(refQuoteServicesDefaultCheck !=null && refQuoteServicesDefaultCheck.equalsIgnoreCase("yes")){
							   refQuoteServices.setRefQuoteServicesDefault(true);
						   }else{
							   refQuoteServices.setRefQuoteServicesDefault(false); 
						   }
						   if((userQuoteServices!=null)&&(!userQuoteServices.equalsIgnoreCase(""))&&(userQuoteServices.equalsIgnoreCase("Separate section"))){}else{
							   if(!refQuoteServices.getCheckIncludeExclude().equalsIgnoreCase("D")){
								   refQuoteServices.setCheckIncludeExclude("N");
							   }
						   }
						   try{
						   refQuoteServices =refQuoteServicesManager.save(refQuoteServices);
						  String key = (isNew) ? "refQuoteSevices.added" : "refQuoteSevices.updated";
					        saveMessage(getText(key));
						   }
						 catch(Exception exc){
							 exc.printStackTrace();
							 errorMessage("Code Is Already Exist.");
							 return INPUT;
						 }
						   
						   id=refQuoteServices.getId();
						   list();
				   return SUCCESS; 
		 }
		 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getLangauge() {
		return langauge;
	}
	public void setLangauge(String langauge) {
		this.langauge = langauge;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public Date getJobType() {
		return jobType;
	}
	public void setJobType(Date jobType) {
		this.jobType = jobType;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	/*public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}*/
	public String getdescription() {
		return description;
	}
	public void setdescription(String description) {
		this.description = description;
	}
	public String getCheckIncludeExclude() {
		return checkIncludeExclude;
	}
	public void setCheckIncludeExclude(String checkIncludeExclude) {
		this.checkIncludeExclude = checkIncludeExclude;
	}
	public Boolean getRefQuoteServicesDefault() {
		return refQuoteServicesDefault;
	}
	public void setRefQuoteServicesDefault(Boolean refQuoteServicesDefault) {
		this.refQuoteServicesDefault = refQuoteServicesDefault;
	}



	public List getRefQuoteServiceList() {
		return refQuoteServiceList;
	}

	@SkipValidation
	public String autoSaveRefQuotesSevisesAjax(){
		String fieldName=getRequest().getParameter("fieldName");
		String fieldVal=getRequest().getParameter("fieldVal");
		if(fieldVal==null || fieldVal.toString().trim().equalsIgnoreCase("")){
			fieldVal=null;
		}
		String fieldId=getRequest().getParameter("fieldId");
		if(fieldId!=null){
			Long serviceId=Long.parseLong(fieldId);
			refQuoteServicesManager.autoSaveRefQuotesSevisesAjax(sessionCorpID,fieldName,fieldVal,serviceId);
		}
		return SUCCESS;
	}

	public void setRefQuoteServiceList(List refQuoteServiceList) {
		this.refQuoteServiceList = refQuoteServiceList;
	}
	public void setRefQuoteServicesManager(
			RefQuoteServicesManager refQuoteServicesManager) {
		this.refQuoteServicesManager = refQuoteServicesManager;
	}



	public RefQuoteServices getRefQuoteServices() {
		return refQuoteServices;
	}



	public void setRefQuoteServices(RefQuoteServices refQuoteServices) {
		this.refQuoteServices = refQuoteServices;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Map<String, String> getLocalParameter() {
		return localParameter;
	}

	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}

	public Map<String, LinkedHashMap<String, String>> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(
			Map<String, LinkedHashMap<String, String>> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getModeMap() {
		return modeMap;
	}
	public void setModeMap(Map<String, String> modeMap) {
		this.modeMap = modeMap;
	}
	public Map<String, String> getJob() {
		return job;
	}
	public void setJob(Map<String, String> job) {
		this.job = job;
	}
	public Map<String, String> getRouting() {
		return routing;
	}
	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}
	public Map<String, String> getLang() {
		return lang;
	}
	public void setLang(Map<String, String> lang) {
		this.lang = lang;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUniqueCodeLi() {
		return uniqueCodeLi;
	}
	public void setUniqueCodeList(String uniqueCodeList) {
		this.uniqueCodeLi = uniqueCodeLi;
	}
	public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public Map<String, String> getCheckIncludeExcludeList() {
		return checkIncludeExcludeList;
	}
	public void setCheckIncludeExcludeList(
			Map<String, String> checkIncludeExcludeList) {
		this.checkIncludeExcludeList = checkIncludeExcludeList;
	}
	public String getRefQuoteServicesDefaultCheck() {
		return refQuoteServicesDefaultCheck;
	}
	public void setRefQuoteServicesDefaultCheck(String refQuoteServicesDefaultCheck) {
		this.refQuoteServicesDefaultCheck = refQuoteServicesDefaultCheck;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getUserQuoteServices() {
		return userQuoteServices;
	}
	public void setUserQuoteServices(String userQuoteServices) {
		this.userQuoteServices = userQuoteServices;
	}
	public Map<String, String> getPkmode() {
		return pkmode;
	}
	public void setPkmode(Map<String, String> pkmode) {
		this.pkmode = pkmode;
	}
	public Map<String, String> getCommodits() {
		return commodits;
	}
	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}
	public List getCompanyDivis() {
		return companyDivis;
	}
	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public Map<String, String> getCheckIncludeExcludeDocumentList() {
		return checkIncludeExcludeDocumentList;
	}
	public void setCheckIncludeExcludeDocumentList(
			Map<String, String> checkIncludeExcludeDocumentList) {
		this.checkIncludeExcludeDocumentList = checkIncludeExcludeDocumentList;
	}
	public Map<String, String> getService() {
		return service;
	}
	public void setService(Map<String, String> service) {
		this.service = service;
	}
	public Map<String, String> getContract() {
		return contract;
	}
	public void setContract(Map<String, String> contract) {
		this.contract = contract;
	}
	


}
