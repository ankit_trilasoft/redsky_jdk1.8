package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.AccountContactManager;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;

public class AccountContactAction extends BaseAction implements Preparable {
	
	private Long id;
	private Long pID;
	private AccountContactManager accountContactManager;
	private RefMasterManager refMasterManager;
	private AccountContact accountContact;
	//private Partner partner;
	private PartnerManager partnerManager;
	private PartnerPublic partner;
	private PartnerPublicManager partnerPublicManager;
	private List accountContacts;
	private String sessionCorpID;
	private String countAccountContactNotes;
	private NotesManager notesManager;
	private Notes notes;
	private String hitFlag;
	private String noteFor;
	private AccountProfileManager accountProfileManager;
	private String fromPage;
	private Map<String, String> states;
	private Map<String, String> countryDesc;
	private CustomerFileManager customerFileManager;
	private String usertype;	/* Added for ticket number: 6176 */
	private List multiplJobTypes;
	private List userNameList;
	private Company company;
	private CompanyManager companyManager;
	private Boolean checkTransfereeInfopackage;
	private Map<String, String> countryCod;
	private String enbState;
	
	/* Added By Kunal for ticket number: 6879 */
	private List crmLeadParameter;
	/* Modification closed */
	
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(AccountContactAction.class);

	 

	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID, "a");
		states = customerFileManager.findDefaultStateList("", sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		enbState = customerFileManager.enableStateList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
}
	
	public AccountContactAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        usertype = user.getUserType();
        this.sessionCorpID = user.getCorpID();
        
	}
	
	public AccountContact getAccountContact() {
		return accountContact;
	}
	public void setAccountContact(AccountContact accountContact) {
		this.accountContact = accountContact;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setAccountContactManager(AccountContactManager accountContactManager) {
		this.accountContactManager = accountContactManager;
	}
	
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(id);
		//accountContacts = partner.getAccountContacts();
		accountContacts = accountContactManager.getAccountContacts(partner.getPartnerCode(), sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private List contractNotes;
	private List notess;
	public String getNotesForContract(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(id);
		contractNotes=accountContactManager.getNotesForContract(partner.getPartnerCode(),sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private Long id1;
	private static List owner;
	public String relatedNotesForContract(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(id);
		accountContact=accountContactManager.get(id1);
		//System.out.println("\n\n\n\npartnerCode-->"+accountContact.getPartnerCode());
		notess=accountContactManager.getRelatedNotesForContract(sessionCorpID,accountContact.getPartnerCode());
		//System.out.println("\n\n\n\nnotess-->"+notess);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String notesListFromAccountContact() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accountContact = accountContactManager.get(id);
        List m = notesManager.countAccountContactNotes(accountContact.getPartnerCode(), sessionCorpID,accountContact.getId());
		
		if (m.isEmpty()) {
			countAccountContactNotes = "0";
		} else {
			countAccountContactNotes = ((m).get(0)).toString();
		}
		notess = notesManager.getListByAccountNotesId(accountContact.getPartnerCode(), noteFor, accountContact.getJobType(), accountContact.getId());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(id != null ){
			accountContact = accountContactManager.get(id);
			List countryCode = partnerManager.getCountryCode(accountContact.getCountry());
			try {
				if (!countryCode.isEmpty()) {
					states = customerFileManager.findDefaultStateList(countryCode.get(0).toString(), sessionCorpID);
				} else {
					states = customerFileManager.findDefaultStateList(accountContact.getCountry(), sessionCorpID);
				}
			
				multiplJobTypes = new ArrayList();
				String[] ac = accountContact.getJobType().split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					if (accConJobType.indexOf("(") == 0) {
						accConJobType = accConJobType.substring(1);
					}
					if (accConJobType.lastIndexOf(")") == accConJobType.length() - 1) {
						accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
					}
					if (accConJobType.indexOf("'") == 0) {
						accConJobType = accConJobType.substring(1);
					}
					if (accConJobType.lastIndexOf("'") == accConJobType.length() - 1) {
						accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
					}

					multiplJobTypes.add(accConJobType.trim());
				}
			} catch (Exception ex) {
				
			}

			//accountContact.setUpdatedOn(new Date());
			//partner = (PartnerPublic) partnerPublicManager.findPartnerCode(accountContact.getPartnerCode()).get(0);
			partner = partnerPublicManager.get(pID);
		}else{
			partner = partnerPublicManager.get(pID);
			accountContact = new AccountContact();
			accountContact.setUpdatedOn(new Date());
			
		}
		//getComboList(sessionCorpID, "a");
		userNameList = partnerPublicManager.getUserNameList(sessionCorpID, partner.getPartnerCode());
		getNotesForIconChange();
		if(fromPage != null && fromPage.equalsIgnoreCase("View")){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "view";
		}else{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	
	public String editNewAccountContact(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(pID);
		accountContact = new AccountContact();
		accountContact.setPartnerCode(partner.getPartnerCode());
		accountContact.setCorpID(sessionCorpID);
		accountContact.setCreatedOn(new Date());
		accountContact.setUpdatedOn(new Date());
		accountContact.setStatus(true);
		accountContact.setAddress(partner.getBillingAddress1());
		accountContact.setCountry(partner.getBillingCountry());
		accountContact.setCity(partner.getBillingCity());
		accountContact.setZip(partner.getBillingZip());
		List countryCode = partnerManager.getCountryCode(partner.getBillingCountry());
		try {
			if (!countryCode.isEmpty()) {
				states = customerFileManager.findDefaultStateList(countryCode.get(0).toString(), sessionCorpID);
			} else {
				states = customerFileManager.findDefaultStateList(partner.getBillingCountry(), sessionCorpID);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		accountContact.setState(partner.getBillingState());
		
		//getComboList(sessionCorpID, "a");
		userNameList = partnerPublicManager.getUserNameList(sessionCorpID, partner.getPartnerCode());
		getNotesForIconChange();
		if(fromPage != null && fromPage.equalsIgnoreCase("View")){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "view";
		}else{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	
	public String save() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(pID);
		boolean isNew = (accountContact.getId() == null);
		if(isNew){
			accountContact.setCreatedOn(new Date());
		}
		accountContact.setUpdatedOn(new Date());
		accountContact.setUpdatedBy(getRequest().getRemoteUser());
		accountContact = accountContactManager.save(accountContact);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Account Contact has been added successfuly" : "Account Contact has been updated successfuly";
		saveMessage(getText(key));
		}
		//getComboList(sessionCorpID, "a");
		userNameList = partnerPublicManager.getUserNameList(sessionCorpID, partner.getPartnerCode());
		getNotesForIconChange();
		
		if(fromPage != null && fromPage.equalsIgnoreCase("View")){
			hitFlag="2";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "view";
		}else{
			list();
			hitFlag="1";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	private static Map<String, String> jobtype;
	private static Map<String, String> prifix;
	private static Map<String, String> phoneTypes;
	private static Map<String, String> contact_frequency;
	public String getComboList(String corpId, String jobType) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		jobtype = refMasterManager.findByParameter(corpId, "JOB");
		contact_frequency = refMasterManager.findByParameter(corpId, "CONT_FREQNECY");
		owner= accountProfileManager.getAccountOwner(sessionCorpID);
		countryDesc = refMasterManager.findCountry(corpId, "COUNTRY");
		crmLeadParameter = refMasterManager.findByParameters(corpId, "CRMLead");
		prifix = refMasterManager.findCountry(corpId, "PREFFIX");
		countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
		phoneTypes = new LinkedHashMap<String, String>();
		phoneTypes.put("OFF","Office");
		phoneTypes.put("MOB","Mobile");
		phoneTypes.put("FAX","Fax");
		phoneTypes.put("RES","Home"); 						/* Modifications Made For Ticket Number: 5946 */
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			accountContactManager.remove(id);
			saveMessage(getText("Account Contact has been deleted successfully."));
		} catch (Exception ex) {
			accountContacts = accountContactManager.getAccountContacts(partner.getPartnerCode(), sessionCorpID);
		}
		id=pID;
		list();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	
	public  Map<String, String> getJobtype() {
		return jobtype;
	}
	public List getAccountContacts() {
		return accountContacts;
	}
	public void setAccountContacts(List accountContacts) {
		this.accountContacts = accountContacts;
	}
	public PartnerPublic getPartner() {
		return partner;
	}
	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public static Map<String, String> getPhoneTypes() {
		return phoneTypes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List m = notesManager.countAccountContactNotes(partner.getPartnerCode(), sessionCorpID,accountContact.getId());
		
		if (m.isEmpty()) {
			countAccountContactNotes = "0";
		} else {
			countAccountContactNotes = ((m).get(0)).toString();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getCountAccountContactNotes() {
		return countAccountContactNotes;
	}

	public void setCountAccountContactNotes(String countAccountContactNotes) {
		this.countAccountContactNotes = countAccountContactNotes;
	}

	public static Map<String, String> getContact_frequency() {
		return contact_frequency;
	}

	public static void setContact_frequency(Map<String, String> contact_frequency) {
		AccountContactAction.contact_frequency = contact_frequency;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	
	private String gotoPageString;
	private String validateFormNav;
	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getGotoPageString() {

	return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	this.gotoPageString = gotoPageString;

	}
	 
	public String saveOnTabChange() throws Exception {
	    //if (option enabled for this company in the system parameters table then call save) {
			validateFormNav = "OK";
	        String s = save();    // else simply navigate to the requested page)
	        return gotoPageString;
	}

	public List getContractNotes() {
		return contractNotes;
	}

	public void setContractNotes(List contractNotes) {
		this.contractNotes = contractNotes;
	}

	public Notes getNotes() {
		return notes;
	}

	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	public List getNotess() {
		return notess;
	}

	public void setNotess(List notess) {
		this.notess = notess;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}

	public List getOwner() {
		return owner;
	}

	public void setOwner(List owner) {
		this.owner = owner;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public Map<String, String> getStates() {
		try {
			partner = partnerPublicManager.get(pID);
			List countryCode = partnerManager.getCountryCode(partner.getBillingCountry());
			if (!countryCode.isEmpty()) {
				return (states != null && !states.isEmpty()) ? states : customerFileManager.findDefaultStateList(countryCode.get(0).toString(), sessionCorpID);
			} else {
				return (states != null && !states.isEmpty()) ? states : customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return states;
	}

	public void setStates(Map<String, String> states) {
		this.states = states;
	}

	public Map<String, String> getCountryDesc() {
		return countryDesc;
	}

	public void setCountryDesc(Map<String, String> countryDesc) {
		this.countryDesc = countryDesc;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Long getPID() {
		return pID;
	}

	public void setPID(Long pid) {
		pID = pid;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public List getMultiplJobTypes() {
		return multiplJobTypes;
	}

	public void setMultiplJobTypes(List multiplJobTypes) {
		this.multiplJobTypes = multiplJobTypes;
	}

	/**
	 * @return the noteFor
	 */
	public String getNoteFor() {
		return noteFor;
	}

	/**
	 * @param noteFor the noteFor to set
	 */
	public void setNoteFor(String noteFor) {
		this.noteFor = noteFor;
	}

	public static Map<String, String> getPrifix() {
		return prifix;
	}

	public static void setPrifix(Map<String, String> prifix) {
		AccountContactAction.prifix = prifix;
	}

	public List getUserNameList() {
		return userNameList;
	}

	public void setUserNameList(List userNameList) {
		this.userNameList = userNameList;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public List getCrmLeadParameter() {
		return crmLeadParameter;
	}

	public void setCrmLeadParameter(List crmLeadParameter) {
		this.crmLeadParameter = crmLeadParameter;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


}
