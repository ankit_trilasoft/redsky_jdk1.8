package com.trilasoft.app.service;

	import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.StandardAddresses;


	public interface StandardAddressesManager extends GenericManager<StandardAddresses, Long> { 
		public List searchByDetail(String job,String addressLine1,String city, String countryCode,String state);

		public List getByJobType(String jobType);

		public List searchByDetailForPopup(String jobType, String addressLine1, String city, String countryCode, String state);

		public List getAddressLine1(String jobType, String corpid);
		
		public StandardAddresses getStandardAddressDetail(String residenceName);
	 
	}
