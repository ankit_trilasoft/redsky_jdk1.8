package com.trilasoft.app.service.impl;

import java.util.Date;

public class ToDoRuleExpressionBuilder {

	public static String getFromClause(String entity,String roleValue,String testDate,String sessionCorpID,String baseExpression) {
		String fromClause = entity.toLowerCase();
		if (entity.equalsIgnoreCase("serviceorder")) {
			if(roleValue.equalsIgnoreCase("OriginAgent")){
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.originAgentCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.originAgentCode inner join app_user on app_user.parentAgent = trackingstatus.originAgentCode and app_user.userType='AGENT' and app_user.account_enabled=true and trackingstatus.originAgentCode not in (select bookingAgentCode from companydivision) ";
			}else if(roleValue.equalsIgnoreCase("DestinationAgent")){
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.destinationAgentCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.destinationAgentCode inner join app_user on app_user.parentAgent = trackingstatus.destinationAgentCode and app_user.userType='AGENT' and app_user.account_enabled=true and trackingstatus.destinationAgentCode not in (select bookingAgentCode from companydivision) ";
			}else if(roleValue.equalsIgnoreCase("OriginSubAgent")){
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.originSubAgentCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.originSubAgentCode inner join app_user on app_user.parentAgent = trackingstatus.originSubAgentCode and app_user.userType='AGENT' and app_user.account_enabled=true and trackingstatus.originSubAgentCode not in (select bookingAgentCode from companydivision) ";
			}else if(roleValue.equalsIgnoreCase("DestinationSubAgent")){
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.destinationSubAgentCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.destinationSubAgentCode inner join app_user on app_user.parentAgent = trackingstatus.destinationSubAgentCode and app_user.userType='AGENT' and app_user.account_enabled=true and trackingstatus.destinationSubAgentCode not in (select bookingAgentCode from companydivision) ";
			}else if(roleValue.equalsIgnoreCase("ForwarderAgent")){
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.forwarderCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.forwarderCode inner join app_user on app_user.parentAgent = trackingstatus.forwarderCode and app_user.userType='PARTNER' and app_user.account_enabled=true";
			}else if(roleValue.equalsIgnoreCase("BrokerAgent")){																																																																																																								
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.brokerCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.brokerCode inner join app_user on app_user.parentAgent = trackingstatus.brokerCode and app_user.userType='PARTNER' and app_user.account_enabled=true";
			}else if(roleValue.equalsIgnoreCase("MM Counselor")){																																																																																																								
				fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = trackingstatus.brokerCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = trackingstatus.brokerCode inner join app_user on app_user.parentAgent = trackingstatus.brokerCode and app_user.userType='PARTNER' and app_user.account_enabled=true";
			}else{
				if(testDate.contains("accountline")){
					fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join accountline on serviceorder.id=accountline.serviceorderid left outer join partnerprivate on partnerprivate.partnerCode = accountline.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = accountline.billToCode ";
				}else{
					String partnerCodeDetails="";
					if(baseExpression.contains("trackingstatus.originAgentCode")){
						partnerCodeDetails="trackingstatus.originAgentCode";
					}else if(baseExpression.contains("trackingstatus.destinationAgentCode")){
						partnerCodeDetails="trackingstatus.destinationAgentCode";
					}else if(baseExpression.contains("trackingstatus.originSubAgentCode")){
						partnerCodeDetails="trackingstatus.originSubAgentCode";
					}else if(baseExpression.contains("trackingstatus.destinationSubAgentCode")){
						partnerCodeDetails="trackingstatus.destinationSubAgentCode";
					}else if(baseExpression.contains("trackingstatus.forwarderCode")){
						partnerCodeDetails="trackingstatus.forwarderCode";
					}else if(baseExpression.contains("trackingstatus.brokerCode")){
						partnerCodeDetails="trackingstatus.brokerCode";
					}else if(baseExpression.contains("serviceorder.bookingAgentCode")){
						partnerCodeDetails="serviceorder.bookingAgentCode";
				     }else{
						partnerCodeDetails="trackingstatus.brokerCode";
					}
					if(partnerCodeDetails.equals("")){
						fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  ";	
					}else{
						fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true left outer join vehicle on serviceorder.id=vehicle.serviceOrderId and vehicle.status is true  left outer join partnerprivate on partnerprivate.partnerCode = "+partnerCodeDetails+" and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = "+partnerCodeDetails+"";
					}
					
				}
			}
			
		} else if (entity.equalsIgnoreCase("billing")) {
			if(testDate.contains("accountline")){
				fromClause += " billing  left outer join trackingstatus on billing.id=trackingstatus.id left outer join miscellaneous on billing.id=miscellaneous.id left outer join serviceorder on serviceorder.id=billing.id left outer join servicepartner on billing.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join accountline on serviceorder.id=accountline.serviceorderid left outer join partnerprivate on partnerprivate.partnerCode = accountline.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = accountline.billToCode ";
			}else{
				fromClause += " billing  left outer join trackingstatus on billing.id=trackingstatus.id left outer join miscellaneous on billing.id=miscellaneous.id left outer join serviceorder on serviceorder.id=billing.id left outer join servicepartner on billing.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";
			}
		} else if (entity.equalsIgnoreCase("customerfile")) {
			if(roleValue.equalsIgnoreCase("OriginAgent")){
				fromClause += " customerfile left outer join serviceorder on customerfile.id=serviceorder.customerFileId  left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join partnerprivate on partnerprivate.partnerCode = customerfile.originAgentCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = customerfile.originAgentCode inner join app_user on app_user.parentAgent = customerfile.originAgentCode and app_user.userType='AGENT' and app_user.account_enabled=true and customerfile.originAgentCode not in (select bookingAgentCode from companydivision) ";
			}else{
				fromClause += " customerfile left outer join serviceorder on customerfile.id=serviceorder.customerFileId left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join partnerprivate on partnerprivate.partnerCode = customerfile.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = customerfile.billToCode ";
			}
		} else if (entity.equalsIgnoreCase("quotationfile")) {
			fromClause="customerfile";
			fromClause += " customerfile left outer join serviceorder on customerfile.id=serviceorder.customerFileId left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join partnerprivate on partnerprivate.partnerCode = customerfile.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = customerfile.billToCode ";
		} else if (entity.equalsIgnoreCase("miscellaneous")) {
			if(testDate.contains("accountline")){
				fromClause += " miscellaneous left outer join  serviceorder on miscellaneous.id=serviceorder.id left outer join trackingstatus on miscellaneous.id=trackingstatus.id left outer join billing on miscellaneous.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join accountline on serviceorder.id=accountline.serviceorderid left outer join partnerprivate on partnerprivate.partnerCode = accountline.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = accountline.billToCode " ;
			}else{
				fromClause += " miscellaneous left outer join  serviceorder on miscellaneous.id=serviceorder.id left outer join trackingstatus on miscellaneous.id=trackingstatus.id left outer join billing on miscellaneous.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";	
			}
		} else if (entity.equalsIgnoreCase("servicepartner")) {
			entity="serviceorder";
			fromClause = "serviceorder";
			fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode  ";
		} else if (entity.equalsIgnoreCase("vehicle")) {
			entity="serviceorder";
			fromClause = "serviceorder";
			fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join vehicle on serviceorder.id=vehicle.serviceOrderId and vehicle.status is true left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";
		} else if (entity.equalsIgnoreCase("AccountLine")) {
	        fromClause += " accountline inner join serviceorder on accountline.serviceOrderId=serviceorder.id inner join trackingstatus on serviceorder.id=trackingstatus.id inner join billing on serviceorder.id=billing.id inner join customerfile on customerfile.id=serviceorder.customerFileId left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode "; 
		} else if (entity.equalsIgnoreCase("WorkTicket")) {
		    fromClause += " workticket left outer join serviceorder on workticket.serviceOrderId=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join refmaster on workticket.warehouse=refmaster.code and workticket.corpID=refmaster.corpID and refmaster.parameter = 'HOUSE' left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";
		}
		else if (entity.equalsIgnoreCase("WorkTicketList")) {
			entity="serviceorder";
			fromClause = "serviceorder";
		    fromClause += " serviceorder left outer join workticket on serviceorder.id=workticket.serviceOrderId left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join refmaster on workticket.warehouse=refmaster.code and workticket.corpID=refmaster.corpID and refmaster.parameter = 'HOUSE' left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";
		}
		else if (entity.equalsIgnoreCase("AccountLineList")) {
			entity="serviceorder";
			fromClause = "serviceorder";
			fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id inner join accountline on accountline.serviceOrderId=serviceorder.id  left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId and servicepartner.status is true  left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";
		} else if (entity.equalsIgnoreCase("DspDetails")) {
			fromClause += " dspdetails left outer join serviceorder on dspdetails.id=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode ";
		}else if (entity.equalsIgnoreCase("Claim")) {	
			fromClause += " claim left outer join serviceorder on claim.serviceorderid=serviceorder.id left outer join loss on claim.id=loss.claimId left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode";
		}
		else if (entity.equalsIgnoreCase("CreditCard")) {   
            fromClause += " creditcard left outer join serviceorder on creditcard.shipnumber=serviceorder.shipnumber left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join partnerprivate on partnerprivate.partnerCode = billing.billToCode and partnerprivate.corpID in ('"+sessionCorpID+"') left outer join partnerpublic on partnerpublic.partnerCode = billing.billToCode";
        }
		return fromClause;
	}

	public static String getAssignedToList(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile") || entity.equals("QuotationFile")) {
			fromClause = "customerfile.coordinator";
		} else{
			fromClause = "serviceorder.coordinator";
		}
		return fromClause;
	}
	
	public static String getFileNumber(String entity) {
		String fromClause = entity;
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous")) {
			fromClause = "serviceorder.shipNumber";
		} else if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.sequenceNumber";
		} else if (entity.equals("WorkTicket")) {
			fromClause = "workticket.ticket";
		} else if (entity.equals("QuotationFile")) {
			fromClause = "customerfile.sequenceNumber";
		}else if (entity.equals("Claim")) {
			fromClause = "claim.claimNumber";
		}else {
			fromClause = "serviceorder.shipNumber";
		}
		return fromClause;
	}

	public static String getBilling(String entity) {
		String fromClause = entity;
		if (entity.equals("QuotationFile") || entity.equals("CustomerFile")) {
			fromClause = "customerfile.personBilling";
		}else{
			fromClause = "billing.personBilling";
		}
		return fromClause;
	}
	
	public static String getOrginAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originAgentCode";
		}
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.originAgentCode";
		}
		return fromClause;
	}
	public static String getDestinationAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationAgentCode";
		}
		return fromClause;
	}
	
	public static String getForwarderAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") ) {
			fromClause = "trackingstatus.forwarderCode";
		}
		return fromClause;
	}
	public static String getBrokerAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.brokerCode";
		}
		return fromClause;
	}
	public static String getOrginSubAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originSubAgentCode";
		}
		return fromClause;
	}
	public static String getDestinationSubAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationSubAgentCode";
		}
		return fromClause;
	}

	public static String getOrginAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originAgent";
		}
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.originAgentName";
		}
		return fromClause;
	}
	public static String getDestinationAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationAgent";
		}
		return fromClause;
	}
	
	public static String getForwarderAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") ) {
			fromClause = "trackingstatus.forwarder";
		}
		return fromClause;
	}
	public static String getBrokerAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.brokerName";
		}
		return fromClause;
	}
	public static String getOrginSubAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originSubAgent";
		}
		return fromClause;
	}
	public static String getDestinationSubAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationSubAgent";
		}
		return fromClause;
	}
	
	public static String getAuditor(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile") || entity.equals("QuotationFile")) {
			fromClause = "customerfile.auditor";
		} else{
			fromClause = "billing.auditor";	
		}		
		return fromClause;
	}
	
	public static String getPayable(String entity) {
		String fromClause = entity;
		if (entity.equals("QuotationFile") || entity.equals("CustomerFile")) {
			fromClause = "customerfile.personPayable";
		}else{
			fromClause = "billing.personPayable";
		}
		return fromClause;
	}
	
	public static String getPricing(String entity) {
		String fromClause = entity;
		if (entity.equals("QuotationFile") || entity.equals("CustomerFile")) {
			fromClause = "customerfile.personPricing";
		} else{
			fromClause = "billing.personPricing";
		}
		return fromClause;
	}
	
	public static String getConsultant(String entity) {
		String fromClause = entity;
		fromClause = "customerfile.estimator";
		return fromClause;
	}
	
	public static String getSalesMan(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.salesMan";
		}else{
			fromClause = "serviceorder.salesMan";
		}
		return fromClause;
	}
	
	public static String getWarehousemanager(String entity) {
		String fromClause = entity;
		fromClause = "refmaster.billCrew";
		return fromClause;
	}
	
	public static String getShipper(String entity) {
		String fromClause = entity;
		fromClause = "customerfile.firstName, customerfile.lastName";
		return fromClause;
	}
	public static String getOpsPerson(String entity) {
		String fromClause = entity;
		fromClause = "serviceorder.opsPerson";
		return fromClause;
	}
	public static String getJoinClause(String entity) {
		String joinClause = entity;
		if (entity.equals("ServiceOrder")) {
			joinClause = "serviceorder.sequenceNumber=customerfile.sequenceNumber and serviceorder.shipNumber= trackingstatus.shipNumber and serviceorder.shipNumber=miscellaneous.shipNumber and serviceorder.shipNumber=billing.shipNumber and serviceorder.shipNumber=accountline.shipNumber and serviceorder.shipNumber=servicepartner.shipNumber and serviceorder.shipNumber=vehicle.shipNumber";
		} else if (entity.equals("CustomerFile")) {
			joinClause = " customerfile.sequenceNumber= serviceorder.sequenceNumber and customerfile.sequenceNumber= miscellaneous.sequenceNumber and customerfile.sequenceNumber=billing.sequenceNumber";
		} else if (entity.equals("QuotationFile")) {
			joinClause = " customerfile.sequenceNumber= serviceorder.sequenceNumber and customerfile.sequenceNumber= miscellaneous.sequenceNumber and customerfile.sequenceNumber=billing.sequenceNumber";
		} else if (entity.equals("Miscellaneous")) {
			joinClause = " miscellaneous.shipNumber= serviceorder.shipNumber";
		} else if (entity.equals("ServicePartner")) {
			joinClause = " servicepartner.shipNumber= serviceorder.shipNumber and servicepartner.shipNumber=trackingstatus.shipNumber and servicepartner.shipNumber=billing.shipNumber";
		} else if (entity.equals("Vehicle")) {
			joinClause = " vehicle.shipNumber= serviceorder.shipNumber and vehicle.shipNumber=trackingstatus.shipNumber and vehicle.shipNumber=billing.shipNumber";
		}
		return joinClause;
	}
	public static String getClaimHandLer(String entity,String roleList) {
		String fromClause = "";
		if (entity.equals("Billing")) {
			fromClause = "billing.claimHandler";
		}else if (entity.equals("Claim")){
			if(roleList.equals("Claim Handler")){
			fromClause = "billing.claimHandler";	
			}else{
			fromClause = "claim.claimPerson";
			}
		}
		return fromClause;
	}
	public static String getForwarder(String entity) {
		String fromClause = "";
		//if (entity.equals("Billing")) {
			fromClause = "billing.personForwarder";
		//}
		return fromClause;
	}
	public static String getMmCounselor(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "serviceorder.mmCounselor";
		}
		return fromClause;
	}
	public static String getReceiptdate(String entity) {
		String fromClause = "";
		if (entity.equals("Billing")) {
			fromClause = "billing.wareHousereceiptDate";
		}
		return fromClause;
	}
	public static String getEmail(String entity) {
		String fromClause = entity;
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.originAgentEmail";
		}else{
			fromClause="";
		}
		return fromClause;
	}
	public static String getBillToCode(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
		    fromClause = "customerfile.billToCode";
		}else{
		    fromClause = "billing.billToCode";	
		}
		return fromClause;
	}
	public static String getBillToName(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
		    fromClause = "customerfile.billToName";
		}else{
		    fromClause = "billing.billToName";	
		}
       
		return fromClause;
	}
	
	
	public static String getBookingAgentName(String entity)
	{
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
		    fromClause = "customerfile.bookingAgentCode";
		}else{
		    fromClause = "serviceorder.bookingAgentCode";	
		}
       
		return fromClause;
		
		
	}
	
	public static String buildExpression(String entity, String baseExpression, String dateExpression, Long recordID, String sessionCorpID,String roleValue , String testDate,String docType) {
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        //User user = (User)auth.getPrincipal();
		String queryParameter="";
		String tempControlCondition="customerfile.controlflag in ('C','A','L')";
		if(entity.equalsIgnoreCase("QuotationFile")){
			entity="CustomerFile";
			tempControlCondition="customerfile.controlflag='Q'";
		}
		if(entity.equalsIgnoreCase("servicepartner") || entity.equalsIgnoreCase("vehicle")){
			entity="ServiceOrder";
		}
		if(!entity.equals("AccountLineList") && !entity.equals("WorkTicketList")){
			if(entity.equalsIgnoreCase("CustomerFile")){
				if(roleValue.equalsIgnoreCase("OriginAgent")){
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " ) AND " + dateExpression +" and ( customerfile.bookingAgentSequenceNumber ='' or  customerfile.bookingAgentSequenceNumber is null ) AND "+tempControlCondition ;
				}else{
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " ) AND "+tempControlCondition+" AND  " + dateExpression ;
				}
			}else{
				if(roleValue.equalsIgnoreCase("OriginAgent") || roleValue.equalsIgnoreCase("DestinationAgent") || roleValue.equalsIgnoreCase("OriginSubAgent") || roleValue.equalsIgnoreCase("DestinationSubAgent")){
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " ) AND " + dateExpression +"  and serviceorder.controlflag='C' and ( serviceorder.bookingAgentShipNumber ='' or  serviceorder.bookingAgentShipNumber is null ) " ;
					}
				else{
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " ) AND " + dateExpression +"  and serviceorder.controlflag='C' " ;
					
				}
			}
		}
		if(entity.equals("AccountLineList")){
			entity="serviceorder";
			queryParameter= " from " + getFromClause("AccountLineList",roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
			+ baseExpression + " ) AND " + dateExpression +"  and serviceorder.controlflag='C' " ;
		}
		if(entity.equals("WorkTicketList")){
			entity="serviceorder";
			queryParameter= " from " + getFromClause("WorkTicketList",roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
			+ baseExpression + " ) AND " + dateExpression +"  and serviceorder.controlflag='C' " ;
		}
		if(docType!=null && !docType.equals("")){
			if(entity.equalsIgnoreCase("CustomerFile")){
				queryParameter=queryParameter+" and customerfile.sequenceNumber not in(select distinct fileid from myfile where fileid=customerfile.sequenceNumber and filetype='"+docType+"' and active is true ) group by " + entity.toLowerCase() + ".id order by null ";
			}else{
			queryParameter=queryParameter+" and serviceorder.shipnumber not in(select distinct fileid from myfile where fileid=serviceorder.shipnumber and filetype='"+docType+"' and active is true ) group by " + entity.toLowerCase() + ".id order by null ";
			}
		}else{
			queryParameter=queryParameter+" group by " + entity.toLowerCase() + ".id order by null " ;
		}
		return queryParameter;
	}
	
	public static String buildExpressionForcating(String entity, String baseExpression, String dateExpression, Long recordID, String sessionCorpID,String roleValue , String testDate,String docType) {
		String queryParameter="";
		String tempControlCondition="customerfile.controlflag in ('C','A','L')";
		if(entity.equalsIgnoreCase("QuotationFile")){
			entity="CustomerFile";
			tempControlCondition="customerfile.controlflag='Q'";
		}
		if(!entity.equals("AccountLineList")){
			if(entity.equalsIgnoreCase("CustomerFile")){
				if(roleValue.equalsIgnoreCase("OriginAgent")){
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " )  " + dateExpression +" and ( customerfile.bookingAgentSequenceNumber ='' or  customerfile.bookingAgentSequenceNumber is null ) AND "+tempControlCondition ;
				}else{
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " )  "+tempControlCondition+" AND  " + dateExpression ;
				}
			}else{
				if(roleValue.equalsIgnoreCase("OriginAgent") || roleValue.equalsIgnoreCase("DestinationAgent") || roleValue.equalsIgnoreCase("OriginSubAgent") || roleValue.equalsIgnoreCase("DestinationSubAgent")){
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " )  " + dateExpression +" AND serviceorder.status <>'CNCL' and serviceorder.controlflag='C' and ( serviceorder.bookingAgentShipNumber ='' or  serviceorder.bookingAgentShipNumber is null ) " ;
					}
				else{
					queryParameter= " from " + getFromClause(entity,roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " )  " + dateExpression +" AND serviceorder.status <>'CNCL' and serviceorder.controlflag='C' " ;
					
				}
			}
		}
		if(entity.equals("AccountLineList")){
			entity="serviceorder";
			queryParameter= " from " + getFromClause("AccountLineList",roleValue,testDate,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
			+ baseExpression + " )  " + dateExpression +" AND serviceorder.status <>'CNCL' and serviceorder.controlflag='C' " ;
		}
		if(docType!=null && !docType.equals("")){
			if(entity.equalsIgnoreCase("CustomerFile")){
				queryParameter=queryParameter+" and customerfile.sequenceNumber not in(select distinct fileid from myfile where fileid=customerfile.sequenceNumber and filetype='"+docType+"' and active is true ) group by " + entity.toLowerCase() + ".id order by null ";
			}else{
			queryParameter=queryParameter+" and serviceorder.shipnumber not in(select distinct fileid from myfile where fileid=serviceorder.shipnumber and filetype='"+docType+"' and active is true ) group by " + entity.toLowerCase() + ".id order by null ";
			}
		}else{
			queryParameter=queryParameter+" group by " + entity.toLowerCase() + ".id order by null " ;
		}
		return queryParameter;
	}
	
}
