<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="haulingGridDetails.title"/></title>   
    <meta name="heading" content="<fmt:message key='haulingGridDetails.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script language="JavaScript">
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
}

function checkFields(){
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.lowDistance'].value.trim()==''){
		alert('Please enter Low Distance');
		return false;
	}
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.highDistance'].value.trim()==''){
		alert('Please enter High Distance');
		return false;
	}
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.rateMile'].value.trim()==''){
		alert('Please enter Rate Mile');
		return false;
	}
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.rateFlat'].value.trim()==''){
		alert('Please enter Rate Flat');
		return false;
	}
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.grid'].value==''){
		alert('Please select Grid');
		return false;
	}
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.unit'].value.trim()==''){
		alert('Please enter Unit');
		return false;
	}
	if(document.forms['haulingGridForm'].elements['partnerRefHaulingGrid.countryCode'].value==''){
		alert('Please select Country');
		return false;
	}

}
</script>
</head> 

<s:form id="haulingGridForm" name="haulingGridForm" action="savehaulingGrid.html" method="post" validate="true">   
<s:hidden name="partnerRefHaulingGrid.id" />
<s:hidden name="partnerRefHaulingGrid.corpID" />
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/editPartnerRatesForm.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<div id="newmnav">
	<ul>
		<li><a href="haulingGrid.html"><span>Hauling Grid List</span></a></li>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Hauling Grid Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div>
<div style="width:100%">
<div class="spn">&nbsp;</div>
</div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
				<table>
					<tbody>
						<tr>
							<td align="left" class="listwhite" colspan="7"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.lowDistance'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefHaulingGrid.lowDistance" maxlength="6"  cssStyle="width:125px" onchange="onlyFloat(this);"/></td>
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.highDistance'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefHaulingGrid.highDistance" maxlength="6"  cssStyle="width:125px" onchange="onlyFloat(this);"/></td>
						</tr>	
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.rateMile'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefHaulingGrid.rateMile" maxlength="10"  cssStyle="width:125px" onchange="onlyFloat(this);"/></td>
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.rateFlat'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefHaulingGrid.rateFlat" maxlength="10"  cssStyle="width:125px" onchange="onlyFloat(this);"/></td>
						</tr>	
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.grid'/></td>
							<td><s:select name="partnerRefHaulingGrid.grid" list="%{grid}" headerKey="" headerValue="" cssStyle="width:129px" cssClass="list-menu"/></td>
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.unit'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefHaulingGrid.unit" maxlength="10"  cssStyle="width:125px"/></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefHaulingGrid.countryCode'/></td>
							<td><s:select name="partnerRefHaulingGrid.countryCode" list="%{country}" headerKey="" headerValue="" cssStyle="width:129px" cssClass="list-menu"/></td>
						</tr>
						<tr>
							<td align="left" class="listwhite" height="20"></td>
						</tr>
					</tbody>
				</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
  
<table>
	<tbody>
		<tr>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefHaulingGrid.createdOn'/></b></td>
			<td valign="top"></td>
			<td style="width:120px">
				<fmt:formatDate var="haulingGridCreatedOnFormattedValue" value="${partnerRefHaulingGrid.createdOn}" pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="partnerRefHaulingGrid.createdOn" value="${haulingGridCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${partnerRefHaulingGrid.createdOn}" pattern="${displayDateTimeFormat}"/>
			</td>		
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefHaulingGrid.createdBy' /></b></td>
			<c:if test="${not empty partnerRefHaulingGrid.id}">
				<s:hidden name="partnerRefHaulingGrid.createdBy"/>
				<td style="width:85px"><s:label name="createdBy" value="%{partnerRefHaulingGrid.createdBy}"/></td>
			</c:if>
			<c:if test="${empty partnerRefHaulingGrid.id}">
				<s:hidden name="partnerRefHaulingGrid.createdBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
			
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefHaulingGrid.updatedOn'/></b></td>
			<fmt:formatDate var="haulingGridUpdatedOnFormattedValue" value="${partnerRefHaulingGrid.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="partnerRefHaulingGrid.updatedOn" value="${haulingGridUpdatedOnFormattedValue}"/>
			<td style="width:120px"><fmt:formatDate value="${partnerRefHaulingGrid.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefHaulingGrid.updatedBy' /></b></td>
			<c:if test="${not empty partnerRefHaulingGrid.id}">
				<s:hidden name="partnerRefHaulingGrid.updatedBy"/>
				<td style="width:85px"><s:label name="updatedBy" value="%{partnerRefHaulingGrid.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty partnerRefHaulingGrid.id}">
				<s:hidden name="partnerRefHaulingGrid.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
</table>               
        <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px;" onclick="return checkFields()"/>   
		
  
  <table><tr><td height="80px"></td></tr></table>
</s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("haulingGridForm"));   
    <c:if test="${hitflag == 1}" >
			<c:redirect url="/haulingGrid.html"/>
	</c:if>
</script>