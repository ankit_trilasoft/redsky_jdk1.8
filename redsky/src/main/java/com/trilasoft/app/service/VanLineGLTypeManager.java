package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.VanLineGLType;

public interface VanLineGLTypeManager extends GenericManager<VanLineGLType, Long> {

	public List<VanLineGLType> vanLineGLTypeList();

	public List<VanLineGLType> searchVanLineGLType(String glType, String description, String glCode);

	public Map<String, String> findByParameter(String corpId);
	
	public List isExisted(String code, String corpId);
	
	public Map<String, String> findGlCode(String corpId);
    
	public List<VanLineGLType> getGLPopupList(String chargesGl);
}