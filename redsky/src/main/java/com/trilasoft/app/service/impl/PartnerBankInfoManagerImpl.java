package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerBankInfoDao;
import com.trilasoft.app.model.PartnerBankInformation;
import com.trilasoft.app.service.PartnerBankInfoManager;

public class PartnerBankInfoManagerImpl  extends GenericManagerImpl<PartnerBankInformation,Long > implements PartnerBankInfoManager{

	PartnerBankInfoDao  partnerBankInfoDao;
	public PartnerBankInfoManagerImpl(PartnerBankInfoDao  partnerBankInfoDao) {
		super(partnerBankInfoDao);
		this.partnerBankInfoDao=partnerBankInfoDao;
	}
	public List findBankData(String paetnercode, String sessionCorpID){
		return partnerBankInfoDao.findBankData( paetnercode, sessionCorpID);
	}
	
}
