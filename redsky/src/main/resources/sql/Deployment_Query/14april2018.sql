

// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


ALTER TABLE workticket ADD COLUMN thirdPartyRequired BIT(1)  DEFAULT b'0';

ALTER TABLE redsky.billing ADD COLUMN FIDIISOAudit DATETIME NULL ;

ALTER TABLE trackingstatus  ADD COLUMN crewArrivalDate DATETIME NULL   , ADD COLUMN customClearance DATETIME NULL   ;

ALTER TABLE servicepartner ADD COLUMN revisedETD DATETIME NULL ,ADD COLUMN revisedETA DATETIME NULL;
ALTER TABLE billing ADD COLUMN valuation VARCHAR(25) NULL   ;
ALTER TABLE defaultaccountline ADD COLUMN originCity VARCHAR(100) NULL  AFTER uploaddataflag ,
ADD COLUMN destinationCity VARCHAR(100) NULL  AFTER originCity ;
