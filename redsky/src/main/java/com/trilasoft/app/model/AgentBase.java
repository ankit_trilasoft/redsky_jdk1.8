package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="agentbase")
@FilterDefs({
	@FilterDef(name = "partnerPrivateCorpID", parameters = { @ParamDef(name = "privateCorpID", type = "string") })
})

@Filters( {
	@Filter(name = "partnerPrivateCorpID", condition = "corpID in (:privateCorpID) ")
} )
public class AgentBase extends BaseObject {
	private String corpID;
	private Long id;
	private String baseCode;
	private String description;
	private String partnerCode;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("baseCode", baseCode).append("description",
				description).append("partnerCode", partnerCode).append(
				"createdBy", createdBy).append("updatedBy", updatedBy).append(
				"createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AgentBase))
			return false;
		AgentBase castOther = (AgentBase) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(baseCode, castOther.baseCode).append(
				description, castOther.description).append(partnerCode,
				castOther.partnerCode).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(baseCode)
				.append(description).append(partnerCode).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column(length=10)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column(length=10)
	public String getBaseCode() {
		return baseCode;
	}
	public void setBaseCode(String baseCode) {
		this.baseCode = baseCode;
	}
	@Column(length=20)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=100)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	

}
