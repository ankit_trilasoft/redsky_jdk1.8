package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.SystemDefault;

public interface PartnerPrivateDao extends GenericDao<PartnerPrivate, Long> {
	
	public PartnerPrivate findPartnerPrivateByCode(String partnerCode,String sessionCorpID);
	
	public List findPartnerCode(String partnerCode);

	public List findMaxByCode();
	
	public List getPartnerPrivateList(String partnerType, String sessionCorpID, String firstName, String lastName, String partnerCode, String status);
	
	public List getPartnerPrivateListByPartnerCode(String sessionCorpID, String partnerCode);
	
	public List getPartnerPrivate(String partnerCode, String corpID);
	
	public List isExistPartnerPrivate(String corpID, String partnerCode);
	
	public List getIsoCode(String vatNumber);	
	public List getPartnerPrivateId(String partnerCode);
	public String getTheCreditTerm(String partnerCode, String corpID);
	public List getPartnerPrivateDefaultAccount(String partnerCode,String corpId);
	public List getGroupAgePermission(String billToCode,String corpID);
	public String getUsedAmount(String partnerCode,String tempCurrency,String corpID);
	public List findprivateParty(String partnerCode,String corpID);
	public String getBillMc(String billToCode, String sessionCorpID);
	public List<SystemDefault> findsysDefault(String corpID);
	public Map<String, String> getPartnerPrivateDescLinkMap(String partnerCode,String CorpID);
	public int updatePartnerPrivate(String excludeFPU,String sessionCorpID,String partnerCode,String excludeOption);
	public List getPartnerPrivateChildAccount(String partnerCode,String corpId);
	public void updateLastName(String sessionCorpID, String lName, Long ppId);
	public List getPartnerPrivateByPartnerCode(String otherCorpId,	String partnerCode);
	public List findContract(String sessionCorpID);
	public Double getminMargin(String billToCode);
	public String getPartnerCodeFromDefaultTemplate(String sessionCorpID,String partnerCode);
	public int getParentCompensationYear(String corpId,String agentParent);
	public List findDoNotInvoicePartnerList(String sessionCorpID);
	public List getDefaultContactUsersList(String partnerCode, String sessionCorpID) ;
	 public List loadUserByUsername(String username);
	public String getClassifiedAgentValue(String agentCode);		
	public PartnerPrivate checkById(Long id);
	public List getBillingCycle(String billToCode,String sessionCorpID);
	//my code
	public List findVatBillimngGroup(String sessionCorpID,String partnerCode);
	public Map<String, String> getVatBillingGroupMap(String sessionCorpID, String parterCodeList);

}
