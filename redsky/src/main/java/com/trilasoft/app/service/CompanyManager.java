package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;



import com.trilasoft.app.model.Company;

public interface CompanyManager extends GenericManager<Company, Long> { 
	
	public List<Company> findByCorpID(String corpId);

	public String findByCompanyDivis(String sessionCorpID);
//public List<Company> findMaximumId();

	public List getcorpID();

	public List getcompanyName(String corpID);

	public List getDateDiffExp(String username, String sessionCorpID, Date passwordExp);

	public List getDateDiffAlert(String username, String sessionCorpID, Date passwordExp, String pwdExpDate);

	public boolean findTracInternalUser(String sessionCorpID);

	public List findCorpIdList();

	public String findAutomaticLinkup(String externalCorpID);

	public String defaultBillingRate(String tempCorpId);

	public boolean getCompanySurveyFlag(String corpid);

	public void updateStorageBillingStatus(String sessionCorpID, boolean storageBillingStatus);
	public String getcorpIDName(String companyName);
	public List getcompanyListBycompanyView( String sessionCorpID,String companyName, String countryID,String billGroup,String status) ;
	public List getcorpIDList();
	public List findCorpIdAgentList(); 
	public int updateVendorExtractDate(String sessionCorpID,String userName); 
	public int updateBillToExtractDate(String sessionCorpID,String userName);
}
