package com.trilasoft.app.service.impl;
 
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AccrualProcessManager;
import com.trilasoft.app.dao.AccrualProcessDao;
import com.trilasoft.app.model.AccountLine;


public class AccrualProcessManagerImpl  extends GenericManagerImpl<AccountLine, Long> implements AccrualProcessManager {

	AccrualProcessDao accrualProcessDao;
    
    public AccrualProcessManagerImpl(AccrualProcessDao accrualProcessDao) { 
        super(accrualProcessDao); 
        this.accrualProcessDao = accrualProcessDao; 
    } 
	public List payableReverseAccruedList(Date endDate, String sessionCorpID){
		return accrualProcessDao.payableReverseAccruedList(endDate, sessionCorpID);
	}
	public List payableNewAccruals(Date endDate, String sessionCorpID){
		return accrualProcessDao.payableNewAccruals(endDate, sessionCorpID);
	}
	public List payableBalance(String sessionCorpID){
		return accrualProcessDao.payableBalance(sessionCorpID);
	}
	
	public List receivableReverseAccruedList(Date endDate, String sessionCorpID){
		return accrualProcessDao.receivableReverseAccruedList(endDate, sessionCorpID);
	}
	public List receivableNewAccruals(Date endDate, String sessionCorpID){
		return accrualProcessDao.receivableNewAccruals(endDate, sessionCorpID);
	}
	public List receivableBalance(String sessionCorpID){
		return accrualProcessDao.receivableBalance(sessionCorpID);
	}
	
	
	}
