package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.model.DsPreviewTrip;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsPreviewTripManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class DsPreviewTripAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private  DsPreviewTrip dsPreviewTrip;
	private DsPreviewTripManager dsPreviewTripManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private  Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countDSPreviewTripNotes;
	private NotesManager notesManager;
	private String validateFormNav;
	private String gotoPageString;
    public void prepare() throws Exception { 
		
	}
    
    public DsPreviewTripAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}

	public DsPreviewTrip getDsPreviewTrip() {
		return dsPreviewTrip;
	}
	private String shipSize;
	private String minShip;
	private String countShip;
	private Map<String, String> relocationServices;
	private RefMasterManager refMasterManager;
	public String edit() { 
		if (id != null) {
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
			dsPreviewTrip = dsPreviewTripManager.get(id);

		} else {
			dsPreviewTrip = new DsPreviewTrip(); 
			dsPreviewTrip.setCreatedOn(new Date());
			dsPreviewTrip.setUpdatedOn(new Date());
			dsPreviewTrip.setServiceOrderId(serviceOrderId);
		} 
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		getNotesForIconChange();
		return SUCCESS;
	}
	public String save() throws Exception {
		
		boolean isNew = (dsPreviewTrip.getId() == null);  
				if (isNew) { 
					dsPreviewTrip.setCreatedOn(new Date());
		        }
				trackingStatus=trackingStatusManager.get(id);
				serviceOrder=serviceOrderManager.get(id);
		    	customerFile = serviceOrder.getCustomerFile();
		    	miscellaneous = miscellaneousManager.get(id); 
				dsPreviewTrip.setCorpID(sessionCorpID);
				dsPreviewTrip.setUpdatedOn(new Date());
				dsPreviewTrip.setUpdatedBy(getRequest().getRemoteUser()); 
				dsPreviewTripManager.save(dsPreviewTrip);  
			     if(!(validateFormNav.equals("OK"))){
				   String key = (isNew) ? "dsPreviewTrip.added" : "dsPreviewTrip.updated";
			       saveMessage(getText(key)); 
			       } 
			    getNotesForIconChange();
				return SUCCESS;
				
			
	}
	@SkipValidation
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
		String s = save();
		return SUCCESS;
	}
	
	@SkipValidation
	public String getNotesForIconChange() {
		List noteDS = notesManager.countForDSPreviewTripNotes(serviceOrder.getShipNumber());
		
		if (noteDS.isEmpty()) {
			countDSPreviewTripNotes = "0";
		} else {
			countDSPreviewTripNotes = ((noteDS).get(0)).toString();
		}

		return SUCCESS;
	}

	public void setDsPreviewTrip(DsPreviewTrip dsPreviewTrip) {
		this.dsPreviewTrip = dsPreviewTrip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setDsPreviewTripManager(DsPreviewTripManager dsPreviewTripManager) {
		this.dsPreviewTripManager = dsPreviewTripManager;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getCountDSPreviewTripNotes() {
		return countDSPreviewTripNotes;
	}

	public void setCountDSPreviewTripNotes(String countDSPreviewTripNotes) {
		this.countDSPreviewTripNotes = countDSPreviewTripNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	/**
	 * @return the countShip
	 */
	public String getCountShip() {
		return countShip;
	}

	/**
	 * @param countShip the countShip to set
	 */
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	/**
	 * @return the minShip
	 */
	public String getMinShip() {
		return minShip;
	}

	/**
	 * @param minShip the minShip to set
	 */
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	/**
	 * @return the shipSize
	 */
	public String getShipSize() {
		return shipSize;
	}

	/**
	 * @param shipSize the shipSize to set
	 */
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	/**
	 * @return the relocationServices
	 */
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	/**
	 * @param relocationServices the relocationServices to set
	 */
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}

	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
}
