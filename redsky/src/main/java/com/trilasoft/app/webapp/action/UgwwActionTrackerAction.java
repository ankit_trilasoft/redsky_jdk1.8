package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.apache.commons.collections.list.SetUniqueList;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;
import com.trilasoft.app.dao.hibernate.UgwwActionTrackerDaoHibernate.SODashboardDTO;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ErrorLog;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.UgwwActionTracker;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.StandardDeductionsManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.UgwwActionTrackerManager;
import com.trilasoft.app.service.VanLineManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;
import com.trilasoft.app.webapp.util.GetSortedMap;

public class UgwwActionTrackerAction extends BaseAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private UgwwActionTracker ugwwActionTracker;
	private UgwwActionTrackerManager ugwwActionTrackerManager;
	private List<UgwwActionTracker> ugwwList;
	Date currentdate = new Date();
	private String soNumber;
	private String action;
	private String userName;
	private Date actionTime;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private List integrationErrorList;
	private ToDoRuleManager toDoRuleManager;
	private String integrationId;
 	private String operation;
	private String orderComplete;
	private String statusCode;
	private Date effectiveDate;
	private List ugwwErrorLogList;
	private String sessionCorpID;
	private List integrationLogs;
	private String trackCorpID;
	private String so;
	private List mssLogList;
	private List centreVanlineList;
	private List serviceOrderSearchType;  
	private String serviceOrderSearchVal;
	private String defaultCompanyCode="";
	private ExtendedPaginatedList serviceOrdersExt;
	private List packingServiceList;
	private Map<String,String> localParameter;
	private Map<String,String> grpStatusList;
	private Map<String,String> grpCountryList;
	private Map<String,String> grpContinentList;
	private Map<String,String> grpModeList;
	private Map<String,String> grpAgeStatusList;
	private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>();
	private Map<String, String> countryCod;
    private Map<String, String> state;
    private Map<String, String> ocountry;
    private Map<String, String> dcountry;
    private Map<String, String> job;
    private Map<String, String> relocationServices;
    private Map<String, String> routing;
    private List mode;
    private Map<String,String> country;
    private Map<String, String> ostates;
    private Map<String, String> dstates;
    private Map<String, String> commodit;
    private Map<String, String> commodits;
    private ServiceOrder serviceOrderCombo;
    private RefMasterManager refMasterManager;
    private CustomerFileManager customerFileManager;   
    private  Map<String, String> coord = new LinkedHashMap<String, String>();
    private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
    private List companyDivis=new ArrayList();
    private String usertype; 
    private String companyCode;
    private Boolean activeStatus;
    private Map<String, String> JOB_STATUS;
    List soDashboardList;
    private String shipNumber;
    private String registrationNumber;
    private String job1;
    private String coordinator;
    private String status;
    private String roleArray;
    private String firstName;
    private  String lastName;
    private ErrorLog errorLog;
    private List distinctCorpId;
    private ErrorLogManager errorLogManager;
    private List errorLogList;
    private String module;
	private String miscVLJob;	
	private String companyDivision;
	private String billToName;
	private String originCity;
	private String destinationCity;
	private List moveForUList;
	private List voxmeEstimatorList;
	private List pricePointList;
	private Company company;
	private CompanyManager companyManager;
	private Boolean enableMoveForYou;
	private Boolean enableMSS;
	private Boolean vanlineEnabled;
	private Boolean voxmeIntegration;
	private Boolean enablePricePoint;
	private List UGWWListForIntegration;
	private Map visibleFields = new LinkedHashMap<String, String>();
	static final Logger logger = Logger.getLogger(UgwwActionTrackerAction.class);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UgwwActionTrackerAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();		
        usertype=user.getUserType();
        companyCode=user.getCompanyDivision();
        if(user.isDefaultSearchStatus()){
 		   activeStatus=true;
 	   }
 	   else{
 		   activeStatus=false;
 	   }
	}
    
	public void setUgwwActionTrackerManager(
			UgwwActionTrackerManager ugwwActionTrackerManager) {
		this.ugwwActionTrackerManager = ugwwActionTrackerManager;
	}

	public String list() {

		ugwwList = ugwwActionTrackerManager.getAllugwwactiontrackeList();

		return SUCCESS;
	}
	// Demo Variable
	private ExtendedPaginatedList SOListExt;
	private PagingLookupManager pagingLookupManager;
	public String getMiscVLJob() {
		return miscVLJob;
	}

	public void setMiscVLJob(String miscVLJob) {
		this.miscVLJob = miscVLJob;
	}
	private PaginateListFactory paginateListFactory;
	private ServiceOrder serviceOrder;
	private String shipno; 
	private String regno ;
	private String lname ;
	private String fname;
	private String page;
	private String dir;
	private String sort;
	@SkipValidation
	public String demoAjaxSearchList() {
		return SUCCESS;
		
	}
	
	@SkipValidation
	public String ajaxDemoSearchMethod(){
		logger.warn("Ajax Demo search method start.");
		SOListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		String regNum="";
		String shipNum="";
		String lastName="";
		String firstName="";
		
			if(regno!=null)
				regNum = regno;
			
			if(shipno!=null)
				shipNum = shipno;
			
			if(lname!=null)
				lastName = lname;
			
			if(fname!=null)
				firstName = fname;
				
		 List<SearchCriterion> searchCriteria = new ArrayList();
		 searchCriteria.add(new SearchCriterion("registrationNumber", regNum , SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
		 searchCriteria.add(new SearchCriterion("lastName", lastName, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
        searchCriteria.add(new SearchCriterion("firstName", firstName, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
        searchCriteria.add(new SearchCriterion("shipNumber",shipNum.replaceAll("'", "").replace("-", "").trim(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
        pagingLookupManager.getAllRecordsPage(ServiceOrder.class, SOListExt, searchCriteria);
        logger.warn("Ajax Demo search method End.");
        return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String twoStepProcessFiles() {
			ugwwList = ugwwActionTrackerManager.getTrackerList(soNumber, action, userName, actionTime, corpID);
		  return "success";
		  }
	

	@SkipValidation
	public String integrationLogListMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long StartTime = System.currentTimeMillis();
		 getCompanyList();
		// integrationErrorList = toDoRuleManager.getIntegrationLogInfoLog(); 
		
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to get log list: "+timeTaken+"\n\n");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}//integrationLogSearch
		
	
	/**
	 * Lisa Integration data display
	 * @return
	 */
	@SkipValidation
	public String lisaIntegrationLogList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long StartTime = System.currentTimeMillis();
		 getCompanyList();
		// integrationErrorList = toDoRuleManager.getIntegrationLogInfoLog(); 
		
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to get log list: "+timeTaken+"\n\n");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	/**
	 * End
	 */
	
	 public String distinctCorpId(){
	        distinctCorpId=errorLogManager.findDistinct();
	    	return SUCCESS;
	    }
	
	 @SkipValidation
	    public void getCompanyList()
	 {
		 company=companyManager.findByCorpID(sessionCorpID).get(0);
		 	enableMoveForYou = company.getEnableMoveForYou();
			enableMSS=company.getEnableMSS();
			if(enableMSS==null)
			{
				enableMSS = false;
			}
			vanlineEnabled=company.getVanlineEnabled();
			voxmeIntegration = company.getVoxmeIntegration();
			enablePricePoint = company.getEnablePricePoint();
	 }
		  
	 		@SkipValidation
			public String integrationCenterMethod(){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				Long StartTime = System.currentTimeMillis();
				getCompanyList();
				// integrationErrorList = toDoRuleManager.getIntegrationLogInfoLog(); 
				integrationLogs = ugwwActionTrackerManager.getWeeklyIntegrationCenterXML(sessionCorpID);
				Long timeTaken =  System.currentTimeMillis() - StartTime;
				logger.warn("\n\nTime taken to get log list: "+timeTaken+"\n\n");
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				
			}//integrationLogSearch
		  
		  @SkipValidation
			public String moveForUList(){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				getCompanyList();
				moveForUList = ugwwActionTrackerManager.getMoveForYouList(sessionCorpID);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		  
		  @SkipValidation
			public String voxmeEstimatorList(){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				getCompanyList();
				voxmeEstimatorList = ugwwActionTrackerManager.getVoxmeEstimatList(sessionCorpID);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			} 
		  
		  @SkipValidation
			public String pricePointList(){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				getCompanyList();
				pricePointList = ugwwActionTrackerManager.getPricePointListForIntegration(sessionCorpID);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		  
		  @SkipValidation
			public String UGWWListForIntegration(){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				getCompanyList();
				UGWWListForIntegration = ugwwActionTrackerManager.UGWWListForIntegration(sessionCorpID);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		  
		  @SkipValidation
		  public Map<String,String> searchSoDashboardList(String parameter){ 
		  	localParameter = new HashMap<String, String>();
		  	if(parameterMap.containsKey(parameter)){
		  		localParameter = parameterMap.get(parameter);
		  	}
		  	return localParameter;
		  }
		  @SkipValidation
		    public String getComboList(String corpId){
			Long StartTime = System.currentTimeMillis();
		    
				
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		    	     try {
		    	    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Getting parameter values from combolist");
		    	    	 String parameters="'CUST_STATUS','groupagestatus','STATE','COUNTRY', 'JOB','ROUTING','COMMODIT','COMMODITS','PKMODE','LOADSITE', 'SPECIFIC','JOBTYPE','MILITARY','SPECIAL','HOWPACK','AGTPER','EQUIP','PEAK','TCKTSERVC','DEPT','HOUSE', 'CONFIRM','TCKTACTN','WOINSTR','PAYTYPE','STATUSREASON','GLCODES','ISDRIVER','YESNO','SHIPTYPE','partnerType','SITOUTTA','omni',  'QUOTESTATUS','JOB_STATUS','ACC_CATEGORY','QUALITYSURVEYBY','ACC_BASIS','ACC_STATUS','CustomerSettled','AccEstmateStatus'" +
		    	    	 		",'RELOCATIONSERVICES','PackingService','CUST_STATUS','MODE','customerfeedback','OAEVALUATION','DAEVALUATION','CURRENCY','EUVAT','AssignmentType','SERVICE','FLAGCARRIER','LANGUAGE'";
		    	    	
		    	    	 logger.warn("Parameters: "+parameters);
		    	    	 JOB_STATUS  =findByParameterLocal("JOB_STATUS");
		    	    	 // String parameters = "'RELOCATIONSERVICES','PackingService','CUST_STATUS','COUNTRY','MODE','customerfeedback','OAEVALUATION','DAEVALUATION','CURRENCY','EUVAT'";
		 //  performence  query call
		    	    	 LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
		    	    	 packingServiceList= new ArrayList();
		    	    	 grpCountryList = new LinkedHashMap<String, String>();
		    	    	 grpContinentList = new LinkedHashMap<String, String>();
		    	    	 grpModeList = new LinkedHashMap<String, String>();
		    	    	 relocationServices = new LinkedHashMap<String, String>();
		    	    	 mode = new ArrayList();
		    	    	 companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,"");
		    	    	// customerFeedBack = new ArrayList();
		    	    	// oaEvaluation = new LinkedHashMap<String, String>();
		    	    	 //daEvaluation = new LinkedHashMap<String, String>();
		    	    	 country = new LinkedHashMap<String, String>();
		    	    	// euVatList = new LinkedHashMap<String, String>();
		    	    	// euVatPercentList = new LinkedHashMap<String, String>();
		    	    	// Special = new LinkedHashMap<String, String>(); 
		    	    	// glcodes = new LinkedHashMap<String, String>();
		    	    	// military = new TreeMap<String, String>();
		    	    	// assignmentTypes = new LinkedHashMap<String, String>();
		    	    	// collectiveAssignmentTypes =  new LinkedHashMap<String, String>();
		    	    	 //reloService = new LinkedHashMap<String, String>();
		    	    	// service = new LinkedHashMap<String, String>();
		    	    	 visibleFields=ugwwActionTrackerManager.getFieldsNameToDisplay();
		    	    	 ostates = new LinkedHashMap<String, String>();
		    			 dstates = new LinkedHashMap<String, String>();
		    			 state = new LinkedHashMap<String, String>();
		    			 //originAddress = new LinkedHashMap<String, String>(); 
		    			 //destinationAddress = new LinkedHashMap<String, String>();
		    			 countryCod = new LinkedHashMap<String, String>();
		    			 routing = new TreeMap<String, String>();
		    			 job = new TreeMap<String, String>();
		    			// flagCarrierList = new LinkedHashMap<String, String>();
		    			// languageList = new LinkedHashMap<String, String>();
		    			 commodits = new TreeMap<String, String>();
		    			 commodit = new TreeMap<String, String>();
		    			 //Gson gson = new Gson();
		    			 String CompanyDivisionCombo=getRequest().getParameter("serviceOrder.companyDivision");
		    			 if(CompanyDivisionCombo!=null && (!(CompanyDivisionCombo.toString().equals("")))){
		    					
		    				}else{
		    					if(serviceOrderCombo!=null){
		    					CompanyDivisionCombo=serviceOrderCombo.getCompanyDivision();
		    					}
		    				}
		    			 String jobCombo=getRequest().getParameter("serviceOrder.job");
		    				if(jobCombo!=null && (!(jobCombo.toString().equals("")))){
		    					
		    				}else{
		    					if(serviceOrderCombo!=null){
		    					jobCombo=serviceOrderCombo.getJob();	
		    					}
		    				}
		    				String originCountry="";
				 			String destCountry ="";
				 			if(serviceOrderCombo!=null){
				 				if(serviceOrderCombo.getOriginCountryCode()!=null){
				 					String temp=getRequest().getParameter("serviceOrder.OriginCountryCode");
				 		            if(temp!=null && (!(temp.toString().equals("")))){
				 					
				 				    }else{
				 					temp = serviceOrderCombo.getOriginCountryCode();
				 				    }
				 					//originCountry = returnCountryCode(temp);
				 				}
				 				if(serviceOrderCombo.getDestinationCountryCode()!=null){
				 					String temp=getRequest().getParameter("serviceOrder.destinationCountryCode");
				 		            if(temp!=null && (!(temp.toString().equals("")))){
				 					
				 				    }else{
				 					temp = serviceOrderCombo.getDestinationCountryCode();
				 				    }
				 					//destCountry = returnCountryCode(temp);
				 				}
				 			}	
		    	    	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
		    	    	 for (RefMasterDTO refObj : allParamValue) {
		    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("PackingService")){
		    	    		 		packingServiceList.add(refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("COUNTRY")){
		    	    		 		grpCountryList.put(refObj.getDescription().trim(),refObj.getDescription().trim());
		    	    		 		countryCod.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("COUNTRY") && !refObj.getFlex4().trim().equals("") ){
		    	    		 		grpContinentList.put(refObj.getFlex4().trim(),refObj.getFlex4().trim());    
		    	    		 	}else if(refObj.getParameter().trim().equals("MODE")){
		    	    		 		grpModeList.put(refObj.getDescription().trim(),refObj.getDescription().trim());
		    	    		 		mode.add(refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("RELOCATIONSERVICES")){
		    	    		 		relocationServices.put(refObj.getCode().trim(), refObj.getFlex1().trim());
		    	    		 	}else if (refObj.getParameter().trim().equals("customerfeedback")){
		    	    		 		//customerFeedBack.add(refObj.getDescription().trim());
		    	    		 	}else if (refObj.getParameter().trim().equals("OAEVALUATION")){
		    	    		 		//oaEvaluation.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
		    	    		 	}else if (refObj.getParameter().trim().equals("DAEVALUATION")){
		    	    		 	//	daEvaluation.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
		    	    		 	}else if (refObj.getParameter().trim().equals("CURRENCY")){
		    	    		 		country.put(refObj.getCode().trim(),refObj.getCode().trim()); 
		    	    		 	}else if (refObj.getParameter().trim().equals("EUVAT")){
		    	    		 	//	euVatList.put(refObj.getCode().trim(),refObj.getDescription().trim()); 
		    	    		 		//euVatPercentList.put(refObj.getCode().trim(),refObj.getFlex1().trim());
		    	    		 	}else if (refObj.getParameter().trim().equals("SPECIAL")){
		    	    		 	//	Special.put(refObj.getCode().trim() + " : "+ refObj.getDescription().trim(),refObj.getCode().trim() + " : "+ refObj.getDescription().trim()); 
		    	    		 	}else if (refObj.getParameter().trim().equals("GLCODES")){
		    	    		 		//glcodes.put(refObj.getCode().trim() ,refObj.getCode().trim() + " : "+ refObj.getDescription().trim()); 
		    	    		 	}else if (refObj.getParameter().trim().equals("MILITARY")){
		    	    		 		//military.put(refObj.getCode().trim() ,refObj.getCode().trim() ); 
		    	    		 	}else if(refObj.getParameter().trim().equals("AssignmentType")){
		    	    		 		//collectiveAssignmentTypes.put(refObj.getCode().trim() ,refObj.getDescription().trim() );
		    	    		 		if(refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("ROLE_CORP_ACC_ASML")){
		    	    		 		//	assignmentTypes.put(refObj.getCode().trim() ,refObj.getDescription().trim() ); 
		    	    		 		}
		    	    	 		}else if (refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals("Relo"))){
		    	    		 		//reloService.put(refObj.getCode().trim() ,refObj.getDescription().trim() ); 
		    	    		 	}else if (refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("Relo"))){
		    	    		 		//service.put(refObj.getCode().trim() ,refObj.getDescription().trim() ); 
		    	    		 	}else if (refObj.getParameter().trim().equals("JOB")){
		    	    		 		if(CompanyDivisionCombo!=null && (!(CompanyDivisionCombo.toString().equals(""))) && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(CompanyDivisionCombo)) ){
		    	    		 			job.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}else{
		    	    		 			job.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}
		    	    		 		if( jobCombo!=null && (!(jobCombo.toString().equals(""))) && refObj.getCode().trim().equals(jobCombo) && refObj.getFlex3()!=null  ){
		    	    		 			//reloJobFlag = refObj.getFlex3().trim();
		        	    		 	}
		    	    		 	}else if(refObj.getParameter().trim().equals("COMMODITS")){
		    	    		 		if(jobCombo!=null && (!(jobCombo.toString().equals("")))&& (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(jobCombo)) ){
		    	    		 			commodits.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}else{
		    	    		 			commodits.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}
		    	    		 	}else if(refObj.getParameter().trim().equals("COMMODIT")){
		    	    		 		if(jobCombo!=null && (!(jobCombo.toString().equals(""))) && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(jobCombo)) ){
		    	    		 			commodit.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}else{
		    	    		 			commodit.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}
		    	    		 	}else if(refObj.getParameter().trim().equals("FLAGCARRIER")){
		    	    		 		//flagCarrierList.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("LANGUAGE")){
		    	    		 		 if(refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") ){
		    	    		 			// languageList.put(refObj.getFlex1().trim(),refObj.getCode().trim());
		    	    		 		 }
		    	    		 	}else {
		    	    		 		if(refObj.getParameter().trim().equals("STATE")){
		    	    		 			
		    	    		 			if(originCountry.equals(refObj.getBucket2().trim()))
		    	    		 				ostates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
		    	    		 			if(destCountry.equals(refObj.getBucket2().trim()))
		    	    		 				dstates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
		    	    		 		}
		    	    		 		if(parameterMap.containsKey(refObj.getParameter().trim())){
		    	    		 			tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
		    	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		    	    		 		}else{
		    	    		 			tempParemeterMap = new LinkedHashMap<String, String>();
		    	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		    	    		 		}
		    	    		 	}
		    	    		
						}
		    	    	 
		    	    	mode = SetUniqueList.decorate(mode);
		    	    	// 
		    	    	 
					//	packingServiceList=refMasterManager.findByParameters(corpId, "PackingService");
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: packingServiceList "+packingServiceList);
						//parameters="";
				//	 parameters="'CUST_STATUS','groupagestatus','STATE','COUNTRY', 'JOB','ROUTING','COMMODIT','COMMODITS','PKMODE','LOADSITE', 'SPECIFIC','JOBTYPE','MILITARY','SPECIAL','HOWPACK','AGTPER','EQUIP','PEAK','TCKTSERVC','DEPT','HOUSE', 'CONFIRM','TCKTACTN','WOINSTR','PAYTYPE','STATUSREASON','GLCODES','ISDRIVER','YESNO','SHIPTYPE','partnerType','SITOUTTA','omni',  'QUOTESTATUS','JOB_STATUS','ACC_CATEGORY','QUALITYSURVEYBY','ACC_BASIS','ACC_STATUS','CustomerSettled','AccEstmateStatus'";
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: parameters "+parameters);
							
						// parameterMap=refMasterManager.findByMultipleParameter(corpId, parameters);
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: parameterMap "+parameterMap);
							
						//grpStatusList=findByParameterLocal( "CUST_STATUS");
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: grpStatusList "+grpStatusList);
							 
						// grpCountryList=refMasterManager.findCountry(corpId, "COUNTRY");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: grpCountryList "+grpCountryList);
							
						// grpContinentList=refMasterManager.findContinentList(corpId, "COUNTRY");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: grpContinentList "+grpContinentList);
							
						// grpModeList=refMasterManager.findDescByParameter(corpId, "MODE");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: grpModeList "+grpModeList);
							
						// grpAgeStatusList=findByParameterLocal( "groupagestatus");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: grpAgeStatusList "+grpAgeStatusList);
							
						 //state = findByParameterLocal( "STATE");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: state "+state);
							
						 //ocountry = refMasterManager.findCountry(corpId, "COUNTRY");
						 //dcountry = refMasterManager.findCountry(corpId, "COUNTRY");
						 ocountry=grpCountryList;
						 dcountry=grpCountryList;
						// countryCod = findByParameterLocal( "COUNTRY");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: countryCod "+countryCod);
							
						// job = findByParameterLocal( "JOB");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: job "+job);
							
						// relocationServices=refMasterManager.findByRelocationServices(corpId, "RELOCATIONSERVICES");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: relocationServices "+relocationServices);
							
						 //job = refMasterManager.findByParameterJobRelo(corpId, "JOB");
						// routing = findByParameterLocal("ROUTING");
						// routing = GetSortedMap.sortByValues(routing);
						 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: routing "+routing);
							
					//	 commodit = findByParameterLocal("COMMODIT");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: commodit "+commodit);
							
						// commodits = findByParameterLocal("COMMODITS");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: commodits "+commodits);
							
						// mode = refMasterManager.findByParameters(corpId, "MODE");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: mode "+mode);
							
//		 	 service = refMasterManager.findByParameter(corpId, "SERVICE");
						/* Added By Kunal for BugID: 6626 */
						// customerFeedBack = refMasterManager.findByParameters(corpId, "customerfeedback");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: customerFeedBack "+customerFeedBack);
							
						// oaEvaluation = refMasterManager.findByParameterCodeDesc(corpId, "OAEVALUATION");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: oaEvaluation "+oaEvaluation);
							
						// daEvaluation = refMasterManager.findByParameterCodeDesc(corpId, "DAEVALUATION");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: daEvaluation "+daEvaluation);
							
						 /* Modifications complete */
							//pkmode = findByParameterLocal("PKMODE");
							//pkmode = GetSortedMap.sortByValues(pkmode);
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: pkmode "+pkmode);
							
						// loadsite =findByParameterLocal("LOADSITE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: loadsite "+loadsite);
							
						 //specific = findByParameterLocal("SPECIFIC");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: specific "+specific);
							
						// jobtype = findByParameterLocal("JOBTYPE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: jobtype "+jobtype);
							
						// military = findByParameterLocal("MILITARY");
							///logger.warn("USER AUDIT: ID: "+currentdate+" ## User: military "+military);
							
						// Special = findByParameterLocal("SPECIAL");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: Special "+Special);
							
						// Howpack =findByParameterLocal("HOWPACK");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: Howpack "+Howpack);
							
						 //AGTPER = findByParameterLocal("AGTPER");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: AGTPER "+AGTPER);
							
						// EQUIP = findByParameterLocal("EQUIP");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: EQUIP "+EQUIP);
							
						// Peak = findByParameterLocal("PEAK");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: Peak "+Peak);
							
						 //tcktservc = findByParameterLocal("TCKTSERVC");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: tcktservc "+tcktservc);
							
						 //dept= findByParameterLocal("DEPT");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: dept "+dept);
							
						 //house= findByParameterLocal("HOUSE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: house "+house);
							
						// confirm= findByParameterLocal("CONFIRM");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: confirm "+confirm);
							
						// tcktactn= findByParameterLocal("TCKTACTN");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: tcktactn "+tcktactn);
							
						// woinstr= findByParameterLocal("WOINSTR");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: woinstr "+woinstr);
							
						// paytype = findByParameterLocal("PAYTYPE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: paytype "+paytype);
							
						 //sale = refMasterManager.findUser(corpId, "ROLE_SALE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: sale "+sale);
							
						coord = refMasterManager.findUser(corpId, "ROLE_COORD");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: coord "+coord);
							
						// qc = refMasterManager.findUser(corpId, "ROLE_QC");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: qc "+qc);
							
						// statusReason = findByParameterLocal("STATUSREASON");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: statusReason "+statusReason);
							
						// glcodes = findByParameterLocal("GLCODES");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: glcodes "+glcodes);
							
						 //isdriver = findByParameterLocal("ISDRIVER");
						//	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: isdriver "+isdriver);
							
						// yesno = findByParameterLocal("YESNO");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: yesno "+yesno);
							
						 //shipmentType = findByParameterLocal("SHIPTYPE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: shipmentType "+shipmentType);
							
						 //partnerType = findByParameterLocal("partnerType");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: partnerType "+partnerType);
							
						 //SITOUTTA = findByParameterLocal("SITOUTTA");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: SITOUTTA "+SITOUTTA);
							
						// omni = findByParameterLocal("omni");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: omni "+omni);
							
						// QUOTESTATUS = findByParameterLocal("QUOTESTATUS");
						//	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: QUOTESTATUS "+QUOTESTATUS);
							
						 JOB_STATUS  =findByParameterLocal("JOB_STATUS");
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: JOB_STATUS "+JOB_STATUS);
							
						// category = findByParameterLocal("ACC_CATEGORY");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: category "+category);
							
						 //qualitySurveyBy = findByParameterLocal("QUALITYSURVEYBY");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: qualitySurveyBy "+qualitySurveyBy);
							
						 //category.put("", "");
						// basis=findByParameterLocal("ACC_BASIS");
						// basis.put("", "");
						//	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: basis "+basis);
							
						// country=refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: country "+country);
							
						// payingStatus=findByParameterLocal("ACC_STATUS");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: payingStatus "+payingStatus);
							
						// pricingData = refMasterManager.findUser(corpId, "ROLE_PRICING");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: pricingData "+pricingData);
							
						// billingData = refMasterManager.findUser(corpId, "ROLE_BILLING");
						//	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: billingData "+billingData);
							
					//	 payableData = refMasterManager.findUser(corpId, "ROLE_PAYABLE");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: payableData "+payableData);
							
						 //customerSettled = findByParameterLocal("CustomerSettled");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: customerSettled "+customerSettled);
							
						 //accEstmateStatus  = refMasterManager.findByParameter(corpId, "AccEstmateStatus");
							//accEstmateStatus = findByParameterLocal("AccEstmateStatus");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: accEstmateStatus "+accEstmateStatus);
							
						 //weightunits = new ArrayList();
						 //weightunits.add("Lbs");
						// w//eightunits.add("Kgs");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: weightunits "+weightunits);
							
						// volumeunits = new ArrayList();
						 //volumeunits.add("Cft");
						 //volumeunits.add("Cbm");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: volumeunits "+volumeunits);
							
						// lengthunits=new ArrayList();
						// lengthunits.add("Ft");
						// lengthunits.add("Mtr");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: lengthunits "+lengthunits);
							
						// feedback=new ArrayList();
						// feedback.add("Yes");
						// feedback.add("No");
							//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: feedback "+feedback);
							
						  
						//responseEvaluation=new HashMap();
						//responseEvaluation.put(new String("-1"),"N");
						//responseEvaluation.put(new String("1"),"Y");
						//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: responseEvaluation "+responseEvaluation);
						
						//euVatList = refMasterManager.findByParameterWithoutParent(corpId, "EUVAT");
					//	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: euVatList "+euVatList);
						
						// euVatPercentList = refMasterManager.findVatPercentList(corpId, "EUVAT");
						//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: euVatPercentList "+euVatPercentList);
						
						//estVatList = refMasterManager.findByParameterWithoutParent(corpId, "EUVAT"); 
						//estVatList=euVatList;
						//estVatPersentList = refMasterManager.findVatPercentList(corpId, "EUVAT");
						//estVatPersentList=euVatPercentList;
						 //company=companyManager.findByCorpID(corpId).get(0);
						//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: company "+company);
						//
						//jsonCountryCodeText = gson.toJson(countryCod);
						
						
						
						// User view  changes
						
						String roles ="'ROLE_COORD','ROLE_SALE','ROLE_QC','ROLE_PRICING','ROLE_PAYABLE','ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC'";
						//sale = new TreeMap<String, String>();
						//estimatorList = new TreeMap<String, String>();
						//qc = new TreeMap<String, String>();
						//pricingData = new TreeMap<String, String>();
						//billingData = new TreeMap<String, String>();
						//payableData = new TreeMap<String, String>();
						List <UserDTO> allUser = refMasterManager.getAllUser(corpId,roles);
						for (UserDTO userDTO : allUser) {
							if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
								if(jobCombo!=null && (!(jobCombo.toString().equals("")))  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo)  ) ){
									coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
								}
								coord.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){
								if(jobCombo!=null && (!(jobCombo.toString().equals(""))) && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo)  ) ){
									//estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
								}
								//sale.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_QC")){
								//qc.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
								//pricingData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
								//billingData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
								//payableData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}
						}
							
					} catch (Exception e) {
						logger.error(" ERROR in ServiceOrderAction getComboList ");
						e.printStackTrace();
					}
		    	     			  	
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
					  
					return SUCCESS;
		   }
		  //so dashboard
		  @SkipValidation
		    public String soDashboardSearchList() {
			  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			  //getComboList(sessionCorpID);
			  if(shipNumber==null){
				  shipNumber="";
			  }
			  if(registrationNumber==null){
				  registrationNumber="";
			  }
			  if(lastName==null){
				  lastName="";
			  }
			  if(firstName==null){
				  firstName="";
			  }
			  if(job1==null){
				  job1="";
			  }
			  if(coordinator==null){
				  coordinator="";
			  }
			  if(status==null){
				  status="";
			  }
			  if(companyDivision==null){
				  companyDivision="";
			  }
			  if(billToName==null){
				  billToName="";
			  }
			  if(originCity==null){
				  originCity="";
			  }
			  if(destinationCity==null){
				  destinationCity="";
			  }
			  if(activeStatus==null){
				  activeStatus=true;
			  }
			//System.out.println("so actionnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"+roleArray);
			  soDashboardList = ugwwActionTrackerManager.soDashboardSearchList(shipNumber,registrationNumber,job1, coordinator,status,sessionCorpID,roleArray,lastName,firstName,companyDivision,billToName,originCity,destinationCity,activeStatus);
			  //
			  getRequest().setAttribute("soLastName","");			
			   getComboList(sessionCorpID);
				serviceOrderSearchType = new ArrayList();
				serviceOrderSearchType.add("Default Match");
				serviceOrderSearchType.add("Start With");
				serviceOrderSearchType.add("End With");
				serviceOrderSearchType.add("Exact Match");
				if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
					serviceOrderSearchVal="Default Match";
				}
				defaultCompanyCode="1";
			   serviceOrdersExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			   List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			   if(sysDefaultDetail!=null && !(sysDefaultDetail.isEmpty()) && sysDefaultDetail.get(0)!=null){
				   SystemDefault sys= (SystemDefault)sysDefaultDetail.get(0);
				    miscVLJob= sys.getMiscVl();
			   }
			  // soDashboardList = ugwwActionTrackerManager.findSoDashboardList(sessionCorpID);
		   	  //String key = "Please enter your search criteria below";
		   	  //saveMessage(getText(key));
			  //
			 // String key = "Please enter your search criteria below";
		   	 // saveMessage(getText(key));
			  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			  return SUCCESS; 
			  
		  }
		  @SkipValidation
		    public String soDashboardSearchCriteria() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			getRequest().setAttribute("soLastName","");			
			   getComboList(sessionCorpID);
				serviceOrderSearchType = new ArrayList();
				serviceOrderSearchType.add("Default Match");
				serviceOrderSearchType.add("Start With");
				serviceOrderSearchType.add("End With");
				serviceOrderSearchType.add("Exact Match");
				if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
					serviceOrderSearchVal="Default Match";
				}
				defaultCompanyCode="1";
			   serviceOrdersExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			   Long t1=System.currentTimeMillis();
			   logger.warn("so dasboard list start .................................. ................");
			   //soDashboardList = ugwwActionTrackerManager.findSoDashboardList(sessionCorpID);
			   soDashboardList =new ArrayList<SODashboardDTO>();
			   Long t2=System.currentTimeMillis();
				logger.warn("so dasboard total execution time.................................. "+(t2-t1));
			   List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			   if(sysDefaultDetail!=null && !(sysDefaultDetail.isEmpty()) && sysDefaultDetail.get(0)!=null){
				   SystemDefault sys= (SystemDefault)sysDefaultDetail.get(0);
				    miscVLJob= sys.getMiscVl();
			   }
		   	  String key = "Please enter your search criteria below";
		   	  saveMessage(getText(key));
		   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		   	   return SUCCESS; 
		   }
		  //
		  @SkipValidation
		  public Map<String,String> findByParameterLocal(String parameter){ 
		  	localParameter = new HashMap<String, String>();
		  	if(parameterMap.containsKey(parameter)){
		  		localParameter = parameterMap.get(parameter);
		  	}
		  	return localParameter;
		  }
		  //
		  
	@SuppressWarnings("unchecked")
	public String integrationLogSearch() {
		getCompanyList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(so==null){
			so="";
		}
		integrationErrorList = ugwwActionTrackerManager
			.getErrorLogList(integrationId, operation, orderComplete, effectiveDate, statusCode,so,sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return "success";
		  }
	
	@SuppressWarnings("unchecked")
	public String lisaIntegrationLogSearch() {
		getCompanyList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(so==null){
			so="";
		}
		integrationErrorList = ugwwActionTrackerManager.getLisaLogList(integrationId, operation, orderComplete, effectiveDate, statusCode,registrationNumber,sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return "success";
		  }
	
	public String getErrorLogCorpId() {
		return errorLogCorpId;
	}

	public void setErrorLogCorpId(String errorLogCorpId) {
		this.errorLogCorpId = errorLogCorpId;
	}

	public String getErrorLogModule() {
		return errorLogModule;
	}

	public void setErrorLogModule(String errorLogModule) {
		this.errorLogModule = errorLogModule;
	}

	public String getErrorLogCreatedBy() {
		return errorLogCreatedBy;
	}

	public void setErrorLogCreatedBy(String errorLogCreatedBy) {
		this.errorLogCreatedBy = errorLogCreatedBy;
	}

	public Date getErrorLogCreatedOn() {
		return errorLogCreatedOn;
	}

	public void setErrorLogCreatedOn(Date errorLogCreatedOn) {
		this.errorLogCreatedOn = errorLogCreatedOn;
	}
	private String errorLogCorpId;
	private String errorLogModule;
	private String errorLogCreatedBy;
	private Date errorLogCreatedOn;
	@SuppressWarnings("unchecked")
	public String searchErrorLogList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		distinctCorpId=errorLogManager.findDistinct();
		if(errorLogCorpId==null){errorLogCorpId="";}
		if(errorLogModule==null){errorLogModule="";}
		if(errorLogCreatedBy==null){errorLogCreatedBy="";}
		if(errorLogCreatedOn==null){errorLogCreatedOn=null;}
		errorLogList = errorLogManager.searchErrorLogFile(errorLogCorpId,errorLogModule,errorLogCreatedBy,errorLogCreatedOn);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
		
	}
	
	@SuppressWarnings("unchecked")
	public String mssLogListMethod() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getCompanyList();
		mssLogList = ugwwActionTrackerManager
			.getMssLogList(sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return "success";
		  }
		  
	@SkipValidation
  public String centreVanlineUploadMethod() {
	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
	 getCompanyList();
	 centreVanlineList = ugwwActionTrackerManager
					.getCentreVanlineList(sessionCorpID);
     logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				  return "success";
	}	  
		  
   @SkipValidation
	public String driverDashboard(){
	   vanLineCodeList = vanLineManager.getVanLineCodeCompDiv(sessionCorpID);
		return SUCCESS;
	}
   
   private List driverDashboardList;
   private Date beginDate;
   private Date endDate;
   private String driverId;
   private List vanLineCodeList;
   private VanLineManager vanLineManager;
   private String driverAgency;
   @SkipValidation
   public String driverDashboardSearch(){
	   vanLineCodeList = vanLineManager.getVanLineCodeCompDiv(sessionCorpID);
	   driverDashboardList = ugwwActionTrackerManager.findDriverDashboardList(beginDate,endDate,driverId,driverAgency,sessionCorpID);
	   return SUCCESS;
   }
   private List driverDashBoardChildList;
   @SkipValidation
   public String driverDashBoardChildAjax(){
	   driverDashBoardChildList = ugwwActionTrackerManager.findDriverDBChildList(beginDate,endDate,driverId,sessionCorpID);
	   return SUCCESS;
   }
   private List driverDBSoDetailsList;
   @SkipValidation
   public String driverDashBoardSoDetailsAjax(){
	   driverDBSoDetailsList = ugwwActionTrackerManager.findDriverDBSoDetails(beginDate,endDate,driverId,sessionCorpID);
	   return SUCCESS;
   }
   private List driverIdList;   
   private String driverGsonData;
   @SkipValidation
   public String autocompleteDriverId(){
	   driverIdList=ugwwActionTrackerManager.findDriverDetails(driverId,sessionCorpID);
	   driverGsonData = new Gson().toJson(driverIdList);
	   return SUCCESS;
   }
   
	public UgwwActionTracker getUgwwActionTracker() {
		return ugwwActionTracker;
	}

	public void setUgwwActionTracker(UgwwActionTracker ugwwActionTracker) {
		this.ugwwActionTracker = ugwwActionTracker;
	}

	public List<UgwwActionTracker> getUgwwList() {
		return ugwwList;
	}

	public void setUgwwList(List<UgwwActionTracker> ugwwList) {
		this.ugwwList = ugwwList;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public String getSoNumber() {
		return soNumber;
	}

	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	

	public Date getActionTime() {
		return actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getTrackCorpID() {
		return trackCorpID;
	}

	public void setTrackCorpID(String trackCorpID) {
		this.trackCorpID = trackCorpID;
	}

	public ExtendedPaginatedList getSOListExt() {
		return SOListExt;
	}

	public void setSOListExt(ExtendedPaginatedList sOListExt) {
		SOListExt = sOListExt;
	}

	public PagingLookupManager getPagingLookupManager() {
		return pagingLookupManager;
	}

	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}

	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}

	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public String getShipno() {
		return shipno;
	}

	public void setShipno(String shipno) {
		this.shipno = shipno;
	}

	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public List getIntegrationErrorList() {
		return integrationErrorList;
	}

	public void setIntegrationErrorList(List integrationErrorList) {
		this.integrationErrorList = integrationErrorList;
	}

	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}

	public String getIntegrationId() {
		return integrationId;
	}

	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getOrderComplete() {
		return orderComplete;
	}

	public void setOrderComplete(String orderComplete) {
		this.orderComplete = orderComplete;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public List getUgwwErrorLogList() {
		return ugwwErrorLogList;
	}

	public void setUgwwErrorLogList(List ugwwErrorLogList) {
		this.ugwwErrorLogList = ugwwErrorLogList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public List getIntegrationLogs() {
		return integrationLogs;
	}

	public void setIntegrationLogs(List integrationLogs) {
		this.integrationLogs = integrationLogs;
	}

	public String getSo() {
		return so;
	}

	public void setSo(String so) {
		this.so = so;
	}

	public List getMssLogList() {
		return mssLogList;
	}

	public void setMssLogList(List mssLogList) {
		this.mssLogList = mssLogList;
	}

	public List getCentreVanlineList() {
		return centreVanlineList;
	}

	public void setCentreVanlineList(List centreVanlineList) {
		this.centreVanlineList = centreVanlineList;
	}

	public List getDriverDashboardList() {
		return driverDashboardList;
	}

	public void setDriverDashboardList(List driverDashboardList) {
		this.driverDashboardList = driverDashboardList;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public List getDriverDashBoardChildList() {
		return driverDashBoardChildList;
	}

	public void setDriverDashBoardChildList(List driverDashBoardChildList) {
		this.driverDashBoardChildList = driverDashBoardChildList;
	}

	public List getVanLineCodeList() {
		return vanLineCodeList;
	}

	public void setVanLineCodeList(List vanLineCodeList) {
		this.vanLineCodeList = vanLineCodeList;
	}

	public void setVanLineManager(VanLineManager vanLineManager) {
		this.vanLineManager = vanLineManager;
	}

	public String getDriverAgency() {
		return driverAgency;
	}

	public void setDriverAgency(String driverAgency) {
		this.driverAgency = driverAgency;
	}

	public List getDriverDBSoDetailsList() {
		return driverDBSoDetailsList;
	}

	public void setDriverDBSoDetailsList(List driverDBSoDetailsList) {
		this.driverDBSoDetailsList = driverDBSoDetailsList;
	}

	public String getDriverGsonData() {
		return driverGsonData;
	}

	public void setDriverGsonData(String driverGsonData) {
		this.driverGsonData = driverGsonData;
	}

	public List getDriverIdList() {
		return driverIdList;
	}

	public void setDriverIdList(List driverIdList) {
		this.driverIdList = driverIdList;
	}

	public List getServiceOrderSearchType() {
		return serviceOrderSearchType;
	}

	public void setServiceOrderSearchType(List serviceOrderSearchType) {
		this.serviceOrderSearchType = serviceOrderSearchType;
	}

	public String getServiceOrderSearchVal() {
		return serviceOrderSearchVal;
	}

	public void setServiceOrderSearchVal(String serviceOrderSearchVal) {
		this.serviceOrderSearchVal = serviceOrderSearchVal;
	}

	public String getDefaultCompanyCode() {
		return defaultCompanyCode;
	}

	public void setDefaultCompanyCode(String defaultCompanyCode) {
		this.defaultCompanyCode = defaultCompanyCode;
	}

	public ExtendedPaginatedList getServiceOrdersExt() {
		return serviceOrdersExt;
	}

	public void setServiceOrdersExt(ExtendedPaginatedList serviceOrdersExt) {
		this.serviceOrdersExt = serviceOrdersExt;
	}

	public List getPackingServiceList() {
		return packingServiceList;
	}

	public void setPackingServiceList(List packingServiceList) {
		this.packingServiceList = packingServiceList;
	}

	public Map<String, String> getLocalParameter() {
		return localParameter;
	}

	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}

	public Map<String, String> getGrpStatusList() {
		return grpStatusList;
	}

	public void setGrpStatusList(Map<String, String> grpStatusList) {
		this.grpStatusList = grpStatusList;
	}

	public Map<String, String> getGrpCountryList() {
		return grpCountryList;
	}

	public void setGrpCountryList(Map<String, String> grpCountryList) {
		this.grpCountryList = grpCountryList;
	}

	public Map<String, String> getGrpContinentList() {
		return grpContinentList;
	}

	public void setGrpContinentList(Map<String, String> grpContinentList) {
		this.grpContinentList = grpContinentList;
	}

	public Map<String, String> getGrpModeList() {
		return grpModeList;
	}

	public void setGrpModeList(Map<String, String> grpModeList) {
		this.grpModeList = grpModeList;
	}

	public Map<String, String> getGrpAgeStatusList() {
		return grpAgeStatusList;
	}

	public void setGrpAgeStatusList(Map<String, String> grpAgeStatusList) {
		this.grpAgeStatusList = grpAgeStatusList;
	}

	public Map<String, LinkedHashMap<String, String>> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(
			Map<String, LinkedHashMap<String, String>> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public Map<String, String> getState() {
		return state;
	}

	public void setState(Map<String, String> state) {
		this.state = state;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public Map<String, String> getOstates() {
		return ostates;
	}

	public void setOstates(Map<String, String> ostates) {
		this.ostates = ostates;
	}

	public Map<String, String> getDstates() {
		return dstates;
	}

	public void setDstates(Map<String, String> dstates) {
		this.dstates = dstates;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}

	public Map<String, String> getCommodits() {
		return commodits;
	}

	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}

	public ServiceOrder getServiceOrderCombo() {
		return serviceOrderCombo;
	}

	public void setServiceOrderCombo(ServiceOrder serviceOrderCombo) {
		this.serviceOrderCombo = serviceOrderCombo;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Map<String, String> getJOB_STATUS() {
		return JOB_STATUS;
	}

	public void setJOB_STATUS(Map<String, String> jOB_STATUS) {
		JOB_STATUS = jOB_STATUS;
	}

	public List getSoDashboardList() {
		return soDashboardList;
	}

	public void setSoDashboardList(List soDashboardList) {
		this.soDashboardList = soDashboardList;
	}

	public String getShipNumber() {
		return shipNumber;
	}
	 public void setCustomerFileManager(CustomerFileManager customerFileManager) {
			this.customerFileManager = customerFileManager;
		}
	 
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getJob1() {
		return job1;
	}

	public void setJob1(String job1) {
		this.job1 = job1;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRoleArray() {
		return roleArray;
	}

	public void setRoleArray(String roleArray) {
		this.roleArray = roleArray;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ErrorLog getErrorLog() {
		return errorLog;
	}

	public void setErrorLog(ErrorLog errorLog) {
		this.errorLog = errorLog;
	}

	public List getDistinctCorpId() {
		return distinctCorpId;
	}

	public void setDistinctCorpId(List distinctCorpId) {
		this.distinctCorpId = distinctCorpId;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}
	public List getErrorLogList() {
		return errorLogList;
	}

	public void setErrorLogList(List errorLogList) {
		this.errorLogList = errorLogList;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Map getVisibleFields() {
		return visibleFields;
	}

	public void setVisibleFields(Map visibleFields) {
		this.visibleFields = visibleFields;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getBillToName() {
		return billToName;
	}

	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public List getMoveForUList() {
		return moveForUList;
	}

	public void setMoveForUList(List moveForUList) {
		this.moveForUList = moveForUList;
	}

	public List getVoxmeEstimatorList() {
		return voxmeEstimatorList;
	}

	public void setVoxmeEstimatorList(List voxmeEstimatorList) {
		this.voxmeEstimatorList = voxmeEstimatorList;
	}

	public List getPricePointList() {
		return pricePointList;
	}

	public void setPricePointList(List pricePointList) {
		this.pricePointList = pricePointList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Boolean getEnableMoveForYou() {
		return enableMoveForYou;
	}

	public void setEnableMoveForYou(Boolean enableMoveForYou) {
		this.enableMoveForYou = enableMoveForYou;
	}

	public Boolean getEnableMSS() {
		return enableMSS;
	}

	public void setEnableMSS(Boolean enableMSS) {
		this.enableMSS = enableMSS;
	}

	public Boolean getVanlineEnabled() {
		return vanlineEnabled;
	}

	public void setVanlineEnabled(Boolean vanlineEnabled) {
		this.vanlineEnabled = vanlineEnabled;
	}

	public Boolean getVoxmeIntegration() {
		return voxmeIntegration;
	}

	public void setVoxmeIntegration(Boolean voxmeIntegration) {
		this.voxmeIntegration = voxmeIntegration;
	}

	public Boolean getEnablePricePoint() {
		return enablePricePoint;
	}

	public void setEnablePricePoint(Boolean enablePricePoint) {
		this.enablePricePoint = enablePricePoint;
	}

	public List getUGWWListForIntegration() {
		return UGWWListForIntegration;
	}

	public void setUGWWListForIntegration(List uGWWListForIntegration) {
		UGWWListForIntegration = uGWWListForIntegration;
	}

	
}
