<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <head> 
 <title>Payable Tracker</title>   
    <meta name="heading" content="Payable Tracker"/>   
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}

div.error, span.error, li.error, div.message {

width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
.table td, .table th, .tableHeaderTable td {   
    padding: 0.4em;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
    <script language="javascript" type="text/javascript">
	function clear_fields(){
		document.forms['PayableTrackerListForm'].elements['payableTrackJob'].value = "";
		document.forms['PayableTrackerListForm'].elements['payableTrackcompDiv'].value = "";
		document.forms['PayableTrackerListForm'].elements['payableTrackVendorName'].value = "";	
		document.forms['PayableTrackerListForm'].elements['payableTrackBillToName'].value = "";	
			document.forms['PayableTrackerListForm'].elements['loadAfterDate'].value = "";
		document.forms['PayableTrackerListForm'].elements['delAfterDate'].value = "";
		document.forms['PayableTrackerListForm'].elements['serviceCompDate'].value = "";
		document.forms['PayableTrackerListForm'].elements['payablePersonName'].value = "";
		document.forms['PayableTrackerListForm'].elements['fileCretedOn'].value="";
		document.forms['PayableTrackerListForm'].elements['decomuntCount'].checked = false;
	}
	function selectSearchField(){		
		var job=document.forms['PayableTrackerListForm'].elements['payableTrackJob'].value; 
		var companyDiv= document.forms['PayableTrackerListForm'].elements['payableTrackcompDiv'].value;
		var VendorName= document.forms['PayableTrackerListForm'].elements['payableTrackVendorName'].value;
		var BillToName= document.forms['PayableTrackerListForm'].elements['payableTrackBillToName'].value;
		var loadAfterDate=document.forms['PayableTrackerListForm'].elements['loadAfterDate'].value;
		var delAfterDate=document.forms['PayableTrackerListForm'].elements['delAfterDate'].value;
		var serviceComp = document.forms['PayableTrackerListForm'].elements['serviceCompDate'].value;
		var payablePerson = document.forms['PayableTrackerListForm'].elements['payablePersonName'].value;
		var fileCretedOn = document.forms['PayableTrackerListForm'].elements['fileCretedOn'].value;
		
		
		
		if(job=='' && companyDiv=='' && payablePerson=='' &&  VendorName=='' && BillToName==''  && loadAfterDate=='' && delAfterDate=='' && serviceComp=='' && fileCretedOn=='' ){
			alert('Check at least one parameter is entered');	
			return false;	
		}else{
			return true;
		}
	}
    </script>
 </head>
 <s:form id="PayableTrackerListForm" action="searchPayableTracker" method="post" > 
 <c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
				<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th>Job Type</th>
			<th>Company Division</th>
			<th>Vendor Name</th>	
			<th>Bill To Name</th>		
			<th>Loading After</th>
			<th>Payable Person</th>
			<th>Delivery After</th>
			<th>Srv. Cmpl. After</th>
		    <th>Docs uploaded after</th>
			<th>Show Records</th>
			<th>Document</th>
			<th></th>			
		</tr>
	</thead>	
	<tbody>
		<tr>			
			<td width="">
			     <s:select cssClass="list-menu" name="payableTrackJob" list="%{job}" cssStyle="width:100px" headerKey="" headerValue="" />
			</td>			
			<td width="">
			    <s:select cssClass="list-menu" name="payableTrackcompDiv" list="%{companyDivis}" cssStyle="width:100px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			<td width="">
			    <s:textfield name="payableTrackVendorName" size="20" required="true" cssStyle="width:100px;margin-top:2px;!width:100px;" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td width="">
			   <s:textfield name="payableTrackBillToName" size="20" required="true" cssStyle="width:100px;margin-top:2px;!width:100px;" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>				
			<c:if test="${not empty loadAfterDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="loadAfterDate" /></s:text>
				<td><s:textfield cssClass="input-text" id="date2" name="loadAfterDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty loadAfterDate}">
				<td><s:textfield cssClass="input-text" id="date2" name="loadAfterDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>		
			<td width="">
			   <s:textfield name="payablePersonName" size="20" required="true" cssStyle="width:100px;margin-top:2px;!width:100px;" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<c:if test="${not empty delAfterDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="delAfterDate" /></s:text>
				<td><s:textfield cssClass="input-text" id="date3" name="delAfterDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date3_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty delAfterDate}">
				<td><s:textfield cssClass="input-text" id="date3" name="delAfterDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date3_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			
			<c:if test="${not empty serviceCompDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceCompDate" /></s:text>
				<td><s:textfield cssClass="input-text" id="date4" name="serviceCompDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty serviceCompDate}">
				<td><s:textfield cssClass="input-text" id="date4" name="serviceCompDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>			
			<c:if test="${not empty fileCretedOn}">
				<s:text id="fileCretedOndate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="fileCretedOn" /></s:text>
				<td><s:textfield cssClass="input-text" id="date6" name="fileCretedOn" value="%{fileCretedOndate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date6_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty fileCretedOn}">
				<td><s:textfield cssClass="input-text" id="date6" name="fileCretedOn" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date6_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<!--	
			<td style="border:none;vertical-align:bottom;" class="listwhitetext">
			<s:checkbox key="billingComplete" cssStyle="vertical-align:middle; margin:5px 20px 3px 25px;"/>
			</td>
			-->		
			<td width="">
			     <s:select cssClass="list-menu" name="showRecord" list="%{showRecordList}" cssStyle="width:100px" />
			</td>
				<td width="">
			     <s:checkbox key="decomuntCount" value="${decomuntCount}" cssStyle="vertical-align:middle; margin:5px 20px 3px 25px;"/>
			</td>
		
		<td >
			<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" method="payableTrackerSearchList"  key="button.search" onclick="return selectSearchField()" />   
           <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/>
           </td>
		</tr>
	</tbody>
</table>
<c:out value="${searchresults}" escapeXml="false"   />  
<div style="!margin-top:7px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
  <div id="otabs" style="margin-bottom:31px;">
	<ul>
		<li><a class="current"><span>Service Order List</span></a></li>
	</ul>
</div> 
<div class="spnblk">&nbsp;</div> 
<s:set name="payableTrackerList" value="payableTrackerList" scope="request"/>
<display:table name="payableTrackerList" class="table"  requestURI="" id="payableTrackerList" >
<c:choose>
    <c:when test="${newAccountline=='Y'}">
    <display:column sortable="true" title="ShipNumber" >
		<a href="pricingList.html?sid=${payableTrackerList.id}" target="_blank">
	          <c:out value="${payableTrackerList.shipnumber}" />
	    </a> 
 	</display:column>
    </c:when>
    <c:otherwise>
	<display:column sortable="true" title="ShipNumber" >
		<a href="accountLineList.html?sid=${payableTrackerList.id}" target="_blank">
	          <c:out value="${payableTrackerList.shipnumber}" />
	    </a> 
	 </display:column>
 </c:otherwise>
 </c:choose>
<display:column property="vendorName" sortable="true" title="Vendor Name"/>
<display:column property="job" sortable="true" title="Job"/>
<display:column property="personPayable" sortable="true" title="Payable Person"/>
<display:column property="billtoname" sortable="true" title="Bill To Name"/>
<display:column sortable="true" title="Document"  sortProperty="documentName" style="width:20px">
	<div align="right">
		<a href="myFilesDocType.html?id=${payableTrackerList.id}&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false" target="_blank">
			 <c:out value="${payableTrackerList.documentName}" />
		</a>
	</div>
</display:column>
<display:column sortable="true" title="Est/Revised">
<div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${payableTrackerList.est}" />
                  </div></display:column>
<display:column sortable="true" title="Actual AMT">
<div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${payableTrackerList.exp}" />
                </div> </display:column>                  
<display:column sortable="true" title="Shortage" style="border-right:1px solid #61A5D0;padding:0px 2px 0px 0px;">
<c:if test="${payableTrackerList.exp != '0'}">
	<div align="right" style="color:red;">
	
		<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${payableTrackerList.shortage}" />
                  
               </div> 
</c:if>   
<c:if test="${payableTrackerList.exp == '0'}">
<div align="right">

	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${payableTrackerList.shortage}" />
                 
      
               </div> 
</c:if>                           
                 </display:column>
<display:column property="status" sortable="true" title="Paying Status"/>              
<display:column property="revRecg" sortable="true" title="Rev Recg" format="{0,date,dd-MMM-yyyy}" />
<display:column property="days" sortable="true" title="Days" style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="loading" sortable="true" title="Loading" format="{0,date,dd-MMM-yyyy}" />
<display:column property="loadDays" sortable="true" title="Days"  style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="delivery" sortable="true" title="Delivery" format="{0,date,dd-MMM-yyyy}" />
<display:column property="delDays" sortable="true" title="Days"  style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="svCompDate" sortable="true" title="Srv. Cmpl." format="{0,date,dd-MMM-yyyy}" />
<display:column property="svDays" sortable="true" title="Days"  style="border-right:1px solid #61A5D0;padding:0px;"/>
</display:table>

<div id="showExtract">
<table style="margin-bottom:5px">
<tr><td width="10px;"></td><td class="bgblue" >Payable Tracker Extract</td></tr>
<tr><td height="5px"></td></tr>
</table>
<table style="margin-bottom:5px">
<tr>
<td width="10px"></td>
<td><button type="button" class="cssbutton" style="width:155px;" onclick="extractFunction();"	>	
        			<span style="color:black;">Download into Excel</span></button></td>
</tr>
</table>
</div>
 </s:form>
 <script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	
	
	
	function extractFunction(){	
 		document.forms['PayableTrackerListForm'].action = 'payableTrackerExtract.html';
    	document.forms['PayableTrackerListForm'].submit(); 		
 	}
</script>
<script type="text/javascript">  
try{
var st2=document.getElementById('showExtract'); 
<c:if test="${payableTrackerList!='[]'}"> 
  st2.style.display = 'block';
 </c:if>
 <c:if test="${payableTrackerList =='[]'}">  	
		st2.style.display = 'none';
 </c:if>
}catch(e){} 
</script> 