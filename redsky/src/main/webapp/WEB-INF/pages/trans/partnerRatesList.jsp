<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerRatesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerRatesList.heading'/>"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
</style> 
</head>
<c:if test="${partnerType == 'AG'}">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partnerId}" />
<c:set var="fileID" value="%{partnerId}"/>
</c:if>
<s:form id="partnerRatesListForm" action="searchPartnerRates" method="post" validate="true">
<div id="newmnav">
				  <ul>
				    <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				    	<li><a href="editPartnerAddForm.html?id=${partner.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
			  			<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=AG&id=${partner.id}"><span>Agent Profile</span></a></li>
			  			<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					
						--><c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
						<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
						<c:if test='${paramValue == "View"}'>
							<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
						</c:if>
						<c:if test='${paramValue != "View"}'>
							<li><a href="searchPartnerAdmin.html?partnerType=AG"><span>Partner List</span></a></li>
						</c:if>
					</sec-auth:authComponent>
					<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
						<li><a href="editPartnerAddForm.html?id=${partner.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
						 <li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=AG&id=${partner.id}"><span>Agent Profile</span></a></li>
    
						<c:if test="${partner.partnerPortalActive == true}">
				  			<li><a href="partnerUsersList.html?id=${partner.id}"><span>Users & contacts</span></a></li>
						</c:if>
			  			
			  			<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						--><li><a href="partnerAgent.html"><span>Agent List</span></a></li>
					</sec-auth:authComponent>
				  </ul>
		</div>
		<div style="width:100%;">
		<div class="spn">&nbsp;</div>
	
		</div>
<div id="Layer4" style="width:100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerRateGrid.html?partnerId=${partner.id}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td >
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
				<table  cellspacing="1" cellpadding="1" border="0" >
					<tbody>
						<tr>
							<td width="" height="30px"><s:label label="  "/></td>
							<td class="listwhitetext"  ><b>Rates For</b></td>
							<td width="50px"><s:hidden name="partner.id" />
							    <s:textfield cssClass="input-textUpper" name="partner.firstName" required="true" size="15" readonly="true"/>
							</td>
							<td>
							    <s:textfield cssClass="input-textUpper" name="partner.lastName" required="true" size="50" readonly="true"/>
							</td>
						</tr>
						
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			
						
			

<s:set name="partnerRatess" value="partnerRatess" scope="request"/>  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>List</span></a></li>
		  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<display:table name="partnerRatess" class="table" requestURI="" id="partnerRatesList" export="true" defaultsort="1" pagesize="10" >   
	<display:column property="type" sortable="true" titleKey="partnerRates.type"
		href="editPartnerRates.html" paramId="id" paramProperty="id" />   
	
	<display:column property="effective" sortable="true" titleKey="partnerRates.effective" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="endDate" sortable="true" titleKey="partnerRates.endDate" format="{0,date,dd-MMM-yyyy}"/>
    
    <display:column sortable="true" title="Loose Min. Rate" style="width: 100px">
    	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${partnerRatesList.l1To999}" /></div>
    </display:column>
    <display:column sortable="true" title="Lift Van Min. Rate" style="width: 100px">
    	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${partnerRatesList.v1To999}" /></div>
    </display:column>
    <display:column sortable="true" title="LCL Min. Rate" style="width: 100px">
    	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${partnerRatesList.lcl499}" /></div>
    </display:column>
    <display:column sortable="true" title="AIR Min. Rate" style="width: 100px;">
    	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${partnerRatesList.air499}" /></div>
    </display:column>
    
    <display:setProperty name="paging.banner.item_name" value="partnerRates"/>   
    <display:setProperty name="paging.banner.items_name" value="partnerRatess"/>   
  
    <display:setProperty name="export.excel.filename" value="PartnerRates List.xls"/>   
    <display:setProperty name="export.csv.filename" value="PartnerRates List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="PartnerRates List.pdf"/>   
</display:table>  
</td>
</tr>
</tbody>
</table> 
<table>
<c:out value="${buttons}" escapeXml="false" /> 
<tr><td style="height:60px; !height:100px"></td></tr></table>
</div>
</s:form>
<script type="text/javascript">   
   highlightTableRows("partnerRatesList");   
   Form.focusFirstElement($("partnerRatesListForm"));      
</script>