package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsHomeFindingAssistancePurchase;

public interface DsHomeFindingAssistancePurchaseManager extends GenericManager<DsHomeFindingAssistancePurchase, Long>{
	List getListById(Long id);
}


