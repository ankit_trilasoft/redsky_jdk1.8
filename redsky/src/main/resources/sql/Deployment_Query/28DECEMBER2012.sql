ALTER TABLE `redsky`.`payrollallocation` ADD COLUMN `mode` VARCHAR(20) NULL  AFTER `companyDivision` ;

ALTER TABLE `redsky`.`billing` ADD COLUMN `primaryVatCode` VARCHAR(30) default '' AFTER `claimHandler` , ADD COLUMN `secondaryVatCode` VARCHAR(30) default '' AFTER `primaryVatCode` , ADD COLUMN `privatePartyVatCode` VARCHAR(30) default '' AFTER `secondaryVatCode` ;

CREATE TABLE `redsky`.`drivercommissionplan` (
  `id` BIGINT(20) UNSIGNED DEFAULT NULL,
  `corpID` VARCHAR(20) DEFAULT NULL,
  `createdBy` VARCHAR(82) DEFAULT NULL,
  `updatedBy` VARCHAR(82) DEFAULT NULL,
  `createdOn` DATETIME,
  `updatedOn` DATETIME,
  `charge` VARCHAR(25) DEFAULT NULL,
  `description` VARCHAR(50) DEFAULT NULL,
  `plan` VARCHAR(20) DEFAULT NULL,
  `contract` VARCHAR(65) DEFAULT NULL,
  `percent` DECIMAL(19,2) NOT NULL DEFAULT '0.00',
  `distributionAmount` BIT(1) DEFAULT b'0',
  `grossRevenue` BIT(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`drivercommissionplan` MODIFY COLUMN `id` BIGINT(20) UNSIGNED NOT NULL DEFAULT NULL AUTO_INCREMENT;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `paymentsent` DATETIME DEFAULT NULL;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `operationMgmtBy` VARCHAR(15) NULL  AFTER `costingDetailShow` ;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `billToName` VARCHAR(90) DEFAULT NULL AFTER `agentCode`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `commissionPlan` VARCHAR(20) DEFAULT NULL AFTER `excludePublicReloSvcsParentUpdate`;

CREATE TABLE `redsky`.`ugwwactiontracker` (
  `id` BIGINT(20) UNSIGNED DEFAULT NULL,
  `corpID` VARCHAR(20) DEFAULT NULL,
  `createdBy` VARCHAR(82) DEFAULT NULL,
  `updatedBy` VARCHAR(82) DEFAULT NULL,
  `createdOn` DATETIME,
  `updatedOn` DATETIME,
  `sonumber` VARCHAR(50) DEFAULT NULL,
  `username` VARCHAR(20) DEFAULT NULL,
  `action` VARCHAR(50) DEFAULT NULL,
  `actiontime` VARCHAR(65) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`ugwwactiontracker` MODIFY COLUMN `id` BIGINT(20) UNSIGNED NOT NULL DEFAULT NULL AUTO_INCREMENT;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('Billing','primaryVatCode' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('Billing','primaryVatCode' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'), ('Billing','secondaryVatCode' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('Billing','secondaryVatCode' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),('Billing','privatePartyVatCode' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),('Billing','privatePartyVatCode' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM');



ALTER TABLE `redsky`.`serviceorder` MODIFY COLUMN `portOfLading` VARCHAR(50)  DEFAULT NULL, MODIFY COLUMN `portOfEntry` VARCHAR(50)  DEFAULT NULL;











 

