
<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
       <title>
        <fmt:message key="Billing.title"/></title> 
    	<meta name="heading" content="<fmt:message key='Billing.heading'/>"/> 

 
 </head>
 
 <s:form name="billingHome" id="billingHome" action="billingDetail" method="post" validate="true" onsubmit="return validateForm()">
	<s:hidden name="corpid" value="${corpIDParam}"></s:hidden>
<div id="Layer1" style="width:80%;margin-top:-30px;!margin-top:0px;"> 
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Billing</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">	
<table>
	<tr>
<th><fmt:message key="year"/></th>
<th><fmt:message key="corpID"/></th>
</tr>
		<tr>
			
			<td><s:select id='year'  name='year' cssStyle="width:100px" cssClass="list-menu"  list="%{''}"/></td>
			<td>
		<c:if test="${corpIDParam == 'TSFT'}">
				<s:select name='corpID'cssStyle="width:100px" cssClass="list-menu" headerKey="" headerValue="" list="%{corpIDList}"/>
	 		</c:if>
 		<c:if test="${corpIDParam != 'TSFT'}">
	 			<s:textfield name='corpID'  id="corpID" value="%{corpIDParam}" readonly="true" cssClass="input-text" size="10" maxlength="11"/>
		</c:if></td>
 		</tr>

 </table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
 <s:submit type="button" cssClass="cssbutton"  key="button.goToBilling" ></s:submit>
 
 </s:form>
 
<script language="javascript" type="text/javascript">
  function yearlist(){
  	var currentTime = new Date()
	var year = currentTime.getFullYear()
	
	year=year+1;
	var ddl = document.getElementById('year');
	for(i = 1; i <= 5; i++) {
	year=year-1;
	var opt = document.createElement("option");
	opt.text=year;
	opt.value=year;
	document.getElementById("year").options.add(opt);
	}
			
}
function validateForm(){

if(document.forms['billingHome'].elements['year'].value=='')
	{
	alert("Select Year");
	return false;
	}
if(document.forms['billingHome'].elements['corpID'].value=='')
	{
	alert("Select CorpID");
	return false;
	}
}
</script>

 <script type="text/javascript">
	try{
	yearlist();	
	}
	catch(e){}
</script>
 