package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsOngoingSupportDao;
import com.trilasoft.app.model.DsOngoingSupport;
import com.trilasoft.app.service.DsOngoingSupportManager;


public class DsOngoingSupportManagerImpl extends GenericManagerImpl<DsOngoingSupport, Long> implements DsOngoingSupportManager { 
	DsOngoingSupportDao dsOngoingSupportDao;
	public DsOngoingSupportManagerImpl(DsOngoingSupportDao dsOngoingSupportDao) {
		super(dsOngoingSupportDao); 
        this.dsOngoingSupportDao = dsOngoingSupportDao; 	
	}
	
	public List getListById(Long id) {
		return dsOngoingSupportDao.getListById(id);
	}

}

