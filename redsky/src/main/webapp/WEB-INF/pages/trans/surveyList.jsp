<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>

<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_SALE") || userRole.equalsIgnoreCase("ROLE_COORD") || userRole.equalsIgnoreCase("ROLE_ADMIN")){
		userRole=role.getName();
		break;
	}
	
}
%>
<head>   
    <title><fmt:message key="surveyList.title"/></title>   
    <meta name="heading" content="<fmt:message key='surveyList.heading'/>"/>   

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:1px;
margin-top:-21px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

/*jitendra 24-10-2018*/
/* table{width:100% !important;} */
#Layer1{width:100% !important;}
/*end jitendra 24-10-2018*/
</style>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->	
<script language="javascript" type="text/javascript">

function clear_fields(){
document.forms['surveyListForm'].elements['consult'].value = "";
document.forms['surveyListForm'].elements['surveyFrom'].value = "";
document.forms['surveyListForm'].elements['surveyTo'].value = "";
document.forms['surveyListForm'].elements['surveyCity'].value = "";
document.forms['surveyListForm'].elements['surveyJob'].value = "";
}
function myEvents(){
	document.forms['surveyListForm'].action = "myCalendar.html";
	document.forms['surveyListForm'].submit();
}

function editCalendar(targetElement2){
	
	document.forms['surveyListForm'].action='editMyCalendar.html?from=list&decorator=popup&popup=true&from1=Survey&id='+targetElement2;
	document.forms['surveyListForm'].submit();
}

function autoPopulate_Date(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		//targetElement.form.elements['surveyListForm.surveyFrom'].value=datam;
		document.forms['surveyListForm'].elements['surveyFrom'].value=datam;
	}
	
	function autoPopulate_DateTo(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = (daym+7)+"-"+month+"-"+y.substring(2,4);
		document.forms['surveyListForm'].elements['surveyTo'].value=datam;
	}
	
	function isDate() {
	document.forms['surveyListForm'].action = "searchSurveysList.html?decorator=popup&popup=true&from=search";	
	var surFrom = document.forms['surveyListForm'].elements['surveyFrom'].value;
	var surTo = document.forms['surveyListForm'].elements['surveyTo'].value;
	if (surFrom=='' || surTo==''){
	alert('Please select From Date and To Date');
	   return false;
	   } else {
	   return isValidDate();
	  }  
	}

function openURL(sURL) {
opener.document.location = sURL;
} 

function findAgent(position,billtocode)
{
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billtocode);
	    ajax_showTooltip(url,position);
	    return false
}

function findOriginAddress(position,sequenceNumber)
{
		var url="customerOriginAddress.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" + encodeURI(sequenceNumber);
	    ajax_showTooltip(url,position);
	    return false
}

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw new SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

function isValidDate(){
/////////alert("ssssssssssssssssssssss");
 var date1 = ''; 
 var date2 = '';
 var date1 = document.forms['surveyListForm'].elements['date1'].value;
 var date2 = document.forms['surveyListForm'].elements['date2'].value; 
 if (date1 != ''){
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = month+"-"+day+"-"+year;
   }
   
   if (date2 != ''){
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
   } 
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  
  //// date1 = finalDate.split("-");
 /// var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
   sDate.setDate(sDate.getDate() + 7);
   var df1 = sDate.format("dd-mmm-yy");
  if(date1 !='' && date2 =='') {
  document.forms['surveyListForm'].elements['date2'].value=df1;
  return true;
  } else if (date2 !='' && daysApart<0){
  alert("From Date should be less than To Date");
 document.forms['surveyListForm'].elements['date2'].value='';
 //// } else {
  return false;
  }  
 }



function copyFromDate(){
 var date1 = ''; 
 date1 = document.forms['surveyListForm'].elements['date1'].value; 
 if(date1 !='')
 {
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = month+"-"+day+"-"+year;
  
   date1 = finalDate.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
   sDate.setDate(sDate.getDate() + 7);
   var df1 = sDate.format("dd-mmm-yy");
  document.forms['surveyListForm'].elements['date2'].value=df1;
  }
 }


function confirmSubmit(targetElement){
	var consult = document.forms['surveyListForm'].elements['consult'].value;
	var surveyFrom = document.forms['surveyListForm'].elements['surveyFrom'].value;
	var surveyTo = document.forms['surveyListForm'].elements['surveyTo'].value;
	var surveyCity = document.forms['surveyListForm'].elements['surveyCity'].value;
	var surveyJob = document.forms['surveyListForm'].elements['surveyJob'].value;
	
	var agree=confirm("Are you sure you want to Delete this  Survey");
	var did = targetElement;
	if (agree){
		location.href='surveyDeleted.html?id='+did+'&consult='+consult+'&surveyFrom='+surveyFrom+'&surveyTo='+surveyTo+'&surveyCity='+surveyCity+'&surveyJob='+surveyJob+'&decorator=popup&popup=true&from=survey';
	}else{
		return false;
	}
}


//function for Survey Map

function scheduleSurvey()
           {
           var jobTypeSchedule =document.forms['surveyListForm'].elements['surveyJob'].value;
           var surveyCity = document.forms['surveyListForm'].elements['surveyCity'].value;
           var surveyFrom = document.forms['surveyListForm'].elements['surveyFrom'].value;
	       var surveyTo = document.forms['surveyListForm'].elements['surveyTo'].value;
           var targetAddress="";
           var surveyTime="";
	       var surveyTime2="";
	       var estimated= document.forms['surveyListForm'].elements['consult'].value;
	       if(estimated!=''){           
           window.open("surveyListSchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&surveyCity="+surveyCity+"&fromDate="+surveyFrom+"&toDate="+surveyTo+"&decorator=popup&popup=true","forms","height=550,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }else{
           alert("Please select the Consultant to proceed.");
           }
           }
function scheduleCalendar()
{
	 var estimated= document.forms['surveyListForm'].elements['consult'].value;
	window.open('surveyCalendarList.html?estimated='+estimated+'&decorator=popup&popup=true','surveyCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');
}           

// End function.
</script>
<script language="javascript" type="text/javascript">
function calendarICSFunction(id){	
	var surveyType = 'SL';
	 	document.forms['surveyListForm'].action = "appointmentICS.html?cfId="+id+"&surveyType="+surveyType+"";
    	document.forms['surveyListForm'].submit();   
 	}
     
</script>
</head>


<div id="Layer5" style="width:100%"> 
<s:hidden name="fileID" id ="fileID" value="" />     
<s:form id="surveyListForm" action="searchSurveysList.html?decorator=popup&popup=true&from=search" method="post" >
<c:set var="from" value="<%=request.getParameter("from") %>" />
<s:hidden name="from" value="<%=request.getParameter("from") %>" />
<c:set var="id1" value="<%=request.getParameter("id1") %>" />
<s:hidden name="id1" value="<%=request.getParameter("id1") %>" />


<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:65px; height:25px"  
        onclick="document.forms['surveyListForm'].action='editMyEvent.html?decorator=popup&popup=true&from1=Survey';document.forms['surveyListForm'].submit();"  
        value="Add Event"/>   
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" key="button.search" onclick="return isDate();" /> 
	 <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set> 
  
<c:if test="${not empty id1}" >
		<div id="newmnav">
		  <ul>
		    <li><a href="editCustomerFile.html?id=<%=request.getParameter("id1") %> "><span>Customer File</span></a></li>
		    <li><a href="customerServiceOrders.html?id=<%=request.getParameter("id1") %> "><span>Service Orders</span></a></li>
		    <li><a href="customerRateOrders.html?id=<%=request.getParameter("id1") %>"><span>Rate Request</span></a></li>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Surveys<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="window.open('subModuleReports.html?id=<%=request.getParameter("id1") %>&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=Survey&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
		
		
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>


<div id="Layer1">

<table class="" style="width:100%;" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>   
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
				
						<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:-20px;!margin-top:-5px;"><span></span></div>
<div class="center-content">
				
				<table class="table" width="90%" >
				<thead>
				<tr>
				<th><fmt:message key="customerFile.estimator"/></th>
				<th>From&nbsp;Date</th>
				<th>To&nbsp;Date</th>
				<th><fmt:message key="customerFile.originCity"/></th>
				<th><fmt:message key="customerFile.job"/></th>
				
				</tr></thead>	
						<tbody>
						<tr>
							<td>
							    <s:select name="consult" list="%{saleConsultant}" cssStyle="width:125px" cssClass="list-menu" headerKey="" headerValue="" />
							</td>
							<%--
							<td class="listwhitetext"><s:select cssClass="list-menu"  name="consult" list="%{sale}" headerKey="" headerValue=""/></td>
							--%>
							<td>
							    <s:textfield name="surveyFrom" cssClass="input-text" id="date1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="isValidDate();"/><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
							<td>
							    <s:textfield name="surveyTo" cssClass="input-text" id="date2" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
							<td>
							    <s:textfield name="surveyCity" cssStyle="width:65px" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
							</td>
							<td>
							    <s:select name="surveyJob" cssStyle="width:165px" list="%{jobs}" cssClass="list-menu" headerKey="" headerValue=""/>
							</td>
							</tr>
							<tr>
							<td colspan="4"></td>
							<td style="border-left: hidden;">
							    <c:out value="${searchbuttons}" escapeXml="false" />   
							</td>
						</tr>
						</tbody>
					</table>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
				<s:set name="scheduledSurveys" value="scheduledSurveys" scope="request"/>  
		<div id="newmnav">
				  <ul>
				  	<c:if test="${not empty id1}" >
				  	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   	 <!--<li><a onclick="myEvents();"><span>Events</span></a></li>
				   	--></c:if>
				   	<c:if test="${empty id1}" >
				   	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   	</c:if>
				   	<li><a onclick="scheduleSurvey();"><span>Survey Map</span></a></li>
				   	<configByCorp:fieldVisibility componentId="component.field.Resource.SurveyCalendarView">
				   	<li><a onclick="scheduleCalendar();"><span>Survey Calendar</span></a></li>
				   	</configByCorp:fieldVisibility>
				  </ul>
		</div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
				<display:table name="scheduledSurveys" class="table" requestURI="" id="customerFileList" export="true" defaultsort="1" defaultorder="ascending" pagesize="50" style="width:100%;margin-top:0px; !margin-top:6px;" decorator='${!accountpopup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
				    <display:column property="surveyDate" sortable="true" style="width:180px;" titleKey="customerFile.survey" format="{0,date,dd-MMM-yyyy}"/>
				    <display:column sortable="true" titleKey="customerFile.estimator"  style="width:110px">
				    <c:choose>
				    <c:when test="${customerFileList.sequenceNumber == ''}">
				    	<c:if test="${customerFileList.userName != surveyCorpID}">
				    		<%--<a onclick="editCalendar(${customerFileList.id});" ><c:out value="${customerFileList.userName}" /></a>--%>
				    		<c:out value="${customerFileList.userName}" />
				    	</c:if>
				    	<c:if test="${customerFileList.userName == surveyCorpID}">
				    		<c:out value="${customerFileList.userName}" />
				    	</c:if>
				    </c:when>
				    <c:otherwise>
				    	<c:if test="${customerFileList.userName == '' || customerFileList.userName == null}">
				    		<%--<div onclick='location.href="editSurvey.html?from=list&id=${customerFileList.id}"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>--%>
				    	</c:if>
				    	<c:if test="${customerFileList.userName != '' || customerFileList.userName != null}">
					    	<%--<a href="editSurvey.html?from=list&id=${customerFileList.id}"><c:out value="${customerFileList.userName}" /></a>--%>
					    	<c:out value="${customerFileList.userName}" />
				    	</c:if>
				    </c:otherwise>
				    </c:choose>
				    </display:column>
				 	<configByCorp:fieldVisibility componentId="component.link.surveyList.eventType">
				    <display:column sortable="true" property="eventType"	titleKey="calendarFile.eventType"  style="width:110px"/>
				   </configByCorp:fieldVisibility>  
				    <display:column headerClass="containeralign" property="fromHrs" sortable="true" title="Start" style="width:10px; text-align: right;"/>
				    <display:column headerClass="containeralign" property="toHrs" sortable="true" title="End" style="width:10px; text-align: right;"/>
				    <display:column property="state" sortable="true" titleKey="customerFile.originState" style="width:80px"/>
				    <display:column title="Add to Calendar" style="width:80px;text-align:center;">
				    <c:if test="${customerFileList.sequenceNumber != ''}">
				     <img src="${pageContext.request.contextPath}/images/event_calendar.png" align="top" onclick="calendarICSFunction('${customerFileList.sequenceNumber}');" ></td>
				     </c:if>
				     </display:column>
				    <%--<display:column property="city" sortable="true" titleKey="customerFile.originCity"style="width:80px"/> --%>
				    <display:column title="City" style="width:290px" sortable="true" >
				    <c:out value="${customerFileList.city}" />
				    <c:if test="${customerFileList.city != '' && customerFileList.city != null}">
   				     <a><img align="middle" onmouseover="findOriginAddress(this,'${customerFileList.sequenceNumber}');" style="margin: 0px 0px 0px 8px;vertical-align:top;" src="images/address2.png"/></a>
   					</c:if>
   					</display:column>
				    <display:column sortable="true" title="Name" style="width:260px">
				     <c:choose>
				    <c:when test="${customerFileList.sequenceNumber == ''}">
				    	<c:if test="${customerFileList.userName != surveyCorpID}">
				    		<a onclick="editCalendar(${customerFileList.id});" ><c:out value="${customerFileList.activity}" /></a>
				    	</c:if>
				    	<c:if test="${customerFileList.userName == surveyCorpID}">
				    		<c:out value="${customerFileList.activity}" />
				    	</c:if>
				    </c:when>
				    <c:otherwise>
				    	<c:if test="${customerFileList.userName == '' || customerFileList.userName == null}">
				    	    <c:if test="${customerFileList.contlFlag == 'C'}">
				    			<a href="javascript:openURL('editCustomerFile.html?from=list&id=${customerFileList.id}');"></a>
				    		</c:if>
				    	    <c:if test="${customerFileList.contlFlag != 'C'}">
				    			<a href="javascript:openURL('QuotationFileForm.html?from=list&id=${customerFileList.id}');"></a>
				    		</c:if>
						</c:if>
				    	<c:if test="${customerFileList.userName != '' || customerFileList.userName != null}">
				    	    <c:if test="${customerFileList.contlFlag == 'C'}">				    	
						    	<a href="javascript:openURL('editCustomerFile.html?from=list&id=${customerFileList.id}');"><c:out value="${customerFileList.activity}" /></a>
					    	</c:if>
				    	    <c:if test="${customerFileList.contlFlag != 'C'}">				    	
						    	<a href="javascript:openURL('QuotationFileForm.html?from=list&id=${customerFileList.id}');"><c:out value="${customerFileList.activity}" /></a>
					    	</c:if>
				    	</c:if>
				    </c:otherwise>
				    </c:choose>
				    </display:column>
				  
				    <display:column property="billTo" sortable="true" title="Bill&nbsp;To" maxLength="12" style="width:80px"/>
				    <%--<display:column title="Bill&nbsp;To" style="width:290px">
				    <c:out value="${customerFileList.billTo}" />
				    <c:if test="${customerFileList.billTo != '' && customerFileList.billTo != null}">
   				     <a><img align="middle" onmouseover="findAgent(this,'${customerFileList.billToCode}');" style="margin: 0px 0px 0px 8px;" src="images/address2.png"/></a>
   					</c:if>
   					</display:column> --%>
	
				    <display:column property="jobType" sortable="true" title="Job" style="width:90px"/>
				   <c:if test="${surveyCorpID =='JKMS'}">
				   <display:column property="surveyType" sortable="true" title="SurveyType" style="width:90px"/>
				   </c:if>
				  <display:column title="Remove" style="width: 15px;">
				  <c:if test="${customerFileList.sequenceNumber == ''}">
				  <a><img align="middle" title="Remove" onclick="confirmSubmit(${customerFileList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>	
     				</a> 
     				</c:if>
     				</display:column>
				    <display:setProperty name="paging.banner.item_name" value="survey"/>   
				    <display:setProperty name="paging.banner.items_name" value="surveys"/>   
				
				    <display:setProperty name="export.excel.filename" value="SurveyDetail List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="SurveyDetail List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="SurveyDetail List.pdf"/>   
				</display:table> 			
			</td>
			
		</tr>
	</tbody>
</table>

</div>
<s:hidden name="popup" value="true"/>
<%-- <authz:authorize ifAnyGranted="ROLE_SALE">
<c:out value="${buttons}" escapeXml="false" />   
</authz:authorize>--%>
<% if(userRole.equalsIgnoreCase("ROLE_SALE") || userRole.equalsIgnoreCase("ROLE_CONSULTANT") || userRole.equalsIgnoreCase("ROLE_COORD")  || userRole.equalsIgnoreCase("ROLE_ADMIN")){ %>
<c:out value="${buttons}" escapeXml="false" /> 
<%} %>  
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<div id="mydiv" style="position:absolute"></div>

</s:form>
<c:if test="${hitFlag == 1}" >
	
		<c:redirect url="/searchSurveysList.html?consult=${consult}&surveyFrom=${surveyFrom}&surveyTo=${surveyTo}&surveyCity=${surveyCity}&surveyJob=${surveyJob}&decorator=popup&popup=true&from=search"/>
	</c:if>
</div>
<script type="text/javascript">  
try{
  Form.focusFirstElement($("surveyListForm")); 
}catch(e){} 
  try{
  if(document.forms['surveyListForm'].elements['from'].value=='survey') { 
    autoPopulate_Date(this);
    autoPopulate_DateTo(this);
    copyFromDate();
   }
   }
   catch(e){}
 </script>
 
 <script type="text/javascript">
	setOnSelectBasedMethods(["isValidDate()"]);
	setCalendarFunctionality();
</script>