package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.hibernate.Hibernate;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.trilasoft.app.dao.ReportBookmarkDao;
import com.trilasoft.app.dao.hibernate.ReportsDaoHibernate.DTO;

import com.trilasoft.app.model.ReportBookmark;



public class ReportBookmarkDaoHibernate extends GenericDaoHibernate<ReportBookmark, Long> implements ReportBookmarkDao {

	    public ReportBookmarkDaoHibernate() {
	        super(ReportBookmark.class);
	    }

	    public class DTO{
	    	private Object id;
	    	private Object description;
	    	private Object subModule;
	    	private Object reportComment;
	    	private Object module;
	    	private Object reportName;
	    	private Object docsxfer;
	    	private Object menu;
	    	private Object bid;
	    	private Object brid;
			private Object createdBy;
	    	private Object bkMarkcreatedBy;
			/**
			 * @return the bid
			 */
			public Object getBid() {
				return bid;
			}
			/**
			 * @param bid the bid to set
			 */
			public void setBid(Object bid) {
				this.bid = bid;
			}
			/**
			 * @return the bkMarkcreatedBy
			 */
			public Object getBkMarkcreatedBy() {
				return bkMarkcreatedBy;
			}
			/**
			 * @param bkMarkcreatedBy the bkMarkcreatedBy to set
			 */
			public void setBkMarkcreatedBy(Object bkMarkcreatedBy) {
				this.bkMarkcreatedBy = bkMarkcreatedBy;
			}
			/**
			 * @return the brid
			 */
			public Object getBrid() {
				return brid;
			}
			/**
			 * @param brid the brid to set
			 */
			public void setBrid(Object brid) {
				this.brid = brid;
			}
			/**
			 * @return the createdBy
			 */
			public Object getCreatedBy() {
				return createdBy;
			}
			/**
			 * @param createdBy the createdBy to set
			 */
			public void setCreatedBy(Object createdBy) {
				this.createdBy = createdBy;
			}
			/**
			 * @return the description
			 */
			public Object getDescription() {
				return description;
			}
			/**
			 * @param description the description to set
			 */
			public void setDescription(Object description) {
				this.description = description;
			}
			/**
			 * @return the docsxfer
			 */
			public Object getDocsxfer() {
				return docsxfer;
			}
			/**
			 * @param docsxfer the docsxfer to set
			 */
			public void setDocsxfer(Object docsxfer) {
				this.docsxfer = docsxfer;
			}
			/**
			 * @return the id
			 */
			public Object getId() {
				return id;
			}
			/**
			 * @param id the id to set
			 */
			public void setId(Object id) {
				this.id = id;
			}
			/**
			 * @return the menu
			 */
			public Object getMenu() {
				return menu;
			}
			/**
			 * @param menu the menu to set
			 */
			public void setMenu(Object menu) {
				this.menu = menu;
			}
			/**
			 * @return the module
			 */
			public Object getModule() {
				return module;
			}
			/**
			 * @param module the module to set
			 */
			public void setModule(Object module) {
				this.module = module;
			}
			/**
			 * @return the reportComment
			 */
			public Object getReportComment() {
				return reportComment;
			}
			/**
			 * @param reportComment the reportComment to set
			 */
			public void setReportComment(Object reportComment) {
				this.reportComment = reportComment;
			}
			/**
			 * @return the reportName
			 */
			public Object getReportName() {
				return reportName;
			}
			/**
			 * @param reportName the reportName to set
			 */
			public void setReportName(Object reportName) {
				this.reportName = reportName;
			}
			/**
			 * @return the subModule
			 */
			public Object getSubModule() {
				return subModule;
			}
			/**
			 * @param subModule the subModule to set
			 */
			public void setSubModule(Object subModule) {
				this.subModule = subModule;
			}
	    	
	    }
	    
		public List isExist(Long reportId, String userName) {
			return (List) getHibernateTemplate().find("from ReportBookmark where reportId='"+reportId+"' and createdBy='"+userName+"'");
		}

		public List getBookMarkedReportList(String userName ,String module, String menu, String subModule, String description, String sessionCorpID, Set<Role> roles) {
			String roleList = "";
			for (Role role : roles){
				if (!roleList.equals("")) roleList += ",";
				roleList += "'" + role.getId() + "'" ;
			}
			List list1 = new ArrayList();
			
			List bookMarkedList = this.getSession().createSQLQuery("select r.id, b.reportId as brid, b.id as bid,r.description, r.module, r.reportName, r.reportComment, r.subModule, r.docsxfer, r.menu,r.createdBy,b.createdBy as bkMarkcreatedBy " +
					"from reports r inner join reportbookmark b on r.id = b.reportId LEFT OUTER JOIN reports_role repRole on r.id=repRole.reports_id " +
					"where  " +
					"r.formReportFlag='R' " + 
					"and r.enabled='yes' " +
					"and b.createdBy='"+userName+"' " +
					"and r.module like '%" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
					"AND r.menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
					"AND r.subModule like '%" +subModule.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
					"AND r.description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
					"AND r.corpID like '" + sessionCorpID.replaceAll("'", "''") +  "%' AND (repRole.role_id in("+roleList+") or repRole.role_id is null ) group by r.id") 
					.addScalar("r.id", Hibernate.LONG)
		  .addScalar("brid", Hibernate.LONG)
		  .addScalar("bid", Hibernate.LONG)
		  .addScalar("r.description", Hibernate.STRING)
		  .addScalar("r.module", Hibernate.STRING)
		  .addScalar("r.reportName", Hibernate.STRING)
		  .addScalar("r.reportComment", Hibernate.STRING)
		  .addScalar("r.subModule", Hibernate.STRING)
		  .addScalar("r.docsxfer", Hibernate.STRING)
		  .addScalar("r.menu", Hibernate.STRING)
		  .addScalar("r.createdBy", Hibernate.STRING)
		  .addScalar("bkMarkcreatedBy", Hibernate.STRING)
		  .list();
		Iterator it=bookMarkedList.iterator();
		DTO dTO=null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dTO=new DTO();
			dTO.setId(Long.parseLong(row[0].toString()));
			dTO.setBrid(Long.parseLong(row[1].toString()));
			dTO.setBid(Long.parseLong(row[2].toString()));
			dTO.setDescription(row[3].toString());
			dTO.setModule(row[4].toString());
			dTO.setReportName(row[5].toString());
			dTO.setReportComment(row[6].toString());
			dTO.setSubModule(row[7].toString());
			dTO.setDocsxfer(row[8].toString());
			dTO.setMenu(row[9].toString());
			dTO.setCreatedBy(row[10].toString());
			dTO.setBkMarkcreatedBy(row[11].toString());
			list1.add(dTO);
		       }
			return list1;
		
		}



		public List getBookMarkedFormList(String userName ,String module, String menu, String subModule, String description, String sessionCorpID){
		List list1 = new ArrayList();
		
		List bookMarkedList = this.getSession().createSQLQuery("select r.id, b.reportId as brid, b.id as bid," +
				"r.description, r.module, r.reportName, r.reportComment, r.subModule, r.docsxfer, " +
				"r.menu,r.createdBy,b.createdBy as bkMarkcreatedBy " +
				"from reports r, reportbookmark b " +
				"where r.id = b.reportId " +
				"and r.formReportFlag='F' "+ 
				"and r.enabled='yes' " +
				"and b.createdBy='"+userName+"' " +
				"and r.module like '%" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
				"AND r.menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
				"AND r.subModule like '%" +subModule.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
				"AND r.description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
				"AND r.corpID like '" + sessionCorpID.replaceAll("'", "''") +  "%'").addScalar("r.id", Hibernate.LONG)
				  .addScalar("brid", Hibernate.LONG)
				  .addScalar("bid", Hibernate.LONG)
				  .addScalar("r.description", Hibernate.STRING)
				  .addScalar("r.module", Hibernate.STRING)
				  .addScalar("r.reportName", Hibernate.STRING)
				  .addScalar("r.reportComment", Hibernate.STRING)
				  .addScalar("r.subModule", Hibernate.STRING)
				  .addScalar("r.docsxfer", Hibernate.STRING)
				  .addScalar("r.menu", Hibernate.STRING)
				  .addScalar("r.createdBy", Hibernate.STRING)
				  .addScalar("bkMarkcreatedBy", Hibernate.STRING)
				  .list();
				Iterator it=bookMarkedList.iterator();
				DTO dTO=null;
				while(it.hasNext()){
					Object []row= (Object [])it.next();
					dTO=new DTO();
					dTO.setId(Long.parseLong(row[0].toString()));
					dTO.setBrid(Long.parseLong(row[1].toString()));
					dTO.setBid(Long.parseLong(row[2].toString()));
					dTO.setDescription(row[3].toString());
					dTO.setModule(row[4].toString());
					dTO.setReportName(row[5].toString());
					dTO.setReportComment(row[6].toString());
					dTO.setSubModule(row[7].toString());
					dTO.setDocsxfer(row[8].toString());
					dTO.setMenu(row[9].toString());
					dTO.setCreatedBy(row[10].toString());
					dTO.setBkMarkcreatedBy(row[11].toString());
					list1.add(dTO);
	       }
		return list1;
	
	}

}
