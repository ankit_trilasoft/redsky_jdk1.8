<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="refCrateList.title"/></title>   
    <meta name="heading" content="<fmt:message key='refCrateList.heading'/>"/>  
    <style>
    span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:0px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0;
text-align:right;
width:100%;
}
    </style> 
</head>
<c:set var="buttons">   
    <input type="button" class="cssbuttonA"  onclick="location.href='<c:url value="/editRefCrate.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<s:set name="refCrates" value="refCrates" scope="request"/> 
<div id="layer1" style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Carton&nbsp;List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<display:table name="refCrates" class="table" requestURI="" id="refCrateList" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%;margin-top:1px;"
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
    <c:if test="${empty param.popup}">  
		<display:column property="cartonType" sortable="true" titleKey="refCrate.cartonType"
		href="editRefCrate.html" paramId="id" paramProperty="id" style="width:100px;" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="refCrate.cartonType" style="width:100px;"/>   
    </c:if>    
    <display:column headerClass="containeralign" property="emptyContWeight" sortable="true" titleKey="refCrate.emptyContWeight" style="width:100px;text-align:right;"/>
    <display:column property="unit1" sortable="true" titleKey="refCrate.unit1" style="width:35px"/>
    <display:column headerClass="containeralign" property="length" sortable="true" titleKey="refCrate.length" style="width:50px; text-align: right;"/>
    <display:column headerClass="containeralign" property="width" sortable="true" titleKey="refCrate.width" style="width:50px;text-align: right;"/>
    <display:column headerClass="containeralign" property="height" sortable="true" titleKey="refCrate.height" style="width:50px;text-align: right;"/>
    <display:column property="unit2" sortable="true" titleKey="refCrate.unit2" style="width:35px"/>
    <display:column headerClass="containeralign" property="volume" sortable="true" titleKey="refCrate.volume" style="width:50px;text-align: right;"/>
    <display:column property="unit3" sortable="true" titleKey="refCrate.unit3" style="width:35px"/>
    
    
    
    <display:setProperty name="paging.banner.item_name" value="refCrate"/>   
    <display:setProperty name="paging.banner.items_name" value="refCrates"/>   
  
    <display:setProperty name="export.excel.filename" value="RefCrate List.xls"/>   
    <display:setProperty name="export.csv.filename" value="RefCrate List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="RefCrate List.pdf"/>   
</display:table>  
</div>
 <c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<script type="text/javascript">   
   highlightTableRows("refCrateList");   
   </script>