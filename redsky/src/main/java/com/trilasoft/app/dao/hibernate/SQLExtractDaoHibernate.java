package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.SQLExtractDao;
import com.trilasoft.app.dao.hibernate.CustomerFileDaoHibernate.QuotationServiceOrder;
import com.trilasoft.app.model.SQLExtract;

public class SQLExtractDaoHibernate extends GenericDaoHibernate<SQLExtract, Long> implements SQLExtractDao {

	public SQLExtractDaoHibernate() {   
        super(SQLExtract.class);   
    }

	public List getAllList(String sessionCorpID){
		String query="select * from sqlextract se where tested='Tested' and corpID in ('TSFT','"+sessionCorpID+"')";
		return getSqlExtract(query);
	}
	
	public List getSqlExtract(String query){
		List <Object> extractList = new ArrayList();
		List list=this.getSession().createSQLQuery(query)
		.addScalar("se.id", Hibernate.LONG)
		.addScalar("se.description", Hibernate.STRING)
        .addScalar("se.fileName", Hibernate.STRING)
        .addScalar("se.fileType", Hibernate.STRING)
        .addScalar("se.sqlText", Hibernate.STRING)
        .addScalar("se.corpID", Hibernate.STRING)
        .addScalar("se.createdBy", Hibernate.STRING)
        .addScalar("se.createdOn", Hibernate.DATE)
        .addScalar("se.updatedBy", Hibernate.STRING)
        .addScalar("se.updatedOn", Hibernate.DATE)
        .addScalar("se.tested", Hibernate.STRING)
        .addScalar("se.whereClause1", Hibernate.STRING)
        .addScalar("se.whereClause2",Hibernate.STRING)
        .addScalar("se.whereClause3",Hibernate.STRING)
        .addScalar("se.whereClause4",Hibernate.STRING)
        .addScalar("se.whereClause5",Hibernate.STRING)
        .addScalar("se.orderBy",Hibernate.STRING)
        .addScalar("se.groupBy",Hibernate.STRING)
        .addScalar("se.isdateWhere1",Hibernate.BOOLEAN)
        .addScalar("se.isdateWhere2",Hibernate.BOOLEAN)
        .addScalar("se.isdateWhere3",Hibernate.BOOLEAN)
        .addScalar("se.isdateWhere4",Hibernate.BOOLEAN)
        .addScalar("se.isdateWhere5",Hibernate.BOOLEAN)
        .addScalar("se.displayLabel1",Hibernate.STRING)
        .addScalar("se.displayLabel2",Hibernate.STRING)
        .addScalar("se.displayLabel3",Hibernate.STRING)
        .addScalar("se.displayLabel4",Hibernate.STRING)
        .addScalar("se.displayLabel5",Hibernate.STRING)
        .addScalar("se.defaultCondition1",Hibernate.STRING)
        .addScalar("se.defaultCondition2",Hibernate.STRING)
        .addScalar("se.defaultCondition3",Hibernate.STRING)
        .addScalar("se.defaultCondition4",Hibernate.STRING)
        .addScalar("se.defaultCondition5",Hibernate.STRING)
        .addScalar("se.email",Hibernate.STRING)
        .addScalar("se.sqlQueryScheduler",Hibernate.STRING)
        .addScalar("se.perMonthScheduler",Hibernate.STRING)
        .addScalar("se.perWeekScheduler",Hibernate.STRING)
      
        .list();	
		Iterator it=list.iterator();
		  while(it.hasNext())
		  {
			  Object [] row=(Object[])it.next();
			  SQLExtract sqlExtractList=new SQLExtract();
			  sqlExtractList.setId((Long) row[0]);
			  sqlExtractList.setDescription((String) row[1]);
			  sqlExtractList.setFileName((String) row[2]);
			  sqlExtractList.setFileType((String) row[3]);
			  sqlExtractList.setSqlText((String) row[4]);
			  sqlExtractList.setCorpID((String) row[5]);
			  sqlExtractList.setCreatedBy((String) row[6]);
			  sqlExtractList.setCreatedOn((Date) row[7]);
			  sqlExtractList.setUpdatedBy((String) row[8]);
			  sqlExtractList.setUpdatedOn((Date) row[9]);
			  sqlExtractList.setTested((String) row[10]);
			  sqlExtractList.setWhereClause1((String) row[11]); 
			  sqlExtractList.setWhereClause2((String) row[12]);
			  sqlExtractList.setWhereClause3((String) row[13]);
			  sqlExtractList.setWhereClause4((String) row[14]);
			  sqlExtractList.setWhereClause5((String) row[15]);
			  sqlExtractList.setOrderBy((String) row[16]);
			  sqlExtractList.setGroupBy((String) row[17]);
			  sqlExtractList.setIsdateWhere1((Boolean) row[18]);
			  sqlExtractList.setIsdateWhere2((Boolean) row[19]);
			  sqlExtractList.setIsdateWhere3((Boolean) row[20]);
			  sqlExtractList.setIsdateWhere4((Boolean) row[21]);
			  sqlExtractList.setIsdateWhere5((Boolean) row[22]);
			  sqlExtractList.setDisplayLabel1((String) row[23]);
			  sqlExtractList.setDisplayLabel2((String) row[24]);
			  sqlExtractList.setDisplayLabel3((String) row[25]);
			  sqlExtractList.setDisplayLabel4((String) row[26]);
			  sqlExtractList.setDisplayLabel5((String) row[27]);
			  sqlExtractList.setDefaultCondition1((String) row[28]);
			  sqlExtractList.setDefaultCondition2((String) row[29]);
			  sqlExtractList.setDefaultCondition3((String) row[30]);
			  sqlExtractList.setDefaultCondition4((String) row[31]);
			  sqlExtractList.setDefaultCondition5((String) row[32]);
			  sqlExtractList.setEmail((String) row[33]);
			  sqlExtractList.setSqlQueryScheduler((String) row[34]);
			  sqlExtractList.setPerMonthScheduler((String) row[35]);
			  sqlExtractList.setPerWeekScheduler((String) row[36]);
			  extractList.add(sqlExtractList);			  
		  }
		  return extractList;
		
	}
	
	public List getAllUserList(String sessionCorpID){
		String query="select * from sqlextract se where corpID in ('TSFT','"+sessionCorpID+"')";
		return getSqlExtract(query);
	}
	
	public String testSQLQuery(String sqlQuery) {
		
		String message = "";
		try{
			this.getSession().createSQLQuery(sqlQuery).list();
		}catch(Exception ex){
			message = ex.getMessage();
			return message;
		}
		return null;		
	}	
	
	public List sqlDataExtract(String sqlText){
		return this.getSession().createSQLQuery(sqlText).list();
	}

	public List getAllCorpId() {
		//return this.getSession().createSQLQuery("select corpID from company").list();
		return getHibernateTemplate().find("select corpID from Company");
	}

	public List  getUniquename(String fileName) {
		/*String query="select fileName from sqlextract se where fileName="+fileName+"";
		return getSqlExtract(query);*/
		return getHibernateTemplate().find("select fileName from SQLExtract where fileName=?",fileName);
	}

	public List getSearch(String fileName, String corpID) {
		String query="select * from sqlextract se where fileName like'%"+fileName.replaceAll("'", "''").replaceAll(":", "''")+"%' and corpID like '%"+corpID.replaceAll("'", "''").replaceAll(":", "''")+"%'";
		return getSqlExtract(query);
	}

	public List getSchedulerQueryList() {
		String query="select id from  sqlextract se where email <> '' and email is not null and corpID in (select corpID from company where status is true)";
		List list=this.getSession().createSQLQuery(query).list();
		return list;
	}

	public List getSqlExtraxtSearch(String fileName, String description,String sessionCorpID) {
		String query="select * from sqlextract se where fileName like'%"+fileName.replaceAll("'", "''").replaceAll(":", "''")+"%' and description like '%"+description.replaceAll("'", "''").replaceAll(":", "''")+"%' and corpID in ('TSFT','"+sessionCorpID+"')";
		return getSqlExtract(query);
	}

	public List getAllSqlExtractFiles(){
		return this.getSession().createSQLQuery("select id from sqlextract").list();
	}
	
	public List getAllListForTSFT(){
		String query="select * from sqlextract se";
		return getSqlExtract(query);		
	}
	
	public List getSqlExtractByID(Long id,String sessionCorpID ){
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
			String query="select * from sqlextract se where id="+id+"";
			return getSqlExtract(query);		
		}else{
			String query="select * from sqlextract se where id="+id+" and corpID in ('TSFT','"+sessionCorpID+"')";
			return getSqlExtract(query);
		}
	//	return getSqlExtract(query);
		
	}
	public int deleteSqlExtract(Long id) {
		return	this.getSession().createSQLQuery("delete from sqlextract where id='"+id+"'").executeUpdate();	
	}
	
}
