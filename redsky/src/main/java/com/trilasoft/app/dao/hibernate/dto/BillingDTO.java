package com.trilasoft.app.dao.hibernate.dto;


public class BillingDTO {
	
	  private Object shipNumber;
	  private Object registrationNumber; 
	  private Object firstName;
	  private Object lastName;
	  private Object billToCode;
	  private Object billToName;
	  private Object storagePerMonth;
	  private Object charge;
	  private Object job;
	  private Object wording;
	  private Object postGrate;
	  private Object onHand;
	  private Object createdBy ;
	  private Object updatedBy;
	  private Object createdOn;
	  private Object updatedOn;
	  private Object insuranceHas;
	  private Object insuranceRate;
	  private Object insuranceValueActual;
	  private Object insuranceBuyRate;
	  private Object totalInsrVal;
	  private Object perValue;
	  private Object storageBillingParty;
	  private Object cycle;
	  private Object contract;
	  private Object minimum;
	  private Object quantityItemRevised ;
	  private Object quantityRevisedPreset;
	  private Object perItem;
	  private Object toDates;
	  private Object dateDiff;
	  private Object glCode;
	  private Object divideMultiply;
	  private Object forChecking;//forChecking length
	  private Object description;
	  private Object useDiscount;
	  private Object pricePre;
	  private Object sitdiscount;
	  private Object billToAuthority;
	  private Object deliveryShipper;
	  private Object deliveryA;
	  private Object worldBankAuthorizationNumber;
	  private Object billThroughFrom;
	  private Object vatPercent;
	  private Object vatAmount;
	  private Object vatDescr;
	  private Object payglCode;
	  private Object baseInsuranceValue;
	  private Object companyDivision;
	  
	  private Object billingCurrency;
	  private Object vendorStoragePerMonth;
	  private Object vendorStorageVatDescr;
	  private Object vendorStorageVatPercent;
	  private Object vendorBillingCurency;
	  private Object vendorCode1;
	  private Object vendorName1;
	  private Object insuranceCharge1;
	  private Object vendorVatAmount;
	  private Object contractCurrency;
	  private Object payableContractCurrency;
	  private Object contractRevenueAmount;
	  private Object contractExpenseAmount;
	  private Object billingContractCurrency;
	  private Object billingPayableContractCurrency;
	  private Object insuranceVatAmount;
	  private Object insuranceStoragePerMonthForPayable;
	  private boolean isInsurancePayableRequired;
	  private Object currency;
	  private boolean VATExclude;
	  private String contractType;
	  private Object insurancePerMonth;
	  private Object insuranceValueEntitle;
	  private Object payableRate;
	  private Object vendorCodeVatCode;
	  private boolean dmmInsurancePolicy;
	  private Object bookingAgentVatCode;
	  private Object formulaText;
	  private Object serviceOrderId;
	  private Object costElement;
	  private Object costElementDescription;
	  private Object storageMeasurement;
	  private Object chargesDescription;
	  
	  public Object getVendorCodeVatCode() {
		return vendorCodeVatCode;
	}
	public void setVendorCodeVatCode(Object vendorCodeVatCode) {
		this.vendorCodeVatCode = vendorCodeVatCode;
	}
	public Object getBillToCode() {
			return billToCode;
		}
		public void setBillToCode(Object billToCode) {
			this.billToCode = billToCode;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getCharge() {
			return charge;
		}
		public void setCharge(Object charge) {
			this.charge = charge;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getRegistrationNumber() {
			return registrationNumber;
		}
		public void setRegistrationNumber(Object registrationNumber) {
			this.registrationNumber = registrationNumber;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		/*public Object getStoragePerMonth() {
			return storagePerMonth;
		}
		public void setStoragePerMonth(Object storagePerMonth) {
			this.storagePerMonth = storagePerMonth;
		}*/
		
		
		public Object getWording() {
			return wording;
		}
		public void setWording(Object wording) {
			this.wording = wording;
		}
		public Object getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Object createdBy) {
			this.createdBy = createdBy;
		}
		public Object getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Object createdOn) {
			this.createdOn = createdOn;
		}
		public Object getOnHand() {
			return onHand;
		}
		public void setOnHand(Object onHand) {
			this.onHand = onHand;
		}
		public Object getPostGrate() {
			return postGrate;
		}
		public void setPostGrate(Object postGrate) {
			this.postGrate = postGrate;
		}
		public Object getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
		public Object getInsuranceHas() {
			return insuranceHas;
		}
		public void setInsuranceHas(Object insuranceHas) {
			this.insuranceHas = insuranceHas;
		}
		public Object getInsuranceRate() {
			return insuranceRate;
		}
		public void setInsuranceRate(Object insuranceRate) {
			this.insuranceRate = insuranceRate;
		}
		public Object getInsuranceValueActual() {
			return insuranceValueActual;
		}
		public void setInsuranceValueActual(Object insuranceValueActual) {
			this.insuranceValueActual = insuranceValueActual;
		}
		public Object getTotalInsrVal() {
			return totalInsrVal;
		}
		public void setTotalInsrVal(Object totalInsrVal) {
			this.totalInsrVal = totalInsrVal;
		}
		public Object getPerValue() {
			return perValue;
		}
		public void setPerValue(Object perValue) {
			this.perValue = perValue;
		}
		public Object getStorageBillingParty() {
			return storageBillingParty;
		}
		public void setStorageBillingParty(Object storageBillingParty) {
			this.storageBillingParty = storageBillingParty;
		}
		public Object getCycle() {
			return cycle;
		}
		public void setCycle(Object cycle) {
			this.cycle = cycle;
		}
		public Object getContract() {
			return contract;
		}
		public void setContract(Object contract) {
			this.contract = contract;
		}
		public Object getMinimum() {
			return minimum;
		}
		public void setMinimum(Object minimum) {
			this.minimum = minimum;
		}
		public Object getPerItem() {
			return perItem;
		}
		public void setPerItem(Object perItem) {
			this.perItem = perItem;
		}
		public Object getQuantityItemRevised() {
			return quantityItemRevised;
		}
		public void setQuantityItemRevised(Object quantityItemRevised) {
			this.quantityItemRevised = quantityItemRevised;
		}
		public Object getQuantityRevisedPreset() {
			return quantityRevisedPreset;
		}
		public void setQuantityRevisedPreset(Object quantityRevisedPreset) {
			this.quantityRevisedPreset = quantityRevisedPreset;
		}
		public Object getDateDiff() {
			return dateDiff;
		}
		public void setDateDiff(Object dateDiff) {
			this.dateDiff = dateDiff;
		}
		public Object getToDates() {
			return toDates;
		}
		public void setToDates(Object toDates) {
			this.toDates = toDates;
		}
		public Object getGlCode() {
			return glCode;
		}
		public void setGlCode(Object glCode) {
			this.glCode = glCode;
		}
		public Object getDivideMultiply() {
			return divideMultiply;
		}
		public void setDivideMultiply(Object divideMultiply) {
			this.divideMultiply = divideMultiply;
		}
		public Object getForChecking() {
			return forChecking;
		}
		public void setForChecking(Object forChecking) {
			this.forChecking = forChecking;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getPricePre() {
			return pricePre;
		}
		public void setPricePre(Object pricePre) {
			this.pricePre = pricePre;
		}
		public Object getUseDiscount() {
			return useDiscount;
		}
		public void setUseDiscount(Object useDiscount) {
			this.useDiscount = useDiscount;
		}
		public Object getSitdiscount() {
			return sitdiscount;
		}
		public void setSitdiscount(Object sitdiscount) {
			this.sitdiscount = sitdiscount;
		}
		public Object getBillToAuthority() {
			return billToAuthority;
		}
		public void setBillToAuthority(Object billToAuthority) {
			this.billToAuthority = billToAuthority;
		}
		public Object getStoragePerMonth() {
			return storagePerMonth;
		}
		public void setStoragePerMonth(Object storagePerMonth) {
			this.storagePerMonth = storagePerMonth;
		}
		public Object getDeliveryA() {
			return deliveryA;
		}
		public void setDeliveryA(Object deliveryA) {
			this.deliveryA = deliveryA;
		}
		public Object getDeliveryShipper() {
			return deliveryShipper;
		}
		public void setDeliveryShipper(Object deliveryShipper) {
			this.deliveryShipper = deliveryShipper;
		}
		public Object getWorldBankAuthorizationNumber() {
			return worldBankAuthorizationNumber;
		}
		public void setWorldBankAuthorizationNumber(Object worldBankAuthorizationNumber) {
			this.worldBankAuthorizationNumber = worldBankAuthorizationNumber;
		}
		public Object getBillThroughFrom() {
			return billThroughFrom;
		}
		public void setBillThroughFrom(Object billThroughFrom) {
			this.billThroughFrom = billThroughFrom;
		}
		public Object getVatPercent() {
			return vatPercent;
		}
		public void setVatPercent(Object vatPercent) {
			this.vatPercent = vatPercent;
		}
		public Object getVatAmount() {
			return vatAmount;
		}
		public void setVatAmount(Object vatAmount) {
			this.vatAmount = vatAmount;
		}
		public Object getVatDescr() {
			return vatDescr;
		}
		public void setVatDescr(Object vatDescr) {
			this.vatDescr = vatDescr;
		}
		public Object getPayglCode() {
			return payglCode;
		}
		public void setPayglCode(Object payglCode) {
			this.payglCode = payglCode;
		}
		public Object getBaseInsuranceValue() {
			return baseInsuranceValue;
		}
		public void setBaseInsuranceValue(Object baseInsuranceValue) {
			this.baseInsuranceValue = baseInsuranceValue;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}
		public Object getBillingCurrency() {
			return billingCurrency;
		}
		public void setBillingCurrency(Object billingCurrency) {
			this.billingCurrency = billingCurrency;
		}
		public Object getVendorStoragePerMonth() {
			return vendorStoragePerMonth;
		}
		public void setVendorStoragePerMonth(Object vendorStoragePerMonth) {
			this.vendorStoragePerMonth = vendorStoragePerMonth;
		}
		public Object getVendorStorageVatDescr() {
			return vendorStorageVatDescr;
		}
		public void setVendorStorageVatDescr(Object vendorStorageVatDescr) {
			this.vendorStorageVatDescr = vendorStorageVatDescr;
		}
		public Object getVendorStorageVatPercent() {
			return vendorStorageVatPercent;
		}
		public void setVendorStorageVatPercent(Object vendorStorageVatPercent) {
			this.vendorStorageVatPercent = vendorStorageVatPercent;
		}
		public Object getVendorBillingCurency() {
			return vendorBillingCurency;
		}
		public void setVendorBillingCurency(Object vendorBillingCurency) {
			this.vendorBillingCurency = vendorBillingCurency;
		}
		public Object getVendorCode1() {
			return vendorCode1;
		}
		public void setVendorCode1(Object vendorCode1) {
			this.vendorCode1 = vendorCode1;
		}
		public Object getVendorName1() {
			return vendorName1;
		}
		public void setVendorName1(Object vendorName1) {
			this.vendorName1 = vendorName1;
		}
		public Object getInsuranceCharge1() {
			return insuranceCharge1;
		}
		public void setInsuranceCharge1(Object insuranceCharge1) {
			this.insuranceCharge1 = insuranceCharge1;
		}
		public Object getVendorVatAmount() {
			return vendorVatAmount;
		}
		public void setVendorVatAmount(Object vendorVatAmount) {
			this.vendorVatAmount = vendorVatAmount;
		}
		public Object getContractCurrency() {
			return contractCurrency;
		}
		public void setContractCurrency(Object contractCurrency) {
			this.contractCurrency = contractCurrency;
		}
		public Object getPayableContractCurrency() {
			return payableContractCurrency;
		}
		public void setPayableContractCurrency(Object payableContractCurrency) {
			this.payableContractCurrency = payableContractCurrency;
		}
		public Object getContractRevenueAmount() {
			return contractRevenueAmount;
		}
		public void setContractRevenueAmount(Object contractRevenueAmount) {
			this.contractRevenueAmount = contractRevenueAmount;
		}
		public Object getContractExpenseAmount() {
			return contractExpenseAmount;
		}
		public void setContractExpenseAmount(Object contractExpenseAmount) {
			this.contractExpenseAmount = contractExpenseAmount;
		}
		public Object getBillingContractCurrency() {
			return billingContractCurrency;
		}
		public void setBillingContractCurrency(Object billingContractCurrency) {
			this.billingContractCurrency = billingContractCurrency;
		}
		public Object getBillingPayableContractCurrency() {
			return billingPayableContractCurrency;
		}
		public void setBillingPayableContractCurrency(
				Object billingPayableContractCurrency) {
			this.billingPayableContractCurrency = billingPayableContractCurrency;
		}
		
		public Object getInsuranceVatAmount() {
			return insuranceVatAmount;
		}
		public void setInsuranceVatAmount(Object insuranceVatAmount) {
			this.insuranceVatAmount = insuranceVatAmount;
		}
		public Object getInsuranceStoragePerMonthForPayable() {
			return insuranceStoragePerMonthForPayable;
		}
		public void setInsuranceStoragePerMonthForPayable(
				Object insuranceStoragePerMonthForPayable) {
			this.insuranceStoragePerMonthForPayable = insuranceStoragePerMonthForPayable;
		}
		public boolean getIsInsurancePayableRequired() {
			return isInsurancePayableRequired;
		}
		public void setIsInsurancePayableRequired(boolean isInsurancePayableRequired) {
			this.isInsurancePayableRequired = isInsurancePayableRequired;
		}
		public Object getCurrency() {
			return currency;
		}
		public void setCurrency(Object currency) {
			this.currency = currency;
		}
		public Object getInsuranceBuyRate() {
			return insuranceBuyRate;
		}
		public void setInsuranceBuyRate(Object insuranceBuyRate) {
			this.insuranceBuyRate = insuranceBuyRate;
		}
		public boolean getVATExclude() {
			return VATExclude;
		}
		public void setVATExclude(boolean vATExclude) {
			VATExclude = vATExclude;
		}
		public String getContractType() {
			return contractType;
		}
		public void setContractType(String contractType) {
			this.contractType = contractType;
		}
		public Object getInsurancePerMonth() {
			return insurancePerMonth;
		}
		public void setInsurancePerMonth(Object insurancePerMonth) {
			this.insurancePerMonth = insurancePerMonth;
		}
		public Object getInsuranceValueEntitle() {
			return insuranceValueEntitle;
		}
		public void setInsuranceValueEntitle(Object insuranceValueEntitle) {
			this.insuranceValueEntitle = insuranceValueEntitle;
		}
		public Object getPayableRate() {
			return payableRate;
		}
		public void setPayableRate(Object payableRate) {
			this.payableRate = payableRate;
		}
		public boolean isDmmInsurancePolicy() {
			return dmmInsurancePolicy;
		}
		public void setDmmInsurancePolicy(boolean dmmInsurancePolicy) {
			this.dmmInsurancePolicy = dmmInsurancePolicy;
		}
		public Object getBookingAgentVatCode() {
			return bookingAgentVatCode;
		}
		public void setBookingAgentVatCode(Object bookingAgentVatCode) {
			this.bookingAgentVatCode = bookingAgentVatCode;
		}
		public Object getFormulaText() {
			return formulaText;
		}
		public void setFormulaText(Object formulaText) {
			this.formulaText = formulaText;
		}
		public Object getServiceOrderId() {
			return serviceOrderId;
		}
		public void setServiceOrderId(Object serviceOrderId) {
			this.serviceOrderId = serviceOrderId;
		}
		public Object getCostElement() {
			return costElement;
		}
		public void setCostElement(Object costElement) {
			this.costElement = costElement;
		}
		public Object getCostElementDescription() {
			return costElementDescription;
		}
		public void setCostElementDescription(Object costElementDescription) {
			this.costElementDescription = costElementDescription;
		}
		public void setInsurancePayableRequired(boolean isInsurancePayableRequired) {
			this.isInsurancePayableRequired = isInsurancePayableRequired;
		}
		public Object getStorageMeasurement() {
			return storageMeasurement;
		}
		public void setStorageMeasurement(Object storageMeasurement) {
			this.storageMeasurement = storageMeasurement;
		}
		public Object getChargesDescription() {
			return chargesDescription;
		}
		public void setChargesDescription(Object chargesDescription) {
			this.chargesDescription = chargesDescription;
		}
		
}
