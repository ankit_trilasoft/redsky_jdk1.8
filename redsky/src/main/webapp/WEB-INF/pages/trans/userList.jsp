<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    
    <style>
 span.pagelinks {display:block;
  font-size:0.85em;
   !font-size:0.90em;
    margin-bottom:5px;
    !margin-bottom:1px;
    margin-top:-33px;
    !margin-top:-24px; 
    padding:2px 0px; 
    text-align:right;
    width:87%;}
.cssbutton {color:#000000;font-family:arial;font-size:12px;
font-style:normal;font-variant:normal;font-weight:bold;height:25px;line-height:normal;padding:2px;width:100px;
}
    </style>
</head>
<%--
<c:set var="button1">
    <input type="button" class="cssbutton"
        onclick="location.href='<c:url value="/editUser.html?method=Add&from=list"/>'"
        value="<fmt:message key="button.addUser"/>"/>
    <input type="button" class="cssbutton"
        onclick="location.href='<c:url value="/editUser.html?method=Add&from=list"/>'"
        value="<fmt:message key="button.addRole"/>"/>
</c:set>

<c:out value="${buttons}" escapeXml="false" />

<c:out value="${button1}" escapeXml="false" />

--%>

<div id="newmnav">
<ul>  
  	<li id="newmnav1" style="background:#FFF"><a class="current""><span>Users<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  	<li><a href="myRoles.html"><span>Roles</span></a></li>
</ul>
		</div><div class="spn">&nbsp;</div>

<s:set name="users" value="users" scope="request"/>

<display:table name="users" requestURI="" defaultsort="1" id="users" class="table" pagesize="50" style="width:100%;" >
   <%--
   <display:column property="username" escapeXml="true" sortable="true" titleKey="user.username" style="width: 25%" url="menuItems.html" paramId="id" paramProperty="id"/>
    
    <display:column property="username" escapeXml="true" sortable="true" titleKey="user.username" style="width: 25%" url="menuItem.html?from=list" paramId="id" paramProperty="id"/>
    --%>
    <display:column property="username" escapeXml="true" sortable="true" titleKey="user.username" style="width: 25%" paramId="id" paramProperty="id"/>
    <display:column title="Assign Menu">
     <input type="button" class="cssbutton" onclick="javascript:openWindow('menuItems.html?id=${users.id}&perRec=${users.username}&decorator=popup&popup=true');"
    value="<fmt:message key="button.assignMenuItem"/>" style="width:100px; height:25px" />
     </display:column>   
    <display:column property="fullName" escapeXml="true" sortable="true" titleKey="activeUsers.fullName" style="width: 34%"/>
    <display:column property="email" sortable="true" titleKey="user.email" style="width: 25%" autolink="true" media="html"/>
    <display:column property="email" titleKey="user.email" media="csv xml excel pdf"/>
    <display:column sortProperty="enabled" sortable="true" titleKey="user.enabled" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${users.enabled}">checked="checked"</c:if>/>
    </display:column>
    <display:column property="enabled" titleKey="user.enabled" media="csv xml excel pdf"/>
</display:table>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<script type="text/javascript">
    //highlightTableRows("users");
</script>
