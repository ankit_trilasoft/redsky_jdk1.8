package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name="quoterate")
public class QuoteRate extends BaseObject{
	private Long id;
	private String corpID;
	private Long quoteId;
	private String service;
	private Double basePrice;
	private Double totalPrice;
	private String mode;
	private String transitDays;
	private Boolean selectedRate;
	private Double containerCost;
	private Double computedPrice1;
	private Double computedPrice2;
	private Double computedRate1;
	private Double computedRate2;
	private Double weight;
	private Double basePriceWithFuelSurcharge;
	
	
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("quoteId", quoteId)
				.append("service", service)
				.append("basePrice", basePrice)
				.append("totalPrice", totalPrice)
				.append("mode", mode)
				.append("transitDays", transitDays)
				.append("selectedRate", selectedRate)
				.append("containerCost", containerCost)
				.append("computedPrice1", computedPrice1)
				.append("computedPrice2", computedPrice2)
				.append("computedRate1", computedRate1)
				.append("computedRate2", computedRate2)
				.append("weight", weight)
				.append("basePriceWithFuelSurcharge",
						basePriceWithFuelSurcharge)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof QuoteRate))
			return false;
		QuoteRate castOther = (QuoteRate) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(quoteId, castOther.quoteId)
				.append(service, castOther.service)
				.append(basePrice, castOther.basePrice)
				.append(totalPrice, castOther.totalPrice)
				.append(mode, castOther.mode)
				.append(transitDays, castOther.transitDays)
				.append(selectedRate, castOther.selectedRate)
				.append(containerCost, castOther.containerCost)
				.append(computedPrice1, castOther.computedPrice1)
				.append(computedPrice2, castOther.computedPrice2)
				.append(computedRate1, castOther.computedRate1)
				.append(computedRate2, castOther.computedRate2)
				.append(weight, castOther.weight)
				.append(basePriceWithFuelSurcharge,
						castOther.basePriceWithFuelSurcharge)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(quoteId)
				.append(service).append(basePrice).append(totalPrice)
				.append(mode).append(transitDays).append(selectedRate)
				.append(containerCost).append(computedPrice1)
				.append(computedPrice2).append(computedRate1)
				.append(computedRate2).append(weight)
				.append(basePriceWithFuelSurcharge).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=10)
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column(length=15)
	public Long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}
	
	public Boolean getSelectedRate() {
		return selectedRate;
	}
	public void setSelectedRate(Boolean selectedRate) {
		this.selectedRate = selectedRate;
	}
	@Column(length=50)
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	@Column(length=50)
	public String getTransitDays() {
		return transitDays;
	}
	public void setTransitDays(String transitDays) {
		this.transitDays = transitDays;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Double getContainerCost() {
		return containerCost;
	}
	public void setContainerCost(Double containerCost) {
		this.containerCost = containerCost;
	}
	public Double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}
	public Double getComputedPrice1() {
		return computedPrice1;
	}
	public void setComputedPrice1(Double computedPrice1) {
		this.computedPrice1 = computedPrice1;
	}
	public Double getComputedPrice2() {
		return computedPrice2;
	}
	public void setComputedPrice2(Double computedPrice2) {
		this.computedPrice2 = computedPrice2;
	}
	
	public Double getComputedRate1() {
		return computedRate1;
	}
	public void setComputedRate1(Double computedRate1) {
		this.computedRate1 = computedRate1;
	}
	public Double getComputedRate2() {
		return computedRate2;
	}
	public void setComputedRate2(Double computedRate2) {
		this.computedRate2 = computedRate2;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getBasePriceWithFuelSurcharge() {
		return basePriceWithFuelSurcharge;
	}
	public void setBasePriceWithFuelSurcharge(Double basePriceWithFuelSurcharge) {
		this.basePriceWithFuelSurcharge = basePriceWithFuelSurcharge;
	}
	
	
}
