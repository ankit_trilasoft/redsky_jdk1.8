package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;

@Entity
@Table(name="extractcolumnmgmt") 
public class ExtractColumnMgmt extends BaseObject implements Comparable<ExtractColumnMgmt> {
	private String corpID;
	private Long id;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String reportName;
	private String tableName;
	private String fieldName;
	private String displayName;
	private Long columnSequenceNumber;
	public int compareTo(final ExtractColumnMgmt other) {
		return new CompareToBuilder().append(corpID, other.corpID).append(id,
				other.id).append(createdBy, other.createdBy).append(updatedBy,
				other.updatedBy).append(createdOn, other.createdOn).append(
				updatedOn, other.updatedOn)
				.append(reportName, other.reportName).append(tableName,
						other.tableName).append(fieldName, other.fieldName)
				.append(displayName, other.displayName).append(
						columnSequenceNumber, other.columnSequenceNumber)
				.toComparison();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("createdBy", createdBy).append("updatedBy",
				updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("reportName", reportName).append("tableName",
				tableName).append("fieldName", fieldName).append("displayName",
				displayName).append("columnSequenceNumber",
				columnSequenceNumber).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ExtractColumnMgmt))
			return false;
		ExtractColumnMgmt castOther = (ExtractColumnMgmt) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(createdBy, castOther.createdBy).append(
				updatedBy, castOther.updatedBy).append(createdOn,
				castOther.createdOn).append(updatedOn, castOther.updatedOn)
				.append(reportName, castOther.reportName).append(tableName,
						castOther.tableName).append(fieldName,
						castOther.fieldName).append(displayName,
						castOther.displayName).append(columnSequenceNumber,
						castOther.columnSequenceNumber).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(reportName).append(tableName).append(
						fieldName).append(displayName).append(
						columnSequenceNumber).toHashCode();
	}
	@Column(length=25)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=100)
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@Column(length=700)
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=255)
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	@Column(length=100)
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=10)
	public Long getColumnSequenceNumber() {
		return columnSequenceNumber;
	}
	public void setColumnSequenceNumber(Long columnSequenceNumber) {
		this.columnSequenceNumber = columnSequenceNumber;
	}
	
}
