package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PortDao;
import com.trilasoft.app.model.Port;
import com.trilasoft.app.service.PortManager;

public class PortManagerImpl extends GenericManagerImpl<Port,Long> implements PortManager {
	PortDao portDao;
	public PortManagerImpl(PortDao portDao) {
		super(portDao);
		this.portDao = portDao;
	}
	public List findByPort(String pName) {
		return portDao.findByPort(pName);
	}
		public List searchByPort(String portCode,String portName,String country,String modeType, String corpId,String active)
		{
			return portDao.searchByPort(portCode, portName, country,modeType,corpId,active);
		}
		public List findMaximumId(){
			return portDao.findMaximumId();
		}
		public List portExisted(String portCode, String corpId)
		{
			return portDao.portExisted(portCode, corpId);
		}
		public List portListByCorpId(String corpId){
			return portDao.portListByCorpId(corpId);
		}
		public List searchByPortCountry(String portCode, String portName, String country, String modeType, String sessionCorpID) {
			
			return portDao.searchByPortCountry(portCode, portName, country, modeType, sessionCorpID);
		}
		public int updatePortActiveStatus(String Status, Long id, String updatedBy)
		{
			return portDao.updatePortActiveStatus(Status, id,updatedBy);
		}
        public List getPortName(String country, String sessionCorpID, String portCode) {
			
			return portDao.getPortName(country, sessionCorpID, portCode);
		}
        public List getPOLAutoComplete(String sessionCorpID,String polCode,String mode){
        	   return portDao.getPOLAutoComplete(sessionCorpID,polCode,mode);
        }
        public List getPOEAutoComplete(String sessionCorpID,String poeCode,String mode){
     	   return portDao.getPOLAutoComplete(sessionCorpID,poeCode,mode);
     }
	}

