<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="CODLetter" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="537" leftMargin="29" rightMargin="29" topMargin="27" bottomMargin="27" isSummaryNewPage="true" isFloatColumnFooter="true" uuid="d6b0e638-fa39-4df1-a368-17f6cfa8c316">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="733"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Service Order Number" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	'CODLetter-F-CWMS.jrxml',
	DATE_FORMAT(now(),'%M %d, %Y') AS currentdate,
	if(serviceorder.shipNumber is null || serviceorder.shipNumber='','',serviceorder.shipNumber) as serviceorder_shipNumber,
	concat(if(serviceorder.firstName is null || trim(serviceorder.firstName)='','',trim(serviceorder.firstName)),if(serviceorder.lastName is null || trim(serviceorder.lastName)='','',if(serviceorder.firstName is null || trim(serviceorder.firstName)='',trim(serviceorder.lastName),concat(' ',trim(serviceorder.lastName))))) AS shipperName,
	concat(if(serviceorder.originAddressLine1 is null || trim(serviceorder.originAddressLine1)='','',trim(serviceorder.originAddressLine1)),if(serviceorder.originAddressLine2 is null || trim(serviceorder.originAddressLine2)='','',if(serviceorder.originAddressLine1 is null || trim(serviceorder.originAddressLine1)='',trim(serviceorder.originAddressLine2),concat(', ',trim(serviceorder.originAddressLine2))))) AS serviceorder_originAddressLines12,
	if(serviceorder.originAddressLine3 is null || trim(serviceorder.originAddressLine3)='','',trim(serviceorder.originAddressLine3)) as serviceorder_originAddressLines3,
	Concat(if(serviceorder.originCity is null || trim(serviceorder.originCity)='',"",trim(serviceorder.originCity)),"",if(serviceorder.originState is null || serviceorder.originState='',"",if(serviceorder.originCity is null || trim(serviceorder.originCity)='',serviceorder.originState,concat(", ",serviceorder.originState))),"",if(serviceorder.originZip is null || trim(serviceorder.originZip)='',"",if((serviceorder.originCity is null || serviceorder.originCity='') AND (serviceorder.originState is null || serviceorder.originState=''),trim(serviceorder.originZip),concat(" ",trim(serviceorder.originZip))))) AS serviceorder_origin_city_state_zip,
	if(serviceorder.origincountrycode is null || serviceorder.origincountrycode='','',serviceorder.origincountrycode)as serviceorder_origincountrycode,
	IF(serviceorder.originCountryCode='USA','',serviceorder.originCountry) AS serviceorder_origincountry,
	Concat("Thank you for this opportunity to present our quotation to handle the door-to-door transportation of your household goods from ",Concat(if(serviceorder.originCity is null || trim(serviceorder.originCity)='',"",trim(serviceorder.originCity)),"",if(serviceorder.originState is null || serviceorder.originState='',"",if(serviceorder.originCity is null || trim(serviceorder.originCity)='',serviceorder.originState,concat(", ",serviceorder.originState))),"",if(serviceorder.originCountry is null || serviceorder.originCountry='',"",if((serviceorder.originCity is null || trim(serviceorder.originCity)='') AND (serviceorder.originState is null || serviceorder.originState=''),serviceorder.originCountry,concat(", ",serviceorder.originCountry))))," to ",Concat(if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='',"",trim(serviceorder.destinationCity)),"",if(serviceorder.destinationState is null || serviceorder.destinationState='',"",if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='',serviceorder.destinationState,concat(", ",serviceorder.destinationState))),"",if(serviceorder.destinationCountry is null || serviceorder.destinationCountry='',"",if((serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='') AND (serviceorder.destinationState is null || serviceorder.destinationState=''),serviceorder.destinationCountry,concat(", ",serviceorder.destinationCountry)))),".") AS Comment,
	Concat("Based on our visual inspection of your household possessions, we have estimated your belongings will weigh ",if(serviceorder.mode ='AIR',if(miscellaneous.estimateGrossWeight is null || miscellaneous.estimateGrossWeight='',0.00,miscellaneous.estimateGrossWeight),if(miscellaneous.estimatedNetWeight is null || miscellaneous.estimatedNetWeight='',0.00,miscellaneous.estimatedNetWeight))," Lbs. and measure ",if(serviceorder.`mode`='AIR',if(miscellaneous.estimateCubicFeet is null || miscellaneous.estimateCubicFeet='',0.00,miscellaneous.estimateCubicFeet),if(miscellaneous.netEstimateCubicFeet is null || miscellaneous.netEstimateCubicFeet='',0.00,miscellaneous.netEstimateCubicFeet))," gross Cft. ",if(systemdefault.company is null,'',systemdefault.company)," is pleased to provide you with the following quotation.") AS comment2,
	serviceorder.mode AS serviceorder_mode,
	concat("I trust you will find our quotation favorable. Should you have any questions or concerns regarding our rate and service proposal, please call me at",if(app_user.`phone_number` is null || trim(app_user.`phone_number`)='',"",concat(" ",trim(app_user.`phone_number`))),".") AS comment3,
	concat("Please fax acceptance to ",if(app_user.faxNumber is null || trim(app_user.faxNumber)='',"",trim(app_user.faxNumber))) AS comment4,
	concat(trim(app_user.`first_name`)," ",trim(app_user.`last_name`),if(app_user.userTitle is null || trim(app_user.userTitle)='',"",concat("/ ",trim(app_user.userTitle)))) AS App_User_first_lastName,
	round(accountline.estimateRevenueAmount,2) AS accountline_estimateRevenueAmount,
	trim(accountline.`quoteDescription`) AS accountline_quoteDescription,
	ref.description AS ref_description,
        serviceorder.transitdays as serviceorder_transitdays,
        systemdefault.company as systemdefault_company
FROM  serviceorder serviceorder
	INNER JOIN miscellaneous miscellaneous ON serviceorder.id = miscellaneous.id AND miscellaneous.corpID=$P{Corporate ID}
	LEFT OUTER JOIN accountline accountline on serviceorder.id=accountline.serviceorderID AND accountline.corpID=$P{Corporate ID} AND (accountline.estimateRevenueAmount!=0.00 || accountline.estimateRevenueAmount!='') and accountline.`status` is true and accountline.`displayOnQuote` is true
	LEFT OUTER JOIN refmaster ref on serviceorder.packingmode=ref.code AND ref.parameter='PKMODE' AND ref.`corpID` in ('TSFT',$P{Corporate ID})
	LEFT OUTER JOIN `app_user` app_user ON serviceorder.`coordinator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}
        LEFT OUTER JOIN systemdefault systemdefault on systemdefault.corpID=$P{Corporate ID}
WHERE serviceorder.corpID=$P{Corporate ID}
AND serviceorder.shipNumber=$P{Service Order Number}
ORDER BY accountline.id]]>
	</queryString>
	<field name="CODLetter-F-CWMS.jrxml" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="shipperName" class="java.lang.String"/>
	<field name="serviceorder_originAddressLines12" class="java.lang.String"/>
	<field name="serviceorder_originAddressLines3" class="java.lang.String"/>
	<field name="serviceorder_origin_city_state_zip" class="java.lang.String"/>
	<field name="serviceorder_origincountrycode" class="java.lang.String"/>
	<field name="serviceorder_origincountry" class="java.lang.String"/>
	<field name="Comment" class="java.lang.String"/>
	<field name="comment2" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="comment3" class="java.lang.String"/>
	<field name="comment4" class="java.lang.String"/>
	<field name="App_User_first_lastName" class="java.lang.String"/>
	<field name="accountline_estimateRevenueAmount" class="java.math.BigDecimal"/>
	<field name="accountline_quoteDescription" class="java.lang.String"/>
	<field name="ref_description" class="java.lang.String"/>
	<field name="serviceorder_transitdays" class="java.lang.String"/>
	<field name="systemdefault_company" class="java.lang.String"/>
	<group name="Page1">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band height="343" splitType="Stretch">
				<textField isBlankWhenNull="true">
					<reportElement key="textField-1" x="41" y="82" width="159" height="14" uuid="94336a1b-41bc-4242-8bd6-093e47515581"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-2" x="41" y="97" width="436" height="14" uuid="044a4c3b-6730-4058-876a-50802048efe0"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{shipperName}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-10" x="41" y="111" width="277" height="68" uuid="516ff0a2-a405-48fa-a465-365a75aca693"/>
					<textElement>
						<font fontName="Arial" size="10"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[(
($F{serviceorder_originAddressLines12}.equals("") || $F{serviceorder_originAddressLines12}==null ? "" : ($F{serviceorder_originAddressLines12}+"\n"))+
($F{serviceorder_originAddressLines3}.equals("") || $F{serviceorder_originAddressLines3}==null ? "" : ($F{serviceorder_originAddressLines3}+"\n"))+
($F{serviceorder_origin_city_state_zip}.equals("") || $F{serviceorder_origin_city_state_zip}==null ? "" :($F{serviceorder_origin_city_state_zip}+"\n"))+
($F{serviceorder_origincountrycode}.equals("") || $F{serviceorder_origincountrycode}==null ? "" : $F{serviceorder_origincountrycode}.equals("USA") ? "" : ($F{serviceorder_origincountry}))
)]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-13" x="41" y="200" width="463" height="35" uuid="7fbfa907-ef80-425e-96ec-324cf628d6c2"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Comment}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-14" x="41" y="277" width="465" height="42" uuid="672e4ffb-3a35-4e05-98d9-90a5181f2f1c"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{comment2}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-8" x="42" y="325" width="217" height="18" uuid="6fcbebde-2a90-4862-ac72-ca4bb7b3cc0a"/>
					<box topPadding="1" leftPadding="2" rightPadding="2">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Basis of shipment]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-9" x="259" y="325" width="48" height="18" uuid="8dae32ee-7072-46ce-9a85-a1e22ac4f26d"/>
					<box topPadding="1" leftPadding="2" rightPadding="2">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Mode]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-10" x="307" y="325" width="140" height="18" uuid="03562a11-9186-4eab-8dbd-d81743f23bdb"/>
					<box topPadding="1" leftPadding="2" rightPadding="2">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Size of shipping container(s)]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-11" x="447" y="325" width="59" height="18" uuid="6d9f9b42-3bc9-4f06-8f2d-728e38b68628"/>
					<box topPadding="1" leftPadding="2" rightPadding="2">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Cost]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-28" x="41" y="179" width="436" height="16" uuid="8bbd1ff6-3207-4722-98a4-b75a70cf786f"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA["Dear "+($F{shipperName}==null || $F{shipperName}.equals("") ? "" :($F{shipperName}+","))]]></textFieldExpression>
				</textField>
				<image scaleImage="FillFrame" hAlign="Center" vAlign="Middle" onErrorType="Blank">
					<reportElement key="image-21" mode="Transparent" x="2" y="0" width="535" height="73" uuid="9d438507-86d1-48cd-bdf6-b9677ee5dd53"/>
					<imageExpression><![CDATA["../../images/cwmsams.jpg"]]></imageExpression>
				</image>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-30" x="41" y="233" width="465" height="54" uuid="93fe430a-9da4-4e3e-8f7a-3f6df2809091"/>
					<textElement>
						<font fontName="Arial" size="10"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($F{systemdefault_company}==null ? "" : $F{systemdefault_company})+" is an ISO 9001 certified mover staffed with highly skilled administrative and operational personnel. Our elite worldwide network of offices guarantees quality performance throughout the entire moving process."]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-34" x="318" y="82" width="219" height="16" uuid="e502d1a6-6aa2-406d-8685-4192a15124bc"/>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA["Order# "+($F{serviceorder_shipNumber}==null || $F{serviceorder_shipNumber}.equals("") ? "" :$F{serviceorder_shipNumber})]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="85" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-2" x="41" y="0" width="496" height="83" uuid="e91795e7-84f9-419e-b9d3-a31c03362f5b"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[We look forward to working with you in making your international relocation a pleasant experience.

Thank you.

Sincerely,]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-19" x="41" y="64" width="465" height="21" uuid="fa8d8318-016b-4751-914e-eb019dc01234"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{App_User_first_lastName}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="Page2" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="205" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-15" x="41" y="6" width="496" height="178" uuid="1fe1a386-6410-4ee5-aa8f-0029f5131a3c"/>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Please note all charges must be prepaid prior to the exportation of your shipment. Payment may be made by cashiers check, wire deposit, money order or cash. Unfortunately we cannot accept personal checks. We do accept payment via credit card for a 3.0% convenience fee.

Upon acceptance of the contract, we will require a 25% deposit of the residence to residence charges, payable at the time of acceptance. This is a fully refundable deposit if the move is cancelled prior to 72 hours of service date. If cancellation is made in less than 72 hours, a $250.00 cancellation fee will be applicable, in addition to any work that may have occurred. Work that may have occurred may be, but is not limited to, building of crates, draying of container, delivery of packing material, etc. For 25% deposit payments that are made via credit card, we will waive the 3.0% convenience fee. If the balance of the charges is payable by credit card, the 3.0% convenience fee will apply. Once shipment is packed and loaded, and final charges are calculated, they must be paid within 24 hours of receipt of our invoice. If payment is not received, the balance of the charges will be charged to the credit card used for deposit, including the 3.0% convenience fee.]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-18" x="41" y="177" width="496" height="28" uuid="bc65c30e-bce0-40ba-8fef-c769492fa28c"/>
					<textElement>
						<font fontName="Arial"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{comment3}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="page3">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="85" splitType="Stretch">
				<textField isBlankWhenNull="true">
					<reportElement key="textField-31" x="41" y="7" width="496" height="78" uuid="d8461214-0ca7-482c-a915-56568b75f6ff"/>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA["We offer full risk insurance coverage for those items packed and export wrapped by our team of professional movers through PacGlobal Insurance. This coverage is available to you at a cost of 3.25 percent of your declared value, zero deductible. Completion of a valued inventory will be required should you wish to insure your belongings. If you elect to decline coverage, "+($F{systemdefault_company}==null ?"" :$F{systemdefault_company})+"'s maximum liability is limited to $.30 per pound per article."]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="page4">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="44" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-7" x="55" y="10" width="451" height="16" uuid="60fdc39b-16cb-4fad-a975-4a1901b0afaa"/>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Please note our rate is valid for shipments moving within 30 days. All rates are in USD.]]></text>
				</staticText>
				<image onErrorType="Icon">
					<reportElement key="image-7" x="43" y="12" width="7" height="7" uuid="c770abf9-3504-4201-90b9-ebe645501dc9"/>
					<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
				</image>
				<staticText>
					<reportElement key="staticText-37" x="55" y="28" width="451" height="16" uuid="c5c2c498-c471-4cee-9fe9-266a271b98c8"/>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Final charges will be based on the actual weight and volume of the shipment.]]></text>
				</staticText>
				<image onErrorType="Blank">
					<reportElement key="image-20" x="43" y="30" width="7" height="7" uuid="3ba0d26c-43d0-4750-83e9-6f6d6b623c54"/>
					<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
				</image>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-23" stretchType="RelativeToTallestObject" x="259" y="0" width="48" height="18" uuid="226c9729-79d0-4c25-a91e-008013573f47"/>
				<box topPadding="1" leftPadding="2" rightPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-24" stretchType="RelativeToTallestObject" x="307" y="0" width="140" height="18" uuid="06bc28aa-8558-4ff3-8422-258a1d3b4958"/>
				<box topPadding="1" leftPadding="2" rightPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ref_description}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-25" stretchType="RelativeToTallestObject" x="42" y="0" width="217" height="18" uuid="533d60e2-34e6-4825-9e96-ae3e39e25262"/>
				<box topPadding="1" leftPadding="2" rightPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_quoteDescription}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-26" stretchType="RelativeToTallestObject" x="447" y="0" width="59" height="18" uuid="2a59b2f3-9aae-4301-83a5-f3246c8b9ed7"/>
				<box topPadding="1" leftPadding="2" rightPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_estimateRevenueAmount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="788" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-20" x="55" y="110" width="429" height="18" uuid="96a90a0f-8da3-4231-abcc-f5031327d7b6"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Door to door service]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" x="55" y="128" width="429" height="18" uuid="dee33382-93a2-480f-85d8-5d8bd49d1ed2"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Full packing and loading service]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" x="55" y="146" width="429" height="18" uuid="80fff65b-4882-4f74-8f2d-0026b2523e97"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[New packing material]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" x="55" y="164" width="429" height="18" uuid="c4971820-60bc-4821-9bfd-77bfcb7d929c"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Export Service]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" x="55" y="182" width="429" height="18" uuid="e838ec64-2e9f-4c1e-a354-152e9b88260e"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Ocean transportation]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-25" x="55" y="200" width="429" height="18" uuid="67d9befe-62d4-46ae-9ccb-fc63d576ce28"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Normal customs clearance at destination]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-26" x="55" y="218" width="429" height="18" uuid="ccd662b2-0902-46bb-aa99-4ded0717e05f"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Unpacking and removal of debris at destination]]></text>
			</staticText>
			<image onErrorType="Icon">
				<reportElement key="image-8" x="43" y="112" width="7" height="7" uuid="4d0a3434-ec26-4645-864e-b4633ae9aeb3"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-9" x="43" y="130" width="7" height="7" uuid="6233e6cc-b15c-4ee1-9e2c-b7a4a51cdefd"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-10" x="43" y="148" width="7" height="7" uuid="27e9b4ac-a8f9-45b1-b055-dc75d4bf1732"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-11" x="43" y="166" width="7" height="7" uuid="8ae4316c-4790-4da6-a237-93d84dfe556b"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-12" x="43" y="184" width="7" height="7" uuid="fed210e7-3bac-40fd-a58a-7f01cdbe7911"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-13" x="43" y="202" width="7" height="7" uuid="6ed7fcae-7ad1-43d5-9afe-761bc91b271c"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-14" x="43" y="220" width="7" height="7" uuid="53c54d2a-fad6-4808-bcdf-2ab559af4517"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<staticText>
				<reportElement key="staticText-27" x="41" y="244" width="465" height="46" uuid="91abf2b0-7b35-4553-8cd9-70c2ad90ff6d"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[As stated in the quotation, full risk insurance coverage is available. Please indicate your option below:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-28" x="41" y="272" width="465" height="68" uuid="4d8b0361-fdbe-4e58-83a2-c115ed86f7cf"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[_____ I wish to take purchase full risk coverage*
_____ I decline purchase of full risk coverage.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-30" x="41" y="376" width="465" height="42" uuid="eb9ce6ae-0e6f-43b7-ad61-565b16aef0d3"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[There are special services that require additional charges. Unless it is specified above, the rate quotation does not include at either origin or destination:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-31" x="55" y="416" width="451" height="18" uuid="e60ce46e-8b57-4e54-be21-840ca03aa3b2"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Inaccessible location difficulties requiring shuttles, hoisting, or non elevator availability]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-32" x="55" y="432" width="451" height="18" uuid="833c6e81-3c33-4d5f-9442-2f5c269a3d95"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Port storage or container detention, port congestion at either origin or destination]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-33" x="55" y="448" width="451" height="32" uuid="bbe1ef4e-aaa1-450d-8418-a7e161dccb50"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Third party services such as servicing grandfather clocks or disconnecting icemakers,
Electricians, plumbers, carpenter service.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-34" x="55" y="478" width="451" height="18" uuid="5514b750-b923-4c38-83f9-0ba05e57b80f"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Special wood crates for delicate items]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-35" x="55" y="494" width="451" height="18" uuid="65a21cee-633c-432a-ad07-8ba7b1980c4a"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Government duties or taxes or exams]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-36" x="41" y="563" width="465" height="71" uuid="2b0ab221-1c39-4277-b78e-b5a4e83e09ac"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Required pack date_________________ Required load date _______________


Buyer’s acceptance__________________________________ Date_____________]]></text>
			</staticText>
			<image onErrorType="Icon">
				<reportElement key="image-15" x="43" y="418" width="7" height="7" uuid="06528561-1634-481e-8867-10126ff18ced"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-16" x="43" y="434" width="7" height="7" uuid="4214ebaf-b9ea-4e87-947c-6839af103912"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-17" x="43" y="450" width="7" height="7" uuid="9a73b1ef-0a18-4619-b2df-5eaa26be8f52"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-18" x="43" y="482" width="7" height="7" uuid="ae34bc23-851c-4fa4-a0b2-00f381efe569"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<image onErrorType="Icon">
				<reportElement key="image-19" x="43" y="496" width="7" height="7" uuid="32d92062-46c3-4257-a0ff-8fa3f9c4967b"/>
				<imageExpression><![CDATA["../../images/bullet7.gif"]]></imageExpression>
			</image>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-20" x="41" y="636" width="448" height="20" uuid="008d9bc3-847c-4ecc-a52a-e7841b531248"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{comment4}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-27" x="171" y="6" width="250" height="27" uuid="9114ca87-ba1a-475f-98ff-6502af0f93a9"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{shipperName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-38" x="41" y="6" width="187" height="19" uuid="7d93a41d-bfa4-4e87-b028-7aa7364c1b52"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Acceptance of contract for ]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-39" x="41" y="356" width="436" height="20" uuid="84ae50a1-3cf4-4342-bc01-e37d9f4dceb5"/>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Please refer to our attached terms and conditions for contract of carriage.]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-29" x="41" y="82" width="465" height="23" uuid="40b2b901-92ae-428c-bea9-0695f3c73b17"/>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Our service includes, with an estimated transit time of "+($F{serviceorder_transitdays}==null ? "" : $F{serviceorder_transitdays})+" days."]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-32" x="41" y="36" width="465" height="46" uuid="f5173006-1a7b-4b8d-89ff-4e437a51f92d"/>
				<textElement>
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["In accordance with "+($F{systemdefault_company}==null ? "" : $F{systemdefault_company})+"'s quotation dated ___________, I accept the estimated cost to proceed with my international move."]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-33" x="41" y="318" width="465" height="42" uuid="b46c2c4f-c878-48f1-bb36-988f23382ed8"/>
				<textElement>
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["*A valued inventory must be completed and returned to "+($F{systemdefault_company}==null ? "" : $F{systemdefault_company})+" prior to services being performed."]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-35" x="405" y="6" width="132" height="16" uuid="09b7220c-aef4-4073-b1a2-bb27f70bce41"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Order# "+($F{serviceorder_shipNumber}==null || $F{serviceorder_shipNumber}.equals("") ? "" :$F{serviceorder_shipNumber})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-35" x="41" y="520" width="463" height="27" uuid="e519479c-c765-4b8b-ad67-0506a771c088"/>
				<textElement>
					<font fontName="Arial" isBold="false" pdfFontName="Helvetica"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Note: during these COVID times, container driver shortages are many, and additional charges may be incurred as a result.]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
