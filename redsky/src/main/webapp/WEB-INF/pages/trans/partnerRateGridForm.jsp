<%@page import="java.util.SortedMap"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
  
<head>   
    <title><fmt:message key="partnerRateGridForm.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerRateGridForm.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>
    <style><%@ include file="/common/calenderStyle.css"%></style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
 

 <script type="text/javascript">
  function rangeExtraDisabled(){
  if((document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value=='')&&(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value=='')){
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra2'].readOnly=true; 
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra2'].className='input-textUpper'; 
  }
  if((document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value=='')&&(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value=='')){
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra3'].readOnly=true; 
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra3'].className='input-textUpper';
  }
  if((document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value=='')&&(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value=='')){
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra4'].readOnly=true; 
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra4'].className='input-textUpper';
  }
  if((document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].value=='')&&(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].value=='')){
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra5'].readOnly=true;
  document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra5'].className='input-textUpper'; 
  }
  }
 
 function convertToMiles(){ 
	var km=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value;
	var check=''; 
    for (i = 0; i < km.length; i++)
    {   
        var c = km.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid'; 
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].focus(); 
        }
    }
    if(check=='Invalid'){
    alert("Invalid data in Geographic Range Kms");
    }else{
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value=rounded;
	}
}

function convertToKm(){
	var miles=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value;
	var check='';
	for (i = 0; i < miles.length; i++)
    {   
        var c = miles.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].focus(); 
        }
    }if(check=='Invalid'){
    alert("Invalid data in Geographic Range ");
    }else{
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value=rounded;
	}
}

function convertToMiles2(){
    var previouskm=0;
    var newkm=0;
    previouskm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value);
    newkm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value); 
	var km=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value;
	var check=''; 
    for (i = 0; i < km.length; i++)
    {   
        var c = km.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid'; 
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].focus(); 
        }
    }
    if(check=='Invalid'){
    alert("Invalid data in Geographic Range Kms");
    }else{
     if(newkm>previouskm){
	   var kms=0.621371192*km;
	   var rounded=Math.round(kms);
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value=rounded;  
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra2'].readOnly=false; 
       document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra2'].className='input-text';  
	} else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].select(); 
	   alert("Geographic Range Kms need to greater than above Geographic Range Kms"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value=''; 
	}
	}
}

function convertToKm2(){
    var previousmiles=0;
    var newmiles=0;
    previousmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value);
    newmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value); 
	var miles=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value;
	var check='';
	for (i = 0; i < miles.length; i++)
    {   
        var c = miles.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].focus(); 
        }
    }if(check=='Invalid'){
    alert("Invalid data in Geographic Range ");
    }else{
    if(newmiles>previousmiles){
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra2'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra2'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].select();
	   alert("Geographic Range need to greater than above Geographic Range"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value=''; 
	}
	}
}

function convertToMiles3(){
    var previouskm=0;
    var previouskm2=0;
    var newkm=0;
    previouskm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value);
    previouskm2=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value);
    newkm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value); 
	var km=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value;
	var check=''; 
    for (i = 0; i < km.length; i++)
    {   
        var c = km.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid'; 
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].focus(); 
        }
    }
    if(check=='Invalid'){
    alert("Invalid data in Geographic Range Kms");
    }else{
    if((newkm>previouskm)&& (newkm>previouskm2)){
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra3'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra3'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].select(); 
	   alert("Geographic Range Kms need to greater than above Geographic Range Kms"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value=''; 
	}
	}
}

function convertToKm3(){
    var previousmiles=0;
    var previousmiles2=0;
    var newmiles=0;
    previousmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value);
    previousmiles2=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value);
    newmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value); 
	var miles=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value;
	var check='';
	for (i = 0; i < miles.length; i++)
    {   
        var c = miles.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].focus(); 
        }
    }if(check=='Invalid'){
    alert("Invalid data in Geographic Range ");
    }else{
    if((newmiles>previousmiles)&& (newmiles>previousmiles2)){
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra3'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra3'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].select(); 
	   alert("Geographic Range need to greater than above Geographic Range"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value=''; 
	}
	}
}

function convertToMiles4(){
    var previouskm=0;
    var previouskm2=0;
    var previouskm3=0;
    var newkm=0;
    previouskm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value);
    previouskm2=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value);
    previouskm3=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value); 
    newkm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value); 
	var km=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value;
	var check=''; 
    for (i = 0; i < km.length; i++)
    {   
        var c = km.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid'; 
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].focus(); 
        }
    }
    if(check=='Invalid'){
    alert("Invalid data in Geographic Range Kms");
    }else{
    if((newkm>previouskm)&& (newkm>previouskm2)&&(newkm>previouskm3)){
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra4'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra4'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].select(); 
	   alert("Geographic Range Kms need to greater than above Geographic Range Kms"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value=''; 
	}
	}
}

function convertToKm4(){
    var previousmiles=0;
    var previousmiles2=0;
    var previousmiles3=0;
    var newmiles=0;
    previousmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value);
    previousmiles2=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value);
    previousmiles3=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value); 
    newmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value); 
	var miles=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value;
	var check='';
	for (i = 0; i < miles.length; i++)
    {   
        var c = miles.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].focus(); 
        }
    }if(check=='Invalid'){
    alert("Invalid data in Geographic Range ");
    }else{
    if((newmiles>previousmiles)&& (newmiles>previousmiles2) &&(newmiles>previousmiles3)){
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra4'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra4'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].select(); 
	   alert("Geographic Range need to greater than above Geographic Range"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value=''; 
	}
	}
}

function convertToMiles5(){
    var previouskm=0;
    var previouskm2=0;
    var previouskm3=0;
    var previouskm4=0;
    var newkm=0;
    previouskm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value);
    previouskm2=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms2'].value);
    previouskm3=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms3'].value); 
    previouskm4=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms4'].value); 
    newkm=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].value); 
	var km=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].value;
	var check=''; 
    for (i = 0; i < km.length; i++)
    {   
        var c = km.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid'; 
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].focus(); 
        }
    }
    if(check=='Invalid'){
    alert("Invalid data in Geographic Range Kms");
    }else{
    if((newkm>previouskm)&& (newkm>previouskm2)&&(newkm>previouskm3)&&(newkm>previouskm4)){
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra5'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra5'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].select(); 
	   alert("Geographic Range Kms need to greater than above Geographic Range Kms"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].value=''; 
	}
	}
}

function convertToKm5(){
    var previousmiles=0;
    var previousmiles2=0;
    var previousmiles3=0;
    var previousmiles4=0;
    var newmiles=0;
    previousmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value);
    previousmiles2=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles2'].value);
    previousmiles3=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles3'].value); 
    previousmiles4=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles4'].value); 
    newmiles=eval(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].value); 
	var miles=document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].value;
	var check='';
	for (i = 0; i < miles.length; i++)
    {   
        var c = miles.charAt(i);
        
        if ((c < '0') || (c > '9')) 
        {
        	check='Invalid';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].value='';
	        document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].focus(); 
        }
    }if(check=='Invalid'){
    alert("Invalid data in Geographic Range ");
    }else{
     if((newmiles>previousmiles)&& (newmiles>previousmiles2) &&(newmiles>previousmiles3)&& (newmiles>previousmiles4)){
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms5'].value=rounded;
	document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra5'].readOnly=false; 
    document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeExtra5'].className='input-text';  
	}else {
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].select();
	   alert("Geographic Range need to greater than above Geographic Range"); 
	   document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles5'].value=''; 
	}
	}
}
function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	
function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	}
function checkFloat(temp)
  { 
    var check='';  
    var i;
    var dotcheck=0;
	var s = temp.value;
	if(s.indexOf(".") == -1){
	dotcheck=eval(s.length)
	}else{
	dotcheck=eval(s.indexOf(".")-1)
	}
	var fieldName = temp.name; 
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	check='Invalid'; 
        }
    }
    if(dotcheck>15){
    check='tolong';
    }
    if(check=='Invalid'){
    document.forms['partnerRateGridForm'].elements[fieldName].select(); 
    alert("Invalid data" ); 
    document.forms['partnerRateGridForm'].elements[fieldName].value='';
    } else if(check=='tolong'){
    document.forms['partnerRateGridForm'].elements[fieldName].select(); 
    alert("Data to long" ); 
    document.forms['partnerRateGridForm'].elements[fieldName].value='';
    } else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['partnerRateGridForm'].elements[fieldName].value=value;
    }
    if((document.forms['partnerRateGridForm'].elements[fieldName].value!='') && (fieldName=='L20_Y_WEIGHT_Flat$Rate_Value')){
    var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
    	for(var i=0; i<numOfElements;i++)
        { 
         var textName = document.forms['partnerRateGridForm'].elements[i].name;
         for(var j=0; j<10;j++)
         { 
         if((textName.substring(0,14) == 'L20_Y_WEIGHT_'+j))
            {
            document.forms['partnerRateGridForm'].elements[i].readOnly=true; 
            document.forms['partnerRateGridForm'].elements[i].className='input-textUpper'; 
            }
        }
        }
    }
    var checkL20 = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL20'].checked; 
    var checkL40 = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL40'].checked;
    if((checkL20==true)&&(document.forms['partnerRateGridForm'].elements[fieldName].value==''||document.forms['partnerRateGridForm'].elements[fieldName].value=='0'||document.forms['partnerRateGridForm'].elements[fieldName].value=='0.00') && (fieldName=='L20_Y_WEIGHT_Flat$Rate_Value')){
    var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
    	for(var i=0; i<numOfElements;i++)
        { 
         var textName = document.forms['partnerRateGridForm'].elements[i].name;
         for(var j=0; j<10;j++)
         { 
         if((textName.substring(0,14) == 'L20_Y_WEIGHT_'+j))
            {
            document.forms['partnerRateGridForm'].elements[i].readOnly=false; 
            document.forms['partnerRateGridForm'].elements[i].className='input-text'; 
            }
            }
        }
    }
    if((document.forms['partnerRateGridForm'].elements[fieldName].value!='') && ((fieldName=='L40_Y_WEIGHT_Flat$Rate_Value')||(fieldName=='L40_Y_WEIGHT_Flat$RateHigh_Value'))){
    var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
    	for(var i=0; i<numOfElements;i++)
        { 
         var textName = document.forms['partnerRateGridForm'].elements[i].name;
         for(var j=0; j<10;j++)
         {
         if((textName.substring(0,14) == 'L40_Y_WEIGHT_'+j))
            {
            document.forms['partnerRateGridForm'].elements[i].readOnly=true; 
            document.forms['partnerRateGridForm'].elements[i].className='input-textUpper'; 
            }
            }
        }
    }
    if((checkL40==true)&&(document.forms['partnerRateGridForm'].elements[fieldName].value==''||document.forms['partnerRateGridForm'].elements[fieldName].value=='0'||document.forms['partnerRateGridForm'].elements[fieldName].value=='0.00') && ((fieldName=='L40_Y_WEIGHT_Flat$Rate_Value')||(fieldName=='L40_Y_WEIGHT_Flat$RateHigh_Value'))){
    var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
    	if((fieldName=='L40_Y_WEIGHT_Flat$Rate_Value' &&(document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].value==''||document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].value=='0'||document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].value=='0.00'))||(fieldName=='L40_Y_WEIGHT_Flat$RateHigh_Value' &&(document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].value==''||document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].value=='0'||document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].value=='0.00'))){
    	
    	for(var i=0; i<numOfElements;i++)
        { 
         var textName = document.forms['partnerRateGridForm'].elements[i].name;
         for(var j=0; j<10;j++)
         {
         if((textName.substring(0,14) == 'L40_Y_WEIGHT_'+j))
            {
            document.forms['partnerRateGridForm'].elements[i].readOnly=false; 
            document.forms['partnerRateGridForm'].elements[i].className='input-text'; 
            }
        }
        }
        }
    }
    document.forms['partnerRateGridForm'].elements[fieldName].readOnly=false;
    document.forms['partnerRateGridForm'].elements[fieldName].className='input-text';  
     if(((document.forms['partnerRateGridForm'].elements[fieldName].value!=''&& document.forms['partnerRateGridForm'].elements[fieldName].value!='0'||document.forms['partnerRateGridForm'].elements[fieldName].value!='0.00'))){ 
    	for(var i=0; i<10;i++)
        { 
         if((fieldName.substring(0,14) == 'L40_Y_WEIGHT_'+i))
            { 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].readOnly=true; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].className='input-textUpper'; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].readOnly=true; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].className='input-textUpper';
            }
          if((fieldName.substring(0,14) == 'L20_Y_WEIGHT_'+i))
            {
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].readOnly=true; 
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].className='input-textUpper';  
            }  
        }
    } 
     if(((document.forms['partnerRateGridForm'].elements[fieldName].value==''&& document.forms['partnerRateGridForm'].elements[fieldName].value=='0'||document.forms['partnerRateGridForm'].elements[fieldName].value=='0.00'))){ 
    	for(var i=0; i<10;i++)
        { 
         if((fieldName.substring(0,14) == 'L40_Y_WEIGHT_'+i))
            {
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].readOnly=false; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].className='input-text'; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].readOnly=false; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].className='input-text';
            }
          if((fieldName.substring(0,14) == 'L20_Y_WEIGHT_'+i))
            {
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].readOnly=false; 
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].className='input-text';  
            }  
        }
    }
}
		
function validation(){ 
 if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.metroCity'].value==''){
   alert("Service Metro City Name is a required field ")
 } else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffApplicability'].value==''){
   alert("Tariff Applicability is a required field ")
 }else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value==''){
   alert("Geographic Range is a required field ")
}else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value==''){
  alert("Geographic Range Kms is a required field ")
} else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.effectiveDate'].value==''){
  alert("Effective From Date is a required field ")
}else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.currency'].value==''){
  alert("Currency is a required field ")
} else{
document.forms['partnerRateGridForm'].action ='savePartnerRateGrid.html';
 document.forms['partnerRateGridForm'].submit();  
}
} 
function basisValidation(parm){
var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
        var chargeAlertFlag=''; 
        var haulingAlertFlag=''; 
        for(var i=0; i<numOfElements;i++)
        {
         var textName = document.forms['partnerRateGridForm'].elements[i].name; 
         if(textName.indexOf("CHARGES")  >= 0 || textName.indexOf("HAULING")  >= 0 ){
         if(textName.indexOf("Basis")  >= 0){
         var to= textName.indexOf("Basis") 
         textName=  textName.substring(0,to);  
         var basisTextName=""+textName+"Basis"; 
         var valueTextNameAIR="AIR_Y_"+textName+"Value";  
         var valueTextNameL20="L20_Y_"+textName+"Value"; 
         var valueTextNameL40="L40_Y_"+textName+"Value"; 
         var valueTextNameLCL="LCL_Y_"+textName+"Value"; 
         var valueTextNameFCL="FCL_Y_"+textName+"Value";   
         var labelTextName=""+textName+"Label" ; 
         if((document.forms['partnerRateGridForm'].elements[basisTextName].value=='')){
         <c:if test="${fn1:indexOf(standardCountryCharges,partnerPublic.terminalCountryCode)<0}">   
         if(textName.indexOf("CHARGES")  >= 0 && chargeAlertFlag==''){
         if(((document.forms['partnerRateGridForm'].elements[valueTextNameAIR].value.trim()!='' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tarriffFlagAir'].checked && document.forms['partnerRateGridForm'].elements[valueTextNameAIR].value.trim()!='0.00' ) || (document.forms['partnerRateGridForm'].elements[valueTextNameL20].value.trim()=='' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL20'].checked) ||(document.forms['partnerRateGridForm'].elements[valueTextNameL40].value.trim()!=''&& document.forms['partnerRateGridForm'].elements[valueTextNameL40].value.trim()!='0.00' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL40'].checked) ||(document.forms['partnerRateGridForm'].elements[valueTextNameLCL].value.trim()!='' && document.forms['partnerRateGridForm'].elements[valueTextNameLCL].value.trim()!='0.00' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagLCL'].checked)||(document.forms['partnerRateGridForm'].elements[valueTextNameFCL].value.trim()!='' && document.forms['partnerRateGridForm'].elements[valueTextNameFCL].value.trim()!='0.00' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagFCL'].checked))&& (document.forms['partnerRateGridForm'].elements[labelTextName].value.indexOf("Enter")  == -1)){
         alert("Please select Basis for Additional Charges. ")
         return false;
         chargeAlertFlag='OK'
         }
         }
         </c:if>
         <c:if test="${fn1:indexOf(standardCountryHauling,partnerPublic.terminalCountryCode)<0}"> 
         if(textName.indexOf("HAULING")  >= 0 && haulingAlertFlag==''){
         if(((document.forms['partnerRateGridForm'].elements[valueTextNameL20].value.trim()!='' && document.forms['partnerRateGridForm'].elements[valueTextNameL20].value.trim()!='0.00' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL20'].checked) ||(document.forms['partnerRateGridForm'].elements[valueTextNameL40].value.trim()!='' && document.forms['partnerRateGridForm'].elements[valueTextNameL40].value.trim()!='0.00' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL40'].checked) ||(document.forms['partnerRateGridForm'].elements[valueTextNameFCL].value.trim()!='' && document.forms['partnerRateGridForm'].elements[valueTextNameFCL].value.trim()!='0.00' && document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagFCL'].checked))&& (document.forms['partnerRateGridForm'].elements[labelTextName].value.indexOf("Select")  == -1)){
         alert("Please select Basis for Hauling Charges . ")
         return false;
         haulingAlertFlag='OK'; 
         }
         }
         </c:if>
         } 
         }
         }
         }
         if(parm=='Save')
         {
         validation();
         }
         if(parm=='Submit')
         {
         	submitRateGrid();
         }
         }

 function submitRateGrid(){
 var  haulingFieldCount=0;
 <c:if test="${fn1:indexOf(standardCountryHauling,partnerPublic.terminalCountryCode)<0}"> 
 for(var i=1; i<6;i++)
     { 
        var haulingField="HAULING_POE"+i+"_Label";  
        try{ 
           if(document.forms['partnerRateGridForm'].elements[haulingField].value.indexOf("Select")  >= 0){ 
           }else{
              haulingFieldCount=1;
           }
          } 
       catch (e) {  
        } 
     } 
</c:if> 
<c:if test="${fn1:indexOf(standardCountryHauling,partnerPublic.terminalCountryCode)>=0}">
 haulingFieldCount=1; 
</c:if>  
  if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.metroCity'].value==''){
   alert("Service Metro City Name is a required field ")
 } else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffApplicability'].value==''){
   alert("Tariff Applicability is a required field ")
 }else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeMiles'].value==''){
   alert("Geographic Range is a required field ")
}else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.serviceRangeKms'].value==''){
   alert("Geographic Range Kms is a required field ")
} else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.effectiveDate'].value==''){
   alert("Effective From Date is a required field ")
}else if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.currency'].value==''){
  alert("Currency is a required field ")
} else if(haulingFieldCount==0){
  alert("At least one Port of Entry is required in order for the tariffs to be useful.")
}else{ 
      var tariffApplicability = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffApplicability'].value;
      if(tariffApplicability=='Origin'){
      if(document.forms['partnerRateGridForm'].elements['originSubmitCount'].value!='0'){
      calcDate(); 
      } else if(document.forms['partnerRateGridForm'].elements['originSubmitCount'].value=='0'){
          openWindow('getRateGridRules.html?&decorator=popup&popup=true',730,550);
      }
      }
      if(tariffApplicability=='Destination'){
       if(document.forms['partnerRateGridForm'].elements['destinationSubmitCount'].value!='0'){
         calcDate(); 
      } else if(document.forms['partnerRateGridForm'].elements['destinationSubmitCount'].value=='0'){
          openWindow('getRateGridRules.html?&decorator=popup&popup=true',730,550);
      }
      } 
    
 }
 }
  
 function submitRateGridRules(){
 document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value='Submitted';
 document.forms['partnerRateGridForm'].action ='submitRateGrid.html';
 document.forms['partnerRateGridForm'].submit(); 
 }
  
  
 function WithdrawRateGrid() { 
     var agree =confirm("When you withdraw your tariff, the tariff will not be available for any more price quotations but will be used for any quotations where it was earlier referred.   Please confirm if you will like to proceed? ");
     if(agree) {
     var id=document.forms['partnerRateGridForm'].elements['partnerRateGrid.id'].value;
     window.location.href="updateRateGridStatus.html?status=Withdraw&partnerId="+${partnerPublic.id}+"&id="+id;
     } else {
     return false;
     }  
 }
 function requestWithdrawal(){
      var id=document.forms['partnerRateGridForm'].elements['partnerRateGrid.id'].value;
      openWindow('requestWithdrawal.html?partnerId='+${partnerPublic.id}+'&id='+id+'&decorator=popup&popup=true',730,550);  
 }
  function changeToNew() { 
     document.forms['partnerRateGridForm1'].elements['id'].value= document.forms['partnerRateGridForm'].elements['partnerRateGrid.id'].value;
     document.forms['partnerRateGridForm1'].elements['partnerId'].value='${partnerPublic.id}';
     document.forms['partnerRateGridForm1'].elements['status'].value="New";
     document.partnerRateGridForm1.submit();
     return false;
 }
 
 function allReadOnly(){
     if((document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value=="Submitted" )||(document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value=="Withdraw")){ 
        var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
        for(var i=0; i<numOfElements;i++)
        {
         var textName = document.forms['partnerRateGridForm'].elements[i].name;  
         document.forms['partnerRateGridForm'].elements[i].disabled=true; 
         }
     }
     if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value=="Submitted"){
         document.forms['partnerRateGridForm'].elements['Close'].disabled=false;
         document.forms['partnerRateGridForm'].elements['Print'].disabled=false;   
         if(document.forms['partnerRateGridForm'].elements['userType'].value!="AGENT") { 
         document.forms['partnerRateGridForm'].elements['partnerRateGrid.publishTariff'].disabled=false; 
         document.forms['partnerRateGridForm'].elements['changetoNew'].disabled=false;
         document.forms['partnerRateGridForm'].elements['Withdraw'].disabled=false;  
         }else{
          document.forms['partnerRateGridForm'].elements['RequestWithdrawal'].disabled=false;
         }
     }
     if(document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value=="Withdraw"){ 
         document.forms['partnerRateGridForm'].elements['Close'].disabled=false;
         document.forms['partnerRateGridForm'].elements['Print'].disabled=false; 
     }
    }
    
    var j;
    
    function tabindexnumber(){
    j=34;
    }
    
    function readOnlyOnclick(toWhome){
    //alert((document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].value!='')&& (document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].value!='0')&&(document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].value!='0.00'))
    if( document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value!="Submitted" && document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value!="Withdraw"){ 
    	var anycheckL20='';
    	var anycheckL40='';
    	var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
    	for(var i=0; i<numOfElements;i++)
        { 
         var textName = document.forms['partnerRateGridForm'].elements[i].name;
         var chkBoxVal ="";
         var flateRateCheck ="";
         if(toWhome == 'AIR_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tarriffFlagAir'].checked;
         	flateRateCheck=false;
         }
         if(toWhome == 'L20_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL20'].checked; 
         	if((document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].value!='')&& (document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].value!='0')&&(document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].value!='0.00'))
         	{
         	flateRateCheck=true;
         	}else{
         	flateRateCheck=false;
         	}
         	
         }
         if(toWhome == 'L40_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL40'].checked;
         	if(((document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].value!='')&& (document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].value!='0')&&(document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].value!='0.00'))||((document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].value!='')&& (document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].value!='0')&&(document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].value!='0.00')))
         	{
         	flateRateCheck=true;
         	}else{
         	flateRateCheck=false;
         	}
         	
         }
         if(toWhome == 'LCL_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagLCL'].checked;
         	flateRateCheck=false;
         }
         if(toWhome == 'FCL_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagFCL'].checked;
         	flateRateCheck=false;
         }
         if(toWhome == 'GRP_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFalgGRP'].checked;
         	flateRateCheck=false;
         }
         var toWhome1=textName.substring(0,4);
         toWhome1=toWhome1+"N_" 
            if((textName.substring(0,6) == toWhome) && (chkBoxVal == true) &&(flateRateCheck == false) )
            {
                
                document.forms['partnerRateGridForm'].elements[i].readOnly=false; 
                document.forms['partnerRateGridForm'].elements[i].className='input-text'; 
                if(document.forms['partnerRateGridForm'].elements[i].tabIndex==0)
                { 
                j = j+1;
                document.forms['partnerRateGridForm'].elements[i].tabIndex = j;
                }
            }
            if((textName.substring(0,6) == toWhome) && (chkBoxVal == true) &&(flateRateCheck == true) )
            { 
            for(var k=0; k<10;k++)
             { 
               if((textName.substring(0,14) == 'L40_Y_WEIGHT_'+k)|| (textName.substring(0,14) == 'L20_Y_WEIGHT_'+k))
               { 
                document.forms['partnerRateGridForm'].elements[i].readOnly=true; 
            	document.forms['partnerRateGridForm'].elements[i].className='input-textUpper';
               } else if((textName.substring(0,14) == 'L20_Y_CHARGES_'||textName.substring(0,14) == 'L40_Y_CHARGES_') && (chkBoxVal == true)){ 
                 document.forms['partnerRateGridForm'].elements[i].readOnly=false; 
                 document.forms['partnerRateGridForm'].elements[i].className='input-text'; 
                 if(document.forms['partnerRateGridForm'].elements[i].tabIndex==0)
                 { 
                 j = j+1;
                 document.forms['partnerRateGridForm'].elements[i].tabIndex = j;
                 }
               }
               
             }
            }
            if((textName.substring(0,6) == toWhome) && (chkBoxVal == false))
            {    
            	document.forms['partnerRateGridForm'].elements[i].readOnly=true; 
            	document.forms['partnerRateGridForm'].elements[i].className='input-textUpper'; 
            }
            if((textName.substring(0,6) == toWhome1))
            {    
            	document.forms['partnerRateGridForm'].elements[i].readOnly=true; 
            	document.forms['partnerRateGridForm'].elements[i].className='input-textUpper'; 
            } 
            for(var k=0; k<10;k++)
            {
            if((textName.substring(0,14) == 'L20_Y_WEIGHT_'+k))
            { 
            if(document.forms['partnerRateGridForm'].elements[i].value!=''&& document.forms['partnerRateGridForm'].elements[i].value!='0'&& document.forms['partnerRateGridForm'].elements[i].value!='0.00'){
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].readOnly=true; 
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].className='input-textUpper'; 
            anycheckL20 = "ok"; 
            }else if(anycheckL20!="ok" ){
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].readOnly=false;
            document.forms['partnerRateGridForm'].elements['L20_Y_WEIGHT_Flat$Rate_Value'].className='input-text'; 
            }
            }
            } 
            for(var k=0; k<10;k++)
            {
            if((textName.substring(0,14) == 'L40_Y_WEIGHT_'+k))
            { 
            if(document.forms['partnerRateGridForm'].elements[i].value!=''&& document.forms['partnerRateGridForm'].elements[i].value!='0'&& document.forms['partnerRateGridForm'].elements[i].value!='0.00'){
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].readOnly=true; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].className='input-textUpper';  
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].readOnly=true; 
            document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].className='input-textUpper';  
            anycheckL40='ok' 
            }else if(anycheckL40!='ok' ){
             document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].readOnly=false;
             document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].readOnly=false;
             document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$RateHigh_Value'].className='input-text';
             document.forms['partnerRateGridForm'].elements['L40_Y_WEIGHT_Flat$Rate_Value'].className='input-text';
            }
            }
            }
         } 
        } 
        checkChargeHauling(toWhome);
    } 
function stateDisabled(){ 
  var terminalCountry =document.forms['partnerRateGridForm'].elements['partnerRateGrid.terminalCountry'].value; 
  if(terminalCountry == "USA" || terminalCountry == "IND" || terminalCountry == "CAN"){
     if( document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value!="Submitted" && document.forms['partnerRateGridForm'].elements['partnerRateGrid.status'].value!="Withdraw"){
       document.forms['partnerRateGridForm'].elements['partnerRateGrid.state'].disabled =false;
    } 
    } else{
      document.forms['partnerRateGridForm'].elements['partnerRateGrid.state'].disabled =true;
    }
}     

function fillStandardInclusions(){
var tariffApplicability=document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffApplicability'].value; 
if(tariffApplicability=='Destination'){
document.getElementById("GIS1").style.display="block";
  document.getElementById("GIS2").style.display="none";
document.forms['partnerRateGridForm'].elements['partnerRateGrid.standardInclusions'].value=document.forms['partnerRateGridForm'].elements['standardDestinationNote'].value;
}
 else if(tariffApplicability=='Origin'){
document.getElementById("GIS1").style.display="none";
document.getElementById("GIS2").style.display="block";
document.forms['partnerRateGridForm'].elements['partnerRateGrid.standardInclusions'].value=document.forms['partnerRateGridForm'].elements['standardOriginNote'].value;
} 
else {
document.getElementById("GIS1").style.display="none";
document.getElementById("GIS2").style.display="none";
}
} 

function putPortName(temp){
var  terminalCountry= document.forms['partnerRateGridForm'].elements['partnerRateGrid.terminalCountry'].value;
<c:if test="${fn1:indexOf(standardCountryHauling,partnerPublic.terminalCountryCode)<0}"> 
//if(document.forms['partnerRateGridForm'].elements['standardCountryHauling'].value!=document.forms['partnerRateGridForm'].elements['partnerRateGrid.terminalCountry'].value){ 
var fieldName= temp.name
fieldName=fieldName.substring(1,fieldName.length); 
openWindow('searchHaulingPortList.html?portCode=&portName=&country=${partnerPublic.terminalCountryCode}&modeType=SEA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code='+fieldName);
document.forms['partnerRateGridForm'].elements[fieldName].select(); 
//}
</c:if>
}   
</script>
<script type="text/javascript">
function clickclear(thisfield, defaulttext) { 
if (thisfield.value == defaulttext) {
thisfield.value = "";
}
}
function clickrecall(thisfield, defaulttext) {  
if (thisfield.value == "") {
thisfield.value = defaulttext;
}
}

function checkChargeHauling(temp1){
         var chkBoxVal="";
         if(temp1 == 'AIR_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tarriffFlagAir'].checked;
         	
         }
         if(temp1 == 'L20_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL20'].checked;
         	
         }
         if(temp1 == 'L40_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagL40'].checked;
         	
         }
         if(temp1 == 'LCL_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagLCL'].checked;
         	
         }
         if(temp1 == 'FCL_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFlagFCL'].checked;
         	
         }
         if(temp1 == 'GRP_Y_')
         {
         	chkBoxVal = document.forms['partnerRateGridForm'].elements['partnerRateGrid.tariffFalgGRP'].checked;
         	
         }
     var temp2=temp1.substring(0,4) 
     temp2=temp2+"N_"
   for(var i=1; i<5;i++)
   { 
    var fieldChargeName=temp1+"CHARGES_charge"+i+"_Value";
    var chargeField="CHARGES_charge"+i+"_Label"; 
    var fieldHaulingName=temp1+"HAULING_POE"+i+"_Value";
    var haulingField="HAULING_POE"+i+"_Label"; 
     if((chkBoxVal == true)){
     try{
     if(document.forms['partnerRateGridForm'].elements[chargeField].value.indexOf("Enter")  == -1){ 
        document.forms['partnerRateGridForm'].elements[fieldChargeName].readOnly=false;  
         document.forms['partnerRateGridForm'].elements[fieldChargeName].className='input-text';   
     }else{
         document.forms['partnerRateGridForm'].elements[fieldChargeName].readOnly=true;  
         document.forms['partnerRateGridForm'].elements[fieldChargeName].className='input-textUpper'; 
         
     } 
     }catch (e) { 
     try{
     fieldChargeName=temp2+"CHARGES_charge"+i+"_Value";
     document.forms['partnerRateGridForm'].elements[fieldChargeName].readOnly=true;  
     document.forms['partnerRateGridForm'].elements[fieldChargeName].className='input-textUpper'; 
     }catch (e){
     }
     }
     }
     }   
     for(var i=1; i<6;i++)
     {
     var fieldHaulingName=temp1+"HAULING_POE"+i+"_Value";
     var haulingField="HAULING_POE"+i+"_Label"; 
     if((chkBoxVal == true && temp1!='AIR_Y_' && temp1!='LCL_Y_')){
      try{
     if(document.forms['partnerRateGridForm'].elements[haulingField].value.indexOf("Select")  == -1){ 
         document.forms['partnerRateGridForm'].elements[fieldHaulingName].readOnly=false;  
         document.forms['partnerRateGridForm'].elements[fieldHaulingName].className='input-text';   
     }else{
         document.forms['partnerRateGridForm'].elements[fieldHaulingName].readOnly=true;  
         document.forms['partnerRateGridForm'].elements[fieldHaulingName].className='input-textUpper'; 
         
     }
     } 
     catch (e) { 
     try{
     fieldHaulingName=temp2+"HAULING_POE"+i+"_Value";
     document.forms['partnerRateGridForm'].elements[fieldHaulingName].readOnly=true;  
     document.forms['partnerRateGridForm'].elements[fieldHaulingName].className='input-textUpper';
     }catch (e){
     }
     }
}
}
}
function checkStandardCountry(){
var numOfElements = document.forms['partnerRateGridForm'].elements.length; 
var  terminalCountry= document.forms['partnerRateGridForm'].elements['partnerRateGrid.terminalCountry'].value;
<c:if test="${fn1:indexOf(standardCountryHauling,partnerPublic.terminalCountryCode)>=0}"> 
         for(var i=0; i<numOfElements;i++){
          var textName = document.forms['partnerRateGridForm'].elements[i].name; 
          if(textName.indexOf("HAULING")!=-1) {
          document.forms['partnerRateGridForm'].elements[i].disabled=true;   
          }
         }
  </c:if>
  var  terminalCountry= document.forms['partnerRateGridForm'].elements['partnerRateGrid.terminalCountry'].value;
<c:if test="${fn1:indexOf(standardCountryCharges,partnerPublic.terminalCountryCode)>=0}">     
       for(var i=0; i<numOfElements;i++){ 
         var textName = document.forms['partnerRateGridForm'].elements[i].name; 
         if(textName.indexOf("CHARGES")!=-1) { 
         document.forms['partnerRateGridForm'].elements[i].disabled=true;  
         }
        }
   </c:if> 
}
</script> 
<script type="text/javascript">  
function calcDate()
  { 
   var date1 = document.forms['partnerRateGridForm'].elements['partnerRateGrid.effectiveDate'].value; 
   var date2=new Date(); 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1]; 
   var year = mySplitResult[2];
   if(month == 'Jan')
   {
       month = "01"; 
   }
   else if(month == 'Feb')
   {
       month = "02"; 
   }
   else if(month == 'Mar')
   {
       month = "03" 
   }
   else if(month == 'Apr')
   {
       month = "04" 
   }
   else if(month == 'May')
   {
       month = "05" 
   }
   else if(month == 'Jun')
   {
       month = "06" 
   }
   else if(month == 'Jul')
   {
       month = "07" 
   }
   else if(month == 'Aug')
   {
       month = "08" 
   }
   else if(month == 'Sep')
   {
       month = "09" 
   }
   else if(month == 'Oct')
   {
       month = "10" 
   }
   else if(month == 'Nov')
   {
       month = "11" 
   }
   else if(month == 'Dec')
   {
       month = "12"; 
   }
   var year1 = '20'+year;  
   var finalDate = month+"-"+day+"-"+year1; 
   date1 = finalDate.split("-"); 
   var eDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]); 
   var  sDate= new Date(); 
   sDate.setDate(sDate.getDate() + 60); 
   var  withsixtyDate= new Date(); 
   withsixtyDate.setDate(withsixtyDate.getDate() + 61);
   var daysApart = Math.round((eDate-sDate)/86400000); 
   var y =withsixtyDate.getFullYear();
   var d =withsixtyDate.getDate();
   var m =""
   m=eval(withsixtyDate.getMonth())+1;
   m =""+m; 
   var m2 ="";
   if(m == "1")
   {
       m2 = "Jan"; 
   }
   else if(m == "2")
   {
       m2 = "Feb"; 
   }
   else if(m == "3")
   {
       m2 = "Mar" 
   }
   else if(m == "4")
   {
       m2 = "Apr" 
   }
   else if(m == "5")
   {
      m2= "May" 
   }
   else if(m == "6")
   {
       m2 = "Jun" 
   }
   else if(m == "7")
   {
      m2= "Jul" 
   }
   else if(m == "8")
   {
       m2 = "Aug" 
   }
   else if(m == "9")
   {
       m2 = "Sep" 
   }
   else if(m == "10")
   {
       m2= "Oct" 
   }
   else if(m == "11")
   {
       m2 = "Nov" 
   }
   else if(m == "12")
   {
       m2= "Dec"; 
   }
   var finalDate1 = d+"-"+m2 +"-"+y;  
   if(daysApart<0)
   {
    alert("The Earliest effective date you can choose is "+finalDate1+", Please correct the effective date before you can submit the tariff. ");  
   } else {
     openWindow('getRateGridRules.html?&decorator=popup&popup=true',730,550);
   }
}
function findPortName(position) { 
   var fieldName= position.name
   fieldName=fieldName.substring(1,fieldName.length); 
   var portCode=document.forms['partnerRateGridForm'].elements[fieldName].value;
   //if(portCode.indexOf("Select")  == -1){ 
   var url="getPortName.html?ajax=1&decorator=simple&popup=true&country=${partnerPublic.terminalCountryCode}&portCode="+portCode;
   ajax_showTooltip(url,position);	
  //} else{
  //alert("Please select port")
  //}
  }
  function openTerms() { 
  openWindow('getTermsCondition.html?&decorator=popup&popup=true',730,550);
  }
  function displayTerms()
  {
  document.getElementById("overlayTerms").style.display="block";
  document.getElementById("TandC").style.display="block";
   document.getElementById("TandC").style.opacity="0";
   document.getElementById("overlayTerms").style.opacity= "0";
  new Effect.Opacity('overlayTerms',{from:0.0,to:0.8,duration:0.8});
   new Effect.Opacity('TandC',{from:0.0,to:1,duration:0.8});
  }
  
  function hideTerms()
  {
	document.getElementById("overlayTerms").style.display="none";
  document.getElementById("TandC").style.display ="none";
  
   }
  
  var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function checkStatusId(targetElement) {
		var Status = targetElement.checked;
		var id =document.forms['partnerRateGridForm'].elements['id'].value;
		var url="updatePublishStatus.html?ajax=1&decorator=simple&popup=true&publishTariff=" + encodeURI(Status)+"&id=" + encodeURI(id);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}
function notExists(){
	alert("The Rate Matrix Detail has not been saved yet, please save Rate Matrix Detail to continue");
}

function printRateGrids(){
	 openWindow('printPartnerRateGrid.html?&decorator=popup&popup=true&partnerId=${partnerPublic.id}&id=${partnerRateGrid.id}');
}

</script> 
<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:550px;
}

 #overlayTerms {
display:none;
filter:alpha(opacity=0);
-moz-opacity:0;
-khtml-opacity:0;
opacity: 0;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:991; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

#TandC {
display:none;
opacity: 0;
}
object.conditions {margin-left:300px;}
</style>

<script language="javascript">
function keyHandler(e)
{
 var pressedKey;
 if(document.all) {e = window.event}
 if(document.layers || e.which){pressedKey = 27; }
 hideTerms();
}
document.onkeypress = keyHandler;
</script>
</head> 

<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partnerId}" />
<c:set var="fileID" value="%{partnerId}"/>
<s:hidden name="ppType" id ="ppType" value="AG" />
<c:set var="ppType" value="AG"/>

<s:form id="partnerRateGridForm"  name="partnerRateGridForm" action="" method="post" validate="true">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<s:hidden name="partnerId" value="${partnerPublic.id}" /> 
<s:hidden name="id" value="${partnerRateGrid.id}"/> 
<s:hidden name="showRateGrig" value="<%=request.getParameter("showRateGrig")%>"/>   
<c:set var="showRateGrig" value="<%=request.getParameter("showRateGrig")%>"/>
<s:hidden name="byAdmin" value="<%=request.getParameter("partnerId")%>"/>   
<c:set var="byAdmin" value="<%=request.getParameter("partnerId")%>"/>
<s:hidden name="partnerRateGrid.id" /> 
<s:hidden name="standardOriginNote" value="${standardOriginNote}"/>
<s:hidden name="standardDestinationNote" value="${standardDestinationNote}"/>
<s:hidden name="originSubmitCount" value="${originSubmitCount}"/>
<s:hidden name="destinationSubmitCount" value="${destinationSubmitCount}"/>
<s:hidden name="standardCountryHauling" value="${standardCountryHauling}"/>
<s:hidden name="standardCountryCharges" value="${standardCountryCharges}"/>
<s:hidden name="secondDescription" />
<s:hidden name="userType" value="${userType}"/> 
<c:set var="userType" value="${userType}"/>
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" /> 
<s:hidden name="description" />
<div id="layer1" style="width:80%">
<div id="newmnav">
	<ul> 
		<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix Detail</span></a></li>
		--><c:if test="${userType!='AGENT'}">
		<c:if test="${empty partnerRateGrid.id}">
		<li><a  onclick="notExists()"><span>Contract Rates</span></a></li>
		</c:if>
		<c:if test="${not empty partnerRateGrid.id}">
		<li><a href="partnerRateGridContractsList.html?partnerId=${partnerPublic.id}&partnerRateGridID=${partnerRateGrid.id}"><span>Contract Rates</span></a></li>
		</c:if>
		</c:if>
		<c:if test="${byAdmin!=''}"> 
			<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix List</span></a></li>
		</c:if>
		<c:if test="${byAdmin==''}">
			<li><a href="partnerRateMatrix.html"><span>Rate Matrix List</span></a></li>
		</c:if> 
	</ul> 
	</div>
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;"></div>      
	<div id="content" align="center">
		<div id="liquid-round-top">
   		<div class="top" style=" "><span></span></div>
   			<div class="center-content"> 
				<table cellspacing="1" cellpadding="1" border="0" width="100%" class="detalTabLabel" style="margin: 3px;">
					<tbody>
					      <tr>
					        <td align="right" width="" class="listwhitetext"><fmt:message key="partnerRateGrid.partnerCode" /></td>
							<td align="left" width="100px" ><s:textfield cssClass="input-text"  name="partnerRateGrid.partnerCode" cssStyle="width:100px" maxlength="8" readonly="true" value="${partnerPublic.partnerCode}" tabindex="1"/></td>
					        <td align="left" width="100px" colspan="2" ><s:textfield cssClass="input-text"  name="partnerRateGrid.partnerName" cssStyle="width:225px" maxlength="8" readonly="true" value="${partnerPublic.lastName}" tabindex="2"/></td>
					       <c:if test="${partnerRateGrid.status=='Submitted'}"> 
					          <td align="right" class="listwhitetext" >Mark as new </td>
					          <td align="left" ><s:checkbox name="changetoNew"  fieldValue="true" tabindex="5" disabled="disabled" onclick="changeToNew();" /></td>
						   </c:if>
					      </tr>
					      <tr>  
					         <td align="right"  class="listwhitetext"><fmt:message key="partnerRateGrid.status"/></td>
							 <td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.status" cssStyle="width:100px" readonly="true" tabindex="3"/></td>  
						     <td align="right" width="" class="listwhitetext">Effective&nbsp;Date<font color="red" size="2">*</font></td>
							 <c:if test="${not empty partnerRateGrid.effectiveDate}"> 
					           <s:text id="partnerRateGridEffectiveDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGrid.effectiveDate"/></s:text>
				                 <td><s:textfield id="effectiveDate" name="partnerRateGrid.effectiveDate" value="%{partnerRateGridEffectiveDate}" readonly="true" cssClass="input-text" size="8" tabindex="4"/>
				                    <img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                 </td> 
				             </c:if>
				             <c:if test="${empty partnerRateGrid.effectiveDate}"> 
					             <td><s:textfield id="effectiveDate" name="partnerRateGrid.effectiveDate"  readonly="true" cssClass="input-text" size="8" tabindex="4"/>
					              <img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					             </td>
					         </c:if>
					         <c:if test="${partnerRateGrid.status=='Submitted'}"> 
					          <td align="right" class="listwhitetext" ><fmt:message key="partnerRateGrid.publishTariff" /></td>
					           <td align="left"> <s:checkbox name="partnerRateGrid.publishTariff" value="${partnerRateGrid.publishTariff}" fieldValue="true" tabindex="5" disabled="disabled" onclick="checkStatusId(this)" /></td>
						     </c:if>
						     <c:if test="${partnerRateGrid.status!='Submitted'}">
						       <s:hidden name="partnerRateGrid.publishTariff" /> 
						     </c:if>
						  </tr>
						  <tr>  
							<td align="right" width=""  class="listwhitetext"><fmt:message key="partnerRateGrid.tariffApplicability"/><font color="red" size="2">*</font></td>
							<td align="left" ><s:select list="%{tariffApplicabilityList}" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue="" name="partnerRateGrid.tariffApplicability" tabindex="6" onchange="fillStandardInclusions(); "/></td>
						    <%--<td align="right" width="100px"  class="listwhitetext">Scope<font color="red" size="2">*</font></td>
							 <c:if test="${partnerRateGrid.status!='Submitted'&& partnerRateGrid.status!='Withdraw'}">
							<td align="left" ><s:select list="%{tariffScopeList}" value="%{multipleTariffScope}" cssClass="list-menu" cssStyle="width:135px;height:50px"  headerKey="" headerValue="" name="partnerRateGrid.tariffScope" tabindex="7" multiple="true"/></td>
						    <td class="listwhitetext" width="240px" align="left">
                              * Use Control + mouse to select multiple scopes
                            </td>
                            </c:if>
                            <c:if test="${partnerRateGrid.status=='Submitted'||partnerRateGrid.status=='Withdraw'}"> 
                            <td align="left" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:290px" name="partnerRateGrid.tariffScope"/></td>
                            </c:if>
						 --%></tr>
						 <tr> 
							 <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.metroCity" /><font color="red" size="2">*</font></td>
							 <td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.metroCity" cssStyle="width:100px" maxlength="50" tabindex="8"/></td>
							 <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.currency" /><font color="red" size="2">*</font></td> 
							 <td align="left"><s:select list="%{currencyList}" cssClass="list-menu" cssStyle="width:135px" headerKey="" headerValue="" name="partnerRateGrid.currency" tabindex="9"/></td>
							 
						 </tr>
						 <tr>
						     <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.terminalCountry"/></td>
							 <td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.terminalCountry" cssStyle="width:100px" maxlength="3" readonly="true" value="${partnerPublic.terminalCountryCode}" tabindex="10"/></td>
						      <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.state" /></td> 
							 <td align="left"><s:select list="%{stateList}" id="state" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.state" value="%{partnerRateGrid.state}" tabindex="11"/></td>  
						     <td align="left" class="listwhitetext"><a href="javascript:;" style="text-decoration:underline;" onclick="displayTerms();" >Terms and Conditions</a></td> 
						 <tr>
						 </tr> 
						
						<%-- 	<td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.endDate"/></td>
							<c:if test="${not empty partnerRateGrid.endDate}"> 
					           <s:text id="partnerRateGridEndDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGrid.endDate"/></s:text>
				               <td><s:textfield id="endDate" name="partnerRateGrid.endDate" value="%{partnerRateGridEndDate}"  readonly="true" cssClass="input-text" size="8" tabindex="8"/>
				                <img id="calender1" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRateGridForm'].endDate,'calender1',document.forms['partnerRateGridForm'].dateFormat.value); return false;" />
				                </td> 
				             </c:if>
				             <c:if test="${empty partnerRateGrid.endDate}"> 
					             <td><s:textfield id="endDate" name="partnerRateGrid.endDate"  readonly="true" cssClass="input-text" size="8" tabindex="8"/>
					             <img id="calender1" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRateGridForm'].endDate,'calender1',document.forms['partnerRateGridForm'].dateFormat.value); return false;" />
					             </td>
					          </c:if> --%>
						
						<tr> 
							
						    
						 </tr>
						 <tr><td width="100%" colspan="6"> 
						<table width="100%" border="0"  class="detalTabLabel" style="margin: 0px;padding: 0px;">
						<tr><td colspan="8">
						 <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                          <tr>
                            <td class="headtab_left"> </td>
                            <td class="headtab_center" >&nbsp;Distance Adjustment Factors </td>
                            <td width="28" valign="top" class="headtab_bg"></td>
                            <td class="headtab_bg_special" >&nbsp; </td>
                            <td class="headtab_right"> </td>
                           </tr>
                         </table>
                        </td>
                       </tr>
                       <tr>
                       <td></td>
                       <td></td>
                       <td align="left"   class="listwhitetext">(Miles)</td>
                       <td align="left"   class="listwhitetext">(Kms)</td>
                       <td align="left"   class="listwhitetext">Extra&nbsp;Charges</td>
                       </tr>	
						<tr> 
							 <td align="right" class="listwhitetext" width="" ><fmt:message key="partnerRateGrid.serviceRangeMiles" /><font color="red" size="2">*</font></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext" width="83px" ><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles" cssStyle="text-align:right" size="10" maxlength="6"  onchange="convertToKm();" tabindex="12" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext" width="85px"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms" cssStyle="text-align:right" size="10" maxlength="6" onchange="convertToMiles();" tabindex="13" onkeydown="return onlyNumsAllowed(event)"/></td>
						     <td align="left" class="listwhitetext">Standard Charges Apply</td>
						 </tr>  
						 <tr> 
						     <td></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles2" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm2();" tabindex="14" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms2" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles2();" tabindex="15" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra2" size="6" maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)"  tabindex="16" onchange="checkFloat(this)" />(per cwt)</td>
						 </tr>
						 <tr> 
							 <td></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles3" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm3();" tabindex="17" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms3" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles3();" tabindex="18" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra3" size="6"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)"  tabindex="19" onchange="checkFloat(this)" />(per cwt)</td>
						 </tr>
						 <tr> 
							 <td></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles4" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm4();" tabindex="20" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms4" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles4();" tabindex="21" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra4" size="6"  maxlength="18" cssStyle="text-align:right"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="22"  onchange="checkFloat(this)"/>(per cwt)</td>
						 </tr>
						 <tr> 
							 <td></td>
							 <td align="right" class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles5"size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm5();" tabindex="23" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms5" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles5();" tabindex="24" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra5" size="6" maxlength="18" cssStyle="text-align:right"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="25"  onchange="checkFloat(this)" />(per cwt)</td>
						 </tr>
						 <tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Base Rate: Flat Charges per Container </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
				     	<%
						 HashMap <String, Set> partnerRateGridWeightMap1 = (HashMap <String, Set>)request.getAttribute("partnerRateGridWeightMap");
					     Iterator mapIteratorFlate = partnerRateGridWeightMap1.entrySet().iterator();
					     while (mapIteratorFlate.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIteratorFlate.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridWeightMap1.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }
							   if((parameter.equals("Flat$Rate"))|| (parameter.equals("Flat$RateHigh"))){
							   %>
							   
					        
					          
					         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR"); 
					           String[] detailsArray=basisDisplay.toString().split("#");
					           String grid=detailsArray[0];
					           String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	  %> 
					        	<tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	<s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					        	<td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" tabindex="<%=indexFlate%>" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td> 
					        	</tr> 
					         <% }
					        %>
					          <% if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	   %>
					        	   <tr>
					               <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
                                   <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" tabindex="<%=indexFlate%>"  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td> 
                                   </tr> 
					          <% } %>
					         
							  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	   %> 
					        	<tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	<s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					            <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" tabindex="<%=indexFlate%>" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					             </tr>
					         <% }%>
							  
							  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	  %> 
					        	  <tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	  <s:hidden name="<%=gridSection+"_"+editable+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					         <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" tabindex="<%=indexFlate%>" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          </tr>
					          <% }%>
							  
							   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
							      String[] detailsArray=basisDisplay.toString().split("#");
							      String grid=detailsArray[0];
							      String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  String flateLavel="";
				                  String indexFlate="26";
				                  if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="20'Loose Container";
					            	   indexFlate="26";
					               } 
					               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="40'Loose Container";
					            	   indexFlate="27";
					               }
					               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
					            	   flateLavel="40'HLoose Container";
					            	   indexFlate="28";
					               }
					        	   %> 
					        	   <tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					             <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" tabindex="<%=indexFlate%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					          </tr>
					          <% }%>
					         
							  <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
							     String[] detailsArray=basisDisplay.toString().split("#");
							     String grid=detailsArray[0];
							     String editable=detailsArray[1];
				                 String textValue= detailsArray[2];
				                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                 String basis=detailsArray[3];
				                 String flateLavel="";
				                 String indexFlate="26";
				                 if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="20'Loose Container";
					            	   indexFlate="26";
					               } 
					               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="40'Loose Container";
					            	   indexFlate="27";
					               }
					               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
					            	   flateLavel="40'HLoose Container";
					            	   indexFlate="28";
					               }
					        	  %>
					        	  <tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	  <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
					             <td ><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>"  value="<%=textValue%>" /></td> 
					             </tr>
					          <% }%>  
							  
						 <% }}%> 
						<tr><td width="100%" colspan="6">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="detalTabLabel" style="margin: 0px; padding: 2px;">
						<tr><td colspan="8">
						 <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Base Rate: Charges per CWT </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                        </td>
                       </tr>			
	                   <tr>
						<td align="right" colspan="2" class="listwhitetext" ></td> 
						<td align="center" class="listwhitetext" ></td>
					    <td align="center" class="listwhitetext"><b>20' Std</b></td> 
					    <td align="center" class="listwhitetext"><b>40' Std./High</b></td>
						<td align="center" class="listwhitetext"><b>Liftvan</b></td>
						<td align="center" class="listwhitetext"><b>Liftvan</b></td>
						<%-- <td align="center" class="listwhitetext"><b><fmt:message key="partnerRateGrid.tariffFalgGRP" /></b></td>--%> 
					  </tr> 
					  <tr>
						<td align="right" colspan="2" class="listwhitetext" ></td> 
						<td align="center" class="listwhitetext" ><b>Air</b></td>
					    <td align="center" class="listwhitetext"><b>Cont. Loose</b></td> 
					    <td align="center" class="listwhitetext"><b>Cont. Loose</b></td>
						<td align="center" class="listwhitetext"><b>LCL</b></td>
						<td align="center" class="listwhitetext"><b>FCL</b></td>
						<%-- <td align="center" class="listwhitetext"><b><fmt:message key="partnerRateGrid.tariffFalgGRP" /></b></td>--%> 
					  </tr> 
					  <tr>
				         <td align="right" colspan="2" class="listwhitetext" ><b>Applicable</b></td>
				         <td align="center"  valign="top"><s:checkbox name="partnerRateGrid.tarriffFlagAir" value="${partnerRateGrid.tarriffFlagAir}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('AIR_Y_');"  tabindex="29"/></td>
						 <td align="center"  valign="top" ><s:checkbox name="partnerRateGrid.tariffFlagL20" value="${partnerRateGrid.tariffFlagL20}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('L20_Y_');"  tabindex="30"/></td>
						 <td align="center"  valign="top"><s:checkbox name="partnerRateGrid.tariffFlagL40" value="${partnerRateGrid.tariffFlagL40}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('L40_Y_');" tabindex="31"/></td>
						 <td align="center" valign="top"><s:checkbox name="partnerRateGrid.tariffFlagLCL" value="${partnerRateGrid.tariffFlagLCL}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('LCL_Y_');" tabindex="32"/></td>
						 <td align="center" valign="top"><s:checkbox name="partnerRateGrid.tariffFlagFCL" value="${partnerRateGrid.tariffFlagFCL}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('FCL_Y_');"  tabindex="33"/></td>
						<%-- <td align="center" valign="top" width="100px"><s:checkbox name="partnerRateGrid.tariffFalgGRP" value="${partnerRateGrid.tariffFalgGRP}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('GRP_Y_');" tabindex="29"/></td> --%>
				       </tr>
				       
				     	<%
						 HashMap <String, Set> partnerRateGridWeightMap = (HashMap <String, Set>)request.getAttribute("partnerRateGridWeightMap");
					     Iterator mapIterator = partnerRateGridWeightMap.entrySet().iterator();
					     while (mapIterator.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIterator.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridWeightMap.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }
							   if((!(parameter.equals("Flat$Rate")))&&(!(parameter.equals("Flat$RateHigh")))){
							   %>
							   
					        
					          <tr>
					          <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=label%></td> 
					         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR"); 
					           String[] detailsArray=basisDisplay.toString().split("#");
					           String grid=detailsArray[0];
					           String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	  %> 
					        	<s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" /></td>  
					         <% }else{ 
					        	 
					         %> 
					            
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					          <% if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %>
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
                                   <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly=""  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% } else {%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
							  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					            <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					             
					         <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	  %> 
					        	  <s:hidden name="<%=gridSection+"_"+editable+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					         <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
							      String[] detailsArray=basisDisplay.toString().split("#");
							      String grid=detailsArray[0];
							      String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					        	   %> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					             <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
							  <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
							     String[] detailsArray=basisDisplay.toString().split("#");
							     String grid=detailsArray[0];
							     String editable=detailsArray[1];
				                 String textValue= detailsArray[2];
				                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                 String basis=detailsArray[3];
					        	  %>
					        	  <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
					             <td><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>"  value="<%=textValue%>" /></td> 
					          <% }else{%>
					        	 <td><s:hidden id="" name=""  /></td>  
					         <% }%> 
					         
					        
					        
							  </tr>    
						 <% }}%>
						 <tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Additional Charges </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>	   
					     
						 <%  HashMap <String, Set> partnerRateGridChargesMap = (HashMap <String, Set>)request.getAttribute("partnerRateGridChargesMap");
					     Iterator mapIterator1 = partnerRateGridChargesMap.entrySet().iterator();
					     while (mapIterator1.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIterator1.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridChargesMap.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }%> 
					          <tr>
					           <c:set var="labelValue"  value="<%=label%>" scope="request"/>
					           <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=gridSection+"_"+parameter+"_Label"%>"  cssClass="input-text"  value="<%=label%>"  onblur="clickrecall(this,'${labelValue}')" onclick="clickclear(this, '${labelValue}')" onchange="checkChargeHauling('AIR_Y_');checkChargeHauling('L20_Y_');checkChargeHauling('L40_Y_');checkChargeHauling('LCL_Y_');checkChargeHauling('FCL_Y_');"/></td>  
					         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR");
					                  String[] detailsArray=basisDisplay.toString().split("#");
					                  String grid=detailsArray[0];
					                  String editable=detailsArray[1];
					                  String textValue= detailsArray[2];
					                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
					                	  textValue="";  
					                  }
					                  String basis=detailsArray[3];
					        	  %> 
					        	 <c:set var="basis"  value="<%=basis%>" scope="request"/>
					        	<td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>  
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>    
					        	
					             
					         <% }else if((!(tdHashMap.containsKey("AIR")))&& (tdHashMap.containsKey("L20"))){
					        	 String basisDisplay=(String)tdHashMap.get("L20");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }else if((!(tdHashMap.containsKey("AIR")))&& (!tdHashMap.containsKey("L20"))&&(tdHashMap.containsKey("L40"))){
					        	 String basisDisplay=(String)tdHashMap.get("L40");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }
					         else if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&((tdHashMap.containsKey("LCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("LCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td> 
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& ((tdHashMap.containsKey("FCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("FCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& (!(tdHashMap.containsKey("FCL")))&& ((tdHashMap.containsKey("GRP")))){
					        	 String basisDisplay=(String)tdHashMap.get("GRP");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
					          if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %>
					        	    
                                 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly=""  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% } else {%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
							  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %> 
					        	   
					            <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					             
					         <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	  %> 
					        	   
					         <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
							      String[] detailsArray=basisDisplay.toString().split("#");
							      String grid=detailsArray[0];
							      String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					        	   %> 
					        	   
					             <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;" ><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
						 <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
							     String[] detailsArray=basisDisplay.toString().split("#");
							     String grid=detailsArray[0];
							     String editable=detailsArray[1];
				                 String textValue= detailsArray[2];
				                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                 String basis=detailsArray[3];
					        	  %>
					        	    
					             <td><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>"  value="<%=textValue%>" /></td> 
					          <% }else{%>
					        	 <td><s:hidden id="" name=""  /></td>  
					         <% }%>
					        
							  </tr>    
						 <% } %>
						 <tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Hauling Charges </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						 
					    <%  HashMap <String, Set> partnerRateGridHaulingMap = (HashMap <String, Set>)request.getAttribute("partnerRateGridHaulingMap");
					     Iterator mapIterator2 = partnerRateGridHaulingMap.entrySet().iterator();
					     while (mapIterator2.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIterator2.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridHaulingMap.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }%>  
							    <tr>
					           <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=gridSection+"_"+parameter+"_Label"%>"  cssClass="input-text"  value="<%=label%>" readonly="true" size="12" onfocus="checkChargeHauling('AIR_Y_');checkChargeHauling('L20_Y_');checkChargeHauling('L40_Y_');checkChargeHauling('LCL_Y_');checkChargeHauling('FCL_Y_');"/>
					           <c:if test="${partnerRateGrid.status!='Submitted' && partnerRateGrid.status!='Withdraw'}"> 
					           <img align="top"  name="<%="b"+gridSection+"_"+parameter+"_Label"%>"  onclick="findPortName(this);" src="${pageContext.request.contextPath}/images/question.png"/>
					           <img align="top" class="openpopup" width="17" name="<%="a"+gridSection+"_"+parameter+"_Label"%>" height="20" onclick="putPortName(this);" src="<c:url value='/images/open-popup.gif'/>" />
					           </c:if>
					           <c:if test="${partnerRateGrid.status=='Submitted' || partnerRateGrid.status=='Withdraw'}">
					           <img align="top"  name="<%="b"+gridSection+"_"+parameter+"_Label"%>"  onclick="findPortName(this);" src="${pageContext.request.contextPath}/images/question.png"/>
					           <img align="top" class="openpopup" width="17" name="<%="a"+gridSection+"_"+parameter+"_Label"%>" height="20"   src="<c:url value='/images/open-popup-disabled.gif'/>" />
					           </c:if> 
					           </td>  
							         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR");
							                  String[] detailsArray=basisDisplay.toString().split("#");
							                  String grid=detailsArray[0];
							                  String editable=detailsArray[1];
							                  String textValue= detailsArray[2];
							                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
							                	  textValue="";  
							                  }
							                  String basis=detailsArray[3]; 
							        	  %> 
							        	 <c:set var="basis"  value="<%=basis%>" scope="request"/> 
							        	 <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>  
							        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>    
							   <% }else if((!(tdHashMap.containsKey("AIR")))&& (tdHashMap.containsKey("L20"))){
					        	 String basisDisplay=(String)tdHashMap.get("L20");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3]; 
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }else if((!(tdHashMap.containsKey("AIR")))&& (!tdHashMap.containsKey("L20"))&&(tdHashMap.containsKey("L40"))){
					        	 String basisDisplay=(String)tdHashMap.get("L40");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }
					         else if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&((tdHashMap.containsKey("LCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("LCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td> 
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& ((tdHashMap.containsKey("FCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("FCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                 
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;" ><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& (!(tdHashMap.containsKey("FCL")))&& ((tdHashMap.containsKey("GRP")))){
					        	 String basisDisplay=(String)tdHashMap.get("GRP");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
							          if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
									   String[] detailsArray=basisDisplay.toString().split("#");
									   String grid=detailsArray[0];
									   String editable=detailsArray[1];
						               String textValue= detailsArray[2];
						               if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						               String basis=detailsArray[3];
						               
							        	   %>
							        	   
		                                 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly=""  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
							          <% } else {%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
							         
									  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
									   String[] detailsArray=basisDisplay.toString().split("#");
									   String grid=detailsArray[0];
									   String editable=detailsArray[1];
						               String textValue= detailsArray[2];
						               if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						               String basis=detailsArray[3];
						               
							        	   %> 
							        	  
							            <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
							             
							         <% }else{%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
									  
									  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
									   String[] detailsArray=basisDisplay.toString().split("#");
									   String grid=detailsArray[0];
									   String editable=detailsArray[1];
						               String textValue= detailsArray[2];
						               if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						               String basis=detailsArray[3];
						               
							        	  %> 
							        	  
							         <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
							          <% }else{%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
									  
									   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
									      String[] detailsArray=basisDisplay.toString().split("#");
									      String grid=detailsArray[0];
									      String editable=detailsArray[1];
						                  String textValue= detailsArray[2];
						                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						                  String basis=detailsArray[3];
						                  
							        	   %> 
							        	   
							             <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
							          <% }else{%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
							         
								 <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
									     String[] detailsArray=basisDisplay.toString().split("#");
									     String grid=detailsArray[0];
									     String editable=detailsArray[1];
						                 String textValue= detailsArray[2];
						                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						                 String basis=detailsArray[3];
						                 
							        	  %>
							        	  
							             <td><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" value="<%=textValue%>" /></td> 
							          <% }else{%>
							        	 <td><s:hidden id="" name=""   /></td>  
							         <% }%>
							      
									  </tr>    
								 <% } %>  
						</table> 
						</td>
						</tr>  
						</table>
						</td>
						</tr> 
						</tbody> 
						</table>
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Auto Handling Charges</td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                         </tr>
						</table>
						
						<table border="1" cellspacing="0" cellpadding="2"  class="p-table" style="margin: 6px;">						
						<tr>
						<td align="center" class="partner-text" ><b>Auto Handling</b></td>
						<td align="center" class="partner-text" >To Door</td>
						<td align="center" class="partner-text" >Whse Collect</td> 
						</tr>
						<tr>
						<td align="center" class="partner-text" >W/HHG FCL</td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_hhg_toDoor"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="200"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_lcl_toDoor"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="201"/></td>
						</tr>
						<tr>
						<td align="center" class="partner-text" >RO/RO</td> 
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_hhg_whse"  maxlength="18" cssStyle="text-align:right"  onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="203"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_lcl_whse"  maxlength="18"  cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="204"/></td>
						</tr>
						</table>
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Storage in Transit Charges </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						</table>
						
						<table border="1" cellspacing="0" cellpadding="2" class="p-table" style="margin: 6px;"> 
						<tr>
						<td align="right" class="partner-text" ><b>Storage in Transit</b></td> 
						<td align="center" class="partner-text" >Period</td>
						<td align="center" class="partner-text" >Basis</td>
						<td align="center" class="partner-text" >Rate</td>
						</tr>
						<tr>
						<td align="right" class="partner-text" >Warehouse Handling Loose</td>
						<td align="left"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" /></td>  
						<td align="left"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_whse_loose_basis" tabindex="205"/></td>  
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.sit_whse_loose_rate"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="206"/></td>
						</tr>
						<tr>
						<td align="right" class="partner-text" >Warehouse Handling Lift Van</td>
						<td align="left"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_whse_liftvan_basis" tabindex="207"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.sit_whse_liftvan_rate"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="208"/></td>
						</tr>
						<tr>
						<td align="right" class="partner-text" >Storage per (state unit period)</td>
						<td align="left"><s:select list="%{sit_storage_periodList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_storage_period" tabindex="209" /></td>
						<td align="left"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_storage_basis" tabindex="210"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.sit_storage_rate"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="211"/></td>
						</tr>
						</table>
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Standard Inclusions and Exclusions </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						</table>
						<table>
						
                       <tr>
                       <td> 
                       <DIV ID="GIS1" ><p align="justify" style="color:#fffff; width:98% ">                                           
						${standardDestinationNote}</p></div></td>
						<td><DIV ID="GIS2" ><p align="justify" style="color:#fffff; width:98% ">  
                       ${standardOriginNote}					    
                        </p></DIV> </td></tr>
                        <s:hidden name="partnerRateGrid.standardInclusions"></s:hidden>
                         </table> 
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Agent Tariff Notes</td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						</table>
						<table>
						<tr>
						<td colspan="6" align="left" class="listwhitetext"><s:textarea  name="partnerRateGrid.tariffNotes" rows="6" cols="99"  cssClass="textarea"/></td>
						</tr>
						</table>
						<table><tr><td></td></tr></table>
					</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div> 
<table>
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${partnerRateGrid.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='serviceOrder.createdOn' /></b></td>
				<s:hidden name="partnerRateGrid.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${partnerRateGrid.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='serviceOrder.createdBy' /></b></td>
				<c:if test="${not empty partnerRateGrid.id}">
					<s:hidden name="partnerRateGrid.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{partnerRateGrid.createdBy}" /></td>
				</c:if>
				<c:if test="${empty partnerRateGrid.id}">
					<s:hidden name="partnerRateGrid.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if> 
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='serviceOrder.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${partnerRateGrid.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="partnerRateGrid.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${partnerRateGrid.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='serviceOrder.updatedBy' /></b></td>
				<c:if test="${not empty partnerRateGrid.id}"> 
                <s:hidden name="partnerRateGrid.updatedBy"/> 
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="%{partnerRateGrid.updatedBy}"/></td>
				</c:if> 
				<c:if test="${empty partnerRateGrid.id}"> 
				<s:hidden name="partnerRateGrid.updatedBy" value="${pageContext.request.remoteUser}"/> 
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				
				</c:if> 
			</tr>
		</tbody>
	</table>
<c:if test="${partnerRateGrid.status!='Submitted' && partnerRateGrid.status!='Withdraw'}"> 
<input type="button" class="cssbuttonA" style="width:55px; height:25px"  name="save"  value="Save" onclick="return basisValidation('Save');"  tabindex="212"/>
<input type="button" class="cssbuttonA" style="width:60px; height:25px"  name="Submit"  value="Submit" onclick="return basisValidation('Submit');" tabindex="213"/>
<input type="button" class="cssbutton1" value="Close" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/partnerRateGrids.html?partnerId=${partnerPublic.id}"/>'" />
<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
<input type="button" class="cssbutton1" value="Print" name="Print" size="20" style="width:55px; height:25px" onclick="printRateGrids();" />
</c:if>
<c:if test="${partnerRateGrid.status=='Submitted'}">
<input type="button" class="cssbutton1" value="Close"  name="Close" style="width:55px; height:25px" onclick="location.href='<c:url value="/partnerRateGrids.html?partnerId=${partnerPublic.id}"/>'" /> 
<sec-auth:authComponent componentId="module.button.partnerRateGrid.withdrawButton">
	<input type="button" class="cssbuttonA" style="width:70px; height:25px"  name="Withdraw"  value="Withdraw" onclick="WithdrawRateGrid();" tabindex="213"/>
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.button.partnerRateGrid.requestWithdrawalButton">
	<input type="button" class="cssbuttonA" style="width:140px; height:25px"  name="RequestWithdrawal"  value="Request Withdrawal" onclick="requestWithdrawal();" tabindex="213"/>
</sec-auth:authComponent>
<input type="button" class="cssbutton1" value="Print" name="Print" size="20" style="width:55px; height:25px" onclick="printRateGrids();" />
</c:if>
<c:if test="${partnerRateGrid.status=='Withdraw'}">
<input type="button" class="cssbutton1" value="Close" name="Close"  style="width:55px; height:25px" onclick="location.href='<c:url value="/partnerRateGrids.html?partnerId=${partnerPublic.id}"/>'" /> 
<input type="button" class="cssbutton1" value="Print" name="Print" size="20" style="width:55px; height:25px" onclick="printRateGrids();" />
</c:if>



</s:form>

<s:form id="partnerRateGridForm1"  name="partnerRateGridForm1" action="updateRateGridStatus.html" method="post" validate="true">
<input type="hidden" name="id">
<input type="hidden" name="partnerId">
<input type="hidden" name="status">
</s:form>
<script type="text/javascript">  
try{
allReadOnly();
}
catch(e){}
try{
tabindexnumber();
}
catch(e){}
try{
readOnlyOnclick('AIR_Y_');
}
catch(e){}
try{
readOnlyOnclick('L20_Y_');
}
catch(e){}
try{
readOnlyOnclick('L40_Y_');
}
catch(e){}
try{
readOnlyOnclick('LCL_Y_');
}
catch(e){}
try{
readOnlyOnclick('FCL_Y_');
}
catch(e){}
//readOnlyOnclick('GRP_Y_');
try{
stateDisabled(); 
}
catch(e){}
try{
rangeExtraDisabled();
}
catch(e){}

checkStandardCountry();
try{
fillStandardInclusions()
}
catch(e){}
</script>  

<iframe id="TandC" src="getTermsCondition.html?&decorator=popup&popup=true" style="background-color: #FFFFFF ;width:700px; height:540px;z-index:999; -moz-border-radius:0.8em; border: 4px solid #525252; margin:0px auto -1px auto;; position:absolute; left:15%; top:0px; overflow:none;" >
</iframe>
<div id="overlayTerms"></div>	

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>