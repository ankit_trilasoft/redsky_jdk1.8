package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;


@Entity
@Table(name = "crewcapacity")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class CrewCapacity extends BaseObject{
	
	private Long id;
	private String corpID;
	private String updatedBy;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;

	private String crewGroup;
	private String jobType;

	private BigDecimal weightRangeMinQty1;
	private BigDecimal weightRangeMaxQty1;
	private BigDecimal weightRangeAverageWeight1;
	private String weightRangeMode1;

	private BigDecimal weightRangeMinQty2;
	private BigDecimal weightRangeMaxQty2;
	private BigDecimal weightRangeAverageWeight2;
	private String weightRangeMode2;

	private BigDecimal weightRangeMinQty3;
	private BigDecimal weightRangeMaxQty3;
	private BigDecimal weightRangeAverageWeight3;
	private String weightRangeMode3;

	private BigDecimal weightRangeMinQty4;
	private BigDecimal weightRangeMaxQty4;
	private BigDecimal weightRangeAverageWeight4;
	private String weightRangeMode4;

	private BigDecimal weightRangeMinQty5;
	private BigDecimal weightRangeMaxQty5;
	private BigDecimal weightRangeAverageWeight5;
	private String weightRangeMode5;

	private BigDecimal weightRangeMinQty6;
	private BigDecimal weightRangeMaxQty6;
	private BigDecimal weightRangeAverageWeight6;
	private String weightRangeMode6;

	private BigDecimal weightRangeMinQty7;
	private BigDecimal weightRangeMaxQty7;
	private BigDecimal weightRangeAverageWeight7;
	private String weightRangeMode7;

	private BigDecimal weightRangeMinQty8;
	private BigDecimal weightRangeMaxQty8;
	private BigDecimal weightRangeAverageWeight8;
	private String weightRangeMode8;

	private BigDecimal weightRangeMinQty9;
	private BigDecimal weightRangeMaxQty9;
	private BigDecimal weightRangeAverageWeight9;
	private String weightRangeMode9;

	private BigDecimal weightRangeMinQty10;
	private BigDecimal weightRangeMaxQty10;
	private BigDecimal weightRangeAverageWeight10;
	private String weightRangeMode10;

	private BigDecimal weightRangeMinQty11;
	private BigDecimal weightRangeMaxQty11;
	private BigDecimal weightRangeAverageWeight11;
	private String weightRangeMode11;

	private BigDecimal weightRangeMinQty12;
	private BigDecimal weightRangeMaxQty12;
	private BigDecimal weightRangeAverageWeight12;
	private String weightRangeMode12;

	private BigDecimal weightRangeMinQty13;
	private BigDecimal weightRangeMaxQty13;
	private BigDecimal weightRangeAverageWeight13;
	private String weightRangeMode13;

	private BigDecimal weightRangeMinQty14;
	private BigDecimal weightRangeMaxQty14;
	private BigDecimal weightRangeAverageWeight14;
	private String weightRangeMode14;

	private BigDecimal weightRangeMinQty15;
	private BigDecimal weightRangeMaxQty15;
	private BigDecimal weightRangeAverageWeight15;
	private String weightRangeMode15;
	
	private BigDecimal weightRangeMinQty16;
	private BigDecimal weightRangeMaxQty16;
	private BigDecimal weightRangeAverageWeight16;
	private String weightRangeMode16;
	
	private BigDecimal weightRangeMinQty17;
	private BigDecimal weightRangeMaxQty17;
	private BigDecimal weightRangeAverageWeight17;
	private String weightRangeMode17;
	
	private BigDecimal weightRangeMinQty18;
	private BigDecimal weightRangeMaxQty18;
	private BigDecimal weightRangeAverageWeight18;
	private String weightRangeMode18;
	
	private BigDecimal weightRangeMinQty19;
	private BigDecimal weightRangeMaxQty19;
	private BigDecimal weightRangeAverageWeight19;
	private String weightRangeMode19;
	
	private BigDecimal weightRangeMinQty20;
	private BigDecimal weightRangeMaxQty20;
	private BigDecimal weightRangeAverageWeight20;
	private String weightRangeMode20;
	
	private BigDecimal weightRangeMinQty21;
	private BigDecimal weightRangeMaxQty21;
	private BigDecimal weightRangeAverageWeight21;
	private String weightRangeMode21;
	
	private BigDecimal weightRangeMinQty22;
	private BigDecimal weightRangeMaxQty22;
	private BigDecimal weightRangeAverageWeight22;
	private String weightRangeMode22;
	
	private BigDecimal weightRangeMinQty23;
	private BigDecimal weightRangeMaxQty23;
	private BigDecimal weightRangeAverageWeight23;
	private String weightRangeMode23;
	
	private BigDecimal weightRangeMinQty24;
	private BigDecimal weightRangeMaxQty24;
	private BigDecimal weightRangeAverageWeight24;
	private String weightRangeMode24;
	
	private BigDecimal weightRangeMinQty25;
	private BigDecimal weightRangeMaxQty25;
	private BigDecimal weightRangeAverageWeight25;
	private String weightRangeMode25;

	private String assumptionServiceType1;
	private String assumptionMode1;
	private BigDecimal assumptionAverageWeight1;
	private String assumptionCrew1;

	private String assumptionServiceType2;
	private String assumptionMode2;
	private BigDecimal assumptionAverageWeight2;
	private String assumptionCrew2;

	private String assumptionServiceType3;
	private String assumptionMode3;
	private BigDecimal assumptionAverageWeight3;
	private String assumptionCrew3;

	private String assumptionServiceType4;
	private String assumptionMode4;
	private BigDecimal assumptionAverageWeight4;
	private String assumptionCrew4;

	private String assumptionServiceType5;
	private String assumptionMode5;
	private BigDecimal assumptionAverageWeight5;
	private String assumptionCrew5;

	private String assumptionServiceType6;
	private String assumptionMode6;
	private BigDecimal assumptionAverageWeight6;
	private String assumptionCrew6;

	private String assumptionServiceType7;
	private String assumptionMode7;
	private BigDecimal assumptionAverageWeight7;
	private String assumptionCrew7;

	private String adjustmentPhysicalCondition1;
	private String adjustmentPercentage1;

	private String adjustmentPhysicalCondition2;
	private String adjustmentPercentage2;

	private String adjustmentPhysicalCondition3;
	private String adjustmentPercentage3;

	private String adjustmentPhysicalCondition4;
	private String adjustmentPercentage4;

	private String adjustmentPhysicalCondition5;
	private String adjustmentPercentage5;

	private String adjustmentPhysicalCondition6;
	private String adjustmentPercentage6;
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CrewCapacity))
			return false;
		CrewCapacity castOther = (CrewCapacity) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(crewGroup, castOther.crewGroup)
				.append(jobType, castOther.jobType)
				.append(weightRangeMinQty1, castOther.weightRangeMinQty1)
				.append(weightRangeMaxQty1, castOther.weightRangeMaxQty1)
				.append(weightRangeAverageWeight1,
						castOther.weightRangeAverageWeight1)
				.append(weightRangeMode1, castOther.weightRangeMode1)
				.append(weightRangeMinQty2, castOther.weightRangeMinQty2)
				.append(weightRangeMaxQty2, castOther.weightRangeMaxQty2)
				.append(weightRangeAverageWeight2,
						castOther.weightRangeAverageWeight2)
				.append(weightRangeMode2, castOther.weightRangeMode2)
				.append(weightRangeMinQty3, castOther.weightRangeMinQty3)
				.append(weightRangeMaxQty3, castOther.weightRangeMaxQty3)
				.append(weightRangeAverageWeight3,
						castOther.weightRangeAverageWeight3)
				.append(weightRangeMode3, castOther.weightRangeMode3)
				.append(weightRangeMinQty4, castOther.weightRangeMinQty4)
				.append(weightRangeMaxQty4, castOther.weightRangeMaxQty4)
				.append(weightRangeAverageWeight4,
						castOther.weightRangeAverageWeight4)
				.append(weightRangeMode4, castOther.weightRangeMode4)
				.append(weightRangeMinQty5, castOther.weightRangeMinQty5)
				.append(weightRangeMaxQty5, castOther.weightRangeMaxQty5)
				.append(weightRangeAverageWeight5,
						castOther.weightRangeAverageWeight5)
				.append(weightRangeMode5, castOther.weightRangeMode5)
				.append(weightRangeMinQty6, castOther.weightRangeMinQty6)
				.append(weightRangeMaxQty6, castOther.weightRangeMaxQty6)
				.append(weightRangeAverageWeight6,
						castOther.weightRangeAverageWeight6)
				.append(weightRangeMode6, castOther.weightRangeMode6)
				.append(weightRangeMinQty7, castOther.weightRangeMinQty7)
				.append(weightRangeMaxQty7, castOther.weightRangeMaxQty7)
				.append(weightRangeAverageWeight7,
						castOther.weightRangeAverageWeight7)
				.append(weightRangeMode7, castOther.weightRangeMode7)
				.append(weightRangeMinQty8, castOther.weightRangeMinQty8)
				.append(weightRangeMaxQty8, castOther.weightRangeMaxQty8)
				.append(weightRangeAverageWeight8,
						castOther.weightRangeAverageWeight8)
				.append(weightRangeMode8, castOther.weightRangeMode8)
				.append(weightRangeMinQty9, castOther.weightRangeMinQty9)
				.append(weightRangeMaxQty9, castOther.weightRangeMaxQty9)
				.append(weightRangeAverageWeight9,
						castOther.weightRangeAverageWeight9)
				.append(weightRangeMode9, castOther.weightRangeMode9)
				.append(weightRangeMinQty10, castOther.weightRangeMinQty10)
				.append(weightRangeMaxQty10, castOther.weightRangeMaxQty10)
				.append(weightRangeAverageWeight10,
						castOther.weightRangeAverageWeight10)
				.append(weightRangeMode10, castOther.weightRangeMode10)
				.append(weightRangeMinQty11, castOther.weightRangeMinQty11)
				.append(weightRangeMaxQty11, castOther.weightRangeMaxQty11)
				.append(weightRangeAverageWeight11,
						castOther.weightRangeAverageWeight11)
				.append(weightRangeMode11, castOther.weightRangeMode11)
				.append(weightRangeMinQty12, castOther.weightRangeMinQty12)
				.append(weightRangeMaxQty12, castOther.weightRangeMaxQty12)
				.append(weightRangeAverageWeight12,
						castOther.weightRangeAverageWeight12)
				.append(weightRangeMode12, castOther.weightRangeMode12)
				.append(weightRangeMinQty13, castOther.weightRangeMinQty13)
				.append(weightRangeMaxQty13, castOther.weightRangeMaxQty13)
				.append(weightRangeAverageWeight13,
						castOther.weightRangeAverageWeight13)
				.append(weightRangeMode13, castOther.weightRangeMode13)
				.append(weightRangeMinQty14, castOther.weightRangeMinQty14)
				.append(weightRangeMaxQty14, castOther.weightRangeMaxQty14)
				.append(weightRangeAverageWeight14,
						castOther.weightRangeAverageWeight14)
				.append(weightRangeMode14, castOther.weightRangeMode14)
				.append(weightRangeMinQty15, castOther.weightRangeMinQty15)
				.append(weightRangeMaxQty15, castOther.weightRangeMaxQty15)
				.append(weightRangeAverageWeight15,
						castOther.weightRangeAverageWeight15)
				.append(weightRangeMode15, castOther.weightRangeMode15)
				.append(weightRangeMinQty16, castOther.weightRangeMinQty16)
				.append(weightRangeMaxQty16, castOther.weightRangeMaxQty16)
				.append(weightRangeAverageWeight16,
						castOther.weightRangeAverageWeight16)
				.append(weightRangeMode16, castOther.weightRangeMode16)
				.append(weightRangeMinQty17, castOther.weightRangeMinQty17)
				.append(weightRangeMaxQty17, castOther.weightRangeMaxQty17)
				.append(weightRangeAverageWeight17,
						castOther.weightRangeAverageWeight17)
				.append(weightRangeMode17, castOther.weightRangeMode17)
				.append(weightRangeMinQty18, castOther.weightRangeMinQty18)
				.append(weightRangeMaxQty18, castOther.weightRangeMaxQty18)
				.append(weightRangeAverageWeight18,
						castOther.weightRangeAverageWeight18)
				.append(weightRangeMode18, castOther.weightRangeMode18)
				.append(weightRangeMinQty19, castOther.weightRangeMinQty19)
				.append(weightRangeMaxQty19, castOther.weightRangeMaxQty19)
				.append(weightRangeAverageWeight19,
						castOther.weightRangeAverageWeight19)
				.append(weightRangeMode19, castOther.weightRangeMode19)
				.append(weightRangeMinQty20, castOther.weightRangeMinQty20)
				.append(weightRangeMaxQty20, castOther.weightRangeMaxQty20)
				.append(weightRangeAverageWeight20,
						castOther.weightRangeAverageWeight20)
				.append(weightRangeMode20, castOther.weightRangeMode20)
				.append(weightRangeMinQty21, castOther.weightRangeMinQty21)
				.append(weightRangeMaxQty21, castOther.weightRangeMaxQty21)
				.append(weightRangeAverageWeight21,
						castOther.weightRangeAverageWeight21)
				.append(weightRangeMode21, castOther.weightRangeMode21)
				.append(weightRangeMinQty22, castOther.weightRangeMinQty22)
				.append(weightRangeMaxQty22, castOther.weightRangeMaxQty22)
				.append(weightRangeAverageWeight22,
						castOther.weightRangeAverageWeight22)
				.append(weightRangeMode22, castOther.weightRangeMode22)
				.append(weightRangeMinQty23, castOther.weightRangeMinQty23)
				.append(weightRangeMaxQty23, castOther.weightRangeMaxQty23)
				.append(weightRangeAverageWeight23,
						castOther.weightRangeAverageWeight23)
				.append(weightRangeMode23, castOther.weightRangeMode23)
				.append(weightRangeMinQty24, castOther.weightRangeMinQty24)
				.append(weightRangeMaxQty24, castOther.weightRangeMaxQty24)
				.append(weightRangeAverageWeight24,
						castOther.weightRangeAverageWeight24)
				.append(weightRangeMode24, castOther.weightRangeMode24)
				.append(weightRangeMinQty25, castOther.weightRangeMinQty25)
				.append(weightRangeMaxQty25, castOther.weightRangeMaxQty25)
				.append(weightRangeAverageWeight25,
						castOther.weightRangeAverageWeight25)
				.append(weightRangeMode25, castOther.weightRangeMode25)
				.append(assumptionServiceType1,
						castOther.assumptionServiceType1)
				.append(assumptionMode1, castOther.assumptionMode1)
				.append(assumptionAverageWeight1,
						castOther.assumptionAverageWeight1)
				.append(assumptionCrew1, castOther.assumptionCrew1)
				.append(assumptionServiceType2,
						castOther.assumptionServiceType2)
				.append(assumptionMode2, castOther.assumptionMode2)
				.append(assumptionAverageWeight2,
						castOther.assumptionAverageWeight2)
				.append(assumptionCrew2, castOther.assumptionCrew2)
				.append(assumptionServiceType3,
						castOther.assumptionServiceType3)
				.append(assumptionMode3, castOther.assumptionMode3)
				.append(assumptionAverageWeight3,
						castOther.assumptionAverageWeight3)
				.append(assumptionCrew3, castOther.assumptionCrew3)
				.append(assumptionServiceType4,
						castOther.assumptionServiceType4)
				.append(assumptionMode4, castOther.assumptionMode4)
				.append(assumptionAverageWeight4,
						castOther.assumptionAverageWeight4)
				.append(assumptionCrew4, castOther.assumptionCrew4)
				.append(assumptionServiceType5,
						castOther.assumptionServiceType5)
				.append(assumptionMode5, castOther.assumptionMode5)
				.append(assumptionAverageWeight5,
						castOther.assumptionAverageWeight5)
				.append(assumptionCrew5, castOther.assumptionCrew5)
				.append(assumptionServiceType6,
						castOther.assumptionServiceType6)
				.append(assumptionMode6, castOther.assumptionMode6)
				.append(assumptionAverageWeight6,
						castOther.assumptionAverageWeight6)
				.append(assumptionCrew6, castOther.assumptionCrew6)
				.append(assumptionServiceType7,
						castOther.assumptionServiceType7)
				.append(assumptionMode7, castOther.assumptionMode7)
				.append(assumptionAverageWeight7,
						castOther.assumptionAverageWeight7)
				.append(assumptionCrew7, castOther.assumptionCrew7)
				.append(adjustmentPhysicalCondition1,
						castOther.adjustmentPhysicalCondition1)
				.append(adjustmentPercentage1, castOther.adjustmentPercentage1)
				.append(adjustmentPhysicalCondition2,
						castOther.adjustmentPhysicalCondition2)
				.append(adjustmentPercentage2, castOther.adjustmentPercentage2)
				.append(adjustmentPhysicalCondition3,
						castOther.adjustmentPhysicalCondition3)
				.append(adjustmentPercentage3, castOther.adjustmentPercentage3)
				.append(adjustmentPhysicalCondition4,
						castOther.adjustmentPhysicalCondition4)
				.append(adjustmentPercentage4, castOther.adjustmentPercentage4)
				.append(adjustmentPhysicalCondition5,
						castOther.adjustmentPhysicalCondition5)
				.append(adjustmentPercentage5, castOther.adjustmentPercentage5)
				.append(adjustmentPhysicalCondition6,
						castOther.adjustmentPhysicalCondition6)
				.append(adjustmentPercentage6, castOther.adjustmentPercentage6)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(updatedBy).append(createdBy).append(createdOn)
				.append(updatedOn).append(crewGroup).append(jobType)
				.append(weightRangeMinQty1).append(weightRangeMaxQty1)
				.append(weightRangeAverageWeight1).append(weightRangeMode1)
				.append(weightRangeMinQty2).append(weightRangeMaxQty2)
				.append(weightRangeAverageWeight2).append(weightRangeMode2)
				.append(weightRangeMinQty3).append(weightRangeMaxQty3)
				.append(weightRangeAverageWeight3).append(weightRangeMode3)
				.append(weightRangeMinQty4).append(weightRangeMaxQty4)
				.append(weightRangeAverageWeight4).append(weightRangeMode4)
				.append(weightRangeMinQty5).append(weightRangeMaxQty5)
				.append(weightRangeAverageWeight5).append(weightRangeMode5)
				.append(weightRangeMinQty6).append(weightRangeMaxQty6)
				.append(weightRangeAverageWeight6).append(weightRangeMode6)
				.append(weightRangeMinQty7).append(weightRangeMaxQty7)
				.append(weightRangeAverageWeight7).append(weightRangeMode7)
				.append(weightRangeMinQty8).append(weightRangeMaxQty8)
				.append(weightRangeAverageWeight8).append(weightRangeMode8)
				.append(weightRangeMinQty9).append(weightRangeMaxQty9)
				.append(weightRangeAverageWeight9).append(weightRangeMode9)
				.append(weightRangeMinQty10).append(weightRangeMaxQty10)
				.append(weightRangeAverageWeight10).append(weightRangeMode10)
				.append(weightRangeMinQty11).append(weightRangeMaxQty11)
				.append(weightRangeAverageWeight11).append(weightRangeMode11)
				.append(weightRangeMinQty12).append(weightRangeMaxQty12)
				.append(weightRangeAverageWeight12).append(weightRangeMode12)
				.append(weightRangeMinQty13).append(weightRangeMaxQty13)
				.append(weightRangeAverageWeight13).append(weightRangeMode13)
				.append(weightRangeMinQty14).append(weightRangeMaxQty14)
				.append(weightRangeAverageWeight14).append(weightRangeMode14)
				.append(weightRangeMinQty15).append(weightRangeMaxQty15)
				.append(weightRangeAverageWeight15).append(weightRangeMode15)
				.append(weightRangeMinQty16).append(weightRangeMaxQty16)
				.append(weightRangeAverageWeight16).append(weightRangeMode16)
				.append(weightRangeMinQty17).append(weightRangeMaxQty17)
				.append(weightRangeAverageWeight17).append(weightRangeMode17)
				.append(weightRangeMinQty18).append(weightRangeMaxQty18)
				.append(weightRangeAverageWeight18).append(weightRangeMode18)
				.append(weightRangeMinQty19).append(weightRangeMaxQty19)
				.append(weightRangeAverageWeight19).append(weightRangeMode19)
				.append(weightRangeMinQty20).append(weightRangeMaxQty20)
				.append(weightRangeAverageWeight20).append(weightRangeMode20)
				.append(weightRangeMinQty21).append(weightRangeMaxQty21)
				.append(weightRangeAverageWeight21).append(weightRangeMode21)
				.append(weightRangeMinQty22).append(weightRangeMaxQty22)
				.append(weightRangeAverageWeight22).append(weightRangeMode22)
				.append(weightRangeMinQty23).append(weightRangeMaxQty23)
				.append(weightRangeAverageWeight23).append(weightRangeMode23)
				.append(weightRangeMinQty24).append(weightRangeMaxQty24)
				.append(weightRangeAverageWeight24).append(weightRangeMode24)
				.append(weightRangeMinQty25).append(weightRangeMaxQty25)
				.append(weightRangeAverageWeight25).append(weightRangeMode25)
				.append(assumptionServiceType1).append(assumptionMode1)
				.append(assumptionAverageWeight1).append(assumptionCrew1)
				.append(assumptionServiceType2).append(assumptionMode2)
				.append(assumptionAverageWeight2).append(assumptionCrew2)
				.append(assumptionServiceType3).append(assumptionMode3)
				.append(assumptionAverageWeight3).append(assumptionCrew3)
				.append(assumptionServiceType4).append(assumptionMode4)
				.append(assumptionAverageWeight4).append(assumptionCrew4)
				.append(assumptionServiceType5).append(assumptionMode5)
				.append(assumptionAverageWeight5).append(assumptionCrew5)
				.append(assumptionServiceType6).append(assumptionMode6)
				.append(assumptionAverageWeight6).append(assumptionCrew6)
				.append(assumptionServiceType7).append(assumptionMode7)
				.append(assumptionAverageWeight7).append(assumptionCrew7)
				.append(adjustmentPhysicalCondition1)
				.append(adjustmentPercentage1)
				.append(adjustmentPhysicalCondition2)
				.append(adjustmentPercentage2)
				.append(adjustmentPhysicalCondition3)
				.append(adjustmentPercentage3)
				.append(adjustmentPhysicalCondition4)
				.append(adjustmentPercentage4)
				.append(adjustmentPhysicalCondition5)
				.append(adjustmentPercentage5)
				.append(adjustmentPhysicalCondition6)
				.append(adjustmentPercentage6).toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("updatedBy", updatedBy)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("crewGroup", crewGroup)
				.append("jobType", jobType)
				.append("weightRangeMinQty1", weightRangeMinQty1)
				.append("weightRangeMaxQty1", weightRangeMaxQty1)
				.append("weightRangeAverageWeight1", weightRangeAverageWeight1)
				.append("weightRangeMode1", weightRangeMode1)
				.append("weightRangeMinQty2", weightRangeMinQty2)
				.append("weightRangeMaxQty2", weightRangeMaxQty2)
				.append("weightRangeAverageWeight2", weightRangeAverageWeight2)
				.append("weightRangeMode2", weightRangeMode2)
				.append("weightRangeMinQty3", weightRangeMinQty3)
				.append("weightRangeMaxQty3", weightRangeMaxQty3)
				.append("weightRangeAverageWeight3", weightRangeAverageWeight3)
				.append("weightRangeMode3", weightRangeMode3)
				.append("weightRangeMinQty4", weightRangeMinQty4)
				.append("weightRangeMaxQty4", weightRangeMaxQty4)
				.append("weightRangeAverageWeight4", weightRangeAverageWeight4)
				.append("weightRangeMode4", weightRangeMode4)
				.append("weightRangeMinQty5", weightRangeMinQty5)
				.append("weightRangeMaxQty5", weightRangeMaxQty5)
				.append("weightRangeAverageWeight5", weightRangeAverageWeight5)
				.append("weightRangeMode5", weightRangeMode5)
				.append("weightRangeMinQty6", weightRangeMinQty6)
				.append("weightRangeMaxQty6", weightRangeMaxQty6)
				.append("weightRangeAverageWeight6", weightRangeAverageWeight6)
				.append("weightRangeMode6", weightRangeMode6)
				.append("weightRangeMinQty7", weightRangeMinQty7)
				.append("weightRangeMaxQty7", weightRangeMaxQty7)
				.append("weightRangeAverageWeight7", weightRangeAverageWeight7)
				.append("weightRangeMode7", weightRangeMode7)
				.append("weightRangeMinQty8", weightRangeMinQty8)
				.append("weightRangeMaxQty8", weightRangeMaxQty8)
				.append("weightRangeAverageWeight8", weightRangeAverageWeight8)
				.append("weightRangeMode8", weightRangeMode8)
				.append("weightRangeMinQty9", weightRangeMinQty9)
				.append("weightRangeMaxQty9", weightRangeMaxQty9)
				.append("weightRangeAverageWeight9", weightRangeAverageWeight9)
				.append("weightRangeMode9", weightRangeMode9)
				.append("weightRangeMinQty10", weightRangeMinQty10)
				.append("weightRangeMaxQty10", weightRangeMaxQty10)
				.append("weightRangeAverageWeight10",
						weightRangeAverageWeight10)
				.append("weightRangeMode10", weightRangeMode10)
				.append("weightRangeMinQty11", weightRangeMinQty11)
				.append("weightRangeMaxQty11", weightRangeMaxQty11)
				.append("weightRangeAverageWeight11",
						weightRangeAverageWeight11)
				.append("weightRangeMode11", weightRangeMode11)
				.append("weightRangeMinQty12", weightRangeMinQty12)
				.append("weightRangeMaxQty12", weightRangeMaxQty12)
				.append("weightRangeAverageWeight12",
						weightRangeAverageWeight12)
				.append("weightRangeMode12", weightRangeMode12)
				.append("weightRangeMinQty13", weightRangeMinQty13)
				.append("weightRangeMaxQty13", weightRangeMaxQty13)
				.append("weightRangeAverageWeight13",
						weightRangeAverageWeight13)
				.append("weightRangeMode13", weightRangeMode13)
				.append("weightRangeMinQty14", weightRangeMinQty14)
				.append("weightRangeMaxQty14", weightRangeMaxQty14)
				.append("weightRangeAverageWeight14",
						weightRangeAverageWeight14)
				.append("weightRangeMode14", weightRangeMode14)
				.append("weightRangeMinQty15", weightRangeMinQty15)
				.append("weightRangeMaxQty15", weightRangeMaxQty15)
				.append("weightRangeAverageWeight15",
						weightRangeAverageWeight15)
				.append("weightRangeMode15", weightRangeMode15)
				.append("weightRangeMinQty16", weightRangeMinQty16)
				.append("weightRangeMaxQty16", weightRangeMaxQty16)
				.append("weightRangeAverageWeight16",
						weightRangeAverageWeight16)
				.append("weightRangeMode16", weightRangeMode16)
				.append("weightRangeMinQty17", weightRangeMinQty17)
				.append("weightRangeMaxQty17", weightRangeMaxQty17)
				.append("weightRangeAverageWeight17",
						weightRangeAverageWeight17)
				.append("weightRangeMode17", weightRangeMode17)
				.append("weightRangeMinQty18", weightRangeMinQty18)
				.append("weightRangeMaxQty18", weightRangeMaxQty18)
				.append("weightRangeAverageWeight18",
						weightRangeAverageWeight18)
				.append("weightRangeMode18", weightRangeMode18)
				.append("weightRangeMinQty19", weightRangeMinQty19)
				.append("weightRangeMaxQty19", weightRangeMaxQty19)
				.append("weightRangeAverageWeight19",
						weightRangeAverageWeight19)
				.append("weightRangeMode19", weightRangeMode19)
				.append("weightRangeMinQty20", weightRangeMinQty20)
				.append("weightRangeMaxQty20", weightRangeMaxQty20)
				.append("weightRangeAverageWeight20",
						weightRangeAverageWeight20)
				.append("weightRangeMode20", weightRangeMode20)
				.append("weightRangeMinQty21", weightRangeMinQty21)
				.append("weightRangeMaxQty21", weightRangeMaxQty21)
				.append("weightRangeAverageWeight21",
						weightRangeAverageWeight21)
				.append("weightRangeMode21", weightRangeMode21)
				.append("weightRangeMinQty22", weightRangeMinQty22)
				.append("weightRangeMaxQty22", weightRangeMaxQty22)
				.append("weightRangeAverageWeight22",
						weightRangeAverageWeight22)
				.append("weightRangeMode22", weightRangeMode22)
				.append("weightRangeMinQty23", weightRangeMinQty23)
				.append("weightRangeMaxQty23", weightRangeMaxQty23)
				.append("weightRangeAverageWeight23",
						weightRangeAverageWeight23)
				.append("weightRangeMode23", weightRangeMode23)
				.append("weightRangeMinQty24", weightRangeMinQty24)
				.append("weightRangeMaxQty24", weightRangeMaxQty24)
				.append("weightRangeAverageWeight24",
						weightRangeAverageWeight24)
				.append("weightRangeMode24", weightRangeMode24)
				.append("weightRangeMinQty25", weightRangeMinQty25)
				.append("weightRangeMaxQty25", weightRangeMaxQty25)
				.append("weightRangeAverageWeight25",
						weightRangeAverageWeight25)
				.append("weightRangeMode25", weightRangeMode25)
				.append("assumptionServiceType1", assumptionServiceType1)
				.append("assumptionMode1", assumptionMode1)
				.append("assumptionAverageWeight1", assumptionAverageWeight1)
				.append("assumptionCrew1", assumptionCrew1)
				.append("assumptionServiceType2", assumptionServiceType2)
				.append("assumptionMode2", assumptionMode2)
				.append("assumptionAverageWeight2", assumptionAverageWeight2)
				.append("assumptionCrew2", assumptionCrew2)
				.append("assumptionServiceType3", assumptionServiceType3)
				.append("assumptionMode3", assumptionMode3)
				.append("assumptionAverageWeight3", assumptionAverageWeight3)
				.append("assumptionCrew3", assumptionCrew3)
				.append("assumptionServiceType4", assumptionServiceType4)
				.append("assumptionMode4", assumptionMode4)
				.append("assumptionAverageWeight4", assumptionAverageWeight4)
				.append("assumptionCrew4", assumptionCrew4)
				.append("assumptionServiceType5", assumptionServiceType5)
				.append("assumptionMode5", assumptionMode5)
				.append("assumptionAverageWeight5", assumptionAverageWeight5)
				.append("assumptionCrew5", assumptionCrew5)
				.append("assumptionServiceType6", assumptionServiceType6)
				.append("assumptionMode6", assumptionMode6)
				.append("assumptionAverageWeight6", assumptionAverageWeight6)
				.append("assumptionCrew6", assumptionCrew6)
				.append("assumptionServiceType7", assumptionServiceType7)
				.append("assumptionMode7", assumptionMode7)
				.append("assumptionAverageWeight7", assumptionAverageWeight7)
				.append("assumptionCrew7", assumptionCrew7)
				.append("adjustmentPhysicalCondition1",
						adjustmentPhysicalCondition1)
				.append("adjustmentPercentage1", adjustmentPercentage1)
				.append("adjustmentPhysicalCondition2",
						adjustmentPhysicalCondition2)
				.append("adjustmentPercentage2", adjustmentPercentage2)
				.append("adjustmentPhysicalCondition3",
						adjustmentPhysicalCondition3)
				.append("adjustmentPercentage3", adjustmentPercentage3)
				.append("adjustmentPhysicalCondition4",
						adjustmentPhysicalCondition4)
				.append("adjustmentPercentage4", adjustmentPercentage4)
				.append("adjustmentPhysicalCondition5",
						adjustmentPhysicalCondition5)
				.append("adjustmentPercentage5", adjustmentPercentage5)
				.append("adjustmentPhysicalCondition6",
						adjustmentPhysicalCondition6)
				.append("adjustmentPercentage6", adjustmentPercentage6)
				.toString();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getCrewGroup() {
		return crewGroup;
	}
	public void setCrewGroup(String crewGroup) {
		this.crewGroup = crewGroup;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public BigDecimal getWeightRangeMinQty1() {
		return weightRangeMinQty1;
	}
	public void setWeightRangeMinQty1(BigDecimal weightRangeMinQty1) {
		this.weightRangeMinQty1 = weightRangeMinQty1;
	}
	public BigDecimal getWeightRangeMaxQty1() {
		return weightRangeMaxQty1;
	}
	public void setWeightRangeMaxQty1(BigDecimal weightRangeMaxQty1) {
		this.weightRangeMaxQty1 = weightRangeMaxQty1;
	}
	public BigDecimal getWeightRangeAverageWeight1() {
		return weightRangeAverageWeight1;
	}
	public void setWeightRangeAverageWeight1(BigDecimal weightRangeAverageWeight1) {
		this.weightRangeAverageWeight1 = weightRangeAverageWeight1;
	}
	public String getWeightRangeMode1() {
		return weightRangeMode1;
	}
	public void setWeightRangeMode1(String weightRangeMode1) {
		this.weightRangeMode1 = weightRangeMode1;
	}
	public BigDecimal getWeightRangeMinQty2() {
		return weightRangeMinQty2;
	}
	public void setWeightRangeMinQty2(BigDecimal weightRangeMinQty2) {
		this.weightRangeMinQty2 = weightRangeMinQty2;
	}
	public BigDecimal getWeightRangeMaxQty2() {
		return weightRangeMaxQty2;
	}
	public void setWeightRangeMaxQty2(BigDecimal weightRangeMaxQty2) {
		this.weightRangeMaxQty2 = weightRangeMaxQty2;
	}
	public BigDecimal getWeightRangeAverageWeight2() {
		return weightRangeAverageWeight2;
	}
	public void setWeightRangeAverageWeight2(BigDecimal weightRangeAverageWeight2) {
		this.weightRangeAverageWeight2 = weightRangeAverageWeight2;
	}
	public String getWeightRangeMode2() {
		return weightRangeMode2;
	}
	public void setWeightRangeMode2(String weightRangeMode2) {
		this.weightRangeMode2 = weightRangeMode2;
	}
	public BigDecimal getWeightRangeMinQty3() {
		return weightRangeMinQty3;
	}
	public void setWeightRangeMinQty3(BigDecimal weightRangeMinQty3) {
		this.weightRangeMinQty3 = weightRangeMinQty3;
	}
	public BigDecimal getWeightRangeMaxQty3() {
		return weightRangeMaxQty3;
	}
	public void setWeightRangeMaxQty3(BigDecimal weightRangeMaxQty3) {
		this.weightRangeMaxQty3 = weightRangeMaxQty3;
	}
	public BigDecimal getWeightRangeAverageWeight3() {
		return weightRangeAverageWeight3;
	}
	public void setWeightRangeAverageWeight3(BigDecimal weightRangeAverageWeight3) {
		this.weightRangeAverageWeight3 = weightRangeAverageWeight3;
	}
	public String getWeightRangeMode3() {
		return weightRangeMode3;
	}
	public void setWeightRangeMode3(String weightRangeMode3) {
		this.weightRangeMode3 = weightRangeMode3;
	}
	public BigDecimal getWeightRangeMinQty4() {
		return weightRangeMinQty4;
	}
	public void setWeightRangeMinQty4(BigDecimal weightRangeMinQty4) {
		this.weightRangeMinQty4 = weightRangeMinQty4;
	}
	public BigDecimal getWeightRangeMaxQty4() {
		return weightRangeMaxQty4;
	}
	public void setWeightRangeMaxQty4(BigDecimal weightRangeMaxQty4) {
		this.weightRangeMaxQty4 = weightRangeMaxQty4;
	}
	public BigDecimal getWeightRangeAverageWeight4() {
		return weightRangeAverageWeight4;
	}
	public void setWeightRangeAverageWeight4(BigDecimal weightRangeAverageWeight4) {
		this.weightRangeAverageWeight4 = weightRangeAverageWeight4;
	}
	public String getWeightRangeMode4() {
		return weightRangeMode4;
	}
	public void setWeightRangeMode4(String weightRangeMode4) {
		this.weightRangeMode4 = weightRangeMode4;
	}
	public BigDecimal getWeightRangeMinQty5() {
		return weightRangeMinQty5;
	}
	public void setWeightRangeMinQty5(BigDecimal weightRangeMinQty5) {
		this.weightRangeMinQty5 = weightRangeMinQty5;
	}
	public BigDecimal getWeightRangeMaxQty5() {
		return weightRangeMaxQty5;
	}
	public void setWeightRangeMaxQty5(BigDecimal weightRangeMaxQty5) {
		this.weightRangeMaxQty5 = weightRangeMaxQty5;
	}
	public BigDecimal getWeightRangeAverageWeight5() {
		return weightRangeAverageWeight5;
	}
	public void setWeightRangeAverageWeight5(BigDecimal weightRangeAverageWeight5) {
		this.weightRangeAverageWeight5 = weightRangeAverageWeight5;
	}
	public String getWeightRangeMode5() {
		return weightRangeMode5;
	}
	public void setWeightRangeMode5(String weightRangeMode5) {
		this.weightRangeMode5 = weightRangeMode5;
	}
	public BigDecimal getWeightRangeMinQty6() {
		return weightRangeMinQty6;
	}
	public void setWeightRangeMinQty6(BigDecimal weightRangeMinQty6) {
		this.weightRangeMinQty6 = weightRangeMinQty6;
	}
	public BigDecimal getWeightRangeMaxQty6() {
		return weightRangeMaxQty6;
	}
	public void setWeightRangeMaxQty6(BigDecimal weightRangeMaxQty6) {
		this.weightRangeMaxQty6 = weightRangeMaxQty6;
	}
	public BigDecimal getWeightRangeAverageWeight6() {
		return weightRangeAverageWeight6;
	}
	public void setWeightRangeAverageWeight6(BigDecimal weightRangeAverageWeight6) {
		this.weightRangeAverageWeight6 = weightRangeAverageWeight6;
	}
	public String getWeightRangeMode6() {
		return weightRangeMode6;
	}
	public void setWeightRangeMode6(String weightRangeMode6) {
		this.weightRangeMode6 = weightRangeMode6;
	}
	public BigDecimal getWeightRangeMinQty7() {
		return weightRangeMinQty7;
	}
	public void setWeightRangeMinQty7(BigDecimal weightRangeMinQty7) {
		this.weightRangeMinQty7 = weightRangeMinQty7;
	}
	public BigDecimal getWeightRangeMaxQty7() {
		return weightRangeMaxQty7;
	}
	public void setWeightRangeMaxQty7(BigDecimal weightRangeMaxQty7) {
		this.weightRangeMaxQty7 = weightRangeMaxQty7;
	}
	public BigDecimal getWeightRangeAverageWeight7() {
		return weightRangeAverageWeight7;
	}
	public void setWeightRangeAverageWeight7(BigDecimal weightRangeAverageWeight7) {
		this.weightRangeAverageWeight7 = weightRangeAverageWeight7;
	}
	public String getWeightRangeMode7() {
		return weightRangeMode7;
	}
	public void setWeightRangeMode7(String weightRangeMode7) {
		this.weightRangeMode7 = weightRangeMode7;
	}
	public BigDecimal getWeightRangeMinQty8() {
		return weightRangeMinQty8;
	}
	public void setWeightRangeMinQty8(BigDecimal weightRangeMinQty8) {
		this.weightRangeMinQty8 = weightRangeMinQty8;
	}
	public BigDecimal getWeightRangeMaxQty8() {
		return weightRangeMaxQty8;
	}
	public void setWeightRangeMaxQty8(BigDecimal weightRangeMaxQty8) {
		this.weightRangeMaxQty8 = weightRangeMaxQty8;
	}
	public BigDecimal getWeightRangeAverageWeight8() {
		return weightRangeAverageWeight8;
	}
	public void setWeightRangeAverageWeight8(BigDecimal weightRangeAverageWeight8) {
		this.weightRangeAverageWeight8 = weightRangeAverageWeight8;
	}
	public String getWeightRangeMode8() {
		return weightRangeMode8;
	}
	public void setWeightRangeMode8(String weightRangeMode8) {
		this.weightRangeMode8 = weightRangeMode8;
	}
	public BigDecimal getWeightRangeMinQty9() {
		return weightRangeMinQty9;
	}
	public void setWeightRangeMinQty9(BigDecimal weightRangeMinQty9) {
		this.weightRangeMinQty9 = weightRangeMinQty9;
	}
	public BigDecimal getWeightRangeMaxQty9() {
		return weightRangeMaxQty9;
	}
	public void setWeightRangeMaxQty9(BigDecimal weightRangeMaxQty9) {
		this.weightRangeMaxQty9 = weightRangeMaxQty9;
	}
	public BigDecimal getWeightRangeAverageWeight9() {
		return weightRangeAverageWeight9;
	}
	public void setWeightRangeAverageWeight9(BigDecimal weightRangeAverageWeight9) {
		this.weightRangeAverageWeight9 = weightRangeAverageWeight9;
	}
	public String getWeightRangeMode9() {
		return weightRangeMode9;
	}
	public void setWeightRangeMode9(String weightRangeMode9) {
		this.weightRangeMode9 = weightRangeMode9;
	}
	public BigDecimal getWeightRangeMinQty10() {
		return weightRangeMinQty10;
	}
	public void setWeightRangeMinQty10(BigDecimal weightRangeMinQty10) {
		this.weightRangeMinQty10 = weightRangeMinQty10;
	}
	public BigDecimal getWeightRangeMaxQty10() {
		return weightRangeMaxQty10;
	}
	public void setWeightRangeMaxQty10(BigDecimal weightRangeMaxQty10) {
		this.weightRangeMaxQty10 = weightRangeMaxQty10;
	}
	public BigDecimal getWeightRangeAverageWeight10() {
		return weightRangeAverageWeight10;
	}
	public void setWeightRangeAverageWeight10(BigDecimal weightRangeAverageWeight10) {
		this.weightRangeAverageWeight10 = weightRangeAverageWeight10;
	}
	public String getWeightRangeMode10() {
		return weightRangeMode10;
	}
	public void setWeightRangeMode10(String weightRangeMode10) {
		this.weightRangeMode10 = weightRangeMode10;
	}
	public BigDecimal getWeightRangeMinQty11() {
		return weightRangeMinQty11;
	}
	public void setWeightRangeMinQty11(BigDecimal weightRangeMinQty11) {
		this.weightRangeMinQty11 = weightRangeMinQty11;
	}
	public BigDecimal getWeightRangeMaxQty11() {
		return weightRangeMaxQty11;
	}
	public void setWeightRangeMaxQty11(BigDecimal weightRangeMaxQty11) {
		this.weightRangeMaxQty11 = weightRangeMaxQty11;
	}
	public BigDecimal getWeightRangeAverageWeight11() {
		return weightRangeAverageWeight11;
	}
	public void setWeightRangeAverageWeight11(BigDecimal weightRangeAverageWeight11) {
		this.weightRangeAverageWeight11 = weightRangeAverageWeight11;
	}
	public String getWeightRangeMode11() {
		return weightRangeMode11;
	}
	public void setWeightRangeMode11(String weightRangeMode11) {
		this.weightRangeMode11 = weightRangeMode11;
	}
	public BigDecimal getWeightRangeMinQty12() {
		return weightRangeMinQty12;
	}
	public void setWeightRangeMinQty12(BigDecimal weightRangeMinQty12) {
		this.weightRangeMinQty12 = weightRangeMinQty12;
	}
	public BigDecimal getWeightRangeMaxQty12() {
		return weightRangeMaxQty12;
	}
	public void setWeightRangeMaxQty12(BigDecimal weightRangeMaxQty12) {
		this.weightRangeMaxQty12 = weightRangeMaxQty12;
	}
	public BigDecimal getWeightRangeAverageWeight12() {
		return weightRangeAverageWeight12;
	}
	public void setWeightRangeAverageWeight12(BigDecimal weightRangeAverageWeight12) {
		this.weightRangeAverageWeight12 = weightRangeAverageWeight12;
	}
	public String getWeightRangeMode12() {
		return weightRangeMode12;
	}
	public void setWeightRangeMode12(String weightRangeMode12) {
		this.weightRangeMode12 = weightRangeMode12;
	}
	public BigDecimal getWeightRangeMinQty13() {
		return weightRangeMinQty13;
	}
	public void setWeightRangeMinQty13(BigDecimal weightRangeMinQty13) {
		this.weightRangeMinQty13 = weightRangeMinQty13;
	}
	public BigDecimal getWeightRangeMaxQty13() {
		return weightRangeMaxQty13;
	}
	public void setWeightRangeMaxQty13(BigDecimal weightRangeMaxQty13) {
		this.weightRangeMaxQty13 = weightRangeMaxQty13;
	}
	public BigDecimal getWeightRangeAverageWeight13() {
		return weightRangeAverageWeight13;
	}
	public void setWeightRangeAverageWeight13(BigDecimal weightRangeAverageWeight13) {
		this.weightRangeAverageWeight13 = weightRangeAverageWeight13;
	}
	public String getWeightRangeMode13() {
		return weightRangeMode13;
	}
	public void setWeightRangeMode13(String weightRangeMode13) {
		this.weightRangeMode13 = weightRangeMode13;
	}
	public BigDecimal getWeightRangeMinQty14() {
		return weightRangeMinQty14;
	}
	public void setWeightRangeMinQty14(BigDecimal weightRangeMinQty14) {
		this.weightRangeMinQty14 = weightRangeMinQty14;
	}
	public BigDecimal getWeightRangeMaxQty14() {
		return weightRangeMaxQty14;
	}
	public void setWeightRangeMaxQty14(BigDecimal weightRangeMaxQty14) {
		this.weightRangeMaxQty14 = weightRangeMaxQty14;
	}
	public BigDecimal getWeightRangeAverageWeight14() {
		return weightRangeAverageWeight14;
	}
	public void setWeightRangeAverageWeight14(BigDecimal weightRangeAverageWeight14) {
		this.weightRangeAverageWeight14 = weightRangeAverageWeight14;
	}
	public String getWeightRangeMode14() {
		return weightRangeMode14;
	}
	public void setWeightRangeMode14(String weightRangeMode14) {
		this.weightRangeMode14 = weightRangeMode14;
	}
	public BigDecimal getWeightRangeMinQty15() {
		return weightRangeMinQty15;
	}
	public void setWeightRangeMinQty15(BigDecimal weightRangeMinQty15) {
		this.weightRangeMinQty15 = weightRangeMinQty15;
	}
	public BigDecimal getWeightRangeMaxQty15() {
		return weightRangeMaxQty15;
	}
	public void setWeightRangeMaxQty15(BigDecimal weightRangeMaxQty15) {
		this.weightRangeMaxQty15 = weightRangeMaxQty15;
	}
	public BigDecimal getWeightRangeAverageWeight15() {
		return weightRangeAverageWeight15;
	}
	public void setWeightRangeAverageWeight15(BigDecimal weightRangeAverageWeight15) {
		this.weightRangeAverageWeight15 = weightRangeAverageWeight15;
	}
	public String getWeightRangeMode15() {
		return weightRangeMode15;
	}
	public void setWeightRangeMode15(String weightRangeMode15) {
		this.weightRangeMode15 = weightRangeMode15;
	}
	public BigDecimal getWeightRangeMinQty16() {
		return weightRangeMinQty16;
	}
	public void setWeightRangeMinQty16(BigDecimal weightRangeMinQty16) {
		this.weightRangeMinQty16 = weightRangeMinQty16;
	}
	public BigDecimal getWeightRangeMaxQty16() {
		return weightRangeMaxQty16;
	}
	public void setWeightRangeMaxQty16(BigDecimal weightRangeMaxQty16) {
		this.weightRangeMaxQty16 = weightRangeMaxQty16;
	}
	public BigDecimal getWeightRangeAverageWeight16() {
		return weightRangeAverageWeight16;
	}
	public void setWeightRangeAverageWeight16(BigDecimal weightRangeAverageWeight16) {
		this.weightRangeAverageWeight16 = weightRangeAverageWeight16;
	}
	public String getWeightRangeMode16() {
		return weightRangeMode16;
	}
	public void setWeightRangeMode16(String weightRangeMode16) {
		this.weightRangeMode16 = weightRangeMode16;
	}
	public BigDecimal getWeightRangeMinQty17() {
		return weightRangeMinQty17;
	}
	public void setWeightRangeMinQty17(BigDecimal weightRangeMinQty17) {
		this.weightRangeMinQty17 = weightRangeMinQty17;
	}
	public BigDecimal getWeightRangeMaxQty17() {
		return weightRangeMaxQty17;
	}
	public void setWeightRangeMaxQty17(BigDecimal weightRangeMaxQty17) {
		this.weightRangeMaxQty17 = weightRangeMaxQty17;
	}
	public BigDecimal getWeightRangeAverageWeight17() {
		return weightRangeAverageWeight17;
	}
	public void setWeightRangeAverageWeight17(BigDecimal weightRangeAverageWeight17) {
		this.weightRangeAverageWeight17 = weightRangeAverageWeight17;
	}
	public String getWeightRangeMode17() {
		return weightRangeMode17;
	}
	public void setWeightRangeMode17(String weightRangeMode17) {
		this.weightRangeMode17 = weightRangeMode17;
	}
	public BigDecimal getWeightRangeMinQty18() {
		return weightRangeMinQty18;
	}
	public void setWeightRangeMinQty18(BigDecimal weightRangeMinQty18) {
		this.weightRangeMinQty18 = weightRangeMinQty18;
	}
	public BigDecimal getWeightRangeMaxQty18() {
		return weightRangeMaxQty18;
	}
	public void setWeightRangeMaxQty18(BigDecimal weightRangeMaxQty18) {
		this.weightRangeMaxQty18 = weightRangeMaxQty18;
	}
	public BigDecimal getWeightRangeAverageWeight18() {
		return weightRangeAverageWeight18;
	}
	public void setWeightRangeAverageWeight18(BigDecimal weightRangeAverageWeight18) {
		this.weightRangeAverageWeight18 = weightRangeAverageWeight18;
	}
	public String getWeightRangeMode18() {
		return weightRangeMode18;
	}
	public void setWeightRangeMode18(String weightRangeMode18) {
		this.weightRangeMode18 = weightRangeMode18;
	}
	public BigDecimal getWeightRangeMinQty19() {
		return weightRangeMinQty19;
	}
	public void setWeightRangeMinQty19(BigDecimal weightRangeMinQty19) {
		this.weightRangeMinQty19 = weightRangeMinQty19;
	}
	public BigDecimal getWeightRangeMaxQty19() {
		return weightRangeMaxQty19;
	}
	public void setWeightRangeMaxQty19(BigDecimal weightRangeMaxQty19) {
		this.weightRangeMaxQty19 = weightRangeMaxQty19;
	}
	public BigDecimal getWeightRangeAverageWeight19() {
		return weightRangeAverageWeight19;
	}
	public void setWeightRangeAverageWeight19(BigDecimal weightRangeAverageWeight19) {
		this.weightRangeAverageWeight19 = weightRangeAverageWeight19;
	}
	public String getWeightRangeMode19() {
		return weightRangeMode19;
	}
	public void setWeightRangeMode19(String weightRangeMode19) {
		this.weightRangeMode19 = weightRangeMode19;
	}
	public BigDecimal getWeightRangeMinQty20() {
		return weightRangeMinQty20;
	}
	public void setWeightRangeMinQty20(BigDecimal weightRangeMinQty20) {
		this.weightRangeMinQty20 = weightRangeMinQty20;
	}
	public BigDecimal getWeightRangeMaxQty20() {
		return weightRangeMaxQty20;
	}
	public void setWeightRangeMaxQty20(BigDecimal weightRangeMaxQty20) {
		this.weightRangeMaxQty20 = weightRangeMaxQty20;
	}
	public BigDecimal getWeightRangeAverageWeight20() {
		return weightRangeAverageWeight20;
	}
	public void setWeightRangeAverageWeight20(BigDecimal weightRangeAverageWeight20) {
		this.weightRangeAverageWeight20 = weightRangeAverageWeight20;
	}
	public String getWeightRangeMode20() {
		return weightRangeMode20;
	}
	public void setWeightRangeMode20(String weightRangeMode20) {
		this.weightRangeMode20 = weightRangeMode20;
	}
	public BigDecimal getWeightRangeMinQty21() {
		return weightRangeMinQty21;
	}
	public void setWeightRangeMinQty21(BigDecimal weightRangeMinQty21) {
		this.weightRangeMinQty21 = weightRangeMinQty21;
	}
	public BigDecimal getWeightRangeMaxQty21() {
		return weightRangeMaxQty21;
	}
	public void setWeightRangeMaxQty21(BigDecimal weightRangeMaxQty21) {
		this.weightRangeMaxQty21 = weightRangeMaxQty21;
	}
	public BigDecimal getWeightRangeAverageWeight21() {
		return weightRangeAverageWeight21;
	}
	public void setWeightRangeAverageWeight21(BigDecimal weightRangeAverageWeight21) {
		this.weightRangeAverageWeight21 = weightRangeAverageWeight21;
	}
	public String getWeightRangeMode21() {
		return weightRangeMode21;
	}
	public void setWeightRangeMode21(String weightRangeMode21) {
		this.weightRangeMode21 = weightRangeMode21;
	}
	public BigDecimal getWeightRangeMinQty22() {
		return weightRangeMinQty22;
	}
	public void setWeightRangeMinQty22(BigDecimal weightRangeMinQty22) {
		this.weightRangeMinQty22 = weightRangeMinQty22;
	}
	public BigDecimal getWeightRangeMaxQty22() {
		return weightRangeMaxQty22;
	}
	public void setWeightRangeMaxQty22(BigDecimal weightRangeMaxQty22) {
		this.weightRangeMaxQty22 = weightRangeMaxQty22;
	}
	public BigDecimal getWeightRangeAverageWeight22() {
		return weightRangeAverageWeight22;
	}
	public void setWeightRangeAverageWeight22(BigDecimal weightRangeAverageWeight22) {
		this.weightRangeAverageWeight22 = weightRangeAverageWeight22;
	}
	public String getWeightRangeMode22() {
		return weightRangeMode22;
	}
	public void setWeightRangeMode22(String weightRangeMode22) {
		this.weightRangeMode22 = weightRangeMode22;
	}
	public BigDecimal getWeightRangeMinQty23() {
		return weightRangeMinQty23;
	}
	public void setWeightRangeMinQty23(BigDecimal weightRangeMinQty23) {
		this.weightRangeMinQty23 = weightRangeMinQty23;
	}
	public BigDecimal getWeightRangeMaxQty23() {
		return weightRangeMaxQty23;
	}
	public void setWeightRangeMaxQty23(BigDecimal weightRangeMaxQty23) {
		this.weightRangeMaxQty23 = weightRangeMaxQty23;
	}
	public BigDecimal getWeightRangeAverageWeight23() {
		return weightRangeAverageWeight23;
	}
	public void setWeightRangeAverageWeight23(BigDecimal weightRangeAverageWeight23) {
		this.weightRangeAverageWeight23 = weightRangeAverageWeight23;
	}
	public String getWeightRangeMode23() {
		return weightRangeMode23;
	}
	public void setWeightRangeMode23(String weightRangeMode23) {
		this.weightRangeMode23 = weightRangeMode23;
	}
	public BigDecimal getWeightRangeMinQty24() {
		return weightRangeMinQty24;
	}
	public void setWeightRangeMinQty24(BigDecimal weightRangeMinQty24) {
		this.weightRangeMinQty24 = weightRangeMinQty24;
	}
	public BigDecimal getWeightRangeMaxQty24() {
		return weightRangeMaxQty24;
	}
	public void setWeightRangeMaxQty24(BigDecimal weightRangeMaxQty24) {
		this.weightRangeMaxQty24 = weightRangeMaxQty24;
	}
	public BigDecimal getWeightRangeAverageWeight24() {
		return weightRangeAverageWeight24;
	}
	public void setWeightRangeAverageWeight24(BigDecimal weightRangeAverageWeight24) {
		this.weightRangeAverageWeight24 = weightRangeAverageWeight24;
	}
	public String getWeightRangeMode24() {
		return weightRangeMode24;
	}
	public void setWeightRangeMode24(String weightRangeMode24) {
		this.weightRangeMode24 = weightRangeMode24;
	}
	public BigDecimal getWeightRangeMinQty25() {
		return weightRangeMinQty25;
	}
	public void setWeightRangeMinQty25(BigDecimal weightRangeMinQty25) {
		this.weightRangeMinQty25 = weightRangeMinQty25;
	}
	public BigDecimal getWeightRangeMaxQty25() {
		return weightRangeMaxQty25;
	}
	public void setWeightRangeMaxQty25(BigDecimal weightRangeMaxQty25) {
		this.weightRangeMaxQty25 = weightRangeMaxQty25;
	}
	public BigDecimal getWeightRangeAverageWeight25() {
		return weightRangeAverageWeight25;
	}
	public void setWeightRangeAverageWeight25(BigDecimal weightRangeAverageWeight25) {
		this.weightRangeAverageWeight25 = weightRangeAverageWeight25;
	}
	public String getWeightRangeMode25() {
		return weightRangeMode25;
	}
	public void setWeightRangeMode25(String weightRangeMode25) {
		this.weightRangeMode25 = weightRangeMode25;
	}
	public String getAssumptionServiceType1() {
		return assumptionServiceType1;
	}
	public void setAssumptionServiceType1(String assumptionServiceType1) {
		this.assumptionServiceType1 = assumptionServiceType1;
	}
	public String getAssumptionMode1() {
		return assumptionMode1;
	}
	public void setAssumptionMode1(String assumptionMode1) {
		this.assumptionMode1 = assumptionMode1;
	}
	public BigDecimal getAssumptionAverageWeight1() {
		return assumptionAverageWeight1;
	}
	public void setAssumptionAverageWeight1(BigDecimal assumptionAverageWeight1) {
		this.assumptionAverageWeight1 = assumptionAverageWeight1;
	}
	public String getAssumptionCrew1() {
		return assumptionCrew1;
	}
	public void setAssumptionCrew1(String assumptionCrew1) {
		this.assumptionCrew1 = assumptionCrew1;
	}
	public String getAssumptionServiceType2() {
		return assumptionServiceType2;
	}
	public void setAssumptionServiceType2(String assumptionServiceType2) {
		this.assumptionServiceType2 = assumptionServiceType2;
	}
	public String getAssumptionMode2() {
		return assumptionMode2;
	}
	public void setAssumptionMode2(String assumptionMode2) {
		this.assumptionMode2 = assumptionMode2;
	}
	public BigDecimal getAssumptionAverageWeight2() {
		return assumptionAverageWeight2;
	}
	public void setAssumptionAverageWeight2(BigDecimal assumptionAverageWeight2) {
		this.assumptionAverageWeight2 = assumptionAverageWeight2;
	}
	public String getAssumptionCrew2() {
		return assumptionCrew2;
	}
	public void setAssumptionCrew2(String assumptionCrew2) {
		this.assumptionCrew2 = assumptionCrew2;
	}
	public String getAssumptionServiceType3() {
		return assumptionServiceType3;
	}
	public void setAssumptionServiceType3(String assumptionServiceType3) {
		this.assumptionServiceType3 = assumptionServiceType3;
	}
	public String getAssumptionMode3() {
		return assumptionMode3;
	}
	public void setAssumptionMode3(String assumptionMode3) {
		this.assumptionMode3 = assumptionMode3;
	}
	public BigDecimal getAssumptionAverageWeight3() {
		return assumptionAverageWeight3;
	}
	public void setAssumptionAverageWeight3(BigDecimal assumptionAverageWeight3) {
		this.assumptionAverageWeight3 = assumptionAverageWeight3;
	}
	public String getAssumptionCrew3() {
		return assumptionCrew3;
	}
	public void setAssumptionCrew3(String assumptionCrew3) {
		this.assumptionCrew3 = assumptionCrew3;
	}
	public String getAssumptionServiceType4() {
		return assumptionServiceType4;
	}
	public void setAssumptionServiceType4(String assumptionServiceType4) {
		this.assumptionServiceType4 = assumptionServiceType4;
	}
	public String getAssumptionMode4() {
		return assumptionMode4;
	}
	public void setAssumptionMode4(String assumptionMode4) {
		this.assumptionMode4 = assumptionMode4;
	}
	public BigDecimal getAssumptionAverageWeight4() {
		return assumptionAverageWeight4;
	}
	public void setAssumptionAverageWeight4(BigDecimal assumptionAverageWeight4) {
		this.assumptionAverageWeight4 = assumptionAverageWeight4;
	}
	public String getAssumptionCrew4() {
		return assumptionCrew4;
	}
	public void setAssumptionCrew4(String assumptionCrew4) {
		this.assumptionCrew4 = assumptionCrew4;
	}
	public String getAssumptionServiceType5() {
		return assumptionServiceType5;
	}
	public void setAssumptionServiceType5(String assumptionServiceType5) {
		this.assumptionServiceType5 = assumptionServiceType5;
	}
	public String getAssumptionMode5() {
		return assumptionMode5;
	}
	public void setAssumptionMode5(String assumptionMode5) {
		this.assumptionMode5 = assumptionMode5;
	}
	public BigDecimal getAssumptionAverageWeight5() {
		return assumptionAverageWeight5;
	}
	public void setAssumptionAverageWeight5(BigDecimal assumptionAverageWeight5) {
		this.assumptionAverageWeight5 = assumptionAverageWeight5;
	}
	public String getAssumptionCrew5() {
		return assumptionCrew5;
	}
	public void setAssumptionCrew5(String assumptionCrew5) {
		this.assumptionCrew5 = assumptionCrew5;
	}
	public String getAssumptionServiceType6() {
		return assumptionServiceType6;
	}
	public void setAssumptionServiceType6(String assumptionServiceType6) {
		this.assumptionServiceType6 = assumptionServiceType6;
	}
	public String getAssumptionMode6() {
		return assumptionMode6;
	}
	public void setAssumptionMode6(String assumptionMode6) {
		this.assumptionMode6 = assumptionMode6;
	}
	public BigDecimal getAssumptionAverageWeight6() {
		return assumptionAverageWeight6;
	}
	public void setAssumptionAverageWeight6(BigDecimal assumptionAverageWeight6) {
		this.assumptionAverageWeight6 = assumptionAverageWeight6;
	}
	public String getAssumptionCrew6() {
		return assumptionCrew6;
	}
	public void setAssumptionCrew6(String assumptionCrew6) {
		this.assumptionCrew6 = assumptionCrew6;
	}
	public String getAssumptionServiceType7() {
		return assumptionServiceType7;
	}
	public void setAssumptionServiceType7(String assumptionServiceType7) {
		this.assumptionServiceType7 = assumptionServiceType7;
	}
	public String getAssumptionMode7() {
		return assumptionMode7;
	}
	public void setAssumptionMode7(String assumptionMode7) {
		this.assumptionMode7 = assumptionMode7;
	}
	public BigDecimal getAssumptionAverageWeight7() {
		return assumptionAverageWeight7;
	}
	public void setAssumptionAverageWeight7(BigDecimal assumptionAverageWeight7) {
		this.assumptionAverageWeight7 = assumptionAverageWeight7;
	}
	public String getAssumptionCrew7() {
		return assumptionCrew7;
	}
	public void setAssumptionCrew7(String assumptionCrew7) {
		this.assumptionCrew7 = assumptionCrew7;
	}
	public String getAdjustmentPhysicalCondition1() {
		return adjustmentPhysicalCondition1;
	}
	public void setAdjustmentPhysicalCondition1(String adjustmentPhysicalCondition1) {
		this.adjustmentPhysicalCondition1 = adjustmentPhysicalCondition1;
	}
	public String getAdjustmentPercentage1() {
		return adjustmentPercentage1;
	}
	public void setAdjustmentPercentage1(String adjustmentPercentage1) {
		this.adjustmentPercentage1 = adjustmentPercentage1;
	}
	public String getAdjustmentPhysicalCondition2() {
		return adjustmentPhysicalCondition2;
	}
	public void setAdjustmentPhysicalCondition2(String adjustmentPhysicalCondition2) {
		this.adjustmentPhysicalCondition2 = adjustmentPhysicalCondition2;
	}
	public String getAdjustmentPercentage2() {
		return adjustmentPercentage2;
	}
	public void setAdjustmentPercentage2(String adjustmentPercentage2) {
		this.adjustmentPercentage2 = adjustmentPercentage2;
	}
	public String getAdjustmentPhysicalCondition3() {
		return adjustmentPhysicalCondition3;
	}
	public void setAdjustmentPhysicalCondition3(String adjustmentPhysicalCondition3) {
		this.adjustmentPhysicalCondition3 = adjustmentPhysicalCondition3;
	}
	public String getAdjustmentPercentage3() {
		return adjustmentPercentage3;
	}
	public void setAdjustmentPercentage3(String adjustmentPercentage3) {
		this.adjustmentPercentage3 = adjustmentPercentage3;
	}
	public String getAdjustmentPhysicalCondition4() {
		return adjustmentPhysicalCondition4;
	}
	public void setAdjustmentPhysicalCondition4(String adjustmentPhysicalCondition4) {
		this.adjustmentPhysicalCondition4 = adjustmentPhysicalCondition4;
	}
	public String getAdjustmentPercentage4() {
		return adjustmentPercentage4;
	}
	public void setAdjustmentPercentage4(String adjustmentPercentage4) {
		this.adjustmentPercentage4 = adjustmentPercentage4;
	}
	public String getAdjustmentPhysicalCondition5() {
		return adjustmentPhysicalCondition5;
	}
	public void setAdjustmentPhysicalCondition5(String adjustmentPhysicalCondition5) {
		this.adjustmentPhysicalCondition5 = adjustmentPhysicalCondition5;
	}
	public String getAdjustmentPercentage5() {
		return adjustmentPercentage5;
	}
	public void setAdjustmentPercentage5(String adjustmentPercentage5) {
		this.adjustmentPercentage5 = adjustmentPercentage5;
	}
	public String getAdjustmentPhysicalCondition6() {
		return adjustmentPhysicalCondition6;
	}
	public void setAdjustmentPhysicalCondition6(String adjustmentPhysicalCondition6) {
		this.adjustmentPhysicalCondition6 = adjustmentPhysicalCondition6;
	}
	public String getAdjustmentPercentage6() {
		return adjustmentPercentage6;
	}
	public void setAdjustmentPercentage6(String adjustmentPercentage6) {
		this.adjustmentPercentage6 = adjustmentPercentage6;
	}	
	
}
