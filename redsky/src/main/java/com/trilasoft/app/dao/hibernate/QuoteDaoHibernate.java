package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.QuoteDao;
import com.trilasoft.app.dao.hibernate.dto.SecorShipmentDTO;
import com.trilasoft.app.model.CustomerInformation;
import com.trilasoft.app.model.Quote;
import com.trilasoft.app.model.QuoteItem;
import com.trilasoft.app.model.QuoteRate;
import com.trilasoft.app.model.SecorQuote;

public class QuoteDaoHibernate extends GenericDaoHibernate<Quote, Long> implements QuoteDao {   
  
    public QuoteDaoHibernate() {   
        super(Quote.class);   
    }   
    
    public List<Quote> findBySequenceNumber(Long sequenceNumber){
    	return getHibernateTemplate().find("from Quote where sequenceNumber=?", sequenceNumber);
    }

	public List searchQuoteDetailList(String firstName, String lastName,Date createdOn, String originCity, String originCountry,String destinationCity, String destinationCountry, String sessionCorpID) { 
		String Query;
		if(createdOn !=null){
		    SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		    StringBuilder formatedDate = new StringBuilder( dateformatYYYYMMDD1.format(createdOn) );
		    Query="select c.quoteId as quoteId,c.firstName,c.lastName,s.createdOn,s.originCity,s.originCountry,s.destinationCity,s.destinationCountry from secorquote s,customerinformation c " +
		  		"where c.quoteId=s.id AND c.firstName like '" + firstName.replaceAll("'", "''").replaceAll(":", "''")+"%' " +
		  		"AND c.lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''")+"%' " +
		  				"AND s.createdOn like '"+formatedDate+"%' " +
		  						"AND s.originCity like '"+originCity+"%' " +
		  								"AND s.originCountry like '"+originCountry+"%' " +
		  										"AND s.destinationCity like '"+destinationCity+"%' " +
		  												"AND s.destinationCountry like '"+destinationCountry+"%' " +
		  														"AND s.corpId='"+sessionCorpID+"' ";
		}else{
			Query="select c.quoteId as quoteId,c.firstName,c.lastName,s.createdOn,s.originCity,s.originCountry,s.destinationCity,s.destinationCountry from secorquote s,customerinformation c " +
	  		"where c.quoteId=s.id AND c.firstName like '" + firstName.replaceAll("'", "''").replaceAll(":", "''")+"%' " +
	  		"AND c.lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''")+"%' " +
	  						"AND s.originCity like '"+originCity.replaceAll("'", "''").replaceAll(":", "''")+"%' " +
	  								"AND s.originCountry like '"+originCountry+"%' " +
	  										"AND s.destinationCity like '"+destinationCity.replaceAll("'", "''").replaceAll(":", "''")+"%' " +
	  												"AND s.destinationCountry like '"+destinationCountry+"%' " +
	  														"AND s.corpId='"+sessionCorpID+"' ";
		}
		  List list= getSession().createSQLQuery(Query).list();
		  List result= new ArrayList(); 
		  
			try
			{
				Iterator it=list.iterator();
				while(it.hasNext()) 
				{ 
					SecorShipmentDTO shipmentDTO = new SecorShipmentDTO();
					Object []obj=(Object[]) it.next();
					shipmentDTO.setQuoteId(obj[0]);
					shipmentDTO.setFirstName(obj[1]);
					shipmentDTO.setLastName(obj[2]);
					shipmentDTO.setCreatedOn(obj[3]);
					shipmentDTO.setOriginCity(obj[4]);
					shipmentDTO.setOriginCountry(obj[5]);
					shipmentDTO.setDestinationCity(obj[6]);
					shipmentDTO.setDestinationCountry(obj[7]);
					result.add(shipmentDTO);
				}
			}
			catch(Exception ex)
			{
				
			}
		
		return result;
	}

	public List<SecorQuote> findSecorQuoteByQuoteId(String sessionCorpID, Long quoteId) {
		return getHibernateTemplate().find("from SecorQuote where id='"+quoteId+"'");
	}

	public List<CustomerInformation> findCustomerInformationByQuoteId(String sessionCorpID, Long quoteId) {
		return getHibernateTemplate().find("from CustomerInformation where quoteId='"+quoteId+"'");
	}

	public List<QuoteItem> findQuoteItemByQuoteId(String sessionCorpID, Long quoteId) {
		return getHibernateTemplate().find("from QuoteItem where quoteId='"+quoteId+"'");
	}

	public List<QuoteRate> findQuoteRateByQuoteId(String sessionCorpID,	Long quoteId) {
		return getHibernateTemplate().find("from QuoteRate where quoteId='"+quoteId+"' and selectedRate is true");
	}

	
}