package com.trilasoft.app.service.impl;

import java.util.Date;

public class UpdateRuleExpressionBuilder {

	public static String getFromClause(String entity,String sessionCorpID,String baseExpression) {
		String fromClause = entity.toLowerCase();
						fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId ";	
		return fromClause;
	}

	public static String getAssignedToList(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile") || entity.equals("QuotationFile")) {
			fromClause = "customerfile.coordinator";
		} else{
			fromClause = "serviceorder.coordinator";
		}
		return fromClause;
	}
	
	public static String getFileNumber(String entity) {
		String fromClause = entity;
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous")) {
			fromClause = "serviceorder.shipNumber";
		} else if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.sequenceNumber";
		} else if (entity.equals("WorkTicket")) {
			fromClause = "workticket.ticket";
		} else if (entity.equals("QuotationFile")) {
			fromClause = "customerfile.sequenceNumber";
		}else if (entity.equals("Claim")) {
			fromClause = "claim.claimNumber";
		}else {
			fromClause = "serviceorder.shipNumber";
		}
		return fromClause;
	}

	public static String getBilling(String entity) {
		String fromClause = entity;
		if (entity.equals("QuotationFile") || entity.equals("CustomerFile")) {
			fromClause = "customerfile.personBilling";
		}else{
			fromClause = "billing.personBilling";
		}
		return fromClause;
	}
	
	public static String getOrginAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originAgentCode";
		}
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.originAgentCode";
		}
		return fromClause;
	}
	public static String getDestinationAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationAgentCode";
		}
		return fromClause;
	}
	
	public static String getForwarderAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") ) {
			fromClause = "trackingstatus.forwarderCode";
		}
		return fromClause;
	}
	public static String getBrokerAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.brokerCode";
		}
		return fromClause;
	}
	public static String getOrginSubAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originSubAgentCode";
		}
		return fromClause;
	}
	public static String getDestinationSubAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationSubAgentCode";
		}
		return fromClause;
	}

	public static String getOrginAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originAgent";
		}
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.originAgentName";
		}
		return fromClause;
	}
	public static String getDestinationAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationAgent";
		}
		return fromClause;
	}
	
	public static String getForwarderAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") ) {
			fromClause = "trackingstatus.forwarder";
		}
		return fromClause;
	}
	public static String getBrokerAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.brokerName";
		}
		return fromClause;
	}
	public static String getOrginSubAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.originSubAgent";
		}
		return fromClause;
	}
	public static String getDestinationSubAgentName(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("ServicePartner")) {
			fromClause = "trackingstatus.destinationSubAgent";
		}
		return fromClause;
	}
	
	public static String getAuditor(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile") || entity.equals("QuotationFile")) {
			fromClause = "customerfile.auditor";
		} else{
			fromClause = "billing.auditor";	
		}		
		return fromClause;
	}
	
	public static String getPayable(String entity) {
		String fromClause = entity;
		if (entity.equals("QuotationFile") || entity.equals("CustomerFile")) {
			fromClause = "customerfile.personPayable";
		}else{
			fromClause = "billing.personPayable";
		}
		return fromClause;
	}
	
	public static String getPricing(String entity) {
		String fromClause = entity;
		if (entity.equals("QuotationFile") || entity.equals("CustomerFile")) {
			fromClause = "customerfile.personPricing";
		} else{
			fromClause = "billing.personPricing";
		}
		return fromClause;
	}
	
	public static String getConsultant(String entity) {
		String fromClause = entity;
		fromClause = "customerfile.estimator";
		return fromClause;
	}
	
	public static String getSalesMan(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.salesMan";
		}else{
			fromClause = "serviceorder.salesMan";
		}
		return fromClause;
	}
	
	public static String getWarehousemanager(String entity) {
		String fromClause = entity;
		fromClause = "refmaster.billCrew";
		return fromClause;
	}
	
	public static String getShipper(String entity) {
		String fromClause = entity;
		fromClause = "customerfile.firstName, customerfile.lastName";
		return fromClause;
	}
	public static String getOpsPerson(String entity) {
		String fromClause = entity;
		fromClause = "serviceorder.opsPerson";
		return fromClause;
	}
	public static String getJoinClause(String entity) {
		String joinClause = entity;
		if (entity.equals("ServiceOrder")) {
			joinClause = "serviceorder.sequenceNumber=customerfile.sequenceNumber and serviceorder.shipNumber= trackingstatus.shipNumber and serviceorder.shipNumber=miscellaneous.shipNumber and serviceorder.shipNumber=billing.shipNumber and serviceorder.shipNumber=accountline.shipNumber and serviceorder.shipNumber=servicepartner.shipNumber and serviceorder.shipNumber=vehicle.shipNumber";
		} else if (entity.equals("CustomerFile")) {
			joinClause = " customerfile.sequenceNumber= serviceorder.sequenceNumber and customerfile.sequenceNumber= miscellaneous.sequenceNumber and customerfile.sequenceNumber=billing.sequenceNumber";
		} else if (entity.equals("QuotationFile")) {
			joinClause = " customerfile.sequenceNumber= serviceorder.sequenceNumber and customerfile.sequenceNumber= miscellaneous.sequenceNumber and customerfile.sequenceNumber=billing.sequenceNumber";
		} else if (entity.equals("Miscellaneous")) {
			joinClause = " miscellaneous.shipNumber= serviceorder.shipNumber";
		} else if (entity.equals("ServicePartner")) {
			joinClause = " servicepartner.shipNumber= serviceorder.shipNumber and servicepartner.shipNumber=trackingstatus.shipNumber and servicepartner.shipNumber=billing.shipNumber";
		} else if (entity.equals("Vehicle")) {
			joinClause = " vehicle.shipNumber= serviceorder.shipNumber and vehicle.shipNumber=trackingstatus.shipNumber and vehicle.shipNumber=billing.shipNumber";
		}
		return joinClause;
	}
	public static String getClaimHandLer(String entity,String roleList) {
		String fromClause = "";
		if (entity.equals("Billing")) {
			fromClause = "billing.claimHandler";
		}else if (entity.equals("Claim")){
			if(roleList.equals("Claim Handler")){
			fromClause = "billing.claimHandler";	
			}else{
			fromClause = "claim.claimPerson";
			}
		}
		return fromClause;
	}
	public static String getForwarder(String entity) {
		String fromClause = "";
		if (entity.equals("Billing")) {
			fromClause = "billing.personForwarder";
		}
		return fromClause;
	}
	public static String getMmCounselor(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "serviceorder.mmCounselor";
		}
		return fromClause;
	}
	public static String getReceiptdate(String entity) {
		String fromClause = "";
		if (entity.equals("Billing")) {
			fromClause = "billing.wareHousereceiptDate";
		}
		return fromClause;
	}
	public static String getEmail(String entity) {
		String fromClause = entity;
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.originAgentEmail";
		}else{
			fromClause="";
		}
		return fromClause;
	}
	public static String getBillToCode(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
		    fromClause = "customerfile.billToCode";
		}else{
		    fromClause = "billing.billToCode";	
		}
		return fromClause;
	}
	public static String getBillToName(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
		    fromClause = "customerfile.billToName";
		}else{
		    fromClause = "billing.billToName";	
		}
       
		return fromClause;
	}
	
	public static String buildExpression(String entity, String baseExpression, Long recordID, String sessionCorpID) {
		String queryParameter="";
								queryParameter= " from " + getFromClause(entity,sessionCorpID,baseExpression) + " where "+entity.toLowerCase()+".corpID in ('"+sessionCorpID+ "') AND " + ((recordID == null) ? "" : entity.toLowerCase() + ".id = " + recordID+" AND ") + "( "
					+ baseExpression + " ) AND serviceorder.status <>'CNCL' and serviceorder.controlflag='C' " ;
					
			queryParameter=queryParameter+" group by " + entity.toLowerCase() + ".id order by null " ;
		return queryParameter;
	}
	
}
