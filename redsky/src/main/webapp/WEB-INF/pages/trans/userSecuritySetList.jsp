<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="userListSecuritySet.title"/></title>   
    <meta name="heading" content="<fmt:message key='userListSecuritySet.heading'/>"/> 
    <style type="text/css"> 
		span.pagelink {height:11px;padding-top: 16px;text-align:left !important;font-size: 1em;}
	</style> 
</head>
<c:if test="${userListSecuritySet!='[]'}"> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>&nbsp;</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="userListSecuritySet" action="" method="post" cssStyle="margin-top:0px !important;">  
<s:set name="userListSecuritySet" value="userListSecuritySet" scope="request"/> 
<display:table name="userListSecuritySet" class="table" requestURI="" id="userListSecuritySet" style="width:200px" defaultsort="1" pagesize="100" >   
 		 <display:column property="userName" headerClass="headerColorInv" title="User Name" style="width:40px" />
 		 
</display:table>  
</s:form>
</c:if> 
<c:if test="${userListSecuritySet=='[]'}">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>No DataSecuritySet assigned</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
</c:if>
		  	