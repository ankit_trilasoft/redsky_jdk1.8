<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";
while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_OPS") || userRole.equalsIgnoreCase("ROLE_OPS_MGMT")){
		userRole=role.getName();
		break;
	}	
}%>
<head>
<title>Storage Library</title>
<meta name="heading" content="Storage Library" />
<script language="javascript" type="text/javascript">
function clear_fields()
{  		    	
	document.forms['storageLibraryList'].elements['storageId'].value = "";
	document.forms['storageLibraryList'].elements['storageTypeValue'].value = "";
	document.forms['storageLibraryList'].elements['storageModeType'].value ="";
	
}

function check1(value){
	if( value != ''){
			document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false;		    
		}else{
			document.forms['assignItemsForm'].elements['forwardBtn'].disabled = true ;
		}
	return true;
}
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="storageLibraryList" name="storageLibraryList" action="storageLibraryUnitSearch.html" method='post'>
<div id="Layer1" style="width:100%">
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="serviceOrder.id" />
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
		<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			       <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
			  <c:if test="${forwardingTabVal!='Y'}"> 
					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
				</c:if>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
              <c:if test="${serviceOrder.job =='INT'}">
               <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
              </c:if>
              </sec-auth:authComponent>
			  
			 			 			  		   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  	    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div>
		<div class="spn">&nbsp;</div>
		
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
				
		<div id="newmnav">
		   			  <ul>
					  <li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
						<li><a href="bookStorageLibraries.html?id=<%=request.getParameter("id") %>"><span>Storage List</span></a></li>
						<li id="newmnav1" style="background:#FFF"><a class="current"><span>Add Storage<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						<li><a href="storageUnit.html?id=<%=request.getParameter("id") %>"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=<%=request.getParameter("id") %>"><span>Move Storage Location</span></a></li>					  	
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div>
		<div class="spn">&nbsp;</div>
<table width="100%" cellspacing="1" cellpadding="0" border="0">
		<tbody>
			<tr>
			  <td>	
			  <div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							
							<td class="listwhite">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhite">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
	</div>
	<div class="spn" style="height:0px;">&nbsp;</div>
<div id="content" align="center">
 <div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
	<table class="table" width="98%">
		<thead>
			<tr>
			    <th>Storage Id</th>
				<th>Storage Type</th>
				<th>Storage Mode</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td><s:textfield name="storageId" size="35" tabindex="1" cssClass="text medium" /></td>
				<td><s:select name="storageTypeValue" cssClass="list-menu" cssStyle="width:200px" list="%{storageType}" headerKey=""  headerValue="" tabindex="2"/></td>
	            <td><s:select name="storageModeType" cssClass="list-menu" cssStyle="width:200px" list="%{storageMode}" headerKey=""  headerValue="" tabindex="3"/></td>
	          	<td style="border-left: hidden; align:right"><s:submit cssClass="cssbutton" method="storageLibraryUnitSearch" cssStyle="width:60px; height:25px;!margin-bottom: 10px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/></td> 
			</tr>
		</tbody>
	</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${storagelibrarybuttons}" escapeXml="false" />
	<div id="Layer5" style="width:100%;">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Storage Library List</span></a></li>
		  </ul>
	</div>
	
	<div class="spnblk">&nbsp;</div>
	<s:set name="storageLibraries" value="storageLibraries" scope="request" />
	<display:table name="storageLibraries" class="table" requestURI="" id="storageLibrariesList" export="true" pagesize="50">
	<%if((userRole.equalsIgnoreCase("ROLE_OPS")) || (userRole.equalsIgnoreCase("ROLE_OPS_MGMT"))){ %>
	<display:column style="width:10px"><input type="radio" name="dd" onclick="check(this); check1('${storageLibrariesList.id}')"	value="${storageLibrariesList.id}" /></display:column>
	<%} %>
	<display:column property="storageId" sortable="true" title="Storage Id" style="width:80px"/>
	<display:column property="storageDes" sortable="true" title="Storage Type" maxLength="15" style="width:100px"/>	
	<display:column property="storageDescription" sortable="true" title="Storage Description" maxLength="15" style="width:100px"/>	
	<display:column headerClass="containeralign" title="Volume<br>Cbm" style="width:40px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibrariesList.volumeCbm}" /></div></display:column>
	<display:column headerClass="containeralign" title="Used&nbsp;Volume<br>Cbm" style="width:50px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibrariesList.usedVolumeCbm}" /></div></display:column>
	<display:column headerClass="containeralign" title="Avail&nbsp;Volume<br>Cbm" style="width:50px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibrariesList.availVolumeCbm}" /></div></display:column>
	<display:column property="storageMode" sortable="true" title="Storage Mode" maxLength="5" style="width:50px"/>
    
    <display:setProperty name="export.excel.filename" value="StorageLibrary List.xls"/>
	<display:setProperty name="export.csv.filename" value="StorageLibrary List.csv"/>
	</display:table>
	</div>
	</div>
</s:form>
<s:form id="assignItemsForm" action="editLocationUnit" method="post">
	<s:hidden name="id" /> 
	<s:hidden name="locate"  />
	<s:hidden name="id1"  value="<%=request.getParameter("id") %>"/>
	<s:hidden name="ticket" value="%{workTicket.ticket}"/>
	<s:hidden name="shipNumber" value="%{workTicket.shipNumber}"/>
	<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
	<input type="submit" name="forwardBtn"  disabled value="Add Storage to Location" class="cssbutton" style="width:150px; height:25px" />
	<input type="submit" name="forward"  disabled value="Add Storage to Storage Units" class="cssbutton" style="width:180px; height:25px" />
    </c:if>
    <c:if test='${serviceOrder.status == "CNCL"}'>
     <input type="button" name="forwardBtn"  class="cssbutton" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before Add Storage.')" value="Add" />
    </c:if>
    <c:if test='${serviceOrder.status == "HOLD"}'>
     <input type="button" name="forwardBtn"  class="cssbutton" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before Add Storage.')" value="Add" />
    </c:if> 
</s:form>  

<script type="text/javascript"> 
highlightTableRows("storageLibraryList"); 
</script>
<script type="text/javascript">
		function check(targetElement) {
		try{
		  	document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
			document.forms['assignItemsForm'].elements['locate'].value=document.forms['assignItemsForm'].elements['id'].value;
		}
		catch(e){}
		}
</script>
