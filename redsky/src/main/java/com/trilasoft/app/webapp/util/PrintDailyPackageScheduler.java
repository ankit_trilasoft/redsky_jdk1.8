/**
 * This scheduler class interacts with Ware House and date to generate pdf.
 * @Class Name	PrintDailyPackageScheduler
 * @Author      Ashis Kumar Mohanty
 * @Version     V01.0
 * @Since       1.0
 * @Date        11-Jan-2013
 * Extended by QuartzJobBean.
 */
package com.trilasoft.app.webapp.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.webapp.action.PrintDailyPackageAction;

public class PrintDailyPackageScheduler extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private static final String SERVLET_CONTEXT_KEY = "servletContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	
	private Company company;
	private PrintDailyPackage printDailyPackage;
	Map<String,String> wareHouseMap = new HashMap<String,String>(); 
	
	static final Logger logger = Logger.getLogger(PrintDailyPackageScheduler.class);
	
	private ApplicationContext getApplicationContext(JobExecutionContext context)throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	private ServletContext getServletContext(JobExecutionContext context)throws Exception {
		ServletContext servletContext =(ServletContext)context.getJobDetail( ).getJobDataMap().get(SERVLET_CONTEXT_KEY);
		if (servletContext == null) {
			throw new JobExecutionException(
					"No servlet context available in scheduler context for key \""+SERVLET_CONTEXT_KEY+"\"");
		}
		return servletContext;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml 
	protected void executeInternal(JobExecutionContext context)	throws JobExecutionException {
		Long startTime = System.currentTimeMillis();
		Set<String> corpIdSet = new HashSet<String>();
		corpIdSet.add("SSCW"); 
		PrintDailyPackageAction printDailyPackageAction;
		try {
			appCtx = getApplicationContext(context);
			ServletContext servletContext = getServletContext(context);
			printDailyPackageAction = (PrintDailyPackageAction)appCtx.getBean("printDailyPackageAction");
			for (String corpId : corpIdSet) {
				Map<String, String> house = printDailyPackageAction.getRefMasterManager().findByParameter(corpId, "HOUSE");
				for (Map.Entry<String,String> entry : house.entrySet()) {
					if(entry.getKey() != null && !"".equals(entry.getKey().trim())){
						printReports(printDailyPackageAction,entry,corpId,context,servletContext);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Long timeTaken = System.currentTimeMillis()-startTime;
		logger.warn("\n\nTime taken to find and merge All jrxml Pdf Scheduler : "+timeTaken+"\n\n");
	}
	
	public void printReports(PrintDailyPackageAction printAction, Map.Entry<String,String> entry,String sessionCorpID,JobExecutionContext context,ServletContext servletContext) throws Exception {
		company= printAction.getCompanyManager().findByCorpID(sessionCorpID).get(0);
		String pathSeparator = System.getProperty("file.separator");
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		byte[] output;
		List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
		JasperReport reportTemplate = null;
		wareHouseMap = new HashMap<String,String>(); 
		
		List<PrintDailyPackage> printList = printAction.getPrintDailyPackageManager().getByCorpId(sessionCorpID,"");
		String path = servletContext.getRealPath("/jasper");
		
		for (PrintDailyPackage printDailyPackage : printList) {
			List<PrintDailyPackage> printChildList = printAction.getPrintDailyPackageManager().findChildByParentId(printDailyPackage.getId());
			for (PrintDailyPackage printChild : printChildList) {
				if (printChild.getJrxmlName() != null && printChild.getJrxmlName().trim().length() > 0 &&
					printChild.getParameters() != null && printChild.getParameters().trim().length() > 0) {
					
					Set<String> SOList = new HashSet<String>();
					Map parameters = new HashMap();
					Reports report = printAction.getReportsManager().getReportTemplateForWT(printChild.getJrxmlName(), "WorkTicket", sessionCorpID);
					reportTemplate = JasperCompileManager.compileReport(path+pathSeparator+sessionCorpID+pathSeparator+ report.getReportName());
					//reportTemplate = JasperCompileManager.compileReport(path + "//"+sessionCorpID+"//" + report.getReportName());
					
					JRParameter[] allParameters = reportTemplate.getParameters();
					List<WorkTicket> list = printAction.getWorkTicketManager().getWorkTicketList(printDailyPackage.getWtServiceType(), entry.getKey(), new Date(), sessionCorpID,printDailyPackage.getMode(),printDailyPackage.getMilitary(), printDailyPackage.getJob(), printDailyPackage.getBillToCode());
					for (WorkTicket workTicket : list) {
						if(("Y".equals(printDailyPackage.getOnePerSOperDay()) && SOList.contains(workTicket.getShipNumber().trim()))
								|| (printChild.getParameters().indexOf("SO") >= 0 && !SOList.contains(workTicket.getShipNumber().trim()))){
								break;
						}
						SOList.add(workTicket.getShipNumber());
						jasperPrints = getParameter(entry,printAction,printDailyPackage,printChild,"WorkTicket",allParameters,parameters,reportTemplate,jasperPrints,workTicket);
					}
					if((list == null || list.size() == 0) && (printChild.getParameters().indexOf("SO") >= 0) && "All".equals(printDailyPackage.getWtServiceType())){
						List<ServiceOrder> SOlist = printAction.getServiceOrderManager().findServiceOrderByDateRange(new Date(), sessionCorpID);
						for (ServiceOrder serviceOrder : SOlist) {
							jasperPrints = getParameter(entry,printAction,printDailyPackage,printChild,"ServiceOrder",allParameters,parameters,reportTemplate,jasperPrints,serviceOrder);
						}
					}
				}
				if(wareHouseMap != null && wareHouseMap.size() > 0){
					for (Map.Entry<String,String> wareHouse : wareHouseMap.entrySet()) {
						JRExporter exporterPDF = new JRPdfExporter();
						exporterPDF.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, true);
						exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrints);
						exporterPDF.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
						exporterPDF.exportReport();
						
						output=byteArrayOutputStream.toByteArray();
						
						SimpleDateFormat sdf2 = new SimpleDateFormat(DATE_FORMAT_NOW);
						String date = sdf2.format(new Date());
						
						String newPath = path.substring(0, path.indexOf(pathSeparator));
						newPath = newPath+pathSeparator+"PrintDailyPackage"+pathSeparator+date+pathSeparator+wareHouse.getValue()+"("+wareHouse.getKey()+")";
						File dirPath = new File(newPath);
				        if (!dirPath.exists()) {
				            dirPath.mkdirs();
				        }
				        
				        FileOutputStream fos = new FileOutputStream(newPath+pathSeparator+"PrintDailyPackage.pdf");
				        fos.write(output);
				        fos.close();
					}
				}
			}
		}
}
	
	public List<JasperPrint> getParameter(Map.Entry<String,String> entry,PrintDailyPackageAction printAction,PrintDailyPackage printDailyPackage,PrintDailyPackage printChild,String flag,JRParameter[] allParameters,Map parameters,JasperReport reportTemplate,List<JasperPrint> jasperPrints,Object obj){
		String ticket = "";
		String ship = "";
		String whouse="";
		String sessionCorpID="";
		if("WorkTicket".equals(flag)){
			WorkTicket wt = (WorkTicket)obj;
			ticket = wt.getTicket().toString();
			whouse = wt.getWarehouse();
			sessionCorpID= wt.getCorpID();
		}else if("ServiceOrder".equals(flag)){
			ServiceOrder so = (ServiceOrder)obj;
			ship = so.getShipNumber();
			sessionCorpID = so.getCorpID();
		}
		for (JRParameter parameter : allParameters) {
			if (!parameter.isSystemDefined()) {
				String paramName = parameter.getName();
				
				if(paramName.equalsIgnoreCase("User Name")){
					parameters.put(parameter.getName(), "amohanty");
				}else if(paramName.equalsIgnoreCase("Service Order Number")){
					parameters.put(parameter.getName(), ship);
				}else if(paramName.equalsIgnoreCase("Warehouse")){
					parameters.put(parameter.getName(), whouse);
				}else if(paramName.equalsIgnoreCase("Work Ticket Number")){
					parameters.put(parameter.getName(), ticket);
				}else if(paramName.equalsIgnoreCase("Corporate ID")){
					parameters.put(parameter.getName(), sessionCorpID);
				}else if(paramName.equalsIgnoreCase("enddate")){
					parameters.put(parameter.getName(), new Date());
				}else if(paramName.equalsIgnoreCase("begindate")){
					parameters.put(parameter.getName(), new Date());
				}else{
					parameters.put(parameter.getName(), "");
				}
			}
		}
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
					
		JasperPrint jasperPrint = new JasperPrint();
		System.out.println("Generate Report for "+ship+" : "+ticket+" Jrxml Name: "+printChild.getJrxmlName());
		try {
			jasperPrint = printAction.getReportsManager().generateReport(reportTemplate, parameters);
			
			for (int i = 1; i <= printDailyPackage.getNoOfCopies(); i++) {
				jasperPrints.add(jasperPrint);
			}
			wareHouseMap.put(entry.getKey(), entry.getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrints;
	}
	

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public PrintDailyPackage getPrintDailyPackage() {
		return printDailyPackage;
	}

	public void setPrintDailyPackage(PrintDailyPackage printDailyPackage) {
		this.printDailyPackage = printDailyPackage;
	}

	public Map<String, String> getWareHouseMap() {
		return wareHouseMap;
	}

	public void setWareHouseMap(Map<String, String> wareHouseMap) {
		this.wareHouseMap = wareHouseMap;
	}
}
