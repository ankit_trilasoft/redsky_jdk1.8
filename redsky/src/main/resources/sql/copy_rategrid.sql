DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`copy_rategrid` $$
CREATE DEFINER=`root`@`%` PROCEDURE `copy_rategrid`(from_contract varchar(500),to_contract varchar(500), lcorpid varchar(4))
BEGIN
DECLARE bDone INT;
DECLARE l_ID INT;
DECLARE l_toID INT;


declare curs CURSOR FOR select  distinct a.id, c.id
from rategrid a,charges b, charges c
where contractName=to_contract and a.corpid=lcorpid and a.charge=b.id and b.contract=from_contract and c.contract=to_contract
and b.corpid=lcorpid
and c.corpid=lcorpid and b.charge=c.charge
and b.status=true and c.status=true;

declare CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

OPEN curs;

  SET bDone = 0;
  REPEAT
    FETCH curs INTO l_ID,l_toID;

    update rategrid set charge=l_toID where id=l_ID;
   
  UNTIL bDone END REPEAT;

  CLOSE curs;


END $$

DELIMITER ;