<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
  <c:if test="${multiCurrency=='Y'}"> 
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = userRole+","+role.getName(); 
}
%> 

<!-- Code Commented by Ankit for 13835 start-->
<%if(userRole.contains("ROLE_EXECUTIVE")){ %>
<s:hidden name="userRoleExecutive" value="yes"/>
<%}else{%>
<s:hidden name="userRoleExecutive" value="no"/>
<%}%>
<!-- Code Commented by Ankit for 13835 End -->

<!-- Code Added by Ankit for 13835 start-->
<%-- <c:if test="${fn:contains(userRole, 'ROLE_EXECUTIVE')}">
<s:hidden name="userRoleExecutive" value="yes"/>
</c:if>

<c:if test="${not fn:contains(userRole, 'ROLE_EXECUTIVE')}">
<s:hidden name="userRoleExecutive" value="no"/>
</c:if> --%>
<!-- Code Added by Ankit for 13835 End-->

</c:if>
<table class="detailTabLabel"  border="0" width="100%" style="margin:0" >
<s:hidden name="componentId" value="module.accountLine" />
<s:hidden name="multiCurrency" />
<s:hidden name="oldRecRateCurrency" />
<s:hidden id="checkForDaysClick" name="checkForDaysClick" />
<s:hidden id="checkReceivedInvoiceDate" name="checkReceivedInvoiceDate" />
<s:hidden  name="accountLine.oldBillToCode" />
<s:hidden name="accountLine.t20Process" />
<c:set var="tenancyBillingVal" value="N" />
<configByCorp:fieldVisibility componentId="component.contract.reloJob.tenancyBilling">		
	<c:set var="tenancyBillingVal" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat1" name="dateFormat1" value="dd-NNN-yy"/>
<sec-auth:authComponent componentId="module.accountLine.section.receivableDetail.edit">
	<%
	String sectionRreceivableDetail="true";
	int permission  = (Integer)request.getAttribute("module.accountLine.section.receivableDetail.edit" + "Permission");
 	if (permission > 2 ){
 		sectionRreceivableDetail = "false"; 
 	} 
  %> 													
														<div class="subcontenttabChild" style="height:20px;padding:0 3px 4px;" background="images/greyband.gif" style="width:99%"><fmt:message key="accountLine.ReceiveableDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" disabled="<%= sectionRreceivableDetail%>" value="Compute" name="accountLine.compute4"  onclick="findRevisedReceivableQuantitys();" style="width:65px"></div>
														<tr style="" height="0px"> 
															<td align="right" class="listwhitetext" style="magin:0px; padding:0px;"></td>
														    <td align="left" class="listwhitetext"></td>
															<td align="left"></td>
															<td align="right" class="listwhitetext"></td>
															<td align="left" class="listwhitetext"></td>
															<td width="10px"></td>
															<td width="10px"></td>
															<c:if test="${multiCurrency!='Y'}"> 
															<td valign="top" rowspan="15"  class="vertlinedata_vert"></td>
															<td></td>
															<td valign="top" rowspan="15" class="vertlinedata_vert"></td>
															<td width="34px"></td>
															<td valign="top" rowspan="15" class="vertlinedata_vert"></td>	
														    </c:if>
														    <c:if test="${multiCurrency=='Y'}">
														    <td valign="middle" rowspan="15" class="vertlinedata_vert"></td>
															<td></td>
															<td valign="middle" rowspan="15" class="vertlinedata_vert"></td>
															<td width="34px"></td>
															<td valign="middle" rowspan="15" class="vertlinedata_vert"></td>	
														    </c:if> 
														</tr>
														<tr>
															<td align="right" class="listwhitetext" ><fmt:message key="accountLine.authorization" />
															</td>
															<td colspan="2" align="left">
															<table style="margin:0px;" cellpadding="0" cellspacing="0">
															<tr>
															<td>
															<s:textfield cssClass="input-text" readonly="<%= sectionRreceivableDetail%>" cssStyle="text-align:left; width:60px;" key="accountLine.authorization" size="17" maxlength="50" tabindex="23" /> 
															</td>
															<td>
															<div  id="hhid" style="float:right;">
															<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
															<img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="findAuthorizationNo('a');"/>
															<%} %>
															</div>
															</td>
															</tr>
															</table>
															</td>
															<td colspan="2" align="left">
															<table style="margin:0px;" cellpadding="0" cellspacing="0">
															<tr>
																	<c:set var="ignoreBilling" value="TRUE" />
																	<configByCorp:fieldVisibility componentId="component.field.Alternative.ignoreBilling">
																			<c:set var="ignoreBilling" value="FALSE" />
																		<sec-auth:authComponent componentId="module.tab.accountline.ignoreBilling">
																			<c:set var="ignoreBilling" value="TRUE" />
																		</sec-auth:authComponent>
																	</configByCorp:fieldVisibility>
															
															<c:choose>
															<c:when test="${ignoreBilling=='TRUE'}">
															<td style="width:75px;!width:11%;" class="listwhitebox" align="right">Ignore&nbsp;for&nbsp;Invoicing&nbsp;</td>
													        <c:set var="isVipFlag" value="false" />
															<c:if test="${accountLine.ignoreForBilling}">
																	<c:set var="isVipFlag" value="true" />
																</c:if>
																<td align="left" class="listwhitetext">
																<s:checkbox name="accountLine.ignoreForBilling" cssStyle="margin:0px;" value="${isVipFlag}" fieldValue="true" onchange="changeStatus();" tabindex="24"/>
																</td>
															</c:when>
															<c:otherwise>
															<td style="width:75px;!width:11%;" class="listwhitebox" align="right">Ignore&nbsp;for&nbsp;Invoicing&nbsp;</td>
													        <c:set var="isVipFlag" value="false" />
															<c:if test="${accountLine.ignoreForBilling}">
																	<c:set var="isVipFlag" value="true" />
																</c:if>
																<td align="left" class="listwhitetext">
																<s:checkbox name="accountLine.ignoreForBilling" cssStyle="margin:0px;" value="${isVipFlag}" fieldValue="true" disabled="true" onchange="changeStatus();" tabindex="24"/>
																</td>
															</c:otherwise>
															</c:choose>
																<c:set var="isRollUpInvoiceFlag" value="false"/>
																<c:if test="${accountLine.rollUpInInvoice}">
																	<c:set var="isRollUpInvoiceFlag" value="true"/>
																</c:if>																
																<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
																<td align="right" style="width:100px;!width:11%;" class="listwhitebox" >Roll&nbsp;Up&nbsp;In&nbsp;Invoice</td>
																<td align="left" class="listwhitetext">
																<s:checkbox name="accountLine.rollUpInInvoice" value="${isRollUpInvoiceFlag}" fieldValue="true" onchange="changeStatus();" tabindex="24"/>
																</td>
																</configByCorp:fieldVisibility>
																<c:set var="checkRoleForCreditNote" value="N"/>
																<sec-auth:authComponent componentId="module.AccountLine.AuthorizedCreditNote.edit">
																<%
																	int permission11  = (Integer)request.getAttribute("module.AccountLine.AuthorizedCreditNote.edit" + "Permission");
																	if (permission11 > 2 ){%>
																			<c:set var="checkRoleForCreditNote" value="Y"/>
																		<%}else{%>
																			<c:set var="checkRoleForCreditNote" value="N"/>
																		<%}
																  %>
																</sec-auth:authComponent>
																<configByCorp:fieldVisibility componentId="component.field.AccountLine.AuthorizedCreditNote">
																	<c:set var="isAuthorizedCreditNote" value="false"/>
																	<c:if test="${accountLine.authorizedCreditNote}">
																		<c:set var="isAuthorizedCreditNote" value="true"/>
																	</c:if>
																	<td align="right" style="padding-left:20px;" class="listwhitebox" >Authorized&nbsp;Credit&nbsp;Note</td>
																	<c:if test="${checkRoleForCreditNote=='N'}">
																		<td align="left" class="listwhitetext">
																		<s:checkbox name="accountLine.authorizedCreditNote" value="${isAuthorizedCreditNote}" disabled="true"/>
																		</td>
																	</c:if>
																	<c:if test="${checkRoleForCreditNote=='Y' && (accountLine.recInvoiceNumber==''|| accountLine.recInvoiceNumber==null)}">
																		<td align="left" class="listwhitetext">
																		<s:checkbox name="accountLine.authorizedCreditNote" id="authorizedCreditNote" value="${isAuthorizedCreditNote}" onchange="checkActualRevenue();"/>
																		</td>
																	</c:if>
																	<c:if test="${checkRoleForCreditNote=='Y' && accountLine.recInvoiceNumber!='' && accountLine.recInvoiceNumber!=null}">
																		<td align="left" class="listwhitetext">
																		<s:checkbox name="accountLine.authorizedCreditNote" value="${isAuthorizedCreditNote}" disabled="true"/>
																		</td>
																	</c:if>
																</configByCorp:fieldVisibility>
															</tr>
															</table>
																</td>
																
															<td colspan="1" class="listwhitetext" align="right">Invoice Created By</td>
															<td colspan="5">
																<s:textfield cssClass="input-textUpper" readonly="true" key="accountLine.invoiceCreatedBy" cssStyle="text-align:left"  size="8" maxlength="25" tabindex="25"/> 
															</td> 
															<td align="right" width="76px">
																	<c:if test="${empty accountLine.id}">
																		<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
																		<img  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/>
																	<%} %>
																	</c:if>
																	<c:if test="${not empty accountLine.id}">
																		<c:choose>
																			<c:when test="${countReceivableDetailNotes == '0' || countReceivableDetailNotes == '' || countReceivableDetailNotes == null}">
																				<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
																				<img id="countReceivableDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);" ></a>
																			<%} %>
																			</c:when>
																			<c:otherwise>
																			<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
																				<img id="countReceivableDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);" ></a>
																			<%} %>
																			</c:otherwise>
																		</c:choose> 
																	</c:if>
																</td> 
														</tr>
											            <tr>
															<td width="" align="right" class="listwhitetext">Bill To</td>
															<td align="left" class="listwhitetext" nowrap="nowrap"><s:textfield  cssClass="input-textUpper"key="accountLine.billToCode" onselect="changeStatus();" onblur="changeStatus();" onchange="changeStatus();" size="8"maxlength="17" readonly="true" tabindex="26"/>
																 <c:if test="${accountInterface=='Y'}">
																<c:if test="${!utsiRecAccDateFlag  && !utsiPayAccDateFlag}"> 
																<c:if test="${(!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber))}"> 
																  <% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																   <img align="top" id="accountLineBillToCodeOpenpopup"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.billToName&fld_code=accountLine.billToCode');document.forms['accountLineForms'].elements['accountLine.billToCode'].select();" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />
																   <%} %>
																 </c:if>
																<c:if test="${(!accNonEditFlag && not empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber!='' && not empty accountLine.recInvoiceNumber))}"> 
																<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																   <c:if test="${(!accNonEditFlag)}">
																  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as Sent To Acc has been already filled.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	</c:if>
																	<c:if test="${(accNonEditFlag)}">
																  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as invoice has been generated.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	</c:if>
																	<%} %>
																</c:if>
																</c:if>
																<c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
																<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	<%} %> 
																</c:if>
																</c:if>
																<c:if test="${accountInterface!='Y'}">
																<c:if test="${accountLine.recInvoiceNumber==''|| accountLine.recInvoiceNumber==null}"> 
																  <% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																   <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup10.img" src="<c:url value='/images/open-popup.gif'/>" />
																   <%} %>
																 </c:if>
																<c:if test="${accountLine.recInvoiceNumber!=''&& accountLine.recInvoiceNumber!=null}"> 
																<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup11.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	<%} %>
																</c:if>
																</c:if> 
															</td>
															<td width="" align="right" class="listwhitetext"><fmt:message key="billing.AccName" /></td>
															<td align="left" colspan="2" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="accountLine.billToName" cssStyle="width:165px"  readonly="true" tabindex="27" /></td>
															<td width="125" align="right"  class="listwhitetext"><fmt:message key="accountLine.invoiceNumber" /></td>
															<td width="100px" align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper"key="accountLine.recInvoiceNumber" size="8" maxlength="17" readonly="true" tabindex="28"/></td>
													</tr>
													<c:if test="${(billingDMMContractType && (!(networkAgent)))}">
													<tr>
													        <td width="" align="right" class="listwhitetext">Network&nbsp;Bill&nbsp;To</td>
															<td align="left" class="listwhitetext" nowrap="nowrap"><s:textfield  cssClass="input-textUpper"key="accountLine.networkBillToCode" onselect="changeStatus();" onblur="changeStatus();" onchange="changeStatus();" size="8"maxlength="17" readonly="true" tabindex="26"/>
																  
																<c:if test="${!utsiRecAccDateFlag  && !utsiPayAccDateFlag}"> 
																<c:if test="${(!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber))}"> 
																  <% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																   <img align="top" id="accountLineNetworkBillToCodeOpenpopup"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('findNetworkBillToCodeList.html?networkPartnerCode=${Billing.networkBillToCode}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.networkBillToName&fld_code=accountLine.networkBillToCode');document.forms['accountLineForms'].elements['accountLine.networkBillToCode'].select();" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />
																  <%} %>
																 </c:if>
																<c:if test="${(!accNonEditFlag && not empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber!='' && not empty accountLine.recInvoiceNumber))}"> 
																<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																   <c:if test="${(!accNonEditFlag)}">
																  <img align="top" id="accountLineNetworkBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Network Bill To code as Sent To Acc has been already filled.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	</c:if>
																	<c:if test="${(accNonEditFlag)}">
																  <img align="top" id="accountLineNetworkBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Network Bill To code as invoice has been generated.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	</c:if>
																	<%} %>
																</c:if>
																</c:if>
																<c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
																<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%> 
																  <img align="top" id="accountLineNetworkBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Network Bill To code as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
																	<%} %> 
																</c:if> 
															</td>
															<td width="" align="right" class="listwhitetext">Network&nbsp;Bill&nbsp;To&nbsp;Name</td>
															<td align="left" colspan="2" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="accountLine.networkBillToName" cssStyle="width:165px"  readonly="true" tabindex="27" /></td>
													</tr>
													</c:if>
													
<sec-auth:authComponent componentId="module.accountLine.section.receivedInvoice.edit">
	<%
	String sectionReceivedInvoiceDetail="true";
	int permission1  = (Integer)request.getAttribute("module.accountLine.section.receivedInvoice.edit" + "Permission");
 	if (permission1 > 2 ){
 		sectionReceivedInvoiceDetail = "false"; 
 	}
 	 
  %>				
													<tr> 	<td align="right"  class="listwhitetext"><fmt:message key="accountLine.invoiceDate" /></td> 
																<c:if test="${not empty accountLine.receivedInvoiceDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue1}"> <s:param name="value" value="accountLine.receivedInvoiceDate" /> </s:text>
																	<c:if test="${!utsiRecAccDateFlag && !utsiPayAccDateFlag}">
																	<c:if test="${(!accNonEditFlag && not empty accountLine.recAccDate) || (accNonEditFlag && ( accountLine.recInvoiceNumber!='' && not  empty accountLine.recInvoiceNumber))}">
																	<td><s:textfield cssClass="input-text"	id="receivedInvoiceDate"
																		name="accountLine.receivedInvoiceDate"
																		value="%{accountLineFormattedValue}" size="8"
																		maxlength="11" onkeydown="" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
																		 
																		   <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%> 
																		  <c:if test="${(!accNonEditFlag)}">
																		   <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled.')" />
																	      </c:if>
																	      <c:if test="${(accNonEditFlag)}">
																		   <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as invoice has been generated.')" />
																	      </c:if>
																	    <%} %>
																	    </c:if>
																	    <c:if test="${(!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber ) )}">
																		 <td><s:textfield cssClass="input-text"	id="receivedInvoiceDate"
																		name="accountLine.receivedInvoiceDate"
																		value="%{accountLineFormattedValue}" size="8"
																		maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
																		  <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>  
																		   <img id="receivedInvoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="copyReceivedInvoiceDate(); return false;" />
																	     <%} %>
																	    </c:if>
																	    </c:if>
																	    <c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
																	    <td><s:textfield cssClass="input-text"	id="receivedInvoiceDate"
																		name="accountLine.receivedInvoiceDate"
																		value="%{accountLineFormattedValue}" size="8"
																		maxlength="11" onkeydown="" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
																		 
																		   <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%> 
																		   <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as sent to Accounting and/or already Invoiced in UTSI Instance')" />
																	    <%} %>
																	    </c:if>
																	</td>
																</c:if>
																<c:if test="${empty accountLine.receivedInvoiceDate}">
																	<td><s:textfield cssClass="input-text"
																		id="receivedInvoiceDate"
																		name="accountLine.receivedInvoiceDate" required="true"
																		size="8" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
																	  <c:if test="${!utsiRecAccDateFlag && !utsiPayAccDateFlag}">
																	   <c:if test="${(!accNonEditFlag && not empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber!='' && not empty accountLine.recInvoiceNumber))}">
																		  <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>   
																		   <c:if test="${(!accNonEditFlag)}">
																		  <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	      </c:if>
																	      <c:if test="${(accNonEditFlag)}">
																		  <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as invoice has been generated.')" />
																	      </c:if>
																	   <%} %>
																	  </c:if>
																	   <c:if test="${(!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && ( accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber))}">
																	      <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>  
																	      <img id="receivedInvoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="copyReceivedInvoiceDate(); return false;" />
																	   <%} %>
																	  </c:if>
																	  </c:if>
																	   <c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
																	   <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>   
																		  <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as sent to Accounting and/or already Invoiced in UTSI Instance')" />
																	   <%} %>
																	   </c:if>
																	</td>
																</c:if>
</sec-auth:authComponent>															
																
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.revisionQuantity" /></td>
																<c:if test="${multiCurrency!='Y'}">
																<td align="left"  class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right; width:74px" key="accountLine.recQuantity"  maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onchange="convertedAmountBilling('accountLine.recQuantity','','','');fillDescription();checkActualRevenue();" onblur="chkSelect('1');checkMultiAuthorization();" tabindex="29" onselect=""/></td>
																</c:if>
																<c:if test="${multiCurrency=='Y'}">
																<td align="left"  class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right; width:74px" key="accountLine.recQuantity"  maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onchange="convertedAmountBilling('accountLine.recQuantity','','','');fillDescription();checkActualRevenue();" onblur="chkSelect('1');checkMultiAuthorization();" tabindex="29" /></td>
																</c:if>
																<td >
																<div id="receivableDeviationShow" > 
                                                                   <table class="detailTabLabel"><tr> 
                                                                   <td align="right" class="listwhitetext" >Sell Deviation</td>
                                                                   <c:if test="${empty accountLine.recAccDate}">
                                                                   <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" cssStyle="text-align:right" readonly="<%= sectionRreceivableDetail%>" key="accountLine.receivableSellDeviation" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="convertedAmountBilling('','','','');" tabindex="17" />%</td>
                                                                   </c:if>
                                                                   <c:if test="${not empty accountLine.recAccDate}">
                                                                   <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" readonly="true" key="accountLine.receivableSellDeviation" size="5" tabindex="17" />%</td>
                                                                   </c:if>
                                                                   <s:hidden name="oldReceivableDeviation" value="${accountLine.receivableSellDeviation}"/>
                                                                   </tr></table>
                                                                </div>
																</td>
																<c:if test="${multiCurrency!='Y'}">
																<td align="right" class="listwhitetext">Sell&nbsp;Rate</td>
																</c:if>
																<c:if test="${multiCurrency=='Y'}">
																<td align="right" class="listwhitetext" width="650px;">Base&nbsp;Curr&nbsp;Sell&nbsp;Rate</td>
																</c:if> 
																<td align="left" class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right" name="accountLine.recRate" value="${accountLine.recRate}" 
																	size="8" maxlength="10" onkeydown="return onlyRateAllowed(event);" onblur="chkSelect('1');"
																	onchange="roundRecRate();convertedAmountBilling('accountLine.recRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');calRecCurrencyRate('accountLine.recRate');fillDescription();checkActualRevenue();" tabindex="30" /></td>
															    <s:hidden   key="accountLine.itemNew" name="accountLine.itemNew"  />
																<td width="70px"></td>
																<td></td>
																<td align="left" class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right" key="accountLine.actualRevenue" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="convertedAmountBilling('','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" tabindex="30" /></td>
																</tr> 
																<c:if test="${contractType}">
																
 		 													 <tr><td align="left" class="vertlinedata" colspan="16" style="padding-bottom:3px;"></td></tr>													
																
																<tr> 
															<td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td> 
															<td align="left" colspan="1" >
														     <configByCorp:customDropDown 	  listType="map" list="${country}"   fieldValue="${accountLine.contractCurrency}"
																     attribute="id=contractCurrency class=list-menu name=accountLine.contractCurrency style=width:76px  headerKey='' headerValue=''  tabindex=18 onchange=changeStatus();findExchangeContractRate('accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate'); "/>	</td>
															
															
															<td align="right" class="listwhitetext" >Value&nbsp;Date</td>
																<c:if test="${not empty accountLine.contractValueDate}">
																	<s:text id="accountLineFormattedContractValue" name="${FormDateValue1}"><s:param name="value" value="accountLine.contractValueDate" /> </s:text>
																	<td><s:textfield  cssClass="input-text" id="contractValue" name="accountLine.contractValueDate" value="%{accountLineFormattedContractValue}" size="8" maxlength="12" readonly="true" /></td>
																	
																</c:if>
																<c:if test="${empty accountLine.contractValueDate}">
																<td><s:textfield  cssClass="input-text" id="contractValueDate" name="accountLine.contractValueDate" required="true" size="8" maxlength="12"  readonly="true"/></td> 
																</c:if> 
																
															</tr>
															<tr>
															<td align="right" class="listwhitetext">Contract&nbsp;Rate</td>
														    <td align="left" class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right" key="accountLine.contractRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateRecRateByContractRate('accountLine.contractRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" onblur="chkSelect('1');"/></td>	 
															<td width="80px" align="right" class="listwhitetext">Contract&nbsp;Exchange&nbsp;Rate</td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.contractExchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateRecRateByContractRate('none','','','');"/></td> 
															<td width="" align="right" class="listwhitetext"colspan="2">Contract&nbsp;Amount</td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.contractRateAmmount" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
															</tr>
															</c:if>
															<c:if test="${multiCurrency=='Y'}">
															 <tr><td align="left" class="vertlinedata" colspan="16" style="padding-top:3px;"></td></tr>
															<tr> 
															<c:choose>
															 <c:when test="${contractType}">
															 <td align="right" class="listwhitetext"><fmt:message key="accountLine.recRateCurrency" /><font color="red" size="2">*</font></td>
															 </c:when>
															 <c:otherwise>
															<td align="right" class="listwhitetext"><fmt:message key="accountLine.recRateCurrency" /></td>
															</c:otherwise></c:choose> 
															<td align="left" colspan="1" >
															<configByCorp:customDropDown 	  listType="map" list="${country}"   fieldValue="${accountLine.recRateCurrency}"
																     attribute="id=recRateCurrency class=list-menu name=accountLine.recRateCurrency style=width:76px  headerKey='' headerValue=''  tabindex=18 onchange=changeStatus(),findExchangeRecRate(); "/>	</td>
															<td align="right" class="listwhitetext" ><fmt:message key="accountLine.racValueDate" />  </td>
																<c:if test="${not empty accountLine.racValueDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue1}"><s:param name="value" value="accountLine.racValueDate" /> </s:text>
																	<td><s:textfield  cssClass="input-text" id="racValueDate" name="accountLine.racValueDate" value="%{accountLineFormattedValue}" size="8" maxlength="12" readonly="true" /></td>
																	
																</c:if>
																<c:if test="${empty accountLine.racValueDate}">
																<td><s:textfield  cssClass="input-text" id="racValueDate" name="accountLine.racValueDate" required="true" size="8" maxlength="12"  readonly="true"/></td> 
																</c:if> 
																
															</tr>
															<tr>
															<td align="right" class="listwhitetext"><fmt:message key="accountLine.recCurrencyRate" /></td>
														    <td align="left" class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right" key="accountLine.recCurrencyRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateRecRate('accountLine.recCurrencyRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" onblur="chkSelect('1');"/></td>	 
															<td width="" align="right" class="listwhitetext"><fmt:message key="accountLine.recRateExchange" /></td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.recRateExchange" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" readonly="${!contractType}" onchange="calculateRecRateByExchangerate();"/></td> 
															<td width="" align="right" class="listwhitetext"colspan="2"><fmt:message key="accountLine.actualRevenueForeign" /></td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.actualRevenueForeign" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
															</tr>
															
															<tr><td align="left" class="vertlinedata" colspan="16" style="padding-top:3px;"></td></tr>
															</c:if>
															<c:if test="${systemDefaultVatCalculation=='true'}">
															<tr>
															
															<td align="right" class="listwhitetext"></td>														    
															<td class="listwhitetext" align="left"><!--<input type="button" class="cssbuttonA" style="width:65px; height:20px" id="VATCalc" name="VATCalc" value="VAT Calc" onclick="VATCalculate(),checkVatForReadonlyFields('first');" disabled="disabled" />--></td>
															
															<td class="listwhitetext" width="" align="right"></td>
															<td align="left" class="listwhitetext" colspan="2">
															<table class="detailTabLabel" cellpadding="0" cellspacing="0">
															<tr>
															<td class="listwhitetext" width="48px" ></td>
															<td class="listwhitetext"></td>
															<td class="listwhitetext">&nbsp;</td>
															<td> 															
															 </td>
															
															</tr>
															</table>
															
															</td> 
															<td width="80px" align="right" class="listwhitetext" ></td>
                        									<td align="left" class="listwhitetext"></td>
                        									<td align="right" class="listwhitetext"></td>
                        									<td align="right" class="listwhitetext">VAT</td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.recVatAmt" id="accountLine.recVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
															</tr></c:if>
															
															<s:hidden name="accountLine.checkNew" />
															<s:hidden name="accountLine.basisNew" />
															<s:hidden name="accountLine.basisNewType" /> 
															<tr>  
<sec-auth:authComponent componentId="module.accountLine.section.sectionReceivedInvoiceDescription.edit">
	<%
	String sectionReceivedInvoiceDescription="true";
	int permission2  = (Integer)request.getAttribute("module.accountLine.section.sectionReceivedInvoiceDescription.edit" + "Permission");
 	if (permission2 > 2 ){
 		sectionReceivedInvoiceDescription = "false";
 		
 	}
  %>											
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.recNote" /></td>
																<c:if test="${!utsiRecAccDateFlag  && !utsiPayAccDateFlag}">
																<td colspan="4" align="left" class="listwhitetext"><s:textarea readonly="<%= sectionReceivedInvoiceDescription%>" name="accountLine.description" cssStyle="width:475px; height:35px;" rows="1" cols="59" tabindex="31" cssClass="textarea"/></td>
																</c:if>
																<c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
																<td colspan="4" align="left" class="listwhitetext"><s:textarea readonly="<%= sectionReceivedInvoiceDescription%>" name="accountLine.description" cssStyle="width:475px; height:35px;" rows="1" cols="59" tabindex="31" cssClass="textarea"/></td>
																</c:if>
</sec-auth:authComponent>                                       
                                                                <td align="right"> <div id="creditInvoice"><table><tr>
                                                                <td width="85px" align="right" class="listwhitetext">Credit Invoice #</td>
																</tr></table></div></td>
																<td align="left"><div id="creditInvoice1"><table cellpadding="0" cellspacing="0"><tr>
																<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.creditInvoiceNumber" size="8"   readonly="true" /></td>
																</tr></table></div></td>
																<td></td>
																<c:if test="${systemDefaultVatCalculation=='true'}">
                       									<td align="right" class="listwhitetext"><div id="qstRecVatAmtGlLabel">VAT</div></td>
															<td align="left" class="listwhitetext"><div id="qstRecVatAmtGlId"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.qstRecVatAmt" id="accountLine.qstRecVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></div></td> 
																</c:if>
															</tr> 
															<tr>
																<td width="103px" align="right" class="listwhitetext"><fmt:message key="accountLine.externalReference" /></td>
																<c:choose> 
                                                                 <c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && accountLine.paymentStatus=='PIR' }">
                                                                 <td align="left" class="listwhitetext" width="91px"><s:textfield  readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" key="accountLine.externalReference" 
																	size="8" maxlength="25" onchange="calReference(); " tabindex="32"/></td>
                                                                 </c:when>
                                                                 <c:otherwise>
																<td align="left" class="listwhitetext" width="86px"><s:textfield  readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" key="accountLine.externalReference" 
																	size="8" maxlength="25" onchange="calReference(); " tabindex="32"/></td>
																</c:otherwise></c:choose>
	<s:hidden name="calOpener" value="notOPen" />												    
	<sec-auth:authComponent componentId="module.accountLine.section.paymentStatus.edit">
	<script language="javascript" type="text/javascript">
		changeCalOpenarvalue();
	</script>
	<%
	String sectionPaymentStatus="true";
	int permissionPaymentStatus  = (Integer)request.getAttribute("module.accountLine.section.paymentStatus.edit" + "Permission");
 	if (permissionPaymentStatus > 2 ){
 		sectionPaymentStatus = "false";
 	}
  %> 													    
															    
															    <td align="left" colspan="3" class="listwhitetext" >
															    <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
															    <tr>
															    <c:choose> 
                                                                 <c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && accountLine.paymentStatus=='PIR' }">
                                                                  <td> <fmt:message key="accountLine.paymentStatus" /> 
															    <configByCorp:customDropDown 	listType="map" list="${paymentStatus}"   fieldValue="${accountLine.paymentStatus}"
																     attribute="id=paystatus tabindex='33'class=list-menu name=accountLine.paymentStatus style=width:75px  headerKey='' headerValue='' onchange=checkCMMAgentPIR();changeStatus();autoPopulateStatusDate(this);"/>		Rec.&nbsp;Amt&nbsp;<s:textfield  cssClass="input-text" cssStyle="text-align:right"
																		 name="accountLine.receivedAmount"
																		required="true"  size="3" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" readonly="<%= sectionPaymentStatus%>" tabindex="34"/>
																</td>
                                                                 </c:when>
                                                                 <c:otherwise>
															    <td> <fmt:message key="accountLine.paymentStatus" /> 
															    <configByCorp:customDropDown 	listType="map" list="${paymentStatus}"   fieldValue="${accountLine.paymentStatus}"
																     attribute="id=paystatus tabindex='33'class=list-menu name=accountLine.paymentStatus style=width:75px  headerKey='' headerValue='' onchange=changeStatus();autoPopulateStatusDate(this);"/>		Rec.&nbsp;Amt&nbsp;<s:textfield  cssClass="input-text" cssStyle="text-align:right"
																		 name="accountLine.receivedAmount"
																		required="true"  size="3" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" readonly="<%= sectionPaymentStatus%>" tabindex="34"/>
																</td>
																</c:otherwise></c:choose>
																</tr>
																</table>
																</td> 
																<c:if test="${not empty accountLine.statusDate}">
																<s:text id="accountLineFormattedValue" name="${FormDateValue1}"><s:param name="value" value="accountLine.statusDate" />
																</s:text>
																<td align="right" class="listwhitetext" colspan=""><fmt:message key="accountLine.statusDate" /></td>
																<td><s:textfield  cssClass="input-text"
																		id="statusDate" name="accountLine.statusDate"
																		value="%{accountLineFormattedValue}" size="8"
																		maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/>
																<% if (sectionPaymentStatus.equalsIgnoreCase("false")){%>
																<img id="statusDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>											
															    <%} %>
															    </td>
																</c:if>
																<c:if test="${empty accountLine.statusDate}">
																<td align="right" class="listwhitetext" colspan=""><fmt:message key="accountLine.statusDate" /></td>
																<td><s:textfield  cssClass="input-text"
																		id="statusDate" name="accountLine.statusDate"
																		required="true" size="8" maxlength="11"
																		onkeydown="return onlyDel(event,this)" readonly="true"/>
																	<% if (sectionPaymentStatus.equalsIgnoreCase("false")){%>
																	<img id="statusDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/> 
																	 <%} %> 
																	 </td>
																  </c:if> 
												</sec-auth:authComponent>
																<td></td> 
																<td></td>
																<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
																<td align="center" valign="bottom" class="listwhitetext"><fmt:message key="accountLine.distributionAmount" /></td>   
																</c:if>
																</tr> 
																<c:if test="${(fn1:indexOf(systemDefaultstorage,serviceOrder.job)>=0) || (fn1:indexOf(serviceOrder.serviceType,'TEN')>-1 && tenancyBillingVal=='Y')}">
																<tr> 
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.storageDateRangeFrom" /></td>  
																<c:if test="${not empty accountLine.storageDateRangeFrom}"> 
																<s:text id="accountLineFormattedValue" name="${FormDateValue1}">
																		<s:param name="value" value="accountLine.storageDateRangeFrom" />
																</s:text>
																<td>
																<s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text"
																		id="storageDateRangeFrom" name="accountLine.storageDateRangeFrom"
																		value="%{accountLineFormattedValue}" size="7"
																		maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/>
																<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
																   <img id="storageDateRangeFrom-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" />											
															   <%} %>
															    </td>
																</c:if>
																<c:if test="${empty accountLine.storageDateRangeFrom}">
																	<td>
																		<s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text"
																		id="storageDateRangeFrom" name="accountLine.storageDateRangeFrom"
																		required="true" size="7" maxlength="11"
																		onkeydown="return onlyDel(event,this)" onselect="calcDays()"/>
																	<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
																	 <img id="storageDateRangeFrom-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20  onclick="forDays(); return false;" /> 
																	 <%} %>
																	 </td>
																  </c:if>
																<td align="left" colspan="3" class="listwhitetext" >
															    <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
															    <tr>  
															    <td width="50"></td>
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.storageDateRangeTo" />&nbsp;</td> 
																	
																<c:if test="${not empty accountLine.storageDateRangeTo}">
																
																<s:text id="accountLineFormattedValue" name="${FormDateValue1}">
																		<s:param name="value" value="accountLine.storageDateRangeTo" />
																</s:text>
																<td>
																<s:textfield  cssClass="input-text"
																		id="storageDateRangeTo" name="accountLine.storageDateRangeTo"
																		value="%{accountLineFormattedValue}" size="6"
																		maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays();" readonly="true"/>
																
																   <img id="storageDateRangeTo-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false; " />											
															    </td>
																</c:if>
																<c:if test="${empty accountLine.storageDateRangeTo}">
																	<td>
																		<s:textfield  cssClass="input-text"
																		id="storageDateRangeTo" name="accountLine.storageDateRangeTo"
																		required="true" size="6" maxlength="11"
																		onkeydown="return onlyDel(event,this)" onselect="calcDays();" readonly="true" />
																	<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
																	 <img id="storageDateRangeTo-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false; " /> 
																	 <%} %> 
																	 </td>
																  </c:if>
																  <td  align="right" class="listwhitetext" colspan="2" width="38"><fmt:message
																	key="accountLine.storageDays" />&nbsp;</td>
																<td align="left" class="listwhitetext"><s:textfield
																	 cssClass="input-text"  cssStyle="text-align:right" id="noOfDays" name="accountLine.storageDays"
																	size="8" maxlength="9"  
																	tabindex="12" onselect="calcDays();" onchange="calcDays();"/></td>
																 </tr></table></td>
																<td align="right" class="listwhitetext" width="100"><fmt:message key="billing.onHand"/></td>
		    	                                                    <td align="left"><s:textfield name="accountLine.onHand" maxlength="15" cssStyle="text-align:right" onchange="onlyFloat(this);" required="true" cssClass="input-text" size="8" /></td>	 
																</tr>
																</c:if> 
																<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
														 			<tr>
														 			<td align="right" class="listwhitetext" colspan="4">Vanline Settle Date</td>
														 		     	<c:if test="${not empty  accountLine.vanlineSettleDate}">
							                                            <s:text id="accountLineFormattedValue" name="${FormDateValue1}"><s:param name="value" value="accountLine.vanlineSettleDate"/></s:text>
							                                            <td><s:textfield cssClass="input-text" id="SettleDate" name="accountLine.vanlineSettleDate" value="%{accountLineFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" /></td>
							                                           </c:if>
							                                            <c:if test="${empty accountLine.vanlineSettleDate}">
							                                           <td><s:textfield cssClass="input-text" id="SettleDate" name="accountLine.vanlineSettleDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" /></td>
							                                          </c:if>
														 			<td align="right" class="listwhitetext"><fmt:message key="accountLine.glType" /></td>
																	<td align="left" >
																	<s:select disabled="<%= sectionRreceivableDetail%>" cssClass="list-menu" headerKey="" headerValue="" name="accountLine.glType" cssStyle="width:85px" list="%{getGlTypeList}" onchange="changeStatus()" />
																	</td>
																	<td><input type="button" class="cssbuttonA" style="width:70px; height: 25px;"   value="Distribution" onclick="getDistributedAmtFromBCode();driverCommission();"/></td>
																	<td></td>
																	<td align="left" class="listwhitetext"><s:textfield  cssClass="input-text" cssStyle="text-align:right" key="accountLine.distributionAmount" size="8" maxlength="15" onkeydown="return onlyRateAllowed(event)" onblur="chkSelect('1');" onchange="chechCreditInvoice();driverCommission();"/></td>
																</tr>
																</c:if> 
														</sec-auth:authComponent>
													</table>