package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CompanySystemDefault;

public interface CompanySystemDefaultManager extends GenericManager<CompanySystemDefault, Long> {

	/*public List<CompanySystemDefault> findByCorpID(String sessionCorpID);*/

}
