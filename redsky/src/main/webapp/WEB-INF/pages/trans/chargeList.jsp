<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="chargeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='chargeList.heading'/>"/>    
   
<style>
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:5px;
	margin-top:-21px;
	!margin-top:-18px;
	padding:2px 0px;
	text-align:right;
	width:100%;
}
table-fc tbody th, .table tbody td {white-space:nowrap;}
.table tbody td a {background:transparent none repeat scroll 0% 50%;cursor:pointer;text-decoration:underline;}
.table td, .table th, .tableHeaderTable td {
border:1px solid #E0E0E0;
padding:0.4em;
}
</style>    
    <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['serviceForm10'].elements['charges.charge'].value = "";
	document.forms['serviceForm10'].elements['charges.description'].value = "";
	document.forms['serviceForm10'].elements['charges.gl'].value = "";
	document.forms['serviceForm10'].elements['charges.expGl'].value = "";
	document.forms['serviceForm10'].elements['comison'].value = "";
	}
</script>
<script language="javascript" type="text/javascript">

function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}


function confirmSubmit(targetElement, targetElement2){
	var agree=confirm("Please confirm that you want to delete this charge permanently?");
	var did = targetElement;
	var chargeCode=targetElement2;
	var contractName=document.forms['serviceForm10'].elements['contracts.contract'].value;
	var conId =document.forms['serviceForm10'].elements['contracts.id'].value;
	if (agree){
	return checkCount(chargeCode,contractName,did, conId);
	}else{
		return false;
	}
}

function checkCount(chargeCode,contractName, chargeId, contractId )
{
	 var url="testCount.html?ajax=1&decorator=simple&popup=true&chargeCode=" + encodeURI(chargeCode)+"&contractName=" + encodeURI(contractName);
     http2.open("GET", url, true);
     http2.onreadystatechange =function(){ handleHttpResponsetestCount(chargeId, contractId);};
     http2.send(null);
	
}

function handleHttpResponsetestCount(chargeId, contractId)
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results!='0')
                {
                	alert('Active Storage Shipments are against this charge so cannot be deleted.');
                	return false;
                }
                location.href="deleteCharge.html?id="+contractId+"&chagneId="+chargeId;
                return true;
             }
             
        }
function checkChargeStatus(targetElement, targetElement2,targetElement3){
	var did = targetElement;
	var chargeCode=targetElement2;
	if(targetElement3.checked){
		 var url="updateChargeStatus.html?decorator=simple&popup=true&soIdNum="+encodeURI(did)+"&status="+encodeURI(true);
		 http4.open("GET", url, true); 
	     http4.onreadystatechange = handleHttpResponseChargeStatus; 
	     http4.send(null);
	}else{
		 var url="updateChargeStatus.html?decorator=simple&popup=true&soIdNum="+encodeURI(did)+"&status="+encodeURI(false);
		 http5.open("GET", url, true); 
	     http5.onreadystatechange = handleHttpResponseChargeStatus; 
	     http5.send(null);
	}
}
function handleHttpResponseChargeStatus(){
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();
    var http5 = getHTTPObject();	
</script>
   <script language="javascript" type="text/javascript"> 
   function checkReadOnly(){
       var chkCorpId= '${contracts.owner}';
       var corpId='${chargeOrgId}'; 
       if(corpId!=chkCorpId){
       		document.forms['serviceForm10'].elements['add'].disabled = true;
       		document.forms['serviceForm10'].elements['copyCharges'].disabled = true;
       }
   }
   function addNewChargeMethod(){
	        var flag="";
	        var id="${id}";
	        try{
       		flag=document.forms['serviceForm10'].elements['countryFlexbilityCheck'].value;
	        }catch(e){}
	        location.href='addNewCharge.html?contractId='+id+'&countryFlexbilityCheck='+flag;
   }
   function addAndCopyNewChargeMethod(){
       var id="${contracts.id}";
       var contract="${contracts.contract}";
       location.href='addAndCopyCharges.html?id='+id+'&contract='+contract;
	}
 
   </script>
</head>

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="searchCharges" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="serviceForm10" name="serviceForm10" action="searchCharges" method="post">
<div id="Layer1" style="width: 100%;">
<s:hidden name="id" value="<%=request.getParameter("id")%>"/> 
<s:hidden name="countCharges" value="${countCharges}"/>
<c:set var="id" value="<%=request.getParameter("id")%>"/> 
<c:set var="cid" value="<%=request.getParameter("id")%>"/> 
<s:hidden name="contracts.id" value="${cid}"/>
<s:hidden name="contractId" value="${cid}"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.showCountryFlexbility">
<s:hidden name="countryFlexbilityCheck" value="YES"/>
</configByCorp:fieldVisibility>
<c:set var="fieldremove" value="NO"/>
<%-- <configByCorp:fieldVisibility componentId="component.charges.field.remove">
<c:set var="fieldremove" value="YES"/>
</configByCorp:fieldVisibility> --%>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table">
<thead>
<tr>
<th><fmt:message key="charges.charge"/></th>
<th><fmt:message key="charges.description"/></th>
<th><fmt:message key="charges.gl"/></th>
<th><fmt:message key="charges.expGl"/></th>
<th style="width:60px;"><fmt:message key="charges.commissionable"/></th>
<th>&nbsp;</th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td><s:textfield name="charges.charge" size="21" required="true" cssClass="input-text"  /></td>
			<td><s:textfield name="charges.description" size="55" required="true" cssClass="input-text" />
			<td><s:textfield name="charges.gl" size="15"  required="true" cssClass="input-text" />
			<td><s:textfield name="charges.expGl" size="15" required="true"  cssClass="input-text" /></td>
			<td><s:select  list="%{commissionDrop}" name="comison" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:65px;margin-right:18px;margin-top:2px;" />
			</td>
			<td>
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>			
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>



<c:set var="buttons">   
      <input type="button" class="cssbutton" name="add" style="width:55px;" onclick="addNewChargeMethod();" value="<fmt:message key="button.add"/>"/> 
      <c:if test="${countCharges == 0}">
      <input type="button" class="cssbutton1" name="copyCharges" style="width:255px;" onclick="addAndCopyNewChargeMethod();" value="<fmt:message  key="button.copyChargesfromanotherContract"/>" style="width:110px; height:25px"/> 
	  </c:if>
    
</c:set>


	
<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Charge List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li><!--
		    <li><a href="searchContracts.html"><span>Contract</span></a></li>
		     -->
		    <li><a href="editContract.html?id=${id}"><span>Contract Details</span></a></li>
		    <li><a href="searchContracts.html"><span>Contract List</span></a></li>
  		</ul>
		</div><div class="spn">&nbsp;</div><div style="padding-bottom:3px;"></div>
		
	<div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
    <table style="margin-bottom: 3px">
    <tr>
        <td align="right" class="listwhitetext">Contract</td>
  <td><s:textfield  name="contracts.contract" required="true" readonly="true" size="50" cssClass="input-textUpper"/></td>
  </tr>
  </table>
  
  </div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
</div>
<s:set name="chargess" value="chargess" scope="request"/>
</s:form>
 <s:form id="serviceForm105" name="serviceForm105" action="" method="post">  
<display:table name="chargess" class="table" requestURI="" id="chargeList" export="true" pagesize="50" defaultsort="1" style="!margin-top:3px; ">   
    <display:column property="charge" sortable="true" titleKey="charges.charge" href="editCharges.html?contractId=${id}" paramId="id" paramProperty="id"/>   
    <display:column property="description" sortable="true" titleKey="charges.description"/>
    <display:column property="bucket" sortable="true" titleKey="charges.bucket"/>
    <display:column property="gl" sortable="true" titleKey="charges.gl"/>
    <display:column property="expGl" sortable="true" titleKey="charges.expGl"/>
    <display:column property="priceType" sortable="true" title="Price Type"/>
    <display:column title="Commissionable">
    	<div align="center">
       		<s:checkbox name="charges.commissionable" value="${chargeList.commissionable }"  disabled="true"/>
       	</div>
    </display:column>
    <display:column property="commission" sortable="true" title="Comm%"/>
    <display:column property="updatedOn" sortable="true" title="Updated On" format="{0,date,dd-MMM-yyyy}"/>
    <display:column title="Status">
    <c:if test ="${chargeOrgId == contracts.owner}">
    	<div align="center">
       		<s:checkbox name="charges.status" value="${chargeList.status }" onclick="checkChargeStatus('${chargeList.id}','${chargeList.charge}',this);"/>
       	</div>
    </c:if>
    <c:if test ="${chargeOrgId != contracts.owner && contracts.published=='Y'}">
    	<div align="center">
       		<s:checkbox name="charges.status" value="${chargeList.status }" onclick="checkChargeStatus('${chargeList.id}','${chargeList.charge}',this);" disabled="true"/>
       	</div>
    </c:if>
    </display:column>
    
   <%-- <c:if test ="${chargeOrgId == contracts.owner && fieldremove=='YES'}">
    <display:column title="Remove" style="width: 15px;">
		<a>
			<img align="middle" title="" onclick="confirmSubmit('${chargeList.id}','${chargeList.charge}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</a>
	</display:column>	
    </c:if> --%>
    <display:setProperty name="paging.banner.item_name" value="charges"/>  
    <display:setProperty name="paging.banner.items_name" value="chargess"/>
  
    <display:setProperty name="export.excel.filename" value="Charges List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Charges List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Charges List.pdf"/>
</display:table>   
 <c:out value="${buttons}" escapeXml="false" />
 
 </div>
 </s:form>

<script type="text/javascript">   
try{checkReadOnly();}
	catch(e){}
    //highlightTableRows("chargeList");
   try{
    if("<%=session.getAttribute("chargeSession")%>"=='null')
   	{
   		document.forms['serviceForm10'].elements['charges.charge'].value='';
   	}
   	}
   	catch(e){}
   	
   	try{
   	if("<%=session.getAttribute("chargeSession")%>"!='null')
   	{
   		document.forms['serviceForm10'].elements['charges.charge'].value="<%=session.getAttribute("chargeSession")%>";
   	}
   		}
   	catch(e){}
   
   	try{
   	if("<%=session.getAttribute("chargeDescriptionSession")%>"=='null')
   	{
   		
   		document.forms['serviceForm10'].elements['charges.description'].value='';
   	}
   	}
   	catch(e){}
   	
   	try{
   	if("<%=session.getAttribute("chargeDescriptionSession")%>"!='null')
   	{	
   		
   		document.forms['serviceForm10'].elements['charges.description'].value="<%=session.getAttribute("chargeDescriptionSession")%>";
   	}
   	}
   	catch(e){}
   	
   	try{
   	   	if('<%=session.getAttribute("chargeCommissionableSession")%>'=='null')
   	   	{
   	   		document.forms['serviceForm10'].elements['chargeForm_charges_commissionable'].checked = false;
   	   	}
   		}
   		catch(e){}
   		try{
   			
   		if('<%=session.getAttribute("chargeCommissionableSession")%>'=='true')
   	   	{
   	   		document.forms['serviceForm10'].elements['chargeForm_charges_commissionable'].checked = true;
   	   	} 
   	   	}
   	   	catch(e){}
   	   	
   	 try{
   	   	if("<%=session.getAttribute("chargeGlSession")%>"=='null')
   	   	{
   	   		
   	   		document.forms['serviceForm10'].elements['charges.gl'].value='';
   	   	}
   	   	}
   	   	catch(e){}
   	   	
   	   	try{
   	   	if("<%=session.getAttribute("chargeGlSession")%>"!='null')
   	   	{	
   	   		
   	   		document.forms['serviceForm10'].elements['charges.gl'].value="<%=session.getAttribute("chargeGlSession")%>";
   	   	}
   	   	}
   	   	catch(e){}
   	   	
   	   	
   	 try{
   	   	if("<%=session.getAttribute("chargeExpGlSession")%>"=='null')
   	   	{
   	   		
   	   		document.forms['serviceForm10'].elements['charges.expGl'].value='';
   	   	}
   	   	}
   	   	catch(e){}
   	   	
   	   	try{
   	   	if("<%=session.getAttribute("chargeExpGlSession")%>"!='null')
   	   	{	
   	   		
   	   		document.forms['serviceForm10'].elements['charges.expGl'].value="<%=session.getAttribute("chargeExpGlSession")%>";
   	   	}
   	   	}
   	   	catch(e){}
   	   	var com ="${comison}";
   	   	if(com != ''){
   	   	 document.forms['serviceForm10'].elements['comison'].value =com;
   	   	}else{
   	 document.forms['serviceForm10'].elements['comison'].value ="All";
   	   	}
   </script> 
