package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.JobStatusDao;
import com.trilasoft.app.model.JobStatus;
import com.trilasoft.app.service.JobStatusManager;

public class JobStatusManagerImpl extends GenericManagerImpl<JobStatus, Long> implements JobStatusManager{

	JobStatusDao jobStatusDao;
	public JobStatusManagerImpl(JobStatusDao jobStatusDao) {
		super(jobStatusDao);
		this.jobStatusDao=jobStatusDao;
		
	}

	public List findMaximumId() {
		return jobStatusDao.findMaximumId();
	}

	public void increaseToTalCount(Long id, String sessionCorpID) {
		jobStatusDao.increaseToTalCount(id, sessionCorpID);
	}

	public void updatePresentCount(Long countTotalNumber, String sessionCorpID, Long id, Long totCount) {
		jobStatusDao.updatePresentCount(countTotalNumber, sessionCorpID, id, totCount);	
	}

	public List getTotalCount(String sessionCorpID, Long id) {
		return jobStatusDao.getTotalCount(sessionCorpID, id);
	}

	public List getPresentCount(String sessionCorpID, Long id) {
		return jobStatusDao.getPresentCount(sessionCorpID, id);
	}

	public List getList(String sessionCorpID) {
		return jobStatusDao.getList(sessionCorpID);
	}

	public List getStatus(String sessionCorpID) {
		return jobStatusDao.getStatus(sessionCorpID);
	}

	

	

}
