package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.SystemConfiguration;


public interface SystemConfigurationDao extends GenericDao<SystemConfiguration, Long> {   
      public List checkById(Long id);
      public List<SystemConfiguration>getList();
      public List<DataCatalog>searchByTableAndField(String tableName,String fieldName,String corpID,String description,String auditable,String defineByToDoRule,String isdateField,String visible,String charge) ;
      public List configurableTable(String b);
      public List configurableField(String tableName,String b);
      public List<DataCatalog>getMasterList(String corpID);
}