package com.trilasoft.app.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "accountprofile")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class AccountProfile extends BaseObject {

	private Long id;

	private String corpID;

	private Date createdOn;

	private String createdBy;

	private Date updatedOn;

	private String updatedBy;

	private String stage;

	private String type;

	private String industry;

	private String employees;

	private String revenue;

	private String country;

	private String source;

	private String owner;

	private String partnerCode;

	private String currency;
	
	private Boolean vip;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID", corpID).append("createdOn", createdOn).append("createdBy", createdBy).append("updatedOn", updatedOn)
		.append("updatedBy", updatedBy).append("stage", stage).append("type", type).append("industry", industry).append("employees", employees).append("revenue", revenue)
		.append("country", country).append("source", source).append("owner", owner).append("partnerCode", partnerCode).append("currency", currency).append("vip",vip).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AccountProfile))
			return false;
		AccountProfile castOther = (AccountProfile) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID, castOther.corpID).append(createdOn, castOther.createdOn).append(createdBy, castOther.createdBy).append(
				updatedOn, castOther.updatedOn).append(updatedBy, castOther.updatedBy).append(stage, castOther.stage).append(type, castOther.type).append(industry,
						castOther.industry).append(employees, castOther.employees).append(revenue, castOther.revenue).append(country, castOther.country).append(source, castOther.source)
						.append(owner, castOther.owner).append(partnerCode, castOther.partnerCode).append(currency, castOther.currency).append(vip, castOther.vip).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(createdOn).append(createdBy).append(updatedOn).append(updatedBy).append(stage).append(type).append(industry)
		.append(employees).append(revenue).append(country).append(source).append(owner).append(partnerCode).append(vip).append(currency).toHashCode();
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 30)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 15)
	public String getEmployees() {
		return employees;
	}

	public void setEmployees(String employees) {
		this.employees = employees;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 50)
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@Column(length = 15)
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Column(length = 25)
	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	@Column(length = 15)
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Column(length = 15)
	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	@Column(length = 15)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 15)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	
	@Column(length=25)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getVip() {
		return vip;
	}

	public void setVip(Boolean vip) {
		this.vip = vip;
	}
	
	
}
