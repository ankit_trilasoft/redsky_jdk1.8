package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;   
import java.util.Set;

import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import org.springframework.dao.DataAccessException;

import com.trilasoft.app.dao.ClaimLossTicketDao;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.ClaimLossTicket;   
import com.trilasoft.app.model.Loss;
   
  
public  class ClaimLossTicketDaoHibernate extends GenericDaoHibernate<ClaimLossTicket, Long> implements ClaimLossTicketDao {   
  
	Logger logger = Logger.getLogger(ClaimLossTicketDaoHibernate.class);
    public ClaimLossTicketDaoHibernate() {   
        super(ClaimLossTicket.class);   
    }   
    public List findClaimLossTickets(String lossNumber, Long claimNum, String corpID) {
    	try{
    	return getHibernateTemplate().find("from ClaimLossTicket where claimNumber = "+claimNum+"  and corpID='"+corpID+"' and lossNumber in ('"+lossNumber+"','a') group by ticket");
    	} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
    	}
    boolean exists;
	public boolean findTicketbyLoss(Long ticket, String seqNum, String lossNum){
		try{
		List clt = getHibernateTemplate().find("from ClaimLossTicket where sequenceNumber = '"+seqNum+"' and lossNumber in ('"+lossNum+"','a') and ticket=?", ticket);
		if(clt == null || clt.isEmpty()){
			exists = false;
		}else{
			exists = true;
		}
		return exists;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return exists;
	}
	boolean ticketExists;
	public boolean findLossTicket(Long ticket, String seqNum, Long claimNum, String lossNum){
		try{
		List clt = getHibernateTemplate().find("from ClaimLossTicket where sequenceNumber = '"+seqNum+"' and claimNumber = "+claimNum+" and lossNumber = '"+lossNum+"' and ticket=?", ticket);
		if(clt == null || clt.isEmpty()){
			ticketExists = false;
		}else{
			ticketExists = true;
		}
		return ticketExists;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return ticketExists;
	}
	public int applyTicketToAllLoss(Long ticket, String seqNum, String lossNum){
		try{
		return getHibernateTemplate().bulkUpdate("update ClaimLossTicket set isAppliedToAllLoss = true where sequenceNumber = '"+seqNum+"' and ticket=?", ticket);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return 0;
	}
	public List<Claim> findClaimBySeqNum(String sequenceNumber){
		
		try {
			return getHibernateTemplate().find("from Claim where sequenceNumber = '"+sequenceNumber+"'");
		}  catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
			
	}
	public List<Loss> findLossBySeqNum(String sequenceNumber){
		try {
			return getHibernateTemplate().find("from Loss where sequenceNumber = '"+sequenceNumber+"'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return null;
		}
	public List findCustLossTickets(Long ticket, String seqNum, boolean isAppliedToAllLoss){
		try{
		return getHibernateTemplate().find("select distinct ticket from ClaimLossTicket  where isAppliedToAllLoss = true and sequenceNumber=?", seqNum);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return null;
		}
	public List findCustClmLossTickets(String seqNum, String corpID){
		try{
		return getHibernateTemplate().find("select distinct ticket from ClaimLossTicket  where isAppliedToAllLoss = true and sequenceNumber='"+seqNum+"' and corpID='"+corpID+"' " );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return null;
		}
	
	public List findCustClmLossPercent(Long claimNum, Long ticket, String corpID){
		try{
		String query="SELECT percent FROM claimlossticket where claimNumber = "+claimNum+" and  isAppliedToAllLoss=true and (percent <> null or percent <> '')  and corpID='"+corpID+"' and ticket="+ticket+" ";
	    List clPList= this.getSession().createSQLQuery(query).list();	
    	return clPList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return null;
		}
	public List findCustClmLossChargeBack(Long claimNum, Long ticket, String corpID){
		try{
		String query="SELECT chargeBack FROM claimlossticket where claimNumber = "+claimNum+" and isAppliedToAllLoss=true and (chargeBack <> null or chargeBack <> '') and corpID='"+corpID+"' and ticket="+ticket+" ";
	    List clCBList= this.getSession().createSQLQuery(query).list();	
    	return clCBList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return null;
		}

	
	public int applyTicketToAllLossOldAlso(Long ticket, Long claimNum, String lossNum, String corpID) {
		try{
		int updateCLT = 0;
		List cLPercent= getHibernateTemplate().find("SELECT percent FROM ClaimLossTicket where lossNumber = '"+lossNum+"' and ticket="+ticket+" and corpID='"+corpID+"' ");
		List cLChargeBack = getHibernateTemplate().find("SELECT chargeBack FROM ClaimLossTicket where lossNumber = '"+lossNum+"' and ticket="+ticket+" and corpID='"+corpID+"' ");
		//System.out.println("n\n\n\n\n\n\n\njjjjjjjjjjjjjjjjjjjjjjjjjjjjjcLPercent"+cLPercent);
		//System.out.println("n\n\n\n\n\n\n\njjjjjjjjjjjjjjjjjjjjjjjjjjjjjcLChargeBack"+cLChargeBack);
		 if((!cLPercent.isEmpty() && cLPercent.get(0) != null  && !cLPercent.get(0).equals("")) || (!cLChargeBack.isEmpty() && cLChargeBack.get(0) != null && !cLChargeBack.get(0).equals("")))
	    		{
			updateCLT=getHibernateTemplate().bulkUpdate("update ClaimLossTicket set isAppliedToAllLoss = true, chargeBack = '"+cLChargeBack.get(0).toString().replace("[", "").replace("]", "")+"', percent = '"+cLPercent.get(0).toString().replace("[", "").replace("]", "")+"' where claimNumber = "+claimNum+" and ticket="+ticket+" and corpID='"+corpID+"' ");
	    		}
		/*
		 
		 else if(((cLPercent.isEmpty() || cLPercent == null) && (!cLChargeBack.isEmpty() && cLChargeBack != null))
 		{
		updateCLT=getHibernateTemplate().bulkUpdate("update ClaimLossTicket set isAppliedToAllLoss = true, chargeBack = '"+cLChargeBack.get(0).toString().replace("[", "").replace("]", "")+"', percent = '' where claimNumber = "+claimNum+" and ticket="+ticket+" and corpID='"+corpID+"' ");
 		}
		
		 
		 else if(((!cLPercent.isEmpty()) && cLPercent != null ) && (cLChargeBack.isEmpty()) || cLChargeBack == null)
 		{
		updateCLT=getHibernateTemplate().bulkUpdate("update ClaimLossTicket set isAppliedToAllLoss = true, chargeBack = '', percent = '"+cLPercent.get(0).toString().replace("[", "").replace("]", "")+"' where claimNumber = "+claimNum+" and ticket="+ticket+" and corpID='"+corpID+"' ");
 		}
		 */
		
		 else if((cLPercent.isEmpty() || cLPercent.get(0) == null || cLPercent.get(0).equals("")) && (cLChargeBack.isEmpty() || cLChargeBack.get(0) == null || cLChargeBack.get(0).equals("")))
	 		{
			updateCLT=getHibernateTemplate().bulkUpdate("update ClaimLossTicket set isAppliedToAllLoss = true, chargeBack = '', percent = '' where claimNumber = "+claimNum+" and ticket="+ticket+" and corpID='"+corpID+"' ");
	 		}
		return updateCLT;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return 0;
		}
	public List<Loss> findLossByClaimNum(Long claimNum, String corpID){
		try{
		return getHibernateTemplate().find("from Loss where claimNumber = "+claimNum+"  and (lossAction <> 'C' and lossAction <> 'D') and corpID='"+corpID+"'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error executing query"+ e,e);
		}
		return null;
		}
	public void removeCLTByAction(String lossNum, String corpID){
		try{
		getHibernateTemplate().bulkUpdate("delete FROM ClaimLossTicket where lossNumber = '"+lossNum+"' and corpID='"+corpID+"'");		
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		
		}
}  

