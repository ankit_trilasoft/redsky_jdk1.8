// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`company` ADD COLUMN `mmStartDate` DATETIME AFTER `cportalBrandingURL`,
ADD COLUMN `mmEndDate` DATETIME AFTER `mmStartDate`,
ADD COLUMN `mmSubscriptionAmt` DECIMAL(19,2) AFTER `mmEndDate`,
ADD COLUMN `mmClientID` VARCHAR(10) AFTER `mmSubscriptionAmt`,
ADD COLUMN `enableMobileMover` bit(1) DEFAULT b'0' AFTER `mmClientID`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN (
`estimateSellQuantity` DECIMAL(19,2) DEFAULT NULL ,
`revisionSellQuantity` DECIMAL(19,2) DEFAULT NULL ,
`estimateDiscount` DECIMAL(6,2) DEFAULT 0.00 ,
`actualDiscount` DECIMAL(6,2) DEFAULT 0.00 ,
`revisionDiscount` DECIMAL(6,2) DEFAULT 0.00);

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'RLS', 'Rental Search', 5, 'SERVICE','en','Relo','dspDetails.RLS_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'CAT', 'Candidate Assessment', 5, 'SERVICE','en','Relo','dspDetails.CAT_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'CLS', 'Closing Services', 5, 'SERVICE','en','Relo','dspDetails.CLS_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'CHS', 'Comparable Housing Study', 5, 'SERVICE','en','Relo','dspDetails.CHS_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DPS', 'Departure Services', 5, 'SERVICE','en','Relo','dspDetails.DPS_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'HSM', 'Home Sale Marketing Assistance', 5, 'SERVICE','en','Relo','dspDetails.HSM_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'PDT', 'Policy Development', 5, 'SERVICE','en','Relo','dspDetails.PDT_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'RCP', 'Relocation Cost Projections', 5, 'SERVICE','en','Relo','dspDetails.RCP_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'SPA', 'Spousal Assistance', 5, 'SERVICE','en','Relo','dspDetails.SPA_serviceEndDate',now(),'dkumar',now(),'dkumar'),
('HSRG', 'TCS', 'Technology Solutions', 5, 'SERVICE','en','Relo','dspDetails.TCS_serviceEndDate',now(),'dkumar',now(),'dkumar');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'DsRentalSearch', 'DsRentalSearch', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsCandidateAssessment', 'DsCandidateAssessment', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsClosingServices', 'DsClosingServices', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsComparableHousingStudy', 'DsComparableHousingStudy', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsDepartureServices', 'DsDepartureServices', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsHomeSaleMarketingAssistance', 'DsHomeSaleMarketingAssistance', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsPolicyDevelopment', 'DsPolicyDevelopment', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsRelocationCostProjections', 'DsRelocationCostProjections', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsSpousalAssistance', 'DsSpousalAssistance', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
('HSRG', 'DsTechnologySolutions', 'DsTechnologySolutions', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar');

ALTER TABLE `redsky`.`dspdetails`
    ADD COLUMN `RLS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `REP_displyOtherVendorCode`,
    ADD COLUMN `RLS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `RLS_vendorCode`,
    ADD COLUMN `RLS_serviceStartDate` DATETIME DEFAULT NULL AFTER `RLS_vendorName`,
    ADD COLUMN `RLS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `RLS_serviceStartDate`,
    ADD COLUMN `RLS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `RLS_vendorCodeEXSO`,
    ADD COLUMN `RLS_serviceEndDate` DATETIME DEFAULT NULL AFTER `RLS_vendorContact`,
    ADD COLUMN `RLS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `RLS_serviceEndDate`,
    ADD COLUMN `RLS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `RLS_vendorEmail`,
    ADD COLUMN `CAT_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `RLS_displyOtherVendorCode`,
    ADD COLUMN `CAT_vendorName` VARCHAR(225) DEFAULT NULL AFTER `CAT_vendorCode`,
    ADD COLUMN `CAT_serviceStartDate` DATETIME DEFAULT NULL AFTER `CAT_vendorName`,
    ADD COLUMN `CAT_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `CAT_serviceStartDate`,
    ADD COLUMN `CAT_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `CAT_vendorCodeEXSO`,
    ADD COLUMN `CAT_serviceEndDate` DATETIME DEFAULT NULL AFTER `CAT_vendorContact`,
    ADD COLUMN `CAT_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `CAT_serviceEndDate`,
    ADD COLUMN `CAT_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `CAT_vendorEmail`,
    ADD COLUMN `CLS_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `CAT_displyOtherVendorCode`,
    ADD COLUMN `CLS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `CLS_vendorCode`,
    ADD COLUMN `CLS_serviceStartDate` DATETIME DEFAULT NULL AFTER `CLS_vendorName`,
    ADD COLUMN `CLS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `CLS_serviceStartDate`,
    ADD COLUMN `CLS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `CLS_vendorCodeEXSO`,
    ADD COLUMN `CLS_serviceEndDate` DATETIME DEFAULT NULL AFTER `CLS_vendorContact`,
    ADD COLUMN `CLS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `CLS_serviceEndDate`,
    ADD COLUMN `CLS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `CLS_vendorEmail`,
    ADD COLUMN `CHS_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `CLS_displyOtherVendorCode`,
    ADD COLUMN `CHS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `CHS_vendorCode`,
    ADD COLUMN `CHS_serviceStartDate` DATETIME DEFAULT NULL AFTER `CHS_vendorName`,
    ADD COLUMN `CHS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `CHS_serviceStartDate`,
    ADD COLUMN `CHS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `CHS_vendorCodeEXSO`,
    ADD COLUMN `CHS_serviceEndDate` DATETIME DEFAULT NULL AFTER `CHS_vendorContact`,
    ADD COLUMN `CHS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `CHS_serviceEndDate`,
    ADD COLUMN `CHS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `CHS_vendorEmail`,
    ADD COLUMN `DPS_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `CHS_displyOtherVendorCode`,
    ADD COLUMN `DPS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `DPS_vendorCode`,
    ADD COLUMN `DPS_serviceStartDate` DATETIME DEFAULT NULL AFTER `DPS_vendorName`,
    ADD COLUMN `DPS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `DPS_serviceStartDate`,
    ADD COLUMN `DPS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `DPS_vendorCodeEXSO`,
    ADD COLUMN `DPS_serviceEndDate` DATETIME DEFAULT NULL AFTER `DPS_vendorContact`,
    ADD COLUMN `DPS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `DPS_serviceEndDate`,
    ADD COLUMN `DPS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `DPS_vendorEmail`,
    ADD COLUMN `HSM_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `DPS_displyOtherVendorCode`,
    ADD COLUMN `HSM_vendorName` VARCHAR(225) DEFAULT NULL AFTER `HSM_vendorCode`,
    ADD COLUMN `HSM_serviceStartDate` DATETIME DEFAULT NULL AFTER `HSM_vendorName`,
    ADD COLUMN `HSM_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `HSM_serviceStartDate`,
    ADD COLUMN `HSM_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `HSM_vendorCodeEXSO`,
    ADD COLUMN `HSM_serviceEndDate` DATETIME DEFAULT NULL AFTER `HSM_vendorContact`,
    ADD COLUMN `HSM_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `HSM_serviceEndDate`,
    ADD COLUMN `HSM_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `HSM_vendorEmail`,
    ADD COLUMN `PDT_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `HSM_displyOtherVendorCode`,
    ADD COLUMN `PDT_vendorName` VARCHAR(225) DEFAULT NULL AFTER `PDT_vendorCode`,
    ADD COLUMN `PDT_serviceStartDate` DATETIME DEFAULT NULL AFTER `PDT_vendorName`,
    ADD COLUMN `PDT_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `PDT_serviceStartDate`,
    ADD COLUMN `PDT_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `PDT_vendorCodeEXSO`,
    ADD COLUMN `PDT_serviceEndDate` DATETIME DEFAULT NULL AFTER `PDT_vendorContact`,
    ADD COLUMN `PDT_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `PDT_serviceEndDate`,
    ADD COLUMN `PDT_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `PDT_vendorEmail`,
    ADD COLUMN `RCP_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `PDT_displyOtherVendorCode`,
    ADD COLUMN `RCP_vendorName` VARCHAR(225) DEFAULT NULL AFTER `RCP_vendorCode`,
    ADD COLUMN `RCP_serviceStartDate` DATETIME DEFAULT NULL AFTER `RCP_vendorName`,
    ADD COLUMN `RCP_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `RCP_serviceStartDate`,
    ADD COLUMN `RCP_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `RCP_vendorCodeEXSO`,
    ADD COLUMN `RCP_serviceEndDate` DATETIME DEFAULT NULL AFTER `RCP_vendorContact`,
    ADD COLUMN `RCP_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `RCP_serviceEndDate`,
    ADD COLUMN `RCP_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `RCP_vendorEmail`,
    ADD COLUMN `SPA_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `RCP_displyOtherVendorCode`,
    ADD COLUMN `SPA_vendorName` VARCHAR(225) DEFAULT NULL AFTER `SPA_vendorCode`,
    ADD COLUMN `SPA_serviceStartDate` DATETIME DEFAULT NULL AFTER `SPA_vendorName`,
    ADD COLUMN `SPA_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `SPA_serviceStartDate`,
    ADD COLUMN `SPA_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `SPA_vendorCodeEXSO`,
    ADD COLUMN `SPA_serviceEndDate` DATETIME DEFAULT NULL AFTER `SPA_vendorContact`,
    ADD COLUMN `SPA_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `SPA_serviceEndDate`,
    ADD COLUMN `SPA_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `SPA_vendorEmail`,
    ADD COLUMN `TCS_vendorCode` VARCHAR(225) DEFAULT NULL AFTER `SPA_displyOtherVendorCode`,
    ADD COLUMN `TCS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `TCS_vendorCode`,
    ADD COLUMN `TCS_serviceStartDate` DATETIME DEFAULT NULL AFTER `TCS_vendorName`,
    ADD COLUMN `TCS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `TCS_serviceStartDate`,
    ADD COLUMN `TCS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `TCS_vendorCodeEXSO`,
    ADD COLUMN `TCS_serviceEndDate` DATETIME DEFAULT NULL AFTER `TCS_vendorContact`,
    ADD COLUMN `TCS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `TCS_serviceEndDate`,
    ADD COLUMN `TCS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `TCS_vendorEmail`;

ALTER TABLE `redsky`.`serviceorder`
    ADD COLUMN `RLS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `REP_vendorCode`,
    ADD COLUMN `CAT_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `RLS_vendorCode`,
    ADD COLUMN `CLS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `CAT_vendorCode`,
    ADD COLUMN `CHS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `CLS_vendorCode`,
    ADD COLUMN `DPS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `CHS_vendorCode`,
    ADD COLUMN `HSM_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `DPS_vendorCode`,
    ADD COLUMN `PDT_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `HSM_vendorCode`,
    ADD COLUMN `RCP_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `PDT_vendorCode`,
    ADD COLUMN `SPA_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `RCP_vendorCode`,
    ADD COLUMN `TCS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `SPA_vendorCode`,
    ADD COLUMN `MTS_vendorCode` VARCHAR(25) DEFAULT NULL,
    ADD COLUMN `DSS_vendorCode` VARCHAR(25) DEFAULT NULL;

ALTER TABLE `redsky`.`workticket` ADD COLUMN(`orderId` VARCHAR(45) NULL, `invoiceNumber` VARCHAR(25) DEFAULT NULL,`originLongCarry` VARCHAR(200) ,ADD COLUMN `destinationLongCarry` VARCHAR(200);

CREATE TABLE `redsky`.`inventorylocation` (
`id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
`workticket` VARCHAR(10) NULL ,
`locationType` VARCHAR(100) NULL ,
`shipNumber` VARCHAR(25) NULL ,
`sequenceNumber` VARCHAR(20) NULL ,
`corpId` VARCHAR(5) NULL ,
`createdOn` DATETIME NULL ,
`updatedOn` DATETIME NULL ,
`createdBy` VARCHAR(80) NULL ,
`updatedBy` VARCHAR(80) NULL ,
`imageId` BIGINT(20) NULL ,
PRIMARY KEY (`id`) );

CREATE TABLE `redsky`.`room` (
`id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
`room` VARCHAR(100) NULL ,
`roomDesc` VARCHAR(2000) NULL ,
`imageId` BIGINT(20) NULL ,
`corpId` VARCHAR(5) NULL ,
`createdOn` DATETIME NULL ,
`createdBy` VARCHAR(80) NULL ,
`updatedOn` DATETIME NULL ,
`updatedBy` VARCHAR(80) NULL ,
`locationId` BIGINT(20) NULL ,
PRIMARY KEY (`id`) );

CREATE TABLE `redsky`.`workticketinventorydata` (
`id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
`item` VARCHAR(100) NULL ,
`itemDesc` VARCHAR(2000) NULL ,
`weight` DECIMAL(6,2) NULL ,
`volume` DECIMAL(6,2) NULL ,
`quantity` INT(3) NULL ,
`corpid` VARCHAR(5) NULL ,
`createdOn` DATETIME NULL ,
`createdBy` VARCHAR(80) NULL ,
`updatedOn` DATETIME NULL ,
`updatedBy` VARCHAR(80) NULL ,
`roomId` BIGINT(20) NULL ,
`locationId` BIGINT(20) NULL ,
`barCode` VARCHAR(15) NULL ,
PRIMARY KEY (`id`) );

ALTER TABLE `redsky`.`inventorypath` ADD COLUMN `workticketInventoryDataId` BIGINT(20) NULL AFTER `inventoryPackingId` , ADD COLUMN `roomId` BIGINT(20) NULL AFTER `workticketInventoryDataId` , ADD COLUMN `locationId` BIGINT(20) NULL AFTER `roomId` ;

ALTER TABLE `redsky`.`workticketinventorydata` ADD COLUMN `shipNumber` VARCHAR(15) NULL AFTER `barCode` ;

ALTER TABLE `redsky`.`room` ADD COLUMN `shipNumber` VARCHAR(15) NULL AFTER `locationId` ;

ALTER TABLE `redsky`.`workticketinventorydata` ADD COLUMN `imageId` BIGINT(20) NULL AFTER `shipNumber` ;

ALTER TABLE `redsky`.`room` ADD COLUMN `floor` VARCHAR(15) NULL AFTER `shipNumber` , ADD COLUMN `comment` VARCHAR(100) NULL AFTER `floor` ;

ALTER TABLE `redsky`.`room` ADD COLUMN `hasDamage` BIT(1) NULL DEFAULT false ;

ALTER TABLE `redsky`.`workticketinventorydata` DROP COLUMN `imageId` , DROP COLUMN `roomId` , ADD COLUMN `oItemRoomId` BIGINT(20) NULL AFTER `shipNumber` , ADD COLUMN `oItemCondition` VARCHAR(45) NULL AFTER `oItemRoomId` , ADD COLUMN `oItemComment` VARCHAR(100) NULL AFTER `oItemCondition` , ADD COLUMN `oItemImageId` BIGINT(20) NULL AFTER `oItemComment` , ADD COLUMN `oPacker` VARCHAR(45) NULL AFTER `oItemImageId` , ADD COLUMN `dItemRoomId` BIGINT(20) NULL AFTER `oPacker` , ADD COLUMN `dItemCondition` VARCHAR(45) NULL AFTER `dItemRoomId` , ADD COLUMN `dItemComment` VARCHAR(100) NULL AFTER `dItemCondition` , ADD COLUMN `dItemImageId` BIGINT(20) NULL AFTER `dItemComment` , ADD COLUMN `dPacker` VARCHAR(45) NULL AFTER `dItemImageId` ;

ALTER TABLE `redsky`.`workticketinventorydata` ADD COLUMN `delivered` VARCHAR(5) NULL AFTER `dPacker` ;
 
ALTER TABLE `redsky`.`billing` ADD COLUMN (`billingInsurancePayableCurrency` VARCHAR(3) DEFAULT NULL,`billName` VARCHAR(255),`locationAgentCode` VARCHAR(8) NULL , 
ADD COLUMN `locationAgentName` VARCHAR(255) NULL);

ALTER TABLE `redsky`.`workticketinventorydata` ADD COLUMN `cartonContent` VARCHAR(200) NULL AFTER `delivered` ;

ALTER TABLE `redsky`.`workticketinventorydata` CHANGE COLUMN `oItemCondition` `oItemCondition` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `redsky`.`workticketinventorydata` CHANGE COLUMN `dItemCondition` `dItemCondition` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `RNT_monthlyRentalAllowanceCurrency` VARCHAR(3) DEFAULT NULL AFTER `REP_permitStartDate`,
ADD COLUMN `RNT_securityDepositCurrency` VARCHAR(3) DEFAULT NULL AFTER  `RNT_monthlyRentalAllowanceCurrency`;
 
ALTER TABLE `redsky`.`dspdetails` CHANGE COLUMN `WOP_visaExpiryDate`  `WOP_permitExpiryDate` DATETIME DEFAULT NULL;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `pricingRevision` BIT(1) DEFAULT b'0' AFTER `acctDisplayRevision`,
ADD COLUMN `pricingActual` BIT(1) DEFAULT b'0' AFTER `pricingRevision`;

ALTER TABLE `redsky`.`crew` ADD COLUMN `integrationTool` VARCHAR(25) NULL AFTER `licenceNumber` ;

ALTER TABLE `redsky`.`notes` ADD COLUMN `requestDate` DATETIME AFTER `memoLastUploadedDate`,
ADD COLUMN `submittedDate` DATETIME AFTER `requestDate`,
ADD COLUMN `actualValue` DECIMAL(19,2) DEFAULT 0.00 AFTER `submittedDate`,
ADD COLUMN `requestedValue` DECIMAL(19,2) DEFAULT 0.00;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `RNT_checkInDate` DATETIME DEFAULT NULL
AFTER `RNT_securityDepositCurrency`;

ALTER TABLE `redsky`.`notes` MODIFY COLUMN `noteStatus` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `redsky`.`t20VisionLogInfo` ADD COLUMN `notProcess` VARCHAR(450) DEFAULT '' AFTER `checkLHF`;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'MTS', 'Mortgage Services', 5, 'SERVICE','en','Relo','dspDetails.MTS_serviceEndDate',now(),'dkumar',now(),'dkumar');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'DsMortgageServices', 'DsMortgageServices', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar');

ALTER TABLE `redsky`.`dspdetails`
    ADD COLUMN `MTS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `RNT_securityDepositCurrency`,
    ADD COLUMN `MTS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `MTS_vendorCode`,
    ADD COLUMN `MTS_serviceStartDate` DATETIME DEFAULT NULL AFTER `MTS_vendorName`,
    ADD COLUMN `MTS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `MTS_serviceStartDate`,
    ADD COLUMN `MTS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `MTS_vendorCodeEXSO`,
    ADD COLUMN `MTS_serviceEndDate` DATETIME DEFAULT NULL AFTER `MTS_vendorContact`,
    ADD COLUMN `MTS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `MTS_serviceEndDate`,
    ADD COLUMN `MTS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `MTS_vendorEmail`,
    ADD COLUMN `MTS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `MTS_paymentResponsibility`;

 Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,bucket)values
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','RLS_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','CAT_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','CLS_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','CHS_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','DPS_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','HSM_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','PDT_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','RCP_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','SPA_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','TCS_vendorCode'),
('HSRG', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'dkumar',now(),'dkumar','MTS_vendorCode');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Employer', 'Employer', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Employee', 'Employee', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Managed Lump Sum', 'Managed Lump Sum', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Highstar Relo', 'Highstar Relo', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar');

ALTER TABLE `redsky`.`dspdetails`
    ADD COLUMN `RLS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `MTS_displyOtherVendorCode`,
    ADD COLUMN `CAT_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `RLS_paymentResponsibility`,
    ADD COLUMN `CLS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `CAT_paymentResponsibility`,
    ADD COLUMN `CHS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `CLS_paymentResponsibility`,
    ADD COLUMN `DPS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `CHS_paymentResponsibility`,
    ADD COLUMN `HSM_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `DPS_paymentResponsibility`,
    ADD COLUMN `PDT_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `HSM_paymentResponsibility`,
    ADD COLUMN `RCP_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `PDT_paymentResponsibility`,
    ADD COLUMN `SPA_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `RCP_paymentResponsibility`,
    ADD COLUMN `TCS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `SPA_paymentResponsibility`;

ALTER TABLE `redsky`.`checklistresult` ADD COLUMN
(`checkResultForDel` VARCHAR(45) DEFAULT NULL ,`toDoResultId` BIGINT(20) DEFAULT NULL,`docType` VARCHAR(65) DEFAULT NULL,`ruleNumber` VARCHAR(10) DEFAULT NULL,rolelist varchar (65),checkedUserName varchar (82),billToName varchar (82));

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','MM-DEVICE',5,true,now(),'Gautam',now(),'Gautam',1,1,'');

ALTER TABLE `redsky`.`crew` ADD COLUMN `deviceNumber` VARCHAR(100) NULL AFTER `integrationTool` ;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','INTEGRATION-TOOL',5,true,now(),'Gautam',now(),'Gautam',1,1,'');

ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `DPS_serviceType` VARCHAR(100) AFTER `mailServiceType`,
 ADD COLUMN `DPS_completionDate` DATETIME AFTER `DPS_serviceType`,
 ADD COLUMN `DPS_comment` TEXT AFTER `DPS_completi
 ADD COLUMN `HSM_brokerCode` VARCHAR(45) AFTER `DPS_onDate`,comment`,
 ADD COLUMN `HSM_brokerName` VARCHAR(100) AFTER `HSM_brokerCode`,
 ADD COLUMN `HSM_brokerContact` VARCHAR(50) AFTER `HSM_brokerName`,
 ADD COLUMN `HSM_brokerEmail` VARCHAR(45) AFTER `HSM_brokerContact`,
 ADD COLUMN `HSM_status` VARCHAR(45) AFTER `HSM_brokerEmail`,
 ADD COLUMN `HSM_comment` TEXT AFTER `HSM_status`,
 ADD COLUMN `HSM_marketingPlanNBMAReceived` DATETIME AFTER `HSM_comment`,
 ADD COLUMN `PDT_serviceType` VARCHAR(100) AFTER `HSM_marketingPlanNBMAReceived`,
 ADD COLUMN `PDT_budget` DECIMAL(19,2) AFTER `PDT_serviceType`,
 ADD COLUMN `RCP_estimateCost` DECIMAL(19,2) AFTER `PDT_budget`,
 ADD COLUMN `RCP_actualCost` DECIMAL(19,2) AFTER `RCP_estimateCost`,
 ADD COLUMN `MTS_lender` VARCHAR(100) AFTER `RCP_actualCost`,
 ADD COLUMN `MTS_initialContact` DATETIME AFTER `MTS_lender`,
 ADD COLUMN `MTS_status` VARCHAR(45) AFTER `MTS_initialContact`,
 ADD COLUMN `MTS_mortgageAmount` DECIMAL(19,2) AFTER `MTS_status`,
 ADD COLUMN `MTS_mortgageRate` DECIMAL(19,2) AFTER `MTS_mortgageAmount`,
 ADD COLUMN `MTS_mortgageTerm` VARCHAR(5) AFTER `MTS_mortgageRate`;

CREATE TABLE `redsky`.`worktickettimemanagement` (
  `id` BIGINT(20) UNSIGNED AUTO_INCREMENT,
  `timeRequirements` VARCHAR(82),
  `startTime` VARCHAR(8),
  `endtime` VARCHAR(8),
  `workTicketId` BIGINT(20) UNSIGNED,
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `updatedBy` VARCHAR(82),
  `createdOn` DATETIME DEFAULT 0,
  `updatedOn` DATETIME,
  `ticket` BIGINT(20),
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`t20VisionLogInfo` ADD COLUMN `notProcess` VARCHAR(450) DEFAULT '' AFTER `checkLHF`;

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `invoiceSeqNumber` VARCHAR(50) DEFAULT '' AFTER `payMethod`;

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Lease Negotiation', 'Lease Negotiation', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'), 
('HSRG', 'Utilities Disconnect', 'Utilities Disconnect', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Departure Documentation', 'Departure Documentation', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Heath Services', 'Heath Services', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Other', 'Other', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('HSRG','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Initial Contact', 'Initial Contact', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'), 
('HSRG', 'Listing', 'Listing', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Showing', 'Showing', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Offer', 'Offer', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Sold', 'Sold', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('HSRG','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'New Policy', 'New Policy', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'), 
('HSRG', 'New Program', 'New Program', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Update Policy', 'Update Policy', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Update Program', 'Update Program', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Benchmarking Review', 'Benchmarking Review', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('HSRG','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Approved', 'Approved', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar'), 
('HSRG', 'Denied', 'Denied', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Alternate Vendor', 'Alternate Vendor', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('HSRG','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

//As per dilip  
Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', 'Lease Negotiation', 'Lease Negotiation', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'), 
('VOER', 'Utilities Disconnect', 'Utilities Disconnect', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Departure Documentation', 'Departure Documentation', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Heath Services', 'Heath Services', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Other', 'Other', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('VOER','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', 'Initial Contact', 'Initial Contact', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'), 
('VOER', 'Listing', 'Listing', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Showing', 'Showing', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Offer', 'Offer', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Sold', 'Sold', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('VOER','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', 'New Policy', 'New Policy', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'), 
('VOER', 'New Program', 'New Program', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Update Policy', 'Update Policy', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Update Program', 'Update Program', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Benchmarking Review', 'Benchmarking Review', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('VOER','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', 'Approved', 'Approved', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar'), 
('VOER', 'Denied', 'Denied', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Alternate Vendor', 'Alternate Vendor', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'Lease Negotiation', 'Lease Negotiation', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'), 
('UTSI', 'Utilities Disconnect', 'Utilities Disconnect', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Departure Documentation', 'Departure Documentation', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Heath Services', 'Heath Services', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Other', 'Other', 50, 'DPS_SERVICES','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'Initial Contact', 'Initial Contact', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'), 
('UTSI', 'Listing', 'Listing', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Showing', 'Showing', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Offer', 'Offer', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Sold', 'Sold', 50, 'HSM_STATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'New Policy', 'New Policy', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'), 
('UTSI', 'New Program', 'New Program', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Update Policy', 'Update Policy', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Update Program', 'Update Program', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Benchmarking Review', 'Benchmarking Review', 50, 'PDT_SERVICES','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('UTSI', 'Approved', 'Approved', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar'), 
('UTSI', 'Denied', 'Denied', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Alternate Vendor', 'Alternate Vendor', 50, 'MTS_STATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','DPS_SERVICES',50,true,now(),'dkumar',now(),'dkumar',1,1,'');
  
Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('UTSI', 'Employer', 'Employer', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'), 
('UTSI', 'Employee', 'Employee', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Managed Lump Sum', 'Managed Lump Sum', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('UTSI', 'Highstar Relo', 'Highstar Relo', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('VOER', 'Employer', 'Employer', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'), 
('VOER', 'Employee', 'Employee', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Managed Lump Sum', 'Managed Lump Sum', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar'),
('VOER', 'Highstar Relo', 'Highstar Relo', 25, 'PAYMENTRESPONSIBILITY','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','PAYMENTRESPONSIBILITY',25,true,now(),'dkumar',now(),'dkumar',1,1,'');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','PAYMENTRESPONSIBILITY',25,true,now(),'dkumar',now(),'dkumar',1,1,'');
//

insert into refmaster (corpid,code,description,parameter,createdby,createdon,updatedby,updatedon,language,fieldlength)
values('TSFT','customerfile.originAgentEmail','customerFile.originAgentEmail','PENTITY_CUSTOMERFILE','TSFT_SETUP',now(),'TSFT_SETUP',now(),'en','100');

insert into parametercontrol (corpid,parameter,fieldlength,createdby,createdon,updatedby,updatedon,tsftflag,hybridflag,customtype,active)
values('TSFT','PENTITY_CUSTOMERFILE','100','TSFT_SETUP',now(),'TSFT_SETUP',now(),'1','1','',true);

alter table customerfile add column( originAgentEmail varchar(65),`jobFunction` VARCHAR(45) NULL,`backToBackAssignment` VARCHAR(3) NULL);

//As per abhishek
CREATE TABLE  `redsky`.`errorlog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpID` varchar(5) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `CreatedBy` varchar(82) DEFAULT NULL,
  `module` varchar(100) DEFAULT '',
  `methods` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5158 DEFAULT CHARSET=latin1;

ALTER TABLE `redsky`.`emailsetup` ADD COLUMN `fileNumber` VARCHAR(25) DEFAULT '' AFTER `signaturePart`;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `COL_estimatedTaxAllowance` VARCHAR(10) DEFAULT NULL AFTER
 `RNT_checkInDate`,
 ADD COLUMN `COL_estimatedHousingAllowance` VARCHAR(10) DEFAULT NULL AFTER `COL_estimatedTaxAllowance`,
 ADD COLUMN `COL_estimatedTransportationAllowance` VARCHAR(10) DEFAULT NULL AFTER
 `COL_estimatedHousingAllowance`,
 ADD COLUMN `RNT_leaseSignee` VARCHAR(45) DEFAULT NULL AFTER `COL_estimatedTransportationAllowance`;

 INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','LEASE_SIGNELIST',50,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'Employee', 'Employee', 50, 'LEASE_SIGNELIST','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Employer', 'Employer', 50, 'LEASE_SIGNELIST','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `salePrice` DOUBLE DEFAULT 0 AFTER `invoiceSeqNumber`;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `SET_governmentID` DATETIME DEFAULT NULL AFTER `RNT_leaseSignee`,
 ADD COLUMN `SET_heathCare` DATETIME DEFAULT NULL AFTER `SET_governmentID`,
 ADD COLUMN `SET_utilities` DATETIME DEFAULT NULL AFTER `SET_heathCare`,
 ADD COLUMN `SET_automobileRegistration` DATETIME DEFAULT NULL AFTER `SET_utilities`,
 ADD COLUMN `SET_dayofServiceAuthorized` DATETIME DEFAULT NULL AFTER `SET_automobileRegistration`,
 ADD COLUMN `SET_initialDateofServiceRequested` DATETIME DEFAULT NULL AFTER `SET_dayofServiceAuthorized`,
 ADD COLUMN `SET_providerNotificationSent` DATETIME DEFAULT NULL AFTER `SET_initialDateofServiceRequested`,
 ADD COLUMN `SET_providerConfirmationReceived` DATETIME DEFAULT NULL AFTER `SET_providerNotificationSent`;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TAX_allowance` VARCHAR(45) DEFAULT NULL AFTER
`SET_providerConfirmationReceived`,
 ADD COLUMN `VIS_immigrationStatus` VARCHAR(45) DEFAULT NULL AFTER `TAX_allowance`,
 ADD COLUMN `VIS_arrivalDate` DATETIME DEFAULT NULL AFTER `VIS_immigrationStatus`,
 ADD COLUMN `VIS_portofEntry` VARCHAR(90) DEFAULT NULL AFTER `VIS_arrivalDate`;

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','ALLOWANCE',50,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'Hour', 'Hour', 50, 'ALLOWANCE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', '$', '$', 50, 'ALLOWANCE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','IMMIGRATION_STATUS',50,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'Initiation Document Received', 'Initiation Document Received', 50, 'IMMIGRATION_STATUS','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Consultant Assigned', 'Consultant Assigned', 50, 'IMMIGRATION_STATUS','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Initial Contact Completed', 'Initial Contact Completed', 50, 'IMMIGRATION_STATUS','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'In Progress', 'In Progress', 50, 'IMMIGRATION_STATUS','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Approved', 'Approved', 50, 'IMMIGRATION_STATUS','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Received', 'Received', 50, 'IMMIGRATION_STATUS','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');

ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `TEN_landlordName` VARCHAR(225) AFTER `MTS_mortgageTerm`,
 ADD COLUMN `TEN_landlordContactNumber` VARCHAR(100) AFTER `TEN_landlordName`,
 ADD COLUMN `TEN_landlordEmailAddress` VARCHAR(225) AFTER `TEN_landlordContactNumber`,
 ADD COLUMN `TEN_propertyAddress` VARCHAR(225) AFTER `TEN_landlordEmailAddress`,
 ADD COLUMN `TEN_accountHolder` VARCHAR(225) AFTER `TEN_propertyAddress`,
 ADD COLUMN `TEN_bankName` VARCHAR(100) AFTER `TEN_accountHolder`,
 ADD COLUMN `TEN_bankAddress` VARCHAR(225) AFTER `TEN_bankName`,
 ADD COLUMN `TEN_bankAccountNumber` VARCHAR(50) AFTER `TEN_bankAddress`,
 ADD COLUMN `TEN_bankIbanNumber` VARCHAR(50) AFTER `TEN_bankAccountNumber`,
 ADD COLUMN `TEN_abaNumber` VARCHAR(50) AFTER `TEN_bankIbanNumber`,
 ADD COLUMN `TEN_accountType` VARCHAR(50) AFTER `TEN_abaNumber`,
 ADD COLUMN `TEN_leaseStartDate` DATETIME AFTER `TEN_accountType`,
 ADD COLUMN `TEN_leaseExpireDate` DATETIME AFTER `TEN_leaseStartDate`,
 ADD COLUMN `TEN_expiryReminderPriorToExpiry` DATETIME AFTER `TEN_leaseExpireDate`,
 ADD COLUMN `TEN_monthlyRentalAllowance` VARCHAR(10) AFTER `TEN_expiryReminderPriorToExpiry`,
 ADD COLUMN `TEN_securityDeposit` VARCHAR(10) AFTER `TEN_monthlyRentalAllowance`,
 ADD COLUMN `TEN_leaseCurrency` VARCHAR(3) AFTER `TEN_securityDeposit`,
 ADD COLUMN `TEN_leaseSignee` VARCHAR(20) AFTER `TEN_leaseCurrency`;

ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `HOM_agentCode` VARCHAR(25) AFTER `TEN_leaseSignee`,
 ADD COLUMN `HOM_agentName` VARCHAR(100) AFTER `HOM_agentCode`,
 ADD COLUMN `HOM_initialContactDate` DATETIME AFTER `HOM_agentName`,
 ADD COLUMN `HOM_agentPhone` VARCHAR(100) AFTER `HOM_initialContactDate`,
 ADD COLUMN `HOM_houseHuntingTripArrival` DATETIME AFTER `HOM_agentPhone`,
 ADD COLUMN `HOM_agentEmail` VARCHAR(100) AFTER `HOM_houseHuntingTripArrival`,
 ADD COLUMN `HOM_houseHuntingTripDeparture` DATETIME AFTER `HOM_agentEmail`,
 ADD COLUMN `HOM_brokerage` VARCHAR(100) AFTER `HOM_houseHuntingTripDeparture`,
 ADD COLUMN `HOM_offerDate` DATETIME AFTER `HOM_brokerage`,
 ADD COLUMN `HOM_closingDate` DATETIME AFTER `HOM_offerDate`;
 

ALTER TABLE `redsky`.`dspdetails` 
    ADD COLUMN `DSS_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `VIS_portofEntry`,
    ADD COLUMN `DSS_vendorName` VARCHAR(225) DEFAULT NULL AFTER `DSS_vendorCode`,
    ADD COLUMN `DSS_serviceStartDate` DATETIME DEFAULT NULL AFTER `DSS_vendorName`,
    ADD COLUMN `DSS_vendorCodeEXSO` VARCHAR(65) DEFAULT NULL AFTER `DSS_serviceStartDate`,
    ADD COLUMN `DSS_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `DSS_vendorCodeEXSO`,
    ADD COLUMN `DSS_serviceEndDate` DATETIME DEFAULT NULL AFTER `DSS_vendorContact`,
    ADD COLUMN `DSS_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `DSS_serviceEndDate`,
    ADD COLUMN `DSS_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `DSS_vendorEmail`,
    ADD COLUMN `DSS_paymentResponsibility` VARCHAR(65) DEFAULT NULL AFTER `DSS_displyOtherVendorCode`,
    ADD COLUMN `DSS_initialContactDate` DATETIME DEFAULT NULL AFTER `DSS_paymentResponsibility`;


    Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
    ('HSRG', 'DSS', 'Destinations School Services', 5, 'SERVICE','en','Relo','dspDetails.DSS_serviceEndDate',now(),'dkumar',now(),'dkumar');

    Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
    ('HSRG', 'DsDestinationsSchoolServices', 'DsDestinationsSchoolServices', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar');

    ALTER TABLE `redsky`.`crew` ADD COLUMN `supervisorName` VARCHAR(50) NULL  AFTER `deviceNumber` ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('UGWW', 'DPS', 'Departure Services', 5, 'SERVICE','en','Relo','dspDetails.DPS_serviceEndDate',now(),'dkumar',now(),'dkumar');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Initial Contact', 'Initial Contact', 25, 'VIEWSTATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'House hunting', 'House hunting', 25, 'VIEWSTATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Offer', 'Offer', 25, 'VIEWSTATUS','en',now(),'dkumar',now(),'dkumar'),
('HSRG', 'Closed', 'Closed', 25, 'VIEWSTATUS','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
  VALUES('HSRG','VIEWSTATUS',25,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE `redsky`.`checklistresult` ADD COLUMN `emailNotification` VARCHAR(100) NULL AFTER `ruleNumber` , ADD COLUMN `manualEmail` VARCHAR(100) NULL AFTER `emailNotification`;


Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Company', 'Company', 25, 'DEPOSITBY','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Assignee', 'Assignee', 25, 'DEPOSITBY','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Agent', 'Agent', 25, 'DEPOSITBY','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Others', 'Others', 25, 'DEPOSITBY','en','','',now(),'kkumar',now(),'kkumar');
 
 
Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Gas', 'Gas', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Water', 'Water', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Electricity', 'Electricity', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'TV', 'TV', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Internet', 'Internet', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Others', 'Others', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar'),
('HSRG', 'None', 'None', 25, 'UTILITIES','en','','',now(),'kkumar',now(),'kkumar');
 
 

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Exception Service', 'Exception Service', 20, 'NOTETYPE','en',now(),'subrat',now(),'subrat');

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Housing', 'Housing', 20, 'NOTESUBTYPE','en','Exception Service',now(),'subrat',now(),'subrat'),
('HSRG', 'Move Management', 'Move Management', 20, 'NOTESUBTYPE','en','Exception Service',now(),'subrat',now(),'subrat'),
('HSRG', 'Relocation Services', 'Relocation Services', 20, 'NOTESUBTYPE','en','Exception Service',now(),'subrat',now(),'subrat');


Insert into refmaster (corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Requested', 'Requested', 20, 'NOTESTATUS','en','Exception Service',now(),'subrat',now(),'subrat'),
('HSRG', 'Partially Approved', 'Partially Approved', 20, 'NOTESTATUS','en','Exception Service',now(),'subrat',now(),'subrat'),
('HSRG', 'Fully Approved', 'Fully Approved', 20, 'NOTESTATUS','en','Exception Service',now(),'subrat',now(),'subrat'),
('HSRG', 'Denied', 'Denied', 20, 'NOTESTATUS','en','Exception Service',now(),'subrat',now(),'subrat');



Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('SSCW', 'NA', 'Not Applicable', 20, 'ELEVATOR_DOCK','en',now(),'subrat',now(),'subrat');

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('SSCW', 'NA', 'Not Applicable', 20, 'PARKING','en',now(),'subrat',now(),'subrat');

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('SSCW', 'NA', 'Not Applicable', 20, 'SITE_LIST','en',now(),'subrat',now(),'subrat');

update workticket set originElevator='NA' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (originElevator='' or originElevator is null) and service in ('PK','PL','LD') ;

update workticket set parking='NA' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (parking='' or parking is null) and service in ('PK','PL','LD') ;

update workticket set farInMeters='Not Applicable' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (farInMeters='' or farInMeters is null) and service in ('PK','PL','LD') ;


update workticket set siteS='NA' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (siteS='' or siteS is null) and service in ('PK','PL','LD') ;

update workticket set originLongCarry='Not Applicable' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (originLongCarry='' or originLongCarry is null) and service in ('PK','PL','LD') ;

update workticket set destinationElevator='NA' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (destinationElevator='' or destinationElevator is null) and service in ('DL','DU','UP');


update workticket set dest_park='NA' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (dest_park='' or dest_park is null) and service in ('DL','DU','UP');


update workticket set dest_farInMeters='Not Applicable' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (dest_farInMeters='' or dest_farInMeters is null) and service in ('DL','DU','UP');


update workticket set siteD='NA' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (siteD='' or siteD is null) and service in ('DL','DU','UP');


update workticket set destinationLongCarry='Not Applicable' where date_format(date1,'%Y-%m-%d')<'2014-04-12' and corpid='SSCW'
and (destinationLongCarry='' or destinationLongCarry is null) and service in ('DL','DU','UP');

ALTER TABLE `redsky`.`documentaccesscontrol` ADD INDEX `Index_2`(`fileType`);

ALTER TABLE `redsky`.`todoresult` ADD INDEX `Index_shipnumber`(`fileNumber`);


//As per sangeeta need this
ALTER TABLE `redsky`.`myfile` ADD INDEX `Index_6`(`networkLinkId`);

