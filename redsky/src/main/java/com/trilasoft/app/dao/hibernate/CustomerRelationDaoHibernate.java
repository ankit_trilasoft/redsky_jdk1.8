package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CustomerRelationDao;
import com.trilasoft.app.model.CustomerRelation;
import com.trilasoft.app.model.OperationsHubLimits;

public class CustomerRelationDaoHibernate extends GenericDaoHibernate<CustomerRelation, Long>implements CustomerRelationDao {

	public CustomerRelationDaoHibernate() {   
        super(CustomerRelation.class);   
    }

	public CustomerRelation getCrmDetails(Long cid,String sessionCorpID){
		List customerRealtionList = new ArrayList();
		CustomerRelation customerRealtion = new CustomerRelation();
		try {
			customerRealtionList= getHibernateTemplate().find("from CustomerRelation where customerFileId='"+ cid+"' and corpId='" + sessionCorpID	+ "'");
		if(!customerRealtionList.isEmpty()){
			customerRealtion = (CustomerRelation)customerRealtionList.get(0);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerRealtion;
	}
	
}
