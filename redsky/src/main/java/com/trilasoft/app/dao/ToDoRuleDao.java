package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.model.User;

import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;

public interface ToDoRuleDao extends GenericDao<ToDoRule, Long>{
	
	public List findMaximumId();
	public List checkById(Long id);
	public List <ToDoRule>searchById(Long id,String fieldToValidate,String testdate,String entitytablerequired,String messagedisplayed,String status, String toDoRuleCheckEnable, Boolean isAgentTdr);
	public List <ToDoRule>searchByDocCheckListId(Long id, String docType,String testdate,String entitytablerequired,String messagedisplayed,String status, String toDoRuleCheckEnable);
	public List <ToDoRule>getAllRules(String sessionCorpID);
	public List executeRule(String ruleExpression,ToDoRule todoRule);
	
	public List <Notes> findByStatus(String reminderStatus,String corpId,String createdBy);
	public List <Notes> findByStatus1(String reminderStatus,String corpId,String createdBy);
	public List <Notes> findByStatus2(String reminderStatus,String corpId,String createdBy);
	
	public List <Notes> findByStatus(String reminderStatus,String corpId);
	public List <Notes> findByStatus1(String reminderStatus,String corpId);
	public List <Notes> findByStatus2(String reminderStatus,String corpId);
	public List getLinkSOs(String sequenceNumber, String sessionCorpID);
	public List <ToDoResult> findByduration(String corpID);
	public List <ToDoResult> findBydurationdueweek(String corpID);
	public List <ToDoResult> findBydurationdtoday(String corpID);
	public List <ToDoResult> findNumberOfResult(String corpID);
	
	public List <ToDoResult> findBydurationUser(String corpID, String owner, String messageDisplayed, String ruleId, String shipper, User appUser);
	public List <ToDoResult> findBydurationdueweekUser(String corpID,String owner);
	public List <ToDoResult> findBydurationdtodayUser(String corpID,String owner, String messageDisplayed, String ruleId, String shipper, User appUser);
	
	public List <DataCatalog> selectfield(String entity, String corpID);
	public List <DataCatalog> selectDatefield(String corpID);
	public List getIntegrationLogInfoLog();
	public void deleteAllResults(String sessionCorpID);
	public void changestatus(String status,Long id);
		
	public List findUserForSupervisor(String supervisor,String corpID);
	
	public List <Notes> findByStatusForSupervisor(String reminderStatus,String corpId,String users);
	public List <Notes> findByStatusForSupervisor1(String reminderStatus,String corpId,String users);
	public List <Notes> findByStatusForSupervisor2(String reminderStatus,String corpId,String users);
	
	public List <ToDoResult> findBydurationForSupervisor(String corpID, String owner);
	public List <ToDoResult> findBydurationdueweekForSupervisor(String corpID,String owner);
	public List <ToDoResult> findBydurationdtodayForSupervisor(String corpID,String owner);
	public List<ToDoRule> getRulesForEntity(String entity);
	public void deleteResultsForRecord(String entitytablerequired, Long id);
	
	public List checkSupervisor(String username);
	
	public void updateCompanyDivisionLastRunDate(String corpID);
	
	public List getTimeToExecute(String corpID);
	
	public List getOwnersList(String sessionCorpID ,String owner,Long userId,String userFirstName,String userType);
	
	public List getOwnerResult(String owner, String sessionCorpID);
	
	public List findResultByOwnerOverDue(String sessionCorpID, String owner);
	public void deleteThisResults(Long id, String sessionCorpID,String role);
	public List<ToDoRule> getThisRule(Long id);
	
	public List findNumberOfResultEntered(String sessionCorpID, Long id, String role);
	public void updateUserCheck(String recordId, String userName, String fieldToValidate);
	public List  getByCorpID(String sessionCorpID, Boolean isAgentTdr);
	public List  getDocCheckListByCorpID(String sessionCorpID);
	public List getDescription(String tableName, String fieldname, String sessionCorpID); 
	public List getTimeToExecute2(String corpID); 
	public List getMessageList(String sessionCorpID, String userName,Long userId,String userType);
	public List findMessageOverDue(String sessionCorpID, String messageDisplayed, String userName);
	public List findMessageToday(String sessionCorpID, String messageDisplayed, String userName); 
	public List getRuleIdList(String sessionCorpID , String username,Long userId,String userType);  
	public List findIDOverDue(String sessionCorpID, String ruleId, String username);
	public List findIdToday(String sessionCorpID, String ruleId, String username);
	public List getToDoRulesSummary(String sessionCorpID,String username,Long userId,String userType);
	public List getToDoRulesPersonSummary(String sessionCorpID,String username,Long userId,String userType);
	public List findMaximum(String sessionCorpID);
	public List getToDoRulesShipperSummary(String sessionCorpID ,String username,Long userId,String userType);
	public List findByDurationComingUpUser(String sessionCorpID, String ownerName, String messageDisplayed, String ruleId, String shipperName, User appUser);
	public List findByDurationComingUp(String sessionCorpID);
	public void setViewFieldOfHistory(Long historyId);
	public void bulkUpdateViewFieldOfHistory(String idList);
	public List<ToDoResult> getToDoResults(String sessionCorpID, String ruleNo, String email, String eMailType, String entity);
	public List getToDoResultEmailList(String sessionCorpID, String ruleNo, String eMailType, String entity);
	public List getToDoResultEmailList(String sessionCorpID, String ruleNumber);
	public List<ToDoResult> getToDoResults(String sessionCorpID, String email, String ruleNumber);
	public List getFilterName(String sessionCorpID ,Long userId);
	public List findNetwokLinkedFiles(String sessionCorpID);
	public void updateNetworkControlSO(String sessionCorpID, String linkShipNumber ,String sourceShipNumber, String action,String childAgentType);
	public List linkSequenceNumberList(String sessionCorpID,String bookingAgentcode, String agentType, String childAgentType ,String childAgentCode, ServiceOrder soObj,String listTarget);
	public List findLinkSO(String sessionCorpID, String seqNumber,String bookingAgentcode, String agentType, ServiceOrder soObj,String hitFlag);
	public List <ServiceOrder> getServiceOrderByCFId(Long cid);
	public String getLinkedShipnumber(String sequenceNumber, String sessionCorpID);
	public String getNetworkAgentType(String networkPartnerCode,String sessionCorpID);
	public List getComingUpList(String ShipNumber,String sessionCorpID,String relatedTask);
	public List getDueToday(String ShipNumber,String sessionCorpID,String relatedTask);
	public List getOverDue(String ShipNumber,String sessionCorpID,String relatedTask);
	public List getResultToday(String ShipNumber,Long id,String sessionCorpID,String relatedTask);
	public List getToDoOverDue(String ShipNumber,Long id,String sessionCorpID,String relatedTask);
	public List getResultListDueToday(String ShipNumber,Long id,String sessionCorpID,String owner, String duration,String relatedTask);
	public List getBillToCode(String companydivisionCode,String sessionCorpID);
	public Map<String, List<String>> getNetworkDataFields();
	public String getUserLastFirstName(String coordinator);
	public List<Long> getModelIDsByShip(String...ships );
	public int removeAll(String shipNumber, String IDs, String Model);
	public int updateCompanyRulesRunning(String corpId,Boolean useDistAmt);
	public int getCount(String model,String shipNumber);
	public <ToDoResult> List getAllToDoResult();
	public <ToDoResult> List getToDoResultByRuleNumber(String ruleNumber);
	public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId);
	// method for extranal TDR
	public void delAndUpdateResult(String corpId);
	public void updateToDoResult(String toDoResultId,String toDoResultUrl,String toDoRecordId,String roleForTransfer,String roleType,String shipnumber);
	public String getCordinfo(ToDoResult tdr);
	public int findCheckListEnterd(String corpID, Long id);
	public <CheckListResult> List getCheckListResultByRuleNumber(String ruleNumber);
	public List getDocCheckListResultEmailList(String sessionCorpID,String ruleNumber);
	public List<CheckListResult> getCheckListResult(String sessionCorpID,String email, String ruleNumber);
	public void updateUserCheckForCheck(String recordId, String userName);
	public List  findByAgentTdrEmail(String corpID,String owner);
	public List  findByAgentTdrEmail(String ShipNumber,String corpID,String owner);
	public <TaskCheckList> List getTaskCheckListByRuleNumber(String ruleNumber);
	public List <ToDoRule> getRuleByTypes (String service,String mode,String routing,String jobType,String billToCode, String bookingAgent,String corpId);
	public <RuleActions> List getToDoActionsByRuleId(Long ruleId, String corpId);
	public List getOwnersListExtCordinator(String sessionCorpID ,String owner,Long userId,String userFirstName,String userType,String bookingAgentPopup,String billToCodePopup) ;
	public List getMessageListExtCordinator(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup) ;
	public List getRuleIdListExtCordinator(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup);
	public List<ToDoResult> findBydurationUserExtCordinator(String corpID, String owner,	String messageDisplayed, String ruleId, String shipper, User appUser,String bookingAgentPopup,String billToCodePopup) ;
	public List findByAgentTdrEmailExtCordinator(String corpID,String owner,String bookingAgentPopup,String billToCodePopup,String userType) ;
	public List<ToDoResult> findBydurationdtodayUserExtCordinator(String corpID,	String owner, String messageDisplayed, String ruleId, String shipper, User appUser,String bookingAgentPopup,String billToCodePopup);
	public List<ToDoResult> findBydurationForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) ;
	public List<ToDoResult> findBydurationdtodayForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType);
	 public List<ToDoResult> findBydurationdueweekForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) ;
	 public List<Notes> findByStatusExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) ;
	 public List<Notes> findByStatus1ExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup);
	 public List<Notes> findByStatus2ExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup);
	 public List<Notes> findByStatusForSupervisor2ExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup);
	 public List<Notes> findByStatusForSupervisor1ExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup);
	 public List<Notes> findByStatusForSupervisorExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup);
	 public List<Notes> findByStatusForSupervisorExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) ;
	 public List<Notes> findByStatusForSupervisor1ExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio);
	 public List<Notes> findByStatusForSupervisor2ExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) ;
	 public List<ToDoResult> findBydurationForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) ;
	 public List<ToDoResult> findBydurationdtodayForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType);
	 public List<ToDoResult> findBydurationdueweekForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) ;
	 public List findByAgentTdrEmailExtCordinatorForTeam(String corpID,String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType);
	 public List getOwnersListExtCordinatorForTeam(String sessionCorpID ,String username,Long userId,String userFirstName,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio);
	 public List getMessageListExtCordinatorForTeam(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) ;
	 public List getRuleIdListExtCordinatorForTeam(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio);
	 public List getSupervisorForTeam(String username, String corpID) ;
	 public List<Notes> findByStatus1ExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio);
	 public List<Notes> findByStatus2ExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio);
	 public List<ToDoResult> findBydurationUserExtCordinatorForTeam(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio); 
	 public List<ToDoResult> findBydurationdtodayUserExtCordinatorForTeam(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio)
;
	public List<Notes> findByStatusExtCordinatorForTeam(String reminderStatus,
			String corpId, String createdBy, String bookingAgentPopup,
			String billToCodePopup, String allradio);
	 public List getToDoRulesSummaryForGroupByRules(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType) ;
	 public List getToDoRulesPersonSummaryForGroupByPerson(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType);
	 public List getToDoRulesShipperSummaryForGroupByShipper(String sessionCorpID ,String username,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType);
}
