/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve the basic "TrackingStatus" objects in Redsky that allows for TrackingStatus  of Shipment .
 * @Class Name	TrackingStatusDaoHibernate
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * Extended by GenericDaoHibernate to  implement TrackingStatusDao
 */

package com.trilasoft.app.dao.hibernate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set; 
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException; 
import javax.servlet.http.HttpServletRequest; 
import com.trilasoft.app.dao.TrackingStatusDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.ErrorLogManager;

public class TrackingStatusDaoHibernate  extends GenericDaoHibernate<TrackingStatus, Long> implements TrackingStatusDao {  
	
	private String sessionCorpID;
	private ErrorLogManager errorLogManager;
  
	static final Logger logger = Logger.getLogger(TrackingStatusDaoHibernate.class);
	
	public TrackingStatusDaoHibernate() {   
        super(TrackingStatus.class);   
    } 
	 protected HttpServletRequest getRequest() {
	        return ServletActionContext.getRequest();  
	    }
	 
	public void getSessionCorpID()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();	
	}
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
    public List<AccountLine> findByBooker(Long id){
      	 try {
			return getHibernateTemplate().find("select estimateVendorName from AccountLine where category='Booking Agent'  AND serviceOrderId=?", id);
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		} 
    return null;  	
    }
    
    public Map<String,String> findByAgent(String shipNumber,String corpId){
    	Map<String,String> oADADetailMap =new HashMap<String, String>();
    	List oaDaDetailList=null;
    	 try {
    		 oaDaDetailList= this.getSession().createSQLQuery("select concat(if(category is null,'',category),'~',if(vendorCode is null,'',vendorCode)) as code,concat(if(category is null,'',category),'~',if(estimateVendorName is null,'',estimateVendorName)) as description from accountline where category in ('Origin','Destin') AND status=true and corpId='"+corpId+"' AND shipNumber='"+shipNumber+"' and vendorCode!='' and vendorCode is not null").list();
    		 Iterator itr=oaDaDetailList.iterator();
    		 String str="";
    		 String str1="";
    		 while(itr.hasNext()){
    			 Object obj[]=(Object[])itr.next();
    			 str=obj[0]+"";
    			 str1=obj[1]+"";
    			 oADADetailMap.put(str.split("~")[0], str.split("~")[1]+"~"+str1.split("~")[1]);
    		 }
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		return oADADetailMap;
    	}
    public List<AccountLine> findByOriginAgent(String shipNumber){
     	 try {
			return getHibernateTemplate().find("select estimateVendorName from AccountLine where category='Origin' AND status=true AND shipNumber=?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		return null;
     	}
    public List<AccountLine> findByDestinationAgent(String shipNumber){
     	 try {
			return getHibernateTemplate().find("select estimateVendorName from AccountLine where category='Destin' AND status=true  AND shipNumber=?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
     	}
    public List<AccountLine> findByOriginAgentCode(String shipNumber){
    	 try {
			return getHibernateTemplate().find("select vendorCode from AccountLine where category='Origin' AND status=true  AND shipNumber=?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    	}
    public List<AccountLine> findByDestinationAgentCode(String shipNumber){
    	 try {
			return getHibernateTemplate().find("select vendorCode from AccountLine where category='Destin' AND status=true  AND shipNumber=?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    	}
    public List findMaximumId() {
    	try {
			return getHibernateTemplate().find("select max(id) from TrackingStatus");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
    public List checkById(Long id) {
    	try {
			return getHibernateTemplate().find("select id from TrackingStatus where id=?",id);
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }

	public List countForStorage(String shipNumber){
		try {
			return getHibernateTemplate().find("select count(*) from Storage where  shipNumber =?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public int updateTrack(Long id, String blNumber,String hblNumber){
		try {
			return getHibernateTemplate().bulkUpdate("update ServicePartner set blNumber= '"+blNumber+"', hblNumber= '"+hblNumber+"' where serviceOrderId=?",id);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	public List findETD(String shipNumber, String etDeparts){
		       try {
				return getHibernateTemplate().find("select etDepart from ServicePartner where id in (select min(id) from ServicePartner where  status=true and  shipNumber='"+shipNumber+"' and partnerType='CR') and  DATE_FORMAT(etDepart,'%Y-%m-%d')>='"+etDeparts+"'");
			} catch (Exception e) {
				 e.printStackTrace();	
				getSessionCorpID();
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
			return null;
         }
	
	public List<TrackingStatus> findByOriginAgentInTrackingStauts(String shipNumber){
    	 try {
			return getHibernateTemplate().find("select originAgent from TrackingStatus where originAgent <> '' AND shipNumber=?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
	
	public List<TrackingStatus> findByOriginAgentCodeInTrackingStauts(Long id){
	   	 try {
			return getHibernateTemplate().find("select originAgentCode from TrackingStatus where originAgentCode <> '' AND id=?", id);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
    public List<TrackingStatus> findByDestinationAgentInTrackingStauts(String shipNumber){
    	 try {
			return getHibernateTemplate().find("select destinationAgent from TrackingStatus where destinationAgent <> '' AND shipNumber=?", shipNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		return null;
    }
    
    public List<TrackingStatus> findByDestinationAgentCodeInTrackingStauts(Long id){
   	 try {
		return getHibernateTemplate().find("select destinationAgentCode from TrackingStatus where destinationAgentCode <> '' AND id=?", id);
	} catch (DataAccessException e) {
		 e.printStackTrace();	
		getSessionCorpID();
		logger.error("Error executing query"+ e,e);
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
	return null;
   	}
    public List omniReportExtract(String bookCode,String endDates,String companyCode, String corpId,String jobTypeMulitiple) {
    	try {
			String query="select distinct s.shipnumber 'Shipnumber',concat(s.firstname, ' ',s.lastname) 'Shippers Name',t.originagent 'Origin Services Perfomed by', ifnull(po.OMNINumber,' ') 'OA OMNI #', t.destinationagent, ifnull(pd.OMNINumber,' ') 'DA OMNI #',m.actualnetweight  'Net Wt Lbs','1' AS 'No of Shipmnets',if(s.mode = 'Air','A~','S~') 'Mode' from serviceorder s , trackingstatus t, partnerpublic po, partnerpublic pd, miscellaneous m where s.id = t.id and s.id   = m.id and s.job in ("+jobTypeMulitiple+") and s.bookingagentcode = '"+bookCode+"' and t.omnireport is null and s.commodity <> 'AUTO' and t.beginload between '2003-01-01' and '"+endDates+"' and s.status not in ('CNCL','DWNLD','CANCEL') and s.createdon >= '2007-01-01' and t.originagentcode = po.partnercode and t.destinationagentcode = pd.partnercode and ((t.originagentcode ='"+bookCode+"' and pd.OMNINumber <> '') or (t.destinationagentcode = '"+bookCode+"' and po.OMNINumber <> '') or ( (t.originagentcode !='"+bookCode+"' and t.destinationagentcode != '"+bookCode+"') and (pd.OMNINumber <> '' or po.OMNINumber <> '')) ) and t.originagentcode <> t.destinationagentcode and s.companydivision = '"+companyCode+"' and s.corpid = '"+corpId+"' and po.corpid  IN ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') and pd.corpid IN ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') ";
			List omniReportExtractList= this.getSession().createSQLQuery(query).list();	
			return omniReportExtractList;
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
    return null;
    }
    
    public Boolean getdefaultVendorAccLine(String corpID){
    	Boolean check = false;
		
		try {
			List defaultVendorAccLine = getHibernateTemplate().find("select flagDefaultVendorAccountLines from Company where corpID ='"+corpID+"'");
			
			if(!defaultVendorAccLine.isEmpty() && defaultVendorAccLine.get(0).toString().equalsIgnoreCase("true")){
				check = true;
			}else{
				check = false;
			}
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		
		return check;
    }
    public List findSalesStatus(String seuenceNumber){
    	try {
			return getHibernateTemplate().find("select t.sequenceNumber from TrackingStatus t, CustomerFile c where (t.loadA is not null or t.beginLoad is not null) and c.status <> 'CNCL' and t.sequenceNumber=c.sequenceNumber and t.sequenceNumber = '"+seuenceNumber+"' ");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
	public int updateSalesStatus(String seuenceNumber,String userName) {
		try {
			return getHibernateTemplate().bulkUpdate("update CustomerFile set salesStatus='Booked',updatedOn=now(),updatedBy='"+userName+"' where sequenceNumber = '"+seuenceNumber+"' ");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	public List findByoriginSubAgentCode(String originSubAgentCode) {
		 try {
			return getHibernateTemplate().find("select id from Partner where partnerCode=?", originSubAgentCode);
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findIdByShipNumber(String shipNumber){
		 try {
			return getHibernateTemplate().find("select id from TrackingStatus where shipNumber=?", shipNumber);
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findVanLineCodeList(String partnerCode, String corpID){
		try {
			return getHibernateTemplate().find("from PartnerVanLineRef where corpID ='" +corpID+ "' and partnerCode='"+partnerCode+"'");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findVanLineAgent(String vanLineCode, String corpID) {
		try {
			return this.getSession().createSQLQuery("select concat(p.partnerCode, '~', p.lastName) FROM partnerpublic p LEFT OUTER JOIN partnervanlineref pv on  p.partnerCode = pv.partnerCode WHERE pv.vanLineCode = '"+ vanLineCode +"' AND p.corpID IN ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') ").list();
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public boolean checkEmailAlertSent(String forwarderCode, String sessionCorpID) {
		Boolean checkEmailAlertSent=false;
		try {
			List emailAlertSent=getHibernateTemplate().find("select emailSentAlert from PartnerPrivate where partnerCode='"+forwarderCode+"'");
			if(emailAlertSent!=null && !emailAlertSent.isEmpty() && emailAlertSent.get(0)!=null){
				if(emailAlertSent.get(0).toString().equalsIgnoreCase("true")){
					checkEmailAlertSent=true;
				}
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return checkEmailAlertSent;
	}
	public List findBookingAgentCode(String companyCode, String corpID) {
		try {
			return getHibernateTemplate().find("select bookingAgentCode from CompanyDivision where companyCode='"+companyCode+"' and corpID=?", corpID);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public void getOmniReport (String shipNumber, String corpID){			     
		try {
			getHibernateTemplate().bulkUpdate("update TrackingStatus set omnireport =now() where corpID='"+corpID+"' and shipNumber ='"+shipNumber+"'");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	public Boolean getIsNetworkBookingAgent(String sessionCorpID,String bookingAgentCode) {
		Boolean isNetworkAgent=false;
		try {
			List list=this.getSession().createSQLQuery("select count(*) from company, companydivision where company.corpID = companydivision.corpID and company.corpID = '"+sessionCorpID+"' and companydivision.bookingAgentCode = '"+bookingAgentCode+"'").list();
			List list1=this.getSession().createSQLQuery("select count(*) from company, companydivision where company.corpID = companydivision.corpID and company.corpID != '"+sessionCorpID+"' and companydivision.bookingAgentCode = '"+bookingAgentCode+"'").list();
			
			if(Integer.parseInt(list.get(0).toString()) > 0 || Integer.parseInt(list1.get(0).toString()) == 0){
				isNetworkAgent=true;
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return isNetworkAgent;
	}
	public Boolean getIsNetworkAgent(String sessionCorpID, String agentCode) {
		Boolean isNetworkAgent=false;
		try {
			List list=this.getSession().createSQLQuery("select count(*) from networkpartners where agentPartnerCode = '"+agentCode+"' and corpId = '"+sessionCorpID+"' ").list();
			if(Integer.parseInt(list.get(0).toString()) > 0){
				isNetworkAgent=true;
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return isNetworkAgent;
	}
	public List getIsNetworkAgentCorpId(String sessionCorpID, String agentCode) {
		try {
			List list=this.getSession().createSQLQuery("select distinct agentCorpId from networkpartners where agentPartnerCode = '"+agentCode+"'").list();
			return list;
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public String getExternalCorpID(String sessionCorpID, String agentCode) {
		List list=this.getSession().createSQLQuery("select agentCorpId from networkpartners where agentPartnerCode = '"+agentCode+"' and corpId = '"+sessionCorpID+"' ").list();
		String corpID=list.get(0).toString();
		return corpID;
	}
	public void updateExternalSo(Long id, String shipNumber) {
		try {
			getHibernateTemplate().bulkUpdate("update TrackingStatus set destinationAgentExSO='"+shipNumber+"' where id="+id+"");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}		
	}
	public String getLocalBookingAgentCode(String corpID,String bookingAgentCode) {
		String bAgentCode="";
		try {
			List list=this.getSession().createSQLQuery("select agentPartnerCode from networkpartners where agentCorpId = '"+corpID+"' and agentLocalPartnerCode = '"+bookingAgentCode+"' ").list();
			if(!list.isEmpty()){
				if(list.get(0)!=null){
					bAgentCode=list.get(0).toString();
				}
				
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		
		return bAgentCode;
	}
	
	public String updateExSoDelinking(String exSo, String remoteUser, Long id,String shipNumber, String extCorpId){
		String fieldName = "";
		
		try {
			if(extCorpId!=null && !extCorpId.equals("")){
				String query = "select if(findnetworkcorpid(originAgentCode)= '"+extCorpId+"' ,'OA'," +
								   		"if(findnetworkcorpid(destinationAgentCode)= '"+extCorpId+"','DA'," +
								   			"if(findnetworkcorpid(networkPartnerCode)= '"+extCorpId+"','NA'," +
								   					"if(findnetworkcorpid(originSubAgentCode)= '"+extCorpId+"','SOA'," +
								   							"if(findnetworkcorpid(destinationSubAgentCode)= '"+extCorpId+"','SDA','NoLink'))))) " +
								   									"from trackingstatus where shipNumber='"+exSo+"' ";
				List list=this.getSession().createSQLQuery(query).list();
				if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equals("")){
					String tempRole = list.get(0).toString();
					if(tempRole.equals("OA"))
						fieldName="originAgentExSO";
					else if(tempRole.equals("DA"))
						fieldName="destinationAgentExSO";
					else if(tempRole.equals("NA"))
						fieldName="networkAgentExSO";
					else if(tempRole.equals("SOA"))
						fieldName="originSubAgentExSO";
					else if(tempRole.equals("SDA"))
						fieldName="destinationSubAgentExSO";
					else
						fieldName="";
					
				}
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return fieldName;
	}
	
	public void removeExternalLink(Long customerFileId, String agentExSo, String userName, Long Id, String shipNum, String sessionCorpID, Boolean soNetworkGroup) {
		try {
			int accCount=0;
			agentExSo=agentExSo.replace("remove", "").trim();
			List cidList=this.getSession().createSQLQuery("select concat(customerFileId,'~',sequenceNumber) from serviceorder where shipNumber='"+agentExSo+"'").list();
			if(cidList !=null && (!(cidList.isEmpty())) && cidList.get(0)!=null && (!(cidList.get(0).toString().trim().equals("")))){
				String[] CFData=cidList.get(0).toString().split("~"); 
				String cFId=CFData[0];
				String sequenceNumber=CFData[1];
				String newSequenceNumber=sequenceNumber+cFId;
			List corpIDList=this.getSession().createSQLQuery("select concat(corpID,'~',id) from serviceorder where shipNumber='"+agentExSo+"'").list();
			if(corpIDList !=null && (!(corpIDList.isEmpty())) && corpIDList.get(0)!=null && (!(corpIDList.get(0).toString().trim().equals("")))){
				String[] Data=corpIDList.get(0).toString().split("~"); 
				String corpID=Data[0];
				String SOid=Data[1];
				String newShipnumberNumber=agentExSo+SOid;
				try{
				boolean agentSoNetworkGroup=false;
				if(soNetworkGroup!=null && soNetworkGroup){
					List agentSoNetworkGroupList=this.getSession().createSQLQuery("select count(*) from trackingstatus where shipNumber='"+agentExSo+"' and soNetworkGroup is true ").list();	
					String countString=agentSoNetworkGroupList.get(0).toString();
					int countSoNetwork=Integer.parseInt(countString);
					if(countSoNetwork>=1){
						agentSoNetworkGroup=true;	
					}
					if(soNetworkGroup!=null && soNetworkGroup && agentSoNetworkGroup){
						this.getSession().createSQLQuery("update accountline set networkSynchedId = null , updatedBy='Networking:"+userName+"',updatedOn = now()   where serviceOrderId in ('"+SOid+"','"+Id+"' )").executeUpdate();
						this.getSession().createSQLQuery("update accountline set status = false , updatedBy='Networking:"+userName+"',updatedOn = now()   where serviceOrderId = '"+SOid+"' and (recInvoiceNumber='' or  recInvoiceNumber is null) and (invoiceNumber='' or  invoiceNumber is null) ").executeUpdate();
						List accList=this.getSession().createSQLQuery("select count(*) from accountline where serviceOrderId = '"+SOid+"'  and status is true ").list();	
						String accCountString=accList.get(0).toString();
						accCount=Integer.parseInt(accCountString);
					}
				}
				}catch (Exception e) {
					getSessionCorpID();
					logger.error("Error executing query"+ e,e);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
				}
			List countList=	this.getSession().createSQLQuery("select count(*) from serviceorder where  status !='CNCL' and shipNumber !='"+agentExSo+"' and customerFileId = '"+cFId+"' and corpID='"+corpID+"' ").list();
			String countString=countList.get(0).toString();
			int count=Integer.parseInt(countString);
			String delinkField = updateExSoDelinking( agentExSo,  userName, Id, shipNum,corpID);
			if(accCount==0){
			if(count==0){
			this.getSession().createSQLQuery("update customerfile set status='CNCL' , statusReason='Delink', statusDate= now(),isNetworkRecord = false ,isNetworkGroup = false ,updatedBy='Networking:"+userName+"',updatedOn = now()   where id='"+cFId+"'").executeUpdate();
			this.getSession().createSQLQuery("update serviceorder set status='CNCL' , statusReason='Delink', statusDate= now() ,isNetworkRecord = false ,updatedBy='Networking:"+userName+"',updatedOn = now()   where shipNumber='"+agentExSo+"'").executeUpdate();	
			if(delinkField.equals("")){
			this.getSession().createSQLQuery("update trackingstatus set status='CNCL' , statusDate= now(), soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
			}else{
				String s = "update trackingstatus set "+delinkField+" = 'Delinked', status='CNCL' , statusDate= now(), soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'";
				this.getSession().createSQLQuery("update trackingstatus set "+delinkField+" = 'Delinked', status='CNCL' , statusDate= now(), soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
			}
			this.getSession().createSQLQuery("update miscellaneous set status='CNCL' ,updatedBy='Networking:"+userName+"',updatedOn = now()  where shipNumber='"+agentExSo+"'").executeUpdate();	
			this.getSession().createSQLQuery("update networkcontrol set actionStatus='Delinked',updatedOn = now()  where sourceSoNum='"+agentExSo+"'").executeUpdate();
			}else{
			this.getSession().createSQLQuery("update serviceorder set status='CNCL' , statusReason='Delink', statusDate= now() ,isNetworkRecord = false ,updatedBy='Networking:"+userName+"',updatedOn = now()  where shipNumber='"+agentExSo+"'").executeUpdate();	
			 
			if(delinkField.equals("")){	
				this.getSession().createSQLQuery("update trackingstatus set status='CNCL' , statusDate= now(), soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
			}else{
				String s = "update trackingstatus set "+delinkField+" = 'Delinked', status='CNCL' , statusDate= now(), soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'";
			this.getSession().createSQLQuery("update trackingstatus set "+delinkField+" = 'Delinked', status='CNCL' , statusDate= now(), soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
			}
			this.getSession().createSQLQuery("update miscellaneous set status='CNCL' ,updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
			this.getSession().createSQLQuery("update networkcontrol set actionStatus='Delinked',updatedOn = now()  where sourceSoNum='"+agentExSo+"'").executeUpdate();
			}
			}else{

				if(count==0){
				this.getSession().createSQLQuery("update customerfile set isNetworkRecord = false ,isNetworkGroup = false ,updatedBy='Networking:"+userName+"',updatedOn = now()   where id='"+cFId+"'").executeUpdate();
				this.getSession().createSQLQuery("update serviceorder set isNetworkRecord = false ,updatedBy='Networking:"+userName+"',updatedOn = now()   where shipNumber='"+agentExSo+"'").executeUpdate();	
				if(delinkField.equals("")){
				this.getSession().createSQLQuery("update trackingstatus set soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
				}else{
					String s = "update trackingstatus set "+delinkField+" = 'Delinked', soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'";
					this.getSession().createSQLQuery("update trackingstatus set "+delinkField+" = 'Delinked', soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
				}
				this.getSession().createSQLQuery("update networkcontrol set actionStatus='Delinked',updatedOn = now()  where sourceSoNum='"+agentExSo+"'").executeUpdate();
				}else{
				this.getSession().createSQLQuery("update serviceorder set isNetworkRecord = false ,updatedBy='Networking:"+userName+"',updatedOn = now()  where shipNumber='"+agentExSo+"'").executeUpdate();	
				 
				if(delinkField.equals("")){	
					this.getSession().createSQLQuery("update trackingstatus set  soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
				}else{
					String s = "update trackingstatus set "+delinkField+" = 'Delinked', soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'";
				this.getSession().createSQLQuery("update trackingstatus set "+delinkField+" = 'Delinked', soNetworkGroup = false , accNetworkGroup=false , utsiNetworkGroup=false , agentNetworkGroup=false , updatedBy='Networking:"+userName+"',updatedOn = now() where shipNumber='"+agentExSo+"'").executeUpdate();	
				}
				this.getSession().createSQLQuery("update networkcontrol set actionStatus='Delinked',updatedOn = now()  where sourceSoNum='"+agentExSo+"'").executeUpdate();
				}
				
			}
}
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public String getAgentTypeRole(String sessionCorpID, Long id){
		
		String tempReturn ="";
		try{
		String query= "select if(findnetworkcorpid(t.originAgentCode)= '"+sessionCorpID+"',  " +
				" 'OA' ,if(findnetworkcorpid(t.destinationAgentCode)= '"+sessionCorpID+"',  " +
				"'DA',if(findnetworkcorpid(t.networkPartnerCode)= '"+sessionCorpID+"',  " +
				" 'NWA',if(findnetworkcorpid(s.bookingagentcode)= '"+sessionCorpID+"',  " +
				"'BA',if(findnetworkcorpid(t.originSubAgentCode)= '"+sessionCorpID+"',  " +
				" 'SOA',if(findnetworkcorpid(t.destinationSubAgentCode)= '"+sessionCorpID+"',  " +
				" 'SDA','')))))) as roleName  " +
				"from serviceorder s ,trackingstatus t where s.id='"+id+"' and t.id = s.id " +
				"and s.corpid='"+sessionCorpID+"' order by 1";
		List tempList = getSession().createSQLQuery(query).list();
		
		if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null )
			tempReturn=tempList.get(0).toString();
		}catch(Exception e){
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return tempReturn;
	}
	public String[] getLocalAgent(String agentCorpID, String sessionCorpID,String agentCode) {
		String  code="";
		String  name="";
		try {
			String query="select concat(agentLocalPartnerCode,'#',agentName) from networkpartners where corpID='"+sessionCorpID+"' and agentCorpID='"+agentCorpID+"' and agentPartnerCode='"+agentCode+"'";
			List aList=this.getSession().createSQLQuery("select concat(agentLocalPartnerCode,'#',agentName) from networkpartners where corpID='"+sessionCorpID+"' and agentCorpID='"+agentCorpID+"' and agentPartnerCode='"+agentCode+"'").list();
			if(!aList.isEmpty()){
				String s1=aList.get(0).toString();
				code=s1.substring(0, s1.indexOf("#"));
				name=s1.substring(s1.indexOf("#")+1,s1.length());
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		String[] agent={code,name};
		return agent;
		
	}
	public String getCompanyDivision(String bookingAgentCode, String sessionCorpID, String externalCorpID) {
		String compDivision="";
		try {
			List compDivisionList=this.getSession().createSQLQuery("select agentCompanyDivision from networkpartners where agentCorpID='"+externalCorpID+"' and agentPartnerCode='"+bookingAgentCode+"' and corpID='"+sessionCorpID+"'").list();
			if(!compDivisionList.isEmpty()){
				compDivision=compDivisionList.get(0).toString();
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return compDivision;
	}
	public List<TrackingStatus> getNetworkedTrackingStatus(String sequenceNumber) {
		try {
			return getHibernateTemplate().find("from  TrackingStatus where sequenceNumber='"+sequenceNumber+"'");
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public Boolean checkNetworkSection(String bookingAgentShipNumber, String section) {
		Boolean isNetworkSection=false;
		try {
			if(section.equalsIgnoreCase("Origin")){
				List checkList=this.getSession().createSQLQuery("select originAgentExSO from trackingstatus where shipNumber='"+bookingAgentShipNumber+"'").list();
				if(checkList.get(0)!=null && !checkList.get(0).equals("")){
					isNetworkSection=true;
				}	
			}
			if(section.equalsIgnoreCase("Destination")){
				List checkList=this.getSession().createSQLQuery("select destinationAgentExSO from trackingstatus where shipNumber='"+bookingAgentShipNumber+"'").list();
				if(checkList.get(0)!=null && !checkList.get(0).equals("")){
					isNetworkSection=true;
				}	
			}
			if(section.equalsIgnoreCase("NetworkPartnerCode")){
				List checkList=this.getSession().createSQLQuery("select networkAgentExSO from trackingstatus where shipNumber='"+bookingAgentShipNumber+"'").list();
				if(checkList.get(0)!=null && !checkList.get(0).equals("")){
					isNetworkSection=true;
				}	
			}
			if(section.equalsIgnoreCase("bookingAgentCode")){
				List checkList=this.getSession().createSQLQuery("select bookingAgentExSO from trackingstatus where shipNumber='"+bookingAgentShipNumber+"'").list();
				if(checkList.get(0)!=null && !checkList.get(0).equals("")){
					isNetworkSection=true;
				}	
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		
		
		return isNetworkSection;
	}
	public List getCountryCode(String fileNumber){
		try {
			return getHibernateTemplate().find("select concat(originCountryCode,'~',destinationCountryCode) from ServiceOrder where shipnumber=?",fileNumber);
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List downloadXMLFile(String sessionCorpID,String fileNumber){
		 try {
			List findData=new ArrayList();
			 String query="select serviceorder.shipnumber,if(accountline.recInvoiceNumber is null,'',accountline.recInvoiceNumber),if(accountline.receivedInvoiceDate is null,'',DATE_FORMAT(accountline.receivedInvoiceDate,'%Y-%m-%d')),if(accountline.recRateCurrency is null,'',accountline.recRateCurrency),if(accountline.actualRevenue is null,'',accountline.actualRevenue),if(trackingstatus.deliveryA is null,'',DATE_FORMAT(trackingstatus.deliveryA,'%Y-%m-%d')),concat(if(serviceorder.lastName is null,'',serviceorder.lastName),if(serviceorder.firstName is null || serviceorder.firstName='','',concat(',',serviceorder.firstName))),if(serviceorder.originAddressLine1 is null,'',serviceorder.originAddressLine1),if(serviceorder.originAddressLine2 is null,'',serviceorder.originAddressLine2),if(serviceorder.originAddressLine3 is null,'',serviceorder.originAddressLine3),if(serviceorder.originCity is null,'',serviceorder.originCity),if(serviceorder.originZip is null,'',serviceorder.originZip),if(serviceorder.originCountry is null,'',serviceorder.originCountry),if(miscellaneous.actualGrossWeightKilo is null,'',miscellaneous.actualGrossWeightKilo),if(miscellaneous.actualNetWeightKilo is null,'',miscellaneous.actualNetWeightKilo),if(container.pieces is null,'',container.pieces),if(serviceorder.destinationAddressLine1 is null,'',serviceorder.destinationAddressLine1),if(serviceorder.destinationAddressLine2 is null,'',serviceorder.destinationAddressLine2),if(serviceorder.destinationAddressLine3 is null,'',serviceorder.destinationAddressLine3),if(serviceorder.destinationCity is null,'',serviceorder.destinationCity),if(serviceorder.destinationZip is null,'',serviceorder.destinationZip),if(serviceorder.destinationCountry is null,'',serviceorder.destinationCountry),refmaster.bucket2 as obucket2,refmaster1.bucket2 as dbucket2,concat(if(app_user.first_Name is null,'',app_user.first_Name),' ',if(app_user.last_Name is null,'',app_user.last_Name)),if(serviceorder.destinationDayPhone is null,'',serviceorder.destinationDayPhone),if(serviceorder.destinationFax is null,'',serviceorder.destinationFax),serviceorder.socialSecurityNumber FROM serviceorder serviceorder left outer join miscellaneous miscellaneous on serviceorder.id = miscellaneous.id left outer join accountline accountline ON serviceorder.id = accountline.serviceOrderId left outer join trackingstatus trackingstatus ON serviceorder.id = trackingstatus.id left outer join container container ON serviceorder.id = container.serviceOrderId and  container.status =true  left outer join refmaster refmaster on refmaster.code=serviceorder.originCountryCode left outer join refmaster refmaster1 on refmaster1.code=serviceorder.destinationCountryCode left outer join app_user app_user on serviceorder.coordinator=app_user.username where serviceorder.corpID='"+sessionCorpID+"' AND serviceorder.shipNumber='"+fileNumber+"' group by serviceorder.shipnumber;";
			List list=this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			DTO dto=null;
			
			  while(it.hasNext())
			  {
				  Object [] row=(Object[])it.next();
				  dto=new DTO();
				  dto.setShipNumber(row[0]);
				  dto.setRecInvoiceNumber(row[1]);
				  dto.setReceivedInvoiceDate(row[2]);
				  dto.setRecRateCurrency(row[3]);
				  dto.setActualRevenue(row[4]);
				  dto.setDeliveryA(row[5]);
				  dto.setShiperName(row[6]);
				  dto.setAddressLine1(row[7]);
				  dto.setAddressLine2(row[8]);
				  dto.setAddressLine3(row[9]);
				  dto.setCity(row[10]);
				  dto.setZip(row[11]);
				  dto.setCountry(row[12]);
				  dto.setActualgrossWeight(row[13]);
				  dto.setActualNetWeight(row[14]);
				  dto.setPieces(row[15]);
				  dto.setBuyerAddressLine1(row[16]);
				  dto.setBuyerAddressLine2(row[17]);
				  dto.setBuyerAddressLine3(row[18]);
				  dto.setBuyerCity(row[19]);
				  dto.setBuyerZip(row[20]);
				  dto.setBuyerCountry(row[21]);
				  dto.setOriginBucket2(row[22]);
				  dto.setDestinationBucket2(row[23]);
				  dto.setCoordinator(row[24]);
				  dto.setWorkPhone(row[25]);
				  dto.setFaxNumber(row[26]);
				  dto.setSocialSecurityNumber(row[27]);
				  findData.add(dto);
			  } 

			return findData;
		} catch (DataAccessResourceFailureException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
		
	}
	public  class DTO
	 {		
		 	private Object shipNumber;
		  	private Object recInvoiceNumber;
			private Object receivedInvoiceDate; 
			private Object recRateCurrency;
			private Object actualRevenue; 
			private Object deliveryA;
			private Object shiperName;
			private Object addressLine1;
			private Object addressLine2;
			private Object addressLine3;
			private Object city;
			private Object zip;
			private Object country;
			private Object actualgrossWeight;
			private Object actualNetWeight;
			private Object pieces;
			private Object buyerAddressLine1;
			private Object buyerAddressLine2;
			private Object buyerAddressLine3;
			private Object buyerCity;
			private Object buyerZip;
			private Object buyerCountry;
			private Object originBucket2;
			private Object destinationBucket2;
			private Object coordinator;
			private Object workPhone;
			private Object faxNumber;	
			private Object socialSecurityNumber;
			public Object getSocialSecurityNumber() {
				return socialSecurityNumber;
			}
			public void setSocialSecurityNumber(Object socialSecurityNumber) {
				this.socialSecurityNumber = socialSecurityNumber;
			}
			public Object getActualgrossWeight() {
				return actualgrossWeight;
			}
			public void setActualgrossWeight(Object actualgrossWeight) {
				this.actualgrossWeight = actualgrossWeight;
			}
			public Object getActualNetWeight() {
				return actualNetWeight;
			}
			public void setActualNetWeight(Object actualNetWeight) {
				this.actualNetWeight = actualNetWeight;
			}
			public Object getActualRevenue() {
				return actualRevenue;
			}
			public void setActualRevenue(Object actualRevenue) {
				this.actualRevenue = actualRevenue;
			}
			public Object getAddressLine1() {
				return addressLine1;
			}
			public void setAddressLine1(Object addressLine1) {
				this.addressLine1 = addressLine1;
			}
			public Object getAddressLine2() {
				return addressLine2;
			}
			public void setAddressLine2(Object addressLine2) {
				this.addressLine2 = addressLine2;
			}
			public Object getAddressLine3() {
				return addressLine3;
			}
			public void setAddressLine3(Object addressLine3) {
				this.addressLine3 = addressLine3;
			}
			public Object getCity() {
				return city;
			}
			public void setCity(Object city) {
				this.city = city;
			}
			public Object getCountry() {
				return country;
			}
			public void setCountry(Object country) {
				this.country = country;
			}
			public Object getDeliveryA() {
				return deliveryA;
			}
			public void setDeliveryA(Object deliveryA) {
				this.deliveryA = deliveryA;
			}
			public Object getReceivedInvoiceDate() {
				return receivedInvoiceDate;
			}
			public void setReceivedInvoiceDate(Object receivedInvoiceDate) {
				this.receivedInvoiceDate = receivedInvoiceDate;
			}
			public Object getRecInvoiceNumber() {
				return recInvoiceNumber;
			}
			public void setRecInvoiceNumber(Object recInvoiceNumber) {
				this.recInvoiceNumber = recInvoiceNumber;
			}
			public Object getRecRateCurrency() {
				return recRateCurrency;
			}
			public void setRecRateCurrency(Object recRateCurrency) {
				this.recRateCurrency = recRateCurrency;
			}
			public Object getShiperName() {
				return shiperName;
			}
			public void setShiperName(Object shiperName) {
				this.shiperName = shiperName;
			}
			public Object getShipNumber() {
				return shipNumber;
			}
			public void setShipNumber(Object shipNumber) {
				this.shipNumber = shipNumber;
			}
			public Object getZip() {
				return zip;
			}
			public void setZip(Object zip) {
				this.zip = zip;
			}
			public Object getPieces() {
				return pieces;
			}
			public void setPieces(Object pieces) {
				this.pieces = pieces;
			}
			public Object getBuyerAddressLine1() {
				return buyerAddressLine1;
			}
			public void setBuyerAddressLine1(Object buyerAddressLine1) {
				this.buyerAddressLine1 = buyerAddressLine1;
			}
			public Object getBuyerAddressLine2() {
				return buyerAddressLine2;
			}
			public void setBuyerAddressLine2(Object buyerAddressLine2) {
				this.buyerAddressLine2 = buyerAddressLine2;
			}
			public Object getBuyerAddressLine3() {
				return buyerAddressLine3;
			}
			public void setBuyerAddressLine3(Object buyerAddressLine3) {
				this.buyerAddressLine3 = buyerAddressLine3;
			}
			public Object getBuyerCity() {
				return buyerCity;
			}
			public void setBuyerCity(Object buyerCity) {
				this.buyerCity = buyerCity;
			}
			public Object getBuyerCountry() {
				return buyerCountry;
			}
			public void setBuyerCountry(Object buyerCountry) {
				this.buyerCountry = buyerCountry;
			}
			public Object getBuyerZip() {
				return buyerZip;
			}
			public void setBuyerZip(Object buyerZip) {
				this.buyerZip = buyerZip;
			}
			public Object getCoordinator() {
				return coordinator;
			}
			public void setCoordinator(Object coordinator) {
				this.coordinator = coordinator;
			}
			public Object getDestinationBucket2() {
				return destinationBucket2;
			}
			public void setDestinationBucket2(Object destinationBucket2) {
				this.destinationBucket2 = destinationBucket2;
			}
			public Object getOriginBucket2() {
				return originBucket2;
			}
			public void setOriginBucket2(Object originBucket2) {
				this.originBucket2 = originBucket2;
			}
			public Object getFaxNumber() {
				return faxNumber;
			}
			public void setFaxNumber(Object faxNumber) {
				this.faxNumber = faxNumber;
			}
			public Object getWorkPhone() {
				return workPhone;
			}
			public void setWorkPhone(Object workPhone) {
				this.workPhone = workPhone;
			}
			
	 }
	
	public List omniReportExtractByOmni(String bookCode,String endDates,String companyCode,String corpId,String jobTypeMulitiple){
	try {
		String query="select distinct s.shipnumber 'Shipnumber',concat(s.firstname, ' ',s.lastname) 'Shippers Name',t.originagent 'Origin Services Perfomed by', ifnull(po.OMNINumber,' ') 'OA OMNI #', t.destinationagent, ifnull(pd.OMNINumber,' ') 'DA OMNI #',m.actualnetweight  'Net Wt Lbs','1' AS 'No of Shipmnets',if(s.mode = 'Air','A~','S~') 'Mode' from serviceorder s , trackingstatus t, partnerpublic po, partnerpublic pd, miscellaneous m where s.id = t.id and s.id   = m.id and s.job in ("+jobTypeMulitiple+") and s.bookingagentcode like '%"+bookCode+"%' and s.commodity <> 'AUTO' and DATE_FORMAT(t.omnireport,'%Y-%m-%d')='"+endDates+"' and s.status not in ('CNCL','DWNLD','CANCEL') and s.createdon >= '2007-01-01' and t.originagentcode = po.partnercode and t.destinationagentcode = pd.partnercode and ((t.originagentcode like '%"+bookCode+"%' and pd.OMNINumber <> '') or (t.destinationagentcode like '%"+bookCode+"%' and po.OMNINumber <> '') or ( (t.originagentcode != '"+bookCode+"' and t.destinationagentcode != '"+bookCode+"') and (pd.OMNINumber <> '' or po.OMNINumber <> '')) ) and t.originagentcode <> t.destinationagentcode and s.companydivision like '%"+companyCode+"%' and s.corpid = '"+corpId+"' and po.corpid  IN ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') and pd.corpid IN ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') ";
		    	List omniReportExtractList= this.getSession().createSQLQuery(query).list();
		    	return omniReportExtractList;
	} catch (Exception e) {
		 e.printStackTrace();	
		getSessionCorpID();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
	return null;
	    }
	public boolean getContractType(String sessionCorpID, String contract) {
		boolean type=false; 
		try {
			List list = getHibernateTemplate().find("select contractType from Contract where contract='"+contract+"' and corpID=?", sessionCorpID);	
			if(list!=null && (!(list.isEmpty())) && list.get(0)!=null && ((list.get(0).toString().trim().equalsIgnoreCase("CMM"))|| (list.get(0).toString().trim().equalsIgnoreCase("DMM"))))
			{
				type=true; 
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return type;
	}
	public boolean getCMMContractType(String sessionCorpID, String contract) { 
		boolean type=false; 
		try {
			List list = getHibernateTemplate().find("select contractType from Contract where contract='"+contract+"' and corpID=?", sessionCorpID);	
			if(list!=null && (!(list.isEmpty())) && list.get(0)!=null && (list.get(0).toString().trim().equalsIgnoreCase("CMM")))
			{
				type=true; 
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return type;
	}
	public boolean getDMMContractType(String sessionCorpID, String contract) { 
		boolean type=false; 
		try {
			List list = getHibernateTemplate().find("select contractType from Contract where contract='"+contract+"' and corpID=?", sessionCorpID);	
			if(list!=null && (!(list.isEmpty())) && list.get(0)!=null && (list.get(0).toString().trim().equalsIgnoreCase("DMM")))
			{
				type=true; 
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return type;
	}
	public String[] findParentBillTo(String billToCode, String sessionCorpID) {
		String  code="";
		String  name=""; 
		try {
			List aList=this.getSession().createSQLQuery("select concat(agentParent,'~',agentParentName) from partnerpublic where corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and partnercode='"+billToCode+"' ").list();
			if(aList!=null && (!(aList.isEmpty()))){
				try{
				String[] Data=aList.get(0).toString().split("~"); 
				code=Data[0];
				name=Data[1];
				}catch(Exception e){
					
				}
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 }
		String[] agent={code,name};
		return agent;
		
	}
	public String getAgentTypeRoleForm(String sessionCorpID, Long id, String bookingAgentCode, String networkPartnerCode, String originAgentCode, String destinationAgentCode, String originSubAgentCode, String destinationSubAgentCode) {
		
		String tempReturn ="";
		try{
		String query= "select if(findnetworkcorpid('"+originAgentCode+"')= '"+sessionCorpID+"',  " +
				" 'OA' ,if(findnetworkcorpid('"+destinationAgentCode+"')= '"+sessionCorpID+"',  " +
				"'DA',if(findnetworkcorpid('"+networkPartnerCode+"')= '"+sessionCorpID+"',  " +
				" 'NWA',if(findnetworkcorpid('"+bookingAgentCode+"')= '"+sessionCorpID+"',  " +
				"'BA',if(findnetworkcorpid('"+originSubAgentCode+"')= '"+sessionCorpID+"',  " +
				" 'SOA',if(findnetworkcorpid('"+destinationSubAgentCode+"')= '"+sessionCorpID+"',  " +
				" 'SDA','')))))) as roleName  " +
				"from serviceorder s ,trackingstatus t where s.id='"+id+"' and t.id = s.id " +
				"and s.corpid='"+sessionCorpID+"' order by 1";
		List tempList = getSession().createSQLQuery(query).list();
		
		if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null )
			tempReturn=tempList.get(0).toString();
		}catch(Exception e){
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return tempReturn;
	}
	public void updateDMMAgentBilltocode(String sessionCorpID, Long id, String networkPartnerCode, String networkPartnerName) {
		try {
			int i=getHibernateTemplate().bulkUpdate( "update ServiceOrder set billToCode='"+networkPartnerCode+"', billToName='"+networkPartnerName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where corpID='" + sessionCorpID + "' and id=" + id + "");
			i=getHibernateTemplate().bulkUpdate( "update Billing set billToCode='"+networkPartnerCode+"', billToName='"+networkPartnerName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where corpID='" + sessionCorpID + "' and id=" + id + "");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		 
	}
	public List getAllBookingAgentList(String sessionCorpID) {
		try {
			return getHibernateTemplate().find("select bookingAgentCode from CompanyDivision where corpID='"+sessionCorpID+"'" );
		} catch (DataAccessException e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}	
		return null; 
	}	
	public String checkIsRedSkyUser(String partnerCode) {
		String type="";		
		try {
			String q2="select partnerPortalActive from partnerpublic where  partnerPortalActive=true and doNotSendEmailtoAgentUser=false and partnerCode ='"+partnerCode+"'"; 
			List res2= this.getSession().createSQLQuery(q2).list();
			if(res2!=null && !res2.isEmpty() && res2.get(0)!=null){
				type="AP";	
			}
			List list = this.getSession().createSQLQuery("select cd.corpid from companydivision cd, company c where bookingAgentCode='"+partnerCode+"' and cd.corpid = c.corpid and c.status is true ").list();	
			if(list!=null && (!(list.isEmpty())) && list.get(0)!=null )
			{
				type="RUC"; 
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return type;
	}
public boolean getAgentsExistInRedSky(String partnerCode, String sessionCorpID,String name, String emailId) {
	boolean type=false;	
	if(name==null){
		name="";
	}
if(emailId==null){
	emailId="";
}

try {
	String condition=" and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	String condition1=" and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	List newTempParent=new ArrayList();	
	String tempPartnerCode="";
	List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpid in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')").list();
	if(tempParent.size()>0){
		Iterator it=tempParent.iterator();
		while(it.hasNext()){
			if(tempPartnerCode.equals("")){
				tempPartnerCode = "'"+it.next().toString()+"'";
			}else{
				tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
			}
		}
	}
	String partnerCodes="";
	if(tempPartnerCode.equals("")){
		partnerCodes ="'"+partnerCode+"'";
	}else{
		partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
	}

	String query="";
	query="Select ap.userType,ap.account_enabled" +
			" from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
			"datasecurityfilter dsf, datasecuritypermission dsp " +
			"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
			""+condition+" "+ 
			"and dsf.id=dsp.datasecurityfilter_id " +
			"and dss.id=dsp.datasecurityset_id and ap.parentAgent =  dsf.filtervalues " +
			"and dsf.filtervalues in("+partnerCodes+")" +
			"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
			"and concat(ap.first_name,' ',ap.last_name) = '" + name.replace("'","''") + "'" +
			"and email like '" + emailId + "%'" +
			"union Select ap.userType,ap.account_enabled" +
			" from app_user ap, user_role u, role r, companydivision c " +
			"where ap.corpid=c.corpid " +
			"and ap.id= u.user_id " +
			"and concat(ap.first_name,' ',ap.last_name) = '" + name.replace("'","''") + "'" +
			"and email like '" + emailId + "%'" +
			""+condition1+" "+ 
			"and u.role_id= r.id " +
			"and c.bookingagentcode in("+partnerCodes+")" +
			"and (r.name='ROLE_COORD')" +
			"union Select ap.userType ,ap.account_enabled" +
			" from app_user ap where ap.basedAt in("+partnerCodes+")"+
			"and concat(ap.first_name,' ',ap.last_name) = '" + name.replace("'","''") + "'" +
			"and email like '" + emailId + "%'" +
			"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
			"AND ap.contact is true AND ap.account_enabled is false order by 1 ";
	List agentUsers= this.getSession().createSQLQuery(query).list();
		
	Iterator it=agentUsers.iterator();
	while(it.hasNext()){	
	       Object []row= (Object [])it.next();
	       String utype = row[0].toString();
	       Boolean  isactive =( Boolean) row[1];       
	       if(utype.equals("AGENT") && isactive){
	    	   type=true;
	       }
	}
} catch (Exception e) {
	 e.printStackTrace();	
	getSessionCorpID();
	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
}

	

return type;


}
public List<TrackingStatus> getCurrentDetail(Long soid) { 
	List <TrackingStatus>list =new  ArrayList<TrackingStatus>();  
	List list1 = new  ArrayList();
	try {  
   list1= getSession().createSQLQuery("select beginload,loadA,deliveryA,packA,leftWHOn,deliveryShipper from trackingstatus where id='"+soid+"'").list();
  
	
    Iterator it =list1.iterator();
    while(it.hasNext()){
    	SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd"); 
    	Object [] row = (Object[])it.next();
    	TrackingStatus trackingStatus=new TrackingStatus();
    	if(row[0] != null){
    		Date beginLoad = sd.parse(row[0].toString());
    		trackingStatus.setBeginLoad(beginLoad);
		}
    	if(row[1] != null){
    		Date loadA = sd.parse(row[1].toString());
    		trackingStatus.setLoadA(loadA);
		}
    	if(row[2] != null){
    		Date deliveryA = sd.parse(row[2].toString());
    		trackingStatus.setDeliveryA(deliveryA);
		}
    	if(row[3] != null){
    		Date packA = sd.parse(row[3].toString());
    		trackingStatus.setPackA(packA);
		}
    	if(row[4] != null){
    		Date leftWHOn = sd.parse(row[4].toString());
    		trackingStatus.setLeftWHOn(leftWHOn);
		}
    	if(row[5] != null){
    		Date deliveryShipper = sd.parse(row[5].toString());
    		trackingStatus.setDeliveryShipper(deliveryShipper);
		}
    	list.add(trackingStatus);
    }
	}catch(Exception e){
		 e.printStackTrace();	
		getSessionCorpID();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    }
	return  list; 
}
	public void updateNetworkflagDSP(Long id) {
		try {
			getHibernateTemplate().bulkUpdate( "update TrackingStatus set networkAgentExSO='', soNetworkGroup = false, accNetworkGroup = false, utsiNetworkGroup = false, agentNetworkGroup = false where  id=" + id + "");
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}	 
	}
	public String findMoveTypePartnerValue(String sessionCorpID, String partnerCode){
		String value ="T" ;
		String agentCorpIdVal="";
		try {
			String query="select agentCorpId from networkpartners where agentPartnerCode = '"+partnerCode+"' and corpId ='"+sessionCorpID+"' ";
			List list1= this.getSession().createSQLQuery(query).list();
			if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
				agentCorpIdVal=list1.get(0).toString(); 
			List l1  = this.getSession().createSQLQuery("select if(accessQuotationFromCustomerFile,'T','F') from company where corpid='"+agentCorpIdVal+"' ").list();
			if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
				value = l1.get(0).toString().trim();
			}
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return value;
	}
	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}
	
	public List findDocumentFromMyFileCF(String seqNumber,String fieldValue,String sessionCorpID){
		List list = new ArrayList();
		String query="";
		try {
			query ="select id,"+fieldValue+",filetype,description,filesize,updatedby,updatedon,customernumber,fileid "
				+ "from myfile where corpid='"+sessionCorpID+"' and (customernumber='"+seqNumber+"' and fileid ='"+seqNumber+"') and "+fieldValue+" is true and active is true ";
			List list1  = this.getSession().createSQLQuery(query).list();
			DocumentListDTO dto = null;
			Iterator it = list1.iterator();
			while(it.hasNext()){
				Object []row= (Object [])it.next();
				dto = new DocumentListDTO();
				dto.setId(row[0]);
				dto.setAgentType(row[1]);
				dto.setFileType(row[2]);
				dto.setFileDescription(row[3]);
				dto.setFileSize(row[4]);
				dto.setUpdatedBy(row[5]);
				dto.setUpdatedOn(row[6]);
				dto.setCustomerNumber(row[7]);
				dto.setFileId(row[8]);
				dto.setFieldValue(fieldValue);
				list.add(dto);
			}
		} catch (Exception e) {
			 e.printStackTrace();	
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}		
		return list;		
	}
	 public List findDocumentFromMyFileSO(String seqNumber,String shpNumber,String fieldValue,String sessionCorpID){
		 List list = new ArrayList();
			String query="";
			try {
				query ="select id,"+fieldValue+",filetype,description,filesize,updatedby,updatedon,customernumber,fileid "
					+ "from myfile where corpid='"+sessionCorpID+"' and (customernumber='"+seqNumber+"' and fileid ='"+shpNumber+"') and "+fieldValue+" is true  and active is true ";
				List list1  = this.getSession().createSQLQuery(query).list();
				DocumentListDTO dto = null;
				Iterator it = list1.iterator();
				while(it.hasNext()){
					Object []row= (Object [])it.next();
					dto = new DocumentListDTO();
					dto.setId(row[0]);
					dto.setAgentType(row[1]);
					dto.setFileType(row[2]);
					dto.setFileDescription(row[3]);
					dto.setFileSize(row[4]);
					dto.setUpdatedBy(row[5]);
					dto.setUpdatedOn(row[6]);
					dto.setCustomerNumber(row[7]);
					dto.setFileId(row[8]);
					dto.setFieldValue(fieldValue);
					list.add(dto);
				}
			} catch (Exception e) {
				 e.printStackTrace();
				getSessionCorpID();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
		 return list;
	 }
	public class DocumentListDTO{
		private Object id;
		private Object agentType;
		private Object fileType;
		private Object fileDescription;
		private Object fileSize;
		private Object updatedBy;
		private Object updatedOn;
		private Object customerNumber;
		private Object fileId;
		private Object fieldValue;
		
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getAgentType() {
			return agentType;
		}
		public void setAgentType(Object agentType) {
			this.agentType = agentType;
		}
		public Object getFileType() {
			return fileType;
		}
		public void setFileType(Object fileType) {
			this.fileType = fileType;
		}
		public Object getFileDescription() {
			return fileDescription;
		}
		public void setFileDescription(Object fileDescription) {
			this.fileDescription = fileDescription;
		}
		public Object getFileSize() {
			return fileSize;
		}
		public void setFileSize(Object fileSize) {
			this.fileSize = fileSize;
		}
		public Object getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
		public Object getCustomerNumber() {
			return customerNumber;
		}
		public void setCustomerNumber(Object customerNumber) {
			this.customerNumber = customerNumber;
		}
		public Object getFileId() {
			return fileId;
		}
		public void setFileId(Object fileId) {
			this.fileId = fileId;
		}
		public Object getFieldValue() {
			return fieldValue;
		}
		public void setFieldValue(Object fieldValue) {
			this.fieldValue = fieldValue;
		}		
	}
	/*public void updateSOExtFalg(Long id){
		String query="update serviceorder set isSoExtract=false"
			+ " where  id='" + id+"'";
			
getSession().createSQLQuery(query).executeUpdate();

	}*/
	public Map<Long, TrackingStatus> getTrackingStatusObjectMap(Set serviceorderIdSet) {
		String ids = serviceorderIdSet.toString().replace("[", "") .replace("]", "");
		Map<Long, TrackingStatus> trackingStatusObjectMap= new LinkedHashMap<Long, TrackingStatus>();
		if(!(ids.trim().equals(""))){
		List trackingStatusList=getHibernateTemplate().find("from TrackingStatus where id in ("+ids+")");
		Iterator<TrackingStatus> it =trackingStatusList.iterator();
		while (it.hasNext()){
			TrackingStatus trackingStatus	=it.next();
			trackingStatusObjectMap.put(trackingStatus.getId(), trackingStatus);
		}
		}
		return trackingStatusObjectMap;
	}
	public List getvenderCodeDetais(String networkVendorCode){
		List venderCo=this.getSession().createSQLQuery("select bookingagentcode from companydivision where corpid=(select corpid from companydivision where bookingagentcode='"+networkVendorCode+"' limit 1)").list();
	 return venderCo;
	}
	public  class SoListByCFDTO
	{
		private Object id;
		private Object originAgentCode;
		private Object originAgent;
		private Object originAgentContact;
		private Object originAgentEmail;
		private Object OriginAgentPhoneNumber;
		private Object mode;
		private Object shipNumber;
		private Object originCity;
		private Object originState;
		private Object originCountry;
		private Object destinationAgentCode;
		private Object destinationAgent;
		private Object destinationAgentContact;
		private Object destinationAgentEmail;
		private Object destinationAgentPhoneNumber;
		private Object destinationCity;
		private Object destinationState;
		private Object destinationCountry;
		private Object originAgentExSO;
		private Object destinationAgentExSO;
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getOriginAgentCode() {
			return originAgentCode;
		}
		public void setOriginAgentCode(Object originAgentCode) {
			this.originAgentCode = originAgentCode;
		}
		public Object getOriginAgent() {
			return originAgent;
		}
		public void setOriginAgent(Object originAgent) {
			this.originAgent = originAgent;
		}
		public Object getOriginAgentContact() {
			return originAgentContact;
		}
		public void setOriginAgentContact(Object originAgentContact) {
			this.originAgentContact = originAgentContact;
		}
		public Object getOriginAgentEmail() {
			return originAgentEmail;
		}
		public void setOriginAgentEmail(Object originAgentEmail) {
			this.originAgentEmail = originAgentEmail;
		}
		public Object getOriginAgentPhoneNumber() {
			return OriginAgentPhoneNumber;
		}
		public void setOriginAgentPhoneNumber(Object originAgentPhoneNumber) {
			OriginAgentPhoneNumber = originAgentPhoneNumber;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getOriginCity() {
			return originCity;
		}
		public void setOriginCity(Object originCity) {
			this.originCity = originCity;
		}
		public Object getOriginState() {
			return originState;
		}
		public void setOriginState(Object originState) {
			this.originState = originState;
		}
		public Object getOriginCountry() {
			return originCountry;
		}
		public void setOriginCountry(Object originCountry) {
			this.originCountry = originCountry;
		}
		public Object getDestinationAgentCode() {
			return destinationAgentCode;
		}
		public void setDestinationAgentCode(Object destinationAgentCode) {
			this.destinationAgentCode = destinationAgentCode;
		}
		public Object getDestinationAgent() {
			return destinationAgent;
		}
		public void setDestinationAgent(Object destinationAgent) {
			this.destinationAgent = destinationAgent;
		}
		public Object getDestinationAgentContact() {
			return destinationAgentContact;
		}
		public void setDestinationAgentContact(Object destinationAgentContact) {
			this.destinationAgentContact = destinationAgentContact;
		}
		public Object getDestinationAgentEmail() {
			return destinationAgentEmail;
		}
		public void setDestinationAgentEmail(Object destinationAgentEmail) {
			this.destinationAgentEmail = destinationAgentEmail;
		}
		public Object getDestinationAgentPhoneNumber() {
			return destinationAgentPhoneNumber;
		}
		public void setDestinationAgentPhoneNumber(Object destinationAgentPhoneNumber) {
			this.destinationAgentPhoneNumber = destinationAgentPhoneNumber;
		}
		public Object getDestinationCity() {
			return destinationCity;
		}
		public void setDestinationCity(Object destinationCity) {
			this.destinationCity = destinationCity;
		}
		public Object getDestinationState() {
			return destinationState;
		}
		public void setDestinationState(Object destinationState) {
			this.destinationState = destinationState;
		}
		public Object getDestinationCountry() {
			return destinationCountry;
		}
		public void setDestinationCountry(Object destinationCountry) {
			this.destinationCountry = destinationCountry;
		}
		public Object getOriginAgentExSO() {
			return originAgentExSO;
		}
		public void setOriginAgentExSO(Object originAgentExSO) {
			this.originAgentExSO = originAgentExSO;
		}
		public Object getDestinationAgentExSO() {
			return destinationAgentExSO;
		}
		public void setDestinationAgentExSO(Object destinationAgentExSO) {
			this.destinationAgentExSO = destinationAgentExSO;
		}
		
	}	
	public List getSOListbyCF(Long cid,Long id){
	List resultList = new ArrayList();
	String query="";
	query="select s.id,ts.originAgentCode,ts.originAgent,ts.originAgentContact,ts.originAgentEmail,ts.OriginAgentPhoneNumber,s.mode,s.shipNumber,s.originCity,s.originState,s.originCountry,ts.destinationAgentCode,ts.destinationAgent,ts.destinationAgentContact,ts.destinationAgentEmail,ts.destinationAgentPhoneNumber,s.destinationCity,s.destinationState,s.destinationCountry,ts.originAgentExSO,ts.destinationAgentExSO from serviceorder s left outer join trackingstatus ts on s.id=ts.id where s.customerFileId='"+cid+"' and s.id not in ('"+id+"') order by s.shipNumber";
	List list= this.getSession().createSQLQuery(query).list();
	if(list!=null && !list.isEmpty()){
	Iterator it=list.iterator();
	SoListByCFDTO  soListByCFDTO=null;
	while(it.hasNext()) {
		Object [] row=(Object[])it.next(); 
		soListByCFDTO= new SoListByCFDTO();
		if(row[0] == null){soListByCFDTO.setId("");}else{soListByCFDTO.setId(row[0]);}
		if(row[1] == null){soListByCFDTO.setOriginAgentCode("");}else{soListByCFDTO.setOriginAgentCode(row[1]);}
		if(row[2] == null){soListByCFDTO.setOriginAgent("");}else{soListByCFDTO.setOriginAgent(row[2]);}
		if(row[3] == null){soListByCFDTO.setOriginAgentContact("");}else{soListByCFDTO.setOriginAgentContact(row[3]);}
		if(row[4] == null){soListByCFDTO.setOriginAgentEmail("");}else{soListByCFDTO.setOriginAgentEmail(row[4]);}
		if(row[5] == null){soListByCFDTO.setOriginAgentPhoneNumber("");}else{soListByCFDTO.setOriginAgentPhoneNumber(row[5]);}
		if(row[6] == null){soListByCFDTO.setMode("");}else{soListByCFDTO.setMode(row[6]);}
		if(row[7] == null){soListByCFDTO.setShipNumber("");}else{soListByCFDTO.setShipNumber(row[7]);}
		if(row[8] == null){soListByCFDTO.setOriginCity("");}else{soListByCFDTO.setOriginCity(row[8]);}
		if(row[9] == null){soListByCFDTO.setOriginState("");}else{soListByCFDTO.setOriginState(row[9]);}
		if(row[10] == null){soListByCFDTO.setOriginCountry("");}else{soListByCFDTO.setOriginCountry(row[10]);}
		if(row[11] == null){soListByCFDTO.setDestinationAgentCode("");}else{soListByCFDTO.setDestinationAgentCode(row[11]);}
		if(row[12] == null){soListByCFDTO.setDestinationAgent("");}else{soListByCFDTO.setDestinationAgent(row[12]);}
		if(row[13] == null){soListByCFDTO.setDestinationAgentContact("");}else{soListByCFDTO.setDestinationAgentContact(row[13]);}
		if(row[14] == null){soListByCFDTO.setDestinationAgentEmail("");}else{soListByCFDTO.setDestinationAgentEmail(row[14]);}
		if(row[15] == null){soListByCFDTO.setDestinationAgentPhoneNumber("");}else{soListByCFDTO.setDestinationAgentPhoneNumber(row[15]);}
		if(row[16] == null){soListByCFDTO.setDestinationCity("");}else{soListByCFDTO.setDestinationCity(row[16]);}
		if(row[17] == null){soListByCFDTO.setDestinationState("");}else{soListByCFDTO.setDestinationState(row[17]);}
		if(row[18] == null){soListByCFDTO.setDestinationCountry("");}else{soListByCFDTO.setDestinationCountry(row[18]);}
		if(row[19] == null){soListByCFDTO.setOriginAgentExSO("");}else{soListByCFDTO.setOriginAgentExSO(row[19]);}
		if(row[20] == null){soListByCFDTO.setDestinationAgentExSO("");}else{soListByCFDTO.setDestinationAgentExSO(row[20]);}
		resultList.add(soListByCFDTO);	
}	
	}
return resultList; 
}
	
public void updateTrackingStatusAgentDetails(StringBuffer query){
	this.getSession().createSQLQuery(query.toString()).executeUpdate();
}
public boolean getCMMContractTypeForOtherCorpid(String sessionCorpID, String contract) { 
	boolean type=false; 
	try {
		List list = this.getSession().createSQLQuery("select contractType from contract where contract='"+contract+"' and corpID='"+sessionCorpID+"'" ).list();	
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null && (list.get(0).toString().trim().equalsIgnoreCase("CMM")))
		{
			type=true;  
		}
	} catch (Exception e) {
		 e.printStackTrace();
		getSessionCorpID();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
	return type;
}
public boolean getDMMContractTypeForOtherCorpid(String sessionCorpID, String contract) { 
	boolean type=false; 
	try {
		List list = this.getSession().createSQLQuery("select contractType from contract where contract='"+contract+"' and corpID='"+sessionCorpID+"'" ).list();	
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null && (list.get(0).toString().trim().equalsIgnoreCase("DMM")))
		{
			type=true;  
		}
	} catch (Exception e) {
		e.printStackTrace();
		getSessionCorpID();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
	return type;
}

public String checkNetworkCoordinatorInUser(String cfCoordinatorname, String soCoordinatorname, String sessionCorpID){
	String type = "";
	List list = this.getSession().createSQLQuery("select count(*) from app_user where username='"+cfCoordinatorname+"' and networkCoordinatorStatus=TRUE " ).list();	
	if(list!=null && (!(list.isEmpty())) && list.get(0)!=null)
	{
		if(Integer.parseInt(list.get(0).toString()) > 0 ){
			 list = this.getSession().createSQLQuery("select count(*) from app_user where (username='"+soCoordinatorname+"') and networkCoordinatorStatus=TRUE " ).list();
			 if(list!=null && (!(list.isEmpty())) && list.get(0)!=null)
				{
					if(Integer.parseInt(list.get(0).toString()) > 0 ){
						type="true";  
					}
				}
		
		}
	}
	return type;
}

public String getRddDaysFromPartner(String fieldName,String soBillToCode,String sessionCorpID){
	String returnVal="";
	String query="select concat(rddBased,'~',if(rddDays is null or rddDays='',' ',rddDays)) from partnerprivate where corpid in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and partnercode ='"+soBillToCode+"' and status ='Approved' ";
	List al = this.getSession().createSQLQuery(query).list();
	if ((al != null) && (!al.isEmpty()) && (al.get(0)!=null)) {
		returnVal = al.get(0).toString();
	}	
	return returnVal;
}
	public List validatePreferredAgentCode(String partnerCode,String corpid, String billToCode){
			String preferredtempIdList = "";
			String query="";
				query="select preferredAgentCode from preferredagentforaccount where preferredAgentCode='"+partnerCode+"' and corpId='"+corpid+"' and partnercode='"+billToCode+"' and status is true";
				List al=this.getSession().createSQLQuery(query).list();
				if(al.size()>0 && !al.isEmpty() && al!=null){
					Iterator itr = al.iterator();
						while(itr.hasNext()){
							if(preferredtempIdList.equals("")){
								preferredtempIdList = "'"+itr.next().toString()+"'";
							}
							else{
								preferredtempIdList = preferredtempIdList+","+"'"+itr.next().toString()+"'";
							}
						}
				}
				if(preferredtempIdList.equals("")){
					preferredtempIdList="'"+preferredtempIdList+"'";
				}
						query=	" select partnerCode,if(partnerCode in ("+preferredtempIdList+"),'Y','N') as preferedagentType "+
						" from partner  where binary partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') and status='Approved' and isAgent = true group by partnerCode ";
				return this.getSession().createSQLQuery(query).list();
	}
	
	public void updateLinkedShipNumberforCompletedDate(Set linkedshipNumber,Date serviceCompleteDate,String corpID,String userName){
		try{
			
			List shipnumberList= new ArrayList();
			if(linkedshipNumber !=null){
			Iterator listIterator= linkedshipNumber.iterator();
			while (listIterator.hasNext()) {
	    		String ShipNo=listIterator.next().toString();
	    		if(ShipNo!=null && (!(ShipNo.trim().equalsIgnoreCase("")))){ 
	    			shipnumberList.add("'" + ShipNo + "'");	
	    		}
	    		
	    	}
			} 
			String shipnumber = shipnumberList.toString().replace("[", "").replace("]", "").replaceAll(" ", "");
			String updateBy=corpID+":"+userName;
			this.getSession().createSQLQuery("update trackingstatus set serviceCompleteDate='"+serviceCompleteDate+"',updatedBy='"+updateBy+"',updatedOn = now() where shipNumber in ("+shipnumber+")").executeUpdate();
		}
		catch(NullPointerException np){
			np.printStackTrace();
			logger.error("Error executing query"+np);
			String logMessage =  "Exception:"+ np.toString()+" at #"+np.getStackTrace()[0].toString();
			String logMethod =   np.getStackTrace()[0].getMethodName().toString();
			errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , np.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		catch(Exception e){
			e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public List getEmailFromPortalUser(String partnerCode,String corpid){
		    String query="";
		    try{
			query="select if(email is null or email='',' ',email) from app_user where parentAgent='"+partnerCode+"' and corpID in ('TSFT') and isDefaultContactPerson is true ";
			return this.getSession().createSQLQuery(query).list();
		    }catch(Exception ex){
		    	ex.printStackTrace();
		    }
			return null;
			
			
}
	 public String loadInfoByCode(String originCode,String corpId) 
	 { String returnVal="";
			String query= "select concat(contactName ,'~',email,'~',phone) from partnerprivate where partnerCode='"+originCode+"' and corpID in ('"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')";	
		  List al = this.getSession().createSQLQuery(query).list();
			if ((al != null) && (!al.isEmpty()) && (al.get(0)!=null)) {
				returnVal = al.get(0).toString();
			}	
	return returnVal;
	
}
	public String checkAllInvoicedLinkedAccountLines(String shipnumber,String linkedshipnumberTS, String contractType) {
		boolean checkAllInvoiced=true;
		String checkInvoiced="true";
		if(contractType!=null && contractType.trim().equalsIgnoreCase("DMM")){
		String returnVal="";
		String query= "select id,networkSynchedId,recinvoicenumber,corpid,chargecode from accountline where shipnumber in('"+shipnumber+"','"+linkedshipnumberTS+"') and status is true and actualRevenue !=0";	
	    List<Long> UTSIInvoiced = new ArrayList<Long>();
	    List<Long> UTSINonInvoiced = new ArrayList<Long>();
	    List<Long> agentInvoiced = new ArrayList<Long>();
	    List<Long> agentNonInvoiced = new ArrayList<Long>();
		List al = this.getSession().createSQLQuery(query).list();
		Iterator itr = al.iterator();
		while(itr.hasNext()){
			 try{
			Object [] row=(Object[])itr.next(); 
			if(row[1] !=null && row[3] != null && row[3].toString().equalsIgnoreCase("UTSI") && row[2] != null && (!(row[2].toString().trim().equals("")))){
				UTSIInvoiced.add(Long.parseLong(row[1].toString()));
				}
			else if(row[1] !=null && row[3] != null && row[3].toString().equalsIgnoreCase("UTSI") ){
				UTSINonInvoiced.add(Long.parseLong(row[1].toString()));
				}
			else if(row[3] != null && (!(row[3].toString().equalsIgnoreCase("UTSI"))) && row[2] != null && (!(row[2].toString().trim().equals("")))){
				agentInvoiced.add(Long.parseLong(row[0].toString()));
				}
			else if(row[3] != null && (!(row[3].toString().equalsIgnoreCase("UTSI"))) ){
				agentNonInvoiced.add(Long.parseLong(row[0].toString()));
				}
			 }catch(Exception ex){
			    	ex.printStackTrace();
			    }
		}
		
		Iterator<Long> it = UTSIInvoiced.iterator();
		while(it.hasNext()){
			 try{
			Long id =it.next(); 
			if(checkAllInvoiced && (!(agentInvoiced.contains(id))) && agentNonInvoiced.contains(id)){
				checkAllInvoiced=false;	
				checkInvoiced="false";
			}
			 }catch(Exception ex){
			    	ex.printStackTrace();
			    }
		}
		}
		return checkInvoiced;
	}
}