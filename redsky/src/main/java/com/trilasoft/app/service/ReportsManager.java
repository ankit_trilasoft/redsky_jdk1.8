package com.trilasoft.app.service;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.appfuse.model.Role;
import org.appfuse.service.GenericManager;   

import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.Reports;   

import java.util.List;   
import java.util.Map;
import java.util.Set;
  
public interface ReportsManager extends GenericManager<Reports, Long> {   
	
    public List findByModule(String module, String corpID);
    
    public List findByModuleCoordinator(String corpID);
    
    public List findByMode();
    
    public List findByModuleEstimator(String sessionCorpID);
    
    public Map<String,String> findByCommodity(String sessionCorpID);
    
    public Map<String,String> findByModuleReports(String corpID);
    
    public List findByCriteria(String module, String subModule, String menu, String reportName, String description, String corpID, String type);

	public JasperPrint generateReport(Long reportId, Map parameters);
	
	public JasperPrint generateReport(Long reportId);

	public JasperReport getReportTemplate(Long reportId);
	
	public List findByCorpID(String corpID);
	
	public List findByEnabled(String module , String menu, String subModule, String description, String sessionCorpID,String userName, Set<Role> roles,Boolean SalesPortalAccess);
	
	public List findByDocsxfer();
	
	
	public List findByFormFlag(String userName ,String module , String menu, String subModule, String description, String sessionCorpID);
	
	public List findBySubModule(String module, String subModule, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes);
	
	public List findBySubModule1(String module, String subModule, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String recInvNumb,String jobNumber);
	public List findByModuleCriteria(String subModule, String module, String menu, String description, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String routingfilter);
	
	public List findReportByUserCriteria(String module, String menu, String reportName, String description, String corpID);
	
	public List findFormByUserCriteria(String module, String menu, String reportName, String description, String corpID);
	
	public List findByRelatedModuleCriteria(String subModule, String module, String menu, String description, String corpID, String enabled);

	public List getReportsValidationFlag(String sessionCorpID);

	public Map<String, String> findByPayableStatus();
	
	public Map<String, String> findByPayingStatus();
	
	public Map<String, String> findByOmni();

	public List findCrewName(String sessionCorpID);
	
	public List findByCompanyCode(String sessionCorpID);
	
	public List findUserRoles(String corpID);

	public Map<String, String> findByRouting(String sessionCorpID);
	
	public Map<String, String> findByJob(String sessionCorpID);
	
	public Map<String, String> findByCurrency();
	
	public Map<String, String> findByBank(String sessionCorpID);

	public void saveReport(Reports reports);
	public String getDefaultStationary(String jobNumber, String invoiceNumber,String sessionCorpID);
	public String generateReportExtract(Long reportId, Map parameters);
	public List findContract(String sessionCorpID);
	public Map<String, String> findByBaseScore(String sessionCorpID);
	public List findBySubModules(String module, String subModule, String corpID, String enabled);
	public List findBySubModules1(String module, String subModule,String companyDivision,String jobType,String billToCode,String modes, String corpID, String enabled,Long id,String jobNumber);

	public Map<String, String> findOwner(String corpId, String parameter);
	public Map<String, String> findJobOwner(String corpId, String parameter);
	public List checkBillCodes(String billCode, String corpID);

	public List checkModes(String modes, String corpID);
	public void updateTableStatus(String modelName,String fieldName,String value,Long sid);
	
	public List getServiceOrderGraph(String sessionCorpID, String jrxmlName);
	public void updateTableStatusWithCondition(String modelName,String fieldName,String value,String condition,Long sid);
	public void updateTableStatusWithCondition1(String modelName,String fieldName,String value,String condition,String sid);
	public Reports getReportTemplateForWT(String jrxmlName,String module,String corpId);
	public List<PrintDailyPackage> getByCorpId(String corpId);
	public JasperPrint generateReport(JasperReport jasperReport, Map parameters);
	public List getdescription(String corpId,String module);
	public String getReportCommentByJrxmlName(String jrxmlName,String corpId);
	public List getReportCommentByModule(String module,String corpId);
	public List getReportId(String corpId);
	public Map<String,String> findByCommodities(String sessionCorpID);
	public String getBillToCodeByFormName(String jrxmlName,String corpId);
	public List findByRedskyCustomer();
	public List findByRedskyCustomerCorp(String RedSkyCustomer);
	public List findCrewType(String sessionCorpID);
	public List findQuotationFormList(String corpID, String companyDivision,String jobType,String billToCode,String modes,String jobNumber);
	public String billToDefaultStationary(String sessionCorpID,	String invoiceNumber);
	public Map<String, String> findRelocationServices(String sessionCorpID);
	public List findRelatedSO(String sessionCorpID, String jobNumber, String custID);
	public List getVanLineCodeCompDiv(String sessionCorpID);
	public List findContainer(String sessionCorpID, String shipNumber);

	public JasperReport getReportTemplate(Long id, String preferredLanguage);
	
	public Map<String,String> findByCompanyDivisions(String sessionCorpID);
	public List getIdfromMyfileforServiceOrder(String JobNumber,String sessionCorpID,String invoiceNumber,String reportName);
	public String getAgentRoleList(Long reportId);

	public List findAgentGroup(String sessionCorpID);
	public List findBySubModuleFromAccountline(String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String shipNumber);
	public String getDefaultStationaryFromServiceorder(String shipNumber,String sessionCorpID);
	public List findFormsForAutocomplete(String moduleName,String jrxmlName,String corpId);
	public List findReportId(String jrxmlName,String corpId);
}   
