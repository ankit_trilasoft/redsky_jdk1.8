<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Commission Structure List</title>   
    <meta name="heading" content="Commission Structure List"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-16px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;

}
</style>
<script type="text/javascript">  
</script>
</head>
<s:form id="commissionStructureForm" action="" method="post" validate="true">
<div id="layer1" style="width:100%;">
<div id="newmnav">
	  <ul>
	  	  	<li><a href="salesCommissionMgmt.html"><span>Sale Commission List</span></a></li>	  	
				<li><a ><span>Commission Structure</span></a></li>					  	
	  </ul>
</div>
<div style="width: 100%" >
<div class="spn">&nbsp;</div>
</div>
<div id="content" align="center">
<div id="Layer1" style="width: 100%;">

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>				
<display:table name="contractList" class="table" id="contractList" requestURI="" pagesize="25" style="width:100%;"> 
	<display:column sortable="true" title="Contract Name" style="width:30px">
	<a href="javascript:openWindow('commissionStructureList.html?sContarct=${contractList.contract}&sChargeCode=&decorator=popup&popup=true');">
	<c:out value="${contractList.contract}"/>
	</a>
	</display:column>		
</display:table>
			</td>
		</tr>
	</tbody>
</table> 
</div>
</div>
</s:form>