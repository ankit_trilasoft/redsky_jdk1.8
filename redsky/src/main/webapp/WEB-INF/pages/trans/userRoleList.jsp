<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title>User Role List</title> 
<meta name="heading" content="User Role List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['userRole'].elements[i].value = "";
			}
}


function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}
</script>

</head>

	<s:form cssClass="form_magn" id="userRole" name="userRole" action="userRoleListSearch" method="post" validate="true">   
	<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
   <div class="center-content">		
	<table class="table">
	<thead>
	<tr>
	<th>Role</th>
	<th>userName</th>
	<th>&nbsp;</th>
	</tr>
	</thead>	
	<tbody>
		<tr>			
		    <td width="" align="left"><s:select name="roleName" list="%{roleList}"  cssClass="list-menu"  cssStyle="width:328px"  headerKey="" headerValue=""/></td>
			<td width="" align="left"><s:textfield name="userName" required="true" cssClass="input-text" size="50" onkeydown="return quotesNotAllowed(event);"/></td>
			<td>
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
   			</td>
			 </tr>			 
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>User Role List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="userRoleList" class="table" requestURI="" id="userRoleListId" export="true" defaultsort="1" style="width:100%" >   
	<display:column property="username"  href="editUser.html?from=list" paramId="id" paramProperty="id" sortable="true" title="User Name"/>
   	<display:column property="first_name"  sortable="true" title="First Name"/>
   	<display:column property="last_name"  sortable="true" title="Last Name"/>
   	<display:column  sortable="true" title="Enabled" style="width: 25%">
    <c:if test="${userRoleListId.account_enabled==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${userRoleListId.account_enabled==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	</display:column>
   	</display:table>
	
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 

<script type="text/javascript"> 

highlightTableRows("userRoleListId"); 

</script> 
		
		