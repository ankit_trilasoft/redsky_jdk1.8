package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="sqlextract")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class SQLExtract extends BaseObject{
	private Long id;
	private String description;
	private String sqlText;
	private String fileName;
	private String tested;
	private String fileType;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String whereClause1;
	private String whereClause2;
	private String whereClause3;
	private String whereClause4;
	private String whereClause5;
	private String orderBy;
	private String groupBy;
	private Boolean isdateWhere1;
	private Boolean isdateWhere2;
	private Boolean isdateWhere3;
	private Boolean isdateWhere4;
	private Boolean isdateWhere5;
	private String displayLabel1;
	private String displayLabel2;
	private String displayLabel3;
	private String displayLabel4;
	private String displayLabel5;
	private String defaultCondition1;
	private String defaultCondition2;
	private String defaultCondition3;
	private String defaultCondition4;
	private String defaultCondition5;
	private String email;
	private String sqlQueryScheduler;
	private String perWeekScheduler;
	private String perMonthScheduler;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("description",
				description).append("sqlText", sqlText).append("fileName",
				fileName).append("tested", tested).append("fileType", fileType)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn).append("whereClause1",
						whereClause1).append("whereClause2", whereClause2)
				.append("whereClause3", whereClause3).append("whereClause4",
						whereClause4).append("whereClause5", whereClause5)
				.append("orderBy", orderBy).append("groupBy", groupBy).append(
						"isdateWhere1", isdateWhere1).append("isdateWhere2",
						isdateWhere2).append("isdateWhere3", isdateWhere3)
				.append("isdateWhere4", isdateWhere4).append("isdateWhere5",
						isdateWhere5).append("displayLabel1", displayLabel1)
				.append("displayLabel2", displayLabel2).append("displayLabel3",
						displayLabel3).append("displayLabel4", displayLabel4)
				.append("displayLabel5", displayLabel5).append(
						"defaultCondition1", defaultCondition1).append(
						"defaultCondition2", defaultCondition2).append(
						"defaultCondition3", defaultCondition3).append(
						"defaultCondition4", defaultCondition4).append(
						"defaultCondition5", defaultCondition5).append("email",
						email).append("sqlQueryScheduler",sqlQueryScheduler)
				        .append("perWeekScheduler",perWeekScheduler)
				        .append("perMonthScheduler",perMonthScheduler)
				       .toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SQLExtract))
			return false;
		SQLExtract castOther = (SQLExtract) other;
		return new EqualsBuilder().append(id, castOther.id).append(description,
				castOther.description).append(sqlText, castOther.sqlText)
				.append(fileName, castOther.fileName).append(tested,
						castOther.tested).append(fileType, castOther.fileType)
				.append(corpID, castOther.corpID).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(whereClause1,
						castOther.whereClause1).append(whereClause2,
						castOther.whereClause2).append(whereClause3,
						castOther.whereClause3).append(whereClause4,
						castOther.whereClause4).append(whereClause5,
						castOther.whereClause5).append(orderBy,
						castOther.orderBy).append(groupBy, castOther.groupBy)
				.append(isdateWhere1, castOther.isdateWhere1).append(
						isdateWhere2, castOther.isdateWhere2).append(
						isdateWhere3, castOther.isdateWhere3).append(
						isdateWhere4, castOther.isdateWhere4).append(
						isdateWhere5, castOther.isdateWhere5).append(
						displayLabel1, castOther.displayLabel1).append(
						displayLabel2, castOther.displayLabel2).append(
						displayLabel3, castOther.displayLabel3).append(
						displayLabel4, castOther.displayLabel4).append(
						displayLabel5, castOther.displayLabel5).append(
						defaultCondition1, castOther.defaultCondition1).append(
						defaultCondition2, castOther.defaultCondition2).append(
						defaultCondition3, castOther.defaultCondition3).append(
						defaultCondition4, castOther.defaultCondition4).append(
						defaultCondition5, castOther.defaultCondition5).append(
						email, castOther.email).append(sqlQueryScheduler, castOther.sqlQueryScheduler)
				        .append(perWeekScheduler, castOther.perWeekScheduler)
				        .append(perMonthScheduler, castOther.perMonthScheduler)
				       .isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(description).append(
				sqlText).append(fileName).append(tested).append(fileType)
				.append(corpID).append(createdBy).append(updatedBy).append(
						updatedOn).append(createdOn).append(whereClause1)
				.append(whereClause2).append(whereClause3).append(whereClause4)
				.append(whereClause5).append(orderBy).append(groupBy).append(
						isdateWhere1).append(isdateWhere2).append(isdateWhere3)
				.append(isdateWhere4).append(isdateWhere5)
				.append(displayLabel1).append(displayLabel2).append(
						displayLabel3).append(displayLabel4).append(
						displayLabel5).append(defaultCondition1).append(
						defaultCondition2).append(defaultCondition3).append(
						defaultCondition4).append(defaultCondition5).append(
						email).append(sqlQueryScheduler).append(perWeekScheduler)
				        .append(perMonthScheduler).toHashCode();
	}
	public Boolean getIsdateWhere1() {
		return isdateWhere1;
	}
	public void setIsdateWhere1(Boolean isdateWhere1) {
		this.isdateWhere1 = isdateWhere1;
	}
	public Boolean getIsdateWhere2() {
		return isdateWhere2;
	}
	public void setIsdateWhere2(Boolean isdateWhere2) {
		this.isdateWhere2 = isdateWhere2;
	}
	public Boolean getIsdateWhere3() {
		return isdateWhere3;
	}
	public void setIsdateWhere3(Boolean isdateWhere3) {
		this.isdateWhere3 = isdateWhere3;
	}
	public Boolean getIsdateWhere4() {
		return isdateWhere4;
	}
	public void setIsdateWhere4(Boolean isdateWhere4) {
		this.isdateWhere4 = isdateWhere4;
	}
	public Boolean getIsdateWhere5() {
		return isdateWhere5;
	}
	public void setIsdateWhere5(Boolean isdateWhere5) {
		this.isdateWhere5 = isdateWhere5;
	}
	@Column(length=45)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length=255)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length=255)
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(length=100)
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Column
	public String getSqlText() {
		return sqlText;
	}
	public void setSqlText(String sqlText) {
		this.sqlText = sqlText;
	}

	@Column(length=10)
	public String getTested() {
		return tested;
	}
	public void setTested(String tested) {
		this.tested = tested;
	}
	public String getGroupBy() {
		return groupBy;
	}
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getWhereClause1() {
		return whereClause1;
	}
	public void setWhereClause1(String whereClause1) {
		this.whereClause1 = whereClause1;
	}
	public String getWhereClause2() {
		return whereClause2;
	}
	public void setWhereClause2(String whereClause2) {
		this.whereClause2 = whereClause2;
	}
	public String getWhereClause3() {
		return whereClause3;
	}
	public void setWhereClause3(String whereClause3) {
		this.whereClause3 = whereClause3;
	}
	public String getWhereClause4() {
		return whereClause4;
	}
	public void setWhereClause4(String whereClause4) {
		this.whereClause4 = whereClause4;
	}
	public String getWhereClause5() {
		return whereClause5;
	}
	public void setWhereClause5(String whereClause5) {
		this.whereClause5 = whereClause5;
	}
	public String getDefaultCondition1() {
		return defaultCondition1;
	}
	public void setDefaultCondition1(String defaultCondition1) {
		this.defaultCondition1 = defaultCondition1;
	}
	public String getDefaultCondition2() {
		return defaultCondition2;
	}
	public void setDefaultCondition2(String defaultCondition2) {
		this.defaultCondition2 = defaultCondition2;
	}
	public String getDefaultCondition3() {
		return defaultCondition3;
	}
	public void setDefaultCondition3(String defaultCondition3) {
		this.defaultCondition3 = defaultCondition3;
	}
	public String getDefaultCondition4() {
		return defaultCondition4;
	}
	public void setDefaultCondition4(String defaultCondition4) {
		this.defaultCondition4 = defaultCondition4;
	}
	public String getDefaultCondition5() {
		return defaultCondition5;
	}
	public void setDefaultCondition5(String defaultCondition5) {
		this.defaultCondition5 = defaultCondition5;
	}
	public String getDisplayLabel1() {
		return displayLabel1;
	}
	public void setDisplayLabel1(String displayLabel1) {
		this.displayLabel1 = displayLabel1;
	}
	public String getDisplayLabel2() {
		return displayLabel2;
	}
	public void setDisplayLabel2(String displayLabel2) {
		this.displayLabel2 = displayLabel2;
	}
	public String getDisplayLabel3() {
		return displayLabel3;
	}
	public void setDisplayLabel3(String displayLabel3) {
		this.displayLabel3 = displayLabel3;
	}
	public String getDisplayLabel4() {
		return displayLabel4;
	}
	public void setDisplayLabel4(String displayLabel4) {
		this.displayLabel4 = displayLabel4;
	}
	public String getDisplayLabel5() {
		return displayLabel5;
	}
	public void setDisplayLabel5(String displayLabel5) {
		this.displayLabel5 = displayLabel5;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSqlQueryScheduler() {
		return sqlQueryScheduler;
	}
	public void setSqlQueryScheduler(String sqlQueryScheduler) {
		this.sqlQueryScheduler = sqlQueryScheduler;
	}
	public String getPerWeekScheduler() {
		return perWeekScheduler;
	}
	public void setPerWeekScheduler(String perWeekScheduler) {
		this.perWeekScheduler = perWeekScheduler;
	}
	public String getPerMonthScheduler() {
		return perMonthScheduler;
	}
	public void setPerMonthScheduler(String perMonthScheduler) {
		this.perMonthScheduler = perMonthScheduler;
	}
		}
