package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsRelocationAreaInfoOrientation;
import com.trilasoft.app.model.Miscellaneous;

import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsRelocationAreaInfoOrientationManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;

import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class DsRelocationAreaInfoOrientationAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private DsRelocationAreaInfoOrientationManager dsRelocationAreaInfoOrientationManager;
	private DsRelocationAreaInfoOrientation dsRelocationAreaInfoOrientation;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private  Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countDsRelocationAreaInfoOrientationNotes;
	private NotesManager notesManager;
	  private String validateFormNav;
		private String gotoPageString;
    public void prepare() throws Exception { 
			
		}
    
    public DsRelocationAreaInfoOrientationAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
    }
	private String shipSize;
	private String minShip;
	private String countShip;
	private Map<String, String> relocationServices;
	private RefMasterManager refMasterManager;
    
    public String edit() { 
    	if (id != null) {
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	dsRelocationAreaInfoOrientation=dsRelocationAreaInfoOrientationManager.get(id);
    	}
    	else{
    		dsRelocationAreaInfoOrientation=new DsRelocationAreaInfoOrientation();
    		dsRelocationAreaInfoOrientation.setCreatedOn(new Date());
    		dsRelocationAreaInfoOrientation.setUpdatedOn(new Date());
    		dsRelocationAreaInfoOrientation.setServiceOrderId(serviceOrderId);
    	}
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
    	
    	getNotesForIconChange();
    	return SUCCESS;
    }
public String save() throws Exception {
		
		boolean isNew = (dsRelocationAreaInfoOrientation.getId() == null);  
				if (isNew) { 
					dsRelocationAreaInfoOrientation.setCreatedOn(new Date());
		        }
				trackingStatus=trackingStatusManager.get(id);
				serviceOrder=serviceOrderManager.get(id);
		    	customerFile = serviceOrder.getCustomerFile();
		    	miscellaneous = miscellaneousManager.get(id); 
		    	dsRelocationAreaInfoOrientation.setCorpID(sessionCorpID);
		    	dsRelocationAreaInfoOrientation.setUpdatedOn(new Date());
		    	dsRelocationAreaInfoOrientation.setUpdatedBy(getRequest().getRemoteUser()); 
		    	dsRelocationAreaInfoOrientationManager.save(dsRelocationAreaInfoOrientation);  
		    	if(!(validateFormNav.equals("OK"))){
		    	String key = (isNew) ? "Successfully Added Area Information Details" : "Successfully Updated Area Information Details";
			    saveMessage(getText(key));
		    	}
			    getNotesForIconChange();
			    return SUCCESS;
				
			
	}
@SkipValidation
public String saveOnTabChange() throws Exception {
	validateFormNav = "OK";
	String s = save();
	return SUCCESS;
}
@SkipValidation
public String getNotesForIconChange() {
	List noteDS = notesManager.countRelocationAreaInfoOrientationNotes(serviceOrder.getShipNumber());
	
	if (noteDS.isEmpty()) {
		countDsRelocationAreaInfoOrientationNotes = "0";
	} else {
		countDsRelocationAreaInfoOrientationNotes = ((noteDS).get(0)).toString();
	}

	return SUCCESS;
}
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}



	public void setDsRelocationAreaInfoOrientationManager(
			DsRelocationAreaInfoOrientationManager dsRelocationAreaInfoOrientationManager) {
		this.dsRelocationAreaInfoOrientationManager = dsRelocationAreaInfoOrientationManager;
	}

	public DsRelocationAreaInfoOrientation getDsRelocationAreaInfoOrientation() {
		return dsRelocationAreaInfoOrientation;
	}

	public void setDsRelocationAreaInfoOrientation(
			DsRelocationAreaInfoOrientation dsRelocationAreaInfoOrientation) {
		this.dsRelocationAreaInfoOrientation = dsRelocationAreaInfoOrientation;
	}

	public String getCountDsRelocationAreaInfoOrientationNotes() {
		return countDsRelocationAreaInfoOrientationNotes;
	}

	public void setCountDsRelocationAreaInfoOrientationNotes(
			String countDsRelocationAreaInfoOrientationNotes) {
		this.countDsRelocationAreaInfoOrientationNotes = countDsRelocationAreaInfoOrientationNotes;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	/**
	 * @return the countShip
	 */
	public String getCountShip() {
		return countShip;
	}

	/**
	 * @param countShip the countShip to set
	 */
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	/**
	 * @return the minShip
	 */
	public String getMinShip() {
		return minShip;
	}

	/**
	 * @param minShip the minShip to set
	 */
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	/**
	 * @return the relocationServices
	 */
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	/**
	 * @param relocationServices the relocationServices to set
	 */
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}

	/**
	 * @return the shipSize
	 */
	public String getShipSize() {
		return shipSize;
	}

	/**
	 * @param shipSize the shipSize to set
	 */
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
}
