DELIMITER $$
CREATE FUNCTION `GetServicePartnerInfo`(so_id bigint(20),var  varchar(10)) RETURNS varchar(270)
BEGIN

declare resultVar varchar(270);
declare carrierDetal varchar(270);
declare bookNum varchar(150);
declare omniDesc varchar(500);

if (var= 'carrier') then 
  select replace(replace(replace(replace(replace(replace(group_concat(concat(concat('''',carrierCode),',',carrierName)separator ','),'\r\n','~^'),'\r',' '),'\t',''),'\"','^'),' ',' '),'\n','~^')
  into carrierDetal 
  from servicepartner 
  where serviceOrderId=so_id 
  and status is true ;
set resultVar=carrierDetal;
  end if;
   if (var= 'bookNum') then 
  select if(replace(group_concat(bookNumber separator '#'),'##','#')='#','',replace(replace(replace(replace(replace(replace(replace(group_concat(bookNumber separator '#'),'\r\n','~^'),'\r',' '),'\t',''),'\"','^'),' ',' '),'\n','~^'),'##','#')) into bookNum 
  from servicepartner 
  where status is true
  and serviceOrderId=so_id 
  and status is true ; 
  set resultVar= CASE  WHEN RIGHT(RTRIM(bookNum),1) = '#' THEN LEFT(bookNum,LENgth(bookNum)-1) ELSE bookNum  END ;
   end if;


   if (var= 'omniDes') then  
  select  if(replace(group_concat(r.description separator ','),',,',',')=',','',replace(group_concat(r.description separator ','),',,',',')) into omniDesc 
  from servicepartner sp
inner join refmaster r 
on r.code= sp.omni 
and r.corpid=sp.corpid
where r.parameter='omni'
and sp.status is true 
and sp.serviceOrderId=so_id;
set resultVar=CASE  WHEN RIGHT(RTRIM(omniDesc),1) = ',' THEN LEFT(omniDesc,LENgth(omniDesc)-1)  ELSE omniDesc  END;
   end if;
RETURN resultVar;
end$$
DELIMITER ;