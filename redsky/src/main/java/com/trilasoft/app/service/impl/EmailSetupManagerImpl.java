package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.cglib.core.EmitUtils;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.EmailSetupDao;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.service.EmailSetupManager;

public class EmailSetupManagerImpl extends GenericManagerImpl<EmailSetup, Long> implements EmailSetupManager{
	EmailSetupDao emailSetupDao;
	public EmailSetupManagerImpl(EmailSetupDao emailSetupDao)
	{
		super(emailSetupDao);
		this.emailSetupDao=emailSetupDao;
		
	}
	public Map<String, String> getEmailStatusWithId(String ids,String sessionCorpID){
		return emailSetupDao.getEmailStatusWithId(ids,sessionCorpID);
	}
	public List getMaxId(){
		return emailSetupDao.getMaxId();
	}
	public void globalEmailSetupProcess(String from,String to,String cc,String bcc,String location,String body,String subject,String corpId,String module,String fileNumber,String signaturePart){
		emailSetupDao.globalEmailSetupProcess(from, to, cc, bcc, location, body,subject,corpId,module,fileNumber, signaturePart);
	}
	public List findEmailSetUpList(String recipientTo,String fileNumber ,String signature,String subject,Date dateSent,String type,String sessionCorpID){
		return emailSetupDao.findEmailSetUpList(recipientTo,fileNumber,signature,subject,dateSent,type,sessionCorpID);
	}
	public String findEmailStatus(String sessionCorpID ,String fileNumber) {
		return emailSetupDao.findEmailStatus(sessionCorpID,fileNumber);
	}
	public int getEmailSetupCount(String sessionCorpID ,String fileNumber){
		return emailSetupDao.getEmailSetupCount(sessionCorpID,fileNumber);
	}
	public void insertMailFromEmailSetupTemplate(String from,String to,String cc,String bcc,String location,String body,String subject,String corpId,String module,String fileNumber,String signaturePart,String saveAs,String lastSentMailBody){
		emailSetupDao.insertMailFromEmailSetupTemplate(from, to, cc, bcc, location, body,subject,corpId,module,fileNumber, signaturePart,saveAs, lastSentMailBody);
	}
}
