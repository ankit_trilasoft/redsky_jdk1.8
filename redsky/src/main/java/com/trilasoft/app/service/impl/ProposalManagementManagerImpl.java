package com.trilasoft.app.service.impl;
import com.trilasoft.app.dao.ProposalManagementDao;
import com.trilasoft.app.model.ProposalManagement;
import com.trilasoft.app.service.ProposalManagementManager;
import org.appfuse.service.impl.GenericManagerImpl;

import java.util.Date;
import java.util.List;

public class ProposalManagementManagerImpl extends GenericManagerImpl<ProposalManagement, Long> implements ProposalManagementManager {
	ProposalManagementDao proposalManagementDao;

	public ProposalManagementManagerImpl(ProposalManagementDao proposalManagementDao) {
        super(proposalManagementDao);
        this.proposalManagementDao = proposalManagementDao;
    }

	public List findDistinct(){
    	return proposalManagementDao.findDistinct();
    }
	public List findListByModuleCorpStatus(String module,String corpid,String status , Date approvalDate , Date initiationDate,String ticketNo,Boolean activeStatus){
		return proposalManagementDao.findListByModuleCorpStatus(module, corpid, status ,approvalDate ,initiationDate, ticketNo, activeStatus); 
	}
	  public String findProposalDescForToolTip(Long id, String sessionCorpID)
	  {
		  return proposalManagementDao.findProposalDescForToolTip(id,sessionCorpID);
	  }
	public void autoSaveProposalManagementAjax(String fieldName,String fieldVal,Long serviceId){
		proposalManagementDao.autoSaveProposalManagementAjax(fieldName, fieldVal, serviceId);
	}
	public List listOnLoad(){
		return proposalManagementDao.listOnLoad();
	 }
}
