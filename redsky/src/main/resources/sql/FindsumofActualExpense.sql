DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`FindsumofActualExpense` $$
CREATE DEFINER=`root`@`%` FUNCTION `FindsumofActualExpense`(v1 VARCHAR(50)) RETURNS double
BEGIN
   DECLARE i1,avg Double;
     select case when ((invoicenumber!='' or invoicenumber is not null) and (payingStatus='A'))
     then sum(actualexpense) else '0' end INTO i1 from accountline where
     invoicenumber!='' and payingStatus='A' and shipnumber=v1;
     
    SET avg = i1;

    RETURN avg;
END $$

DELIMITER ;