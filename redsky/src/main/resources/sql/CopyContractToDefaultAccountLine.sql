DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`CopyContractToDefaultAccountLine` $$
CREATE PROCEDURE `CopyContractToDefaultAccountLine`(NCorpid varchar(15),FromContract varchar(100), ToContract varchar(100),IdString LONGTEXT  )
begin

	
DROP TABLE IF EXISTS defaultaccountlinetemp;
create table defaultaccountlinetemp
(SELECT * FROM defaultaccountline  WHERE FIND_IN_SET(id, IdString));
update defaultaccountlinetemp set id=NULL ,contract=ToContract,createdon=now();
 
insert into defaultaccountline
select * from defaultaccountlinetemp;

END $$

DELIMITER ; 