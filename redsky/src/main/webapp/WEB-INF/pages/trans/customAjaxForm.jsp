<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>
    
<script language="JavaScript" type="text/javascript" >
 function setCountry() {    
	      var move = document.forms['customForm'].elements['custom.movement'].value; 
	      if(move=='In'){ 
				document.forms['customForm'].elements['custom.documentType'].value = 'T-1'; 
	  	  }else{ 
			   document.forms['customForm'].elements['custom.documentType'].value = '';
	      }
			 
    }
       
 function customsInMovement() {
   var sid=document.forms['customForm'].elements['serviceOrderId'].value;
   var move = document.forms['customForm'].elements['custom.movement'].value;      
   if(move=='Out') { 
	   var url="findCustomsIn.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid);
	   http66.open("GET", url, true);
	   http66.onreadystatechange = handleHttpResponse444;
	   http66.send(null);
   }else {
   	document.forms['customForm'].elements['custom.transactionId'].value='';
   }
 }    
     
  function handleHttpResponse444()  {	 
	    if (http66.readyState == 4)  {		    
            var results = http66.responseText
            results = results.trim();
            resu = results.replace("[",'');
            resu = resu.replace("]",'');
		    var res = resu.split(",");
     		targetElement = document.forms['customForm'].elements['custom.transactionId'];
			targetElement.length = res.length;
           	for(i=1;i<res.length;i++) {
           	if(res[i] == ''){
				document.forms['customForm'].elements['custom.transactionId'].options[i].text = '';
				document.forms['customForm'].elements['custom.transactionId'].options[i].value = '';
		    }else{
				document.forms['customForm'].elements['custom.transactionId'].options[0].text = '';
				document.forms['customForm'].elements['custom.transactionId'].options[0].value = '';				
				document.forms['customForm'].elements['custom.transactionId'].options[i].text = res[i].trim();
				document.forms['customForm'].elements['custom.transactionId'].options[i].value = res[i].trim();
				document.forms['customForm'].elements['custom.transactionId'].options[i].selected = true;
		     }
           }
          document.forms['customForm'].elements['custom.transactionId'].value='${custom.transactionId}';
         }
     }   
  
 
 
 var http6 = getHTTPObject2();
 function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
   }
   
 var http66 = getHTTPObject22();
 function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
   }
   
   function movet(){ 
   if(document.forms['customForm'].elements['custom.movement'].value==''){
       alert("Movement is required field");
       document.forms['customForm'].elements['custom.movement'].focus();
	    return false;
       } 
   if(document.forms['customForm'].elements['custom.status'].value==''){
       alert("Status is required field");
       document.forms['customForm'].elements['custom.status'].focus();
       return false;
       }
   if(document.forms['customForm'].elements['custom.movement'].value == 'Out' && document.forms['customForm'].elements['custom.transactionId'].value==''){
       alert("Customs In Id is required field");
       document.forms['customForm'].elements['custom.transactionId'].focus();
       return false;
       }
 }
   function closepopup(){
	   window.open('editCustomAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();
		   window.close();
   }  
   function disable_transId(temp)
   {
	   var test13 =""
	  if(temp=='reset'){
		  test13='${custom.movement}' ; 
		  }else{
	   test13= document.forms['customForm'].elements['custom.movement'].value; 
		  }  
      var el = document.getElementById('hidStatusReg26');	
      var el1 = document.getElementById('hidStatusReg27');
    	 if(test13=='Out'){
    	   el.style.display = 'block';
    	   el1.style.display = 'block';
    	 }else{
   		el.style.display = 'none';
   		el1.style.display = 'none';
   		}		  	   
   }
   
 
	 function findCustomerOtherSO(position) {
		 var sid=document.forms['customForm'].elements['customerFile.id'].value;
		 var soIdNum=document.forms['customForm'].elements['serviceOrder.id'].value;
		 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
		  ajax_showTooltip(url,position);	
		  }   
		var http5 = getHTTPObject();
function checkCustomsId (target){
	var customtransId = document.forms['customForm'].elements['customtransId'].value;
	 if(target.value != customtransId){
	   var cid = document.forms['customForm'].elements['custom.transactionId'].value;
	   var cMove = document.forms['customForm'].elements['custom.movement'].value; 
	   if(cMove=='Out' && cid!='') { 
		   var url="findCustomsOut.html?ajax=1&decorator=simple&popup=true&cid="+encodeURI(cid)+"&cMove="+encodeURI(cMove);;
		   http51.open("GET", url, true); 
		   http51.onreadystatechange = handleHttp90; 
		   http51.send(null); 
	   }
	 }
}	
function handleHttp90(){
    if (http51.readyState == 4){
      var results = http51.responseText;
      results = results.trim();	
      if(results > '0'){    	  
        	alert('This is already linked, Please choose another');
      	document.forms['customForm'].elements['custom.transactionId'].value='';
      }else{
    	  document.forms['customForm'].elements['custom.transactionId'].selected=true;
      }
    }
} 
var http51 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function validateWeight(targetElement){
	if (document.forms['customForm'].elements['custom.weight'].value == '.' ){ 
		alert('Please Enter valid number');
		document.forms['customForm'].elements['custom.weight'].value='';
	}
}
function validateVolume(targetElement){
	if(document.forms['customForm'].elements['custom.volume'].value == '.' ){
		alert('Please Enter valid number');
		document.forms['customForm'].elements['custom.volume'].value='';
	}	
}
</script>   
</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>

<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>

<style><%@ include file="/common/calenderStyle.css"%></style>   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<%-- Modification closed here --%>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<s:form id="customForm" action="saveCustomAjax.html?decorator=popup&popup=true" method="post" validate="true">   
<s:hidden name="custom.id"/>
<s:hidden name="custom.networkId"/> 
<s:hidden name="custom.corpID" /> 
<s:hidden name="sid" value="${serviceOrder.id}" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="customerFile.id" />
<s:hidden name="shipSize" />
    <s:hidden name="minShip" />
    <s:hidden name="countShip" />
    <s:hidden name="minChild" />
    <s:hidden name="maxChild" />
    <s:hidden name="countChild" />
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="customtransId" value="${custom.transactionId}"/>
<div id="Layer4" style="width:100%;">
 <div id="newmnav" style="!float:left;"> 
 <ul> 
  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Custom</span></a></li>   
  </ul>
</div>


<div class="spn">&nbsp;</div>

</div>
<div id="Layer1">
<div style="padding-bottom:0px;"></div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
   
<table class="" cellspacing="1" cellpadding="0" border="0">
<tbody>
<tr>
<td>
<table class="" cellspacing="0" cellpadding="3" border="0">
<tbody>

<tr>
<td align="right" class="listwhitetext">Ticket#</td>
<td width="150px"><s:textfield id="ticket" cssClass="input-text" name="custom.ticket" cssStyle="width:65px" readonly="true"/>
<td align="right" class="listwhitetext">Status<font color="red" size="2">*</font></td>
<td align="left"><s:select cssClass="list-menu" name="custom.status" list="%{customStatus}" headerKey=""  headerValue="" cssStyle="width:90px" /></td>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="custom.movement"/><font color="red" size="2">*</font></td>
<td align="left"><s:select cssClass="list-menu" name="custom.movement" list="%{customsMovement}"  onchange="disable_transId('field');setCountry();" headerKey=""  headerValue="" cssStyle="width:75px" /></td>
<td align="right" class="listwhitetext" width="78px"><div id="hidStatusReg26">Customs In Id#<font color="red" size="2">*</font></div></td>
<td align="left" width="90px"><div id="hidStatusReg27">
<s:select cssClass="list-menu" name="custom.transactionId" list="%{customsInList}" onchange="checkCustomsId(this);" cssStyle="width:75px" />
</div>
</td>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="custom.documentType"/></td>
<td align="left"><s:select cssClass="list-menu" name="custom.documentType" list="%{customsDocType}" headerKey=""  headerValue="" cssStyle="width:90px" /></td>
<td align="right" class="listwhitetext"><fmt:message key="custom.entryDate"/></td>
<c:if test="${not empty custom.entryDate}">
<s:text id="customEntryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="custom.entryDate"/></s:text>
<td><s:textfield id="entryDate" cssClass="input-text" name="custom.entryDate" value="%{customEntryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="changeStatus();"/>
<img id="entryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty custom.entryDate}">
<td><s:textfield id="entryDate" cssClass="input-text" name="custom.entryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="changeStatus();"/>
<img id="entryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="custom.volume"/></td>
<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="custom.volume" onchange="onlyFloat(this);validateVolume(this);" size="7" maxlength="18" required="true" />
<c:out value="${miscellaneous.unit2}" /></td>
<td align="right" class="listwhitetext"><fmt:message key="custom.documentRef"/></td>
<td align="left"><s:textfield cssClass="input-text" name="custom.documentRef" size="40" maxlength="80" required="true" /></td>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="custom.weight"/></td>
<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="custom.weight" onchange="onlyFloat(this);validateWeight(this);" size="7" maxlength="18" required="true" />
<c:out value="${miscellaneous.unit1}" /></td>
<td align="right" class="listwhitetext"><fmt:message key="custom.goods"/></td>
<td align="left"><s:textfield cssClass="input-text" name="custom.goods" size="40" maxlength="60" required="true" /></td>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="custom.pieces"/></td>
<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="custom.pieces" onchange="onlyNumeric(this);" size="7" maxlength="9" required="true" /></td>
</tr>

</tbody>
</table>
</td>
</tr>	
</tbody>
</table>
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>

<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='custom.createdOn'/></b></td>
							<fmt:formatDate var="customCreatedOnFormattedValue" value="${custom.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="custom.createdOn" value="${customCreatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${custom.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='custom.createdBy' /></b></td>
							<c:if test="${not empty custom.id}">
								<s:hidden name="custom.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{custom.createdBy}"/></td>
							</c:if>
							<c:if test="${empty custom.id}">
								<s:hidden name="custom.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='custom.updatedOn'/></b></td>
							<fmt:formatDate var="customupdatedOnFormattedValue" value="${custom.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="custom.updatedOn" value="${customupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${custom.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='custom.updatedBy' /></b></td>
							<c:if test="${not empty custom.id}">
								<s:hidden name="custom.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{custom.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty custom.id}">
								<s:hidden name="custom.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
<s:hidden name="serviceOrderId" value="%{serviceOrder.id}" />
<s:hidden name="serviceOrder.id" />
<sec-auth:authComponent componentId="module.tab.custom.customTab">
<s:submit cssClass="cssbutton" method="save" key="button.save" onclick="return movet();" cssStyle="width:70px; height:25px"/>  
 
<s:reset cssClass="cssbutton" key="Reset" onclick="disable_transId('reset');" cssStyle="width:70px; height:25px "/>
</sec-auth:authComponent>
 <div id="mydiv" style="position: absolute; margin-top:-48px;" ></div>
</s:form>
<script type="text/javascript">
	<c:if test="${hitFlag=='1'}">
	window.close();
	  try {
			window.opener.getCustomDetails();
		}catch(e){
			window.opener.document.forms[0].submit();
		}
	</c:if>
</script>
<script type="text/javascript">
	var fieldName = document.forms['customForm'].elements['field'].value;
	var fieldName1 = document.forms['customForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['customForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['customForm'].elements[fieldName1].className = 'rules-textUpper';
	}
</script>  
<script type="text/javascript">   
  customsInMovement();
  disable_transId('field');
</script>  
<script type="text/javascript">
	setOnSelectBasedMethods(["changeStatus()"]);
	setCalendarFunctionality();
</script>