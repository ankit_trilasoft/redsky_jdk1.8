<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Check List</title> 
<meta name="heading" content="Check List"/>


<style type="text/css">
#overlay11 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style> 
</head>
 <s:form id="CheckListForm" name="CheckListForm" action="saveCheckList" method="post" validate="true">
 <s:hidden name="checkList.id"/>
 <s:hidden name="btn"/>
  <s:hidden name="checkList.status"/>
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <div id="Layer1" style="width:100%">
		<div id="newmnav">
		<ul>		  	
		  	<li><a href="checkLists.html"><span>Check List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>CheckList Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a href="toDoRuleLists.html"><span>To Do List</span></a></li>
		  	<li><a href="docCheckLists.html" style="cursor: pointer;"><span>Doc Check List</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:2px; "><span></span></div>
   <div class="center-content">
		
<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="100%">
		  <tbody>  			  	
		  	<tr>
		  		<td align="left" colspan="10">
		  		<table class="detailTabLabel" border="0" style="margin:0px;padding:0px;">
		  		<tr>
			  		<td align="right" width="63"></td>
			  <c:if test="${not empty checkList.id}">
			  		<td align="right" style="font-size: 1.10em"><b>Id#</b></td>
			  		<td align="left" style="font-size: 1.10em" width="30"><b>${checkList.id}</b></td>
			  		
			  		<td align="right"style="font-size: 1.10em" ><b>Status:</b></td>
			  	<c:if test="${checkList.status==true }">
			  		<td align="left" style="font-size: 1.10em"><b>Correct</b></td>
			  		
			  		<c:set var="ischecked" value="false"/>
					<c:if test="${checkList.enable==true}">
					<c:set var="ischecked" value="true"/>
					</c:if>
	   				<td align="right" width="204px"><s:checkbox key="checkList.enable" value="${ischecked}" fieldValue="true"/></td>
	        		<td align="left"  class="listwhitetext"><fmt:message key="toDoRule.check"/></td>
	        	</c:if>
	        		
			  	<c:if test="${checkList.status==false }">
			  		<td align="left" style="font-size: 1.10em; color: red">InCorrect</td>
			  	</c:if>
			 </c:if>
			  	
		  		</tr>
		  		</table>
		  		</td>
		  	</tr>	  	
		  	
		  	<tr>
		  		<td align="left" colspan="10">
		  		<table class="detailTabLabel" border="0" style="margin:0px;padding:0px;">
		  		<tr>
			  		<td align="right" width="63"></td>
			  		<td align="right">Table</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.tableRequired" headerKey="" headerValue="" list="%{pentity}" cssStyle="width:150px"/></td>
			  		<td align="right" width="147">Doc Type</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.documentType" headerKey="" headerValue="" list="%{docsList}" cssStyle="width:150px"/></td>
		  		</tr>
		  		</table>
		  		</td>
		  	</tr>
		  	
		  	<tr>
		  		
		  		<td align="left" colspan="10">
		  		<table class="detailTabLabel" border="0" style="margin:0px;padding:0px;">
		  		<tr>
			  		<td align="right" width="45"></td>
			  		<td align="right">Job Type</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.jobType" headerKey="" headerValue="" list="%{job}" cssStyle="width:150px"/></td>
			  		<td align="right" width="147">Routing</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.routing"  list="%{routing}" cssStyle="width:150px"/></td>
			  		<td align="right" width="80">Service</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.service"  list="%{service}" cssStyle="width:150px"/></td>
		  		</tr>
		  		<tr>
			  		<td align="right" width="30"></td>
			  		<td align="right">Mode</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.mode"  list="%{mode}" cssStyle="width:150px"/></td>
			  		<td align="right">IM Country</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.importCountry" headerKey="" headerValue="" list="%{ocountry}" cssStyle="width:150px"/></td>
			  		<td align="right">EX Country</td>
			  		<td align="left"><s:select cssClass="list-menu" name="checkList.exportCountry" headerKey="" headerValue="" list="%{dcountry}" cssStyle="width:150px"/></td>
		  		</tr>
		  		</table>
		  		</td>
		  	</tr>
		  	
		  	<tr>
		  		<td align="left" colspan="10">
		  		<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1" style="margin:0px;padding:0px;">
		  		<tr>
			  		<td align="left" width="29">
			  		<td align="right">Partner Code</td>
			  		<td align="left"><s:textfield name="checkList.partnerCode"  maxlength="200" size="40" cssClass="input-text" readonly="false"/></td>
			  		<td align="right">Exclude Bill To</td>
			  		<td align="left"><s:textfield name="checkList.billToExcludes"  maxlength="200" size="39" cssClass="input-text" readonly="false"/></td>
		  		</tr>
		  		</table>
		  		</td>
		  	</tr>
		  		<tr><td height="5" align="left"></td></tr>
		  	<tr>
		  		<td align="left" colspan="10">
		  		<table class="detailTabLabel" border="0" style="margin:0px;padding:0px;">
		  		<tr>
			  		<td align="right" valign="top">Control Date</td>
					<td align="left"><s:select cssClass="list-menu"  id="" list="%{controlDateList}"  cssStyle="width:300px" headerKey="" headerValue="" name="checkList.controlDate"/></td>
					<td align="right" valign="top" width="152">is less than equal to +/- days</td>
					<td align="left"><s:textfield name="checkList.duration"  maxlength="10" size="7" cssClass="input-text" readonly="false"/></td>
		 		</tr>
		 		<tr>
			  		<td align="right" valign="top" >Message to display</td>
					<td align="left" colspan="5"><s:textfield name="checkList.messageDisplayed"  maxlength="100" size="100" cssClass="input-text" readonly="false"/></td>
		 		</tr>
		 		<tr>
			  		<td align="right" valign="top" >Owner</td>
					<td align="left" colspan="5"><s:select cssClass="list-menu"  id="" list="%{ownerList}"  cssStyle="width:300px" headerKey="" headerValue="" name="checkList.owner"/></td>
		 		</tr>
		 		</table>
		 		
		 		<table class="detailTabLabel" border="0" style="margin:0px;padding:0px;width:100%;">
		  		<tr>
		  		<td align="left" class="vertlinedata" colspan="15"></td>
		 		</tr>
		 		</table>
		 		
		 		<table>
		 		<tr><td height="2" align="left"></td></tr>
		 		<tr>
		 			<td align="left"  class="listwhitetext" colspan="1">ServiceOrder Stop Date</td>
			  		<c:if test="${not empty checkList.serviceOrderDate}">
						<s:text id="serviceOrderDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="checkList.serviceOrderDate"/></s:text>
						<td width="20px"><s:textfield cssClass="input-text" id="serviceOrderDate" name="checkList.serviceOrderDate" value="%{serviceOrderDateFormattedValue}" required="true" size="8" cssStyle="" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
						<td><img id="serviceOrderDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty checkList.serviceOrderDate}">
						<td colspan="" width="20px"><s:textfield cssClass="input-text" id="serviceOrderDate" name="checkList.serviceOrderDate" required="true" cssStyle="" size="8" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/></td>
						<td><img id="serviceOrderDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<td width="150px"></td>
					<td align="left"  class="listwhitetext" colspan="1">CustomerFile Stop Date</td>
			  		<c:if test="${not empty checkList.customerFileDate}">	
						<s:text id="customerFileDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="checkList.customerFileDate"/></s:text>
						<td width="20px"><s:textfield cssClass="input-text" id="customerFileDate" name="checkList.customerFileDate" value="%{customerFileDateFormattedValue}" required="true" size="8" cssStyle="" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
						<td><img id="customerFileDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty checkList.customerFileDate}">
						<td colspan="" width="20px"><s:textfield cssClass="input-text" id="customerFileDate" name="checkList.customerFileDate" required="true" cssStyle="" size="8" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/></td>
						<td><img id="customerFileDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
		 		</tr>
		  		</table>
		  		</td>
		  		
		  		<td colspan="7" style="margin:0px;">
			<DIV ID="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Executing Rule</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
			</td>
		 	</td>
		  	</tr>			  	
		  		</table>
		  		</td>		  		
		  	</tr>
		  	
	 </tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</td></tr></tbody></table>
<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="checkListCreatedOnFormattedValue" value="${checkList.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="checkList.createdOn" value="${checkListCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${checkList.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty checkList.id}">
								<s:hidden name="checkList.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{checkList.createdBy}"/></td>
							</c:if>
							<c:if test="${empty checkList.id}">
								<s:hidden name="checkList.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="checkListupdatedOnFormattedValue" value="${checkList.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="checkList.updatedOn" value="${checkListupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${checkList.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty checkList.id}">
								<s:hidden name="checkList.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{checkList.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty checkList.id}">
								<s:hidden name="checkList.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" value="Save" onclick="return checkFields(this);"/> 
        		<s:submit cssClass="cssbutton1" type="button" method="save" value="Test" name="Test" onclick="return checkFields(this)"/>  
        		<c:if test="${checkList.status==true}">
        		<s:submit cssClass="cssbutton1" type="button" method="save" value="Execute" name="Execute" onclick="return checkFields(this)"/>  
        		</c:if>
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		 		
        		<td align="right">
        		<c:if test="${not empty checkList.id}">
		       <input type="button" class="cssbutton1" value="Add" onclick="location.href='<c:url value="/checkListForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table>
</s:form>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script type="text/javascript">
function btntype(targerelement){
		//alert(targerelement.value);
	document.forms['CheckListForm'].elements['btn'].value=targerelement.value;
}

function checkFields(target){
	if(document.forms['CheckListForm'].elements['checkList.tableRequired'].value==''){
		alert('Please select table');
		return false;
	}
	if(document.forms['CheckListForm'].elements['checkList.documentType'].value==''){
		alert('Please select Doc Type');
		return false;
	}
	if(document.forms['CheckListForm'].elements['checkList.controlDate'].value==''){
		alert('Please select Control Date');
		return false;
	}
	if(document.forms['CheckListForm'].elements['checkList.duration'].value.trim()==''){
		alert('Please enter number of days');
		return false;
	}
	if(document.forms['CheckListForm'].elements['checkList.messageDisplayed'].value.trim()==''){
		alert('Please enter message to display');
		return false;
	}
	if(document.forms['CheckListForm'].elements['checkList.owner'].value==''){
		alert('Please select owner');
		return false;
	}
	btntype(target);
	showOrHide(1);
}
</script>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>


<script type="text/javascript">
showOrHide(0);
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
</script>	

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>