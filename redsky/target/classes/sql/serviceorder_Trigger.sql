	DELIMITER $$
	DROP trigger IF EXISTS `redsky`.`trigger_add_history_serviceorder` $$
	
CREATE
DEFINER=`root`@`localhost`
TRIGGER `redsky`.`trigger_add_history_serviceorder`
BEFORE UPDATE ON `redsky`.`serviceorder`
FOR EACH ROW
BEGIN
	DECLARE fieldNameList LONGTEXT;
	DECLARE oldValueList LONGTEXT;
	DECLARE newValueList LONGTEXT;
	DECLARE updatedby LONGTEXT;
	DECLARE updatedon DATETIME;
  	declare l_distinctStatus bigint;
  	
	SET fieldNameList = " ";
	SET oldValueList = " ";
	SET newValueList = " ";
	SET updatedby = " ";
	
	
	IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.sequenceNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
	END IF;
	IF (OLD.ship <> NEW.ship ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ship~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.ship,'~') into newValueList;
	END IF;
	IF (OLD.shipNumber <> NEW.shipNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.shipNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
	    set NEW.isSOExtract=false;
        END IF;
	IF (OLD.registrationNumber <> NEW.registrationNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.registrationNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.registrationNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.registrationNumber,'~') into newValueList;
	END IF;
	IF ((OLD.statusDate <> NEW.statusDate) or (OLD.statusDate is null and NEW.statusDate is not null)
	  or (OLD.statusDate is not null and NEW.statusDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.statusDate~') into fieldNameList;
	    IF(OLD.statusDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.statusDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.statusDate is not null) then
        	select CONCAT(oldValueList,OLD.statusDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.statusDate is not null) then
        	select CONCAT(newValueList,NEW.statusDate,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.actualSurveyDate <> NEW.actualSurveyDate) or (OLD.actualSurveyDate is null and NEW.actualSurveyDate is not null)
	  or (OLD.actualSurveyDate is not null and NEW.actualSurveyDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.actualSurveyDate~') into fieldNameList;
	    IF(OLD.actualSurveyDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.actualSurveyDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.actualSurveyDate is not null) then
        	select CONCAT(oldValueList,OLD.actualSurveyDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.actualSurveyDate is not null) then
        	select CONCAT(newValueList,NEW.actualSurveyDate,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.priceSubmissionToAccDate <> NEW.priceSubmissionToAccDate) or (OLD.priceSubmissionToAccDate is null and NEW.priceSubmissionToAccDate is not null)
	  or (OLD.priceSubmissionToAccDate is not null and NEW.priceSubmissionToAccDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.priceSubmissionToAccDate~') into fieldNameList;
	    IF(OLD.priceSubmissionToAccDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.priceSubmissionToAccDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.priceSubmissionToAccDate is not null) then
        	select CONCAT(oldValueList,OLD.priceSubmissionToAccDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.priceSubmissionToAccDate is not null) then
        	select CONCAT(newValueList,NEW.priceSubmissionToAccDate,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.reptHourReceived <> NEW.reptHourReceived) or (OLD.reptHourReceived is null and NEW.reptHourReceived is not null)
	  or (OLD.reptHourReceived is not null and NEW.reptHourReceived is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.reptHourReceived~') into fieldNameList;
	    IF(OLD.reptHourReceived is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.reptHourReceived is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.reptHourReceived is not null) then
        	select CONCAT(oldValueList,OLD.reptHourReceived,'~') into oldValueList;
     	END IF;
      	IF(NEW.reptHourReceived is not null) then
        	select CONCAT(newValueList,NEW.reptHourReceived,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.quoteStopEmail <> NEW.quoteStopEmail) or (OLD.quoteStopEmail is null and NEW.quoteStopEmail is not null)
	  or (OLD.quoteStopEmail is not null and NEW.quoteStopEmail is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.quoteStopEmail~') into fieldNameList;
	    IF(OLD.quoteStopEmail is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.quoteStopEmail is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.quoteStopEmail is not null) then
        	select CONCAT(oldValueList,OLD.quoteStopEmail,'~') into oldValueList;
     	END IF;
      	IF(NEW.quoteStopEmail is not null) then
        	select CONCAT(newValueList,NEW.quoteStopEmail,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.priceSubmissionToTranfDate <> NEW.priceSubmissionToTranfDate) or (OLD.priceSubmissionToTranfDate is null and NEW.priceSubmissionToTranfDate is not null)
	  or (OLD.priceSubmissionToTranfDate is not null and NEW.priceSubmissionToTranfDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.priceSubmissionToTranfDate~') into fieldNameList;
	    IF(OLD.priceSubmissionToTranfDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.priceSubmissionToTranfDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.priceSubmissionToTranfDate is not null) then
        	select CONCAT(oldValueList,OLD.priceSubmissionToTranfDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.priceSubmissionToTranfDate is not null) then
        	select CONCAT(newValueList,NEW.priceSubmissionToTranfDate,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.priceSubmissionToBookerDate <> NEW.priceSubmissionToBookerDate) or (OLD.priceSubmissionToBookerDate is null and NEW.priceSubmissionToBookerDate is not null)
	  or (OLD.priceSubmissionToBookerDate is not null and NEW.priceSubmissionToBookerDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.priceSubmissionToBookerDate~') into fieldNameList;
	    IF(OLD.priceSubmissionToBookerDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.priceSubmissionToBookerDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.priceSubmissionToBookerDate is not null) then
        	select CONCAT(oldValueList,OLD.priceSubmissionToBookerDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.priceSubmissionToBookerDate is not null) then
        	select CONCAT(newValueList,NEW.priceSubmissionToBookerDate,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.quoteAcceptenceDate <> NEW.quoteAcceptenceDate) or (OLD.quoteAcceptenceDate is null and NEW.quoteAcceptenceDate is not null)
	  or (OLD.quoteAcceptenceDate is not null and NEW.quoteAcceptenceDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.quoteAcceptenceDate~') into fieldNameList;
	    IF(OLD.quoteAcceptenceDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.quoteAcceptenceDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.quoteAcceptenceDate is not null) then
        	select CONCAT(oldValueList,OLD.quoteAcceptenceDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.quoteAcceptenceDate is not null) then
        	select CONCAT(newValueList,NEW.quoteAcceptenceDate,'~') into newValueList;
      	END IF;
	END IF;
	
	
	IF ((OLD.importPaperOn <> NEW.importPaperOn) or (OLD.importPaperOn is null and NEW.importPaperOn is not null)
	  or (OLD.importPaperOn is not null and NEW.importPaperOn is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.importPaperOn~') into fieldNameList;
	    IF(OLD.importPaperOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.importPaperOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.importPaperOn is not null) then
        	select CONCAT(oldValueList,OLD.importPaperOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.importPaperOn is not null) then
        	select CONCAT(newValueList,NEW.importPaperOn,'~') into newValueList;
      	END IF;
	END IF;
	
	IF (OLD.status != NEW.status)  THEN
    IF (OLD.status ='CNCL' AND NEW.status ='REOP') THEN 
       set NEW.jimExtract=false;
    END IF; 
    IF (NEW.status ='CNCL' ) THEN 
    set NEW.jimExtract=false;
    END IF;
    END IF;
    
	IF (OLD.status <> NEW.status ) THEN
	    select CONCAT(fieldNameList,'serviceorder.status~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.status,'~') into newValueList;
	END IF;
	
	IF (OLD.job <> NEW.job ) THEN
	    select CONCAT(fieldNameList,'serviceorder.job~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.job,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.job,'~') into newValueList;
	END IF;
	IF (OLD.routing <> NEW.routing ) THEN
	    select CONCAT(fieldNameList,'serviceorder.routing~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.routing,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.routing,'~') into newValueList;
	END IF;
	IF (OLD.serviceType <> NEW.serviceType ) THEN
	    select CONCAT(fieldNameList,'serviceorder.serviceType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.serviceType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.serviceType,'~') into newValueList;
	END IF;
	IF (OLD.mode <> NEW.mode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.mode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.mode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.mode,'~') into newValueList;
	END IF;
	IF (OLD.packingMode <> NEW.packingMode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.packingMode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.packingMode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.packingMode,'~') into newValueList;
	END IF;
	IF (OLD.payType <> NEW.payType ) THEN
	    select CONCAT(fieldNameList,'serviceorder.payType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.payType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.payType,'~') into newValueList;
	END IF;
	IF (OLD.coordinator <> NEW.coordinator ) THEN
	    select CONCAT(fieldNameList,'serviceorder.coordinator~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.coordinator,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.coordinator,'~') into newValueList;
	       IF (OLD.corpid='VOER') THEN
            IF (OLD.isUpdater=true) THEN
            set NEW.isUpdater=false;
               END IF;
            END IF;
	END IF;
	IF (OLD.salesMan <> NEW.salesMan ) THEN
	    select CONCAT(fieldNameList,'serviceorder.salesMan~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.salesMan,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.salesMan,'~') into newValueList;
	END IF;
	IF (OLD.prefix <> NEW.prefix ) THEN
	    select CONCAT(fieldNameList,'serviceorder.prefix~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.prefix,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.prefix,'~') into newValueList;
	END IF;
	IF (OLD.firstName <> NEW.firstName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.firstName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
            set NEW.isSOExtract=false;
	END IF;
	IF (OLD.lastName <> NEW.lastName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.lastName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
            set NEW.isSOExtract=false;
	END IF;
	IF (OLD.mi <> NEW.mi ) THEN
	    select CONCAT(fieldNameList,'serviceorder.mi~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.mi,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.mi,'~') into newValueList;
	END IF;
	IF (OLD.originAddressLine1 <> NEW.originAddressLine1 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAddressLine1~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAddressLine1,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAddressLine1,'~') into newValueList;
	END IF;
	IF (OLD.suffix <> NEW.suffix ) THEN
	    select CONCAT(fieldNameList,'serviceorder.suffix~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.suffix,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.suffix,'~') into newValueList;
	END IF;
	IF (OLD.originAddressLine2 <> NEW.originAddressLine2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAddressLine2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAddressLine2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAddressLine2,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originAddressLine3 <> NEW.originAddressLine3 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAddressLine3~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAddressLine3,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAddressLine3,'~') into newValueList;
	END IF;
	IF (OLD.originCity <> NEW.originCity ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originCity~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originCity,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originCity,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originState <> NEW.originState ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originState~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originState,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originState,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originCounty <> NEW.originCountryCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originCountryCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originCountryCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originCountryCode,'~') into newValueList;
	END IF;
	
	IF (OLD.originCountry <> NEW.originCountry ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originCountry~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originCountry,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originCountry,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originZip <> NEW.originZip ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originZip~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originZip,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originZip,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originDayPhone <> NEW.originDayPhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originDayPhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originDayPhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originDayPhone,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originHomePhone <> NEW.originHomePhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originHomePhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originHomePhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originHomePhone,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.originFax <> NEW.originFax ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originFax~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originFax,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originFax,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationAddressLine1 <> NEW.destinationAddressLine1 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationAddressLine1~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationAddressLine1,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationAddressLine1,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationAddressLine2 <> NEW.destinationAddressLine2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationAddressLine2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationAddressLine2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationAddressLine2,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationAddressLine3 <> NEW.destinationAddressLine3 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationAddressLine3~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationAddressLine3,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationAddressLine3,'~') into newValueList;
	END IF;
	IF (OLD.destinationCity <> NEW.destinationCity ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationCity~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationCity,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationCity,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationCityCode <> NEW.destinationCityCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationCityCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationCityCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationCityCode,'~') into newValueList;
	END IF;
	IF (OLD.originCityCode <> NEW.originCityCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originCityCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originCityCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originCityCode,'~') into newValueList;
	END IF;
	IF (OLD.destinationState <> NEW.destinationState ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationState~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationState,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationState,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationCountryCode <> NEW.destinationCountryCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationCountryCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationCountryCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationCountryCode,'~') into newValueList;
	END IF;
	
	IF (OLD.destinationCountry <> NEW.destinationCountry ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationCountry~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationCountry,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationCountry,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationZip <> NEW.destinationZip ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationZip~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationZip,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationZip,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationDayPhone <> NEW.destinationDayPhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationDayPhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationDayPhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationDayPhone,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationHomePhone <> NEW.destinationHomePhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationHomePhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationHomePhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationHomePhone,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.destinationFax <> NEW.destinationFax ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationFax~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationFax,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationFax,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.contactName <> NEW.contactName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.contactName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.contactName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.contactName,'~') into newValueList;
	END IF;
	IF (OLD.contactPhone <> NEW.contactPhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.contactPhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.contactPhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.contactPhone,'~') into newValueList;
	    set NEW.isSOExtract=false;
	END IF;
	IF (OLD.contactFax <> NEW.contactFax ) THEN
	    select CONCAT(fieldNameList,'serviceorder.contactFax~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.contactFax,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.contactFax,'~') into newValueList;
	END IF;
	IF (OLD.clientCode <> NEW.clientCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.clientCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.clientCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.clientCode,'~') into newValueList;
	END IF;
	IF (OLD.clientName <> NEW.clientName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.clientName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.clientName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.clientName,'~') into newValueList;
	END IF;
	IF (OLD.clientReference <> NEW.clientReference ) THEN
	    select CONCAT(fieldNameList,'serviceorder.clientReference~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.clientReference,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.clientReference,'~') into newValueList;
	END IF;
	IF (OLD.commodity <> NEW.commodity ) THEN
	    select CONCAT(fieldNameList,'serviceorder.commodity~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.commodity,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.commodity,'~') into newValueList;
	END IF;
	
	IF (OLD.contract <> NEW.contract ) THEN
	    select CONCAT(fieldNameList,'serviceorder.contract~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.contract,'~') into newValueList;
	END IF;
	IF (OLD.socialSecurityNumber <> NEW.socialSecurityNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.socialSecurityNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.socialSecurityNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.socialSecurityNumber,'~') into newValueList;
	END IF;
	
	IF (OLD.consignee <> NEW.consignee ) THEN
	    select CONCAT(fieldNameList,'serviceorder.consignee~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.consignee,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.consignee,'~') into newValueList;
	    set NEW.isSOExtract=false;
	  END IF;
	    IF (OLD.surveyCoordinator <> NEW.surveyCoordinator ) THEN
	    select CONCAT(fieldNameList,'serviceorder.surveyCoordinator~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.surveyCoordinator,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.surveyCoordinator,'~') into newValueList;
	    set NEW.isSOExtract=false;
     END IF;
	IF (OLD.destinationLoadSite <> NEW.destinationLoadSite ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationLoadSite~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationLoadSite,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationLoadSite,'~') into newValueList;
	END IF;
	IF (OLD.estimator <> NEW.estimator ) THEN
	    select CONCAT(fieldNameList,'serviceorder.estimator~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.estimator,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.estimator,'~') into newValueList;
	END IF;
	IF ((OLD.lastExtra <> NEW.lastExtra) or (OLD.lastExtra is null and NEW.lastExtra is not null)
	  or (OLD.lastExtra is not null and NEW.lastExtra is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.lastExtra~') into fieldNameList;
		IF(OLD.lastExtra is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.lastExtra is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.lastExtra is not null) then
			select CONCAT(oldValueList,OLD.lastExtra,'~') into oldValueList;
		END IF;
		IF(NEW.lastExtra is not null) then
			select CONCAT(newValueList,NEW.lastExtra,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.estimateOverallMarkupPercentage <> NEW.estimateOverallMarkupPercentage) or (OLD.estimateOverallMarkupPercentage is null and NEW.estimateOverallMarkupPercentage is not null)
	  or (OLD.estimateOverallMarkupPercentage is not null and NEW.estimateOverallMarkupPercentage is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimateOverallMarkupPercentage~') into fieldNameList;
		IF(OLD.estimateOverallMarkupPercentage is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimateOverallMarkupPercentage is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimateOverallMarkupPercentage is not null) then
			select CONCAT(oldValueList,OLD.lastExtra,'~') into oldValueList;
		END IF;
		IF(NEW.estimateOverallMarkupPercentage is not null) then
			select CONCAT(newValueList,NEW.estimateOverallMarkupPercentage,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.lastItem <> NEW.lastItem) or (OLD.lastItem is null and NEW.lastItem is not null) 
	  or (OLD.lastItem is not null and NEW.lastItem is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.lastItem~') into fieldNameList;
		IF(OLD.lastItem is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.lastItem is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.lastItem is not null) then
			select CONCAT(oldValueList,OLD.lastItem,'~') into oldValueList;
		END IF;
		IF(NEW.lastItem is not null) then
			select CONCAT(newValueList,NEW.lastItem,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.nextCheckOn <> NEW.nextCheckOn) or (OLD.nextCheckOn is null and NEW.nextCheckOn is not null)
	  or (OLD.nextCheckOn is not null and NEW.nextCheckOn is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.nextCheckOn~') into fieldNameList;
	    IF(OLD.nextCheckOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.nextCheckOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.nextCheckOn is not null) then
        	select CONCAT(oldValueList,OLD.nextCheckOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.nextCheckOn is not null) then
        	select CONCAT(newValueList,NEW.nextCheckOn,'~') into newValueList;
      	END IF;
	END IF;
	
	IF ((OLD.updaterUpdatedOn <> NEW.updaterUpdatedOn) or (OLD.updaterUpdatedOn is null and NEW.updaterUpdatedOn is not null)
	  or (OLD.updaterUpdatedOn is not null and NEW.updaterUpdatedOn is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.updaterUpdatedOn~') into fieldNameList;
	    IF(OLD.updaterUpdatedOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.updaterUpdatedOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.updaterUpdatedOn is not null) then
        	select CONCAT(oldValueList,OLD.updaterUpdatedOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.updaterUpdatedOn is not null) then
        	select CONCAT(newValueList,NEW.updaterUpdatedOn,'~') into newValueList;
      	END IF;
	END IF;
	
	IF (OLD.originContactName <> NEW.originContactName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactName,'~') into newValueList;
	END IF;
	IF (OLD.originContactLastName <> NEW.originContactLastName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactLastName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactLastName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactLastName,'~') into newValueList;
	END IF;
	IF (OLD.originContactPhone <> NEW.originContactPhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactPhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactPhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactPhone,'~') into newValueList;
	END IF;
	IF (OLD.originContactWork <> NEW.originContactWork ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactWork~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactWork,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactWork,'~') into newValueList;
	END IF;
	IF (OLD.originLoadSite <> NEW.originLoadSite ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originLoadSite~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originLoadSite,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originLoadSite,'~') into newValueList;
	END IF;
	IF (OLD.orderBy <> NEW.orderBy ) THEN
	    select CONCAT(fieldNameList,'serviceorder.orderBy~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.orderBy,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.orderBy,'~') into newValueList;
	END IF;
	IF (OLD.orderPhone <> NEW.orderPhone ) THEN
	    select CONCAT(fieldNameList,'serviceorder.orderPhone~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.orderPhone,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.orderPhone,'~') into newValueList;
	END IF;
	
	IF (OLD.suffixNext <> NEW.suffixNext ) THEN
	    select CONCAT(fieldNameList,'serviceorder.suffixNext~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.suffixNext,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.suffixNext,'~') into newValueList;
	END IF;
	IF (OLD.email <> NEW.email ) THEN
	    select CONCAT(fieldNameList,'serviceorder.email~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.email,'~') into newValueList;
	END IF;
	IF (OLD.packingDays <> NEW.packingDays ) THEN
	    select CONCAT(fieldNameList,'serviceorder.packingDays~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.packingDays,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.packingDays,'~') into newValueList;
	END IF;
	IF (OLD.originCompany <> NEW.originCompany ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originCompany~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originCompany,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originCompany,'~') into newValueList;
	END IF;
	IF (OLD.destinationCompany <> NEW.destinationCompany ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationCompany~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationCompany,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationCompany,'~') into newValueList;
	END IF;
	IF (OLD.originMilitary <> NEW.originMilitary ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originMilitary~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originMilitary,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originMilitary,'~') into newValueList;
	END IF;
	IF (OLD.destinationMilitary <> NEW.destinationMilitary ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationMilitary~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationMilitary,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationMilitary,'~') into newValueList;
	END IF;
	IF (OLD.originCounty <> NEW.originCounty ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originCounty~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originCounty,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originCounty,'~') into newValueList;
	END IF;
	IF (OLD.destinationCounty <> NEW.destinationCounty ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationCounty~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationCounty,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationCounty,'~') into newValueList;
	END IF;

	IF (OLD.lastName2 <> NEW.lastName2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.lastName2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.lastName2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.lastName2,'~') into newValueList;
	END IF;
	IF (OLD.firstName2 <> NEW.firstName2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.firstName2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.firstName2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.firstName2,'~') into newValueList;
	END IF;
	IF (OLD.prefix2 <> NEW.prefix2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.prefix2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.prefix2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.prefix2,'~') into newValueList;
	END IF;
	IF (OLD.initial2 <> NEW.initial2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.initial2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.initial2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.initial2,'~') into newValueList;
	END IF;
	IF (OLD.andor <> NEW.andor ) THEN
	    select CONCAT(fieldNameList,'serviceorder.andor~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.andor,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.andor,'~') into newValueList;
	END IF;
	IF (OLD.billToCode <> NEW.billToCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.billToCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
	END IF;
	IF (OLD.billToName <> NEW.billToName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.billToName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.billToName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.billToName,'~') into newValueList;
	END IF;
	
	IF (OLD.originDayExtn <> NEW.originDayExtn ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originDayExtn~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originDayExtn,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originDayExtn,'~') into newValueList;
	END IF;
	IF (OLD.destinationDayExtn <> NEW.destinationDayExtn ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationDayExtn~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationDayExtn,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationDayExtn,'~') into newValueList;
	END IF;
	IF (OLD.vip <> NEW.vip ) THEN
	    select CONCAT(fieldNameList,'serviceorder.vip~') into fieldNameList;
	    IF(OLD.vip = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.vip = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.vip = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.vip = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.originArea <> NEW.originArea ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originArea~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originArea,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originArea,'~') into newValueList;
	END IF;
	IF (OLD.destinationArea <> NEW.destinationArea ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationArea~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationArea,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationArea,'~') into newValueList;
	END IF;
	IF (OLD.tripNumber <> NEW.tripNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.tripNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.tripNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.tripNumber,'~') into newValueList;
	END IF;
	IF (OLD.email2 <> NEW.email2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.email2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.email2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.email2,'~') into newValueList;
	END IF;
	IF (OLD.originContactExtn <> NEW.originContactExtn ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactExtn~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactExtn,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactExtn,'~') into newValueList;
	END IF;
	IF (OLD.destinationContactExtn <> NEW.destinationContactExtn ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationContactExtn~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationContactExtn,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationContactExtn,'~') into newValueList;
	END IF;
	

	IF (OLD.quoteStatus <> NEW.quoteStatus ) THEN
	    select CONCAT(fieldNameList,'serviceorder.quoteStatus~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.quoteStatus,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.quoteStatus,'~') into newValueList;
	END IF;
	
	IF ((OLD.estimateGrossWeight <> NEW.estimateGrossWeight) or (OLD.estimateGrossWeight is null and NEW.estimateGrossWeight is not null)
	  or (OLD.estimateGrossWeight is not null and NEW.estimateGrossWeight is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimateGrossWeight~') into fieldNameList;
		IF(OLD.estimateGrossWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimateGrossWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimateGrossWeight is not null) then
			select CONCAT(oldValueList,OLD.estimateGrossWeight,'~') into oldValueList;
		END IF;
		IF(NEW.estimateGrossWeight is not null) then
			select CONCAT(newValueList,NEW.estimateGrossWeight,'~') into newValueList;
		END IF;
	END IF;
	IF ((OLD.actualGrossWeight <> NEW.actualGrossWeight) or (OLD.actualGrossWeight is null and NEW.actualGrossWeight is not null) 
	  or (OLD.actualGrossWeight is not null and NEW.actualGrossWeight is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualGrossWeight~') into fieldNameList;
		IF(OLD.actualGrossWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualGrossWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualGrossWeight is not null) then
			select CONCAT(oldValueList,OLD.actualGrossWeight,'~') into oldValueList;
		END IF;
		IF(NEW.actualGrossWeight is not null) then
			select CONCAT(newValueList,NEW.actualGrossWeight,'~') into newValueList;
		END IF; 
	END IF;
	

	IF ((OLD.estimatedNetWeight <> NEW.estimatedNetWeight) or (OLD.estimatedNetWeight is null and NEW.estimatedNetWeight is not null)
	  or (OLD.estimatedNetWeight is not null and NEW.estimatedNetWeight is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimatedNetWeight~') into fieldNameList;
		IF(OLD.estimatedNetWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedNetWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedNetWeight is not null) then
			select CONCAT(oldValueList,OLD.estimatedNetWeight,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedNetWeight is not null) then
			select CONCAT(newValueList,NEW.estimatedNetWeight,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.actualNetWeight <> NEW.actualNetWeight) or (OLD.actualNetWeight is null and NEW.actualNetWeight is not null) 
	  or (OLD.actualNetWeight is not null and NEW.actualNetWeight is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualNetWeight~') into fieldNameList;
		IF(OLD.actualNetWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualNetWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualNetWeight is not null) then
			select CONCAT(oldValueList,OLD.actualNetWeight,'~') into oldValueList;
		END IF;
		IF(NEW.actualNetWeight is not null) then
			select CONCAT(newValueList,NEW.actualNetWeight,'~') into newValueList;
		END IF; 
	END IF;
	
	IF ((OLD.projectedDistributedTotalAmount <> NEW.projectedDistributedTotalAmount) or (OLD.projectedDistributedTotalAmount is null and NEW.projectedDistributedTotalAmount is not null) 
	  or (OLD.projectedDistributedTotalAmount is not null and NEW.projectedDistributedTotalAmount is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.projectedDistributedTotalAmount~') into fieldNameList;
		IF(OLD.projectedDistributedTotalAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.projectedDistributedTotalAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.projectedDistributedTotalAmount is not null) then
			select CONCAT(oldValueList,OLD.projectedDistributedTotalAmount,'~') into oldValueList;
		END IF;
		IF(NEW.projectedDistributedTotalAmount is not null) then
			select CONCAT(newValueList,NEW.projectedDistributedTotalAmount,'~') into newValueList;
		END IF; 
	END IF;
	
	IF ((OLD.projectedActualExpense <> NEW.projectedActualExpense) or (OLD.projectedActualExpense is null and NEW.projectedActualExpense is not null) 
	  or (OLD.projectedActualExpense is not null and NEW.projectedActualExpense is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.projectedActualExpense~') into fieldNameList;
		IF(OLD.projectedActualExpense is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.projectedActualExpense is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.projectedActualExpense is not null) then
			select CONCAT(oldValueList,OLD.projectedActualExpense,'~') into oldValueList;
		END IF;
		IF(NEW.projectedActualExpense is not null) then
			select CONCAT(newValueList,NEW.projectedActualExpense,'~') into newValueList;
		END IF; 
	END IF;
	
	IF ((OLD.projectedActualRevenue <> NEW.projectedActualRevenue) or (OLD.projectedActualRevenue is null and NEW.projectedActualRevenue is not null) 
	  or (OLD.projectedActualRevenue is not null and NEW.projectedActualRevenue is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.projectedActualRevenue~') into fieldNameList;
		IF(OLD.projectedActualRevenue is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.projectedActualRevenue is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.projectedActualRevenue is not null) then
			select CONCAT(oldValueList,OLD.projectedActualRevenue,'~') into oldValueList;
		END IF;
		IF(NEW.projectedActualRevenue is not null) then
			select CONCAT(newValueList,NEW.projectedActualRevenue,'~') into newValueList;
		END IF; 
	END IF;
	
	IF ((OLD.entitleNumberAuto <> NEW.entitleNumberAuto) or (OLD.entitleNumberAuto is null and NEW.entitleNumberAuto is not null) 
	  or (OLD.entitleNumberAuto is not null and NEW.entitleNumberAuto is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.entitleNumberAuto~') into fieldNameList;
		IF(OLD.entitleNumberAuto is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.entitleNumberAuto is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.entitleNumberAuto is not null) then
			select CONCAT(oldValueList,OLD.entitleNumberAuto,'~') into oldValueList;
		END IF;
		IF(NEW.entitleNumberAuto is not null) then
			select CONCAT(newValueList,NEW.entitleNumberAuto,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.entitleBoatYN <> NEW.entitleBoatYN) or (OLD.entitleBoatYN is null and NEW.entitleBoatYN is not null) 
	  or (OLD.entitleBoatYN is not null and NEW.entitleBoatYN is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.entitleBoatYN~') into fieldNameList;
		IF(OLD.entitleBoatYN is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.entitleBoatYN is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.entitleBoatYN is not null) then
			select CONCAT(oldValueList,OLD.entitleBoatYN,'~') into oldValueList;
		END IF;
		IF(NEW.entitleBoatYN is not null) then
			select CONCAT(newValueList,NEW.entitleBoatYN,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.estimateAuto <> NEW.estimateAuto) or (OLD.estimateAuto is null and NEW.estimateAuto is not null) 
	  or (OLD.estimateAuto is not null and NEW.estimateAuto is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimateAuto~') into fieldNameList;
		IF(OLD.estimateAuto is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimateAuto is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimateAuto is not null) then
			select CONCAT(oldValueList,OLD.estimateAuto,'~') into oldValueList;
		END IF;
		IF(NEW.estimateAuto is not null) then
			select CONCAT(newValueList,NEW.estimateAuto,'~') into newValueList;
		END IF;
	END IF;
	IF ((OLD.estimateBoat <> NEW.estimateBoat) or (OLD.estimateBoat is null and NEW.estimateBoat is not null) 
	  or (OLD.estimateBoat is not null and NEW.estimateBoat is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimateBoat~') into fieldNameList;
		IF(OLD.estimateBoat is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimateBoat is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimateBoat is not null) then
			select CONCAT(oldValueList,OLD.estimateBoat,'~') into oldValueList;
		END IF;
		IF(NEW.estimateBoat is not null) then
			select CONCAT(newValueList,NEW.estimateBoat,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.actualAuto <> NEW.actualAuto) or (OLD.actualAuto is null and NEW.actualAuto is not null) 
	  or (OLD.actualAuto is not null and NEW.actualAuto is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualAuto~') into fieldNameList;
		IF(OLD.actualAuto is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualAuto is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualAuto is not null) then
			select CONCAT(oldValueList,OLD.actualAuto,'~') into oldValueList;
		END IF;
		IF(NEW.actualAuto is not null) then
			select CONCAT(newValueList,NEW.actualAuto,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.actualBoat <> NEW.actualBoat) or (OLD.actualBoat is null and NEW.actualBoat is not null)
	  or (OLD.actualBoat is not null and NEW.actualBoat is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualBoat~') into fieldNameList;
		IF(OLD.actualBoat is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualBoat is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualBoat is not null) then
			select CONCAT(oldValueList,OLD.actualBoat,'~') into oldValueList;
		END IF;
		IF(NEW.actualBoat is not null) then
			select CONCAT(newValueList,NEW.actualBoat,'~') into newValueList;
		END IF; 
	END IF;
		
	IF ((OLD.estimateCubicFeet <> NEW.estimateCubicFeet) or (OLD.estimateCubicFeet is null and NEW.estimateCubicFeet is not null) 
	  or (OLD.estimateCubicFeet is not null and NEW.estimateCubicFeet is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimateCubicFeet~') into fieldNameList;
		IF(OLD.estimateCubicFeet is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimateCubicFeet is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimateCubicFeet is not null) then
			select CONCAT(oldValueList,OLD.estimateCubicFeet,'~') into oldValueList;
		END IF;
		IF(NEW.estimateCubicFeet is not null) then
			select CONCAT(newValueList,NEW.estimateCubicFeet,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.actualCubicFeet <> NEW.actualCubicFeet) or (OLD.actualCubicFeet is null and NEW.actualCubicFeet is not null) 
	  or (OLD.actualCubicFeet is not null and NEW.actualCubicFeet is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualCubicFeet~') into fieldNameList;
		IF(OLD.actualCubicFeet is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualCubicFeet is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualCubicFeet is not null) then
			select CONCAT(oldValueList,OLD.actualCubicFeet,'~') into oldValueList;
		END IF;
		IF(NEW.actualCubicFeet is not null) then
			select CONCAT(newValueList,NEW.actualCubicFeet,'~') into newValueList;
		END IF; 
	END IF;

	IF (OLD.originMobile <> NEW.originMobile ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originMobile~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originMobile,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originMobile,'~') into newValueList;
	END IF;
	IF (OLD.destinationMobile <> NEW.destinationMobile ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationMobile~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationMobile,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationMobile,'~') into newValueList;
	END IF;
	
	IF (OLD.unit1 <> NEW.unit1 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.unit1~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
	END IF;
	IF (OLD.unit2 <> NEW.unit2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.unit2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
	END IF;
	IF (OLD.equipment <> NEW.equipment ) THEN
	    select CONCAT(fieldNameList,'serviceorder.equipment~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.equipment,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.equipment,'~') into newValueList;
	END IF;
	IF (OLD.statusReason <> NEW.statusReason ) THEN
	    select CONCAT(fieldNameList,'serviceorder.statusReason~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.statusReason,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.statusReason,'~') into newValueList;
	END IF;
	IF (OLD.bookingAgentCode <> NEW.bookingAgentCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.bookingAgentCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.bookingAgentCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.bookingAgentCode,'~') into newValueList;
	END IF;
	IF (OLD.bookingAgentName <> NEW.bookingAgentName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.bookingAgentName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.bookingAgentName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.bookingAgentName,'~') into newValueList;
	END IF;
	IF ((OLD.entitledTotalAmount <> NEW.entitledTotalAmount) or (OLD.entitledTotalAmount is null and NEW.entitledTotalAmount is not null) 
	  or (OLD.entitledTotalAmount is not null and NEW.entitledTotalAmount is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.entitledTotalAmount~') into fieldNameList;
		IF(OLD.entitledTotalAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.entitledTotalAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.entitledTotalAmount is not null) then
			select CONCAT(oldValueList,OLD.entitledTotalAmount,'~') into oldValueList;
		END IF;
		IF(NEW.entitledTotalAmount is not null) then
			select CONCAT(newValueList,NEW.entitledTotalAmount,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.estimatedTotalExpense <> NEW.estimatedTotalExpense) or (OLD.estimatedTotalExpense is null and NEW.estimatedTotalExpense is not null) 
	  or (OLD.estimatedTotalExpense is not null and NEW.estimatedTotalExpense is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimatedTotalExpense~') into fieldNameList;
		IF(OLD.estimatedTotalExpense is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedTotalExpense is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedTotalExpense is not null) then
			select CONCAT(oldValueList,OLD.estimatedTotalExpense,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedTotalExpense is not null) then
			select CONCAT(newValueList,NEW.estimatedTotalExpense,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.estimatedTotalRevenue <> NEW.estimatedTotalRevenue) or (OLD.estimatedTotalRevenue is null and NEW.estimatedTotalRevenue is not null) 
	  or (OLD.estimatedTotalRevenue is not null and NEW.estimatedTotalRevenue is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimatedTotalRevenue~') into fieldNameList;
		IF(OLD.estimatedTotalRevenue is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedTotalRevenue is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedTotalRevenue is not null) then
			select CONCAT(oldValueList,OLD.estimatedTotalRevenue,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedTotalRevenue is not null) then
			select CONCAT(newValueList,NEW.estimatedTotalRevenue,'~') into newValueList;
		END IF; 
	END IF;
	
	IF ((OLD.distributedTotalAmount <> NEW.distributedTotalAmount) or (OLD.distributedTotalAmount is null and NEW.distributedTotalAmount is not null) 
	  or (OLD.distributedTotalAmount is not null and NEW.distributedTotalAmount is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.distributedTotalAmount~') into fieldNameList;
		IF(OLD.distributedTotalAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.distributedTotalAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.distributedTotalAmount is not null) then
			select CONCAT(oldValueList,OLD.distributedTotalAmount,'~') into oldValueList;
		END IF;
		IF(NEW.distributedTotalAmount is not null) then
			select CONCAT(newValueList,NEW.distributedTotalAmount,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.estimatedGrossMargin <> NEW.estimatedGrossMargin) or (OLD.estimatedGrossMargin is null and NEW.estimatedGrossMargin is not null) 
	  or (OLD.estimatedGrossMargin is not null and NEW.estimatedGrossMargin is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimatedGrossMargin~') into fieldNameList;
		IF(OLD.estimatedGrossMargin is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedGrossMargin is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedGrossMargin is not null) then
			select CONCAT(oldValueList,OLD.estimatedGrossMargin,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedGrossMargin is not null) then
			select CONCAT(newValueList,NEW.estimatedGrossMargin,'~') into newValueList;
		END IF;
	END IF;
	
	IF ((OLD.estimatedGrossMarginPercentage <> NEW.estimatedGrossMarginPercentage) or (OLD.estimatedGrossMarginPercentage is null and NEW.estimatedGrossMarginPercentage is not null) 
	  or (OLD.estimatedGrossMarginPercentage is not null and NEW.estimatedGrossMarginPercentage is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.estimatedGrossMarginPercentage~') into fieldNameList;
		IF(OLD.estimatedGrossMarginPercentage is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedGrossMarginPercentage is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedGrossMarginPercentage is not null) then
			select CONCAT(oldValueList,OLD.estimatedGrossMarginPercentage,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedGrossMarginPercentage is not null) then
			select CONCAT(newValueList,NEW.estimatedGrossMarginPercentage,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.revisedTotalExpense <> NEW.revisedTotalExpense) or (OLD.revisedTotalExpense is null and NEW.revisedTotalExpense is not null) 
	  or (OLD.revisedTotalExpense is not null and NEW.revisedTotalExpense is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.revisedTotalExpense~') into fieldNameList;
		IF(OLD.revisedTotalExpense is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.revisedTotalExpense is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.revisedTotalExpense is not null) then
			select CONCAT(oldValueList,OLD.revisedTotalExpense,'~') into oldValueList;
		END IF;
		IF(NEW.revisedTotalExpense is not null) then
			select CONCAT(newValueList,NEW.revisedTotalExpense,'~') into newValueList;
		END IF;
	END IF;

	IF ((OLD.revisedTotalRevenue <> NEW.revisedTotalRevenue) or (OLD.revisedTotalRevenue is null and NEW.revisedTotalRevenue is not null) 
	  or (OLD.revisedTotalRevenue is not null and NEW.revisedTotalRevenue is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.revisedTotalRevenue~') into fieldNameList;
		IF(OLD.revisedTotalRevenue is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.revisedTotalRevenue is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.revisedTotalRevenue is not null) then
			select CONCAT(oldValueList,OLD.revisedTotalRevenue,'~') into oldValueList;
		END IF;
		IF(NEW.revisedTotalRevenue is not null) then
			select CONCAT(newValueList,NEW.revisedTotalRevenue,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.revisedGrossMargin <> NEW.revisedGrossMargin) or (OLD.revisedGrossMargin is null and NEW.revisedGrossMargin is not null)
	  or (OLD.revisedGrossMargin is not null and NEW.revisedGrossMargin is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.revisedGrossMargin~') into fieldNameList;
		IF(OLD.revisedGrossMargin is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.revisedGrossMargin is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.revisedGrossMargin is not null) then
			select CONCAT(oldValueList,OLD.revisedGrossMargin,'~') into oldValueList;
		END IF;
		IF(NEW.revisedGrossMargin is not null) then
			select CONCAT(newValueList,NEW.revisedGrossMargin,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.revisedGrossMarginPercentage <> NEW.revisedGrossMarginPercentage) or (OLD.revisedGrossMarginPercentage is null and NEW.revisedGrossMarginPercentage is not null) 
	  or (OLD.revisedGrossMarginPercentage is not null and NEW.revisedGrossMarginPercentage is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.revisedGrossMarginPercentage~') into fieldNameList;
		IF(OLD.revisedGrossMarginPercentage is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.revisedGrossMarginPercentage is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.revisedGrossMarginPercentage is not null) then
			select CONCAT(oldValueList,OLD.revisedGrossMarginPercentage,'~') into oldValueList;
		END IF;
		IF(NEW.revisedGrossMarginPercentage is not null) then
			select CONCAT(newValueList,NEW.revisedGrossMarginPercentage,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.actualExpense <> NEW.actualExpense) or (OLD.actualExpense is null and NEW.actualExpense is not null) 
	  or (OLD.actualExpense is not null and NEW.actualExpense is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualExpense~') into fieldNameList;
		IF(OLD.actualExpense is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualExpense is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualExpense is not null) then
			select CONCAT(oldValueList,OLD.actualExpense,'~') into oldValueList;
		END IF;
		IF(NEW.actualExpense is not null) then
			select CONCAT(newValueList,NEW.actualExpense,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.actualRevenue <> NEW.actualRevenue) or (OLD.actualRevenue is null and NEW.actualRevenue is not null) 
	  or (OLD.actualRevenue is not null and NEW.actualRevenue is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualRevenue~') into fieldNameList;
		IF(OLD.actualRevenue is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualRevenue is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualRevenue is not null) then
			select CONCAT(oldValueList,OLD.actualRevenue,'~') into oldValueList;
		END IF;
		IF(NEW.actualRevenue is not null) then
			select CONCAT(newValueList,NEW.actualRevenue,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.actualGrossMargin <> NEW.actualGrossMargin) or (OLD.actualGrossMargin is null and NEW.actualGrossMargin is not null) 
	  or (OLD.actualGrossMargin is not null and NEW.actualGrossMargin is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualGrossMargin~') into fieldNameList;
		IF(OLD.actualGrossMargin is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualGrossMargin is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualGrossMargin is not null) then
			select CONCAT(oldValueList,OLD.actualGrossMargin,'~') into oldValueList;
		END IF;
		IF(NEW.actualGrossMargin is not null) then
			select CONCAT(newValueList,NEW.actualGrossMargin,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.actualGrossMarginPercentage <> NEW.actualGrossMarginPercentage) or (OLD.actualGrossMarginPercentage is null and NEW.actualGrossMarginPercentage is not null) 
	  or (OLD.actualGrossMarginPercentage is not null and NEW.actualGrossMarginPercentage is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.actualGrossMarginPercentage~') into fieldNameList;
		IF(OLD.actualGrossMarginPercentage is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualGrossMarginPercentage is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualGrossMarginPercentage is not null) then
			select CONCAT(oldValueList,OLD.actualGrossMarginPercentage,'~') into oldValueList;
		END IF;
		IF(NEW.actualGrossMarginPercentage is not null) then
			select CONCAT(newValueList,NEW.actualGrossMarginPercentage,'~') into newValueList;
		END IF; 
	END IF;

	IF (OLD.destinationEmail <> NEW.destinationEmail ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationEmail~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationEmail,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationEmail,'~') into newValueList;
	END IF;
	IF (OLD.destinationEmail2 <> NEW.destinationEmail2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationEmail2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationEmail2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationEmail2,'~') into newValueList;
	END IF;
	IF ((OLD.statusNumber <> NEW.statusNumber) or (OLD.statusNumber is null and NEW.statusNumber is not null) 
	  or (OLD.statusNumber is not null and NEW.statusNumber is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.statusNumber~') into fieldNameList;
		IF(OLD.statusNumber is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.statusNumber is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.statusNumber is not null) then
			select CONCAT(oldValueList,OLD.statusNumber,'~') into oldValueList;
		END IF;
		IF(NEW.statusNumber is not null) then
			select CONCAT(newValueList,NEW.statusNumber,'~') into newValueList;
		END IF; 
	END IF;
	IF (OLD.defaultAccountLineStatus <> NEW.defaultAccountLineStatus ) THEN
	    select CONCAT(fieldNameList,'serviceorder.defaultAccountLineStatus~') into fieldNameList;
	    IF(OLD.defaultAccountLineStatus = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.defaultAccountLineStatus = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.defaultAccountLineStatus = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.defaultAccountLineStatus = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	IF (OLD.customerFeedback <> NEW.customerFeedback ) THEN
	    select CONCAT(fieldNameList,'serviceorder.customerFeedback~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.customerFeedback,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.customerFeedback,'~') into newValueList;
	END IF;
	IF ((OLD.distributedTotalAmount <> NEW.distributedTotalAmount) or (OLD.distributedTotalAmount is null and NEW.distributedTotalAmount is not null) 
	  or (OLD.distributedTotalAmount is not null and NEW.distributedTotalAmount is null)) THEN
		select CONCAT(fieldNameList,'serviceorder.distributedTotalAmount~') into fieldNameList;
		IF(OLD.distributedTotalAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.distributedTotalAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.distributedTotalAmount is not null) then
			select CONCAT(oldValueList,OLD.distributedTotalAmount,'~') into oldValueList;
		END IF;
		IF(NEW.distributedTotalAmount is not null) then
			select CONCAT(newValueList,NEW.distributedTotalAmount,'~') into newValueList;
		END IF; 
	END IF;

IF (OLD.companyDivision <> NEW.companyDivision ) THEN
	    select CONCAT(fieldNameList,'serviceorder.companyDivision~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
	END IF;
	
	IF (OLD.portOfLading <> NEW.portOfLading ) THEN
	    select CONCAT(fieldNameList,'serviceorder.portOfLading~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.portOfLading,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.portOfLading,'~') into newValueList;
	END IF;
	
	IF (OLD.portOfEntry <> NEW.portOfEntry ) THEN
	    select CONCAT(fieldNameList,'serviceorder.portOfEntry~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.portOfEntry,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.portOfEntry,'~') into newValueList;
	END IF;
	
	IF (OLD.controlFlag <> NEW.controlFlag ) THEN
	    select CONCAT(fieldNameList,'serviceorder.controlFlag~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.controlFlag,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.controlFlag,'~') into newValueList;
	END IF;
	IF (OLD.originContactNameExt <> NEW.originContactNameExt ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactNameExt~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactNameExt,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactNameExt,'~') into newValueList;
	END IF;
	
	IF (OLD.contactNameExt <> NEW.contactNameExt ) THEN
	    select CONCAT(fieldNameList,'serviceorder.contactNameExt~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.contactNameExt,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.contactNameExt,'~') into newValueList;
	END IF;
	
	IF (OLD.destinationContactEmail <> NEW.destinationContactEmail ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationContactEmail~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationContactEmail,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationContactEmail,'~') into newValueList;
	END IF;
	
	IF (OLD.originContactEmail <> NEW.originContactEmail ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactEmail~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactEmail,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactEmail,'~') into newValueList;
	END IF;
	
	IF (OLD.bookingAgentCheckedBy <> NEW.bookingAgentCheckedBy ) THEN
	    select CONCAT(fieldNameList,'serviceorder.bookingAgentCheckedBy~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.bookingAgentCheckedBy,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.bookingAgentCheckedBy,'~') into newValueList;
	END IF;
	IF (OLD.emailSent <> NEW.emailSent ) THEN
	    select CONCAT(fieldNameList,'serviceorder.emailSent~') into fieldNameList;
	    IF(OLD.emailSent is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.emailSent is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.emailSent is not null) then
        	select CONCAT(oldValueList,OLD.emailSent,'~') into oldValueList;
     	END IF;
      	IF(NEW.emailSent is not null) then
        	select CONCAT(newValueList,NEW.emailSent,'~') into newValueList;
      	END IF;
	END IF;
	
	IF (OLD.revised <> NEW.revised ) THEN
	    select CONCAT(fieldNameList,'serviceorder.revised~') into fieldNameList;
	    IF(OLD.revised = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.revised = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.revised = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.revised = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.carrierDeparture <> NEW.carrierDeparture ) THEN
	    select CONCAT(fieldNameList,'serviceorder.carrierDeparture~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.carrierDeparture,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.carrierDeparture,'~') into newValueList;
	END IF;
	
	IF (OLD.carrierArrival <> NEW.carrierArrival ) THEN
	    select CONCAT(fieldNameList,'serviceorder.carrierArrival~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.carrierArrival,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.carrierArrival,'~') into newValueList;
	END IF;
	
	IF (OLD.ETD <> NEW.ETD ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ETD~') into fieldNameList;
	    IF(OLD.ETD is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ETD is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ETD is not null) then
        	select CONCAT(oldValueList,OLD.ETD,'~') into oldValueList;
     	END IF;
      	IF(NEW.ETD is not null) then
        	select CONCAT(newValueList,NEW.ETD,'~') into newValueList;
      	END IF;
	END IF;
	
	IF (OLD.ATD <> NEW.ATD ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ATD~') into fieldNameList;
	    IF(OLD.ATD is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ATD is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ATD is not null) then
        	select CONCAT(oldValueList,OLD.ATD,'~') into oldValueList;
     	END IF;
      	IF(NEW.ATD is not null) then
        	select CONCAT(newValueList,NEW.ATD,'~') into newValueList;
      	END IF;
	END IF;
	IF (OLD.ETA <> NEW.ETA ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ETA~') into fieldNameList;
	    IF(OLD.ETA is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ETA is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ETA is not null) then
        	select CONCAT(oldValueList,OLD.ETA,'~') into oldValueList;
     	END IF;
      	IF(NEW.ETA is not null) then
        	select CONCAT(newValueList,NEW.ETA,'~') into newValueList;
      	END IF;
	END IF;
	IF (OLD.ATA <> NEW.ATA ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ATA~') into fieldNameList;
	    IF(OLD.ATA is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ATA is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ATA is not null) then
        	select CONCAT(oldValueList,OLD.ATA,'~') into oldValueList;
     	END IF;
      	IF(NEW.ATA is not null) then
        	select CONCAT(newValueList,NEW.ATA,'~') into newValueList;
      	END IF;
	END IF;
	

	IF (OLD.brokerCode <> NEW.brokerCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.brokerCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.brokerCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.brokerCode,'~') into newValueList;
	END IF;
	
	
	IF (OLD.originAgentCode <> NEW.originAgentCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAgentCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAgentCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAgentCode,'~') into newValueList;
	END IF;
	
	IF (OLD.originSubAgentCode <> NEW.originSubAgentCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originSubAgentCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originSubAgentCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originSubAgentCode,'~') into newValueList;
	END IF;
	

	IF (OLD.destinationAgentCode <> NEW.destinationAgentCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationAgentCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationAgentCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationAgentCode,'~') into newValueList;
	END IF;
	
	IF (OLD.destinationSubAgentCode <> NEW.destinationSubAgentCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationSubAgentCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationSubAgentCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationSubAgentCode,'~') into newValueList;
	END IF;
	
	IF (OLD.vendorCode <> NEW.vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
	END IF;
	
	IF (OLD.forwarderCode <> NEW.forwarderCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.forwarderCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.forwarderCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.forwarderCode,'~') into newValueList;
	END IF;

	IF (OLD.clientOverallResponse <> NEW.clientOverallResponse ) THEN
	    select CONCAT(fieldNameList,'serviceorder.clientOverallResponse~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.clientOverallResponse,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.clientOverallResponse,'~') into newValueList;
	END IF;
	
	IF (OLD.OAEvaluation <> NEW.OAEvaluation ) THEN
	    select CONCAT(fieldNameList,'serviceorder.OAEvaluation~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.OAEvaluation,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.OAEvaluation,'~') into newValueList;
	END IF;
	
	IF (OLD.DAEvaluation <> NEW.DAEvaluation ) THEN
	    select CONCAT(fieldNameList,'serviceorder.DAEvaluation~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.DAEvaluation,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.DAEvaluation,'~') into newValueList;
	END IF;
	IF (OLD.rank <> NEW.rank ) THEN
	    select CONCAT(fieldNameList,'serviceorder.rank~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.rank,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.rank,'~') into newValueList;
	END IF;
	
	IF (OLD.inlandCode <> NEW.inlandCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.inlandCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.inlandCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.inlandCode,'~') into newValueList;
	END IF;
	
	IF (OLD.bookingAgentVanlineCode <> NEW.bookingAgentVanlineCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.bookingAgentVanlineCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.bookingAgentVanlineCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.bookingAgentVanlineCode,'~') into newValueList;
	END IF;
	IF (OLD.feedBack <> NEW.feedBack ) THEN
       select CONCAT(fieldNameList,'serviceorder.feedBack~') into fieldNameList;
       select CONCAT(oldValueList,OLD.feedBack,'~') into oldValueList;
       select CONCAT(newValueList,NEW.feedBack,'~') into newValueList;
   END IF;
    IF (OLD.customerFileId <> NEW.customerFileId ) THEN
       select CONCAT(fieldNameList,'serviceorder.customerFileId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.customerFileId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.customerFileId,'~') into newValueList;
   END IF;


 IF (OLD.serviceOrderFlagCarrier <> NEW.serviceOrderFlagCarrier ) THEN
       select CONCAT(fieldNameList,'serviceorder.serviceOrderFlagCarrier~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderFlagCarrier,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderFlagCarrier,'~') into newValueList;
   END IF;
IF (OLD.projectmanager <> NEW.projectmanager ) THEN
       select CONCAT(fieldNameList,'serviceorder.projectmanager~') into fieldNameList;
       select CONCAT(oldValueList,OLD.projectmanager,'~') into oldValueList;
       select CONCAT(newValueList,NEW.projectmanager,'~') into newValueList;
   END IF;

   
   
   
   
   
   
   
   
   IF (OLD.gbl <> NEW.gbl ) THEN
	    select CONCAT(fieldNameList,'serviceorder.gbl~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.gbl,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.gbl,'~') into newValueList;
	END IF;
	
	IF (OLD.distanceInKmMiles <> NEW.distanceInKmMiles ) THEN
	    select CONCAT(fieldNameList,'serviceorder.distanceInKmMiles~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.distanceInKmMiles,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.distanceInKmMiles,'~') into newValueList;
	END IF;
	
	IF (OLD.isNetworkRecord <> NEW.isNetworkRecord ) THEN
	    select CONCAT(fieldNameList,'serviceorder.isNetworkRecord~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.isNetworkRecord,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.isNetworkRecord,'~') into newValueList;
	END IF;
	
	IF (OLD.bookingAgentShipNumber <> NEW.bookingAgentShipNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.bookingAgentShipNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.bookingAgentShipNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.bookingAgentShipNumber,'~') into newValueList;
	END IF;
	
	IF (OLD.quoteAccept <> NEW.quoteAccept ) THEN
	    select CONCAT(fieldNameList,'serviceorder.quoteAccept~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.quoteAccept,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.quoteAccept,'~') into newValueList;
	END IF;
	
	IF (OLD.transitDays <> NEW.transitDays ) THEN
	    select CONCAT(fieldNameList,'serviceorder.transitDays~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.transitDays,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.transitDays,'~') into newValueList;
	END IF;
	
	IF (OLD.grpStatus <> NEW.grpStatus ) THEN
	    select CONCAT(fieldNameList,'serviceorder.grpStatus~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.grpStatus,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.grpStatus,'~') into newValueList;
	END IF;
	
	
	
	IF ((OLD.redskyBillDate <> NEW.redskyBillDate) or (OLD.redskyBillDate is null and NEW.redskyBillDate is not null)
	  or (OLD.redskyBillDate is not null and NEW.redskyBillDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.redskyBillDate~') into fieldNameList;
	    IF(OLD.redskyBillDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.redskyBillDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.redskyBillDate is not null) then
        	select CONCAT(oldValueList,OLD.redskyBillDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.redskyBillDate is not null) then
        	select CONCAT(newValueList,NEW.redskyBillDate,'~') into newValueList;
      	END IF;
	END IF;

	
	IF (OLD.grpPref <> NEW.grpPref ) THEN
	    select CONCAT(fieldNameList,'serviceorder.grpPref~') into fieldNameList;
	    IF(OLD.grpPref = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.grpPref = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.grpPref = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.grpPref = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	
	
	IF ((OLD.grpDeliveryDate <> NEW.grpDeliveryDate) or (OLD.grpDeliveryDate is null and NEW.grpDeliveryDate is not null)
	  or (OLD.grpDeliveryDate is not null and NEW.grpDeliveryDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.grpDeliveryDate~') into fieldNameList;
	    IF(OLD.grpDeliveryDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.grpDeliveryDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.grpDeliveryDate is not null) then
        	select CONCAT(oldValueList,OLD.grpDeliveryDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.grpDeliveryDate is not null) then
        	select CONCAT(newValueList,NEW.grpDeliveryDate,'~') into newValueList;
      	END IF;
	END IF;

	
	
	
	
	IF (OLD.grpID <> NEW.grpID ) THEN
	    select CONCAT(fieldNameList,'serviceorder.grpID~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.grpID,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.grpID,'~') into newValueList;
	END IF;
	
	IF (OLD.packingService <> NEW.packingService ) THEN
	    select CONCAT(fieldNameList,'serviceorder.packingService~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.packingService,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.packingService,'~') into newValueList;
	END IF;
	
	
	
	IF (OLD.networkSO <> NEW.networkSO ) THEN
	    select CONCAT(fieldNameList,'serviceorder.networkSO~') into fieldNameList;
	    IF(OLD.networkSO = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.networkSO = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.networkSO = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.networkSO = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	
	IF (OLD.est <> NEW.est ) THEN
	    select CONCAT(fieldNameList,'serviceorder.est~') into fieldNameList;
	    IF(OLD.est = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.est = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.est = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.est = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	
	IF ((OLD.sentDate <> NEW.sentDate) or (OLD.sentDate is null and NEW.sentDate is not null)
	  or (OLD.sentDate is not null and NEW.sentDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.sentDate~') into fieldNameList;
	    IF(OLD.sentDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.sentDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.sentDate is not null) then
        	select CONCAT(oldValueList,OLD.sentDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.sentDate is not null) then
        	select CONCAT(newValueList,NEW.sentDate,'~') into newValueList;
      	END IF;
	END IF;
	
	
	
	
	IF ((OLD.returnDate <> NEW.returnDate) or (OLD.returnDate is null and NEW.returnDate is not null)
	  or (OLD.returnDate is not null and NEW.returnDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.returnDate~') into fieldNameList;
	    IF(OLD.returnDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.returnDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.returnDate is not null) then
        	select CONCAT(oldValueList,OLD.returnDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.returnDate is not null) then
        	select CONCAT(newValueList,NEW.returnDate,'~') into newValueList;
      	END IF;
	END IF;
	
	
	
	
	IF (OLD.qualitySurveyExecutedBy <> NEW.qualitySurveyExecutedBy ) THEN
	    select CONCAT(fieldNameList,'serviceorder.qualitySurveyExecutedBy~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.qualitySurveyExecutedBy,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.qualitySurveyExecutedBy,'~') into newValueList;
	END IF;
	
	IF (OLD.preMoveSurvey <> NEW.preMoveSurvey ) THEN
	    select CONCAT(fieldNameList,'serviceorder.preMoveSurvey~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.preMoveSurvey,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.preMoveSurvey,'~') into newValueList;
	END IF;
	
	IF (OLD.originServices <> NEW.originServices ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originServices~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originServices,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originServices,'~') into newValueList;
	END IF;
	
	IF (OLD.destinationServices <> NEW.destinationServices ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destinationServices~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destinationServices,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destinationServices,'~') into newValueList;
	END IF;
	
	IF (OLD.overAll <> NEW.overAll ) THEN
	    select CONCAT(fieldNameList,'serviceorder.overAll~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.overAll,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.overAll,'~') into newValueList;
	END IF;
	
	IF (OLD.recommendation <> NEW.recommendation ) THEN
	    select CONCAT(fieldNameList,'serviceorder.recommendation~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.recommendation,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.recommendation,'~') into newValueList;
	END IF;
	
	IF (OLD.general <> NEW.general ) THEN
	    select CONCAT(fieldNameList,'serviceorder.general~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.general,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.general,'~') into newValueList;
	END IF;
   
   
   
   IF (OLD.transferee <> NEW.transferee ) THEN
	    select CONCAT(fieldNameList,'serviceorder.transferee~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.transferee,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.transferee,'~') into newValueList;
	END IF;
   
   
   IF (OLD.internal <> NEW.internal ) THEN
	    select CONCAT(fieldNameList,'serviceorder.internal~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.internal,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.internal,'~') into newValueList;
	END IF;
   
   
   IF (OLD.shipmentType <> NEW.shipmentType ) THEN
	    select CONCAT(fieldNameList,'serviceorder.shipmentType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.shipmentType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.shipmentType,'~') into newValueList;
	END IF;
   
   
   
   
   IF ((OLD.emailRecvd <> NEW.emailRecvd) or (OLD.emailRecvd is null and NEW.emailRecvd is not null)
	  or (OLD.emailRecvd is not null and NEW.emailRecvd is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.emailRecvd~') into fieldNameList;
	    IF(OLD.emailRecvd is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.emailRecvd is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.emailRecvd is not null) then
        	select CONCAT(oldValueList,OLD.emailRecvd,'~') into oldValueList;
     	END IF;
      	IF(NEW.emailRecvd is not null) then
        	select CONCAT(newValueList,NEW.emailRecvd,'~') into newValueList;
      	END IF;
	END IF;
   
   
   IF (OLD.originPreferredContactTime <> NEW.originPreferredContactTime ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originPreferredContactTime~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originPreferredContactTime,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originPreferredContactTime,'~') into newValueList;
	END IF;
   
   
   IF (OLD.destPreferredContactTime <> NEW.destPreferredContactTime ) THEN
	    select CONCAT(fieldNameList,'serviceorder.destPreferredContactTime~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.destPreferredContactTime,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.destPreferredContactTime,'~') into newValueList;
	END IF;
   
   
   IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ugwIntId~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
	END IF;
   
   
   IF (OLD.originContactLastName <> NEW.originContactLastName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originContactLastName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originContactLastName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originContactLastName,'~') into newValueList;
	END IF;
   
   
   IF (OLD.contactLastName <> NEW.contactLastName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.contactLastName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.contactLastName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.contactLastName,'~') into newValueList;
	END IF;
   IF (OLD.CAR_vendorCode <> NEW.CAR_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.CAR_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.CAR_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.CAR_vendorCode,'~') into newValueList;
	END IF;
   
   IF (OLD.INS_vendorCode <> NEW.INS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.INS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.INS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.INS_vendorCode,'~') into newValueList;
	END IF;
   IF (OLD.INP_vendorCode <> NEW.INP_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.INP_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.INP_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.INP_vendorCode,'~') into newValueList;
	END IF;
	IF (OLD.EDA_vendorCode <> NEW.EDA_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.EDA_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.EDA_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.EDA_vendorCode,'~') into newValueList;
	END IF;
   IF (OLD.TAS_vendorCode <> NEW.TAS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.TAS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.TAS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.TAS_vendorCode,'~') into newValueList;
	END IF;
   
   IF (OLD.COL_vendorCode <> NEW.COL_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.COL_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.COL_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.COL_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.TRG_vendorCode <> NEW.TRG_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.TRG_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.TRG_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.TRG_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.HOM_vendorCode <> NEW.HOM_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.HOM_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.HOM_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.HOM_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.RNT_vendorCode <> NEW.RNT_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.RNT_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.RNT_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.RNT_vendorCode,'~') into newValueList;
	END IF;
   
   IF (OLD.SET_vendorCode <> NEW.SET_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.SET_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.SET_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.SET_vendorCode,'~') into newValueList;
	END IF;
   
      IF (OLD.LAN_vendorCode <> NEW.LAN_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.LAN_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.LAN_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.LAN_vendorCode,'~') into newValueList;
	END IF;
	
      IF (OLD.MMG_vendorCode <> NEW.MMG_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.MMG_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.MMG_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.MMG_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.ONG_vendorCode <> NEW.ONG_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.ONG_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.ONG_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.ONG_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.PRV_vendorCode <> NEW.PRV_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.PRV_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.PRV_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.PRV_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.AIO_vendorCode <> NEW.AIO_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.AIO_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.AIO_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.AIO_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.EXP_vendorCode <> NEW.EXP_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.EXP_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.EXP_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.EXP_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.RPT_vendorCode <> NEW.RPT_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.RPT_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.RPT_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.RPT_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.SCH_schoolSelected <> NEW.SCH_schoolSelected ) THEN
	    select CONCAT(fieldNameList,'serviceorder.SCH_schoolSelected~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.SCH_schoolSelected,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.SCH_schoolSelected,'~') into newValueList;
	END IF;
   
   
      IF (OLD.TAX_vendorCode <> NEW.TAX_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.TAX_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.TAX_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.TAX_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.TAC_vendorCode <> NEW.TAC_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.TAC_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.TAC_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.TAC_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.TEN_vendorCode <> NEW.TEN_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.TEN_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.TEN_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.TEN_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.VIS_vendorCode <> NEW.VIS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.VIS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.VIS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.VIS_vendorCode,'~') into newValueList;
	END IF;
   
   
      IF (OLD.mmCounselor <> NEW.mmCounselor ) THEN
	    select CONCAT(fieldNameList,'serviceorder.mmCounselor~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.mmCounselor,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.mmCounselor,'~') into newValueList;
	END IF;
   
   
    IF (OLD.mmCounselor <> NEW.mmCounselor ) THEN
	    select CONCAT(fieldNameList,'serviceorder.mmCounselor~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.mmCounselor,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.mmCounselor,'~') into newValueList;
	END IF;
   
   
      IF (OLD.noOfEmailsSent <> NEW.noOfEmailsSent ) THEN
	    select CONCAT(fieldNameList,'serviceorder.noOfEmailsSent~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.noOfEmailsSent,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.noOfEmailsSent,'~') into newValueList;
	END IF;
   
   
    IF ((OLD.distance <> NEW.distance) or (OLD.distance is null and NEW.distance is not null)
      or (OLD.distance is not null and NEW.distance is null)) THEN
        select CONCAT(fieldNameList,'serviceorder.distance~') into fieldNameList;
        IF(OLD.distance is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.distance is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.distance is not null) then
            select CONCAT(oldValueList,OLD.distance,'~') into oldValueList;
        END IF;
        IF(NEW.distance is not null) then
            select CONCAT(newValueList,NEW.distance,'~') into newValueList;
        END IF;
    END IF;
   
   
   
    IF ((OLD.projectedGrossMarginPercentage <> NEW.projectedGrossMarginPercentage) or (OLD.projectedGrossMarginPercentage is null and NEW.projectedGrossMarginPercentage is not null)
      or (OLD.projectedGrossMarginPercentage is not null and NEW.projectedGrossMarginPercentage is null)) THEN
        select CONCAT(fieldNameList,'serviceorder.projectedGrossMarginPercentage~') into fieldNameList;
        IF(OLD.projectedGrossMarginPercentage is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.projectedGrossMarginPercentage is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.projectedGrossMarginPercentage is not null) then
            select CONCAT(oldValueList,OLD.projectedGrossMarginPercentage,'~') into oldValueList;
        END IF;
        IF(NEW.projectedGrossMarginPercentage is not null) then
            select CONCAT(newValueList,NEW.projectedGrossMarginPercentage,'~') into newValueList;
        END IF;
    END IF;
   
   
    IF ((OLD.projectedGrossMargin <> NEW.projectedGrossMargin) or (OLD.projectedGrossMargin is null and NEW.projectedGrossMargin is not null)
      or (OLD.projectedGrossMargin is not null and NEW.projectedGrossMargin is null)) THEN
        select CONCAT(fieldNameList,'serviceorder.projectedGrossMargin~') into fieldNameList;
        IF(OLD.projectedGrossMargin is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.projectedGrossMargin is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.projectedGrossMargin is not null) then
            select CONCAT(oldValueList,OLD.projectedGrossMargin,'~') into oldValueList;
        END IF;
        IF(NEW.projectedGrossMargin is not null) then
            select CONCAT(newValueList,NEW.projectedGrossMargin,'~') into newValueList;
        END IF;
    END IF;
   
   
    IF (OLD.serviceOrderFlagCarrier <> NEW.serviceOrderFlagCarrier ) THEN
	    select CONCAT(fieldNameList,'serviceorder.serviceOrderFlagCarrier~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.serviceOrderFlagCarrier,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.serviceOrderFlagCarrier,'~') into newValueList;
	END IF;
   
    IF (OLD.customerRating <> NEW.customerRating ) THEN
	    select CONCAT(fieldNameList,'serviceorder.customerRating~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.customerRating,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.customerRating,'~') into newValueList;
	END IF;
   
    IF (OLD.incExcServiceType <> NEW.incExcServiceType ) THEN
	    select CONCAT(fieldNameList,'serviceorder.incExcServiceType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.incExcServiceType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.incExcServiceType,'~') into newValueList;
	END IF;
   
   
   
   IF (OLD.isSOExtract <> NEW.isSOExtract ) THEN
	    select CONCAT(fieldNameList,'serviceorder.isSOExtract~') into fieldNameList;
	    IF(OLD.isSOExtract = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSOExtract = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSOExtract = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSOExtract = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
 IF (OLD.incExcServiceTypeLanguage <> NEW.incExcServiceTypeLanguage ) THEN
	    select CONCAT(fieldNameList,'serviceorder.incExcServiceTypeLanguage~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.incExcServiceTypeLanguage,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.incExcServiceTypeLanguage,'~') into newValueList;
	END IF;
   
   
   IF (OLD.WOP_vendorCode <> NEW.WOP_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.WOP_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.WOP_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.WOP_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.REP_vendorCode <> NEW.REP_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.REP_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.REP_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.REP_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.RLS_vendorCode <> NEW.RLS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.RLS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.RLS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.RLS_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.CAT_vendorCode <> NEW.CAT_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.CAT_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.CAT_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.CAT_vendorCode,'~') into newValueList;
	END IF;
   
   
   IF (OLD.CAT_vendorCode <> NEW.CAT_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.CAT_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.CAT_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.CAT_vendorCode,'~') into newValueList;
	END IF;
	
	
	
   IF (OLD.CLS_vendorCode <> NEW.CLS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.CLS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.CLS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.CLS_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.CHS_vendorCode <> NEW.CHS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.CHS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.CHS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.CHS_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.DPS_vendorCode <> NEW.DPS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.DPS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.DPS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.DPS_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.HSM_vendorCode <> NEW.HSM_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.HSM_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.HSM_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.HSM_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.PDT_vendorCode <> NEW.PDT_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.PDT_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.PDT_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.PDT_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.RCP_vendorCode <> NEW.RCP_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.RCP_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.RCP_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.RCP_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.SPA_vendorCode <> NEW.SPA_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.SPA_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.SPA_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.SPA_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.TCS_vendorCode <> NEW.TCS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.TCS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.TCS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.TCS_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.MTS_vendorCode <> NEW.MTS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.MTS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.MTS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.MTS_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.DSS_vendorCode <> NEW.DSS_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.DSS_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.DSS_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.DSS_vendorCode,'~') into newValueList;
	END IF;
	
	
   IF (OLD.quoteAcceptReason <> NEW.quoteAcceptReason ) THEN
	    select CONCAT(fieldNameList,'serviceorder.quoteAcceptReason~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.quoteAcceptReason,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.quoteAcceptReason,'~') into newValueList;
	END IF;
	
	
	
	IF (OLD.isSOExtract <> NEW.isSOExtract ) THEN
	    select CONCAT(fieldNameList,'serviceorder.isSOExtract~') into fieldNameList;
	    IF(OLD.isSOExtract = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSOExtract = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSOExtract = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSOExtract = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	
	IF ((OLD.hsrgSoInsertRelovision <> NEW.hsrgSoInsertRelovision) or (OLD.hsrgSoInsertRelovision is null and NEW.hsrgSoInsertRelovision is not null)
	  or (OLD.hsrgSoInsertRelovision is not null and NEW.hsrgSoInsertRelovision is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.hsrgSoInsertRelovision~') into fieldNameList;
	    IF(OLD.hsrgSoInsertRelovision is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.hsrgSoInsertRelovision is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.hsrgSoInsertRelovision is not null) then
        	select CONCAT(oldValueList,OLD.hsrgSoInsertRelovision,'~') into oldValueList;
     	END IF;
      	IF(NEW.hsrgSoInsertRelovision is not null) then
        	select CONCAT(newValueList,NEW.hsrgSoInsertRelovision,'~') into newValueList;
      	END IF;
	END IF;
	
	
   IF (OLD.moveType <> NEW.moveType ) THEN
	    select CONCAT(fieldNameList,'serviceorder.moveType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.moveType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.moveType,'~') into newValueList;
	END IF;
   
   
    IF (OLD.surveyorEvaluation <> NEW.surveyorEvaluation ) THEN
	    select CONCAT(fieldNameList,'serviceorder.surveyorEvaluation~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.surveyorEvaluation,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.surveyorEvaluation,'~') into newValueList;
	END IF;
   
   
    IF (OLD.coordinatorEvaluation <> NEW.coordinatorEvaluation ) THEN
	    select CONCAT(fieldNameList,'serviceorder.coordinatorEvaluation~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.coordinatorEvaluation,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.coordinatorEvaluation,'~') into newValueList;
	END IF;
   
   
   
    IF (OLD.projectManager <> NEW.projectManager ) THEN
	    select CONCAT(fieldNameList,'serviceorder.projectManager~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.projectManager,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.projectManager,'~') into newValueList;
	END IF;
   
   
    IF (OLD.OACoordinatorEval <> NEW.OACoordinatorEval ) THEN
	    select CONCAT(fieldNameList,'serviceorder.OACoordinatorEval~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.OACoordinatorEval,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.OACoordinatorEval,'~') into newValueList;
	END IF;
   
    IF (OLD.DACoordinatorEval <> NEW.DACoordinatorEval ) THEN
	    select CONCAT(fieldNameList,'serviceorder.DACoordinatorEval~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.DACoordinatorEval,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.DACoordinatorEval,'~') into newValueList;
	END IF;
   
    IF (OLD.HOB_vendorCode <> NEW.HOB_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.HOB_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.HOB_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.HOB_vendorCode,'~') into newValueList;
	END IF;
   
    IF (OLD.FLB_vendorCode <> NEW.FLB_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.FLB_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.FLB_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.FLB_vendorCode,'~') into newValueList;
	END IF;
   
   IF (OLD.isUpdater <> NEW.isUpdater ) THEN
	    select CONCAT(fieldNameList,'serviceorder.isUpdater~') into fieldNameList;
	    IF(OLD.isUpdater = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isUpdater = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isUpdater = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isUpdater = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;

	IF (OLD.thirdPartyServiceRequired <> NEW.thirdPartyServiceRequired ) THEN
	    select CONCAT(fieldNameList,'serviceorder.thirdPartyServiceRequired~') into fieldNameList;
	    IF(OLD.thirdPartyServiceRequired = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.thirdPartyServiceRequired = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.thirdPartyServiceRequired = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.thirdPartyServiceRequired = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	IF (OLD.jimExtract <> NEW.jimExtract ) THEN
	    select CONCAT(fieldNameList,'serviceorder.jimExtract~') into fieldNameList;
	    IF(OLD.jimExtract = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.jimExtract = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.jimExtract = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.jimExtract = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	 IF (OLD.opsPerson <> NEW.opsPerson ) THEN
	    select CONCAT(fieldNameList,'serviceorder.opsPerson~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.opsPerson,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.opsPerson,'~') into newValueList;
	END IF;
   
   
    IF (OLD.clientId <> NEW.clientId ) THEN
	    select CONCAT(fieldNameList,'serviceorder.clientId~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.clientId,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.clientId,'~') into newValueList;
	END IF;
   
   
    IF (OLD.FRL_vendorCode <> NEW.FRL_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.FRL_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.FRL_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.FRL_vendorCode,'~') into newValueList;
	END IF;
   
   
    IF (OLD.APU_vendorCode <> NEW.APU_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.APU_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.APU_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.APU_vendorCode,'~') into newValueList;
	END IF;
   
   
    IF (OLD.accrualReadyUpdatedBy <> NEW.accrualReadyUpdatedBy ) THEN
	    select CONCAT(fieldNameList,'serviceorder.accrualReadyUpdatedBy~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.accrualReadyUpdatedBy,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.accrualReadyUpdatedBy,'~') into newValueList;
	END IF;
	
	IF (OLD.quoteNumber <> NEW.quoteNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.quoteNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.quoteNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.quoteNumber,'~') into newValueList;
	END IF;
   IF (OLD.surveyTime <> NEW.surveyTime ) THEN
	    select CONCAT(fieldNameList,'serviceorder.surveyTime~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.surveyTime,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.surveyTime,'~') into newValueList;
	END IF;
	IF (OLD.surveyTime2 <> NEW.surveyTime2 ) THEN
	    select CONCAT(fieldNameList,'serviceorder.surveyTime2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.surveyTime2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.surveyTime2,'~') into newValueList;
	END IF;
	IF (OLD.surveyEmailLanguage <> NEW.surveyEmailLanguage ) THEN
	    select CONCAT(fieldNameList,'serviceorder.surveyEmailLanguage~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.surveyEmailLanguage,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.surveyEmailLanguage,'~') into newValueList;
	END IF;
	IF (OLD.originAgentEmail <> NEW.originAgentEmail ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAgentEmail~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAgentEmail,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAgentEmail,'~') into newValueList;
	END IF;
	IF (OLD.supplementGBL <> NEW.supplementGBL ) THEN
	    select CONCAT(fieldNameList,'serviceorder.supplementGBL~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.supplementGBL,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.supplementGBL,'~') into newValueList;
	END IF;
	IF (OLD.oIOverallProjectview <> NEW.oIOverallProjectview ) THEN
	    select CONCAT(fieldNameList,'serviceorder.oIOverallProjectview~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.oIOverallProjectview,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.oIOverallProjectview,'~') into newValueList;
	END IF;
	
	IF (OLD.npsScore <> NEW.npsScore ) THEN
	    select CONCAT(fieldNameList,'serviceorder.npsScore~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.npsScore,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.npsScore,'~') into newValueList;
	END IF;
	IF (OLD.originAgentContact <> NEW.originAgentContact ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAgentContact~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAgentContact,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAgentContact,'~') into newValueList;
	END IF;
	
	IF (OLD.originAgentPhoneNumber <> NEW.originAgentPhoneNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAgentPhoneNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAgentPhoneNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAgentPhoneNumber,'~') into newValueList;
	END IF;
	
	
	IF (OLD.originAgentName <> NEW.originAgentName ) THEN
	    select CONCAT(fieldNameList,'serviceorder.originAgentName~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.originAgentName,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.originAgentName,'~') into newValueList;
	END IF;
   
    IF (OLD.incExcDocTypeLanguage <> NEW.incExcDocTypeLanguage ) THEN
	    select CONCAT(fieldNameList,'serviceorder.incExcDocTypeLanguage~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.incExcDocTypeLanguage,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.incExcDocTypeLanguage,'~') into newValueList;
	END IF;
   
   
    IF (OLD.incExcDocServiceType <> NEW.incExcDocServiceType ) THEN
	    select CONCAT(fieldNameList,'serviceorder.incExcDocServiceType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.incExcDocServiceType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.incExcDocServiceType,'~') into newValueList;
	END IF;
   
   
    IF (OLD.FLB_vendorCode <> NEW.FLB_vendorCode ) THEN
	    select CONCAT(fieldNameList,'serviceorder.FLB_vendorCode~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.FLB_vendorCode,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.FLB_vendorCode,'~') into newValueList;
	END IF;
   
   
   
	IF ((OLD.accrualReadyDate <> NEW.accrualReadyDate) or (OLD.accrualReadyDate is null and NEW.accrualReadyDate is not null)
	  or (OLD.accrualReadyDate is not null and NEW.accrualReadyDate is null)) THEN
	    select CONCAT(fieldNameList,'serviceorder.accrualReadyDate~') into fieldNameList;
	    IF(OLD.accrualReadyDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.accrualReadyDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.accrualReadyDate is not null) then
        	select CONCAT(oldValueList,OLD.accrualReadyDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.accrualReadyDate is not null) then
        	select CONCAT(newValueList,NEW.accrualReadyDate,'~') into newValueList;
      	END IF;
	END IF;
	
	
	
	
	IF (OLD.surveyEmail <> NEW.surveyEmail ) THEN
	    select CONCAT(fieldNameList,'serviceorder.surveyEmail~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.surveyEmail,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.surveyEmail,'~') into newValueList;
	END IF;
   
   
	
	IF (OLD.recInvoiceNumber <> NEW.recInvoiceNumber ) THEN
	    select CONCAT(fieldNameList,'serviceorder.recInvoiceNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.recInvoiceNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.recInvoiceNumber,'~') into newValueList;
	END IF;
   
   
	IF (OLD.additionalAssistance <> NEW.additionalAssistance ) THEN
	    select CONCAT(fieldNameList,'serviceorder.additionalAssistance~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.additionalAssistance,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.additionalAssistance,'~') into newValueList;
	END IF;
   
   IF (OLD.accrueChecked <> NEW.accrueChecked ) THEN
	    select CONCAT(fieldNameList,'serviceorder.accrueChecked~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.accrueChecked,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.accrueChecked,'~') into newValueList;
	END IF;
	
	IF (OLD.welcomeMailSentOn <> NEW.welcomeMailSentOn ) THEN
	    select CONCAT(fieldNameList,'serviceorder.welcomeMailSentOn~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.welcomeMailSentOn,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.welcomeMailSentOn,'~') into newValueList;
	END IF;
   
   if(NEW.email<>'' and NEW.destinationEmail='') then
   	set NEW.destinationEmail= NEW.email;
   end if;
   IF (OLD.quoteStatus='PE' and NEW.quoteStatus ='AC') THEN
   	set NEW.status='NEW';
   	SET NEW.statusNumber='1';
   end if;
		CALL add_tblHistory (OLD.id,"serviceorder", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

    IF (OLD.status <> NEW.status ) THEN
		call CustomerFile_UpdateStatus(NEW.customerfileid,NEW.updatedby);
	end if;
	
END
$$