<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Document Bundle Details</title>   
    <meta name="heading" content="Document Bundle Details"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">
  
   	function validation(){
        return true;
   	}
    
    </script>
</head>

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>


<c:set var="buttons"> 
    <input name="save" id="save" type="submit"" class="cssbutton1" onclick="return validation();" value="Save" style="width:55px; height:25px" tabindex="83"/> 
</c:set>

<s:form id="documentBundleForm" name="documentBundleForm"
	action="saveDocumentBundle.html" method="post" validate="true">
	<s:hidden name="documentBundle.id" />
	<s:hidden name="newlineid" id="newlineid" value="" />
	<s:hidden name="reportId" id="reportId" value="" />
	<s:hidden name="id" value="${documentBundle.id}" />
	<s:hidden name="tempId1"/>
	<s:hidden name="tempId2"/>
	<s:hidden name="reportsId"/>

	<div id="layer1" style="width: 100%">
		<div id="otabs">
			<ul>
				<li><a class="current"><span>Document Bundle</span></a></li>
			</ul>
		</div>
		<div class="spnblk">&nbsp;</div>

		<div id="content" align="center">
			<div id="liquid-round-top">
				<div class="top" style="margin-top: 10px; ! margin-top: -5px;">
					<span></span>
				</div>
				<div class="center-content">
					<table class="table" style="width: 100%; margin-bottom: 0.3em;"
						border="0">
						<thead>
							<tr>
								<th>Bundle Name<font color="RED">*</font></th>
								<th>Send Point</th>
								<th>Email</th>
								<th>Email Body</th>
								<th>Print Seq<font color="RED">*</font></th>
								<th>Service Type</th>
								<th>Routing</th>
								<th>SO Mode</th>
								<th>Job Type</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="" align="left"><s:textfield
										name="documentBundle.bundleName" cssClass="input-text"
										cssStyle="width:100px;" maxlength="200" /></td>
								<td width="" align="left"><s:select
										name="documentBundle.sendPointTableName" list="%{tableList}"
										onchange="getFieldList(this)" cssStyle="width:120px;"
										cssClass="list-menu" headerKey="" headerValue="" /><br>
									<s:select name="documentBundle.sendPointFieldName"
										list="%{fieldList}" id="fieldName"
										cssStyle="width:120px;margin-top:5px;" cssClass="list-menu"
										headerKey="" headerValue="" /></td>
								<td width="" align="left"><s:select
										name="emailTable" list="%{emailTableList}"
										onchange="getFieldList1(this)" cssStyle="width:120px;"
										cssClass="list-menu" headerKey="" headerValue="" /><br>

									<s:select name="emailField"
										list="%{emailFieldList}" id="emailField"
										cssStyle="width:120px;margin-top:5px;" cssClass="list-menu"
										headerKey="" headerValue="" /></td>
								
								<td width="" align="left"><s:textarea
										name="documentBundle.description"
										cssStyle="width:150px;height:50px;" cssClass="textarea" /></td>
								
								<td width="" align="left"><s:textfield
										name="documentBundle.printSeq" cssClass="input-text" size="2"
										maxlength="2" onchange="isInteger(this)"/></td>
								<td width="" align="left"><s:select
										name="documentBundle.serviceType" list="%{service}"
										value="%{multiplServiceTypes}" multiple="true"
										cssClass="list-menu" cssStyle="width:150px; height:60px"
										/></td>
								<td width="" align="left"><s:select
										name="documentBundle.routing" list="%{routing}"
										value="%{multiplRouting}" multiple="true"
										cssClass="list-menu" cssStyle="width:150px; height:60px"
										headerKey="" headerValue="" /></td>
								<td width="" align="left"><s:select
										name="documentBundle.mode" list="%{mode}" cssClass="list-menu"
										/></td>
								<td width="" align="left"><s:select
										name="documentBundle.job" list="%{jobs}"
										value="%{multiplJobType}" multiple="true" cssClass="list-menu"
										cssStyle="width:150px;height:60px" headerKey="" headerValue="" />
								</td>
							</tr>

							<div class="spn">&nbsp;</div>

							<div id="para1" style="clear: both;">
								<table class="table" id="dataTable"
									style="width: 100%; margin-top: 10px;">
									<thead>
										<tr>
											<th width="10px">#</th>
											<th width="70px">Jrxml Name</th>
											<th width="70px">Form Description</th>
											<th width="70px" class="centeralign">PDF</th>
											<th width="70px" class="centeralign">Docx</th>
											<th width="70px" class="centeralign">Remove</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty documentBundleFormsList}">
											<c:forEach var="individualItem"
												items="${documentBundleFormsList}" varStatus="rowCounter">
												<tr>
													<td>${rowCounter.count}</td>
													<td><s:textfield cssClass="input-textUpper"
															name="reportName" value="${individualItem.reportName}"
															id="rep${rowCounter.count}" readonly="true"
															onkeyup="autoCompleterAjaxCall(this.value,'${rowCounter.count}','choices');"
															cssStyle="width:300px;" />
														<div id="choices${rowCounter.count}" class="listwhitetext"></div>
													</td>
													<td><s:textfield cssClass="input-textUpper"
															name="reportDescription" readonly="true"
															value="${individualItem.description}"
															id="des${rowCounter.count}" cssStyle="width:300px;" /></td>
													<td>
														<c:if test="${individualItem.pdf=='true' }">
															<div ALIGN="center">
																<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
															</div>
														</c:if>
														<c:if test="${individualItem.pdf=='false' || individualItem.pdf==''}">
															<div ALIGN="center">
																<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
															</div>
														</c:if>
													</td>
													<td>
														<c:if test="${individualItem.docx=='true' }">
															<div ALIGN="center">
																<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
															</div>
														</c:if>
														<c:if test="${individualItem.docx=='false' || individualItem.docx==''}">
															<div ALIGN="center">
																<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
															</div>
														</c:if>
													</td>
													<td>
														<div ALIGN="center">
															<a><img align="middle"
																onclick="confirmSubmit('${individualItem.id}');"
																style="margin: 0px 0px 0px 8px;"
																src="${pageContext.request.contextPath}/images/recycle.gif" />
															</a>
														</div>
													</td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>

								</table>
								<table>
									<tr>
										<td><input type="button" class="cssbutton1" id="addLine"
											name="addLine" style="width: 70px; height: 25px;"
											value="Add Line" onclick="addRow('dataTable');" /></td>
									</tr>
								</table>
							</div>

						</tbody>
					</table>
				</div>
				<div class="bottom-header" style="margin-top: 40px;">
					<span></span>
				</div>
			</div>
		</div>

	</div>
	<s:submit cssClass="cssbutton1" value="Save"
		onclick="return validate();"></s:submit>
	<input type="button" class="cssbutton1" name="back" value="Back"
		onclick="goBack();" />

</s:form>
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div>


<script type="text/javascript">
var http = getHTTPObject();
var http1 = getHTTPObject();
var http2 = getHTTPObject();
var http3 = getHTTPObject();

function isInteger(targetElement)
{   var i;
	var s = targetElement.value;
	for (i = 0; i < s.length; i++)
	{   
	    var c = s.charAt(i);
	    if (((c < "0") || (c > "9"))) {
	    alert("Enter valid number");
	    targetElement.value="";
	    
	    return false;
	    }
	}
	return true;
}

function validate(){
	var formName = document.forms['documentBundleForm'].elements['documentBundle.bundleName'].value;
	var printSeq = document.forms['documentBundleForm'].elements['documentBundle.printSeq'].value;
	if(formName.length == 0 || printSeq.length == 0){
		alert('Please Enter Mandatory Fields');
		return false;
	}else{
		
	}
	var reportsId='';
	<c:forEach items="${documentBundleFormsList}" var="list1">
		var aid="${list1.id}";
		if(reportsId==''){
			reportsId = aid;
		}else{
			reportsId = reportsId+','+aid;
		}
	</c:forEach>
	 document.forms['documentBundleForm'].elements['reportsId'].value=reportsId;
}

function addRow(tableID) {
	var id = '${documentBundle.id}';
	if(id.length == 0){
		alert('Please Save Document Bundle first before adding Jrxml Name.');
		return false;
	}else{
	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
		var row = table.insertRow(rowCount); 
	    var newlineid=document.getElementById('newlineid').value;
	    if(newlineid==''){
	        newlineid=rowCount;
	      }else{
	      newlineid=newlineid+"~"+rowCount;
	      }
	    document.getElementById('newlineid').value=newlineid;
	    
	    var cell1 = row.insertCell(0);
	    var element1 = document.createElement("LABEL");
		element1.type = "text";
		element1.setAttribute("value", rowCount );
		cell1.appendChild(element1);
	 	
	    var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.style.width="300px";
		element2.setAttribute("class", "input-text" );
		element2.id='rep'+rowCount;
		element2.setAttribute("onkeyup","autoCompleterAjaxCall(this,"+rowCount+",'choices')");
		
		var element5 = document.createElement("div");
		element5.id = 'choices'+rowCount;
		cell2.appendChild(element2);
		cell2.appendChild(element5);
		
		var cell3 = row.insertCell(2);
		var element3 = document.createElement("input");
		element3.type = "text";
		element3.style.width="300px";
		element3.setAttribute("class", "input-text" );
		element3.id='des'+rowCount;
		cell3.appendChild(element3);
		}
	}

function getFieldList(targetElement) {
	var tableNames = targetElement.value;
	var url="findfieldListAudit.html?ajax=1&decorator=simple&popup=true&tableNames=" + encodeURI(tableNames);
    http22.open("GET", url, true);
    http22.onreadystatechange = handleHttpResponse3;
    http22.send(null);
}

function handleHttpResponse3(){
             if (http22.readyState == 4){
                var results = http22.responseText
                results = results.trim();
                var res = results.split("@");
                targetElement = document.getElementById('fieldName');
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.getElementById('fieldName').options[i].text = '';
						document.getElementById('fieldName').options[i].value = '';
					}else{
						document.getElementById('fieldName').options[i].text = res[i];
						document.getElementById('fieldName').options[i].value = res[i];
					}
				}
 				document.getElementById("fieldName").value = '';
       }
 }
        
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http22 = getHTTPObject();

function getFieldList1(targetElement) {
	document.getElementById('emailField').value='';
	var tableNames = targetElement.value;
	var url="findfieldListAudit.html?ajax=1&decorator=simple&popup=true&tableNames=" + encodeURI(tableNames);
    http222.open("GET", url, true);
    http222.onreadystatechange = handleHttpResponse33;
    http222.send(null);
}

function handleHttpResponse33(){
             if (http222.readyState == 4){
                var results = http222.responseText
                results = results.trim();
                var res = results.split("@");
                targetElement = document.getElementById('emailField');
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.getElementById('emailField').options[i].text = '';
						document.getElementById('emailField').options[i].value = '';
					}else{
						document.getElementById('emailField').options[i].text = res[i];
						document.getElementById('emailField').options[i].value = res[i];
					}
				}
 				document.getElementById("emailField").value = '';
       }
 }
        
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http222 = getHTTPObject();

function autoCompleterAjaxCall(jrxmlName,id,divId){
	document.getElementById('documentBundleForm').setAttribute("AutoComplete","off");
	document.forms['documentBundleForm'].elements['tempId1'].value=id;
	document.forms['documentBundleForm'].elements['tempId2'].value=divId;
	jrxmlName =jrxmlName.value;
	if(jrxmlName.length>0){
	 new Ajax.Request('/redsky/printPackageAutocomplete.html?ajax=1&jrxmlName='+jrxmlName+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
		    
			      var response = transport.responseText || "no response text";
			      
                  var mydiv = document.getElementById(divId+id);
                  document.getElementById(divId+id).style.display = "block";
                  mydiv.innerHTML = response;
                  mydiv.show(); 
			    },
			    onFailure: function(){ 
				    }
			  });
	}else{
		var mydiv = document.getElementById(divId+id);
        document.getElementById(divId+id).style.display = "none";
	}
}

function getReportDescAndIdByJrxmlNameAjax(jrxmlName,id){

	 new Ajax.Request('/redsky/getReportDescAndIdByJrxmlNameAjax.html?ajax=1&jrxmlName='+jrxmlName+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
		    
			      var response = transport.responseText || "no response text";
			      var res = response.split("~");
                  document.getElementById('des'+id).value = res[0];
                  document.forms['documentBundleForm'].elements['reportId'].value = res[1];
                saveReportId(id);
			    },
			    onFailure: function(){ 
				    }
			  });
}

function saveReportId(id){
	var reportsId = document.forms['documentBundleForm'].elements['reportId'].value;
	reportsId = reportsId.trim();
	var formId = '${documentBundle.formsId}';
	formId = formId.trim();
	var check = formId.indexOf(reportsId);
	if(check >= 0){
		alert('This Jrxml has been already added, Please choose another.')
		document.getElementById('rep'+id).value ='';
			document.getElementById('des'+id).value ='';
	}else{
		if(formId!=null && !formId==''){
			reportsId = formId+','+reportsId;
		}else{
			reportsId = reportsId;
		}	
	var documentId = '${documentBundle.id}';
	if(documentId!=null){
	 	new Ajax.Request('/redsky/saveReportIdInDocumentBundleAjax.html?ajax=1&reportsId='+reportsId+'&id='+documentId+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
		    
			      var response = transport.responseText || "no response text";
			      window.location.reload();
			    },
			    onFailure: function(){ 
				    }
			  });
		}
	}
}

function checkValue(result){
	var targetID=document.forms['documentBundleForm'].elements['tempId1'].value;
	var targetID2=document.forms['documentBundleForm'].elements['tempId2'].value;
	
	document.getElementById('rep'+targetID).value=result.innerHTML;
	document.getElementById(targetID2+targetID).style.display = "none";
	var jrxmlName = document.getElementById('rep'+targetID).value;
	getReportDescAndIdByJrxmlNameAjax(jrxmlName,targetID);
}

function goBack(){
	var url = "documentBundleList.html";
	location.href = url;
}

function confirmSubmit(parentId){
	var reportsId='';
	<c:forEach items="${documentBundleFormsList}" var="list1">
		var aid="${list1.id}";
		if(aid!=parentId){
			reportsId = aid+','+reportsId;
		}else{
			reportsId = reportsId;
		}
	</c:forEach>
	var documentId = '${documentBundle.id}';
	//alert(reportsId)
	//alert(documentId)
	if(documentId!=null){
	var agree=confirm("Are you sure you wish to remove this row ?");
	if (agree){
		 new Ajax.Request('/redsky/deleteReportIdInDocumentBundleAjax.html?ajax=1&reportsId='+reportsId+'&id='+documentId+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
			    
				      var response = transport.responseText || "no response text";
				      window.location.reload();
				    },
				    onFailure: function(){ 
					    }
				  });
	 	}else{
			return false;
		}
	}
}
</script>
	