
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPhoneNumberTypesResult" type="{http://www.mss1.com/webservices/}ArrayOfMSSPhoneNumberType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPhoneNumberTypesResult"
})
@XmlRootElement(name = "GetPhoneNumberTypesResponse")
public class GetPhoneNumberTypesResponse {

    @XmlElement(name = "GetPhoneNumberTypesResult")
    protected ArrayOfMSSPhoneNumberType getPhoneNumberTypesResult;

    /**
     * Gets the value of the getPhoneNumberTypesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSPhoneNumberType }
     *     
     */
    public ArrayOfMSSPhoneNumberType getGetPhoneNumberTypesResult() {
        return getPhoneNumberTypesResult;
    }

    /**
     * Sets the value of the getPhoneNumberTypesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSPhoneNumberType }
     *     
     */
    public void setGetPhoneNumberTypesResult(ArrayOfMSSPhoneNumberType value) {
        this.getPhoneNumberTypesResult = value;
    }

}
