package com.trilasoft.app.service.impl;

import java.util.HashMap;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.PartnerRateGridDetailsDao;

import com.trilasoft.app.model.PartnerRateGridDetails;
import com.trilasoft.app.service.PartnerRateGridDetailsManager;


public class PartnerRateGridDetailsManagerImpl extends GenericManagerImpl<PartnerRateGridDetails, Long> implements PartnerRateGridDetailsManager{
	PartnerRateGridDetailsDao partnerRateGridDetailsDao;
	
	public PartnerRateGridDetailsManagerImpl(PartnerRateGridDetailsDao partnerRateGridDetailsDao) { 
        super(partnerRateGridDetailsDao); 
        this.partnerRateGridDetailsDao = partnerRateGridDetailsDao; 
    }

	public List getRateGridDetais(Long id, String sessionCorpID) {
		// TODO Auto-generated method stub
		return partnerRateGridDetailsDao.getRateGridDetais(id, sessionCorpID);
	}

	public int updateRateGridData(Long id, String sessionCorpID, HashMap saveDataDetailsMap) {
		
		return partnerRateGridDetailsDao.updateRateGridData(id, sessionCorpID, saveDataDetailsMap);
	}

	public List checkPartnerRateGridId(Long id, String sessionCorpID) { 
		return partnerRateGridDetailsDao.checkPartnerRateGridId(id, sessionCorpID); 
	}

	public int updateRateGridBasis(Long id, String sessionCorpID, HashMap saveBasisMap) {
		return partnerRateGridDetailsDao.updateRateGridBasis(id, sessionCorpID, saveBasisMap);
	}

	public int updateRateGridLabel(Long id, String sessionCorpID, HashMap saveLabelMap) {
		return partnerRateGridDetailsDao.updateRateGridLabel(id, sessionCorpID, saveLabelMap);
	}

	public List getRateGridDetaisData(long id, String sessionCorpID) {
		
		return partnerRateGridDetailsDao.getRateGridDetaisData(id, sessionCorpID);
	}
}
 