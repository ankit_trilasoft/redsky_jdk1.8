DELIMITER $$
	DROP trigger IF EXISTS `redsky`.`trigger_add_history_app_user` $$
	CREATE trigger redsky.trigger_add_history_app_user
	BEFORE UPDATE on redsky.app_user
	for each row
	BEGIN
	DECLARE fieldNameList LONGTEXT;
	DECLARE oldValueList LONGTEXT;
	DECLARE newValueList LONGTEXT;

	SET fieldNameList = " ";
	SET oldValueList = " ";
	SET newValueList = " ";
	
	IF (OLD.address <> NEW.address ) THEN
	select CONCAT(fieldNameList,'app_user.address~') into fieldNameList;
	select CONCAT(oldValueList,OLD.address,'~') into oldValueList;
	select CONCAT(newValueList,NEW.address,'~') into newValueList;
	END IF;
	IF (OLD.country <> NEW.country ) THEN
	select CONCAT(fieldNameList,'app_user.country~') into fieldNameList;
	select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
	select CONCAT(newValueList,NEW.country,'~') into newValueList;
	END IF;
	IF (OLD.city <> NEW.city ) THEN
	select CONCAT(fieldNameList,'app_user.city~') into fieldNameList;
	select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
	select CONCAT(newValueList,NEW.city,'~') into newValueList;
	END IF;
	IF (OLD.province <> NEW.province ) THEN
	select CONCAT(fieldNameList,'app_user.province~') into fieldNameList;
	select CONCAT(oldValueList,OLD.province,'~') into oldValueList;
	select CONCAT(newValueList,NEW.province,'~') into newValueList;
	END IF;
	IF (OLD.postal_code <> NEW.postal_code ) THEN
	select CONCAT(fieldNameList,'app_user.postal_code~') into fieldNameList;
	select CONCAT(oldValueList,OLD.postal_code,'~') into oldValueList;
	select CONCAT(newValueList,NEW.postal_code,'~') into newValueList;
	END IF;
	IF (OLD.location <> NEW.location ) THEN
	select CONCAT(fieldNameList,'app_user.location~') into fieldNameList;
	select CONCAT(oldValueList,OLD.location,'~') into oldValueList;
	select CONCAT(newValueList,NEW.location,'~') into newValueList;
	END IF;
	IF (OLD.signature <> NEW.signature ) THEN
	select CONCAT(fieldNameList,'app_user.signature~') into fieldNameList;
	select CONCAT(oldValueList,OLD.signature,'~') into oldValueList;
	select CONCAT(newValueList,NEW.signature,'~') into newValueList;
	END IF;
	IF (OLD.username <> NEW.username ) THEN
	select CONCAT(fieldNameList,'app_user.username~') into fieldNameList;
	select CONCAT(oldValueList,OLD.username,'~') into oldValueList;
	select CONCAT(newValueList,NEW.username,'~') into newValueList;
	END IF;
	IF (OLD.account_enabled <> NEW.account_enabled ) THEN
	select CONCAT(fieldNameList,'app_user.account_enabled~') into fieldNameList;
	IF(OLD.account_enabled = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	
		IF(OLD.account_enabled = true) THEN
			select CONCAT(oldValueList,'TRUE','~') into oldValueList;
		END IF;
		IF(NEW.account_enabled = false) THEN
			select CONCAT(newValueList,'FALSE','~') into newValueList;
		END IF;
		IF(NEW.account_enabled = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.email <> NEW.email ) THEN
	select CONCAT(fieldNameList,'app_user.email~') into fieldNameList;
	select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
	select CONCAT(newValueList,NEW.email,'~') into newValueList;
	END IF;
	IF (OLD.password_hint <> NEW.password_hint ) THEN
	select CONCAT(fieldNameList,'app_user.password_hint~') into fieldNameList;
	select CONCAT(oldValueList,OLD.password_hint,'~') into oldValueList;
	select CONCAT(newValueList,NEW.password_hint,'~') into newValueList;
	END IF;
	IF (OLD.first_name <> NEW.first_name ) THEN
	select CONCAT(fieldNameList,'app_user.first_name~') into fieldNameList;
	select CONCAT(oldValueList,OLD.first_name,'~') into oldValueList;
	select CONCAT(newValueList,NEW.first_name,'~') into newValueList;
	END IF;
	IF (OLD.last_name <> NEW.last_name ) THEN
	select CONCAT(fieldNameList,'app_user.last_name~') into fieldNameList;
	select CONCAT(oldValueList,OLD.last_name,'~') into oldValueList;
	select CONCAT(newValueList,NEW.last_name,'~') into newValueList;
	END IF;
	IF (OLD.phone_number <> NEW.phone_number ) THEN
	select CONCAT(fieldNameList,'app_user.phone_number~') into fieldNameList;
	select CONCAT(oldValueList,OLD.phone_number,'~') into oldValueList;
	select CONCAT(newValueList,NEW.phone_number,'~') into newValueList;
	END IF;
	IF (OLD.website <> NEW.website ) THEN
	select CONCAT(fieldNameList,'app_user.website~') into fieldNameList;
	select CONCAT(oldValueList,OLD.website,'~') into oldValueList;
	select CONCAT(newValueList,NEW.website,'~') into newValueList;
	END IF;
	IF (OLD.credentials_expired <> NEW.credentials_expired ) THEN
	select CONCAT(fieldNameList,'app_user.credentials_expired~') into fieldNameList;
	IF(OLD.credentials_expired = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.credentials_expired = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.credentials_expired = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.credentials_expired = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.account_expired <> NEW.account_expired ) THEN
	select CONCAT(fieldNameList,'app_user.account_expired~') into fieldNameList;
	IF(OLD.account_expired = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.account_expired = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.account_expired = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.account_expired = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.account_locked <> NEW.account_locked ) THEN
	select CONCAT(fieldNameList,'app_user.account_locked~') into fieldNameList;
	IF(OLD.account_locked = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.account_locked = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.account_locked = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.account_locked = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.corpID <> NEW.corpID ) THEN
	select CONCAT(fieldNameList,'app_user.corpID~') into fieldNameList;
	select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
	select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
	END IF;
	IF (OLD.initial <> NEW.initial ) THEN
	select CONCAT(fieldNameList,'app_user.initial~') into fieldNameList;
	select CONCAT(oldValueList,OLD.initial,'~') into oldValueList;
	select CONCAT(newValueList,NEW.initial,'~') into newValueList;
	END IF;
	IF (OLD.branch <> NEW.branch ) THEN
	select CONCAT(fieldNameList,'app_user.branch~') into fieldNameList;
	select CONCAT(oldValueList,OLD.branch,'~') into oldValueList;
	select CONCAT(newValueList,NEW.branch,'~') into newValueList;
	END IF;
	IF (OLD.department <> NEW.department ) THEN
	select CONCAT(fieldNameList,'app_user.department~') into fieldNameList;
	select CONCAT(oldValueList,OLD.department,'~') into oldValueList;
	select CONCAT(newValueList,NEW.department,'~') into newValueList;
	END IF;
	IF (OLD.faxNumber <> NEW.faxNumber ) THEN
	select CONCAT(fieldNameList,'app_user.faxNumber~') into fieldNameList;
	select CONCAT(oldValueList,OLD.faxNumber,'~') into oldValueList;
	select CONCAT(newValueList,NEW.faxNumber,'~') into newValueList;
	END IF;
	IF (OLD.warehouse <> NEW.warehouse ) THEN
	select CONCAT(fieldNameList,'app_user.warehouse~') into fieldNameList;
	select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
	select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
	END IF;
	IF (OLD.userTitle <> NEW.userTitle ) THEN
	select CONCAT(fieldNameList,'app_user.userTitle~') into fieldNameList;
	select CONCAT(oldValueList,OLD.userTitle,'~') into oldValueList;
	select CONCAT(newValueList,NEW.userTitle,'~') into newValueList;
	END IF;
	IF (OLD.supervisor <> NEW.supervisor ) THEN
	select CONCAT(fieldNameList,'app_user.supervisor~') into fieldNameList;
	select CONCAT(oldValueList,OLD.supervisor,'~') into oldValueList;
	select CONCAT(newValueList,NEW.supervisor,'~') into newValueList;
	END IF;
	IF (OLD.passwordHintQues <> NEW.passwordHintQues ) THEN
	select CONCAT(fieldNameList,'app_user.passwordHintQues~') into fieldNameList;
	select CONCAT(oldValueList,OLD.passwordHintQues,'~') into oldValueList;
	select CONCAT(newValueList,NEW.passwordHintQues,'~') into newValueList;
	END IF;
	IF (OLD.password_reset <> NEW.password_reset ) THEN
	select CONCAT(fieldNameList,'app_user.password_reset~') into fieldNameList;
	IF(OLD.password_reset = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.password_reset = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.password_reset = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.password_reset = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.userStatus <> NEW.userStatus ) THEN
	select CONCAT(fieldNameList,'app_user.userStatus~') into fieldNameList;
	select CONCAT(oldValueList,OLD.userStatus,'~') into oldValueList;
	select CONCAT(newValueList,NEW.userStatus,'~') into newValueList;
	END IF;
	
	IF (OLD.addressHome <> NEW.addressHome ) THEN
	select CONCAT(fieldNameList,'app_user.addressHome~') into fieldNameList;
	select CONCAT(oldValueList,OLD.addressHome,'~') into oldValueList;
	select CONCAT(newValueList,NEW.addressHome,'~') into newValueList;
	END IF;
	IF (OLD.cityHome <> NEW.cityHome ) THEN
	select CONCAT(fieldNameList,'app_user.cityHome~') into fieldNameList;
	select CONCAT(oldValueList,OLD.cityHome,'~') into oldValueList;
	select CONCAT(newValueList,NEW.cityHome,'~') into newValueList;
	END IF;
	IF (OLD.provinceHome <> NEW.provinceHome ) THEN
	select CONCAT(fieldNameList,'app_user.provinceHome~') into fieldNameList;
	select CONCAT(oldValueList,OLD.provinceHome,'~') into oldValueList;
	select CONCAT(newValueList,NEW.provinceHome,'~') into newValueList;
	END IF;
	IF (OLD.countryHome <> NEW.countryHome ) THEN
	select CONCAT(fieldNameList,'app_user.countryHome~') into fieldNameList;
	select CONCAT(oldValueList,OLD.countryHome,'~') into oldValueList;
	select CONCAT(newValueList,NEW.countryHome,'~') into newValueList;
	END IF;
	IF (OLD.postalCodeHome <> NEW.postalCodeHome ) THEN
	select CONCAT(fieldNameList,'app_user.postalCodeHome~') into fieldNameList;
	select CONCAT(oldValueList,OLD.postalCodeHome,'~') into oldValueList;
	select CONCAT(newValueList,NEW.postalCodeHome,'~') into newValueList;
	END IF;
	IF (OLD.phoneHome <> NEW.phoneHome ) THEN
	select CONCAT(fieldNameList,'app_user.phoneHome~') into fieldNameList;
	select CONCAT(oldValueList,OLD.phoneHome,'~') into oldValueList;
	select CONCAT(newValueList,NEW.phoneHome,'~') into newValueList;
	END IF;
	IF (OLD.phoneCell <> NEW.phoneCell ) THEN
	select CONCAT(fieldNameList,'app_user.phoneCell~') into fieldNameList;
	select CONCAT(oldValueList,OLD.phoneCell,'~') into oldValueList;
	select CONCAT(newValueList,NEW.phoneCell,'~') into newValueList;
	END IF;
	IF (OLD.defaultURL <> NEW.defaultURL ) THEN
	select CONCAT(fieldNameList,'app_user.defaultURL~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultURL,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultURL,'~') into newValueList;
	END IF;
	IF (OLD.alias <> NEW.alias ) THEN
	select CONCAT(fieldNameList,'app_user.alias~') into fieldNameList;
	select CONCAT(oldValueList,OLD.alias,'~') into oldValueList;
	select CONCAT(newValueList,NEW.alias,'~') into newValueList;
	END IF;
	IF (OLD.cityHomeCode <> NEW.cityHomeCode ) THEN
	select CONCAT(fieldNameList,'app_user.cityHomeCode~') into fieldNameList;
	select CONCAT(oldValueList,OLD.cityHomeCode,'~') into oldValueList;
	select CONCAT(newValueList,NEW.cityHomeCode,'~') into newValueList;
	END IF;
	IF (OLD.countryHomeCode <> NEW.countryHomeCode ) THEN
	select CONCAT(fieldNameList,'app_user.countryHomeCode~') into fieldNameList;
	select CONCAT(oldValueList,OLD.countryHomeCode,'~') into oldValueList;
	select CONCAT(newValueList,NEW.countryHomeCode,'~') into newValueList;
	END IF;
	IF (OLD.userType <> NEW.userType ) THEN
	select CONCAT(fieldNameList,'app_user.userType~') into fieldNameList;
	select CONCAT(oldValueList,OLD.userType,'~') into oldValueList;
	select CONCAT(newValueList,NEW.userType,'~') into newValueList;
	END IF;
	IF (OLD.defaultSoURL <> NEW.defaultSoURL ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSoURL~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultSoURL,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultSoURL,'~') into newValueList;
	END IF;
	IF (OLD.jobType <> NEW.jobType ) THEN
	select CONCAT(fieldNameList,'app_user.jobType~') into fieldNameList;
	select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
	select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
	END IF;
	IF (OLD.defaultSortForJob <> NEW.defaultSortForJob ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSortForJob~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultSortForJob,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultSortForJob,'~') into newValueList;
	END IF;
	IF (OLD.defaultSortForCso <> NEW.defaultSortForCso ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSortForCso~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultSortForCso,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultSortForCso,'~') into newValueList;
	END IF;
	IF (OLD.defaultSortForTicket <> NEW.defaultSortForTicket ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSortForTicket~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultSortForTicket,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultSortForTicket,'~') into newValueList;
	END IF;
	IF (OLD.defaultSortForClaim <> NEW.defaultSortForClaim ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSortForClaim~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultSortForClaim,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultSortForClaim,'~') into newValueList;
	END IF;
	IF (OLD.sortOrderForJob <> NEW.sortOrderForJob ) THEN
	select CONCAT(fieldNameList,'app_user.sortOrderForJob~') into fieldNameList;
	select CONCAT(oldValueList,OLD.sortOrderForJob,'~') into oldValueList;
	select CONCAT(newValueList,NEW.sortOrderForJob,'~') into newValueList;
	END IF;
	IF (OLD.sortOrderForCso <> NEW.sortOrderForCso ) THEN
	select CONCAT(fieldNameList,'app_user.sortOrderForCso~') into fieldNameList;
	select CONCAT(oldValueList,OLD.sortOrderForCso,'~') into oldValueList;
	select CONCAT(newValueList,NEW.sortOrderForCso,'~') into newValueList;
	END IF;
	IF (OLD.sortOrderTicket <> NEW.sortOrderTicket ) THEN
	select CONCAT(fieldNameList,'app_user.sortOrderTicket~') into fieldNameList;
	select CONCAT(oldValueList,OLD.sortOrderTicket,'~') into oldValueList;
	select CONCAT(newValueList,NEW.sortOrderTicket,'~') into newValueList;
	END IF;
	IF (OLD.sortOrderClaim <> NEW.sortOrderClaim ) THEN
	select CONCAT(fieldNameList,'app_user.sortOrderClaim~') into fieldNameList;
	select CONCAT(oldValueList,OLD.sortOrderClaim,'~') into oldValueList;
	select CONCAT(newValueList,NEW.sortOrderClaim,'~') into newValueList;
	END IF;
	IF ((date_format(OLD.pwdexpiryDate,'%Y-%m-%d') <> date_format(NEW.pwdexpiryDate,'%Y-%m-%d')) or (date_format(OLD.pwdexpiryDate,'%Y-%m-%d') is null and date_format(NEW.pwdexpiryDate,'%Y-%m-%d') is not null) or (date_format(OLD.pwdexpiryDate,'%Y-%m-%d') is not null and date_format(NEW.pwdexpiryDate,'%Y-%m-%d') is null)) THEN
	select CONCAT(fieldNameList,'app_user.pwdexpiryDate~') into fieldNameList;
	IF(OLD.pwdexpiryDate is null) THEN
	select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.pwdexpiryDate is null) THEN
	select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.pwdexpiryDate is not null) THEN
	select CONCAT(oldValueList,OLD.pwdexpiryDate,'~') into oldValueList;
	END IF;
	IF(NEW.pwdexpiryDate is not null) THEN
	select CONCAT(newValueList,NEW.pwdexpiryDate,'~') into newValueList;
	END IF;
	END IF;
	
	IF ((date_format(OLD.login_time,'%Y-%m-%d') <> date_format(NEW.login_time,'%Y-%m-%d')) or (date_format(OLD.login_time,'%Y-%m-%d') is null and date_format(NEW.login_time,'%Y-%m-%d') is not null) or (date_format(OLD.login_time,'%Y-%m-%d') is not null and date_format(NEW.login_time,'%Y-%m-%d') is null)) THEN
	select CONCAT(fieldNameList,'app_user.login_time~') into fieldNameList;
	IF(OLD.login_time is null) THEN
	select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.login_time is null) THEN
	select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.login_time is not null) THEN
	select CONCAT(oldValueList,OLD.login_time,'~') into oldValueList;
	END IF;
	IF(NEW.login_time is not null) THEN
	select CONCAT(newValueList,NEW.login_time,'~') into newValueList;
	END IF;
	END IF;
	
	IF ((date_format(OLD.pricePointStartDate,'%Y-%m-%d') <> date_format(NEW.pricePointStartDate,'%Y-%m-%d')) or (date_format(OLD.pricePointStartDate,'%Y-%m-%d') is null and date_format(NEW.pricePointStartDate,'%Y-%m-%d') is not null) or (date_format(OLD.pricePointStartDate,'%Y-%m-%d') is not null and date_format(NEW.pricePointStartDate,'%Y-%m-%d') is null)) THEN
	select CONCAT(fieldNameList,'app_user.pricePointStartDate~') into fieldNameList;
	IF(OLD.pricePointStartDate is null) THEN
	select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.pricePointStartDate is null) THEN
	select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.pricePointStartDate is not null) THEN
	select CONCAT(oldValueList,OLD.pricePointStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.pricePointStartDate is not null) THEN
	select CONCAT(newValueList,NEW.pricePointStartDate,'~') into newValueList;
	END IF;
	END IF;
		
	IF (OLD.defaultCss <> NEW.defaultCss ) THEN
	select CONCAT(fieldNameList,'app_user.defaultCss~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultCss,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultCss,'~') into newValueList;
	END IF;
	
	IF (OLD.parentAgent <> NEW.parentAgent ) THEN
	select CONCAT(fieldNameList,'app_user.parentAgent~') into fieldNameList;
	select CONCAT(oldValueList,OLD.parentAgent,'~') into oldValueList;
	select CONCAT(newValueList,NEW.parentAgent,'~') into newValueList;
	END IF;
	IF (OLD.functionalArea <> NEW.functionalArea ) THEN
	select CONCAT(fieldNameList,'app_user.functionalArea~') into fieldNameList;
	select CONCAT(oldValueList,OLD.functionalArea,'~') into oldValueList;
	select CONCAT(newValueList,NEW.functionalArea,'~') into newValueList;
	END IF;
	IF (OLD.skypeID <> NEW.skypeID ) THEN
	select CONCAT(fieldNameList,'app_user.skypeID~') into fieldNameList;
	select CONCAT(oldValueList,OLD.skypeID,'~') into oldValueList;
	select CONCAT(newValueList,NEW.skypeID,'~') into newValueList;
	END IF;
	IF (OLD.defaultSearchStatus <> NEW.defaultSearchStatus ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSearchStatus~') into fieldNameList;
	IF(OLD.defaultSearchStatus = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.defaultSearchStatus = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.defaultSearchStatus = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.defaultSearchStatus = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.contact <> NEW.contact ) THEN
	select CONCAT(fieldNameList,'app_user.contact~') into fieldNameList;
	IF(OLD.contact = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.contact = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.contact = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.contact = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.newsUpdateFlag <> NEW.newsUpdateFlag ) THEN
	select CONCAT(fieldNameList,'app_user.newsUpdateFlag~') into fieldNameList;
	IF(OLD.newsUpdateFlag = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.newsUpdateFlag = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.newsUpdateFlag = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.newsUpdateFlag = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.doNotEmail <> NEW.doNotEmail ) THEN
	select CONCAT(fieldNameList,'app_user.doNotEmail~') into fieldNameList;
	IF(OLD.doNotEmail = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.doNotEmail = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.doNotEmail = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.doNotEmail = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
	IF (OLD.extractJobType <> NEW.extractJobType ) THEN
	select CONCAT(fieldNameList,'app_user.extractJobType~') into fieldNameList;
	select CONCAT(oldValueList,OLD.extractJobType,'~') into oldValueList;
	select CONCAT(newValueList,NEW.extractJobType,'~') into newValueList;
	END IF;
	IF (OLD.bookingAgent <> NEW.bookingAgent ) THEN
	select CONCAT(fieldNameList,'app_user.bookingAgent~') into fieldNameList;
	select CONCAT(oldValueList,OLD.bookingAgent,'~') into oldValueList;
	select CONCAT(newValueList,NEW.bookingAgent,'~') into newValueList;
	END IF;
	IF (OLD.contract <> NEW.contract ) THEN
	select CONCAT(fieldNameList,'app_user.contract~') into fieldNameList;
	select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
	select CONCAT(newValueList,NEW.contract,'~') into newValueList;
	END IF;
	IF (OLD.specialAccessCorpIds <> NEW.specialAccessCorpIds ) THEN
	select CONCAT(fieldNameList,'app_user.specialAccessCorpIds~') into fieldNameList;
	select CONCAT(oldValueList,OLD.specialAccessCorpIds,'~') into oldValueList;
	select CONCAT(newValueList,NEW.specialAccessCorpIds,'~') into newValueList;
	END IF;
	IF (OLD.defaultWarehouse <> NEW.defaultWarehouse ) THEN
	select CONCAT(fieldNameList,'app_user.defaultWarehouse~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultWarehouse,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultWarehouse,'~') into newValueList;
	END IF;
	IF (OLD.gender <> NEW.gender ) THEN
	select CONCAT(fieldNameList,'app_user.gender~') into fieldNameList;
	select CONCAT(oldValueList,OLD.gender,'~') into oldValueList;
	select CONCAT(newValueList,NEW.gender,'~') into newValueList;
	END IF;
	IF (OLD.basedAt <> NEW.basedAt ) THEN
	select CONCAT(fieldNameList,'app_user.basedAt~') into fieldNameList;
	select CONCAT(oldValueList,OLD.basedAt,'~') into oldValueList;
	select CONCAT(newValueList,NEW.basedAt,'~') into newValueList;
	END IF;
	IF (OLD.basedAtName <> NEW.basedAtName ) THEN
	select CONCAT(fieldNameList,'app_user.basedAtName~') into fieldNameList;
	select CONCAT(oldValueList,OLD.basedAtName,'~') into oldValueList;
	select CONCAT(newValueList,NEW.basedAtName,'~') into newValueList;
	END IF;
	IF (OLD.jobFunction <> NEW.jobFunction ) THEN
	select CONCAT(fieldNameList,'app_user.jobFunction~') into fieldNameList;
	select CONCAT(oldValueList,OLD.jobFunction,'~') into oldValueList;
	select CONCAT(newValueList,NEW.jobFunction,'~') into newValueList;
	END IF;
	IF (OLD.mobileNumber <> NEW.mobileNumber ) THEN
	select CONCAT(fieldNameList,'app_user.mobileNumber~') into fieldNameList;
	select CONCAT(oldValueList,OLD.mobileNumber,'~') into oldValueList;
	select CONCAT(newValueList,NEW.mobileNumber,'~') into newValueList;
	END IF;
	IF (OLD.digitalSignature <> NEW.digitalSignature ) THEN
	select CONCAT(fieldNameList,'app_user.digitalSignature~') into fieldNameList;
	select CONCAT(oldValueList,OLD.digitalSignature,'~') into oldValueList;
	select CONCAT(newValueList,NEW.digitalSignature,'~') into newValueList;
	END IF;
	IF (OLD.networkCoordinator <> NEW.networkCoordinator ) THEN
	select CONCAT(fieldNameList,'app_user.networkCoordinator~') into fieldNameList;
	select CONCAT(oldValueList,OLD.networkCoordinator,'~') into oldValueList;
	select CONCAT(newValueList,NEW.networkCoordinator,'~') into newValueList;
	END IF;
	IF (OLD.companyDivision <> NEW.companyDivision ) THEN
	select CONCAT(fieldNameList,'app_user.companyDivision~') into fieldNameList;
	select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
	select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
	END IF;
	IF (OLD.macid <> NEW.macid ) THEN
	select CONCAT(fieldNameList,'app_user.macid~') into fieldNameList;
	select CONCAT(oldValueList,OLD.macid,'~') into oldValueList;
	select CONCAT(newValueList,NEW.macid,'~') into newValueList;
	END IF;
	
IF (OLD.login_status <> NEW.login_status ) THEN
	    select CONCAT(fieldNameList,'app_user.login_status~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.login_status ,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.login_status ,'~') into newValueList;
	END IF;

IF (OLD.rank <> NEW.rank ) THEN
	select CONCAT(fieldNameList,'app_user.rank~') into fieldNameList;
	select CONCAT(oldValueList,OLD.rank,'~') into oldValueList;
	select CONCAT(newValueList,NEW.rank,'~') into newValueList;
	END IF;

IF (OLD.workStartTime <> NEW.workStartTime ) THEN
	select CONCAT(fieldNameList,'app_user.workStartTime~') into fieldNameList;
	select CONCAT(oldValueList,OLD.workStartTime,'~') into oldValueList;
	select CONCAT(newValueList,NEW.workStartTime,'~') into newValueList;
	END IF;

IF (OLD.workendTime <> NEW.workendTime ) THEN
	select CONCAT(fieldNameList,'app_user.workendTime~') into fieldNameList;
	select CONCAT(oldValueList,OLD.workendTime,'~') into oldValueList;
	select CONCAT(newValueList,NEW.workendTime,'~') into newValueList;
	END IF;

IF (OLD.reminderDuration <> NEW.reminderDuration ) THEN
	select CONCAT(fieldNameList,'app_user.reminderDuration~') into fieldNameList;
	select CONCAT(oldValueList,OLD.reminderDuration,'~') into oldValueList;
	select CONCAT(newValueList,NEW.reminderDuration,'~') into newValueList;
	END IF;

IF (OLD.mobile_access <> NEW.mobile_access ) THEN
	select CONCAT(fieldNameList,'app_user.mobile_access~') into fieldNameList;
	IF(OLD.mobile_access = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.mobile_access = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.mobile_access = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.mobile_access = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;

IF (OLD.loginFailureReason <> NEW.loginFailureReason ) THEN
	select CONCAT(fieldNameList,'app_user.loginFailureReason~') into fieldNameList;
	select CONCAT(oldValueList,OLD.loginFailureReason,'~') into oldValueList;
	select CONCAT(newValueList,NEW.loginFailureReason,'~') into newValueList;
	END IF;

IF (OLD.fileCabinetView <> NEW.fileCabinetView ) THEN
	select CONCAT(fieldNameList,'app_user.fileCabinetView~') into fieldNameList;
	select CONCAT(oldValueList,OLD.fileCabinetView,'~') into oldValueList;
	select CONCAT(newValueList,NEW.fileCabinetView,'~') into newValueList;
	END IF;

IF (OLD.acctDisplayEntitled <> NEW.acctDisplayEntitled ) THEN
	select CONCAT(fieldNameList,'app_user.acctDisplayEntitled~') into fieldNameList;
	IF(OLD.acctDisplayEntitled = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.acctDisplayEntitled = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.acctDisplayEntitled = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.acctDisplayEntitled = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;

IF (OLD.acctDisplayEstimate <> NEW.acctDisplayEstimate ) THEN
	select CONCAT(fieldNameList,'app_user.acctDisplayEstimate~') into fieldNameList;
	IF(OLD.acctDisplayEstimate = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.acctDisplayEstimate = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.acctDisplayEstimate = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.acctDisplayEstimate = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;

IF (OLD.acctDisplayRevision <> NEW.acctDisplayRevision ) THEN
	select CONCAT(fieldNameList,'app_user.acctDisplayRevision~') into fieldNameList;
	IF(OLD.acctDisplayRevision = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.acctDisplayRevision = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.acctDisplayRevision = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.acctDisplayRevision = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;

IF (OLD.pricingRevision <> NEW.pricingRevision ) THEN
	select CONCAT(fieldNameList,'app_user.pricingRevision~') into fieldNameList;
	IF(OLD.pricingRevision = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.pricingRevision = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.pricingRevision = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.pricingRevision = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;

IF (OLD.pricingActual <> NEW.pricingActual ) THEN
	select CONCAT(fieldNameList,'app_user.pricingActual~') into fieldNameList;
	IF(OLD.pricingActual = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.pricingActual = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.pricingActual = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.pricingActual = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;
  IF ((date_format(OLD.pricePointStartDate,'%Y-%m-%d') <> date_format(NEW.pricePointStartDate,'%Y-%m-%d')) or (date_format(OLD.pricePointStartDate,'%Y-%m-%d') is null and date_format(NEW.pricePointStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.pricePointStartDate,'%Y-%m-%d') is not null and date_format(NEW.pricePointStartDate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'app_user.pricePointStartDate~') into fieldNameList;
       IF(OLD.pricePointStartDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.pricePointStartDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.pricePointStartDate is not null) THEN
        	select CONCAT(oldValueList,OLD.pricePointStartDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.pricePointStartDate is not null) THEN
        	select CONCAT(newValueList,NEW.pricePointStartDate,'~') into newValueList;
      	END IF;
   END IF;

IF (OLD.defaultSortforFileCabinet <> NEW.defaultSortforFileCabinet ) THEN
	select CONCAT(fieldNameList,'app_user.defaultSortforFileCabinet~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultSortforFileCabinet,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultSortforFileCabinet,'~') into newValueList;
	END IF;
IF (OLD.sortOrderForFileCabinet <> NEW.sortOrderForFileCabinet ) THEN
	select CONCAT(fieldNameList,'app_user.sortOrderForFileCabinet~') into fieldNameList;
	select CONCAT(oldValueList,OLD.sortOrderForFileCabinet,'~') into oldValueList;
	select CONCAT(newValueList,NEW.sortOrderForFileCabinet,'~') into newValueList;
	END IF;
IF (OLD.followUpEmailAlert <> NEW.followUpEmailAlert ) THEN
	select CONCAT(fieldNameList,'app_user.followUpEmailAlertl~') into fieldNameList;
	IF(OLD.followUpEmailAlert = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.followUpEmailAlert = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.followUpEmailAlert = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.followUpEmailAlert = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
IF (OLD.salesPortalAccess <> NEW.salesPortalAccess ) THEN
	select CONCAT(fieldNameList,'app_user.salesPortalAccess~') into fieldNameList;
	IF(OLD.salesPortalAccess = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.salesPortalAccess = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.salesPortalAccess = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.salesPortalAccess = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
IF (OLD.moveType <> NEW.moveType ) THEN
	select CONCAT(fieldNameList,'app_user.moveType~') into fieldNameList;
	select CONCAT(oldValueList,OLD.moveType,'~') into oldValueList;
	select CONCAT(newValueList,NEW.moveType,'~') into newValueList;
	END IF;

IF (OLD.soListView <> NEW.soListView ) THEN
	select CONCAT(fieldNameList,'app_user.soListView~') into fieldNameList;
	select CONCAT(oldValueList,OLD.soListView,'~') into oldValueList;
	select CONCAT(newValueList,NEW.soListView,'~') into newValueList;
	END IF;
IF (OLD.relocationContact <> NEW.relocationContact ) THEN
	select CONCAT(fieldNameList,'app_user.relocationContact~') into fieldNameList;
	IF(OLD.relocationContact = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.relocationContact = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.relocationContact = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.relocationContact = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;
	END IF;
 IF ((OLD.NPSscore <> NEW.NPSscore) or (OLD.NPSscore is null and NEW.NPSscore is not null)
      or (OLD.NPSscore is not null and NEW.NPSscore is null)) THEN
        select CONCAT(fieldNameList,'app_user.NPSscore~') into fieldNameList;
        IF(OLD.NPSscore is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.NPSscore is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.NPSscore is not null) then
            select CONCAT(oldValueList,OLD.NPSscore,'~') into oldValueList;
        END IF;
        IF(NEW.NPSscore is not null) then
            select CONCAT(newValueList,NEW.NPSscore,'~') into newValueList;
        END IF;
    END IF;
IF (OLD.commodity <> NEW.commodity ) THEN
	select CONCAT(fieldNameList,'app_user.commodity~') into fieldNameList;
	select CONCAT(oldValueList,OLD.commodity,'~') into oldValueList;
	select CONCAT(newValueList,NEW.commodity,'~') into newValueList;
	END IF;
IF (OLD.serviceType <> NEW.serviceType ) THEN
	select CONCAT(fieldNameList,'app_user.serviceType~') into fieldNameList;
	select CONCAT(oldValueList,OLD.serviceType,'~') into oldValueList;
	select CONCAT(newValueList,NEW.serviceType,'~') into newValueList;
	END IF;

IF (OLD.recordsForSoDashboard <> NEW.recordsForSoDashboard ) THEN
	select CONCAT(fieldNameList,'app_user.recordsForSoDashboard~') into fieldNameList;
	select CONCAT(oldValueList,OLD.recordsForSoDashboard,'~') into oldValueList;
	select CONCAT(newValueList,NEW.recordsForSoDashboard,'~') into newValueList;
	END IF;

IF (OLD.additionalCommissionType <> NEW.additionalCommissionType ) THEN
	select CONCAT(fieldNameList,'app_user.additionalCommissionType~') into fieldNameList;
	select CONCAT(oldValueList,OLD.additionalCommissionType,'~') into oldValueList;
	select CONCAT(newValueList,NEW.additionalCommissionType,'~') into newValueList;
	END IF;

IF (OLD.networkCoordinatorStatus <> NEW.networkCoordinatorStatus ) THEN
	select CONCAT(fieldNameList,'app_user.networkCoordinatorStatus~') into fieldNameList;
	IF(OLD.networkCoordinatorStatus = false) THEN
	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	END IF;
	IF(OLD.networkCoordinatorStatus = true) THEN
	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	END IF;
	IF(NEW.networkCoordinatorStatus = false) THEN
	select CONCAT(newValueList,'FALSE','~') into newValueList;
	END IF;
	IF(NEW.networkCoordinatorStatus = true) THEN
	select CONCAT(newValueList,'TRUE','~') into newValueList;
	END IF;

	END IF;


IF (OLD.defaultOperationCalendar <> NEW.defaultOperationCalendar ) THEN
	select CONCAT(fieldNameList,'app_user.defaultOperationCalendar~') into fieldNameList;
	select CONCAT(oldValueList,OLD.defaultOperationCalendar,'~') into oldValueList;
	select CONCAT(newValueList,NEW.defaultOperationCalendar,'~') into newValueList;
	END IF;

IF (OLD.password <> NEW.password ) THEN
	select CONCAT(fieldNameList,'app_user.password~') into fieldNameList;
	select CONCAT(oldValueList,'*****','~') into oldValueList;
	select CONCAT(newValueList,'*****','~') into newValueList;
	END IF;

IF (OLD.confirmPassword <> NEW.confirmPassword ) THEN
	select CONCAT(fieldNameList,'app_user.confirmPassword~') into fieldNameList;
	select CONCAT(oldValueList,'*****','~') into oldValueList;
	select CONCAT(newValueList,'*****','~') into newValueList;
	END IF;
	CALL add_tblHistory (OLD.id,"app_user", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

	END $$
	delimiter;
	