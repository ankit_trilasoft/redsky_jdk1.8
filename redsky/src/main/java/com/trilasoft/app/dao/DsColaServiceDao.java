package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsColaService;

public interface DsColaServiceDao extends GenericDao<DsColaService, Long>{
	
	List getListById(Long id);

}
