package com.trilasoft.app.model;

public class RefMasterDTO {

	private String parameter;
	private String code; 
	private String description;
	private String 	flex1;
	private String flex2;
	private String flex3;
	private String flex4;
	private String bucket2;
	private String status;
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFlex1() {
		return flex1;
	}
	public void setFlex1(String flex1) {
		this.flex1 = flex1;
	}
	public String getFlex2() {
		return flex2;
	}
	public void setFlex2(String flex2) {
		this.flex2 = flex2;
	}
	public String getFlex3() {
		return flex3;
	}
	public void setFlex3(String flex3) {
		this.flex3 = flex3;
	}
	public String getFlex4() {
		return flex4;
	}
	public void setFlex4(String flex4) {
		this.flex4 = flex4;
	}
	public String getBucket2() {
		return bucket2;
	}
	public void setBucket2(String bucket2) {
		this.bucket2 = bucket2;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
