<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="lossList.title"/></title>   
    <meta name="heading" content="<fmt:message key='lossList.heading'/>"/>
    <style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>

<script>
function goToLossDetail(targetValue){
       document.forms['lossForm'].elements['id'].value = targetValue;
       document.forms['lossForm'].action = 'editLoss.html?from=list';
       document.forms['lossForm'].submit();
}

  <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
 	window.onload = function() { 
 		document.getElementById("addButton").style.display="none";	
	}
</sec-auth:authComponent>
function lossdetails(itmClmsItemSeqNbr,claimNumber,position){
 var url="pertaininglossList.html?itmClmsItem="+itmClmsItemSeqNbr+"&claimNum="+claimNumber+"&decorator=popup&popup=true";
ajax_SoTooltip(url,position);	
}
</script>    
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="lossForm" action="saveLoss" method="post" validate="true">
<s:hidden name="id" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="serviceOrder.id"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="button1">
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<sec-auth:authComponent componentId="module.tab.claims.serviceorderTab">
     <input type="button" id="addButton" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/editNewLoss.html?id=${claim.id}"/>'"  
        value="<fmt:message key="button.add"/>"/> 
</sec-auth:authComponent>         
</c:set>
<div id="Layer3" style="width:100%;">
<div id="newmnav">
			<ul>
			 <sec-auth:authComponent componentId="module.tab.claims.serviceorderTab">
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.billingTab">
			 <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			 </sec-auth:authComponent>
			 </sec-auth:authComponent>
			 <%-- <li><a href="servicePartners.html?id=${serviceOrder.id}"><span>Partner</span></a></li>  --%>
			 <sec-auth:authComponent componentId="module.tab.claims.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose> 
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		    <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose>
		    </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
	         <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
		  	<sec-auth:authComponent componentId="module.tab.claims.forwardingTab">
    		  	<c:if test="${userType!='ACCOUNT' && serviceOrder.job !='RLO'}">
    		  	<c:if test="${forwardingTabVal!='Y'}"> 
	   				<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			</c:if>
	  			<c:if test="${userType=='ACCOUNT' && serviceOrder.job !='RLO'}">
	  			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.claims.domesticTab">  
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                 <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.claims.statusTab">
			 			  		   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.claims.ticketTab">  
			  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.claims.summaryTab">
			  	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
			 </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</sec-auth:authComponent>
			
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
           	  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>
	
		</div>


<div id="Layer" style="width:100%;">
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
  <div id="newmnav">
  <ul>
   <li><a href="editClaim.html?id=${claim.id}"><span>Claim Detail</span></a></li>
   <li id="newmnav1" style="background:#FFF"><a class="current""><span>Claim Items</span></a></li>
  <sec-auth:authComponent componentId="module.tab.claims.reportTab">
   <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=loss&reportSubModule=loss&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
                 </ul></div><div class="spn" style="!margin-bottom:2px;">&nbsp;</div>
                
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">

<s:set name="losss" value="losss" scope="request"/>
<display:table name="losss" class="table" requestURI="" id="lossList" export="true" defaultsort="1" style="width:99%;margin-top:1px;margin-left: 5px;" pagesize="100">   
  <%--
  <tbody>
<tr>
  <td align="left">
</td></tr>
</tbody></table>
    <display:column property="id" sortable="true" href="editLoss.html" paramId="id" paramProperty="id" titleKey="loss.id"/>
     <display:column property="idNumber1" sortable="true" titleKey="loss.idNumber1" href="editLoss.html" paramId="id" paramProperty="id" />
     
  --%>
    <display:column sortable="true" titleKey="loss.idNumber1"><a onclick="goToLossDetail(${lossList.id});" style="cursor:pointer"><c:out value="${lossList.idNumber1}" /></a>
    <c:if test="${lossList.itmClmsItemSeqNbr!=''}">
    <img align="top" onclick="lossdetails('${lossList.itmClmsItemSeqNbr}','${lossList.claimNumber}',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
    </c:if>
    </display:column> 
    <display:column property="itemDescription" sortable="true" titleKey="loss.itemDescription" />
    <display:column property="lossComment" sortable="true" titleKey="loss.lossComment" />
    <display:column property="lossType" sortable="true" titleKey="loss.lossType" />
    <display:column property="warehouse" sortable="true" titleKey="loss.warehouse" />
    <display:column property="damageByAction" sortable="true" titleKey="loss.damageByAction" /> 
    <display:column property="lossAction" sortable="true" titleKey="loss.lossAction" /> 
    
    <display:column headerClass="containeralign" style="text-align: right;" property="paidCompensation3Party" sortable="true" titleKey="loss.paidCompensation3Party" /> 
    <display:column property="chequeNumber3Party" sortable="true" title="Cheque #" />
    <display:column headerClass="containeralign" style="text-align: right;" property="paidCompensationCustomer" sortable="true" titleKey="loss.paidCompensationCustomer" />     
    <display:column property="chequeNumberCustmer" sortable="true" title="Cheque #" />
    <display:setProperty name="export.excel.filename" value="Claim Items.xls" />
    <display:setProperty name="export.csv.filename" value="Claim Items.csv" />
</display:table> 
<%-- 
<a href="editNewLoss.html?id=${claim.id}"><input type="button" value="Add" /></a>
--%>
</div>
<c:out value="${button1}" escapeXml="false" />  
</s:form>
 
    
<script type="text/javascript">
	<sec-auth:authComponent componentId="module.script.form.agentScript">
	document.getElementById("addButton").style.display="none";		 
	</sec-auth:authComponent>
	try{
		<c:if test="${defaultVal == 'true'}"> 
		location.href = 'editLoss.html?from=list&id=${lossList.id}'; 	   
		</c:if> 
	}catch(e){}
  //   highlightTableRows("lossList");
     Form.focusFirstElement($("lossForm"));   
</script>  
 