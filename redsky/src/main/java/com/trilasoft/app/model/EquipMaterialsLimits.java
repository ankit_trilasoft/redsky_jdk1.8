package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "equipmaterialslimits")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")

public class EquipMaterialsLimits extends BaseObject {
	
	private Long id;

	private String branch;

	private String category;
	
	private String resource;
	
	private BigDecimal maxResourceLimit;
	
	private BigDecimal minResourceLimit;
	
	private String corpId;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;
	
	private String division;
	
	private Integer qty;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("hubId", branch).append("category", category)
				.append("resource", resource)
				.append("minResourceLimit", minResourceLimit)
				.append("maxResourceLimit", maxResourceLimit)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("qty", qty)
				.append("division", division).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof OperationsResourceLimits))
			return false;
		EquipMaterialsLimits castOther = (EquipMaterialsLimits) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(branch, castOther.branch)
				.append(category, castOther.category)
				.append(resource, castOther.resource)
				.append(maxResourceLimit, castOther.maxResourceLimit)
				.append(minResourceLimit, castOther.minResourceLimit)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(division, castOther.division)
				.append(updatedOn, castOther.updatedOn)
				.append(qty, castOther.qty)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(branch).append(category)
				.append(resource).append(maxResourceLimit).append(minResourceLimit).append(corpId)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn)
				.append(division)
				.append(qty)
				.toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 15)
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	@Column(length = 55)
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}	

	
	@Column(length = 15)
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
    
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column
	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}
	
	@Column
	public BigDecimal getMinResourceLimit() {
		return minResourceLimit;
	}

	public void setMinResourceLimit(BigDecimal minResourceLimit) {
		this.minResourceLimit = minResourceLimit;
	}
	
	@Column
	public BigDecimal getMaxResourceLimit() {
		return maxResourceLimit;
	}

	public void setMaxResourceLimit(BigDecimal maxResourceLimit) {
		this.maxResourceLimit = maxResourceLimit;
	}

	@Column
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}


}

