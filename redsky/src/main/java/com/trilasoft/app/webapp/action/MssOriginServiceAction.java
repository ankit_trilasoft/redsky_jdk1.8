package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.MssOriginService;
import com.trilasoft.app.service.MssOriginServiceManager;


public class MssOriginServiceAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private MssOriginService mssOriginService;
	private MssOriginServiceManager mssOriginServiceManager;
	
	public MssOriginServiceAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	public String getComboList(String corpID){
		
	 	 return SUCCESS;
	}
	
	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			mssOriginService = mssOriginServiceManager.get(id);
		} else {			
			mssOriginService = new MssOriginService(); 
			mssOriginService.setCreatedOn(new Date());
			mssOriginService.setUpdatedOn(new Date());
		
		} 
		return SUCCESS;
	}
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (mssOriginService.getId() == null);  
				if (isNew) { 
					mssOriginService.setCreatedOn(new Date());
		        } 
				mssOriginService.setCorpId(sessionCorpID);
				mssOriginService.setUpdatedOn(new Date());
				mssOriginService.setUpdatedBy(getRequest().getRemoteUser()); 
				mssOriginServiceManager.save(mssOriginService);  
			    String key = (isNew) ?"Mss Origin have been saved." :"Mss Origin have been saved." ;
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	public String delete(){		
		mssOriginServiceManager.remove(id);    
	  return SUCCESS;		    
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public MssOriginService getMssOriginService() {
		return mssOriginService;
	}
	public void setMssOriginService(MssOriginService mssOriginService) {
		this.mssOriginService = mssOriginService;
	}
	public void setMssOriginServiceManager(
			MssOriginServiceManager mssOriginServiceManager) {
		this.mssOriginServiceManager = mssOriginServiceManager;
	}

}
