package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.OperationsDailyLimits;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.OperationsDailyLimitsManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ResourceGridManager;

public class OperationsDailyLimitsAction extends BaseAction {

	private List dailyLimits;
	
	private OperationsDailyLimits operationsDailyLimits;
	
	private OperationsDailyLimitsManager operationsDailyLimitsManager;

	private Long id;

	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;
	
	private String hitFlag;
	
	private String hub;
	private RefMasterManager refMasterManager;
    private Map<String, String> resourceCategory;
    private String newWorkDate;
    private String resourceListServer;
	private String categoryListServer;
	private String resourceLimitListServer;
	private String resourceslist;
	private ResourceGridManager resourceGridManager;
	private Date workDate;
	private Long oId;
	private CustomerFileManager customerFileManager;
	private String operationChk;
	private String hubWarehouseLimit;
	private List serviceType;
	private String typeService="";
	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}

	public OperationsDailyLimitsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	private List<SystemDefault> sysDefaultDetail;
	public String edit() {
		serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
	    Iterator iter  =serviceType.iterator();
	
			while (iter.hasNext()) {
				if (typeService.equals("")) {
					typeService = iter.next().toString();
				} else {
					typeService = typeService + "," + iter.next().toString();
				}
			}
		
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub1);
		distinctHubDescription.putAll(house1);
		resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
	            	operationChk ="H";
	            }else{
	            	operationChk ="W";
	            }
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					hubWarehouseLimit = "Crw";
				}else{
					hubWarehouseLimit = "Tkt";
				}
			}
		}
		if (id != null) {
			operationsDailyLimits = operationsDailyLimitsManager.get(id);
			itemList=resourceGridManager.findResource(operationsDailyLimits.getHubID(), operationsDailyLimits.getWorkDate(), sessionCorpID);
		} else {
			operationsDailyLimits = new OperationsDailyLimits();
			//operationsDailyLimits.setResourceLimit("0");
			operationsDailyLimits.setCorpID(sessionCorpID);
			
			operationsDailyLimits.setCreatedOn(new Date());
			operationsDailyLimits.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}
	private ResourceGrid resourceGrid;
	private List itemList;			
	private String dummsectionName;
	private String operationDailyLimitValue;
	private BigDecimal operationDailyMaxEstWtValue;
	private BigDecimal operationDailyMaxEstVolValue;
	private String operationDailyCommentValue;
	private String opWorkDate;
	private String operationDailyCrewValue;
	public String save() throws Exception {
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub1);
		distinctHubDescription.putAll(house1);
		operationsDailyLimits.setCorpID(sessionCorpID);
		resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		operationsDailyLimits.setUpdatedBy(getRequest().getRemoteUser());
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
	            	operationChk ="H";
	            }else{
	            	operationChk ="W";
	            }
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					hubWarehouseLimit = "Crw";
				}else{
					hubWarehouseLimit = "Tkt";
				}
			}
		}
		boolean isNew = (operationsDailyLimits.getId() == null);
		List isexisted = operationsDailyLimitsManager.isExisted(operationsDailyLimits.getWorkDate(), operationsDailyLimits.getHubID(), sessionCorpID);
	
		if (!isexisted.isEmpty() && (opWorkDate!=null) && (!opWorkDate.equalsIgnoreCase("")) && (CommonUtil.multiParse(opWorkDate).compareTo(operationsDailyLimits.getWorkDate())!=0)) {
			String workDate ="";
			if (!(operationsDailyLimits.getWorkDate() == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD2.format(operationsDailyLimits.getWorkDate()));
				workDate = nowYYYYMMDD1.toString();
			}
			errorMessage("Operational Limit for date{"+workDate+"} already exist."); 
			operationsDailyLimits.setWorkDate(CommonUtil.multiParse(opWorkDate));			
			return INPUT;
		}	
		if (!isexisted.isEmpty() && isNew) {
			String workDate ="";
			if (!(operationsDailyLimits.getWorkDate() == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD2.format(operationsDailyLimits.getWorkDate()));
				workDate = nowYYYYMMDD1.toString();
			}
			errorMessage("Operational Limit for date{"+workDate+"} already exist."); 
			return INPUT;
		}
			try{
			if (isNew) {
				operationsDailyLimits.setCreatedOn(new Date());
			}
			operationsDailyLimits.setUpdatedOn(new Date());
			operationsDailyLimits=operationsDailyLimitsManager.save(operationsDailyLimits);
			
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "Operations Daily Limits added successfully" : "Operations Daily Limits updated successfully";
				saveMessage(getText(key));
			}
			
			operationsDailyLimits = operationsDailyLimitsManager.get(operationsDailyLimits.getId());
			if(operationsDailyLimits.getHubID().equalsIgnoreCase("DC-Metro")){
				List<OperationsDailyLimits> childList = operationsDailyLimitsManager.getListById(operationsDailyLimits.getId(),sessionCorpID);
				if(childList.isEmpty()){
				OperationsDailyLimits operDailyLimits = new OperationsDailyLimits() ;
				operDailyLimits.setCorpID(operationsDailyLimits.getCorpID());
				operDailyLimits.setCreatedBy(getRequest().getRemoteUser());
				operDailyLimits.setComment(operationsDailyLimits.getComment());
				operDailyLimits.setCreatedOn(new Date());
				operDailyLimits.setDailyLimits(operationsDailyLimits.getDailyLimits());
				operDailyLimits.setDailyMaxEstVol(operationsDailyLimits.getDailyMaxEstVol());
				operDailyLimits.setDailyMaxEstWt(operationsDailyLimits.getDailyMaxEstWt());
				operDailyLimits.setHubID("0");
				operDailyLimits.setWorkDate(operationsDailyLimits.getWorkDate());
				operDailyLimits.setUpdatedBy(getRequest().getRemoteUser());
				operDailyLimits.setUpdatedOn(new Date());
				operDailyLimits.setParentId(operationsDailyLimits.getId());
				operDailyLimits.setDailyCrewCapacityLimit(operationsDailyLimits.getDailyCrewCapacityLimit());
				operationsDailyLimitsManager.save(operDailyLimits);
				}else{
					for(OperationsDailyLimits oprDelLimit: childList){
						oprDelLimit.setComment(operationsDailyLimits.getComment());
						oprDelLimit.setDailyLimits(operationsDailyLimits.getDailyLimits());
						oprDelLimit.setDailyMaxEstVol(operationsDailyLimits.getDailyMaxEstVol());
						oprDelLimit.setDailyMaxEstWt(operationsDailyLimits.getDailyMaxEstWt());
						oprDelLimit.setWorkDate(operationsDailyLimits.getWorkDate());
						oprDelLimit.setUpdatedBy(getRequest().getRemoteUser());
						oprDelLimit.setUpdatedOn(new Date());
						oprDelLimit.setDailyCrewCapacityLimit(operationsDailyLimits.getDailyCrewCapacityLimit());
						operationsDailyLimitsManager.save(oprDelLimit);
					}
				}
			}
			if(resourceslist!=null && (!(resourceslist.trim().equals("")))){
					String[] str = resourceslist.split("~");
					for (String string : str) {
					String[] strRes=string.split(":");
					
					String tempCat = strRes[0].toString().trim();
					String tempRes = strRes[1].toString().trim();
					String tempQty = strRes[2].toString().trim();				
					
					resourceGrid = new ResourceGrid();
					resourceGrid.setCorpID(sessionCorpID);		        		
					resourceGrid.setCategory(tempCat);
					resourceGrid.setResource(tempRes);
					resourceGrid.setResourceLimit(new BigDecimal(tempQty));
					resourceGrid.setHubID(operationsDailyLimits.getHubID());
					resourceGrid.setWorkDate(operationsDailyLimits.getWorkDate());
					resourceGrid.setParentId(operationsDailyLimits.getId());
					resourceGrid.setCreatedBy(getRequest().getRemoteUser());
					resourceGrid.setCreatedOn(new Date());
					resourceGrid.setUpdatedBy(getRequest().getRemoteUser());
					resourceGrid.setUpdatedOn(new Date());
					resourceGrid=resourceGridManager.save(resourceGrid);
					
					if(resourceGrid.getHubID().equalsIgnoreCase("DC-Metro")){
						resourceGrid = resourceGridManager.get(resourceGrid.getId());
						ResourceGrid resGrid = new ResourceGrid();
						resGrid.setCategory(tempCat);
						resGrid.setResource(tempRes);
						resGrid.setResourceLimit(new BigDecimal(tempQty));
						resGrid.setCorpID(resourceGrid.getCorpID());
						resGrid.setHubID("0");
						resGrid.setWorkDate(resourceGrid.getWorkDate());
						resGrid.setCreatedBy(getRequest().getRemoteUser());
						resGrid.setCreatedOn(new Date());
						resGrid.setUpdatedBy(getRequest().getRemoteUser());
						resGrid.setUpdatedOn(new Date());
						resGrid.setParentId(operationsDailyLimits.getId());
						resourceGridManager.save(resGrid);
					}
				}
	      }	
			
			try{
					if(resourceListServer!=null && (!resourceListServer.equalsIgnoreCase(""))){
						String [] temp = resourceListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							ResourceGrid resObj = resourceGridManager.get(tempResId);
							resObj.setResource(resArr[1].toString().trim());
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							resourceGridManager.save(resObj);
							
							if(resObj.getHubID().equalsIgnoreCase("DC-Metro")){
								List childList = resourceGridManager.getListById(resObj.getId(),sessionCorpID);
								if(childList!=null && !childList.isEmpty()&& (childList.get(0)!=null)){
								ResourceGrid childTempObj = (ResourceGrid) childList.get(0);
								childTempObj.setResource(resArr[1].toString().trim());
								childTempObj.setUpdatedBy(getRequest().getRemoteUser());
								childTempObj.setUpdatedOn(new Date());
								resourceGridManager.save(childTempObj);
								}
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(categoryListServer!=null && (!categoryListServer.equalsIgnoreCase(""))){
						String [] temp = categoryListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							ResourceGrid resObj = resourceGridManager.get(tempResId);
							resObj.setCategory(resArr[1].toString().trim());
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							resourceGridManager.save(resObj);
							
							if(resObj.getHubID().equalsIgnoreCase("DC-Metro")){
								List childList = resourceGridManager.getListById(resObj.getId(),sessionCorpID);
								if(childList!=null && !childList.isEmpty()&& (childList.get(0)!=null)){
								ResourceGrid childTempObj = (ResourceGrid) childList.get(0);
								childTempObj.setCategory(resArr[1].toString().trim());
								childTempObj.setUpdatedBy(getRequest().getRemoteUser());
								childTempObj.setUpdatedOn(new Date());
								resourceGridManager.save(childTempObj);
								}
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(resourceLimitListServer!=null && (!resourceLimitListServer.equalsIgnoreCase(""))){
						String [] temp = resourceLimitListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							ResourceGrid resObj = resourceGridManager.get(tempResId);
							resObj.setResourceLimit(new BigDecimal(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							resourceGridManager.save(resObj);
							
							if(resObj.getHubID().equalsIgnoreCase("DC-Metro")){
								List childList = resourceGridManager.getListById(resObj.getId(),sessionCorpID);
								if(childList!=null && !childList.isEmpty()&& (childList.get(0)!=null)){
								ResourceGrid childTempObj = (ResourceGrid) childList.get(0);
								childTempObj.setResourceLimit(new BigDecimal(resArr[1].toString().trim()));
								childTempObj.setUpdatedBy(getRequest().getRemoteUser());
								childTempObj.setUpdatedOn(new Date());
								resourceGridManager.save(childTempObj);
								}
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(operationChk =="W" && hubWarehouseLimit=="Tkt"){
					if((operationDailyLimitValue==null)||(operationDailyLimitValue.equalsIgnoreCase(""))){
						operationDailyLimitValue="0";
					}	
					if((operationDailyMaxEstWtValue==null) || (operationDailyMaxEstWtValue.equals(""))){
						operationDailyMaxEstWtValue = new BigDecimal("0.00");
					}
					if((operationDailyMaxEstVolValue==null) || (operationDailyMaxEstVolValue.equals(""))){
						operationDailyMaxEstVolValue = new BigDecimal("0.00");
					}
					if((operationDailyLimitValue!=null && !operationDailyLimitValue.equalsIgnoreCase(""))){
						int tempDailyLimitOld =Integer.parseInt(operationDailyLimitValue);
						int tempDailyLimitNew = Integer.parseInt(operationsDailyLimits.getDailyLimits());
						int tempDailyLimit = tempDailyLimitOld - tempDailyLimitNew;
						tempDailyLimit = Math.abs(tempDailyLimit);
							if(tempDailyLimitOld!=tempDailyLimitNew){
								operationsDailyLimitsManager.updateDailyLimitParent(tempDailyLimit,tempDailyLimitNew,tempDailyLimitOld,sessionCorpID,operationsDailyLimits.getHubID(),operationsDailyLimits.getWorkDate());
							}
					}
					if((operationDailyMaxEstWtValue!=null && !operationDailyMaxEstWtValue.equals(""))){
						BigDecimal tempDailyMaxWtOld = operationDailyMaxEstWtValue;
						BigDecimal tempDailyMaxWtNew = operationsDailyLimits.getDailyMaxEstWt();
						BigDecimal tempDailymaxWt =  (tempDailyMaxWtOld).subtract(tempDailyMaxWtNew);
						tempDailymaxWt = tempDailymaxWt.abs();
						BigDecimal bg1, bg2;
						int res;
				        bg1 = (tempDailyMaxWtOld);
				        bg2 = (tempDailyMaxWtNew);
				        res = bg1.compareTo(bg2);
							if(res != 0){
								operationsDailyLimitsManager.updateDailyMaxEstWtParent(tempDailymaxWt,tempDailyMaxWtNew,tempDailyMaxWtOld,sessionCorpID,operationsDailyLimits.getHubID(),operationsDailyLimits.getWorkDate());
							}
					}
					if((operationDailyMaxEstVolValue!=null && !operationDailyMaxEstVolValue.equals(""))){
						BigDecimal tempDailyMaxEstVolOld = operationDailyMaxEstVolValue;
						BigDecimal tempDailyMaxEstVolNew = operationsDailyLimits.getDailyMaxEstVol();
						BigDecimal tempDailyMaxEstVol =  (tempDailyMaxEstVolOld).subtract(tempDailyMaxEstVolNew);
						tempDailyMaxEstVol = tempDailyMaxEstVol.abs();
						BigDecimal bg1, bg2;
						int res;
				        bg1 = (tempDailyMaxEstVolOld);
				        bg2 = (tempDailyMaxEstVolNew);
				        res = bg1.compareTo(bg2);
							if(res != 0){
								operationsDailyLimitsManager.updateDailyMaxEstVolParent(tempDailyMaxEstVol,tempDailyMaxEstVolNew,tempDailyMaxEstVolOld,sessionCorpID,operationsDailyLimits.getHubID(),operationsDailyLimits.getWorkDate());
							}
					}
				}
			if(operationChk =="W" && hubWarehouseLimit=="Crw"){
				if(operationDailyCrewValue==null || operationDailyCrewValue.equalsIgnoreCase("")){
					operationDailyCrewValue="0";
				}
				if(operationDailyCrewValue!=null && !operationDailyCrewValue.equalsIgnoreCase("")){
					int tempDailyCrewLimitOld =Integer.parseInt(operationDailyCrewValue);
					int tempDailyCrewLimitNew = Integer.parseInt(operationsDailyLimits.getDailyCrewCapacityLimit());
					int tempDailyCrewLimit = tempDailyCrewLimitOld - tempDailyCrewLimitNew;
					tempDailyCrewLimit = Math.abs(tempDailyCrewLimit);
					if(tempDailyCrewLimitOld!=tempDailyCrewLimitNew){
						operationsDailyLimitsManager.updateDailyCrewLimitParent(tempDailyCrewLimit,tempDailyCrewLimitNew,tempDailyCrewLimitOld,sessionCorpID,operationsDailyLimits.getHubID(),operationsDailyLimits.getWorkDate());
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
			itemList=resourceGridManager.findResource(hub, operationsDailyLimits.getWorkDate(), sessionCorpID);	
			String newWorkDate="";
			String oldWorkDate="";
			 if(operationsDailyLimits.getWorkDate()!=null){
				 SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(operationsDailyLimits.getWorkDate()));
					newWorkDate = nowYYYYMMDD1.toString();					
		            operationsDailyLimitsManager.updateWorkDate(hub,newWorkDate,operationsDailyLimits.getId(),sessionCorpID);
				}
			if (!isNew) {
				hitFlag="1";
				return INPUT;
			} else {
				hitFlag="1";
				return SUCCESS;
			}
		}
	
	private  Map<String, String> distinctHubDescription;
	private  Map<String, String> hub1;
	private  Map<String, String> house1;
	private Integer listCount ;
	@SkipValidation
	public String list() {
		resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		dailyLimits = operationsDailyLimitsManager.getDailyLimitsByHub(hub, workDate, sessionCorpID,category,resource);
		listCount = dailyLimits.size();
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub1);
		distinctHubDescription.putAll(house1);
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {	            
            if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
				hubWarehouseLimit = "Crw";
			}else{
				hubWarehouseLimit = "Tkt";
			}
		}
	 }
		return SUCCESS;
	}
		
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}
	private String category;
	private String resource;
	
	private String operationsDailyLimitsURL; 
	public String delete(){		
		hub=hub;
		oId=oId;
		resourceGridManager.remove(id);
		operationsDailyLimitsURL="?hub="+hub+"&id="+oId;
  	return SUCCESS;		    
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public List getDailyLimits() {
		return dailyLimits;
	}

	public void setDailyLimits(List dailyLimits) {
		this.dailyLimits = dailyLimits;
	}

	public OperationsDailyLimits getOperationsDailyLimits() {
		return operationsDailyLimits;
	}

	public void setOperationsDailyLimits(OperationsDailyLimits operationsDailyLimits) {
		this.operationsDailyLimits = operationsDailyLimits;
	}

	public void setOperationsDailyLimitsManager(OperationsDailyLimitsManager operationsDailyLimitsManager) {
		this.operationsDailyLimitsManager = operationsDailyLimitsManager;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}

	public String getNewWorkDate() {
		return newWorkDate;
	}

	public void setNewWorkDate(String newWorkDate) {
		this.newWorkDate = newWorkDate;
	}

	public String getResourceListServer() {
		return resourceListServer;
	}

	public void setResourceListServer(String resourceListServer) {
		this.resourceListServer = resourceListServer;
	}

	public String getCategoryListServer() {
		return categoryListServer;
	}

	public void setCategoryListServer(String categoryListServer) {
		this.categoryListServer = categoryListServer;
	}

	public String getResourceLimitListServer() {
		return resourceLimitListServer;
	}

	public void setResourceLimitListServer(String resourceLimitListServer) {
		this.resourceLimitListServer = resourceLimitListServer;
	}

	public String getResourceslist() {
		return resourceslist;
	}

	public void setResourceslist(String resourceslist) {
		this.resourceslist = resourceslist;
	}

	public void setResourceGridManager(ResourceGridManager resourceGridManager) {
		this.resourceGridManager = resourceGridManager;
	}

	public List getItemList() {
		return itemList;
	}

	public void setItemList(List itemList) {
		this.itemList = itemList;
	}

	

	public ResourceGrid getResourceGrid() {
		return resourceGrid;
	}

	public void setResourceGrid(ResourceGrid resourceGrid) {
		this.resourceGrid = resourceGrid;
	}

	public Long getoId() {
		return oId;
	}

	public void setoId(Long oId) {
		this.oId = oId;
	}

	public String getDummsectionName() {
		return dummsectionName;
	}

	public void setDummsectionName(String dummsectionName) {
		this.dummsectionName = dummsectionName;
	}

	public String getOperationsDailyLimitsURL() {
		return operationsDailyLimitsURL;
	}

	public void setOperationsDailyLimitsURL(String operationsDailyLimitsURL) {
		this.operationsDailyLimitsURL = operationsDailyLimitsURL;
	}

	public Integer getListCount() {
		return listCount;
	}

	public void setListCount(Integer listCount) {
		this.listCount = listCount;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public Map<String, String> getDistinctHubDescription() {
		return distinctHubDescription;
	}

	public void setDistinctHubDescription(Map<String, String> distinctHubDescription) {
		this.distinctHubDescription = distinctHubDescription;
	}

	public Map<String, String> getHub1() {
		return hub1;
	}

	public void setHub1(Map<String, String> hub1) {
		this.hub1 = hub1;
	}

	public Map<String, String> getHouse1() {
		return house1;
	}

	public void setHouse1(Map<String, String> house1) {
		this.house1 = house1;
	}

	public String getOperationChk() {
		return operationChk;
	}

	public void setOperationChk(String operationChk) {
		this.operationChk = operationChk;
	}

	public String getOperationDailyLimitValue() {
		return operationDailyLimitValue;
	}

	public void setOperationDailyLimitValue(String operationDailyLimitValue) {
		this.operationDailyLimitValue = operationDailyLimitValue;
	}

	public BigDecimal getOperationDailyMaxEstWtValue() {
		return operationDailyMaxEstWtValue;
	}

	public void setOperationDailyMaxEstWtValue(
			BigDecimal operationDailyMaxEstWtValue) {
		this.operationDailyMaxEstWtValue = operationDailyMaxEstWtValue;
	}

	public BigDecimal getOperationDailyMaxEstVolValue() {
		return operationDailyMaxEstVolValue;
	}

	public void setOperationDailyMaxEstVolValue(
			BigDecimal operationDailyMaxEstVolValue) {
		this.operationDailyMaxEstVolValue = operationDailyMaxEstVolValue;
	}

	public String getOperationDailyCommentValue() {
		return operationDailyCommentValue;
	}

	public void setOperationDailyCommentValue(String operationDailyCommentValue) {
		this.operationDailyCommentValue = operationDailyCommentValue;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getOpWorkDate() {
		return opWorkDate;
	}

	public void setOpWorkDate(String opWorkDate) {
		this.opWorkDate = opWorkDate;
	}

	public String getHubWarehouseLimit() {
		return hubWarehouseLimit;
	}

	public void setHubWarehouseLimit(String hubWarehouseLimit) {
		this.hubWarehouseLimit = hubWarehouseLimit;
	}

	public String getOperationDailyCrewValue() {
		return operationDailyCrewValue;
	}

	public void setOperationDailyCrewValue(String operationDailyCrewValue) {
		this.operationDailyCrewValue = operationDailyCrewValue;
	}

	public List getServiceType() {
		return serviceType;
	}

	public void setServiceType(List serviceType) {
		this.serviceType = serviceType;
	}

	public String getTypeService() {
		return typeService;
	}

	public void setTypeService(String typeService) {
		this.typeService = typeService;
	}

}