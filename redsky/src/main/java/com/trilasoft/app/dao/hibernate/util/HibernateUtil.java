package com.trilasoft.app.dao.hibernate.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.PropertyUtils;
import org.appfuse.model.User;
import org.hibernate.Query;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateUtil extends HibernateDaoSupport {
	
	public List convertScalarToComponent(String columnList, List scalarValuesList, Class beanClass) throws Exception {
		List results = new ArrayList();
		String[] attributeNames = columnList.replaceAll(" ", "").split(",");
		for (Object obj : scalarValuesList){
			Object[] objArray = (Object[])obj;
			  Object bean = beanClass.newInstance();
			  for (int j = 0; j < objArray.length; j++) {
				if (objArray[j] != null) {
					initialisePath(bean, attributeNames[j]);
					PropertyUtils.setProperty(bean, attributeNames[j], objArray[j]);
				}
			  }
			  results.add(bean);
			}		
		return results;

	}
	
	public String getParentCorpID(String sessionCorpID){
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        String userType=user.getUserType();
        String corpid="";
        if((userType.equalsIgnoreCase("PARTNER")||userType.equalsIgnoreCase("AGENT")) && (sessionCorpID.equalsIgnoreCase("TSFT"))){
        List corpidList= this.getSession().createSQLQuery("select corpID from company where networkFlag is true ").list();
        Iterator it=corpidList.iterator();
        while(it.hasNext()){
        	String newCorpid=(String)it.next();
        	if(!(corpid.lastIndexOf("'") == corpid.length() - 1))
        		corpid=corpid+"','"+newCorpid+"','";
				else if(corpid.lastIndexOf("'") == corpid.length() - 1) 
					corpid=corpid+newCorpid+"','";
			    else
			    	corpid=corpid+newCorpid+"','";
			
        } 
        if(corpid.lastIndexOf("'") == corpid.length() - 1) 
		   {
        	corpid = corpid.substring(0, corpid.length() - 1);
		    }
		if(corpid.lastIndexOf(",") == corpid.length() - 1) 
		   {
			corpid = corpid.substring(0, corpid.length() - 1);
		    } 
		if(corpid.lastIndexOf("'") == corpid.length() - 1) 
		   {
			corpid = corpid.substring(0, corpid.length() - 1);
		    }
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n   Hibernate util     "+corpid);
        return corpid;
        }else{
		return this.getSession().createSQLQuery("select parentCorpId from company where corpId ='"+sessionCorpID+"' ").list().get(0).toString();	
        }
		}catch(Exception e){
		return this.getSession().createSQLQuery("select parentCorpId from company where corpId ='"+sessionCorpID+"' ").list().get(0).toString();		
		}
        }
	
	public String getParentCorpIDForAgentAllParameter(String sessionCorpID){
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        String userType=user.getUserType();
        String corpid=""; 
		return this.getSession().createSQLQuery("select parentCorpId from company where corpId ='"+sessionCorpID+"' ").list().get(0).toString(); 
		}catch(Exception e){
		return this.getSession().createSQLQuery("select parentCorpId from company where corpId ='"+sessionCorpID+"' ").list().get(0).toString();		
		}
        }
	
	public List find(final String hqlQuery, Class beanClass) throws Exception {

		List results = new ArrayList();
		//
		// Prepare a Hibernate query
		//
		Query query = getSession().createQuery(hqlQuery);
		//
		// Determine the return type for this query
		//
		Type beanType = query.getReturnTypes()[0];
		if (beanClass == null )
			beanClass = beanType.getReturnedClass();

		String[] columns = extractColumns(hqlQuery);

		String[] attributeNames = getAttributeFieldNames(columns);
		//
		// Pre-process result field names, stripping spaces and retaining
		// alias field names instead of the original colum	n names where necessary
		//
		String[] resultFieldNames = getAttributeFieldNames(columns);
		//
		// Execute query and build result list
		//
		Iterator iter = query.iterate();
		while(iter.hasNext()) {
			  Object[] row = (Object[]) iter.next();
			  Object bean = beanClass.newInstance();
			  for (int j = 0; j < row.length; j++) {
				if (row[j] != null) {
					initialisePath(bean, attributeNames[j]);
					PropertyUtils.setProperty(bean, attributeNames[j], row[j]);
				}
			  }
			  results.add(bean);
		}
		return results;
	}

	//
	// Pre-process bean attribute names, stripping spaces 'as' clauses
	//	
	private String[] getAttributeFieldNames(String[] columns) {
		return columns;
	}

	//
	// Extract the list of columns returned by this query
	//
	private String[] extractColumns(String hqlQuery) {
		String hqlQueryUpper = hqlQuery.toUpperCase();
		return hqlQuery.substring(hqlQueryUpper.indexOf("SELECT") + 7, hqlQueryUpper.indexOf("FROM")).replaceAll(" ", "").split(",");
	}

	private static void initialisePath(final Object bean,
									   final String fieldName)
		throws Exception {
		int dot = fieldName.indexOf('.');
		while (dot >= 0) {
			String attributeName = fieldName.substring(0, dot);
			Class attributeClass = PropertyUtils.getPropertyType(bean, attributeName);
			if (PropertyUtils.getProperty(bean, attributeName) == null) {
				PropertyUtils.setProperty(bean, attributeName, attributeClass.newInstance());
			}
			dot = fieldName.indexOf('.', dot + 1);
		}
	}
	
	public String getSpecialCorpIDs(String corpID){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        String specialAccessCorpIds=user.getSpecialAccessCorpIds();
       try{
        specialAccessCorpIds=specialAccessCorpIds.replaceAll(",", "','");
		
       }catch(Exception ex){//String[] speAcessCorps=specialAccessCorpIds.split(",");
    	  System.out.println("\n\n\n\n error----->"+ex); 
       }
		return specialAccessCorpIds;
	}
}
