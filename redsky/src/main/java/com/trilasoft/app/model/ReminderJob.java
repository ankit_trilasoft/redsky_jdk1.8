package com.trilasoft.app.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.trilasoft.app.service.MessageManager;

public class ReminderJob implements Job {

	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	private MyMessage myMessage;
	private MessageManager messageManager;
	
	private String jmessage;
	private String jfromUser;
	private String jtoUser;
	//private Date jsentOn;
	private String jsubject;
	
	
	public void setMessageManager(MessageManager messageManager) {
        this.messageManager = messageManager;
    }
    public MyMessage getMyMessage() {   
        return myMessage;   
    }   

    public void setMyMessage(MyMessage myMessage) {   
        this.myMessage = myMessage;   
    } 

	
	public void execute(JobExecutionContext context) throws JobExecutionException {
	
	try {
		appCtx = getApplicationContext(context);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	JobDataMap jdMap = context.getJobDetail().getJobDataMap();
    jmessage = jdMap.getString("message");
    jtoUser = jdMap.getString("toUser");
    jfromUser = jdMap.getString("fromUser");
    jsubject = jdMap.getString("subject");
    myMessage = new MyMessage();
	myMessage.setMessage(jmessage);
	myMessage.setFromUser(jfromUser);
	myMessage.setToUser(jtoUser);
	myMessage.setSubject(jsubject);
	myMessage.setStatus("UnRead");
	MessageManager mm = (MessageManager) appCtx.getBean("messageManager");
	mm.save(myMessage);
//	 place rest of your Job code here
	//System.out.println("EXECUTING QUARTZ JOB");
	}

	private ApplicationContext getApplicationContext(JobExecutionContext context )
	throws Exception {
	ApplicationContext appCtx = null;
	appCtx = (ApplicationContext)context.getScheduler().getContext().get(APPLICATION_CONTEXT_KEY);
	if (appCtx == null) {
	throw new JobExecutionException(
	"No application context available in scheduler context for key \"" + APPLICATION_CONTEXT_KEY + "\"");
	}
	return appCtx;
	}
	}
