<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>All Pricing List</title> 
<meta name="heading" content="All Pricing List"/>
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;margin-top:-17px;!margin-top:-20px;padding:2px 0px;text-align:right;width:100%;}

.headtab_bg_center {background:url("images/headtab_bg.png") repeat-x scroll 0 0 #BDDFF3;cursor:pointer;height:21px;text-align:left;width:75%;color:#484848;cursor:pointer;
font:bold 12px Arial,Helvetica,sans-serif;}
#priceinfotab_bg{background:#ffffff url(images/price_tab_bg.png) repeat-x;height:26px; !width:70%; width:90%; color: #3A8AB3;font:13px Arial, Helvetica, sans-serif ; font-weight:bold; cursor: pointer;padding:0px; border:1px solid #5FA0C0; border-bottom:none;}
.inner-tab{background:#ffffff url(images/basic-inner.png) repeat-x; height:35px;}
.inner-tab a, a:link a:active {color:#FFFFFF;text-decoration:none;font-weight:bold;font-family:arial,verdana; font-size:11px;}
.price_tleft{float:left;padding:3px 2px 0px 8px;!margin-top:0px;}
.filter-head{color: #484848;font:11px arial,verdana ; font-weight:bold; cursor: pointer; }
.price-bg{background-image:url(images/ng-bg.png); background-repeat:repeat-x;margin:0px;padding:0px;}
.filter-bg{background-image:none; background-repeat:no-repeat; text-align:left;margin:3px; height:77px; width:982px;border:1px dotted #3dafcb;border-radius:3px;}
</style>
</head>
<s:form id="agentPricingList" name="agentPricingList" action='${empty param.popup?"agentPricingSearch.html":"agentPricingSearch.html?decorator=popup&popup=true"}' method="post" validate="true">   
<s:hidden key="pricingControl.isOrigin"/>
<s:hidden key="pricingControl.isDestination"/>
<s:hidden key="pricingControl.isFreight"/>

<div id="Layer1" style="width:100%;">	 
	 <div id="newmnav" style="!margin-bottom:5px;float:left;">
			  <ul>
			 	 	<li><a href="agentPricingBasicInfo.html?id=${pricingControl.id}"><span>Basic Info</span></a></li>
				  	<li><a onclick="return checkOriginScope();" href="agentPricingOriginInfo.html?id=${pricingControl.id}"><span>Origin Options</span></a></li>
				  	<li><a onclick="return checkFreightScope();" href="agentPricingFreightInfo.html?id=${pricingControl.id}"><span>Freight Options</span></a></li>
				  	<li><a onclick="return checkDestinationScope();" href="agentPricingDestinationInfo.html?id=${pricingControl.id}"><span>Destination Options</span></a></li>
				  	<li><a href="agentPricingSummaryInfo.html?id=${pricingControl.id}"><span>Pricing Summary</span></a></li>
				  	<li id="newmnav1" ><a class="current"><span>Pricing Options<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  	<li><a href="agentPricingList.html"><span>Quote List</span></a></li>
				<sec-auth:authComponent componentId="module.tab.pricingWizard.accountingTab">
				 <c:if test="${not empty pricingControl.serviceOrderId}">
				  	<li><a onclick="return checkControlFlag();"><span>Accounting</span></a></li>
			   	</c:if>	
			  </sec-auth:authComponent> 
		  </ul>
			</div>
			<div class="spn">&nbsp;</div>

	<div  onClick="javascript:animatedcollapse.toggle('originagent')" style="margin: 0px">
	      				<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
						<tr>
						<td class="headtab_left">
						</td>
						<td NOWRAP class="headtab_center">&nbsp;&nbsp;Origin Agent Filters
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center"><span style="padding-left:247px;">Destination Agent Filters</span>
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
	</div>
<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width:100%;margin:0px;padding:0px;" >
  <tbody>
  <tr>
  <td colspan="2" align="left" valign="top" height="82" class="price-bg">
  <div class="filter-bg">
  <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;margin-top:0px;!margin-top:0px;">
  <tr>  
  <td rowspan="4" width="10" align="center">   
  </td> 
  
  <td align="left" valign="top">
  <table border="0" style="margin:0px;padding:0px;margin-top:2px;!margin-top:0px;">
  <tr> 
   <td colspan="2">
   <table border="0" style="margin:0px;padding:0px;">
   <tr>
   <sec-auth:authComponent componentId="module.checkBox.pricingWizard.markUp">
   <c:set var="ischecked" value="false"/>
	<c:if test="${originMarkup=='true'}">
		<c:set var="ischecked" value="true"/>
	</c:if>
   <td align="center" class="listwhitetext" valign="middle" style="font-size:13px;">Price Markup</td>	
   <td class="listwhitetext" valign="bottom"><s:checkbox key="originMarkup"  fieldValue="true" value="${ischecked}" onclick=""/></td>
 	</sec-auth:authComponent>
  </tr>
   </table>   
   </td>
  
  <td align="right" class="listwhitetext">FIDI</td>
  <td align="left" class="listwhitetext"><s:checkbox key="originFIDI" fieldValue="true" onclick=""/></td> 
  
  <td align="right" class="listwhitetext">OMNI</td>
  <td align="left" class="listwhitetext"><s:checkbox key="originOMNI" fieldValue="true" onclick=""/></td>
  
   <td align="left" colspan="6">
   <table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
   <tr>
   <td align="left" class="listwhitetext">Minimum Feedback Rating&nbsp;</td>
   <td><s:select cssClass="list-menu" name="originMinimumFeedbackRating" list="{'50','60','70','80','90','100'}" headerKey="" headerValue="" cssStyle="width:50px" onchange=";"/></td>
   </tr>
   </table>
  
   </td>
   
   </tr>
   
   <tr>  
  <td width="20"></td>
  <td align="left" class="filter-head">Certifications:&nbsp;</td>
  
  <td align="right" class="listwhitetext">FAIM ISO</td>
  <td class="listwhitetext"><s:checkbox key="originFAIM"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">ISO 9002</td>
  <td class="listwhitetext"><s:checkbox key="originISO9002"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">ISO 27001</td>
  <td class="listwhitetext"><s:checkbox key="originISO27001" fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">ISO 14000</td>
  <td class="listwhitetext"><s:checkbox key="originISO14000"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">RIM</td>
  <td class="listwhitetext"><s:checkbox key="originRIM"  fieldValue="true" onclick=""/></td>
  
   </tr>
   
   <!--<tr>  
  <td width="20"></td>
  <td align="left" class="filter-head">Service Lines:&nbsp;</td>
  
  <td align="right" class="listwhitetext">DOS</td> 
  <td class="listwhitetext"><s:checkbox key="originDOS"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">GSA</td>
  <td class="listwhitetext"><s:checkbox key="originGSA" fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">Military</td>
  <td class="listwhitetext"><s:checkbox key="originMilitary"  fieldValue="true" onclick=""/></td>
  </tr>
  --></table>
  </td>
  
 
  <td valign="top" class="vertlinedata_vert" style="height:77px;" width="10" rowspan="4"></td>
  
  
  <td valign="top">
  <table border="0" style="margin:0px;padding:0px;margin-top:2px;!margin-top:0px;">
  <tr> 
   <td colspan="2">
   <table border="0" style="margin:0px;padding:0px;">
   <tr>
   <sec-auth:authComponent componentId="module.checkBox.pricingWizard.markUp">
   <c:set var="ischecked" value="false"/>
	<c:if test="${destinationMarkup=='true'}">
		<c:set var="ischecked" value="true"/>
	</c:if>
   <td align="center" class="listwhitetext" valign="middle" style="font-size:13px;">Price Markup</td>	
   <td class="listwhitetext" valign="bottom"><s:checkbox key="destinationMarkup"  fieldValue="true" value="${ischecked}" onclick=""/></td>
 	</sec-auth:authComponent>
  </tr>
   </table>   
   </td>
  
  <td align="right" class="listwhitetext">FIDI</td>
  <td align="left" class="listwhitetext"><s:checkbox key="destinationFIDI" fieldValue="true" onclick=""/></td> 
  
  <td align="right" class="listwhitetext">OMNI</td>
  <td align="left" class="listwhitetext"><s:checkbox key="destinationOMNI" fieldValue="true" onclick=""/></td>
  
   <td align="left" colspan="6">
   <table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
   <tr>
   <td align="left" class="listwhitetext">Minimum Feedback Rating&nbsp;</td>
   <td><s:select cssClass="list-menu" name="destinationMinimumFeedbackRating" list="{'50','60','70','80','90','100'}" headerKey="" headerValue="" cssStyle="width:50px" onchange=";"/></td>
   </tr>
   </table>
  
   </td>
   
   </tr>
   
   <tr>  
  <td width="20"></td>
  <td align="left" class="filter-head">Certifications:&nbsp;</td>
  
  <td align="right" class="listwhitetext">FAIM ISO</td>
  <td class="listwhitetext"><s:checkbox key="destinationFAIM"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">ISO 9002</td>
  <td class="listwhitetext"><s:checkbox key="destinationISO9002"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">ISO 27001</td>
  <td class="listwhitetext"><s:checkbox key="destinationISO27001" fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">ISO 14000</td>
  <td class="listwhitetext"><s:checkbox key="destinationISO14000"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">RIM</td>
  <td class="listwhitetext"><s:checkbox key="destinationRIM"  fieldValue="true" onclick=""/></td>
  
  <c:set var="isDTHCchecked" value="false"/>
	<c:if test="${DTHC=='true'}">
		<c:set var="isDTHCchecked" value="true"/>
	</c:if>
  <td align="right" class="listwhitetext" style="font-size:11px;">DTHC</td> 
  <td class="listwhitetext"><s:checkbox key="DTHC"  fieldValue="true" value="${isDTHCchecked}" onclick=""/></td>
  
   </tr>
   
   <tr>  
 	
  
  <!--<td align="right" class="listwhitetext">DOS</td> 
  <td class="listwhitetext"><s:checkbox key="destinationDOS"  fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">GSA</td>
  <td class="listwhitetext"><s:checkbox key="destinationGSA" fieldValue="true" onclick=""/></td>
  
  <td align="right" class="listwhitetext">Military</td>
  <td class="listwhitetext"><s:checkbox key="destinationMilitary"  fieldValue="true" onclick=""/></td>
  --></tr>
  </table>
  </td>
   </tr>
   
  </table>
	</div>
  </td>
  </tr>
  </tbody>
  </table>
  
  
  <table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;width:900px;">  
   <tr>
   <td height="10px"></td>  
   </tr>
   <tr>
   <td width="90px"></td>  
  <td align="right">
  <input type="button" class="cssbutton1" value="Apply Filters" style="width:85px;" onclick="applyFilters();"/>
  
  </td>
  <td width="50px"></td>  
  <td>
  <input type="button" class="cssbutton1" value="Reset Filters" style="width:85px;" onclick="reset_fields();"/>
  </td>  
  </tr>
  <tr>
   <td height="10px"></td>  
   </tr>
   </table>
  
  


<div  onClick="javascript:animatedcollapse.toggle('pricing')" style="margin:0px; !margin:5px;">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Pricing Options
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
			<div id="content" align="center">
<div id="liquid-round-top">
<div class="top" style="margin-top: 2px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">	
   
   
   
	<table class="detailTabLabel" style="margin:0px;padding:0px;width:100%;" border="0">
	<tbody>
	<tr>
                  
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin:0px;padding:0px;">
                	<tr>
               
               		<td>
						<display:table name="agentMasterList" class="table" requestURI="" id="agentMasterListId" export="true" defaultsort="12" defaultorder="ascending" pagesize="200" style="width:100%" >   
						<display:column  group="1" sortable="true" title="Origin Agent">
							<a  onclick="openPartnerProfile('${agentMasterListId.originPartnerId}');"><c:out value="${agentMasterListId.originAgent}" /></a>
						</display:column>
						<display:column sortable="true" title="Activity/ Feedback%">
					   		<c:out value="${agentMasterListId.originAgentActivity}" />/
					   		<c:out value="${agentMasterListId.originAgentFeedback}" />%
					   	</display:column>
					   	
					   	<display:column property="originPort"  sortable="true" title="Origin Port"/>
					   
					   <display:column  sortable="true" title="Origin"  style="list-style: circle; text-align: right" format="{0,number,0,000.00}">
				 			<fmt:formatNumber type="number" value="${agentMasterListId.originCost}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
			      		</display:column>
					   
					   	<display:column property="freightCarrier"  sortable="true" title="Carrier"/>
					   
					   <display:column  sortable="true" title="Freight"  style="list-style: circle; text-align: right" format="{0,number,0,000.00}">
				 			<fmt:formatNumber type="number" value="${agentMasterListId.freightCost}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
			      		</display:column>
						
						<display:column   sortable="true" title="Destination Agent">
					   <a  onclick="openPartnerProfile('${agentMasterListId.destinationPartnerId}');"><c:out value="${agentMasterListId.destinationAgent}" /></a>
					   </display:column>
					   	<display:column sortable="true" title="Activity/ Feedback%">
					   		<c:out value="${agentMasterListId.destinationAgentActivity}" />/ 
					   		<c:out value="${agentMasterListId.destinationAgentFeedback}" />%
					   	</display:column>
					  
					   	<display:column property="destinationPort"  sortable="true" title="Destination Port"/>
					    	
					   	 <display:column  sortable="true" title="Destination"  style="list-style: circle; text-align: right" format="{0,number,0,000.00}">
				 			<fmt:formatNumber type="number" value="${agentMasterListId.destinationCost}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
			      		</display:column>
			      		
			      		<display:column  sortable="Discount" title="Discount"  style="list-style: circle; text-align: right" format="{0,number,0,000.00}">
				 			<fmt:formatNumber type="number" value="${agentMasterListId.discountvalue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
			      		</display:column>
					   	
					   	<display:column property="netCost"  sortable="true" title="Net Cost" format="{0,number,0,000.00}"/>
					   
					   	<display:column   title="Select" style="width:10px">
					 			<c:if test="${agentMasterListId.originSelection=='true' }">
					 			<input style="vertical-align:bottom;" type="radio" name="radioFreight" checked="checked" onclick="selectRespectiveRecords('${agentMasterListId.originAgentId}','${agentMasterListId.destinationAgentId}','${agentMasterListId.freightId}')"/>
					 			</c:if>
					 			<c:if test="${agentMasterListId.originSelection!='true' }">
					 			<input style="vertical-align:bottom;" type="radio" name="radioFreight" onclick="selectRespectiveRecords('${agentMasterListId.originAgentId}','${agentMasterListId.destinationAgentId}','${agentMasterListId.freightId}')"/>
					 			</c:if>
				 		</display:column>
					   	</display:table>				
					</td>
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                </div></td>
            </tr>
</tbody>
       </table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</s:form>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>

<script type="text/javascript">
animatedcollapse.addDiv('originagent', 'fade=1,hide=1')
animatedcollapse.addDiv('destinationagent', 'fade=1,hide=1')
animatedcollapse.addDiv('pricing', 'fade=1,hide=1')
animatedcollapse.init()
</script>

<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>

<script language="javascript" type="text/javascript">

function checkOriginScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].value;
if(destinationScope!='true')
{
	alert('Origin information not provided.');
	return false;
}

}

function checkDestinationScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isDestination'].value;
if(destinationScope!='true')
{
	alert('Destination information not provided.');
	return false;
}

}
function checkFreightScope(){
var freightScope=document.forms['agentPricing'].elements['pricingControl.isFreight'].value;
if(freightScope!='true')
{
	alert('Origin and/or Destination Ports information not provided.');
	return false;
}
}


function selectRespectiveRecords(originAgentId, destinationAgentId, freightId){
   	var pricingControlID = '${pricingControl.id}';
    var url="updateRespectiveRecords.html?ajax=1&decorator=simple&popup=true&originAgentRecordId="+originAgentId+"&destinationAgentRecordId="+destinationAgentId+"&freightRecordId="+freightId+"&pricingControlID="+pricingControlID;
	http2.open("GET", url, true);
  	http2.send(null);
    
    }


function checkControlFlag(){
	var id= '${pricingControl.id}';
	var sequenceNumber='${pricingControl.sequenceNumber}';
	var url="checkControlFlag.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" +sequenceNumber;
	alert(url);
	http2.open("GET", url, true);
  	http2.onreadystatechange = handleHttpResponseCheckControlFlag;
   	http2.send(null);
	
}
   
function handleHttpResponseCheckControlFlag(){
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results=='C'){
                var sid='${pricingControl.serviceOrderId}';
                	location.href='accountLineList.html?sid='+sid;
                	return true;
                }else{
                	alert('Quote is still not accepted');
                	return false;
                }
             }
}

function applyFilters(){
	document.forms['agentPricingList'].action='agentPricingAllPricingInfo.html?id='+${pricingControl.id};
	document.forms['agentPricingList'].submit();
	
}
  
String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();
    
     function reset_fields(){
   	document.forms['agentPricingList'].elements['originFIDI'].checked = false;
	document.forms['agentPricingList'].elements['originOMNI'].checked = false;
	document.forms['agentPricingList'].elements['originMinimumFeedbackRating'].value = "";
	document.forms['agentPricingList'].elements['originFAIM'].checked = false;
	document.forms['agentPricingList'].elements['originISO9002'].checked = false;
	document.forms['agentPricingList'].elements['originISO27001'].checked = false;
	document.forms['agentPricingList'].elements['originISO14000'].checked = false;
	document.forms['agentPricingList'].elements['originRIM'].checked = false;

	
	document.forms['agentPricingList'].elements['destinationFIDI'].checked = false;
	document.forms['agentPricingList'].elements['destinationOMNI'].checked = false;
	document.forms['agentPricingList'].elements['destinationMinimumFeedbackRating'].value = "";
	document.forms['agentPricingList'].elements['destinationFAIM'].checked = false;
	document.forms['agentPricingList'].elements['destinationISO9002'].checked = false;
	document.forms['agentPricingList'].elements['destinationISO27001'].checked = false;
	document.forms['agentPricingList'].elements['destinationISO14000'].checked = false;
	document.forms['agentPricingList'].elements['destinationRIM'].checked = false;

	document.forms['agentPricingList'].elements['originMarkup'].checked=true;
	document.forms['agentPricingList'].elements['destinationMarkup'].checked=true;
	
	
	
   applyFilters();
   }
   
   function openPartnerProfile(partnerId){
   
   window.open('findPartnerProfileList.html?id='+partnerId+'&decorator=popup&popup=true','profile','scrollbars=1,left=20,top=100,resizable=yes,width=1000,height=450');
   
   }

</script>