

delimiter $$

CREATE  trigger redsky.trigger_add_history_user_role_add
after update on redsky.user_role

for each row

BEGIN

  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE oldCorpid LONGTEXT;
  DECLARE newUser LONGTEXT;
  DECLARE newRoleName LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET oldCorpid = " ";
  SET newUser = " ";
  SET newRoleName = " ";

select corpid into oldCorpid from role where id=NEW.role_id limit 1;
if(old.updatedby is null and new.updatedby is not null) then
IF (NEW.role_id <> '' or NEW.role_id is not null) THEN
    select name into newRoleName from role where id=NEW.role_id;
       select CONCAT(fieldNameList,'user_role.role_id~') into fieldNameList;
       select CONCAT(newValueList,newRoleName,'~') into newValueList;
   END IF;
 END IF;
CALL add_tblHistory (NEW.user_id,"user_role", fieldNameList, oldValueList, newValueList, NEW.updatedby, oldCorpid, now());
END $$
delimiter;