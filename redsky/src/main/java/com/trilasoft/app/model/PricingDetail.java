package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="pricingdetail")
public class PricingDetail extends BaseObject{
	private Long id;
    private String corpID;
    private String createdBy;
	private String updatedBy;
    private Date updatedOn;
	private Date createdOn;
	private String sequenceNumber;
	private String shipNumber;
	private String rateType;
	private String description;
	private BigDecimal qty;
	private BigDecimal rate;
	private BigDecimal discount;
	private String discountType;
	private BigDecimal charge;
	private String displayIndic;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn)
				.append("sequenceNumber", sequenceNumber)
				.append("shipNumber", shipNumber).append("rateType", rateType)
				.append("description", description).append("qty", qty)
				.append("rate", rate).append("discount", discount)
				.append("discountType", discountType).append("charge", charge)
				.append("displayIndic", displayIndic).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PricingDetail))
			return false;
		PricingDetail castOther = (PricingDetail) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(shipNumber, castOther.shipNumber)
				.append(rateType, castOther.rateType)
				.append(description, castOther.description)
				.append(qty, castOther.qty).append(rate, castOther.rate)
				.append(discount, castOther.discount)
				.append(discountType, castOther.discountType)
				.append(charge, castOther.charge)
				.append(displayIndic, castOther.displayIndic).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(sequenceNumber).append(shipNumber)
				.append(rateType).append(description).append(qty).append(rate)
				.append(discount).append(discountType).append(charge)
				.append(displayIndic).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=3)
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	@Column(length=50)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	@Column
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	@Column
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@Column(length=3)
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	@Column
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	@Column(length=1)
	public String getDisplayIndic() {
		return displayIndic;
	}
	public void setDisplayIndic(String displayIndic) {
		this.displayIndic = displayIndic;
	}
}