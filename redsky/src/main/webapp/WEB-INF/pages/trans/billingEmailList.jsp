 <%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
</head> 
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<style>

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-32.5px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style>

<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	


<s:form name="billingPopupForm" method="post" >	
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>

<div id="layer1" style="width:100%">
<div id="otabs">
	<ul>
		<li><a class="current"><span>Users</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div> 

<div id="newmnav">   
</div><div class="spnblk" style="width:800px">&nbsp;</div>
<s:set name="billingEmailList" value="billingEmailList"/>   
<display:table name="billingEmailList" class="table" requestURI="" id="billingEmailList" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >
	<c:if test="${empty param.popup}">  
		<display:column property="userName" sortable="true" title="User Name"
		href="editBilling.html" paramId="billingEmail" paramProperty="billing.billingEmail" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" title="User Name" titleKey="billingEmailList.firstName"/>   
    </c:if>
    	<display:column property="firstName" sortable="true" title="First Name" paramId="billingEmail" paramProperty="billing.billingEmail"/>
    
    	<display:column property="lastName" sortable="true" title="Last Name" paramId="billingEmail" paramProperty="billing.billingEmail"/>
    
    	<display:column property="email" sortable="true" title="Email" paramId="billingEmail" paramProperty="billing.billingEmail"/>
</display:table>
</div>
</div> 
</s:form>