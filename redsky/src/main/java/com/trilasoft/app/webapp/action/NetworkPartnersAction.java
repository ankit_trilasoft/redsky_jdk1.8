package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.NetworkPartnersDaoHibernate.NetworkPartnerByCompDivDTO;
import com.trilasoft.app.model.NetworkPartners;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.NetworkPartnersManager;

public class NetworkPartnersAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private List corpidList;
	private List networkPartnerList;
	private NetworkPartnersManager networkPartnersManager;
	private String selectedCorpId;
	private Integer networkPartnerListSize;
	private Integer countByCorpId;
	private Integer countCompanyDivision;
	private List maintainNetworkPartnerList;
	private NetworkPartners networkPartners;
	private String hitFlag;
	public NetworkPartnersAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
    public void prepare() throws Exception { 
		
	}
	
    public String  findNetworkCompany(){
    	corpidList=networkPartnersManager.getCorpidList(sessionCorpID);
    	return SUCCESS;	
    }
    
    public String findNetworkPartnerList(){
    	networkPartnerList=networkPartnersManager.findNetworkPartnerList(sessionCorpID,selectedCorpId);
    	networkPartnerListSize=networkPartnerList.size();
    	if(networkPartnerListSize==0){
    		saveMessage("No record exist for CorpId "+selectedCorpId); 
    		return INPUT;
    	}else{
    		return SUCCESS;	 	
    	}
        	
    }
    public String maintainNetworkPartnerList(){
    	countByCorpId = networkPartnersManager.countNetworkPartnerByCorpId(sessionCorpID,selectedCorpId);
    	countCompanyDivision = networkPartnersManager.countCompanyDivisionByCorpId(sessionCorpID,selectedCorpId);
    	if(countByCorpId < countCompanyDivision){
    		networkPartnersManager.removeByCorpId(sessionCorpID,selectedCorpId);
    	    List list=networkPartnersManager.findByCorpId(sessionCorpID,selectedCorpId); 
    	    Iterator it = list.iterator();
    	    while(it.hasNext())
			{		
    	    	NetworkPartnerByCompDivDTO networkPartnerByCompDivDTO=(NetworkPartnerByCompDivDTO)it.next();
    	    	NetworkPartners networkPartners=new NetworkPartners();
    	    	networkPartners.setCorpId(networkPartnerByCompDivDTO.getCorpId().toString()); 
    	    	networkPartners.setAgentCorpId(networkPartnerByCompDivDTO.getAgentCorpId().toString());
    	    	networkPartners.setAgentPartnerCode(networkPartnerByCompDivDTO.getAgentPartnerCode().toString());
    	    	networkPartners.setAgentLocalPartnerCode(networkPartnerByCompDivDTO.getAgentLocalPartnerCode().toString());
    	    	networkPartners.setAgentCompanyDivision(networkPartnerByCompDivDTO.getAgentCompanyDivision().toString());
    	    	networkPartners.setAgentName(networkPartnerByCompDivDTO.getAgentName().toString());
    	    	networkPartnersManager.save(networkPartners);
			}
    	    networkPartnerList=networkPartnersManager.findNetworkPartnerList(sessionCorpID,selectedCorpId);
    	    return SUCCESS; 
    	}
    	else{
    		saveMessage("Records existed for selected CorpId "+selectedCorpId); 
    		return INPUT;	
    	}  	
    }
    
    public String save(){
    	if(id!=null){
    		networkPartners= networkPartnersManager.get(id);   		
    	    networkPartners.setCreatedOn(new Date());
    	    networkPartners.setCreatedBy(getRequest().getRemoteUser());
    		networkPartnersManager.save(networkPartners);
    	}else{
    		networkPartnersManager.save(networkPartners);
    		hitFlag="1";
    	}
    	return SUCCESS;
    }
    
    public String list(){
    	networkPartnerList=networkPartnersManager.findNetworkPartnerList(sessionCorpID,selectedCorpId);
     	return SUCCESS;
    }
    
    public String edit(){
    	if(id!=null){
    		networkPartners= networkPartnersManager.get(id);   		
    	}else{
    		networkPartners=new NetworkPartners();
    		networkPartners.setCorpId(sessionCorpID);
    		networkPartners.setAgentCorpId(selectedCorpId);
    		networkPartners.setCreatedOn(new Date());
     	    networkPartners.setCreatedBy(getRequest().getRemoteUser());
    	}
    	 return SUCCESS;
    }
    
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setNetworkPartnersManager(
			NetworkPartnersManager networkPartnersManager) {
		this.networkPartnersManager = networkPartnersManager;
	}
	public List getCorpidList() {
		return corpidList;
	}
	public void setCorpidList(List corpidList) {
		this.corpidList = corpidList;
	}
	public List getNetworkPartnerList() {
		return networkPartnerList;
	}
	public void setNetworkPartnerList(List networkPartnerList) {
		this.networkPartnerList = networkPartnerList;
	}
	public String getSelectedCorpId() {
		return selectedCorpId;
	}
	public void setSelectedCorpId(String selectedCorpId) {
		this.selectedCorpId = selectedCorpId;
	}
	public Integer getNetworkPartnerListSize() {
		return networkPartnerListSize;
	}
	public void setNetworkPartnerListSize(Integer networkPartnerListSize) {
		this.networkPartnerListSize = networkPartnerListSize;
	}
	
	public Integer getCountByCorpId() {
		return countByCorpId;
	}
	public void setCountByCorpId(Integer countByCorpId) {
		this.countByCorpId = countByCorpId;
	}
	
	public List getMaintainNetworkPartnerList() {
		return maintainNetworkPartnerList;
	}
	public void setMaintainNetworkPartnerList(List maintainNetworkPartnerList) {
		this.maintainNetworkPartnerList = maintainNetworkPartnerList;
	}
	public NetworkPartners getNetworkPartners() {
		return networkPartners;
	}
	public void setNetworkPartners(NetworkPartners networkPartners) {
		this.networkPartners = networkPartners;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public Integer getCountCompanyDivision() {
		return countCompanyDivision;
	}
	public void setCountCompanyDivision(Integer countCompanyDivision) {
		this.countCompanyDivision = countCompanyDivision;
	}
	
}
