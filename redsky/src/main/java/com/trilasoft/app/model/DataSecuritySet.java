package com.trilasoft.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.appfuse.model.LabelValue;
import org.appfuse.model.Role;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="datasecurityset")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DataSecuritySet extends BaseObject{
	private Long id;
	private String name;
	private String description;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	protected Set<DataSecurityFilter> dataSecurityFilters=new HashSet<DataSecurityFilter>();
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name)
				.append("description", description).append("corpID", corpID).append("createdBy",createdBy)
				.append("updatedBy",updatedBy).append("createdOn",createdOn).append("updatedOn",updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DataSecuritySet))
			return false;
		DataSecuritySet castOther = (DataSecuritySet) other;
		return new EqualsBuilder().append(id, castOther.id).append(name,
				castOther.name).append(description, castOther.description)
				.append(corpID, castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(name)
				.append(description).append(corpID).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToMany(fetch = FetchType.EAGER) 
    @JoinTable(
            name="datasecuritypermission",
            joinColumns = { @JoinColumn( name="datasecurityset_id") },
            inverseJoinColumns = @JoinColumn( name="datasecurityfilter_id")
            
    )    
    public Set<DataSecurityFilter> getDataSecurityFilters() {
        return dataSecurityFilters;
    }
	public void setDataSecurityFilters(Set<DataSecurityFilter> dataSecurityFilters) {
		this.dataSecurityFilters = dataSecurityFilters;
	}
	
	@Transient
    public List<LabelValue> getFilterList() {
        List<LabelValue> filters = new ArrayList<LabelValue>();

        if (this.dataSecurityFilters != null) {
            for (DataSecurityFilter dataSecurityFilter : dataSecurityFilters) {
                // convert the user's roles to LabelValue Objects
            	filters.add(new LabelValue(dataSecurityFilter.getName(), dataSecurityFilter.getName()));
            }
        }

        return filters;
    }

    
    public void addFilters(DataSecurityFilter dataSecurityFilter) {
    	//System.out.println("\n\n\n\n\n dataSecurityFilter--->"+dataSecurityFilter);
    	getDataSecurityFilters().add(dataSecurityFilter);
    }
    @Column
	public String getCreatedBy() {
		return createdBy;
	}
    @Column
	public String getUpdatedBy() {
		return updatedBy;
	}
    @Column
	public Date getCreatedOn() {
		return createdOn;
	}
    @Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

     

    
}
