package com.trilasoft.app.model;

import java.util.Date;

public class AvailableCrew {

	private Long id;
	private String lastName;
	private String firstName;
	private String warehouse;
	private String typeofWork;
	private String availableHours;
	private String isUnioun;
	private String doneForTheDay;
	private String crewName;
	private Date workDate;
	private String ticket;
	private String shipper;
	private String action;
	private String crewType; 
	private String calculationCode;
	private String distributionCode;
	private String regularHours;
	private String paidRevenueShare;
	private String overTime;
	private String doubleOverTime;
	private String beginHours;
	private String endHours;
	private String regularPayPerHour;
	private Boolean isCrewUsed;
	private String userName;
	private String integrationTool;
	private String deviceNumber;
	private String drivingClass;
	
	public String getAvailableHours() {
		return availableHours;
	}
	public void setAvailableHours(String availableHours) {
		this.availableHours = availableHours;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTypeofWork() {
		return typeofWork;
	}
	public void setTypeofWork(String typeofWork) {
		this.typeofWork = typeofWork;
	}
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	public String getIsUnioun() {
		return isUnioun;
	}
	public void setIsUnioun(String isUnioun) {
		this.isUnioun = isUnioun;
	}
	public String getDoneForTheDay() {
		return doneForTheDay;
	}
	public void setDoneForTheDay(String doneForTheDay) {
		this.doneForTheDay = doneForTheDay;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getCalculationCode() {
		return calculationCode;
	}
	public void setCalculationCode(String calculationCode) {
		this.calculationCode = calculationCode;
	}
	public String getCrewName() {
		return crewName;
	}
	public void setCrewName(String crewName) {
		this.crewName = crewName;
	}
	public String getCrewType() {
		return crewType;
	}
	public void setCrewType(String crewType) {
		this.crewType = crewType;
	}
	public String getDistributionCode() {
		return distributionCode;
	}
	public void setDistributionCode(String distributionCode) {
		this.distributionCode = distributionCode;
	}
	public String getDoubleOverTime() {
		return doubleOverTime;
	}
	public void setDoubleOverTime(String doubleOverTime) {
		this.doubleOverTime = doubleOverTime;
	}
	public String getOverTime() {
		return overTime;
	}
	public void setOverTime(String overTime) {
		this.overTime = overTime;
	}
	public String getPaidRevenueShare() {
		return paidRevenueShare;
	}
	public void setPaidRevenueShare(String paidRevenueShare) {
		this.paidRevenueShare = paidRevenueShare;
	}
	public String getRegularHours() {
		return regularHours;
	}
	public void setRegularHours(String regularHours) {
		this.regularHours = regularHours;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	public String getBeginHours() {
		return beginHours;
	}
	public void setBeginHours(String beginHours) {
		this.beginHours = beginHours;
	}
	public String getEndHours() {
		return endHours;
	}
	public void setEndHours(String endHours) {
		this.endHours = endHours;
	}
	public String getRegularPayPerHour() {
		return regularPayPerHour;
	}
	public void setRegularPayPerHour(String regularPayPerHour) {
		this.regularPayPerHour = regularPayPerHour;
	}
	public Boolean getIsCrewUsed() {
		return isCrewUsed;
	}
	public void setIsCrewUsed(Boolean isCrewUsed) {
		this.isCrewUsed = isCrewUsed;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIntegrationTool() {
		return integrationTool;
	}
	public void setIntegrationTool(String integrationTool) {
		this.integrationTool = integrationTool;
	}
	public String getDeviceNumber() {
		return deviceNumber;
	}
	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}
	public String getDrivingClass() {
		return drivingClass;
	}
	public void setDrivingClass(String drivingClass) {
		this.drivingClass = drivingClass;
	}
	
	
	
}
