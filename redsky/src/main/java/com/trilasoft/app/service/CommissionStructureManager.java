package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CommissionStructure;

public interface CommissionStructureManager extends GenericManager<CommissionStructure, Long>{
	public List getCommPersentAcRange(String corpId,String contract,String chargeCode);
}
