package com.trilasoft.app.webapp.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.UgwwActionTrackerManager;

public class QuickSearchAction extends BaseAction implements Preparable {

	private UgwwActionTrackerManager ugwwActionTrackerManager;
	private String sessionCorpID;
	private String usertype;
	private Boolean activeStatus;
	private Long id;
	private String searchId;
	private String cfURL;
	private String billToCode;
	private String jobNumber;
	private String code;
	private String job;
	private String reportModule;
	private String reportSubModule;
	private String formReportFlag;
	private String Status;
	private String registrationNumber;
	private String companyDivision;
	private String mode;
	private String shipNumber;
	private String currentPageLocation;
	private PartnerPublicManager partnerPublicManager;
	private Set partners;
	private String countryCodeSearch;
	private String vanlineCodeSearch;
	private String stateSearch;
	private Boolean isIgnoreInactive = false;
	private String vanlineCode;
	private String partnerType;
	private String countrySearch;
	private String sessionUserName;
	
	private List partnerRatess;
	private PartnerPrivateManager partnerPrivateManager;
	private Partner partner1;
	private PartnerManager partnerManager;
	private List findPartnerProfileList;
    private ServiceOrderManager serviceOrderManager;
	private CompanyManager companyManager;
	private Company company;
	static final Logger logger = Logger.getLogger(QuickSearchAction.class);
	private Boolean visibilityFornewAccountline=false;
	private String dashBoardHideJobsList;
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	StringBuilder systemDate = new StringBuilder(format.format(new Date()));
	Partner partner = new Partner();
	PartnerPublic partnerPublic = new PartnerPublic();
	Date currentdate = new Date();
	ServiceOrder serviceOrder = new ServiceOrder();
    CustomerFile  customerFile = new CustomerFile(); 
    PartnerPrivate partnerPrivate = new PartnerPrivate();
    private CustomerFileManager customerFileManager;
	
	public QuickSearchAction() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.sessionUserName = user.getUsername();
		usertype = user.getUserType();
		if (user.isDefaultSearchStatus()) {
			activeStatus = true;
		} else {
			activeStatus = false;
		}

	}

	public void prepare() throws Exception {

	}
private boolean checkFieldVisibility=false;
	public String checkIdType() {
		String[] suffixArray = { "db","so", "bl", "ac", "cl", "fo", "ts", "wt",
				"cf", "qf", "or", "gr", "qu", "pr","fc","uf","nt","nn" };
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if (searchId != null) {
			String str = searchId;
			 str = str.replaceAll("[;\\\\/:*?\"<>|&']", "")+"";
			
			String suffixAtBegin = str.substring(0, 2);
			String suffixAtLast = str.substring(str.length() - 2, str.length());
			if (suffixAtBegin.equalsIgnoreCase("so")|| suffixAtLast.equalsIgnoreCase("so")) {
				
				String withoutSuffix = str.replace("so", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findsodId(searchstr,
						sessionCorpID);
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?id=" + id;
					return "SoForm";
				} else if(temp.isEmpty()) {
					  serviceOrder.setShipNumber(searchstr);
					  serviceOrder.setJob(""); serviceOrder.setFirstName("");
					  serviceOrder.setLastName(""); 
					//Bug 11839 - Possibility to modify Current status to Prior status
					 serviceOrder.setStatus("");
					  serviceOrder.setJob(""); serviceOrder.setRegistrationNumber("");
					  serviceOrder.setCoordinator("");
					  serviceOrder.setSocialSecurityNumber(""); serviceOrder.setOriginCity("");
					  serviceOrder.setDestinationCity(""); serviceOrder.setGbl("");
					  serviceOrder.setBillToName("");serviceOrder.setCompanyDivision("");
					  return "SoList"; 
				}else{
					errorMessage("Please enter valid order number");
					return "serviceFormPage";
				}

			}
			else if (suffixAtBegin.equalsIgnoreCase("db")|| suffixAtLast.equalsIgnoreCase("db")) {
				 if (company != null && company.getDashBoardHideJobs() != null) {
						dashBoardHideJobsList = company.getDashBoardHideJobs();	 
					}
				String withoutSuffix = str.replace("db", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findsodId(searchstr,
						sessionCorpID);
				/*if (temp.size() == 0) {
					return "mainMenu";
				}*/
        
                 
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?sid=" + id;
					serviceOrder = serviceOrderManager.get(id);
					String permKey = sessionCorpID + "-" + "component.Dynamic.DashBoard";
					visibilityFornewAccountline = AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey);
					if (visibilityFornewAccountline) {
						if (dashBoardHideJobsList != null) {
							if (dashBoardHideJobsList.contains(serviceOrder.getJob())) {
								errorMessage("Dashboard is not valid for this order");
								return "mainMenu";

							} else {
								return "Dashboard";
							}
						} else {
							return "Dashboard";
						}

					} else {
						errorMessage("Dashboard is not valid for this CorpID " + sessionCorpID);
						return "mainMenu";

					}
				}  
                 else if(temp.isEmpty()) {
					  serviceOrder.setShipNumber(searchstr);
					  serviceOrder.setJob(""); serviceOrder.setFirstName("");
					  serviceOrder.setLastName(""); 
					//Bug 11839 - Possibility to modify Current status to Prior status
					  serviceOrder.setStatus("");
					  serviceOrder.setJob(""); serviceOrder.setRegistrationNumber("");
					  serviceOrder.setCoordinator("");
					  serviceOrder.setSocialSecurityNumber(""); serviceOrder.setOriginCity("");
					  serviceOrder.setDestinationCity(""); serviceOrder.setGbl("");
					  serviceOrder.setBillToName("");serviceOrder.setCompanyDivision("");
					  return "SoList"; 
				}else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			}else if (suffixAtBegin.equalsIgnoreCase("bl")|| suffixAtLast.equalsIgnoreCase("bl")) {
					
				String withoutSuffix = str.replace("bl", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findsodId(searchstr,
						sessionCorpID);
				/*if (temp.size() == 0) {
					return "mainMenu";
				}*/
				if (temp.size() == 1) {
				id = Long.parseLong(temp.get(0).toString());
				cfURL = "?id=" + id;
				return "BillingPage";
				}else if(temp.isEmpty()) {
					  serviceOrder.setShipNumber(searchstr);
					  serviceOrder.setJob(""); serviceOrder.setFirstName("");
					  serviceOrder.setLastName(""); 
					//Bug 11839 - Possibility to modify Current status to Prior status
					  serviceOrder.setStatus("");
					  serviceOrder.setJob(""); serviceOrder.setRegistrationNumber("");
					  serviceOrder.setCoordinator("");
					  serviceOrder.setSocialSecurityNumber(""); serviceOrder.setOriginCity("");
					  serviceOrder.setDestinationCity(""); serviceOrder.setGbl("");
					  serviceOrder.setBillToName("");serviceOrder.setCompanyDivision("");
					  return "SoList"; 
				}else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			} else if (suffixAtBegin.equalsIgnoreCase("ac")|| suffixAtLast.equalsIgnoreCase("ac")) {
					
				String withoutSuffix = str.replace("ac", "");
				String searchstr = withoutSuffix;
				Boolean accessQFromCF=company.getAccessQuotationFromCustomerFile();
				List temp = ugwwActionTrackerManager.findAccountLineId(
						searchstr, sessionCorpID, accessQFromCF);
				if (temp.size() == 0) {
					return "mainMenu";
				}
				id = Long.parseLong(temp.get(0).toString());
				cfURL = "?sid=" + id;
				String permKey = sessionCorpID +"-"+"module.tab.serviceorder.newAccountingTab";
				visibilityFornewAccountline=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
				if(visibilityFornewAccountline){
					return "NewAccountLine";	
				}else{
				return "AccountLine";
				}

			} else if (suffixAtBegin.equalsIgnoreCase("fo")|| suffixAtLast.equalsIgnoreCase("fo")) {
					
				String withoutSuffix = str.replace("fo", "");
				String searchstr = withoutSuffix;
				Boolean accessQFromCF=company.getAccessQuotationFromCustomerFile();
				List temp = ugwwActionTrackerManager.findAccountLineId(
						searchstr, sessionCorpID, accessQFromCF);
				if (temp.size() == 0) {
					return "mainMenu";
				}				
				id = Long.parseLong(temp.get(0).toString());				
				String permKey = sessionCorpID +"-"+"component.field.forwardingTabAjax";
				checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
				if(checkFieldVisibility){
					cfURL = "containersAjaxList.html?id=" + id;
				}else{
					cfURL = "containers.html?id=" + id;
				}
				serviceOrder=serviceOrderManager.get(id);
				if(serviceOrder.getJob()!=null && serviceOrder.getJob().equals("RLO")){
					return "mainMenu";
				}
				return "ForwardingPage";

			} else if (suffixAtBegin.equalsIgnoreCase("ts")	|| suffixAtLast.equalsIgnoreCase("ts")) {
				String withoutCorpID;
				/*if(str.contains(sessionCorpID.toLowerCase())){
				 str = str.replace(sessionCorpID.toLowerCase(), "");
				}*/
				String withoutSuffix=str; 
				if(withoutSuffix.startsWith("ts")){
				withoutSuffix = str.replaceFirst("ts", "");
				}
				else{
					if(suffixAtLast.equalsIgnoreCase("ts")){
					int i=	withoutSuffix.lastIndexOf("ts") ;
					withoutSuffix=withoutSuffix.substring(0, i);
					}
				}
				
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findsodId(searchstr,
						sessionCorpID);
				/*if (temp.size() == 0) {
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}*/
				if (temp.size() == 1) {
				id = Long.parseLong(temp.get(0).toString());
				cfURL = "?id=" + id;
				serviceOrder=serviceOrderManager.get(id);
				String serachString="StatusPage";
				if(serviceOrder.getJob()!=null && serviceOrder.getJob().equals("RLO")){
					serachString="dspDetailsPage";
				}
				return serachString;
				}else if(temp.isEmpty()) {
					  serviceOrder.setShipNumber(searchstr);
					  serviceOrder.setJob(""); serviceOrder.setFirstName("");
					  serviceOrder.setLastName(""); 
					//Bug 11839 - Possibility to modify Current status to Prior status
					  serviceOrder.setStatus("");
					  serviceOrder.setJob(""); serviceOrder.setRegistrationNumber("");
					  serviceOrder.setCoordinator("");
					  serviceOrder.setSocialSecurityNumber(""); serviceOrder.setOriginCity("");
					  serviceOrder.setDestinationCity(""); serviceOrder.setGbl("");
					  serviceOrder.setBillToName("");serviceOrder.setCompanyDivision("");
					  return "SoList"; 
				}else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			} else if (suffixAtBegin.equalsIgnoreCase("wt")	|| suffixAtLast.equalsIgnoreCase("wt")) {
				
				String withoutSuffix = str.replace("wt", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findsodId(searchstr,
						sessionCorpID);
				List detailList = ugwwActionTrackerManager.findTicketDetailsId(searchstr,sessionCorpID);
						
				if (detailList != null && detailList.size()>0 ) {
					id = Long.parseLong(detailList.get(0).toString());
					cfURL = "?id=" + id;
					return "TktDetailsPage";
				}
				if(temp != null && temp.size()>0){
				id = Long.parseLong(temp.get(0).toString());
				cfURL = "?id=" + id;
				return "TicketPage";
				}else{
					errorMessage("Ticket file not exist");
					return "mainMenu";
				}
			} else if (suffixAtBegin.equalsIgnoreCase("cl")|| suffixAtLast.equalsIgnoreCase("cl")) {
				String withoutSuffix = str.replace("cl", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findsodId(searchstr,sessionCorpID);
				List tempDetails = ugwwActionTrackerManager.findClaimDetailsId(searchstr,sessionCorpID);
				
				if(tempDetails!=null && tempDetails.size() > 0){
				id = Long.parseLong(tempDetails.get(0).toString());
				cfURL = "?id=" + id;
				return "clmsDetailsPage";
				}else if(temp.size() > 0){
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?id=" + id;
					return "claimsPage";
					}else{
						errorMessage("Claim File not Exist");
						return "mainMenu";
					}

			} else if (suffixAtBegin.equalsIgnoreCase("cf")	|| suffixAtLast.equalsIgnoreCase("cf")) {
				String withoutSuffix = str.replace("cf", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findId(searchstr,
						sessionCorpID);
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?from=list&id=" + id;
					return "CustomerPage";
				}else if(temp.size()>1){
					customerFile.setSequenceNumber(searchstr);
					customerFile.setLastName("");customerFile.setJob("");customerFile.setBillToName("");customerFile.setFirstName("");
					customerFile.setStatus("");customerFile.setCoordinator("");customerFile.setCompanyDivision("");
					customerFile.setStatus("");customerFile.setCompanyDivision("");customerFile.setRegistrationNumber("");
					customerFile.setOriginCity("");customerFile.setDestinationCity("");
					customerFile.setAccountName("");customerFile.setStatus("");customerFile.setCoordinator("");
					customerFile.setDestinationCity("");customerFile.setOriginCity("");
					 return "cusList"; 
				}
				errorMessage("Customer file does not exist");
				return "mainMenu";

			} else if (suffixAtBegin.equalsIgnoreCase("qf")|| suffixAtLast.equalsIgnoreCase("qf")) {
					
				String withoutSuffix = str.replace("qf", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findQuotesID(searchstr,
						sessionCorpID);
			      if (temp != null && temp.size() > 0) {
					   Object[] obj = (Object[]) temp.get(0);
					   id = Long.parseLong(obj[0].toString());
					    String flag = obj[1].toString();
					    if (flag.equalsIgnoreCase("Q")) {
						cfURL = "?from=list&id=" + id;
						return "quotationPage";
					} else {
						return "quotationsFileForm";

					}

				}
				errorMessage("Quotation File not Exist");
				return "mainMenu";
			} else if (suffixAtBegin.equalsIgnoreCase("or")|| suffixAtLast.equalsIgnoreCase("or")) {
					
				String withoutSuffix = str.replace("or", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findOrderId(searchstr,
						sessionCorpID);
				if (temp != null && temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?from=list&id=" + id;
					return "orderManagement";
				}
				if(temp != null && temp.size()> 1){
					customerFile.setLastName("");
					 customerFile.setFirstName("");
					customerFile.setSequenceNumber(searchstr);
					customerFile.setBillToAuthorization("");
					customerFile.setBillToReference(""); 
			        customerFile.setQuotationStatus(""); 
					customerFile.setOrderIntiationStatus("Submitted"); 
					return "orderList";
				
				}

				errorMessage("Please enter valid order number");
				return "mainMenu";

			} else if (suffixAtBegin.equalsIgnoreCase("gr")|| suffixAtLast.equalsIgnoreCase("gr")) {
				String withoutSuffix = str.replace("gr", "");
				String searchstr = withoutSuffix;
				return "groupage";
					

			} else if (suffixAtBegin.equalsIgnoreCase("qu")|| suffixAtLast.equalsIgnoreCase("qu")) {
					
				String withoutSuffix = str.replace("qu", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.findServiceQuotesID(searchstr, sessionCorpID);
				List tempList = null;	
				if (temp != null && temp.size() != 0){
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?id=" + id;
					return "quotespage";
				}else if(temp.isEmpty()){
					 tempList = ugwwActionTrackerManager.findQuotesList(searchstr, sessionCorpID);
					 if(tempList.isEmpty()){
							errorMessage("Quotes File not Exist");
						 return "quotationsFileForm"; 
					 }if(tempList.size()==1){
						 List tempListId  = ugwwActionTrackerManager.findQuotesListId(tempList.get(0).toString(), sessionCorpID);
						 id = Long.parseLong(tempListId.get(0).toString());
							cfURL = "?id=" + id;
							return "quotespage";
						 
					 }
					 List idList = new ArrayList();
	                 idList.add(tempList.get(0));
	                 String id1= idList.get(0).toString();
					 cfURL = "?id=" + id1+"&forQuotation=QC";
					 return "quoteList";
				}else if(tempList.isEmpty()){
					return "quotationsFileForm";
				}
				
			
		
			} else if (suffixAtBegin.equalsIgnoreCase("pr")|| suffixAtLast.equalsIgnoreCase("pr")) {
				
				String withoutSuffix = str.replace("pr", "");
				String searchstr = withoutSuffix;
				List temp = ugwwActionTrackerManager.getPartner(withoutSuffix,sessionCorpID);
				
				 if (temp != null && temp.size() == 1) {
					   Object[] obj = (Object[]) temp.get(0);
					   id = Long.parseLong(obj[0].toString());
					    String code =  obj[1].toString();
					    Boolean isAgent = (Boolean) obj[2];
					    Boolean isprivateparty = (Boolean) obj[3];
					    Boolean isaccount = (Boolean) obj[4];
					    Boolean isvendor = (Boolean) obj[5];
					    Boolean isownerop = (Boolean) obj[6];
					    Boolean iscarrier = (Boolean) obj[7];			    
					    if (isAgent) {
					    	cfURL = "?id="+id+"&partnerType=AG"; 
						return "agentPage";
					} else if(isprivateparty){
						// "editPartnerPublic.html?id=192223&partnerType=PP"
						cfURL = "?id="+id+"&partnerType=PP"; 
						return "privatePage";

					}else if(isaccount){
						//editPartnerPublic.html?id=192863&partnerType=AC
						cfURL = "?id="+id+"&partnerType=AC"; 
						return "privatePage";
						
				}else if(isvendor){
					cfURL = "?id="+id+"&partnerType=VN";
					return "privatePage";
					
				}else if(isownerop){
			//		a href="viewPartner.html?id=209911&partnerType=OO"
					cfURL = "?id="+id+"&partnerType=OO";
					return "privatePage";
					
				}else if(iscarrier){
					// href="viewPartner.html?id=144154&partnerType=CR"
					cfURL = "?id="+id+"&partnerType=CR"; 
					return "privatePage";
					
				}else{
					errorMessage(" partner code not exist");
					return "viewPage";
			 }
					    
				 }else{
					 if(temp.size()>1){
						 partnerPrivate.setExtReference(null);
						 partnerPublic.setIsAgent(true);
						 partnerPublic.setIsAccount(true);
						 partnerPublic.setIsCarrier(true);
						 partnerPublic.setIsOwnerOp(true);
						 partnerPublic.setIsVendor(true);
						 partnerPublic.setIsPrivateParty(true);
						 partnerPublic.setPartnerCode(searchstr);
							return "partnerList"; 
					 }
						errorMessage(" partner code not exist");
						return "viewPage";
					 
				 }

			}
			else if (suffixAtBegin.equalsIgnoreCase("fc")	|| suffixAtLast.equalsIgnoreCase("fc")) {
				String quickFileCabinetView="";
				Boolean forceDocCenter=false;
				try{
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User quickUser = (User) auth.getPrincipal();
				if(quickUser !=null){
				quickFileCabinetView = quickUser.getFileCabinetView();
				}
				if(company !=null){
				forceDocCenter = company.getForceDocCenter();
				}
				}catch(Exception e){
					e.printStackTrace();
				}
				String withoutCorpID;
				/*if(str.contains(sessionCorpID.toLowerCase())){
				 str = str.replace(sessionCorpID.toLowerCase(), "");
				}*/
				String withoutSuffix = str.replace("fc", "");
				String searchstr = withoutSuffix; 
				if(searchstr.startsWith("0") || searchstr.startsWith("1")|| searchstr.startsWith("2")|| searchstr.startsWith("3")|| searchstr.startsWith("4")||
				 searchstr.startsWith("5")|| searchstr.startsWith("6")|| searchstr.startsWith("7")|| searchstr.startsWith("8")|| searchstr.startsWith("9")){
					searchstr=sessionCorpID+searchstr;	
				}
				List temp = ugwwActionTrackerManager.findsoFullId(searchstr,
						sessionCorpID);
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?id=" + id+"&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false"; 
					String serachString="fileCabinet";
					if((quickFileCabinetView !=null && quickFileCabinetView.equalsIgnoreCase("Document Centre")) || (forceDocCenter !=null && forceDocCenter)){
						serachString="fileCabinetDocType";
					} 
					return serachString;
					}
				else if(temp.isEmpty()) {
					 temp = ugwwActionTrackerManager.findId(searchstr,
							sessionCorpID);
					if (temp.size() == 1) {
						id = Long.parseLong(temp.get(0).toString());
						cfURL = "?id=" + id+"&myFileFor=CF&noteFor=CustomerFile&active=true&secure=false"; 
						String serachString="fileCabinet"; 
						if((quickFileCabinetView !=null && quickFileCabinetView.equalsIgnoreCase("Document Centre")) || (forceDocCenter !=null && forceDocCenter)){
							serachString="fileCabinetDocType";
						} 
						return serachString;
						}
					else{
						errorMessage("Please enter valid order number");
						return "mainMenu";
					}
				} 
				else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			}
			else if (suffixAtBegin.equalsIgnoreCase("uf")	|| suffixAtLast.equalsIgnoreCase("uf")) {
				String withoutCorpID;
				/*if(str.contains(sessionCorpID.toLowerCase())){
				 str = str.replace(sessionCorpID.toLowerCase(), "");
				}*/
				String withoutSuffix = str.replace("uf", "");
				String searchstr = withoutSuffix; 
				if(searchstr.startsWith("0") || searchstr.startsWith("1")|| searchstr.startsWith("2")|| searchstr.startsWith("3")|| searchstr.startsWith("4")||
						 searchstr.startsWith("5")|| searchstr.startsWith("6")|| searchstr.startsWith("7")|| searchstr.startsWith("8")|| searchstr.startsWith("9")){
							searchstr=sessionCorpID+searchstr;	
						}
				List temp = ugwwActionTrackerManager.findsoFullId(searchstr,
						sessionCorpID);
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					cfURL = "?id=" + id+"&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false&ppType=&forQuotation=&PPID=&docUpload=docCentre"; 
					String serachString="fileCabinetUpload"; 
					return serachString;
					}
				else if(temp.isEmpty()) {
					 temp = ugwwActionTrackerManager.findId(searchstr,
							sessionCorpID);
					if (temp.size() == 1) {
						id = Long.parseLong(temp.get(0).toString());
						cfURL = "?id=" + id+"&myFileFor=CF&noteFor=CustomerFile&active=true&secure=false&ppType=&forQuotation=&PPID=&docUpload=docCentre"; 
						String serachString="fileCabinetUpload"; 
						return serachString;
						}
					else{
						errorMessage("Please enter valid order number");
						return "mainMenu";
					}
				} 
				else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			}
			else if (suffixAtBegin.equalsIgnoreCase("nt")	|| suffixAtLast.equalsIgnoreCase("nt")) {
				String withoutCorpID;
				/*if(str.contains(sessionCorpID.toLowerCase())){
				 str = str.replace(sessionCorpID.toLowerCase(), "");
				}*/
				String withoutSuffix = str.replace("nt", "");
				String searchstr = withoutSuffix; 
				if(searchstr.startsWith("0") || searchstr.startsWith("1")|| searchstr.startsWith("2")|| searchstr.startsWith("3")|| searchstr.startsWith("4")||
						 searchstr.startsWith("5")|| searchstr.startsWith("6")|| searchstr.startsWith("7")|| searchstr.startsWith("8")|| searchstr.startsWith("9")){
							searchstr=sessionCorpID+searchstr;	
						}
				List temp = ugwwActionTrackerManager.findsoFullId(searchstr,
						sessionCorpID);
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					serviceOrder=serviceOrderManager.get(id);
					String shipnumber=serviceOrder.getShipNumber();
					cfURL = "?id=" + id+"&notesId="+shipnumber+"&noteFor=ServiceOrder"; 
					String serachString="notesList"; 
					return serachString;
					}
				else if(temp.isEmpty()) {
					 temp = ugwwActionTrackerManager.findId(searchstr,
							sessionCorpID);
					if (temp.size() == 1) {
						id = Long.parseLong(temp.get(0).toString());
						customerFile=customerFileManager.get(id);
						String sequenceNumber=customerFile.getSequenceNumber();
						cfURL = "?id=" + id+"&notesId="+sequenceNumber+"&noteFor=CustomerFile"; 
						String serachString="notesList"; 
						return serachString;
						}
					else{
						errorMessage("Please enter valid order number");
						return "mainMenu";
					}
				} 
				else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			}
			else if (suffixAtBegin.equalsIgnoreCase("nn")	|| suffixAtLast.equalsIgnoreCase("nn")) {
				String withoutCorpID;
				/*if(str.contains(sessionCorpID.toLowerCase())){
				 str = str.replace(sessionCorpID.toLowerCase(), "");
				}*/
				String withoutSuffix = str.replace("nn", "");
				String searchstr = withoutSuffix;
				if(searchstr.startsWith("0") || searchstr.startsWith("1")|| searchstr.startsWith("2")|| searchstr.startsWith("3")|| searchstr.startsWith("4")||
						 searchstr.startsWith("5")|| searchstr.startsWith("6")|| searchstr.startsWith("7")|| searchstr.startsWith("8")|| searchstr.startsWith("9")){
							searchstr=sessionCorpID+searchstr;	
						}
				List temp = ugwwActionTrackerManager.findsoFullId(searchstr,
						sessionCorpID);
				if (temp.size() == 1) {
					id = Long.parseLong(temp.get(0).toString());
					serviceOrder=serviceOrderManager.get(id);
					String shipnumber=serviceOrder.getShipNumber();
					cfURL = "?id=" + id+"&notesId="+shipnumber+"&noteFor=ServiceOrder"; 
					String serachString="soNotesNew"; 
					return serachString;
					}
				else if(temp.isEmpty()) {
					 temp = ugwwActionTrackerManager.findId(searchstr,
							sessionCorpID);
					if (temp.size() == 1) {
						id = Long.parseLong(temp.get(0).toString());
						customerFile=customerFileManager.get(id);
						String sequenceNumber=customerFile.getSequenceNumber();
						cfURL = "?id1=" + id+"&notesId="+sequenceNumber+"&noteFor=CustomerFile";  
						String serachString="cfNotesNew"; 
						return serachString;
						}
					else{
						errorMessage("Please enter valid order number");
						return "mainMenu";
					}
				} 
				else{
					errorMessage("Please enter valid order number");
					return "mainMenu";
				}


			}
		}
		return Status;

	}

	

		public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public SimpleDateFormat getFormat() {
		return format;
	}

	public void setFormat(SimpleDateFormat format) {
		this.format = format;
	}

	public StringBuilder getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(StringBuilder systemDate) {
		this.systemDate = systemDate;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public void setUgwwActionTrackerManager(
			UgwwActionTrackerManager ugwwActionTrackerManager) {
		this.ugwwActionTrackerManager = ugwwActionTrackerManager;
	}

	public String getCfURL() {
		return cfURL;
	}

	public void setCfURL(String cfURL) {
		this.cfURL = cfURL;
	}

	public String getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getReportModule() {
		return reportModule;
	}

	public void setReportModule(String reportModule) {
		this.reportModule = reportModule;
	}

	public String getReportSubModule() {
		return reportSubModule;
	}

	public void setReportSubModule(String reportSubModule) {
		this.reportSubModule = reportSubModule;
	}

	public String getFormReportFlag() {
		return formReportFlag;
	}

	public void setFormReportFlag(String formReportFlag) {
		this.formReportFlag = formReportFlag;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getCurrentPageLocation() {
		return currentPageLocation;
	}

	public void setCurrentPageLocation(String currentPageLocation) {
		this.currentPageLocation = currentPageLocation;
	}

	public Set getPartners() {
		return partners;
	}

	public void setPartners(Set partners) {
		this.partners = partners;
	}

	public String getCountryCodeSearch() {
		return countryCodeSearch;
	}

	public void setCountryCodeSearch(String countryCodeSearch) {
		this.countryCodeSearch = countryCodeSearch;
	}

	public String getVanlineCodeSearch() {
		return vanlineCodeSearch;
	}

	public void setVanlineCodeSearch(String vanlineCodeSearch) {
		this.vanlineCodeSearch = vanlineCodeSearch;
	}

	public String getStateSearch() {
		return stateSearch;
	}

	public void setStateSearch(String stateSearch) {
		this.stateSearch = stateSearch;
	}

	public Boolean getIsIgnoreInactive() {
		return isIgnoreInactive;
	}

	public void setIsIgnoreInactive(Boolean isIgnoreInactive) {
		this.isIgnoreInactive = isIgnoreInactive;
	}

	public String getVanlineCode() {
		return vanlineCode;
	}

	public void setVanlineCode(String vanlineCode) {
		this.vanlineCode = vanlineCode;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getCountrySearch() {
		return countrySearch;
	}

	public void setCountrySearch(String countrySearch) {
		this.countrySearch = countrySearch;
	}

	public List getPartnerRatess() {
		return partnerRatess;
	}

	public void setPartnerRatess(List partnerRatess) {
		this.partnerRatess = partnerRatess;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public Partner getPartner1() {
		return partner1;
	}

	public void setPartner1(Partner partner1) {
		this.partner1 = partner1;
	}

	public List getFindPartnerProfileList() {
		return findPartnerProfileList;
	}

	public void setFindPartnerProfileList(List findPartnerProfileList) {
		this.findPartnerProfileList = findPartnerProfileList;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPublicManager(
			PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public void setPartnerPrivateManager(
			PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getSessionUserName() {
		return sessionUserName;
	}

	public void setSessionUserName(String sessionUserName) {
		this.sessionUserName = sessionUserName;
	}

	

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public boolean isCheckFieldVisibility() {
		return checkFieldVisibility;
	}

	public void setCheckFieldVisibility(boolean checkFieldVisibility) {
		this.checkFieldVisibility = checkFieldVisibility;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public String getDashBoardHideJobsList() {
		return dashBoardHideJobsList;
	}

	public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
		this.dashBoardHideJobsList = dashBoardHideJobsList;
	}

}
