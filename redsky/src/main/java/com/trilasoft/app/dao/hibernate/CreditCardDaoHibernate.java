package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CreditCardDao;
import com.trilasoft.app.model.CreditCard;


public class CreditCardDaoHibernate extends GenericDaoHibernate<CreditCard, Long> implements CreditCardDao {   
	//private List charges;
    public CreditCardDaoHibernate() {   
        super(CreditCard.class);   
    }   
	public List findMaxId() {
		
		return getHibernateTemplate().find("select max(id) from CreditCard");
	}
	
public List findByShipNumber(String shipNumber) {
		
		return getHibernateTemplate().find("from CreditCard where shipNumber=?",shipNumber);
	}
public List getPrimaryFlagStatus(String shipNumber, String sessionCorpID) {
	
	return getHibernateTemplate().find("select id,primaryFlag from CreditCard where primaryFlag=true and shipNumber='"+shipNumber+"' and corpID='"+sessionCorpID+"'");
}
public List getCreditCardMasking(String number) {
	//System.out.println("\n\n\n\n\n number-->>>"+number);
	return this.getSession().createSQLQuery("select concat('xxxx xxxx xxxx ',right('"+number+"',4))").list();
	//return getHibernateTemplate().find("select concat('xxxx xxxx xxxx ',right('"+number+"',4))");
}
public List getCreditcardNumber(Long id) {
	return getHibernateTemplate().find("select ccNumber from CreditCard where id=?",id);
}
public List getServiceOrder(String shipNumber, String sessionCorpID) {
	return  getHibernateTemplate().find("from ServiceOrder where shipNumber='"+shipNumber+"' and corpID='"+sessionCorpID+"'");
}
}