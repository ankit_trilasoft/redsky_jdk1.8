package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CalendarFile;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.CostElement;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CostElementManager;
import com.trilasoft.app.service.GlCodeRateGridManager;
import com.trilasoft.app.service.RefMasterManager;

public class CostElementAction extends BaseAction{
	private CostElementManager costElementManager;
	private CostElement costElement;
	private List costElements;
	private List costElementList;
	private String sessionCorpID;
	private String chargeCostElement;
    private Long id;
    private ChargesManager chargesManager;
    private GlCodeRateGridManager glCodeRateGridManager;
	private Charges charges;
	private  Map<String, String> category;
	private String jobType;
	private String routing;
	private String soCompanyDivision="";
	private String contract;
    Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(CostElementAction.class);

    
	public CostElementAction() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID=user.getCorpID();

	}
	private List costElementListData;
	@SkipValidation
	public String openCostElementPopup(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		costElementListData=costElementManager.getCostListDataSummary();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String getComboList(String corpId) { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		glcodes=refMasterManager.findOnlyCode("GLCODES", sessionCorpID);
		category = refMasterManager.findByParameter(corpId, "ACC_CATEGORY");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private RefMasterManager refMasterManager;
	private List glcodes;
	public String list()
	{ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		costElements=costElementManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String costEl;
	private String costElementDetails;
	@SkipValidation
	public String costElDetailCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		costElementDetails=costElementManager.costElDetailCode(costEl);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String costDes;
	private String costDescriptionDetails;
	@SkipValidation
	public String costDesDetailCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		costDescriptionDetails=costElementManager.costDesDetailCode(costDes);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findCostElement(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		costElementList=costElementManager.findCostElementList(chargeCostElement,sessionCorpID,jobType,routing,soCompanyDivision);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private String chargeCode;
	@SkipValidation
	public String findCostElementTemplate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		costElementList=costElementManager.findCostElementListTemplate(chargeCode,sessionCorpID,jobType,routing,contract,soCompanyDivision);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private String costId;
	private String costEleUsedFlag;
	public String checkForCostEleUseInCharge(){
		//getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			costElement=costElementManager.get(Long.parseLong(costId));
			costEleUsedFlag="";
			int rec=costElementManager.checkForCostEleUseInCharge(costElement.getCostElement(),costElement.getDescription(),costElement.getRecGl(),costElement.getPayGl(),sessionCorpID);
			if(rec>0)
			{
				costEleUsedFlag="used";
			}else{
				costEleUsedFlag="notUsed";
			}
		}catch(Exception e){e.printStackTrace();}
	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String costElementDelete(){
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		costElementManager.remove(id);
			String key ="Cost element deleted successfully";
	      	saveMessage(getText(key));
	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	}
	
	public String save() throws Exception {
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (costElement.getId() == null);  
				if (isNew) { 
					costElement.setCreatedOn(new Date());
		        }
				try
				{
				costElement.setCorpId(sessionCorpID);
				costElement.setUpdatedOn(new Date());
				costElement.setUpdatedBy(getRequest().getRemoteUser()); 
				costElementManager.save(costElement);  
				String key = (isNew) ? "Cost Element added" : "Cost Element updated";
			    saveMessage(getText(key));
			    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 		return SUCCESS;
				}
				catch(Exception e)
				{e.printStackTrace();
					errorMessage("CostElement {"+costElement.getCostElement()+"} already exist");
					costElement.setCostElement(oldCostElement);
					return INPUT;
				}
	}	
	private String oldCostElement;
	private List glCodeRateGridList;
	public String edit()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		if(id != null){
			costElement = costElementManager.get(id);
			glCodeRateGridList=glCodeRateGridManager.getAllRateGridList(sessionCorpID,id+"","","","","");

		}else{
			costElement = new CostElement();
			costElement.setCorpId(sessionCorpID);
			costElement.setCreatedOn(new Date());
			costElement.setUpdatedOn(new Date());
			costElement.setCreatedBy(getRequest().getRemoteUser());
			costElement.setUpdatedBy(getRequest().getRemoteUser());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateChargesByCostElementMethod(){
		String returnVal ="";
		try{
			returnVal = save();
		}catch(Exception e){e.printStackTrace();}
		if(returnVal.equalsIgnoreCase("SUCCESS"))
		{
				List chargesList = costElementManager.getCharges(oldCostElement,sessionCorpID);
				if(!(chargesList.isEmpty()) && chargesList.size()!=0){
					Iterator itr = chargesList.iterator();
					while(itr.hasNext()){
						charges = chargesManager.get(new Long((Long) itr.next()));
						charges.setCostElement(costElement.getCostElement());
						charges.setScostElementDescription(costElement.getDescription());
						charges.setGl(costElement.getRecGl());
						charges.setExpGl(costElement.getPayGl());
						try{
							charges=chargesManager.save(charges);
						}catch(Exception e){
				        	e.printStackTrace();
				        }
						try{
						int rec=costElementManager.updateAccountlineGLByCostElement(charges.getCharge(),charges.getContract(),sessionCorpID,costElement.getRecGl(),costElement.getPayGl(),costElement.getId());
						System.out.print(charges.getCharge()+"-->"+rec);
						}catch(Exception e){e.printStackTrace();}
					}
				}
		}
		return returnVal;
	}
   	private String tableN;
   	private String tableD;
   	public String searchCostElementRecord()
   	{
   		costElements=costElementManager.getCostElementValue(tableN,tableD);
   		costElementListData=costElements;
   		return SUCCESS;
   	}
	public CostElement getCostElement() {
		return costElement;
	}

	public void setCostElement(CostElement costElement) {
		this.costElement = costElement;
	}

	public List getCostElements() {
		return costElements;
	}

	public void setCostElements(List costElements) {
		this.costElements = costElements;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setCostElementManager(CostElementManager costElementManager) {
		this.costElementManager = costElementManager;
	}

	public List getGlcodes() {
		return glcodes;
	}

	public void setGlcodes(List glcodes) {
		this.glcodes = glcodes;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public List getCostElementListData() {
		return costElementListData;
	}
	public void setCostElementListData(List costElementListData) {
		this.costElementListData = costElementListData;
	}
	public String getCostEl() {
		return costEl;
	}
	public void setCostEl(String costEl) {
		this.costEl = costEl;
	}
	public String getCostElementDetails() {
		return costElementDetails;
	}
	public void setCostElementDetails(String costElementDetails) {
		this.costElementDetails = costElementDetails;
	}
	public String getCostDes() {
		return costDes;
	}
	public void setCostDes(String costDes) {
		this.costDes = costDes;
	}
	public String getCostDescriptionDetails() {
		return costDescriptionDetails;
	}
	public void setCostDescriptionDetails(String costDescriptionDetails) {
		this.costDescriptionDetails = costDescriptionDetails;
	}
	public List getCostElementList() {
		return costElementList;
	}
	public void setCostElementList(List costElementList) {
		this.costElementList = costElementList;
	}
	public String getChargeCostElement() {
		return chargeCostElement;
	}
	public void setChargeCostElement(String chargeCostElement) {
		this.chargeCostElement = chargeCostElement;
	}
	public Charges getCharges() {
		return charges;
	}
	public void setCharges(Charges charges) {
		this.charges = charges;
	}
	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}
	public String getCostId() {
		return costId;
	}
	public void setCostId(String costId) {
		this.costId = costId;
	}
	public String getCostEleUsedFlag() {
		return costEleUsedFlag;
	}
	public void setCostEleUsedFlag(String costEleUsedFlag) {
		this.costEleUsedFlag = costEleUsedFlag;
	}
	public String getTableN() {
		return tableN;
	}
	public void setTableN(String tableN) {
		this.tableN = tableN;
	}
	public String getTableD() {
		return tableD;
	}
	public void setTableD(String tableD) {
		this.tableD = tableD;
	}
	public String getOldCostElement() {
		return oldCostElement;
	}
	public void setOldCostElement(String oldCostElement) {
		this.oldCostElement = oldCostElement;
	}
	public Map<String, String> getCategory() {
		return category;
	}
	public void setCategory(Map<String, String> category) {
		this.category = category;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getSoCompanyDivision() {
		return soCompanyDivision;
	}
	public void setSoCompanyDivision(String soCompanyDivision) {
		this.soCompanyDivision = soCompanyDivision;
	}	
    public void setGlCodeRateGridManager(GlCodeRateGridManager glCodeRateGridManager) {
		this.glCodeRateGridManager = glCodeRateGridManager;
	}
	public List getGlCodeRateGridList() {
		return glCodeRateGridList;
	}
	public void setGlCodeRateGridList(List glCodeRateGridList) {
		this.glCodeRateGridList = glCodeRateGridList;
	}

}
