package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="networkdatafields")
public class NetworkDataFields extends BaseObject{
	private Long id;
	private String modelName;
	private String fieldName;
	private String transactionType;
	private Date createdOn;
	private Date updatedOn;
	private String createdBy;
	private String updatedBy;
	private Boolean useSysDefault;
	private String toModelName;
	private String toFieldName;
	private String type;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("modelName",
				modelName).append("fieldName", fieldName).append(
				"transactionType", transactionType).append("createdOn",
				createdOn).append("updatedOn", updatedOn).append("createdBy",
				createdBy).append("updatedBy", updatedBy).append(
				"useSysDefault", useSysDefault).append("toModelName",
				toModelName).append("toFieldName", toFieldName).append("type",
				type).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof NetworkDataFields))
			return false;
		NetworkDataFields castOther = (NetworkDataFields) other;
		return new EqualsBuilder().append(id, castOther.id).append(modelName,
				castOther.modelName).append(fieldName, castOther.fieldName)
				.append(transactionType, castOther.transactionType).append(
						createdOn, castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(useSysDefault,
						castOther.useSysDefault).append(toModelName,
						castOther.toModelName).append(toFieldName,
						castOther.toFieldName).append(type, castOther.type)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(modelName).append(
				fieldName).append(transactionType).append(createdOn).append(
				updatedOn).append(createdBy).append(updatedBy).append(
				useSysDefault).append(toModelName).append(toFieldName).append(
				type).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=45)
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	@Column(length=60)
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public Boolean getUseSysDefault() {
		return useSysDefault;
	}
	public void setUseSysDefault(Boolean useSysDefault) {
		this.useSysDefault = useSysDefault;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	@Column
	public String getToFieldName() {
		return toFieldName;
	}
	public void setToFieldName(String toFieldName) {
		this.toFieldName = toFieldName;
	}
	@Column
	public String getToModelName() {
		return toModelName;
	}
	public void setToModelName(String toModelName) {
		this.toModelName = toModelName;
	}
	@Column
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
