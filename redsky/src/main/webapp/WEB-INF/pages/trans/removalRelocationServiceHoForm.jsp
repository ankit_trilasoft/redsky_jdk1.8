<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head>
<title>Removal Relocation Service</title>
<meta name="heading"
	content="Removal Relocation Service" />
<style type="text/css">	

legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
.listgreytext {
	font-family: arial,verdana;
	font-size: 11px;
	color: #484848;
	text-decoration: none;
	background-color: none;
	font-weight: normal;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
input[type="checkbox"]{
vertical-align:middle;
}

</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>

<script type="text/javascript">
function changeStatus(){
	document.forms['removalRelocationServiceForm'].elements['formStatus'].value = '1';
}
function focusDate(target){
	document.forms['removalRelocationServiceForm'].elements[target].focus();
}
function allReadOnly(){ 
        var numOfElements = document.forms['removalRelocationServiceForm'].elements.length;  
        for(var i=0; i<numOfElements;i++)
         {
         var textName = document.forms['removalRelocationServiceForm'].elements[i].name;  
         document.forms['removalRelocationServiceForm'].elements[i].disabled=true; 
         }
     }    


function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	}
function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
</script>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<style><%@ include file="/common/calenderStyle.css"%></style>   
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>
</head>
<s:form id="removalRelocationServiceForm" name="removalRelocationServiceForm" action="saveRemovalRelocationService">
<s:hidden name="removalRelocationService.id" value="${removalRelocationService.id}"/>
<s:hidden name="removalRelocationService.customerFileId" value="${removalRelocationService.customerFileId}"/>
<s:hidden name="removalRelocationService.corpId" value="${removalRelocationService.corpId}"/> 
<s:hidden name="formStatus" value=""/>
<s:hidden name="submitCheck" value="${submitCheck}"/>
<s:hidden name="partnerAbbreviation" value="${partnerAbbreviation}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="isIta60" value="false"/>
<c:if test="${removalRelocationService.isIta60}">
<c:set var="isIta60" value="true"/>
</c:if>
<c:set var="isIta1m3" value="false"/>
<c:if test="${removalRelocationService.isIta1m3}">
<c:set var="isIta1m3" value="true"/>
</c:if>
<c:set var="isIta2m3" value="false"/>
<c:if test="${removalRelocationService.isIta2m3}">
<c:set var="isIta2m3" value="true"/>
</c:if>
<c:set var="isIta3m3" value="false"/>
<c:if test="${removalRelocationService.isIta3m3}">
<c:set var="isIta3m3" value="true"/>
</c:if>
<c:set var="isFtExpat20" value="false"/>
<c:if test="${removalRelocationService.isFtExpat20}">
<c:set var="isFtExpat20" value="true"/>
</c:if>
<c:set var="isFtExpat40" value="false"/>
<c:if test="${removalRelocationService.isFtExpat40}">
<c:set var="isFtExpat40" value="true"/>
</c:if>
<c:set var="isKgExpat30" value="false"/>
<c:if test="${removalRelocationService.isKgExpat30}">
<c:set var="isKgExpat30" value="true"/>
</c:if>
<c:set var="isTransportAllow" value="false"/>
<c:if test="${removalRelocationService.isTransportAllow}">
<c:set var="isTransportAllow" value="true"/>
</c:if>
<c:set var="isFtSingle20" value="false"/>
<c:if test="${removalRelocationService.isFtSingle20}">
<c:set var="isFtSingle20" value="true"/>
</c:if>
<c:set var="isFtFamily40" value="false"/>
<c:if test="${removalRelocationService.isFtFamily40}">
<c:set var="isFtFamily40" value="true"/>
</c:if>
<c:set var="isStorage" value="false"/>
<c:if test="${removalRelocationService.isStorage}">
<c:set var="isStorage" value="true"/>
</c:if>
<c:set var="isOrientation8" value="false"/>
<c:if test="${removalRelocationService.isOrientation8}">
<c:set var="isOrientation8" value="true"/>
</c:if>
<c:set var="isOrientation16" value="false"/>
<c:if test="${removalRelocationService.isOrientation16}">
<c:set var="isOrientation16" value="true"/>
</c:if>
<c:set var="isOrientation24" value="false"/>
<c:if test="${removalRelocationService.isOrientation24}">
<c:set var="isOrientation24" value="true"/>
</c:if>
<c:set var="isHomeSearch8" value="false"/>
<c:if test="${removalRelocationService.isHomeSearch8}">
<c:set var="isHomeSearch8" value="true"/>
</c:if>
<c:set var="isHomeSearch16" value="false"/>
<c:if test="${removalRelocationService.isHomeSearch16}">
<c:set var="isHomeSearch16" value="true"/>
</c:if>
<c:set var="isHomeSearch24" value="false"/>
<c:if test="${removalRelocationService.isHomeSearch24}">
<c:set var="isHomeSearch24" value="true"/>
</c:if>
<c:set var="isHomeFurnished" value="false"/>
<c:if test="${removalRelocationService.isHomeFurnished}">
<c:set var="isHomeFurnished" value="true"/>
</c:if>
<c:set var="isHomeUnfurnished" value="false"/>
<c:if test="${removalRelocationService.isHomeUnfurnished}">
<c:set var="isHomeUnfurnished" value="true"/>
</c:if>
<c:set var="isUsdollar" value="false"/>
<c:if test="${removalRelocationService.isUsdollar}">
<c:set var="isUsdollar" value="true"/>
</c:if>
<c:set var="isDollar" value="false"/>
<c:if test="${removalRelocationService.isDollar}">
<c:set var="isDollar" value="true"/>
</c:if>
<c:set var="isOtherCurrency" value="false"/>
<c:if test="${removalRelocationService.isOtherCurrency}">
<c:set var="isOtherCurrency" value="true"/>
</c:if>
<c:set var="isSchoolSearch8" value="false"/>
<c:if test="${removalRelocationService.isSchoolSearch8}">
<c:set var="isSchoolSearch8" value="true"/>
</c:if>
<c:set var="isSchoolSearch16" value="false"/>
<c:if test="${removalRelocationService.isSchoolSearch16}">
<c:set var="isSchoolSearch16" value="true"/>
</c:if>
<c:set var="isSchoolSearch24" value="false"/>
<c:if test="${removalRelocationService.isSchoolSearch24}">
<c:set var="isSchoolSearch24" value="true"/>
</c:if>
<c:set var="isSelfInSearch8" value="false"/>
<c:if test="${removalRelocationService.isSelfInSearch8}">
<c:set var="isSelfInSearch8" value="true"/>
</c:if>
<c:set var="isSelfInSearch16" value="false"/>
<c:if test="${removalRelocationService.isSelfInSearch16}">
<c:set var="isSelfInSearch16" value="true"/>
</c:if>
<c:set var="isSelfInSearch24" value="false"/>
<c:if test="${removalRelocationService.isSelfInSearch24}">
<c:set var="isSelfInSearch24" value="true"/>
</c:if> 

<c:set var="isorientationTrip" value="false"/>
<c:if test="${removalRelocationService.orientationTrip}">
<c:set var="isorientationTrip" value="true"/>
</c:if>
<c:set var="ishomeSearch" value="false"/>
<c:if test="${removalRelocationService.homeSearch}">
<c:set var="ishomeSearch" value="true"/>
</c:if>
<c:set var="issettlingInAssistance" value="false"/>
<c:if test="${removalRelocationService.settlingInAssistance}">
<c:set var="issettlingInAssistance" value="true"/>
</c:if>
<c:set var="isentryVisa" value="false"/>
<c:if test="${removalRelocationService.entryVisa}">
<c:set var="isentryVisa" value="true"/>
</c:if>
<c:set var="islongTermStayVisa" value="false"/>
<c:if test="${removalRelocationService.longTermStayVisa}">
<c:set var="islongTermStayVisa" value="true"/>
</c:if>
<c:set var="isworkPermit" value="false"/>
<c:if test="${removalRelocationService.workPermit}">
<c:set var="isworkPermit" value="true"/>
</c:if>

<c:set var="isschoolSearch" value="false"/>
<c:if test="${removalRelocationService.schoolSearch}">
<c:set var="isschoolSearch" value="true"/>
</c:if>

<c:set var="furnished" value="false"/>
<c:if test="${removalRelocationService.furnished}">
<c:set var="furnished" value="true"/>
</c:if> 
<c:set var="unfurnished" value="false"/>
<c:if test="${removalRelocationService.unFurnished}">
<c:set var="unfurnished" value="true"/>
</c:if> 

<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%" border="0">
<tr><td style="height:0px;!height:5px;"></td></tr>
<tr><td align="center"><span style="font-size:18px;font-weight:bold;text-align:center;line-height:22px;"><u>Removal Services</u></span></td></tr>
<tr><td height="5px;"></td></tr>
<tr><td><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>Intercontinental</u></span></td></tr> 
<tr><td height="5px;"></td></tr>
<tr><td class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isIta60" value="${isIta60}" fieldValue="true" onclick="" tabindex="65"/>&nbsp;&nbsp;60Kgs. airfreight per person</td></tr>
<tr><td class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isIta1m3" value="${isIta1m3}" fieldValue="true" onclick="" tabindex="65"/>&nbsp;&nbsp;1 m3 by seafreight per person (at start of assignment)</td></tr>
<tr><td class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isIta2m3" value="${isIta2m3}" fieldValue="true" onclick=" " tabindex=""/>&nbsp;&nbsp;2 m3 by seafreight and 60 kgs by airfreight (at the end of ITA assignment)</td></tr>
<tr><td class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isIta3m3" value="${isIta3m3}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;3 m3 by seafreight per person (at the end of assignment)</td></tr>
<tr></tr>
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isFtExpat20" value="${isFtExpat20}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Removal in 20ft seafreight container (for single OR couple without children on Expat assignment)</td></tr>
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isFtExpat40" value="${isFtExpat40}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Removal in 40ft seafreight container (for family with children on Expat assignment)
<span style="padding-left:10px "><s:textfield cssClass="input-text" key="removalRelocationService.ftExpat40ExceptionalItems" size="45" maxlength="45"  /></span></td>
</tr>
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isKgExpat30" value="${isKgExpat30}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;30Kgs. per person airfreight (at start)
<span style="padding-left:227px "><s:textfield cssClass="input-text" key="removalRelocationService.kgExpat30ExceptionalItems" size="45" maxlength="45"  /></span></td>
</tr>
<tr></tr>
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isTransportAllow" value="${isTransportAllow}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Transport of other exceptional items allowed, like:
<span style="padding-left:169px "><s:textfield cssClass="input-text" key="removalRelocationService.transportAllowExceptionalItems" size="45" maxlength="45"  /></span>	</td>
</tr>
<tr><td height="5px;"></td></tr>
<tr><th  class="listgreytext" ><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>Continental</u></span></th></tr>
<tr><td height="5px;"></td></tr>
<tr><th><h3 class="listgreytext"><b>Removal / Transport by road :</b></h3></th></tr>
<tr></tr>
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isFtSingle20" value="${isFtSingle20}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Removal in 20 ft container OR comparable m3 (for single OR couple)</td></tr>
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isFtFamily40" value="${isFtFamily40}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Removal in 40 ft container OR comparable m3 (for family with children)</td></tr>
<tr><td>&nbsp;&nbsp;</td></tr> 	
<tr><td  class="listgreytext">&nbsp;&nbsp;<s:checkbox key="removalRelocationService.isStorage" value="${isStorage}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;<b>Storage</b></td></tr>	
</table> 

<div id="relocationServices" class="switchgroup1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
<tr><td height="5px;"></td></tr>
<tr><td align="center"><span style="font-size:18px;font-weight:bold;text-align:center;line-height:22px;"><u>Relocation Services</u></span></td></tr>
<%--<tr><td><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>General Guidelines:</u></span></td></tr>
<tr><td height="5px;"></td></tr> 
<tr><td class="listgreytext" >&nbsp;&nbsp;&nbsp;&nbsp;The required relocation services filled-in below should not exceed a maximum of:</td></tr>
<tr><td height="5px;"></td></tr> 
<tr><td class="listgreytext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Orientation Trip - 8 hours</td></tr>
<tr><td height="3px;"></td></tr> 
<tr><td class="listgreytext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Home-Search Assistance - 12 hours</td></tr>
<tr><td height="3px;"></td></tr>
<tr><td class="listgreytext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Settling-In Assistance - 8 hours</td></tr>
<tr><td height="3px;"></td></tr>
<tr><td class="listgreytext" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - School Search & Registering School - 8 hours</td></tr> 
<tr><td height="5px;"></td></tr>
<tr><td class="listgreytext" >&nbsp;&nbsp;&nbsp;&nbsp;If additional time is required UTS / Eurohome must contact Host HR&O for approval.</td></tr>
<tr><td height="5px;"></td></tr>
--%>
<tr><td><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>Service Packages to be assisted:</u></span></td></tr>
<tr><td height="5px;"></td></tr> 
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.orientationTrip" value="${isorientationTrip}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;OrientationTrip </td></tr>  
<tr><td height="5px;"></td></tr>
<tr><td class="listgreytext" style="padding-left:42px;">Pre-arrival / Area Orientation Tour; prior to final arrival an orientation to get familiar to the area and learn more about housing and schooling options.</td></tr>
<tr><td height="5px;"></td></tr> 
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.homeSearch" value="${ishomeSearch}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Home-Search Assistance </td></tr> 
<tr><td height="5px;"></td></tr> 
<tr><td class="listgreytext" style="padding-left:42px;">Set up the visit (s) of selected houses for permanent stay (being the length of assignment). Accompanied assistance; lease negotiations, arrangement of utilities, TV & Internet, check-in and inventory.</td></tr>
<%--<tr><td height="5px;"></td></tr>
<tr><td class="listgreytext" style="padding-left:42px;">House Search by Eurohome based on rental budget provided by Host HR&O.</td></tr>
--%><tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext" style="padding-left:42px;">Rental budget authorized per month (excluding utilities)</td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext" style="padding-left:44px;"><s:checkbox key="removalRelocationService.isUsdollar" value="${isUsdollar}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;US$<span style="padding-left:73px "><s:textfield cssClass="input-text" key="removalRelocationService.usdollarAmounts" size="7" maxlength="6" onkeydown="return onlyFloatNumsAllowed(event);" />&nbsp;&nbsp;per month </span></td></tr>
<tr><td  class="listgreytext" style="padding-left:44px;"><s:checkbox key="removalRelocationService.isDollar" value="${isDollar}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;&euro;<span style="padding-left:87px "><s:textfield cssClass="input-text" key="removalRelocationService.dollarAmounts" size="7" maxlength="6" onkeydown="return onlyFloatNumsAllowed(event);" />&nbsp;&nbsp;per month</span></td></tr>
<tr><td  class="listgreytext" style="padding-left:44px;"><s:checkbox key="removalRelocationService.isOtherCurrency" value="${isOtherCurrency}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;other currency<span style="padding-left:21px"><s:textfield cssClass="input-text" key="removalRelocationService.otherCurrencyAmounts" size="20" maxlength="26" />&nbsp;&nbsp;per month</span></td></tr>
<tr><td height="5px;"></td></tr> 
 <tr><td  class="listgreytext" style="padding-left:44px;"><s:checkbox key="removalRelocationService.furnished" value="${furnished}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Furnished</td>
<tr><td  class="listgreytext" style="padding-left:44px;"><s:checkbox key="removalRelocationService.unFurnished" value="${unfurnished}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Unfurnished</td>
<tr><td height="5px;"></td></tr> 
<tr><td class="listgreytext" style="padding-left:42px;line-height:15px;">Set up visiting of selected houses for permanent stay (being the length of assignment). Accompanied assistance, lease negotiations, arrange for utilities, TV & Internet, check-in & inventory.</td></tr>
<tr><td height="5px;"></td></tr> 
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.settlingInAssistance" value="${issettlingInAssistance}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Settling-In Assistance </td></tr>
<tr><td height="5px;"></td></tr>
<tr><td class="listgreytext" style="padding-left:42px;line-height:15px;">Local registration procedures, opening of bank-account, social security number, area familiarization, visiting Municipal Health Service, finding family doctor & dentist, etc.</td></tr>  
<tr><td height="5px;"></td></tr> 
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.schoolSearch" value="${isschoolSearch}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;School-Search and Registering school for children</td></tr>  
<tr><td height="5px;"></td></tr>
<tr><td class="listgreytext" style="padding-left:42px;line-height:15px;">Meeting the transferee at  hotel / company, arrange for meetings with Secretary OR Principal of the school (s), accompanied visits to the school (s), assistance with the application forms, arrange for information to take home and share with the children. If necessary, guard the application in absence of the transferee.</td></tr>  
<s:hidden name="removalRelocationService.isOrientation8" value="${isOrientation8}"/>
<s:hidden name="removalRelocationService.isOrientation16" value="${isOrientation16}"/>
<s:hidden name="removalRelocationService.isOrientation24" value="${isOrientation24}"/>
<s:hidden name="removalRelocationService.isHomeSearch8" value="${isHomeSearch8}"/>
<s:hidden name="removalRelocationService.isHomeSearch16" value="${isHomeSearch16}"/>
<s:hidden name="removalRelocationService.isHomeSearch24" value="${isHomeSearch24}"/>
<s:hidden name="removalRelocationService.isHomeFurnished" value="${isHomeFurnished}"/>
<s:hidden name="removalRelocationService.isHomeUnfurnished" value="${isHomeUnfurnished}"/> 
<s:hidden name="removalRelocationService.isSchoolSearch8" value="${isSchoolSearch8}"/>
<s:hidden name="removalRelocationService.isSchoolSearch16" value="${isSchoolSearch16}"/>
<s:hidden name="removalRelocationService.isSchoolSearch24" value="${isSchoolSearch24}"/>
<s:hidden name="removalRelocationService.isSelfInSearch8" value="${isSelfInSearch8}"/>
<s:hidden name="removalRelocationService.isSelfInSearch16" value="${isSelfInSearch16}"/>
<s:hidden name="removalRelocationService.isSelfInSearch24" value="${isSelfInSearch24}"/> 
<tr><td height="5px;"></td></tr>  
<tr><td><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>Immigration Assistance:</u></span></td></tr> 
<tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.entryVisa" value="${isentryVisa}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Entry Visa </td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.longTermStayVisa" value="${islongTermStayVisa}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Long Term Stay Visa </td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext" > &nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox key="removalRelocationService.workPermit" value="${isworkPermit}" fieldValue="true" onclick=" " tabindex="65"/>&nbsp;&nbsp;Work Permit </td></tr>
<tr><td height="5px;"></td></tr>

<tr><td><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>Additional Services:</u></span></td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext" style="padding-left:16px;">Eventual additional services are always to be confirmed separately by ${partnerAbbreviation} Description of the specific services:</td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  align="left"  >&nbsp;&nbsp;&nbsp;&nbsp;<s:textarea name="removalRelocationService.otherServicesRequired" cols="80" rows="8" cssClass="textarea" tabindex="82"/></td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  class="listgreytext">Maximum number of additional days authorized 
<span style="padding-left:10px "><s:textfield cssClass="input-text" key="removalRelocationService.authorizedDays" size="5" maxlength="10" onkeydown="return onlyNumsAllowed(event);" /></span>&nbsp;&nbsp;days</td>
</tr>
<tr><td height="5px;"></td></tr>
<tr><td><span style="font-size:13px;font-weight:bold;text-align:center;line-height:17px;"><u>Additional Notes:</u></span></td></tr>
<tr><td height="5px;"></td></tr>
<tr><td  align="left"  >&nbsp;&nbsp;&nbsp;&nbsp;<s:textarea name="removalRelocationService.additionalNotes" cols="80" rows="8" cssClass="textarea" tabindex="82"/></td></tr>
</table>
</div> 
</s:form>
<div id="mydiv" style="position:absolute;margin-top:-15px;"></div>

<script type="text/javascript">

try{
<c:if test="${submitCheck=='true'}" >
allReadOnly();
</c:if>
}
catch(e){}

 </script>
