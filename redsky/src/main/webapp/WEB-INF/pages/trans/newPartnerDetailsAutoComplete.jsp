<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>   
<s:form id="partnerDetailsForm" method="post" > 
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
 <div style="overflow-y:auto;height:215px;">
 <display:table name="partners" class="table pr-f11" id="partners" style="width:100%;">
 <c:choose>
 <c:when test="${partners.preferedagentType == 'Y'}">
	<display:column title="Code" style="background:#E4D970;">
   	<c:set var="d" value="'"/>
     <c:set var="des" value="${fn:replace(partners.lastName,d, '~')}" />
   	<a onclick="copyPartnerDetails('${partners.partnerCode}','${des}','${partnerNameId}','${paertnerCodeId}','${autocompleteDivId}');">${partners.partnerCode}</a>
   	</display:column>
   	<display:column property="lastName" title="Name" style="background:#E4D970;"></display:column>
	<display:column property="billingCountry" title="Country" style="background:#E4D970;"></display:column>	
	<display:column property="billingState" title="State" style="background:#E4D970;"></display:column>
	<display:column property="billingCity" title="City" style="background:#E4D970;"></display:column>
	<display:column title="Agent Classification" style="background:#E4D970;">
 	<c:if test="${partners.partnerType == 'Agent' && agentClassificationShow=='Y'}">
    	<c:out value="${partners.agentClassification}" />
	</c:if>
	</display:column>
	<display:column  title="Type" style="width:90px; whitespace: nowrap;background:#E4D970;" >
	<c:choose>
	<c:when test="${not empty partners.bookingagentCode}">
	<c:out value="${partners.partnerType}"></c:out>
	<img class="openpopup" style="text-align:right;vertical-align:top;" src="${pageContext.request.contextPath}/images/rslogo_small_14.png" />
	</c:when>
	<c:otherwise>
	<c:out value="${partners.partnerType}"></c:out>
	</c:otherwise>
	</c:choose>
	</display:column>
  	</c:when>
	<c:otherwise>
	<display:column title="Code" style="background:#F59696;">
   	<%-- <a onclick="alert('Selected agent is not part of Preferred  agent list. Kindly select another agent.');">${partners.partnerCode}</a> --%>
   	<c:choose>
     <c:when test="${(partners.agentClassification == 'null') || (empty partners.agentClassification ) }">
         <a onclick="alert('This Agent cannot be processed as it is not classified.');">${partners.partnerCode}</a>
     </c:when>
     <c:otherwise>
      <c:set var="d" value="'"/>
          <c:set var="des" value="${fn:replace(partners.lastName,d, '~')}" />
         <a onclick="copyPartnerDetails('${partners.partnerCode}','${des}','${partnerNameId}','${paertnerCodeId}','${autocompleteDivId}');">${partners.partnerCode}</a>
     </c:otherwise>
    </c:choose>
   	</display:column>
   	<display:column property="lastName" title="Name" style="background:#F59696;"></display:column>
	<display:column property="billingCountry" title="Country" style="background:#F59696;"></display:column>	
	<display:column property="billingState" title="State" style="background:#F59696;"></display:column>
	<display:column property="billingCity" title="City" style="background:#F59696;"></display:column>
	<display:column title="Agent Classification" style="background:#F59696;">
 	<c:if test="${partners.partnerType == 'Agent' && agentClassificationShow=='Y'}">
    	<c:out value="${partners.agentClassification}" />
	</c:if>
	</display:column>
	<display:column  title="Type" style="width:90px; whitespace: nowrap;background:#F59696;" >
	<c:choose>
	<c:when test="${not empty partners.bookingagentCode}">
	<c:out value="${partners.partnerType}"></c:out>
	<img class="openpopup" style="text-align:right;vertical-align:top;" src="${pageContext.request.contextPath}/images/rslogo_small_14.png" />
	</c:when>
	<c:otherwise>
	<c:out value="${partners.partnerType}"></c:out>
	</c:otherwise>
	</c:choose>
	</display:column>
	</c:otherwise>
	</c:choose>
</display:table>
</div>
</td>
<td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyDiv('${autocompleteDivId}','${partnerNameId}','${paertnerCodeId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
                                <c:if test="${not empty partners}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="false"/>
								</c:if>
								<c:if test="${empty partners}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="true"/>
								</c:if>
</s:form>