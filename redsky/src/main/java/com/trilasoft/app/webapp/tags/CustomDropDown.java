/**
 * For Customize DropDown in Redsky.
 * @Class Name	CustomDropDown
 * @Author      Dilip Kumar
 * @Date        03-Dec-2015
 */
package com.trilasoft.app.webapp.tags;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

public class CustomDropDown extends TagSupport {

	// ~ Instance fields
	// ================================================================================================
	private String listType;
	private String fieldValue;
	private Object list;
	private String attribute;
	public CustomDropDown(){
	}


	@SuppressWarnings("unchecked")
	public int doStartTag() throws JspException {
		JspWriter out=pageContext.getOut();
		try{
		out.println("<select "+attribute+">");
		if(attribute.indexOf("headerKey")>-1){
			out.println("<option value=''></option>");
		}
		String key="";
		String value="";
		
		if(listType.equalsIgnoreCase("map")){
			Map<String,String> list2=new LinkedHashMap<String,String>();
			list2=(Map<String, String>)list;
			for (Map.Entry<String,String> map : list2.entrySet()) {
				 key=map.getKey();
				 value=map.getValue();
				 if(key!=null && !key.equalsIgnoreCase("") && (key.split("~")[1].equalsIgnoreCase("Active") || fieldValue.equalsIgnoreCase(key.split("~")[0]))){
					if(fieldValue.equalsIgnoreCase(key.split("~")[0])){
						out.println("<option value='"+key.split("~")[0]+"' selected>"+value+"</option>");
					}else{
						out.println("<option value='"+key.split("~")[0]+"'>"+value+"</option>");
					}
				 }
			}			
		}else{
			List<String> list3=new LinkedList<String>();
			list3=(List<String>)list;
			for (Iterator<String> it = list3.iterator(); it.hasNext(); ) {
			    if (!it.hasNext()) {
					 key=it.next();
					 value=key;
					 if(key!=null && !key.equalsIgnoreCase("") && (key.split("~")[1].equalsIgnoreCase("Active") || fieldValue.equalsIgnoreCase(key.split("~")[0]))){
						if(fieldValue.equalsIgnoreCase(key.split("~")[0])){
							out.println("<option value='"+key.split("~")[0]+"' selected>"+value+"</option>");
						}else{
							out.println("<option value='"+key.split("~")[0]+"'>"+value+"</option>");
						}
					 }
			    }
			}			
		}
        out.println("</select>");
		}catch(Exception e){}
		return Tag.EVAL_BODY_INCLUDE;
	}

	public String getListType() {
		return listType;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public Object getList() {
		return list;
	}

	public void setList(Object list) {
		this.list = list;
	}

	public String getAttribute() {
		return attribute;
	}


	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}


	public String getFieldValue() {
		return fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
}
