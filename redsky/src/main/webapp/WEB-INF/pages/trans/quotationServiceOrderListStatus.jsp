<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Quotes List</title>   
    <meta name="heading" content="Quotes List"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
   <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['serviceOrderListForm'].elements['serviceOrder.shipNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.firstName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.lastName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.status'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.statusDate'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.job'].value = "";
}
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:80%;
!width:80%;
}
</style>
<style type="text/css">
.subcontenttabChild {
	background:#DCDCDC url(images/greylinebg.gif) repeat scroll 0% 0%;
	border:1px solid #DCDCDC;	
	color:#15428B;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-weight:bold;
	height:22px;
	padding:2px 3px 3px 5px;
	text-decoration:none;
}

</style>
</head>   
  <div id="Layer5" style="width:100%">

  
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>	 
<s:hidden name="fileID"  id= "fileID" value="%{customerFile.id}"/> 
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="serviceOrderListForm" action="searchServiceOrders" method="post" > 
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>  

<script language="javascript" type="text/javascript"> 
function requestedQuotes(targetElement,bookingAgentCode,billToCode,evt){ 
	   var bookingAgent=bookingAgentCode;
	  var billToCode=billToCode;
       var requestedAQP = evt.value;
       var targetElement=targetElement;
       if(requestedAQP=='A'){
   		var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookingAgent);
   		http45.open("GET", url, true);
   		http45.onreadystatechange = function(){handleHttpResponse45(targetElement,bookingAgent,billToCode,requestedAQP)};
   		http45.send(null);
       }else{
    	requestedQuotes111(targetElement,bookingAgent,billToCode,requestedAQP);
    	<c:if test="${customerFile.controlFlag !='C'}">
    	requestedQuotesCust(targetElement,requestedAQP);
    	</c:if>
       }
      	
	}
	function handleHttpResponse45(targetElement,bookingAgent,billToCode,requestedAQP){
		if (http45.readyState == 4) {
	          var results = http45.responseText
	          results = results.trim();
	          var res = results.split("@"); 
	       		if(res.size() >= 2){ 
	       			if(res[2] == 'Approved'){
	       				checkQuotationBillToCode(targetElement,bookingAgent,billToCode,requestedAQP);
	           		}else{
	           			alert("Quote cannot be accepted as selected partners have not been approved yet");	
	           		}			
	      		}else{
	           		alert("Booking Agent code not valid");
		   		}
	     
	  }
	} 
	function checkQuotationBillToCode(targetElement,bookingAgent,billToCode,requestedAQP){
	  var billToCode=billToCode;
	     //  var requestedAQP =requestedAQP;
	      //alert(requestedAQP)
			var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(billToCode);
			http46.open("GET", url, true);
			http46.onreadystatechange = function(){handleHttpResponse46(targetElement,bookingAgent,billToCode,requestedAQP)};
			http46.send(null);	
	
		}
	function handleHttpResponse46(targetElement,bookingAgent,billToCode,requestedAQP){
	       //var requestedAQP =requestedAQP;
	//alert(requestedAQP)	
		if (http46.readyState == 4) {
	          var results = http46.responseText
	          results = results.trim();
	          var res = results.split("@"); 
	       		if(res.size() >= 2){ 
	       			if(res[2] == 'Approved'){
	       				requestedQuotes111(targetElement,bookingAgent,billToCode,requestedAQP);
	       				<c:if test="${customerFile.controlFlag !='C'}">
	       				requestedQuotesCust(targetElement,requestedAQP);
	       		    	</c:if>
	           		}else{
	           			alert("Quote cannot be accepted as selected partners have not been approved yet");	
	           		}			
	      		}else{
	           		alert("Booking Agent code not valid");
		   		}
	  }
	}

  function requestedQuotes111(targetElement,bookingAgent,billToCode, requestedAQP){
	  var requestedAQP =requestedAQP;
       soid=targetElement;
	 var url = "quoteAcceptance.html?requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid);
	  	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }
function handleHttpResponse52(){
		if (http52.readyState == 4){
  			var results = http52.responseText
  			///alert("results"+results);
                }
}

 function requestedQuotesCust(targetElement, evt){
     var requestedAQP = evt;
    soid=targetElement;
	 var url = "quoteAcceptanceForQuote.html?requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id};
	  	http55.open("GET", url, true);
     	http55.onreadystatechange = handleHttpResponse5222;
     	http55.send(null);
    }
function handleHttpResponse5222(){
		if (http55.readyState == 4){
  			var results = http55.responseText
  		     }
}
var http45 = getHTTPObject45();
var http46 = getHTTPObject46();
var http55 = getHTTPObject52()
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	function getHTTPObject45()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	function getHTTPObject46()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}


</script> 
<table><tr>
<tr>
	<td align="left" colspan="2" class="subcontenttabChild"><b>Please mark the quote status below: </b></td>  
</tr>
<td>  
<div id="layer2" style="width:100%">
<div id="otabs">  <ul> <li><a class="current"><span>Quotes List</span></a></li> </ul> </div>
 <div class="spnblk">&nbsp;</div> 
<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList"  defaultsort="1" pagesize="10" style="width:99%;margin-top:1px;margin-left:5px;">   
<display:column property="shipNumber" sortable="true" title="Quotes List" href="editQuotationServiceOrderUpdate.html" paramId="id" paramProperty="id" style="width:75px"/>
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/> 
<display:column property="packingMode" sortable="true" titleKey="serviceOrder.packingMode" style="width:50px"/> 
<c:choose>
  <c:when test="${weightUnit == 'Lbs'}">
    <display:column property="estimateGrossWeight" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:when>
  <c:otherwise>
	 <display:column property="estimateGrossWeightKilo" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:otherwise>
 </c:choose>
<c:choose>
  <c:when test="${volumeUnit == 'Cft'}">
    <display:column property="estimateCubicFeet" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:when>
  <c:otherwise>
	 <display:column property="estimateCubicMtr" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:otherwise>
 </c:choose>

<display:column property="estimatedTotalExpense" sortable="true" title="Expense" style="width:50px"/>
<display:column property="estimatedTotalRevenue" sortable="true" title="Revenue" style="width:50px"/>
<display:column property="estimatedGrossMargin" sortable="true" title="Margin" style="width:20px"/>
<c:if test="${customerFile.controlFlag =='C'}">
<display:column title="Accept" style="width:50px"> 
<select name="quoteAccept${serviceOrderList.id}" onchange="requestedQuotes('${serviceOrderList.id}','${serviceOrderList.bookingAgentCode}','${serviceOrderList.billToCode}',this);" class="list-menu"/> 
<c:forEach var="chrms" items="${quoteAccept}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == serviceOrderList.quoteAccept}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
</c:forEach> 
</select>
</display:column>
</c:if>
<c:if test="${customerFile.controlFlag !='C'}">
<display:column title="Accept" style="width:50px"> 
<select name="quoteAccept${serviceOrderList.id}" onchange="requestedQuotes('${serviceOrderList.id}','${serviceOrderList.bookingAgentCode}','${serviceOrderList.billToCode}',this);" class="list-menu"/> 
<c:forEach var="chrms" items="${quoteAccept}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == serviceOrderList.quoteAccept}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
</c:forEach> 
								</select> 
</display:column>
</c:if>
 <display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
</td></tr>
<tr>
<td><input type="button"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="customerFile.id" />
<s:hidden name="requestedAQP" />
<c:if test="${not empty customerFile.sequenceNumber}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<c:if test="${empty customerFile.sequenceNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>
</div> 
</s:form>
<script type="text/javascript">   
</script>   
 
