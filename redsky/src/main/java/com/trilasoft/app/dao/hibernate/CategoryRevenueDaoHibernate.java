package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CategoryRevenueDao;
import com.trilasoft.app.model.CategoryRevenue;

public class CategoryRevenueDaoHibernate extends GenericDaoHibernate<CategoryRevenue,Long> implements CategoryRevenueDao{
	
	public CategoryRevenueDaoHibernate(){
		super(CategoryRevenue.class);
	}
	
	static String replaceWord(String original, String find, String replacement)
	{
		int i = original.indexOf(find);
	    if (i < 0) {
	        return original;  // return original if 'find' is not in it.
	    }
	  
	    String partBefore = original.substring(0, i);
	    String partAfter  = original.substring(i + find.length());
	  
	    return partBefore + replacement + partAfter;

	}
	
	public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from CategoryRevenue");
    }
	
	public List findById(Long id) {
    	return getHibernateTemplate().find("select id from CategoryRevenue where id=?",id);
    }

	public List searchCategoryRevenue(String secondSet, String totalCrew, String numberOfCrew1, String numberOfCrew2, String numberOfCrew3, String numberOfCrew4) {
		return getHibernateTemplate().find("from CategoryRevenue where secondSet like '" + secondSet +"%' AND totalCrew like '" +  replaceWord(totalCrew, "'", "''") + "%' AND numberOfCrew1 like '"+numberOfCrew1.replace("'", "''")+"%' AND numberOfCrew2 like '"+numberOfCrew2.replace("'", "''")+"%' AND numberOfCrew3 like '"+numberOfCrew3.replace("'", "''")+"%' AND numberOfCrew4 like '"+numberOfCrew4.replace("'", "''")+"%'");
	}

}
