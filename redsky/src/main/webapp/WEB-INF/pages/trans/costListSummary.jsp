<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Cost Element List</title> 
<meta name="heading" content="Cost Element List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-4px;
padding:0px;
text-align:right;
width:100%;
}
#otabs {
margin-bottom:0;
!margin-bottom:-20px;
margin-left:40px;
position:relative;
}
</style>

</head>

<s:form id="costListDataForm" name="costListDataForm" action="searchCostElementListPopUp.html?decorator=popup&popup=true" method="post" validate="true">
<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Cost Element</th>
		<th>Description</th>		
		<th></th>	
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:textfield name="tableN" required="true" cssClass="input-text" size="45"/>
					</td>
					<td align="left">
					    <s:textfield name="tableD" required="true" cssClass="input-text" size="45"/>
					</td>
					<td></td>					
				</tr>		
				<tr><td></td><td></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Cost Element List</span></a></li>
		  </ul>
	</div>
<div class="spnblk">&nbsp;</div>
	<display:table name="costElementListData" class="table" requestURI="" id="costElementListData" export="true" defaultsort="" pagesize="10" style="width:100%" >   
	<display:column title="Cost Element"><a href="#" onclick="getOwnerResult('CostData${costElementListData.id}','DescriptionData${costElementListData.id}','recGl${costElementListData.id}','payGl${costElementListData.id}')"/><c:out value="${costElementListData.costElement}"/></a>
	<s:hidden name="CostData${costElementListData.id}" value="${costElementListData.costElement}"  />
	<s:hidden name="DescriptionData${costElementListData.id}" value="${costElementListData.description}"  />
	<s:hidden name="recGl${costElementListData.id}" value="${costElementListData.recGl}"  />
	<s:hidden name="payGl${costElementListData.id}" value="${costElementListData.payGl}"  />
	</display:column>
	<display:column property="description"  sortable="true" title="Description"/>
   	</display:table>
	</div>
</s:form> 

<script language="javascript" type="text/javascript">
function getOwnerResult(costEl,desc,recGl,payGl)
{	
	var costE = document.forms['costListDataForm'].elements[costEl].value;
	var costD = document.forms['costListDataForm'].elements[desc].value;
	var rgl = document.forms['costListDataForm'].elements[recGl].value;
	var pgl = document.forms['costListDataForm'].elements[payGl].value;
	window.opener.document.forms['chargeForm'].elements['charges.costElement'].value=costE;
	window.opener.document.forms['chargeForm'].elements['charges.scostElementDescription'].value=costD;
	window.opener.document.forms['chargeForm'].elements['charges.gl'].value=rgl;
	window.opener.document.forms['chargeForm'].elements['charges.expGl'].value=pgl;
	window.close();
}
function clear_fields(){
	document.forms['costListDataForm'].elements['tableN'].value = '';
	document.forms['costListDataForm'].elements['tableD'].value = '';
 }
</script>