package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.MyFile;

public interface MyFileDao extends GenericDao<MyFile, Long> {

	public List getListByDocId(String fileId, String active, String secure ,String categorySearchValue);

	public List getListByCustomerNumber(String customerNumber, String active);

	public List getListByDesc(Long id);

	public List<MyFile> findByReleventDocs(String fileType, String fileId);

	public List<MyFile> findByDocs(String fileType, String fileId);
	
	public List findByDocs(String fileType, String fileId,String sessionCorpID);

	public List getMaxId(String sessionCorpID);

	public int updateMyfileStatus(String Status, Long ids, String updatedBy);

	public int updateMyfileAccStatus(String Status, Long ids, String updatedBy);

	public List getAccountFiles(String seqNum, String sessionCorpID);

	public int updateMyfilePartnerStatus(String status, Long ids, String updatedBy);

	public List searchMyFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID, String index, String duplicates);

	public List getIndexedList(String sessionCorpID, String updatedBy);

	public List getDocAccessControl(String fileType, String corpID);

	public void updateMyfileStripped(Long id);

	public List findSOName(String fileId, String sessionCorpID);

	public void updateActive(Long id, String updatedBy);

	public List getBasketList(Long fileId);

	public void recoverDoc(Long id, String updatedBy);

	public List getSequanceNum(String fileId, String sessionCorpID);

	public List getSequanceNumByInvNum(String fileId, String sessionCorpID);

	public List getSequanceNumByClm(String fileId, String sessionCorpID);

	public List getSequanceNumByTkt(String fileId, String sessionCorpID);

	public void getUpdateTransDate(String id,String updatedBy);

	public List getMapFolder(String fileType, String corpID);

	public void updateFileSize(String updateSize, Long id);

	public List getIsSecure(String fileType, String sessionCorpID);

	public List getWasteBasketAll(String corpId);

	public List searchWasteFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID);
	public List findAccountPortalFileList(String sequenceNumber,String sessionCorpID);
	public List getAllMyfile(String fileId, String sessionCorpID);
	public List getMyFileList(String linkFile, String sessionCorpID);
	public List getLinkedShipNumber(String shipNumber, String type, Boolean isBookingAgent, Boolean isNetworkAgent,	Boolean isOriginAgent, Boolean isSubOriginAgent,Boolean isDestAgent, Boolean isSubDestAgent);
	public List getParentShipNumber(String shipNumber);
	public List getListByCustomerNumberForTicket(String sequenceNumber,String shipNumber, String active);
	public void upDateNetworkLinkId(String networkLinkId, Long id);
	public List findlinkedIdList(String networkLinkId);
	public List findMyFilePortalList(String extSO, String sessionCorpID);
	public void removeNetworkLinkId(String networkLinkId, String fileId,String sessionCorpID);
	public List getLinkedAllShipNumber(String shipNumber, String checkFlagBA,String checkFlagNA, String checkFlagOA, String checkFlagSOA,String checkFlagDA, String checkFlagSDA, String type);
	public List getDefCheckedShipNumber(String shipNumber);
	public void updateAgentFlag(String networkLinkId, String checkAgent);
	public List getShipNumberList(String shipNumber);
	public List findTransDocSystemDefault(String corpID);
	public List searchMaxSizeFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID, String docName, Integer docSize,String index);
	public String getAccountLinePayingStatus(String accountlineShipNumber,String accountlineVendorCode,String sessionCorpID);
	public List getcompressListId(Long fid);
	public List findShipNumberBySequenceNumer(String sequenceNumber,String sessionCorpID,Boolean isNetworkRecord);
	public void updateNetworkLinkIdCheck(String networkLinkId, String linkedSeqNum,String valueTarget,String FileId,Long id,String checkValue);
	public List getLinkedId(String networkLinkId, String sessionCorpID);
	public TreeMap<String, List<MyFile>> getdocTypeMapList(String shipNumber,String active, String secure, String categorySearchValue,String tempSortOrder,String sortingOrder);
	public TreeMap<String, List<MyFile>> getListByCustomerNumberGrpByType(String sequenceNumber, String active,String tempSortOrder,String sortingOrder);
	public int updateMyfileServiceProviderStatus(String Status, Long ids, String updatedBy);
	public List recipientWithEmailStatus(String jobNumber,String noteFor ,String sessionCorpID,String fileId);
	public List fileTypeForPartnerCode(String corpID);
	public Long findByLocation(String location,String sessionCorpID);
	public int updateInvoiceAttachment(String Status, Long ids, String updatedBy);
	public List accountPortalFiles(String securityQuery, String corpID);
	//13674 - Create dashboard
	public List getDocumentCategoryList(String shipNumber,String documentCategory,String sessionCorpID);
	public List getListByDesc(String shipNumber,String sessionCorpID);
	public List findFileLocation(String openFrom, String closeFrom, String companyCorpID);
	//End DashBoard
	
	public int updateMyfileDriverPortalStatus(String Status, Long ids, String updatedBy);
	public TreeMap<String, List<MyFile>> getDriverDocTypeMapList(String shipNumber,String active, String secure, String categorySearchValue,String tempSortOrder,String sortingOrder);
	public List getdocAgentTypeMapList(String shipNumber,String vendorCode,String sessionCorpID) ;
	public List fileTypeForPayableProcessing(String sessionCorpID);	
	
	public List getDocumentCode(String fileType, String sessionCorpID);

}
