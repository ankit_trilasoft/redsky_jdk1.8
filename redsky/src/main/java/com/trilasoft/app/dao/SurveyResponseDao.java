package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SurveyResponse;

public interface SurveyResponseDao extends GenericDao<SurveyResponse, Long> {

}
