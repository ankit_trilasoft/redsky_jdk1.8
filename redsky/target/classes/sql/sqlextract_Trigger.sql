delimiter $$

CREATE trigger redsky.trigger_add_history_sqlextract
BEFORE UPDATE on redsky.sqlextract
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";




   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'sqlextract.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;




   IF (OLD.fileName <> NEW.fileName ) THEN
      select CONCAT(fieldNameList,'sqlextract.fileName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileName,'~') into newValueList;
  END IF;





   IF (OLD.fileType <> NEW.fileType ) THEN
      select CONCAT(fieldNameList,'sqlextract.fileType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileType,'~') into newValueList;
  END IF;



   IF (OLD.sqlText <> NEW.sqlText ) THEN
      select CONCAT(fieldNameList,'sqlextract.sqlText~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sqlText,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sqlText,'~') into newValueList;
  END IF;






   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'sqlextract.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;








   IF (OLD.tested <> NEW.tested ) THEN
      select CONCAT(fieldNameList,'sqlextract.tested~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tested,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tested,'~') into newValueList;
  END IF;




   IF (OLD.whereClause1 <> NEW.whereClause1 ) THEN
      select CONCAT(fieldNameList,'sqlextract.whereClause1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.whereClause1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.whereClause1,'~') into newValueList;
  END IF;





   IF (OLD.whereClause2 <> NEW.whereClause2 ) THEN
      select CONCAT(fieldNameList,'sqlextract.whereClause2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.whereClause2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.whereClause2,'~') into newValueList;
  END IF;






   IF (OLD.whereClause3 <> NEW.whereClause3 ) THEN
      select CONCAT(fieldNameList,'sqlextract.whereClause3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.whereClause3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.whereClause3,'~') into newValueList;
  END IF;







   IF (OLD.whereClause4 <> NEW.whereClause4 ) THEN
      select CONCAT(fieldNameList,'sqlextract.whereClause4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.whereClause4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.whereClause4,'~') into newValueList;
  END IF;







   IF (OLD.whereClause5 <> NEW.whereClause5 ) THEN
      select CONCAT(fieldNameList,'sqlextract.whereClause5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.whereClause5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.whereClause5,'~') into newValueList;
  END IF;










   IF (OLD.orderBy <> NEW.orderBy ) THEN
      select CONCAT(fieldNameList,'sqlextract.orderBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.orderBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.orderBy,'~') into newValueList;
  END IF;









   IF (OLD.groupBy <> NEW.groupBy ) THEN
      select CONCAT(fieldNameList,'sqlextract.groupBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.groupBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.groupBy,'~') into newValueList;
  END IF;






   IF (OLD.displayLabel1 <> NEW.displayLabel1 ) THEN
      select CONCAT(fieldNameList,'sqlextract.displayLabel1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayLabel1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayLabel1,'~') into newValueList;
  END IF;





   IF (OLD.displayLabel2 <> NEW.displayLabel2 ) THEN
      select CONCAT(fieldNameList,'sqlextract.displayLabel2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayLabel2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayLabel2,'~') into newValueList;
  END IF;







   IF (OLD.displayLabel3 <> NEW.displayLabel3 ) THEN
      select CONCAT(fieldNameList,'sqlextract.displayLabel3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayLabel3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayLabel3,'~') into newValueList;
  END IF;









   IF (OLD.displayLabel4 <> NEW.displayLabel4 ) THEN
      select CONCAT(fieldNameList,'sqlextract.displayLabel4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayLabel4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayLabel4,'~') into newValueList;
  END IF;








   IF (OLD.displayLabel5 <> NEW.displayLabel5 ) THEN
      select CONCAT(fieldNameList,'sqlextract.displayLabel5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayLabel5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayLabel5,'~') into newValueList;
  END IF;







   IF (OLD.defaultCondition1 <> NEW.defaultCondition1 ) THEN
      select CONCAT(fieldNameList,'sqlextract.defaultCondition1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultCondition1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultCondition1,'~') into newValueList;
  END IF;






   IF (OLD.defaultCondition2 <> NEW.defaultCondition2 ) THEN
      select CONCAT(fieldNameList,'sqlextract.defaultCondition2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultCondition2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultCondition2,'~') into newValueList;
  END IF;








   IF (OLD.defaultCondition3 <> NEW.defaultCondition3 ) THEN
      select CONCAT(fieldNameList,'sqlextract.defaultCondition3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultCondition3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultCondition3,'~') into newValueList;
  END IF;





   IF (OLD.defaultCondition4 <> NEW.defaultCondition4 ) THEN
      select CONCAT(fieldNameList,'sqlextract.defaultCondition4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultCondition4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultCondition4,'~') into newValueList;
  END IF;






   IF (OLD.defaultCondition5 <> NEW.defaultCondition5 ) THEN
      select CONCAT(fieldNameList,'sqlextract.defaultCondition5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultCondition5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultCondition5,'~') into newValueList;
  END IF;







   IF (OLD.email <> NEW.email ) THEN
      select CONCAT(fieldNameList,'sqlextract.email~') into fieldNameList;
      select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
      select CONCAT(newValueList,NEW.email,'~') into newValueList;
  END IF;






   IF (OLD.isdateWhere1 <> NEW.isdateWhere1 ) THEN
       select CONCAT(fieldNameList,'sqlextract.isdateWhere1~') into fieldNameList;
       IF(OLD.isdateWhere1 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isdateWhere1 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isdateWhere1 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isdateWhere1 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

   IF (OLD.isdateWhere2 <> NEW.isdateWhere2 ) THEN
       select CONCAT(fieldNameList,'sqlextract.isdateWhere2~') into fieldNameList;
       IF(OLD.isdateWhere2 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isdateWhere2 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isdateWhere2 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isdateWhere2 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

   IF (OLD.isdateWhere3 <> NEW.isdateWhere3 ) THEN
       select CONCAT(fieldNameList,'sqlextract.isdateWhere3~') into fieldNameList;
       IF(OLD.isdateWhere3 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isdateWhere3 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isdateWhere3 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isdateWhere3 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

   IF (OLD.isdateWhere4 <> NEW.isdateWhere4 ) THEN
       select CONCAT(fieldNameList,'sqlextract.isdateWhere4~') into fieldNameList;
       IF(OLD.isdateWhere4 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isdateWhere4 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isdateWhere4 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isdateWhere4 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

   IF (OLD.isdateWhere5 <> NEW.isdateWhere5 ) THEN
       select CONCAT(fieldNameList,'sqlextract.isdateWhere5~') into fieldNameList;
       IF(OLD.isdateWhere5 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isdateWhere5 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isdateWhere5 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isdateWhere5 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



 CALL add_tblHistory (OLD.id,"sqlextract", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;