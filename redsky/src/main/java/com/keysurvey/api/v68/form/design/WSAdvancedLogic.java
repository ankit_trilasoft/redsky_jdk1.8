/**
 * WSAdvancedLogic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSAdvancedLogic  extends com.keysurvey.api.v68.form.design.WSLogic  implements java.io.Serializable {
    private java.lang.String conditionalExpression;

    private long orderId;

    private long questionId;

    public WSAdvancedLogic() {
    }

    public WSAdvancedLogic(
           long versionId,
           java.lang.String conditionalExpression,
           long orderId,
           long questionId) {
        super(
            versionId);
        this.conditionalExpression = conditionalExpression;
        this.orderId = orderId;
        this.questionId = questionId;
    }


    /**
     * Gets the conditionalExpression value for this WSAdvancedLogic.
     * 
     * @return conditionalExpression
     */
    public java.lang.String getConditionalExpression() {
        return conditionalExpression;
    }


    /**
     * Sets the conditionalExpression value for this WSAdvancedLogic.
     * 
     * @param conditionalExpression
     */
    public void setConditionalExpression(java.lang.String conditionalExpression) {
        this.conditionalExpression = conditionalExpression;
    }


    /**
     * Gets the orderId value for this WSAdvancedLogic.
     * 
     * @return orderId
     */
    public long getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this WSAdvancedLogic.
     * 
     * @param orderId
     */
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the questionId value for this WSAdvancedLogic.
     * 
     * @return questionId
     */
    public long getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this WSAdvancedLogic.
     * 
     * @param questionId
     */
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSAdvancedLogic)) return false;
        WSAdvancedLogic other = (WSAdvancedLogic) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.conditionalExpression==null && other.getConditionalExpression()==null) || 
             (this.conditionalExpression!=null &&
              this.conditionalExpression.equals(other.getConditionalExpression()))) &&
            this.orderId == other.getOrderId() &&
            this.questionId == other.getQuestionId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConditionalExpression() != null) {
            _hashCode += getConditionalExpression().hashCode();
        }
        _hashCode += new Long(getOrderId()).hashCode();
        _hashCode += new Long(getQuestionId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSAdvancedLogic.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAdvancedLogic"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conditionalExpression");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conditionalExpression"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
