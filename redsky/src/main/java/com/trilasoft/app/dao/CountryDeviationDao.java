package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CountryDeviation;

public interface CountryDeviationDao extends GenericDao<CountryDeviation, Long> {
	public List findDeviation(String charge, String deviationType);
	public List findMaximumId();
	public void delSelectedDevitaion(String delId);
	public List findAllDeviation(String chargeId, String sessionCorpID,	String contract);

}
