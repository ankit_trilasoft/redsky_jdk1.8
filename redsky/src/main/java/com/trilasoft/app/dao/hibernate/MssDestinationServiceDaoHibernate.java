package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.MssDestinationServiceDao;
import com.trilasoft.app.model.MssDestinationService;

public class MssDestinationServiceDaoHibernate extends GenericDaoHibernate<MssDestinationService, Long>implements MssDestinationServiceDao{
		
	public MssDestinationServiceDaoHibernate() {
		super(MssDestinationService.class);
	}
}
