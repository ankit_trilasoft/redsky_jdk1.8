package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RefCrate;

public interface RefCrateManager extends GenericManager<RefCrate, Long> {
	public List<RefCrate> findByLastName(String lastName);
	public List refCrateList(String corpID);

}
