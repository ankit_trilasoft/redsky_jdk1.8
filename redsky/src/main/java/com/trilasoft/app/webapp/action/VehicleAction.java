/**
 * @Class Name  Vehicle Action
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;

public class VehicleAction extends BaseAction implements Preparable{
    
    private ServiceOrder serviceOrder;
    private CustomerFile customerFile;
    private Container container;
    private TrackingStatus trackingStatus;
    private  Miscellaneous  miscellaneous;
    private Company company;
	private CompanyManager companyManager;
    private RefMasterManager refMasterManager;
	 private ServiceOrderManager serviceOrderManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager ;
	private VehicleManager vehicleManager;
	private CustomerFileManager customerFileManager;
	private ContainerManager containerManager;
	private NotesManager notesManager;
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	private String sessionCorpID;
	private String gotoPageString;
	private String validateFormNav;
	private String idNumber;
    private String countVehicleNotes;
    private Long idMax;
    private Long sid;
    private Long soId;
    private Long autoId;
    private List refMasters;
    private List vehicles;  
    private static List containerNumberList;
    private static List containerNumberListVehicle;
    private static List weightunits;
    private static List volumeunits;
   // private static List lengthunits;
    private String hitFlag="";
    private static Map<String, String> EPAClass;
    private Map<String,String> vehicleTypeList;
    private String shipSize;
	private String minShip;
	private String countShip;
	private List customerSO;
	
	private CustomManager customManager;
	private String countBondedGoods;
	
	private String maxChild;
	private String countChild;
	private String minChild;
	private Long soIdNum;
	private Long sidNum;
	private long tempVehicle;
	private List vehicleSO;
	private String voxmeIntergartionFlag;
	private String  usertype;
	private Boolean surveyTab = new Boolean(false);
	private Map<String,String> valuationCurrencyList;
	private Map<String,String> valuationCurrencyList_isactive;
	private String vehicleStatus;
	private Map<String,String> vehicleTypeList_isactive;
	private static Map<String, String> EPAClass_isactive;
	private Map<String,String> lengthunits;
	private String oiJobList;
	
		 //  A Method to Authenticate User, calling from main menu.
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(VehicleAction.class);

    
    public VehicleAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
	}
    
//  End of Method.
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
    	company=companyManager.findByCorpID(sessionCorpID).get(0);
       if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
	   }
		if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		}
		try {
			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
				 Long soId = null;
				 try {
					 soId = new Long(getRequest().getParameter("sid").toString());
					} catch (Exception e) {
						soId = new Long(getRequest().getParameter("id").toString());
						e.printStackTrace();
					}
			 if(soId!=null){
				 ServiceOrder agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(soId);
				 String corpId=agentServiceOrderCombo.getCorpID();
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(soId);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
//  Method used to save through tabs.

    
    public String saveOnTabChange() throws Exception {
    	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	    validateFormNav = "OK";
            String s = save();
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return gotoPageString;
    }
    
//  End of Method.
    
//  A Method to get the number of container.
    
    public List containerNumberList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
    	containerNumberList=vehicleManager.getContainerNumber(vehicle.getShipNumber());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return containerNumberList;
    }
   
    
    
//  End of Method.
    
//  A Method to get the list.
	private Billing billing;
	private BillingManager billingManager;  
    public String vehiclelist() {   
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	 serviceOrder = serviceOrderManager.get(id);
    	 getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    	 trackingStatus=trackingStatusManager.get(id);
 		 miscellaneous = miscellaneousManager.get(id); 
 		billing = billingManager.get(id);
 		 customerFile=serviceOrder.getCustomerFile();
   	  vehicles = new ArrayList( serviceOrder.getVehicles());
    	shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	    minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
       countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
       if(!customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0)!=null){
           countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0).toString();
       }
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
   	  return SUCCESS;
    }
    
//  End of Method.
    
//  Method for editing and adding the record.
    private ToDoRuleManager toDoRuleManager;
    private CustomerFileAction customerFileAction;
    private void postSave(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	toDoRuleManager.executeRules("vehicle", vehicle.getId(),sessionCorpID);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    }
 
 	public String edit() {
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
 		weightunits = new ArrayList();
      	weightunits.add("Lbs");
      	weightunits.add("Kgs");
      	volumeunits = new ArrayList();
      	volumeunits.add("Cft");
      	volumeunits.add("Cbm");
        lengthunits=new LinkedHashMap<String,String>();
        List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,"'LENGTHUNITSVECHILE'");
        for (RefMasterDTO  refObj : allParamValue) {
         	   if(refObj.getParameter().trim().equalsIgnoreCase("LENGTHUNITSVECHILE")){
         	   lengthunits.put(refObj.getCode().trim(),refObj.getDescription().trim());}}
     	//getComboList(sessionCorpID);
        if (id != null) {   
			          vehicle = vehicleManager.get(id);
			          serviceOrder = vehicle.getServiceOrder();
			          getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			          miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			          trackingStatus=trackingStatusManager.get(serviceOrder.getId());
			          try {
			      			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
			      				 
			      			 if(serviceOrder!=null){
			      				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(serviceOrder.getCorpID()); 
			      				 if(serviceOrder.getIsNetworkRecord() && (trackingStatus.getSoNetworkGroup() || chkCompanySurveyFlag)){
			      						surveyTab= true;
			      				}
			      				}
			      			 }
			      		 } catch (Exception e) {
			      			e.printStackTrace();
			      		 }
			          billing = billingManager.get(serviceOrder.getId());
			          customerFile = serviceOrder.getCustomerFile();
			          maxChild = vehicleManager.findMaximumChild(serviceOrder.getShipNumber()).get(0).toString();
			            minChild =  vehicleManager.findMinimumChild(serviceOrder.getShipNumber()).get(0).toString();
			            countChild =  vehicleManager.findCountChild(serviceOrder.getShipNumber()).get(0).toString();
			          } else {   
        	    	  vehicle = new Vehicle();
        	    	  serviceOrder=serviceOrderManager.get(sid);
        	    	  getRequest().setAttribute("soLastName",serviceOrder.getLastName());
        	    	  miscellaneous = miscellaneousManager.get(sid);
      	        	  trackingStatus=trackingStatusManager.get(sid);
      	        	  billing = billingManager.get(sid);
      	        	  customerFile = serviceOrder.getCustomerFile();
      	        	 
        	    	  vehicle.setShipNumber(serviceOrder.getShipNumber());
        	    	  vehicle .setCreatedOn(new Date());
        	    	  vehicle .setUpdatedOn(new Date());
        	    	  vehicle.setCorpID(serviceOrder.getCorpID());
        	    	  vehicle.setUnit1("Lbs");
        	    	  vehicle.setUnit2("Cft");
        	    	  //vehicle.setUnit3("Ft");
        }
        shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        if(!customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0)!=null){
            countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0).toString();
        }
        getNotesForIconChange();
        containerNumberList= containerNumberList();
        if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
    		
    	//	containerNumberList.clear();
    		containerNumberList= new ArrayList();
    		
    	}
   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }   
    
//  End of Method.
    
 	public String updateVehicleStatus()
	{
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
 		  	String key = "Vehicle has been deleted successfully.";   
        saveMessage(getText(key));
        try{
        	vehicle=vehicleManager.get(id);
            serviceOrder=vehicle.getServiceOrder();
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			deleteVehicle(serviceOrderRecords,vehicle.getIdNumber(),vehicleStatus);
          }
          }catch(Exception ex){
        	  ex.printStackTrace();
          }
		vehicleManager.updateDeleteStatus(id);
		id=sid;
		vehiclelist();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
 	private void deleteVehicle(List<Object> serviceOrderRecords,String idNumber,String vehicleStatus) {
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 			
 		Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		vehicleManager.deleteNetworkVehicle(serviceOrder.getId(),idNumber,vehicleStatus);
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	}

	@SkipValidation 
    public String vehicleNext(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		tempVehicle=Long.parseLong((vehicleManager.goNextSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;   
    }
    @SkipValidation 
    public String vehiclePrev(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	tempVehicle=Long.parseLong((vehicleManager.goPrevSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    } 
      
    @SkipValidation 
    public String vehicleSO(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	vehicleSO=vehicleManager.goSOChild(sidNum,sessionCorpID,soIdNum);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
 	
//  Method getting the count of notes according to which the icon color changes in form
 	
	@SkipValidation 
    public String getNotesForIconChange() { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List m = notesManager.countForVehicleNotes(vehicle.getId());
    	if( m.isEmpty() ){
			countVehicleNotes = "0";
		}else {
			countVehicleNotes="";
			if((m).get(0)!=null){
			countVehicleNotes = ((m).get(0)).toString() ;
			}
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }  
	
//  End of Method.
    
//  Method used to save and update the record.
	
    public String save() throws Exception {  
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	  boolean isNew = (vehicle.getId() == null);   
        vehicle.setServiceOrder(serviceOrder);
        miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
        vehicle.setUpdatedOn(new Date());
        lengthunits=new LinkedHashMap<String,String>();
        List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,"'LENGTHUNITSVECHILE'");
        for (RefMasterDTO  refObj : allParamValue) {
         	   if(refObj.getParameter().trim().equalsIgnoreCase("LENGTHUNITSVECHILE")){
         	   lengthunits.put(refObj.getCode().trim(),refObj.getDescription().trim());}}
        //vehicle.setCorpID(sessionCorpID);
        vehicle.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	vehicle.setCreatedOn(new Date());
        }
        List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
	       if ( maxIdNumber.get(0) == null ) 
	        {          
	      	 idNumber = "01";
         }else {
	          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	        	if((autoId.toString()).length() == 1) 
	        		{
	        			idNumber = "0"+(autoId.toString());
	        		}else {
	        			 idNumber=autoId.toString();
	        			        
	        			}
	        	} 
	       if(isNew){
	    	   vehicle.setIdNumber(idNumber);
	       }else{
	    	   vehicle.setIdNumber(vehicle.getIdNumber());
	       }
	          
        
        vehicle=vehicleManager.save(vehicle); 
        if(isNew && sessionCorpID.equalsIgnoreCase("UGWW")){
        	vehicle.setUgwIntId(vehicle.getId().toString());
        	vehicle=vehicleManager.save(vehicle); 
       	}
        serviceOrder=vehicle.getServiceOrder();
        try{
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			synchornizeVehicle(serviceOrderRecords,vehicle,isNew,serviceOrder);
			
        }
        
        /*
         * Bug 15084 - Changes on the EDI Report for ONLY customers using EDI Reporting
         */
        if(serviceOrder.getCommodity().equals("AUTO")){
        if(!vehicleManager.getWeight(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
        	vehicleManager.updateMisc(vehicle.getShipNumber(),vehicleManager.getWeight(serviceOrder.getShipNumber()).get(0).toString(),vehicleManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),vehicleManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
 	   }
 	   if(!vehicleManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
 		  vehicleManager.updateMiscVolume(vehicle.getShipNumber(), vehicleManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),vehicleManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
 	   }
        }
 	   /*End
        
        /*Long StartTime = System.currentTimeMillis();
		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
			if(agentShipNumber!=null && !agentShipNumber.equals(""))
				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
   			
   			}
			
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
   
        
        }catch(Exception ex){
			 ex.printStackTrace();
		}
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "vehicle.added" : "vehicle.updated";   
        saveMessage(getText(key)); 
        }
        
        if (!isNew) { 
        	serviceOrder=serviceOrderManager.get(serviceOrder.getId());
        	vehicles = new ArrayList( serviceOrder.getVehicles());
      	   	getSession().setAttribute("vehicles", vehicles);
      	   	if(sessionCorpID.equalsIgnoreCase("SSCW")){
      	   	try
			{
				//postSave();
			}catch(Exception ex)
			{
				 ex.printStackTrace();
			}
      	   	}
      	    hitFlag="1";
            return SUCCESS;   
            
        } else {   
        	idMax = Long.parseLong(vehicleManager.findMaxId().get(0).toString());
            vehicle = vehicleManager.get(idMax);
        }
        serviceOrder=serviceOrderManager.get(serviceOrder.getId());
        vehicles = new ArrayList( serviceOrder.getVehicles());
  	   	getSession().setAttribute("vehicles", vehicles);
  	   	if(sessionCorpID.equalsIgnoreCase("SSCW")){
  	   	try
		{
			//postSave();
		}catch(Exception ex)
		{
			 ex.printStackTrace();
		}
  	   	}
  	     hitFlag="1";
  	   	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS; 
    }
    
private void synchornizeVehicle(List<Object> serviceOrderRecords,Vehicle vehicle, boolean isNew ,ServiceOrder serviceOrderold) {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
	Iterator  it=serviceOrderRecords.iterator();
	while(it.hasNext()){
		ServiceOrder serviceOrder=(ServiceOrder)it.next();
		try{
			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){	
			if(isNew){
				Vehicle vehicleNew= new Vehicle();
				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(vehicleNew, vehicle);
				
				vehicleNew.setId(null);
				vehicleNew.setCorpID(serviceOrder.getCorpID());
				vehicleNew.setShipNumber(serviceOrder.getShipNumber());
				vehicleNew.setSequenceNumber(serviceOrder.getSequenceNumber());
				vehicleNew.setServiceOrder(serviceOrder);
				vehicleNew.setServiceOrderId(serviceOrder.getId());
				vehicleNew.setCreatedBy("Networking");
				vehicleNew.setUpdatedBy("Networking");
				vehicleNew.setCreatedOn(new Date());
				vehicleNew.setUpdatedOn(new Date()); 
				// added to fixed issue of merge functionality in 2 step linking
				try{
				 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
			       if ( maxIdNumber.get(0) == null ) 
			        {          
			      	 idNumber = "01";
		         }else {
			          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
			        	if((autoId.toString()).length() == 1) 
			        		{
			        			idNumber = "0"+(autoId.toString());
			        		}else {
			        			 idNumber=autoId.toString();
			        			        
			        			}
			        	} 
			    }catch(Exception e){
					e.printStackTrace();
				}
			    vehicleNew.setIdNumber(idNumber);
				vehicleNew = vehicleManager.save(vehicleNew);
				
				 if(vehicleNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 vehicleNew.setUgwIntId(vehicleNew.getId().toString());
					 vehicleNew= vehicleManager.save(vehicleNew);
					}
				
				 
			}else{
				Vehicle vehicleTo= vehicleManager.getForOtherCorpid(vehicleManager.findRemoteVehicle(vehicle.getIdNumber(),serviceOrder.getId()));  
    			Long id=vehicleTo.getId();
    			String createdBy=vehicleTo.getCreatedBy();
    			Date createdOn=vehicleTo.getCreatedOn();
    			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(vehicleTo, vehicle);
				
				vehicleTo.setId(id);
				vehicleTo.setCreatedBy(createdBy);
				vehicleTo.setCreatedOn(createdOn);
				vehicleTo.setCorpID(serviceOrder.getCorpID());
				vehicleTo.setShipNumber(serviceOrder.getShipNumber());
				vehicleTo.setSequenceNumber(serviceOrder.getSequenceNumber());
				vehicleTo.setServiceOrder(serviceOrder);
				vehicleTo.setServiceOrderId(serviceOrder.getId());
				vehicleTo.setUpdatedBy(vehicle.getCorpID()+":"+getRequest().getRemoteUser());
				vehicleTo.setUpdatedOn(new Date()); 
				/*// added to fixed issue of merge functionality in 2 step linking
				try{
				 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
			       if ( maxIdNumber.get(0) == null ) 
			        {          
			      	 idNumber = "01";
		         }else {
			          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
			        	if((autoId.toString()).length() == 1) 
			        		{
			        			idNumber = "0"+(autoId.toString());
			        		}else {
			        			 idNumber=autoId.toString();
			        			        
			        			}
			        	} 
			    }catch(Exception e){
					System.out.println("Error in id number");
				}
			    vehicleTo.setIdNumber(idNumber);*/
				vehicleTo =vehicleManager.save(vehicleTo);
				 if(vehicleTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 vehicleTo.setUgwIntId(vehicleTo.getId().toString());
					 vehicleTo= vehicleManager.save(vehicleTo);
				}
				
			}
			}
		}catch(Exception ex){
				
			 ex.printStackTrace();
		}
	}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
	}

private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
		Iterator it =linkedShipNumber.iterator();
		while(it.hasNext()){
			String shipNumber= it.next().toString();
			if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
    			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    			recordList.add(serviceOrderRemote);
    		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return recordList;
	}

private List findLinkedShipNumber(ServiceOrder serviceOrder) {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List linkedShipNumberList= new ArrayList();
		if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		}else{
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return linkedShipNumberList;
	}

	//  End of Method.
    public String getComboList(String corpId){
    	EPAClass = refMasterManager.findByParameter(corpId, "EPAClass");
    	vehicleTypeList = refMasterManager.findByParameter(corpId, "VEHICLETYPE");
    	valuationCurrencyList=refMasterManager.findByParameterWithCurrency("TSFT", "CURRENCY");
    	LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
   	   String parameters="'VEHICLETYPE','CURRENCY','EPAClass'";
   	 valuationCurrencyList_isactive  = new LinkedHashMap<String, String>();
   	vehicleTypeList_isactive= new LinkedHashMap<String, String>();
    EPAClass_isactive=new LinkedHashMap<String,String>();
  	    List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue("TSFT",parameters);
  	  List <RefMasterDTO> allParamValue1 = refMasterManager.getAllParameterValue(corpId,parameters);
  	   for (RefMasterDTO  refObj : allParamValue) {
  	    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("CURRENCY")){
  	    	    		 		valuationCurrencyList_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());}}
  	 for (RefMasterDTO  refObj : allParamValue1) {
		 	if(refObj.getParameter().trim().equalsIgnoreCase("VEHICLETYPE")){
		 		vehicleTypeList_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		
		 	}
		 	if(refObj.getParameter().trim().equalsIgnoreCase("EPAClass")){
		 		
		 		EPAClass_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());	
		 	}}
    	return SUCCESS;
    }
    
    @SkipValidation
	public String getVehicleDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			containerNumberListVehicle=vehicleManager.getContainerNumber(serviceOrder.getShipNumber());
			vehicles = new ArrayList( serviceOrder.getVehicles());
		} catch (Exception e) {	
			e.printStackTrace();
	    	 return CANCEL;
		}		
		return SUCCESS;
	}
    
    @SkipValidation
	public String deleteVehicleDetailsAjax(){
		try {
			vehicle=vehicleManager.get(id);
			//vehicleManager.remove(id);
			vehicleManager.updateStatus(id,vehicleStatus,getRequest().getRemoteUser());
            serviceOrder=vehicle.getServiceOrder();
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			deleteVehicle(serviceOrderRecords,vehicle.getIdNumber(),vehicleStatus);
          }
		} catch (Exception e) {	
			e.printStackTrace();
	    	 return CANCEL;
		}		
		return SUCCESS;
	}
    
    public void setSid(Long sid) {   
	       this.sid = sid;   
	   }
    public String getValidateFormNav() {
        return validateFormNav;
        }
    public void setValidateFormNav(String validateFormNav) {
        this.validateFormNav = validateFormNav;
        }
    public String getGotoPageString() {
        return gotoPageString;
    }
    public void setGotoPageString(String gotoPageString) {
        this.gotoPageString = gotoPageString;
    }
    public  List getWeightunits() {
		return weightunits;
	}
    public  List getVolumeunits() {
		return volumeunits;
	}
  
    private Vehicle vehicle;   
    private Long id;   
      
    public void setId(Long id) {   
        this.id = id;   
    }   
    public Vehicle getVehicle() {   
        return vehicle;   
    }   
    public void setVehicle(Vehicle vehicle) {   
        this.vehicle = vehicle;   
    }   
    public ServiceOrder getServiceOrder() {   
        return serviceOrder;   
    }   
    public void setServiceOrder(ServiceOrder serviceOrder  ) {   
        this.serviceOrder = serviceOrder;   
    }    
    public void setContainerManager(ContainerManager containerManager) {
        this.containerManager = containerManager;
    }
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }
    public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
        this.serviceOrderManager = serviceOrderManager;
    }
    public List getVehicles() {   
        return vehicles;   
    }
	public String getCountVehicleNotes() {
		return countVehicleNotes;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}

	/**
	 * @return the containerNumberList
	 */
	public static List getContainerNumberList() {
		return containerNumberList;
	}

	/**
	 * @param containerNumberList the containerNumberList to set
	 */
	public static void setContainerNumberList(List containerNumberList) {
		VehicleAction.containerNumberList = containerNumberList;
	}

	public static Map<String, String> getEPAClass() {
		return EPAClass;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}

	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getSoId() {
		return soId;
	}

	public void setSoId(Long soId) {
		this.soId = soId;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public List getCustomerSO() {
		return customerSO;
	}

	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}

	public String getCountChild() {
		return countChild;
	}

	public void setCountChild(String countChild) {
		this.countChild = countChild;
	}

	public String getMaxChild() {
		return maxChild;
	}

	public void setMaxChild(String maxChild) {
		this.maxChild = maxChild;
	}

	public String getMinChild() {
		return minChild;
	}

	public void setMinChild(String minChild) {
		this.minChild = minChild;
	}

	public Long getSidNum() {
		return sidNum;
	}

	public void setSidNum(Long sidNum) {
		this.sidNum = sidNum;
	}

	public Long getSoIdNum() {
		return soIdNum;
	}

	public void setSoIdNum(Long soIdNum) {
		this.soIdNum = soIdNum;
	}

	public long getTempVehicle() {
		return tempVehicle;
	}

	public void setTempVehicle(long tempVehicle) {
		this.tempVehicle = tempVehicle;
	}

	public List getVehicleSO() {
		return vehicleSO;
	}

	public void setVehicleSO(List vehicleSO) {
		this.vehicleSO = vehicleSO;
	}

	public String getCountBondedGoods() {
		return countBondedGoods;
	}

	public void setCountBondedGoods(String countBondedGoods) {
		this.countBondedGoods = countBondedGoods;
	}

	public CustomManager getCustomManager() {
		return customManager;
	}

	public void setCustomManager(CustomManager customManager) {
		this.customManager = customManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	

	public void setCustomerFileAction(CustomerFileAction customerFileAction) {
		this.customerFileAction = customerFileAction;
	}

	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}

	public Map<String, String> getVehicleTypeList() {
		return vehicleTypeList;
	}

	public void setVehicleTypeList(Map<String, String> vehicleTypeList) {
		this.vehicleTypeList = vehicleTypeList;
	}

	public Company getCompany() {
		return company;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Boolean getSurveyTab() {
		return surveyTab;
	}

	public void setSurveyTab(Boolean surveyTab) {
		this.surveyTab = surveyTab;
	}

	public static List getContainerNumberListVehicle() {
		return containerNumberListVehicle;
	}

	public static void setContainerNumberListVehicle(List containerNumberListVehicle) {
		VehicleAction.containerNumberListVehicle = containerNumberListVehicle;
	}

	public Map<String, String> getValuationCurrencyList() {
		return valuationCurrencyList;
	}

	public void setValuationCurrencyList(Map<String, String> valuationCurrencyList) {
		this.valuationCurrencyList = valuationCurrencyList;
	}

	public String getVehicleStatus() {
		return vehicleStatus;
	}

	public void setVehicleStatus(String vehicleStatus) {
		this.vehicleStatus = vehicleStatus;
	}

	public Map<String, String> getValuationCurrencyList_isactive() {
		return valuationCurrencyList_isactive;
	}

	public void setValuationCurrencyList_isactive(
			Map<String, String> valuationCurrencyList_isactive) {
		this.valuationCurrencyList_isactive = valuationCurrencyList_isactive;
	}

	public Map<String, String> getVehicleTypeList_isactive() {
		return vehicleTypeList_isactive;
	}

	public void setVehicleTypeList_isactive(
			Map<String, String> vehicleTypeList_isactive) {
		this.vehicleTypeList_isactive = vehicleTypeList_isactive;
	}

	public static Map<String, String> getEPAClass_isactive() {
		return EPAClass_isactive;
	}

	public static void setEPAClass_isactive(Map<String, String> ePAClass_isactive) {
		EPAClass_isactive = ePAClass_isactive;
	}

	public Map<String, String> getLengthunits() {
		return lengthunits;
	}

	public void setLengthunits(Map<String, String> lengthunits) {
		this.lengthunits = lengthunits;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	
}  

//End of Class.   
