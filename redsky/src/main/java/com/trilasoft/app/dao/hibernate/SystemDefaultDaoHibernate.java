package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.SystemDefaultDao;
import com.trilasoft.app.model.SystemDefault;

public class SystemDefaultDaoHibernate extends GenericDaoHibernate<SystemDefault, Long> implements SystemDefaultDao {

	public SystemDefaultDaoHibernate() {
		super(SystemDefault.class);
	}

	public List<SystemDefault> findByCorpID(String corpId) {
		return getHibernateTemplate().find("from SystemDefault where corpID=?", corpId);
	}

	public List findIdByCorpID(String corpId) {
		return getHibernateTemplate().find("select id from SystemDefault where corpID=?", corpId);
	}

	public String getCorpIdUnit(String corpID, String WnVol){
		String unit ="";
		List l =  this.getSession().createSQLQuery("select "+WnVol+" from systemdefault where corpid='"+corpID+"'").list();
		if(l!=null && !l.isEmpty() && l.get(0)!=null){
			unit = l.get(0).toString();
		}
		return unit;
		
	}
	public List findInvExtractSeq(String corpID) {
		return getHibernateTemplate().find("select invExtractSeq from SystemDefault where invExtractSeq<>'' and invExtractSeq is not null and corpID=?", corpID);
	}

	public int updateInvExtractSeq(String invExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set invExtractSeq='" + invExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public int updateMamutInvExtractSeq(String invExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set invExtractSeq='" + invExtractSeq + "'  where corpID=?", corpID);
		return i;
	}

	public List findPayExtractSeq(String corpID) {
		return getHibernateTemplate().find("select payExtractSeq from SystemDefault where payExtractSeq<>'' and payExtractSeq is not null and corpID=?", corpID);
	}

	public List findMamutPayExtractSeq(String corpID) {
		return getHibernateTemplate().find("select payExtractSeq from SystemDefault where payExtractSeq<>'' and payExtractSeq is not null and corpID=?", corpID);
	}

	public int updatePayExtractSeq(String payExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set payExtractSeq='" + payExtractSeq + "'  where corpID=?", corpID);
		return i;
	}

	public int updateMamutPayExtractSeq(String payExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set payExtractSeq='" + payExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	
	public List findPrtExtractSeq(String corpID) {
		return getHibernateTemplate().find("select prtExtractSeq from SystemDefault where prtExtractSeq<>'' and prtExtractSeq is not null and corpID=?", corpID);
	}

	public int updatePrtExtractSeq(String prtExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set prtExtractSeq='" + prtExtractSeq + "'  where corpID=?", corpID);
		return i;
	}

	public void updateDailyOpsLimit(String dailyOperationalLimit, String dailyOpLimitPC, String minServiceDay, String sessionCorpID) {
		getHibernateTemplate().bulkUpdate("update SystemDefault set dailyOperationalLimit='" + dailyOperationalLimit + "', dailyOpLimitPC='" + dailyOpLimitPC + "', minServiceDay='" + minServiceDay + "'  where corpID=?", sessionCorpID);
	}
	public int updateSAPExtractSeq(String invExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set invExtractSeq='" + invExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public List findSAPExtractSeq(String corpID) {
		return getHibernateTemplate().find("select prtExtractSeq from SystemDefault where prtExtractSeq<>'' and prtExtractSeq is not null and corpID=?", corpID);
	}
	public List findSAPExtractSeqForUGCN(String corpID) {
		return getHibernateTemplate().find("select prtExtractSeq from SystemDefault where prtExtractSeq<>'' and prtExtractSeq is not null and corpID=?", corpID);
	}
	public int updateSAPPrtExtractSeq(String prtExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set prtExtractSeq='" + prtExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public List findSAPPayExtractSeq(String corpID) {
		return getHibernateTemplate().find("select payExtractSeq from SystemDefault where payExtractSeq<>'' and payExtractSeq is not null and corpID=?", corpID);
	}
	public int updateSAPInvExtractSeq(String invExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set invExtractSeq='" + invExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public int updateInvExtractSeqUTSI(String invExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set invExtractSeq='" + invExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public List findInvExtractSeqUTSI(String corpID) {
		return getHibernateTemplate().find("select invExtractSeq from SystemDefault where invExtractSeq<>'' and invExtractSeq is not null and corpID=?", corpID);
	}
	public List findPrtExtractSeqUTSI(String corpID) {
		return getHibernateTemplate().find("select prtExtractSeq from SystemDefault where prtExtractSeq<>'' and prtExtractSeq is not null and corpID=?", corpID);
	}
	public int updatePrtExtractSeqUTSI(String prtExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set prtExtractSeq='" + prtExtractSeq + "' , validation = 'C,2,3,4' where corpID=?", corpID);
		return i;
	}
	public List findPayExtractSeqUTSI(String corpID) {
		return getHibernateTemplate().find("select payExtractSeq from SystemDefault where payExtractSeq<>'' and payExtractSeq is not null and corpID=?", corpID);
	}
	public int updatePayExtractSeqUTSI(String payExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set payExtractSeq='" + payExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	
	public int updateSeqNumber(String refType, String seq, String sessionCorpID){
		int i=0;
		if(refType.equalsIgnoreCase("P")){
			i = getHibernateTemplate().bulkUpdate("update SystemDefault set currentPay='" + seq + "'  where corpID=?", sessionCorpID);
		}
		if(refType.equalsIgnoreCase("R")){
			i = getHibernateTemplate().bulkUpdate("update SystemDefault set currentRec='" + seq + "'  where corpID=?", sessionCorpID);
		}
		if(refType.equalsIgnoreCase("S")){
			//i = getHibernateTemplate().bulkUpdate("update SystemDefault set suspenseDefault='" + seq + "'  where corpID=?", sessionCorpID);
		}
			return i;
	}
	public List getjaournalValidationValue(String sessionCorpID){
		return getHibernateTemplate().find("select validation from SystemDefault where corpID=?", sessionCorpID);
	}
	public int updateJournalValidation(String validationStr,String sessionCorpID){
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set validation = '"+validationStr+"' where corpID=?", sessionCorpID);
		return i;
	}
	public List findPaymentApplicationExtractSeq(String corpID) {
		return getHibernateTemplate().find("select paymentApplicationExtractSeq from SystemDefault where paymentApplicationExtractSeq<>'' and paymentApplicationExtractSeq is not null and corpID=?", corpID);
	}
	public int updatePaymentApplicationExtractSeq(String invExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set paymentApplicationExtractSeq='" + invExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public void updateSoextractSeq(String sessionCorpID,String soextractSeq){
		getHibernateTemplate().bulkUpdate("update SystemDefault set soextractSeq='" + soextractSeq + "'  where corpID=?", sessionCorpID);
		
	}
	
	public String OIServicesFromSystemDefault(String corpID)
	{
		String Query ="select service,warehouse from systemdefault where corpid='"+corpID+"'";
		List list = getSession().createSQLQuery(Query).list();
		String OIServices = "";
		if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
		{
			Iterator it = list.iterator();
			while(it.hasNext())
			{
				Object obj[]=(Object[])it.next();
				 if(obj[0] != null && obj[1] != null)
				 {
					 OIServices=obj[0]+"~"+obj[1];
			}
		}
	}
	return	OIServices;
}
}