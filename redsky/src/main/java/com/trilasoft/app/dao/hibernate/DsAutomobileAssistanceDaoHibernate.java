 package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsAutomobileAssistanceDao;
import com.trilasoft.app.model.DsAutomobileAssistance;


public class DsAutomobileAssistanceDaoHibernate extends GenericDaoHibernate<DsAutomobileAssistance, Long> implements DsAutomobileAssistanceDao {

	public DsAutomobileAssistanceDaoHibernate() {
		super(DsAutomobileAssistance.class);
		
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsAutomobileAssistance where id = "+id+" ");
	}

}

