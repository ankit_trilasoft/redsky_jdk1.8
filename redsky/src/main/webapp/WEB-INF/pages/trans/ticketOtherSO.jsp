<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>SO Ticket List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="ticketSO" class="table" requestURI="" id="workTicketList" export="false" defaultsort="1" >
		<display:column title="Ticket#" ><a href="#" onclick="goToUrlChild(${workTicketList.id});" ><c:out value="${workTicketList.ticket}" /></a></display:column>   
		<display:column property="service" titleKey="workTicket.service" />
		<display:column property="warehouse"  style="width:5px" title="Whse" />
	</display:table>
</div>
