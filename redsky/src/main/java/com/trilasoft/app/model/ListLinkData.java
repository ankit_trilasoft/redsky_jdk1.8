package com.trilasoft.app.model;

public interface ListLinkData {
	public String getListCode();
	public String getListDescription();
	public String getListSecondDescription();
	public String getListThirdDescription();
	public String getListFourthDescription();
	public String getListFifthDescription();
	public String getListSixthDescription();
	//public String getListParam1();
	public String getListSeventhDescription();
	public String getListEigthDescription();
	public String getListNinthDescription();
	public String getListTenthDescription();
}
