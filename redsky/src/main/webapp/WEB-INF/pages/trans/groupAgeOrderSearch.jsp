<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>  

 <head>   
    <title><fmt:message key="groupAgeOrderSearch.title"/></title>   
    <meta name="heading" content="<fmt:message key='groupAgeOrderSearch.heading'/>"/>   
    
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
   
    <script language="javascript" type="text/javascript">
    function clear_fields(){
    document.forms['searchForm'].elements['groupSequenceNumber'].value="";
    document.forms['searchForm'].elements['groupOriginCountry'].value="";
    document.forms['searchForm'].elements['groupDestinationCountry'].value="";
    document.forms['searchForm'].elements['groupPreferredDelivery'].value="";
    document.forms['searchForm'].elements['groupOriginContinent'].value="";
    document.forms['searchForm'].elements['groupDestinationContinent'].value="";
    document.forms['searchForm'].elements['groupMode'].value="";
    document.forms['searchForm'].elements['draft1'].checked=false;
     document.forms['searchForm'].elements['draft'].checked=false;
      document.forms['searchForm'].elements['finalized'].checked=false;
    
    }
    function confirmUser(targetVolume,targetElement){
      var targetId=targetElement.id;
      if(targetElement.checked==false){
      var agree=confirm("You are about to remove this service order from a group. Do you wish to continue?");
      if(agree){
        updateUsedVolume(targetVolume,targetElement);
      }
      else{
       document.getElementById(targetId).checked='true';
      }
     }
     else{
     updateUsedVolume(targetVolume,targetElement);
     }
    }
    </script>
    <script language="javascript" type="text/javascript">
    function consolidateGroup(target,grpStatus,targetVol){
      var targetId=target.id;
      var containerVal1=document.getElementById("containerSizeId").value;
   if(containerVal1!=''){
     if((target.checked==true) && (grpStatus=='Unassigned' || grpStatus=='unassigned' || grpStatus=='' )){
    var newIds=document.forms['searchForm'].elements['newIdList'].value;
    var soVolIds=document.forms['searchForm'].elements['soVolList'].value;
    if(newIds==''){
    newIds=targetId;
    soVolIds=targetVol;
    }
    else{
    newIds=newIds + ',' + targetId;
    soVolIds=soVolIds + ',' + targetVol;
    }
    document.forms['searchForm'].elements['newIdList'].value=newIds;
    document.forms['searchForm'].elements['soVolList'].value=soVolIds;
    var draftIds1=document.forms['searchForm'].elements['draftOrdersId'].value;
    if((newIds=="")&&(draftIds1==""))
    {
    	 document.forms['searchForm'].elements['selectMode'].value="";
    }    
    }
    else if((target.checked==false) && (grpStatus=='Unassigned' || grpStatus=='unassigned' || grpStatus=='' )){
    var newIds=document.forms['searchForm'].elements['newIdList'].value;
    var soVolIds=document.forms['searchForm'].elements['soVolList'].value;
    if(newIds.indexOf(targetId)>-1){
    newIds=newIds.replace(targetId,"");
    newIds=newIds.replace(",,",",");

    soVolIds=soVolIds.replace(targetVol,"");
    soVolIds=soVolIds.replace(",,",",");

    var len=newIds.length-1;
    var len1=soVolIds.length-1;
    
    if(len==newIds.lastIndexOf(",")){
    newIds=newIds.substring(0,len);
    
    }
    if(len1==soVolIds.lastIndexOf(",")){
    	soVolIds=soVolIds.substring(0,len1);
        }
        
    if(newIds.indexOf(",")==0){
    newIds=newIds.substring(1,newIds.length);
    
    }
    if(soVolIds.indexOf(",")==0){
    	soVolIds=soVolIds.substring(1,soVolIds.length);
        
        }    
    document.forms['searchForm'].elements['newIdList'].value=newIds;
    document.forms['searchForm'].elements['soVolList'].value=soVolIds;
    var draftIds1=document.forms['searchForm'].elements['draftOrdersId'].value;
    if((newIds=="")&&(draftIds1==""))
    {
    	 document.forms['searchForm'].elements['selectMode'].value="";
    }
    
    }
    }}
    var btnStatus=document.forms['searchForm'].elements['newIdList'].value;
    var newIdRes=btnStatus.split(",");
    if(newIdRes!='' && newIdRes.length > 0){
    document.getElementById("consolidateButton").disabled=false;
    document.getElementById("finalizeButton").disabled=false;
    }
    else{
     document.getElementById("consolidateButton").disabled=true;
     if(document.forms['searchForm'].elements['draftOrdersId'].value==''){
      document.getElementById("finalizeButton").disabled=true;
     }
    
    }
    }
   
   </script>
      <script language="javascript" type="text/javascript">
   function getHttpObject(){
   var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
   }
   var http1=getHttpObject();
   function getNewVolume(){
   var defaultVol=document.forms['searchForm'].elements['defaulVolUnit'].value;
   var containerSize=document.forms['searchForm'].elements['containerSize'].value;
   
   var url="findNewVolume.html?ajax=1&decorator=simple&popup=true&defaulVolUnit="+encodeURI(defaultVol)+"&containerSize="+encodeURI(containerSize);
   http1.open("GET",url,true);
   http1.onreadystatechange=httpResponseNewVolume;
   http1.send(null);
   }
   function httpResponseNewVolume(){
   if(http1.readyState==4){
     var result=http1.responseText
     var usedVol=document.forms['searchForm'].elements['usedVolume'].value;
     
     result=result.trim();
     document.forms['searchForm'].elements['avaliableVolume'].value=result;
     if(usedVol!=''){
     var newVol=document.forms['searchForm'].elements['avaliableVolume'].value;
     if(newVol!=''){
       var total=parseFloat(newVol)-parseFloat(usedVol);
       total=parseFloat(total).toFixed(2);
       document.forms['searchForm'].elements['avaliableVolume'].value=total;
     }
     else{
     total=0-parseFloat(usedVol);
      total=parseFloat(total).toFixed(2);
     document.forms['searchForm'].elements['avaliableVolume'].value=total;
     }
     
     }
     
   }
   }

   function updateUsedVolume(targetVolume,targetElement,targetMode){
			   var flagSim="";
			   targetMode=targetMode.trim();
			   if(targetMode!="") {
			   		var usedMode=document.forms['searchForm'].elements['selectMode'].value;
			   		usedMode=usedMode.trim();
			   		if(usedMode!=""){
				   		if(usedMode!=targetMode)  {					   			flagSim="NotMatch";				   		}
				   		else{					   		flagSim="Match";				   		}
			   		}else{				   		document.forms['searchForm'].elements['selectMode'].value=targetMode;				   		flagSim="Match";			   		}
			   }else{				   flagSim="NotBlank";			   }
	   var usedVol=document.forms['searchForm'].elements['usedVolume'].value;
	   var availVol=document.forms['searchForm'].elements['avaliableVolume'].value;
	   var volOfNewId=document.forms['searchForm'].elements['sumOfNewWeights'].value;
	   var total;
	   var containerVal1=document.getElementById("containerSizeId").value;
   if(containerVal1!=''){
   		if(targetVolume==''){
   			targetVolume=0.0;
   		}
   		if(usedVol==''){
   			usedVol=0.0;
   		}
   		if(availVol==''){
   			availVol=0.0;
   		}
   		if(targetElement.checked==false){
   			total=parseFloat(usedVol)-parseFloat(targetVolume);
   			availVol=parseFloat(availVol)+parseFloat(targetVolume);
   			total=parseFloat(total).toFixed(2);
   			availVol=parseFloat(availVol).toFixed(2);
   			var tot1=parseFloat(volOfNewId)-parseFloat(targetVolume);
   			tot1=parseFloat(tot1).toFixed(2);
   			document.forms['searchForm'].elements['sumOfNewWeights'].value=tot1;
   			document.forms['searchForm'].elements['usedVolume'].value=total;
   			document.forms['searchForm'].elements['avaliableVolume'].value=availVol;
   		}else{
   			var containerVal=document.getElementById("containerSizeId").value;
   			if(containerVal!=''){
	   			if(flagSim=="Match"){
				   if(targetVolume!='0.0' && targetVolume!='0' && targetVolume!='0.00'){
					   total=parseFloat(usedVol)+parseFloat(targetVolume);
					   availVol=parseFloat(availVol)-parseFloat(targetVolume);
	  		   	       total=parseFloat(total).toFixed(2);
					   availVol=parseFloat(availVol).toFixed(2);
					   var tot1=parseFloat(volOfNewId)+parseFloat(targetVolume);
		   	   	       tot1=parseFloat(tot1).toFixed(2);
					   if(parseFloat(availVol)>-1){
						   document.forms['searchForm'].elements['sumOfNewWeights'].value=tot1;
		 				   document.forms['searchForm'].elements['usedVolume'].value=total;
						   document.forms['searchForm'].elements['avaliableVolume'].value=availVol;
				   		}
				   		else{
						   var targetId=targetElement.id;
				   		   document.getElementById(targetId).checked=false;
				   		   alert("Volume of selected service orders exceeds maximum capacity of the chosen container. Please make an adjustment.");
				   		}
				   }else{
					   var targetId=targetElement.id;
					   document.getElementById(targetId).checked=false;
					   alert("Volume of selected service orders is 0.");
				   }
			   }else if(flagSim=="NotBlank"){
		   			var targetId=targetElement.id;
		   			alert("The mode for the selected service order is blank, please update the mode for this service order.");
		   			document.getElementById(targetId).checked=false;		   
	   		   }else{
		   			var targetId=targetElement.id;
		   			alert("You cannot select service orders with different mode. Please select service orders with same mode.");
		   			document.getElementById(targetId).checked=false;
	   			}
   			} else{
   				var targetId=targetElement.id;
   				document.getElementById(targetId).checked=false;
   				var newIds1=document.forms['searchForm'].elements['newIdList'].value;
   				var draftIds1=document.forms['searchForm'].elements['draftOrdersId'].value;
   				if((newIds1=="")&&(draftIds1==""))
   				{
   	 				document.forms['searchForm'].elements['selectMode'].value="";
   				}
      
				alert("Please select a working container size.");
   			}
          }
   	   } else{
   			var targetId=targetElement.id;
   			if(targetElement.checked==false){
   				document.getElementById(targetId).checked=true;
   			} else{
   				document.getElementById(targetId).checked=false;
   			}
   			var newIds1=document.forms['searchForm'].elements['newIdList'].value;
   			var draftIds1=document.forms['searchForm'].elements['draftOrdersId'].value;
   			if((newIds1=="")&&(draftIds1==""))
   			{
   	 			document.forms['searchForm'].elements['selectMode'].value="";
   			}   
   				alert("Please select a working container size.");
   		}
   }
   
   function confirmConsolidate(){
   var equipSize=document.getElementById("containerSizeId").value;
   var availVolume=document.forms['searchForm'].elements['avaliableVolume'].value;
   if(equipSize!=''){
      var newIddd=document.forms['searchForm'].elements['newIdList'].value;
      var draftsss=document.forms['searchForm'].elements['draftOrdersId'].value;
      var newIddSplitResu=newIddd.split(",");
      var draftIddSplitResu=draftsss.split(",");
   if((draftIddSplitResu!='' && (draftIddSplitResu.length > 1)) ){
  
               alert("You cannot consolidate serviceorder's as you have multiple draft serviceorder's in the list. Please make an adjustment.");
            }
 else{
   if(availVolume<0){
   alert("You cannot consolidate as the used volume exceeds avaliable volume. Please make an adjustment.");
   }
  else{
   var agree=confirm("You are about to consolidate the selected service orders into a group. Do you wish to continue?");
   var oldIdList=document.forms['searchForm'].elements['oldIdList'].value;
   var newIdList=document.forms['searchForm'].elements['newIdList'].value;
   var containerSize=document.forms['searchForm'].elements['containerSize'].value;
   var masterShipNo=document.forms['searchForm'].elements['masterShipNo'].value;
   var draftOrdersId=document.forms['searchForm'].elements['draftOrdersId'].value;
   if(agree){
  //  var destinationCity=prompt("Please enter destination city." , " ");
 //    document.forms['searchForm'].elements['grpDestinationCity'].value=destinationCity;
//    location.href='editGroupageOrder.html?oldIdList='+encodeURI(oldIdList)+'&newIdList='+encodeURI(newIdList)+'&grpDestinationCity='+encodeURI(destinationCity)+'&containerSize='+encodeURI(containerSize)+'&draftOrdersId='+encodeURI(draftOrdersId)+'&masterShipNo='+encodeURI(masterShipNo);
		document.forms['searchForm'].elements['grpFlag'].value='Draft';
        document.forms['searchForm'].action ='editGroupageOrder.html';
		document.forms['searchForm'].submit();
   }
   else{
   
   }
   }
   }
    }
    else{
     alert("Please select a working container size.");
    }
   }
   
   
   var http2=getHttpObject();
   function getContainerSize(){
     var masterShipNo=document.forms['searchForm'].elements['masterShipNo'].value;
     if(masterShipNo!=''){
     var shipID="shipNumber"+masterShipNo;
    var contSizeID="container"+masterShipNo;
    var avilVolID="avaliable"+masterShipNo;
    var usedVolID="used"+masterShipNo;
    var portEntry1="poeC"+masterShipNo;
    var carrierArrival1="poeD"+masterShipNo;
    var portLoading1="polC"+masterShipNo;
    var carrierDeparture1="polD"+masterShipNo;            
    
    try{
    var shipIDValue=document.forms['searchForm'].elements[shipID].value;
    var contSizeIDValue=document.forms['searchForm'].elements[contSizeID].value;
    var avilVolIDValue=document.forms['searchForm'].elements[avilVolID].value;
    var usedVolIDValue=document.forms['searchForm'].elements[usedVolID].value;

    var portEntry2=document.forms['searchForm'].elements[portEntry1].value;
    var carrierArrival2=document.forms['searchForm'].elements[carrierArrival1].value;
    var portLoading2=document.forms['searchForm'].elements[portLoading1].value;
    var carrierDeparture2=document.forms['searchForm'].elements[carrierDeparture1].value;      
    
    }
    catch(e){
    document.forms['searchForm'].elements['masterShipNo'].value="";
    document.forms['searchForm'].elements['containerSize'].value= "";
    document.forms['searchForm'].elements['avaliableVolume'].value= "";
    document.forms['searchForm'].elements['usedVolume'].value= "";

    document.forms['searchForm'].elements['portOfEntry'].value="";
    document.forms['searchForm'].elements['portOfArrival'].value="";
    document.forms['searchForm'].elements['portOfLoading'].value="";
    document.forms['searchForm'].elements['portOfDepart'].value="";    
    }

    if(shipIDValue!=null && shipIDValue!=undefined){
    document.forms['searchForm'].elements['masterShipNo'].value=shipIDValue;
    document.forms['searchForm'].elements['containerSize'].value= contSizeIDValue;
    document.forms['searchForm'].elements['avaliableVolume'].value= avilVolIDValue;
    document.forms['searchForm'].elements['usedVolume'].value= usedVolIDValue;

    document.forms['searchForm'].elements['portOfEntry'].value=portEntry2;
    document.forms['searchForm'].elements['portOfArrival'].value=carrierArrival2;
    document.forms['searchForm'].elements['portOfLoading'].value=portLoading2;
    document.forms['searchForm'].elements['portOfDepart'].value=carrierDeparture2;

    }
    else{
    
    }
    
     }
   }
   
 
   function alertUser(targetValue){
   alert("The shipnumber "+targetValue+" is not master serviceorder.");
   
   }
   </script>
  <script language="javascript" type="text/javascript">
   var zeroVolCount=0;
   function updateDraftOrdersId(target,tarVol,targetMode,volStatus){
  var targetId=target.id;
  var targetVolume1=tarVol;
  if((target.checked==true)){
	 
    var newIds=document.forms['searchForm'].elements['draftOrdersId'].value;
    var volIds=document.forms['searchForm'].elements['soVolList1'].value;
    if(targetVolume1=='0.0' || targetVolume1=='0' || targetVolume1=='0.00'){
    zeroVolCount=zeroVolCount+1;
    document.forms['searchForm'].elements['countZeroVolume'].value=zeroVolCount;
    }
    if(newIds==''){
    newIds=targetId;
    volIds=volStatus;
    }
    else{
    newIds=newIds + ',' + targetId;
    volIds=volIds + ',' + volStatus;
    }
    document.forms['searchForm'].elements['draftOrdersId'].value=newIds;
    document.forms['searchForm'].elements['soVolList1'].value=volIds;
    var usedMode=document.forms['searchForm'].elements['selectMode'].value;
	if(usedMode=="")
	{
		document.forms['searchForm'].elements['selectMode'].value=targetMode;
	}else
	  	{
	  	  	if(targetMode!=usedMode && targetMode.trim()!="")
	  	  	{
		  	var targetId=target.id;
 			alert("You cannot select service orders with different mode. Please select service orders with same mode.");
 			document.getElementById(targetId).checked=false;
 			document.forms['searchForm'].elements['draftOrdersId'].value="";
 			document.forms['searchForm'].elements['soVolList1'].value="";
	  	  	}
	  	}
    
    
    }
    else if((target.checked==false) ){
   
    var newIds=document.forms['searchForm'].elements['draftOrdersId'].value;
    var volIds=document.forms['searchForm'].elements['soVolList1'].value;
    if(targetVolume1=='0.0' || targetVolume1=='0' || targetVolume1=='0.00'){
    zeroVolCount=zeroVolCount-1;
    document.forms['searchForm'].elements['countZeroVolume'].value=zeroVolCount;
    }
    if(newIds.indexOf(targetId)>-1){
    newIds=newIds.replace(targetId,"");
    newIds=newIds.replace(",,",",");
    var len=newIds.length-1;
   
    if(len==newIds.lastIndexOf(",")){
    newIds=newIds.substring(0,len);
    
    }
    if(newIds.indexOf(",")==0){
    newIds=newIds.substring(1,newIds.length);
    
    }
    document.forms['searchForm'].elements['draftOrdersId'].value=newIds;
    }
    if(volIds.indexOf(volStatus)>-1){
    	volIds=volIds.replace(volStatus,"");
    	volIds=volIds.replace(",,",",");
        var len1=volIds.length-1;
       
        if(len1==volIds.lastIndexOf(",")){
        	volIds=volIds.substring(0,len1);
        
        }
        if(volIds.indexOf(",")==0){
        	volIds=volIds.substring(1,volIds.length);
        
        }
        document.forms['searchForm'].elements['soVolList1'].value=volIds;
    }    
    var draftIds1=document.forms['searchForm'].elements['draftOrdersId'].value;
    var newIds2=document.forms['searchForm'].elements['newIdList'].value;
    if((newIds2=="")&&(draftIds1==""))
    {
    	 document.forms['searchForm'].elements['selectMode'].value="";
    }
    }
    var draftIds=document.forms['searchForm'].elements['draftOrdersId'].value;
    var result=draftIds.split(",");
    if(result!='' && (result.length == 1)){
    getContainerSizeById(result[0]);    
    }
    else{
    document.forms['searchForm'].elements['containerSize'].value="";
    document.forms['searchForm'].elements['avaliableVolume'].value="";
    document.forms['searchForm'].elements['masterShipNo'].value="";
    document.forms['searchForm'].elements['portOfEntry'].value="";
    document.forms['searchForm'].elements['portOfArrival'].value="";
    document.forms['searchForm'].elements['portOfLoading'].value="";
    document.forms['searchForm'].elements['portOfDepart'].value="";
    if(document.forms['searchForm'].elements['newIdList'].value!=""){
    document.forms['searchForm'].elements['usedVolume'].value=document.forms['searchForm'].elements['sumOfNewWeights'].value;   
    }
    else{
    document.forms['searchForm'].elements['usedVolume'].value="";
    }
    }
    if((result!='' && result.length > 0) || (document.forms['searchForm'].elements['newIdList'].value!="")){
    document.getElementById("finalizeButton").disabled=false;
    }
    else{
     document.getElementById("finalizeButton").disabled=true;
    
    }
   
    }
    </script>
    <script language="javascript" type="text/javascript">
 //   var http4=getHttpObject();
    function getContainerSizeById(id){
    if(hitCount<=1){
    var shipID="shipNumber"+id;
    var contSizeID="container"+id;
    var avilVolID="avaliable"+id;
    var usedVolID="used"+id;
    var portEntry1="poeC"+id;
    var carrierArrival1="poeD"+id;

    var portLoading1="polC"+id;
    var carrierDeparture1="polD"+id;            
    var tempUsed=document.forms['searchForm'].elements['usedVolume'].value;
    var newUsed=document.getElementById(usedVolID).value;
    var availNew=document.getElementById(avilVolID).value;
    var portEntry2=document.getElementById(portEntry1).value;
    var carrierArrival2=document.getElementById(carrierArrival1).value;
    var portLoading2=document.getElementById(portLoading1).value;
    var carrierDeparture2=document.getElementById(carrierDeparture1).value;      
    document.forms['searchForm'].elements['portOfEntry'].value=portEntry2;
    document.forms['searchForm'].elements['portOfArrival'].value=carrierArrival2;
    document.forms['searchForm'].elements['portOfLoading'].value=portLoading2;
    document.forms['searchForm'].elements['portOfDepart'].value=carrierDeparture2;
    
    if(tempUsed==''){
    tempUsed=0;
    }
    if(newUsed==''){
    newUsed=0;
    }
    if(availNew==''){
    availNew=0;
    }
    
    var calculateUsed=parseFloat(tempUsed)+parseFloat(newUsed);
    calculateUsed=calculateUsed.toFixed(2);
    
    var calculateAvaliable;
   var sumOfNewWeights= document.forms['searchForm'].elements['sumOfNewWeights'].value;
    if(sumOfNewWeights==0 || sumOfNewWeights==''){
    calculateAvaliable=parseFloat(availNew);
    }
    else{
    calculateAvaliable=parseFloat(availNew)-sumOfNewWeights;
    }
    
    calculateAvaliable=calculateAvaliable.toFixed(2);
    document.forms['searchForm'].elements['masterShipNo'].value=document.getElementById(shipID).value;
    document.forms['searchForm'].elements['containerSize'].value=document.getElementById(contSizeID).value;
    document.forms['searchForm'].elements['avaliableVolume'].value=document.getElementById(avilVolID).value;
  //  document.forms['searchForm'].elements['usedVolume'].value=document.getElementById(usedVolID).value;
  document.forms['searchForm'].elements['avaliableVolume'].value=calculateAvaliable;
  document.forms['searchForm'].elements['usedVolume'].value=calculateUsed;
    }
    else{
    document.forms['searchForm'].elements['containerSize'].value="";
    document.forms['searchForm'].elements['avaliableVolume'].value="";
  //   document.forms['searchForm'].elements['usedVolume'].value="";
    document.forms['searchForm'].elements['masterShipNo'].value="";
    
    document.forms['searchForm'].elements['portOfEntry'].value="";
    document.forms['searchForm'].elements['portOfArrival'].value="";
    document.forms['searchForm'].elements['portOfLoading'].value="";
    document.forms['searchForm'].elements['portOfDepart'].value="";    
    if(document.forms['searchForm'].elements['newIdList'].value!=""){
    document.forms['searchForm'].elements['usedVolume'].value=document.forms['searchForm'].elements['sumOfNewWeights'].value;   
    }
    else{
    document.forms['searchForm'].elements['usedVolume'].value="";
    }
    }
    }
 
   function finalizeServiceOrders(){
      var checkList=document.forms['searchForm'].elements['countZeroVolume'].value;
      var newId=document.forms['searchForm'].elements['newIdList'].value;
      var drafts=document.forms['searchForm'].elements['draftOrdersId'].value;
      var availVolume=document.forms['searchForm'].elements['avaliableVolume'].value;
      var soVolStatChild=document.forms['searchForm'].elements['soVolList'].value;
      var soVolStatMaster=document.forms['searchForm'].elements['soVolList1'].value;
      soVolStatChild=soVolStatChild.trim();
      soVolStatMaster=soVolStatMaster.trim();
      var contSize=document.getElementById("containerSizeId").value;
      var notOkShip="";
      var tempDraft=drafts;
     var tempShipNumber="";
      if((availVolume<0 || availVolume=='') && (contSize!='')){
      if(availVolume=='' ){
             alert("Please select another container as avaliable volume for this container is blank");
             }else{
                alert("You cannot finalize as the used volume exceeds avaliable volume. Please make an adjustment.");
          }
      }
      else if((soVolStatChild.indexOf('notOk')>-1)||(soVolStatMaster.indexOf('notOk')>-1))
      {
    	  alert("Actual volumes not available for all S/O. Please correct before proceeding to Finalize. Click Ok to exit.");
      }else{
         drafts=drafts.split(",");
         if(checkList>0 && newId==''){
            alert("You cannot finalize as you have serviceorder's with zero volume.");
         }
         else{
            if((drafts!='' && (drafts.length > 1)) && (newId!='')){
               alert("You cannot finalize multiple serviceorder's as you have unassigned serviceorder's in the list. Please make an adjustment.");
            }
             else{
             
             
             if(drafts.length>1){
               tempDraft=tempDraft.split(",");
               for(i=0;i<tempDraft.length;i++){
               var findUsed=document.getElementById("used"+tempDraft[i]).value;
                
               if((parseFloat(availVolume)-parseFloat(findUsed))<0){

                 if(notOkShip!=''){
                    tempShipNumber=document.getElementById("shipNumber"+tempDraft[i]).value;
                    notOkShip=notOkShip+","+tempShipNumber;
                 }
                 else{
                  tempShipNumber=document.getElementById("shipNumber"+tempDraft[i]).value;
                  notOkShip=tempShipNumber;
               
                 }
   
               }  
             }
             }
           if(notOkShip!=''){
             alert("You cannot change the size of the container for the Groupage order :"+tempShipNumber+" as the used volume exceeds avaliable volume. Please make an adjustment.");
   
          }
          else{
             var agree=confirm("You cannot change your assignments once the groupage order is finalized. Do you wish to continue?");
             if(agree){
         		document.forms['searchForm'].elements['grpFlag'].value='Finalize';
	            document.forms['searchForm'].action ='finalizeServiceOrders.html';
		        document.forms['searchForm'].submit();
             }
         }
       }
     }
   }
  } 
   
    var hitCount=0;
    function countHits(targetElement){
     if(targetElement.checked==false){
     hitCount--;
     }
     else{
     hitCount++;
     }
     if(hitCount>1){
     document.forms['searchForm'].elements['containerSize'].value="";
    document.forms['searchForm'].elements['avaliableVolume'].value="";
    document.forms['searchForm'].elements['usedVolume'].value="";
    document.forms['searchForm'].elements['masterShipNo'].value="";

       
    document.forms['searchForm'].elements['portOfEntry'].value="";
    document.forms['searchForm'].elements['portOfArrival'].value="";
    document.forms['searchForm'].elements['portOfLoading'].value="";
    document.forms['searchForm'].elements['portOfDepart'].value="";
     
     }
    
    }
    </script>
    <script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>" djConfig="parseOnLoad:true, isDebug:false"></script>
<script>
function initLoader() { 
	 var script = document.createElement("script");
     script.type = "text/javascript";
  	 script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadMap";
//  	 			  script.src = "http://maps.googleapis.com/maps/api/directions/json?origin=Boston,MA&destination=Concord,MA&waypoints=Charlestown,MA|Lexington,MA&sensor=false";
     document.body.appendChild(script); 
}
var geocoder;
var map;
var letter1 = "";
var letter = "";
var letter3 = "";
var flag1="0";
var count1=-1;
var k=1;
var letterMap = new Object();
var driverDataMap = new Object();
var addrArray =new Array();
function loadMap() { 
	var address1="";
	var city="";
	var zip="";
	var state="";
	var country = "";
    map = new GMap2(document.getElementById('map'));
    geocoder = new GClientGeocoder();

		dojo.xhrPost({
	       form: "searchForm",
	       url:"groupDestinationAddress.html",	       
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	       			var j = 0;
	           for(var i=0; i < jsonData.ports.length; i++){
				   var address = jsonData.ports[i].address;	        	   
	        	   console.log("address: " + address);
	        	   driverDataMap[jsonData.ports[i].address] = jsonData.ports[i];				   
	        	   letterMap[jsonData.ports[i].address] = k;
	        	   k++;
	   			var timeout = i * 225; 
				addrArray[i] = jsonData.ports[i].address;
				window.setTimeout(function() { geoCodeLookup(); }, timeout);  	        	   
	               
	        	}
	       }	       
	   });
}
var addrCtr = 0;
function 	geoCodeLookup() {
	var addr = addrArray[addrCtr];
	addrCtr++;
	geocoder = new GClientGeocoder();
	geocoder.getLocations(addr, addDriverToMap); 
}
     var j = 0; 
   function addDriverToMap(response)
   {
         if(!response.Placemark) return;
   			   place = response.Placemark[0];
		      point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
	          map.setCenter(point, 13);
	          map.setZoom(4);
	     	  var baseIcon = new GIcon(G_DEFAULT_ICON);
  	          var letteredIconR = new GIcon(baseIcon);
  	          
  	             
					letteredIconR.image = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=4.5|0|00FF00|100|b|"+letterMap[response.name];	
				    markerOptions = { icon:letteredIconR };
				    marker = new GMarker(point, markerOptions); 
					map.addOverlay(marker, markerOptions);
					map.setUIToDefault();								
	  			    map.disableScrollWheelZoom();

			var fn = markerClickFnPorts(point,driverDataMap[response.name]);
				GEvent.addListener(marker, "mouseover", fn);
   }
   


	  function markerClickFnPorts(point, portDetails) {
	   return function() {
		   if (!portDetails) return;
		   var address = portDetails.address
		   var shipNumber = portDetails.shipNumber

		   if(address=='null')		
		    {		 
		     address='';		  
		    }else{
		       var arr=address.split("#");
		    }
	
		    if(shipNumber=='null')
            {
               shipNumber='';
            }
		
	
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>S/O# : </b>'+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Address:</b> '+arr[0]+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"> '+arr[1]+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;">'+arr[2]+'</h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	  
	       
	       map.openInfoWindowHtml(point, infoHtml);
	   }
   }
   
   function notAllowed(targetElement){
   var targetId=targetElement.id;
   if(targetElement.checked==true){
   alert("No volume found. Please correct before proceeding to consolidation. Click Ok to exit.");
   document.getElementById(targetId).checked=false;
   }
   }
   function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer,jobType){
		window.open('viewFormParam.html?id='+id+'&invoiceNumber='+invNum+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID=${noteID}&custID=${custID}&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
		//&reportName=${reportsList.description}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&
		//window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
	}	  
   function findUserPermission1(name,position) { 
	   var url="reportBySubModuleAccountLineList1.html?ajax=1&decorator=simple&popup=true&recInvNumb=" + encodeURI(name)+"&id=${serviceOrder.id}&jobType=${serviceOrder.job}&jobNumber="+name+"&companyDivision=${serviceOrder.companyDivision}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&reportModule=Groupage&reportSubModule=Groupage";
	   ajax_showTooltip(url,position);	
	   }
   function pickOriginMap()
   {
	    document.forms['searchForm'].elements['addressFlag'].value='Origin';  
		document.forms['searchForm'].action=document.location.href;	 
		document.forms['searchForm'].submit();
   }
   function pickDestinAddress(type)
   {	   
	    document.forms['searchForm'].elements['addressFlag'].value='Destination';
	    if(type=='simple'){
		document.forms['searchForm'].action=document.location.href;
	    }else{
	    document.forms['searchForm'].action='searchGroupAgeOrders.html';   
	    }	 
		document.forms['searchForm'].submit();
   }
   function getBillingState(targetElement,type) {
		var continent = targetElement.value;
		if(continent.trim()!="")
		{
		var url="findCountryList.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(continent);
	     http54.open("GET", url, true);
	     http54.onreadystatechange = function(){ handleHttpResponse254(type);};	     
	     http54.send(null);
		}
		}
   function handleHttpResponse254(type){
		if (http54.readyState == 4){
	        var results = http54.responseText
	        results = results.trim();
	        res = results.split("@");
			tempOrig=document.forms['searchForm'].elements['groupOriginCountry'].value;
			tempDestin=document.forms['searchForm'].elements['groupDestinationCountry'].value;
			if(type=="Origin")
			{
	        targetElement = document.forms['searchForm'].elements['groupOriginCountry'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['searchForm'].elements['groupOriginCountry'].options[i].text = '';
					document.forms['searchForm'].elements['groupOriginCountry'].options[i].value = '';
				}else{
					stateVal = res[i];
					document.forms['searchForm'].elements['groupOriginCountry'].options[i].text = stateVal;
					document.forms['searchForm'].elements['groupOriginCountry'].options[i].value = stateVal;				
				}
			}
			document.forms['searchForm'].elements['groupOriginCountry'].value=tempOrig;
			}else{
		        targetElement = document.forms['searchForm'].elements['groupDestinationCountry'];
				targetElement.length = res.length;
				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['searchForm'].elements['groupDestinationCountry'].options[i].text = '';
						document.forms['searchForm'].elements['groupDestinationCountry'].options[i].value = '';
					}else{
						stateVal = res[i];
						document.forms['searchForm'].elements['groupDestinationCountry'].options[i].text = stateVal;
						document.forms['searchForm'].elements['groupDestinationCountry'].options[i].value = stateVal;				
					}
				}
				document.forms['searchForm'].elements['groupDestinationCountry'].value=tempDestin;
			}	
		}
	}  
   function getBillingCountry(targetElement,type) {
		var country = targetElement.value;
		if(country.trim()!="")
		{
		var url="findContinent.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(country);
	     http55.open("GET", url, true);
	     http55.onreadystatechange = function(){ handleHttpResponse255(type);};	     
	     http55.send(null);
		}}
  function handleHttpResponse255(type){
		if (http55.readyState == 4){
	        var results = http55.responseText
	        results = results.trim();
	        res = results.split("@");
			tempOrig=document.forms['searchForm'].elements['groupOriginContinent'].value;
			tempDestin=document.forms['searchForm'].elements['groupDestinationContinent'].value;
		    if(type=="Origin")
			{ 
	        targetElement = document.forms['searchForm'].elements['groupOriginContinent'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['searchForm'].elements['groupOriginContinent'].options[i].text = '';
					document.forms['searchForm'].elements['groupOriginContinent'].options[i].value = '';
				}else{
					stateVal = res[i];
					document.forms['searchForm'].elements['groupOriginContinent'].options[i].text = stateVal;
					document.forms['searchForm'].elements['groupOriginContinent'].options[i].value = stateVal;				
				}
			}
			document.forms['searchForm'].elements['groupOriginContinent'].value=tempOrig;
			}else{
		        targetElement = document.forms['searchForm'].elements['groupDestinationContinent'];
				targetElement.length = res.length;
				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['searchForm'].elements['groupDestinationContinent'].options[i].text = '';
						document.forms['searchForm'].elements['groupDestinationContinent'].options[i].value = '';
					}else{
						stateVal = res[i];
						document.forms['searchForm'].elements['groupDestinationContinent'].options[i].text = stateVal;
						document.forms['searchForm'].elements['groupDestinationContinent'].options[i].value = stateVal;				
					}
				}
				document.forms['searchForm'].elements['groupDestinationContinent'].value=tempDestin;
			}	
		}
	}
  var http55=getHttpObject();	  
   var http54=getHttpObject();
</script>  
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/calendar.js"></script>
 <style>
form {
margin-top:-40px;!margin-top:-5px;
}
span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-16px;
!margin-top:-16px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
.listheadred {
    color: red;font-family: arial,verdana; font-size: 11px;font-weight: bold;text-decoration: none;
}

/*#inner-content{margin-top:50px;}*/
</style>
 </head> 
 <s:form id="searchForm" action="searchGroupAgeOrders" method="post" validate="true"> 
 <% int count=0; %>
 <c:set var="searchbuttons">   
    <s:submit type="button" cssClass="cssbutton1" id="search" value="Search" onclick="pickDestinAddress('search');"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>    
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="defaulVolUnit"/>
<s:hidden name="groupAgeStatus" value=""/>
<s:hidden name="selectMode" value=""/>
<s:hidden name="newIdList" value=""/>
<s:hidden name="soVolList" value=""/>
<s:hidden name="soVolList1" value=""/>
<s:hidden name="oldIdList" value=""/>
<s:hidden name="draftOrdersId" value="" />
<s:hidden name="grpDestinationCity" value=""/>
<s:hidden name="countZeroVolume" />
<s:hidden name="sumOfNewWeights" value="0"/>
<s:hidden name="grpFlag" value=""/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription"/>
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="eigthDescription" />
<s:hidden name="ninthDescription" />
<s:hidden name="tenthDescription" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${addressFlag == '' or addressFlag == null}">
<c:set var="addressFlag" value="Destination"/>
</c:if>
<c:if test="${addressFlag!=''}">
<s:hidden name="addressFlag" value="${addressFlag}"/>
</c:if>

<div id="otabs" style="margin-left: 20px;">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<table class="table" style="width:100%">
<thead>
<tr>
<th>Groupage Order#</th>
<th>Last/Company Name#</th>
<th>First Name#</th>
<th>Origin Country</th>
<th>Origin Continent</th>
<!--<th><fmt:message key="serviceOrder.status"/></th>
--><th>Destination&nbsp;Country</th>
<th>Destination&nbsp;Continent</th>
<!--<th>Destination City</th>
<th>Range Within(<span class="listheadred">miles</span>)</th> 
--><th>Preferred Delivery</th>
<th><fmt:message key="serviceOrder.mode"/></th>
<!--<th colspan="2">Groupage Status</th>
--></tr></thead>	
		<tbody>
		<tr>
			<td width="20" align="left">
			    <s:textfield name="groupSequenceNumber" required="true" cssClass="input-text" cssStyle="width:100px" size="15" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td width="20" align="left">
			    <s:textfield name="lastName" required="true" cssClass="input-text" cssStyle="width:100px" size="15" />
			</td>
			<td width="20" align="left">
			    <s:textfield name="firstName" required="true" cssClass="input-text" cssStyle="width:100px" size="15" />
			</td>
			<!--<td width="20" align="left">
			    <s:select name="groupStatus" list="%{grpStatusList}" cssClass="list-menu" cssStyle="min-width:105px;" headerKey="" headerValue=""/>
			</td>
			-->
			<td width="20" align="left">
			     <s:select name="groupOriginCountry" list="%{grpCountryList}" cssClass="list-menu"  cssStyle="width:120px;"  onchange="getBillingCountry(this,'Origin');document.getElementById('search').focus();"  headerKey="" headerValue=""/>
			</td>
			<td width="20" align="left">
			     <s:select name="groupOriginContinent" list="%{grpContinentList}" cssClass="list-menu"  cssStyle="width:120px;"   onchange="getBillingState(this,'Origin');document.getElementById('search').focus();" headerKey="" headerValue=""/>
			</td>			
			<td width="20" align="left">
			     <s:select name="groupDestinationCountry" list="%{grpCountryList}" cssClass="list-menu"  cssStyle="width:120px;"  onchange="getBillingCountry(this,'Destination');document.getElementById('search').focus();"  headerKey="" headerValue=""/>
			</td>
			<td width="20" align="left">
			     <s:select name="groupDestinationContinent" list="%{grpContinentList}" cssClass="list-menu"  cssStyle="width:120px;" onchange="getBillingState(this,'Destination');document.getElementById('search').focus();"  headerKey="" headerValue=""/>
			</td>
			<!--<td width="20" align="left">
			<s:textfield name="groupDestinationCity" required="true" cssClass="input-text" cssStyle="width:150px" size="15"  />
			</td>
			--><!--<td width="20" align="left">
			    <s:select name="groupStatus" list="%{grpStatusList}" cssClass="list-menu" cssStyle="min-width:105px;" headerKey="" headerValue=""/>
			</td>
			--><!--<td width="120" align="left">
			<s:select name="groupDestinationMiles" list="{'25','50','100','200','300','400','500','600','700','800','900','1000','1100'}" cssClass="list-menu"  cssStyle="width:90px;"   headerKey="" headerValue=""/>
			</td>
			
		
			--><c:if test="${not empty serviceOrder.grpDeliveryDate}">
				<s:text id="groupPreferredDeliveryFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.grpDeliveryDate" /></s:text>
				<td align="left" style="width:100px;!width:120px;"><s:textfield cssClass="input-text" id="groupPreferredDelivery" name="serviceOrder.grpDeliveryDate" value="%{groupPreferredDeliveryFormattedValue}" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/><img id="groupPreferredDelivery_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty serviceOrder.grpDeliveryDate}">
				<td align="left" style="width:100px;!width:120px;"><s:textfield cssClass="input-text" id="groupPreferredDelivery" onkeydown="return onlyDel(event,this);" name="serviceOrder.grpDeliveryDate" required="true" size="7" maxlength="11"/><img id="groupPreferredDelivery_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			
			<td width="105" align="left">			
			   <s:select name="groupMode" list="%{grpModeList}" cssClass="list-menu" cssStyle="width:118px;" onchange="document.getElementById('search').focus();" />			
			</td>
			
			<!--<td align="left" class="listwhitetext">		
			<s:checkbox key="draft1"  cssStyle="vertical-align: middle;" />Unassigned
			<s:checkbox name="draft" cssStyle="vertical-align: middle;" />Draft
			<s:checkbox name="finalized" cssStyle="vertical-align: middle;" />Finalized	
			</td>
			<td align="left" class="listwhitetext" style="border-left: hidden;">		
			<c:out value="${searchbuttons}" escapeXml="false" />   
			</td>		
		--></tr>
		
		<tr>					
			<td  class="listwhitetext" colspan="6">
			Groupage Status
			<s:checkbox key="draft1"  cssStyle="vertical-align: middle;" />Unassigned
			<s:checkbox name="draft" cssStyle="vertical-align: middle;" />Draft
			<s:checkbox name="finalized" cssStyle="vertical-align: middle;" />Finalized	
			</td>
			<td align="left" class="listwhitetext" style="border-left: hidden;">		
			<c:out value="${searchbuttons}" escapeXml="false" />   
			</td>		
			</tr>
		</tbody>
	</table>
	
<div id="otabs" style="margin-left: 20px;margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Groupage Container</span></a></li>
		  </ul>
		</div>
		
<display:table name="groupOrdersList" class="table"  id="groupOrdersList"   style="width:100%;margin-bottom:0px"  requestURI="/searchGroupAgeOrders.html"> 
<display:column title="">
<s:hidden name="shipNumber${groupOrdersList.shipNumber}"  id="shipNumber${groupOrdersList.id}" value="${groupOrdersList.shipNumber}"/>
<s:hidden name="container${groupOrdersList.shipNumber}"  id="container${groupOrdersList.id}" value="${groupOrdersList.equipmentSize}"/>
<s:hidden name="avaliable${groupOrdersList.shipNumber}" id="avaliable${groupOrdersList.id}" value="${groupOrdersList.flexValue}"/>
<s:hidden name="used${groupOrdersList.shipNumber}" id="used${groupOrdersList.id}"  value="${groupOrdersList.volume}"/>
<s:hidden  name="polD${groupOrdersList.shipNumber}" id="polD${groupOrdersList.id}"  value="${groupOrdersList.carrierDeparture}"/>
<s:hidden  name="polC${groupOrdersList.shipNumber}" id="polC${groupOrdersList.id}" value="${groupOrdersList.portLoading}"/> 
<s:hidden name="poeD${groupOrdersList.shipNumber}" id="poeD${groupOrdersList.id}"  value="${groupOrdersList.carrierArrival}" />
<s:hidden name="poeC${groupOrdersList.shipNumber}" id="poeC${groupOrdersList.id}" value="${groupOrdersList.portEntry}"/>
<a onclick="javascript:openWindow('childServiceOrders.html?id=${groupOrdersList.id}&masterGroupStatus=${groupOrdersList.grpStatus}&&decorator=popup&popup=true',800,700);">View Manifest</a>
<a><img align="top" title="Forms" onclick="findUserPermission1('${groupOrdersList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
</display:column>
<display:column title="G/O#" sortable="true">

<c:if test="${forwardingTabVal!='Y'}">  
	<a href="containers.html?id=${groupOrdersList.id}">
       <c:out value="${groupOrdersList.shipNumber}" />
    </a>
</c:if>
<c:if test="${forwardingTabVal=='Y'}">
	<a href="containersAjaxList.html?id=${groupOrdersList.id}">
	<c:out value="${groupOrdersList.shipNumber}" />
	</a>
</c:if> 

</display:column>
<display:column title="Last/Company Name" sortable="true">
<c:out value="${groupOrdersList.lastName}">
</c:out>
</display:column>
<display:column title="First Name" sortable="true">
<c:out value="${groupOrdersList.firstName}">
</c:out>
</display:column>

<display:column title="Port of Loading" sortable="true">

<c:out value="${groupOrdersList.carrierDeparture}">

</c:out>

</display:column>

<display:column title="Port of Entry" sortable="true">

<c:out value="${groupOrdersList.carrierArrival}"> 

</c:out>

</display:column>
<display:column title="Mode" property="gMode" sortable="true"/>
<display:column title="Container" property="containerNumber"  sortable="true" >

</display:column>
<display:column title="Available" style="text-align:right;" sortProperty="flexValue"  sortable="true" sortName="flexValue">
<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" value="${groupOrdersList.flexValue}" />
</display:column>
<display:column title="Used" style="text-align:right;" sortable="true" sortProperty="volume"  >
<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" value="${groupOrdersList.volume}" />
</display:column>

<display:column title="Pieces" style="text-align:right;" sortProperty="totalPieces"  >
<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" value="${groupOrdersList.totalPieces}" />
</display:column>

<display:column title="Select">

<c:if test="${groupOrdersList.grpStatus=='draft' || groupOrdersList.grpStatus=='Draft'}">																
<input type="checkbox" id="${groupOrdersList.id}"  onclick="countHits(this);updateDraftOrdersId(this,'${groupOrdersList.volume}','${groupOrdersList.gMode}','${groupOrdersList.volStatus}');"  />
</c:if>
<c:if test="${groupOrdersList.grpStatus=='finalized' || groupOrdersList.grpStatus=='Finalized'}">
<input type="checkbox" id="${groupOrdersList.id}"  checked="checked" disabled="disabled"/>
</c:if>
</display:column>

<display:column property="grpStatus"   title="Groupage Status"  sortable="true"/> 
</display:table>
<br>
<table class="detailTabLabel" style="margin-bottom:0px;width:100%; margin-left:-3px;">
<tr>
<!-- <td width="50%">
<div id="otabs" style="margin-left:15px;margin-bottom:0px;position:absolute;">
		  <ul>
		    <li><a class="current"><span>S/O List</span></a></li>
		  </ul>
		</div>
</td>
<td class="listwhitetext" style="padding-left:80px;"></td>
 -->
<td style="padding-bottom:18px; width:650px;" height="25px;">
<table class="detailTabLabel" style="margin-bottom:-15px;border:2px solid #74B3DC;">
    <tr>
    <td class="listwhitetext"></td>
   <td class="listwhitetext">Groupage Order#</td>
    <td class="listwhitetext">Container</td>
   <td class="listwhitetext" style="padding-left:10px;">Available</td>
    <td class="listwhitetext" style="padding-left:10px;">Used</td>
    <td class="listwhitetext" style="padding-left:10px;">POE</td>
    <td class="listwhitetext"></td>
    <td class="listwhitetext" style="padding-left:10px;">POL</td>    
    </tr>
    <tr>
    <td class="listwhitetext">Working&nbsp;Container</td>
   <td><s:textfield name="masterShipNo" required="true" cssClass="input-text" cssStyle="width:100px" size="15" onchange="getContainerSize();" value="" /></td>
  <td><s:select list="%{EQUIP}" id="containerSizeId" name="containerSize" headerKey="" headerValue="" onchange="getNewVolume();" cssClass="list-menu" cssStyle="width:105px;"></s:select></td>
  <td ><s:textfield name="avaliableVolume" cssClass="input-textUpper" cssStyle="width:80px" value="" readonly="true" ></s:textfield></td>
  <td ><s:textfield name="usedVolume" cssClass="input-textUpper" cssStyle="width:80px" value="" readonly="true"></s:textfield></td>
  <td ><s:textfield name="portOfEntry" cssClass="input-textUpper" cssStyle="width:80px" value="" readonly="true" ></s:textfield></td>
  <td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:openWindow('searchPortList.html?portCode=&portName=&country=&modeType=${groupMode}&active=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=portOfArrival&fld_code=portOfEntry')" src="<c:url value='/images/open-popup.gif'/>" /></td>
  <s:hidden name="portOfArrival" cssClass="input-textUpper" cssStyle="width:80px" value="" />
  <td><s:textfield name="portOfLoading" cssClass="input-textUpper" cssStyle="width:80px" value="" readonly="true"></s:textfield></td>
  <td><img align="right" class="openpopup" width="17" height="20" onclick="javascript:openWindow('searchPortList.html?portCode=&portName=&country=&modeType=${groupMode}&active=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=portOfDepart&fld_code=portOfLoading')" src="<c:url value='/images/open-popup.gif'/>" /></td>
  <s:hidden name="portOfDepart" cssClass="input-textUpper" cssStyle="width:80px" value=""/>
  <td></td>
    </tr>
    </table>
 </td> 
<td valign="bottom" style="width:80px;">
<input type="button" class="cssbutton1" style="width:80px; height:23px;margin-bottom:5px;" align="top" id="consolidateButton" value="Consolidate" onclick="confirmConsolidate();" disabled="disabled"/>
</td>
 <td valign="bottom">
<input type="button" class="cssbutton1" value="Finalize" style="width:60px; height:23px;margin-bottom:5px;" id="finalizeButton" disabled="disabled" onclick="finalizeServiceOrders();"/>
</td>
</tr>
</table>

		<div class="spnblk">&nbsp;</div>
		<div id="otabs" style="margin-left:15px;margin-bottom:0px;position:absolute;">
		  <ul>
		    <li><a class="current"><span>S/O List</span></a></li>
		  </ul>
		</div>
<display:table name="unassignOrdersList" class="table"   id="unassignOrdersList" style="width:100%;margin-bottom:0px; margin-top:23px;" requestURI="/searchGroupAgeOrders.html" > 
<display:column title="#">
<c:out value="${unassignOrdersList_rowNum}"/>
</display:column>
<display:column title="S/O#" property="shipNumber" paramId="id" paramProperty="id" url="/editServiceOrderUpdate.html" sortable="true"/>
<display:column title="Last/Company Name" property="lastName" sortable="true"/>
<display:column title="First Name" property="firstName" sortable="true" />
<display:column title="Origin City" property="originCity" sortable="true"/>
<display:column title="Origin Country" property="originCountry" sortable="true"/>
<display:column title="Dest. City" property="destinationCity" sortable="true"/>
<display:column title="Dest. Country" property="destinationCountry" sortable="true"/>
<display:column title="Mode" property="mode" sortable="true"/>
<display:column title="Loading (T/A)" property="destinationState" />
<display:column title="Pref. Delivery" property="grpDeliveryDate" />
<display:column title="Volume (E/A)" style="text-align:right;" sortProperty="volume" sortable="true">

<!--<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" value="${unassignOrdersList.volume}" />
-->
<c:choose>
<c:when test="${unassignOrdersList.weight1!=''}">
<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0" value="${unassignOrdersList.weight1}" />
<font color="red">(A)</font>
</c:when>
<c:when test="${unassignOrdersList.weight1=='' && unassignOrdersList.weight2!=''}">
<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0" value="${unassignOrdersList.weight2}" />
<font color="red">(E)</font>
</c:when>
</c:choose>
</display:column>
<display:column title="Pieces" style="text-align:right;" sortProperty="totalPieces"  >
<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" value="${unassignOrdersList.totalPieces}" />
</display:column>
<display:column title="Select">
<!--<c:if test="${unassignOrdersList.grpStatus=='unassigned' || unassignOrdersList.grpStatus==''}">
<input type="checkbox" id="${unassignOrdersList.id}" onclick="updateUsedVolume('${unassignOrdersList.volume}',this);consolidateGroup(this,'${unassignOrdersList.grpStatus}');" />
</c:if>

--><c:choose>
<c:when test="${unassignOrdersList.weight1!='' || unassignOrdersList.weight2!=''}">
<c:choose>
<c:when test="${unassignOrdersList.weight1!=''}">
<c:if test="${unassignOrdersList.grpStatus=='unassigned' || unassignOrdersList.grpStatus==''}">
<input type="checkbox" id="${unassignOrdersList.id}" onclick="updateUsedVolume('${unassignOrdersList.volume}',this,'${unassignOrdersList.mode}');consolidateGroup(this,'${unassignOrdersList.grpStatus}','ok');" />
</c:if>
</c:when>
<c:otherwise>
<c:if test="${unassignOrdersList.grpStatus=='unassigned' || unassignOrdersList.grpStatus==''}">
<input type="checkbox" id="${unassignOrdersList.id}" onclick="updateUsedVolume('${unassignOrdersList.volume}',this,'${unassignOrdersList.mode}');consolidateGroup(this,'${unassignOrdersList.grpStatus}','notOk');" />
</c:if>
</c:otherwise>
</c:choose>
</c:when>
<c:when test="${unassignOrdersList.weight1=='' && unassignOrdersList.weight2==''}">
<input type="checkbox" id="${unassignOrdersList.id}" onclick="notAllowed(this);" />
</c:when>
<c:otherwise>
<input type="checkbox" id="${unassignOrdersList.id}" onclick="updateUsedVolume('${unassignOrdersList.volume}',this,'${unassignOrdersList.mode}');consolidateGroup(this,'${unassignOrdersList.grpStatus}');" />
</c:otherwise>
</c:choose>

</display:column>
</display:table>

<br/>

<div id="newmnav" style="margin-left:20px;margin-bottom:0px;">
<B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
		  <ul style="    margin-top: 10px;">


		  	<c:if test="${addressFlag=='Destination'}">  
			<li id="newmnav1" style="background:#FFF;"><a class="current"><span>S/O Destination Map</span></a></li>
		    <li><a onclick="pickOriginMap();"><span>S/O Origin Map</span></a></li>		    
		    </c:if>
		  	<c:if test="${addressFlag!='Destination'}">  
		    <li id="newmnav1" style="background:#FFF;"><a class="current"><span>S/O Origin Map</span></a></li>
		    <li><a onclick="pickDestinAddress('simple');"><span>S/O Destination Map</span></a></li>
		    </c:if>
		    
		  </ul>
		</div>
		
<table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0"> 
    <tr>
          <td align="left" width="80%" valign="top">
		   <div id="map" style="width:100%;!width:99%;border:1px dotted #219DD1; height:400px; background:#EFEFEF;"></div>
		   <div id="googleImage" style="position:absolute;bottom:10px;!bottom:50px;left:30px;"><img src="${pageContext.request.contextPath}/images/poweredby.png"></div>
      </td>
      
   </tr>
  </table>
</s:form>
<script type="text/javascript">
try{
document.getElementById("containerSizeId").value="";
}
catch(e){
}
try{
	document.forms['searchForm'].elements['groupSequenceNumber'].focus();
}catch(e){}
</script>
<script type="text/javascript">
<c:if test="${not empty unassignOrdersList}">
try{
document.getElementById("googleImage").style.display='none';
initLoader();
}
catch(e){}
</c:if>

</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>