<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="passwordHelp.title"/></title>
    <meta name="heading" content="<fmt:message key='passwordHelp.heading'/>"/>
    <meta name="menu" content="PasswordHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-1col.css'/>" />
  
    <script language="javascript" type="text/javascript">
       function clear_fileds(){
    var i;
			for(i=0;i<=4;i++)
 	{
       	document.forms['passwordHelpForm'].elements['j_username'].value = "";
    	document.forms['passwordHelpForm'].elements['j_passwordHint'].value = "";
    	document.forms['passwordHelpForm'].elements['user.email'].value = "";
    	document.forms['passwordHelpForm'].elements['j_passwordHintQues'].value = "";
    }
    }
    </script>
</head>
<form method="post" id="passwordHelpForm" name="passwordHelpForm" action="findPwordHintQ.html">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="24%">&nbsp;</td>
    <td width="52%"><table width="400" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a2c9f8">
      <tr>
        <td height="260" valign="top" style="background:url(${pageContext.request.contextPath}/images/loginbg.jpg) top repeat-x #2955b1; "><table width="365" border="0" cellspacing="0" cellpadding="4" style="font:normal 12px arial,verdana; color:#FFFFFF ">
          <tr align="center">
            <td colspan="2"><img src="${pageContext.request.contextPath}/images/rsloginlogo.gif" width="133" height="64"></td>
            </tr>
          <tr>
            <td width="172" height="30" align="right"><label for="j_username" class="required desc">
            	<fmt:message key="label.username"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
        	<td width="193"><input type="text" class="text medium" name="user.username" id="j_username" tabindex="1" /></td>
          </tr>
          <tr>
            <td width="500" height="30" align="right"><label for="j_passwordHintQues" class="required desc">
            <fmt:message key="label.passwordHintQues"/><span class="req"><font color="#e30000">*</font></span></label></td>
       		<td><input type="passwordHintQues" class="text medium" name="user.passwordHintQues" value="${user.passwordHintQues}" tabindex="2" size="40"/></td>
          </tr>
          <tr>
            <td width="172" height="30" align="right"><label for="j_passwordHint" class="required desc">
            	<fmt:message key="label.passwordHint"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td><input type="passwordHint" class="text medium" name="j_passwordHint" id="j_passwordHint" tabindex="3" /></td>
          </tr>
          <tr>
			 <td align="right"><fmt:message key="user.email"/><font color="red" size="2">*</font></td>
			 <td align="left"><s:textfield name="user.email" cssClass="input-text" size="40"/></td>
		 </tr>
		 <tr>
            <td>&nbsp;</td>
<!--          <td><input type="button" onclick="location.href='<c:url value="passwordHelp.html"/>'" name="submit" value="<fmt:message key='button.submit'/>" tabindex="4" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>-->
           <td><input type="button" onclick="" name="submit" value="<fmt:message key='button.submit'/>" tabindex="4" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>
          
          </tr>
          </table></td>
      </tr>
    </table></td>
    <td width="24%">&nbsp;</td>
  </tr>
</table>
</form>

<%@ include file="/scripts/login.js"%>