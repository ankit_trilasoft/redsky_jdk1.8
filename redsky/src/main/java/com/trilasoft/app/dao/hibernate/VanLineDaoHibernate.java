package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.VanLineDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.ShowLineDetail;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.VanLine;


public class VanLineDaoHibernate extends GenericDaoHibernate<VanLine, Long> implements VanLineDao {

	private HibernateUtil hibernateUtil;
	
	 /**
     * Convenience method to get the request
     * @return current request
     */
    protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
	public VanLineDaoHibernate() {   
        super(VanLine.class);   
    }
	
	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original; // return original if 'find' is not in it.
		}
		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());
		return partBefore + replacement + partAfter;
	}

	public List<VanLine> vanLineList(){
		List vanLineList = null;
        String columns = "id,agent, weekEnding, shipper, amtDueAgent, amtDueHq, glType, reconcileStatus";
        vanLineList = getHibernateTemplate().find("select " + columns + " from VanLine");
        try {
        	vanLineList = hibernateUtil.convertScalarToComponent(columns, vanLineList, VanLine.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return vanLineList;
	}
	
	public List getSerachedSOList(String shipNum, String regNum,String lastName, String firstName, String sessionCorpID){
		try{
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from ServiceOrder where shipNumber like '%"+shipNum+"%' and registrationNumber like '%"+regNum+"%' and lastName like '%"+lastName+"%'  and firstName like '%"+firstName+"%' and corpid = '"+sessionCorpID+"'  ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public List getProcessedXMLList(String shipNum, String sessionCorpID){
		try{
		List list1=new ArrayList();
		String query = "select i.transactionid,i.filename,i.processedOn,i.batchID from integrationlog i , accountline a where " +
				" i.filename = a. createdby and a.corpid='"+sessionCorpID+"' and  i.corpid='"+sessionCorpID+"' and (i.filename like 'rt%' or i.filename like 'ds%'  ) and i.transactionID = '"+shipNum+"'  and a.status = true and a.download = false group by i.filename  ";
		List lista= this.getSession().createSQLQuery(query)
		.addScalar("i.transactionid", Hibernate.STRING)
			  .addScalar("i.filename", Hibernate.STRING) 
			  .addScalar("i.processedOn", Hibernate.DATE) 
			  .addScalar("i.batchID", Hibernate.STRING) 
			  .list(); 
		Iterator it=lista.iterator();
		while(it.hasNext())
		{
			Object []row= (Object [])it.next();
			VanLineDTO dto = new VanLineDTO(); 
			dto.setTransactionId(row[0].toString());
			dto.setFileName(row[1].toString()); 
			dto.setProcessedOn((Date)row[2]);
			dto.setBatchId(row[3].toString());
			List listacc=this.getSession().createSQLQuery("select id from accountline where corpid='"+sessionCorpID+"' and  shipNumber = '"+shipNum+"' and status=true and  createdBy = '"+row[1].toString()+"' and download = false").list();
			dto.setAccIdList(listacc);
			list1.add(dto);
		}
		
		return list1;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
		//return getHibernateTemplate().find("from IntegrationLog where corpID='"+sessionCorpID+"' and (filename like 'rt%' or filename like 'ds%'  ) and transactionID = '"+shipNum+"' group by filename ");
	}
	 public  class VanLineDTO
	  {
		  private String transactionId;
		  private String fileName;
		  private Date processedOn;
		  private String batchId;
		  private List accIdList;
		public String getTransactionId() {
			return transactionId;
		}
		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public Date getProcessedOn() {
			return processedOn;
		}
		public void setProcessedOn(Date processedOn) {
			this.processedOn = processedOn;
		}
		public String getBatchId() {
			return batchId;
		}
		public void setBatchId(String batchId) {
			this.batchId = batchId;
		}
		public List getAccIdList() {
			return accIdList;
		}
		public void setAccIdList(List accIdList) {
			this.accIdList = accIdList;
		}
		
		  
		
	  }
	public List getDownloadXMLList(String shipNum, String sessionCorpID,String fileName){
		try{
		String q = "select * from accountline where corpid='"+sessionCorpID+"' and  shipNumber = '"+shipNum+"' and status=true and  createdBy = '"+fileName+"' and download = false ";
		return getHibernateTemplate().find(" from AccountLine where corpID='"+sessionCorpID+"' and  shipNumber = '"+shipNum+"' and status=true and createdBy = '"+fileName+"' and download = false ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public Map getvenderCodeDetails(String shipNum,String corpId){
		Map codeDetails=new LinkedHashMap();
		String query="select * from miscellaneous m inner join companydivision c on m.haulingAgentVanlineCode=c.vanLineCode and m.corpid=c.corpid where m.shipNumber='"+shipNum+"' and  m.corpid='"+corpId+"' and m.haulingAgentVanlineCode!=''";
		List haulingAgent=this.getSession().createSQLQuery(query).list();	
		String queryTemp="select if(driverCrewType is null OR driverCrewType='' OR driverCrewType='()','N',driverCrewType) from systemdefault where corpid='"+corpId+"' limit 1";
		List alQueryTemp=this.getSession().createSQLQuery(queryTemp).list();
		String ts="";
		if((alQueryTemp!=null)&&(!alQueryTemp.isEmpty())&&(alQueryTemp.get(0)!=null)&&(!alQueryTemp.get(0).toString().equalsIgnoreCase("N"))){
			ts=alQueryTemp.get(0).toString();
		}
		List venderList=new ArrayList();			
		if(!ts.equalsIgnoreCase("")){
			if(haulingAgent!=null && !haulingAgent.isEmpty()){
			String query1="select concat(if(m.driverId is null or m.driverId='','A',m.driverId),'~',if(m.driverName is null or m.driverName='','A',m.driverName)) from  miscellaneous m inner join partner p on  m.driverId=p.partnerCode and  p.status='Approved' where m.corpId='"+corpId+"' and m.driverId is not null and m.shipNumber='"+shipNum+"' union select concat(if(c.partnerCode is null or c.partnerCode='','A',c.partnerCode),'~',if(c.partnerName is null or c.partnerName='','A',c.partnerName)) from workticket w,timesheet t,crew c where w.shipNumber='"+shipNum+"' and w.corpId='"+corpId+"' and w.ticket=t.ticket and c.partnerCode is not null "+(ts.equalsIgnoreCase("")?"":" AND c.typeOfWork in"+ts)+" and c.userName=t.userName group by c.partnerCode ";
			venderList=this.getSession().createSQLQuery(query1).list();	
			}else{
				String query1="select concat(if(c.partnerCode is null or c.partnerCode='','A',c.partnerCode),'~',if(c.partnerName is null or c.partnerName='','A',c.partnerName)) from workticket w,timesheet t,crew c where w.shipNumber='"+shipNum+"' and w.corpId='"+corpId+"' and w.ticket=t.ticket and c.partnerCode is not null "+(ts.equalsIgnoreCase("")?"":" AND c.typeOfWork in"+ts)+" and c.userName=t.userName group by c.partnerCode union select concat(if(m.driverId is null or m.driverId='','A',m.driverId),'~',if(m.driverName is null or m.driverName='','A',m.driverName)) from  miscellaneous m inner join partner p on  m.driverId=p.partnerCode and  p.status='Approved' where m.corpId='"+corpId+"' and m.driverId is not null and m.shipNumber='"+shipNum+"'";
				venderList=this.getSession().createSQLQuery(query1).list();	
			}			
		}else{
			String queryString="select concat(if(m.driverId is null or m.driverId='','A',m.driverId),'~',if(m.driverName is null or m.driverName='','A',m.driverName)) from  miscellaneous m inner join partner p on  m.driverId=p.partnerCode and  p.status='Approved' where m.corpId='"+corpId+"' and m.driverId is not null and m.shipNumber='"+shipNum+"'";
			venderList=this.getSession().createSQLQuery(queryString).list();	
		}	
		for (Object object : venderList) {
			String a[] = object.toString().split("~");
			if(!a[0].toString().equalsIgnoreCase("A")){
				codeDetails.put(a[0].toString()+"@"+a[1].toString(), a[0].toString()+" : "+a[1].toString());
			}		
		}
		//if(codeDetails!=null && !codeDetails.isEmpty()){
			codeDetails.put("B"+"@"+"B","No Driver");
		//}
	/*	if(venderList!=null && !venderList.isEmpty()){
			Iterator it=venderList.iterator();
			while(it.hasNext())
			{
			Object []row= (Object [])it.next();
			String a[] = row[0].toString().split("~");	
			codeDetails.put(a[1].toString(), a[0].toString());
			}	
		}*/
		return codeDetails;
	}
	
	public int updateAccByFileName(String fileName,String fileNoVenderDetails,String venderDetailsList, String sessionCorpID){
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		Map accFileVen=new HashMap();
		if(fileNoVenderDetails!=null && !fileNoVenderDetails.equals("")){
			String abc[]=fileNoVenderDetails.split(",");
			for (String string : abc) {
				String cda[]=string.split("~");
				accFileVen.put(cda[0].trim(), cda[1]);
			}
		}
		if(venderDetailsList!=null && !venderDetailsList.equals("")){
			String abc[]=venderDetailsList.split(",");
			for (String string : abc) {
				String cda[]=string.split("~");
				accFileVen.put(cda[0].trim(), cda[1]);
			}
		}
		List commissionPercent=new ArrayList();
		BigDecimal driverpercent=new BigDecimal(0.00);
		List<AccountLine> AccDetails=getHibernateTemplate().find("from AccountLine where createdBy in ("+fileName+") and corpID='"+sessionCorpID+"' and download = false and status=true ");
		for (AccountLine accountLine : AccDetails) {
			
			String venderCodeValue="";
			String venderName="";
			String ownerPlan="";
			String planVender = (String) accFileVen.get(accountLine.getId().toString());
			if(planVender!=null && planVender.contains("@")){
				String planVenderDetails[]=planVender.split("@");
				venderCodeValue=planVenderDetails[0];
				venderName=planVenderDetails[1];
			}
		/*	String queryTemp="select if(driverCrewType is null OR driverCrewType='' OR driverCrewType='()','N',driverCrewType) from systemdefault where corpid='"+sessionCorpID+"' limit 1";
			List alQueryTemp=this.getSession().createSQLQuery(queryTemp).list();
			String ts="";
			if((alQueryTemp!=null)&&(!alQueryTemp.isEmpty())&&(alQueryTemp.get(0)!=null)&&(!alQueryTemp.get(0).toString().equalsIgnoreCase("N"))){
				ts=alQueryTemp.get(0).toString();
			}
			List al=null;			
			if(!ts.equalsIgnoreCase("")){
				String query1="select concat(if(c.partnerCode is null or c.partnerCode='','A',c.partnerCode),'~',if(c.partnerName is null or c.partnerName='','A',c.partnerName)) from workticket w,timesheet t,crew c where w.shipNumber='"+accountLine.getShipNumber()+"' and w.corpId='"+sessionCorpID+"' and w.ticket=t.ticket and c.partnerCode is not null "+(ts.equalsIgnoreCase("")?"":" AND c.typeOfWork in"+ts)+" and c.userName=t.userName group by c.partnerCode";
				al=this.getSession().createSQLQuery(query1).list();
				if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
					String a[] = al.get(0).toString().split("~");
					if(!a[0].toString().equals("A")){
					venderCodeValue=a[0].toString();
					venderName = a[1].toString();
					}
				}
				
			}
			if(venderCodeValue.equalsIgnoreCase("")){
				String query="select concat(if(driverId is null or driverId='','A',driverId),'~',if(driverName is null or driverName='','A',driverName)) from  miscellaneous where corpId='"+sessionCorpID+"' and driverId is not null and shipNumber='"+accountLine.getShipNumber()+"'";
				al=this.getSession().createSQLQuery(query).list();
				if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
					String a[] = al.get(0).toString().split("~");
					if(!a[0].toString().equals("A")){
					venderCodeValue=a[0].toString();
					venderName = a[1].toString();
					}
				}
			}*/
			if(venderCodeValue!=null && !venderCodeValue.equalsIgnoreCase("") && !venderCodeValue.equalsIgnoreCase("B") && !venderName.equalsIgnoreCase("B")){
				
				
				String accountCrossReference="";
				List accountCrossReferencelist = new ArrayList();
					accountCrossReferencelist = this.getSession().createSQLQuery("select if(accountCrossReference is null,'' ,accountCrossReference) from partneraccountref where corpID='"+sessionCorpID+"' and partnerCode='"+venderCodeValue+"' and refType='P' ").list();
					if(accountCrossReferencelist!= null && (!(accountCrossReferencelist.isEmpty())) && accountCrossReferencelist.get(0)!= null ){
						accountCrossReference=accountCrossReferencelist.get(0).toString();
					} 
				
				
				List list = this.getSession().createSQLQuery("select pp.commissionPlan from partnerprivate pp left outer join partnerpublic p on pp.partnerCode=p.partnerCode and p.corpID ='"+sessionCorpID+"' where pp.partnerCode='"+venderCodeValue+"' and pp.corpID ='"+sessionCorpID+"' and p.isOwnerOp is true").list();
				if(list!=null && !(list.isEmpty()) && (list.get(0)!=null)){
					ownerPlan=list.get(0).toString();
				}
				if(ownerPlan!=""){
					BigDecimal percent=new BigDecimal(0.0000);
					String amountBasis="";
					List<Billing> billingDetails=getHibernateTemplate().find("from Billing where shipNumber=?",accountLine.getShipNumber());
				    for (Billing billing : billingDetails) {
				    commissionPercent = this.getSession().createSQLQuery("select percent,amountBasis from drivercommissionplan  where charge='"+accountLine.getChargeCode()+"' and contract like '%"+billing.getContract()+"%' and plan='"+ownerPlan+"' and corpId='"+sessionCorpID+"'").list();	
					} 					
					if(commissionPercent==null || commissionPercent.isEmpty()){
					 commissionPercent = this.getSession().createSQLQuery("select percent,amountBasis from drivercommissionplan  where charge='"+accountLine.getChargeCode()+"' and contract='' and plan='"+ownerPlan+"' and corpId='"+sessionCorpID+"'").list();	
					}
					Iterator it1=commissionPercent.iterator();
					while(it1.hasNext())
					{
						Object []row= (Object [])it1.next();
						percent=new BigDecimal(row[0].toString());
						amountBasis=row[1].toString();
						
					}
					if(accountLine.getRevisionRevenueAmount().compareTo(new BigDecimal("0.00"))!=0 
							&& percent.compareTo(new BigDecimal("0.00"))!=0 && amountBasis.equalsIgnoreCase("RevisionRevenue")){
						driverpercent=accountLine.getRevisionRevenueAmount().multiply((percent));	
						driverpercent= driverpercent.divide(new BigDecimal(100));
					 getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',actualExpense='"+driverpercent+"',localAmount='"+driverpercent+"',category='Driver',vendorCode='"+venderCodeValue+"',estimateVendorName='"+venderName.replaceAll("'", "''")+"',valueDate=now(), payPostDate = now(), settledDate=now(),payingStatus='A',country='USD',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
					}else{
						//getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',vendorCode='"+venderCodeValue+"',estimateVendorName='"+venderName.replaceAll("'", "''")+"',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
						getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
					}
					
			}else{
			 // getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',vendorCode='"+venderCodeValue+"',estimateVendorName='"+venderName.replaceAll("'", "''")+"',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
				 getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
			}			
			}else{
			  getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode=''  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
			}
		  }
		//return getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"'  where createdBy in ("+fileName+") and corpID='"+sessionCorpID+"' and status=true ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return 0;
	}
	
	public int updateDownloadByFileName(String fileName, String sessionCorpID){
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		return getHibernateTemplate().bulkUpdate("update AccountLine set download = false , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"'  where createdBy in ("+fileName+") and corpID='"+sessionCorpID+"' and status=true ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return 0;
	}
	public int updateAccByID(String accID,String accVenderDetails,String venderDetailsList, String sessionCorpID){
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		List commissionPercent=new ArrayList();
		BigDecimal driverpercent=new BigDecimal(0.00);
		Map accVen=new HashMap();
		if(accVenderDetails!=null && !accVenderDetails.equals("")){
			String abc[]=accVenderDetails.split(",");
			for (String string : abc) {
				String cda[]=string.split("~");
				if(!cda[0].toString().equals("") && !cda[1].toString().equals("A")){
					accVen.put(cda[0].trim(), cda[1]);	
				}
				
			}
		}	
		if(venderDetailsList!=null && !venderDetailsList.equals("")){
			String abc[]=venderDetailsList.split(",");
			for (String string : abc) {
				String cda[]=string.split("~");
				if(!cda[0].toString().equals("") && !cda[1].toString().equals("A")){
				accVen.put(cda[0].trim(), cda[1]);
				}
			}
		}
		List<AccountLine> AccDetails=getHibernateTemplate().find("from AccountLine where id in ("+accID+") and corpID='"+sessionCorpID+"' and download = false and status=true ");
		for (AccountLine accountLine : AccDetails) {
			
			String venderCodeValue="";
			String venderName="";
			String ownerPlan="";
			String planVender = (String) accVen.get(accountLine.getId().toString());
			if(planVender!=null && planVender.contains("@")){
				String planVenderDetails[]=planVender.split("@");
				venderCodeValue=planVenderDetails[0];
				venderName=planVenderDetails[1];
			}
		/*	String queryTemp="select if(driverCrewType is null OR driverCrewType='' OR driverCrewType='()','N',driverCrewType) from systemdefault where corpid='"+sessionCorpID+"' limit 1";
			List alQueryTemp=this.getSession().createSQLQuery(queryTemp).list();
			String ts="";
			if((alQueryTemp!=null)&&(!alQueryTemp.isEmpty())&&(alQueryTemp.get(0)!=null)&&(!alQueryTemp.get(0).toString().equalsIgnoreCase("N"))){
				ts=alQueryTemp.get(0).toString();
			}*/
	/*		List al=null;			
			if(!ts.equalsIgnoreCase("")){
				String query1="select concat(if(c.partnerCode is null or c.partnerCode='','A',c.partnerCode),'~',if(c.partnerName is null or c.partnerName='','A',c.partnerName)) from workticket w,timesheet t,crew c where w.shipNumber='"+accountLine.getShipNumber()+"' and w.corpId='"+sessionCorpID+"' and w.ticket=t.ticket and c.partnerCode is not null "+(ts.equalsIgnoreCase("")?"":" AND c.typeOfWork in"+ts)+" and c.userName=t.userName group by c.partnerCode";
				al=this.getSession().createSQLQuery(query1).list();
				if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
					String a[] = al.get(0).toString().split("~");
					if(!a[0].toString().equals("A")){
						venderCodeValue=a[0].toString();
						venderName = a[1].toString();	
					}
					
				}
				
			}*/
		/*	if(venderCodeValue.equalsIgnoreCase("")){
				String query="select concat(if(driverId is null or driverId='','A',driverId),'~',if(driverName is null or driverName='','A',driverName)) from  miscellaneous where corpId='"+sessionCorpID+"' and driverId is not null and shipNumber='"+accountLine.getShipNumber()+"'";
				al=this.getSession().createSQLQuery(query).list();
				if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
					String a[] = al.get(0).toString().split("~");
					if(!a[0].toString().equals("A")){
						venderCodeValue=a[0].toString();
						venderName = a[1].toString();	
					}
				
				}
			}*/
			if(venderCodeValue!=null && !venderCodeValue.equalsIgnoreCase("") && !venderCodeValue.equalsIgnoreCase("B") && !venderName.equalsIgnoreCase("B") ){
				
				String accountCrossReference="";
				List accountCrossReferencelist = new ArrayList();
					accountCrossReferencelist = this.getSession().createSQLQuery("select if(accountCrossReference is null,'' ,accountCrossReference) from partneraccountref where corpID='"+sessionCorpID+"' and partnerCode='"+venderCodeValue+"' and refType='P' ").list();
					if(accountCrossReferencelist!= null && (!(accountCrossReferencelist.isEmpty())) && accountCrossReferencelist.get(0)!= null ){
						accountCrossReference=accountCrossReferencelist.get(0).toString();
					}
				
				List list = this.getSession().createSQLQuery("select pp.commissionPlan from partnerprivate pp left outer join partnerpublic p on pp.partnerCode=p.partnerCode and p.corpID ='"+sessionCorpID+"' where pp.partnerCode='"+venderCodeValue+"' and pp.corpID ='"+sessionCorpID+"' and p.isOwnerOp is true").list();
				if(list!=null && !(list.isEmpty()) && (list.get(0)!=null)){
					ownerPlan=list.get(0).toString();
				}
				if(ownerPlan!=""){
					BigDecimal percent=new BigDecimal(0.0000);
					String amountBasis="";
					List<Billing> billingDetails=getHibernateTemplate().find("from Billing where shipNumber=?",accountLine.getShipNumber());
				    for (Billing billing : billingDetails) {
				    commissionPercent = this.getSession().createSQLQuery("select percent,amountBasis from drivercommissionplan  where charge='"+accountLine.getChargeCode()+"' and contract like '%"+billing.getContract()+"%' and plan='"+ownerPlan+"' and corpId='"+sessionCorpID+"'").list();	
					} 					
					if(commissionPercent==null || commissionPercent.isEmpty()){
					 commissionPercent = this.getSession().createSQLQuery("select percent,amountBasis from drivercommissionplan  where charge='"+accountLine.getChargeCode()+"' and contract='' and plan='"+ownerPlan+"' and corpId='"+sessionCorpID+"'").list();	
					}
					Iterator it1=commissionPercent.iterator();
					while(it1.hasNext())
					{
						Object []row= (Object [])it1.next();
						percent=new BigDecimal(row[0].toString());
						amountBasis=row[1].toString();
						
					}
					if(accountLine.getRevisionRevenueAmount().compareTo(new BigDecimal("0.00"))!=0 
							&& percent.compareTo(new BigDecimal("0.00"))!=0 && amountBasis.equalsIgnoreCase("RevisionRevenue")){
						driverpercent=accountLine.getRevisionRevenueAmount().multiply((percent));	
						driverpercent= driverpercent.divide(new BigDecimal(100));
					 getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',actualExpense='"+driverpercent+"',localAmount='"+driverpercent+"',category='Driver',vendorCode='"+venderCodeValue+"',estimateVendorName='"+venderName.replaceAll("'", "''")+"',valueDate=now(), payPostDate = now(), settledDate=now(),payingStatus='A',country='USD',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
					}else{
						//getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',vendorCode='"+venderCodeValue+"',estimateVendorName='"+venderName.replaceAll("'", "''")+"',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
						getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
					}
					
			}else{
			     //getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',vendorCode='"+venderCodeValue+"',estimateVendorName='"+venderName.replaceAll("'", "''")+"',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
				getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode='"+accountCrossReference+"'  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
			}
			}else{
			     getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"',category='Driver',revisionCurrency='USD',revisionValueDate=now(),revisionExchangeRate=1,invoiceNumber='',invoiceDate=null,payingStatus='',actgCode=''  where id='"+accountLine.getId()+"' and corpID='"+sessionCorpID+"' and status=true ");
			}
		  }	
		
		//return getHibernateTemplate().bulkUpdate("update AccountLine set download = true , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"' where id in ("+accID+") and corpID='"+sessionCorpID+"' and status=true ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return 0;
	}
	
	public int updateDownloadByID(String accID, String sessionCorpID){
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		return getHibernateTemplate().bulkUpdate("update AccountLine set download = false , updatedon = now(), updatedby= 'DS:"+user.getUsername()+"' where id in ("+accID+") and corpID='"+sessionCorpID+"' and status=true ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return 0;
	}

	String statusDateNew;
	public List<VanLine> searchVanLine(String agent,Date weekEnding, String glType, String reconcileStatus) {
		List vanLineList = null;
		
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	        
	 	}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
		}
		String columns = "id,agent, weekEnding, shipper, amtDueAgent, amtDueHq, glType, reconcileStatus";
        if(weekEnding==null){
        	vanLineList = getHibernateTemplate().find("select " + columns + " from VanLine where agent like '" + agent .replaceAll("'", "''")+ "%' AND glType like '" + glType.replaceAll("'", "''")+ "%' AND reconcileStatus like '" +reconcileStatus.replaceAll("'", "''") + "%'" );
        }
        if(weekEnding!=null){
            vanLineList = getHibernateTemplate().find("select " + columns + " from VanLine where agent like '" + agent .replaceAll("'", "''")+ "%' AND glType like '" + glType.replaceAll("'", "''")+ "%' AND reconcileStatus like '" +reconcileStatus.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%'");		
        }
        try {
        	vanLineList = hibernateUtil.convertScalarToComponent(columns, vanLineList, VanLine.class);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error executing query"+ e,e);
        }
		return vanLineList; 
	}	
	
	
	/*public List<VanLine>  showLine(String regNum) {
		//List vanLineListShow = getHibernateTemplate().find("select firstName, glType,distributionAmount, registrationNumber from ServiceOrder as s, AccountLine as a where s.shipNumber = a.shipNumber and s.registrationNumber = '"+regNum+"");		
		List showVanLines = getHibernateTemplate().find("select s.firstName, s.registrationNumber, s.shipNumber, a.glType, a.distributionAmount  from ServiceOrder as s, AccountLine as a where s.shipNumber=a.shipNumber and s.registrationNumber =?",regNum);		
        
		return showVanLines; 
				
	}*/	
	public List<VanLine>  showLine(String regNum){
		List showVanLines = new ArrayList();
		String availCrewQuery = "select s.firstName, s.registrationNumber,  a.glType, a.distributionAmount  from serviceorder as s, accountline as a where s.shipNumber=a.shipNumber and s.registrationNumber ='"+regNum+"' group by s.id";
		List listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
		Iterator it=listAvailCrew.iterator();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           ShowLineDetail showLineDetail = new ShowLineDetail();	           
	             
	           if(row[0] == null){showLineDetail.setShipperName("");}else{showLineDetail.setShipperName(row[0].toString());}        
	           if(row[1] == null){showLineDetail.setRegistrationNumber("");}else{showLineDetail.setRegistrationNumber(row[1].toString());}	          
	           if(row[2] == null){showLineDetail.setGlType("");}else{showLineDetail.setGlType(row[2].toString());}
	           if(row[3] == null){showLineDetail.setDistributionAmount(new BigDecimal("0"));}else{showLineDetail.setDistributionAmount(new BigDecimal(row[3].toString()));}
	         	           
	           showVanLines.add(showLineDetail);
	       }
	    	return showVanLines;
	    }
	
	public List<SubcontractorCharges>  splitCharge(Long id){
		try{
		 return getHibernateTemplate().find("from SubcontractorCharges where parentId='"+id+"'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
				 
		}
	
/*	public List getEntitleSum(String shipNumber)
    {
    	List entitleSum=new ArrayList(); 
    	entitleSum=getHibernateTemplate().find("select sum(entitlementAmount) from AccountLine where shipNumber=? and status=true",shipNumber);
    	if(entitleSum.get(0)==null)
        {
	       List entitleSum1=new ArrayList();
	       entitleSum1.add("0.00");
	        return  entitleSum1;
        }
       else
       {
	   return entitleSum;
       }
    }*/
	public List<SubcontractorCharges> totalAmountSum(Long id){
		List subcontractorChargesExt=new ArrayList(); 
		try{
		subcontractorChargesExt=getHibernateTemplate().find("select sum(amount) from SubcontractorCharges where parentId='"+id+"'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		
    	if(subcontractorChargesExt.get(0)==null)
        {
	       List entitleSum1=new ArrayList();
	       entitleSum1.add("0.00");
	       
	        return  entitleSum1;
        }
       else
       {
    	   
	   return subcontractorChargesExt;
       }
	}
	
	public List getVanLineListWithReconcile(String agent,Date weekEnding,String sessionCorpID){
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	    }catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
		}
	    List <Object> VanLineListWithReconcile = new ArrayList<Object>();
	    List <Object> lista = new ArrayList<Object>();
	    if(agent.indexOf(":")==0){
	    	//return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and regNum is not null and regNum!=''");
	    	String query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.amtDueAgent,v.reconcileAmount,reconcileStatus,s.id  from vanline v force index (agent), serviceorder s  where s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  and v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' group by v.regNum ";
	    	lista= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.LONG)
	    	.list();
	    }
	 	else{
	 		//return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%' and regNum is not null and regNum!='' ");
	 		//String query="select agent,regNum,weekEnding,shipper,sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount,reconcileStatus  from vanline where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%' and regNum is not null and regNum!='' group by regNum ";
	 		String query="select v.agent,v.regNum,v.weekEnding,v.shipper,sum(v.amtDueAgent) as amtDueAgent,sum(v.reconcileAmount) as reconcileAmount,reconcileStatus,s.id from vanline v force index (agent),serviceorder s where  s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  and  v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' group by v.regNum ";
	 		lista= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.LONG)
	    	.list();
	 	}
	    Iterator it1=lista.iterator(); 
	    DTOForVanLine DTOVanLine =null;
	    while(it1.hasNext()){
	    Object [] row=(Object[])it1.next();
	    DTOVanLine=new DTOForVanLine();
	    DTOVanLine.setAgent(row[0]);
	    if(row[1]!=null){
	    	DTOVanLine.setRegNum(row[1]);
	    List reconcileStatus=this.getSession().createSQLQuery("select if(v.reconcileStatus='Suspense',concat('a',v.reconcileStatus),if(v.reconcileStatus='Dispute',concat('b',v.reconcileStatus),if(v.reconcileStatus='Approved',concat('c',v.reconcileStatus),if(v.reconcileStatus='Reconcile',concat('d',v.reconcileStatus),if(v.reconcileStatus='',concat('e',v.reconcileStatus),''))))) AS reconcileStatus from vanline v force index (agent)  where  v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum='"+row[1].toString()+"' order by reconcileStatus").list();	
		if(reconcileStatus!=null && !reconcileStatus.isEmpty() && reconcileStatus.get(0)!=null){
			if(reconcileStatus.get(0).toString().equalsIgnoreCase("aSuspense")){
		    	DTOVanLine.setReconcileStatus("Suspense");
		    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("bDispute")){
		    	DTOVanLine.setReconcileStatus("Dispute");
		    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("cApproved")){
		    	DTOVanLine.setReconcileStatus("Approved");
		    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("dReconcile")){
		    	DTOVanLine.setReconcileStatus("Reconcile");
		    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("e")){
		    	DTOVanLine.setReconcileStatus("");
		    }else{
		    	DTOVanLine.setReconcileStatus("");
		    }
		}else{
			DTOVanLine.setReconcileStatus("");
		}
	    }else{
			DTOVanLine.setRegNum(" ");
			DTOVanLine.setReconcileStatus("");
		}
	    DTOVanLine.setWeekEnding(row[2]);
	    DTOVanLine.setShipper(row[3]);
	    DTOVanLine.setAmtDueAgent(row[4]);
	    DTOVanLine.setReconcileAmount(row[5]);
	    
	    DTOVanLine.setSoId(row[7]);
	   
	    VanLineListWithReconcile.add(DTOVanLine);
	    }
	    //added for #7921 29 aprl 2013
	    String s1="select sum(v.amtDueAgent) as reconcileStatusAmount from vanline v force index (agent), serviceorder s  where s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  and v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' and reconcilestatus in ('Reconcile','Approved') ";
	    String sqlq1="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Reconcile','Approved') and regNum is not null and regNum!=''";
	    List totalAmountRecApp = this.getSession().createSQLQuery(s1).list(); 	                             
	    if(totalAmountRecApp.get(0)!=null){
	    	DTOVanLine.setReconcileStatusAmount(totalAmountRecApp.get(0));
	    }
	  //Added For #7921--29 Aprl--
	    String s2="select sum(v.amtDueAgent) as reconcileStatusAmount from vanline v force index (agent) , serviceorder s  where s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  and v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' and reconcilestatus in ('Suspense')  ";
	    String sqlq11="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Suspense') and regNum is not null and regNum!=''";
	    List totalAmountSuspense = this.getSession().createSQLQuery(s2).list();
	    if(totalAmountSuspense!=null && totalAmountSuspense.get(0)!=null){
	    	DTOVanLine.setSuspenseStatusAmount(totalAmountSuspense.get(0));
	    }
	    String s3="select sum(v.amtDueAgent) as reconcileStatusAmount from vanline v force index (agent), serviceorder s  where s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  and v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!=''   ";
	    String sqlq111="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and  regNum is not null and regNum!=''";
	    List totalAmountAllStatus = this.getSession().createSQLQuery(s3).list();
	    if(totalAmountAllStatus!=null && totalAmountAllStatus.get(0)!=null){
	    	DTOVanLine.setTotalAllStatusAmount(totalAmountAllStatus.get(0));
	    }
	    VanLineListWithReconcile.add(DTOVanLine);
	    //end........
	    return VanLineListWithReconcile;
	    }
	
	
	/*public List<VanLine> getVanLineListWithoutReconcile(String agent,Date weekEnding){
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	        
	 	}catch (Exception e) {
			e.printStackTrace();
		}
	 	if(agent.indexOf(":")==0){
		return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%'");
           }
	 	else{
	 		return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%'");
	 	}
	 	
	 	}*/
	   public List getVanLineListWithoutReconcile(String agent,Date weekEnding,String sessionCorpID){
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	        
	 	}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
		}
	 	 List <Object> VanLineListWithoutReconcile = new ArrayList<Object>();
		    List <Object> listaVanLine = new ArrayList<Object>();
	 	if(agent.indexOf(":")==0){
		//return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%'");
	 		//String query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.amtDueAgent,v.reconcileAmount,v.reconcileStatus,s.id  from vanline v , serviceorder s where s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' and v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' group by v.regNum ";
	 		String query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.amtDueAgent,v.reconcileAmount,v.reconcileStatus,s.id  from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' group by v.regNum ";
	 		listaVanLine= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.LONG)
	    	.list();  
	 	}
	 	else{
	 		//return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%'");
	 		//String query="select v.agent,v.regNum,v.weekEnding,v.shipper,sum(v.amtDueAgent) as amtDueAgent,sum(v.reconcileAmount) as reconcileAmount,v.reconcileStatus,s.id  from vanline v , serviceorder s where s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' and  v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' group by v.regNum ";
	 		String query="select v.agent,v.regNum,v.weekEnding,v.shipper,sum(v.amtDueAgent) as amtDueAgent,sum(v.reconcileAmount) as reconcileAmount,v.reconcileStatus,s.id from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' group by v.regNum";
	 		listaVanLine= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.LONG)
	    	.list();
	 	}
	 	Iterator it1=listaVanLine.iterator(); 
	    DTOForVanLine DTOVanLine =null;
	    while(it1.hasNext()){
	    Object [] row=(Object[])it1.next();
	    DTOVanLine=new DTOForVanLine();
	    DTOVanLine.setAgent(row[0]);
	    if(row[1]!=null){
	    	DTOVanLine.setRegNum(row[1]);
	    	List reconcileStatus=this.getSession().createSQLQuery("select if(v.reconcileStatus='Suspense',concat('a',v.reconcileStatus),if(v.reconcileStatus='Dispute',concat('b',v.reconcileStatus),if(v.reconcileStatus='Approved',concat('c',v.reconcileStatus),if(v.reconcileStatus='Reconcile',concat('d',v.reconcileStatus),if(v.reconcileStatus='',concat('e',v.reconcileStatus),''))))) AS reconcileStatus from vanline v force index (agent)  where  v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum='"+row[1].toString()+"' order by reconcileStatus").list();	
			if(reconcileStatus!=null && !reconcileStatus.isEmpty() && reconcileStatus.get(0)!=null){
				if(reconcileStatus.get(0).toString().equalsIgnoreCase("aSuspense")){
			    	DTOVanLine.setReconcileStatus("Suspense");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("bDispute")){
			    	DTOVanLine.setReconcileStatus("Dispute");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("cApproved")){
			    	DTOVanLine.setReconcileStatus("Approved");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("dReconcile")){
			    	DTOVanLine.setReconcileStatus("Reconcile");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("e")){
			    	DTOVanLine.setReconcileStatus("");
			    }else{
			    	DTOVanLine.setReconcileStatus("");
			    }
			}else{
				DTOVanLine.setReconcileStatus("");
			}
		}else{
			DTOVanLine.setRegNum(" ");
			DTOVanLine.setReconcileStatus("");
		}
	    DTOVanLine.setWeekEnding(row[2]);
	    DTOVanLine.setShipper(row[3]);
	    DTOVanLine.setAmtDueAgent(row[4]);
	    DTOVanLine.setReconcileAmount(row[5]);
	    DTOVanLine.setSoId(row[7]);
	    String s1="select sum(v.amtDueAgent) from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' and reconcilestatus in ('Reconcile','Approved')";
	    String sqlq1="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Reconcile','Approved') and regNum is not null and regNum!=''";
	    List totalAmountRecApp = this.getSession().createSQLQuery(s1).list(); 	                             
	    if(totalAmountRecApp.get(0)!=null){
	    	DTOVanLine.setReconcileStatusAmount(totalAmountRecApp.get(0));
	    }
	  //Added For #7921--29 Aprl--
	    String s2="select sum(v.amtDueAgent) from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!='' and reconcilestatus in ('Suspense')";
	    String sqlq11="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Suspense') and regNum is not null and regNum!=''";
	    List totalAmountSuspense = this.getSession().createSQLQuery(s2).list();
	    if(totalAmountSuspense!=null && totalAmountSuspense.get(0)!=null){
	    	DTOVanLine.setSuspenseStatusAmount(totalAmountSuspense.get(0));
	    }
	    String s3="select sum(v.amtDueAgent) from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"'  where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum is not null and v.regNum!=''";
	    String sqlq111="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and  regNum is not null and regNum!=''";
	    List totalAmountAllStatus = this.getSession().createSQLQuery(s3).list();
	    if(totalAmountAllStatus!=null && totalAmountAllStatus.get(0)!=null){
	    	DTOVanLine.setTotalAllStatusAmount(totalAmountAllStatus.get(0));
	    }
	    VanLineListWithoutReconcile.add(DTOVanLine);
	    }
	    return VanLineListWithoutReconcile;
	 	}
	public int vanLineListForReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode){
    double vanlineAmount=0.0;
        String loginUser=getRequest().getRemoteUser();
		
		if(vanlineMinimumAmount!=null && !vanlineMinimumAmount.toString().equals("") ){
			vanlineAmount=vanlineMinimumAmount.doubleValue();
			
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	        
	 	}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
		}
	 	int i=0;
	 	if(automaticReconcile){
	 		try{
	 		List accGLList = this.getSession().createSQLQuery("select a.id,a.chargecode,b.contract from accountline as a, serviceorder as s, vanline as v force index (agent) , companydivision  as c , billing as b   where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and  a.serviceOrderId = s.id and  b.id = s.id and b.corpId='"+sessionCorpID+"' and (a.recgl is null or a.recgl ='')  and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and a.status = true and a.download = true and a.billToCode='"+uvlBillToCode+"' and  v.agent like '"
									+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus not in('Approved','Dispute') group by a.shipNumber, if((a.glType = '' or a.glType is null), a.chargeCode, a.glType)").list();
	 		if(accGLList.size()>0)
			{
			Iterator it=accGLList.iterator();
			 while(it.hasNext())
		       {
		           Object []row= (Object [])it.next(); 
		            List glList =this.getSession().createSQLQuery("select concat((if(gl is null or gl = '','NO',gl)),'~',(if(expGl is null or expGl = '','NO',expGl))) from charges where charge='"+row[1]+"' and contract='"+row[2]+"' and corpID = '"+sessionCorpID+"' ").list(); 
		            if(glList!=null && (!(glList.isEmpty())) && glList.get(0)!=null){
						try{
						String[] GLarrayData=glList.get(0).toString().split("~"); 
						String recGl=GLarrayData[0];
						String payGl=GLarrayData[1];
						if((recGl.equalsIgnoreCase("NO"))){
							recGl="";
						}
						if((payGl.equalsIgnoreCase("NO"))){
							payGl="";
						}
						if(!(recGl.equals(""))){
							getHibernateTemplate().bulkUpdate("update AccountLine set recGl='"+recGl+"',payGl='"+payGl+"'  , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now()     where id='"+row[0]+"' ");
						}
					}catch(Exception e){
						
					}
					}
		       }
			}
	 		}catch(Exception e){
	 			
	 		}

		 	List list = this.getSession().createSQLQuery("select v.glType, v.amtDueAgent - sum(a.distributionAmount),v.regNum,v.id,v.reconcileStatus from accountline as a, serviceorder as s,vanline as v force index (agent) , companydivision  as c  where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and  a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and a.billToCode='"+uvlBillToCode+"' and  v.agent like '"
									+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus not in('Approved','Dispute') group by a.shipNumber, if((a.glType = '' or a.glType is null), a.chargeCode, a.glType)").list();
			
			 
			if(list.size()>0)
			{
			Iterator it=list.iterator();
			 while(it.hasNext())
		       {
		           Object []row= (Object [])it.next(); 
		           if(new BigDecimal(row[1].toString()).doubleValue()<=vanlineAmount && new BigDecimal(row[1].toString()).doubleValue()>= - vanlineAmount){
		        	 if(row[4]!=null && row[4].toString().equalsIgnoreCase("Reconcile")){
		        		 
		        	 }else{
		             i= this.getSession().createSQLQuery("update vanline force index (agent)  set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Reconcile', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"' and corpID='"+sessionCorpID+"'"  ).executeUpdate();;
		           }
		           }
		         else{
		        	 i= this.getSession().createSQLQuery("update vanline force index (agent)  set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Suspense', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"' and corpID='"+sessionCorpID+"'"  ).executeUpdate();;
		         }
		       }
			}
			else{}
			 String PaymentStatus="";
			List accountLineList = this.getSession().createSQLQuery("select a.id,v.weekEnding from accountline as a, serviceorder as s,vanline as v  force index (agent)  , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> ''  and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and a.vanlineSettleDate is null and a.billToCode='"+uvlBillToCode+"' and  v.agent like '"
					+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Approved','Reconcile')").list();
			if(automaticReconcile){
				List accountLineList1 = this.getSession().createSQLQuery("select a.id,v.weekEnding,v.amtDueAgent,a.distributionAmount from accountline as a, serviceorder as s,vanline as v  force index (agent) , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and a.status = true and a.download = true   and a.recGl is not null and a.recGl <>''   and a.vanlineSettleDate is null and a.billToCode='"+uvlBillToCode+"' and  v.agent like '"
						+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Reconcile')").list();
				if((accountLineList1!=null) && (!accountLineList1.isEmpty())){
					Iterator accountLineIterator1=accountLineList1.iterator();
					while(accountLineIterator1.hasNext())
				       {
						try{
						 Object []row= (Object [])accountLineIterator1.next(); 
						 try {
							 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MM/yy");
						        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(row[1]) );
						        statusDateNew = nowYYYYMMDD.toString();
						        
						 	}catch (Exception e) {
								e.printStackTrace();
							}
						 	String PaymentSent="";
						 	String receivedAmount="0";
						 	if(new BigDecimal(row[2].toString()).compareTo(new BigDecimal(row[3].toString()))==0){
						 		
						 		PaymentStatus="Fully Paid"; 
						 		PaymentSent=" ,paymentSent=null";
						 		receivedAmount=row[2].toString();
						 	}else{
						 		if((Double.parseDouble(row[3].toString()))>(Double.parseDouble(row[2].toString()))){
						 			PaymentStatus="Fully Paid"; 
							 		PaymentSent=" ,paymentSent=null";
							 		receivedAmount=row[3].toString();	
						 		}else{ 
						 			PaymentStatus="Fully Paid"; 
							 		PaymentSent=" ,paymentSent=null";
							 		receivedAmount=row[3].toString();	
						 		}
						 	}
						 getHibernateTemplate().bulkUpdate("update AccountLine set externalReference='UVL "+statusDateNew+"',receivedAmount='"+receivedAmount+"',paymentStatus='"+PaymentStatus+"',statusDate='"+row[1]+"',vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' "+PaymentSent+" where id='"+row[0]+"' and distributionAmount <> 0 ");
				       }catch (Exception e) {
							e.printStackTrace();
							logger.error("Error executing query"+ e,e);
						}
				       }
				}
			}
			
			if((accountLineList!=null) && (!accountLineList.isEmpty())){
				Iterator accountLineIterator=accountLineList.iterator();
				while(accountLineIterator.hasNext())
			       {
					 Object []row= (Object [])accountLineIterator.next();
					 getHibernateTemplate().bulkUpdate("update AccountLine set vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' where id='"+row[0]+"' and distributionAmount <> 0 ");
			       }	
			}
		 		
	 	}else{
	 	List list = this.getSession().createSQLQuery("select v.glType, v.amtDueAgent - sum(a.distributionAmount),v.regNum,v.id,v.reconcileStatus from accountline as a, serviceorder as s,vanline as v  force index (agent)   where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and  a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and  v.agent like '"
								+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus not in('Approved','Dispute') group by a.shipNumber, if((a.glType = '' or a.glType is null), a.chargeCode, a.glType)").list();
		
		 
		if(list.size()>0)
		{
		Iterator it=list.iterator();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next(); 
	           if(new BigDecimal(row[1].toString()).doubleValue()<=vanlineAmount && new BigDecimal(row[1].toString()).doubleValue()>= - vanlineAmount){
	        	   if(row[4]!=null && row[4].toString().equalsIgnoreCase("Reconcile")){
		        		 
		        	 }else{
	        	   i= this.getSession().createSQLQuery("update vanline  force index (agent)  set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Reconcile', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"' and corpID='"+sessionCorpID+"' "  ).executeUpdate();;
	           }
	           }
	         else{
	        	 i= this.getSession().createSQLQuery("update vanline force index (agent)   set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Suspense', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"' and corpID='"+sessionCorpID+"' "  ).executeUpdate();;
	         }
	       }
		}
		else{}
		 String PaymentStatus="";
		List accountLineList = this.getSession().createSQLQuery("select a.id,v.weekEnding from accountline as a, serviceorder as s,vanline as v  force index (agent)  where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> ''  and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and a.vanlineSettleDate is null and  v.agent like '"
				+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Approved','Reconcile')").list();
		
		if((accountLineList!=null) && (!accountLineList.isEmpty())){
			Iterator accountLineIterator=accountLineList.iterator();
			while(accountLineIterator.hasNext())
		       {
				 Object []row= (Object [])accountLineIterator.next();
				 getHibernateTemplate().bulkUpdate("update AccountLine set vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' where id='"+row[0]+"' and (distributionAmount <> 0 or actualRevenue <> 0)");
		       }	
		}
	 	}
		return i;
	}

	public List chargeAllocation(Long id) { 
		
		return getHibernateTemplate().find("select parentId from SubcontractorCharges where parentId=?",id);
	}
	
	public List getVanLineCodeCompDiv(String corpID){
		return getHibernateTemplate().find("select distinct vanLineCode from CompanyDivision where vanLineCode != '' and corpID=?",corpID);
	}
	
	
	public List getWeekendingList(String agent, String corpId) { 
		List list1=new ArrayList();
		String query="select weekending, reconcilestatus, count(*)  from vanline force index (agent) where corpID='"+corpId+"' and agent = '"+agent+"'  and weekending >= date_sub(now(),interval 365 DAY) group by weekending, reconcilestatus order by weekending desc, reconcilestatus";					 
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			parameterMap.put(row[0].toString()+""+row[1].toString(), row[2].toString());
		}
		String query1="select distinct weekending from vanline force index (agent) where corpID='"+corpId+"' and agent = '"+agent+"'  and weekending >= date_sub(now(),interval 365 DAY) group by weekending, reconcilestatus order by weekending desc, reconcilestatus";					 
		List listb= this.getSession().createSQLQuery(query1).list(); 
		Iterator it1=listb.iterator();
		while(it1.hasNext()){
			Object row= (Object)it1.next();
			DtoAgencyWeekending dtoAgencyWeekending = new DtoAgencyWeekending(); 
			dtoAgencyWeekending.setWeekending((Date)row);
			if(parameterMap.get(row.toString()+"Suspense")!=null){
				dtoAgencyWeekending.setSuspenseCount(parameterMap.get(row.toString()+"Suspense").toString());
			}else{
				dtoAgencyWeekending.setSuspenseCount("0");
			}
			if(parameterMap.get(row.toString()+"Approved")!=null){
				dtoAgencyWeekending.setApprovedCount(parameterMap.get(row.toString()+"Approved").toString());
			}else{
				dtoAgencyWeekending.setApprovedCount("0");
			}
			if(parameterMap.get(row.toString()+"Reconcile")!=null){
				dtoAgencyWeekending.setReconciledCount(parameterMap.get(row.toString()+"Reconcile").toString());
			}else{
				dtoAgencyWeekending.setReconciledCount("0");
			}
			list1.add(dtoAgencyWeekending);
		}
		return list1;
	}
	
	public  class DtoAgencyWeekending{
		private Date weekending;
		private String recordCount;		
		private String suspenseCount;
		private String approvedCount;
		private String reconciledCount;
		
		public String getRecordCount() {
			return recordCount;
		}
		public void setRecordCount(String recordCount) {
			this.recordCount = recordCount;
		}
		public Date getWeekending() {
			return weekending;
		}
		public void setWeekending(Date weekending) {
			this.weekending = weekending;
		}
		public String getSuspenseCount() {
			return suspenseCount;
		}
		public void setSuspenseCount(String suspenseCount) {
			this.suspenseCount = suspenseCount;
		}
		public String getApprovedCount() {
			return approvedCount;
		}
		public void setApprovedCount(String approvedCount) {
			this.approvedCount = approvedCount;
		}
		public String getReconciledCount() {
			return reconciledCount;
		}
		public void setReconciledCount(String reconciledCount) {
			this.reconciledCount = reconciledCount;
		}
	}
	
	public List getMiscellaneousSettlement(String agent,Date weekEnding,String sessionCorpID){
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	    }catch (Exception e) {
			e.printStackTrace();
		}
	    List <Object> MiscellaneousVanLine = new LinkedList<Object>();
	    List <Object> lista = new LinkedList<Object>();
	    
	    if(agent.indexOf(":")==0){
	    	//return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and regNum is null and regNum =''");
	    	String query="select agent,regNum,weekEnding,description,amtDueAgent,reconcileAmount,reconcileStatus,id,statementCategory,shipper,driverName  from vanline force index (agent) where corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and (regNum is null or regNum ='') order by statementCategory";
	    	lista= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("description", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.BIG_INTEGER)
	    	.addScalar("statementCategory", Hibernate.STRING)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.addScalar("driverName", Hibernate.STRING)
	    	.list();
	    	
	    }
	 	else{
	 		String query="select agent,regNum,weekEnding,description,amtDueAgent,reconcileAmount,reconcileStatus ,id ,statementCategory,shipper,driverName from vanline force index (agent) where corpID='"+sessionCorpID+"' and  agent like '" + agent.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%' and (regNum is null or regNum ='') order by statementCategory";
	    	 lista= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("description", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.BIG_INTEGER)
	    	.addScalar("statementCategory", Hibernate.STRING)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.addScalar("driverName", Hibernate.STRING)
	    	 .list();
	 	}
	    Iterator it=lista.iterator(); 
	    DTOForVanLine DTOVanLine =null;
	    while(it.hasNext()){
	    Object [] row=(Object[])it.next();
	    DTOVanLine=new DTOForVanLine();
	    DTOVanLine.setAgent(row[0]);
	    if(row[1]!=null && ! row[1].toString().equals("")){
	    	DTOVanLine.setRegNum(row[1]);
	    	String abc ="select if(v.reconcileStatus='Suspense',concat('a',v.reconcileStatus),if(v.reconcileStatus='Dispute',concat('b',v.reconcileStatus),if(v.reconcileStatus='Approved',concat('c',v.reconcileStatus),if(v.reconcileStatus='Reconcile',concat('d',v.reconcileStatus),if(v.reconcileStatus='',concat('e',v.reconcileStatus),''))))) AS reconcileStatus from vanline v force index (agent) where  v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum='"+row[1].toString()+"' order by reconcileStatus";
	    	List reconcileStatus=this.getSession().createSQLQuery("select if(v.reconcileStatus='Suspense',concat('a',v.reconcileStatus),if(v.reconcileStatus='Dispute',concat('b',v.reconcileStatus),if(v.reconcileStatus='Approved',concat('c',v.reconcileStatus),if(v.reconcileStatus='Reconcile',concat('d',v.reconcileStatus),if(v.reconcileStatus='',concat('e',v.reconcileStatus),''))))) AS reconcileStatus from vanline v force index (agent)  where  v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum='"+row[1].toString()+"' order by reconcileStatus").list();	
			if(reconcileStatus!=null && !reconcileStatus.isEmpty() && reconcileStatus.get(0)!=null){
				if(reconcileStatus.get(0).toString().equalsIgnoreCase("aSuspense")){
			    	DTOVanLine.setReconcileStatus("Suspense");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("bDispute")){
			    	DTOVanLine.setReconcileStatus("Dispute");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("cApproved")){
			    	DTOVanLine.setReconcileStatus("Approved");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("dReconcile")){
			    	DTOVanLine.setReconcileStatus("Reconcile");
			    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("e")){
			    	DTOVanLine.setReconcileStatus("");
			    }else{
			    	DTOVanLine.setReconcileStatus("");
			    }
			}else{
				DTOVanLine.setReconcileStatus("");
			}
		}else{
			DTOVanLine.setRegNum(" ");
			//DTOVanLine.setReconcileStatus("");
			DTOVanLine.setReconcileStatus(row[6]);
		}
	    DTOVanLine.setWeekEnding(row[2]);
	    if(row[3]!=null){
	    DTOVanLine.setDescription(row[3]);
	    }
	    else
	    	DTOVanLine.setDescription("");
	    DTOVanLine.setAmtDueAgent(row[4]);
	    DTOVanLine.setReconcileAmount(row[5]);
	   // DTOVanLine.setReconcileStatus(row[6]);
	    DTOVanLine.setId(row[7]);
	    if(row[8]!=null){
	    DTOVanLine.setStatementCategory(row[8]);
	    }
	    else
	    	 DTOVanLine.setStatementCategory("");
	    if(null!=row[9]){
	    DTOVanLine.setShipper(row[9]);
	    }
	    else
	    	  DTOVanLine.setShipper("");
	 
	    String sqlq1="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Reconcile','Approved')and (regNum is null or regNum ='')";
	    List totalAmountRecApp = this.getSession().createSQLQuery(sqlq1).list(); 	                             
	    if(totalAmountRecApp.get(0)!=null){
	    	DTOVanLine.setReconcileStatusAmount(totalAmountRecApp.get(0));
	    }
	  //Added For #7921--29 Aprl--
	    String sqlq11="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Suspense') and (regNum is null or regNum ='')";
	    List totalAmountSuspense = this.getSession().createSQLQuery(sqlq11).list();
	    if(totalAmountSuspense!=null && totalAmountSuspense.get(0)!=null){
	    	DTOVanLine.setSuspenseStatusAmount(totalAmountSuspense.get(0));
	    }
	    String sqlq111="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and (regNum is null or regNum ='')";
	    List totalAmountAllStatus = this.getSession().createSQLQuery(sqlq111).list();
	    if(totalAmountAllStatus!=null && totalAmountAllStatus.get(0)!=null){
	    	DTOVanLine.setTotalAllStatusAmount(totalAmountAllStatus.get(0));
	    }
	    DTOVanLine.setDriverName(row[10]);
	    MiscellaneousVanLine.add(DTOVanLine);
	}
	    return MiscellaneousVanLine;
	    }
	//Start For Category Settlement
	public List getCategoryWithSettlementList(String agent,Date weekEnding,String sessionCorpID){
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
	        statusDateNew = nowYYYYMMDD.toString();
	    }catch (Exception e) {
			e.printStackTrace();
		}
	    List <Object> MiscellaneousVanLine = new LinkedList<Object>();
	    List <Object> lista = new LinkedList<Object>();
	    String query="";
	    if(agent.indexOf(":")==0){
	    	//return getHibernateTemplate().find("from VanLine where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and regNum is null and regNum =''");
	    	//String query="select agent,regNum,weekEnding,description,amtDueAgent,reconcileAmount,
	    	//reconcileStatus,id,statementCategory,shipper  from vanline where 
	    	//corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and (regNum is null or regNum ='')";
	    	//String query="select agent,regNum,weekEnding,shipper,description,statementCategory,id,amtDueAgent,reconcileAmount,reconcileStatus  from vanline where corpID='"+sessionCorpID+"' and  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''")  + "%' and weekEnding like '"+statusDateNew+"%' and (regNum is null or regNum ='') group by statementCategory union select agent,regNum,weekEnding,shipper,description,statementCategory,id,sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount,reconcileStatus  from vanline where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''")   + "%' and weekEnding like '"+statusDateNew+"%' and regNum is not null and regNum!='' group by statementCategory ";
	    	/*String query="select * from (select agent,regNum,weekEnding,shipper,v.description,statementCategory , " +
	    			"v.id,amtDueAgent,reconcileAmount,reconcileStatus,r.flex4  from vanline v,refmaster r " +
	    			"where v.corpID='"+sessionCorpID+"' " +
	    			"and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' " +
	    			"and weekEnding like '"+statusDateNew+"%' " +
	    			"and (regNum is null or regNum ='') and r.code=v.statementCategory " +
	    			"and r.parameter= 'VLSTMTCATEGORY' group by v.statementCategory " +
	    			"union  select agent,regNum,weekEnding,shipper,v.description, " +
	    			"statementCategory,v.id,sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount, " +
	    			"reconcileStatus,r.flex4  from vanline v , refmaster r where v.corpID='"+sessionCorpID+"' " +
	    			"and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' " +
	    			" and weekEnding like '"+statusDateNew+"%' " +
	    			"and r.code=v.statementCategory and r.parameter= 'VLSTMTCATEGORY' " +
	    			"and regNum is not null and regNum!='' group by v.statementCategory) a order by flex4";
	    	*/
	    	
	    	 query="select agent,regNum,weekEnding,shipper,v.description,statementCategory , " +
			"v.id,sum(amtDueAgent)as amtDueAgent,sum(reconcileAmount) as reconcileAmount," +
			"reconcileStatus,r.flex4,r.description  as statementDescription from vanline v force index (agent),refmaster r " +
			"where v.corpID='"+sessionCorpID+"' " +
			"and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' " +
			"and weekEnding like '"+statusDateNew+"%' " +
			" and r.code=v.statementCategory " +
			"and r.parameter= 'VLSTMTCATEGORY' group by v.statementCategory  order by CAST(r.flex4 AS UNSIGNED INTEGER)";
	/*
	    	lista= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("description", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.BIG_INTEGER)
	    	.addScalar("statementCategory", Hibernate.STRING)
	    	.addScalar("shipper", Hibernate.STRING)
	    	.list();
	    	*/
	    }
	 	else{
	 		//String query="select agent,regNum,weekEnding,description,amtDueAgent,reconcileAmount,reconcileStatus ,id ,statementCategory,shipper from vanline where corpID='"+sessionCorpID+"' and  agent like '" + agent.replaceAll("'", "''") + "%' and weekEnding like '"+statusDateNew+"%' and (regNum is null or regNum ='')";
	    	/*String query="select * from (select agent,regNum,weekEnding,shipper,v.description,statementCategory,v.id, " +
	    			"amtDueAgent,reconcileAmount,reconcileStatus,r.flex4  from vanline v ,refmaster r  where " +
	    			"v.corpID='"+sessionCorpID+"' and  agent like '" + agent.replaceAll("'", "''") + "%' " +
	    			"and weekEnding like '"+statusDateNew+"%' and (regNum is null or regNum ='') " +
	    			"and r.code=v.statementCategory and r.parameter= 'VLSTMTCATEGORY' " +
	    			"group by statementCategory " +
	    			"union select agent,regNum,weekEnding,shipper,v.description,statementCategory,v.id, " +
	    			"sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount, " +
	    			"reconcileStatus,r.flex4  from vanline v , refmaster r where  v.corpID='"+sessionCorpID+"' " +
	    			"and agent like '" + agent.replaceAll("'", "''") + "%' " +
	    		    "and weekEnding like '"+statusDateNew+"%' " +
	    		    "and r.code=v.statementCategory and r.parameter= 'VLSTMTCATEGORY' " +
	    		    "and regNum is not null and regNum!='' group by statementCategory) a order by flex4 ";*/
	 		
	 		 query = "select agent,regNum,weekEnding,shipper,v.description,statementCategory,v.id, " +
			"sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount, " +
			"reconcileStatus,r.flex4,r.description as statementDescription  from vanline v , refmaster r where  v.corpID='"+sessionCorpID+"' " +
			"and agent like '" + agent.replaceAll("'", "''") + "%' " +
		    "and weekEnding like '"+statusDateNew+"%' " +
		    "and r.code=v.statementCategory and r.parameter= 'VLSTMTCATEGORY' " +
		    " group by statementCategory  order by CAST(r.flex4 AS UNSIGNED INTEGER)"; 
	 	/*	
	 		lista= this.getSession().createSQLQuery(query)
	    	.addScalar("agent",Hibernate.STRING)
	    	.addScalar("regNum", Hibernate.STRING)
	    	.addScalar("weekEnding", Hibernate.DATE)
	    	.addScalar("description", Hibernate.STRING)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileStatus", Hibernate.STRING)
	    	.addScalar("id", Hibernate.BIG_INTEGER)
	    	.addScalar("statementCategory", Hibernate.STRING)
	    	.addScalar("shipper", Hibernate.STRING)
	    	 .list();*/
	 		
	 	}
	    lista= this.getSession().createSQLQuery(query)
    	.addScalar("agent",Hibernate.STRING)
    	.addScalar("regNum", Hibernate.STRING)
    	.addScalar("weekEnding", Hibernate.DATE)
    	.addScalar("description", Hibernate.STRING)
    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
    	.addScalar("reconcileStatus", Hibernate.STRING)
    	.addScalar("id", Hibernate.BIG_INTEGER)
    	.addScalar("statementCategory", Hibernate.STRING)
    	.addScalar("shipper", Hibernate.STRING)
    	.addScalar("statementDescription", Hibernate.STRING)
    	 .list();
	    Iterator it=lista.iterator(); 
	    DTOForVanLine DTOVanLine =null;
	    while(it.hasNext()){
	    Object [] row=(Object[])it.next();
	    DTOVanLine=new DTOForVanLine();
	    DTOVanLine.setAgent(row[0]);
	    if(row[1]!=null){
	    	DTOVanLine.setRegNum(row[1]);
		}else{
			DTOVanLine.setRegNum(" ");
		}
	    DTOVanLine.setWeekEnding(row[2]);
	    if(row[3]!=null){
	    DTOVanLine.setDescription(row[3]);
	    }
	    else
	    	DTOVanLine.setDescription("");
	    DTOVanLine.setAmtDueAgent(row[4]);
	    DTOVanLine.setReconcileAmount(row[5]);
	    DTOVanLine.setReconcileStatus(row[6]);
	    DTOVanLine.setId(row[7]);
	    if(row[8]!=null){
	    DTOVanLine.setStatementCategory(row[8]);
	    }
	    else
	    	 DTOVanLine.setStatementCategory("");
	    if(null!=row[9]){
	    DTOVanLine.setShipper(row[9]);
	    }
	    else
	    	  DTOVanLine.setShipper("");
	    if(null!=row[10]){
		    DTOVanLine.setStatementDescription(row[10]);
		    }
		    else
		    {
		    	  DTOVanLine.setStatementDescription("");
		    }
	 String sqlq1="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Reconcile','Approved')";
	    List totalAmountRecApp = this.getSession().createSQLQuery(sqlq1).list(); 	                             
	    if(totalAmountRecApp.get(0)!=null){
	    	DTOVanLine.setReconcileStatusAmount(totalAmountRecApp.get(0));
	    }
	  //Added For #7921--29 Aprl--
	    String sqlq11="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%'  and reconcilestatus in ('Suspense')";
	    List totalAmountSuspense = this.getSession().createSQLQuery(sqlq11).list();
	    if(totalAmountSuspense!=null && totalAmountSuspense.get(0)!=null){
	    	DTOVanLine.setSuspenseStatusAmount(totalAmountSuspense.get(0));
	    }
	    String sqlq111="select sum(amtDueAgent) as reconcileStatusAmount from vanline force index (agent) where  corpID='"+sessionCorpID+"' and agent like '" + agent.replaceAll("'", "''") 
		+ "%' and weekEnding like '"+statusDateNew+"%' ";
	    List totalAmountAllStatus = this.getSession().createSQLQuery(sqlq111).list();
	    if(totalAmountAllStatus!=null && totalAmountAllStatus.get(0)!=null){
	    	DTOVanLine.setTotalAllStatusAmount(totalAmountAllStatus.get(0));
	    }
	    MiscellaneousVanLine.add(DTOVanLine);
	    }
	    return MiscellaneousVanLine;
	    }
	
	
	
	
	//End
	
	 public  class DTOForVanLine{
		 private Object agent;
		 private Object regNum;
		 private Object weekEnding;
		 private Object description;
		 private Object amtDueAgent;
		 private Object reconcileAmount;
		 private Object reconcileStatus;
		 private Object id;
		 private Object statementCategory;
		 private Object shipper;
		 private Object tranDate;
		 private Object driverName;
		 private Object distributionCodeDescription;
		 private Object glType;
		 private Object statementDescription;
		 private Object reconcileStatusAmount;
		 private Object soId;
		 private Object suspenseStatusAmount;
		 private Object totalAllStatusAmount;
		public Object getAgent() {
			return agent;
		}
		public void setAgent(Object agent) {
			this.agent = agent;
		}
		public Object getRegNum() {
			return regNum;
		}
		public void setRegNum(Object regNum) {
			this.regNum = regNum;
		}
		public Object getWeekEnding() {
			return weekEnding;
		}
		public void setWeekEnding(Object weekEnding) {
			this.weekEnding = weekEnding;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getAmtDueAgent() {
			return amtDueAgent;
		}
		public void setAmtDueAgent(Object amtDueAgent) {
			this.amtDueAgent = amtDueAgent;
		}
		public Object getReconcileAmount() {
			return reconcileAmount;
		}
		public void setReconcileAmount(Object reconcileAmount) {
			this.reconcileAmount = reconcileAmount;
		}
	
		public Object getReconcileStatus() {
			return reconcileStatus;
		}
		public void setReconcileStatus(Object reconcileStatus) {
			this.reconcileStatus = reconcileStatus;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getStatementCategory() {
			return statementCategory;
		}
		public void setStatementCategory(Object statementCategory) {
			this.statementCategory = statementCategory;
		}
		public Object getShipper() {
			return shipper;
		}
		public void setShipper(Object shipper) {
			this.shipper = shipper;
		}
		public Object getTranDate() {
			return tranDate;
		}
		public Object getDriverName() {
			return driverName;
		}
		public Object getDistributionCodeDescription() {
			return distributionCodeDescription;
		}
		public void setTranDate(Object tranDate) {
			this.tranDate = tranDate;
		}
		public void setDriverName(Object driverName) {
			this.driverName = driverName;
		}
		public void setDistributionCodeDescription(Object distributionCodeDescription) {
			this.distributionCodeDescription = distributionCodeDescription;
		}
		public Object getGlType() {
			return glType;
		}
		public void setGlType(Object glType) {
			this.glType = glType;
		}
		public Object getStatementDescription() {
			return statementDescription;
		}
		public void setStatementDescription(Object statementDescription) {
			this.statementDescription = statementDescription;
		}
		public Object getReconcileStatusAmount() {
			return reconcileStatusAmount;
		}
		public void setReconcileStatusAmount(Object reconcileStatusAmount) {
			this.reconcileStatusAmount = reconcileStatusAmount;
		}
		public Object getSoId() {
			return soId;
		}
		public void setSoId(Object soId) {
			this.soId = soId;
		}
		public Object getSuspenseStatusAmount() {
			return suspenseStatusAmount;
		}
		public void setSuspenseStatusAmount(Object suspenseStatusAmount) {
			this.suspenseStatusAmount = suspenseStatusAmount;
		}
		public Object getTotalAllStatusAmount() {
			return totalAllStatusAmount;
		}
		public void setTotalAllStatusAmount(Object totalAllStatusAmount) {
			this.totalAllStatusAmount = totalAllStatusAmount;
		}
	 }
	 public List getRegNumList(String vanRegNum,String agent,Date weekending){
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekending) );
		        statusDateNew = nowYYYYMMDD.toString();
		    }catch (Exception e) {
				e.printStackTrace();
			}
		    
		 return getHibernateTemplate().find("from VanLine  where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and regNum='"+vanRegNum+"'");
	 }
	 public List vanLineCategoryRegNum(String vanRegNum,String agent,String weekending,String sessionCorpID){
		 /*try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekending) );
		        statusDateNew = nowYYYYMMDD.toString();
		    }catch (Exception e) {
				e.printStackTrace();
			}*/
		    
		// return getHibernateTemplate().find("from VanLine  where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+weekending+"%' and statementCategory ='"+vanRegNum+"' group by regNum");
		 List <Object> MiscellaneousVanLine = new ArrayList<Object>();
		 List <Object> lista = new ArrayList<Object>();
		// String query="select agent,regNum,weekEnding,shipper,description,statementCategory,id,sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount,reconcileStatus,tranDate,driverName,distributionCodeDescription,glType from vanline where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+weekending+"%' and statementCategory ='"+vanRegNum+"' and (regNum is not null and regNum!='' )group by regNum union select agent,regNum,weekEnding,shipper,description,statementCategory,id,amtDueAgent,reconcileAmount,reconcileStatus,tranDate,driverName,distributionCodeDescription,glType from vanline where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+weekending+"%' and statementCategory ='"+vanRegNum+"' and (regNum is  null or regNum='' ) group by regNum";
		 /*if(vanRegNum!=null && !vanRegNum.equals("")){
			 query="select agent,regNum,weekEnding,shipper,description,statementCategory,id,sum(amtDueAgent) as amtDueAgent,sum(reconcileAmount) as reconcileAmount,reconcileStatus,tranDate,driverName,distributionCodeDescription,glType from vanline where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+weekending+"%' and statementCategory ='"+vanRegNum+"' group by regNum"; 
		 }
		 else{
		 query="select agent,regNum,weekEnding,shipper,description,statementCategory,id,amtDueAgent,reconcileAmount,reconcileStatus,tranDate,driverName,distributionCodeDescription,glType from vanline where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+weekending+"%' and statementCategory ='"+vanRegNum+"' group by regNum";
		 }*/
		 String query="";
		 if(agent.indexOf(":")==0){
			 if(vanRegNum.equals("DSTR") || vanRegNum.equals("BALF")){
				 query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.description,v.statementCategory,v.id,sum(v.amtDueAgent) as amtDueAgent,sum(v.reconcileAmount) as reconcileAmount,v.reconcileStatus,v.tranDate,v.driverName,v.distributionCodeDescription,v.glType,s.id as soId from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+weekending+"%' and v.statementCategory ='"+vanRegNum+"' group by v.regNum";
			 }else{
			     query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.description,v.statementCategory,v.id,v.amtDueAgent as amtDueAgent,v.reconcileAmount as reconcileAmount,v.reconcileStatus,v.tranDate,v.driverName,v.distributionCodeDescription,v.glType,s.id as soId from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+weekending+"%' and v.statementCategory ='"+vanRegNum+"' group by id ";
			}
		 }
			else{
				if(vanRegNum.equals("DSTR") || vanRegNum.equals("BALF")){
				query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.description,v.statementCategory,v.id,sum(v.amtDueAgent) as amtDueAgent,sum(v.reconcileAmount)as reconcileAmount,v.reconcileStatus,v.tranDate,v.driverName,v.distributionCodeDescription,v.glType,s.id as soId from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' where  v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+weekending+"%' and v.statementCategory ='"+vanRegNum+"' group by v.regNum";
				}else{
				query="select v.agent,v.regNum,v.weekEnding,v.shipper,v.description,v.statementCategory,v.id,v.amtDueAgent as amtDueAgent,v.reconcileAmount as reconcileAmount,v.reconcileStatus,v.tranDate,v.driverName,v.distributionCodeDescription,v.glType,s.id as soId from vanline v force index (agent) left outer join serviceorder s on s.registrationNumber=v.regNum and s.corpID='"+sessionCorpID+"' where v.corpID='"+sessionCorpID+"' and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+weekending+"%' and v.statementCategory ='"+vanRegNum+"' group by id";
				}
			 
			}
	 		lista= this.getSession().createSQLQuery(query)
	    	.addScalar("v.agent",Hibernate.STRING)
	    	.addScalar("v.regNum", Hibernate.STRING)
	    	.addScalar("v.weekEnding", Hibernate.DATE)
	    	.addScalar("v.shipper", Hibernate.STRING)
	    	.addScalar("v.description", Hibernate.STRING)
	    	.addScalar("v.statementCategory", Hibernate.STRING)
	    	.addScalar("v.id", Hibernate.BIG_INTEGER)
	    	.addScalar("amtDueAgent", Hibernate.BIG_DECIMAL)
	    	.addScalar("reconcileAmount", Hibernate.BIG_DECIMAL)
	    	.addScalar("v.reconcileStatus", Hibernate.STRING)
	    	.addScalar("v.tranDate", Hibernate.DATE)
	    	.addScalar("v.driverName", Hibernate.STRING)
	    	.addScalar("v.distributionCodeDescription", Hibernate.STRING)
	    	.addScalar("v.glType", Hibernate.STRING) 
	    	.addScalar("soId", Hibernate.BIG_INTEGER)
	    	 .list();
	 		 Iterator it=lista.iterator(); 
	 	    DTOForVanLine DTOVanLine =null;
	 	    while(it.hasNext()){
	 	    Object [] row=(Object[])it.next();
	 	    DTOVanLine=new DTOForVanLine();
	 	    DTOVanLine.setAgent(row[0]);
	 	    if(row[5]!=null && (!(row[5].toString().equals(""))) && ((row[5].toString().equals("DSTR")) || (row[5].toString().equals("BALF"))) ){
	 	    	DTOVanLine.setRegNum(row[1]);
		    	List reconcileStatus=this.getSession().createSQLQuery("select if(v.reconcileStatus='Suspense',concat('a',v.reconcileStatus),if(v.reconcileStatus='Dispute',concat('b',v.reconcileStatus),if(v.reconcileStatus='Approved',concat('c',v.reconcileStatus),if(v.reconcileStatus='Reconcile',concat('d',v.reconcileStatus),if(v.reconcileStatus='',concat('e',v.reconcileStatus),''))))) AS reconcileStatus from vanline v force index (agent) where  v.corpID='"+sessionCorpID+"' and  v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' and v.regNum='"+row[1].toString()+"' order by reconcileStatus").list();	
				if(reconcileStatus!=null && !reconcileStatus.isEmpty() && reconcileStatus.get(0)!=null){
					if(reconcileStatus.get(0).toString().equalsIgnoreCase("aSuspense")){
				    	DTOVanLine.setReconcileStatus("Suspense");
				    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("bDispute")){
				    	DTOVanLine.setReconcileStatus("Dispute");
				    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("cApproved")){
				    	DTOVanLine.setReconcileStatus("Approved");
				    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("dReconcile")){
				    	DTOVanLine.setReconcileStatus("Reconcile");
				    }else if(reconcileStatus.get(0).toString().equalsIgnoreCase("e")){
				    	DTOVanLine.setReconcileStatus("");
				    }else{
				    	DTOVanLine.setReconcileStatus("");
				    }
				}
	 		}else{
	 			DTOVanLine.setRegNum(" ");
	 			if(row[9]!=null){
	 			DTOVanLine.setReconcileStatus(row[9]);
	 			}else{
	 			DTOVanLine.setReconcileStatus("");	
	 			}
	 		}
	 	    DTOVanLine.setWeekEnding(row[2]);
	 	    if(row[3]!=null){
	 	    DTOVanLine.setShipper(row[3]);
	 	    }
	 	    else{
	 	    DTOVanLine.setShipper("");
	 	    }
	 	    DTOVanLine.setDescription(row[4]); 
	 	    DTOVanLine.setStatementCategory(row[5]);
	 	    DTOVanLine.setId(row[6]);
	 	    DTOVanLine.setAmtDueAgent(row[7]);
	 	    if(row[8]!=null){
	 	    DTOVanLine.setReconcileAmount(row[8]);
	 	    }
	 	    else{
	 	    DTOVanLine.setReconcileAmount("");
	 	    } 
	 	    DTOVanLine.setTranDate(row[10]);
		 	if(row[11]!=null){
		 	    DTOVanLine.setDriverName(row[11]);
		 	}
	 	    else{
	    	  DTOVanLine.setDriverName("");
	 	    }
	 	    if(row[12]!=null){
		 	    DTOVanLine.setDistributionCodeDescription(row[12]);
		 	}
	 	    else{
	    	  DTOVanLine.setDistributionCodeDescription("");
	 	    }
	    	if(row[13]!=null){
			   DTOVanLine.setGlType(row[13]);
			}
		 	else{
		      DTOVanLine.setGlType("");
		 	}
		 	if(row[14]!=null){
		 	  DTOVanLine.setSoId(row[14]);  
		 	}
		 	else{
		 	  DTOVanLine.setSoId("");  
		 	}
	 	    MiscellaneousVanLine.add(DTOVanLine);
	 	    }
	 	    return MiscellaneousVanLine;
	 }
	 public List vanLineCategoryRegNumberListMethod(String category,String vanRegNum,String agent,String weekEnding ){
		 //return getHibernateTemplate().find("from VanLine  where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+weekEnding+"%' and statementCategory ='"+category+"' and regNum='"+vanRegNum+"'");
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
		        statusDateNew = nowYYYYMMDD.toString();
		    }catch (Exception e) {
				e.printStackTrace();
			}
		    return getHibernateTemplate().find("from VanLine  where  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and statementCategory ='"+category+"' and regNum='"+vanRegNum+"'");
	 }
	 
	 public List getRegNumVanline(String agent, Date weekEnding){
		 
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				e.printStackTrace();
			}
		//List vanlineregNum=this.getSession().createSQLQuery("select distinct v.regNum from vanline as v where v.regNum <> '' and v.regNum is not null and  v.agent like '"+agent+ "' and v.weekEnding like '" +statusDateNew+ "%' and v.reconcileStatus='Suspense'").list();
			//List vanlineregNum=this.getSession().createSQLQuery("select  regNum from vanline where regNum <> '' and regNum is not null and glType <> '' and glType is not null and glCode<>'' and glType is not null and  v.agent like '"+agent+ "' and weekEnding like '2012-06-14%' and reconcileStatus='Suspense'").list();
			
		 return getHibernateTemplate().find("select id from VanLine where regNum <> '' and regNum is not null and glType <> '' and glType is not null  and  agent like '"+agent+ "' and weekEnding like '" +statusDateNew+ "%' and statementCategory !='MCLM' and reconcileStatus='Suspense' order by  regNum ");
	 }
	 public List getShipNumber(String vanRegNum){
		 //String ars="select id from ServiceOrder where registrationNumber='"+vanRegNum+"'";
		 return	getHibernateTemplate().find("select id from ServiceOrder where registrationNumber='"+vanRegNum+"'");
		  
	 }
		public List<SystemDefault> findsysDefault(String corpID){
	    	return getHibernateTemplate().find("from SystemDefault where corpID = ?", corpID);
	    }

		public String getCompanyCode(String accountingAgent, String sessionCorpID) {
			String companyCode="";
			List companyCodeList = getHibernateTemplate().find("select companyCode from CompanyDivision where vanLineCode='" + accountingAgent +"' and corpID = '"+sessionCorpID+"' ");
			if(companyCodeList!=null && (!(companyCodeList.isEmpty())) && companyCodeList.get(0)!=null && (!(companyCodeList.get(0).toString().equals("")))){
				companyCode=companyCodeList.get(0).toString();
			}
			return companyCode;
		}
		
		public List getvanLineExtract(String agent,Date weekending,String sessionCorpID){
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekending) );
	        statusDateNew = nowYYYYMMDD.toString();
	      List  extractVanLine = new ArrayList();
		  List getvanLineExtract=new ArrayList();
		  String query="select v.regNum,s.shipNumber,v.distributionCodeDescription,v.glType,v.shipper,v.amtDueAgent,v.reconcileAmount,v.reconcileStatus,a.RecInvoiceNumber,sum(a.actualrevenue) as actualrevenue ,sum(a.distributionAmount) as distributionAmount  " +
		  		"from vanline  v force index (agent) INNER JOIN serviceorder  s on v.regNum=s.registrationNumber " +
		  		"and s.corpid='"+sessionCorpID+"' " +
		  		"INNER JOIN accountline a on a.serviceOrderId = s.id and a.chargeCode=v.glType and a.corpid='"+sessionCorpID+"'" +
		  		" where v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' " +
		  		"and v.corpid='"+sessionCorpID+"' and v.regNum is not null and v.regNum!=''  group by a.shipNumber,v.glType ";
			//String query="select v.regNum,s.shipNumber,v.distributionCodeDescription,v.glType,v.shipper,v.amtDueAgent,v.reconcileAmount,v.reconcileStatus,a.RecInvoiceNumber,a.actualrevenue,a.distributionAmount from accountline as a, serviceorder as s,vanline as v where s.registrationNumber =v.regNum and s.shipNumber=a.shipNumber and a.corpid=s.corpid and s.corpid=v.corpid and agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '"+statusDateNew+"%' and v.corpid='"+sessionCorpID+"' and regNum is not null and regNum!='' group by regNum";
			getvanLineExtract=this.getSession().createSQLQuery(query).list();
			Iterator it=getvanLineExtract.iterator();
			DTOForVanLineExtract DTOVanExtract =null;
			 while(it.hasNext()){
				 Object [] ob=( Object[])it.next();
				 DTOVanExtract=new DTOForVanLineExtract();
				 if(ob[0]!=null){
					 DTOVanExtract.setRegNum(ob[0]);	 
				 }else{
					 DTOVanExtract.setRegNum(""); 
				 }
				 if(ob[1]!=null){
					 DTOVanExtract.setShipNumber(ob[1]);	 
				 }else{
					 DTOVanExtract.setShipNumber("");
				 } 
				 if(ob[2]!=null){
					 DTOVanExtract.setDistributionCodeDescription(ob[2]);	 
				 }else{
					 DTOVanExtract.setDistributionCodeDescription("");
				 } 
				 if(ob[3]!=null){
					 DTOVanExtract.setGlType(ob[3]);	 
				 }else{
					 DTOVanExtract.setGlType("");
				 } 
				 if(ob[4]!=null){
					 DTOVanExtract.setShipper(ob[4]);	 
				 }else{
					 DTOVanExtract.setShipper(""); 
				 } 
				 if(ob[5]!=null){
					 DTOVanExtract.setAmtDueAgent(ob[5]);	 
				 }else{
					 DTOVanExtract.setAmtDueAgent("");
				 } 
				 if(ob[6]!=null){
					 DTOVanExtract.setReconcileAmount(ob[6]);	 
				 }else{
					 DTOVanExtract.setReconcileAmount("");
				 } 
				 if(ob[7]!=null){
					 DTOVanExtract.setReconcileStatus(ob[7]); 
				 }else{
					 DTOVanExtract.setReconcileStatus("");
				 } 
				 if(ob[8]!=null){
					 DTOVanExtract.setRecInvoiceNumber(ob[8]);	 
				 }else{
					 DTOVanExtract.setRecInvoiceNumber("");
				 }
				 if(ob[9]!=null){
					 DTOVanExtract.setActualrevenue(ob[9]);	 
				 }else{
					 DTOVanExtract.setActualrevenue("");
				 }
				 if(ob[10]!=null){
					 DTOVanExtract.setDistributionAmount(ob[10]);	 
				 }else{
					 DTOVanExtract.setDistributionAmount("");
				 }
				
				 extractVanLine.add(DTOVanExtract);
			 }
			return  extractVanLine;
			
		}
		 public  class DTOForVanLineExtract{
			 private Object regNum;
			 private Object shipNumber;
			 private Object distributionCodeDescription;
			 private Object glType;
			 private Object shipper;
			 private Object amtDueAgent;
			 private Object reconcileAmount;
			 private Object reconcileStatus;
			 private Object RecInvoiceNumber;
			 private Object actualrevenue;
			 private Object distributionAmount;
			public Object getRegNum() {
				return regNum;
			}
			public void setRegNum(Object regNum) {
				this.regNum = regNum;
			}
			public Object getShipNumber() {
				return shipNumber;
			}
			public void setShipNumber(Object shipNumber) {
				this.shipNumber = shipNumber;
			}
			public Object getDistributionCodeDescription() {
				return distributionCodeDescription;
			}
			public void setDistributionCodeDescription(Object distributionCodeDescription) {
				this.distributionCodeDescription = distributionCodeDescription;
			}
			public Object getGlType() {
				return glType;
			}
			public void setGlType(Object glType) {
				this.glType = glType;
			}
			public Object getShipper() {
				return shipper;
			}
			public void setShipper(Object shipper) {
				this.shipper = shipper;
			}
			public Object getAmtDueAgent() {
				return amtDueAgent;
			}
			public void setAmtDueAgent(Object amtDueAgent) {
				this.amtDueAgent = amtDueAgent;
			}
			public Object getReconcileAmount() {
				return reconcileAmount;
			}
			public void setReconcileAmount(Object reconcileAmount) {
				this.reconcileAmount = reconcileAmount;
			}
			public Object getReconcileStatus() {
				return reconcileStatus;
			}
			public void setReconcileStatus(Object reconcileStatus) {
				this.reconcileStatus = reconcileStatus;
			}
			public Object getRecInvoiceNumber() {
				return RecInvoiceNumber;
			}
			public void setRecInvoiceNumber(Object recInvoiceNumber) {
				RecInvoiceNumber = recInvoiceNumber;
			}
			public Object getActualrevenue() {
				return actualrevenue;
			}
			public void setActualrevenue(Object actualrevenue) {
				this.actualrevenue = actualrevenue;
			}
			public Object getDistributionAmount() {
				return distributionAmount;
			}
			public void setDistributionAmount(Object distributionAmount) {
				this.distributionAmount = distributionAmount;
			}
			@Override
			public String toString() {
				return  regNum + ", " + shipNumber+ ", "
						+ distributionCodeDescription + ", " + glType
						+ ", " + shipper + ", "
						+ amtDueAgent + ", " + reconcileAmount
						+ ", " + reconcileStatus
						+ ", " + RecInvoiceNumber
						+ ", " + actualrevenue
						+ ", " + distributionAmount;
			}
			
			 
		 }
		public int updateAccountLineWithReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode) {
			int i=0;
			double vanlineAmount=0.0;
	        String loginUser=getRequest().getRemoteUser();
			
			if(vanlineMinimumAmount!=null && !vanlineMinimumAmount.toString().equals("") ){
				vanlineAmount=vanlineMinimumAmount.doubleValue();
				
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				e.printStackTrace();
			}
		 	 String PaymentStatus="";
			if(automaticReconcile){
				List accountLineList1 = this.getSession().createSQLQuery("select a.id,v.weekEnding,v.amtDueAgent,a.distributionAmount from accountline as a, serviceorder as s,vanline as v  force index (agent)   , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and a.status = true and a.download = true  and v.statementCategory !='MCLM' and a.recGl is not null and a.recGl <>''   and a.vanlineSettleDate is null  and a.billToCode='"+uvlBillToCode+"' and  v.agent like '"
						+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Reconcile')").list();
				if((accountLineList1!=null) && (!accountLineList1.isEmpty())){
					Iterator accountLineIterator1=accountLineList1.iterator();
					while(accountLineIterator1.hasNext())
				       {
						try{
						 Object []row= (Object [])accountLineIterator1.next(); 
						 try {
							 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MM/yy");
						        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(row[1]) );
						        statusDateNew = nowYYYYMMDD.toString();
						        
						 	}catch (Exception e) {
								e.printStackTrace();
							}
						 	String receivedAmount="0";
						 	String PaymentSent="";
						 	if(new BigDecimal(row[2].toString()).compareTo(new BigDecimal(row[3].toString()))==0){
						 		
						 		PaymentStatus="Fully Paid"; 
						 		PaymentSent=" ,paymentSent=null";
						 		receivedAmount=row[2].toString();
						 	}else{
						 		if((Double.parseDouble(row[3].toString()))>(Double.parseDouble(row[2].toString()))){
						 			PaymentStatus="Fully Paid"; 
							 		PaymentSent=" ,paymentSent=null";
							 		receivedAmount=row[3].toString();	
						 		}else{ 
						 			PaymentStatus="Fully Paid"; 
							 		PaymentSent=" ,paymentSent=null";
							 		receivedAmount=row[3].toString();	
						 		}
						 	}
						i= getHibernateTemplate().bulkUpdate("update AccountLine set externalReference='UVL "+statusDateNew+"',receivedAmount='"+receivedAmount+"',paymentStatus='"+PaymentStatus+"',statusDate='"+row[1]+"',vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' "+PaymentSent+" where id='"+row[0]+"' and distributionAmount <> 0   ");
				       }catch (Exception e) {
							e.printStackTrace();
						}
				       }
				}
			}
			return i;
		}

		public List checkInvoiceDateForWithReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode) {
			int i=0;
			double vanlineAmount=0.0;
	        String loginUser=getRequest().getRemoteUser();
			
			if(vanlineMinimumAmount!=null && !vanlineMinimumAmount.toString().equals("") ){
				vanlineAmount=vanlineMinimumAmount.doubleValue();
				
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				e.printStackTrace();
			}
		 	
		 	List accountLineList1=new ArrayList();
			if(automaticReconcile){
				accountLineList1= this.getSession().createSQLQuery("select concat(a.id,'~',a.serviceOrderId,'~',a.billToCode) from accountline as a, serviceorder as s,vanline as v  force index (agent)  , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and a.status = true and a.download = true  and v.statementCategory !='MCLM' and a.recGl is not null and a.recGl <>''   and a.distributionAmount <> 0 and (a.recInvoiceNumber='' or  a.recInvoiceNumber is null) and a.billToCode !='' and a.billToCode is not null and a.billToCode='"+uvlBillToCode+"' and  v.agent like '"
						+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Reconcile')  order by a.serviceOrderId,a.billToCode ").list();
				if((accountLineList1!=null) && (!accountLineList1.isEmpty())){}
			}
			return accountLineList1;
		}

		public int updateVanLineWithReconcile(Long id, String loginUser) {
			int i=0;
			i= this.getSession().createSQLQuery("update vanline force index (agent)   set updatedOn=now(), updatedBy='"+loginUser+"',  reconcileStatus ='Reconcile', accCreatedOn=now() where id='"+id+"'"  ).executeUpdate();;
			return i;
		}

		public void updateAccRecInvoiceNumberWithReconcile(Long id, String loginUser, String recAccDateToFormat, String recInvoiceNumber1) {
			
			getHibernateTemplate().bulkUpdate("update AccountLine set updatedon = now(), updatedBy='Vanline : "+loginUser+"',recPostDate = '" + recAccDateToFormat+ "',recInvoiceNumber='"+recInvoiceNumber1+"',receivedInvoiceDate=now(),invoiceCreatedBy='"+loginUser+"'  where id='"+id+"' ");
			
		}

		public int ownBillingVanLineListForReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode) {

		    double vanlineAmount=0.0; 
		        String loginUser=getRequest().getRemoteUser();
				
				if(vanlineMinimumAmount!=null && !vanlineMinimumAmount.toString().equals("") ){
					vanlineAmount=vanlineMinimumAmount.doubleValue();
					
				} 
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();
				try {
				 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
			        statusDateNew = nowYYYYMMDD.toString();
			        
			 	}catch (Exception e) {
					e.printStackTrace();
					logger.error("Error executing query"+ e,e);
				}
			 	int i=0;
			 	if(automaticReconcile){
			 		try{
			 		List accGLList = this.getSession().createSQLQuery("select a.id,a.chargecode,b.contract from accountline as a, serviceorder as s, vanline as v force index (agent) , companydivision  as c , billing as b  where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent  and  a.serviceOrderId = s.id and  b.id = s.id and b.corpId='"+sessionCorpID+"' and (a.recgl is null or a.recgl ='')  and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType   and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and v.statementCategory !='ARCV' and a.status = true and a.download = true and  v.agent like '"
							+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus not in('Approved','Dispute') group by a.shipNumber, if((a.glType = '' or a.glType is null), a.chargeCode, a.glType)").list();
			 		if(accGLList.size()>0)
					{
					Iterator it=accGLList.iterator();
					 while(it.hasNext())
				       {
				           Object []row= (Object [])it.next(); 
				            List glList =this.getSession().createSQLQuery("select concat((if(gl is null or gl = '','NO',gl)),'~',(if(expGl is null or expGl = '','NO',expGl))) from charges where charge='"+row[1]+"' and contract='"+row[2]+"' and corpID = '"+sessionCorpID+"' ").list(); 
				            if(glList!=null && (!(glList.isEmpty())) && glList.get(0)!=null){
        						try{
        						String[] GLarrayData=glList.get(0).toString().split("~"); 
        						String recGl=GLarrayData[0];
        						String payGl=GLarrayData[1];
        						if((recGl.equalsIgnoreCase("NO"))){
        							recGl="";
        						}
        						if((payGl.equalsIgnoreCase("NO"))){
        							payGl="";
        						}
        						if(!(recGl.equals(""))){
        							getHibernateTemplate().bulkUpdate("update AccountLine set recGl='"+recGl+"',payGl='"+payGl+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now()     where id='"+row[0]+"' ");
        						}
        					}catch(Exception e){
        						
        					}
        					}
				       }
					}
			 		}catch(Exception e){
			 			
			 		}
			 		
				 	List list = this.getSession().createSQLQuery("select v.glType, v.amtDueAgent - sum(a.distributionAmount),v.regNum,v.id,v.reconcileStatus from accountline as a, serviceorder as s,vanline as v force index (agent) , companydivision  as c  where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and  a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType   and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and v.statementCategory !='ARCV' and a.status = true and a.download = true and a.recGl is not null and a.recGl <>''  and  v.agent like '"
											+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus not in('Approved','Dispute') group by a.shipNumber, if((a.glType = '' or a.glType is null), a.chargeCode, a.glType)").list();
					
					 
					if(list.size()>0)
					{
					Iterator it=list.iterator();
					 while(it.hasNext())
				       {
				           Object []row= (Object [])it.next(); 
				           if(new BigDecimal(row[1].toString()).doubleValue()<=vanlineAmount && new BigDecimal(row[1].toString()).doubleValue()>= - vanlineAmount){
				        	   if(row[4]!=null && row[4].toString().equalsIgnoreCase("Reconcile")){
					        		 
					        	 }else{  
				             i= this.getSession().createSQLQuery("update vanline force index (agent)   set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Reconcile', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"'"  ).executeUpdate();;
				           }
				           }
				         else{
				        	 i= this.getSession().createSQLQuery("update vanline force index (agent)   set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Suspense', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"'"  ).executeUpdate();;
				         }
				       }
					}
					else{}
					/* String PaymentStatus="";
					 
					List accountLineList = this.getSession().createSQLQuery("select a.id,v.weekEnding from accountline as a, serviceorder as s,vanline as v  force index (agent)  , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> ''  and v.shipper is not null and v.regNum <> '' and v.regNum is not null and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and a.vanlineSettleDate is null  and  v.agent like '"
							+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Approved','Reconcile')").list();
					if(automaticReconcile){
						List accountLineList1 = this.getSession().createSQLQuery("select a.id,v.weekEnding,v.amtDueAgent,a.distributionAmount from accountline as a, serviceorder as s,vanline as v  force index (agent) , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType   and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and a.vanlineSettleDate is null  and  v.agent like '"
								+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Reconcile')").list();
						if((accountLineList1!=null) && (!accountLineList1.isEmpty())){
							Iterator accountLineIterator1=accountLineList1.iterator();
							while(accountLineIterator1.hasNext())
						       {
								try{
								 Object []row= (Object [])accountLineIterator1.next(); 
								 try {
									 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MM/yy");
								        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(row[1]) );
								        statusDateNew = nowYYYYMMDD.toString();
								        
								 	}catch (Exception e) {
										e.printStackTrace();
									}
								 	String PaymentSent="";
								 	String receivedAmount="0";
								 	if(new BigDecimal(row[2].toString()).compareTo(new BigDecimal(row[3].toString()))==0){
								 		
								 		PaymentStatus="Fully Paid"; 
								 		PaymentSent=" ,paymentSent=null";
								 		receivedAmount=row[2].toString();
								 	}else{
								 		if((Double.parseDouble(row[3].toString()))>(Double.parseDouble(row[2].toString()))){
								 			PaymentStatus="Fully Paid"; 
									 		PaymentSent=" ,paymentSent=null";
									 		receivedAmount=row[3].toString();	
								 		}else{ 
								 			PaymentStatus="Fully Paid"; 
									 		PaymentSent=" ,paymentSent=null";
									 		receivedAmount=row[3].toString();	
								 		}
								 	}
								 getHibernateTemplate().bulkUpdate("update AccountLine set externalReference='UVL "+statusDateNew+"',receivedAmount='"+receivedAmount+"',paymentStatus='"+PaymentStatus+"',statusDate='"+row[1]+"',vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' "+PaymentSent+" where id='"+row[0]+"' and distributionAmount <> 0 ");
						       }catch (Exception e) {
									e.printStackTrace();
									logger.error("Error executing query"+ e,e);
								}
						       }
						}
					}
					
					if((accountLineList!=null) && (!accountLineList.isEmpty())){
						Iterator accountLineIterator=accountLineList.iterator();
						while(accountLineIterator.hasNext())
					       {
							 Object []row= (Object [])accountLineIterator.next();
							 getHibernateTemplate().bulkUpdate("update AccountLine set vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' where id='"+row[0]+"' and distributionAmount <> 0 ");
					       }	
					}*/
				 		
			 	}
				return i;
			
		}

		public int updateAccountLineWithOwnBillingReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode) {

		    double vanlineAmount=0.0; 
		        String loginUser=getRequest().getRemoteUser();
				
				if(vanlineMinimumAmount!=null && !vanlineMinimumAmount.toString().equals("") ){
					vanlineAmount=vanlineMinimumAmount.doubleValue();
					
				} 
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();
				try {
				 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
			        statusDateNew = nowYYYYMMDD.toString();
			        
			 	}catch (Exception e) {
					e.printStackTrace();
					logger.error("Error executing query"+ e,e);
				}
			 	int i=0;
			 	if(automaticReconcile){  
				 	List list = this.getSession().createSQLQuery("select v.glType, v.amtDueAgent - sum(a.distributionAmount),v.regNum,v.id,v.reconcileStatus from accountline as a, serviceorder as s,vanline as v force index (agent) , companydivision  as c  where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent  and  a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType   and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and v.statementCategory !='ARCV' and a.status = true and a.download = true and a.recGl is not null and a.recGl <>'' and a.recInvoiceNumber<>'' and a.recInvoiceNumber is not null  and  v.agent like '"
											+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus not in('Approved','Dispute') group by a.shipNumber, if((a.glType = '' or a.glType is null), a.chargeCode, a.glType)").list();
					
					 
					if(list.size()>0)
					{
					Iterator it=list.iterator();
					 while(it.hasNext())
				       {
				           Object []row= (Object [])it.next(); 
				           if(new BigDecimal(row[1].toString()).doubleValue()<=vanlineAmount && new BigDecimal(row[1].toString()).doubleValue()>= - vanlineAmount){
				        	   if(row[4]!=null && row[4].toString().equalsIgnoreCase("Reconcile")){
					        		 
					        	 }else{ 
				        	   i= this.getSession().createSQLQuery("update vanline force index (agent)   set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Reconcile', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"'"  ).executeUpdate();;
				           }
				           }
				         else{
				        	 i= this.getSession().createSQLQuery("update vanline force index (agent)   set updatedOn=now(), updatedBy='"+user.getUsername()+"',  reconcileStatus ='Suspense', reconcileAmount='"+row[1]+"' where id='"+row[3]+"'and regNum='"+row[2]+"'"  ).executeUpdate();;
				         }
				       }
					}
					else{}
					 String PaymentStatus="";
					 
					List accountLineList = this.getSession().createSQLQuery("select a.id,v.weekEnding from accountline as a, serviceorder as s,vanline as v  force index (agent)  , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent  and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> ''  and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and v.statementCategory !='ARCV' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''  and a.recInvoiceNumber<>'' and a.recInvoiceNumber is not null   and a.vanlineSettleDate is null  and  v.agent like '"
							+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Approved','Reconcile')").list();
					if(automaticReconcile){
						List accountLineList1 = this.getSession().createSQLQuery("select a.id,v.weekEnding,v.amtDueAgent,a.distributionAmount from accountline as a, serviceorder as s,vanline as v  force index (agent) , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent  and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType   and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and v.statementCategory !='ARCV' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>'' and a.recInvoiceNumber<>'' and a.recInvoiceNumber is not null   and a.vanlineSettleDate is null  and  v.agent like '"
								+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Reconcile')").list();
						if((accountLineList1!=null) && (!accountLineList1.isEmpty())){
							Iterator accountLineIterator1=accountLineList1.iterator();
							while(accountLineIterator1.hasNext())
						       {
								try{
								 Object []row= (Object [])accountLineIterator1.next(); 
								 try {
									 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MM/yy");
								        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(row[1]) );
								        statusDateNew = nowYYYYMMDD.toString();
								        
								 	}catch (Exception e) {
										e.printStackTrace();
									}
								 	String PaymentSent="";
								 	String receivedAmount="0";
								 	if(new BigDecimal(row[2].toString()).compareTo(new BigDecimal(row[3].toString()))==0){
								 		
								 		PaymentStatus="Fully Paid"; 
								 		PaymentSent=" ,paymentSent=null";
								 		receivedAmount=row[2].toString();
								 	}else{
								 		if((Double.parseDouble(row[3].toString()))>(Double.parseDouble(row[2].toString()))){
								 			PaymentStatus="Fully Paid"; 
									 		PaymentSent=" ,paymentSent=null";
									 		receivedAmount=row[3].toString();	
								 		}else{ 
								 			PaymentStatus="Fully Paid"; 
									 		PaymentSent=" ,paymentSent=null";
									 		receivedAmount=row[3].toString();	
								 		}
								 	}
								 getHibernateTemplate().bulkUpdate("update AccountLine set externalReference='UVL "+statusDateNew+"',receivedAmount='"+receivedAmount+"',paymentStatus='"+PaymentStatus+"',statusDate='"+row[1]+"',vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' "+PaymentSent+" where id='"+row[0]+"' and distributionAmount <> 0 ");
						       }catch (Exception e) {
									e.printStackTrace();
									logger.error("Error executing query"+ e,e);
								}
						       }
						}
					}
					
					if((accountLineList!=null) && (!accountLineList.isEmpty())){
						Iterator accountLineIterator=accountLineList.iterator();
						while(accountLineIterator.hasNext())
					       {
							 Object []row= (Object [])accountLineIterator.next();
							 getHibernateTemplate().bulkUpdate("update AccountLine set vanlineSettleDate='"+row[1]+"',updatedOn=now(),updatedBy='Vanline : "+loginUser+"' where id='"+row[0]+"' and distributionAmount <> 0 ");
					       }	
					}
				 		
			 	}
				return i;
			
		}

		public List checkInvoiceOwnBillingDateForWithReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode) {
			int i=0;
			double vanlineAmount=0.0;
	        String loginUser=getRequest().getRemoteUser();
			
			if(vanlineMinimumAmount!=null && !vanlineMinimumAmount.toString().equals("") ){
				vanlineAmount=vanlineMinimumAmount.doubleValue();
				
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				e.printStackTrace();
			}
		 	
		 	List accountLineList1=new ArrayList();
			if(automaticReconcile){
				accountLineList1= this.getSession().createSQLQuery("select concat(a.id,'~',a.serviceOrderId,'~',a.billToCode,'~',a.distributionAmount,'~',a.actualRevenue,'~',a.chargeCode,'~',v.id) from accountline as a, serviceorder as s,vanline as v  force index (agent)  , companydivision  as c where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and c.vanLineCode= v.accountingAgent  and a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' and v.regNum is not null and v.statementCategory !='MCLM' and v.statementCategory !='ARCV' and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>''   and a.distributionAmount <> 0 and a.distributionAmount <>'' and a.distributionAmount is not null and a.chargeCode <>'' and a.chargeCode is not null and (a.recInvoiceNumber='' or  a.recInvoiceNumber is null) and a.billToCode !='' and a.billToCode is not null and  v.agent like '"
						+ agent + "' and v.weekEnding like '" + statusDateNew + "%' and v.reconcileStatus in('Reconcile')  order by a.serviceOrderId,a.billToCode ").list();
				if((accountLineList1!=null) && (!accountLineList1.isEmpty())){}
			}
			return accountLineList1;
		}

		public void updateAccRecInvoiceNumberWithOwnBillingReconcile( String idList, String loginUser, String recAccDateTo, String recInvoiceNumber1, String creditInvoiceNumber, Long soId, String sessionCorpID) {
          //String creditInvoiceNumber1="";
          if(creditInvoiceNumber.trim().equals("NA")){
        	   
        	  int i =getHibernateTemplate().bulkUpdate("update AccountLine set updatedon = now(), updatedBy='Vanline : "+loginUser+"', recPostDate = '" + recAccDateTo+ "', recInvoiceNumber='"+recInvoiceNumber1+"', receivedInvoiceDate=now(), invoiceCreatedBy='"+loginUser+"'   where serviceOrderId = '"+soId+"' and corpID='"+sessionCorpID+"' and  id in ("+idList+") ");
          }else{
        	  
        	int i =  getHibernateTemplate().bulkUpdate("update AccountLine set updatedon = now(), updatedBy='Vanline : "+loginUser+"', recPostDate = '" + recAccDateTo+ "', recInvoiceNumber='"+recInvoiceNumber1+"', receivedInvoiceDate=now(), invoiceCreatedBy='"+loginUser+"', creditInvoiceNumber='"+creditInvoiceNumber+"'  where serviceOrderId = '"+soId+"' and corpID='"+sessionCorpID+"' and  id in ("+idList+") ");
          }
		}
		public Map<String, String> getChargeCodeList(String billingContract,String corpId){
			List<Charges> chargesList=getHibernateTemplate().find("from Charges where contract='"+billingContract.replaceAll("'", "''")+"' and corpID='"+corpId+"'");
			Map<String, String> chargeCodeList=new TreeMap<String, String>();
			for (Charges charges : chargesList) {
				chargeCodeList.put(charges.getCharge(), charges.getCharge()+ " : " +charges.getDescription());
			}
			return chargeCodeList;
		}

		public String findCompanyDivision(String agent, String sessionCorpID) {
			String accountingcode="";
		    List accountingcodeList= new ArrayList();
		    accountingcodeList= getHibernateTemplate().find("select companyCode  from CompanyDivision where vanLineCode='" + agent + "' and corpID='" + sessionCorpID + "' ");
	       if(accountingcodeList !=null && (!(accountingcodeList.isEmpty())) && accountingcodeList.get(0)!=null){
	    	 accountingcode=accountingcodeList.get(0).toString();
	        }
			return accountingcode;
		}

		public List findVanLineAgent(String agent, String sessionCorpID) {
			return this.getSession().createSQLQuery("select concat(p.partnerCode, '~', p.lastName) FROM partnerpublic p LEFT OUTER JOIN partnervanlineref pv on  p.partnerCode = pv.partnerCode WHERE pv.vanLineCode = '"+ agent +"' AND p.corpID IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list();
			 
		}

		public List getOwnBillingRegNumVanline(String agent, Date weekEnding) {
			 
			 try {
				 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
			        statusDateNew = nowYYYYMMDD.toString();
			        
			 	}catch (Exception e) {
					e.printStackTrace();
				}
			//List vanlineregNum=this.getSession().createSQLQuery("select distinct v.regNum from vanline as v where v.regNum <> '' and v.regNum is not null and  v.agent like '"+agent+ "' and v.weekEnding like '" +statusDateNew+ "%' and v.reconcileStatus='Suspense'").list();
				//List vanlineregNum=this.getSession().createSQLQuery("select  regNum from vanline where regNum <> '' and regNum is not null and glType <> '' and glType is not null and glCode<>'' and glType is not null and  v.agent like '"+agent+ "' and weekEnding like '2012-06-14%' and reconcileStatus='Suspense'").list();
				
			 	 return getHibernateTemplate().find("select id from VanLine where regNum <> '' and regNum is not null and glType <> '' and glType is not null  and  agent like '"+agent+ "' and weekEnding like '" +statusDateNew+ "%' and statementCategory !='MCLM'  and statementCategory !='ARCV' and reconcileStatus='Suspense' order by  regNum ");
		    }

		public List getDSTROwnBillingRegNumVanline(String agent, Date weekEnding,String sessionCorpID) {
			 
			 try {
				 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
			        statusDateNew = nowYYYYMMDD.toString();
			        
			 	}catch (Exception e) {
					e.printStackTrace();
				}
			//List vanlineregNum=this.getSession().createSQLQuery("select distinct v.regNum from vanline as v where v.regNum <> '' and v.regNum is not null and  v.agent like '"+agent+ "' and v.weekEnding like '" +statusDateNew+ "%' and v.reconcileStatus='Suspense'").list();
				//List vanlineregNum=this.getSession().createSQLQuery("select  regNum from vanline where regNum <> '' and regNum is not null and glType <> '' and glType is not null and glCode<>'' and glType is not null and  v.agent like '"+agent+ "' and weekEnding like '2012-06-14%' and reconcileStatus='Suspense'").list();
			
			 return this.getSession().createSQLQuery("select distinct regNum from vanline force index (agent) where regNum <> '' and regNum is not null and  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '" +statusDateNew+ "%' and statementCategory ='DSTR' and reconcileStatus='Reconcile' and corpID='"+sessionCorpID+"' order by regNum ").list();
		 }

		public List getOwnBillingRegNumVanlineAmount(String agent, Date weekEnding, String regnum,String sessionCorpID) {
	    	List estimateRevSum=new ArrayList();
	    	 try {
				 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekEnding) );
			        statusDateNew = nowYYYYMMDD.toString();
			        
			 	}catch (Exception e) {
					e.printStackTrace();
				}
	    	estimateRevSum= this.getSession().createSQLQuery("select sum(amtDueAgent) from vanline force index (agent) where regNum <> '' and regNum is not null and regNum='"+regnum+"' and  agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and weekEnding like '" +statusDateNew+ "%' and corpID='"+sessionCorpID+"' ").list();
	    	if(estimateRevSum.get(0)==null)
	        {  
	    		
		       List estimateRevSum1=new ArrayList();
		       estimateRevSum1.add("0.00");
		       return  estimateRevSum1;
	        }
	       else
	       {
		   return estimateRevSum;
	       }
	    }

		public List findByOwnerAndContact(String driverID, String sessionCorpID) {
			return getHibernateTemplate().find("select concat(firstName,'#',lastName,'#',billingPhone) from Partner where isOwnerOp is true and status='Approved' and partnerCode='"+driverID+"'");
		}
		
}
