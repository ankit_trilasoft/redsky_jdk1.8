package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CustomerRelationDao;
import com.trilasoft.app.model.CustomerRelation;
import com.trilasoft.app.service.CustomerRelationManager;

public class CustomerRelationManagerImpl extends GenericManagerImpl<CustomerRelation, Long> implements CustomerRelationManager {
	CustomerRelationDao customerRelationDao;
	public CustomerRelationManagerImpl(CustomerRelationDao customerRelationDao) {
		super(customerRelationDao); 
        this.customerRelationDao = customerRelationDao; 
	}
	
	public CustomerRelation getCrmDetails(Long cid,String sessionCorpID){
		return customerRelationDao.getCrmDetails(cid,sessionCorpID);
	}
	
}
