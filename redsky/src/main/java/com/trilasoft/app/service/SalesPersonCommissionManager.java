package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.SalesPersonCommission;

public interface SalesPersonCommissionManager extends GenericManager<SalesPersonCommission, Long>{
	public List getCommPersentAcRange(String corpId,String contractName,String chargeCode);
	public Map<Double,BigDecimal > getCommPersentAcMap(String corpId,String contractName,String chargeCode,String companyDiv);
}
