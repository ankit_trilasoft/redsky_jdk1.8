package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.*;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.BaseObject;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.SystemConfiguration;
import com.trilasoft.app.service.DataCatalogManager;
import com.trilasoft.app.service.SystemConfigurationManager;

public class SystemConfigurationAction extends BaseAction{
	private String sessionCorpID;
    private Long systemId;
    private Long id;
    private SystemConfigurationManager systemConfigurationManager;
    private List systemList;
    private List fieldList;
    private SystemConfiguration systemConfiguration;
	private  static Date createdon;
	private DataCatalogManager dataCatalogManager; 
	private DataCatalog dataCatalog;
	private boolean flag;
	private String tableNames;
	private List tableList;
	
	public SystemConfigurationAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	public String list()
	{
		systemList=systemConfigurationManager.getMasterList(sessionCorpID);
		//System.out.println("\n\nsystemList-->"+systemList);
		return SUCCESS;
	}
	public String edit()
	{
		//systemConfigurable();
		if (id != null) { 
			dataCatalog = dataCatalogManager.get(id); 
			createdon=dataCatalog.getCreatedOn();
			dataCatalog.setCreatedOn(createdon);
			//dataCatalog.setUpdatedOn(new Date());
			//fieldList=new ArrayList();
			//fieldList.add(systemConfiguration.getFieldName());
			
	
	
		} else { 
			dataCatalog = new DataCatalog();
			dataCatalog.setCreatedOn(new Date());
			dataCatalog.setUpdatedOn(new Date());
			
		}
		//dataCatalog.setCorpID(sessionCorpID);
		
		return SUCCESS;
	}
	public String delete() { 
    	
		     systemConfigurationManager.remove(systemConfiguration.getId()); 
 	         systemConfiguration.setCreatedOn(createdon);
		     systemConfiguration.setUpdatedOn(new Date());
		     saveMessage(getText("systemConfiguration.deleted"));
		     list();
    
 	return "delete";
      
 } 
	public String save()
	{
		if (cancel != null) {
    		
            return "cancel"; 
        } 
    	
        if (delete != null) { 
        	
            return delete(); 
        } 
        boolean isNew = (systemConfiguration.getId() == null); 
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){   
        String key = (isNew) ? "System Configuration added" : "System Configuration updated"; 
        saveMessage(getText(key)); 
        }
        systemConfiguration.setCorpId(sessionCorpID);
        
        systemConfigurable();
        if (!isNew) { 
        	
        	systemConfiguration.setCreatedOn(createdon);
        	systemConfiguration.setUpdatedOn(new Date());
        	fieldList=new ArrayList();
			fieldList.add(systemConfiguration.getFieldName());
			systemConfiguration=systemConfigurationManager.save(systemConfiguration);
            return INPUT; 
        } else {
        	
        	systemConfiguration.setCreatedOn(new Date());
        	systemConfiguration.setUpdatedOn(new Date());
        	fieldList=new ArrayList();
			fieldList.add(systemConfiguration.getFieldName());
			systemConfiguration=systemConfigurationManager.save(systemConfiguration);
		    return SUCCESS;
        }
	}
	private String systemConfigurationtableName;
	private String systemConfigurationfieldName;
	private String description;
	private String auditable;
	private String defineByToDoRule;
	private String isdateField;
	private String visible;
	private String charge;
	public String search()
	{
		//System.out.println("\n\ncharge-->>"+charge);
		
		systemList=systemConfigurationManager.searchByTableAndField(systemConfigurationtableName.trim(), systemConfigurationfieldName.trim(),sessionCorpID,description.trim(),auditable,defineByToDoRule,isdateField,visible,charge);
		return SUCCESS;
	}
	private String rulesfield;
	private Long maxId;
	public String saveSystemConfigurationMainForm()
	{
		if(dataCatalog.getCorpID().equalsIgnoreCase("TSFT")){
			List isexisted=dataCatalogManager.checkData(dataCatalog.getTableName(),dataCatalog.getFieldName(),sessionCorpID);
			if(isexisted==null || (isexisted.isEmpty())){
			dataCatalog.setId(null);
			dataCatalog.setCreatedOn(new Date());
			rulesfield=dataCatalog.getTableName().toLowerCase() +"."+ dataCatalog.getFieldName();
			dataCatalog.setRulesFiledName(rulesfield);
			dataCatalog.setCorpID(sessionCorpID);
			dataCatalog.setUpdatedOn(new Date());			
			dataCatalogManager.save(dataCatalog);	
			}else if(isexisted!=null && !(isexisted.isEmpty())){
				dataCatalogManager.mergeDataCatalog(dataCatalog);
				//errorMessage("Same combination "+dataCatalog.getTableName()+" and "+ dataCatalog.getFieldName()+" already exists, Please select another combination");
				
			}
		}else{
			dataCatalog.setUpdatedOn(new Date());
			dataCatalog.setUpdatedBy(getRequest().getRemoteUser());
			dataCatalogManager.save(dataCatalog);	
		}
		boolean isNew = (dataCatalog.getId() == null); 
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "System Configuration has been added successfully" : "System Configuration has been updated successfully";
		saveMessage(getText(key));
		}
		if (!isNew) {
			//dataCatalog.setCreatedBy(getRequest().getRemoteUser());
			//dataCatalog.setUpdatedOn(new Date());
			//dataCatalog.setUpdatedBy(getRequest().getRemoteUser());
        	//return SUCCESS;   
        } else { 
        	//dataCatalog.setCreatedOn(new Date());
        	maxId = Long.parseLong(dataCatalogManager.findMaximumId().get(0).toString());
        	dataCatalog = dataCatalogManager.get(maxId);
        	dataCatalog.setUpdatedOn(new Date());
			dataCatalog.setUpdatedBy(getRequest().getRemoteUser());
        	dataCatalogManager.save(dataCatalog);
        } 
		 return SUCCESS;
	}
	
    
	private String gotoPageString;
	private String validateFormNav;
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
    	String s = saveSystemConfigurationMainForm();
    	return gotoPageString;
    }
	
	
	public String systemConfigurableField()
	{

		//System.out.println("\n\ntableName-->>"+tableNames);
		fieldList=systemConfigurationManager.configurableField(tableNames,"true");
		//System.out.println("\n\nfieldList-->>"+fieldList);
				
		return SUCCESS;
	}
	public String systemConfigurable()
	{
		systemConfigurableField();
		tableList=systemConfigurationManager.configurableTable("true");
		return SUCCESS;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getTableNames() {
		return tableNames;
	}

	public void setTableNames(String tableNames) {
		this.tableNames = tableNames;
	}
	public Long getSystemId() {
		return systemId;
	}
	public void setSystemId(Long systemId) {
		this.systemId = systemId;
	}
	public void setSystemConfigurationManager(
			SystemConfigurationManager systemConfigurationManager) {
		this.systemConfigurationManager = systemConfigurationManager;
	}
	public List getSystemList() {
		return systemList;
	}
	public void setSystemList(List systemList) {
		this.systemList = systemList;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public SystemConfiguration getSystemConfiguration() {
		return systemConfiguration;
	}
	public void setSystemConfiguration(SystemConfiguration systemConfiguration) {
		this.systemConfiguration = systemConfiguration;
	}
	public static Date getCreatedon() {
		return createdon;
	}
	public static void setCreatedon(Date createdon) {
		SystemConfigurationAction.createdon = createdon;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public List getTableList() {
		return tableList;
	}
	public void setTableList(List tableList) {
		this.tableList = tableList;
	}
	public void setDataCatalogManager(DataCatalogManager dataCatalogManager) {
		this.dataCatalogManager = dataCatalogManager;
	}
	public List getFieldList() {
		return fieldList;
	}
	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}
	public String getSystemConfigurationfieldName() {
		return systemConfigurationfieldName;
	}
	public void setSystemConfigurationfieldName(String systemConfigurationfieldName) {
		this.systemConfigurationfieldName = systemConfigurationfieldName;
	}
	public String getSystemConfigurationtableName() {
		return systemConfigurationtableName;
	}
	public void setSystemConfigurationtableName(String systemConfigurationtableName) {
		this.systemConfigurationtableName = systemConfigurationtableName;
	}
	public String getAuditable() {
		return auditable;
	}
	public void setAuditable(String auditable) {
		this.auditable = auditable;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getDefineByToDoRule() {
		return defineByToDoRule;
	}
	public void setDefineByToDoRule(String defineByToDoRule) {
		this.defineByToDoRule = defineByToDoRule;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIsdateField() {
		return isdateField;
	}
	public void setIsdateField(String isdateField) {
		this.isdateField = isdateField;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public DataCatalog getDataCatalog() {
		return dataCatalog;
	}
	public void setDataCatalog(DataCatalog dataCatalog) {
		this.dataCatalog = dataCatalog;
	}
	public String getRulesfield() {
		return rulesfield;
	}
	public void setRulesfield(String rulesfield) {
		this.rulesfield = rulesfield;
	}
	public String getGotoPageString() {
		return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}
	public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
}