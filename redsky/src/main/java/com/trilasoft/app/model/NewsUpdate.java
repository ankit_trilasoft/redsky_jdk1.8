package com.trilasoft.app.model;

import java.util.Date;

import org.appfuse.model.BaseObject;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;



/*
Generate your getters and setters using your favorite IDE: 
In Eclipse:
Right-click -> Source -> Generate Getters and Setters
*/
@Entity
@Table(name="newsupdate")
public class NewsUpdate extends BaseObject{
    /**
     * 
    */
	//private static final long serialVersionUID = 1L;
	private Long id;
    private String functionality;
    private String details;
    private String corpId;
    private Date publishedDate;
    private Date createdOn;
    private Date modifyOn;
    private String createdBy;
    private String modifyBy;
    private Date effectiveDate;
    private Date endDisplayDate;
    private String module;
    private String retention;
    private String location;
    private String fileName;
    private Boolean visible = false;
    private String ticketNo;
    private Boolean status = false;
    private String category;
    
	
    @Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("functionality", functionality)
				.append("details", details).append("corpId", corpId)
				.append("publishedDate", publishedDate)
				.append("createdOn", createdOn).append("modifyOn", modifyOn)
				.append("createdBy", createdBy).append("modifyBy", modifyBy)
				.append("effectiveDate", effectiveDate)
				.append("endDisplayDate", endDisplayDate)
				.append("module", module).append("retention", retention)
				.append("location", location).append("visible", visible)
				.append("fileName",fileName).append("ticketNo",ticketNo)
				.append("status",status).append("category",category)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof NewsUpdate))
			return false;
		NewsUpdate castOther = (NewsUpdate) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(functionality, castOther.functionality)
				.append(details, castOther.details)
				.append(corpId, castOther.corpId)
				.append(publishedDate, castOther.publishedDate)
				.append(createdOn, castOther.createdOn)
				.append(modifyOn, castOther.modifyOn)
				.append(createdBy, castOther.createdBy)
				.append(modifyBy, castOther.modifyBy)
				.append(effectiveDate, castOther.effectiveDate)
				.append(endDisplayDate, castOther.endDisplayDate)
				.append(module, castOther.module)
				.append(retention, castOther.retention)
				.append(location, castOther.location)
				.append(visible, castOther.visible)
				.append(fileName,castOther.fileName)
				.append(ticketNo,castOther.ticketNo)
				.append(status,castOther.status)
				.append(category,castOther.category)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(functionality)
				.append(details).append(corpId).append(publishedDate)
				.append(createdOn).append(modifyOn).append(createdBy)
				.append(modifyBy).append(effectiveDate).append(endDisplayDate)
				.append(module).append(retention).append(location)
				.append(visible).append(ticketNo).append(status).append(category).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=4000)
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	@Column(length=200)
	public String getFunctionality() {
		return functionality;
	}
	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}
	@Column(length=82)
	public String getModifyBy() {
		return modifyBy;
	}
	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}
	@Column
	public Date getModifyOn() {
		return modifyOn;
	}
	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}
	@Column
	public Date getPublishedDate() {
		return publishedDate;
	}
	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}
	@Column
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	@Column
	public Date getEndDisplayDate() {
		return endDisplayDate;
	}
	public void setEndDisplayDate(Date endDisplayDate) {
		this.endDisplayDate = endDisplayDate;
	}
	@Column
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	@Column
	public String getRetention() {
		return retention;
	}
	public void setRetention(String retention) {
		this.retention = retention;
	}
	@Column
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Column
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	@Column
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Column
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	@Column
	public Boolean getStatus() {
		return status;
	}
	public String getCategory() {
		return category;
	}
	@Column
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
}