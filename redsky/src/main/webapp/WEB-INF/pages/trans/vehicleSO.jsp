<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>SO Vehicle List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>



<display:table name="vehicleSO" class="table" requestURI="" id="vehicleList" export="false" defaultsort="1" pagesize="100" style="width:99%; margin-left:5px;margin-top:2px;">   
     <display:column title="Piece#"><a href="#" onclick="goToUrlChild(${vehicleList.id});" ><c:out value="${vehicleList.idNumber}" /></a></display:column>
    <display:column property="year" headerClass="containeralign" style="text-align: right;" titleKey="vehicle.year"></display:column>
    <display:column property="make"  titleKey="vehicle.make"></display:column>
    <display:column property="model"  titleKey="vehicle.model"></display:column>
    <display:column  headerClass="containeralign" style="text-align: right;" titleKey="vehicle.volume"></display:column>
</display:table>  
