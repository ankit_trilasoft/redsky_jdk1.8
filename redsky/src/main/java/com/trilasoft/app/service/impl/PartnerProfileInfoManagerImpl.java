package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerProfileInfoDao;
import com.trilasoft.app.model.PartnerProfileInfo;
import com.trilasoft.app.service.PartnerProfileInfoManager;

public class PartnerProfileInfoManagerImpl extends GenericManagerImpl<PartnerProfileInfo, Long> implements PartnerProfileInfoManager {   
	PartnerProfileInfoDao partnerProfileInfoDao;   
	  
    public PartnerProfileInfoManagerImpl(PartnerProfileInfoDao partnerProfileInfoDao) {   
        super(partnerProfileInfoDao);   
        this.partnerProfileInfoDao = partnerProfileInfoDao;   
    }
    
    public List getProfileInfoListByPartnerCode(String sessionCorpID, String partnerCode) {
		return partnerProfileInfoDao.getProfileInfoListByPartnerCode(sessionCorpID, partnerCode);
	}

}
