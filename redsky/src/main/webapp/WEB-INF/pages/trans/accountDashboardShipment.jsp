<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title>Account Dashboard Embassy of Australia</title> 
<meta name="heading" content="Account Dashboard Embassy of Australia"/>
<style>
body {padding:0px !important;}
div#inner-content {
margin-left:5px;
}
div#main {width:99% !important;}
.export{padding: 4px; width: 100px; background: none repeat scroll 0px 0px #F4F4F4; border: 1px solid rgb(227, 227, 227);}
</style>
<script language="javascript" type="text/javascript">
function validateSoNumber(){
	var url='viewReportWithParam.html?id=2016&reportName=Shipment Analysis By SO&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=355320&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['accountDashboardEOA'].action =url;
	document.forms['accountDashboardEOA'].submit();
	return true;
}

function validateSoDPort(){
	var url='viewReportWithParam.html?id=2017&reportName=Shipment Analysis By DPort&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=355320&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['accountDashboardEOA'].action =url;
	document.forms['accountDashboardEOA'].submit();
	return true;
}

function validateSoWeight(){
	var url='viewReportWithParam.html?id=2015&reportName=Shipment Analysis By Weight&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=355320&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['accountDashboardEOA'].action =url;
	document.forms['accountDashboardEOA'].submit();
	return true;
}
</script>

</head> 
<s:form id="accountDashboardEOA" name="accountDashboardEOA" action="viewReportWithParam" method="post" validate="true">   
 <div id="Layer1" align="center" style="width:100%; margin:0px; padding: 0px;  ">
 <div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:10px;"><span></span></div>
<div class="center-content" style="padding-right:0px;padding-left:0px;">
<table cellspacing="2" cellpadding="2" border="0" width="95%" style="margin:0px auto;padding:0px;">
<tbody>	
	<tr>
     <td align="center" style="margin: 0px; padding: 0px">
     <iframe  style="margin: 0px; padding: 0px;" src ="${pageContext.request.contextPath}/images/ShipmentAnalysisSO.jpg"  WIDTH=940 HEIGHT=493 FRAMEBORDER=0 scrolling="no">     
	 </iframe>
	 </td>
	 <!--<td align="center" style="margin: 0px; padding: 0px">
     <iframe  style="margin: 0px; padding: 0px;" src ="${pageContext.request.contextPath}/images/RevenueByModeEOA.jpg"  WIDTH=485 HEIGHT=330 FRAMEBORDER=0 scrolling="no">    
	 </iframe>
	 </td>
	 --></tr>
	 <tr>
	 <td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateSoNumber();"/><a href="javascript:void(0)" onClick="return validateSoNumber();"> XLS </a></div></td>
  	 <!--<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateMode();"/><a href="javascript:void(0)" onClick="return validateMode();"> XLS </a></div></td>-->
  	 </tr>
	 
     <tr>
     <td align="center" style="margin: 0px; padding: 0px">
     <iframe  style="margin: 0px; padding: 0px; " src ="${pageContext.request.contextPath}/images/ShipmentAnalysisDPort.jpg"  WIDTH=940 HEIGHT=493 FRAMEBORDER=0 scrolling="no"></iframe>
	</td>
	<!--<td align="center" style="margin: 0px; padding: 0px">
     <iframe  style="margin: 0px; padding: 0px; " src ="${pageContext.request.contextPath}/images/RevenueByRoutingEOA.jpg"  WIDTH=485 HEIGHT=330 FRAMEBORDER=0 scrolling="no"></iframe>
	</td>
	--></tr>
	<tr>
	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateSoDPort();"/><a href="javascript:void(0)" onClick="return validateSoDPort();"> XLS </a></div></td>
  	<!--<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateRouting();"/><a href="javascript:void(0)" onClick="return validateRouting();"> XLS </a></div></td>-->
  	</tr>
	
	<tr>
    <td align="center" style="margin: 0px; padding: 0px">
    <iframe  style="margin: 0px; padding: 0px;" src ="${pageContext.request.contextPath}/images/ShipmentAnalysisWeight.jpg"  WIDTH=940 HEIGHT=493 FRAMEBORDER=0 scrolling="no"></iframe>
	</td>
	<!--<td align="center" style="margin: 0px; padding: 0px">
     <iframe  style="margin: 0px; padding: 0px;" src ="${pageContext.request.contextPath}/images/RevenueByDCEOA.jpg"  WIDTH=485 HEIGHT=330 FRAMEBORDER=0 scrolling="no"></iframe>
	</td>
	--></tr>
	
  <tr>
	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateSoWeight();"/><a href="javascript:void(0)" onClick="return validateSoWeight();"> XLS </a></div></td>
  	<!--<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateDC();"/><a href="javascript:void(0)" onClick="return validateDC();"> XLS </a></div></td>-->
  </tr>
  
  
 <tr>
 </tr>
 <tr>
 </tr>

</tbody></table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div> 
</div>
</div>
</div>      
</s:form>