package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name = "userguides")
public class UserGuides extends BaseObject {
	
	private Long id;
	
	private String corpId;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;
	
	private String module;
	
	private String documentName;
	
	private String corpidChecks;
	
	private String location;
	
	private Boolean visible;
	
	private String fileName;
	
	private Integer displayOrder;
	
	private Boolean videoFlag;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id",id)
				.append("corpId",corpId)
				.append("updatedBy",updatedBy)
				.append("createdBy",createdBy)
				.append("createdOn",createdOn)
				.append("updatedOn",updatedOn)
				.append("module",module)
				.append("documentName",documentName)
				.append("corpidChecks",corpidChecks)
				.append("location",location)
				.append("visible",visible)
				.append("fileName",fileName)
				.append("displayOrder",displayOrder)
				.append("videoFlag",videoFlag)
				.toString();
	}

	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof UserGuides))
			return false;
		UserGuides castOther = (UserGuides) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(module, castOther.module)
				.append(documentName, castOther.documentName)
				.append(corpidChecks, castOther.corpidChecks)
				.append(location, castOther.location)
				.append(visible, castOther.visible)
				.append(fileName, castOther.fileName)
				.append(displayOrder,castOther.displayOrder)
				.append(videoFlag,castOther.videoFlag)
				.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(updatedBy).append(createdBy).append(createdOn).append(updatedOn).append(module).append(documentName)
				.append(corpidChecks).append(location).append(visible).append(fileName).append(displayOrder).append(videoFlag)
				.toHashCode();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	@Column
	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	@Column
	public String getCorpidChecks() {
		return corpidChecks;
	}

	public void setCorpidChecks(String corpidChecks) {
		this.corpidChecks = corpidChecks;
	}

	@Column
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column
	public Integer getDisplayOrder() {
		return displayOrder;
	}


	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	@Column
	public Boolean getVisible() {
		return visible;
	}


	public void setVisible(Boolean visible) {
		this.visible = visible;
	}


	public Boolean getVideoFlag() {
		return videoFlag;
	}


	public void setVideoFlag(Boolean videoFlag) {
		this.videoFlag = videoFlag;
	}

}
