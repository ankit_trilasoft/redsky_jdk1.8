package com.trilasoft.app.service;
import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsTenancyManagement;


public interface DsTenancyManagementManager extends GenericManager<DsTenancyManagement, Long>{
	List getListById(Long id);
}

