<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="myFileList.title"/></title>
<script language="javascript">
	
	function updateNotes(form){
		var chckedVal="";
		var chkObj = document.forms['myFileForm'].elements['myFlieId'];
		if(chkObj.length>1){
		for(var i=0; i<chkObj.length; i++){
			if(chkObj[i].checked){
				chckedVal=chckedVal+","+chkObj[i].value;
			}
		}
		chckedVal = chckedVal.replace(chckedVal.charAt(0),'').trim();
		}
		else{
		if(document.forms['myFileForm'].elements['myFlieId'].checked){
		chckedVal=document.forms['myFileForm'].elements['myFlieId'].value
		 }
		} 
		document.forms['myFileForm'].elements['myFileIds'].value=chckedVal;
		document.forms['myFileForm'].action= "updateNotesMyFileList.html";
		document.forms['myFileForm'].submit();
		}
	
	function checkMybox(){
		var checkedFiles = "";
		var column="";
		var oneColumn="";
		checkedFiles = document.forms['myFileForm'].elements['notesMyFileID'].value;
		if(checkedFiles!=""){
			if(checkedFiles.indexOf(",")  >= 0){
			column = checkedFiles.split(",");
			}else{
			oneColumn =checkedFiles
			} 
			if(column.length>1){
			for(var i = 0; i<column.length ; i++){
			try{
			     document.getElementById(column[i]).checked=true;
			     }catch(e){   }
			} 
			}else{ 
			    document.getElementById(oneColumn).checked=true;
			  
			}
			} 
			if(checkedFiles==""){
			}
	}
	function closeMyWindow(){
		var count="";
		count = document.forms['myFileForm'].elements['count'].value;
		if(count==""){
			document.forms['myFileForm'].elements['count'].value ="Counter";
		}else{
			parent.window.opener.document.location.reload();
			window.close();
		}
}
	
</script>
</head>
<s:form name="myFileForm" id="myFileForm" method="" action="">
	<s:hidden name="id" value="<%= request.getParameter("id") %>" />
	<s:hidden name="myFileIds" />
	<s:hidden name="count" value="<%= request.getParameter("count") %>" />
	<s:hidden name="notesMyFileID" value="${notes.myFileId}" />
	
	
	<table>
		<tr>
			<td>
			 	<display:table name="myFiles" class="table" requestURI="" id="myFiles" export="true" defaultsort="2" defaultorder="ascending" pagesize="50" style="width:100%;" >   
					   <display:column><input type="checkbox" name="myFlieId" id="${ myFiles.id}" value="${ myFiles.id}" ></display:column>
					   <display:column property="fileContentType" sortable="true" title="Type" />
					   <display:column property="description" sortable="true" title="Description" />
					   <display:column property="fileFileName" sortable="true" title="Document" />
					   <display:column property="fileSize" sortable="true" title="Size" />
					   <display:column property="updatedBy" sortable="true" title="Updated By" />
					   <display:column property="updatedOn" sortable="true" title="Updated On" />
				</display:table>
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:100px; font-size: 15;" 
				onclick="updateNotes(form);" value="Link to Note"/> 
			</td>
		</tr>
		</table>
</s:form>
<script type="text/javascript">
try{ 
	closeMyWindow();
	checkMybox();
}
catch(e){}
</script>
