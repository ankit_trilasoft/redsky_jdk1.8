// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;


INSERT INTO refmaster(corpID, code, description, fieldLength,parameter,createdOn,updatedOn,createdBy,updatedBy)
VALUES('PUTT','SurveyMail', 'Dear [customerFile.prefix] [customerFile.lastName] 1#
Date: [customerFile.survey] 4#
Arrival time: between [customerFile.surveyTime] hrs and [customerFile.surveyTime2] hrs 5#
We would like to confirm your appointment for the pre-move survey. 2#
During this visit we will make an estimate of the volume of the belongings you wish to move. We will also discuss your individual requirements, answer any questions you may have and outline what we can do for you. We would like to ask you to guide us through your residence and show us which items you would like to move. Please do not forget places like the attic, basement or garden shed. 3#
Your consultant will be: [customerFile.estimator] 6#
Mobile: [user.mobileNumber] 7#
Email: [user.email] 8#
[user.gender] will meet you at the following address: [customerFile.originAddress] 9#
For your removal to: [customerFile.destinationAddress] 10#
Please do not hesitate to contact us if there is anything we can do to help in the meantime.
If you have any specific questions that might require further research before our visit please let us know in advance so we can be sure to bring the right information along. 11#

Best Regards [customerFile.coordinator] 12#', 25, 'SurveyEmailMessage',now(),now(),'ssingh','ssingh');


INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('PUTT','SurveyEmailMessage',25,true,now(),'ssingh',now(),'ssingh',1,1,'');

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_billThroughDate` DATETIME NULL DEFAULT NULL  AFTER `TEN_housingRentVatDesc` ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_housingRentVatPercent` VARCHAR(6) NULL  AFTER `TEN_billThroughDate` ;

ALTER TABLE `redsky`.`tenancyutilityservice` ADD COLUMN `TEN_utilityVatPercent` VARCHAR(6) NULL  AFTER `TEN_utilityEndDate` ;

ALTER TABLE `redsky`.`claim` ADD COLUMN `claimEmailSent` DATETIME NULL  AFTER `responsibleAgentName` ;

ALTER TABLE redsky.accountline MODIFY COLUMN actualExpense DECIMAL(19,4), MODIFY COLUMN localAmount DECIMAL(19,4),
MODIFY COLUMN payableContractRateAmmount DECIMAL(19,4), MODIFY COLUMN payPayableAmount DECIMAL(19,4),
MODIFY COLUMN recRate DECIMAL(19,4), MODIFY COLUMN actualRevenue DECIMAL(19,4), MODIFY COLUMN recCurrencyRate DECIMAL(19,4),
MODIFY COLUMN actualRevenueForeign DECIMAL(19,4), MODIFY COLUMN contractRate DECIMAL(19,4), MODIFY COLUMN contractRateAmmount DECIMAL(19,4),
MODIFY COLUMN receivedAmount DECIMAL(19,4), MODIFY COLUMN entitlementAmount DECIMAL(19,4), MODIFY COLUMN estimateRate DECIMAL(19,4),
MODIFY COLUMN estimateExpense DECIMAL(19,4), MODIFY COLUMN estLocalAmount DECIMAL(19,4), MODIFY COLUMN estLocalRate DECIMAL(19,4),
MODIFY COLUMN estimatePayableContractRateAmmount DECIMAL(19,4), MODIFY COLUMN estimatePayableContractRate DECIMAL(19,4),
MODIFY COLUMN estimateSellRate DECIMAL(19,4), MODIFY COLUMN estimateRevenueAmount DECIMAL(19,4), MODIFY COLUMN estSellLocalRate DECIMAL(19,4),
MODIFY COLUMN estSellLocalAmount DECIMAL(19,4), MODIFY COLUMN estimateContractRate DECIMAL(19,4),
MODIFY COLUMN estimateContractRateAmmount DECIMAL(19,4), MODIFY COLUMN revisionRate DECIMAL(19,4),
MODIFY COLUMN revisionExpense DECIMAL(19,4), MODIFY COLUMN revisionLocalRate DECIMAL(19,4), MODIFY COLUMN revisionLocalAmount DECIMAL(19,4),
MODIFY COLUMN revisionPayableContractRate DECIMAL(19,4), MODIFY COLUMN revisionPayableContractRateAmmount DECIMAL(19,4),
MODIFY COLUMN revisionSellRate DECIMAL(19,4), MODIFY COLUMN revisionRevenueAmount DECIMAL(19,4),
MODIFY COLUMN revisionSellLocalRate DECIMAL(19,4), MODIFY COLUMN revisionSellLocalAmount DECIMAL(19,4),
MODIFY COLUMN revisionContractRate DECIMAL(19,4), MODIFY COLUMN revisionContractRateAmmount DECIMAL(19,4),
MODIFY COLUMN convertedAmount DECIMAL(19,4),MODIFY COLUMN estExpVatAmt DECIMAL(19,4),MODIFY COLUMN estVatAmt DECIMAL(19,4),
MODIFY COLUMN revisionExpVatAmt DECIMAL(19,4),MODIFY COLUMN revisionVatAmt DECIMAL(19,4),MODIFY COLUMN recVatAmt DECIMAL(19,4),
MODIFY COLUMN payVatAmt DECIMAL(19,4),
MODIFY COLUMN qstPayVatAmt DECIMAL(19,4),
MODIFY COLUMN qstRecVatAmt DECIMAL(19,4),MODIFY COLUMN distributionAmount DECIMAL(19,4);