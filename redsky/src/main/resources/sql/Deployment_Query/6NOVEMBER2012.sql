ALTER TABLE `redsky`.`accountline` ADD COLUMN `estimateStatus` VARCHAR(25) NULL  AFTER `onHand` ;

ALTER TABLE `redsky`.`reports` ADD COLUMN `formCondition` TEXT NULL  AFTER `formValue` ;

Insert into refmaster (corpid, code, description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values ('TSFT', '', '', 50, 'AccEstmateStatus','en',now(),'rchandra',now(),'rchandra'),('TSFT', 'Estimate Sent', 'Estimate Sent', 50, 'AccEstmateStatus','en',now(),'rchandra',now(),'rchandra'),('TSFT', 'Estimate Approved', 'Estimate Approved', 50, 'AccEstmateStatus','en',now(),'rchandra',now(),'rchandra'),('TSFT', 'Estimate Rejected', 'Estimate Rejected', 50, 'AccEstmateStatus','en',now(),'rchandra',now(),'rchandra');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','AccEstmateStatus',50,true,now(),'rchandra',now(),'rchandra',1,1,'');

ALTER TABLE `redsky`.`billing` ADD COLUMN `claimHandler` VARCHAR(45) DEFAULT NULL AFTER `ugwIntId`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `division` VARCHAR(10) AFTER `estimateStatus`;

ALTER TABLE `redsky`.`claim` ADD COLUMN `idNumber` VARCHAR(10) DEFAULT NULL AFTER `claimPerson`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `dosPartnerCode` VARCHAR(150) DEFAULT '' AFTER `miscellaneousdefaultdriver`;



