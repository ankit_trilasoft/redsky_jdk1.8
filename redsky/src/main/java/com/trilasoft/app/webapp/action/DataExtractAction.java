package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOvatCodeTotal;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTOStorageAnalysis;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTOUserDataSecurity;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.stoExtractDTO;
import com.trilasoft.app.dao.hibernate.dto.ExtractHibernateDTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.ExtractQueryFile;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExtractQueryFileManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;

public class DataExtractAction extends BaseAction {

	private Long id;

	private Date stoRecInvoiceDate;
	
	private Date beginDate;

	private Date endDate;

	private Date createDate;

	private Date toCreateDate;

	private Date deliveryTBeginDate;

	private Date deliveryTEndDate;

	private Date deliveryActBeginDate;

	private Date deliveryActEndDate;

	private Date loadTgtBeginDate;

	private Date loadTgtEndDate;

	private Date invPostingBeginDate;

	private Date invPostingEndDate;
	
	private Date payPostingBeginDate;
	
	private Date payPostingEndDate;
	
    private Date receivedBeginDate;
	
	private Date receivedEndDate;
	
	private Date postingPeriodBeginDate;
		
    private Date postingPeriodEndDate;
	
	private Date invoicingBeginDate;

	private Date invoicingEndDate;

	private Date revenueRecognitionBeginDate;

	private Date revenueRecognitionEndDate;
	
	private Date storageBeginDate;

	private Date storageEndDate;
	
	private Date flat_Report_from;
	
	private Date flat_Report_to;
	
	private Date claim_Report_from;
	
	private Date claim_Report_to;
	
	private Date additionalCharges_from;
	
	private Date additionalCharges_to;

	private List list;

	private String routingTypes;

	private String jobTypes;
	
	private String jobTypesCondition;

	private String billToCodeType;

	private String bookingCodeType;

	private String modeType;

	private String packModeType;

	private String commodityType;

	private String coordinatorType;

	private String salesmanType;

	private String oACountryType;

	private Date createdOnType;

	private String dACountryType;

	private String actualWgtType;

	private String actualVolumeType;

	private String conditionA1;

	private String conditionA2;

	private String conditionA3;

	private String conditionA4;

	private String conditionA5;

	private String conditionC21;

	private String conditionC22;

	private String conditionC211;

	private String conditionC222;

	private String conditionC233;

	private String conditionC244;

	private String conditionC200;

	private String conditionC201;

	private String conditionC255;

	private String conditionC266;
	
	private String conditionC300;
	
	private String conditionC301;

    private String conditionC302;
	
	private String conditionC303;
	
    private String conditionC304;
	
	private String conditionC305;
	
	private String conditionC277;

	private String conditionC288;
	
	private String conditionC311;
	
	private String conditionC322;
	
	private String conditionC333;
	
	private String conditionC344;

	private String conditionC1;

	private String conditionC12;

	private String conditionC13;

	private String conditionC14;

	private String conditionC15;

	private String conditionC16;

	private String conditionC17;

	private String conditionC18;

	private String conditionC19;

	private String conditionC120;

	private String conditionC121;

	private String conditionC122;

	private String conditionC31;

	private String conditionC3;

	private String conditionC115;

	private String conditionC77;

	private String conditionC78;

	private String conditionC79;
	
	private String conditionC80;
	private String publicPrivateFlag;
	private String queryName;
	private String conditionC81;
	
	private String conditionC82;
	private String companyCode;
    private String extractVal;
	private String activeStatus;
	private String addChargesType;
	private Date customerFileInitiation_from;
	private Date customerFileInitiation_to;
	private String instructionCode;
	
	/* Added By Kunal for ticket number: 6790*/
	private String billingStatus;
	/* Addition Ends */
	
	private String accountInterface;
	private String companyDivision;
	private Date date;

	private List companyCodeList;
	
	private List queryNameList;

	private String companyDivisionFlag;

	private List imfEstimate;

	private String targetActualW;

	private String serviceW;

	private String warehouseW;

	private String track;

	private String balance;

	private Date beginDateW;

	private Date endDateW;

	private Date storagedate;

	private Map<String, String> jobtype;

	private Map<String, String> routing;

	private Map<String, String> ocountry;

	private Map<String, String> dcountry;

	private Map<String, String> commodit;

	private Map<String, String> pkmode;

	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	
	private  Map<String, String> coord = new LinkedHashMap<String, String>();
	
	private Map<String, String> tcktactn;

	private Map<String, String> tcktservc;

	private Map<String, String> house;
	
	private Map<String, String> qltyServeyResMap;
	private Map<String, String> childMap;

	private Long timeDiff;

	//	 private static Map<String, String> mode;
	private List mode;

	private List bookingCode;

	private List billToCode;
	private List queryList;

	private String sessionCorpID;

	private RefMasterManager refMasterManager;

	private ServiceOrderManager serviceOrderManager;

	private CustomerFileManager customerFileManager;

	private CompanyDivisionManager companyDivisionManager;
	private ExtractQueryFile extractQueryFile;
    private ExtractQueryFileManager extractManager;
    
    private PartnerPrivateManager partnerPrivateManager;
    private PartnerPublicManager partnerPublicManager;
    
    private List childList = new ArrayList();
    
    private User user;
    
    Date currentdate = new Date();
    
    private List availChilds = new ArrayList();
    
    private String childParentIdList;
    private String reportType;
    
    private List<SystemDefault> sysDefaultDetail;
    private String volumeUnit;
    private Date invoice_Report_from;
	private Date invoice_Report_to;
	private Map<String, String> woinstr;
    
    static final Logger logger = Logger.getLogger(DataExtractAction.class);
     	
	public DataExtractAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		companyDivisionFlag = companyDivisionManager.findCompanyDivFlag(sessionCorpID).get(0).toString();
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString(); 
		companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//#8855
	//#8855 
	@SkipValidation
	public String soPartnerdataExtracts(){
		//getComboList(sessionCorpID);
		return SUCCESS;
	}
	
	public String checkQueryName(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		queryNameList=extractManager.fingQueryName(getRequest().getRemoteUser(),sessionCorpID,queryName,publicPrivateFlag);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		jobtype = refMasterManager.findByParameter(corpId, "JOB");
		 Map filterMap=user.getFilterMap(); 
	    	try {
	    		if (filterMap!=null && (!(filterMap.isEmpty()))) { 
	    			if(filterMap.containsKey("serviceOrderJobFilter")){
			    		List partnerCorpIDList = new ArrayList(); 
			    		Map<String, String> agentJobtype = new LinkedHashMap<String, String>();
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderJobFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	String job="";
			    	while (listIterator.hasNext()) {
			    		 job=listIterator.next().toString();
			    		if(jobtype.containsKey(job)){
			    			agentJobtype.put(job, jobtype.get(job))	;
			    		}
			    	}
			    	jobtype=agentJobtype;
			    	}
	    			
	    		} 
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
		routing = refMasterManager.findByParameter(corpId, "ROUTING");
		ocountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		dcountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		mode = refMasterManager.findByParameters(corpId, "MODE");
		pkmode = refMasterManager.findByParameter(corpId, "PKMODE");
		commodit = refMasterManager.findByParameter(corpId, "COMMODIT");
		sale = refMasterManager.findUser(corpId, "ROLE_SALE");
		coord = refMasterManager.findUser(corpId, "ROLE_COORD");
		tcktactn = refMasterManager.findByParameter(corpId, "TCKTACTN");
		tcktservc = refMasterManager.findByParameter(corpId, "TCKTSERVC");
		house = refMasterManager.findByParameter(corpId, "HOUSE");
		//bookingCode = customerFileManager.findBookingCode();
		//billToCode = customerFileManager.findBillToCode();
		woinstr = refMasterManager.findByParameter(corpId, "WOINSTR");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String worldBankInvoice() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public void worldBankInvoiceExtract() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		StringBuffer query = new StringBuffer();
		StringBuffer fileName = new StringBuffer();
		String header = new String();
		boolean isAnd = false;
		boolean storage = false;
		//Storage Quaterly Date for searching 
		String bDate = "";
		if (storagedate != null) {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			bDate = new String(dateformatYYYYMMDD.format(storagedate));
		}

		//Storage Quaterly Date for filename 		
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
		String bDate1 = dateFormat.format(c.getTime());
		//System.out.println("\n\n current date::"+bDate1);		

		/*String bDate1="";		
		 if(storagedate!=null){
		 SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MMddyyyy");
		 bDate1 = new String( dateformatYYYYMMDD.format( storagedate ) );	       
		 }*/

		// Non Quarterly Storage...........................		
		//System.out.println("\n\ntrack::" + track);
		if (track.equals("Non Quarterly Storage") && balance.equals("Debit")) {
			//isAnd=true;
			storage = true;
			if(sessionCorpID.equals("JKMS"))
			{
				query.append(" and a.actualRevenue" + "<>" + "0" + " and s.job in ('STO', 'TPS')" + " and date(a.receivedInvoiceDate)" + "<>" + "'" + bDate + "'"
						+ " and date(a.receivedInvoiceDate) >= '2008-12-17'" + " and  s.corpid = 'JKMS'" + " and a.status = true" + " and a.billToCode='T166386' " + "  and b.billToCode='T166386'"
						+ " group by a.recInvoiceNumber, au.invoicenumber "
						+ " having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber ");
						
				fileName.append("NQS_" + bDate1 + "_DB");
				header = " S/O Number" + "\t"+" SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + "STO Type" + "\t" + " InvoiceAmount"+ "\t" + "Pack and Haul and Delivery out"	+ "\t" + "Storage"	+ "\t" + "Crating" + "\t" + "Shuttle" + "\t" + "Coordination fee in/out" + "\t" + "Others" + "\t"	+ " Description"	+ "\t" + " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country " +   "\n";

			}
			else
			{
				query.append(" and a.actualRevenue" + "<>" + "0" + " and s.job in ('STO', 'TPS')" + " and date(a.receivedInvoiceDate)" + "<>" + "'" + bDate + "'"
						+ " and date(a.receivedInvoiceDate) >= '2008-12-17'" + " and  s.corpid = 'SSCW'" + " and a.status = true" + " and a.billToCode='500270' " + "  and b.billToCode='500270'"
						+ " group by a.recInvoiceNumber, au.invoicenumber "
						+ " having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber ");
					
				fileName.append("NQS_" + bDate1 + "_DB");
			    header = " SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + "STO Type" + "\t" + " InvoiceAmount"+ "\t" + "Pack and Haul and Delivery out"	+ "\t" + "Storage"	+ "\t" + "Crating" + "\t" + "Shuttle" + "\t" + "Coordination fee in/out" + "\t" + "Others" + "\t"	+ " Description"	+ "\t" + " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country " + "\t" + "  Invoice contract " + "\t" + "  Billing contract "+ "\n";
			}}
		if (track.equals("Non Quarterly Storage") && balance.equals("Credit")) {

			//isAnd=true;
			storage = true;
		if(sessionCorpID.equals("JKMS"))
			{
				query.append(" and a.actualRevenue" + "<>" + "0" + " and s.job in ('STO', 'TPS')" + " and date(a.receivedInvoiceDate)" + "<>" + "'" + bDate + "'"
						+ " and date(a.receivedInvoiceDate) >= '2008-12-17'" + " and  s.corpid = 'JKMS'" + " and a.status = true" + " and a.billToCode='T166386' " + "  and b.billToCode='T166386'"
						+ " group by a.recInvoiceNumber, au.invoicenumber "
						+ " having sum(a.actualRevenue) < 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber ");
				fileName.append("NQS_" + bDate1 + "_CR");
				header = " S/O Number" + "\t"+" SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + " InvoiceAmount" + "\t" +"Crating" + "\t" + "Coordination fee in/out" + "\t" + " Description" + "\t" +  " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country "+"\n";
			}
			else
			{
				query.append(" and a.actualRevenue" + "<>" + "0" + " and s.job in ('STO', 'TPS')" + " and date(a.receivedInvoiceDate)" + "<>" + "'" + bDate + "'"
						+ " and date(a.receivedInvoiceDate) >= '2008-12-17'" + " and  s.corpid = 'SSCW'" + " and a.status = true" + " and a.billToCode='500270' " + "  and b.billToCode='500270'"
						+ " group by a.recInvoiceNumber, au.invoicenumber "
						+ " having sum(a.actualRevenue) < 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber ");
					
			fileName.append("NQS_" + bDate1 + "_CR");
			header = " SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + " InvoiceAmount" + "\t" +"Crating" + "\t" + "Coordination fee in/out" + "\t" + " Description" + "\t" +  " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country "+"\t" + "  Invoice contract " + "\t" + "  Billing contract "+"\n";
			}}

		// Quarterly Storage..................................		
		if (track.equals("Quarterly Storage") && balance.equals("Debit")) {
			storage = true;
			if(sessionCorpID.equals("JKMS"))
			{
				query.append(" and a.actualRevenue" + "<>" + "0" + " and s.job in ('STO', 'TPS')" + " and date(a.receivedInvoiceDate)" + "=" + "'" + bDate + "'"
						+ " and date(a.receivedInvoiceDate) >= '2008-12-17'" + " and  s.corpid = 'JKMS'" + " and a.status = true" + " and a.billToCode='T166386' " + "   and b.billToCode='T166386'"
						+ " group by a.recInvoiceNumber, au.invoicenumber "
						+ " having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber ");
				fileName.append("QS_" + bDate1 + "_DB");
				header = " S/O Number" + "\t"+" SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + "STO Type" + "\t" + " InvoiceAmount"+ "\t" + "Pack and Haul and Delivery out"	+ "\t" + "Storage"	+ "\t" + "Crating" + "\t" + "Shuttle" + "\t" + "Coordination fee in/out" + "\t" + "Others" + "\t"	+ " Description"	+ "\t" + " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country " + "\n";

			}
			else
			{
			query.append(" and a.actualRevenue" + "<>" + "0" + " and s.job in ('STO', 'TPS')" + " and date(a.receivedInvoiceDate)" + "=" + "'" + bDate + "'"
					+ " and date(a.receivedInvoiceDate) >= '2008-12-17'" + " and  s.corpid = 'SSCW'" + " and a.status = true" + " and a.billToCode='500270' " + "   and b.billToCode='500270'"
					+ " group by a.recInvoiceNumber, au.invoicenumber "
					+ " having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber ");
			
			fileName.append("QS_" + bDate1 + "_DB");
			header = " SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + "STO Type" + "\t" + " InvoiceAmount"+ "\t" + "Pack and Haul and Delivery out"	+ "\t" + "Storage"	+ "\t" + "Crating" + "\t" + "Shuttle" + "\t" + "Coordination fee in/out" + "\t" + "Others" + "\t"	+ " Description"	+ "\t" + " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country " +"\t" + "  Invoice contract " + "\t" + "  Billing contract " + "\n";
			}
		}
		
		if (track.equals("Quarterly Storage") && balance.equals("Credit")) {
			//System.out.println("\n\n....... Quarterly Storage.......Credit.......(not run)............" + date);

		}

		//Transit Jobs..............................
		if (track.equals("Transit Jobs") && balance.equals("Debit")) {
			isAnd = true; 
			if(sessionCorpID.equals("JKMS"))
			{if (bDate.equals("")) { 
				query.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='JKMS' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id left outer join `refmaster` refmaster_OR on s.originCountryCode =refmaster_OR.`code` and refmaster_OR.`parameter` = 'COUNTRY' and refmaster_OR.`corpID` in ('JKMS','TSFT') and refmaster_OR.`language`='en' left outer join `refmaster` refmaster_DR on s.destinationCountryCode =refmaster_DR.`code` and refmaster_DR.`parameter` = 'COUNTRY' and refmaster_DR.`corpID` in ('JKMS','TSFT') and refmaster_DR.`language`='en' where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and date(a.receivedInvoiceDate) is not null and date(a.receivedInvoiceDate) >= '2008-12-17' and s.corpid = 'JKMS' and a.status = true  and a.billToCode='T166386' and b.billToCode='T166386'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");
			 } else { 
				query.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='JKMS' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id left outer join `refmaster` refmaster_OR on s.originCountryCode =refmaster_OR.`code` and refmaster_OR.`parameter` = 'COUNTRY' and refmaster_OR.`corpID` in ('JKMS','TSFT') and refmaster_OR.`language`='en' left outer join `refmaster` refmaster_DR on s.destinationCountryCode =refmaster_DR.`code` and refmaster_DR.`parameter` = 'COUNTRY' and refmaster_DR.`corpID` in ('JKMS','TSFT') and refmaster_DR.`language`='en' where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and date(a.receivedInvoiceDate) <> '"
						+ bDate
						+ "' and date(a.receivedInvoiceDate) >= '2008-12-17' and s.corpid = 'JKMS' and a.status = true  and a.billToCode='T166386'  and b.billToCode='T166386'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");
				
                  }
            fileName.append("TJ_" + bDate1 + "_DB"); 
			header = " S/O Number" + "\t"+" SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + " Ship Type" + "\t" + " Ship Mode" + "\t" + " InvoiceAmount"	+ "\t" +  "OA/Freight/DA"	+ "\t" + "Port Storage"	+ "\t" + "Demmurage"	+ "\t" + "Crating"	+ "\t" + "Per Diem"+ "\t" + "Shuttle"+ "\t" + "Parking Permit"+ "\t" + "SIT"+ "\t" + "Whse.Handling"+ "\t" + "Coordination Fee"	+ "\t" + "SOLAS" + "\t" + "Other Charges" + "\t" + " Actual Wgt "+ "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin Country "
					+ "\t" + "Origin Region"+ "\t" + "  Dest Agent " + "\t" + "  Dest City " + "\t" +"  Dest.Country "	+ "\t" + "Destination Region" + "\t" + " Staff Rule " +"\t" + "Load Date" + "\t" + "Customs Clearance Date" + "\t" + " Commodity " + "\t" + " Cont. Size " + "\t" + " IBRD/IFC " + "\n";
 			}
			else
			{if (bDate.equals("")) { 
				query.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='SSCW' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id left outer join `refmaster` refmaster_OR on s.originCountryCode =refmaster_OR.`code` and refmaster_OR.`parameter` = 'COUNTRY' and refmaster_OR.`corpID` in ('SSCW','TSFT') and refmaster_OR.`language`='en' left outer join `refmaster` refmaster_DR on s.destinationCountryCode =refmaster_DR.`code` and refmaster_DR.`parameter` = 'COUNTRY' and refmaster_DR.`corpID` in ('SSCW','TSFT') and refmaster_DR.`language`='en' where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and date(a.receivedInvoiceDate) is not null and date(a.receivedInvoiceDate) >= '2008-12-17' and s.corpid = 'SSCW' and a.status = true  and a.billToCode='500270' and b.billToCode='500270'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");
			 } else { 
				query.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='SSCW' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id left outer join `refmaster` refmaster_OR on s.originCountryCode =refmaster_OR.`code` and refmaster_OR.`parameter` = 'COUNTRY' and refmaster_OR.`corpID` in ('SSCW','TSFT') and refmaster_OR.`language`='en' left outer join `refmaster` refmaster_DR on s.destinationCountryCode =refmaster_DR.`code` and refmaster_DR.`parameter` = 'COUNTRY' and refmaster_DR.`corpID` in ('SSCW','TSFT') and refmaster_DR.`language`='en' where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and date(a.receivedInvoiceDate) <> '"
						+ bDate
						+ "' and date(a.receivedInvoiceDate) >= '2008-12-17' and s.corpid = 'SSCW' and a.status = true  and a.billToCode='500270'  and b.billToCode='500270'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) > 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");
            }
			fileName.append("TJ_" + bDate1 + "_DB"); 
			header =" S/O Number" + "\t"+ " SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + " Ship Type" + "\t" + " Ship Mode" + "\t" + " InvoiceAmount"	+ "\t" +  "OA/Freight/DA"	+ "\t" + "Port Storage"	+ "\t" + "Demmurage"	+ "\t" + "Crating"	+ "\t" + "Per Diem"+ "\t" + "Shuttle"+ "\t" + "Parking Permit"+ "\t" + "SIT"+ "\t" + "Whse.Handling"+ "\t" + "Coordination Fee"	+ "\t" + "SOLAS" + "\t" + "Other Charges" + "\t" + " Actual Wgt "+ "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin Country "
					+ "\t" + "Origin Region"+ "\t" + "  Dest Agent " + "\t" + "  Dest City " + "\t" +"  Dest.Country "	+ "\t" + "Destination Region" + "\t" + " Staff Rule " +"\t" + "Load Date" + "\t" + "Customs Clearance Date" + "\t" + " Commodity " + "\t" + " Cont. Size " + "\t" + " IBRD/IFC " +"\t" + "  Invoice contract " + "\t" + "  Billing contract " + "\n";
			}}
		if (track.equals("Transit Jobs") && balance.equals("Credit")) {
			isAnd = true;
			
			if(sessionCorpID.equals("JKMS"))
			{
			if (bDate.equals("")) {
				query
				.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='JKMS' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and a.receivedInvoiceDate is not null and a.receivedInvoiceDate >= '2008-12-17' and s.corpid = 'JKMS' and a.status = true  and a.billToCode='T166386' and b.billToCode='T166386'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) < 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");

			} else {
				query
				.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='JKMS' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and a.receivedInvoiceDate <> '"
						+ bDate
						+ "' and a.receivedInvoiceDate >= '2008-12-17' and s.corpid = 'JKMS' and a.status = true and a.billToCode='T166386' and b.billToCode='T166386'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) < 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");
                }
			fileName.append("TJ_" + bDate1 + "_CR"); 
			header =" S/O Number" + "\t"+" SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + " Ship Type" + "\t" + " Ship Mode" + "\t" + " InvoiceAmount"			+ "\t" + " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country "
			+ "\t" + "  Dest Agent " + "\t" + "  Dest City " + "\t" + "  Dest.State " + "\t" + "  Dest.Country " + "\t" + " Staff Rule " + "\t" + " PACKA " + "\t" + " LOADA " + "\t" + " CLEARCUSTOMS " + "\t" + " Commodity " + "\t" + " SIZE " + "\t" + " Origin Company " + "\t" + " Origin Organization "  + "\t" + " Origin Primary Email "  + "\t" + " Destination  Primary Email "  +"\n";
			}
			else
			{if (bDate.equals("")) {
				query
				.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='SSCW' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and a.receivedInvoiceDate is not null and a.receivedInvoiceDate >= '2008-12-17' and s.corpid = 'SSCW' and a.status = true  and a.billToCode='500270' and b.billToCode='500270'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) < 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");

			} else {
				query
				.append(" from serviceorder s inner join accountline a on s.id = a.serviceOrderId inner join billing b on b.id = s.id inner join miscellaneous m on m.id = s.id inner join trackingstatus t on t.id = s.id  inner join customerfile c on c.id = s.customerfileId left outer join authorizationno au on s.id = au.serviceOrderId and au.id in (select min(auu.id) from authorizationno auu where auu.invoiceNumber=a.recInvoiceNumber and auu.corpid='SSCW' group by a.recInvoiceNumber) left outer join container co on s.id = co.serviceOrderId and co.idNumber = (select min(aa.idNumber) from container aa where aa.serviceOrderId = co.serviceOrderId group by co.idNumber) left outer join vr_servicepartner_summary_by_soid sp on  sp.serviceOrderId = s.id where a.recInvoiceNumber>0 and a.sendActualToClient is null and s.status <> 'CNCL' and a.status is true and a.actualRevenue <> 0  and s.job not in ('STO', 'TPS') and a.receivedInvoiceDate <> '"
						+ bDate
						+ "' and a.receivedInvoiceDate >= '2008-12-17' and s.corpid = 'SSCW' and a.status = true and a.billToCode='500270' and b.billToCode='500270'  group by a.recInvoiceNumber, au.invoicenumber having sum(a.actualRevenue) < 0 and a.recinvoicenumber = au.invoicenumber order by a.recInvoiceNumber, au.invoicenumber");
                }
			fileName.append("TJ_" + bDate1 + "_CR"); 
			header = " S/O Number" + "\t"+" SAP" + "\t" + " Invnum" + "\t" + "Staff Name" + "\t" + " Ship Type" + "\t" + " Ship Mode" + "\t" + " InvoiceAmount"			+ "\t" + " Actual Wgt " + "\t" + "  Origin Agent " + "\t" + " Origin City " + "\t" + "  Origin State " + "\t" + "  Origin Country "
			+ "\t" + "  Dest Agent " + "\t" + "  Dest City " + "\t" + "  Dest.State " + "\t" + "  Dest.Country " + "\t" + " Staff Rule " + "\t" + " PACKA " + "\t" + " LOADA " + "\t" + " CLEARCUSTOMS " + "\t" + " Commodity " + "\t" + " SIZE " + "\t" + " Origin Company " + "\t" + " Origin Organization "  + "\t" + " Origin Primary Email "  + "\t" + " Destination  Primary Email "  +"\t" + "  Invoice contract " + "\t" + "  Billing contract " +"\n";

			}
			} 
		imfEstimate = serviceOrderManager.findWorldBankExtract(query.toString(), track, balance,sessionCorpID); 
		try { 
			File file1 = new File(fileName.toString());
			if (!imfEstimate.isEmpty()) {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(header.getBytes());
				Iterator it = imfEstimate.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
					if(sessionCorpID.equals("JKMS"))
					{
					if (track.equals("Non Quarterly Storage") && balance.equals("Debit")) {
						if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					}
					if (track.equals("Non Quarterly Storage") && balance.equals("Credit")) {
						if (row[15] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					}
					if (track.equals("Quarterly Storage")) {
						//System.out.println("\n\n Non Quarterly Storage + Quarterly Storage");
						/*if(sessionCorpID.equals("JKMS"))
						{*/
						if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
					if (track.equals("Transit Jobs") && balance.equals("Credit")) {
						
					
						if (row[30] != null) {
							outputStream.write((row[30].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
					if(track.equals("Transit Jobs")&& balance.equals("Debit")){
						//System.out.println("\n\n Transit Jobs");
					
						if (row[42] != null) {
							outputStream.write((row[42].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						}}
					if (row[0] != null) {
						outputStream.write((row[0].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[1] != null) {
						outputStream.write((row[1].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[2] != null) {
						outputStream.write((row[2].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[3] != null) {
						outputStream.write((row[3].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					
					if (track.equals("Non Quarterly Storage") && balance.equals("Debit")) {
						if (row[4] != null) {
							outputStream.write((row[4].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[14] != null) {
							outputStream.write((row[14].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if(!sessionCorpID.equals("JKMS"))
						{
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[18] != null) {
							outputStream.write((row[18].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
						
								outputStream.write("\n".getBytes());
					}
					
					
					if (track.equals("Non Quarterly Storage") && balance.equals("Credit")) {
						if (row[4] != null) {
							outputStream.write((row[4].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if(!sessionCorpID.equals("JKMS"))
						{
						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
								outputStream.write("\n".getBytes());
		
					}					
					
					if (track.equals("Quarterly Storage")) {
						//System.out.println("\n\n Non Quarterly Storage + Quarterly Storage");
						if (row[4] != null) {
							outputStream.write((row[4].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						//System.out.println("\n\nrow7::"+row[7]);

						//System.out.println("\n\nrow8::"+row[8]);

						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						//System.out.println("\n\nrow9::"+row[9]);

						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[14] != null) {
							outputStream.write((row[14].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if(!sessionCorpID.equals("JKMS"))
						{
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[18] != null) {
							outputStream.write((row[18].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
						if(sessionCorpID.equals("JKMS"))
						{
						if (row[19] != null) {
							outputStream.write((row[19].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						}
						outputStream.write("\n".getBytes());
					}
					if (track.equals("Transit Jobs") && balance.equals("Credit")) {
						//System.out.println("\n\n Transit Jobs");
						if (row[4] != null) {
							if (row[19] != null && row[19].toString().equalsIgnoreCase("SIT")) {
								outputStream.write(("SIT" + "\t").getBytes());
							} else {
								outputStream.write((row[4].toString() + "\t").getBytes());
							}
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[14] != null) {
							outputStream.write((row[14].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						
						if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						/*if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						/*if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						/*if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/

						//System.out.println("\n\n row[19]::" + row[19]);
						//System.out.println("\n\n row[20]::" + row[20]);
						if (row[16] != null) {
							Date date = (Date) row[16];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							String nowYYYYMMDD = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((nowYYYYMMDD + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[17] != null) {
							Date date = (Date) row[17];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							String nowYYYYMMDD = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((nowYYYYMMDD + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}

						if (row[18] != null) {
							Date date = (Date) row[18];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							String nowYYYYMMDD = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((nowYYYYMMDD + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						
						if (row[19] != null) {
							outputStream.write((row[19].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[21] != null) {
							outputStream.write((row[21].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[22] != null) {
							outputStream.write((row[22].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[23] != null) {
							outputStream.write((row[23].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[24] != null) {
							outputStream.write((row[24].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[25] != null) {
							outputStream.write((row[25].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if(!sessionCorpID.equals("JKMS"))
						{
						if (row[26] != null) {
							outputStream.write((row[26].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[27] != null) {
							outputStream.write((row[27].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
						
						if (isAnd == true) {

						/*	if (row[21] != null) {
								outputStream.write((row[21].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());

							}
							if (row[22] != null) {
								outputStream.write((row[22].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());

							}*/
							
							
							
							outputStream.write("\n".getBytes());
						}

					}
					if(track.equals("Transit Jobs")&& balance.equals("Debit")){
						//System.out.println("\n\n Transit Jobs");
						if (row[4] != null) {
							if (row[33] != null && row[33].toString().equalsIgnoreCase("SIT")) {
								outputStream.write(("SIT" + "\t").getBytes());
							} else {
								outputStream.write((row[4].toString() + "\t").getBytes());
							}
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}		
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[14] != null) {
							outputStream.write((row[14].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}						
						if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[18] != null) {
							outputStream.write((row[18].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[19] != null) {
							outputStream.write((row[19].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						/*if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						if (row[21] != null) {
							outputStream.write((row[21].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[22] != null) {
							outputStream.write((row[22].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[23] != null) {
							outputStream.write((row[23].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[24] != null) {
							outputStream.write((row[24].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[25] != null) {
							outputStream.write((row[25].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						/*if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						if (row[27] != null) {
							outputStream.write((row[27].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[28] != null) {
							outputStream.write((row[28].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						
						if (row[34] != null) {
							outputStream.write((row[34].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						/*if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						/*if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						/*if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/

						//System.out.println("\n\n row[19]::" + row[19]);
						//System.out.println("\n\n row[20]::" + row[20]);
						/*if (row[24] != null) {
							Date date = (Date) row[24];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							String nowYYYYMMDD = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((nowYYYYMMDD + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}*/
						if (row[31] != null) {
							Date date = (Date) row[31];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							String nowYYYYMMDD = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((nowYYYYMMDD + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}

						if (row[32] != null) {
							Date date = (Date) row[32];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							String nowYYYYMMDD = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((nowYYYYMMDD + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						
						if (row[33] != null) {
							outputStream.write((row[33].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[35] != null) {
							outputStream.write((row[35].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						/*if (row[30] != null) {
						 * 
							outputStream.write((row[30].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}*/
						if (row[37] != null) {
							outputStream.write((row[37].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if(!sessionCorpID.equals("JKMS"))
						{
						if (row[40] != null) {
							outputStream.write((row[40].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[41] != null) {
							outputStream.write((row[41].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}}
						
						/*if (row[32] != null) {
							outputStream.write((row[32].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[33] != null) {
							outputStream.write((row[33].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}*/
						if (isAnd == true) {

						/*	if (row[21] != null) {
								outputStream.write((row[21].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());

							}
							if (row[22] != null) {
								outputStream.write((row[22].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());

							}*/
							
							
							
							outputStream.write("\n".getBytes());
						}

					}

				}

			} else {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				//header=" SAP"+"\t"+" Invnum"+"\t"+"Staff Name"+"\t"+" Ship Type"+"\t"+" Ship Mode"+"\t"+" UGWW"+"\t"+" Shipnum"+"\t"+" InvoiceAmount"+"\t"+" Booker "+"\n";
				header = "NO Record Found , thanks";
				outputStream.write(header.getBytes());

			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	//second time.
	private List deliveryExtract;
	private List componentMillitaryShipment;
	public void worldBankdeliveryExtract(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		BigDecimal totWeight = new BigDecimal(0.00);
		String header = new String();
		deliveryExtract = serviceOrderManager.getDeliveryReportList(sessionCorpID);
		Set shipnmber = new HashSet();
		try{
			  if(!deliveryExtract.isEmpty()){
				  HttpServletResponse response =getResponse();
				  ServletOutputStream outputStream = response.getOutputStream();
				  File deliveryFile =new File("deliveryExtract");
				  response.setContentType("application/vnd.ms-excel");
				  //response.setHeader("Cache-Control", "no-cache");	
				  response.setHeader("Content-Disposition", "attachment; filename="+deliveryFile.getName()+".xls");			 
				  response.setHeader("Pragma", "public");
				  response.setHeader("Cache-Control", "max-age=0");
				  if(sessionCorpID.equalsIgnoreCase("JKMS"))
				  {
				  header=" S/O Number"+"\t"+" Shipper"+"\t"+" Move Manager"+"\t"+" Shipment Number"+"\t"+" Bill To Authority"+"\t"+" Staff Rule"+"\t"+" Job Type"+"\t"+" Routing"+"\t"+" Mode"+"\t"+" Commodity"+"\t"+" City"+"\t"+" Zip"+"\t"+" Home Phone"+"\t"+" Office Phone"+"\t"+" Email"+"\t"+" Actual Weight"+"\t"+" Booker"+"\t"+" Origin Agent"+"\t"+" Destination Agent"+"\t"+" Delivery Date"  + "\t" + " Origin Company "+"\n";
				  }
				  else
					  {
					  header=" Shipper"+"\t"+" Move Manager"+"\t"+" Shipment Number"+"\t"+" Bill To Authority"+"\t"+" Staff Rule"+"\t"+" Job Type"+"\t"+" Routing"+"\t"+" Mode"+"\t"+" Commodity"+"\t"+" City"+"\t"+" Zip"+"\t"+" Home Phone"+"\t"+" Office Phone"+"\t"+" Email"+"\t"+" Actual Weight"+"\t"+" Booker"+"\t"+" Origin Agent"+"\t"+" Destination Agent"+"\t"+" Delivery Date"  + "\t" + " Origin Company " + "\t" + " Billing Contract " +"\n";
					  }
				  outputStream.write(header.getBytes());
				    
				  Iterator it = deliveryExtract.iterator();
				  while(it.hasNext()){
					  
					  Object []row= (Object [])it.next();
					  if(sessionCorpID.equalsIgnoreCase("JKMS"))
					  {
					  if(row[21] != null){
						     outputStream.write((row[21].toString()+"\t").getBytes()) ;
					  }
					  else{
						     outputStream.write("\t".getBytes());
					  }}
					  if(row[0] != null){
						     outputStream.write((row[0].toString()+"\t").getBytes()) ;
					  }
					  else{
						     outputStream.write("\t".getBytes());
					  }
					  if(row[1] != null){
							 outputStream.write((row[1].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[2] != null){
							
						  
						  //shipnmber.add(row[2].toString());
						  outputStream.write((row[2].toString()+"\t").getBytes()) ;
					 }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[3] != null){
							 outputStream.write((row[3].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[4] != null){
							 outputStream.write((row[4].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[5] != null){
							 outputStream.write((row[5].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[6] != null){
							 outputStream.write((row[6].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[7] != null){
							 outputStream.write((row[7].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[8] != null){
							 outputStream.write((row[8].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[9] != null){
							 outputStream.write((row[9].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[10] != null){
							 outputStream.write((row[10].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[11] != null){
							 outputStream.write((row[11].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[12] != null){
							 outputStream.write((row[12].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[13] != null){
							 outputStream.write((row[13].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if (row[14] != null) {
				            String str = (String) row[14].toString();
				            totWeight = new BigDecimal(str);
				            totWeight.setScale(2);
				            outputStream.write((totWeight + "\t").getBytes());
				          } else {
				            outputStream.write("\t".getBytes());
				          }
					  if(row[15] != null){
							 outputStream.write((row[15].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[16] != null){
							 outputStream.write((row[16].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if(row[17] != null){
							 outputStream.write((row[17].toString()+"\t").getBytes()) ;
					  }
					  else{
							 outputStream.write("\t".getBytes());
					  }
					  if (row[18] != null) {
						    String rsDate = "";
							Date date = (Date) row[18];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
							rsDate = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((rsDate+"\t").getBytes());
							} else {
							outputStream.write("\t".getBytes());
							}
					  if(row[19] != null){
						 outputStream.write((row[19].toString()+"\t").getBytes()) ;
					  }
				  else{
						outputStream.write("\t".getBytes());
				  	} 
					  if(!sessionCorpID.equalsIgnoreCase("JKMS"))
					  {
					  if(row[20] != null){
							 outputStream.write((row[20].toString()+"\t").getBytes()) ;
						  }
					  else{
							outputStream.write("\t".getBytes());
					  	} }
				   outputStream.write("\n".getBytes()) ;
				  }
			  }
		
		else
		{
			HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();
			File deliveryFile =new File("deliveryExtract");
			response.setContentType("application/vnd.ms-excel");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+deliveryFile.getName()+".xls");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			header="NO Record Found , thanks";
			outputStream.write(header.getBytes());
		}
	}
		catch(Exception e){e.printStackTrace();}
		/*Iterator its = shipnmber.iterator();
		StringBuffer shipnmber1 = new StringBuffer();
		  while(its.hasNext()){
			  String Ship=(String) its.next();
			  shipnmber1=shipnmber1.append("'");
			  shipnmber1=shipnmber1.append(Ship);
			  shipnmber1=shipnmber1.append("',");
		  }
		
		  String shipnmberUpdate=new String(shipnmber1);
		  if(!shipnmberUpdate.equals(""))
	        {
			  shipnmberUpdate=shipnmberUpdate.substring(0, shipnmberUpdate.length()-1);
	        }
	        else if(shipnmberUpdate.equals(""))
	        {
	        	shipnmberUpdate =new String("''");
	        } 
		int a=serviceOrderManager.updateSeviceOrdersentcomp(shipnmberUpdate);
*/		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
}
	public String billingPersonFlag;
	private String vendorCode;
	private String vendorName;
	 private boolean checkFieldVisibility=false;
	public void workTicketExtract() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		StringBuffer query = new StringBuffer();
		BigDecimal totWeight = new BigDecimal(0.00);
		boolean isAnd = false;
		String bDate = "";
		if (beginDateW != null) {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			bDate = new String(dateformatYYYYMMDD.format(beginDateW));

		}
		String eDate = "";
		if (endDateW != null) {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			eDate = new String(dateformatYYYYMMDD.format(endDateW));

		}

		if (conditionA1.equals("") && conditionA2.equals("") && conditionA3.equals("") && bDate.equals("") && eDate.equals("")) {
			//System.out.println("\n\n All Condition are blank.....................\n");
		} else {

			// Ticket Status
			if (conditionA1.trim().equalsIgnoreCase("Equal to")) {
				if (isAnd == true) {
					query.append(" and targetActual" + "=" + "'" + targetActualW + "'");
				} else {
					query.append(" targetActual" + "=" + "'" + targetActualW + "'");
				}

			}
			if (conditionA1.trim().equalsIgnoreCase("Not Equal to")) {
				if (isAnd == true) {
					query.append(" and targetActual" + "<>" + "'" + targetActualW + "'");
				} else {
					query.append(" targetActual" + "<>" + "'" + targetActualW + "'");
				}

			}

			//Service  	
			if (conditionA2.trim().equalsIgnoreCase("Equal to")) {
				if (isAnd == true) {
					query.append(" and service" + "=" + "'" + serviceW + "'");
				} else {
					if (targetActualW != null && !targetActualW.equals("")) {
						query.append(" and ");
					}
					query.append(" service" + "=" + "'" + serviceW + "'");
				}

			}

			if (conditionA2.trim().equalsIgnoreCase("Not Equal to")) {
				if (isAnd == true) {
					query.append(" and service" + "<>" + "'" + serviceW + "'");
				} else {
					if (targetActualW != null && !targetActualW.equals("")) {
						query.append(" and ");
					}

					query.append(" service" + "<>" + "'" + serviceW + "'");
				}

			}
			//Warehouse
			if (conditionA3.trim().equalsIgnoreCase("Equal to")) {
				if (isAnd == true) {

					query.append(" and w.warehouse" + "=" + "'" + warehouseW + "'");
				} else {
					if (serviceW != null && !serviceW.equals("") || targetActualW != null && !targetActualW.equals("")) {
						query.append(" and ");
					}

					query.append(" w.warehouse" + "=" + "'" + warehouseW + "'");
				}

			}
			if (conditionA3.trim().equalsIgnoreCase("Not Equal to")) {
				if (isAnd == true) {

					query.append(" and w.warehouse" + "<>" + "'" + warehouseW + "'");
				} else {
					if (serviceW != null && !serviceW.equals("") || targetActualW != null && !targetActualW.equals("")) {
						query.append(" and ");
					}
					query.append(" w.warehouse" + "<>" + "'" + warehouseW + "'");
				}

			}
			//Begin Date  	
			if (conditionA4.trim().equalsIgnoreCase("Greater than Equal to")) {
				if (isAnd == true) {

					query.append(" and date1" + ">=" + "'" + bDate + "'");
				} else {
					if (warehouseW != null && !warehouseW.equals("") || serviceW != null && !serviceW.equals("") || targetActualW != null && !targetActualW.equals("")) {
						query.append(" and ");
					}
					query.append(" date1" + ">=" + "'" + bDate + "'");
				}

			}
			if (conditionA4.trim().equalsIgnoreCase("Less than Equal to")) {
				if (isAnd == true) {

					query.append(" and date1" + "<=" + "'" + bDate + "'");
				} else {
					if (warehouseW != null && !warehouseW.equals("") || serviceW != null && !serviceW.equals("") || targetActualW != null && !targetActualW.equals("")) {
						query.append(" and ");
					}
					query.append(" date1" + "<=" + "'" + bDate + "'");
				}

			}
			//End Date  	
			if (conditionA5.trim().equalsIgnoreCase("Greater than Equal to")) {
				if (isAnd == true) {

					query.append(" and date2" + ">=" + "'" + eDate + "'");
				} else {
					if (bDate != null) {
						query.append(" and ");
					}
					query.append(" date2" + ">=" + "'" + eDate + "'");
				}

			}
			if (conditionA5.trim().equalsIgnoreCase("Less than Equal to")) {
				if (isAnd == true) {

					query.append(" and date2" + "<=" + "'" + eDate + "'");
				} else {
					if (bDate != null) {
						query.append(" and ");
					}
					query.append(" date2" + "<=" + "'" + eDate + "'");
				}

			}
			/* Added variable billingStatus for ticket number: 6790*/
			if (billingStatus.trim().equalsIgnoreCase("Billed")) {
					query.append(" and reviewStatus " + "=" + "'Billed'");
			}
			else if (billingStatus.trim().equalsIgnoreCase("Unbilled")) {
					query.append(" and reviewStatus " + "=" + "'UnBilled'");
			}
			else {
			}
			
			if(!instructionCode.equals("")){
			query.append(" and w.instructionCode " + "=" + "'" + instructionCode+"'");
			}
			
			if(vendorCode!=null && !vendorCode.equalsIgnoreCase("")){
				query.append(" and w.vendorCode ='"+vendorCode+"'");
			}
			if(vendorName!=null && !vendorName.equalsIgnoreCase("")){
				query.append(" and w.vendorName ='"+vendorName+"'");
			}
			/* Modification Ends */
			query.append(" and " + " w.corpid =  '" + sessionCorpID +"'" );
		}
		
		
		// dao method called 
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {				
				volumeUnit = systemDefault.getVolumeUnit();
				}
		}
		imfEstimate = serviceOrderManager.findWorkTicketExtract(query.toString());
		componentMillitaryShipment=serviceOrderManager.findComponentMillitaryShipment(sessionCorpID);
		try{
			String permKey = sessionCorpID +"-"+"component.tab.workTicket.projectManager";
			checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			}catch(Exception e){
				e.printStackTrace();
			}
		try {
			File file1 = new File("workticket");
			String state = new String();
			String service = new String();
			if (!imfEstimate.isEmpty()) {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				//response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				if(!checkFieldVisibility && !sessionCorpID.equals("WEST"))
				{
				if(billingPersonFlag!=null && billingPersonFlag.equalsIgnoreCase("Yes")){
				if(!componentMillitaryShipment.isEmpty()){
				String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
				+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
				+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
				+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+"openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
				+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+      "\n";
				outputStream.write(header.getBytes());
				} else {
				String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"+"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "\n"   ;
					outputStream.write(header.getBytes());					    
				}
				}else{ 
					if(!componentMillitaryShipment.isEmpty()){
						String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "\n";
						outputStream.write(header.getBytes());
						} else {
						String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
							+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
							+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
							+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
							+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"  +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+  "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "\n";
							outputStream.write(header.getBytes());					    
						}
				}}
				else 
					if(sessionCorpID.equals("WEST"))
					{
					if(billingPersonFlag!=null && billingPersonFlag.equalsIgnoreCase("Yes")){
					if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+"openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+     "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"+"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+  "\n"   ;
						outputStream.write(header.getBytes());					    
					}
					}else{ 
						if(!componentMillitaryShipment.isEmpty()){
							String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
							+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
							+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
							+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
							+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+  "\n";
							outputStream.write(header.getBytes());
							} else {
							String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
								+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
								+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
								+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
								+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"  +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+  "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+  "\n";
								outputStream.write(header.getBytes());					    
							}
					}}
				else if(!checkFieldVisibility && !sessionCorpID.equals("WEST"))
				{
				if(billingPersonFlag!=null && billingPersonFlag.equalsIgnoreCase("Yes")  ){
				if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+"openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"+"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "\n";
						outputStream.write(header.getBytes());					    
					}
				}
				else
				{
					if(!componentMillitaryShipment.isEmpty() ){
						String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
								+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
								+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
								+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
								+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" + "\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "\n";
								outputStream.write(header.getBytes());
								} else {
								String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
									+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
									+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
									+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
									+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"  +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense"  +"\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+    "\n";
									outputStream.write(header.getBytes());					    
								}
				}
				}
				else if(checkFieldVisibility)
				{

				if(billingPersonFlag!=null && billingPersonFlag.equalsIgnoreCase("Yes")  ){
				if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+"openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "Hours"+  "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"+"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Hours"+ "\n";
						outputStream.write(header.getBytes());					    
					}
				}
				else
				{
					if(!componentMillitaryShipment.isEmpty() ){
						String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
								+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
								+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
								+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
								+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" + "\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+  "Hours"+  "\n";
								outputStream.write(header.getBytes());
								} else {
								String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
									+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
									+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
									+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
									+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"  +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense"  +"\t"+ "Project Manager"+"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Hours"+  "\n";
									outputStream.write(header.getBytes());					    
								}
				}
				
				}
				Iterator it = imfEstimate.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					if (row[6] != null) {
						outputStream.write((row[6].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[16] != null) {
						outputStream.write((row[16].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[31] != null) {
						outputStream.write((row[31].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[29] != null) {
						String targetActualValue="";
						targetActualValue = row[29].toString();
						if(targetActualValue.equalsIgnoreCase("T")){
							targetActualValue = "Target";
						}else if(targetActualValue.equalsIgnoreCase("A")){
							targetActualValue = "Actual";
						}else if(targetActualValue.equalsIgnoreCase("C")){
							targetActualValue = "Cancelled";
						}else if(targetActualValue.equalsIgnoreCase("D")){
							targetActualValue = "Dispatching";
						}else if(targetActualValue.equalsIgnoreCase("F")){
							targetActualValue = "Forecasting";
						}else if(targetActualValue.equalsIgnoreCase("P")){
							targetActualValue = "Pending";
						}else if(targetActualValue.equalsIgnoreCase("R")){
							targetActualValue = "Rejected";
						}
						outputStream.write((targetActualValue + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}

					if (row[2] != null) {
						outputStream.write((row[2].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[3] != null) {
						outputStream.write((row[3].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[4] != null) {
						outputStream.write((row[4].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[1] != null) {
						outputStream.write((row[1].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[21] != null) {
						String date1 = "";
						Date date = (Date) row[21];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						date1 = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((date1 + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[22] != null) {
						String date2 = "";
						Date date = (Date) row[22];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						date2 = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((date2 + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[7] != null) {
						outputStream.write((row[7].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[8] != null) {
						outputStream.write((row[8].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[43] != null) {
						outputStream.write((row[43].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[9] != null) {
						outputStream.write((row[9].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[14] != null) {
						outputStream.write((row[14].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[11] != null) {
						outputStream.write((row[11].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[10] != null) {
						outputStream.write((row[10].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[12] != null) {
						outputStream.write((row[12].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[0] != null) {
						outputStream.write((row[0].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[13] != null) {
						outputStream.write((row[13].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[15] != null) {
						outputStream.write((row[15].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[17] != null) {
						outputStream.write((row[17].toString() + "\t").getBytes());

					} else {
						outputStream.write("\t".getBytes());
					}
					if(billingPersonFlag!=null && billingPersonFlag.equalsIgnoreCase("Yes")){
						if (row[50] != null) {
							outputStream.write((row[50].toString() + "\t").getBytes());

						} else {
							outputStream.write("\t".getBytes());
						}
					}
					if (row[19] != null) {
						String str = (String) row[19].toString();
						totWeight = new BigDecimal(str);
						totWeight.setScale(2, BigDecimal.ROUND_HALF_UP);
						outputStream.write((totWeight + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[23] != null) {
						String str = (String) row[23].toString();
						totWeight = new BigDecimal(str);
						totWeight.setScale(2, BigDecimal.ROUND_HALF_UP);
						outputStream.write((totWeight + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[54] != null) {
						outputStream.write((row[54].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[49] != null) {
						outputStream.write((row[49].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[20] != null) {
						String openDate = "";
						Date date = (Date) row[20];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						openDate = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((openDate + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[23] != null) {
						outputStream.write((row[23].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[24] != null) {
						outputStream.write((row[24].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[25] != null) {
						String scanned = "";
						Date date = (Date) row[25];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						scanned = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((scanned + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[26] != null) {
						outputStream.write((row[26].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[27] != null) {
						String rsDate = "";
						Date date = (Date) row[27];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						rsDate = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((rsDate + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[28] != null) {
						outputStream.write((row[28].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[32] != null) {
						outputStream.write((row[32].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[35] != null) {
						outputStream.write(("'"+row[35].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[36] != null) {
						outputStream.write((row[36].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[33] != null) {
						outputStream.write((row[33].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[34] != null) {
						outputStream.write((row[34].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[29] != null) {
						if (row[29].toString().trim().equals("A") || row[29].toString().trim().equals("Actual")) {
							if (row[30] != null) {
								String stsDate = "";
								Date date = (Date) row[30];
								SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
								stsDate = new String(dateformatYYYYMMDD.format(date));
								outputStream.write((stsDate + "\t").getBytes());
								outputStream.write("\t".getBytes());
							} else {
								outputStream.write("\t".getBytes());
								outputStream.write("\t".getBytes());
							}

						} else {
							if (row[29].toString().trim().equals("T") || row[29].toString().trim().equals("Target")) {
								if (row[30] != null) {
									String stsDate = "";
									Date date = (Date) row[30];
									SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
									stsDate = new String(dateformatYYYYMMDD.format(date));
									outputStream.write("\t".getBytes());
									outputStream.write((stsDate + "\t").getBytes());
								} else {
									outputStream.write("\t".getBytes());
									outputStream.write("\t".getBytes());
								} 
							} else {
								outputStream.write("\t".getBytes());
								outputStream.write("\t".getBytes());
						} 
						}
					} else {
						outputStream.write("\t".getBytes());
						outputStream.write("\t".getBytes());
					}
					if(!componentMillitaryShipment.isEmpty()){
						if (row[37] != null) {
							outputStream.write((row[37].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					} else {
//					    outputStream.write("\t".getBytes());
					}
					if("Cbm".equalsIgnoreCase(volumeUnit)){
						if (row[38] != null) {
							outputStream.write((row[38].toString() + "\t").getBytes());
						}else {
							outputStream.write("\t".getBytes());
						}
						if (row[40] != null) {
							outputStream.write((row[40].toString() + "\t").getBytes());
						}else {
							outputStream.write("\t".getBytes());
						}
						
					}else if("Cft".equalsIgnoreCase(volumeUnit)){
						if (row[39] != null) {
							outputStream.write((row[39].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[41] != null) {
							outputStream.write((row[41].toString() + "\t").getBytes());
						}else {
							outputStream.write("\t".getBytes());
						}
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[42] != null) {
						outputStream.write((row[42].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[44] != null) {
						outputStream.write((row[44].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[45] != null) {
						outputStream.write((row[45].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[46] != null) {
						outputStream.write((row[46].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[47] != null) {
						outputStream.write((row[47].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[48] != null) {
						outputStream.write((row[48].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[49] != null) {
						outputStream.write((row[49].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[51] != null) {
						outputStream.write((row[51].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[52] != null) {
						outputStream.write((row[52].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[53] != null) {
						outputStream.write((row[53].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[55] != null) {
						outputStream.write((row[55].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if(row[55] != null && (!(row[55].toString().trim().equals(""))) ){
						List RevenueExpenseAmt = serviceOrderManager.findTotalRevenueExpense(sessionCorpID,row[55].toString().trim());
						if(RevenueExpenseAmt!=null && !RevenueExpenseAmt.isEmpty() && RevenueExpenseAmt.get(0)!=null){
							Object revenueExpense=RevenueExpenseAmt.get(0);
							if (((HibernateDTO)revenueExpense).getActualRevenueSum()!= null){
								outputStream.write(((((HibernateDTO)revenueExpense).getActualRevenueSum().toString())+ "\t").getBytes());
							}else{
								outputStream.write("0\t".getBytes());
							}
							if (((HibernateDTO)revenueExpense).getActualExpenseSum()!= null){
								outputStream.write(((((HibernateDTO)revenueExpense).getActualExpenseSum().toString())+ "\t").getBytes());
							}else{
								outputStream.write("0\t".getBytes());
							}
						}else{
							outputStream.write("0\t".getBytes());
							outputStream.write("0\t".getBytes());
						}
					}else{
						outputStream.write("0\t".getBytes());
						outputStream.write("0\t".getBytes());
					}
					if(checkFieldVisibility)
					{
					if (row[56] != null) {
						outputStream.write((row[56].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					}
					if (row[57] != null) {
						
						
						String stsDate2 = "";
						Date date = (Date) row[57];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						stsDate2 = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((stsDate2 + "\t").getBytes());
					//	outputStream.write("\t".getBytes());
					} else {
						outputStream.write("\t".getBytes());
						//outputStream.write("\t".getBytes());
					} 
					
					
					if (row[58] != null) {
						
					
							String stsDate = "";
							Date date = (Date) row[58];
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
							stsDate = new String(dateformatYYYYMMDD.format(date));
						
							outputStream.write((stsDate + "\t").getBytes());
						//	outputStream.write("\t".getBytes());
						} else {
							outputStream.write("\t".getBytes());
							//outputStream.write("\t".getBytes());
						} 
						
				
					
					
					if (row[59] != null) {
						
						
						String stsDate1 = "";
						Date date = (Date) row[59];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						stsDate1 = new String(dateformatYYYYMMDD.format(date));
						outputStream.write((stsDate1 + "\t").getBytes());
						//outputStream.write("\t".getBytes());
					} else {
						outputStream.write("\t".getBytes());
						//outputStream.write("\t".getBytes());
					} 
					
				
					
					
					if (row[60] != null) {
						
						
						String stsDate4 = "";
						Date date = (Date) row[60];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd/MMM/yy ");
						stsDate4 = new String(dateformatYYYYMMDD.format(date));
							outputStream.write((stsDate4 + "\t").getBytes());
						//outputStream.write("\t".getBytes());
					} else {
						outputStream.write("\t".getBytes());
						//outputStream.write("\t".getBytes());
					}
					
					if (row[61] != null) {
						outputStream.write((row[61].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if(sessionCorpID.equals("WEST"))
						{
					if (row[62] != null) {
						outputStream.write((row[62].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[63] != null) {
						outputStream.write((row[63].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
				}
					if(sessionCorpID.equals("CWMS"))
					{
				if (row[64] != null) {
					outputStream.write((row[64].toString() + "\t").getBytes());
				} else {
					outputStream.write("\t".getBytes());
				}
				
			}
					
					    outputStream.write("\n".getBytes());
				}
				
				
				
				
				
				
			} else {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				//response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				if(!checkFieldVisibility && !sessionCorpID.equals("WEST"))
				{
				if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator " + "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + "  actualWeight   " + "\t" + "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract " +"\t" + " Company Division" +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" + "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator " + "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + "  actualWeight   " + "\t" + "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " + "\t" + " Contract " +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" + "\n";
						outputStream.write(header.getBytes());					    
					}

			}
				else 
					if(sessionCorpID.equals("WEST"))
					{
					if(billingPersonFlag!=null && billingPersonFlag.equalsIgnoreCase("Yes")){
					if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+"openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+     "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + " BillingPerson" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"+"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+  "\n"   ;
						outputStream.write(header.getBytes());					    
					}
					}else{ 
						if(!componentMillitaryShipment.isEmpty()){
							String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
							+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
							+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
							+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
							+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume "+ "\t" + " Contract"+"\t" + " AccountName" +"\t" + " VIP"  +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field"+"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+ "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+  "\n";
							outputStream.write(header.getBytes());
							} else {
							String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
								+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator "+ "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
								+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
								+ "   estimator" + "\t" + " salesMan" + "\t" + "  TicketActualWeight   " + "\t"+"TicketEstimatedWeight"+"\t"+"TicketActualVolume"+"\t"+"TicketEstimatedVolume"+"\t"+ "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
								+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date "+ "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract" + "\t" + " AccountName" +"\t" + " VIP"+"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume"+"\t" + " HSE"  +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" +"\t"+ "Invoice Number" +"\t"+ "Invoice Revenue" +"\t"+ "Invoice Expense" +"\t"+  "Packing/weight Date" +"\t"+ "Recieved At WareHouse Date" +"\t"+ "Left Warehouse Date" +"\t"+ "Confirmation Date " +"\t"+ "PRO#" +"\t"+ "Truck#" +"\t"+ "Truck Description" +"\t"+  "\n";
								outputStream.write(header.getBytes());					    
							}
					}}
			else if(!checkFieldVisibility && !sessionCorpID.equals("WEST"))
			{
				if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator " + "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + "  actualWeight   " + "\t" + "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract " +"\t" + " Company Division" +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" + "\t"+ "Project Manager"+  "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator " + "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + "  actualWeight   " + "\t" + "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " + "\t" + " Contract " +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" + "\t"+ "Project Manager"+  "\n";
						outputStream.write(header.getBytes());					    
					}
			}
		  else if(checkFieldVisibility){
			  if(!componentMillitaryShipment.isEmpty()){
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
					+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator " + "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
					+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
					+ "   estimator" + "\t" + " salesMan" + "\t" + "  actualWeight   " + "\t" + "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
					+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Military Shipment " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " +"\t" + " Contract " +"\t" + " Company Division" +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" + "\t"+ "Project Manager"+ "\t"+ "Hours"+ "\n";
					outputStream.write(header.getBytes());
					} else {
					String header = " shipNumber" + "\t" + " registrationNumber" + "\t" + "Ticket#" + "\t" + "Ticket Status" + "\t" + " firstName" + "\t" + "lastName " + "\t" + " warehouse" + "\t"
						+ " service" + "\t" + " date1" + "\t" + " date2 " + "\t" + "  city    " + "\t" + "  coordinator " + "\t" + " Created By" + "\t" + " zip" + "\t" + " originCountry" + "\t"
						+ " destinationCity" + "\t" + "  destinationCountry" + "\t" + " destinationState" + "\t" + " state" + "\t" + " destinationZip" + "\t"
						+ "   estimator" + "\t" + " salesMan" + "\t" + "  actualWeight   " + "\t" + "openDate   " + "\t" + "    estimatedWeight" + "\t" + "jobMode" + "\t"
						+ "scanned " + "\t" + " reviewStatus" + "\t" + " reviewStatusDate" + "\t" + " payMethod" +  "\t" + " Job Type " + "\t" + " BillToCode "+ "\t" + " BillToName "+"\t" + " Revenue " + "\t" + " CrewRevenue " + "\t" + " Actual Load Date " + "\t" + " Target Load Date " + "\t" + " Actual Gross Volume "+ "\t" + " Actual Net Volume " + "\t" + " Contract " +"\t" + " Company Division"  +"\t" + " Vendor Code" +"\t" + " Vendor Name"+"\t" + " Estimated Volume" +"\t"+ " Instruction Code "+"\t"+ "Instruction Field" + "\t"+ "Project Manager"+  "\t"+ "Hours"+ "\n";
						outputStream.write(header.getBytes());					    
					}	
					
					
				}
		}} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	//first time 
	public String workTicketExtractAll() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	
	public void dynamicFinancialSummary() throws Exception { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long t1 = new Date().getTime();
		if(extractVal == null || (!extractVal.equals("true")) ){  
		StringBuffer jobBuffer = new StringBuffer();
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		} 
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}

		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
		
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
		
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 }
		if (conditionC1.equals("") && conditionC12.equals("") && conditionC13.equals("") && conditionC14.equals("") && conditionC15.equals("") && conditionC16.equals("")
				&& conditionC17.equals("") && conditionC18.equals("") && conditionC19.equals("") && conditionC120.equals("") && conditionC121.equals("")
				&& conditionC3.equals("") && conditionC31.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && conditionC115.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&&payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")) {
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'    AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status in ('CLSD','CNCL') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status not in ('CNCL') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  group by s.shipNumber");
			}

			// query.append("  where  s.corpId='"+sessionCorpID+"'  AND  s.status not in ('CLSD','CNCL')  group by s.shipNumber");
		} else {

			query.append("  where ");

			if (conditionC1.equals("") || conditionC1 == null) {

			} else {
				query.append(" s.job  ");
				isAnd = true;
				if (conditionC1.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			if (conditionC12.equals("") || conditionC12 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}

				if (conditionC12.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;
			}
			if (conditionC13.equals("") || conditionC13 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append(" s.bookingAgentCode");
				}
				if (conditionC13.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (conditionC14.equals("") || conditionC14 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("  s.routing");
				}
				if (conditionC14.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (conditionC15.equals("") || conditionC15 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (conditionC15.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (conditionC16.equals("") || conditionC16 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("  s.packingMode");
				}
				if (conditionC16.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (conditionC17.equals("") || conditionC17 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (conditionC17.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (conditionC18.equals("") || conditionC18 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (conditionC18.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (conditionC19.equals("") || conditionC19 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (conditionC19.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (conditionC120.equals("") || conditionC120 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (conditionC120.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (conditionC121.equals("") || conditionC121 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (conditionC121.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (conditionC3.equals("") || conditionC3 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (conditionC31.equals("") || conditionC31 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}
			if (createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ((postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals("")))) &&(!(postingPeriodEndDates.toString().equals("")))) {
				if (isAnd == true) {
					query.append("  AND ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			
			
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (conditionC115 == null || conditionC115.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (conditionC115.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status  in ('CLSD','CNCL') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CNCL') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' group by s.shipNumber");
			}
			//query.append("   AND s.corpId='"+sessionCorpID+"' AND  s.status not in ('CLSD','CNCL') group by s.shipNumber");			
       } 
       //String queryExtract=extractManager.saveForExtract(query.toString()); 
        String check= getConditionC79();
	    if(check != null && (check.equals("trueeee"))){
		extractQueryFile = new ExtractQueryFile();
		extractQueryFile.setUserId(getRequest().getRemoteUser());
		extractQueryFile.setCreatedon(new Date());
		extractQueryFile.setCreatedby(getRequest().getRemoteUser());
		extractQueryFile.setCorpID(sessionCorpID);
		extractQueryFile.setExtractType("dynamicFinancialSummary");
		extractQueryFile.setPublicPrivateFlag(getPublicPrivateFlag());
		extractQueryFile.setQueryName(getQueryName());
		extractQueryFile.setQueryCondition(query.toString());
		extractQueryFile.setModifiedby(getRequest().getRemoteUser());
		extractQueryFile.setModifiedon(new Date());
		extractManager.save(extractQueryFile);
		String key = "dynamicFinancialSummary Query is saved";
		saveMessage(getText(key));
		setConditionC79("");
		setConditionC80("");
		edit(); 
		} 
		//System.out.println("query in action dynamicFinancialSummary >>>>>>>>>>>>>" + query);
		List financeSummary = serviceOrderManager.financeSummary(query.toString(),sessionCorpID); 
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("DynamicFinancialSummary");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + financeSummary.size() + "\t is extracted");
		String bA_SSCW = new String("");
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");
		BigDecimal cuDivisormar = new BigDecimal(100);
		BigDecimal actualGrossMarginPercentage = new BigDecimal("0");
		BigDecimal dynamicRevenue = new BigDecimal("0");
		BigDecimal dynamicExpense = new BigDecimal("0");
		BigDecimal actualGrossMargin = new BigDecimal("0"); 
		Set set = new HashSet();
		Iterator it = financeSummary.iterator();
		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("REGISTRATION NUMBER\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("JOB STAT\t".getBytes());
		outputStream.write("BILLTOCODE\t".getBytes());
		outputStream.write("BILLTONAME\t".getBytes());
		outputStream.write("BILLGROUP\t".getBytes());
		outputStream.write("ABBRV\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("BOOKING AGENT NAME\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("OA AGENT\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("DA AGENT\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("PKMODE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ESTGROSS\t".getBytes());
		outputStream.write("ESTNET \t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("DYNWEIGHT \t".getBytes());
		outputStream.write("LOADING\t".getBytes());
		outputStream.write("LOAD MTH\t".getBytes());
		outputStream.write("LOAD YR\t".getBytes());
		outputStream.write("DELVA\t".getBytes());
		outputStream.write("SITO\t".getBytes());
		outputStream.write("SITTESTA\t".getBytes());
		outputStream.write("BILLCOMPLETE\t".getBytes());
		outputStream.write("AUDITCOMPLETE\t".getBytes());  
		outputStream.write("SERVICE\t".getBytes());
		//outputStream.write("OA_RECD\t".getBytes());
		//outputStream.write("DA_RECD\t".getBytes()); 
		outputStream.write("DYN_REV\t".getBytes());
		outputStream.write("DYN_EXP\t".getBytes());
		outputStream.write("MARGIN\t".getBytes());
		outputStream.write("MARGIN PER\t".getBytes());
		outputStream.write("ACT.REV\t".getBytes());
		outputStream.write("ACT.DIST-AMT\t".getBytes());
		outputStream.write("ACT.EXP\t".getBytes());
		outputStream.write("COMPANY DIVISION\t".getBytes());
		outputStream.write("REVENUE RECOGNITION\t".getBytes());
		outputStream.write("REVRECMONTH\t".getBytes());
		outputStream.write("LAST SENT TO CL DATE\t".getBytes());
		outputStream.write("NOTES SUBJECT\t".getBytes()); 
		outputStream.write("MLTRYFLAG\t".getBytes()); 
		outputStream.write("DP3\t".getBytes()); 
		outputStream.write("COMPTETIVE\t".getBytes()); 
		outputStream.write("SALESSTATUS\t".getBytes()); 
		outputStream.write("BILLING PARTY PERSON\t".getBytes()); 
		outputStream.write("\r\n".getBytes());
		while (it.hasNext()) {
			Object extracts = it.next();
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				shipNumber = (((HibernateDTO) extracts).getShipNumber().toString());
				set.add(shipNumber);
			} else {

			}
		}
		Iterator its = set.iterator();
		while (its.hasNext()) {
			Object ship = (Object) its.next();

			Iterator itss = financeSummary.iterator();

			while (itss.hasNext()) {
				Object extract = (Object) itss.next();
				if (ship.toString().equalsIgnoreCase(((HibernateDTO) extract).getShipNumber().toString())) {
					if (((HibernateDTO) extract).getCategoryType() != null) {
						if (((HibernateDTO) extract).getCategoryType().toString().equalsIgnoreCase("Origin Agent")) {
							if (((HibernateDTO) extract).getVendorCode() != null) {

								if (((HibernateDTO) extract).getVendorCode().toString().startsWith("0")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("1")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("2")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("3")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("4")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("5")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("6")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("7")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("8")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("9"))
									orignAentCode = "'" + (((HibernateDTO) extract).getVendorCode().toString());
								else {

									orignAentCode = (((HibernateDTO) extract).getVendorCode().toString());
								}

							} else {

							}
						}
						if (((HibernateDTO) extract).getCategoryType().toString().equalsIgnoreCase("Destination Agent")) {
							if (((HibernateDTO) extract).getVendorCode() != null) {
								if (((HibernateDTO) extract).getVendorCode().toString().startsWith("0")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("1")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("2")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("3")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("4")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("5")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("6")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("7")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("8")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("9"))
									destAentCode = "'" + (((HibernateDTO) extract).getVendorCode().toString());
								else {

									destAentCode = (((HibernateDTO) extract).getVendorCode().toString());
								}

							} else {

							}
						} else {

						}

					} else {
					}
				}

			}

			outputStream.write(("'" + (ship.toString() + "\t")).getBytes());
			Iterator itsss = financeSummary.iterator();
			outer: while (itsss.hasNext()) {
				Object extracts = (Object) itsss.next();
				if (ship.toString().equalsIgnoreCase(((HibernateDTO) extracts).getShipNumber().toString())) {
					if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
						outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getContract() != null) {
						outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());
					}

					else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getStatus() != null) {
						outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
					}

					else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBillToCode() != null) {

						if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
							outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
						} else {
							outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBillToName() != null) {
						outputStream.write((((HibernateDTO) extracts).getBillToName().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getBillGroup() != null) {
						outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getAbbreviation() != null && (!(((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))) {
						outputStream.write((((HibernateDTO) extracts).getAbbreviation().toString() + "\t").getBytes());
					}
					else if(((HibernateDTO) extracts).getAbbreviation() == null || ((((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))){
						if (((HibernateDTO) extracts).getBillGroup() != null) {
							outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						} 	
					}
					else { 
						outputStream.write("\t".getBytes());
					} 
					
					if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
						bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();
						if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBookingAgentName() != null) {
						outputStream.write((((HibernateDTO) extracts).getBookingAgentName().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getOriginAgentCode() != null) {

						if (((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginAgent() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {

						if (((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationAgent() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getJob() != null) {
						outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getRouting() != null) {
						outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getMode() != null) {
						outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getPackingMode() != null) {
						outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getCommodity() != null) {
						outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					String firstName = new String("");
					String lastName = new String("");
					if (((HibernateDTO) extracts).getFirstName() != null) {
						firstName = ((HibernateDTO) extracts).getFirstName().toString();

					} else {
						firstName = new String("");
					}
					if (((HibernateDTO) extracts).getLastName() != null) {
						lastName = ((HibernateDTO) extracts).getLastName().toString();

					} else {
						lastName = new String("");
					}
					outputStream.write(( firstName + "\t").getBytes());
					outputStream.write((lastName + "\t").getBytes());
					if (((HibernateDTO) extracts).getCoordinator() != null) {
						outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSalesMan() != null) {
						outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginCity() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationCity() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
					} else {
						String s = "\"" + "\",";
						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getEstimateGrossWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getEstimateGrossWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getEstimatedNetWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getEstimatedNetWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualNetWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getNetWeight() != null && (((BigDecimal)((HibernateDTO) extracts).getNetWeight()).doubleValue()!=0)) {
						outputStream.write((((HibernateDTO) extracts).getNetWeight().toString() + "\t").getBytes());
					} else {
						if (((HibernateDTO) extracts).getNetEstWeight() != null) {
							outputStream.write((((HibernateDTO) extracts).getNetEstWeight().toString() + "\t").getBytes());
						} else { 
						outputStream.write("\t".getBytes());
						}
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
						StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write((month.toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
						StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

						outputStream.write((year.toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					// for delva==========================================================================
					if (((HibernateDTO) extracts).getDeliveryA() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSitOriginA() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSitDestinationA() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					// for  job_cc====================================================================
					//outputStream.write("\t".getBytes());
					if (((HibernateDTO) extracts).getBillComplete() != null) {

						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getAuditComplete()!= null) {

						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
						outputStream.write((date.toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}  
					if (((HibernateDTO) extracts).getServiceType() != null) {
						outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}  
					if ((((HibernateDTO) extracts).getDYN_REV() != null)) {
						outputStream.write((((HibernateDTO) extracts).getDYN_REV().toString() + "\t").getBytes());
					} 
					else {
						outputStream.write("0\t".getBytes());
					}

					if ((((HibernateDTO) extracts).getDYN_EXP() != null)) {
						outputStream.write((((HibernateDTO) extracts).getDYN_EXP().toString() + "\t").getBytes());
					} 
					else {

						outputStream.write("0\t".getBytes());
					}
					if ((((HibernateDTO) extracts).getDYN_REV() != null) && (((HibernateDTO) extracts).getDYN_EXP() != null)) {
						dynamicRevenue = (BigDecimal) ((HibernateDTO) extracts).getDYN_REV();
						dynamicExpense = (BigDecimal) ((HibernateDTO) extracts).getDYN_EXP();
						actualGrossMargin = dynamicRevenue.subtract(dynamicExpense);
						outputStream.write((actualGrossMargin.toString() + "\t").getBytes());
					} else {

						outputStream.write("0\t".getBytes());
					}
					if ((((HibernateDTO) extracts).getDYN_REV() != null) && (((HibernateDTO) extracts).getDYN_EXP() != null)) {
						actualGrossMarginPercentage = new BigDecimal("0");
						dynamicRevenue = (BigDecimal) ((HibernateDTO) extracts).getDYN_REV();
						dynamicExpense = (BigDecimal) ((HibernateDTO) extracts).getDYN_EXP();
						actualGrossMargin = dynamicRevenue.subtract(dynamicExpense);
						if (dynamicRevenue.toString().equals("0") || dynamicRevenue.toString().equals("0.0") || dynamicRevenue.toString().equals("0.00")) {
							actualGrossMarginPercentage = new BigDecimal("0");
						} else {
							try {
								actualGrossMarginPercentage = actualGrossMargin.multiply(cuDivisormar).divide(dynamicRevenue, 2);
							} catch (Exception e) {
								e.printStackTrace();
						
							}
						}
						outputStream.write((actualGrossMarginPercentage.toString() + "\t").getBytes());
					} else {
						outputStream.write("0\t".getBytes());

					}
					if (((HibernateDTO) extracts).getActualRevenueSum() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualRevenueSum().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSumDistributionAmount() != null) {
						outputStream.write((((HibernateDTO) extracts).getSumDistributionAmount().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualExpenseSum() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualExpenseSum().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getCompanyDivision() != null) {
						outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getRevenueRecognition() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getRevenueRecognition() != null) {
						SimpleDateFormat format = new SimpleDateFormat("MM");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSendActualToClient() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSendActualToClient()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getNotesSubject() != null) {
						outputStream.write((((HibernateDTO) extracts).getNotesSubject().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
						outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDp3() != null) {
						outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getComptetive() != null) {
						outputStream.write((((HibernateDTO) extracts).getComptetive().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSalesStatus() != null) {
						outputStream.write((((HibernateDTO) extracts).getSalesStatus().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getPersonBilling() != null) {
						outputStream.write((((HibernateDTO) extracts).getPersonBilling().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					
					break outer;
				}

				else {

				}
			}
			outputStream.write("\r\n".getBytes());
			Long t2 = new Date().getTime();
			timeDiff = (t2 - t1);
			this.setTimeDiff(timeDiff);
			getRequest().setAttribute("timeDiff", timeDiff);
		}

		//System.out.println("\n\n\n\n timeDiff" + timeDiff);
		
		
		
		}
		else{
			extractQueryFile = extractManager.get(id);
			String query = extractQueryFile.getQueryCondition();
			List financeSummary = serviceOrderManager.financeSummary(query, sessionCorpID); 
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("DynamicFinancialSummary");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + financeSummary.size() + "\t is extracted");
		String bA_SSCW = new String("");
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");
		BigDecimal cuDivisormar = new BigDecimal(100);
		BigDecimal actualGrossMarginPercentage = new BigDecimal("0");
		BigDecimal dynamicRevenue = new BigDecimal("0");
		BigDecimal dynamicExpense = new BigDecimal("0");
		BigDecimal actualGrossMargin = new BigDecimal("0"); 
		Set set = new HashSet();
		Iterator it = financeSummary.iterator();
		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("REGISTRATION NUMBER\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("JOB STAT\t".getBytes());
		outputStream.write("BILLTOCODE\t".getBytes());
		outputStream.write("BILLTONAME\t".getBytes());
		outputStream.write("BILLGROUP\t".getBytes());
		outputStream.write("ABBRV\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("BOOKING AGENT NAME\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("OA AGENT\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("DA AGENT\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("PKMODE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ESTGROSS\t".getBytes());
		outputStream.write("ESTNET \t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("DYNWEIGHT \t".getBytes());
		outputStream.write("LOADING\t".getBytes());
		outputStream.write("LOAD MTH\t".getBytes());
		outputStream.write("LOAD YR\t".getBytes());
		outputStream.write("DELVA\t".getBytes());
		outputStream.write("SITO\t".getBytes());
		outputStream.write("SITTESTA\t".getBytes());
		outputStream.write("BILLCOMPLETE\t".getBytes());
		outputStream.write("AUDITCOMPLETE\t".getBytes());  
		outputStream.write("SERVICE\t".getBytes());
		//outputStream.write("OA_RECD\t".getBytes());
		//outputStream.write("DA_RECD\t".getBytes()); 
		outputStream.write("DYN_REV\t".getBytes());
		outputStream.write("DYN_EXP\t".getBytes());
		outputStream.write("MARGIN\t".getBytes());
		outputStream.write("MARGIN PER\t".getBytes());
		outputStream.write("ACT.REV\t".getBytes());
		outputStream.write("ACT.DIST-AMT\t".getBytes());
		outputStream.write("ACT.EXP\t".getBytes());
		outputStream.write("COMPANY DIVISION\t".getBytes());
		outputStream.write("REVENUE RECOGNITION\t".getBytes());
		outputStream.write("REVRECMONTH\t".getBytes());
		outputStream.write("LAST SENT TO CL DATE\t".getBytes());
		outputStream.write("NOTES SUBJECT\t".getBytes()); 
		outputStream.write("MLTRYFLAG\t".getBytes()); 
		outputStream.write("DP3\t".getBytes());
		outputStream.write("COMPTETIVE\t".getBytes()); 
		outputStream.write("SALESSTATUS\t".getBytes()); 
		outputStream.write("BILLING PARTY PERSON\t".getBytes());
		outputStream.write("\r\n".getBytes());
		while (it.hasNext()) {
			Object extracts = it.next();
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				shipNumber = (((HibernateDTO) extracts).getShipNumber().toString());
				set.add(shipNumber);
			} else {

			}
		}
		Iterator its = set.iterator();
		while (its.hasNext()) {
			Object ship = (Object) its.next();

			Iterator itss = financeSummary.iterator();

			while (itss.hasNext()) {
				Object extract = (Object) itss.next();
				if (ship.toString().equalsIgnoreCase(((HibernateDTO) extract).getShipNumber().toString())) {
					if (((HibernateDTO) extract).getCategoryType() != null) {
						if (((HibernateDTO) extract).getCategoryType().toString().equalsIgnoreCase("Origin Agent")) {
							if (((HibernateDTO) extract).getVendorCode() != null) {

								if (((HibernateDTO) extract).getVendorCode().toString().startsWith("0")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("1")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("2")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("3")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("4")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("5")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("6")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("7")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("8")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("9"))
									orignAentCode = "'" + (((HibernateDTO) extract).getVendorCode().toString());
								else {

									orignAentCode = (((HibernateDTO) extract).getVendorCode().toString());
								}

							} else {

							}
						}
						if (((HibernateDTO) extract).getCategoryType().toString().equalsIgnoreCase("Destination Agent")) {
							if (((HibernateDTO) extract).getVendorCode() != null) {
								if (((HibernateDTO) extract).getVendorCode().toString().startsWith("0")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("1")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("2")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("3")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("4")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("5")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("6")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("7")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("8")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("9"))
									destAentCode = "'" + (((HibernateDTO) extract).getVendorCode().toString());
								else {

									destAentCode = (((HibernateDTO) extract).getVendorCode().toString());
								}

							} else {

							}
						} else {

						}

					} else {
					}
				}

			}

			outputStream.write(("'" + (ship.toString() + "\t")).getBytes());
			Iterator itsss = financeSummary.iterator();
			outer: while (itsss.hasNext()) {
				Object extracts = (Object) itsss.next();
				if (ship.toString().equalsIgnoreCase(((HibernateDTO) extracts).getShipNumber().toString())) {
					if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
						outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getContract() != null) {
						outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());
					}

					else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getStatus() != null) {
						outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
					}

					else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBillToCode() != null) {

						if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
							outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
						} else {
							outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBillToName() != null) {
						outputStream.write((((HibernateDTO) extracts).getBillToName().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getBillGroup() != null) {
						outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getAbbreviation() != null && (!(((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))) {
						outputStream.write((((HibernateDTO) extracts).getAbbreviation().toString() + "\t").getBytes());
					}
					else if(((HibernateDTO) extracts).getAbbreviation() == null || ((((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))){
						if (((HibernateDTO) extracts).getBillGroup() != null) {
							outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
						} else { 
							outputStream.write("\t".getBytes());
						} 	
					}
					else { 
						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
						bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();
						if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBookingAgentName() != null) {
						outputStream.write((((HibernateDTO) extracts).getBookingAgentName().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getOriginAgentCode() != null) {

						if (((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginAgent() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {

						if (((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationAgent() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getJob() != null) {
						outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getRouting() != null) {
						outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getMode() != null) {
						outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getPackingMode() != null) {
						outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getCommodity() != null) {
						outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					String firstName = new String("");
					String lastName = new String("");
					if (((HibernateDTO) extracts).getFirstName() != null) {
						firstName = ((HibernateDTO) extracts).getFirstName().toString();

					} else {
						firstName = new String("");
					}
					if (((HibernateDTO) extracts).getLastName() != null) {
						lastName = ((HibernateDTO) extracts).getLastName().toString();

					} else {
						lastName = new String("");
					}
					outputStream.write(( firstName + "\t").getBytes());
					outputStream.write((lastName + "\t").getBytes());
					if (((HibernateDTO) extracts).getCoordinator() != null) {
						outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSalesMan() != null) {
						outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginCity() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationCity() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
					} else {
						String s = "\"" + "\",";
						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getEstimateGrossWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getEstimateGrossWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getEstimatedNetWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getEstimatedNetWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualNetWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getNetWeight() != null && (((BigDecimal)((HibernateDTO) extracts).getNetWeight()).doubleValue()!=0)) {
						outputStream.write((((HibernateDTO) extracts).getNetWeight().toString() + "\t").getBytes());
					} else {
						if (((HibernateDTO) extracts).getNetEstWeight() != null) {
							outputStream.write((((HibernateDTO) extracts).getNetEstWeight().toString() + "\t").getBytes());
						} else { 
						outputStream.write("\t".getBytes());
						}
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
						StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write((month.toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
						StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

						outputStream.write((year.toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					// for delva==========================================================================
					if (((HibernateDTO) extracts).getDeliveryA() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSitOriginA() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSitDestinationA() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					// for  job_cc====================================================================
					//outputStream.write("\t".getBytes());
					if (((HibernateDTO) extracts).getBillComplete() != null) {

						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getAuditComplete()!= null) {

						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
						outputStream.write((date.toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					} 
					if (((HibernateDTO) extracts).getServiceType() != null) {
						outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					
					if ((((HibernateDTO) extracts).getDYN_REV() != null)) {
						outputStream.write((((HibernateDTO) extracts).getDYN_REV().toString() + "\t").getBytes());
					} 
					else {
						outputStream.write("0\t".getBytes());
					}

					if ((((HibernateDTO) extracts).getDYN_EXP() != null)) {
						outputStream.write((((HibernateDTO) extracts).getDYN_EXP().toString() + "\t").getBytes());
					} 
					else {

						outputStream.write("0\t".getBytes());
					}
					if ((((HibernateDTO) extracts).getDYN_REV() != null) && (((HibernateDTO) extracts).getDYN_EXP() != null)) {
						dynamicRevenue = (BigDecimal) ((HibernateDTO) extracts).getDYN_REV();
						dynamicExpense = (BigDecimal) ((HibernateDTO) extracts).getDYN_EXP();
						actualGrossMargin = dynamicRevenue.subtract(dynamicExpense);
						outputStream.write((actualGrossMargin.toString() + "\t").getBytes());
					} else {

						outputStream.write("0\t".getBytes());
					}
					if ((((HibernateDTO) extracts).getDYN_REV() != null) && (((HibernateDTO) extracts).getDYN_EXP() != null)) {
						actualGrossMarginPercentage = new BigDecimal("0");
						dynamicRevenue = (BigDecimal) ((HibernateDTO) extracts).getDYN_REV();
						dynamicExpense = (BigDecimal) ((HibernateDTO) extracts).getDYN_EXP();
						actualGrossMargin = dynamicRevenue.subtract(dynamicExpense);
						if (dynamicRevenue.toString().equals("0") || dynamicRevenue.toString().equals("0.0") || dynamicRevenue.toString().equals("0.00")) {
							actualGrossMarginPercentage = new BigDecimal("0");
						} else {
							try {
								actualGrossMarginPercentage = actualGrossMargin.multiply(cuDivisormar).divide(dynamicRevenue, 2);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						outputStream.write((actualGrossMarginPercentage.toString() + "\t").getBytes());
					} else {
						outputStream.write("0\t".getBytes());

					}
					if (((HibernateDTO) extracts).getActualRevenueSum() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualRevenueSum().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSumDistributionAmount() != null) {
						outputStream.write((((HibernateDTO) extracts).getSumDistributionAmount().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualExpenseSum() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualExpenseSum().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getCompanyDivision() != null) {
						outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getRevenueRecognition() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getRevenueRecognition() != null) {
						SimpleDateFormat format = new SimpleDateFormat("MM");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSendActualToClient() != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSendActualToClient()));
						outputStream.write((date.toString() + "\t").getBytes());

					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getNotesSubject() != null) {
						outputStream.write((((HibernateDTO) extracts).getNotesSubject().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
						outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDp3() != null) {
						outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getComptetive() != null) {
						outputStream.write((((HibernateDTO) extracts).getComptetive().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getSalesStatus() != null) {
						outputStream.write((((HibernateDTO) extracts).getSalesStatus().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					
					if (((HibernateDTO) extracts).getPersonBilling() != null) {
						outputStream.write((((HibernateDTO) extracts).getPersonBilling().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					
					
					break outer;
				}

				else {

				}
			}
			outputStream.write("\r\n".getBytes());
			Long t2 = new Date().getTime();
			timeDiff = (t2 - t1);
			this.setTimeDiff(timeDiff);
			getRequest().setAttribute("timeDiff", timeDiff);
		}

		//System.out.println("\n\n\n\n timeDiff" + timeDiff);
		
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	public String PartnerExtractEdit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
		companyDivisionFlag = companyDivisionManager.findCompanyDivFlag(sessionCorpID).get(0).toString();
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString(); 
		companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	
	public String utsiPartnerExtractEdit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		user = userManager.getUserByUsername(getRequest().getRemoteUser()); 
		String parentAgent= user.getParentAgent();
		qltyServeyResMap = partnerPrivateManager.getPartnerPrivateDescLinkMap(user.getParentAgent(), sessionCorpID);
//		childList = partnerPublicManager.getPartnerChildList(sessionCorpID, user.getParentAgent());
		childMap = partnerPublicManager.getPartnerChildMap(sessionCorpID, user.getId());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String globalPartnerExtractEdit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		user = userManager.getUserByUsername(getRequest().getRemoteUser()); 
		String parentAgent= user.getParentAgent();
		qltyServeyResMap = partnerPrivateManager.getPartnerPrivateDescLinkMap(user.getParentAgent(), sessionCorpID);
//		childList = partnerPublicManager.getPartnerChildList(sessionCorpID, user.getParentAgent());
		childMap = partnerPublicManager.getPartnerChildMap(sessionCorpID, user.getId());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String globalPartnerExtract()throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String reportType = getReportType();
		String childParents = getChildParentIdList();
		List fileReport = new ArrayList();
		String filename = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try{
			String permKey = sessionCorpID +"-"+"component.accPortal.nationality";
			checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			}catch(Exception e){
				e.printStackTrace();
			}
		String[] columnName = {};
		String[] conditionalColumn={};
		if("FLATFILE".equalsIgnoreCase(reportType)){
			StringBuilder fdate = new StringBuilder(format.format(getFlat_Report_from()));
			String flatFromDt = fdate.toString();
			StringBuilder tdate = new StringBuilder(format.format(getFlat_Report_to()));
			String flatToDt = tdate.toString();
			boolean qualitySurvey = customerFileManager.getQualitySurvey(sessionCorpID);
			columnName = new String[]{"Service Order","Status","Account","Company","Move Initiator","Employee","Transport Type","From To","Origin City","Destination City"
					,"Authorization Received","Survey Date","Packing Date","Loading Date","Delivery Date","Invoice Date","Authorized Volume CBM"
					,"Estimated Volume CBM","Shipped Volume CBM","Distance km","Total Amount Invoiced EUR","Door to Door EUR","All Other additional Charges"
					,"Assignment Type"};
			if(checkFieldVisibility)
			{
				conditionalColumn=new String[]{"Recommendation","Pre move Services Rating","Origin Services Rating","Destination Services Rating","Over All Rating","Complaints","Nationality"};
			}
			else
			{conditionalColumn=new String[]{"Recommendation","Pre move Services Rating","Origin Services Rating","Destination Services Rating","Over All Rating","Complaints"};
			}
			String[] modifiedColumnName = new String[columnName.length];
			if(qualitySurvey){
				int len = columnName.length;
				modifiedColumnName = new String[columnName.length + conditionalColumn.length];
				System.arraycopy(columnName, 0, modifiedColumnName, 0, columnName.length);
				System.arraycopy(conditionalColumn, 0, modifiedColumnName, len, conditionalColumn.length);
			}else{
				modifiedColumnName = columnName;
			}
			fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,flatFromDt,flatToDt,addChargesType,checkFieldVisibility);
			ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "FlatFileExtract", modifiedColumnName, fileReport, "FlatFile_Sheet", "@");
		}else if("CLAIMFILE".equalsIgnoreCase(reportType)) {
			StringBuilder fdate = new StringBuilder(format.format(getClaim_Report_from()));
			StringBuilder tdate = new StringBuilder(format.format(getClaim_Report_to()));
			
			String claimFromDt = fdate.toString();
			String claimToDt = tdate.toString();
			fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,claimFromDt,claimToDt,addChargesType,checkFieldVisibility);
			getClaimByResponse(reportType,fileReport);
		}else if("ADDITIONALCHARGES".equalsIgnoreCase(reportType)){
			/*columnName = new String[]{"Transferee","Customer File","Service Order","Cost","Description","Currency","Amount","Total Amount"};
			StringBuilder fdate = new StringBuilder(format.format(getAdditionalCharges_from()));
			String flatFromDt = fdate.toString();
			StringBuilder tdate = new StringBuilder(format.format(getAdditionalCharges_to()));
			String flatToDt = tdate.toString();*/
			if("accountlineInvoice".equalsIgnoreCase(addChargesType)){
				columnName = new String[]{"Transferee","Customer File","Service Order","Cost","Description","Currency","Amount","Total Amount"};
				StringBuilder fdate = new StringBuilder(format.format(getAdditionalCharges_from()));
				String flatFromDt = fdate.toString();
				StringBuilder tdate = new StringBuilder(format.format(getAdditionalCharges_to()));
				String flatToDt = tdate.toString();
				fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,flatFromDt,flatToDt,addChargesType,checkFieldVisibility);
				ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "AdditionalChargesExtract", columnName, fileReport, "AccountlineInvoice_Sheet", "seprator");
			}else{
				columnName = new String[]{"Transferee","Customer File","Service Order","Cost","Description","Currency","Amount","Total Amount"};
				StringBuilder fdate = new StringBuilder(format.format(getCustomerFileInitiation_from()));
				String flatFromDt = fdate.toString();
				StringBuilder tdate = new StringBuilder(format.format(getCustomerFileInitiation_to()));
				String flatToDt = tdate.toString();
				fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,flatFromDt,flatToDt,addChargesType,checkFieldVisibility);
				ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "AdditionalChargesExtract", columnName, fileReport, "CustomerFileInitiation_Sheet", "seprator");
			}
				
			/*fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,flatFromDt,flatToDt,addChargesType);
			ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "AdditionalChargesExtract", columnName, fileReport, "AdditionalCharges_Sheet", ",");*/
		}else if("invoiceFile".equalsIgnoreCase(reportType)){
			StringBuilder fdate = new StringBuilder(format.format(getInvoice_Report_from()));
			String InvoiceFromDt = fdate.toString();
			StringBuilder tdate = new StringBuilder(format.format(getInvoice_Report_to()));
			String InvoiceToDt = tdate.toString();
			columnName = new String[]{"Name","Origin Country","Destination Country","Charge Code","Invoice#","Invoice Date","Invoice Amount per line excluding VAT","Invoice VAT amount per Line","VAT%","Invoice amount per line including VAT","Invoice currency"};
			fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,InvoiceFromDt,InvoiceToDt,addChargesType,checkFieldVisibility);
			ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "FlatFileExtract", columnName, fileReport, "PrepaidServices_Sheet", "seprator");
		}else{
			columnName = new String[]{"Transferee","Initiated","Move","Job","Status","#Invoices"};
			fileReport = customerFileManager.getAdditionalChargesExtraction(reportType,childParents,"Prepaid services",sessionCorpID);
			ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "PrepaidServicesExtract", columnName, fileReport, "PrepaidServices_Sheet", "seprator");
		}
		
		
	//	Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + filename);
	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String utsiPartnerExtract()throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String reportType = getReportType();
		String childParents = getChildParentIdList();
		List fileReport = new ArrayList();
		String filename = null;
//		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		//uploadDir = uploadDir.replace(uploadDir, "/" + "extract" + "/");
//		File dirPath = new File(uploadDir);
//		if (!dirPath.exists()) {
//			dirPath.mkdirs();
//		}
		try{
			String permKey = sessionCorpID +"-"+"component.accPortal.nationality";
			checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			}catch(Exception e){
				e.printStackTrace();
			}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		if("FLATFILE".equalsIgnoreCase(reportType)){
			StringBuilder fdate = new StringBuilder(format.format(getFlat_Report_from()));
			String flatFromDt = fdate.toString();
			StringBuilder tdate = new StringBuilder(format.format(getFlat_Report_to()));
			String flatToDt = tdate.toString();
			
			fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,flatFromDt,flatToDt, addChargesType,checkFieldVisibility);
//			filename="d:/"+reportType+"_Extraction.xls" ;
//			filename = uploadDir+reportType+"_Extraction.xls";
//			getFlatfileExtraction(filename,fileReport);
			getFlatReportByResponse(reportType, fileReport);
		}else if("CLAIMFILE".equalsIgnoreCase(reportType)) {
			StringBuilder fdate = new StringBuilder(format.format(getClaim_Report_from()));
			StringBuilder tdate = new StringBuilder(format.format(getClaim_Report_to()));
			
			String claimFromDt = fdate.toString();
			String claimToDt = tdate.toString();
			fileReport = customerFileManager.getFlatFileExtraction(reportType,childParents,sessionCorpID,claimFromDt,claimToDt, addChargesType,checkFieldVisibility);
//			filename="d:/"+reportType+"_Extraction.xls" ;
//			filename = uploadDir+reportType+"_Extraction.xls";
//			getClaimfileExtraction(filename,fileReport);
			getClaimByResponse(reportType,fileReport);
		}
		
//		Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + filename);

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public void getClaimByResponse(String reportType,List queryResultList) throws Exception {
		HSSFWorkbook workBook = getClaimfileExtraction(queryResultList);
		/*String header=new String();
		File file1 = new File("FlatFilePartnerExtract");*/
		String fileName = "PartnerExtract.xls";
		HttpServletResponse response = getResponse();
		ServletOutputStream outputStream = response.getOutputStream();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		workBook.write(outputStream);
		outputStream.close();
		/*String header=new String();
		File file1 = new File("PartnerExtract");
		
		HttpServletResponse response = getResponse();
		ServletOutputStream outputStream = response.getOutputStream();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		
		outputStream.write(header.getBytes());

		Iterator it = queryResultList.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();
			
			if (((ExtractHibernateDTO) extracts).getClientName_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getClientName_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 

			if (((ExtractHibernateDTO) extracts).getAccount_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getAccount_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getServiceOrder_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getServiceOrder_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getFromTo_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getFromTo_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getHandleByInsurer_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getHandleByInsurer_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getType_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getType_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			
			if (((ExtractHibernateDTO) extracts).getCause_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getCause_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getCauseBy_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getCauseBy_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			
			if (((ExtractHibernateDTO) extracts).getDateReceived_Claim() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((ExtractHibernateDTO) extracts).getDateReceived_Claim()));
				outputStream.write((date + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getClaimAmt_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getClaimAmt_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getClaimCurrency_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getClaimCurrency_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getPaidAmt_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getPaidAmt_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getPaidCurrency_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getPaidCurrency_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getClaimStatus_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getClaimStatus_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			
			if (((ExtractHibernateDTO) extracts).getStatusDt_Claim() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((ExtractHibernateDTO) extracts).getStatusDt_Claim()));
				outputStream.write((date + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			if (((ExtractHibernateDTO) extracts).getRemark_Claim() != null) {
				outputStream.write((((ExtractHibernateDTO) extracts).getRemark_Claim().toString() + "\t").getBytes());
			} 
			else {
				outputStream.write("\t".getBytes());
			} 
			outputStream.write("\r\n".getBytes());
		}*/
		
	}
	
	public void  partnerShipmentExtract() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		//Map userDataSecurityMap = new LinkedHashMap();
		Map<String, LinkedHashMap> userDataSecuritySetMap = new LinkedHashMap<String, LinkedHashMap>();
		String key = new String("");
		String samekey = new String("");
		StringBuffer allFilterValues=new StringBuffer();
		StringBuffer securityQueryBuffer=new StringBuffer("  ");
		String securityQuery=new String ();
		String dataSetName="";
		//StringBuffer allFilterValues1=new StringBuffer();
		List userDataSecurityList=serviceOrderManager.getUserDateSecurity(user.getId(), sessionCorpID);
		Iterator userDataSecurityIterator = userDataSecurityList.iterator();
		while (userDataSecurityIterator.hasNext()) {
			Object DataSecurityObject = userDataSecurityIterator.next();
			String tableName = (String) ((DTOUserDataSecurity) DataSecurityObject).getTableName();
			String fieldName = (String) ((DTOUserDataSecurity) DataSecurityObject).getFieldName();
			String filterValues = (String) ((DTOUserDataSecurity) DataSecurityObject).getFilterValues();
			dataSetName=(String) ((DTOUserDataSecurity) DataSecurityObject).getDataSetName();
			if (dataSetName.startsWith("DATA_SECURITY_SET_AGENT_")){
				dataSetName="DATA_SECURITY_SET_AGENT_";
			}
			else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_RLOVENDORCODE_"))
			{
				dataSetName="DATA_SECURITY_SET_SO_RLOVENDORCODE_";	
			}
			else if (dataSetName.startsWith("DATA_SECURITY_SET_SO_JOB_"))
			{
				dataSetName="DATA_SECURITY_SET_SO_JOB_";	
			}
			else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOCODE_"))
			{
				dataSetName="DATA_SECURITY_SET_SO_BILLTOCODE_";
			}
			else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOVENDORCODE_"))
			{
				dataSetName="DATA_SECURITY_SET_SO_BILLTOVENDORCODE_";
			}
			if (!(dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOCODE_"))) {
			if(userDataSecuritySetMap.containsKey(dataSetName)){ 
				LinkedHashMap userDataSecurityMap=userDataSecuritySetMap.get(dataSetName);
				key = tableName.trim().charAt(0)+"." +fieldName.trim(); 
			if (key.equals(samekey)) {
				allFilterValues=allFilterValues.append("'");
				allFilterValues=allFilterValues.append((String)(filterValues));
				allFilterValues=allFilterValues.append("',"); 
			} else if (!key.equals(samekey)) {
				allFilterValues= new StringBuffer();
				allFilterValues=allFilterValues.append("'");
				allFilterValues=allFilterValues.append((String)(filterValues));
				allFilterValues=allFilterValues.append("',"); 
			} 
			samekey = tableName.trim().charAt(0)+"." +fieldName.trim(); 
			try { 
				String userDataSecurityMapValue = allFilterValues.toString() ; 
				userDataSecurityMap.put(key, userDataSecurityMapValue); 
				userDataSecuritySetMap.put(dataSetName, userDataSecurityMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			LinkedHashMap userDataSecurityMap = new LinkedHashMap(); 
			key = tableName.trim().charAt(0)+"." +fieldName.trim(); 
		if (key.equals(samekey)) {
			allFilterValues=allFilterValues.append("'");
			allFilterValues=allFilterValues.append((String)(filterValues));
			allFilterValues=allFilterValues.append("',"); 
		} else if (!key.equals(samekey)) {
			allFilterValues= new StringBuffer();
			allFilterValues=allFilterValues.append("'");
			allFilterValues=allFilterValues.append((String)(filterValues));
			allFilterValues=allFilterValues.append("',"); 
		} 
		samekey = tableName.trim().charAt(0)+"." +fieldName.trim(); 
		try { 
			String userDataSecurityMapValue = allFilterValues.toString() ; 
			userDataSecurityMap.put(key, userDataSecurityMapValue);
			userDataSecuritySetMap.put(dataSetName, userDataSecurityMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		}
		}else{

			Map filterMap=user.getFilterMap(); 
			if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
				//String partnerCodePopup =""; 
				StringBuffer allFilterValues1=new StringBuffer();
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							//String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    			allFilterValues1=allFilterValues1.append("'");
	    			allFilterValues1=allFilterValues1.append((String)(code));
	    			allFilterValues1=allFilterValues1.append("',"); 
	    		}
	    	} 
	    	key ="s.billToCode";
	    	String userDataSecurityMapValue = allFilterValues1.toString() ;
	    	LinkedHashMap userDataSecurityMap = new LinkedHashMap(); 
	    	userDataSecurityMap.put(key, userDataSecurityMapValue);
	    	userDataSecuritySetMap.put(dataSetName, userDataSecurityMap);
	    	}
		
		}
		}
		/*if(dataSetName.startsWith("DATA_SECURITY_SET_AGENT_") || dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOVENDORCODE_"))
		{
			securityQueryBuffer=securityQueryBuffer.append("(");
		}*/
		Iterator globelmapIterator = userDataSecuritySetMap.entrySet().iterator();
		    while (globelmapIterator.hasNext()) {
		    Map.Entry globelEntry = (Map.Entry) globelmapIterator.next();
		    securityQueryBuffer=securityQueryBuffer.append(" and (");
		    LinkedHashMap userDataSecurityMap = (LinkedHashMap) globelEntry.getValue(); 
		    Iterator mapIterator = userDataSecurityMap.entrySet().iterator();
		    while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			String filtertable = (String) entry.getKey();
			String filterValue = (String) entry.getValue(); 
			if (!filterValue.equals("")) {
				filterValue = filterValue.substring(0, filterValue.length() - 1);
			} else if (filterValue.equals("")) {
				filterValue = new String("''");
			}
			/*if(dataSetName.startsWith("DATA_SECURITY_SET_AGENT_") || dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOVENDORCODE_"))
			{*/
				//securityQueryBuffer=securityQueryBuffer.append("(");
				if(mapIterator.hasNext()){
					securityQueryBuffer=securityQueryBuffer.append("  "+filtertable +" in  ("+filterValue+ ") or");
					}
					else if(!(mapIterator.hasNext())){
						securityQueryBuffer=securityQueryBuffer.append("  "+filtertable +" in  ("+filterValue+ ") ");	
					}
				if(!(mapIterator.hasNext())){
					securityQueryBuffer=securityQueryBuffer.append(")");
				}
			/*} 
			else {
				if(mapIterator.hasNext()){
				securityQueryBuffer=securityQueryBuffer.append("  "+filtertable +" in  ("+filterValue+ ") and");
				}
				else if(!(mapIterator.hasNext())){
					securityQueryBuffer=securityQueryBuffer.append("  "+filtertable +" in  ("+filterValue+ ") ");	
				}
			}*/
		}
		}
		if(user!=null && user.getUserType()!=null && user.getUserType().trim().equalsIgnoreCase("AGENT")){
		    	securityQueryBuffer=securityQueryBuffer.append(" and s.networkSO is false  ");
        }
		securityQuery=securityQueryBuffer.toString();
		
		System.out.println("\n\n\n\n\n\n securityQuery"+securityQuery);
		//securityQuery=securityQueryBuffer.substring(0, securityQueryBuffer.lastIndexOf("and"));
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer(); 
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(); 
		} 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
		
		
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayJobType.length; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		
		
		if (conditionC14.equals("") && conditionC15.equals("") && conditionC16.equals("")
			&& conditionC17.equals("") && conditionC120.equals("") && conditionC121.equals("")
			&& conditionC21.equals("") && conditionC22.equals("") && conditionC211.equals("") 
			&& conditionC222.equals("")  && conditionC233.equals("") && conditionC244.equals("") 
			&& conditionC200.equals("") && conditionC201.equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where    s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null "+securityQuery+ "  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where   s.status in ('CLSD','CNCL') AND s.status is not null  "+securityQuery+ " group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where    s.status not in ('CNCL') AND s.status is not null  "+securityQuery+ " group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where   "+securityQuery+ "  group by s.shipNumber ");
			} 
		} else { 
			query.append("  where  ");  
			
			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				query.append(" s.job ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}
			}
			if (conditionC14.equals("") || conditionC14 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (conditionC14.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (conditionC15.equals("") || conditionC15 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (conditionC15.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (conditionC16.equals("") || conditionC16 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (conditionC16.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (conditionC17.equals("") || conditionC17 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (conditionC17.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			} 
			if (conditionC120.equals("") || conditionC120 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (conditionC120.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (conditionC121.equals("") || conditionC121 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (conditionC121.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (conditionC21.equals("") || conditionC21 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			} 
			

			if (conditionC22.equals("") || conditionC22 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (conditionC211.equals("") || conditionC211 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (conditionC222.equals("") || conditionC222 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (conditionC233.equals("") || conditionC233 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (conditionC244.equals("") || conditionC244 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (conditionC200.equals("") || conditionC200 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (conditionC201.equals("") || conditionC201 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					
				}
				isAnd = true;
			} 
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("    AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null "+securityQuery+ "  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   AND  s.status  in ('CLSD','CNCL') AND s.status is not null "+securityQuery+ " group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   AND  s.status not in ('CNCL') AND s.status is not null "+securityQuery+ " group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("   "+securityQuery+ " group by s.shipNumber");
			}
			
		}

		System.out.println("\n\n\n\n\n\n\n\n query For Agent And Account Portal" + query); 
		List activeShipments = serviceOrderManager.partnerActiveShipments(query.toString());
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("ActiveShipments");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		//System.out.println("/*********************************************************************************************************");
		//System.out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
		//System.out.println("/*********************************************************************************************************");
		String bA_SSCW = "";
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");
		outputStream.write("CORPID\t".getBytes()); 
		outputStream.write("SHIPNUMBER\t".getBytes()); 
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("JOB STAT\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes()); 
		outputStream.write("OANAME\t".getBytes()); 
		outputStream.write("DANAME\t".getBytes()); 
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("PKMODE\t".getBytes());
		outputStream.write("SERVICE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OSTATE\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DSTATE\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("ACTCU\t".getBytes());// not confirm
		outputStream.write("SO OPENED\t".getBytes());
		outputStream.write("ON HOLD\t".getBytes());
		outputStream.write("PACK T\t".getBytes());
		outputStream.write("PACK A\t".getBytes());
		outputStream.write("LOADING-T\t".getBytes());
		outputStream.write("LOADING-A\t".getBytes());
		outputStream.write("LOAD-A MTH\t".getBytes());
		outputStream.write("LOAD-A YR\t".getBytes()); 
		outputStream.write("SIT-O\t".getBytes()); 
		outputStream.write("ETD\t".getBytes());
		outputStream.write("ATD\t".getBytes());
		outputStream.write("POL\t".getBytes());
		
		outputStream.write("ETA\t".getBytes());
		outputStream.write("ATA\t".getBytes());
		outputStream.write("POE\t".getBytes());
		
		outputStream.write("Booking#\t".getBytes());
		outputStream.write("BL/AWB#\t".getBytes());
		outputStream.write("AES#\t".getBytes());
		outputStream.write("Vessel/Flight#\t".getBytes());
		
		outputStream.write("SIT-D\t".getBytes());
		outputStream.write("DELV-T\t".getBytes());
		outputStream.write("DELV-A\t".getBytes()); 
		outputStream.write("AW\t".getBytes());
		outputStream.write("DENSITY\t".getBytes()); 
		outputStream.write("AUTHORIZATION#\t".getBytes());
		outputStream.write("REFERENCE#\t".getBytes());
		outputStream.write("INSURANCE ENTITLED\t".getBytes());
		outputStream.write("INSURANCE VALUE\t".getBytes()); 
		outputStream.write("LEFT WAREHOUSE\t".getBytes()); 
		outputStream.write("EMPLOYEE#\t".getBytes()); 
		outputStream.write("STATUS#\t".getBytes());
		outputStream.write("\r\n".getBytes());
		Iterator it = activeShipments.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();
			if (((HibernateDTO) extracts).getCorpID() != null) {
				outputStream.write((((HibernateDTO) extracts).getCorpID().toString() + "\t").getBytes());
			} 
			else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString() + "\t").getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			} 
			String firstName = new String("");
			String lastName = new String("");
			if (((HibernateDTO) extracts).getFirstName() != null) {
				firstName = ((HibernateDTO) extracts).getFirstName().toString();

			} else {
				firstName = new String("");
			}
			if (((HibernateDTO) extracts).getLastName() != null) {
				lastName = ((HibernateDTO) extracts).getLastName().toString();

			} else {
				lastName = new String("");
			}
			outputStream.write((firstName+ "\t").getBytes());
			outputStream.write((lastName + "\t").getBytes());
			if (((HibernateDTO) extracts).getStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCoordinator() != null) {
				outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesMan() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getOriginAgent() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getDestinationAgent() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}  
			if (((HibernateDTO) extracts).getRouting() != null) {
				outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPackingMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getServiceType() != null) {
				outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCommodity() != null) {
				outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getJob() != null) {
				outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginState() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginState().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
			} else {
				String s = "\"" + "\",";
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationState() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationState().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getActualCubicFeet() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualCubicFeet().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getCreatedon() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getCreatedon())); 
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActivate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getActivate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginPacking() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginPacking()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPackA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPackA()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoadTarget() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoadTarget()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
				StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((month.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
				StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

				outputStream.write((year.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getSitOriginA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getServiceOrderETD() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderETD()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServiceOrderATD() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderATD()));
				outputStream.write((date.toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			} 
			
/*Addition from kunal */
			if (((HibernateDTO) extracts).getServicePartnerCarrierDeparture() != null) {
				outputStream.write((((HibernateDTO) extracts).getServicePartnerCarrierDeparture().toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			} 	
/*Done*/			
			
			if (((HibernateDTO) extracts).getServiceOrderETA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderETA()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServiceOrderATA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderATA()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}
/* Addition From Kunal */
			if (((HibernateDTO) extracts).getServicePartnerCarrierArrival() != null) {
				outputStream.write((((HibernateDTO) extracts).getServicePartnerCarrierArrival().toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServicePartnerbookNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getServicePartnerbookNumber().toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServicePartnerblNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getServicePartnerblNumber().toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServicePartneraesNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getServicePartneraesNumber().toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServicePartnercarrierVessels() != null) {
				outputStream.write((((HibernateDTO) extracts).getServicePartnercarrierVessels().toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}
			
/*Done*/
			if (((HibernateDTO) extracts).getSitDestinationA() != null)//  sitdesta
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDeliveryShipper() != null) //DELV T
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryShipper()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}  
			if (((HibernateDTO) extracts).getDeliveryA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDensity() != null) {
				if (((HibernateDTO) extracts).getDensity().toString().startsWith("0") || ((HibernateDTO) extracts).getDensity().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("2") || ((HibernateDTO) extracts).getDensity().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("4") || ((HibernateDTO) extracts).getDensity().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("6") || ((HibernateDTO) extracts).getDensity().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("8") || ((HibernateDTO) extracts).getDensity().toString().startsWith("9")) {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
					outputStream.write(("N/A\t").getBytes());
				}
			} else {

				outputStream.write(("N/A\t").getBytes());
			} 
			if (((HibernateDTO) extracts).getBillToAuthority() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillToAuthority().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillToReference() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillToReference().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getInsuranceValueEntitle() != null) {
				outputStream.write((((HibernateDTO) extracts).getInsuranceValueEntitle().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getInsuranceValueActual() != null) {
				outputStream.write((((HibernateDTO) extracts).getInsuranceValueActual().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getLeftWHOn() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getLeftWHOn()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else { 
				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getSocialSecurityNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getSocialSecurityNumber().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getQuoteStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getQuoteStatus().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			outputStream.write("\r\n".getBytes());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
}

	public void activeShip() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//System.out.println("\n\n\n\n jobTypes in activeship extract>>>>>>>>   "+jobTypes);
		if(extractVal == null || (!extractVal.equals("true")) ){
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}

		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
		
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
		
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		} else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			postingPeriodEndDates = new StringBuffer();
		} else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			postingPeriodBeginDates = new StringBuffer();
			postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		} else {
			postingPeriodBeginDates = new StringBuffer();
			postingPeriodEndDates = new StringBuffer();
		}
		if (conditionC1.equals("") && conditionC12.equals("") && conditionC13.equals("") && conditionC14.equals("") && conditionC15.equals("") && conditionC16.equals("")
				&& conditionC17.equals("") && conditionC18.equals("") && conditionC19.equals("") && conditionC120.equals("") && conditionC121.equals("")
				&& conditionC3.equals("") && conditionC31.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && conditionC115.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status in ('CLSD','CNCL') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status not in ('CNCL') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  group by s.shipNumber ");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  AND  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append("  where  ");

			if (conditionC1.equals("") || conditionC1 == null) {

			} else {
				query.append(" s.job ");
				isAnd = true;
				if (conditionC1.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			if (conditionC12.equals("") || conditionC12 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (conditionC12.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (conditionC13.equals("") || conditionC13 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (conditionC13.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (conditionC14.equals("") || conditionC14 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (conditionC14.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (conditionC15.equals("") || conditionC15 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (conditionC15.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (conditionC16.equals("") || conditionC16 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (conditionC16.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (conditionC17.equals("") || conditionC17 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (conditionC17.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (conditionC18.equals("") || conditionC18 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (conditionC18.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (conditionC19.equals("") || conditionC19 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (conditionC19.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (conditionC120.equals("") || conditionC120 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (conditionC120.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (conditionC121.equals("") || conditionC121 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (conditionC121.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (conditionC3.equals("") || conditionC3 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (conditionC31.equals("") || conditionC31 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}

			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
				if (isAnd == true) {
					query.append("  AND ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			 
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			

			if (conditionC115 == null || conditionC115.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (conditionC115.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status  in ('CLSD','CNCL') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CNCL') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  AND s.corpId='" + sessionCorpID + "' group by s.shipNumber");
			}
			//query.append("   AND s.corpId='"+sessionCorpID+"'  AND  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
		}
   
		//String queryExtract=extractManager.saveForExtract(query.toString());
		
		
		String check= getConditionC79();
	    if(check != null && (check.equals("trueeee"))){
		extractQueryFile = new ExtractQueryFile();
		extractQueryFile.setUserId(getRequest().getRemoteUser());
		extractQueryFile.setCreatedon(new Date());
		extractQueryFile.setCreatedby(getRequest().getRemoteUser());
		extractQueryFile.setCorpID(sessionCorpID);
		extractQueryFile.setExtractType("activeShip");
		extractQueryFile.setPublicPrivateFlag(getPublicPrivateFlag());
		extractQueryFile.setQueryName(getQueryName());
		extractQueryFile.setQueryCondition(query.toString());
		extractQueryFile.setModifiedby(getRequest().getRemoteUser());
		extractQueryFile.setModifiedon(new Date());
		extractManager.save(extractQueryFile);
		String key = "Active Ship Query is saved";
		saveMessage(getText(key));
		setConditionC79("");
		setConditionC80("");
		edit();
		}	
	
	//System.out.println("\n\n\n\n\n\n\n\n query" + query);
	//List activeShipments=new ArrayList();
	List activeShipments = serviceOrderManager.activeShipments(query.toString(),sessionCorpID);
	HttpServletResponse response = getResponse();
	response.setContentType("application/vnd.ms-excel");
	ServletOutputStream outputStream = response.getOutputStream();
	File file = new File("ActiveShipments");
	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	response.setHeader("Pragma", "public");
	response.setHeader("Cache-Control", "max-age=0");
	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
	String bA_SSCW = "";
	String categoryType = new String("");
	String orignAentCode = new String("");
	String destAentCode = new String("");
	String shipNumber = new String("");
	outputStream.write("SHIPNUMBER\t".getBytes());
	outputStream.write("REG Number\t".getBytes());
	outputStream.write("FIRST NAME\t".getBytes());
	outputStream.write("LAST NAME\t".getBytes());
	outputStream.write("JOB STAT\t".getBytes());
	outputStream.write("COORD\t".getBytes());
	outputStream.write("SALESMAN\t".getBytes());
	outputStream.write("CONTRACT\t".getBytes());
	outputStream.write("BILLTOCDE\t".getBytes());
	outputStream.write("BILLGROUP\t".getBytes());
	outputStream.write("ABBRV\t".getBytes());
	//outputStream.write("ACNT\t".getBytes());
	//outputStream.write("COMPETE\t".getBytes());
	outputStream.write("BOOKCODE\t".getBytes());
	outputStream.write("BA_SSCW\t".getBytes());
	outputStream.write("OACODE\t".getBytes());
	outputStream.write("OANAME\t".getBytes());
	//outputStream.write("pucode\t".getBytes());
	outputStream.write("DACODE\t".getBytes());
	outputStream.write("DANAME\t".getBytes());
	//outputStream.write("dlvcode\t".getBytes());
	outputStream.write("JOB\t".getBytes());
	outputStream.write("ROUTING\t".getBytes());
	outputStream.write("MODE\t".getBytes());
	outputStream.write("PKMODE\t".getBytes());
	outputStream.write("SERVICE\t".getBytes());
	outputStream.write("COMMODITY\t".getBytes());
	outputStream.write("OCITY\t".getBytes());
	outputStream.write("OSTATE\t".getBytes());
	outputStream.write("OCNTRYCD\t".getBytes());
	outputStream.write("DCITY\t".getBytes());
	outputStream.write("DSTATE\t".getBytes());
	outputStream.write("DCNTRYCD\t".getBytes());
	outputStream.write("ESTGROSS\t".getBytes());
	outputStream.write("ESTNET\t".getBytes());
	outputStream.write("ACTGROSS\t".getBytes());
	outputStream.write("ACTNET\t".getBytes());
	outputStream.write("DYNWEIGHT\t".getBytes());
	outputStream.write("ACTCU\t".getBytes());// not confirm
	outputStream.write("SO OPENED\t".getBytes());
	outputStream.write("On HOLD\t".getBytes());
	outputStream.write("PACK T\t".getBytes());
	outputStream.write("PACK A\t".getBytes());
	outputStream.write("LOADING-T\t".getBytes());
	outputStream.write("LOADING-A\t".getBytes());
	outputStream.write("LOAD-A MTH\t".getBytes());
	outputStream.write("LOAD-A YR\t".getBytes()); 
	outputStream.write("SIT-O\t".getBytes());
	//outputStream.write("SAILING\t".getBytes());//not confirm
	outputStream.write("ETD\t".getBytes());
	outputStream.write("ATD\t".getBytes());
	outputStream.write("ETA\t".getBytes());
	outputStream.write("ATA\t".getBytes());
	outputStream.write("SIT-D\t".getBytes());
	outputStream.write("DELV-T\t".getBytes());
	outputStream.write("DELV-A\t".getBytes());
	//outputStream.write("JOB_CC\t".getBytes());//not confirm
	outputStream.write("BLLCOMPLETE\t".getBytes());
	outputStream.write("AUDITCOMPLETE\t".getBytes()); 
	outputStream.write("REV-RECOG\t".getBytes()); 
	outputStream.write("DENSITY\t".getBytes());
	outputStream.write("COMPANY DIVISION\t".getBytes());  
	outputStream.write("PO\t".getBytes());
	outputStream.write("REF\t".getBytes());
	outputStream.write("INSURANCE ENTITLED\t".getBytes());
	outputStream.write("INSURANCE VALUE\t".getBytes());
	outputStream.write("INSURANCE CERTIFICATE NBR\t".getBytes());
	outputStream.write("REQUEST GBL\t".getBytes());
	outputStream.write("RECEIVED GBL\t".getBytes()); 
	outputStream.write("MLTRYFLAG\t".getBytes());
	outputStream.write("DP3\t".getBytes());
	outputStream.write("COMPTETIVE\t".getBytes()); 
	outputStream.write("SALESSTATUS\t".getBytes()); 
	//outputStream.write("f_stat\t".getBytes());
	outputStream.write("Instructions to Forwarder \t".getBytes()); 
	outputStream.write("Preliminary Notification\t".getBytes()); 
	outputStream.write("Final Notification\t".getBytes()); 
	outputStream.write("Email Sent\t".getBytes()); 
	outputStream.write("Client Overall Response\t".getBytes()); 
	outputStream.write("\r\n".getBytes());
	Iterator it = activeShipments.iterator();

	while (it.hasNext()) {
		Object extracts = (Object) it.next();

		if (((HibernateDTO) extracts).getShipNumber() != null) {
			outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString() + "\t").getBytes());
		}

		else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
			outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		String firstName = new String("");
		String lastName = new String("");
		if (((HibernateDTO) extracts).getFirstName() != null) {
			firstName = ((HibernateDTO) extracts).getFirstName().toString();

		} else {
			firstName = new String("");
		}
		if (((HibernateDTO) extracts).getLastName() != null) {
			lastName = ((HibernateDTO) extracts).getLastName().toString();

		} else {
			lastName = new String("");
		}
		outputStream.write((firstName+ "\t").getBytes());
		outputStream.write((lastName + "\t").getBytes());
		if (((HibernateDTO) extracts).getStatus() != null) {
			outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
		}

		else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getCoordinator() != null) {
			outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getSalesMan() != null) {
			outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getContract() != null) {
			if (((HibernateDTO) extracts).getContract().toString().equalsIgnoreCase("NO CONTRACT")) {
				outputStream.write(("**" + (((HibernateDTO) extracts).getContract().toString() + "**" + "\t")).getBytes());
			} else {
				outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());
			}

		}

		else {

			outputStream.write("\t".getBytes());
		}

		if (((HibernateDTO) extracts).getBillToCode() != null) {
			if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
					|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
					|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
					|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
					|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
			} else {
				outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
			}
		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getBillGroup() != null) {
			outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getAbbreviation() != null && (!(((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))) {
			outputStream.write((((HibernateDTO) extracts).getAbbreviation().toString() + "\t").getBytes());
		}
		else if(((HibernateDTO) extracts).getAbbreviation() == null || ((((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))){
			if (((HibernateDTO) extracts).getBillGroup() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
			} else { 
				outputStream.write("\t".getBytes());
			} 	
		}
		else { 
			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getBookingAgentCode() != null) {

			if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
					|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9")) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
			} else {
				outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
			}
			bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();

		} else {
			outputStream.write("\t".getBytes());
		}

		if (!bA_SSCW.equals("000061")) {
			outputStream.write(("\t").getBytes());
		} else {
			outputStream.write(("Y\t").getBytes());
			bA_SSCW = "";
		}
		if (((HibernateDTO) extracts).getOriginAgentCode() != null)
		{ 	
			if (((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("1")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("2")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("3")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("4")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("5")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("6")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("7")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("8")
					|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("9"))
				outputStream.write(("'" + ((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
			else {

				outputStream.write((((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
			}

		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getOriginAgent() != null) {
			outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}

		
		if (((HibernateDTO) extracts).getDestinationAgentCode() != null) { 
			if (((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("0")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("1")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("2")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("3")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("4")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("5")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("6")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("7")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("8")
					|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("9"))
				outputStream.write(("'" + ((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
			else {

				outputStream.write((((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
			}
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getDestinationAgent() != null) {
			outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}

		
		if (((HibernateDTO) extracts).getJob() != null) {
			outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getRouting() != null) {
			outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getMode() != null) {
			outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getPackingMode() != null) {
			outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes());
			//System.out.println("singhraj\n\n in dataExtractAction"+(((HibernateDTO)extracts).getPackingMode()));
		} else {

			outputStream.write("\t".getBytes());
		}

		if (((HibernateDTO) extracts).getServiceType() != null) {
			outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getCommodity() != null) {
			outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getOriginCity() != null) {
			outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getOriginState() != null) {
			outputStream.write((((HibernateDTO) extracts).getOriginState().toString() + "\t").getBytes());

		} else {
			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
			outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getDestinationCity() != null) {
			outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
		} else {
			String s = "\"" + "\",";
			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getDestinationState() != null) {
			outputStream.write((((HibernateDTO) extracts).getDestinationState().toString() + "\t").getBytes());

		} else {
			outputStream.write("\t".getBytes());
		}
		
		if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
			outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getEstimateGrossWeight() != null) {
			outputStream.write((((HibernateDTO) extracts).getEstimateGrossWeight().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getEstimatedNetWeight() != null) {
			outputStream.write((((HibernateDTO) extracts).getEstimatedNetWeight().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
			outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getActualNetWeight() != null) {
			outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getNetWeight() != null && (((BigDecimal)((HibernateDTO) extracts).getNetWeight()).doubleValue()!=0)) {
			outputStream.write((((HibernateDTO) extracts).getNetWeight().toString() + "\t").getBytes());
		} else {
			if (((HibernateDTO) extracts).getNetEstWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getNetEstWeight().toString() + "\t").getBytes());
			} else { 
			outputStream.write("\t".getBytes());
			}
		} 
		////////////////ACTU==========================================
		if (((HibernateDTO) extracts).getActualCubicFeet() != null) {
			outputStream.write((((HibernateDTO) extracts).getActualCubicFeet().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}

		if (((HibernateDTO) extracts).getCreatedon() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getCreatedon()));
			//outputStream.write(("format Cells...").getBytes());
			//outputStream.write(("category=Date format=31-Dec-99").getBytes());
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getActivate() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getActivate()));
			outputStream.write((date.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getBeginPacking() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginPacking()));
			outputStream.write((date.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getPackA() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPackA()));
			outputStream.write((date.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getBeginLoadTarget() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoadTarget()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getBeginLoad() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}

		if (((HibernateDTO) extracts).getBeginLoad() != null) {
			SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
			StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
			outputStream.write((month.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getBeginLoad() != null) {
			SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
			StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

			outputStream.write((year.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getSitOriginA() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		
		// ETD  and ATD 
		if (((HibernateDTO) extracts).getServiceOrderETD() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderETD()));
			outputStream.write((date.toString() + "\t").getBytes()); 
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getServiceOrderATD() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderATD()));
			outputStream.write((date.toString() + "\t").getBytes());
			
		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getServiceOrderETA() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderETA()));
			outputStream.write((date.toString() + "\t").getBytes()); 
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getServiceOrderATA() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderATA()));
			outputStream.write((date.toString() + "\t").getBytes()); 
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getSitDestinationA() != null)//  sitdesta
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getDeliveryShipper() != null) //DELV T
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryShipper()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		
		//outputStream.write("\t".getBytes()); //==============DELV T
		
		if (((HibernateDTO) extracts).getDeliveryA() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		//outputStream.write("\t".getBytes()); //==============JOB_CC
		
		if (((HibernateDTO) extracts).getBillComplete() != null) {

			DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
			outputStream.write((date.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}

		if (((HibernateDTO) extracts).getAuditComplete() != null) {

			DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
			
			outputStream.write((date.toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getRevenueRecognition() != null) { 
				 SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				 StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				 outputStream.write((date.toString() + "\t").getBytes()); 
			 } else { 
						outputStream.write("\t".getBytes());
		 } 
		if (((HibernateDTO) extracts).getDensity() != null) {
			if (((HibernateDTO) extracts).getDensity().toString().startsWith("0") || ((HibernateDTO) extracts).getDensity().toString().startsWith("1")
					|| ((HibernateDTO) extracts).getDensity().toString().startsWith("2") || ((HibernateDTO) extracts).getDensity().toString().startsWith("3")
					|| ((HibernateDTO) extracts).getDensity().toString().startsWith("4") || ((HibernateDTO) extracts).getDensity().toString().startsWith("5")
					|| ((HibernateDTO) extracts).getDensity().toString().startsWith("6") || ((HibernateDTO) extracts).getDensity().toString().startsWith("7")
					|| ((HibernateDTO) extracts).getDensity().toString().startsWith("8") || ((HibernateDTO) extracts).getDensity().toString().startsWith("9")) {
				outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
			} else {
				outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
				outputStream.write(("N/A\t").getBytes());
			}
		} else {

			outputStream.write(("N/A\t").getBytes());
		}
		if (((HibernateDTO) extracts).getCompanyDivision() != null) {
			outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}  
		if (((HibernateDTO) extracts).getBillToAuthority() != null) {
			outputStream.write((((HibernateDTO) extracts).getBillToAuthority().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getBillToReference() != null) {
			outputStream.write((((HibernateDTO) extracts).getBillToReference().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getInsuranceValueEntitle() != null) {
			outputStream.write((((HibernateDTO) extracts).getInsuranceValueEntitle().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getInsuranceValueActual() != null) {
			outputStream.write((((HibernateDTO) extracts).getInsuranceValueActual().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getCertnum() != null) {
			outputStream.write((((HibernateDTO) extracts).getCertnum().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getRequestGBL() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRequestGBL()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getRecivedGBL() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRecivedGBL()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
			outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getDp3() != null) {
			outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getComptetive() != null) {
			outputStream.write((((HibernateDTO) extracts).getComptetive().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getSalesStatus() != null) {
			outputStream.write((((HibernateDTO) extracts).getSalesStatus().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getInstructionsToForwarder() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getInstructionsToForwarder()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		}
		if (((HibernateDTO) extracts).getPreliminaryNotification() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPreliminaryNotification()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		} 
		if (((HibernateDTO) extracts).getFinalNotification() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getFinalNotification()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		} 
		
		if (((HibernateDTO) extracts).getEmailSent() != null) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getEmailSent()));
			outputStream.write((date.toString() + "\t").getBytes());

		} else {

			outputStream.write("\t".getBytes());
		} 
		
		if (((HibernateDTO) extracts).getClientOverallResponse() != null) {
			outputStream.write((((HibernateDTO) extracts).getClientOverallResponse().toString() + "\t").getBytes());
		} else {

			outputStream.write("\t".getBytes());
		}
		//outputStream.write("\t".getBytes());//  f_stat====================
		outputStream.write("\r\n".getBytes());
	}

		
		
		
		}
		else{
			extractQueryFile = extractManager.get(id);
			String query = extractQueryFile.getQueryCondition();
			//StringBuffer query = new StringBuffer();
		
		
		//System.out.println("\n\n\n\n\n\n\n\n query" + query);
		//List activeShipments=new ArrayList();
		List activeShipments = serviceOrderManager.activeShipments(query,sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("ActiveShipments");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
		String bA_SSCW = "";
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");
		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("REG Number\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("JOB STAT\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("BILLTOCDE\t".getBytes());
		outputStream.write("BILLGROUP\t".getBytes());
		outputStream.write("ABBRV\t".getBytes());
		//outputStream.write("ACNT\t".getBytes());
		//outputStream.write("COMPETE\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("BA_SSCW\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("OANAME\t".getBytes());
		//outputStream.write("pucode\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("DANAME\t".getBytes());
		//outputStream.write("dlvcode\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("PKMODE\t".getBytes());
		outputStream.write("SERVICE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OSTATE\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DSTATE\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ESTGROSS\t".getBytes());
		outputStream.write("ESTNET\t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("DYNWEIGHT\t".getBytes());
		outputStream.write("ACTCU\t".getBytes());// not confirm
		outputStream.write("SO OPENED\t".getBytes());
		outputStream.write("On HOLD\t".getBytes());
		outputStream.write("PACK T\t".getBytes());
		outputStream.write("PACK A\t".getBytes());
		outputStream.write("LOADING-T\t".getBytes());
		outputStream.write("LOADING-A\t".getBytes());
		outputStream.write("LOAD-A MTH\t".getBytes());
		outputStream.write("LOAD-A YR\t".getBytes()); 
		outputStream.write("SIT-O\t".getBytes());
		//outputStream.write("SAILING\t".getBytes());//not confirm
		outputStream.write("ETD\t".getBytes());
		outputStream.write("ATD\t".getBytes());
		outputStream.write("ETA\t".getBytes());
		outputStream.write("ATA\t".getBytes());
		outputStream.write("SIT-D\t".getBytes());
		outputStream.write("DELV-T\t".getBytes());
		outputStream.write("DELV-A\t".getBytes());
		//outputStream.write("JOB_CC\t".getBytes());//not confirm
		outputStream.write("BillComplete\t".getBytes());
		outputStream.write("AuditComplete\t".getBytes()); 
		outputStream.write("REV-RECOG\t".getBytes()); 
		outputStream.write("DENSITY\t".getBytes());
		outputStream.write("COMPANY DIVISION\t".getBytes());  
		outputStream.write("PO\t".getBytes());
		outputStream.write("REF\t".getBytes());
		outputStream.write("INSURANCE ENTITLED\t".getBytes());
		outputStream.write("INSURANCE VALUE\t".getBytes());
		outputStream.write("INSURANCE CERTIFICATE NBR\t".getBytes());
		outputStream.write("REQUEST GBL\t".getBytes());
		outputStream.write("RECEIVED GBL\t".getBytes()); 
		outputStream.write("MLTRYFLAG\t".getBytes());
		outputStream.write("DP3\t".getBytes());
		outputStream.write("COMPTETIVE\t".getBytes()); 
		outputStream.write("SALESSTATUS\t".getBytes()); 
		//outputStream.write("f_stat\t".getBytes());
		outputStream.write("Instructions to Forwarder \t".getBytes()); 
		outputStream.write("Preliminary Notification\t".getBytes()); 
		outputStream.write("Final Notification\t".getBytes()); 
		outputStream.write("Email Sent\t".getBytes()); 
		outputStream.write("Client Overall Response\t".getBytes()); 
		outputStream.write("\r\n".getBytes());
		Iterator it = activeShipments.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();

			if (((HibernateDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString() + "\t").getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			String firstName = new String("");
			String lastName = new String("");
			if (((HibernateDTO) extracts).getFirstName() != null) {
				firstName = ((HibernateDTO) extracts).getFirstName().toString();

			} else {
				firstName = new String("");
			}
			if (((HibernateDTO) extracts).getLastName() != null) {
				lastName = ((HibernateDTO) extracts).getLastName().toString();

			} else {
				lastName = new String("");
			}
			outputStream.write((firstName+ "\t").getBytes());
			outputStream.write((lastName + "\t").getBytes());
			if (((HibernateDTO) extracts).getStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCoordinator() != null) {
				outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesMan() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getContract() != null) {
				if (((HibernateDTO) extracts).getContract().toString().equalsIgnoreCase("NO CONTRACT")) {
					outputStream.write(("**" + (((HibernateDTO) extracts).getContract().toString() + "**" + "\t")).getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());
				}

			}

			else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getBillToCode() != null) {
				if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillGroup() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getAbbreviation() != null && (!(((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))) {
				outputStream.write((((HibernateDTO) extracts).getAbbreviation().toString() + "\t").getBytes());
			}
			else if(((HibernateDTO) extracts).getAbbreviation() == null || ((((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))){
				if (((HibernateDTO) extracts).getBillGroup() != null) {
					outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
				} else { 
					outputStream.write("\t".getBytes());
				} 	
			}
			else { 
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {

				if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
				}
				bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();

			} else {
				outputStream.write("\t".getBytes());
			}

			if (!bA_SSCW.equals("000061")) {
				outputStream.write(("\t").getBytes());
			} else {
				outputStream.write(("Y\t").getBytes());
				bA_SSCW = "";
			}
			if (((HibernateDTO) extracts).getOriginAgentCode() != null)//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			{
				//System.out.println("getOriginAgentCode()"+((HibernateDTO)extracts).getOriginAgentCode());
				//outputStream.write((((HibernateDTO)extracts).getOriginAgentCode().toString()+"\t").getBytes());
				if (((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("9"))
					outputStream.write(("'" + ((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
				else {

					outputStream.write((((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
				}

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginAgent() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//////////////////pucode=================================================
			
			if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {
				//System.out.println("\n getDestinationAgentCode()->"+((HibernateDTO)extracts).getDestinationAgentCode());
				//outputStream.write((((HibernateDTO)extracts).getDestinationAgentCode().toString()+"\t").getBytes());
				if (((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("0")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("9"))
					outputStream.write(("'" + ((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
				else {

					outputStream.write((((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationAgent() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			
			if (((HibernateDTO) extracts).getJob() != null) {
				outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRouting() != null) {
				outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPackingMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes());
				//System.out.println("singhraj\n\n in dataExtractAction"+(((HibernateDTO)extracts).getPackingMode()));
			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getServiceType() != null) {
				outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCommodity() != null) {
				outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginState() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginState().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
			} else {
				String s = "\"" + "\",";
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationState() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationState().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimateGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getEstimateGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimatedNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getEstimatedNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getNetWeight() != null && (((BigDecimal)((HibernateDTO) extracts).getNetWeight()).doubleValue()!=0)) {
				outputStream.write((((HibernateDTO) extracts).getNetWeight().toString() + "\t").getBytes());
			} else {
				if (((HibernateDTO) extracts).getNetEstWeight() != null) {
					outputStream.write((((HibernateDTO) extracts).getNetEstWeight().toString() + "\t").getBytes());
				} else { 
				outputStream.write("\t".getBytes());
				}
			} 
			////////////////ACTU==========================================
			if (((HibernateDTO) extracts).getActualCubicFeet() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualCubicFeet().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getCreatedon() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getCreatedon()));
				//outputStream.write(("format Cells...").getBytes());
				//outputStream.write(("category=Date format=31-Dec-99").getBytes());
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActivate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getActivate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginPacking() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginPacking()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPackA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPackA()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoadTarget() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoadTarget()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
				StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((month.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
				StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

				outputStream.write((year.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getSitOriginA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			
			// ETD  and ATD 
			if (((HibernateDTO) extracts).getServiceOrderETD() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderETD()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServiceOrderATD() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderATD()));
				outputStream.write((date.toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getServiceOrderETA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderETA()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getServiceOrderATA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getServiceOrderATA()));
				outputStream.write((date.toString() + "\t").getBytes()); 
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSitDestinationA() != null)//  sitdesta
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDeliveryShipper() != null) //DELV T
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryShipper()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			
			//outputStream.write("\t".getBytes()); //==============DELV T
			
			if (((HibernateDTO) extracts).getDeliveryA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			//outputStream.write("\t".getBytes()); //==============JOB_CC
			
			if (((HibernateDTO) extracts).getBillComplete() != null) {

				DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getAuditComplete() != null) {

				DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) { 
					 SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					 StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
					 outputStream.write((date.toString() + "\t").getBytes()); 
				 } else { 
							outputStream.write("\t".getBytes());
			 }
			
			
			if (((HibernateDTO) extracts).getDensity() != null) {
				if (((HibernateDTO) extracts).getDensity().toString().startsWith("0") || ((HibernateDTO) extracts).getDensity().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("2") || ((HibernateDTO) extracts).getDensity().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("4") || ((HibernateDTO) extracts).getDensity().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("6") || ((HibernateDTO) extracts).getDensity().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("8") || ((HibernateDTO) extracts).getDensity().toString().startsWith("9")) {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
					outputStream.write(("N/A\t").getBytes());
				}
			} else {

				outputStream.write(("N/A\t").getBytes());
			}
			if (((HibernateDTO) extracts).getCompanyDivision() != null) {
				outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}  
			if (((HibernateDTO) extracts).getBillToAuthority() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillToAuthority().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillToReference() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillToReference().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getInsuranceValueEntitle() != null) {
				outputStream.write((((HibernateDTO) extracts).getInsuranceValueEntitle().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getInsuranceValueActual() != null) {
				outputStream.write((((HibernateDTO) extracts).getInsuranceValueActual().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCertnum() != null) {
				outputStream.write((((HibernateDTO) extracts).getCertnum().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRequestGBL() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRequestGBL()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRecivedGBL() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRecivedGBL()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
				outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDp3() != null) {
				outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getComptetive() != null) {
				outputStream.write((((HibernateDTO) extracts).getComptetive().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesStatus().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getInstructionsToForwarder() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getInstructionsToForwarder()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPreliminaryNotification() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPreliminaryNotification()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getFinalNotification() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getFinalNotification()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEmailSent() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getEmailSent()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			} 
			
			if (((HibernateDTO) extracts).getClientOverallResponse() != null) {
				outputStream.write((((HibernateDTO) extracts).getClientOverallResponse().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			//outputStream.write("\t".getBytes());//  f_stat====================
			outputStream.write("\r\n".getBytes());
		}
		
	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public String findOldQueries(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		// extractQueryFile.setUserId(getRequest().getRemoteUser());
		queryList = extractManager.oldQueries(getRequest().getRemoteUser());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String deleteQuery(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		extractManager.remove(id);
		String key = " Query has been removed successfully.";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return findOldQueries();
	}
	
	public String extractSupport(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public void dynamicDomesticAnalysis() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(extractVal == null || (!extractVal.equals("true")) ){
		StringBuffer jobBuffer = new StringBuffer();
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		
		boolean isAnd = false;
		
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
		
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}

		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
		
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 }
		
		if (conditionC1.equals("") && conditionC12.equals("") && conditionC13.equals("") && conditionC14.equals("") && conditionC15.equals("") && conditionC16.equals("")
				&& conditionC17.equals("") && conditionC18.equals("") && conditionC19.equals("") && conditionC120.equals("") && conditionC121.equals("")
				&& conditionC3.equals("") && conditionC31.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && conditionC115.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("")&& storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&&payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status in ('CLSD','CNCL') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status not in ('CNCL') AND s.status is not null  group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  group by s.shipNumber");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  AND  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append("  where  ");

			if (conditionC1.equals("") || conditionC1 == null) {

			} else {
				query.append(" s.job  ");
				isAnd = true;
				if (conditionC1.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			// create date
			if (createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			//create date end
			if (conditionC12.equals("") || conditionC12 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (conditionC12.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (conditionC13.equals("") || conditionC13 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (conditionC13.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (conditionC14.equals("") || conditionC14 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (conditionC14.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (conditionC15.equals("") || conditionC15 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (conditionC15.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (conditionC16.equals("") || conditionC16 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (conditionC16.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (conditionC17.equals("") || conditionC17 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (conditionC17.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (conditionC18.equals("") || conditionC18 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (conditionC18.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (conditionC19.equals("") || conditionC19 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (conditionC19.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (conditionC120.equals("") || conditionC120 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (conditionC120.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (conditionC121.equals("") || conditionC121 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (conditionC121.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			}
			/*if(conditionC122.equals("")|| conditionC122==null)
			 {

			 }
			 else{
			 if(isAnd==true)
			 {
			 query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
			 }
			 else
			 {
			 query.append("   (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
			 }
			 if(conditionC122.toUpperCase().trim().equalsIgnoreCase("Equal to".trim()))
			 {
			 query.append("=");
			 query.append("'"+createdDates+"')");
			 }
			 else 
			 {
			 query.append("<>");
			 query.append("'"+createdDates+"')");
			 }
			 isAnd=true;
			 }*/
			if (conditionC3.equals("") || conditionC3 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (conditionC31.equals("") || conditionC31 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}
			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if ( payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ((postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&& (!(postingPeriodEndDates.toString().equals("")))) {
				if (isAnd == true) {
					query.append("  AND ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (conditionC115 == null || conditionC115.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (conditionC115.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status  in ('CLSD','CNCL') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CNCL') AND s.status is not null group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  AND s.corpId='" + sessionCorpID + "' group by s.shipNumber");
			}
			//query.append("   AND s.corpId='"+sessionCorpID+"'  AND  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
		}

		//System.out.println("\n\n\n query" + query);
		
        //String queryExtract=extractManager.saveForExtract(query.toString());
		
        String check= getConditionC79();
        if(check != null && (check.equals("trueeee"))){
         extractQueryFile = new ExtractQueryFile();
         extractQueryFile.setUserId(getRequest().getRemoteUser());
         extractQueryFile.setCreatedon(new Date());
         extractQueryFile.setCreatedby(getRequest().getRemoteUser());
         extractQueryFile.setCorpID(sessionCorpID);
         extractQueryFile.setExtractType("dynamicDomesticAnalysis");
         extractQueryFile.setPublicPrivateFlag(getPublicPrivateFlag());
         extractQueryFile.setQueryName(getQueryName());
         extractQueryFile.setQueryCondition(query.toString());
         extractQueryFile.setModifiedby(getRequest().getRemoteUser());
         extractQueryFile.setModifiedon(new Date());
         extractManager.save(extractQueryFile);
          String key = "dynamicDomesticAnalysis Query is saved";
          saveMessage(getText(key));
          setConditionC79("");
          setConditionC80("");
          edit();
      
       }

		
		
		
		//List ments=new ArrayList();
		List activeShipments = serviceOrderManager.activeShipments(query.toString(),sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("DomesticAnalysis");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
		String bA_SSCW = "";
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");

		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("JOB STAT\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("BILLTOCDE\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("DIVISION\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OSTATE\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DSTATE\t".getBytes());
		outputStream.write("HAULER CODE\t".getBytes());
		outputStream.write("HAULER NAME\t".getBytes());
		outputStream.write("DRIVER ID\t".getBytes());
		outputStream.write("DRIVER NAME\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("OPENED\t".getBytes());
		outputStream.write("ON HOLD\t".getBytes());
		outputStream.write("LOADING\t".getBytes());
		outputStream.write("REG NUM\t".getBytes());
		outputStream.write("TARGET LOAD\t".getBytes());
		outputStream.write("PACK T\t".getBytes());
		outputStream.write("SITO\t".getBytes());
		outputStream.write("SITDESTA\t".getBytes());
		outputStream.write("DELV T\t".getBytes());
		outputStream.write("BILLCOMPLETE\t".getBytes());
		outputStream.write("AUDITCOMPLETE\t".getBytes());
		outputStream.write("REVENUE RECOGNITION\t".getBytes());
		outputStream.write("MLTRYFLAG\t".getBytes());
		outputStream.write("DP3\t".getBytes());
		outputStream.write("GBL#\t".getBytes());
		outputStream.write("COMPTETIVE\t".getBytes()); 
		outputStream.write("SALESSTATUS\t".getBytes()); 
		outputStream.write("SOURCE\t".getBytes());
		outputStream.write("BILLING ID\t".getBytes());
		outputStream.write("BILL TO AUTHORITY\t".getBytes());
		outputStream.write("\r\n".getBytes());
		Iterator it = activeShipments.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();
			//shipnumber
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString()).getBytes());
				outputStream.write("\t".getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}
			// shipper
			String firstName = new String("");
			String lastName = new String("");

			//firstName=((HibernateDTO)extracts).getFirstName().toString();
			//lastName=((HibernateDTO)extracts).getLastName().toString();

			if (((HibernateDTO) extracts).getFirstName() != null) {
				firstName = ((HibernateDTO) extracts).getFirstName().toString();

			} else {
				firstName = new String("");
			}
			if (((HibernateDTO) extracts).getLastName() != null) {
				lastName = ((HibernateDTO) extracts).getLastName().toString();

			} else {
				lastName = new String("");
			}

			if (!firstName.equals("") && !lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write((lastName + "\t").getBytes());

			}

			if (firstName.equals("") && lastName.equals("")) {
				outputStream.write("\t".getBytes());

			}

			if (!firstName.equals("") && lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write(("" + "\t").getBytes());

			}

			if (firstName.equals("") && !lastName.equals("")) {
				outputStream.write(("" + "\t").getBytes());
				outputStream.write((lastName + "\t").getBytes());

			}

			//job
			if (((HibernateDTO) extracts).getStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getStatus().toString()).getBytes());
				outputStream.write("\t".getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}

			//coordinator
			if (((HibernateDTO) extracts).getCoordinator() != null) {
				outputStream.write((((HibernateDTO) extracts).getCoordinator().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//	salesman
			if (((HibernateDTO) extracts).getSalesMan() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesMan().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			// contract
			if (((HibernateDTO) extracts).getContract() != null) {

				outputStream.write((((HibernateDTO) extracts).getContract().toString()).getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			// billtoCode
			if (((HibernateDTO) extracts).getBillToCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getBillToCode().toString()).getBytes());
				String billtoCode = ((HibernateDTO) extracts).getBillToCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			// book code
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getBookingAgentCode().toString()).getBytes());					
				//outputStream.write("\t".getBytes());					
				String billtoCode = ((HibernateDTO) extracts).getBookingAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}

			//company division
			if (((HibernateDTO) extracts).getCompanyDivision() != null) {
				outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//OriginAgentCode
			if (((HibernateDTO) extracts).getOriginAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getOriginAgentCode().toString()).getBytes());
				//outputStream.write("\t".getBytes());
				String billtoCode = ((HibernateDTO) extracts).getOriginAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			// destin agent code
			if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getDestinationAgentCode().toString()).getBytes());
				//outputStream.write("\t".getBytes());
				String billtoCode = ((HibernateDTO) extracts).getDestinationAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}

			//job

			if (((HibernateDTO) extracts).getJob() != null) {
				outputStream.write((((HibernateDTO) extracts).getJob().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			//commodity
			if (((HibernateDTO) extracts).getCommodity() != null) {
				outputStream.write((((HibernateDTO) extracts).getCommodity().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			// ocity
			if (((HibernateDTO) extracts).getOriginCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCity().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			// ostate
			if (((HibernateDTO) extracts).getOriginState() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginState().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			// dicity
			if (((HibernateDTO) extracts).getDestinationCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// dstate
			if (((HibernateDTO) extracts).getDestinationState() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationState().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//hauler Code
			if (((HibernateDTO) extracts).getHaulingAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getHaulingAgentCode().toString()).getBytes());
				//outputStream.write("\t".getBytes());
				String billtoCode = ((HibernateDTO) extracts).getHaulingAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			//hauler name
			if (((HibernateDTO) extracts).getHaulerName() != null) {
				outputStream.write((((HibernateDTO) extracts).getHaulerName().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//driver id
			if (((HibernateDTO) extracts).getDriverId() != null) {
				outputStream.write((((HibernateDTO) extracts).getDriverId().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//driver  name			
			if (((HibernateDTO) extracts).getDriverName() != null) {
				outputStream.write((((HibernateDTO) extracts).getDriverName().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//act net

			if (((HibernateDTO) extracts).getActualNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//open ed
			if (((HibernateDTO) extracts).getCreatedon() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getCreatedon()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//on hold
			if (((HibernateDTO) extracts).getActivate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getActivate()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//loading
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//reg num
			if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//target load
			if (((HibernateDTO) extracts).getBeginLoadTarget() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoadTarget()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//pack t
			if (((HibernateDTO) extracts).getPackA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPackA()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//sito		

			if (((HibernateDTO) extracts).getSitOriginA() != null) {
				//outputStream.write((((HibernateDTO)extracts).getSitOriginA().toString()+"\t").getBytes());
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
				outputStream.write((date.toString() + "\t").getBytes());

				//System.out.println("origin a..............................");
			} else {
				outputStream.write("\t".getBytes());
			}

			//sitdesta
			if (((HibernateDTO) extracts).getSitDestinationA() != null)//sailing===========================
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//	delut
			if (((HibernateDTO) extracts).getDeliveryShipper() != null) //DELV T
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryShipper()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//bil cmpltA
			if (((HibernateDTO) extracts).getBillComplete() != null) {

				
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}

			// bil cmplt
			if (((HibernateDTO) extracts).getAuditComplete() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
				

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
				outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDp3() != null) {
				outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getGblNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getGblNumber().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getComptetive() != null) {
				outputStream.write((((HibernateDTO) extracts).getComptetive().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesStatus().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSource() != null) {
				outputStream.write((((HibernateDTO) extracts).getSource().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillingId() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillingId().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillToAuthority() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillToAuthority().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			
			outputStream.write("\r\n".getBytes());
		}
		
	
		
		
		}	
		else{
		
		
		extractQueryFile =extractManager.get(id);
		String query=extractQueryFile.getQueryCondition();
		//List ments=new ArrayList();
		List activeShipments = serviceOrderManager.activeShipments(query,sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("DomesticAnalysis");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
		String bA_SSCW = "";
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");

		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("JOB STAT\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("BILLTOCDE\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("DIVISION\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OSTATE\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DSTATE\t".getBytes());
		outputStream.write("HAULER CODE\t".getBytes());
		outputStream.write("HAULER NAME\t".getBytes());
		outputStream.write("DRIVER ID\t".getBytes());
		outputStream.write("DRIVER NAME\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("OPENED\t".getBytes());
		outputStream.write("ON HOLD\t".getBytes());
		outputStream.write("LOADING\t".getBytes());
		outputStream.write("REG NUM\t".getBytes());
		outputStream.write("TARGET LOAD\t".getBytes());
		outputStream.write("PACK T\t".getBytes());
		outputStream.write("SITO\t".getBytes());
		outputStream.write("SITDESTA\t".getBytes());
		outputStream.write("DELV T\t".getBytes());
		outputStream.write("BILLCOMPLETE\t".getBytes());
		outputStream.write("AUDITCOMPLETE\t".getBytes());
		outputStream.write("REVENUE RECOGNITION\t".getBytes());
		outputStream.write("MLTRYFLAG\t".getBytes());
		outputStream.write("DP3\t".getBytes());
		outputStream.write("GBL#\t".getBytes());
		outputStream.write("COMPTETIVE\t".getBytes()); 
		outputStream.write("SALESSTATUS\t".getBytes()); 
		outputStream.write("SOURCE\t".getBytes());
		outputStream.write("BILLING ID\t".getBytes());
		outputStream.write("BILL TO AUTHORITY\t".getBytes());
		outputStream.write("\r\n".getBytes());
		Iterator it = activeShipments.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();
			//shipnumber
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString()).getBytes());
				outputStream.write("\t".getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}
			// shipper
			String firstName = new String("");
			String lastName = new String("");

			//firstName=((HibernateDTO)extracts).getFirstName().toString();
			//lastName=((HibernateDTO)extracts).getLastName().toString();

			if (((HibernateDTO) extracts).getFirstName() != null) {
				firstName = ((HibernateDTO) extracts).getFirstName().toString();

			} else {
				firstName = new String("");
			}
			if (((HibernateDTO) extracts).getLastName() != null) {
				lastName = ((HibernateDTO) extracts).getLastName().toString();

			} else {
				lastName = new String("");
			}

			if (!firstName.equals("") && !lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write((lastName + "\t").getBytes());

			}

			if (firstName.equals("") && lastName.equals("")) {
				outputStream.write("\t".getBytes());

			}

			if (!firstName.equals("") && lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write(("" + "\t").getBytes());

			}

			if (firstName.equals("") && !lastName.equals("")) {
				outputStream.write(("" + "\t").getBytes());
				outputStream.write((lastName + "\t").getBytes());

			}

			//job
			if (((HibernateDTO) extracts).getStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getStatus().toString()).getBytes());
				outputStream.write("\t".getBytes());
			}

			else {

				outputStream.write("\t".getBytes());
			}

			//coordinator
			if (((HibernateDTO) extracts).getCoordinator() != null) {
				outputStream.write((((HibernateDTO) extracts).getCoordinator().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//	salesman
			if (((HibernateDTO) extracts).getSalesMan() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesMan().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			// contract
			if (((HibernateDTO) extracts).getContract() != null) {

				outputStream.write((((HibernateDTO) extracts).getContract().toString()).getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			// billtoCode
			if (((HibernateDTO) extracts).getBillToCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getBillToCode().toString()).getBytes());
				String billtoCode = ((HibernateDTO) extracts).getBillToCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			// book code
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getBookingAgentCode().toString()).getBytes());					
				//outputStream.write("\t".getBytes());					
				String billtoCode = ((HibernateDTO) extracts).getBookingAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}

			//company division
			if (((HibernateDTO) extracts).getCompanyDivision() != null) {
				outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//OriginAgentCode
			if (((HibernateDTO) extracts).getOriginAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getOriginAgentCode().toString()).getBytes());
				//outputStream.write("\t".getBytes());
				String billtoCode = ((HibernateDTO) extracts).getOriginAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			// destin agent code
			if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getDestinationAgentCode().toString()).getBytes());
				//outputStream.write("\t".getBytes());
				String billtoCode = ((HibernateDTO) extracts).getDestinationAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}

			//job

			if (((HibernateDTO) extracts).getJob() != null) {
				outputStream.write((((HibernateDTO) extracts).getJob().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			//commodity
			if (((HibernateDTO) extracts).getCommodity() != null) {
				outputStream.write((((HibernateDTO) extracts).getCommodity().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			// ocity
			if (((HibernateDTO) extracts).getOriginCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCity().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			// ostate
			if (((HibernateDTO) extracts).getOriginState() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginState().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			// dicity
			if (((HibernateDTO) extracts).getDestinationCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// dstate
			if (((HibernateDTO) extracts).getDestinationState() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationState().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//hauler Code
			if (((HibernateDTO) extracts).getHaulingAgentCode() != null) {
				//outputStream.write((((HibernateDTO)extracts).getHaulingAgentCode().toString()).getBytes());
				//outputStream.write("\t".getBytes());
				String billtoCode = ((HibernateDTO) extracts).getHaulingAgentCode().toString();
				String billtoCodeResult = "'" + billtoCode;
				outputStream.write(billtoCodeResult.getBytes());
				outputStream.write("\t".getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			//hauler name
			if (((HibernateDTO) extracts).getHaulerName() != null) {
				outputStream.write((((HibernateDTO) extracts).getHaulerName().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//driver id
			if (((HibernateDTO) extracts).getDriverId() != null) {
				outputStream.write((((HibernateDTO) extracts).getDriverId().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//driver  name			
			if (((HibernateDTO) extracts).getDriverName() != null) {
				outputStream.write((((HibernateDTO) extracts).getDriverName().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			//act net

			if (((HibernateDTO) extracts).getActualNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//open ed
			if (((HibernateDTO) extracts).getCreatedon() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getCreatedon()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//on hold
			if (((HibernateDTO) extracts).getActivate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getActivate()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//loading
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//reg num
			if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//target load
			if (((HibernateDTO) extracts).getBeginLoadTarget() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoadTarget()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//pack t
			if (((HibernateDTO) extracts).getPackA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPackA()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}

			//sito		

			if (((HibernateDTO) extracts).getSitOriginA() != null) {
				//outputStream.write((((HibernateDTO)extracts).getSitOriginA().toString()+"\t").getBytes());
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
				outputStream.write((date.toString() + "\t").getBytes());

				//System.out.println("origin a..............................");
			} else {
				outputStream.write("\t".getBytes());
			}

			//sitdesta
			if (((HibernateDTO) extracts).getSitDestinationA() != null)//sailing===========================
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//	delut
			if (((HibernateDTO) extracts).getDeliveryShipper() != null) //DELV T
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryShipper()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			//bil cmpltA
			if (((HibernateDTO) extracts).getBillComplete() != null) {

				
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
				
			} else {

				outputStream.write("\t".getBytes());
			}

			// bil cmplt
			if (((HibernateDTO) extracts).getAuditComplete() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
				

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
				outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDp3() != null) {
				outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getGblNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getGblNumber().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getComptetive() != null) {
				outputStream.write((((HibernateDTO) extracts).getComptetive().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesStatus().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getSource() != null) {
				outputStream.write((((HibernateDTO) extracts).getSource().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getBillingId() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillingId().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillToAuthority() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillToAuthority().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			outputStream.write("\r\n".getBytes());
		}
	
	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void dynamicFinancialDetails() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		StringBuffer jobBuffer = new StringBuffer();
		if(extractVal == null || (!extractVal.equals("true")) ){ 
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer query = new StringBuffer(); 
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer(); 
		boolean isAnd = false; 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}  
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(); 
		}
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}

		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		} 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 }


		if (conditionC1.equals("") && conditionC12.equals("") && conditionC13.equals("") && conditionC14.equals("") && conditionC15.equals("") && conditionC16.equals("")
				&& conditionC17.equals("") && conditionC18.equals("") && conditionC19.equals("") && conditionC120.equals("") && conditionC121.equals("")
				&& conditionC3.equals("") && conditionC31.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && conditionC115.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&&payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")) {
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status in ('CLSD','CNCL') AND s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   AND  s.status not in ('CNCL') AND s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  ");
			}
			//query.append("  where  s.corpId='"+sessionCorpID+"'  AND  s.status not in ('CLSD','CNCL')");
		} else { 
			query.append("  where  ");

			if (conditionC1.equals("") || conditionC1 == null) {

			} else {
				query.append(" s.job  ");
				isAnd = true;
				if (conditionC1.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}
			}
			if (conditionC12.equals("") || conditionC12 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}

				if (conditionC12.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;
			}
			if (conditionC13.equals("") || conditionC13 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append(" s.bookingAgentCode");
				}
				if (conditionC13.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (conditionC14.equals("") || conditionC14 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("  s.routing");
				}
				if (conditionC14.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (conditionC15.equals("") || conditionC15 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (conditionC15.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (conditionC16.equals("") || conditionC16 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("  s.packingMode");
				}
				if (conditionC16.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (conditionC17.equals("") || conditionC17 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (conditionC17.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (conditionC18.equals("") || conditionC18 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (conditionC18.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (conditionC19.equals("") || conditionC19 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (conditionC19.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (conditionC120.equals("") || conditionC120 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (conditionC120.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (conditionC121.equals("") || conditionC121 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (conditionC121.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (conditionC3.equals("") || conditionC3 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (conditionC31.equals("") || conditionC31 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if ( payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			} 
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ((postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&& (!(postingPeriodEndDates.toString().equals("")))) {
				if (isAnd == true) {
					query.append("  AND ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (conditionC115 == null || conditionC115.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (conditionC115.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status  in ('CLSD','CNCL') AND s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CNCL') AND s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("   AND s.corpId='" + sessionCorpID + "'");
			}
			//query.append("   AND s.corpId='"+sessionCorpID+"' AND  s.status not in ('CLSD','CNCL')");			

		}
		
      //String queryExtract=extractManager.saveForExtract(query.toString());
		
      String check= getConditionC79();
	    if(check != null && (check.equals("trueeee"))){
		extractQueryFile = new ExtractQueryFile();
		extractQueryFile.setUserId(getRequest().getRemoteUser());
		extractQueryFile.setCreatedon(new Date());
		extractQueryFile.setCreatedby(getRequest().getRemoteUser());
		extractQueryFile.setCorpID(sessionCorpID);
		extractQueryFile.setExtractType("dynamicFinancialDetails");
		extractQueryFile.setPublicPrivateFlag(getPublicPrivateFlag());
		extractQueryFile.setQueryName(getQueryName());
		extractQueryFile.setQueryCondition(query.toString());
		extractQueryFile.setModifiedby(getRequest().getRemoteUser());
		extractQueryFile.setModifiedon(new Date());
		extractManager.save(extractQueryFile);
		String key = "dynamicFinancialDetails Query is saved";
		saveMessage(getText(key));
		setConditionC79("");
		setConditionC80("");
		edit(); 
		} 
		//System.out.println("jobtype==" + jobTypes + "routings--" + routingTypes + "beginDates" + beginDates.toString() + "endDates" + endDates.toString());
		//System.out.println("\n\n\n\n\n dynamicFinanceDetails>>>>>>>>>>>>>>>>>>>" + query);
		List dynamicFinance = serviceOrderManager.dynamicFinanceDetails(query.toString(),sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("DynamicFinancialDetails");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + dynamicFinance.size() + "\t is extracted");
		String bA_SSCW = new String("");
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String(""); 
		Set set = new HashSet(); 
		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("REGISTRATION NUMBER\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("BILL-CODE\t".getBytes());
		outputStream.write("BILLGROUP\t".getBytes());
		outputStream.write("ABBRV\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("BA_SSCW\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("PKMODE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ESTGROSS \t".getBytes());
		outputStream.write("ESTNET\t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("DYNWEIGHT\t".getBytes());
		outputStream.write("ACTCU\t".getBytes()); // ACTCU=============
		outputStream.write("LOAD-A\t".getBytes());
		outputStream.write("LOAD MTH\t".getBytes());
		outputStream.write("LOAD YR\t".getBytes());
		outputStream.write("DELV-A\t".getBytes());
		outputStream.write("SIT-O\t".getBytes());
		outputStream.write("SIT-D\t".getBytes()); 
		outputStream.write("BILLCOMPLETE\t".getBytes());
		outputStream.write("AUDITCOMPLETE\t".getBytes()); 
		outputStream.write("DENSITY\t".getBytes()); //DENSITY 
		outputStream.write("SERVICE\t".getBytes()); 
		outputStream.write("OA_RECD\t".getBytes());
		outputStream.write("DA_RECD\t".getBytes());  //TYPE2
		outputStream.write("CHARGE CODE\t".getBytes()); 
		outputStream.write("ACCT-LINE\t".getBytes()); 
		outputStream.write("REV. AMOUNT\t".getBytes()); //AMOUNT
		outputStream.write("DIST. AMOUNT\t".getBytes()); 
		outputStream.write("INVOICE DESC\t".getBytes()); //DESC
		outputStream.write("OUR INVOICE#\t".getBytes()); 
		outputStream.write("INVOICE DATE\t".getBytes()); 
		outputStream.write("REC-POST\t".getBytes()); //GL
		outputStream.write("REC-GL\t".getBytes()); //GL
		outputStream.write("ACCT-BILL-CODE\t".getBytes());  //Detail_Invoice_payable_TR_LINES_BILLTOCDE
		outputStream.write("ACT EXPENSE\t".getBytes()); 
		outputStream.write("VENDOR INVOICE#\t".getBytes());
		outputStream.write("VENDOR INVOICE DATE\t".getBytes());
		outputStream.write("PAY-POST\t".getBytes());
		outputStream.write("BILL-NOTE\t".getBytes()); //Detail_Invoice_payable_TR_LINES_NAME
		outputStream.write("VENDOR NAME\t".getBytes()); //Fin Amts detail Report Extract_IAGENTS_NAME
		outputStream.write("COMPANY DIVISION\t".getBytes());
		outputStream.write("JOB STATUS\t".getBytes());
		outputStream.write("REV RECOGNITION\t".getBytes());
		outputStream.write("REVRECMNTH\t".getBytes());
		outputStream.write("SEND TO CLIENT DATE\t".getBytes());
		outputStream.write("DYNAMIC REVENUE\t".getBytes());
		outputStream.write("DYNAMIC EXPENSE\t".getBytes()); 
		outputStream.write("MLTRYFLAG\t".getBytes()); 
		outputStream.write("DP3\t".getBytes());  
		outputStream.write("BILLING PARTY PERSON\t".getBytes());
		outputStream.write("\r\n".getBytes());

		Iterator it = dynamicFinance.iterator();
		while (it.hasNext()) {

			Object extracts = it.next();
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getContract() != null) {
				outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillToCode() != null) {

				if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillGroup() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getAbbreviation() != null && (!(((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))) {
				outputStream.write((((HibernateDTO) extracts).getAbbreviation().toString() + "\t").getBytes());
			}
			else if(((HibernateDTO) extracts).getAbbreviation() == null || ((((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))){
				if (((HibernateDTO) extracts).getBillGroup() != null) {
					outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
				} else { 
					outputStream.write("\t".getBytes());
				} 	
			}
			else { 
				outputStream.write("\t".getBytes());
			}
			
			//			 for ACNT===============================================================
			//outputStream.write("\t".getBytes());
			/*if(((HibernateDTO)extracts).getComptetive()!=null)
			 {
			 outputStream.write((((HibernateDTO)extracts).getComptetive().toString()+"\t").getBytes());
			 }
			 else
			 {

			 outputStream.write("\t".getBytes());
			 }*/
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {

				if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
				}
				bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
				bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();
				if (bA_SSCW.equalsIgnoreCase("000061")) {
					outputStream.write(("Y\t").getBytes());
					bA_SSCW = "";
				} else {
					outputStream.write("\t".getBytes());
				}
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCategoryType() != null) {
				if (((HibernateDTO) extracts).getCategoryType().toString().equalsIgnoreCase("Origin Agent")) {
					if (((HibernateDTO) extracts).getVendorCode() != null) {

						if (((HibernateDTO) extracts).getVendorCode().toString().startsWith("0") || ((HibernateDTO) extracts).getVendorCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("9"))
							orignAentCode = "'" + (((HibernateDTO) extracts).getVendorCode().toString());
						else {

							orignAentCode = (((HibernateDTO) extracts).getVendorCode().toString());
						}

					} else {

					}
				}
				if (((HibernateDTO) extracts).getCategoryType().toString().equalsIgnoreCase("Destination Agent")) {
					if (((HibernateDTO) extracts).getVendorCode() != null) {
						if (((HibernateDTO) extracts).getVendorCode().toString().startsWith("0") || ((HibernateDTO) extracts).getVendorCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("9"))
							destAentCode = "'" + (((HibernateDTO) extracts).getVendorCode().toString());
						else {

							destAentCode = (((HibernateDTO) extracts).getVendorCode().toString());
						}

					} else {

					}
				} else {

				}

			} else {

			}
			if (((HibernateDTO) extracts).getOriginAgentCode() != null) {

				if (((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("9"))
					outputStream.write(("'" + ((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
				else {

					outputStream.write((((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {

				if (((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("0")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("9"))
					outputStream.write(("'" + ((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
				else {

					outputStream.write((((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getJob() != null) {
				outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRouting() != null) {
				outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPackingMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCommodity() != null) {
				outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			String firstName = new String("");
			String lastName = new String("");
			if (((HibernateDTO) extracts).getFirstName() != null) {
				firstName = ((HibernateDTO) extracts).getFirstName().toString();

			} else {
				firstName = new String("");
			}
			if (((HibernateDTO) extracts).getLastName() != null) {
				lastName = ((HibernateDTO) extracts).getLastName().toString();

			} else {
				lastName = new String("");
			}
			outputStream.write((firstName + "\t").getBytes());
			outputStream.write((lastName  + "\t").getBytes());
			if (((HibernateDTO) extracts).getCoordinator() != null) {
				outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesMan() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getEstimateGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getEstimateGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimatedNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getEstimatedNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualWeight() != null && (((BigDecimal)((HibernateDTO) extracts).getActualWeight()).doubleValue()!=0)) {
				outputStream.write((((HibernateDTO) extracts).getActualWeight().toString() + "\t").getBytes());
			} else {
				if (((HibernateDTO) extracts).getNetEstWeight() != null) {
					outputStream.write((((HibernateDTO) extracts).getNetEstWeight().toString() + "\t").getBytes());
				} else { 
				outputStream.write("\t".getBytes());
				}
			}
			//ACTCU=============================================================================
			if (((HibernateDTO) extracts).getActualCubicFeet() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualCubicFeet().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
				StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((month.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
				StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

				outputStream.write((year.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			// for delva==========================================================================
			if (((HibernateDTO) extracts).getDeliveryA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSitOriginA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSitDestinationA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			// for  job_cc====================================================================
			//outputStream.write("\t".getBytes());
			if (((HibernateDTO) extracts).getBillComplete() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getAuditComplete() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}  
			if (((HibernateDTO) extracts).getDensity() != null) {
				if (((HibernateDTO) extracts).getDensity().toString().startsWith("0") || ((HibernateDTO) extracts).getDensity().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("2") || ((HibernateDTO) extracts).getDensity().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("4") || ((HibernateDTO) extracts).getDensity().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("6") || ((HibernateDTO) extracts).getDensity().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("8") || ((HibernateDTO) extracts).getDensity().toString().startsWith("9")) {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
					outputStream.write(("N/A\t").getBytes());
				}
			} else {

				outputStream.write(("N/A\t").getBytes());
			}
			
			if (((HibernateDTO) extracts).getServiceType() != null) {
				outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
		
			if (bA_SSCW.equalsIgnoreCase("000061")) {

				outputStream.write("\t".getBytes());
				outputStream.write("\t".getBytes());
			} else {
				if (((HibernateDTO) extracts).getOriginAgent() != null) {
					outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
				} else {

					outputStream.write("\t".getBytes());
				}
				if (((HibernateDTO) extracts).getDestinationAgent() != null) {
					outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
				} else {

					outputStream.write("\t".getBytes());
				}

			}
			
			// Charge Code
			if (((HibernateDTO) extracts).getChargeCode() != null)
			{
				outputStream.write((((HibernateDTO) extracts).getChargeCode().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			// Accountline Number
			if (((HibernateDTO) extracts).getAccountLineNumber() != null)
			{
				outputStream.write(("'" + ((HibernateDTO) extracts).getAccountLineNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			
			if (((HibernateDTO) extracts).getAactualRevenue() != null)//Amount
			{
				outputStream.write((((HibernateDTO) extracts).getAactualRevenue().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDistributionAmount() != null)//Amount
			{
				outputStream.write((((HibernateDTO) extracts).getDistributionAmount().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDescription() != null) {
				// String sameDescription=(((HibernateDTO)extracts).getDescription().toString());
				// String lastC=sameDescription.toString().replaceAll("\"", "");
				//String last=lastC.toString().replaceAll(",", "");
				outputStream.write((((HibernateDTO) extracts).getDescription().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//Our Invoice No.--- recInvoiceNumber
			if (((HibernateDTO) extracts).getRecInvoiceNumber()!= null)
			{
				outputStream.write((((HibernateDTO) extracts).getRecInvoiceNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//Invoice Darte--- recInvoiceDate
			if (((HibernateDTO) extracts).getReceivedInvoiceDate()!= null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getReceivedInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRecPostDate()!= null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRecPostDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getRecGl() != null) {
				if (((HibernateDTO) extracts).getRecGl().toString().startsWith("0") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("2") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("4") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("6") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("8") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getRecGl().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getRecGl().toString() + "\t").getBytes());
				}
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getABillToCode() != null)//Detail_Invoice_payable_TR_LINES_BILLTOCDE
			{
				if (((HibernateDTO) extracts).getABillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("2") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("4") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("6") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("8") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getABillToCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getABillToCode().toString() + "\t").getBytes());
				}
			} else {
				outputStream.write("\t".getBytes());
			}
			
			
			//aCUTAL eXPENCE
			if (((HibernateDTO) extracts).getAActualExpense() != null)
			{
				outputStream.write((((HibernateDTO) extracts).getAActualExpense().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//VANDOR INVOICE NO.
			if (((HibernateDTO) extracts).getInvoiceNumber() != null)
			{
				outputStream.write((((HibernateDTO) extracts).getInvoiceNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//VANDOR INVOICE DATE.
			if (((HibernateDTO) extracts).getInvoiceDate() != null)
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPayPostDate() != null)
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPayPostDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//DETAIL_INVOICE_PAYABLE_TR_LINES_BILLTOCDE -- NOTE
			if (((HibernateDTO) extracts).getNote() != null) {
				outputStream.write((((HibernateDTO) extracts).getNote().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimateVendorName() != null)//Fin Amts detail Report Extract_IAGENTS_NAME
			{
				outputStream.write((((HibernateDTO) extracts).getEstimateVendorName().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCompanyDivision() != null) {
				outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) {

				SimpleDateFormat format = new SimpleDateFormat("MM");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSendActualToClient() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSendActualToClient()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDynamicRevenue() != null) {
				outputStream.write((((HibernateDTO) extracts).getDynamicRevenue().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDynamicExpense() != null) {
				outputStream.write((((HibernateDTO) extracts).getDynamicExpense().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
				outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDp3() != null) {
				outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPersonBilling() != null) {
				outputStream.write((((HibernateDTO) extracts).getPersonBilling().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			
			outputStream.write("\r\n".getBytes());
		}
		
	
		
		
		}
		else{
		
		
		 extractQueryFile =extractManager.get(id);
		String  query=extractQueryFile.getQueryCondition();
		
		//System.out.println("jobtype==" + jobTypes + "routings--" + routingTypes + "beginDates" + beginDates.toString() + "endDates" + endDates.toString());
		//System.out.println("\n\n\n\n\n dynamicFinanceDetails>>>>>>>>>>>>>>>>>>>" + query);
		List dynamicFinance = serviceOrderManager.dynamicFinanceDetails(query,sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("DynamicFinancialDetails");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + dynamicFinance.size() + "\t is extracted");
		String bA_SSCW = new String("");
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");

		Set set = new HashSet();

		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("REGISTRATION NUMBER\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("BILL-CODE\t".getBytes());
		outputStream.write("BILLGROUP\t".getBytes());
		outputStream.write("ABBRV\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("BA_SSCW\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("PKMODE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("COORD\t".getBytes());
		outputStream.write("SALESMAN\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ESTGROSS \t".getBytes());
		outputStream.write("ESTNET\t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("DYNWEIGHT\t".getBytes());
		outputStream.write("ACTCU\t".getBytes()); // ACTCU=============
		outputStream.write("LOAD-A\t".getBytes());
		outputStream.write("LOAD MTH\t".getBytes());
		outputStream.write("LOAD YR\t".getBytes());
		outputStream.write("DELV-A\t".getBytes());
		outputStream.write("SIT-O\t".getBytes());
		outputStream.write("SIT-D\t".getBytes()); 
		outputStream.write("BILLCOMPLETE\t".getBytes());
		outputStream.write("AUDITCOMPLETE\t".getBytes());  
		outputStream.write("DENSITY\t".getBytes()); //DENSITY 
		outputStream.write("SERVICE\t".getBytes()); 
		outputStream.write("OA_RECD\t".getBytes());
		outputStream.write("DA_RECD\t".getBytes());  //TYPE2
		outputStream.write("CHARGE CODE\t".getBytes()); 
		outputStream.write("ACCT-LINE\t".getBytes()); 
		outputStream.write("REV. AMOUNT\t".getBytes()); //AMOUNT
		outputStream.write("DIST. AMOUNT\t".getBytes()); 
		outputStream.write("INVOICE DESC\t".getBytes()); //DESC
		outputStream.write("OUR INVOICE#\t".getBytes()); 
		outputStream.write("INVOICE DATE\t".getBytes()); 
		outputStream.write("REC-POST\t".getBytes()); //GL
		outputStream.write("REC-GL\t".getBytes()); //GL
		outputStream.write("ACCT-BILL-CODE\t".getBytes());  //Detail_Invoice_payable_TR_LINES_BILLTOCDE
		outputStream.write("ACT EXPENSE\t".getBytes()); 
		outputStream.write("VENDOR INVOICE#\t".getBytes());
		outputStream.write("VENDOR INVOICE DATE\t".getBytes());
		outputStream.write("PAY-POST\t".getBytes());
		outputStream.write("BILL-NOTE\t".getBytes()); //Detail_Invoice_payable_TR_LINES_NAME
		outputStream.write("VENDOR NAME\t".getBytes()); //Fin Amts detail Report Extract_IAGENTS_NAME
		outputStream.write("COMPANY DIVISION\t".getBytes());
		outputStream.write("JOB STATUS\t".getBytes());
		outputStream.write("REV RECOGNITION\t".getBytes());
		outputStream.write("REVRECMNTH\t".getBytes());
		outputStream.write("SEND TO CLIENT DATE\t".getBytes());
		outputStream.write("DYNAMIC REVENUE\t".getBytes());
		outputStream.write("DYNAMIC EXPENSE\t".getBytes()); 
		outputStream.write("MLTRYFLAG\t".getBytes()); 
		outputStream.write("DP3\t".getBytes());  
		outputStream.write("BILLING PARTY PERSON\t".getBytes());
		outputStream.write("\r\n".getBytes());

		Iterator it = dynamicFinance.iterator();
		while (it.hasNext()) {

			Object extracts = it.next();
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((HibernateDTO) extracts).getShipNumber().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRegistrationNumber() != null) {
				outputStream.write((((HibernateDTO) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getContract() != null) {
				outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillToCode() != null) {

				if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBillGroup() != null) {
				outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getAbbreviation() != null && (!(((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))) {
				outputStream.write((((HibernateDTO) extracts).getAbbreviation().toString() + "\t").getBytes());
			}
			else if(((HibernateDTO) extracts).getAbbreviation() == null || ((((HibernateDTO) extracts).getAbbreviation().toString()).trim().equals(""))){
				if (((HibernateDTO) extracts).getBillGroup() != null) {
					outputStream.write((((HibernateDTO) extracts).getBillGroup().toString() + "\t").getBytes());
				} else { 
					outputStream.write("\t".getBytes());
				} 	
			}
			else { 
				outputStream.write("\t".getBytes());
			}
			
			//			 for ACNT===============================================================
			//outputStream.write("\t".getBytes());
			/*if(((HibernateDTO)extracts).getComptetive()!=null)
			 {
			 outputStream.write((((HibernateDTO)extracts).getComptetive().toString()+"\t").getBytes());
			 }
			 else
			 {

			 outputStream.write("\t".getBytes());
			 }*/
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {

				if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
				}
				bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
				bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();
				if (bA_SSCW.equalsIgnoreCase("000061")) {
					outputStream.write(("Y\t").getBytes());
					bA_SSCW = "";
				} else {
					outputStream.write("\t".getBytes());
				}
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCategoryType() != null) {
				if (((HibernateDTO) extracts).getCategoryType().toString().equalsIgnoreCase("Origin Agent")) {
					if (((HibernateDTO) extracts).getVendorCode() != null) {

						if (((HibernateDTO) extracts).getVendorCode().toString().startsWith("0") || ((HibernateDTO) extracts).getVendorCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("9"))
							orignAentCode = "'" + (((HibernateDTO) extracts).getVendorCode().toString());
						else {

							orignAentCode = (((HibernateDTO) extracts).getVendorCode().toString());
						}

					} else {

					}
				}
				if (((HibernateDTO) extracts).getCategoryType().toString().equalsIgnoreCase("Destination Agent")) {
					if (((HibernateDTO) extracts).getVendorCode() != null) {
						if (((HibernateDTO) extracts).getVendorCode().toString().startsWith("0") || ((HibernateDTO) extracts).getVendorCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getVendorCode().toString().startsWith("9"))
							destAentCode = "'" + (((HibernateDTO) extracts).getVendorCode().toString());
						else {

							destAentCode = (((HibernateDTO) extracts).getVendorCode().toString());
						}

					} else {

					}
				} else {

				}

			} else {

			}
			if (((HibernateDTO) extracts).getOriginAgentCode() != null) {

				if (((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("0") || ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getOriginAgentCode().toString().startsWith("9"))
					outputStream.write(("'" + ((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
				else {

					outputStream.write((((HibernateDTO) extracts).getOriginAgentCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationAgentCode() != null) {

				if (((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("0")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("2")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("4")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("6")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("8")
						|| ((HibernateDTO) extracts).getDestinationAgentCode().toString().startsWith("9"))
					outputStream.write(("'" + ((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
				else {

					outputStream.write((((HibernateDTO) extracts).getDestinationAgentCode().toString() + "\t").getBytes());
				}
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getJob() != null) {
				outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRouting() != null) {
				outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPackingMode() != null) {
				outputStream.write((((HibernateDTO) extracts).getPackingMode().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCommodity() != null) {
				outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			String firstName = new String("");
			String lastName = new String("");
			if (((HibernateDTO) extracts).getFirstName() != null) {
				firstName = ((HibernateDTO) extracts).getFirstName().toString();

			} else {
				firstName = new String("");
			}
			if (((HibernateDTO) extracts).getLastName() != null) {
				lastName = ((HibernateDTO) extracts).getLastName().toString();

			} else {
				lastName = new String("");
			}
			outputStream.write((firstName + "\t").getBytes());
			outputStream.write((lastName  + "\t").getBytes());
			if (((HibernateDTO) extracts).getCoordinator() != null) {
				outputStream.write((((HibernateDTO) extracts).getCoordinator().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSalesMan() != null) {
				outputStream.write((((HibernateDTO) extracts).getSalesMan().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationCity() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
				outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimateGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getEstimateGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimatedNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getEstimatedNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualNetWeight() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getActualWeight() != null && (((BigDecimal)((HibernateDTO) extracts).getActualWeight()).doubleValue()!=0)) {
				outputStream.write((((HibernateDTO) extracts).getActualWeight().toString() + "\t").getBytes());
			} else {
				if (((HibernateDTO) extracts).getNetEstWeight() != null) {
					outputStream.write((((HibernateDTO) extracts).getNetEstWeight().toString() + "\t").getBytes());
				} else { 
				outputStream.write("\t".getBytes());
				}
			}
			//ACTCU=============================================================================
			if (((HibernateDTO) extracts).getActualCubicFeet() != null) {
				outputStream.write((((HibernateDTO) extracts).getActualCubicFeet().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}

			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
				StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
				outputStream.write((month.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getBeginLoad() != null) {
				SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
				StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));

				outputStream.write((year.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			// for delva==========================================================================
			if (((HibernateDTO) extracts).getDeliveryA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getDeliveryA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSitOriginA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitOriginA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSitDestinationA() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSitDestinationA()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			// for  job_cc====================================================================
			//outputStream.write("\t".getBytes());
			if (((HibernateDTO) extracts).getBillComplete() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBillComplete()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getAuditComplete() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getAuditComplete()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}  
			if (((HibernateDTO) extracts).getDensity() != null) {
				if (((HibernateDTO) extracts).getDensity().toString().startsWith("0") || ((HibernateDTO) extracts).getDensity().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("2") || ((HibernateDTO) extracts).getDensity().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("4") || ((HibernateDTO) extracts).getDensity().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("6") || ((HibernateDTO) extracts).getDensity().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getDensity().toString().startsWith("8") || ((HibernateDTO) extracts).getDensity().toString().startsWith("9")) {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getDensity().toString() + "\t").getBytes());
					outputStream.write(("N/A\t").getBytes());
				}
			} else {

				outputStream.write(("N/A\t").getBytes());
			}
			
			if (((HibernateDTO) extracts).getServiceType() != null) {
				outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			if (bA_SSCW.equalsIgnoreCase("000061")) {

				outputStream.write("\t".getBytes());
				outputStream.write("\t".getBytes());
			} else {
				if (((HibernateDTO) extracts).getOriginAgent() != null) {
					outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
				} else {

					outputStream.write("\t".getBytes());
				}
				if (((HibernateDTO) extracts).getDestinationAgent() != null) {
					outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
				} else {

					outputStream.write("\t".getBytes());
				}

			}
			
			// Charge Code
			if (((HibernateDTO) extracts).getChargeCode() != null)
			{
				outputStream.write((((HibernateDTO) extracts).getChargeCode().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			// Accountline Number
			if (((HibernateDTO) extracts).getAccountLineNumber() != null)
			{
				outputStream.write(("'" + ((HibernateDTO) extracts).getAccountLineNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			
			if (((HibernateDTO) extracts).getAactualRevenue() != null)//Amount
			{
				outputStream.write((((HibernateDTO) extracts).getAactualRevenue().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDistributionAmount() != null)//Amount
			{
				outputStream.write((((HibernateDTO) extracts).getDistributionAmount().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDescription() != null) {
				// String sameDescription=(((HibernateDTO)extracts).getDescription().toString());
				// String lastC=sameDescription.toString().replaceAll("\"", "");
				//String last=lastC.toString().replaceAll(",", "");
				outputStream.write((((HibernateDTO) extracts).getDescription().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//Our Invoice No.--- recInvoiceNumber
			if (((HibernateDTO) extracts).getRecInvoiceNumber()!= null)
			{
				outputStream.write((((HibernateDTO) extracts).getRecInvoiceNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//Invoice Darte--- recInvoiceDate
			if (((HibernateDTO) extracts).getReceivedInvoiceDate()!= null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getReceivedInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRecPostDate()!= null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRecPostDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getRecGl() != null) {
				if (((HibernateDTO) extracts).getRecGl().toString().startsWith("0") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("2") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("4") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("6") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getRecGl().toString().startsWith("8") || ((HibernateDTO) extracts).getRecGl().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getRecGl().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getRecGl().toString() + "\t").getBytes());
				}
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getABillToCode() != null)//Detail_Invoice_payable_TR_LINES_BILLTOCDE
			{
				if (((HibernateDTO) extracts).getABillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("1")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("2") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("3")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("4") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("5")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("6") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("7")
						|| ((HibernateDTO) extracts).getABillToCode().toString().startsWith("8") || ((HibernateDTO) extracts).getABillToCode().toString().startsWith("9")) {
					outputStream.write(("'" + ((HibernateDTO) extracts).getABillToCode().toString() + "\t").getBytes());
				} else {
					outputStream.write((((HibernateDTO) extracts).getABillToCode().toString() + "\t").getBytes());
				}
			} else {
				outputStream.write("\t".getBytes());
			}
			
			
			//aCUTAL eXPENCE
			if (((HibernateDTO) extracts).getAActualExpense() != null)
			{
				outputStream.write((((HibernateDTO) extracts).getAActualExpense().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//VANDOR INVOICE NO.
			if (((HibernateDTO) extracts).getInvoiceNumber() != null)
			{
				outputStream.write((((HibernateDTO) extracts).getInvoiceNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//VANDOR INVOICE DATE.
			if (((HibernateDTO) extracts).getInvoiceDate() != null)
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getPayPostDate() != null)
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getPayPostDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//DETAIL_INVOICE_PAYABLE_TR_LINES_BILLTOCDE -- NOTE
			if (((HibernateDTO) extracts).getNote() != null) {
				outputStream.write((((HibernateDTO) extracts).getNote().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getEstimateVendorName() != null)//Fin Amts detail Report Extract_IAGENTS_NAME
			{
				outputStream.write((((HibernateDTO) extracts).getEstimateVendorName().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getCompanyDivision() != null) {
				outputStream.write((((HibernateDTO) extracts).getCompanyDivision().toString() + "\t").getBytes());

			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getStatus() != null) {
				outputStream.write((((HibernateDTO) extracts).getStatus().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getRevenueRecognition() != null) {

				SimpleDateFormat format = new SimpleDateFormat("MM");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getRevenueRecognition()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getSendActualToClient() != null) {

				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getSendActualToClient()));
				outputStream.write((date.toString() + "\t").getBytes());

			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDynamicRevenue() != null) {
				outputStream.write((((HibernateDTO) extracts).getDynamicRevenue().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDynamicExpense() != null) {
				outputStream.write((((HibernateDTO) extracts).getDynamicExpense().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			} 
			if (((HibernateDTO) extracts).getMillitaryShipment() != null) {
				outputStream.write((((HibernateDTO) extracts).getMillitaryShipment().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			if (((HibernateDTO) extracts).getDp3() != null) {
				outputStream.write((((HibernateDTO) extracts).getDp3().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			
			if (((HibernateDTO) extracts).getPersonBilling() != null) {
				outputStream.write((((HibernateDTO) extracts).getPersonBilling().toString() + "\t").getBytes());
			} else {

				outputStream.write("\t".getBytes());
			}
			outputStream.write("\r\n".getBytes());
		}
		
	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void reciprocityReport() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List financeSummary = new ArrayList();// I will do later
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder beginDates = new StringBuilder(formats.format(beginDate));
		StringBuilder endDates = new StringBuilder(formats.format(endDate));
		//System.out.println("jobtype==" + jobTypes + "routings--" + routingTypes + "beginDates" + beginDates.toString() + "endDates" + endDates.toString());
		//List financeSummary=serviceOrderManager.financeSummary(query.toString());
		HttpServletResponse response = getResponse();
		HttpServletRequest request = getRequest(); 
		if (request.getMethod().equalsIgnoreCase("post")) {
			if (request.getAttributeNames().hasMoreElements()) {
				out.println(request.getAttributeNames().nextElement());
			}
		}

		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("ReciprocityReport");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + financeSummary.size() + "\t is extracted");
		String bA_SSCW = new String("");
		String categoryType = new String("");
		String orignAentCode = new String("");
		String destAentCode = new String("");
		String shipNumber = new String("");

		Set set = new HashSet();
		Iterator it = financeSummary.iterator();
		outputStream.write("SHIPNUM\t".getBytes());
		outputStream.write("CONTRACT\t".getBytes());
		outputStream.write("BILLTOCDE\t".getBytes());
		outputStream.write("ACNT\t".getBytes());
		outputStream.write("SSCW BA\t".getBytes());
		outputStream.write("BOOKCODE\t".getBytes());
		outputStream.write("OACODE\t".getBytes());
		outputStream.write("DACODE\t".getBytes());
		outputStream.write("JOB\t".getBytes());
		outputStream.write("ROUTING\t".getBytes());
		outputStream.write("SERVICE\t".getBytes());
		outputStream.write("MODE\t".getBytes());
		outputStream.write("COMMODITY\t".getBytes());
		outputStream.write("Shipper\t".getBytes());
		outputStream.write("OCITY\t".getBytes());
		outputStream.write("OCNTRYCD\t".getBytes());
		outputStream.write("DCITY\t".getBytes());
		outputStream.write("DCNTRYCD\t".getBytes());
		outputStream.write("ACTGROSS\t".getBytes());
		outputStream.write("ACTNET\t".getBytes());
		outputStream.write("LOADING\t".getBytes());
		outputStream.write("LOAD YR\t".getBytes());
		outputStream.write("LOAD MTH\t".getBytes());
		outputStream.write("WEIGHT\t".getBytes());
		outputStream.write("BOOKER\t".getBytes());
		outputStream.write("BA GRP\t".getBytes());
		outputStream.write("OA\t".getBytes());
		outputStream.write("OA GRP\t".getBytes());
		outputStream.write("DA\t".getBytes());
		outputStream.write("DA GRP\t".getBytes());
		outputStream.write("OA_GIVEN\t".getBytes());
		outputStream.write("DA_GIVEN\t".getBytes());
		outputStream.write("OA GIVEN AMT\t".getBytes());
		outputStream.write("DA GIVEN AMT\t".getBytes());
		outputStream.write("OA_GIVEN_WT\t".getBytes());
		outputStream.write("DA_GIVEN_WT\t".getBytes());
		outputStream.write("OA_GIVEN_NO\t".getBytes());
		outputStream.write("DA_GIVEN_NO\t".getBytes());
		outputStream.write("OA_RECD\t".getBytes());
		outputStream.write("DA_RECD\t".getBytes());
		outputStream.write("OA PMT\t".getBytes());
		outputStream.write("DA PMT\t".getBytes());
		outputStream.write("OA_RECD_WT\t".getBytes());
		outputStream.write("DA_RECD_WT\t".getBytes());
		outputStream.write("OA_RECD_NO\t".getBytes());
		outputStream.write("DA_RECD_NO\t".getBytes());
		outputStream.write("\r\n".getBytes());
		while (it.hasNext()) {
			Object extracts = it.next();
			if (((HibernateDTO) extracts).getShipNumber() != null) {
				shipNumber = (((HibernateDTO) extracts).getShipNumber().toString());
				set.add(shipNumber);
			} else {

			}
		}
		Iterator its = set.iterator();
		while (its.hasNext()) {
			Object ship = (Object) its.next();

			Iterator itss = financeSummary.iterator();

			while (itss.hasNext()) {
				Object extract = (Object) itss.next();
				if (ship.toString().equalsIgnoreCase(((HibernateDTO) extract).getShipNumber().toString())) {
					if (((HibernateDTO) extract).getCategoryType() != null) {
						if (((HibernateDTO) extract).getCategoryType().toString().equalsIgnoreCase("Origin Agent")) {
							if (((HibernateDTO) extract).getVendorCode() != null) {

								if (((HibernateDTO) extract).getVendorCode().toString().startsWith("0")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("1")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("2")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("3")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("4")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("5")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("6")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("7")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("8")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("9"))
									orignAentCode = "'" + (((HibernateDTO) extract).getVendorCode().toString());
								else {

									orignAentCode = (((HibernateDTO) extract).getVendorCode().toString());
								}

							} else {

							}
						}
						if (((HibernateDTO) extract).getCategoryType().toString().equalsIgnoreCase("Destination Agent")) {
							if (((HibernateDTO) extract).getVendorCode() != null) {
								if (((HibernateDTO) extract).getVendorCode().toString().startsWith("0")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("1")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("2")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("3")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("4")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("5")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("6")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("7")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("8")
										|| ((HibernateDTO) extract).getVendorCode().toString().startsWith("9"))
									destAentCode = "'" + (((HibernateDTO) extract).getVendorCode().toString());
								else {

									destAentCode = (((HibernateDTO) extract).getVendorCode().toString());
								}

							} else {

							}
						} else {

						}

					} else {
					}
				}

			}

			outputStream.write((ship.toString() + "\t").getBytes());
			Iterator itsss = financeSummary.iterator();
			outer: while (itsss.hasNext()) {
				Object extracts = (Object) itsss.next();
				if (ship.toString().equalsIgnoreCase(((HibernateDTO) extracts).getShipNumber().toString())) {
					if (((HibernateDTO) extracts).getContract() != null) {
						outputStream.write((((HibernateDTO) extracts).getContract().toString() + "\t").getBytes());
					}

					else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getBillToCode() != null) {

						if (((HibernateDTO) extracts).getBillToCode().toString().startsWith("0") || ((HibernateDTO) extracts).getBillToCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getBillToCode().toString().startsWith("9")) {
							outputStream.write(("'" + ((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
						} else {
							outputStream.write((((HibernateDTO) extracts).getBillToCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}
					// for ACNT===============================================================
					outputStream.write("\t".getBytes());
					if (((HibernateDTO) extracts).getBookingAgentCode() != null) {
						bA_SSCW = ((HibernateDTO) extracts).getBookingAgentCode().toString();
						if (bA_SSCW.equalsIgnoreCase("000061")) {
							outputStream.write(("Y\t").getBytes());
							bA_SSCW = "";
						} else {
							outputStream.write("N\t".getBytes());
						}
					} else {
						outputStream.write("N\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBookingAgentCode() != null) {

						if (((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("0")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("1")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("2")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("3")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("4")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("5")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("6")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("7")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("8")
								|| ((HibernateDTO) extracts).getBookingAgentCode().toString().startsWith("9"))
							outputStream.write(("'" + ((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
						else {

							outputStream.write((((HibernateDTO) extracts).getBookingAgentCode().toString() + "\t").getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
					}

					outputStream.write((orignAentCode + "\t").getBytes());
					outputStream.write((destAentCode + "\t").getBytes());
					if (((HibernateDTO) extracts).getJob() != null) {
						outputStream.write((((HibernateDTO) extracts).getJob().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getRouting() != null) {
						outputStream.write((((HibernateDTO) extracts).getRouting().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getServiceType() != null) {
						outputStream.write((((HibernateDTO) extracts).getServiceType().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getMode() != null) {
						outputStream.write((((HibernateDTO) extracts).getMode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}

					if (((HibernateDTO) extracts).getCommodity() != null) {
						outputStream.write((((HibernateDTO) extracts).getCommodity().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					String firstName = new String("");
					String lastName = new String("");
					if (((HibernateDTO) extracts).getFirstName() != null) {
						firstName = ((HibernateDTO) extracts).getFirstName().toString();

					} else {
						firstName = new String("");
					}
					if (((HibernateDTO) extracts).getLastName() != null) {
						lastName = ((HibernateDTO) extracts).getLastName().toString();

					} else {
						lastName = new String("");
					}
					outputStream.write((lastName + ",   " + firstName + "\t").getBytes());

					if (((HibernateDTO) extracts).getOriginCity() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginCity().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getOriginCountryCode() != null) {
						outputStream.write((((HibernateDTO) extracts).getOriginCountryCode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationCity() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationCity().toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getDestinationCountryCode() != null) {
						outputStream.write((((HibernateDTO) extracts).getDestinationCountryCode().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualGrossWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualGrossWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getActualNetWeight() != null) {
						outputStream.write((((HibernateDTO) extracts).getActualNetWeight().toString() + "\t").getBytes());
					} else {

						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
						StringBuilder date = new StringBuilder(format.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write(("'" + date.toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat formatsYear = new SimpleDateFormat("yyyy");
						StringBuilder year = new StringBuilder(formatsYear.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write((year.toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (((HibernateDTO) extracts).getBeginLoad() != null) {
						SimpleDateFormat formatsMonth = new SimpleDateFormat("MM");
						StringBuilder month = new StringBuilder(formatsMonth.format(((HibernateDTO) extracts).getBeginLoad()));
						outputStream.write((month.toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					// for Weight==========================================================================
					outputStream.write("\t".getBytes());

					//BOOKER,BA GRP,OA,OA GRP,DA,DA GRP======================================================================
					outputStream.write("\t".getBytes());//BOOKER
					outputStream.write("\t".getBytes());//BA GRP
					outputStream.write("\t".getBytes());//OA
					outputStream.write("\t".getBytes());//OA GRP
					outputStream.write("\t".getBytes());//DA
					outputStream.write("\t".getBytes());//DA GRP

					if (bA_SSCW.equalsIgnoreCase("000061")) //OA GIVEN AND DA GIVEN
					{
						if (((HibernateDTO) extracts).getOriginAgent() != null) {
							outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
						} else {

							outputStream.write("\t".getBytes());
						}
						if (((HibernateDTO) extracts).getDestinationAgent() != null) {
							outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
						} else {

							outputStream.write("\t".getBytes());
						}
					} else {

						outputStream.write("\t".getBytes());
						outputStream.write("\t".getBytes());
					}

					// OA GIVEN AMT,DA GIVEN AMT,OA_GIVEN_WT,DA_GIVEN_WT,OA_GIVEN_NO,DA_GIVEN_NO=============================================================
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					if (bA_SSCW.equalsIgnoreCase("000061")) //OA RECEIVED AND DA RECEIVED
					{

						outputStream.write("\t".getBytes());
						outputStream.write("\t".getBytes());
					} else {
						if (((HibernateDTO) extracts).getOriginAgent() != null) {
							outputStream.write((((HibernateDTO) extracts).getOriginAgent().toString() + "\t").getBytes());
						} else {

							outputStream.write("\t".getBytes());
						}
						if (((HibernateDTO) extracts).getDestinationAgent() != null) {
							outputStream.write((((HibernateDTO) extracts).getDestinationAgent().toString() + "\t").getBytes());
						} else {

							outputStream.write("\t".getBytes());
						}

					}
					//OA PMT,DA PMT,OA_RECD_WT,DA_RECD_WT,OA_RECD_NO,DA_RECD_NO============================================
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("\t".getBytes());

					break outer;
				}

				else {

				}
			}
			outputStream.write("\r\n".getBytes());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void storageAnalysis() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(extractVal == null || (!extractVal.equals("true")) ){
		StringBuffer jobBuffer = new StringBuffer();
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}

		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}

		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}

		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}

		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}

		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}

		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}

		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}

		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
		
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
		if (conditionC1.equals("") && conditionC12.equals("") && conditionC13.equals("") && conditionC14.equals("") && conditionC15.equals("") && conditionC16.equals("")
				&& conditionC17.equals("") && conditionC18.equals("") && conditionC19.equals("") && conditionC120.equals("") && conditionC121.equals("")
				&& conditionC3.equals("") && conditionC31.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && conditionC115.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")
				&&payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status in ('CLSD','CNCL') AND s.status is not null");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'  AND  s.status not in ('CNCL') AND s.status is not null");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'");
			}

		} else {
			query.append("  where  ");
			if (conditionC1.equals("") || conditionC1 == null) {

			} else {
				query.append(" s.job  ");
				isAnd = true;
				if (conditionC1.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}
			}
			// create date
			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (conditionC77.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {
				}
				isAnd = true;
			}

			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (conditionC78.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {
				}
				isAnd = true;
			}
			//create date end
			if (conditionC12.equals("") || conditionC12 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (conditionC12.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;
			}

			if (conditionC13.equals("") || conditionC13 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (conditionC13.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}

			if (conditionC14.equals("") || conditionC14 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (conditionC14.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}

			if (conditionC15.equals("") || conditionC15 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (conditionC15.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (conditionC16.equals("") || conditionC16 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (conditionC16.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (conditionC17.equals("") || conditionC17 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (conditionC17.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (conditionC18.equals("") || conditionC18 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (conditionC18.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (conditionC19.equals("") || conditionC19 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (conditionC19.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (conditionC120.equals("") || conditionC120 == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (conditionC120.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (conditionC121.equals("") || conditionC121 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (conditionC121.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (conditionC3.equals("") || conditionC3 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (conditionC3.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (conditionC31.equals("") || conditionC31 == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (conditionC31.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (conditionC21.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}
			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (conditionC22.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (conditionC211.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (conditionC222.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (conditionC233.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (conditionC244.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (conditionC200.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (conditionC201.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (conditionC255.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}

				if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (conditionC266.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if ( payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (conditionC300.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (conditionC301.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (conditionC302.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (conditionC303.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&& (!(postingPeriodEndDates.toString().equals("")))) {
				if (isAnd == true) {
					query.append("  AND ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (conditionC304.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  AND ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (conditionC305.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (conditionC277.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (conditionC288.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (storageBeginDates.toString().equals("") || conditionC311 == null) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (conditionC311.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			
			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (conditionC322.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (conditionC333.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  AND  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (conditionC344.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					//query.append("  AND (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') AND ('"+endDates+"'))"+" AND s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (conditionC115 == null || conditionC115.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}

				if (conditionC115.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CLSD','CNCL','DWND','DWNLD') AND s.status is not null");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status  in ('CLSD','CNCL') AND s.status is not null");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   AND s.corpId='" + sessionCorpID + "' AND  s.status not in ('CNCL') AND s.status is not null");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  AND s.corpId='" + sessionCorpID + "'");
			}
		}

		//System.out.println("\n\n Query---->>" + query);
		
		
//String queryExtract=extractManager.saveForExtract(query.toString());
		
String check= getConditionC79();
if(check != null && (check.equals("trueeee"))){
extractQueryFile = new ExtractQueryFile();
extractQueryFile.setUserId(getRequest().getRemoteUser());
extractQueryFile.setCreatedon(new Date());
extractQueryFile.setCreatedby(getRequest().getRemoteUser());
extractQueryFile.setCorpID(sessionCorpID);
extractQueryFile.setExtractType("storageAnalysis");
extractQueryFile.setPublicPrivateFlag(getPublicPrivateFlag());
extractQueryFile.setQueryName(getQueryName());
extractQueryFile.setQueryCondition(query.toString());
extractQueryFile.setModifiedby(getRequest().getRemoteUser());
extractQueryFile.setModifiedon(new Date());
extractManager.save(extractQueryFile);
String key = "storageAnalysis Query is saved";
saveMessage(getText(key));
setConditionC79("");
setConditionC80("");
edit();

} 	
		List storagelist = serviceOrderManager.storageAnalysis(query.toString(),sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("StorageBillingAnalysis");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + storagelist.size() + "\t is extracted");
		
		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST NAME\t".getBytes());
		outputStream.write("MI\t".getBytes());
		outputStream.write("SSN\t".getBytes());
		outputStream.write("Office#\t".getBytes());
		outputStream.write("Oroginal TSR#\t".getBytes());
		outputStream.write("Storage Lot#\t".getBytes());
		outputStream.write("Unit of Weight\t".getBytes());
		outputStream.write("Date In\t".getBytes());
		outputStream.write("Qty\t".getBytes());
		outputStream.write("Monthly Measurement\t".getBytes());
		outputStream.write("Rate\t".getBytes());
		outputStream.write("Months to Bill\t".getBytes());
		outputStream.write("# of Billable Days\t".getBytes());
		outputStream.write("Rate Charge\t".getBytes());
		outputStream.write("Acutal Revenue\t".getBytes());
		outputStream.write("Invoice#\t".getBytes());
		outputStream.write("Invoice Date\t".getBytes());
		outputStream.write("Charge Code \t".getBytes());
		outputStream.write("Actual Sent to Client\t".getBytes());
		outputStream.write("Billing ID\t".getBytes());
		outputStream.write("Invoice Description\t".getBytes()); 
		outputStream.write("\r\n".getBytes());
		Iterator it = storagelist.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();

			//ShipNumber
			if (((DTOStorageAnalysis) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((DTOStorageAnalysis) extracts).getShipNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// shipper
			String firstName = new String("");
			String lastName = new String("");

			if (((DTOStorageAnalysis) extracts).getFirstName() != null) {
				firstName = ((DTOStorageAnalysis) extracts).getFirstName().toString();
			} else {
				firstName = new String("");
			}
			if (((DTOStorageAnalysis) extracts).getLastname() != null) {
				lastName = ((DTOStorageAnalysis) extracts).getLastname().toString();
			} else {
				lastName = new String("");
			}

			if (!firstName.equals("") && !lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write((lastName  + "\t").getBytes());

			}

			if (firstName.equals("") && lastName.equals("")) {
				outputStream.write("\t".getBytes());

			}

			if (!firstName.equals("") && lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write(("" + "\t").getBytes());

			}

			if (firstName.equals("") && !lastName.equals("")) {
				outputStream.write(("" + "\t").getBytes());
				outputStream.write((lastName + "\t").getBytes());

			}

			//MiddleInitial
			if (((DTOStorageAnalysis) extracts).getMi() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getMi().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Social Security Number
			if (((DTOStorageAnalysis) extracts).getSocialSecurityNumber() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getSocialSecurityNumber().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Bill To Ref
			if (((DTOStorageAnalysis) extracts).getBillToReference() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getBillToReference().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Bill To Auth
			if (((DTOStorageAnalysis) extracts).getBillToAuthority() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getBillToAuthority().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Reg num
			if (((DTOStorageAnalysis) extracts).getRegistrationNumber() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Act Net
			if (((DTOStorageAnalysis) extracts).getActualNetWeight() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Begin Load
			if (((DTOStorageAnalysis) extracts).getBeginLoad() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((DTOStorageAnalysis) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//On Hand
			if (((DTOStorageAnalysis) extracts).getOnHand() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getOnHand().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Storage Measurement
			if (((DTOStorageAnalysis) extracts).getStorageMeasurement() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getStorageMeasurement().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Post Grate
			if (((DTOStorageAnalysis) extracts).getPostGrate() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getPostGrate().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Cycle
			if (((DTOStorageAnalysis) extracts).getCycle() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getCycle().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Storage Days
			if (((DTOStorageAnalysis) extracts).getStorageDays() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getStorageDays().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Storage Per Month
			if (((DTOStorageAnalysis) extracts).getStoragePerMonth() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getStoragePerMonth().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Acutal Revenue Amount
			if (((DTOStorageAnalysis) extracts).getActualRevenue() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getActualRevenue().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Rec Inv No.
			if (((DTOStorageAnalysis) extracts).getRecInvoiceNumber() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getRecInvoiceNumber().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//Rec Inv Date
			if (((DTOStorageAnalysis) extracts).getRecInvoiceDate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((DTOStorageAnalysis) extracts).getRecInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			if (((DTOStorageAnalysis) extracts).getChargeCode() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getChargeCode().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			} 
			
			if (((DTOStorageAnalysis) extracts).getSendActualToClient() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((DTOStorageAnalysis) extracts).getSendActualToClient()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			} 
			if (((DTOStorageAnalysis) extracts).getBillingId() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getBillingId().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((DTOStorageAnalysis) extracts).getDescription() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getDescription().toString().replaceAll("'", "")).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			outputStream.write("\r\n".getBytes());
		}
		
		
		
		
	}	
		else{
				extractQueryFile=extractManager.get(id);
				String query=extractQueryFile.getQueryCondition();
		List storagelist = serviceOrderManager.storageAnalysis(query.toString(),sessionCorpID);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("StorageBillingAnalysis");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + storagelist.size() + "\t is extracted");
		
		outputStream.write("SHIPNUMBER\t".getBytes());
		outputStream.write("FIRST NAME\t".getBytes());
		outputStream.write("LAST LAME\t".getBytes());
		outputStream.write("MI\t".getBytes());
		outputStream.write("SSN\t".getBytes());
		outputStream.write("Office#\t".getBytes());
		outputStream.write("Oroginal TSR#\t".getBytes());
		outputStream.write("Storage Lot#\t".getBytes());
		outputStream.write("Unit of Weight\t".getBytes());
		outputStream.write("Date In\t".getBytes());
		outputStream.write("Qty\t".getBytes());
		outputStream.write("Monthly Measurement\t".getBytes());
		outputStream.write("Rate\t".getBytes());
		outputStream.write("Months to Bill\t".getBytes());
		outputStream.write("# of Billable Days\t".getBytes());
		outputStream.write("Rate Charge\t".getBytes());
		outputStream.write("Acutal Revenue\t".getBytes());
		outputStream.write("Invoice#\t".getBytes());
		outputStream.write("Invoice Date\t".getBytes());
		outputStream.write("Charge Code \t".getBytes());
		outputStream.write("Actual Sent to Client\t".getBytes());
		outputStream.write("Billing ID\t".getBytes());
		outputStream.write("Invoice Description\t".getBytes()); 
		outputStream.write("\r\n".getBytes());
		Iterator it = storagelist.iterator();

		while (it.hasNext()) {
			Object extracts = (Object) it.next();

			//ShipNumber
			if (((DTOStorageAnalysis) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((DTOStorageAnalysis) extracts).getShipNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// shipper
			String firstName = new String("");
			String lastName = new String("");

			if (((DTOStorageAnalysis) extracts).getFirstName() != null) {
				firstName = ((DTOStorageAnalysis) extracts).getFirstName().toString();
			} else {
				firstName = new String("");
			}
			if (((DTOStorageAnalysis) extracts).getLastname() != null) {
				lastName = ((DTOStorageAnalysis) extracts).getLastname().toString();
			} else {
				lastName = new String("");
			}

			if (!firstName.equals("") && !lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write((lastName  + "\t").getBytes());

			}

			if (firstName.equals("") && lastName.equals("")) {
				outputStream.write("\t".getBytes());

			}

			if (!firstName.equals("") && lastName.equals("")) {
				outputStream.write((firstName + "\t").getBytes());
				outputStream.write(("" + "\t").getBytes());

			}

			if (firstName.equals("") && !lastName.equals("")) {
				outputStream.write(("" + "\t").getBytes());
				outputStream.write((lastName + "\t").getBytes());

			}

			//MiddleInitial
			if (((DTOStorageAnalysis) extracts).getMi() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getMi().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Social Security Number
			if (((DTOStorageAnalysis) extracts).getSocialSecurityNumber() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getSocialSecurityNumber().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Bill To Ref
			if (((DTOStorageAnalysis) extracts).getBillToReference() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getBillToReference().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Bill To Auth
			if (((DTOStorageAnalysis) extracts).getBillToAuthority() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getBillToAuthority().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Reg num
			if (((DTOStorageAnalysis) extracts).getRegistrationNumber() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getRegistrationNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Act Net
			if (((DTOStorageAnalysis) extracts).getActualNetWeight() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getActualNetWeight().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Begin Load
			if (((DTOStorageAnalysis) extracts).getBeginLoad() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((DTOStorageAnalysis) extracts).getBeginLoad()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//On Hand
			if (((DTOStorageAnalysis) extracts).getOnHand() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getOnHand().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Storage Measurement
			if (((DTOStorageAnalysis) extracts).getStorageMeasurement() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getStorageMeasurement().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Post Grate
			if (((DTOStorageAnalysis) extracts).getPostGrate() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getPostGrate().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Cycle
			if (((DTOStorageAnalysis) extracts).getCycle() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getCycle().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Storage Days
			if (((DTOStorageAnalysis) extracts).getStorageDays() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getStorageDays().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Storage Per Month
			if (((DTOStorageAnalysis) extracts).getStoragePerMonth() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getStoragePerMonth().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			//Acutal Revenue Amount
			if (((DTOStorageAnalysis) extracts).getActualRevenue() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getActualRevenue().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}

			// Rec Inv No.
			if (((DTOStorageAnalysis) extracts).getRecInvoiceNumber() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getRecInvoiceNumber().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			//Rec Inv Date
			if (((DTOStorageAnalysis) extracts).getRecInvoiceDate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((DTOStorageAnalysis) extracts).getRecInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((DTOStorageAnalysis) extracts).getChargeCode() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getChargeCode().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			} 
			
			if (((DTOStorageAnalysis) extracts).getSendActualToClient() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(((DTOStorageAnalysis) extracts).getSendActualToClient()));
				outputStream.write((date.toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((DTOStorageAnalysis) extracts).getBillingId() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getBillingId().toString()).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			} 
			if (((DTOStorageAnalysis) extracts).getDescription() != null) {
				outputStream.write((((DTOStorageAnalysis) extracts).getDescription().toString().replaceAll("'", "")).getBytes());
				outputStream.write("\t".getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			
			outputStream.write("\r\n".getBytes());
		}
		
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	public String storageExtractEdit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	public void storageExtractCredit() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer stoRecInvoiceDates = new StringBuffer();	
		stoRecInvoiceDates = new StringBuffer(formats.format(stoRecInvoiceDate));
		List stoExtractCreditList = serviceOrderManager.storageExtractCredit( sessionCorpID, stoRecInvoiceDates.toString(),companyDivision);
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("StorageInvoiceExtract");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + stoExtractCreditList.size() + "\t is extracted");
		outputStream.write("Invoice #\t".getBytes());
		outputStream.write("Name\t".getBytes());
		outputStream.write("ShipNumber\t".getBytes());
		outputStream.write("ActualRevenue\t".getBytes());
		outputStream.write("Description\t".getBytes());
		outputStream.write("CCType\t".getBytes());
		outputStream.write("CCNumber\t".getBytes());
		outputStream.write("Expires\t".getBytes());
		outputStream.write("CCFlag\t".getBytes());
		outputStream.write("BillToCode\t".getBytes());
		outputStream.write("BillToName\t".getBytes()); 
		outputStream.write("Address\t".getBytes());
		outputStream.write("ZIP\t".getBytes()); 
		outputStream.write("\r\n".getBytes());
		Iterator it = stoExtractCreditList.iterator();
		while (it.hasNext()) {
			Object extracts = (Object) it.next();  
			if (((stoExtractDTO) extracts).getRecInvoiceNumber() != null) {
				outputStream.write(( ((stoExtractDTO) extracts).getRecInvoiceNumber().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getName() != null) {
				outputStream.write((((stoExtractDTO) extracts).getName().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			} 
			if (((stoExtractDTO) extracts).getShipNumber() != null) {
				outputStream.write(("'" + ((stoExtractDTO) extracts).getShipNumber().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getActualRevenue() != null) {
				outputStream.write((((stoExtractDTO) extracts).getActualRevenue().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getDescription() != null) {
				outputStream.write((((stoExtractDTO) extracts).getDescription().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getCctype() != null) {
				outputStream.write(( ((stoExtractDTO) extracts).getCctype().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getCcNumber() != null) {
				outputStream.write(("'" + ((stoExtractDTO) extracts).getCcNumber().toString() + "\t").getBytes()); 
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getExpires() != null) {
				outputStream.write(("'" + ((stoExtractDTO) extracts).getExpires().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getCcFlag() != null) {
				outputStream.write((((stoExtractDTO) extracts).getCcFlag().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getBillToCode() != null) {
				outputStream.write(("'" + ((stoExtractDTO) extracts).getBillToCode().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getBillToName() != null) {
				outputStream.write((((stoExtractDTO) extracts).getBillToName().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getBillingaddress1() != null) {
				outputStream.write((((stoExtractDTO) extracts).getBillingaddress1().toString().replaceAll("'", "") + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			if (((stoExtractDTO) extracts).getBillingzip() != null) {
				outputStream.write(("'"+((stoExtractDTO) extracts).getBillingzip().toString() + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
			outputStream.write("\r\n".getBytes());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	
	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	

	

	 

	

	 

	public String getJobTypes() {
		return jobTypes;
	}

	public void setJobTypes(String jobTypes) {
		this.jobTypes = jobTypes;
	}

	public String getRoutingTypes() {
		return routingTypes;
	}

	public void setRoutingTypes(String routingTypes) {
		this.routingTypes = routingTypes;
	}

	

	

	 
	

	

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	 

	

	

	public String getActualVolumeType() {
		return actualVolumeType;
	}

	public void setActualVolumeType(String actualVolumeType) {
		this.actualVolumeType = actualVolumeType;
	}

	public String getActualWgtType() {
		return actualWgtType;
	}

	public void setActualWgtType(String actualWgtType) {
		this.actualWgtType = actualWgtType;
	}

	public String getBillToCodeType() {
		return billToCodeType;
	}

	public void setBillToCodeType(String billToCodeType) {
		this.billToCodeType = billToCodeType;
	}

	public String getBookingCodeType() {
		return bookingCodeType;
	}

	public void setBookingCodeType(String bookingCodeType) {
		this.bookingCodeType = bookingCodeType;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public String getCoordinatorType() {
		return coordinatorType;
	}

	public void setCoordinatorType(String coordinatorType) {
		this.coordinatorType = coordinatorType;
	}

	public Date getCreatedOnType() {
		return createdOnType;
	}

	public void setCreatedOnType(Date createdOnType) {
		this.createdOnType = createdOnType;
	}

	public String getDACountryType() {
		return dACountryType;
	}

	public void setDACountryType(String countryType) {
		dACountryType = countryType;
	}

	public String getModeType() {
		return modeType;
	}

	public void setModeType(String modeType) {
		this.modeType = modeType;
	}

	public String getOACountryType() {
		return oACountryType;
	}

	public void setOACountryType(String countryType) {
		oACountryType = countryType;
	}

	public String getPackModeType() {
		return packModeType;
	}

	public void setPackModeType(String packModeType) {
		this.packModeType = packModeType;
	}

	public String getSalesmanType() {
		return salesmanType;
	}

	public void setSalesmanType(String salesmanType) {
		this.salesmanType = salesmanType;
	}

	public String getConditionC1() {
		return conditionC1;
	}

	public void setConditionC1(String conditionC1) {
		this.conditionC1 = conditionC1;
	}

	public String getConditionC21() {
		return conditionC21;
	}

	public void setConditionC21(String conditionC21) {
		this.conditionC21 = conditionC21;
	}

	public String getConditionC22() {
		return conditionC22;
	}

	public void setConditionC22(String conditionC22) {
		this.conditionC22 = conditionC22;
	}

	public String getConditionC3() {
		return conditionC3;
	}

	public void setConditionC3(String conditionC3) {
		this.conditionC3 = conditionC3;
	}

	public String getConditionC12() {
		return conditionC12;
	}

	public void setConditionC12(String conditionC12) {
		this.conditionC12 = conditionC12;
	}

	public String getConditionC120() {
		return conditionC120;
	}

	public void setConditionC120(String conditionC120) {
		this.conditionC120 = conditionC120;
	}

	public String getConditionC121() {
		return conditionC121;
	}

	public void setConditionC121(String conditionC121) {
		this.conditionC121 = conditionC121;
	}

	public String getConditionC122() {
		return conditionC122;
	}

	public void setConditionC122(String conditionC122) {
		this.conditionC122 = conditionC122;
	}

	public String getConditionC13() {
		return conditionC13;
	}

	public void setConditionC13(String conditionC13) {
		this.conditionC13 = conditionC13;
	}

	public String getConditionC14() {
		return conditionC14;
	}

	public void setConditionC14(String conditionC14) {
		this.conditionC14 = conditionC14;
	}

	public String getConditionC15() {
		return conditionC15;
	}

	public void setConditionC15(String conditionC15) {
		this.conditionC15 = conditionC15;
	}

	public String getConditionC16() {
		return conditionC16;
	}

	public void setConditionC16(String conditionC16) {
		this.conditionC16 = conditionC16;
	}

	public String getConditionC17() {
		return conditionC17;
	}

	public void setConditionC17(String conditionC17) {
		this.conditionC17 = conditionC17;
	}

	public String getConditionC18() {
		return conditionC18;
	}

	public void setConditionC18(String conditionC18) {
		this.conditionC18 = conditionC18;
	}

	public String getConditionC19() {
		return conditionC19;
	}

	public void setConditionC19(String conditionC19) {
		this.conditionC19 = conditionC19;
	}

	public String getConditionC31() {
		return conditionC31;
	}

	public void setConditionC31(String conditionC31) {
		this.conditionC31 = conditionC31;
	}

	public Long getTimeDiff() {
		return timeDiff;
	}

	public void setTimeDiff(Long timeDiff) {
		this.timeDiff = timeDiff;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public List getCompanyCodeList() {
		return companyCodeList;
	}

	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}

	public void setCompanyDivisionManager(CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public String getCompanyDivisionFlag() {
		return companyDivisionFlag;
	}

	public void setCompanyDivisionFlag(String companyDivisionFlag) {
		this.companyDivisionFlag = companyDivisionFlag;
	}

	public String getConditionC115() {
		return conditionC115;
	}

	public void setConditionC115(String conditionC115) {
		this.conditionC115 = conditionC115;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Date getBeginDateW() {
		return beginDateW;
	}

	public void setBeginDateW(Date beginDateW) {
		this.beginDateW = beginDateW;
	}

	public Date getEndDateW() {
		return endDateW;
	}

	public void setEndDateW(Date endDateW) {
		this.endDateW = endDateW;
	}

	public String getServiceW() {
		return serviceW;
	}

	public void setServiceW(String serviceW) {
		this.serviceW = serviceW;
	}

	public String getTargetActualW() {
		return targetActualW;
	}

	public void setTargetActualW(String targetActualW) {
		this.targetActualW = targetActualW;
	}

	public String getWarehouseW() {
		return warehouseW;
	}

	public void setWarehouseW(String warehouseW) {
		this.warehouseW = warehouseW;
	}

	public List getImfEstimate() {
		return imfEstimate;
	}

	public void setImfEstimate(List imfEstimate) {
		this.imfEstimate = imfEstimate;
	}

	public String getConditionA1() {
		return conditionA1;
	}

	public void setConditionA1(String conditionA1) {
		this.conditionA1 = conditionA1;
	}

	public String getConditionA2() {
		return conditionA2;
	}

	public void setConditionA2(String conditionA2) {
		this.conditionA2 = conditionA2;
	}

	public String getConditionA3() {
		return conditionA3;
	}

	public void setConditionA3(String conditionA3) {
		this.conditionA3 = conditionA3;
	}

	public String getConditionA4() {
		return conditionA4;
	}

	public void setConditionA4(String conditionA4) {
		this.conditionA4 = conditionA4;
	}

	public String getConditionA5() {
		return conditionA5;
	}

	public void setConditionA5(String conditionA5) {
		this.conditionA5 = conditionA5;
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public Date getStoragedate() {
		return storagedate;
	}

	public void setStoragedate(Date storagedate) {
		this.storagedate = storagedate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getConditionC77() {
		return conditionC77;
	}

	public void setConditionC77(String conditionC77) {
		this.conditionC77 = conditionC77;
	}

	public Date getToCreateDate() {
		return toCreateDate;
	}

	public void setToCreateDate(Date toCreateDate) {
		this.toCreateDate = toCreateDate;
	}

	public String getConditionC78() {
		return conditionC78;
	}

	public void setConditionC78(String conditionC78) {
		this.conditionC78 = conditionC78;
	}

	public Date getDeliveryTBeginDate() {
		return deliveryTBeginDate;
	}

	public void setDeliveryTBeginDate(Date deliveryTBeginDate) {
		this.deliveryTBeginDate = deliveryTBeginDate;
	}

	public String getConditionC211() {
		return conditionC211;
	}

	public void setConditionC211(String conditionC211) {
		this.conditionC211 = conditionC211;
	}

	public String getConditionC222() {
		return conditionC222;
	}

	public void setConditionC222(String conditionC222) {
		this.conditionC222 = conditionC222;
	}

	public String getConditionC233() {
		return conditionC233;
	}

	public void setConditionC233(String conditionC233) {
		this.conditionC233 = conditionC233;
	}

	public String getConditionC244() {
		return conditionC244;
	}

	public void setConditionC244(String conditionC244) {
		this.conditionC244 = conditionC244;
	}

	public Date getDeliveryActBeginDate() {
		return deliveryActBeginDate;
	}

	public void setDeliveryActBeginDate(Date deliveryActBeginDate) {
		this.deliveryActBeginDate = deliveryActBeginDate;
	}

	public Date getDeliveryActEndDate() {
		return deliveryActEndDate;
	}

	public void setDeliveryActEndDate(Date deliveryActEndDate) {
		this.deliveryActEndDate = deliveryActEndDate;
	}

	public Date getDeliveryTEndDate() {
		return deliveryTEndDate;
	}

	public void setDeliveryTEndDate(Date deliveryTEndDate) {
		this.deliveryTEndDate = deliveryTEndDate;
	}

	public Date getLoadTgtEndDate() {
		return loadTgtEndDate;
	}

	public void setLoadTgtEndDate(Date loadTgtEndDate) {
		this.loadTgtEndDate = loadTgtEndDate;
	}

	public Date getLoadTgtBeginDate() {
		return loadTgtBeginDate;
	}

	public void setLoadTgtBeginDate(Date loadTgtBeginDate) {
		this.loadTgtBeginDate = loadTgtBeginDate;
	}

	public String getConditionC200() {
		return conditionC200;
	}

	public void setConditionC200(String conditionC200) {
		this.conditionC200 = conditionC200;
	}

	public String getConditionC201() {
		return conditionC201;
	}

	public void setConditionC201(String conditionC201) {
		this.conditionC201 = conditionC201;
	}

	public String getConditionC255() {
		return conditionC255;
	}

	public void setConditionC255(String conditionC255) {
		this.conditionC255 = conditionC255;
	}

	public String getConditionC266() {
		return conditionC266;
	}

	public void setConditionC266(String conditionC266) {
		this.conditionC266 = conditionC266;
	}

	public Date getInvPostingBeginDate() {
		return invPostingBeginDate;
	}

	public void setInvPostingBeginDate(Date invPostingBeginDate) {
		this.invPostingBeginDate = invPostingBeginDate;
	}

	public Date getInvPostingEndDate() {
		return invPostingEndDate;
	}

	public void setInvPostingEndDate(Date invPostingEndDate) {
		this.invPostingEndDate = invPostingEndDate;
	}

	public String getConditionC277() {
		return conditionC277;
	}

	public void setConditionC277(String conditionC277) {
		this.conditionC277 = conditionC277;
	}

	public String getConditionC288() {
		return conditionC288;
	}

	public void setConditionC288(String conditionC288) {
		this.conditionC288 = conditionC288;
	}

	public Date getInvoicingBeginDate() {
		return invoicingBeginDate;
	}

	public void setInvoicingBeginDate(Date invoicingBeginDate) {
		this.invoicingBeginDate = invoicingBeginDate;
	}

	public Date getInvoicingEndDate() {
		return invoicingEndDate;
	}

	public void setInvoicingEndDate(Date invoicingEndDate) {
		this.invoicingEndDate = invoicingEndDate;
	}

	public class DTO {
		private Object etd;

		private Object ata;

		public Object getAta() {
			return ata;
		}

		public void setAta(Object ata) {
			this.ata = ata;
		}

		public Object getEtd() {
			return etd;
		}

		public void setEtd(Object etd) {
			this.etd = etd;
		}
	}

	public String getConditionC311() {
		return conditionC311;
	}

	public void setConditionC311(String conditionC311) {
		this.conditionC311 = conditionC311;
	}

	public String getConditionC322() {
		return conditionC322;
	}

	public void setConditionC322(String conditionC322) {
		this.conditionC322 = conditionC322;
	}

	public Date getStorageBeginDate() {
		return storageBeginDate;
	}

	public void setStorageBeginDate(Date storageBeginDate) {
		this.storageBeginDate = storageBeginDate;
	}

	public Date getStorageEndDate() {
		return storageEndDate;
	}

	public void setStorageEndDate(Date storageEndDate) {
		this.storageEndDate = storageEndDate;
	}

	public String getConditionC333() {
		return conditionC333;
	}

	public void setConditionC333(String conditionC333) {
		this.conditionC333 = conditionC333;
	}

	public String getConditionC344() {
		return conditionC344;
	}

	public void setConditionC344(String conditionC344) {
		this.conditionC344 = conditionC344;
	}

	public Date getRevenueRecognitionBeginDate() {
		return revenueRecognitionBeginDate;
	}

	public void setRevenueRecognitionBeginDate(Date revenueRecognitionBeginDate) {
		this.revenueRecognitionBeginDate = revenueRecognitionBeginDate;
	}

	public Date getRevenueRecognitionEndDate() {
		return revenueRecognitionEndDate;
	}

	public void setRevenueRecognitionEndDate(Date revenueRecognitionEndDate) {
		this.revenueRecognitionEndDate = revenueRecognitionEndDate;
	}

	public String getAccountInterface() {
		return accountInterface;
	}

	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

	public String getConditionC300() {
		return conditionC300;
	}

	public void setConditionC300(String conditionC300) {
		this.conditionC300 = conditionC300;
	}

	public String getConditionC301() {
		return conditionC301;
	}

	public void setConditionC301(String conditionC301) {
		this.conditionC301 = conditionC301;
	}

	public Date getPayPostingBeginDate() {
		return payPostingBeginDate;
	}

	public void setPayPostingBeginDate(Date payPostingBeginDate) {
		this.payPostingBeginDate = payPostingBeginDate;
	}

	public Date getPayPostingEndDate() {
		return payPostingEndDate;
	}

	public void setPayPostingEndDate(Date payPostingEndDate) {
		this.payPostingEndDate = payPostingEndDate;
	}

	public Date getReceivedBeginDate() {
		return receivedBeginDate;
	}

	public void setReceivedBeginDate(Date receivedBeginDate) {
		this.receivedBeginDate = receivedBeginDate;
	}

	public Date getReceivedEndDate() {
		return receivedEndDate;
	}

	public void setReceivedEndDate(Date receivedEndDate) {
		this.receivedEndDate = receivedEndDate;
	}

	public String getConditionC302() {
		return conditionC302;
	}

	public void setConditionC302(String conditionC302) {
		this.conditionC302 = conditionC302;
	}

	public String getConditionC303() {
		return conditionC303;
	}

	public void setConditionC303(String conditionC303) {
		this.conditionC303 = conditionC303;
	}

	public Date getPostingPeriodBeginDate() {
		return postingPeriodBeginDate;
	}

	public void setPostingPeriodBeginDate(Date postingPeriodBeginDate) {
		this.postingPeriodBeginDate = postingPeriodBeginDate;
	}

	public Date getPostingPeriodEndDate() {
		return postingPeriodEndDate;
	}

	public void setPostingPeriodEndDate(Date postingPeriodEndDate) {
		this.postingPeriodEndDate = postingPeriodEndDate;
	}

	public String getConditionC304() {
		return conditionC304;
	}

	public void setConditionC304(String conditionC304) {
		this.conditionC304 = conditionC304;
	}

	public String getConditionC305() {
		return conditionC305;
	}

	public void setConditionC305(String conditionC305) {
		this.conditionC305 = conditionC305;
	}


	public void setExtractManager(ExtractQueryFileManager extractManager) {
		this.extractManager = extractManager;
	}

	public ExtractQueryFile getExtractQueryFile() {
		return extractQueryFile;
	}

	public void setExtractQueryFile(ExtractQueryFile extractQueryFile) {
		this.extractQueryFile = extractQueryFile;
	}

	public String getConditionC79() {
		return conditionC79;
	}

	public void setConditionC79(String conditionC79) {
		this.conditionC79 = conditionC79;
	} 
	public List getQueryList() {
		return queryList;
	}

	public void setQueryList(List queryList) {
		this.queryList = queryList;
	}

	public String getConditionC80() {
		return conditionC80;
	}

	public void setConditionC80(String conditionC80) {
		this.conditionC80 = conditionC80;
	}

	public String getConditionC81() {
		return conditionC81;
	}

	public void setConditionC81(String conditionC81) {
		this.conditionC81 = conditionC81;
	}

	public String getConditionC82() {
		return conditionC82;
	}

	public void setConditionC82(String conditionC82) {
		this.conditionC82 = conditionC82;
	}

	public String getExtractVal() {
		return extractVal;
	}

	public void setExtractVal(String extractVal) {
		this.extractVal = extractVal;
	}

	public String getPublicPrivateFlag() {
		return publicPrivateFlag;
	}

	public void setPublicPrivateFlag(String publicPrivateFlag) {
		this.publicPrivateFlag = publicPrivateFlag;
	}

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}

	public Date getStoRecInvoiceDate() {
		return stoRecInvoiceDate;
	}

	public void setStoRecInvoiceDate(Date stoRecInvoiceDate) {
		this.stoRecInvoiceDate = stoRecInvoiceDate;
	}

	public List getQueryNameList() {
		return queryNameList;
	}

	public void setQueryNameList(List queryNameList) {
		this.queryNameList = queryNameList;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public List getDeliveryExtract() {
		return deliveryExtract;
	}

	public void setDeliveryExtract(List deliveryExtract) {
		this.deliveryExtract = deliveryExtract;
	}

	public List getComponentMillitaryShipment() {
	    return componentMillitaryShipment;
	}

	public void setComponentMillitaryShipment(List componentMillitaryShipment) {
	    this.componentMillitaryShipment = componentMillitaryShipment;
	}

	public List getAvailChilds() {
		return availChilds;
	}

	public void setAvailChilds(List availChilds) {
		this.availChilds = availChilds;
	}

	 

	public PartnerPrivateManager getPartnerPrivateManager() {
		return partnerPrivateManager;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public List getChildList() {
		return childList;
	}

	public void setChildList(List childList) {
		this.childList = childList;
	}

	 

	public String getChildParentIdList() {
		return childParentIdList;
	}

	public void setChildParentIdList(String childParentIdList) {
		this.childParentIdList = childParentIdList;
	}

	public Date getFlat_Report_from() {
		return flat_Report_from;
	}

	public void setFlat_Report_from(Date flat_Report_from) {
		this.flat_Report_from = flat_Report_from;
	}

	public Date getFlat_Report_to() {
		return flat_Report_to;
	}

	public void setFlat_Report_to(Date flat_Report_to) {
		this.flat_Report_to = flat_Report_to;
	}

	public Date getClaim_Report_from() {
		return claim_Report_from;
	}

	public void setClaim_Report_from(Date claim_Report_from) {
		this.claim_Report_from = claim_Report_from;
	}

	public Date getClaim_Report_to() {
		return claim_Report_to;
	}

	public void setClaim_Report_to(Date claim_Report_to) {
		this.claim_Report_to = claim_Report_to;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
	public HSSFCellStyle[] setRowCellFormat(HSSFWorkbook hwb)
	{
		
		HSSFCellStyle cellStyleOdd = hwb.createCellStyle();
		cellStyleOdd.setFillForegroundColor(HSSFColor.WHITE.index);
		cellStyleOdd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				
		HSSFCellStyle cellStyleEven = hwb.createCellStyle();
		cellStyleEven.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyleEven.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		
		return new HSSFCellStyle[]{cellStyleOdd,cellStyleEven};
	}
	
	public HSSFWorkbook getClaimfileExtraction(List fileReport){
		HSSFWorkbook hwb=new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet("ClaimFileExtraction Sheet");
		HSSFCell cell = null;
		int cellLength = 0 ;
		HSSFCellStyle cellStyleHeader = hwb.createCellStyle();
		cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyleHeader.setFillForegroundColor(HSSFColor.RED.index);
		HSSFFont hssfFont = hwb.createFont();
		hssfFont.setColor((short)1);
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		hssfFont.setFontHeight((short)230);
		cellStyleHeader.setFont(hssfFont);
		cellStyleHeader.setWrapText(true);
		cellStyleHeader.setVerticalAlignment(cellStyleHeader.VERTICAL_TOP);
		HSSFRow rowhead=   sheet.createRow((short)0);
		String[] columnName = {"Client Name","Account","Service Order","Claim#","Loss ID#","From To","Handle by Insurer","Type","Cause","Cause by"
				,"Date Received","Claimed Amount","Claimed Currency","Paid Amount","Paid Currency","Claim Status","Status Date","Remark"};
		for(int i = 0 ; i < columnName.length ; i++){
			rowhead.setHeight((short)1000);
			cell = rowhead.createCell((short) i);
			cell.setCellValue(columnName[i]);
			cell.setCellStyle(cellStyleHeader);
			cellLength = 0;
			cellLength= columnName[i].length();
			sheet.setColumnWidth((short)i, (short)(cellLength + 4500));
			cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_CENTER);
		}
		
		short length = rowhead.getLastCellNum();
		HSSFCellStyle cellStyle, cellStyleArray[] = setRowCellFormat(hwb);
		int formatter = 2;
		int objectNumber = 0 ;
		for (int i = 1; i <= fileReport.size(); i++) {
			if(formatter%2 == 0){
				cellStyle = cellStyleArray[1];
				formatter++;
			}
			else{
				cellStyle = cellStyleArray[0];
				formatter++;
			}

			ExtractHibernateDTO extractHibernateDTOs = (ExtractHibernateDTO)fileReport.get(objectNumber++);
			HSSFRow row=   sheet.createRow((short)i);
			row.setHeight((short)500);
			short count = 0;
			if(count != rowhead.getLastCellNum()) {
				if(extractHibernateDTOs.getClientName_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getClientName_Claim().toString());
					cellLength= extractHibernateDTOs.getClientName_Claim().toString().length() ;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getAccount_Claim() != null){  
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getAccount_Claim().toString());
					cellLength= extractHibernateDTOs.getAccount_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getServiceOrder_Claim() != null){  
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getServiceOrder_Claim().toString());
					cellLength= extractHibernateDTOs.getServiceOrder_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getClaimNumber_Claim() != null){  
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getClaimNumber_Claim().toString());
					cellLength= extractHibernateDTOs.getClaimNumber_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getLossNumber1_Claim() != null){  
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getLossNumber1_Claim().toString());
					cellLength= extractHibernateDTOs.getLossNumber1_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getFromTo_Claim() != null){
//					String fromTo = extractHibernateDTOs.getOriginCountrycode().toString()+"->"+extractHibernateDTOs.getDestinationCountrycode().toString();
//					row.createCell((short) count++).setCellValue(fromTo);
					sheet.setColumnWidth(count, (short)(extractHibernateDTOs.getFromTo_Claim().toString().length() + 8000));
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getFromTo_Claim().toString());
					cell.setCellStyle(cellStyle);
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getHandleByInsurer_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getHandleByInsurer_Claim().toString());
					cellLength= extractHibernateDTOs.getHandleByInsurer_Claim().toString().length() + 1;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				
				if(extractHibernateDTOs.getType_Claim() != null){
					Map<String,String> lossMap = refMasterManager.findByParameter(sessionCorpID, "LOSS");
					cell = row.createCell((short) count++);
					cell.setCellValue((lossMap.get(extractHibernateDTOs.getType_Claim().toString())!=null?lossMap.get(extractHibernateDTOs.getType_Claim().toString()):""));
					cellLength= extractHibernateDTOs.getType_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getCause_Claim() != null ){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getCause_Claim().toString());
					cellLength= extractHibernateDTOs.getCause_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getCauseBy_Claim() != null){
					Map<String,String> damaged=refMasterManager.findByParameter(sessionCorpID, "DAMAGED");
					cell = row.createCell((short) count++);
					cell.setCellValue((damaged.get(extractHibernateDTOs.getCauseBy_Claim().toString()) != null)?damaged.get(extractHibernateDTOs.getCauseBy_Claim().toString()):"");
					cellLength= extractHibernateDTOs.getCauseBy_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				Date receivedClaim = (Date)extractHibernateDTOs.getDateReceived_Claim();
				if(receivedClaim!= null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(receivedClaim));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= date.toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getClaimAmt_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getClaimAmt_Claim().toString());
					cellLength= extractHibernateDTOs.getClaimAmt_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getClaimCurrency_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getClaimCurrency_Claim().toString());
					cellLength= extractHibernateDTOs.getClaimCurrency_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				
				if(extractHibernateDTOs.getPaidAmt_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getPaidAmt_Claim().toString());
					cellLength= extractHibernateDTOs.getPaidAmt_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getPaidCurrency_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getPaidCurrency_Claim().toString());
					cellLength= extractHibernateDTOs.getPaidCurrency_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				
				if(extractHibernateDTOs.getClaimStatus_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getClaimStatus_Claim().toString());
					cellLength= extractHibernateDTOs.getClaimStatus_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				Date statusDateClaim = (Date)extractHibernateDTOs.getStatusDt_Claim();
				if(statusDateClaim != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(statusDateClaim));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= date.toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getRemark_Claim() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getRemark_Claim().toString());
					cellLength= extractHibernateDTOs.getRemark_Claim().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				/*if(extractHibernateDTOs.getQuoteStatus() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getQuoteStatus().toString());
					cellLength= extractHibernateDTOs.getQuoteStatus().toString().length();
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}*/
			}
		}
		return hwb;
	}
	
	public HSSFWorkbook getFlatfileExtraction(List fileReport){
		HSSFWorkbook hwb=new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet("FlatFileExtraction Sheet");
		HSSFCell cell = null;
		int cellLength = 0 ;
		HSSFCellStyle cellStyleHeader = hwb.createCellStyle();
		cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyleHeader.setFillForegroundColor(HSSFColor.RED.index);
		HSSFFont hssfFont = hwb.createFont();
		hssfFont.setColor((short)1);
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		hssfFont.setFontHeight((short)230);
		cellStyleHeader.setFont(hssfFont);
		cellStyleHeader.setWrapText(true);
		cellStyleHeader.setVerticalAlignment(cellStyleHeader.VERTICAL_TOP);
		HSSFRow rowhead=   sheet.createRow((short)0);
		String[] columnName = {"Service Order","Account","Company","Move Initiator","Employee","Transport Type","From To","Origin City","Destination City"
				,"Authorization Received","Survey Date","Packing Date","Loading Date","Delivery Date","Invoice Date","Authorized Volume CBM"
				,"Estimated Volume CBM","Shipped Volume CBM","Distance km","Total Amount Invoiced EUR","Door to Door EUR","All Other additional Charges"
				,"Assignment Type","Recommendation","Pre move Services Rating","Origin Services Rating","Destination Services Rating","Over All Rating","Complaints"};
		for(int i = 0 ; i < columnName.length ; i++){
			rowhead.setHeight((short)1000);
			cell = rowhead.createCell((short) i);
			cell.setCellValue(columnName[i]);
			cell.setCellStyle(cellStyleHeader);
			cellLength = 0;
			sheet.setColumnWidth((short)i, (short)(columnName[i].length() + 5000));
			cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_CENTER);
		}
		short length = rowhead.getLastCellNum();
		HSSFCellStyle cellStyle, cellStyleArray[] = setRowCellFormat(hwb);
		int formatter = 2;
		int objectNumber = 0 ;
		for (int i = 1; i <= fileReport.size(); i++) {
			if(formatter%2 == 0){
				cellStyle = cellStyleArray[1];
				formatter++;
			}
			else{
				cellStyle = cellStyleArray[0];
				formatter++;
			}
			ExtractHibernateDTO extractHibernateDTOs = (ExtractHibernateDTO)fileReport.get(objectNumber++);
			HSSFRow row=   sheet.createRow((short)i);
			row.setHeight((short)500);
			short count = 0;
			if(count != rowhead.getLastCellNum()) {
				if(extractHibernateDTOs.getSequenceNumber() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getSequenceNumber().toString());
					cellLength= (extractHibernateDTOs.getSequenceNumber().toString().length() > cellLength)?extractHibernateDTOs.getSequenceNumber().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getPartnerCode() != null){  
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getPartnerCode().toString());
					cellLength= (extractHibernateDTOs.getPartnerCode().toString().length() > cellLength)?extractHibernateDTOs.getPartnerCode().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getBilltoCode() != null){  
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getBilltoCode().toString());
					cellLength= (extractHibernateDTOs.getBilltoCode().toString().length() > cellLength)?extractHibernateDTOs.getBilltoCode().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getAccountContact() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getAccountContact().toString());
					cellLength= (extractHibernateDTOs.getAccountContact().toString().length() > cellLength)?extractHibernateDTOs.getAccountContact().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getTransferee() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getTransferee().toString());
					cellLength= (extractHibernateDTOs.getTransferee().toString().length() > cellLength)?extractHibernateDTOs.getTransferee().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				
				if(extractHibernateDTOs.getMode() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getMode().toString());
					cellLength= (extractHibernateDTOs.getMode().toString().length() > cellLength)?extractHibernateDTOs.getMode().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getOriginCountrycode() != null && extractHibernateDTOs.getDestinationCountrycode() != null){
					String fromTo = extractHibernateDTOs.getOriginCountrycode().toString()+"->"+extractHibernateDTOs.getDestinationCountrycode().toString();
					cell = row.createCell((short) count++);
					cell.setCellValue(fromTo);
					cellLength= (fromTo.length() > cellLength)?fromTo.length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				
				if(extractHibernateDTOs.getOriginCity() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getOriginCity().toString());
					cellLength= (extractHibernateDTOs.getOriginCity().toString().length() > cellLength)?extractHibernateDTOs.getOriginCity().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getDestinationCity() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getDestinationCity().toString());
					cellLength= (extractHibernateDTOs.getDestinationCity().toString().length() > cellLength)?extractHibernateDTOs.getDestinationCity().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getAuthorizationReceivedDt() != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getAuthorizationReceivedDt()));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= (date.toString().length() > cellLength)?date.toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				
				if(extractHibernateDTOs.getSurvey() != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getSurvey()));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= (date.toString().length() > cellLength)?date.toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getBeginPacking() != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getBeginPacking()));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= (date.toString().length() > cellLength)?date.toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getBeginload() != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getBeginload()));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= (date.toString().length() > cellLength)?date.toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getRequiredDeliveryDate() != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getRequiredDeliveryDate()));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= (date.toString().length() > cellLength)?date.toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getInvoiceDate() != null){
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getInvoiceDate()));
					cell = row.createCell((short) count++);
					cell.setCellValue(date.toString());
					cellLength= (date.toString().length() > cellLength)?date.toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getAuthorizedVolumeCBM() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getAuthorizedVolumeCBM().toString());
					cellLength= (extractHibernateDTOs.getAuthorizedVolumeCBM().toString().length() > cellLength)?extractHibernateDTOs.getAuthorizedVolumeCBM().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getEstimatedVolumeCBM() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getEstimatedVolumeCBM().toString());
					cellLength= (extractHibernateDTOs.getEstimatedVolumeCBM().toString().length() > cellLength)?extractHibernateDTOs.getEstimatedVolumeCBM().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getShippedVolumeCBM() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getShippedVolumeCBM().toString());
					cellLength= (extractHibernateDTOs.getShippedVolumeCBM().toString().length() > cellLength)?extractHibernateDTOs.getShippedVolumeCBM().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getDistanceKM() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getDistanceKM().toString());
					cellLength= (extractHibernateDTOs.getDistanceKM().toString().length() > cellLength)?extractHibernateDTOs.getDistanceKM().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getTotalAmtInvoicedEUR() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getTotalAmtInvoicedEUR().toString());
					cellLength= (extractHibernateDTOs.getTotalAmtInvoicedEUR().toString().length() > cellLength)?extractHibernateDTOs.getTotalAmtInvoicedEUR().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}
				if(extractHibernateDTOs.getDoorToDoorEUR() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getDoorToDoorEUR().toString());
					cellLength= (extractHibernateDTOs.getDoorToDoorEUR().toString().length() > cellLength)?extractHibernateDTOs.getDoorToDoorEUR().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getAllAditionalCharges() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getAllAditionalCharges().toString());
					cellLength= (extractHibernateDTOs.getAllAditionalCharges().toString().length() > cellLength)?extractHibernateDTOs.getAllAditionalCharges().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getAssignmentType() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getAssignmentType().toString());
					cellLength= (extractHibernateDTOs.getAssignmentType().toString().length() > cellLength)?extractHibernateDTOs.getAssignmentType().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getRecommendation() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getRecommendation().toString());
					cellLength= (extractHibernateDTOs.getRecommendation().toString().length() > cellLength)?extractHibernateDTOs.getRecommendation().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getPreMoveServicsRating() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getPreMoveServicsRating().toString());
					cellLength= (extractHibernateDTOs.getPreMoveServicsRating().toString().length() > cellLength)?extractHibernateDTOs.getPreMoveServicsRating().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getOriginServicesRating() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getOriginServicesRating().toString());
					cellLength= (extractHibernateDTOs.getOriginServicesRating().toString().length() > cellLength)?extractHibernateDTOs.getOriginServicesRating().toString().length():cellLength;
					cell.setCellStyle(cellStyle);sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getDestServicesRating() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getDestServicesRating().toString());
					cellLength= (extractHibernateDTOs.getDestServicesRating().toString().length() > cellLength)?extractHibernateDTOs.getDestServicesRating().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getOverAllRating() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getOverAllRating().toString());
					cellLength= (extractHibernateDTOs.getOverAllRating().toString().length() > cellLength)?extractHibernateDTOs.getOverAllRating().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
				if(extractHibernateDTOs.getComplaints() != null){
					cell = row.createCell((short) count++);
					cell.setCellValue(extractHibernateDTOs.getComplaints().toString());
					cellLength= (extractHibernateDTOs.getComplaints().toString().length() > cellLength)?extractHibernateDTOs.getComplaints().toString().length():cellLength;
					cell.setCellStyle(cellStyle);
					sheet.setColumnWidth(count, (short)(cellLength + 4500));
				}else{
					cell = row.createCell((short) count++);
					cell.setCellStyle(cellStyle);
				}
			}
		}
		return hwb;
	}
	public void getFlatReportByResponse(String reportType, List queryResultList) throws Exception{

		HSSFWorkbook workBook = getFlatfileExtraction(queryResultList);
		/*String header=new String();
		File file1 = new File("FlatFilePartnerExtract");*/
		String fileName = "FlatFilePartnerExtract.xls";
		HttpServletResponse response = getResponse();
		ServletOutputStream outputStream = response.getOutputStream();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		workBook.write(outputStream);
		outputStream.close();
		
		/*header = " Service Order" + "\t" +" Account" + "\t" +"Company"+ "\t" +"Move initiator" +"\t" +" Employee" +"\t"+ " Transport type" + "\t" +" From -> To " + "\t" + " Origin City" + "\t" + "Destination City" + "\t" + "Authorization Received" + "\t" + " Survey Date" + "\t" + " Packing Date" + "\t" + " Loading Date" + "\t" + " Delivery Date"
		+ "\t" + " Invoice Date " + "\t" + " Authorized volume CBM "+ "\t" +" Estimated volume CBM" + "\t" +"Shipped volume CBM"+ "\t" +"Distance km" + "\t" +" Total Amount invoiced EUR " + "\t" + " Door to door EUR" + "\t" + "All other additional charges" + "\t" + "Recommendation" + "\t" + " Pre move services rating" + "\t" + " Origin services rating" + "\t" + " Destination services rating" + "\t" + " Overall rating"
		+ "\t" + " Complaints " + "\t" +"\n";
		outputStream.write(arg0)

		Iterator it = queryResultList.iterator();

		while (it.hasNext()) {
			
			Object extracts = (Object) it.next();
			ExtractHibernateDTO extractHibernateDTOs = (ExtractHibernateDTO)extracts;
			
			//  Column 1
			if(extractHibernateDTOs.getSequenceNumber() != null){   
				outputStream.write((extractHibernateDTOs.getSequenceNumber().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 2
			
			if(extractHibernateDTOs.getPartnerCode() != null){  
				outputStream.write((extractHibernateDTOs.getPartnerCode().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 3
			
			if(extractHibernateDTOs.getBilltoCode() != null){  
				outputStream.write((extractHibernateDTOs.getBilltoCode().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 

			
			//  Column 4
			
			if(extractHibernateDTOs.getAccountContact() != null){
				outputStream.write((extractHibernateDTOs.getAccountContact().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 5
			
			if(extractHibernateDTOs.getTransferee() != null){
				outputStream.write((extractHibernateDTOs.getTransferee().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 6
			
			if(extractHibernateDTOs.getMode() != null){
				outputStream.write((extractHibernateDTOs.getMode().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 7
			
			if(extractHibernateDTOs.getFromTo() != null){
				outputStream.write((extractHibernateDTOs.getFromTo().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 8
			
			if(extractHibernateDTOs.getOriginCity() != null){
				outputStream.write((extractHibernateDTOs.getOriginCity().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 9
			
			if(extractHibernateDTOs.getDestinationCity() != null){
				outputStream.write((extractHibernateDTOs.getDestinationCity().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 10
			
			if(extractHibernateDTOs.getAuthorizationReceivedDt() != null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getAuthorizationReceivedDt()));
				outputStream.write((date.toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 11
			
			if(extractHibernateDTOs.getSurvey() != null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getSurvey()));
				outputStream.write((date.toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 12
			
			if(extractHibernateDTOs.getBeginPacking() != null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getBeginPacking()));
				outputStream.write((date.toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 13
			
			if(extractHibernateDTOs.getBeginload() != null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getBeginload()));
				outputStream.write((date.toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 14
			
			if(extractHibernateDTOs.getRequiredDeliveryDate() != null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getRequiredDeliveryDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 15
			
			if(extractHibernateDTOs.getInvoiceDate() != null){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				StringBuilder date = new StringBuilder(format.format(extractHibernateDTOs.getInvoiceDate()));
				outputStream.write((date.toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 16
			
			if(extractHibernateDTOs.getAuthorizedVolumeCBM() != null){
				outputStream.write((extractHibernateDTOs.getAuthorizedVolumeCBM().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 17
			
			if(extractHibernateDTOs.getEstimatedVolumeCBM() != null){
				outputStream.write((extractHibernateDTOs.getEstimatedVolumeCBM().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 18
			
			if(extractHibernateDTOs.getShippedVolumeCBM() != null){
				outputStream.write((extractHibernateDTOs.getShippedVolumeCBM().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 19
			
			if(extractHibernateDTOs.getDistanceKM() != null){
				outputStream.write((extractHibernateDTOs.getDistanceKM().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 20
			
			if(extractHibernateDTOs.getTotalAmtInvoicedEUR() != null){
				outputStream.write((extractHibernateDTOs.getTotalAmtInvoicedEUR().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 21
			
			if(extractHibernateDTOs.getDoorToDoorEUR() != null){
				outputStream.write((extractHibernateDTOs.getDoorToDoorEUR().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 22
			
			if(extractHibernateDTOs.getAllAditionalCharges() != null){
				outputStream.write((extractHibernateDTOs.getAllAditionalCharges().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 23
			
			if(extractHibernateDTOs.getRecommendation() != null){
				outputStream.write((extractHibernateDTOs.getRecommendation().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 24
			
			if(extractHibernateDTOs.getPreMoveServicsRating() != null){
				outputStream.write((extractHibernateDTOs.getPreMoveServicsRating().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 25
			
			if(extractHibernateDTOs.getOriginServicesRating() != null){
				outputStream.write((extractHibernateDTOs.getOriginServicesRating().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 26
			
			if(extractHibernateDTOs.getDestServicesRating() != null){
				outputStream.write((extractHibernateDTOs.getDestServicesRating().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 27
			
			if(extractHibernateDTOs.getOverAllRating() != null){
				outputStream.write((extractHibernateDTOs.getOverAllRating().toString() + "\t").getBytes());
			}else {
				outputStream.write("\t".getBytes());
			} 
			
			//  Column 28
			
			if(extractHibernateDTOs.getComplaints() != null && "Issue".toString().equalsIgnoreCase(extractHibernateDTOs.getComplaints().toString())){
				outputStream.write((extractHibernateDTOs.getComplaints().toString() + "\t").getBytes());
			}else {
				outputStream.write(("NO" + "\t").getBytes());
			} 
			outputStream.write("\r\n".getBytes());
		}*/
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getJobTypesCondition() {
		return jobTypesCondition;
	}

	public void setJobTypesCondition(String jobTypesCondition) {
		this.jobTypesCondition = jobTypesCondition;
	}

	public String getBillingStatus() {
		return billingStatus;
	}

	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}

	public Date getAdditionalCharges_from() {
		return additionalCharges_from;
	}

	public Date getAdditionalCharges_to() {
		return additionalCharges_to;
	}

	public void setAdditionalCharges_from(Date additionalCharges_from) {
		this.additionalCharges_from = additionalCharges_from;
	}

	public void setAdditionalCharges_to(Date additionalCharges_to) {
		this.additionalCharges_to = additionalCharges_to;
	}

	public String getAddChargesType() {
		return addChargesType;
	}

	public void setAddChargesType(String addChargesType) {
		this.addChargesType = addChargesType;
	}

	public Date getCustomerFileInitiation_from() {
		return customerFileInitiation_from;
	}

	public Date getCustomerFileInitiation_to() {
		return customerFileInitiation_to;
	}

	public void setCustomerFileInitiation_from(Date customerFileInitiation_from) {
		this.customerFileInitiation_from = customerFileInitiation_from;
	}

	public void setCustomerFileInitiation_to(Date customerFileInitiation_to) {
		this.customerFileInitiation_to = customerFileInitiation_to;
	}

	public Date getInvoice_Report_from() {
		return invoice_Report_from;
	}

	public void setInvoice_Report_from(Date invoice_Report_from) {
		this.invoice_Report_from = invoice_Report_from;
	}

	public Date getInvoice_Report_to() {
		return invoice_Report_to;
	}

	public void setInvoice_Report_to(Date invoice_Report_to) {
		this.invoice_Report_to = invoice_Report_to;
	}

	public String getBillingPersonFlag() {
		return billingPersonFlag;
	}

	public void setBillingPersonFlag(String billingPersonFlag) {
		this.billingPersonFlag = billingPersonFlag;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public Map<String, String> getTcktactn() {
		return tcktactn;
	}

	public void setTcktactn(Map<String, String> tcktactn) {
		this.tcktactn = tcktactn;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public Map<String, String> getJobtype() {
		return jobtype;
	}

	public void setJobtype(Map<String, String> jobtype) {
		this.jobtype = jobtype;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public void setPkmode(Map<String, String> pkmode) {
		this.pkmode = pkmode;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public Map<String, String> getChildMap() {
		return childMap;
	}

	public void setChildMap(Map<String, String> childMap) {
		this.childMap = childMap;
	}

	public Map<String, String> getQltyServeyResMap() {
		return qltyServeyResMap;
	}

	public void setQltyServeyResMap(Map<String, String> qltyServeyResMap) {
		this.qltyServeyResMap = qltyServeyResMap;
	}

	public List getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(List billToCode) {
		this.billToCode = billToCode;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public Map<String, String> getWoinstr() {
		return woinstr;
	}

	public void setWoinstr(Map<String, String> woinstr) {
		this.woinstr = woinstr;
	}

	public String getInstructionCode() {
		return instructionCode;
	}

	public void setInstructionCode(String instructionCode) {
		this.instructionCode = instructionCode;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

}