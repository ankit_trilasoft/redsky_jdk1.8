package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="dsfamilydetails")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class DsFamilyDetails extends BaseObject {
	private Long id;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private Long customerFileId;
	private String firstName;
	private String relationship;
	private String cellNumber;
	private String email;
	private Date dateOfBirth;
	private String passportNumber;
	private String countryOfIssue;
	private Date expiryDate  ;
	private String lastName;
	private String middleName;
	private String gender;
	private String countryOfBirth;
	private String age;
	private Long networkId;
	private String nationality;
	private String nativeLanguage;
	private Date passportIssuedDate;
	private String grade;
	private String comment;
	private String relocatingWithCoWorker;
	private String prefix;
	private Date dateOfDeath;
	private Date relocationDate;
	private Date benefitDate;
	private String benefitReasion;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn)
				.append("customerFileId", customerFileId)
				.append("firstName", firstName)
				.append("relationship", relationship)
				.append("cellNumber", cellNumber).append("email", email)
				.append("dateOfBirth", dateOfBirth)
				.append("passportNumber", passportNumber)
				.append("countryOfIssue", countryOfIssue)
				.append("expiryDate", expiryDate).append("lastName", lastName)
				.append("middleName", middleName).append("gender", gender)
				.append("countryOfBirth", countryOfBirth).append("age", age)
				.append("networkId", networkId)
				.append("nationality", nationality)
				.append("nativeLanguage", nativeLanguage)
				.append("passportIssuedDate", passportIssuedDate)
				.append("prefix", prefix)
				.append("dateOfDeath", dateOfDeath)
				.append("relocationDate", relocationDate)
				.append("benefitDate", benefitDate)
				.append("benefitReasion", benefitReasion)
				.append("grade", grade).append("comment",comment).append("relocatingWithCoWorker",relocatingWithCoWorker).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DsFamilyDetails))
			return false;
		DsFamilyDetails castOther = (DsFamilyDetails) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(customerFileId, castOther.customerFileId)
				.append(firstName, castOther.firstName)
				.append(relationship, castOther.relationship)
				.append(cellNumber, castOther.cellNumber)
				.append(email, castOther.email)
				.append(dateOfBirth, castOther.dateOfBirth)
				.append(passportNumber, castOther.passportNumber)
				.append(countryOfIssue, castOther.countryOfIssue)
				.append(expiryDate, castOther.expiryDate)
				.append(lastName, castOther.lastName)
				.append(middleName, castOther.middleName)
				.append(gender, castOther.gender)
				.append(countryOfBirth, castOther.countryOfBirth)
				.append(age, castOther.age)
				.append(networkId, castOther.networkId)
				.append(nationality, castOther.nationality)
				.append(nativeLanguage, castOther.nativeLanguage)
				.append(passportIssuedDate, castOther.passportIssuedDate)
				.append(prefix, castOther.prefix)
				.append(dateOfDeath, castOther.dateOfDeath)
				.append(relocationDate, castOther.relocationDate)
				.append(benefitDate, castOther.benefitDate)
				.append(benefitReasion, castOther.benefitReasion)
				.append(grade, castOther.grade).append(comment,castOther.comment).append(relocatingWithCoWorker,castOther.relocatingWithCoWorker).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(customerFileId).append(firstName)
				.append(relationship).append(cellNumber).append(email)
				.append(dateOfBirth).append(passportNumber)
				.append(countryOfIssue).append(expiryDate).append(lastName)
				.append(middleName).append(gender).append(countryOfBirth)
				.append(age).append(networkId).append(nationality)
				.append(nativeLanguage).append(passportIssuedDate)
				.append(grade).append(comment)
				.append(prefix)
				.append(dateOfDeath)
				.append(relocationDate)
				.append(benefitDate)
				.append(benefitReasion)
				.append(relocatingWithCoWorker).toHashCode();
	}
	@Column(length=20)
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	@Column(length=30)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=100)
	public String getCountryOfIssue() {
		return countryOfIssue;
	}
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=20)
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	
	@Column
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	@Column(length = 65)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	} 
	
	@Column(length=25)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(length=100)
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	
	@Column(length=25)
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getLastName() {
		return lastName;
	}
	
	@Column(length=25)
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(length=25)
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	@Column(length=2)
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(length=6)
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	@Column(length=45)
	public String getCountryOfBirth() {
		return countryOfBirth;
	}
	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}
	@Column
	public Long getNetworkId() {
		return networkId;
	}
	public void setNetworkId(Long networkId) {
		this.networkId = networkId;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getNativeLanguage() {
		return nativeLanguage;
	}
	public void setNativeLanguage(String nativeLanguage) {
		this.nativeLanguage = nativeLanguage;
	}
	public Date getPassportIssuedDate() {
		return passportIssuedDate;
	}
	public void setPassportIssuedDate(Date passportIssuedDate) {
		this.passportIssuedDate = passportIssuedDate;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	@Column
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getRelocatingWithCoWorker() {
		return relocatingWithCoWorker;
	}
	public void setRelocatingWithCoWorker(String relocatingWithCoWorker) {
		this.relocatingWithCoWorker = relocatingWithCoWorker;
	}
	@Column(length=10)
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	@Column(length=45)
	public Date getDateOfDeath() {
		return dateOfDeath;
	}
	public void setDateOfDeath(Date dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}
	public Date getRelocationDate() {
		return relocationDate;
	}
	public void setRelocationDate(Date relocationDate) {
		this.relocationDate = relocationDate;
	}
	public Date getBenefitDate() {
		return benefitDate;
	}
	public void setBenefitDate(Date benefitDate) {
		this.benefitDate = benefitDate;
	}
	public String getBenefitReasion() {
		return benefitReasion;
	}
	public void setBenefitReasion(String benefitReasion) {
		this.benefitReasion = benefitReasion;
	}
}
