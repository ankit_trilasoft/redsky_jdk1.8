package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DataSecurityFiltersDao;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.service.DataSecurityFilterManager;

public class DataSecurityFilterManagerImpl extends GenericManagerImpl<DataSecurityFilter, Long> implements DataSecurityFilterManager{
 	 DataSecurityFiltersDao dataSecurityFiltersDao;

	public DataSecurityFilterManagerImpl(DataSecurityFiltersDao dataSecurityFiltersDao) {
		super(dataSecurityFiltersDao);
		this.dataSecurityFiltersDao = dataSecurityFiltersDao;
	}

	public List findMaximumId() {
		return dataSecurityFiltersDao.findMaximumId();
	}

	public List findFilters(String filter) {
		return dataSecurityFiltersDao.findFilters(filter);
	}

	public List getTableList(String sessionCorpID) {
		return dataSecurityFiltersDao.getTableList(sessionCorpID);
	}

	public List getFieldList(String tableName) {
		return dataSecurityFiltersDao.getFieldList(tableName);
	}

	public List getAgentFilterList(String prefix, String sessionCorpID) {
		return dataSecurityFiltersDao.getAgentFilterList(prefix, sessionCorpID);
	}

	public void deleteFilters(String prefix, String sessionCorpID) {
		dataSecurityFiltersDao.deleteFilters(prefix, sessionCorpID)	;
	}

	public List search(String name, String tableName, String fieldName, String filterValues) {
		return dataSecurityFiltersDao.search(name, tableName, fieldName, filterValues);
	}
	
	public List getOtherCorpidFilterList(String prefix, String corpidList){
		return dataSecurityFiltersDao.getOtherCorpidFilterList(prefix, corpidList);
	}
	
}
