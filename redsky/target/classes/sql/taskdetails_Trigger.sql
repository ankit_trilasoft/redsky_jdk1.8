


delimiter $$
CREATE trigger redsky.trigger_add_history_taskdetails
BEFORE UPDATE on redsky.taskdetails
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  
  
  
  
  
 
   IF (OLD.details <> NEW.details ) THEN
      select CONCAT(fieldNameList,'taskdetails.details~') into fieldNameList;
      select CONCAT(oldValueList,OLD.details,'~') into oldValueList;
      select CONCAT(newValueList,NEW.details,'~') into newValueList;
  END IF;
   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'taskdetails.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
  END IF;
  IF ((OLD.date <> NEW.date) or (OLD.date is null and NEW.date is not null) or (OLD.date is not null and NEW.date is null)) THEN
       select CONCAT(fieldNameList,'taskdetails.date~') into fieldNameList;
       IF(OLD.date is null) THEN
       select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.date is null) THEN
       select CONCAT(newValueList," ",'~') into newValueList;
        END IF;
        IF(OLD.date is not null) THEN
            select CONCAT(oldValueList,OLD.date,'~') into oldValueList;
        END IF;
        IF(NEW.date is not null) THEN
            select CONCAT(newValueList,NEW.date,'~') into newValueList;
        END IF;
   END IF;
   IF (OLD.time <> NEW.time ) THEN
      select CONCAT(fieldNameList,'taskdetails.time~') into fieldNameList;
      select CONCAT(oldValueList,OLD.time,'~') into oldValueList;
      select CONCAT(newValueList,NEW.time,'~') into newValueList;
  END IF;
   IF (OLD.transfer <> NEW.transfer ) THEN
      select CONCAT(fieldNameList,'taskdetails.transfer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.transfer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.transfer,'~') into newValueList;
  END IF;
   IF (OLD.workorder <> NEW.workorder ) THEN
      select CONCAT(fieldNameList,'taskdetails.workorder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workorder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workorder,'~') into newValueList;
  END IF;
  IF (OLD.comment <> NEW.comment ) THEN
      select CONCAT(fieldNameList,'taskdetails.comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.comment,'~') into newValueList;
  END IF;
  
    IF (OLD.category <> NEW.category ) THEN
      select CONCAT(fieldNameList,'taskdetails.category~') into fieldNameList;
      select CONCAT(oldValueList,OLD.category,'~') into oldValueList;
      select CONCAT(newValueList,NEW.category,'~') into newValueList;
  END IF;
  
 CALL add_tblHistory (OLD.id,"taskdetails", fieldNameList, oldValueList, newValueList,NEW.updatedBy, OLD.corpId, now());
 END
$$
delimiter;