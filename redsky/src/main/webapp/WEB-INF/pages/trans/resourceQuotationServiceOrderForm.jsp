<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
  <style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; !margin-bottom:2px; margin-top:-22px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
.hidden { display: none;}
.inputLink {width:45px;height:19px;cursor:pointer;text-decoration:underline;background: transparent!important;padding:0px}
.textarea-comment {border:1px solid #219dd1;color:#000;height:17px; width:100px}
.textarea-comment:focus{height:45px; width: 200px;}
.avoid-clicks { pointer-events: none;}
</style>


</head>
<div id="oAndIRows">
<table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:0px;padding:0px;" >
	<tr>
  		<td>
		  	<input type="button" id="resourceTemplate" class="update-btn-OI" onclick="return contractForOI(this);getDayTemplate();" value="Add Resource Template" style="width:150px; margin:5px 0px " tabindex=""/>
		</td>
		<td align="left" width="5px"></td>
		<td>
		 	<c:if test="${quoteFlag =='y'}">
		  	<c:if test="${resourceListForOI!='[]'}"> 
			<input type="button" class="update-btn-OI" onclick="createPriceForQuote()" value="Create Pricing" style="width:100px; margin:5px 0px;" tabindex=""/>
			<input type="button" class="update-btn-dis-OI" readonly="true" onclick="transferResourceToWorkTicket();" value="Transfer Resource to WorkTicket" disabled="true" style="width:210px; margin:5px 0px;" tabindex="" />
		
			<c:if test="${not empty serviceOrder.id}">
                 <c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
                     <input type="button" class="update-btn-OI" value="Copy Quotes with Resources" style="width:178px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
                 </c:if> 
                 <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'>
                     <input type="button" class="update-btn-OI" value="Copy Quotes with Resources" style="width:178px; height:25px" onclick="CopyOrderWithResources();" tabindex=""/>
                 </c:if>
             </c:if>
            <input type="button" class="update-btn-OI" onclick="copyEstimateOandI()" value="Copy Estimate" style="width:100px; margin:5px 0px;" tabindex=""/>
			<input type="button" value="Save" onclick="saveOI();" class="update-btn-OI" style="width:60px;margin-bottom:0px;" tabindex="" />
			</c:if>
			<c:if test="${resourceListForOI=='[]'}"> 
			<input type="button" class="update-btn-dis-OI" readonly="true" onclick="createPriceForQuote()" value="Create Pricing" style="width:100px; margin:5px 0px;" tabindex=""/>
			<input type="button" class="update-btn-dis-OI" readonly="true" onclick="transferResourceToWorkTicket();" value="Transfer Resource to WorkTicket" disabled="true" style="width:210px; margin:5px 0px;" tabindex="" />

               <c:if test="${not empty serviceOrder.id}">
                	<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
                    	<input type="button" class="update-btn-OI" value="Copy Quotes with Resources" style="width:178px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
                  </c:if> 
                  <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'>
                      <input type="button" class="update-btn-OI" value="Copy Quotes with Resources" style="width:178px; height:25px" onclick="CopyOrderWithResources();" tabindex=""/>
                  </c:if>
               </c:if>
               <input type="button" class="update-btn-OI" readonly="true" disabled="true" value="Copy Estimate" style="width:100px; margin:5px 0px;" tabindex=""/>							
			</c:if>
		</c:if>
		<c:if test="${quoteFlag !='y'}">
			<c:if test="${not empty serviceOrder.id}">
                   <c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
                       <input type="button" class="update-btn-OI" value="Copy Order with Resources" style="width:170px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
                   </c:if> 
                   <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'>
                       <input type="button" class="update-btn-OI" value="Copy Order with Resources" style="width:170px; height:25px" onclick="CopyOrderWithResources();" tabindex=""/>
                   </c:if>
             </c:if> 
		  	<c:if test="${resourceListForOI!='[]'}">
		  		<input type="button" class="update-btn-OI" onclick="transferResourceToWorkTicket();" value="Transfer Resource to WorkTicket" style="width:210px;  margin:5px 0px;" tabindex="" /> 
				<input type="button" class="update-btn-OI" onclick="operationsDetailsList()" value="Operations&nbsp;Details" style="width:120px; margin:5px 0px;" tabindex=""/>	  
				<input type="button" class="update-btn-OI" onclick="copyEstimateOandI()" value="Copy Estimate" style="width:100px; margin:5px 0px;" tabindex=""/>
				<input type="button" class="update-btn-OI" onclick="createPrice()" value="Create Pricing" style="width:100px; margin:5px 0px;" tabindex=""/>
				<input type="button" value="Save" onclick="saveOI();" class="update-btn-OI" style="width:60px;margin-bottom:0px;" tabindex="" />
			</c:if>
			 <c:if test="${resourceListForOI=='[]'}">
			 	<input type="button" class="update-btn-dis-OI" onclick="" readonly="true" disabled="true" value="Transfer Resource to WorkTicket" style="width:210px;  margin:5px 0px;" tabindex="" /> 
				<input type="button" class="update-btn-dis-OI" onclick="" value="Operations&nbsp;Details" readonly="true" disabled="true" style="width:120px; margin:5px 0px;" tabindex=""/>
				<input type="button" class="update-btn-dis-OI" onclick="" readonly="true" disabled="true" value="Copy Estimate" style="width:100px; margin:5px 0px;" tabindex=""/>
				<input type="button" class="update-btn-dis-OI" onclick="" readonly="true" disabled="true" value="Create Pricing" style="width:100px; margin:5px 0px;" tabindex=""/>
			</c:if>
		</c:if>
		
	</td>
	<td>
		<input type="button" class="update-btn-OI" onclick="repositionClose();findDistinctWorkOrder(this);" value="Display WO &#9658;" style="width:95px; margin:5px" tabindex=""/>
	</td>
</tr>
</table> 
 <c:if test="${resourceListForOIStatus=='true'}">
       <div id="newmnav" style="margin-left:12px;">
		   <ul> 
		    <li id="newmnav1" style="background:#FFF"><a onclick="findAllResourceActAll('','ACT');" class="current"><span>Active List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="findAllResourceActAll('','ALL');"><span>All List</span></a></li>
		   </ul>
	   </div> 
	   <div class="spn">&nbsp;</div>
	  <div style="padding-bottom:0px;"></div>
</c:if> 
<c:if test="${resourceListForOIStatus=='ALL'}">
     <div id="newmnav" style="margin-left:12px;">
		  <ul>
		    <li><a onclick="findAllResourceActAll('','ACT');" ><span>Active List</span></a></li>
		    <li id="newmnav1" style="background:#FFF">
		    <a onclick="findAllResourceActAll('','ALL');;" class="current">
		    <span>All List</span></a></li>
         </ul>
	</div> 
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;"></div>
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <c:set value="00" var="workOrder"/> 
 <table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-top:2px;margin:0px;padding:0px;height:23px;background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;border:2px solid #74B3DC; border-bottom:none;">
<tbody>
<c:if test="${not empty resourceListForOI}">
<tr> 
<td width="2%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td width="30%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">
<div style="line-height: 25px; border-right: 3px solid #003366;margin-right:0px;">Resource</div>
</td>
<td width="16%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">
<div style="line-height: 25px; border-right: 3px solid #003366;margin-right:-17px;">Estimate</div>
</td>
<td width="26%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">Revision</td>                      
</tr>
</c:if>
 <c:if test="${empty resourceListForOI}">
<tr> 
<td width="1%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td width="30%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">
<div style="line-height: 25px; border-right: 3px solid rgb(0, 51, 102); margin-right:3px;">Resource</div>
</td>
<td width="18%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">
<div style="line-height: 25px; border-right: 3px solid rgb(0, 51, 102); margin-right: -13px;">Estimate</div>
</td>
<td width="28%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Revision</td>                      
</tr>
</c:if> 
</tbody>
</table>
 <c:set var="myStyle" value="background:#CBEBFF none repeat scroll 0 0;" />
<%--<c:set var="myStyle" value="background:#bff6c2 none repeat scroll 0 0;" /> --%>
<c:set var="myStylesec" value="background:#FBFBFB none repeat scroll 0 0;" />
<s:set name="resourceListForOI" value="resourceListForOI" scope="request"/>

 <c:if test="${resourceListForOI!='[]'}">
 <s:hidden name="workTicketNumberForValidation" id="workTicketNumberForValidation"  value="${returnAjaxStringValue}" />
 <s:hidden name="distinctWorkOrderListVal" id="distinctWorkOrderListVal"  value="${distinctWorkOrderList}" />
 <display:table name="resourceListForOI" class="table" requestURI="" id="resourceListForOI" style="margin-bottom:5px;">
  <display:column  title="Act" style="width:10px;${myStyle}">
 	<c:if test="${resourceListForOI.status}">
     <input type="checkbox"  name="statusCheck${resourceListForOI.id}" id="statusCheck${resourceListForOI.id}" checked="checked"  value="${resourceListForOI.status}" onclick="setValuesForInactiveId('${resourceListForOI.id}',this,'${resourceListForOI.itemsJbkEquipId}','${resourceListForOI.ticket}','${resourceListForOI.workorder}');"/> 
   	</c:if>
    <c:if test="${!(resourceListForOI.status)}">
     <input type="checkbox"  name="statusCheck${resourceListForOI.id}" id="statusCheck${resourceListForOI.id}" value="${resourceListForOI.status}" onclick="setValuesForActiveId('${resourceListForOI.id}',this,'${resourceListForOI.itemsJbkEquipId}','${resourceListForOI.ticket}','${resourceListForOI.workorder}')"/> 
   </c:if>
  </display:column>
 <c:choose>
<c:when test="${myVar !=resourceListForOI.workorder}">
 <c:set var="myVar" value="${resourceListForOI.workorder}"></c:set> 
  <display:column style="width:5px;${myStyle}" >
  <c:if test="${prevWorkOrder==resourceListForOI.workorder}">
 	<img  id="scope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="" onmouseout="" onclick=""/> 
  </c:if>
  <c:if test="${prevWorkOrder!=resourceListForOI.workorder}">
 	<img  id="scope${resourceListForOI.id}" src="<c:url value='/images/externalScope.png'/>" onmouseover="repositionClose(); showScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick="openScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> 
  </c:if>
  </display:column>
<display:column  title="WO#" style="width:30px;${myStyle}">
   <input type="text" class="input-text pr-f11" name="workOrderOI${resourceListForOI.id}" id="workOrderOI${resourceListForOI.id}"  value="${resourceListForOI.workorder}" onclick="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false)"  onchange ="getId('${resourceListForOI.id}');preventWorkOrder(this,'${resourceListForOI.id}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}','workorder','${resourceListForOI.ticket}','${resourceListForOI.workorder}'),validateWorkOrder('${resourceListForOI.id}',this);" style="width:37px" />
  </display:column>
    <display:column  title='Date'  style="width:60px;${myStyle}">
    <fmt:parseDate pattern="yyyy-MM-dd" value="${resourceListForOI.date}" var="parsedAtDepartDate" />
	<fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" />
	<c:if test="${resourceListForOI.ticket != null && resourceListForOI.ticket !=  ''}"> 
    	<s:textfield cssClass="input-textUpper pr-f11" id="date${resourceListForOI.id}" name="date${resourceListForOI.id}" value="${formattedAtDepartDate}" onselect="checkLimitFoTicket()" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
  		<img id="date${resourceListForOI.id}-trigger" style="vertical-align:bottom" name="date" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${resourceListForOI.id}" HEIGHT=20 WIDTH=17   onclick="setFieldId('${resourceListForOI.id}')" />
	</c:if>
	<c:if test="${resourceListForOI.ticket == null || resourceListForOI.ticket ==''}"> 
    	<s:textfield cssClass="input-textUpper pr-f11" id="date${resourceListForOI.id}" name="date${resourceListForOI.id}" value="${formattedAtDepartDate}" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
  		<img id="date${resourceListForOI.id}-trigger" style="vertical-align:bottom" class="avoid-clicks" name="date" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${resourceListForOI.id}" HEIGHT=20 WIDTH=17 />
	</c:if>
    </display:column>
   
 <c:if test="${resourceListForOI.invoiceNumber==true}">
	 <display:column  title="Ticket" style="width:40px;${myStyle}background:#b3ffb3!important;">
	  <c:if test="${resourceListForOI.ticket != null && resourceListForOI.ticket !=  ''}">
	  <input type="button" class="input-text pr-f11 inputLink" id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}" 
	    onclick="javascript:window.open('editWorkTicketUpdate.html?id=${resourceListForOI.workTicketId}','_blank');"  /> 
	 </c:if>
	 <c:if test="${resourceListForOI.ticket == null || resourceListForOI.ticket ==  ''}">
	  <input type="button" class="input-textUpper pr-f11"  id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}"  style="width:45px;height:19px;" /> 
	 </c:if>
  	</display:column>
 </c:if>
  <c:if test="${resourceListForOI.invoiceNumber==false}">
	 <display:column  title="Ticket" style="width:40px;${myStyle}">
	  <c:if test="${resourceListForOI.ticket != null && resourceListForOI.ticket !=  ''}">
	  <input type="button" class="input-text pr-f11 inputLink" id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}" 
	    onclick="javascript:window.open('editWorkTicketUpdate.html?id=${resourceListForOI.workTicketId}','_blank');"  /> 
	 </c:if>
	 <c:if test="${resourceListForOI.ticket == null || resourceListForOI.ticket ==  ''}">
	  <input type="button" class="input-textUpper pr-f11"  id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}"  style="width:45px;height:19px;" /> 
	 </c:if>
  </display:column>
 </c:if> 

	<display:column title="Category" style="width:77px;margin-top:2px;${myStyle}"> 
	<select id="type${resourceListForOI.id}" name ="type${resourceListForOI.id}" style="width:77px" class="list-menu pr-f11" onchange="getId('${resourceListForOI.id}');validateWorkOrderForType('${resourceListForOI.id}');findResource(this.value,'resourcedescription${resourceListForOI.id}','${resourceListForOI.id}');"> 
	<c:forEach var="chrms" items="${resourceCategory}" varStatus="loopStatus">
	<c:choose>
	<c:when test="${chrms.key == resourceListForOI.type}">
	<c:set var="selectedInd" value=" selected"></c:set>
	</c:when>
	<c:otherwise>
	<c:set var="selectedInd" value=""></c:set>
	</c:otherwise>
	</c:choose>
	<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
	<c:out value="${chrms.value}"></c:out>
    </option>
	</c:forEach> 
	</select>
	</display:column>
 <display:column  title="Resource" style="width:35px;${myStyle}">
 	<s:hidden id="checkLimit${resourceListForOI.id}" name="checkLimit${resourceTransferOI.id}"/>
 <input type="hidden" value="" id="resourcedescriptionTemp${resourceListForOI.id}"/>
  <input type="text" class="input-text pr-f11" id="resourcedescription${resourceListForOI.id}" name="resourcedescription${resourceListForOI.id}"  value="${resourceListForOI.description}" style="width:90px" onclick="" onkeyup="checkChar(this,'resourcedescription${resourceListForOI.id}','type${resourceListForOI.id}','choices${resourceListForOI.id}','${resourceListForOI.id}',this,'${resourceListForOI.description}','${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}');" onchange="getId('${resourceListForOI.id}');" />
  <div id="choices${resourceListForOI.id}" class="listwhitetext" ></div>
 </display:column>
 <display:column title="Est. Quantity" style="width:40px;${myStyle}">
 	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}"> 
  		<input type="text" class="input-text pr-f11" id="resourceQuantity${resourceListForOI.id}" name="resourceQuantity${resourceListForOI.id}"  value="${resourceListForOI.quantitiy}" style="width:40px;text-align:right;" onchange="getId('${resourceListForOI.id}');return validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.quantitiy}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',true);calculateRevenueForOI();" />
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}"> 
  		<input type="text" class="input-textUpper pr-f11" id="resourceQuantity${resourceListForOI.id}" name="resourceQuantity${resourceListForOI.id}"  value="${resourceListForOI.quantitiy}" style="width:40px;text-align:right;" readonly="true"/>
 	</c:if>
 </display:column> 
 <display:column  title="Est. Hour" style="width:30px;${myStyle}"> 
 	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}"> 
  		<input type="text" class="input-text pr-f11" id="resourceEstHour${resourceListForOI.id}" name="resourceEstHour${resourceListForOI.id}"  value="${resourceListForOI.esthours}" style="width:30px;text-align:right;" onchange="getId('${resourceListForOI.id}');return validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.esthours}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',true);calculateRevenueForOI();"/>
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}"> 
  		<input type="text" class="input-textUpper pr-f11" id="resourceEstHour${resourceListForOI.id}" name="resourceEstHour${resourceListForOI.id}"  value="${resourceListForOI.esthours}" style="width:30px;text-align:right;" readonly="true"/>
 	</c:if>
 </display:column>
 
 <display:column  title="" headerClass="header-rt" style="width:20px;border-right:medium solid #003366;${myStyle}"> 
 <%-- <input type="text" class="input-text pr-f11" id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" value="<c:out value='${resourceListForOI.comments}'/>"  onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" /> --%>
    <%-- <textarea class="textarea-comment pr-f11" title="<c:out value="${resourceListForOI.comments}"></c:out>" maxlength="200" id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)" onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" ><c:out value='${resourceListForOI.comments}'/></textarea> --%>
	<c:if test="${empty resourceListForOI.comments}">
	<a><img align="middle" id="comments${resourceListForOI.id}" title="" onclick="repositionClose();openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','comments',this);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi.png"/></a>
	</c:if>

	<c:if test="${not empty resourceListForOI.comments}">
	<a><img align="middle" id="comments${resourceListForOI.id}" title="" onclick="repositionClose();openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','comments',this);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi-red.png"/></a>
	</c:if>


 </display:column>
 
  <display:column  title="Qty" style="width:38px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id='estimateQuantity${resourceListForOI.id}' name="estimateQuantity${resourceListForOI.id}" readonly="true" value="${resourceListForOI.estimatedquantity}"  style="width:38px;text-align:right;"  onchange="fillExpenseByQty(this);getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Buy Rate" style="width:45px;${myStyle}"> 
  	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}">
  		<input type="text" class="input-text pr-f11" id="estimateBuyRate${resourceListForOI.id}" name="estimateBuyRate${resourceListForOI.id}" value="${resourceListForOI.estimatedbuyrate}" style="width:45px;text-align:right;"  onchange="fillExpenseByRate(this);calculateRevenueForOI();getId('${resourceListForOI.id}');"/>
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}">
  		<input type="text" class="input-textUpper pr-f11" id="estimateBuyRate${resourceListForOI.id}" name="estimateBuyRate${resourceListForOI.id}" value="${resourceListForOI.estimatedbuyrate}" readonly="true" style="width:45px;text-align:right;"  onchange=""/>
 	</c:if>
 </display:column> 
   <display:column  title="Expense" style="width:47px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateExpense${resourceListForOI.id}" name="estimateExpense${resourceListForOI.id}" value="${resourceListForOI.estimatedexpense}" readonly="true" style="width:47px;text-align:right;"   onchange="getId('${resourceListForOI.id}');"/>
 </display:column> 
 <display:column  title="Sell Rate" style="width:45px;${myStyle}">
  	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}"> 
  		<input type="text" class="input-text pr-f11" id="estimateSellRate${resourceListForOI.id}" name="estimateSellRate${resourceListForOI.id}" value="${resourceListForOI.estimatedsellrate}" style="width:45px;text-align:right;" onchange="fillRevnueBySellRate(this);calculateRevenueForOI();getId('${resourceListForOI.id}');" />
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}"> 
  		<input type="text" class="input-textUpper pr-f11" id="estimateSellRate${resourceListForOI.id}" name="estimateSellRate${resourceListForOI.id}" value="${resourceListForOI.estimatedsellrate}" readonly="true" style="width:45px;text-align:right;" onchange="" />
 	</c:if>
 </display:column>
  <display:column  title="Revenue" headerClass="header-rt" style="width:47px;border-right:medium solid #003366;${myStyle}">
  <input type="text" class="input-textUpper pr-f11" id="estimateRevenue${resourceListForOI.id}" name="estimateRevenue${resourceListForOI.id}" readonly="true" value="${resourceListForOI.estimatedrevenue}"  style="width:47px;text-align:right;"  onchange="getId('${resourceListForOI.id}');"/>
 </display:column>
 
 <display:column style="width:5px;${myStyle}" >
 <%-- <img  id="revisionScope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="repositionClose(); showRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick="openRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> --%> 
  <c:if test="${prevWorkOrder==resourceListForOI.workorder}">
 	<img  id="revisionScope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="" onmouseout="" onclick=""/> 
  </c:if>
  <c:if test="${prevWorkOrder!=resourceListForOI.workorder}">
 	<img  id="revisionScope${resourceListForOI.id}" src="<c:url value='/images/externalScope.png'/>" onmouseover="repositionClose(); showRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick=" openRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> 
  </c:if>
  </display:column>
  <display:column title="Rev. Quantity" style="width:40px;${myStyle}">
 <input type="text" class="input-text pr-f11" id="revisedQuantity${resourceListForOI.id}" name="revisedQuantity${resourceListForOI.id}"  value="${resourceListForOI.revisedQuantity}" style="width:40px;text-align:right;" onchange="fillExpenseByRevisedQty(this,'${resourceListForOI.revisedQuantity}','${resourceListForOI.revisionexpense}','${resourceListForOI.revisionrevenue}');calculateRevenueForOI();getId('${resourceListForOI.id}');" />  
 </display:column> 
  <display:column  title="Rev. Hours" style="width:40px;${myStyle}"> 
  <input type="text" class="input-text pr-f11" id="revisionQuantity${resourceListForOI.id}" name="revisionQuantity${resourceListForOI.id}"  value="${resourceListForOI.revisionquantity}"  style="width:40px;text-align:right;"  onchange="fillExpenseByRevQty(this,'${resourceListForOI.revisionquantity}','${resourceListForOI.revisionexpense}','${resourceListForOI.revisionrevenue}');calculateRevenueForOI();getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Buy Rate" style="width:45px;${myStyle}"> 
  <input type="text" class="input-text pr-f11" id="revisionbuyrate${resourceListForOI.id}" name="revisionbuyrate${resourceListForOI.id}"  value="${resourceListForOI.revisionbuyrate}" style="width:45px;text-align:right;"  onchange="changeExpanseBuyRevRate(this);getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Expense" style="width:47px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionexpense${resourceListForOI.id}" name="revisionexpense${resourceListForOI.id}"  value="${resourceListForOI.revisionexpense}" readonly="true" style="width:47px;text-align:right;"  />
 </display:column>
  <display:column  title="Sell Rate" style="width:45px;${myStyle}"> 
  <input type="text" class="input-text pr-f11" id="revisionsellrate${resourceListForOI.id}" name="revisionsellrate${resourceListForOI.id}"  value="${resourceListForOI.revisionsellrate}" style="width:45px;text-align:right;"  onchange="changerevenueByRevSellRate(this);calculateRevenueForOI();getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Revenue" style="width:47px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionrevenue${resourceListForOI.id}" name="revisionrevenue${resourceListForOI.id}"  value="${resourceListForOI.revisionrevenue}" readonly="true" style="width:47px;text-align:right;" />
 </display:column>
 
 <display:column  title="" headerClass="" style="width:20px;${myStyle}"> 
    <c:if test="${empty resourceListForOI.revisionComment}">
        <a><img align="middle" id="revisionComment${resourceListForOI.id}" title="" onclick="openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','revisionComment',this);reposition();" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi.png"/></a>
            </c:if> 
   
     <c:if test="${not empty resourceListForOI.revisionComment}">
         <a><img align="middle" id="revisionComment${resourceListForOI.id}" title="" onclick="openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','revisionComment',this);reposition();" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi-red.png"/></a>
            </c:if> 
   
 <%-- <input type="text" class="input-text pr-f11" id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" value="<c:out value='${resourceListForOI.comments}'/>"  onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" /> --%>
   <%--  <textarea class="textarea-comment pr-f11" title="<c:out value="${resourceListForOI.comments}"></c:out>" maxlength="200" id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)" onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" ><c:out value='${resourceListForOI.comments}'/></textarea> --%>
    </display:column>
  
  <display:column title="Audit" style="text-align:center;width:30px;${myStyle}"> 
    <a><img align="middle" onclick="auditSetUp('${resourceListForOI.id}');" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/report-ext.png"/></a>
    </display:column>
 <%-- <display:column title="" style="width:5px;${myStyle}">
<a><img align="middle" onclick="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.itemsJbkEquipId}','','${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
</display:column> --%>
  <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="workTicketId${resourceTransferOI.id}" name="workTicketId${resourceTransferOI.id}" value="${resourceTransferOI.workTicketId}"> 
    </display:column>
    </c:when>
    <c:otherwise>
  <display:column style="width:5px;${myStyle}" >
 	<%-- <img id="scope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>"  onmouseover="repositionClose();showScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick="openScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> --%> 
  <c:if test="${prevWorkOrder==resourceListForOI.workorder}">
 	<img  id="scope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="" onmouseout="" onclick=""/> 
  </c:if>
  <c:if test="${prevWorkOrder!=resourceListForOI.workorder}">
 	<img  id="scope${resourceListForOI.id}" src="<c:url value='/images/externalScope.png'/>" onmouseover="repositionClose(); showScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick=" openScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> 
  </c:if>
  </display:column>
<display:column  title="WO#" style="width:30px;${myStylesec}">
   <input type="text" class="input-text pr-f11" name="workOrderOI${resourceListForOI.id}" id="workOrderOI${resourceListForOI.id}"  value="${resourceListForOI.workorder}" onclick="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false)"  onchange ="preventWorkOrder(this,'${resourceListForOI.id}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}','workorder','${resourceListForOI.ticket}','${resourceListForOI.workorder}'),validateWorkOrder('${resourceListForOI.id}');getId('${resourceListForOI.id}');" style="width:37px" />
  </display:column>
    <display:column  title='Date' style="width:60px;${myStylesec}">
    <fmt:parseDate pattern="yyyy-MM-dd" value="${resourceListForOI.date}" var="parsedAtDepartDate" />
	<fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" /> 
	    <c:if test="${resourceListForOI.ticket!= null && resourceListForOI.ticket!=''}"> 
	    	<s:textfield cssClass="input-textUpper pr-f11" id="date${resourceListForOI.id}" name="date${resourceListForOI.id}" value="${formattedAtDepartDate}" onselect="checkLimitFoTicket()" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
	  		<img id="date${resourceListForOI.id}-trigger" style="vertical-align:bottom" name="date" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldId('${resourceListForOI.id}')" textelementname="date${resourceListForOI.id}" HEIGHT=20 WIDTH=17 />
		</c:if>
		<c:if test="${resourceListForOI.ticket == null || resourceListForOI.ticket ==''}"> 
	    	<s:textfield cssClass="input-textUpper pr-f11" id="date${resourceListForOI.id}" name="date${resourceListForOI.id}" value="${formattedAtDepartDate}" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
	  		<img id="date${resourceListForOI.id}-trigger" style="vertical-align:bottom" class="avoid-clicks" name="date" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${resourceListForOI.id}" HEIGHT=20 WIDTH=17 />
		</c:if>	
    </display:column>
 <c:if test="${resourceListForOI.invoiceNumber==true}">
	 <display:column  title="Ticket" style="width:40px;${myStylesec}background:#b3ffb3 !important;">
	  <c:if test="${resourceListForOI.ticket != null && resourceListForOI.ticket !=  ''}">
	  <input type="button" class="input-text pr-f11 inputLink" id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}" 
	    onclick="javascript:window.open('editWorkTicketUpdate.html?id=${resourceListForOI.workTicketId}','_blank');"  /> 
	 </c:if>
	 <c:if test="${resourceListForOI.ticket == null || resourceListForOI.ticket ==  ''}">
	  <input type="button" class="input-textUpper pr-f11"  id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}"  style="width:45px;height:19px;" /> 
	 </c:if>
	  </display:column>
 </c:if>
  <c:if test="${resourceListForOI.invoiceNumber==false}">
	 <display:column  title="Ticket" style="width:40px;${myStylesec}">
	  <c:if test="${resourceListForOI.ticket != null && resourceListForOI.ticket !=  ''}">
	  <input type="button" class="input-text pr-f11 inputLink" id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}" 
	    onclick="javascript:window.open('editWorkTicketUpdate.html?id=${resourceListForOI.workTicketId}','_blank');"  /> 
	 </c:if>
	 <c:if test="${resourceListForOI.ticket == null || resourceListForOI.ticket ==  ''}">
	  <input type="button" class="input-textUpper pr-f11"  id="ticket${resourceListForOI.id}" name="ticket${resourceListForOI.id}" value="${resourceListForOI.ticket}"  style="width:45px;height:19px;" /> 
	 </c:if>
	  </display:column>
 </c:if> 

	<display:column title="Category" style="width:77px;margin-top:2px;${myStylesec}"> 
	<select id="type${resourceListForOI.id}" name ="type${resourceListForOI.id}" style="width:77px" class="list-menu pr-f11" onmousedown="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.type}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);" onclick="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.type}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);"  onchange="validateWorkOrderForType('${resourceListForOI.id}');findResource(this.value,'resourcedescription${resourceListForOI.id}','${resourceListForOI.id}');validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.type}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);getId('${resourceListForOI.id}');"> 
	<c:forEach var="chrms" items="${resourceCategory}" varStatus="loopStatus">
	<c:choose>
	<c:when test="${chrms.key == resourceListForOI.type}">
	<c:set var="selectedInd" value=" selected"></c:set>
	</c:when>
	<c:otherwise>
	<c:set var="selectedInd" value=""></c:set>
	</c:otherwise>
	</c:choose>
	<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
	<c:out value="${chrms.value}"></c:out>
    </option>
	</c:forEach> 
	</select>
	</display:column>
 <display:column  title="Resource" style="width:35px;${myStylesec}">
 <input type="hidden" value="" id="resourcedescriptionTemp${resourceListForOI.id}"/>
  <input type="text" class="input-text pr-f11" id="resourcedescription${resourceListForOI.id}" name="resourcedescription${resourceListForOI.id}"  value="${resourceListForOI.description}" style="width:90px" onclick="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.description}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);" onkeyup="checkChar(this,'resourcedescription${resourceListForOI.id}','type${resourceListForOI.id}','choices${resourceListForOI.id}','${resourceListForOI.id}',this,'${resourceListForOI.description}','${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}');" onchange="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.description}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);getId('${resourceListForOI.id}');" />
  <div id="choices${resourceListForOI.id}" class="listwhitetext" ></div>
 </display:column>
 <display:column title="Est. Quantity" style="width:40px;${myStylesec}">
 	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}"> 
 		<input type="text" class="input-text pr-f11" id="resourceQuantity${resourceListForOI.id}" name="resourceQuantity${resourceListForOI.id}"  value="${resourceListForOI.quantitiy}" style="width:40px;text-align:right;" onchange="getId('${resourceListForOI.id}');return validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.quantitiy}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',true);calculateRevenueForOI();" />
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}"> 
  		<input type="text" class="input-textUpper pr-f11" id="resourceQuantity${resourceListForOI.id}" name="resourceQuantity${resourceListForOI.id}"  value="${resourceListForOI.quantitiy}" style="width:40px;text-align:right;" readonly="true"/>
 	</c:if>
 </display:column> 
 <display:column  title="Est. Hour" style="width:30px;${myStylesec}"> 
 	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}"> 
 		<input type="text" class="input-text pr-f11" id="resourceEstHour${resourceListForOI.id}" name="resourceEstHour${resourceListForOI.id}"  value="${resourceListForOI.esthours}" style="width:30px;text-align:right;" onchange="getId('${resourceListForOI.id}');return validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.esthours}',this,'${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',true);calculateRevenueForOI();"/>
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}"> 
  		<input type="text" class="input-textUpper pr-f11" id="resourceEstHour${resourceListForOI.id}" name="resourceEstHour${resourceListForOI.id}"  value="${resourceListForOI.esthours}" style="width:30px;text-align:right;" readonly="true"/>
 	</c:if>
 </display:column>
 
 <display:column  title="" headerClass="header-rt" style="width:20px;border-right:medium solid #003366;${myStylesec}"> 
 <%-- <input type="text" class="input-text pr-f11" id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" value="<c:out value='${resourceListForOI.comments}'/>"  onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" /> --%>
   <%--  <textarea class="textarea-comment pr-f11" title="<c:out value="${resourceListForOI.comments}"></c:out>"  maxlength="200"  id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)" onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" ><c:out value='${resourceListForOI.comments}'/></textarea> --%>
   <c:if test="${empty resourceListForOI.comments}">
 <a><img align="middle" id="comments${resourceListForOI.id}" onclick="repositionClose();openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','comments',this);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi.png"/></a>
 </c:if>
 <c:if test="${not empty resourceListForOI.comments}">
  <a><img align="middle" id="comments${resourceListForOI.id}" onclick="repositionClose();openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','comments',this);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi-red.png"/></a> 
  </c:if>
 </display:column>
 
  <display:column  title="Qty" style="width:38px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id='estimateQuantity${resourceListForOI.id}' name="estimateQuantity${resourceListForOI.id}" readonly="true" value="${resourceListForOI.estimatedquantity}"  style="width:38px;text-align:right;"  onchange="fillExpenseByQty(this);getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Buy Rate" style="width:45px;${myStylesec}">
  	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}">
  		<input type="text" class="input-text pr-f11" id="estimateBuyRate${resourceListForOI.id}" name="estimateBuyRate${resourceListForOI.id}" value="${resourceListForOI.estimatedbuyrate}" style="width:45px;text-align:right;"  onchange="fillExpenseByRate(this);calculateRevenueForOI();getId('${resourceListForOI.id}');"/>
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}">
  		<input type="text" class="input-textUpper pr-f11" id="estimateBuyRate${resourceListForOI.id}" name="estimateBuyRate${resourceListForOI.id}" value="${resourceListForOI.estimatedbuyrate}" readonly="true" style="width:45px;text-align:right;"  onchange=""/>
 	</c:if> 
 </display:column> 
   <display:column  title="Expense" style="width:47px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateExpense${resourceListForOI.id}" name="estimateExpense${resourceListForOI.id}" value="${resourceListForOI.estimatedexpense}" readonly="true" style="width:47px;text-align:right;" onchange="getId('${resourceListForOI.id}');"/>
 </display:column> 
 <display:column  title="Sell Rate" style="width:45px;${myStylesec}">
   	<c:if test="${resourceListForOI.targetActual!='A' && resourceListForOI.targetActual!='D'}"> 
  		<input type="text" class="input-text pr-f11" id="estimateSellRate${resourceListForOI.id}" name="estimateSellRate${resourceListForOI.id}" value="${resourceListForOI.estimatedsellrate}" style="width:45px;text-align:right;" onchange="fillRevnueBySellRate(this);calculateRevenueForOI();getId('${resourceListForOI.id}');" />
 	</c:if>
 	<c:if test="${resourceListForOI.targetActual=='A' || resourceListForOI.targetActual=='D'}"> 
  		<input type="text" class="input-textUpper pr-f11" id="estimateSellRate${resourceListForOI.id}" name="estimateSellRate${resourceListForOI.id}" value="${resourceListForOI.estimatedsellrate}" readonly="true" style="width:45px;text-align:right;" onchange="" />
 	</c:if> 
 </display:column>
  <display:column  title="Revenue" headerClass="header-rt" style="width:47px;border-right:medium solid #003366;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateRevenue${resourceListForOI.id}" name="estimateRevenue${resourceListForOI.id}" readonly="true" value="${resourceListForOI.estimatedrevenue}"  style="width:47px;text-align:right;"  onchange="getId('${resourceListForOI.id}');"/>
 </display:column>
 <display:column style="width:5px;${myStyle}" >
 <%-- <img  id="revisionScope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="repositionClose(); showRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick="openRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> --%> 
  <c:if test="${prevWorkOrder==resourceListForOI.workorder}">
 	<img  id="revisionScope${resourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="" onmouseout="" onclick=""/> 
  </c:if>
  <c:if test="${prevWorkOrder!=resourceListForOI.workorder}">
 	<img  id="revisionScope${resourceListForOI.id}" src="<c:url value='/images/externalScope.png'/>" onmouseover="repositionClose(); showRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}',this,'${resourceListForOI.ticket}');" onmouseout="ajax_hideTooltip();" onclick=" openRevisionScopeForWO('${resourceListForOI.id}','${resourceListForOI.workorder}','${resourceListForOI.ticket}');"/> 
  </c:if>
  </display:column>
 <display:column title="Rev. Quantity" style="width:40px;${myStylesec}">
 <input type="text" class="input-text pr-f11" id="revisedQuantity${resourceListForOI.id}" name="revisedQuantity${resourceListForOI.id}"  value="${resourceListForOI.revisedQuantity}" style="width:40px;text-align:right;" onchange="fillExpenseByRevisedQty(this,'${resourceListForOI.revisedQuantity}','${resourceListForOI.revisionexpense}','${resourceListForOI.revisionrevenue}');calculateRevenueForOI();getId('${resourceListForOI.id}');" />  
 </display:column> 
 
  <display:column  title="Rev. Hours" style="width:40px;${myStylesec}"> 
  <input type="text" class="input-text pr-f11" id="revisionQuantity${resourceListForOI.id}" name="revisionQuantity${resourceListForOI.id}"  value="${resourceListForOI.revisionquantity}"  style="width:40px;text-align:right;"  onchange="fillExpenseByRevQty(this,'${resourceListForOI.revisionquantity}','${resourceListForOI.revisionexpense}','${resourceListForOI.revisionrevenue}');calculateRevenueForOI();getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Buy Rate" style="width:45px;${myStylesec}"> 
  <input type="text" class="input-text pr-f11" id="revisionbuyrate${resourceListForOI.id}" name="revisionbuyrate${resourceListForOI.id}"  value="${resourceListForOI.revisionbuyrate}" style="width:45px;text-align:right;"  onchange="changeExpanseBuyRevRate(this);getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Expense" style="width:47px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionexpense${resourceListForOI.id}" name="revisionexpense${resourceListForOI.id}"  value="${resourceListForOI.revisionexpense}" readonly="true" style="width:47px;text-align:right;"  />
 </display:column>
  <display:column  title="Sell Rate" style="width:45px;${myStylesec}"> 
  <input type="text" class="input-text pr-f11" id="revisionsellrate${resourceListForOI.id}" name="revisionsellrate${resourceListForOI.id}"  value="${resourceListForOI.revisionsellrate}" style="width:45px;text-align:right;"  onchange="changerevenueByRevSellRate(this);calculateRevenueForOI();getId('${resourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Revenue" style="width:47px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionrevenue${resourceListForOI.id}" name="revisionrevenue${resourceListForOI.id}"  value="${resourceListForOI.revisionrevenue}" readonly="true" style="width:47px;text-align:right;" />
 </display:column> 
 
  <display:column  title="" headerClass="" style="width:20px;${myStylesec}"> 
 <%-- <input type="text" class="input-text pr-f11" id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" value="<c:out value='${resourceListForOI.comments}'/>"  onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" /> --%>
   <c:if test="${empty resourceListForOI.revisionComment}"> 
    <a><img align="middle" id="revisionComment${resourceListForOI.id}" title="" onclick="openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','revisionComment',this);reposition();" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi.png"/></a>
    </c:if>
    
    <c:if test="${not empty resourceListForOI.revisionComment}"> 
     <a><img align="middle" id="revisionComment${resourceListForOI.id}" title="" onclick="openCommentForRevised('${resourceListForOI.id}','${resourceListForOI.workorder}','revisionComment',this);reposition();" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/comment-oi-red.png"/></a>
    </c:if>
    <%-- <textarea class="textarea-comment pr-f11" title="<c:out value="${resourceListForOI.comments}"></c:out>"  maxlength="200"  id="resourceComments${resourceListForOI.id}" name="resourceComments${resourceListForOI.id}" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)" onchange="validateWorkOrder('${resourceListForOI.id}');" style="width:100px" ><c:out value='${resourceListForOI.comments}'/></textarea> --%>
 </display:column>
 
  <display:column title="Audit" style="width:40px;text-align:center;${myStylesec}"> 
    <a><img align="middle" onclick="auditSetUp('${resourceListForOI.id}');" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/report-ext.png"/></a>
    </display:column>
 <%-- <display:column title="" style="width:5px;${myStylesec}">
<a><img align="middle" onclick="validateWorkTicketStatus('${resourceListForOI.id}','${resourceListForOI.itemsJbkEquipId}','','${resourceListForOI.ticket}','${resourceListForOI.corpID}','${resourceListForOI.shipNumber}',false);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
</display:column> --%>
  <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="workTicketId${resourceTransferOI.id}" name="workTicketId${resourceTransferOI.id}" value="${resourceTransferOI.workTicketId}"> 
    </display:column>
</c:otherwise>
</c:choose>
      <display:footer>                               
            
                  <tr> 
  	          <td align="right" colspan="10"><b><div align="right">Sales&nbsp;Tax</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedSalesTax" name="estmatedSalesTax" value="${resourceListForOI.estmatedSalesTax}" readonly="true" onchange="onlyFloatAllowed(this);" size="6" class="input-textUpper pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
          	<td></td>
          	<td></td>
          	<td></td>      
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionSalesTax" name="revisionSalesTax" value="${resourceListForOI.revisionSalesTax}" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>  
  	      <td align="left" width="70px">
  	      </td> 
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
          </tr>
          <tr>
          	  <td align="left" colspan="3" ><div align="left"><input type="button" class="cssbuttonA" style="width:90px; height:20px; margin-top:-4px;" name="activeInactive" id="activeInactive"  value="Active/Inactive" onclick="checkActiveInactive();"/></div></td> 
  	          <td align="right" colspan="7"><b><div align="right">Valuation</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedValuation" name="estmatedValuation" value="${resourceListForOI.estmatedValuation}" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
          	<td></td>  
          	<td></td>
          	<td></td>    
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionValuation" name="revisionValuation" value="${resourceListForOI.revisionValuation}" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>  
  	      <td align="left" width="70px">
  	      </td> 
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
          </tr>
          <tr> 
  	          <td align="right" colspan="10"><b><div align="right">Equipment Rental</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedConsumables" name="estmatedConsumables" value="${resourceListForOI.estmatedConsumables}" readonly="true" onchange="onlyFloatAllowed(this);" size="6" class="input-textUpper pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
          	<td></td> 
          	<td></td>
          	<td></td>    
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionConsumables" name="revisionConsumables" value="${resourceListForOI.revisionConsumables}" readonly="true" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-textUpper pr-f11 "/>
               </td>  
  	      <td align="left" width="70px">
  	      </td> 
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
          </tr>
                <tr> 
  	          <td align="right" colspan="10"><b><div align="right">AB5 Surcharge</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedAB5Surcharge" name="estmatedAB5Surcharge" value="${resourceListForOI.estmatedAB5Surcharge}" readonly="true" onchange="onlyFloatAllowed(this);" size="6" class="input-textUpper pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
          	<td></td> 
          	<td></td>
          	<td></td>    
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionaB5Surcharge" name="revisionaB5Surcharge" value="${resourceListForOI.revisionaB5Surcharge}" readonly="true" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-textUpper pr-f11 "/>
               </td>  
  	      <td align="left" width="70px">
  	      </td> 
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
          </tr>
            
            
             <tr> 
  	          <td align="right" colspan="10"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="left" colspan="1">
               <input type="text" style="text-align:right;width:50px;" id="estimatedTotalExpense" name="estimatedTotalExpense" value="${resourceListForOI.estimatedTotalExpenseForOI}"  readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
               </td>
               <td></td>
              <td><input type="text" style="text-align:right;width:50px;" id="estimatedTotalRevenue" name="estimatedTotalRevenue" value="${resourceListForOI.estimatedTotalRevenueForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/></td>
              <td></td>
          	<td></td> 
          	<td></td>
          	<td></td>    
          <td align="left" width="40px">
          <input type="text" style="text-align:right;width:50px;" name="revTotalExpense" id="revTotalExpense" value="${resourceListForOI.revisedTotalExpenseForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td>
          <td align="left"></td>
  	      <td align="left" width="70px">
  	      <input type="text" style="text-align:right;width:50px;" name="revTotalRevenue" id="revTotalRevenue" value="${resourceListForOI.revisedTotalRevenueForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
  	      </td> 
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
          </tr>
                   
           <tr>
            <td align="right" colspan="10"><b><div align="right">Gross&nbsp;Margin</div></b></td>
           <td align="left" colspan="2" ></td>		  	                     
           <td align="left" colspan="1">
          <%--  <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/> --%>
           </td>
           <td></td>
           <td>
           <input type="text" style="text-align:right;width:50px;" id="estGrossMarginForRevenue" name="estGrossMarginForRevenue" value="${resourceListForOI.estimatedGrossMarginForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
           </td>
           <td></td><td></td><td></td><td></td>  
          <td align="left" width="40px">
         <%--  <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/> --%>
          </td>
  	      <td align="left"></td>
          <td align="left" width="70px" >
  	      <input type="text" style="text-align:right;width:50px;" id="revGrossMarginForRevenue" name="revGrossMarginForRevenue" value="${resourceListForOI.revisedGrossMarginForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
  	      </td>
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
           </tr>                  
                   
            <tr>
             <td align="right" colspan="10"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
            <td align="left" colspan="2" ></td>
            <td align="left" colspan="1">
            <%-- <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/> --%>
            </td>
            <td></td>
            <td><input type="text" style="text-align:right;width:50px;" id="estGrossMarginForRevenuePercentage" name="estGrossMarginForRevenuePercentage" value="${resourceListForOI.estimatedGrossMarginPercentageForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
            </td>
            <td></td><td></td><td></td><td></td><td></td>  
  	      <td align="left"> </td>
          <td align="left" width="40px">
  	      <input type="text" style="text-align:right;width:50px;" id="revGrossMarginForRevenuePercentage" name="revGrossMarginForRevenuePercentage" value="${resourceListForOI.revisedGrossMarginPercentageForOI}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
  	      </td>
  	       <td align="left"></td>
  	          <td align="left"></td>
  	            
         </tr>		  	                 
     </display:footer>
     <c:set var="prevWorkOrder" value="${resourceListForOI.workorder}" />
 </display:table> 
 </c:if>
 <c:if test="${resourceListForOI=='[]'}"> 
 <display:table name="resourceListForOI" class="table" requestURI="" id="resourceListForOI" style="margin-bottom:5px;"> 
  <display:column style="width:5px;${myStyle}" >
  </display:column>
<display:column  title="WO#" style="width:30px;${myStyle}">
  </display:column>
    <display:column  title='Date'  style="${myStyle}">
    </display:column>
  <display:column  title="Ticket" style="width:40px;${myStyle}">
  </display:column>

	<display:column title="Category" style="width:77px;margin-top:2px;${myStyle}"> 
	</display:column>
 <display:column  title="Resource" style="width:35px;${myStyle}">
 </display:column>
 <display:column title="Est. Quantity" style="width:40px;${myStyle}">
 </display:column> 
 <display:column  title="Est. Hour" style="width:35px;${myStyle}"> 
 </display:column>
 <display:column  title="Comments" headerClass="header-rt" style="width:100px;border-right:medium solid #003366;${myStyle}"> 
 </display:column>
 
  <display:column  title="Qty" style="width:38px;${myStyle}"> 
 </display:column>
  <display:column  title="Buy Rate" style="width:50px;${myStyle}"> 
 </display:column> 
   <display:column  title="Expense" style="width:50px;${myStyle}"> 
 </display:column> 
   <display:column  title="Sell Rate" style="width:50px;${myStyle}"> 
 </display:column>
  <display:column  title="Revenue" headerClass="header-rt" style="width:50px;border-right:medium solid #003366;${myStyle}"> 
 </display:column>
 
 <display:column  title="Rev. Quantity" headerClass="" style="width:50px;border-right:medium solid #003366;${myStyle}"> 
 </display:column>
 
  <display:column  title="Rev. Hours" style="width:40px;${myStyle}"> 
 </display:column>
  <display:column  title="Buy Rate" style="width:50px;${myStyle}"> 
 </display:column>
  <display:column  title="Expense" style="width:50px;${myStyle}"> 
 </display:column>
  <display:column  title="Sell Rate" style="width:50px;${myStyle}"> 
 </display:column>
  <display:column  title="Revenue" style="width:50px;${myStyle}"> 
 </display:column> 
 

  <display:column title="" class="hidden" headerClass="hidden" > 
    </display:column>
      <display:footer>                               
	                                
             <tr> 
  	          <td align="right" colspan="9"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="left" colspan="1">
               <input type="text" style="text-align:right;width:50px;" id="estimatedTotalExpense" name="estimatedTotalExpense${resourceTransferOI.id}"  readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
               </td>
               <td></td>
              <td><input type="text" style="text-align:right;width:50px;" id="estimatedTotalRevenue" name="estimatedTotalRevenue${resourceTransferOI.id}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/></td>
              <td></td>
          	<td></td>    
          <td align="left" width="40px">
          <input type="text" style="text-align:right;width:50px;" name="revTotalExpense" id="revTotalExpense" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td>
          <td align="left"></td>
  	      <td align="left" width="70px">
  	      <input type="text" style="text-align:right;width:50px;" name="revTotalRevenue" id="revTotalRevenue" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
  	      </td> 
  	       <td align="left"></td>
          </tr>
                   
           <tr>
            <td align="right" colspan="9"><b><div align="right">Gross&nbsp;Margin</div></b></td>
           <td align="left" colspan="2" ></td>		  	                     
           <td align="left" colspan="1">
          <%--  <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/> --%>
           </td>
           <td></td>
           <td>
           <input type="text" style="text-align:right;width:50px;" id="estGrossMarginForRevenue" name="estGrossMarginForRevenue" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
           </td>
           <td></td><td></td>
          <td align="left" width="40px">
         <%--  <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/> --%>
          </td>
  	      <td align="left"></td>
          <td align="left" width="70px" >
  	      <input type="text" style="text-align:right;width:50px;" id="revGrossMarginForRevenue" name="revGrossMarginForRevenue" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
  	      </td>
  	       <td align="left"></td>
           </tr>                  
                   
            <tr>
             <td align="right" colspan="9"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
            <td align="left" colspan="2" ></td>
            <td align="left" colspan="1">
            <%-- <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/> --%>
            </td>
            <td></td>
            <td><input type="text" style="text-align:right;width:50px;" id="estGrossMarginForRevenuePercentage" name="estGrossMarginForRevenuePercentage" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
            </td>
            <td></td><td></td><td></td>
  	      <td align="left"> </td>
          <td align="left" width="40px">
  	       <input type="text" style="text-align:right;width:50px;" id="revGrossMarginForRevenuePercentage" name="revGrossMarginForRevenuePercentage" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
  	      </td>
  	       <td align="left"></td>
         </tr>		  	                 
     </display:footer>
 </display:table> 
 </c:if>
  <c:if test="${resourceListForOI!='[]'}"> 
 <input type="button" name="OiSave" value="Save" onclick="saveOI();" class="update-btn-OI" style="width:60px;margin-bottom:5px;" tabindex="" />
 </c:if>
 
   <c:if test="${resourceListForOI!='[]'}"> 
	<input type="button" id="pricingAddLine" name="pricingAddLine" value="Add Resource" onclick="return contractForOI(this);addResources();" class="update-btn-OI" style="width:100px;margin-bottom:5px;" tabindex=""/> 
	<sec-auth:authComponent componentId="module.OITab.forSalesPersons">		
	<input type="button" name="pricingAddLine" value="Download Quote" onclick="showWorkOrderForQuote(this)" class="update-btn-OI" style="width:110px;margin-bottom:5px;" tabindex=""/>
	<input type="button" value="Copy Work Order" name="copyWorkOrder"  onclick="showDistinctWorkOrder(this)" class="update-btn-OI" style="width:120px;margin-bottom:5px;" tabindex=""/>
	<input type="button" value="Insert WO &#9658" name="insertWorkOrder"  onclick="repositionClose();insertDistinctWorkOrder(this)" class="update-btn-OI" style="width:90px;margin-bottom:5px;" tabindex=""/>
	 </sec-auth:authComponent> 
	 </c:if>
	  <c:if test="${resourceListForOI == '[]'}"> 
	  <input type="button" id="pricingAddLine" name="pricingAddLine" value="Add Resource" onclick="return contractForOI(this);addResources();" class="update-btn-OI" style="width:100px;margin-bottom:5px;" tabindex=""/> 
	<sec-auth:authComponent componentId="module.OITab.forSalesPersons">		
	<input type="button" name="pricingAddLine" value="Download Quote" onclick="repositionClose();downloadQuoteForOI()" disabled="true" class="update-btn-dis-OI" style="width:110px;margin-bottom:5px;" tabindex=""/>
	 </sec-auth:authComponent> 
	  <input type="button" value="Copy Work Order" name="copyWorkOrder"  disabled="true" class="update-btn-dis-OI" style="width:120px;margin-bottom:5px;" tabindex=""/>
	  </c:if>
<jsp:include flush="true" page="operationsIntelligenceJs.jsp"></jsp:include>
</div>
 <script type="text/javascript"> 
setOnSelectBasedMethods(["checkLimitFoTicket()"]);
setCalendarFunctionality();
</script>