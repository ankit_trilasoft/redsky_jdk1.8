package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Userlogfile;

public interface SSOLoginTokenDao extends GenericDao<Userlogfile, Long> {

	public void registerLoginToken(String sessionId, String loginToken, String userName) throws Exception;
}
