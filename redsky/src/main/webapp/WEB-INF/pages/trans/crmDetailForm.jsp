<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<head>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
    </style>
	 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
</head>
<s:form id="crmDetailForm" name="crmDetailForm" >
<s:hidden name="isCrmFieldValueChanged" id="isCrmFieldValueChanged"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>

<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1" border="0">
<tr><td height="5px" align="left"></td></tr>
<tr>
<td align="right" class="listwhitetext"  width="83">Work&nbsp;Area:</td>
<td align="left" style="width:255px"><configByCorp:customDropDown listType="map" list="${workareaList}" fieldValue="${customerRelation.workArea}"
    attribute=" class=list-menu name=customerRelation.workArea  style=width:200px  headerKey='' headerValue='' tabindex=''  onchange='crmFieldValueChanged();'"/>
</td>		
<td class="listwhitetext" align="right" width="40">From:</td>
 <c:if test="${not empty customerRelation.loadBeginDate}">
 <s:text id="loadBeginDateValue" name="${FormDateValue}"><s:param name="value" value="customerRelation.loadBeginDate"/></s:text>
 <td align="left"><s:textfield id="loadBeginDate1" cssClass="input-text" name="customerRelation.loadBeginDate" value="%{loadBeginDateValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="deleteDate(event,this);crmFieldValueChanged();"/></td>
 <td><img id="loadBeginDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calculateDate('${customerRelation.loadBeginDate}','loadBegin');checkDateValue('loadBegin');"/></td>
</c:if>

<c:if test="${empty customerRelation.loadBeginDate}">
<td align="left"><s:textfield id="loadBeginDate1" cssClass="input-text" name="customerRelation.loadBeginDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="deleteDate(event,this);crmFieldValueChanged();" /></td>
<td><img id="loadBeginDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calculateDate('${customerRelation.loadBeginDate}','loadBegin');checkDateValue('loadBegin');"/></td>
</c:if>				   
				
<td class="listwhitetext" align="right" width="20">To:</td>
<c:if test="${not empty customerRelation.loadEndDate}">
<s:text id="loadEndDateValue" name="${FormDateValue}"><s:param name="value" value="customerRelation.loadEndDate"/></s:text>
<td align="left"><s:textfield id="loadEndDate1" cssClass="input-text" name="customerRelation.loadEndDate" value="%{loadEndDateValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="deleteDate(event,this);crmFieldValueChanged();"   /></td>
<td><img id="loadEndDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calculateDate('${customerRelation.loadEndDate}','loadEnd');checkDateValue('loadEnd');" /></td>
</c:if>
<c:if test="${empty customerRelation.loadEndDate}">
<td align="left"><s:textfield id="loadEndDate1" cssClass="input-text" name="customerRelation.loadEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="deleteDate(event,this);crmFieldValueChanged();"/></td>
<td><img id="loadEndDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calculateDate('${customerRelation.loadEndDate}','loadEnd');checkDateValue('loadEnd');"/></td>
</c:if>
</td>		
<td class="listwhitetext" align="right" width="40">Period:</td>
<td align="left" style="width:70px"><s:select cssClass="list-menu" cssStyle="width:70px"  name="customerRelation.timeOfReturn" id="timeOfReturn" list="%{periodScheduler}"  headerKey="" headerValue=""  onchange="checkPeriodDate(this);crmFieldValueChanged();" /></td> 	

<td align="right" class="listwhitetext"  width="83">Favourite&nbsp;Hobby:</td>
<td align="left" style="width:218px"><configByCorp:customDropDown listType="map" list="${favHobbyList}" fieldValue="${customerRelation.favHobby}"
    attribute=" class=list-menu name=customerRelation.favHobby  style=width:150px  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
</tr>

<tr>
<td align="right" class="listwhitetext"  width="83">Position:</td>
<td align="left" style="width:159px"><configByCorp:customDropDown listType="map" list="${positionList}" fieldValue="${customerRelation.position}"
    attribute=" class=list-menu name=customerRelation.position  style=width:200px  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
<td align="right" class="listwhitetext" rowspan="2" width="108">Service&nbsp;Expectations 3 Principal Choices:</td>
<td align="left" rowspan="2" colspan="7" style="width:200px"><s:select cssClass="list-menu" name="customerRelation.serviceExpectations" list="%{serviceExpectationsList}" value="%{multiplServiceExpecTypes}" multiple="true"  headerKey="" headerValue=""  cssStyle="width:326px;height:59px;margin:3px 0 0px 0;" onchange="crmFieldValueChanged();" /></td>

<td align="right" class="listwhitetext"  width="60">Favourite&nbsp;Place:</td>
<td align="left" style="width:183px"><configByCorp:customDropDown listType="map" list="${favPalaceList}" fieldValue="${customerRelation.favPlace}"
	attribute=" class=list-menu name=customerRelation.favPlace  style=width:150px  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
</tr>

<tr>
<td align="right" class="listwhitetext"  style="vertical-align:top;">Frequency&nbsp;Of&nbsp;Move:</td>
<td align="left" style="width:220px;vertical-align:top;"><configByCorp:customDropDown listType="map" list="${frequencyOfMoveList}" fieldValue="${customerRelation.frequencyOfMove}"
    attribute=" class=list-menu name=customerRelation.frequencyOfMove  style=width:200px  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
<td align="right" class="listwhitetext"  style="vertical-align:top;" width="110">Favourite Color:</td>
<td align="left" style="width:50px;vertical-align:top;"><configByCorp:customDropDown listType="map" list="${favColorList}" fieldValue="${customerRelation.favColour}"
    attribute=" class=list-menu name=customerRelation.favColour  style=width:150px;  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
</tr>

<tr>
<td align="right" class="listwhitetext"  style="vertical-align:top;">Reason&nbsp;For&nbsp;Transfer:</td>
<td align="left" style="width:50px;vertical-align:top;"><configByCorp:customDropDown listType="map" list="${reasonForTransferList}" fieldValue="${customerRelation.reasonForTransfer}"
    attribute=" class=list-menu name=customerRelation.reasonForTransfer  style=width:200px;margin-top:-5px;  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
<td align="right" class="listwhitetext"  width="">Quality&nbsp;RS:</td>
<td align="left" colspan="7" style="width:50px"><configByCorp:customDropDown listType="map" list="${qualityRSSystemList}" fieldValue="${customerRelation.qualityRS}"
	attribute=" class=list-menu name=customerRelation.qualityRS  style=width:65px;margin-top:3px;  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
<td align="right" class="listwhitetext"  style="vertical-align:top;">Favourite&nbsp;Cake:</td>
<td align="left" style="width:50px;vertical-align:top;"><configByCorp:customDropDown listType="map" list="${favCakeList}" fieldValue="${customerRelation.favCake}"
	attribute=" class=list-menu name=customerRelation.favCake  style=width:150px;margin-top:-5px;  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
</tr>

<tr><td height="0px" align="left"></td></tr>
<tr>
<td colspan="2"></td>
<td align="right" class="listwhitetext"  width="106">Special&nbsp;Item:</td>
<td align="left" colspan="6" style="width:50px"><configByCorp:customDropDown listType="map" list="${specialItemList}" fieldValue="${customerRelation.specialItem}"
    attribute=" class=list-menu name=customerRelation.specialItem  style=width:215px  headerKey='' headerValue='' tabindex='' onchange='crmFieldValueChanged();'"/>
</td>
</tr>
<tr><td height="5px" align="left"></td></tr>
</table>

</s:form>
<script type="text/javascript"> 
setOnSelectBasedMethods(["findPeriodDateChanged();fieldValidate();crmFieldValueChanged()"]);
setCalendarFunctionalityForCrm();
</script>