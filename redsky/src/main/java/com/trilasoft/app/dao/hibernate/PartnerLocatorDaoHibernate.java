package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.PartnerLocatorDao;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.PartnerLocation;


public class PartnerLocatorDaoHibernate extends GenericDaoHibernate<PartnerLocation,Long> implements PartnerLocatorDao {
	
	public PartnerLocatorDaoHibernate() {   
        super(PartnerLocation.class);   
    }

	public String locatePartners(double inputLatitute, double inputLongitude, String countryCode, int withinMiles) {
		List allPotentialPartners= this.getSession().createSQLQuery("select latitude, longitude, id, lastName, " +
				"concat(billingAddress1, ', ', billingCity, '', billingState, ', ', billingZip, ',', billingCountry) as address, " +
				"billingPhone, 'www.sscw.com' as partnerUrl, location1 from partner where latitude is not null and " +
				"longitude is not null and isAgent=true and billingCountryCode like '" + countryCode + "%'").list();
		StringBuilder partners = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPartners.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			if (isPartnerNear(inputLatitute, inputLongitude, ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue(), withinMiles)) {
				count++;
				if (count > 1) partners.append(","); else partners.append("");
				partners.append("{\"id\":" + obj[2] + ", \"name\": \"" + obj[3] + "\", \"address\": \"" + obj[4] + "\", \"phone\": \"" + 
				obj[5] + "\", \"url\": \"" + obj[6] + "\", \"locationPhotoUrl\": \"" + obj[7] + "\", \"latitude\": " + obj[0] + ", \"longitude\": " + obj[1] + "}");
				
				PartnerLocation partnerLocation = new PartnerLocation();
				partnerLocation.setLatitude(((BigDecimal) obj[0]).doubleValue());
				partnerLocation.setLongitude(((BigDecimal) obj[0]).doubleValue());
				partnerLocation.setName("");
				partnerLocation.setAddress("");
				partnerLocation.setPhone("");
				partnerLocation.setUrl("");
				partnerLocation.setFacilityPhoto("");
			}

		}
		String prependStr = "{ \"count\":" + count + ", \"partners\":[";
		String appendStr = "]}";
		return prependStr + partners.toString() + appendStr;
	}
	
	private double getDistance(double originLatitude, double originLongitude,
			double destinationLatitude, double destinationLongitude) {
		double distance = distance(originLatitude,
				originLongitude, destinationLatitude, destinationLongitude, "M");
		return distance;
	}	

	private boolean isPartnerNear(double originLatitude, double originLongitude,
			double destinationLatitude, double destinationLongitude, int proximity) {
		double distance = distance(originLatitude,
				originLongitude, destinationLatitude, destinationLongitude, "M");
		if (distance <= proximity) return true;
		else return false;
	}   
	
	private static double distance(double lat1, double lon1, double lat2,
			double lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}
		return (dist);
	}
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts decimal degrees to radians : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	/*
	 *  TODO 
	 *  1. Need to handle auto
	 *  2. What happens if user does not provide some input parameters?
	 */
	public String locateAgents(double weight, double latitude,
			double longitude, String tariffApplicability, String countryCode, String packingMode, String poe) {
		String sql = "select p.latitude, p.longitude, p.id, p.lastName, " + //0,1,2,3
		"concat(p.billingAddress1) as address1, " + //4
		"concat(p.billingCity, ', ', p.billingState, ' ', " +
		"p.billingZip) as address2, " +	//5	
		"p.billingPhone, 'www.sscw.com' as partnerUrl, p.location1," + //6,7,8
		"group_concat(concat_ws(',', prgd.gridSection, prgd.label, prgd.basis, prgd.display) separator '!') as chargelist," + //9
		"prg.serviceRangeMiles " + //TODO: Handle additional columns cprg.serviceRangeMiles2, prg.serviceRangeMiles3.." //10
		"from partner p, partnerrategrid prg, partnerrategriddetails prgd where " +
		"p.partnerCode = prg.partnerCode and prg.id = prgd.partnerRateGridId and " +
		"p.latitude is not null and p.longitude is not null and " +
		"p.isAgent=true and p.billingCountryCode like '"+countryCode+"%' and prg.tariffApplicability = '"+tariffApplicability+"'" +
		"and ((prgd.gridSection = 'WEIGHT' and prgd.grid = '"+packingMode+"' and "+weight+" >= prgd.weight1 and "+weight+" <=  prgd.weight2) or " +
		"(prgd.gridSection = 'CHARGES'  and prgd.grid = '"+packingMode+"' ) or " +
		"(prgd.gridSection = 'HAULING' and prgd.grid = '"+packingMode+"' and prgd.label = '"+poe+"' ) )" +
		"and prg.effectivedate  = (select max(effectivedate) from partnerrategrid prg2 Where prg2.effectivedate <= sysdate()  " + //TODO: replace by loading date
		"and prg.partnercode = prg2.partnercode and prg2.status = 'Submitted' )"+
		"and convert(prgd.display, DECIMAL) > 0 " +
		"group by prg.partnercode";
		List allPotentialPartners= this.getSession().createSQLQuery(sql).list();

		StringBuilder partners = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPartners.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();

			double distance = getDistance(latitude, longitude, ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue());
			String[] charges = ((String) obj[9]).split("!");
			double[] extraMileageLimits = {200, 400}; //TODO: hardcoded -- read these values from the table
			double[] extraMileageCharges = {2, 4}; //TODO: hardcoded -- read these values from the table
			
			double serviceMileage = ((BigDecimal) obj[10]).doubleValue();
			if (distance <= (extraMileageLimits != null?extraMileageLimits[extraMileageLimits.length-1]:serviceMileage)) {
				count++;
				double quote = calculateQuote(weight, charges, serviceMileage, extraMileageLimits, extraMileageCharges, distance);
				if (count > 1) partners.append(","); else partners.append("");
				partners.append("{\"id\":" + obj[2] + ", \"name\": \"" + obj[3] + "\", \"address1\": \"" + obj[4] + "\", \"address2\": \"" + obj[5] + "\", \"phone\": \"" + 
				obj[6] + "\", \"url\": \"" + obj[7] + "\", \"locationPhotoUrl\": \"" + obj[8] + "\", \"latitude\": " + obj[0] + 
				", \"longitude\": " + obj[1] + ", \"rate\": " + quote + "}");
			}
		}
		String prependStr = "{ \"count\":" + count + ", \"partners\":[";
		String appendStr = "]}";
		return prependStr + partners.toString() + appendStr;
	}

	private double calculateQuote(double weight, String[] charges, double serviceMileage, double[] extraMileageLimits, double[] extraMileageCharges,   
			double distance) {
		double quote = 0;
		
		for (String charge:charges){
			String[] chargeDetail = charge.split(",");
			if(chargeDetail[0].equals("WEIGHT")){
				quote += computeWeightCharge(weight, distance, serviceMileage, Double.parseDouble(chargeDetail[3]),
						extraMileageLimits, extraMileageCharges);  
			} else if(chargeDetail[0].equals("CHARGES") || chargeDetail[0].equals("HAULING")){
				quote += computeOtherCharges(weight, chargeDetail[2], Double.parseDouble(chargeDetail[3]));  
			}  
		}
		return quote;
	}

	private double computeOtherCharges(double weight, String basis, double charge) {
		double computedCharge = 0;
		//if (basis.equalsIgnoreCase("FLAT")){
			computedCharge = charge; 
		//} //TODO: else -- provide implementation for other bases
		return computedCharge;
	}

	private double computeWeightCharge(double weight, double distance,
			double serviceMileage, double stdWeightCharge, double[] extraMileageLimits, double[] extraMileageCharges) {
		double weightCharge = stdWeightCharge*(weight/100);
		if (extraMileageLimits != null && distance > serviceMileage ) { 
			for (int i=0; i < extraMileageLimits.length;i++){
				if (distance <= extraMileageLimits[i]){
					weightCharge += extraMileageCharges[i]*(weight/100);
					break;
				}
			}
		}
		return weightCharge;
	}	
}

