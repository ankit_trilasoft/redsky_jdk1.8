//----Created By Bibhash---

package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AuditSetupDao;

import com.trilasoft.app.model.AuditSetup;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.service.AuditSetupManager;


public class AuditSetupManagerImpl extends GenericManagerImpl<AuditSetup, Long> implements AuditSetupManager{

	
	AuditSetupDao auditSetupDao;
	

    public AuditSetupManagerImpl(AuditSetupDao auditSetupDao) { 
        super(auditSetupDao); 
        this.auditSetupDao = auditSetupDao; 
    }


	public List findById(Long id) {
		return auditSetupDao.findById(id);
	}
	
	public List<AuditSetup> searchAuditSetup(String tableName, String corpID, String fieldName, String auditable,String isAlertValue) 
	{   
        return auditSetupDao.searchAuditSetup(tableName,fieldName,corpID,auditable, isAlertValue);   
    }

	public List auditSetupTable(String b)
	{
		return auditSetupDao.auditSetupTable(b);
	}
    public List auditSetupField(String tableName,String b)
    {
    	return auditSetupDao.auditSetupField(tableName, b);
    }


	public List getList(String corpID) {
		return auditSetupDao.getList(corpID);
	}
	
	 public List findMaximumId(){
	    	return auditSetupDao.findMaximumId(); 
	    }


	public List checkData(String tableName, String fieldName, String sessionCorpID) {
		return auditSetupDao.checkData(tableName, fieldName, sessionCorpID);
	}
	    

}
