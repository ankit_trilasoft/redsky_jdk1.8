package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PrintDailyPackage;

public interface PrintDailyPackageDao extends GenericDao<PrintDailyPackage, Long> {
	public List<PrintDailyPackage> getByCorpId(String corpId,String tabId);
	public List<PrintDailyPackage> findChildByParentId(long id);
	public void deleteByParentId(long id);
	public List findAutoCompleterList(String jrxmlName, String corpId);
	public List<PrintDailyPackage> searchPrintPackage(String formName,String corpId, String serviceType, String serviceMode,String military);
	public void deleteChild(String corpId);
	
}
