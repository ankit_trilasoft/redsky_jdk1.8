package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ContractAccountDao;
import com.trilasoft.app.model.ContractAccount;
import com.trilasoft.app.service.ContractAccountManager;

public class ContractAccountManagerImpl extends GenericManagerImpl<ContractAccount,Long> implements ContractAccountManager{
	
	ContractAccountDao contractAccountDao;
	
	public ContractAccountManagerImpl(ContractAccountDao contractAccountDao){
		super(contractAccountDao);
		this.contractAccountDao=contractAccountDao;
		
	}

	public List findMaximumId() {
		return contractAccountDao.findMaximumId();
	}

	public List findContractAccountList(String contract, String corpId) {
		return contractAccountDao.findContractAccountList(contract, corpId);
	}

	public List isExisted(String accountCode,String contract, String corpId) {
		return contractAccountDao.isExisted(accountCode,contract, corpId);
	}
	public List findContractAccountByContract(String sessionCorpID){
		return contractAccountDao.findContractAccountByContract(sessionCorpID);
	}

	public List validateAccountcode(String accountCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		return contractAccountDao.validateAccountcode(accountCode, sessionCorpID);
	}
	public List findContractAccountRespectToContract(String contract,String sessionCorpID){
		return contractAccountDao.findContractAccountRespectToContract(contract, sessionCorpID);
	}
	public List findChildCode(String parentCode , String sessionCorpID){
		return contractAccountDao.findChildCode(parentCode ,sessionCorpID);
	}
	public int countPartnerCode(String childCode,String partnerCode,String contract,String sessionCorpID ){
		return contractAccountDao.countPartnerCode(childCode,partnerCode,contract,sessionCorpID);
	}
	public List findChildAccountCode(String partnerCode,String contract,String sessionCorpID ){
		return contractAccountDao.findChildAccountCode(partnerCode,contract,sessionCorpID);
	}
	public List checkPublicParentCode(String ChildCode,String partnerCode,String sessionCorpID ){
		return contractAccountDao.checkPublicParentCode( ChildCode , partnerCode ,sessionCorpID);
	}
	public void findDeleteContractAccountOfOtherCorpId(String contractName, String agentCorpId){
		contractAccountDao.findDeleteContractAccountOfOtherCorpId( contractName , agentCorpId);
	}

}
