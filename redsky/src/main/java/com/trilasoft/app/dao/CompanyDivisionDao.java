package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CompanyDivision;

public interface CompanyDivisionDao extends GenericDao<CompanyDivision, Long>  {
	public List getInvCompanyCode(String corpID);
	public List getPayCompanyCode(String corpID);
	public List getPayCompanyCodeForUGCN(String corpID);
	public List findCompanyCodeList(String corpID);
	public List findCompanyDivFlag(String corpID);
	public Map<String, String> findDefaultStateList(String bucket2, String corpId);
	public List checkCompayCode(String compayCode ,String corpID);
	public List checkBookingAgentCode(String bookingAgentCode ,String corpID);
	public String searchBaseCurrencyCompanyDivision(String compayCode ,String corpID);
	public List getBookingAgentCode(String companyDivision, String sessionCorpID);
	public List getForUGCNInvCompanyCode(String corpID);
	public List findCompanyCodeByBookingAgent(String bookingAgentCode,String corpID);
	public Map<String, String> getUTSICompanyDivision(String sessionCorpID);
	public Map<String, String> getCompanyDivisionCurrencyMap(String sessionCorpID);
	public Map<String, String> getAgentCompanyDivisionCurrencyMap();
	public Map<String, String> getAgentBookingAgentCompanyDivisionCurrencyMap();
	public int findGlobalCompanyCode(String corpID);
	public Map<String,String> getComDivCodePerCorpIdList();
	public List findCompanyCodeListForVanLine(String sessionCorpID);
	public List findCMMDMMAgentList();
}
