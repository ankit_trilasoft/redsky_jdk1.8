/**
 * WSAnswerResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.result;

public class WSAnswerResponse  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private long answerId;

    private long respondentId;

    public WSAnswerResponse() {
    }

    public WSAnswerResponse(
           long versionId,
           long answerId,
           long respondentId) {
        super(
            versionId);
        this.answerId = answerId;
        this.respondentId = respondentId;
    }


    /**
     * Gets the answerId value for this WSAnswerResponse.
     * 
     * @return answerId
     */
    public long getAnswerId() {
        return answerId;
    }


    /**
     * Sets the answerId value for this WSAnswerResponse.
     * 
     * @param answerId
     */
    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }


    /**
     * Gets the respondentId value for this WSAnswerResponse.
     * 
     * @return respondentId
     */
    public long getRespondentId() {
        return respondentId;
    }


    /**
     * Sets the respondentId value for this WSAnswerResponse.
     * 
     * @param respondentId
     */
    public void setRespondentId(long respondentId) {
        this.respondentId = respondentId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSAnswerResponse)) return false;
        WSAnswerResponse other = (WSAnswerResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.answerId == other.getAnswerId() &&
            this.respondentId == other.getRespondentId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getAnswerId()).hashCode();
        _hashCode += new Long(getRespondentId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSAnswerResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://result.form.v68.api.keysurvey.com", "WSAnswerResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("answerId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "answerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respondentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "respondentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
