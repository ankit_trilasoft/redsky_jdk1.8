/**
 * FormDesignManagementLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class FormDesignManagementLocator extends org.apache.axis.client.Service implements com.keysurvey.api.v68.form.design.FormDesignManagement {

    public FormDesignManagementLocator() {
    }


    public FormDesignManagementLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FormDesignManagementLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FormDesignManagementServicePort
    private java.lang.String FormDesignManagementServicePort_address = "http://www.keysurvey.co.uk:80/Member/api/v68/form/design/FormDesignManagementService";

    public java.lang.String getFormDesignManagementServicePortAddress() {
        return FormDesignManagementServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FormDesignManagementServicePortWSDDServiceName = "FormDesignManagementServicePort";

    public java.lang.String getFormDesignManagementServicePortWSDDServiceName() {
        return FormDesignManagementServicePortWSDDServiceName;
    }

    public void setFormDesignManagementServicePortWSDDServiceName(java.lang.String name) {
        FormDesignManagementServicePortWSDDServiceName = name;
    }

    public com.keysurvey.api.v68.form.design.FormDesignManagementService getFormDesignManagementServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FormDesignManagementServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFormDesignManagementServicePort(endpoint);
    }

    public com.keysurvey.api.v68.form.design.FormDesignManagementService getFormDesignManagementServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.keysurvey.api.v68.form.design.FormDesignManagementServicePortBindingStub _stub = new com.keysurvey.api.v68.form.design.FormDesignManagementServicePortBindingStub(portAddress, this);
            _stub.setPortName(getFormDesignManagementServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFormDesignManagementServicePortEndpointAddress(java.lang.String address) {
        FormDesignManagementServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.keysurvey.api.v68.form.design.FormDesignManagementService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.keysurvey.api.v68.form.design.FormDesignManagementServicePortBindingStub _stub = new com.keysurvey.api.v68.form.design.FormDesignManagementServicePortBindingStub(new java.net.URL(FormDesignManagementServicePort_address), this);
                _stub.setPortName(getFormDesignManagementServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FormDesignManagementServicePort".equals(inputPortName)) {
            return getFormDesignManagementServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "FormDesignManagement");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "FormDesignManagementServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FormDesignManagementServicePort".equals(portName)) {
            setFormDesignManagementServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
