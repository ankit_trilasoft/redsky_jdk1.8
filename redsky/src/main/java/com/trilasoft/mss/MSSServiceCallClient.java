package com.trilasoft.mss;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.OperationContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.log4j.Logger;

import com.trilasoft.mss.webservices.GetQuotePdf;
import com.trilasoft.mss.webservices.GetQuotePdfResponse;
import com.trilasoft.mss.webservices.MSSOrder;
import com.trilasoft.mss.webservices.PlaceOrder;
import com.trilasoft.mss.webservices.PlaceOrderResponse;

public class MSSServiceCallClient {
	 static final Logger logger=Logger.getLogger(MSSServiceCallClient.class); 
	private static  byte[] pdf;
	public static byte[] callMSSService(Object mssobj, String customerID ,String password ) throws Exception  {
		ServiceMSSOrderStub stub = new ServiceMSSOrderStub();
			try{
				Options options = stub._getServiceClient().getOptions();
				HttpTransportProperties.Authenticator auth = new HttpTransportProperties.Authenticator();
				auth.setUsername(customerID);
				auth.setPassword(password);
				options.setProperty(
						org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE,
						auth);
				options.setProperty(HTTPConstants.HTTP_PROTOCOL_VERSION,
						HTTPConstants.HEADER_PROTOCOL_10);
				stub._getServiceClient().setOptions(options);
				GetQuotePdf quotepdf= new GetQuotePdf();
				quotepdf.setCustomerID(Integer.parseInt(customerID));
				quotepdf.setPassword(password);
				quotepdf.setOrd((MSSOrder)mssobj);
				//GetServiceCategoryTypes getServiceCategoryTypes42 = new GetServiceCategoryTypes();
				try {
					GetQuotePdfResponse quotePdfResponse=stub.getQuotePdf(quotepdf);
					 pdf= quotePdfResponse.getGetQuotePdfResult();
				} catch (RemoteException e) {
					e.printStackTrace();
					pdf=e.getMessage().toString().getBytes();
				}
				
			}catch (Exception e) {
			e.printStackTrace();
		}
			finally{
			try {
			    OperationContext operationContext = stub
			      ._getServiceClient().getLastOperationContext();
			    logger.warn("Printing Messages");
			    if (operationContext != null) {
			     MessageContext outMessageContext = operationContext
			       .getMessageContext("Out");
			     if (outMessageContext != null) {
			    	 logger.warn("OUT SOAP: Pricing Preview "
			        + outMessageContext.getEnvelope().toString());
			     }
			     MessageContext inMessageContext = operationContext
			       .getMessageContext("In");
			     if (inMessageContext != null) {
			    	 logger.warn("IN SOAP:  Pricing Preview "
			        + inMessageContext.getEnvelope().toString());
			     }
			    }
			   } catch (Throwable e) {
			    System.out.println("Cannot log soap messages: "
			      + e.getMessage());
			   }
		}
   return pdf;
	}
public static String CallPlaceOrderService(Object mssobj,String customerID,String password) throws Exception{
	String ordernumber = "";
	ServiceMSSOrderStub stub = new ServiceMSSOrderStub();
		try {
			Options options = stub._getServiceClient().getOptions();
			HttpTransportProperties.Authenticator auth = new HttpTransportProperties.Authenticator();
			auth.setUsername(customerID);
			auth.setPassword(password);
			options.setProperty(
					org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE,
					auth);
			options.setProperty(HTTPConstants.HTTP_PROTOCOL_VERSION,
					HTTPConstants.HEADER_PROTOCOL_10);
			stub._getServiceClient().setOptions(options);
			PlaceOrder placeOrder=new PlaceOrder();
			placeOrder.setCustomerID(Integer.parseInt(customerID));
			placeOrder.setPassword(password);
			placeOrder.setOrd((MSSOrder) mssobj);
			
			try {
				PlaceOrderResponse placeOrderResponse=stub.placeOrder(placeOrder);
				ordernumber=placeOrderResponse.getPlaceOrderResult();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.warn(e.getMessage() +"---"+(e.getCause()!= null?e.getCause().getMessage():""));
				ordernumber=e.getMessage().toString();
				return ordernumber.concat("@@@@");
			}
			
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		try {
		    OperationContext operationContext = stub
		      ._getServiceClient().getLastOperationContext();
		    logger.warn("Printing Messages");
		    if (operationContext != null) {
		     MessageContext outMessageContext = operationContext
		       .getMessageContext("Out");
		     if (outMessageContext != null) {
		    	 logger.warn("OUT SOAP: MSS Order "
		        + outMessageContext.getEnvelope().toString());
		     }
		     MessageContext inMessageContext = operationContext
		       .getMessageContext("In");
		     if (inMessageContext != null) {
		    	 logger.warn("IN SOAP: MSS Order "
		        + inMessageContext.getEnvelope().toString());
		     }
		    }
		   } catch (Throwable e) {
		    System.out.println("Cannot log soap messages: "
		      + e.getMessage());
		   }
	}
	return ordernumber;
}
}
