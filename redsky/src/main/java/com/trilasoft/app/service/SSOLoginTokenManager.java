package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Userlogfile;

public interface SSOLoginTokenManager extends GenericManager<Userlogfile, Long>{

	public void registerLoginToken(String sessionId, String loginToken, String userName) throws Exception;
}
