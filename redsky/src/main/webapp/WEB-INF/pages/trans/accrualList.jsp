<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="accural.heading"/></title>   
    <meta name="heading" content="<fmt:message key='accural.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    

<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
}


</script>
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:5px;
!margin-bottom:-1px;
margin-top:-32px;
!margin-top:-52px;
padding:2px 0px;

text-align:right;
width:100%;

}
</style>

</head>
	<s:set name="accrualRecord" value="accrualRecord" scope="request"/>
<div id="layer4" style="width:835px">	
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Accruall List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<display:table name="accrualRecord" class="table" requestURI="" id="accrualRecord" export="${empty param.popup}" defaultsort="1" pagesize="10" 
		style="width:835px" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	
		<display:column property="shipNumber" sortable="true" titleKey="accural.shipNumber" />
		<display:column property="registrationNumber" sortable="true" titleKey="accural.registrationNumber" />
		<display:column property="firstName" sortable="true" titleKey="accural.name1" />
		
		<display:column property="job" sortable="true" titleKey="accural.job" />
		<display:column property="billToCode"  sortable="true" titleKey="accural.bilToCode" />
		<c:if test="${i == 20}"> 
		<display:column style="text-align: right;" property="revisionRevenueAmount" headerClass="containeralign" sortable="true" titleKey="accural.revisionRevenueAmount" />
		<display:column property="recGl" style="text-align: right;" sortable="true" headerClass="containeralign" titleKey="accural.recGl" />		
		</c:if>
		<c:if test="${i == 10}"> 
		<display:column property="revisionExpense" style="text-align: right;" headerClass="containeralign" sortable="true" titleKey="accural.revisionExpense" />
		<display:column property="payGl" sortable="true" style="text-align: right;" headerClass="containeralign" titleKey="accural.payGl" />
		</c:if>
		<display:column property="revenueRecognition" style="text-align: right;" headerClass="containeralign" sortable="true" titleKey="accural.recognition" />
		

    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>     
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table> 
</div>
<script type="text/javascript">   
   Form.focusFirstElement($("partnerListForm"));      
</script>