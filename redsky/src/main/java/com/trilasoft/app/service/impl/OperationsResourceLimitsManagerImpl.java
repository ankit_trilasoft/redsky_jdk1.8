package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.OperationsResourceLimitsDao;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.service.OperationsResourceLimitsManager;

public class OperationsResourceLimitsManagerImpl extends GenericManagerImpl<OperationsResourceLimits, Long> implements OperationsResourceLimitsManager {
	OperationsResourceLimitsDao operationsResourceLimitsDao;

	public OperationsResourceLimitsManagerImpl(OperationsResourceLimitsDao operationsResourceLimitsDao) {
		super(operationsResourceLimitsDao);
		this.operationsResourceLimitsDao = operationsResourceLimitsDao;
	}

	public List isExisted(Date workDate, String hub, String sessionCorpID) {
		return operationsResourceLimitsDao.isExisted(workDate, hub, sessionCorpID);
	}

	public List getResourceLimitsByHub(String hub, String category) {
		return operationsResourceLimitsDao.getResourceLimitsByHub(hub, category);
	}
	public List getItemListByCategory(String category, String resource, String sessionCorpID){
		return operationsResourceLimitsDao.getItemListByCategory(category, resource, sessionCorpID);
	}
	
	public List checkResource(String resource, String sessionCorpID){
		return operationsResourceLimitsDao.checkResource(resource, sessionCorpID);	
	}
}
