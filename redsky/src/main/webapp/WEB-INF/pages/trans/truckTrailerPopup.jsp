<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>TrailerCode</title>      
</head> 
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<s:form id="truckTrailerPopup" action='${empty param.popup?"truckTrailerPopup.html":"truckTrailerPopup.html?decorator=popup&popup=true"}' method="post" > 
<div id="layer4" style="width:100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" method="searchTrailerPopupList" key="button.search"/>   
    <s:reset  cssClass="cssbutton" value="Clear" cssStyle="width:55px; height:25px;!margin-bottom: 10px;"/> 
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:98%"  >
		<thead>
		<tr>
		<th>Local Truck #</th>
		<th>Owner/Pay To</th>
		<th colspan="2">&nbsp;</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="localNumber"  required="true" cssClass="input-text" size="20"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="ownerPayTo" required="true" cssClass="input-text" size="20"/>
					</td>
					<td width="" align="center" style="border-left: hidden;">
					    <c:out value="${searchbuttons}" escapeXml="false" />   
					</td>
				</tr>
				</tbody>
			</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>

 <display:table name="trailerPopupList" class="table" requestURI="" id="trailerPopupList" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'> 
        <c:if test="${empty param.popup}">  
		<display:column property="localNumber" sortable="true" title="GL Type" href="" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" title="Local Truck#"/>   
    </c:if> 
    	<display:column property="description" sortable="true" title="Description"/> 
    	<display:column property="ownerPayTo" sortable="true" title="Owner/Pay To"/> 
        </display:table> 
</s:form>