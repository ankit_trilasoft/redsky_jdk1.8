package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.TableCatalog;

public interface TableCatalogDao extends GenericDao<TableCatalog, Long>{
	
	public List getTableListValue(String tableN,String tableD);
	public List getData(String tableName,String sessionCorpID);

}

