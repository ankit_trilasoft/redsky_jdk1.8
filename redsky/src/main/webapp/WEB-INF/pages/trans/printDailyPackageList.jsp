<%@ include file="/common/taglibs.jsp"%>   
<head>   
    <title>Print Daily Package Details</title>   
    <meta name="heading" content="Print Daily Package List"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
 <style>
 span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
</style>
</head>

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:60px; height:25px"  
        onclick="location.href='<c:url value="/editPrintDailyPackage.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:60px; height:25px;margin-right:2px;" align="top" key="button.search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:60px; height:25px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="searchForm" action="searchPrintPackage">  
<div id="layer1" style="width:100%"> 
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
<table class="table" style="width:99%;" border="0">
<thead>
<tr>
<th>Form Name</th>
<th>Service Type</th>
<th>Mode</th>
<th>Military</th>
</tr>
</thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <input type="text" id="formName" name="formName" value="${formName}" class="input-text" size="50"/>
			</td>
			<td width="" align="left">
			    <s:select id="wtServiceType" name="serviceType" list="%{wtServiceType}" value="%{serviceType}" headerKey=""	headerValue="" cssStyle="width:272px;" cssClass="list-menu" />
			</td>
			<td width="" align="left">
			    <s:select id="mode" name="serviceMode" list="%{mode}" value="%{serviceMode}" cssStyle="width:90px;" cssClass="list-menu" />
			</td>
			<td width="" align="left">
			    <s:select id="military" name="military" list="%{yesno}" value="%{military}" cssStyle="width:72px;" cssClass="list-menu" />
			</td>
						
			</tr>
			<tr>
				<td width="" align="left" colspan="3"></td>	
							
				<td style="border-left:hidden;text-align:right;padding:0px;" class="listwhitetext">
				<table style="margin:0px;padding:0px;border:none; float:right;">
				<tr>
				
				<td style="border:none;vertical-align:bottom;">
				    <c:out value="${searchbuttons}" escapeXml="false" />   
				    </td>
				    </tr>
				    </table>
				</td>
			</tr>	
			
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>


<c:out value="${searchresults}" escapeXml="false" />  
		<div id="newmnav" style="margin-bottom:0px;!margin-bottom:-13px;">
		  <ul>
		    <li><a onclick="targetPage('Print');"><span>Print</span></a></li>
		  </ul>
		  
		  	<ul>
		    	<li id="newmnav1"><a class="current"><span>SetUp</span></a></li>
		  	</ul>
		  	<ul>
		    	<li><a onclick="targetPage('Waste');"><span>Waste Basket</span></a></li>
		  	</ul>
		  
		</div>
		<div class="spnblk">&nbsp;</div>

<display:table name="printDailyPackageList" class="table" requestURI="" id="printDailyPackageList" pagesize="10" style="width:99%;margin-top:1px;margin-left: 5px; " >   
	<display:column  style="width:5px;">	
		<img id="print-${printDailyPackageList.id}" onclick ="showChild('${printDailyPackageList.id}','child${printDailyPackageList.id}',this,this.id);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		<img id="hideprint-${printDailyPackageList.id}" onclick ="hideChild('${printDailyPackageList.id}','${printDailyPackageList_rowNum}',this.id);" src="${pageContext.request.contextPath}/images/minus1-small.png" style="display: none;" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<display:column title="Form Name" style="width: 90px;">
		<a href="editPrintDailyPackage.html?id=${printDailyPackageList.id}"> ${printDailyPackageList.formName}</a>
	</display:column>
	<display:column property="module" title="Module Name" style="width: 50px;"></display:column>
	<display:column title="One per S/O per day" style="width: 15px;">
		<c:if test="${printDailyPackageList.onePerSOperDay == 'Y'}">
			<c:out value="YES"></c:out>
		</c:if>
		<c:if test="${printDailyPackageList.onePerSOperDay == 'N'}">
			<c:out value="NO"></c:out>
		</c:if>
	</display:column>
	<display:column property="printSeq" title="Print Seq" style="width: 10px;"></display:column>
	<display:column property="noOfCopies" title="# of copies" style="width: 15px;"></display:column>
	<display:column property="wtServiceType" title="WT ServiceType" style="width: 15px;">		
	</display:column>
	<configByCorp:fieldVisibility componentId="component.field.PrintDailyPackage.jobBillToCode">
		<display:column property="job" title="Job Type" style="width: 15px;"></display:column>
	</configByCorp:fieldVisibility>
	<display:column property="mode" title="Mode" style="width: 15px;"></display:column>
	<display:column title="Military" style="width: 15px;">
		<c:if test="${printDailyPackageList.military == 'Y'}">
			<c:out value="YES"></c:out>
		</c:if>
		<c:if test="${printDailyPackageList.military == 'N'}">
			<c:out value="NO"></c:out>
		</c:if>
	</display:column>
	<configByCorp:fieldVisibility componentId="component.field.PrintDailyPackage.jobBillToCode">
		<display:column property="billToCode" title="Bill To Code" style="width: 15px;"></display:column>
	</configByCorp:fieldVisibility>
	<c:if test="${tabId == 'waste'}">
		<display:column title="Recover" style="width:15px;">
			<a><img align="middle" onclick="confirmSubmit('${printDailyPackageList.id}','recover');" style="margin: 0px 0px 0px 8px;" src="images/recover.png"/></a>
		</display:column>
	</c:if>
	
	<display:column title="Remove" style="width: 10px;">
		<a><img align="middle" onclick="confirmSubmit('${printDailyPackageList.id}','delete');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	
	<%-- <div id="child${printDailyPackageList.id}"></div>  --%>
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</div>

</s:form> 

<script type="text/javascript">
function clear_fields(){
	document.getElementById('formName').value = '';
	document.getElementById('wtServiceType').value = '';
	document.getElementById('mode').value = '';
	document.getElementById('military').value = '';
}
function confirmSubmit(printId,str){
	var agree=confirm("Are you sure you wish to "+str+" this Print Item ?");
	if (agree){
		var url = 'moveWasteBasket.html?&id='+printId;
		location.href = url;
	 }else{
		return false;
	}
}

function hideChild(id,rowNum,rowId){
	val = "ch"+id;
	var table = document.getElementById('printDailyPackageList');
    var rowCount = table.rows.length;
    for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        if(row != undefined && row != null && val == row.id){
        	table.deleteRow(i);
        	document.getElementById('print-'+id).style.display='block';
        	document.getElementById('hideprint-'+id).style.display='none';
        }
    }
}

function showChild(id,divId,position,rowId){
	document.getElementById('print-'+id).style.display='none';
	document.getElementById('hideprint-'+id).style.display='block';
	
	var table=document.getElementById("printDailyPackageList");
	var rownum = document.getElementById(rowId);
	var myrow = rownum.parentNode;
	var newrow = myrow.parentNode.rowIndex;
	val = newrow+1;
	var row=table.insertRow(val);
	row.setAttribute("id","ch"+id);
	row.setAttribute("name",id);
	row.innerHTML = "<td colspan=\"10\"><div id="+id+"></div></td>";
	
	var url='printPackageChildAjax.html?ajax=1&id='+id+'&decorator=simple&popup=true';
	new Ajax.Request(url,
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      //alert(response)
			       var mydiv = document.getElementById(id);
			      mydiv.innerHTML = response;
			      mydiv.style.display='block';
                  //mydiv.show(); 
			    },
			    onFailure: function(){
				    alert('Something went wrong...') 
				    }
			  });
	     return 0;
}
function targetPage(target){
	var url = '';
	if(target == 'Print'){
		url = 'printPackage.html?tabId=Print';
	}else if(target == 'Waste'){
		url = 'printWasteBasket.html?tabId=waste';
	}else if(target == 'setup'){
		url = 'printSetUp.html?tabId=setup';
	}
	
	location.href = url;
}
</script>