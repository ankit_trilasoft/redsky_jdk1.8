<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="exportDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='exportDetail.heading'/>"/>   
</head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<s:form id="internationalForm" action="saveInternational" method="post" validate="true">   
<s:hidden name="international.id" value="%{international.id}"/>
<table width="100%" border="0">

<tr>
<td align="left" width="5%"><s:label label="Release Storage" /></td>
<td width="18%"><s:select name="international.releaseFromStorage" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Prelim.Notification" /></td>
<td align="left" width="12%"><s:textfield name="international.preliminaryNotification" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Clear at Wh or Pier" /></td>
<td width="18%"><s:select name="international.allRecivedWHTA" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="5%"><s:label label="Final Notification" /></td>
<td align="left" width="12%"><s:textfield name="international.finalNotification" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Instrc to Forwarder" /></td>
<td align="left" width="12%"><s:textfield name="international.instruction2FF" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="P U container" /></td>
<td width="18%"><s:select name="international.pickUp2PierWH" list="{':','T','A'}"/></td>
<td align="left" width="12%"><s:textfield name="international.pickUp2PierWHTA" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Instrc From Forwarder" /></td>
<td align="left" width="12%"><s:textfield name="international.instructionFromFF" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Return cntnr" /></td>
<td width="18%"><s:select name="international.returnContainer" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="12%"><s:textfield name="international.returnContainerDateA" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Requested GBL" /></td>
<td align="left" width="12%"><s:textfield name="international.requestGBL" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Last Free Day" /></td>
<td align="left" width="12%"><s:textfield name="international.lastFree" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Received GBL" /></td>
<td align="left" width="12%"><s:textfield name="international.recivedGBL" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="GST/VAT refund" /></td>
<td align="left" width="12%"><s:textfield name="international.gst" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="OMNI" /></td>
<td align="left" width="12%"><s:textfield name="international.omniReport" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="OSO" /></td>
<td align="left" width="12%"><s:textfield name="international.overseaShippingOrder" required="true" cssClass="text medium"/></td>
</tr>



<tr>
<td align="left" width="5%"><s:label label="IT#" /></td>
<td align="left" width="12%"><s:textfield name="international.itNumber" required="true" cssClass="text medium"/></td>
</tr>


<tr>
<td align="left" width="5%"><s:label label="Location" /></td>
<td align="left" width="12%"><s:textfield name="international.returnLocation" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="SED" /></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Flag carrier" /></td>
<td align="left" width="12%"><s:textfield name="international.flagCarrier" required="true" cssClass="text medium"/></td>
</tr>


<tr>
<td align="left" width="5%"><s:label label="Are there vendor Item to Pickup" /></td>
<td width="18%"><s:select name="international.vendorItem2Ship" list="{':','Y','N'}"/></td>
</tr>
</table>
  </s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("internationalForm"));   
</script>
