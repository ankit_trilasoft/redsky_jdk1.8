/**
 * @Class Name ServicePartner Manager
 * @Author Sunil Kumar Singh
 * @Version V01.0
 * @Since 1.0
 * @Date 01-Dec-2008
 */

package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.ServicePartner;

public interface ServicePartnerManager extends GenericManager<ServicePartner, Long> {
	public List<ServicePartner> findByLastName(String lastName);

	public List findByCarrierCode(String carrierCode);

	public List findMaxId();

	public List checkById(Long id);

	public List<ServicePartner> findByBroker(String shipNumber);

	public List<ServicePartner> findByCarriers(String shipNumber);

	public List<ServicePartner> findByAgent(String shipNumber);

	public List<ServicePartner> findByPartner(String shipNumber);

	public List<ServicePartner> findForParter(String corpId, String shipNumber);

	public List checkByShipNumber(String ship);

	public List<ServicePartner> findByCarrierFromAccountLine(String shipNumber);

	public int updateTrack(String shipNumber, String billLading,String hbillLading,String userName);

	public List<ServicePartner> portName(String portCode, String sessionCorpID);

	public List findCarrierFromAccLine(String carr, String vendorCode, String shipNumber);

	public List routingListOtherCorpId(Long sid);
	
	public List<ServicePartner> findByCarrier(String shipNumber);

	public List findTranshipped(String shipNumber);

	public int updateDeleteStatus(Long ids);

	public int updateServiceOrder(String carrierDeparture, String carrierArrival, String shipNumber,String userName);

	public int updateServiceOrderDate(Long id, String corpId,String userName);

	public int updateServiceOrderActualDate(Long id, String corpId, Long customerFileId,String userName);

	public List getContainerNumber(String shipNumber);

	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum);

	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum);

	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum);

	public List findMaximumChild(String shipNm);

	public List findMinimumChild(String shipNm);
	public List findTranshipChild(String shipNm);
	public List findCountChild(String shipNm);

	public List findClone(String shipNumber);
	
	public int copyAtd(Date atd, String shipNumber);
	
	public int copyAta(Date ata, String shipNumber);

	public List routingList(Long serviceOrderId);

	public Long findRemoteServicePartner(String carrierNumber, Long serviceOrderId);

	public void deleteNetworkServicePartner(Long serviceOrderId, String carrierNumber, String routingStatus);

	public List findServicePartnerBySid(Long sofid, String sessionCorpID);
	
	public List findRoutingLines(Long grpId, String sessionCorpID);
	
	public List routingListByGrpId(Long servicePartnerId);
	
	public List routingListByIdAndgrpId(Long grpId);
	
	public List routingWithNoLine(String sessionCorpId,Long grpId,Long masterServicePartnerId);
	
	public void updateRouting(String oldIdList);
	public List findRemoteRoutingList(String shipNumber);
	public List routingListByGrpId(Long grpId, Long contId);
	public void deleteNetworkServicePartnerByParentId(Long parentId, String shipNumber);
	public List getMaxAtArrivalDate(Long soId, String sessionCorpID);
	public List getCarrierListvalue(String partnerCode,String partnerlastName,String partneraliasName,String partnerbillingCountryCode,String partnerbillingState,String corpID);

	public List<ServicePartner> findByCarriersID(Long id);
	public List getCarrierNameForAgent(String partnerCode,String corpID);
	public void updateServicePartnerDetails(Long id,String fieldName,String fieldValue,String fieldName1,String fieldValue1,String tableName, String sessionCorpID, String userName);

	public void updateStatus(Long id, String routingStatus, String updateRecords);
	public List getCarrierName(String partnerCode,String corpID);
	
	//13674 - Create dashboard
	public List<ServicePartner> findTransptDetailList(String shipNumber,String corpID);
	//End DashBoard
	
}  
