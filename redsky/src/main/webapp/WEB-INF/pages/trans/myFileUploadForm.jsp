<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/scripts/login.js"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<head>
<title><fmt:message key="upload.title" /></title>
<meta name="heading" content="<fmt:message key='upload.heading'/>" />
<c:if test="${param.popup}">
	<link rel="stylesheet" type="text/css" media="all"
		href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" />
</c:if>
<style type="text/css">
input[type="checkbox"]
{
vertical-align:middle;
}

 .fd-zone {
      position: relative;
      overflow: hidden;
      width: 37em;
      margin: 0 auto;
      text-align: center;
      height:123px;
    }

    .fd-file {
      opacity: 0;
      font-size: 118px;
      position: absolute;
      right: 0;
      top: 0;
      z-index: 1;
      padding: 0;
      margin: 0;
       pointer-events: none;
      filter: alpha(opacity=0);
      font-family: sans-serif;
    }

    .fd-zone.over { border-color: maroon; background: #eee; }
/* collapse */

</style>


<script>




function exportExcel(){
	
	ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "Hauling Management", str, haulingMgt, "Hauling Info", ",");
}

function validateFields1(){
	if(document.forms['uploadForm'].elements['fileNameFor'].value=='SO'){
	if((document.forms['uploadForm'].elements['bookingAgent'].value=='BA') && (document.getElementById('isBookingAgent').checked)){
		document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['networkAgent'].value=='NA') && (document.getElementById('isNetworkAgent').checked)){
		document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['originAgent'].value=='OA') && (document.getElementById('isOriginAgent').checked)){
		document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['subOriginAgent'].value=='SOA') && (document.getElementById('isSubOriginAgent').checked)){
		document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['destAgent'].value=='DA') && (document.getElementById('isDestAgent').checked)){
		document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['subDestAgent'].value=='SDA') && (document.getElementById('isSubDestAgent').checked)){
		document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=false;
	}
	}
	if(document.forms['uploadForm'].elements['fileNameFor'].value=='CF'){
		if((document.forms['uploadForm'].elements['bookingAgent'].value=='BA') && (document.getElementById('isBookingAgent').checked)){
			document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=true;
		}else{
			document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=false;
		}
		if((document.forms['uploadForm'].elements['networkAgent'].value=='NA') && (document.getElementById('isNetworkAgent').checked)){
			document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=true;
		}else{
			document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=false;
		}
		if((document.forms['uploadForm'].elements['originAgent'].value=='OA') && (document.getElementById('isOriginAgent').checked)){
			document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=true;
		}else{
			document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=false;
		}
		if((document.forms['uploadForm'].elements['subOriginAgent'].value=='SOA') && (document.getElementById('isSubOriginAgent').checked)){
			document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=true;
		}else{
			document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=false;
		}
		if((document.forms['uploadForm'].elements['destAgent'].value=='DA') && (document.getElementById('isDestAgent').checked)){
			document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=true;
		}else{
			document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=false;
		}
		if((document.forms['uploadForm'].elements['subDestAgent'].value=='SDA') && (document.getElementById('isSubDestAgent').checked)){
			document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=true;
		}else{
			document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=false;
		}}
	if(document.forms['uploadForm'].elements['fileType'].value==""){
		alert("Select a file type");
		return false;
	}
	if(document.forms['uploadForm'].elements['file'].value==""){
		//document.forms['uploadForm'].elements['method:save'].disabled=true;
		document.forms['uploadForm'].elements['file'].focus;
		alert("Please select a file to upload");
		return false;
	}
	if(document.forms['uploadForm'].elements['venderInvoiceValue'].value=="true" && document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value==""){
		document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].focus;
		alert("Please Select Partner Code");
		return false;	
	}	
}

function validateFields(){	
	if(document.forms['uploadForm'].elements['fileType'].value==""){
		alert("Select a file type");
		return false;
	}
	if(document.forms['uploadForm'].elements['file'].value==""){
		document.forms['uploadForm'].elements['file'].focus;
		alert("Please select a file to upload");
		return false;
	}
	/* var fileTypeForPartnerCode = '${fileTypeForPartnerCode}'; 
    var fileTypePartnerCode = document.forms['uploadForm'].elements['fileType'].value ;
     if(fileTypeForPartnerCode.contains(fileTypePartnerCode)){ */
	if(document.forms['uploadForm'].elements['venderInvoiceValue'].value=="true" && document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value==""){
		document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].focus;
		alert("Please Select Partner Code");
		return false;	
	}
}

function validateUpload(){

			if(document.forms['uploadForm'].elements['file'].value==""){
			document.forms['uploadForm'].elements['method:save'].disabled=true;
			document.forms['uploadForm'].elements['file'].focus;
		}
		else{
			document.forms['uploadForm'].elements['method:save'].disabled=false;
			document.forms['uploadForm'].elements['file'].focus;
			
			
	}
}
function documentAccessControlAjax(){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	var Flag ='${bookingAgentFlag}';
    //alert(Flag)
    if(fileType=='Credit Card Authorization'){
        
   	 document.forms['uploadForm'].elements['isCportal'].checked=false;
   	 document.forms['uploadForm'].elements['isAccportal'].checked=false;
   	 document.forms['uploadForm'].elements['isPartnerPortal'].checked=false;
     document.forms['uploadForm'].elements['isServiceProvider'].checked=false;
   	if(Flag!='BA'){
   		document.forms['uploadForm'].elements['isBookingAgent'].checked=false;
   	}
   	if(Flag!='NA'){
   		document.forms['uploadForm'].elements['isNetworkAgent'].checked=false;
   	}
    if(Flag!='OA'){
   		document.forms['uploadForm'].elements['isOriginAgent'].checked=false;
   	}
    if(Flag!='SOA'){
   		document.forms['uploadForm'].elements['isSubOriginAgent'].checked=false;
   	}
    if(Flag!='DA'){
   	 document.forms['uploadForm'].elements['isDestAgent'].checked=false;
   	}
   	if(Flag!='SDA'){
   		document.forms['uploadForm'].elements['isSubDestAgent'].checked=false;
   	}
    }
    var fileType = document.forms['uploadForm'].elements['fileType'].value;
    //alert(fileType)
    var url="docAccessControl.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
    http331.open("GET", url, true);
    http331.onreadystatechange = handleHttpResponse331;
    http331.send(null);
	
}
function handleHttpResponse331(){
	 if (http331.readyState == 4){
         var results = http331.responseText
         results = results.trim();
         var res = results.replace("[",'');
         res = res.replace("]",'');
        // alert(res)
         if(res !='' && res.length>2){
        	 res = res.split("#");
        	 if(res[0]=='true'){
            			document.getElementById('isCportal').checked=true;
            		}else{
            			document.getElementById('isCportal').checked=false;
            		}
	        	 if(res[1]=='true'){
	         			document.getElementById('isAccportal').checked=true;
	         		}else{
	        			document.getElementById('isAccportal').checked=false;
	        		}
	        	 if(res[2]=='true'){
	         			document.getElementById('isPartnerPortal').checked=true;
	         		}else{
	        			document.getElementById('isPartnerPortal').checked=false;
	        		}
	        	 if(res[9]=='true'){
	         			document.getElementById('isServiceProvider').checked=true;
	         		}else{
	        			document.getElementById('isServiceProvider').checked=false;
	        		}
	        	 if(res[10]=='true'){
	         			document.getElementById('invoiceAttachment').checked=true;
	         		}else{
	        			document.getElementById('invoiceAttachment').checked=false;
	        		}
	        	 //alert(res[11])
	        	 if(res[11]=='true'){	                   
	                	var e1 = document.getElementById('venderInvoicesId');	
	                   	e1.style.display= 'block';
	                	e1.style.visibility= 'visible';
	                	document.getElementById('venderInvoiceValue').value=true;        			
	             	}else{
	             		var e1 = document.getElementById('venderInvoicesId');
	             		e1.style.display= 'none';
	             		e1.style.visibility= 'collapse';
	             		document.getElementById('venderInvoiceValue').value=false;
	             	}
	        	// alert(res[12])
	                if(res[12]=='true'){
	                	//alert(res[12])
	                	var e2 = document.getElementById('paymentStatusId');
	                	e2.style.display= 'block';
	                	e2.style.visibility= 'visible';
	             	}else{
	             		var e2 = document.getElementById('paymentStatusId');
	             		e2.style.display= 'none';
	             		e2.style.visibility= 'collapse';
	             	}
         }else{
             var e1 = document.getElementById('venderInvoicesId');
      		e1.style.display= 'none';
      		e1.style.visibility= 'collapse';
      		document.getElementById('venderInvoiceValue').value=false;
      		var e2 = document.getElementById('paymentStatusId');
     		e2.style.display= 'none';
     		e2.style.visibility= 'collapse';
         }
	 }
}

function documentAccessControl(){
     var fileType = document.forms['uploadForm'].elements['fileType'].value;
     var url="docAccessControl.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
}

function handleHttpResponse3(){
             if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
               //alert(results)
                var res = results.replace("[",'');
                res = res.replace("]",'');
                if(res !=''){
                if(results.length>2){
                res = res.split("#");
                if(res[0]=='true'){
           		document.forms['uploadForm'].elements['myFile.isCportal'].checked=true;
           		var checkedValueCp=document.forms['uploadForm'].elements['myFile.isCportal'].checked;
	             //document.uploadApplet.setCportal(checkedValueCp);
           		}
           		else{
           		document.forms['uploadForm'].elements['myFile.isCportal'].checked=false;
           	    //document.uploadApplet.setCportal('false');
           		}
           		if(res[1]=='true'){
           		document.forms['uploadForm'].elements['myFile.isAccportal'].checked=true;
           		var checkedValue=document.forms['uploadForm'].elements['myFile.isAccportal'].checked;
	           //document.uploadApplet.setAccportal(checkedValue);           		
           		}
           		else{
           		document.forms['uploadForm'].elements['myFile.isAccportal'].checked=false;
           		//document.uploadApplet.setAccportal('false');
           		}
           		if(res[2]=='true'){
           		document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked=true;
           		var checkedValuePP=document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked;
	            //document.uploadApplet.setPartnerPortal(checkedValuePP);
           		}
           		else{
           		document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked=false;
           		//document.uploadApplet.setPartnerPortal('false');
           		}
           		if(res[3]=='true'){
           			document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=true;
               		var isBookingAgentPP=document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked;
    	           //document.uploadApplet.setBookingAgent(isBookingAgentPP);
           		}else if(${bookingAgentFlag!='BA'}){
           			document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=false;
               		//document.uploadApplet.setBookingAgent('false');
           		}
                if(res[4]=='true'){
                	document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=true;
               		var isNetworkAgentPP=document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked;
    	           //document.uploadApplet.setNetworkAgent(isNetworkAgentPP);
           		}else if(${bookingAgentFlag!='NA'}){
           			document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=false;
               		//document.uploadApplet.setNetworkAgent('false');
           		}
                if(res[5]=='true'){
                	document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=true;
               		var isOriginAgentPP=document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked;
    	           //document.uploadApplet.setOriginAgent(isOriginAgentPP);
           		}else if(${bookingAgentFlag!='OA'}){
           			document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=false;
               		//document.uploadApplet.setOriginAgent('false');
           		}
                if(res[6]=='true'){
                	document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=true;
               		var isSubOriginAgentPP=document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked;
    	            //document.uploadApplet.setSubOriginAgent(isSubOriginAgentPP);
           		}else if(${bookingAgentFlag!='SOA'}){
           			document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=false;
               		//document.uploadApplet.setSubOriginAgent('false');
           		}
                if(res[7]=='true'){
                	document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=true;
               		var isDestAgentPP=document.forms['uploadForm'].elements['myFile.isDestAgent'].checked;
    	           // document.uploadApplet.setDestAgent(isDestAgentPP);
           		}else if(${bookingAgentFlag!='DA'}){
           			document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=false;
               		//document.uploadApplet.setDestAgent('false');
           		}
                if(res[8]=='true'){
                	document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=true;
               		var isSubDestAgentPP=document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked;
    	          // document.uploadApplet.setSubDestAgent(isSubDestAgentPP);
           		}else if(${bookingAgentFlag!='SDA'}){
           			document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=false;
               		//document.uploadApplet.setSubDestAgent('false');
           		}
                if(res[9]=='true'){
	             	document.forms['uploadForm'].elements['myFile.isServiceProvider'].checked=true;
	             	var checkedValueSp=document.forms['uploadForm'].elements['myFile.isServiceProvider'].checked;
	  	            //document.uploadApplet.setisServiceProvider2(checkedValueSp);
             	}else{
	             	document.forms['uploadForm'].elements['myFile.isServiceProvider'].checked=false;
	             	//document.uploadApplet.setisServiceProvider2('false');
             	}
                if(res[10]=='true'){
	             	document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=true;
	             	var checkedValueIA=document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked;
	  	            //document.uploadApplet.setinvoiceAttachment2(checkedValueIA);
             	}else{
	             	document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=false;
	             //	document.uploadApplet.setinvoiceAttachment2('false');
             	}
                if(res[11]=='true'){
                   //alert(res[11])
                   <c:if test="${not empty serviceOrder.id}"> 
                	var e1 = document.getElementById('venderInvoicesId');               
                	e1.style.display= 'block';
               	    e1.style.visibility= 'visible';
                	document.getElementById('venderInvoiceValue').value=true;                 	 
                	// document.uploadApplet.setVenderInvoiceStatus('true'); 
                	</c:if>
				     <c:if test="${empty serviceOrder.id}">
				     document.getElementById('venderInvoiceValue').value=false; 
				   //  document.uploadApplet.setVenderInvoiceStatus('false');
				     </c:if>   			
             	}else{
             		var e1 = document.getElementById('venderInvoicesId');
             		e1.style.display= 'none';
             		e1.style.visibility= 'collapse';
             		document.getElementById('venderInvoiceValue').value=false;
             		//document.uploadApplet.setVenderInvoiceStatus('false');
             		//document.uploadApplet.setIsVendorCode('');
             		//document.uploadApplet.setIsPayingStatus('');
             	}
                if(res[12]=='true'){
                	//alert(res[12])
                	<c:if test="${not empty serviceOrder.id}"> 
                	var e2 = document.getElementById('paymentStatusId');                
                	e2.style.display= 'block';
                	e2.style.visibility= 'visible';
                	</c:if>
             	}else{
             		var e2 = document.getElementById('paymentStatusId');
             		e2.style.display= 'none';
             		e2.style.visibility= 'collapse';
             	}
                if(res[3]=='true' && res[4]=='true' && res[5]=='true' && res[6]=='true' && res[7]=='true' && res[8]=='true' ){
                	document.forms['uploadForm'].elements['checkbox1'].checked=true;
                }else{
                	document.forms['uploadForm'].elements['checkbox1'].checked=false;
                }
           }
           }
           }
           else{
           document.forms['uploadForm'].elements['myFile.isAccportal'].checked=false;
          // document.uploadApplet.setCportal('false');
           document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked=false;
           //document.uploadApplet.setPartnerPortal('false');
           document.forms['uploadForm'].elements['myFile.isCportal'].checked=false;
           //document.uploadApplet.setAccportal('false');
           document.forms['uploadForm'].elements['myFile.isServiceProvider'].checked=false;
           //document.uploadApplet.setisServiceProvider2('false');
           document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=false;
           //document.uploadApplet.setinvoiceAttachment2('false');
           if(${bookingAgentFlag!='BA'}){
        	document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=false;   
           }
           if(${bookingAgentFlag!='NA'}){
        	  document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=false;   
           }
           if(${bookingAgentFlag!='OA'}){
        	  document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=false;   
           }
           if(${bookingAgentFlag!='SOA'}){
        	  document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=false;   
           }
           if(${bookingAgentFlag!='DA'}){
        	  document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=false;   
           }
           if(${bookingAgentFlag!='SDA'}){
        	 document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=false; 
           }
           
           	try{
           		document.forms['uploadForm'].elements['checkbox1'].checked=false;
           	var e1 = document.getElementById('venderInvoicesId');
           	e1.style.display= 'none';
     		e1.style.visibility = 'collapse';
     		document.getElementById('venderInvoiceValue').value=false;
     		//document.uploadApplet.setVenderInvoiceStatus('false');
     		//document.uploadApplet.setIsVendorCode('');
     		//document.uploadApplet.setIsPayingStatus('');
     		var e2 = document.getElementById('paymentStatusId');
     		e2.style.visibility = 'collapse';
           	}catch(e){}
           }
}
function isVenderValueSet(){
	//alert(document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value)
	 //document.uploadApplet.setVenderInvoiceStatus('true'); 
	//document.uploadApplet.setIsVendorCode(document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value);
	//document.uploadApplet.setIsPayingStatus(document.forms['uploadForm'].elements['myFile.accountLinePayingStatus'].value);
}
function winOpen(){
 	var sid=document.forms['uploadForm'].elements['serviceOrder.id'].value;
 	  openWindow('findVendorCode.html?sid=${serviceOrder.id}&partnerType=DF&accType=myFileNew&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=isVenderFilecabitateValue&fld_code=myFile.accountLineVendorCode&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].select();	
 }
function findAccountLinePayingStatus(){
	var accountLineVendorCode =document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value;
	//alert(accountLineVendorCode)
	if(accountLineVendorCode!=''){
		var url="findAccountLinePayingStatus.html?ajax=1&decorator=simple&popup=true&accountlineShipNumber=${serviceOrder.shipNumber}&accountlineVendorCode=" + encodeURI(accountLineVendorCode);
	   	http22.open("GET", url, true);
	   	http22.onreadystatechange = handleHttpResponseMap22;
	   	http22.send(null);
	}else{
	} 	
}
function handleHttpResponseMap22(){
    if(http22.readyState == 4){
		var results = http22.responseText
      results = results.trim();
      var res = results.substring(1,results.length-1); 
     // alert(res) 
     var payValue="";
     if(res=='A'){
    	 payValue='Approved';
     } else if(res=='I'){
    	 payValue='Internal Cost';
     }else if(res=='N'){
    	 payValue='Not Authorized';
     }else if(res=='P'){
    	 payValue='Pending with note';
     }else if(res=='S'){
    	 payValue='Short pay with notes';
     }   	
      document.forms['uploadForm'].elements['myFile.accountLinePayingStatus'].value=payValue;			     	 
	}
}
function winOpenEdit(){
 	var sid=document.forms['uploadForm'].elements['serviceOrder.id'].value;
 	  openWindow('findVendorCode.html?sid=${serviceOrder.id}&partnerType=DF&accType=myFileEdit&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description1&fld_code=accountLineVendorCode&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].select();	
 }
function findAccountLinePayingStatusEdit(){
	var accountLineVendorCode =document.forms['uploadForm'].elements['accountLineVendorCode'].value;
	//alert(accountLineVendorCode)
	if(accountLineVendorCode!=''){
		var url="findAccountLinePayingStatus.html?ajax=1&decorator=simple&popup=true&accountlineShipNumber=${serviceOrder.shipNumber}&accountlineVendorCode=" + encodeURI(accountLineVendorCode);
	   	http223.open("GET", url, true);
	   	http223.onreadystatechange = handleHttpResponseMap223;
	   	http223.send(null);
	}else{
	} 	
}
function handleHttpResponseMap223(){
    if(http223.readyState == 4){
		var results = http223.responseText
      results = results.trim();
	     // alert(results)
      var res = results.substring(1,results.length-1); 
     //alert(res) 
     var payValue="";
     if(res=='A'){
    	 payValue='Approved';
     } else if(res=='I'){
    	 payValue='Internal Cost';
     }else if(res=='N'){
    	 payValue='Not Authorized';
     }else if(res=='P'){
    	 payValue='Pending with note';
     }else if(res=='S'){
    	 payValue='Short pay with notes';
     }   	
      document.forms['uploadForm'].elements['accountLinePayingStatus'].value=payValue;			     	 
	}
}

function documentMap(targetElement){
   	var fileType = targetElement.value;
   	var url="docMapFolder.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
   	http2.open("GET", url, true);
   	http2.onreadystatechange = handleHttpResponseMap;
   	http2.send(null);
}
function handleHttpResponseMap(){
      if(http2.readyState == 4){
		var results = http2.responseText
        results = results.trim();
        var res = results.substring(1,results.length-1);
		document.forms['uploadForm'].elements['mapFolder'].options[0].text = res; 
		document.forms['uploadForm'].elements['mapFolder'].options[0].value = res;
		document.forms['uploadForm'].elements['mapFolder'].options[0].selected=true;					     	 
	}
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
var http223 = getHTTPObject();                  
var http22 = getHTTPObject();       
var http2 = getHTTPObject();
var http3 = getHTTPObject();
var http125678 = getHTTPObject();
var http331 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 
function setDescription(targetElement){
	documentMap(targetElement);	
	document.forms['uploadForm'].elements['myFile.description'].value = targetElement.options[targetElement.selectedIndex].text;
	if(targetElement.options[targetElement.selectedIndex].text==""){
	setForAlert();
	setForVenderCode();}
	else{
	//document.uploadApplet.setFileType((targetElement.options[targetElement.selectedIndex].value).replace(/ /g,'%20'));
	//alert(targetElement.options[targetElement.selectedIndex].text);
	//document.uploadApplet.setFileDescription((targetElement.options[targetElement.selectedIndex].text).replace(/ /g,'%20'));
	//document.uploadApplet.setFileDescription(document.forms['uploadForm'].elements['myFile.description'].value.replace(/ /g,'%20'));
	//document.uploadApplet.setCustomerNumber(document.forms['uploadForm'].elements['myFile.customerNumber'].value);
//	document.uploadApplet.setFileId(document.forms['uploadForm'].elements['myFile.fileId'].value);
//	document.uploadApplet.setId(document.forms['uploadForm'].elements['fileId'].value);
//	document.uploadApplet.setMyFileFor(document.forms['uploadForm'].elements['fileNameFor'].value);
	//alert(document.forms['uploadForm'].elements['fileNameFor'].value)
//	setTimeout("document.uploadApplet.setDocumentCategory(document.forms['uploadForm'].elements['myFile.documentCategory'].value.replace(/ /g,'%20'))",2000);
	//alert(document.forms['uploadForm'].elements['myFile.documentCategory'].value);
//	setTimeout("document.uploadApplet.setMapFolder(document.forms['uploadForm'].elements['mapFolder'].value)",2000);
	//document.uploadApplet.setMapFolder(document.forms['uploadForm'].elements['mapFolder'].value);
	//var field =document.forms['uploadForm'].elements['venderInvoiceValue'].value;
			
	//setTimeout("document.uploadApplet.setIsVendorCode(document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value)",2000);
	//setTimeout("document.uploadApplet.setIsPayingStatus(document.forms['uploadForm'].elements['myFile.accountLinePayingStatus'].value)",2000);
	
	}
}
function setDescriptionDuplicate(targetElement){
	document.forms['uploadForm'].elements['description'].value = targetElement.options[targetElement.selectedIndex].text;
}
function checkSecureDoc(target){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 target.checked=false;
	}
}
function setDocCat(targetElement){
	//alert("hi");
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	var Flag ='${bookingAgentFlag}';
    //alert(Flag)
    if(fileType=='Credit Card Authorization'){
        
   	 document.forms['uploadForm'].elements['myFile.isCportal'].checked=false;
   	 document.forms['uploadForm'].elements['myFile.isAccportal'].checked=false;
   	 document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked=false;
     document.forms['uploadForm'].elements['myFile.isServiceProvider'].checked=false;
   	if(Flag!='BA'){
   		document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked=false;
   	}
   	if(Flag!='NA'){
   		document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked=false;
   	}
    if(Flag!='OA'){
   		document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked=false;
   	}
    if(Flag!='SOA'){
   		document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked=false;
   	}
    if(Flag!='DA'){
   	 document.forms['uploadForm'].elements['myFile.isDestAgent'].checked=false;
   	}
   	if(Flag!='SDA'){
   		document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked=false;
   	}
    }
	var doctype=targetElement.options[targetElement.selectedIndex].value;
	var url="docCategoryByDocType.html?ajax=1&decorator=simple&popup=true&docType="+encodeURI(doctype);
	//alert(url);
	http125678.open("GET", url, true);
	http125678.onreadystatechange = handleHttpResponseFordocCategory;
	http125678.send(null);
}

function handleHttpResponseFordocCategory(){
	//alert("re...");
    if(http125678.readyState == 4){
		var results = http125678.responseText
      results = results.trim();
		//alert(results);
	document.forms['uploadForm'].elements['myFile.documentCategory'].value = results; 
							     	 
	}
}
function setDocCat1(targetElement){
	//alert("hi");
	var doctype=targetElement.options[targetElement.selectedIndex].value;
	var url="docCategoryByDocType.html?ajax=1&decorator=simple&popup=true&docType="+encodeURI(doctype);
	//alert(url);
	http125678.open("GET", url, true);
	http125678.onreadystatechange = handleHttpResponseFordocCategory1;
	http125678.send(null);
}
function handleHttpResponseFordocCategory1(){
	//alert("re...");
    if(http125678.readyState == 4){
		var results = http125678.responseText
      results = results.trim();
		//alert(results);
	document.forms['uploadForm'].elements['documentCategory'].value = results; 
							     	 
	}
}
function setDescriptionAccAppl(targetElement){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 targetElement.checked=false;
		}else{
	var checkedValue=document.forms['uploadForm'].elements['myFile.isAccportal'].checked;
//	document.uploadApplet.setAccportal(checkedValue);
		}
}

function setDescriptionPartnerAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValuePP=false;
	checkedValuePP=document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked;
	//document.uploadApplet.setPartnerPortal(checkedValuePP);
		}
}
function setDescriptionCportalAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	  var checkedValueCp=false;
	  checkedValueCp=document.forms['uploadForm'].elements['myFile.isCportal'].checked;
	 // document.uploadApplet.setCportal(checkedValueCp);
		}
}

function setDescriptionServiceProviderAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueSp=false;
	checkedValueSp=document.forms['uploadForm'].elements['myFile.isServiceProvider'].checked;
	//document.uploadApplet.setServiceProvider(checkedValueSp);
		}
}
function setDescriptionDriverAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueSp=false;
	checkedValueSp=document.forms['uploadForm'].elements['myFile.isDriver'].checked;
	//document.uploadApplet.setServiceProvider(checkedValueSp);
		}
}

function setDescriptionInvoiceAttachAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueIA=false;
	checkedValueIA=document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked;
	//document.uploadApplet.setInvoiceAttachachment(checkedValueIA);
		}
}

function setDescriptionBookingAgentAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueBA=false;
	checkedValueBA=document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked;
	//document.uploadApplet.setBookingAgent(checkedValueBA);	
		}
}
function setDescriptionNetworkAgentAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	   var checkedValueNA=false;
	    checkedValueNA=document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked;
		//document.uploadApplet.setNetworkAgent(checkedValueNA);
		}
}
function setDescriptionOriginAgentAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueOA=false;
	    checkedValueOA=document.forms['uploadForm'].elements['myFile.isOriginAgent'].checked;
		//document.uploadApplet.setOriginAgent(checkedValueOA);
		}
	
}
function setDescriptionSubOriginAgentAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueSOA=false;
		checkedValueSOA=document.forms['uploadForm'].elements['myFile.isSubOriginAgent'].checked;		
		//document.uploadApplet.setSubOriginAgent(checkedValueSOA);
		}
		
}
function setDescriptionDestAgentAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueDA=false;
		checkedValueDA=document.forms['uploadForm'].elements['myFile.isDestAgent'].checked;
		//document.uploadApplet.setDestAgent(checkedValueDA);
		}
	
}
function setDescriptionSubDestAgentAppl(event){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	var checkedValueSDA=false;
		checkedValueSDA=document.forms['uploadForm'].elements['myFile.isSubDestAgent'].checked;
		//document.uploadApplet.setSubDestAgent(checkedValueSDA);
		}
	
}

function setMap(targetElement){
	var mapfolder = targetElement.value;
	//document.uploadApplet.setMapFolder(mapfolder);
}

function setForAlert(){
    if(document.forms['uploadForm'].elements['myFile.description'].value==""){
    	alert("Please choose a Document Type before uploading a file");
    	self.document.location.reload(true);
   }
}
function setForVenderCode(){
	 if(document.forms['uploadForm'].elements['venderInvoiceValue'].value=="true" && document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value==""){
	  document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].focus;
	  alert("Please Select Partner Code");
	  self.document.location.reload(true);
	 }
	}
function setForPageLoad(){
    	self.document.location.reload(true);
}

function onPageLoad(){
	//document.uploadApplet.setCustomerNumber(document.forms['uploadForm'].elements['myFile.customerNumber'].value);
	//document.uploadApplet.setFileId(document.forms['uploadForm'].elements['myFile.fileId'].value);
	///document.uploadApplet.setId(document.forms['uploadForm'].elements['fileId'].value);
	//document.uploadApplet.setMyFileFor(document.forms['uploadForm'].elements['fileNameFor'].value);
	
	<%
	String appUrl = request.getRequestURL().toString();
	appUrl = appUrl.substring(0, appUrl.indexOf("/", appUrl.indexOf("/", appUrl.indexOf("//")+2 ) + 1)+1) + "uploadFiles.html";
	if (appUrl.indexOf("localhost") >= 0) {
		appUrl = appUrl.substring(0, appUrl.indexOf("/", appUrl.indexOf("//")+2 ) + 1) + "uploadFiles.html";
	}
	%>
	<%-- document.uploadApplet.setServerUrl("<%=appUrl%>");	 --%>
	
}


function clearCheckbox1(){
	var chkbox = document.getElementById('checkbox1');
	if(document.forms['uploadForm'].elements['isDestAgent'].checked){
	}else{
	}
	if(chkbox.value){
		chkbox.checked=false;
		
	}else{
		chkbox.checked=true;	
		
	}
}

function checkAllGroupBox1(obj,target)
{
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 target.checked=false;
		}else{
var ss;
var kk;
kk= obj;
ss=document.uploadForm.checkbox1.checked;
if (ss == false){	kk ="no";}
var frm = document.uploadForm;
var el = frm.elements;
for(i=0;i<el.length;i++) {
	if((el[i].type == "checkbox") && ((el[i].id == "isBookingAgent") || (el[i].id == "isNetworkAgent") || (el[i].id == "isOriginAgent") || (el[i].id == "isSubOriginAgent") || (el[i].id == "isDestAgent") || (el[i].id == "isSubDestAgent"))) {
    if(kk == "yes"){
    el[i].checked = true;
    }else{
    el[i].checked = false;
    }
    if(kk == "yes"){
    	  //document.uploadApplet.setBookingAgent(true);
    	//  document.uploadApplet.setNetworkAgent(true);
    	 // document.uploadApplet.setOriginAgent(true);
    	//  document.uploadApplet.setSubOriginAgent(true);
    	//  document.uploadApplet.setDestAgent(true);
    	//  document.uploadApplet.setSubDestAgent(true);
    }
    }
  }
		}
}

function clearCheckbox2(){
	var chkbox = document.getElementById('checkbox2');
	if(document.forms['uploadForm'].elements['isDestAgent'].checked){
	}else{
		document.forms['uploadForm'].elements['flagBeforeCheckDA'].value="yes";
	}
	if(document.forms['uploadForm'].elements['isBookingAgent'].checked){
	}else{
		document.forms['uploadForm'].elements['flagBeforeCheck'].value="yes";
	}
	if(document.forms['uploadForm'].elements['isOriginAgent'].checked){
	}else{
		document.forms['uploadForm'].elements['flagBeforeCheckOA'].value="yes";
	}
	if(document.forms['uploadForm'].elements['isNetworkAgent'].checked){
	}else{
		document.forms['uploadForm'].elements['flagBeforeCheckNA'].value="yes";
	}
	if(document.forms['uploadForm'].elements['isSubOriginAgent'].checked){
	}else{
		document.forms['uploadForm'].elements['flagBeforeCheckSOA'].value="yes";
	}
	if(document.forms['uploadForm'].elements['isSubDestAgent'].checked){
	}else{
		document.forms['uploadForm'].elements['flagBeforeCheckSDA'].value="yes";
	}
	if(chkbox.value){
		chkbox.checked=false;
	}else{
		chkbox.checked=true;	
	}
}

function checkAllGroupBox2(obj,target){
	var fileType=document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 target.checked=false;
		}else{
var ss;
var kk;
kk= obj;
ss=document.uploadForm.checkbox2.checked;
if (ss == false){	kk ="no";}
var frm = document.uploadForm;
var el = frm.elements;
for(i=0;i<el.length;i++) {
	if((el[i].type == "checkbox") && ((el[i].name == "isBookingAgent") || (el[i].name == "isNetworkAgent") || (el[i].name == "isOriginAgent") || (el[i].name == "isSubOriginAgent") || (el[i].name == "isDestAgent") || (el[i].name == "isSubDestAgent"))) {
		 if(kk == "yes"){
			    if(document.forms['uploadForm'].elements['isBookingAgent'].checked==false){
			    document.forms['uploadForm'].elements['checkFlagBA'].value='BA';
			    }
			    if(document.forms['uploadForm'].elements['isNetworkAgent'].checked==false){
			    document.forms['uploadForm'].elements['checkFlagNA'].value='NA';
			    }
			    if(document.forms['uploadForm'].elements['isOriginAgent'].checked==false){
			    document.forms['uploadForm'].elements['checkFlagOA'].value='OA';
			    }
			    if(document.forms['uploadForm'].elements['isSubOriginAgent'].checked==false){
			    document.forms['uploadForm'].elements['checkFlagSOA'].value='SOA';
			    }
			    if(document.forms['uploadForm'].elements['isDestAgent'].checked==false){
			    document.forms['uploadForm'].elements['checkFlagDA'].value='DA';
			    }
			    if(document.forms['uploadForm'].elements['isSubDestAgent'].checked==false){
			    document.forms['uploadForm'].elements['checkFlagSDA'].value='SDA';
			    }
			    el[i].checked = true;
			}else{
			    el[i].checked = false;
			    document.forms['uploadForm'].elements['checkFlagBA'].value='';
			    document.forms['uploadForm'].elements['checkFlagNA'].value='';
			    document.forms['uploadForm'].elements['checkFlagOA'].value='';
			    document.forms['uploadForm'].elements['checkFlagSOA'].value='';
			    document.forms['uploadForm'].elements['checkFlagDA'].value='';
			    document.forms['uploadForm'].elements['checkFlagSDA'].value='';
		  }
	   }
	}
		}
}

function setFlagValue(){	
	   document.forms['uploadForm'].elements['hitFlag'].value = '3';
}

function activeStatusCheck1(){
	if((document.forms['uploadForm'].elements['bookingAgent'].value=='BA') && (document.forms['uploadForm'].elements['isBookingAgent'].checked==true)) {
		document.forms['uploadForm'].elements['isBookingAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['isBookingAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['networkAgent'].value=='NA') && (document.forms['uploadForm'].elements['isNetworkAgent'].checked==true)){
		document.forms['uploadForm'].elements['isNetworkAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['isNetworkAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['originAgent'].value=='OA') && (document.forms['uploadForm'].elements['isOriginAgent'].checked==true)){
		document.forms['uploadForm'].elements['isOriginAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['isOriginAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['subOriginAgent'].value=='SOA') && (document.forms['uploadForm'].elements['isSubOriginAgent'].checked==true)){
		document.forms['uploadForm'].elements['isSubOriginAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['isSubOriginAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['destAgent'].value=='DA') && (document.forms['uploadForm'].elements['isDestAgent'].checked==true)){
		document.forms['uploadForm'].elements['isDestAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['isDestAgent'].checked=false;
	}
	if((document.forms['uploadForm'].elements['subDestAgent'].value=='SDA') && (document.forms['uploadForm'].elements['isSubDestAgent'].checked==true)){
		document.forms['uploadForm'].elements['isSubDestAgent'].checked=true;
	}else{
		document.forms['uploadForm'].elements['isSubDestAgent'].checked=false;
	}
	   document.forms['uploadForm'].elements['isBookingAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isNetworkAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isOriginAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isSubOriginAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isDestAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isSubDestAgent'].disabled=false;
	  //alert(document.forms['uploadForm'].elements['accountLineVendorCode'].value)
	   //alert(document.forms['uploadForm'].elements['venderInvoiceValue'].value)
		if(document.forms['uploadForm'].elements['venderInvoiceValue'].value=="true" && document.forms['uploadForm'].elements['accountLineVendorCode'].value==""){
			document.forms['uploadForm'].elements['accountLineVendorCode'].focus;
			alert("Please Select Partner Code");
			return false;	
		}
}
function activeStatusCheck(){	
	   document.forms['uploadForm'].elements['isBookingAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isNetworkAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isOriginAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isSubOriginAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isDestAgent'].disabled=false;
	   document.forms['uploadForm'].elements['isSubDestAgent'].disabled=false;
	  
		if(document.forms['uploadForm'].elements['venderInvoiceValue'].value=="true" && document.forms['uploadForm'].elements['accountLineVendorCode'].value==""){
			document.forms['uploadForm'].elements['accountLineVendorCode'].focus;
			alert("Please Select Partner Code");
			return false;	
		}
}

function setCheckFlagBA(targetElement,type){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 targetElement.checked=false;
	}else{
	var status = targetElement.checked;
	if( status == true){
		document.forms['uploadForm'].elements['checkFlagBA'].value=type;
	   }
	 else{
		document.forms['uploadForm'].elements['checkFlagBA'].value='N'+type;   
      }
	}
}
function setCheckFlagNA(targetElement,type){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 targetElement.checked=false;
	}else{
	var status = targetElement.checked;
	if( status == true){
		document.forms['uploadForm'].elements['checkFlagNA'].value=type;
	   }
	 else{
		document.forms['uploadForm'].elements['checkFlagNA'].value='N'+type;   
      }
	}
}
function setCheckFlagOA(targetElement,type){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 targetElement.checked=false;
	}else{
	var status = targetElement.checked;
	if( status == true){
		document.forms['uploadForm'].elements['checkFlagOA'].value=type;
	   }
	 else{
		document.forms['uploadForm'].elements['checkFlagOA'].value='N'+type;   
      }
  }
}
function setCheckFlagSOA(targetElement,type){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 targetElement.checked=false;
	}else{
	var status = targetElement.checked;
	if( status == true){
		document.forms['uploadForm'].elements['checkFlagSOA'].value=type;
	   }
	 else{
		document.forms['uploadForm'].elements['checkFlagSOA'].value='N'+type;   
      }
	}
}
function setCheckFlagDA(targetElement,type){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 targetElement.checked=false;
	}else{
	var status = targetElement.checked;
	if( status == true){
		document.forms['uploadForm'].elements['checkFlagDA'].value=type;
	   }
	 else{
		document.forms['uploadForm'].elements['checkFlagDA'].value='N'+type;   
      }
	}
}
function setCheckFlagSDA(targetElement,type){
	var fileType = document.forms['uploadForm'].elements['fileType'].value;
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		 targetElement.checked=false;
	}else{
	var status = targetElement.checked;
	if( status == true){
		document.forms['uploadForm'].elements['checkFlagSDA'].value=type;
	   }
	 else{
		document.forms['uploadForm'].elements['checkFlagSDA'].value='N'+type;   
      }
	}
}

</script> 

</head>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
<c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>"/>
<s:hidden name="noteForTemp" value="<%=request.getParameter("noteFor") %>" />
<c:set var="noteForTemp" value="<%=request.getParameter("noteFor") %>"/>
<s:hidden name="docUpload" value="<%=request.getParameter("docUpload") %>" />
<c:set var="docUpload" value="<%=request.getParameter("docUpload") %>"/>
<s:hidden name="PPID" id="PPID" value="<%=request.getParameter("PPID") %>" />
<c:set var="PPID" value="<%=request.getParameter("PPID") %>"/>
<s:form action="uploadMyFile.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=${secure}&ppType=${ppType}&forQuotation=${forQuotation}&docUpload=${docUpload}&PPID=${PPID}" enctype="multipart/form-data" method="post" onsubmit="return submit_form()" validate="true" id="uploadForm" name="uploadForm">
<s:hidden name="customerFile.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="flagBeforeCheckSDA" />
<s:hidden name="flagBeforeCheckSOA" />
<s:hidden name="flagBeforeCheckNA" />
<s:hidden name="flagBeforeCheck" />
<s:hidden name="flagBeforeCheckDA" />
<s:hidden name="flagBeforeCheckOA" />
<s:hidden name="workTicket.ticket" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="myFile.fileId"/>
<s:hidden name="myFile.fileSize"/>
<s:hidden name="myFile.isSecure"/>
<s:hidden name="myFile.coverPageStripped" value="true"/>
<s:hidden name="myFile.customerNumber"/>
<s:hidden name="myFileFor" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="forQuotation" name="forQuotation" />
<s:hidden name="checkFlagBA" />
<s:hidden name="checkFlagNA" />
<s:hidden name="checkFlagOA" />
<s:hidden name="checkFlagSOA" />
<s:hidden name="checkFlagDA" />
<s:hidden name="checkFlagSDA" />
<s:hidden name="bookingAgent" />
<s:hidden name="networkAgent" />
<s:hidden name="originAgent" />
<s:hidden name="subOriginAgent" />
<s:hidden name="destAgent" />
<s:hidden name="subDestAgent" />
<s:hidden name="venderInvoiceValue" id="venderInvoiceValue" />
<s:hidden name="firstDescription"/>
<s:hidden name="secondDescription"/>
<s:hidden name="thirdDescription"/>
<s:hidden name="fourthDescription"/>
<s:hidden name="fifthDescription"/>
<s:hidden name="sixthDescription"/>
<s:hidden name="seventhDescription"/>
<s:hidden name="description1" />
<s:hidden name="isVenderFilecabitateValue" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="fid" value="${myFile.id}"/>
<s:hidden name="fid" value="${myFile.id}"/>

<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>

<c:set var="description" value="${myFile.description}"/>
<c:set var="documentCategory" value="${myFile.documentCategory}"/>

<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>"/>

<c:if test='${fileNameFor=="CF"}'> 
	<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
	<c:set var="noteID" value="${TempnotesId}" scope="session"/>
	<c:set var="custID" value="" scope="session"/>
	<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>" scope="session"/>
	<c:if test="${noteFor=='WorkTicket' || noteFor=='' }"> 
     <c:set var="noteFor" value="" /> 
     <c:set var="noteID" value="" /> 
    </c:if>
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>

<c:if test="${myFileFor!='CF'}"> 

<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
<c:set var="noteID" value="${TempnotesId}" scope="session"/>
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>" scope="session"/>
<c:if test="${noteFor=='WorkTicket' || noteFor==''}">
 <c:set var="noteFor" value=""  />
<c:set var="noteID" value="" scope="session" /> 
</c:if> 
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>

<s:hidden name="customerFile.sequenceNumber" />
<c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">
<div id="layer6" style="width:100%; ">
<div id="newmnav" style="float:left; ">
 <c:choose>
	<c:when test="${forQuotation!='QC'}">
            <ul>
              <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
				<s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
				<c:set var="relocationServicesKey" value="" />
				<c:set var="relocationServicesValue" value="" /> 
			    <c:forEach var="entry" items="${relocationServices}">
					<c:if test="${relocationServicesKey==''}">
					<c:if test="${entry.key==serviceOrder.serviceType}">
					<c:set var="relocationServicesKey" value="${entry.key}" />
					<c:set var="relocationServicesValue" value="${entry.value}" /> 
					</c:if>
					</c:if> 
               </c:forEach>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	            <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
	            	</c:if>
	            	 <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	            	 <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
	            	</c:if>
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose> 
				     </c:if>
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose> 
				     </c:if>
			     </sec-auth:authComponent>
			        <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	              </c:if>
	              </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         	 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         	 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         	 </sec-auth:authComponent>
	          	 </c:if>
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			     <c:if test="${serviceOrder.job !='RLO'}"> 
			     	<c:if test="${forwardingTabVal!='Y'}"> 
	   					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  				</c:if>
	  				<c:if test="${forwardingTabVal=='Y'}">
	  					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  				</c:if>
			     </c:if>
			     </c:if>	
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             <c:if test="${serviceOrder.job !='RLO'}"> 
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>
		             </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
	             <c:if test="${serviceOrder.job =='RLO'}">  
                  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                 </c:if>
                 <c:if test="${serviceOrder.job !='RLO'}"> 
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </c:if>
	             </c:if>	
	             </sec-auth:authComponent>
	             <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             </c:if>
	            </c:if>	
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	       </ul> </c:when>
	      <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		</c:otherwise></c:choose>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
</div>
</div>
</c:if>       
</c:if>
<c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:85%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
	<div id="newmnav">
		  <ul>
		 <c:if test="${customerFile.controlFlag=='A'}">
		    <li><a href="editOrderManagement.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		    </c:if> 
		    <c:if test="${customerFile.controlFlag!='A'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li> 
		    </c:if>
		    <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		    </c:if> 
		    <c:if test="${salesPortalAccess=='false'}"> 
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		    </c:if>
		    </c:if> 
          </ul>
		</div>
	 </c:when>
	 <c:otherwise>
	  <div id="newmnav">
       <ul>
		    <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 		  	
	  </ul>
		</div></c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;!padding-bottom:18px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top: -14px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:770px">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>  

<div id="Layer1" style="width: 100%">
	<div id="Layer4" style="width:700px;">
		<div id="newmnav">
			<ul>			
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Upload<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				<c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">
					<c:if test="${fileCabinetView ne 'Document Centre' }">
						<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document List</span></a></li>
						<li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Related Docs</span></a></li>
						<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Waste Basket</span></a></li>
						<sec-auth:authComponent componentId="module.tab.myfile.securedocTab">
						<li><a href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=true&forQuotation=${forQuotation}"><span>Secure List</span></a></li>
						</sec-auth:authComponent>
					</c:if>
					<c:if test="${fileCabinetView == 'Document Centre' }">
						<li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre</span></a></li>
					</c:if>
				</c:if>
				<c:if test="${myFileFor=='Truck'}">	
				<c:if test="${fileCabinetView ne 'Document Centre' }">
					<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&ppType=${ppType}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&ppType=${ppType}"><span>Waste Basket</span></a></li>
				</c:if>
				<c:if test="${fileCabinetView == 'Document Centre' }">
						<li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre</span></a></li>
				</c:if>
				<li><a href="editTruck.html?id=${fileId}" ><span>Truck Details</span></a></li>
				</c:if>
				<c:if test="${myFileFor=='PO'}">
				<c:if test="${fileCabinetView ne 'Document Centre' }">
					<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&ppType=${ppType}&PPID=${PPID}"><span>Document List</span></a></li>
					<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&ppType=${ppType}&PPID=${PPID}"><span>Waste Basket</span></a></li>
				</c:if>
				<c:if test="${fileCabinetView == 'Document Centre' }">
					<li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre</span></a></li>
				</c:if>
				<c:if test="${!param.popup  && ppType=='AG'}"> 
				<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Agent Detail</span></a></li>
				</c:if>
				<c:if test="${!param.popup && ppType=='AC'}"> 
				<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
				</c:if>
				<c:if test="${!param.popup && ppType=='PP'}"> 
				<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
				</c:if>
				<c:if test="${!param.popup && ppType=='CR'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
					</c:if>
					<c:if test="${!param.popup && ppType=='VN'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
					</c:if>
					<c:if test="${!param.popup && ppType=='OO'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
					</c:if>
					
				</c:if>
				<c:if test="${not empty myFile.id}">
				<li><a onclick="window.open('auditList.html?id=${myFile.id}&tableName=myfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				</c:if>
			</ul>
		</div>	
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
		<tr>
		<td class="CFprobg_left"></td>
		<td NOWRAP class="CFprobg_center">&nbsp;Upload A Document </td>
		<td class="CFprobg_right"></td>
		</tr>
		</table>
	<table class="notesDetailTableNew" cellspacing="0" cellpadding="0" border="0" width="">
		<tbody>
		<tr>
			<td colspan="5" width="720px">			
			</td>
		</tr>
                  
		<c:if test="${empty myFile.id}">
			<!--<tr> 
				<td colspan=""></td>
				<td rowspan="7" width="2px" align="center" class="vertlinedata_vert" height="100%">&nbsp; </td>
				<td></td>
			</tr>
			--><tr>
				<td colspan="" valign="top">
					<table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;">
						<tr>
							<td align="right" class="listwhitetextFC" style="width:90px;" height="30px">Document Type</td>
							<td style="width:100px;"><s:select name="fileType" cssStyle="width:230px;" cssClass="list-menuBlackFC" list="%{docsList}" headerKey="" headerValue="" onchange="setDocCat(this);setDescription(this),documentAccessControl(this);"/></td>
							<td align="left" class="listwhitetextFC" width="100px">&nbsp;Map&nbsp;&nbsp;<s:select name="mapFolder" cssClass="list-menuBlackFC" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px" onchange="setMap(this);" /></td>
						   <c:if test="${not empty serviceOrder.id}">
						   <td colspan="2" valign="top">
						    <div id="venderInvoicesId" style="width:0px;height:0px;display:none" >
						    <table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;width:280px;">
							<tr>
						       <td align="left" class="listwhitetextFC" width="162px">&nbsp;&nbsp;Partner Code<font color="red" size="2">*</font>&nbsp;<s:textfield cssClass="input-textBlackUpperFC" name="myFile.accountLineVendorCode"   cssStyle="width:80px" size="10" readonly="true" /></td>
						       <td align="left"width=""><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" id="openpopup2.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
						    </tr>
						    </table>
						    </div>
						    </td>
						    </c:if>
						     <c:if test="${empty serviceOrder.id}">
						     <s:hidden name="myFile.accountLineVendorCode" />
						     </c:if>
						</tr>
						<tr>
						<td align="right" class="listwhitetextFC" style="width:110px;" height="30px">Document&nbsp;Category<!-- <font color="red" size="2">*</font> --></td>
							<td><s:textfield cssClass="input-textBlackUpperFC" name="myFile.documentCategory"   cssStyle="width:229px" size="25" readonly="true"/></td>
							<td class="listwhitetextFC" width="120" align="left"><s:checkbox key="myFile.invoiceAttachment" onchange="setDescriptionInvoiceAttachAppl()"  fieldValue="true" /> Invoice&nbsp;Attachment</td>
						     <c:if test="${not empty serviceOrder.id}">
						      <td colspan="2" valign="top">
						    <div id="paymentStatusId" style="width:0px;height:0px;display:none" >
						     <table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;width:180px;">
							<tr>
						    <td align="left" class="listwhitetextFC" width="100px">&nbsp;Payment Status&nbsp;<s:textfield cssClass="input-textBlackUpperFC" name="myFile.accountLinePayingStatus" cssStyle="width:80px" size="10" readonly="true" /></td>
						    </tr>
						    </table>
						    </div>
						    </td>
						    </c:if>
						     <c:if test="${empty serviceOrder.id}">
						     <s:hidden name="myFile.accountLinePayingStatus" />
						     </c:if>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetextFC" height="80px" style="width:90px">Description</td>
											
							<td colspan="3" valign="top"><s:textarea cssClass="textareaFC"  name="myFile.description"  rows="5" cssStyle="width:331px;" />
								<c:if test="${empty myFile.id}">
									<tr valign="top">
										<td align="left" valign="top" colspan="3" >
											<table border="0">
												<tr>
													<td align="left" valign="top" colspan="2" width="">
														<table cellpadding="1" cellspacing="0" border="0" class="detailTabLabel" width="443">
															<tr>
																<td class="listwhitetextFC" style="width:105px" align="right"><fmt:message key='myFile.file' /></td>
																<td><s:file name="file" label="%{getText('uploadForm.file')}" required="true" size="25" /></td>																	
																<td align="right" style="padding-top:6px;"><s:submit key="button.upload" name="myFile.upload" method="save" cssClass="uploadBtFC"  onclick="return validateFields();" /></td>
															</tr>
														</table>						
													</td>
												</tr>					
											</table>
										</td>
									</tr>
								</c:if>
							</td>
						</tr>	
					</table>
				</td>
				<td valign="top">
					<table border="0" style="margin: 0px; padding: 0px;">
						<tr>
							<td></td>
							<td class="listwhitetext" width="100" align="left">	
									<table class="detailTabLabelFC-PO" border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                    <td colspan="5">
									 <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
                                    <tr>
                                    <td class="CFCtrl_left"></td>
                                    <td NOWRAP class="CFCtrl_right">&nbsp;&nbsp;Portal Access Control </td>
                                    </tr>
                                    </table>                                  
                                    </td>
                                    </tr>
                                    <tr><td height="10"></td></tr>
										<tr>
											<td width="30"></td><configByCorp:fieldVisibility componentId="component.standard.cPortalActivation"><td class="listwhitetextFC" width="100px" align="left"><s:checkbox key="myFile.isCportal" value="false" onclick="setDescriptionCportalAppl(this)"/> &nbsp;Customer</td></configByCorp:fieldVisibility>
										
										<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">	<td class="listwhitetextFC" width="" align="left"><s:checkbox key="myFile.isAccportal" onchange="setDescriptionAccAppl(this)" fieldValue="true" /> &nbsp;Account</td></configByCorp:fieldVisibility>
										</tr>
										 <tr><td height="5"></td></tr>
										<tr>	
											<td width=""></td><td class="listwhitetextFC" width="" align="left"><s:checkbox key="myFile.isPartnerPortal" onchange="setDescriptionPartnerAppl(this)"  fieldValue="true" /> &nbsp;Partner</td>
											
											<td class="listwhitetextFC" width="" align="left"><s:checkbox key="myFile.isServiceProvider" onchange="setDescriptionServiceProviderAppl(this)"  fieldValue="true" /> Service&nbsp;Provider</td>
										</tr>
										<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
										<tr>
											<td width="30"></td><td class="listwhitetextFC" width="100px" align="left"><s:checkbox key="myFile.isDriver" value="false" onclick="setDescriptionDriverAppl(this)"/> &nbsp;Driver</td>
										</tr>
										</configByCorp:fieldVisibility>
										
										<tr><td height="10"></td></tr>
									</table>								
								<c:if test="${fileNameFor=='SO' || fileNameFor=='CF'}">								
								<table class="detailTabLabelFC-NW" border="0" cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px;margin-bottom:3px;">
                                    <tr>
                                    <td colspan="5">
									 <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
                                    <tr>
                                    <td class="CFCtrl_left_new"></td>
                                    <td NOWRAP class="CFCtrl_right_new">&nbsp;&nbsp;Network Access Control </td>
                                    </tr>
                                    </table>                                  
                                    </td>
                                    </tr>
                                    <tr><td height="7"></td></tr>	
                                    	<tr>
											<td align="left" class="listwhitetextFC" width="130px" style="padding-left:30px;"><s:checkbox id="checkbox1" name="checkbox1" onclick="checkAllGroupBox1('yes',this);"/><b>Check/Uncheck&nbsp;All</b></td>               
										</tr>
										<tr>
										<c:if test="${bookingAgentFlag=='BA'}">	
											</td><td align="left" class="listwhitetextFC" width="120px" style="padding-left: 30px;"><s:checkbox id="isBookingAgent" key="myFile.isBookingAgent" fieldValue="true" disabled="true" onclick="clearCheckbox1();setDescriptionBookingAgentAppl(this);"/>&nbsp;Booking Agent</td>
										</c:if>
										<c:if test="${bookingAgentFlag==''}">	
											</td><td align="left" class="listwhitetextFC" width="120px" style="padding-left: 30px;"><s:checkbox id="isBookingAgent" key="myFile.isBookingAgent" fieldValue="true" onclick="clearCheckbox1();setDescriptionBookingAgentAppl(this);"/>&nbsp;Booking Agent</td>
										</c:if>
										<c:if test="${networkAgentFlag=='NA'}">
											<td align="left" class="listwhitetextFC" width=""><s:checkbox id="isNetworkAgent" key="myFile.isNetworkAgent" fieldValue="true" disabled="true" onclick="clearCheckbox1();setDescriptionNetworkAgentAppl(this);"/>&nbsp;Network Agent</td>
										</c:if>
										<c:if test="${networkAgentFlag==''}">
											<td align="left" class="listwhitetextFC" width=""><s:checkbox id="isNetworkAgent" key="myFile.isNetworkAgent" fieldValue="true" onclick="clearCheckbox1();setDescriptionNetworkAgentAppl(this);"/>&nbsp;Network Agent</td>
										</c:if>
										</tr>
										 <tr><td height="5"></td></tr>
										<tr>
										<c:if test="${originAgentFlag=='OA'}">
										<td class="listwhitetextFC" align="left" style="padding-left: 30px;" width=""><s:checkbox id="isOriginAgent" key="myFile.isOriginAgent" fieldValue="true" disabled="true" onclick="clearCheckbox1();setDescriptionOriginAgentAppl(this);"/>&nbsp;Origin Agent</td>	
										</c:if>
										<c:if test="${originAgentFlag==''}">
										<td class="listwhitetextFC" align="left" style="padding-left: 30px;" width=""><s:checkbox id="isOriginAgent" key="myFile.isOriginAgent" fieldValue="true" onclick="clearCheckbox1();setDescriptionOriginAgentAppl(this);"/>&nbsp;Origin Agent</td>
										</c:if>
										<c:if test="${subOriginAgentFlag=='SOA'}">
										<td align="left" class="listwhitetextFC" width=""><s:checkbox id="isSubOriginAgent" key="myFile.isSubOriginAgent" disabled="true" fieldValue="true" onclick="clearCheckbox1();setDescriptionSubOriginAgentAppl(this);"/>&nbsp;Sub Origin Agent</td>
										</c:if>
										<c:if test="${subOriginAgentFlag==''}">
										<td align="left" class="listwhitetextFC" width=""><s:checkbox id="isSubOriginAgent" key="myFile.isSubOriginAgent" fieldValue="true" onclick="clearCheckbox1();setDescriptionSubOriginAgentAppl(this);"/>&nbsp;Sub Origin Agent</td>
										</c:if>
										</tr>
										 <tr><td height="5"></td></tr>
										<tr>
										<c:if test="${destAgentFlag=='DA'}">
										<td class="listwhitetextFC" align="left" style="padding-left: 30px;" width=""><s:checkbox id="isDestAgent" key="myFile.isDestAgent" fieldValue="true" disabled="true" onclick="clearCheckbox1();setDescriptionDestAgentAppl(this);"/>&nbsp;Destination Agent</td>
										</c:if>
										<c:if test="${destAgentFlag==''}">
										<td class="listwhitetextFC" align="left" style="padding-left: 30px;" width=""><s:checkbox id="isDestAgent" key="myFile.isDestAgent" fieldValue="true" onclick="clearCheckbox1();setDescriptionDestAgentAppl(this);"/>&nbsp;Destination Agent</td>
										</c:if>
										<c:if test="${subDestAgentFlag=='SDA'}">
										<td align="left" class="listwhitetextFC" width=""><s:checkbox id="isSubDestAgent" key="myFile.isSubDestAgent" fieldValue="true" disabled="true" onclick="clearCheckbox1();setDescriptionSubDestAgentAppl(this);"/>&nbsp;Sub Destination Agent</td>	
										</c:if>
										<c:if test="${subDestAgentFlag==''}">
										<td align="left" class="listwhitetextFC" width=""><s:checkbox id="isSubDestAgent" key="myFile.isSubDestAgent" fieldValue="true" onclick="clearCheckbox1();setDescriptionSubDestAgentAppl(this);"/>&nbsp;Sub Destination Agent</td>
										</c:if>
										</tr>
										<tr><td height="10"></td></tr>			
									</table>								
								</c:if>
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
			
			<tr>
						<td colspan="2">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
						<tr>
						<td class="CFprobg_left"></td>
						<td NOWRAP class="CFprobg_center">&nbsp;Drag and Drop File Cabinet </td>
						<td class="CFprobg_right"></td>
						</tr>
						</table>
						</td>
						</tr>
						<tr>
							<td valign="top" colspan="2" align="center" style="margin: 0px; padding: 0px;" colspan="0" width="">
							<div style="float:left;margin-left:14%;">
								<table width="100%" border="0" class="detailTabLabel" style="background-image:url(images/UploadBg-FC.png); background-repeat:no-repeat; height:150px; width:440px;margin-top:5px;">
									<tr>
										<td align="right">
<!-- 										<div >
	<APPLET CODE="com.redsky.filedragandupload.DragAndDropApplet" ARCHIVE="outlookdd.jar,outldd.jar" WIDTH="800" HEIGHT="500"></APPLET>
</div> -->
										<!-- <applet name="uploadApplet" id="uploadApplet"
											code="com.redsky.filedragandupload.DragAndDropApplet"
											codebase="archive"  archive="outldd.jar"
											mayscript="true" scriptable="true" width="115" height="116"  style="margin-right:5px; margin-top:7px;"/>
			
											this parameter must be set  
											<param name="uploadPath" value="./imageview.htm">
			
											 these parameters are optional. Each fuction you define will be called 
											<param name="funcNameHandleError" value="handleError">
											<param name="funcNameHandleStartUpload" value="handleStartUpload">
											<param name="funcNameHandleCurrentUpload" value="handleCurrentUpload">
											<param name="funcNameHandleEndUpload" value="handleEndUpload">
											<param name="funcNameHandleStringMimeType" value="handleStringMimeType">
											<param name="funcNameHandleUnknownMimeType" value="handleUnkownMimeType">							
											</applet>  -->
					
					
					<noscript style="color: maroon">
      <h2>JavaScript is disabled in your browser. How do you expect FileDrop to work?</h2>
    </noscript>

          
    <fieldset id="zone" style="border:none;">
       <!-- <p style="z-index: 10; position: relative">
   <input type="checkbox" id="multiple">
    <label for="multiple">Allow multiple selection</label>
      </p>  -->
    </fieldset>
					
										</td>
									</tr>
								</table>
							</div>
							<div style="float:left; padding:50px 0px 0px 10px;">
							<table style="margin: 0px; padding: 0px;">
							<tr>
							<td colspan=""></td>
							<td valign="bottom"><font style="color:#FF0C0C;"><b>Note:</b><i> Max file size allowed is 10MB</i></font></td></tr>
							<tr><td colspan=""></td><td valign="bottom"><font style="color:#FF0C0C;"><b>Note:</b><i> Press Ctrl when Drag outlook mail </i></font></td></tr>
							</table>
							</div>
							</td>
						</tr>
			
		</c:if>
		<c:if test="${not empty myFile.id}">
			<!--<tr>
				<td></td>
				<td rowspan="7" width="5px" align="center" valign="top" class="vertlinedata_vert">&nbsp;</td>
				<td></td>
			</tr>
			--><tr>
				<td width="" valign="top">
					<table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;">
						<tr>
							<td align="right" class="listwhitetextFC" style="width:84px; !width:84px" height="30px">Document Type</td>
							<td width="100px"><s:select name="fileType" cssClass="list-menuBlackFC" list="%{docsList}" headerKey="" headerValue="" onchange="documentAccessControlAjax();setDocCat1(this);setDescriptionDuplicate(this),documentMap(this);"  cssStyle="width:245px"/></td>
							<td align="right" class="listwhitetextFC" width="100px">Map&nbsp;&nbsp;<s:select name="mapFolder" cssClass="list-menuBlackFC" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px"/></td>
						      <c:if test="${not empty serviceOrder.id}">
						    <td colspan="2" valign="top">
						     <div id="venderInvoicesId" style="width:0px;height:0px;display:none" >
						     <table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;width:190px;">
							  <tr>	
							  <c:if test="${accountLinePayingStatus!='Approved'}">				    
						       <td align="left" class="listwhitetextFC" width="190px">&nbsp;&nbsp;Partner Code<font color="red" size="2">*</font>&nbsp;<s:textfield cssClass="input-textBlackUpperFC" name="accountLineVendorCode" readonly="true"  cssStyle="width:80px" size="10"  /></td>
						      <td align="left"width=""><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenEdit()" id="openpopup2.img" src="<c:url value='/images/open-popup.gif'/>" /></td>						  
						      </c:if>
						      <c:if test="${accountLinePayingStatus=='Approved'}">	
						      <td align="left" class="listwhitetextFC" width="190px">&nbsp;&nbsp;Partner Code<font color="red" size="2">*</font>&nbsp;<s:textfield cssClass="input-textBlackUpperFC" name="accountLineVendorCode"   cssStyle="width:80px" size="10" readonly="true" /></td>
						      <td align="left"width=""></td>
						      </c:if>
						     </tr>
						   </table>
						    </div>
						    </td>
						    </c:if>
						</tr>
						<tr>
						<td align="right" class="listwhitetextFC" style="width:110px;" height="30px">Document&nbsp;Category<!-- <font color="red" size="2">*</font> --></td>
							<td><s:textfield cssClass="input-textBlackUpperFC" name="documentCategory" value = "${documentCategory}" cssStyle="width:242px" size="25" readonly="true"/></td>
							<c:set var="ischeckedIA" value="false"/>
							<c:if test="${myFile.invoiceAttachment}">
								<c:set var="ischeckedIA" value="true"/>
							</c:if>
							<td class="listwhitetextFC" align="left" width="120"><s:checkbox id="invoiceAttachment" name="invoiceAttachment" key="myFile.invoiceAttachment" value="${ischeckedIA}" fieldValue="true"/>Invoice&nbsp;Attachment</td>	
					        <c:if test="${not empty serviceOrder.id}">	
					        <td colspan="2" valign="top">				        
					           <div id="paymentStatusId" style="width:0px;height:0px;display:none" >
					           <table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;width:190px;">
							   <tr>
					           <td align="left" class="listwhitetextFC" width="190px">&nbsp;Payment Status&nbsp;&nbsp;<s:textfield cssClass="input-textBlackUpperFC" name="accountLinePayingStatus"   cssStyle="width:80px" size="10" readonly="true" /></td>
					           </tr>
					           </table>
					           </div>
					           </td>						    
						    </c:if>
						</tr>
						<tr>
						<tr>
							<td align="right" valign="top" class="listwhitetextFC" height="70px">Description</td>
							<td colspan="2" valign="top" align="left"><s:textarea cssClass="textareaFC"  name="description" value="${description}"  rows="3" cols="47" /></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetextFC" height="40px">File Name</td>
							<td colspan="2" valign="top" align="left"><s:textfield name="myFile.fileFileName" cssClass="input-textBlackUpperFC" cssStyle="width:347px" readonly="true" /></td>
						</tr>
					</table>
				</td>
				<td width="20"></td>
				<td valign="top" align="left" width="">
					<table border="0" style="margin: 0px;">
						<tr>
							<td>
								<table class="detailTabLabelFC-PO" border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                    <td colspan="5">
									 <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
                                    <tr>
                                    <td class="CFCtrl_left"></td>
                                    <td NOWRAP class="CFCtrl_right">&nbsp;&nbsp;Portal Access Control </td>
                                    </tr>
                                    </table>                                  
                                    </td>
                                    </tr>
                                    <tr><td height="10"></td></tr>									
										<tr>
											<c:set var="ischecked" value="false"/>
											<c:if test="${myFile.isCportal}">
												<c:set var="ischecked" value="true"/>
											</c:if>	
											<td width="7"></td><configByCorp:fieldVisibility componentId="component.standard.cPortalActivation"><td align="left" class="listwhitetextFC" width="80px"><s:checkbox id="isCportal" name="isCportal" key="myFile.isCportal" onclick="checkSecureDoc(this);" value="${ischecked}" fieldValue="true"/>&nbsp;Customer</td></configByCorp:fieldVisibility>
										
											<c:set var="ischeckedACC" value="false"/>
											<c:if test="${myFile.isAccportal}">
												<c:set var="ischeckedACC" value="true"/>
											</c:if>
										<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation"><td class="listwhitetextFC" align="left" width="70px"><s:checkbox id="isAccportal" name="isAccportal" key="myFile.isAccportal" onclick="checkSecureDoc(this);" value="${ischeckedACC}" fieldValue="true"/>&nbsp;Account</td></configByCorp:fieldVisibility>
										</tr>
										 <tr><td height="5"></td></tr>		
										<tr>
											<c:set var="ischeckedPP" value="false"/>
											<c:if test="${myFile.isPartnerPortal}">
												<c:set var="ischeckedPP" value="true"/>
											</c:if>
											<td width=""></td><td class="listwhitetextFC" align="left" width="70px"><s:checkbox id="isPartnerPortal" name="isPartnerPortal" key="myFile.isPartnerPortal" onclick="checkSecureDoc(this);" value="${ischeckedPP}" fieldValue="true"/>&nbsp;Partner</td>	
										
											<c:set var="ischeckedSP" value="false"/>
											<c:if test="${myFile.isServiceProvider}">
												<c:set var="ischeckedSP" value="true"/>
											</c:if>
											<td class="listwhitetextFC" align="left" width="70px"><s:checkbox id="isServiceProvider" name="isServiceProvider" key="myFile.isServiceProvider" onclick="checkSecureDoc(this);" value="${ischeckedSP}" fieldValue="true"/>Service&nbsp;Provider</td>	
										</tr>
											<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
										<tr>
											<c:set var="ischeckedDr" value="false"/>
											<c:if test="${myFile.isDriver}">
												<c:set var="ischeckedDr" value="true"/>
											</c:if>	
											<td width="7"></td><td align="left" class="listwhitetextFC" width="80px"><s:checkbox id="isDriver" name="isDriver" key="myFile.isDriver" onclick="checkSecureDoc(this);" value="${ischeckedDr}" fieldValue="true"/>&nbsp;Driver</td>
										
										</tr>
										</configByCorp:fieldVisibility>
										<tr><td height="10"></td></tr>		
									</table>
								
								<c:if test="${fileNameFor=='SO' || fileNameFor=='CF'}">
								
								<table class="detailTabLabelFC-NW" border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                    <td colspan="5">
									 <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
                                    <tr>
                                    <td class="CFCtrl_left"></td>
                                    <td NOWRAP class="CFCtrl_right">&nbsp;&nbsp;Network Access Control </td>
                                    </tr>
                                    </table>                                  
                                    </td>
                                    </tr>
                                    <tr><td height="7"></td></tr>					
								
										<tr>
											<c:set var="ischeckedMain" value="false"/>
											<c:if test="${myFile.isBookingAgent && myFile.isNetworkAgent && myFile.isOriginAgent && myFile.isSubOriginAgent && myFile.isDestAgent && myFile.isSubDestAgent}">
											<c:set var="ischeckedMain" value="true"/>
											</c:if>	
											<td height="32" style="!width:10px;"></td><td align="left" class="listwhitetextFC" width="220px" colspan="4"><s:checkbox id="checkbox2" name="checkbox2" value="${ischeckedMain}" onclick="checkAllGroupBox2('yes',this);"/><b>Check/Uncheck All</b></td>                
										</tr>
										<tr><td height="1"></td></tr>		
										<tr>
											<c:set var="ischeckedBA" value="false"/>
											<c:if test="${myFile.isBookingAgent}">
												<c:set var="ischeckedBA" value="true"/>
											</c:if>	
											<c:if test="${bookingAgentFlag=='BA'}">
											<td width="32" style="!width:10px;"></td><td align="left" class="listwhitetextFC" width="120px"><s:checkbox name="isBookingAgent" key="myFile.isBookingAgent" value="true" fieldValue="true" disabled="true" />&nbsp;Booking Agent</td>
											</c:if>
											<c:if test="${bookingAgentFlag==''}">
											<td width="32" style="!width:10px;"></td><td align="left" class="listwhitetextFC" width="120px"><s:checkbox name="isBookingAgent" key="myFile.isBookingAgent" value="${ischeckedBA}" fieldValue="true" onclick="clearCheckbox2();setCheckFlagBA(this,'BA');"/>&nbsp;Booking Agent</td>
											</c:if>
											<c:set var="ischeckedNA" value="false"/>
											<c:if test="${myFile.isNetworkAgent}">
												<c:set var="ischeckedNA" value="true"/>
											</c:if>	
											<c:if test="${networkAgentFlag=='NA'}">
											<td width="5"></td><td align="left" class="listwhitetextFC" width=""><s:checkbox name="isNetworkAgent" key="myFile.isNetworkAgent" value="true" fieldValue="true" disabled="true" />&nbsp;Network Agent</td>
											</c:if>
											<c:if test="${networkAgentFlag==''}">
											<td width="5"></td><td align="left" class="listwhitetextFC" width=""><s:checkbox name="isNetworkAgent" key="myFile.isNetworkAgent" value="${ischeckedNA}" fieldValue="true" onclick="clearCheckbox2();setCheckFlagNA(this,'NA');"/>&nbsp;Network Agent</td>
											</c:if>
										</tr>
										<tr><td height="5"></td></tr>		
										<tr>
											<c:set var="ischeckedOA" value="false"/>
											<c:if test="${myFile.isOriginAgent}">
												<c:set var="ischeckedOA" value="true"/>
											</c:if>
											<c:if test="${originAgentFlag=='OA'}">
											<td width="32" style="!width:10px;"></td><td class="listwhitetextFC" align="left" width=""><s:checkbox name="isOriginAgent" key="myFile.isOriginAgent" value="true" fieldValue="true" disabled="true" />&nbsp;Origin Agent</td>
											</c:if>
											<c:if test="${originAgentFlag==''}">
											<td width="32" style="!width:10px;"></td><td class="listwhitetextFC" align="left" width=""><s:checkbox name="isOriginAgent" key="myFile.isOriginAgent" value="${ischeckedOA}" fieldValue="true" onclick="clearCheckbox2();setCheckFlagOA(this,'OA');"/>&nbsp;Origin Agent</td>
											</c:if>		
											<c:set var="ischeckedSOA" value="false"/>
											<c:if test="${myFile.isSubOriginAgent}">
												<c:set var="ischeckedSOA" value="true"/>
											</c:if>	
											<c:if test="${subOriginAgentFlag=='SOA'}">
											<td width="5"></td><td align="left" class="listwhitetextFC" width=""><s:checkbox name="isSubOriginAgent" key="myFile.isSubOriginAgent" value="true" fieldValue="true" disabled="true"/>&nbsp;Sub Origin Agent</td>
											</c:if>
											<c:if test="${subOriginAgentFlag==''}">
											<td width="5"></td><td align="left" class="listwhitetextFC" width=""><s:checkbox name="isSubOriginAgent" key="myFile.isSubOriginAgent" value="${ischeckedSOA}" fieldValue="true" onclick="clearCheckbox2();setCheckFlagSOA(this,'SOA');"/>&nbsp;Sub Origin Agent</td>
											</c:if>
											
										</tr>
										<tr><td height="5"></td></tr>		
										<tr>
											<c:set var="ischeckedDA" value="false"/>
											<c:if test="${myFile.isDestAgent}">
												<c:set var="ischeckedDA" value="true"/>
											</c:if>
											<c:if test="${destAgentFlag=='DA'}">
											<td width="32" style="!width:10px;"></td><td class="listwhitetextFC" align="left" width=""><s:checkbox name="isDestAgent" key="myFile.isDestAgent" value="true" fieldValue="true" disabled="true"/>&nbsp;Destination Agent</td>
											</c:if>
											<c:if test="${destAgentFlag==''}">
											<td width="32" style="!width:10px;"></td><td class="listwhitetextFC" align="left" width=""><s:checkbox name="isDestAgent" key="myFile.isDestAgent" value="${ischeckedDA}" fieldValue="true" onclick="clearCheckbox2();setCheckFlagDA(this,'DA');"/>&nbsp;Destination Agent</td>
											</c:if>
											
											<c:set var="ischeckedSDA" value="false"/>
											<c:if test="${myFile.isSubDestAgent}">
												<c:set var="ischeckedSDA" value="true"/>
											</c:if>	
											<c:if test="${subDestAgentFlag=='SDA'}">
											<td width="5"></td><td align="left" class="listwhitetextFC" width=""><s:checkbox name="isSubDestAgent" key="myFile.isSubDestAgent" value="true" fieldValue="true" disabled="true" />&nbsp;Sub Destination Agent</td>
											</c:if>
											<c:if test="${subDestAgentFlag==''}">
											<td width="5"></td><td align="left" class="listwhitetextFC" width=""><s:checkbox name="isSubDestAgent" key="myFile.isSubDestAgent" value="${ischeckedSDA}" fieldValue="true" onclick="clearCheckbox2();setCheckFlagSDA(this,'SDA');"/>&nbsp;Sub Destination Agent</td>
											</c:if>
												
										</tr>
										<tr><td height="10"></td></tr>		
									</table>								
								</c:if>
							</td>
						</tr>
					</table>
				</td>	
			</c:if>
		</tbody>
	</table>

	<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.createdOn' /></b></td>
				<td style="width:130px"><fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${myFile.createdOn}" pattern="${displayDateTimeEditFormat}" />
					<s:hidden name="myFile.createdOn" value="${customerFileCreatedOnFormattedValue}" /> 
					<fmt:formatDate value="${myFile.createdOn}" pattern="${displayDateTimeFormat}" />
				</td>
				<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message key='billing.createdBy' /></b></td>
				<c:if test="${not empty myFile.id}">
					<s:hidden name="myFile.createdBy" />
					<td style="width:65px"><s:label name="createdBy" value="%{myFile.createdBy}" /></td>
				</c:if>
				<c:if test="${empty myFile.id}">
					<s:hidden name="myFile.createdBy" value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>				
			
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedOn' /></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${myFile.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="myFile.updatedOn" value="${customerFileupdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate value="${myFile.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedBy' /></b></td>
				<c:if test="${not empty myFile.id}">
					<s:hidden name="myFile.updatedBy"/>
					<td style="width:85px"><s:label name="updatedBy" value="%{myFile.updatedBy}"/></td>
				</c:if>
				<c:if test="${empty myFile.id}">
					<s:hidden name="myFile.updatedBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
			</tr>
			<!-- <tr>
				<td colspan="20">
					<a href="#" onclick="exportExcel()"><b>Excel</b></a>
				</td>
			</tr> -->
		</tbody>
	</table>
	<c:if test="${empty myFile.id}">
		<table>
			<tr>
				<!--  td style="padding-left:">
		 			<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:70px; font-size:15;" value="Close" class="button" onclick="window.close()" />
		 		</td>	-->
				<td align="right">
					<c:if test="${fileCabinetView ne 'Document Centre' }">
						<input class="cssbutton" type="button" style="margin-right: 5px;height: 25px;width:85px; font-size:15;" value="Go To List" onclick="location.href='myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&forQuotation=${forQuotation}'" />
					</c:if>
					<c:if test="${fileCabinetView == 'Document Centre' }">
						<input class="cssbutton" type="button" style="margin-right: 5px;height: 25px;width:140px; font-size:15;" value="Go To Document Centre" onclick="location.href='myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&forQuotation=${forQuotation}'" />
					</c:if>
				</td>
			</tr>				
		</table>
	</c:if>
			
	<c:if test="${not empty myFile.id}">
		<tr>
			<td><s:submit id="saveButton"  method="save" key="button.save" cssClass="cssbutton" cssStyle="margin-right: 5px;height: 25px;width:60px; font-size: 15" onclick="return activeStatusCheck();"/></td>
			
			<td align="right">
				<c:if test="${fileCabinetView ne 'Document Centre' }">
					<input class="cssbutton" type="button" style="margin-right: 5px;height: 25px;width:90px; font-size: 15" value="Go To List" onclick="location.href='myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&forQuotation=${forQuotation}'" />
				</c:if>
				<c:if test="${fileCabinetView == 'Document Centre' }">
					<input class="cssbutton" type="button" style="margin-right: 5px;height: 25px;width:140px; font-size: 15" value="Go To Document Centre" onclick="location.href='myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&forQuotation=${forQuotation}'" />
				</c:if>
			</td>
		</tr>	
	</c:if>
</div>
<s:hidden name="hitFlag" />
<s:hidden name="secure" value="${secure}"/>
</s:form>


<c:if test="${hitFlag == 1}" >
	<c:if test="${secure == 'false' && ppType=='' }" >
		<c:redirect url="/myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&forQuotation=${forQuotation}"/>
	</c:if>
	<c:if test="${secure == 'false' && ppType!='' }" >
		<c:redirect url="/myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&ppType=${ppType}&forQuotation=${forQuotation}&PPID=${PPID}"/>
	</c:if>
	<c:if test="${secure == 'true'}" >
		<c:redirect url="/secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=true&forQuotation=${forQuotation}"/>
	</c:if>
</c:if>
<script type="text/javascript">
try{
	var fileTypeForPartnerCode = '${fileTypeForPartnerCode}'; 
    var fileTypePartnerCode = document.forms['uploadForm'].elements['fileType'].value ;
   if(fileTypeForPartnerCode.indexOf(fileTypePartnerCode)) 
	   {
	<c:if test="${documentAccessIsVendorCode}">
	var e2 = document.getElementById('venderInvoicesId');
	//alert(e2)
	e2.style.display= 'block';
	e2.style.visibility= 'visible';
	document.getElementById('venderInvoiceValue').value=true;  
	</c:if>
	}else{
		document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value = "";
		}
	<c:if test="${documentAccessIsPaymentStatus}">
	var e1 = document.getElementById('paymentStatusId');
	e1.style.display= 'block';
	e1.style.visibility= 'visible';
	</c:if>
}catch(e){}
</script>
<script type="text/javascript">
	try{		
	var errorType = '${fileNotExists}';
	}
	catch(e){}
	try{
	if( errorType != ''){
		alert('${fileNotExists}');
		location.href="uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=${secure}&forQuotation=${forQuotation}&docUpload=${docUpload}";
	}
	}
	catch(e){}
	//validateUpload();
   	Form.focusFirstElement($('uploadForm'));
   	function goToList(){
   	try{
   		<c:if test="${fileCabinetView ne 'Document Centre' }">
	   		if('${secure}' == 'true'){
	   			location.href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=true&forQuotation=${forQuotation}";
	   		}if('${secure}' == 'false' && '${ppType}' ==''){
	   			location.href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&forQuotation=${forQuotation}";
	   		}
	   		if('${secure}' == 'false' && '${ppType}' !=''){
	   			location.href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&ppType=${ppType}&PPID=${PPID}";
	   		}  	
   		</c:if>	
   		<c:if test="${fileCabinetView == 'Document Centre' }">
   		if('${secure}' == 'true'){
   			location.href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=true&forQuotation=${forQuotation}";
   		}if('${secure}' == 'false' && '${ppType}' ==''){
   			location.href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&forQuotation=${forQuotation}";
   		}
   		if('${secure}' == 'false' && '${ppType}' !=''){
   			location.href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&ppType=${ppType}&PPID=${PPID}";
   		}  	
		</c:if>	
	}
	catch(e){}
	}
</script>
 <script type="text/javascript">
function showOrHide(value) {
    if (value == 0) {
        if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
    }else if (value == 1) {
        if (document.layers)
          document.layers["overlay"].visibility='show';
        else
          document.getElementById("overlay").style.visibility='visible';
    }
}
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/filedrop.js"></script> 
    <script type="text/javascript">
      // Tell FileDrop we can deal with iframe uploads using this URL:
      var options = {iframe: {url: ''}};
      // Attach FileDrop to an area ('zone' is an ID but you can also give a DOM node):
      var zone = new FileDrop('zone', options);

      // Do something when a user chooses or drops a file:
      zone.event('send', function (files) {
        // Depending on browser support files (FileList) might contain multiple items.
        files.each(function (file) {
          // React on successful AJAX upload:
          file.event('done', function (xhr) {
            // 'this' here points to fd.File instance that has triggered the event.
         //  fileUplodedInPath(this);
        	  alert("File has been Successfully uploaded");
              showOrHide(0)
                      <c:if test="${fileCabinetView == 'Document Centre' }">
                      location.href ="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&ppType=${ppType}&PPID=${PPID}&active=true&secure=false&forQuotation=${forQuotation}";
                      </c:if>
                      <c:if test="${fileCabinetView != 'Document Centre' }">
                      location.href ="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false&forQuotation=${forQuotation}&ppType=${ppType}&PPID=${PPID}";
                      </c:if>
          });
          file.event('error', function (xhr) { 
          	  alert("File has not been uploaded, please try again");
                showOrHide(0) 
            });

          // Send the file:
           /*  var  fileFileName = file.name;
            var fileType = document.forms['uploadForm'].elements['fileType'].value;
            var fileDescription = document.forms['uploadForm'].elements['myFile.description'].value;  */
        
            var customerNumber = document.forms['uploadForm'].elements['myFile.customerNumber'].value;
            var fileId = document.forms['uploadForm'].elements['myFile.fileId'].value;
            var SoId = '${fileId}';
           
            var isAccportal =document.getElementById('uploadForm_myFile_isAccportal').checked;
            var isCportal =document.getElementById('uploadForm_myFile_isCportal').checked;
            var isPartnerPortal =document.getElementById('uploadForm_myFile_isPartnerPortal').checked;
            <c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">
            var isBookingAgent =document.forms['uploadForm'].elements['myFile.isBookingAgent'].checked;
            var isNetworkAgent =document.forms['uploadForm'].elements['myFile.isNetworkAgent'].checked;
            var isOriginAgent =document.getElementById('isOriginAgent').checked;
            var isSubOriginAgent =document.getElementById('isSubOriginAgent').checked;
            var isDestAgent =document.getElementById('isDestAgent').checked;
            var isSubDestAgent =document.getElementById('isSubDestAgent').checked;
            </c:if>
            <c:if test="${myFileFor=='PO' || myFileFor =='Truck'}">
            var isBookingAgent =false;
            var isNetworkAgent =false;
            var isOriginAgent =false;
            var isSubOriginAgent =false;
            var isDestAgent =false;
            var isSubDestAgent =false;
            </c:if>
            var documentCategory = document.forms['uploadForm'].elements['myFile.documentCategory'].value;
            var mapFolder = document.forms['uploadForm'].elements['mapFolder'].value; 
            var  isServiceProvider = document.getElementById('uploadForm_myFile_isServiceProvider').checked;
            var invoiceAttachment = document.getElementById('uploadForm_myFile_invoiceAttachment').checked;
            var isVendorCode = document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value
            var myFileFor = '${fileNameFor}';
            var draggedFileName = file.name;
            var uploadFileSize = file.size;
            var fileType = document.forms['uploadForm'].elements['fileType'].value;
            var fileDescription = document.forms['uploadForm'].elements['myFile.description'].value;    
            var isPayingStatus = document.forms['uploadForm'].elements['myFile.accountLinePayingStatus'].value;
            var tempCheck = "tempCheck";
            var vendorCode = document.getElementById('uploadForm_myFile_accountLineVendorCode').value;

            var agree = confirm("Would you like to upload the following file?\n" + draggedFileName);
            if(agree){ 
            if(fileType == null || fileType == ''){
            alert("Please choose a Document Type before uploading a file");
            return false;
            }else if(document.forms['uploadForm'].elements['venderInvoiceValue'].value=="true" && document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].value==""){
        		document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].focus;
            	 alert("Please Select Partner Code");
            	 return false;
            }else if(uploadFileSize > 10485760){
            	alert("Files greater than 10MB cannot be uploaded, please rescan and upload");
            	return false;
            } else{
            var valueForDragDrop= fileType+"~"+fileDescription+"~"+customerNumber+"~"+fileId+"~"+SoId+"~"+isAccportal+"~"+isCportal+"~"+isPartnerPortal+"~"+isBookingAgent+"~"+isNetworkAgent+"~"+isOriginAgent+"~"+isSubOriginAgent+"~"+isDestAgent+"~"+isSubDestAgent+"~"+mapFolder+"~"+documentCategory+"~"+myFileFor+"~"+isServiceProvider+"~"+invoiceAttachment+"~"+isVendorCode+"~"+isPayingStatus+"~"+tempCheck;
            file.sendTo('uploadFiles.html?id=${fileId}&noteFor=${noteForTemp}&active=true&secure=false&ppType=${ppType}&PPID=${PPID}&valueForDragDrop='+encodeURIComponent(valueForDragDrop)+'&draggedFileName='+encodeURIComponent(draggedFileName)+'&uploadFileSize='+uploadFileSize);
            showOrHide(1) 
           /*  showOrHide(1) ; 
            setTimeout(function(){
            alert("File has been Successfully uploaded");
            showOrHide(0)
            		<c:if test="${fileCabinetView == 'Document Centre' }">
                    location.href ="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}";
                    </c:if>
                    <c:if test="${fileCabinetView != 'Document Centre' }">
                    location.href ="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}";
                    </c:if>
             }, 8000); return false; */
            }
            }else{
            	return false;
            }
        });
      });

      // React on successful iframe fallback upload (this is separate mechanism
      // from proper AJAX upload hence another handler):
      zone.event('iframeDone', function (xhr) {
        alert('Done uploading via <iframe>, response:\n\n' + xhr.responseText);
      });

      // A bit of sugar - toggling multiple selection:
      fd.addEvent(fd.byID('multiple'), 'change', function (e) {
        zone.multiple(e.currentTarget || e.srcElement.checked);
      });
    </script>