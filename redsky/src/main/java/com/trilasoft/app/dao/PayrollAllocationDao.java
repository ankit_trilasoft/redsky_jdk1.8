package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;
import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PayrollAllocation;

public interface PayrollAllocationDao extends GenericDao<PayrollAllocation, Long>{
	
	
	public List findMaximumId();
	
	public List findById(Long id);
	
	public List<PayrollAllocation> searchpayrollAllocation(String code,String job,String service,String calculation);
	
	public Map<String, String> findRecalc(String corpId);
}
