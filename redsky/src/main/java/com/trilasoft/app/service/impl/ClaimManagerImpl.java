/**
 * Implementation of ClaimManager interface.</p>
 * Set the Dao for communication with the data layer.
 * This class represents the basic "Claim" object in Redsky that allows for Claim management.
 * @Class Name	ClaimManagerImpl
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * @param 		dao
 */

package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ClaimDao;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.service.ClaimManager;


 
public class ClaimManagerImpl extends GenericManagerImpl<Claim, Long> implements ClaimManager { 
    ClaimDao claimDao; 
    
 
    public ClaimManagerImpl(ClaimDao claimDao) { 
        super(claimDao); 
        this.claimDao = claimDao; 
    } 
    public Long findMaximum(String corpId) {
    	return claimDao.findMaximum(corpId);
    }
    public List<Claim> findByShipNumber(String shipNumber) { 
        return claimDao.findByShipNumber(shipNumber); 
    } 
    public List findMaximumId(){
    	return claimDao.findMaximumId(); 
    }
    public List findClaims(Long claimNumber, String lastName, String shipNumber, Date closeDate,String registrationNumber,String claimPerson,BigInteger origClmsAmt,String corpId){
    	return claimDao.findClaims(claimNumber, lastName, shipNumber, closeDate,registrationNumber,claimPerson,origClmsAmt,corpId); 
    }
    public List findUserDetails(String userName){
		return claimDao.findUserDetails(userName);
	}
	public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName){
		claimDao.resetPassword(passwordNew,confirmPasswordNew,pwdHintQues,pwdHint,userName);
	}
	public List findPwordHintQ(String pwdHintQues,String userName){
		return claimDao.findPwordHintQ(pwdHintQues,userName);	
	}
	public void forgotPassword(String pwdHintQues,String pwdHint,String userName){
		claimDao.forgotPassword(pwdHintQues,pwdHint,userName);
	}
	public List checkByShipNumber(String ship){
		return claimDao.checkByShipNumber(ship);
	}
	public List checkByCloseDate(String ship){
		return claimDao.checkByCloseDate(ship);
	}
	public List checkBySirviceOrderId(Long serviceOrderId, String corpId) {
		
		return claimDao.checkBySirviceOrderId(serviceOrderId, corpId);
	}
	public List claimSortOrderByUser(String username){
		return claimDao.claimSortOrderByUser(username);
	}
	public List claimList(Long sid){
		return claimDao.claimList(sid);
	}
	public List findResponsiblePerson(String custJobType, String companyDivision, String sessionCorpID){
		return claimDao.findResponsiblePerson(custJobType, companyDivision, sessionCorpID);
	}
	public Long findRemoteClaim(String idNumber, Long sid){
		return claimDao.findRemoteClaim(idNumber, sid);
	}
	public Long findMaximumForOtherCorpid(String sessionCorpID){
		return claimDao.findMaximumForOtherCorpid(sessionCorpID);
	}
	public String getClaimPersonName(String claimPerson, String sessionCorpID){
		return claimDao.getClaimPersonName(claimPerson, sessionCorpID);
	}
	public List findPartnerCodeOtherCorpID(String insurerCode,String sessionCorpID){
		return claimDao.findPartnerCodeOtherCorpID(insurerCode, sessionCorpID);
	}
	public List findMaximumIdNumber(String shipNumber){
		return claimDao.findMaximumIdNumber(shipNumber);
	}
	public Integer findClaimByValue(String corpId,String shipNumber){
		return claimDao.findClaimByValue(corpId,shipNumber);
	}
	public List findUserByBasedAt(String basedAt,String corpId)
	{
		return claimDao.findUserByBasedAt(basedAt,corpId);
	}
	public List findUserCompanyDivision(String basedAt,String corpId){
		return claimDao.findUserCompanyDivision(basedAt,corpId);
	}
	public int updateSurveyCalData(String startdate,String id)
	{
		return claimDao.updateSurveyCalData(startdate,id);
	}


}