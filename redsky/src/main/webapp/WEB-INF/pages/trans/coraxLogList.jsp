<%@ include file="/common/taglibs.jsp"%> 


<head> 

<title>Corax Log List</title> 

<meta name="heading" content="Corax Log List"/> 

<script language="javascript" type="text/javascript">


</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-17px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head> 
 
<s:form id="coraxLogList" name="coraxLogList" action="" method="post" validate="true">  
<div id="otabs" style="margin-top:-15px; ">
		  <ul>
		    <li><a class="current"><span>Corax Log</span></a></li>
		    
		  </ul>
		</div> 
<div id="Layer1" style="width:100%">

		<div class="spnblk">&nbsp;</div>



		<div class="spnblk">&nbsp;</div>
<s:set name="coraxLogList" value="coraxLogLists" scope="request"/>
<c:if test="${coraxLogLists!='[]'}">
<display:table name="coraxLogList" class="table" requestURI="" id="coraxLogList"  defaultsort="1" export="true" pagesize="10" style="margin-top:1px;"> 
<display:column property="ticketNo"  sortable="true" paramId="ticketNo" paramProperty="ticketNo" title="Ticket No "/>
<display:column property="shipNumber"  sortable="true" paramId="shipNumber" paramProperty="shipNumber" title="Ship Number"/>
<display:column property="actionName" sortable="true" paramId="actionName" paramProperty="actionName" title="Action Name"/>
<display:column property="actionStatus" sortable="true" paramId="actionStatus" paramProperty="actionStatus" title="Action Status"/>
<display:column property="message" sortable="true" paramId="message" paramProperty="message" title="Message"/>
<display:column property="typeOfFlow" sortable="true" paramId="typeOfFlow" paramProperty="typeOfFlow" title="Type Of Flow"/>
</display:table> 
</c:if>
<c:if test="${coraxLogLists=='[]'}">
<display:table name="coraxLogList" class="table" requestURI="" id="coraxLogList"  defaultsort="1" export="true" pagesize="10" style="margin-top:1px;"> 
<display:column property="ticketNo"  sortable="true" paramId="ticketNo" paramProperty="ticketNo" title="Ticket No "/>
<display:column property="shipNumber"  sortable="true" paramId="shipNumber" paramProperty="shipNumber" title="Ship Number"/>
<display:column property="actionName" sortable="true" paramId="actionName" paramProperty="actionName" title="Action Name"/>
<display:column property="actionStatus" sortable="true" paramId="actionStatus" paramProperty="actionStatus" title="Action Status"/>
<display:column property="message" sortable="true" paramId="message" paramProperty="message" title="Message"/>
<display:column property="typeOfFlow" sortable="true" paramId="typeOfFlow" paramProperty="typeOfFlow" title="Type Of Flow"/>
</display:table> 
</c:if>

<td align="right">
</td>
</div>

</s:form>
<script type="text/javascript"> 
</script> 
