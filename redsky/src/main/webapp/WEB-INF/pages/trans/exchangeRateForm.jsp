<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "ExchangeRate" in Redsky.
 * @File Name	exchangeRateForm
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        15-Dec-2008
 * --%>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags"%>  

<head>
    <title><fmt:message key="exchangeRate.title"/></title>
    <meta name="heading" content="<fmt:message key='exchangeRate.heading'/>"/>  

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="JavaScript">
	function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;	  
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==110)|| ( keyCode==190)||(keyCode==109); 
	 
	}
</script>	
<!-- This javascript method used to change form status -->
<script>
function changeStatus()
{
    document.forms['exchangeForm'].elements['formStatus'].value = '1';
}

function setOfficialRate()
{
   var officialRate = document.forms['exchangeForm'].elements['exchangeRate.officialRate'].value;
   var margin = document.forms['exchangeForm'].elements['exchangeRate.marginApplied'].value;   
   if(officialRate>0)
   {
   var baseCur=officialRate*(1-margin/100);
   var baseCurrencyValue=baseCur.toFixed(4);   
   var currencyBase=1/baseCurrencyValue;
   var currencyBaseRate=currencyBase.toFixed(4);
   document.forms['exchangeForm'].elements['exchangeRate.baseCurrencyRate'].value=baseCurrencyValue;   
   document.forms['exchangeForm'].elements['exchangeRate.currencyBaseRate'].value=currencyBaseRate;  
   document.forms['exchangeForm'].elements['exchangeRate.valueDate'].focus(); 
   }
   if(officialRate == 0)
   {
   	document.forms['exchangeForm'].elements['exchangeRate.baseCurrencyRate'].value=0;
    document.forms['exchangeForm'].elements['exchangeRate.currencyBaseRate'].value=0;  
   	document.forms['exchangeForm'].elements['exchangeRate.valueDate'].focus(); 
   }
   
}

// This javascript method used to set baseCurrencyRate.
function setBaseCurrency()
{
   var currencyBase = document.forms['exchangeForm'].elements['exchangeRate.currencyBaseRate'].value;
   var baseCurrency = document.forms['exchangeForm'].elements['exchangeRate.baseCurrencyRate'].value;   
   if(currencyBase>0)
   {
   var baseCur=(1/currencyBase);
   var baseCurrencyValue=baseCur.toFixed(4);   
   document.forms['exchangeForm'].elements['exchangeRate.baseCurrencyRate'].value=baseCurrencyValue;   
   document.forms['exchangeForm'].elements['exchangeRate.valueDate'].focus(); 
   }
   if(currencyBase == 0)
   {
   	document.forms['exchangeForm'].elements['exchangeRate.baseCurrencyRate'].value=0;
   	document.forms['exchangeForm'].elements['exchangeRate.valueDate'].focus(); 
   }
   
}

// This javascript method used to set currencyBaseRate.
function setCurrencyBase()
{
   var currencyBase = document.forms['exchangeForm'].elements['exchangeRate.baseCurrencyRate'].value;
   if(currencyBase>0)
   {
   var baseCur=(1/currencyBase);
   var currencyBaseValue=baseCur.toFixed(4);
   document.forms['exchangeForm'].elements['exchangeRate.currencyBaseRate'].value=currencyBaseValue;
   document.forms['exchangeForm'].elements['exchangeRate.valueDate'].focus(); 
   }
   if(currencyBase == 0)
   {
   	document.forms['exchangeForm'].elements['exchangeRate.currencyBaseRate'].value=0;
   	document.forms['exchangeForm'].elements['exchangeRate.valueDate'].focus(); 
   }
}
///////////////////
var form_submitted = false;
function saveSubmit()
{

	if (form_submitted)
	  {
	    alert ("Your form has already been submitted. Please wait...");
	    return false;
	  }
	else{
			var currency = document.forms['exchangeForm'].elements['exchangeRate.currency'].value;
			if(currency=='')
				{
					alert("Currency is required field.");
					return false;
				}
			form_submitted = true;
			return true;
	  	}
}

// This javascript method used for auto save functionality.
function autoSave(clickType){
    var checkStatus=document.forms['exchangeForm'].elements['formStatus'].value;    
	var currency = document.forms['exchangeForm'].elements['exchangeRate.currency'].value;
	
	if(checkStatus=='1' && currency=='')
	{
	alert("Currency is required field.");
	return false;
	}
	if(!(clickType == 'save')){
		if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${auditSetup.id}"/>';
			var id1 = document.forms['exchangeForm'].elements['exchangeRate.id'].value;

			if(document.forms['exchangeForm'].elements['gotoPageString'].value == 'gototab.exchangeRateFormList'){

			noSaveAction = 'exchangeRateFormList.html';

			}
		if(checkStatus=='1'){	
		processAutoSave(document.forms['exchangeForm'], 'saveExchangeRate!saveOnTabChange.html', noSaveAction);
        }else{       		
			location.href = 'exchangeRateFormList.html';	
        }
		}else{
			var id1 = document.forms['exchangeForm'].elements['exchangeRate.id'].value;
			
			if (document.forms['exchangeForm'].elements['formStatus'].value == '1'){
				var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the Audit Setup Form");
				if(agree){
					document.forms['exchangeForm'].action = 'saveExchangeRate!saveOnTabChange.html';
					document.forms['exchangeForm'].submit();
				}else{
					if(id1 != ''){
					if(document.forms['exchangeForm'].elements['gotoPageString'].value == 'gototab.exchangeRateFormList'){
						location.href = 'exchangeRateFormList.html';
						}
					
							
				}
		}
	}else{
		if(id1 != ''){
					if(document.forms['exchangeForm'].elements['gotoPageString'].value == 'gototab.exchangeRateFormList'){	
							location.href = 'exchangeRateFormList.html';
							}
			
			
					}
			}
		}
	}
}
 
</script>
</head>
<!-- Starting of form tag --> 
<s:form id="exchangeForm" name="exchangeForm" action="saveExchangeRate" method="post" validate="true"> 
<s:hidden name="exchangeRate.id"/> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.exchangeRateFormList' }">
    <c:redirect url="/exchangeRateFormList.html"/>
</c:when>

</c:choose>
</c:if>
<div id="layer3" style="width: 100%">
<div id="newmnav" onkeydown="changeStatus()">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Exchange Rate Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a onclick="setReturnString('gototab.exchangeRateFormList');return autoSave();"><span>Exchange Rate List</span></a></li>
			<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${exchangeRate.id}&tableName=exchangerate&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			
		  </ul>
		</div>
	
		<div class="spn">&nbsp;</div>
	
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:2px;" ><span></span></div>
   <div class="center-content">		

				<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td align="right" class="listwhitetext" style="height:2px;"></td>	
						</tr>
						<tr>						
							<td class="listwhitetext" align="right">Currency<font color="red" size="2">*</font></td>							
							<td>
							<s:select name="exchangeRate.currency" list="%{currency}" cssClass="list-menu" cssStyle="width:124px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="1"/>							
							</td>
							<td style="width:10px"></td>
							<td class="listwhitetext"  align="right">Base Currency</td>							
							<td align="left"><s:textfield key="exchangeRate.baseCurrency"  required="false"  cssClass="input-text" cssStyle="width:120px" onchange="changeStatus();" tabindex="2"/></td>							
						</tr>
						
						<tr>						
						    <td class="listwhitetext" align="right" style="width:150px" >Official Rate</td>						
							<td class="listwhitetext"><s:textfield key="exchangeRate.officialRate" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumsAllowed(event)" required="false" onchange="setOfficialRate();changeStatus();"   cssClass="input-text" tabindex="3"/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td class="listwhitetext" align="right" style="width:150px" >Base Currency Rate</td>							
							<td class="listwhitetext"><s:textfield key="exchangeRate.baseCurrencyRate" cssStyle="text-align:right;width:120px" onkeydown="return onlyNumsAllowed(event)" required="false" onchange="setCurrencyBase();changeStatus();"   cssClass="input-text" tabindex="4"/></td>
							
						</tr>
						
						<tr>								
							<td class="listwhitetext" align="right">Currency Base Rate</td>						
							<td class="listwhitetext"><s:textfield key="exchangeRate.currencyBaseRate" cssStyle="text-align:right;width:120px" onkeydown="return onlyNumsAllowed(event)" required="false" onchange="setBaseCurrency();changeStatus();" cssClass="input-text" tabindex="5"/></td>							
						    <td align="right" class="listwhitetext" style="width:10px"></td>				
							<td class="listwhitetext" align="right">Value Date</td>														
							<c:if test="${not empty exchangeRate.valueDate}">
								<s:text id="exchangeRatevalueDate" name="${FormDateValue}"><s:param name="value" value="exchangeRate.valueDate"/></s:text>
								<td><s:textfield id="valueDate" cssClass="input-text" name="exchangeRate.valueDate" value="%{exchangeRatevalueDate}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event, this)" onselect="changeStatus();" readonly="false" />
								<img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
								</td>
							</c:if>
							<c:if test="${empty exchangeRate.valueDate}">
								<td><s:textfield id="valueDate" cssClass="input-text" name="exchangeRate.valueDate" cssStyle="width:65px" maxlength="11" readonly="false" onselect="changeStatus();" />
								<img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
								</td>
							</c:if>
                            </td>							
							<td align="right" class="listwhitetext" style="width:10px"></td>							
							<td>							
						</tr>
						
						<tr>
						<td align="right" class="listwhitetext">Margin</td>										
				        <td align="left"><s:textfield name="exchangeRate.marginApplied" required="true" cssClass="input-text" cssStyle="width:120px;" maxlength="9" onkeydown="return onlyNumsAllowed(event)" onchange="setOfficialRate();changeStatus();" tabindex="6"/><b>%</b></td>	
				        <td align="right" class="listwhitetext" style="width:10px"></td>
				        <td align="right" class="listwhitetext" width="150">Manual Update </td>
		                <td width="70px"><s:select cssClass="list-menu" name="exchangeRate.manualUpdate" list="%{yesno}" cssStyle="width:69px;" onchange="changeStatus();" tabindex="7"/></td>
					    </tr>
						<tr> <td style="width:10px;height:20px;"></td> </tr>
					</tbody>
				</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
			<table border="0" width="780px">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>							
							<td valign="top">							
							</td>
							<td style="width:140px">
							<fmt:formatDate var="exchangeRateCreatedOnFormattedValue" value="${exchangeRate.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="exchangeRate.createdOn" value="${exchangeRateCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${exchangeRate.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty exchangeRate.id}">
								<s:hidden name="exchangeRate.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{exchangeRate.createdBy}"/></td>
							</c:if>
							<c:if test="${empty exchangeRate.id}">
								<s:hidden name="exchangeRate.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:80px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="exchangeRateupdatedOnFormattedValue" value="${exchangeRate.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="exchangeRate.updatedOn" value="${exchangeRateupdatedOnFormattedValue}"/>
							<td style="width:140px"><fmt:formatDate value="${exchangeRate.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty exchangeRate.id}">
								<s:hidden name="exchangeRate.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{exchangeRate.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty exchangeRate.id}">
								<s:hidden name="exchangeRate.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
<!-- Save button  --> 
<s:submit cssClass="cssbutton1"  cssStyle="width:55px;"  key="button.save" onclick="return saveSubmit();" tabindex=""/>  

<!-- Reset button  --> 
<s:reset cssClass="cssbutton1"  type="button" key="Reset" cssStyle="width:55px; height:25px"/>  	
    <input type="button" id="cancel" class="cssbutton1"tabindex="8" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/exchangeRateFormList.html"/>'"  
        value="Cancel"/> 
</s:form>
<!-- End of form tag  --> 
   
<script type="text/javascript">
try{
	allReadOnly(); 
	}
	catch(e){}
	
function allReadOnly()
{
<c:if test="${not empty exchangeRate.id}">
var currency = document.forms['exchangeForm'].elements['exchangeRate.currency'].value;
var baseCurrency = document.forms['exchangeForm'].elements['exchangeRate.baseCurrency'].value;
var imgElement = document.getElementById('valueDate_trigger');
	if(currency==baseCurrency) { 
     var elementsLen=document.forms['exchangeForm'].elements.length; 
    for(i=0;i<=elementsLen-1;i++)
	{
	document.forms['exchangeForm'].elements[i].readOnly =true;
	document.forms['exchangeForm'].elements[i].disabled=true;
	imgElement.style.display = 'none'; 
	}
    document.getElementById('cancel').disabled=false; 
	}
</c:if>
}
    Form.focusFirstElement($("exchangeForm"));   
    var f = document.getElementById('exchangeForm'); 
	f.setAttribute("autocomplete", "off"); 
</script> 
<script type="text/javascript">
	setOnSelectBasedMethods(["changeStatus()"]);
	setCalendarFunctionality();
</script>

