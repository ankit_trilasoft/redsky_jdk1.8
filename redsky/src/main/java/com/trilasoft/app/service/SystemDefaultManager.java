package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.SystemDefault;

public interface SystemDefaultManager extends GenericManager<SystemDefault, Long> {

	public List<SystemDefault> findByCorpID(String corpId);

	public List findIdByCorpID(String sessionCorpID);

	public List findInvExtractSeq(String corpID);

	public int updateInvExtractSeq(String invExtractSeq, String corpID);
	public int updateMamutInvExtractSeq(String invExtractSeq, String corpID);
	
	public List findPayExtractSeq(String corpID);
	public List findMamutPayExtractSeq(String corpID);

	public int updatePayExtractSeq(String payExtractSeq, String corpID);
	public int updateMamutPayExtractSeq(String payExtractSeq, String corpID);

	public List findPrtExtractSeq(String corpID);

	public int updatePrtExtractSeq(String prtExtractSeq, String corpID);

	public void updateDailyOpsLimit(String dailyOperationalLimit, String dailyOpLimitPC, String minServiceDay, String sessionCorpID);
	public int updateSAPExtractSeq(String invExtractSeq, String corpID);
	public List findSAPExtractSeq(String corpID);
	public List findSAPExtractSeqForUGCN(String corpID);
	public int updateSAPPrtExtractSeq(String prtExtractSeq, String corpID) ;
	public List findSAPPayExtractSeq(String corpID);
	public int updateSAPInvExtractSeq(String invExtractSeq, String corpID);
	public int updateInvExtractSeqUTSI(String invExtractSeq, String corpID);
	public List findInvExtractSeqUTSI(String corpID);
	public List findPrtExtractSeqUTSI(String corpID);
	public int updatePrtExtractSeqUTSI(String prtExtractSeq, String corpID);
	public List findPayExtractSeqUTSI(String corpID);
	public int updatePayExtractSeqUTSI(String payExtractSeq, String corpID);
	public int updateSeqNumber(String refType, String seq, String sessionCorpID);
	public List getjaournalValidationValue(String sessionCorpID);
	public int updateJournalValidation(String validationStr,String sessionCorpID);
	public List findPaymentApplicationExtractSeq(String corpID);
	public int updatePaymentApplicationExtractSeq(String invExtractSeq, String corpID);
	public void updateSoextractSeq(String sessionCorpID,String soextractSeq);
	public String getCorpIdUnit(String corpID, String WnVol);
	public String OIServicesFromSystemDefault(String corpID);
}