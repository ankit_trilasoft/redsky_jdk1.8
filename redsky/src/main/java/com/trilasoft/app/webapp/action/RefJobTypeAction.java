package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.RefJobType;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.RefJobTypeManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;


public class RefJobTypeAction extends BaseAction {

	private Long id;

	private List refMasters;

	private RefJobType refJobType;

	private RefJobTypeManager refJobTypeManager;

	private RefMasterManager refMasterManager;

	private String sessionCorpID;
	
	private List jobType;
	
	private  Map<String, String> pricing = new LinkedHashMap<String, String>();

	private  Map<String, String> billing = new LinkedHashMap<String, String>();

	private  Map<String, String> payable = new LinkedHashMap<String, String>();
	
	private  Map<String, String> personClaimsList = new LinkedHashMap<String, String>();
	
	private  Map<String, String> auditor = new LinkedHashMap<String, String>();
	
	private Map<String, String> surveyToolMap;
	
	private Map<String, String> ocountry;
	
	private List<String> division;
	
	private Map<String, String> jobs;

	private  Map<String, String> coordinator = new LinkedHashMap<String, String>();

	private Map<String, String> estimator;
	
	private String hitflag;
	private String validateStringjob;
	private String validatecompanyDivision;
	private boolean accountLineAccountPortalFlag=false;
	private ServiceOrderManager serviceOrderManager;
	private List<SystemDefault> sysDefaultDetail;
	private  Map<String, String> internalBillingPersonList = new LinkedHashMap<String, String>();

	private  Map<String, String> opsPerson = new LinkedHashMap<String, String>();
	private  Map<String, String> forwarderList = new LinkedHashMap<String, String>();
	public RefJobTypeAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		//hitflag="0";
	}

	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			refJobType = refJobTypeManager.get(id);
			validateStringjob=refJobType.getJob();
			validatecompanyDivision=refJobType.getCompanyDivision();
			coordinator=refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_COORD",sessionCorpID);
			estimator=refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_SALE",sessionCorpID);
			pricing = refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_PRICING",sessionCorpID);
			billing = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_BILLING",sessionCorpID);
			payable = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_PAYABLE",sessionCorpID);
			personClaimsList = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_CLAIM_HANDLER",sessionCorpID);
			auditor = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_AUDITOR",sessionCorpID);
			internalBillingPersonList = refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_INTERNAL_BILLING",sessionCorpID);
			opsPerson = refMasterManager.findUser(sessionCorpID,"ROLE_OPS");
			forwarderList = refMasterManager.findUser(sessionCorpID,"ROLE_FORWARDER");
			division=refMasterManager.findCompanyList(sessionCorpID);
		} else {
			refJobType = new RefJobType();
			refJobType.setCorpID(sessionCorpID);
			refJobType.setCreatedOn(new Date());
			refJobType.setUpdatedOn(new Date());

			coordinator = refMasterManager.findUser(sessionCorpID, "ROLE_COORD");
			pricing = refMasterManager.findUser(sessionCorpID, "ROLE_PRICING");
			billing = refMasterManager.findUser(sessionCorpID, "ROLE_BILLING");
			payable = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			personClaimsList = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_CLAIM_HANDLER",sessionCorpID);
			auditor = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			estimator = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
			internalBillingPersonList = refMasterManager.findUser(sessionCorpID,"ROLE_INTERNAL_BILLING");
			opsPerson = refMasterManager.findUser(sessionCorpID,"ROLE_OPS");
			forwarderList = refMasterManager.findUser(sessionCorpID,"ROLE_FORWARDER");
			division =refMasterManager.findCompanyList(sessionCorpID);
		}
		return SUCCESS;
	}

	public String save() throws Exception {
		getComboList(sessionCorpID);
		refJobType.setCorpID(sessionCorpID);
		boolean isNew = (refJobType.getId() == null);
		if (isNew) {
			refJobType.setCreatedOn(new Date());
			Long i=Long.parseLong(refJobTypeManager.isExisted(refJobType.getJob(),refJobType.getCompanyDivision(),sessionCorpID).get(0).toString());
		if(i>=1){
			coordinator = refMasterManager.findUser(sessionCorpID, "ROLE_COORD");
			pricing = refMasterManager.findUser(sessionCorpID, "ROLE_PRICING");
			billing = refMasterManager.findUser(sessionCorpID, "ROLE_BILLING");
			payable = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			personClaimsList = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_CLAIM_HANDLER",sessionCorpID);
			auditor = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			division =refMasterManager.findCompanyList(sessionCorpID);
			estimator = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
			internalBillingPersonList = refMasterManager.findUser(sessionCorpID,"ROLE_INTERNAL_BILLING");
			opsPerson = refMasterManager.findUser(sessionCorpID,"ROLE_OPS");
			forwarderList = refMasterManager.findUser(sessionCorpID,"ROLE_FORWARDER");
			errorMessage("Job type for this Company division already exist.");
			return SUCCESS;
		}
		}
		refJobType.setUpdatedOn(new Date());
		refJobType.setUpdatedBy(getRequest().getRemoteUser());
		String newValidatejob=refJobType.getJob();
		String newValidateStringCDivision=refJobType.getCompanyDivision();
		if(!validateStringjob.equals(newValidatejob) || !validatecompanyDivision.equals(newValidateStringCDivision)){
		Long i=Long.parseLong(refJobTypeManager.isExisted(refJobType.getJob(),refJobType.getCompanyDivision(),sessionCorpID).get(0).toString());
		if(i>=1){
			coordinator = refMasterManager.findUser(sessionCorpID, "ROLE_COORD");
			pricing = refMasterManager.findUser(sessionCorpID, "ROLE_PRICING");
			billing = refMasterManager.findUser(sessionCorpID, "ROLE_BILLING");
			payable = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			personClaimsList = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_CLAIM_HANDLER",sessionCorpID);
			auditor = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			division =refMasterManager.findCompanyList(sessionCorpID);
			estimator = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
			internalBillingPersonList = refMasterManager.findUser(sessionCorpID,"ROLE_INTERNAL_BILLING");
			opsPerson = refMasterManager.findUser(sessionCorpID,"ROLE_OPS");
			forwarderList = refMasterManager.findUser(sessionCorpID,"ROLE_FORWARDER");
			errorMessage("Job type for this Company division already exist.");
			return SUCCESS; 
		} 
		}
		   refJobType = refJobTypeManager.save(refJobType);
	
			coordinator=refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_COORD",sessionCorpID);
			estimator=refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_SALE",sessionCorpID);
			pricing = refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_PRICING",sessionCorpID);
			billing = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_BILLING",sessionCorpID);
			payable = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_PAYABLE",sessionCorpID);
			personClaimsList = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_CLAIM_HANDLER",sessionCorpID);
			auditor = refMasterManager.findCordinaor(refJobType.getJob(), "ROLE_AUDITOR",sessionCorpID);
			internalBillingPersonList = refMasterManager.findCordinaor(refJobType.getJob(),"ROLE_INTERNAL_BILLING",sessionCorpID);
			opsPerson = refMasterManager.findUser(sessionCorpID,"ROLE_OPS");
			forwarderList = refMasterManager.findUser(sessionCorpID,"ROLE_FORWARDER");
			division = refMasterManager.findCompanyList(sessionCorpID);
			
			String key = (isNew) ? "refJobType.added" : "refJobType.updated";   
		    saveMessage(getText(key));  
			return SUCCESS;
		
	}

	@SkipValidation
	public String list() {
		getComboList(sessionCorpID);
		division = refMasterManager.findCompanyList(sessionCorpID);
		jobType = refJobTypeManager.getAll();
		//String key = "Please enter your search criteria below";
		if(null!=hitflag && hitflag.equals("trues")){
			saveMessage(getText("refJobType.deleted"));
			//hitflag="0";
		}
		else{
		//saveMessage(getText(key));
		}
		return SUCCESS;
	}

	@SkipValidation
	public String getComboList(String corpId) {
		jobs = refMasterManager.findByParameter(corpId, "JOB");
		ocountry = refMasterManager.findCountry(corpId, "COUNTRY");
		surveyToolMap=refMasterManager.findByParameter(corpId, "SurveyTool");
		sysDefaultDetail = serviceOrderManager.findSysDefault(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
					accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
				}
			}
		}
		
		/*coordinator = refMasterManager.findUser(corpId, "ROLE_COORD");
		pricing = refMasterManager.findUser(corpId, "ROLE_PRICING");
		billing = refMasterManager.findUser(corpId, "ROLE_BILLING");
		payable = refMasterManager.findUser(corpId, "ROLE_PAYABLE");
		estimator = refMasterManager.findUser(corpId, "ROLE_SALE");*/
		return SUCCESS;
	}
	
	@SkipValidation
	public String delete(){
		refJobTypeManager.remove(id);
		//saveMessage(getText("refJobType.deleted"));
	  // setHitflag("1");
		return SUCCESS;
	} 
	
	private String cJob;
	private String cCompanydivision;
	private String cSurveytool;	
	
	@SkipValidation
	  public String search(){ 
		getComboList(sessionCorpID);
		division = refMasterManager.findCompanyList(sessionCorpID);
			jobType = refJobTypeManager.searchResult(cJob,cCompanydivision,cSurveytool,sessionCorpID);	    	   
	    return SUCCESS;     
	 } 
	

	public void setId(Long id) {
		this.id = id;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}

	public void setRefJobTypeManager(RefJobTypeManager refJobTypeManager) {
		this.refJobTypeManager = refJobTypeManager;
	}

	public RefJobType getRefJobType() {
		return refJobType;
	}

	public void setRefJobType(RefJobType refJobType) {
		this.refJobType = refJobType;
	}

	public List getJobType() {
		return jobType;
	}

	public void setJobType(List jobType) {
		this.jobType = jobType;
	}

	public Map<String, String> getBilling() {
		return billing;
	}

	public void setBilling(Map<String, String> billing) {
		this.billing = billing;
	}

	public Map<String, String> getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(Map<String, String> coordinator) {
		this.coordinator = coordinator;
	}

	public Map<String, String> getEstimator() {
		return estimator;
	}

	public void setEstimator(Map<String, String> estimator) {
		this.estimator = estimator;
	}

	public Map<String, String> getJobs() {
		return jobs;
	}

	public void setJobs(Map<String, String> jobs) {
		this.jobs = jobs;
	}

	public Map<String, String> getPayable() {
		return payable;
	}

	public void setPayable(Map<String, String> payable) {
		this.payable = payable;
	}

	public Map<String, String> getPricing() {
		return pricing;
	}

	public void setPricing(Map<String, String> pricing) {
		this.pricing = pricing;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public Map<String, String> getAuditor() {
		return auditor;
	}

	public void setAuditor(Map<String, String> auditor) {
		this.auditor = auditor;
	}

	public List<String> getDivision() {
		return division;
	}

	public void setDivision(List<String> division) {
		this.division = division;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public Map<String, String> getSurveyToolMap() {
		return surveyToolMap;
	}

	public void setSurveyToolMap(Map<String, String> surveyToolMap) {
		this.surveyToolMap = surveyToolMap;
	}

	public String getValidateStringjob() {
		return validateStringjob;
	}

	public void setValidateStringjob(String validateStringjob) {
		this.validateStringjob = validateStringjob;
	}

	public String getValidatecompanyDivision() {
		return validatecompanyDivision;
	}

	public void setValidatecompanyDivision(String validatecompanyDivision) {
		this.validatecompanyDivision = validatecompanyDivision;
	}

	public Map<String, String> getPersonClaimsList() {
		return personClaimsList;
	}

	public void setPersonClaimsList(Map<String, String> personClaimsList) {
		this.personClaimsList = personClaimsList;
	}

	public String getcJob() {
		return cJob;
	}

	public void setcJob(String cJob) {
		this.cJob = cJob;
	}

	public String getcCompanydivision() {
		return cCompanydivision;
	}

	public void setcCompanydivision(String cCompanydivision) {
		this.cCompanydivision = cCompanydivision;
	}

	public String getcSurveytool() {
		return cSurveytool;
	}

	public void setcSurveytool(String cSurveytool) {
		this.cSurveytool = cSurveytool;
	}

	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}

	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public Map<String, String> getInternalBillingPersonList() {
		return internalBillingPersonList;
	}

	public void setInternalBillingPersonList(
			Map<String, String> internalBillingPersonList) {
		this.internalBillingPersonList = internalBillingPersonList;
	}

	public Map<String, String> getOpsPerson() {
		return opsPerson;
	}

	public void setOpsPerson(Map<String, String> opsPerson) {
		this.opsPerson = opsPerson;
	}

	public Map<String, String> getForwarderList() {
		return forwarderList;
	}

	public void setForwarderList(Map<String, String> forwarderList) {
		this.forwarderList = forwarderList;
	}

	

}