<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Agent Detail For SO</title>
<meta name="heading"
	content="Agent Detail For SO" />
	<script type="text/javascript">
function setOriginDetail(){
	var soSetId = document.forms['popupOAGConformation'].elements['soSetId'].value;
	var ship=document.forms['popupOAGConformation'].elements['shipNumber'].value;
	location.href="copyOriginAgentDetail.html?soGetId=${serviceOrder.id}&type=origin&soSetId="+soSetId+"&shipNumber="+ship;	
}
function setOriginAgDetail(soSetId,shipNumber){
	document.getElementById('okButton').disabled=false;
	document.forms['popupOAGConformation'].elements['soSetId'].value = soSetId ;
	document.forms['popupOAGConformation'].elements['shipNumber'].value = shipNumber ;
}
window.onload = function (){
<c:if test="${hitFlag == 1}">
alert('OA details have been added to ${shipNumber}');
window.open('editTrackingStatus.html?id=${soSetId}','_blank');
self.close();
</c:if>
}
</script>
</head>
<body>
<s:form action="" name="popupOAGConformation">
<s:hidden name="soSetId" />
<s:hidden name="hitFlag" />
<s:hidden name="shipNumber" />
</s:form >

	     <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>			
	  		<li><a class="current"><span>Origin Agent For SO</span></a></li>  				   
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div>
		<table cellspacing="0" cellpadding="0" border="0" class="table"> 
			<thead> 	
	  	    <tr>
	  	      <th height="5px"></th>
	  	      <th><b>Mode</b></th> 			
	  		  <th><b>Service Order</b></th>	  			
	  		  <th><b>City, State, Country</b></th>
	  		</tr>
	  		</thead>
	  	    <c:forEach var="soList1" items="${soList}" >	  	
	  	    <tbody>    
	  	    <tr id="solistfor${soList1.id}">
	  	    	<td><input type="radio" class="input-textUpper" name="radioOrgAgDetail" id="radioOrgAgDetail" value="${soList1.id}" onclick="setOriginAgDetail('${soList1.id}','${soList1.shipNumber}');"</td> 	       
	  			<td><input type="text" class="input-textUpper" readonly="readonly" value="${soList1.mode}"></td>
	  			<td><input type="text" class="input-text" readonly="readonly" value="${soList1.shipNumber}" ></td>
	  		    <td><input type="text" class="input-textUpper" style="width:200px;" readonly="readonly" value="${soList1.originCity}, ${soList1.originState}, ${soList1.originCountry}"></td>
	  		</tr>
	  		</tbody>
	  		</c:forEach>
</table>
</div>
<input type="button"  Class="cssbutton" Style="width:60px;" disabled="true" id="okButton"  value="OK" onclick="setOriginDetail()"/>
<input type="button"  Class="cssbutton" Style="width:60px; margin-left:10px;"  value="Cancel" onclick="self.close()"/>
</body>


