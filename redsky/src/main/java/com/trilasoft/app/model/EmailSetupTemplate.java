package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "emailsetuptemplate")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class EmailSetupTemplate extends BaseObject{
	
	private Long id;
	private String corpId;
	private String updatedBy;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;
	private String saveAs;
	private String module;
	private String subModule;
	private String jobType;
	private String mode;
	private String routing;
	private String moveType;
	private String service;
	private String language;
	private String others;
	private String companyDivision;
	private String triggerField;
	private String emailFrom;
	private String emailTo;
	private String emailCc;
	private String emailBcc;
	private String emailSubject;
	private String attachedFileLocation;
	private String distributionMethod;
	private String fileTypeOfFileCabinet;
	private String formsId;
	private String status;
	private String emailBody;
	private Boolean enable= false;
	private Boolean addSignatureFromAppUser = false;
	private int orderNumber=0;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("updatedBy", updatedBy)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("saveAs", saveAs)
				.append("module", module).append("subModule", subModule)
				.append("jobType", jobType).append("mode", mode)
				.append("routing", routing).append("moveType", moveType)
				.append("service", service).append("language", language)
				.append("others", others).append("companyDivision", companyDivision)
				.append("triggerField", triggerField).append("emailFrom", emailFrom)
				.append("emailTo", emailTo).append("emailCc", emailCc)
				.append("emailBcc", emailBcc).append("emailSubject", emailSubject)
				.append("attachedFileLocation", attachedFileLocation).append("distributionMethod", distributionMethod)
				.append("fileTypeOfFileCabinet", fileTypeOfFileCabinet).append("formsId", formsId)
				.append("status", status).append("emailBody", emailBody).append("enable", enable)
				.append("addSignatureFromAppUser", addSignatureFromAppUser)
				.append("orderNumber", orderNumber)
				.toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof EmailSetupTemplate))
			return false;
		EmailSetupTemplate castOther = (EmailSetupTemplate) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(saveAs, castOther.saveAs)
				.append(module, castOther.module)
				.append(subModule, castOther.subModule)
				.append(jobType, castOther.jobType)
				.append(mode, castOther.mode).append(routing, castOther.routing)
				.append(moveType, castOther.moveType)
				.append(service, castOther.service)
				.append(language, castOther.language)
				.append(others, castOther.others)
				.append(companyDivision, castOther.companyDivision)
				.append(triggerField, castOther.triggerField)
				.append(emailFrom, castOther.emailFrom)
				.append(emailTo, castOther.emailTo)
				.append(emailCc, castOther.emailCc)
				.append(emailBcc, castOther.emailBcc)
				.append(emailSubject, castOther.emailSubject)
				.append(attachedFileLocation, castOther.attachedFileLocation)
				.append(distributionMethod, castOther.distributionMethod)
				.append(fileTypeOfFileCabinet, castOther.fileTypeOfFileCabinet)
				.append(formsId, castOther.formsId)
				.append(status, castOther.status)
				.append(emailBody, castOther.emailBody)
				.append(enable, castOther.enable)
				.append(addSignatureFromAppUser, castOther.addSignatureFromAppUser)
				.append(orderNumber, castOther.orderNumber)
				.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(updatedBy).append(createdBy).append(createdOn)
				.append(updatedOn).append(saveAs)
				.append(module).append(subModule)
				.append(jobType).append(mode).append(routing).append(moveType)
				.append(service).append(language).append(others)
				.append(companyDivision).append(triggerField)
				.append(emailFrom).append(emailTo)
				.append(emailCc).append(emailBcc)
				.append(emailSubject).append(attachedFileLocation)
				.append(distributionMethod).append(fileTypeOfFileCabinet)
				.append(formsId).append(status)
				.append(emailBody).append(enable).append(addSignatureFromAppUser)
				.append(orderNumber)
				.toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getSaveAs() {
		return saveAs;
	}

	public void setSaveAs(String saveAs) {
		this.saveAs = saveAs;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getSubModule() {
		return subModule;
	}

	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public String getMoveType() {
		return moveType;
	}

	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getTriggerField() {
		return triggerField;
	}

	public void setTriggerField(String triggerField) {
		this.triggerField = triggerField;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailCc() {
		return emailCc;
	}

	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}

	public String getEmailBcc() {
		return emailBcc;
	}

	public void setEmailBcc(String emailBcc) {
		this.emailBcc = emailBcc;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getAttachedFileLocation() {
		return attachedFileLocation;
	}

	public void setAttachedFileLocation(String attachedFileLocation) {
		this.attachedFileLocation = attachedFileLocation;
	}

	public String getDistributionMethod() {
		return distributionMethod;
	}

	public void setDistributionMethod(String distributionMethod) {
		this.distributionMethod = distributionMethod;
	}

	public String getFormsId() {
		return formsId;
	}

	public void setFormsId(String formsId) {
		this.formsId = formsId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getFileTypeOfFileCabinet() {
		return fileTypeOfFileCabinet;
	}

	public void setFileTypeOfFileCabinet(String fileTypeOfFileCabinet) {
		this.fileTypeOfFileCabinet = fileTypeOfFileCabinet;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public Boolean getAddSignatureFromAppUser() {
		return addSignatureFromAppUser;
	}

	public void setAddSignatureFromAppUser(Boolean addSignatureFromAppUser) {
		this.addSignatureFromAppUser = addSignatureFromAppUser;
	}
	
}
