<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_SALE") || userRole.equalsIgnoreCase("ROLE_COORD") || userRole.equalsIgnoreCase("ROLE_ADMIN")){
		userRole=role.getName();
		break;
	}
	
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html>
<head>
<!-- <link href='../fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='../fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' /> -->
<!-- <script src='../lib/jquery.min.js'></script>
<script src='../lib/jquery-ui.custom.min.js'></script>
<script src='../fullcalendar/fullcalendar.min.js'></script> -->
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendar.css'/>" /> 
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendar.print.css'/>" media='print'/> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquery-ui.custom.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/styles/fullcalendar/fullcalendar.min.js"></script>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script>
//
function clear_fields(){
document.forms['surveyListForm'].elements['consult'].value = "";
document.forms['surveyListForm'].elements['surveyFrom'].value = "";
document.forms['surveyListForm'].elements['surveyTo'].value = "";
document.forms['surveyListForm'].elements['surveyCity'].value = "";
document.forms['surveyListForm'].elements['surveyJob'].value = "";
}
function myEvents(){
	document.forms['surveyListForm'].action = "myCalendar.html";
	document.forms['surveyListForm'].submit();
}

function editCalendar(targetElement2){
	
	document.forms['surveyListForm'].action='editMyCalendar.html?from=list&decorator=popup&popup=true&from1=Survey&id='+targetElement2;
	document.forms['surveyListForm'].submit();
}

function autoPopulate_Date(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		//targetElement.form.elements['surveyListForm.surveyFrom'].value=datam;
		document.forms['surveyListForm'].elements['surveyFrom'].value=datam;
	}
	
	function autoPopulate_DateTo(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = (daym+7)+"-"+month+"-"+y.substring(2,4);
		document.forms['surveyListForm'].elements['surveyTo'].value=datam;
	}
	
	function isDate() {
	var surFrom = document.forms['surveyListForm'].elements['surveyFrom'].value;
	var surTo = document.forms['surveyListForm'].elements['surveyTo'].value;
	if (surFrom=='' || surTo==''){
	alert('Please select From Date and To Date');
	   return false;
	   } else {
	   return isValidDate();
	  }  
	}

function openURL(sURL) {
opener.document.location = sURL;
} 

function findAgent(position,billtocode)
{
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billtocode);
	    ajax_showTooltip(url,position);
	    return false
}

function findOriginAddress(position,sequenceNumber)
{
		var url="customerOriginAddress.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" + encodeURI(sequenceNumber);
	    ajax_showTooltip(url,position);
	    return false
}

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw new SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

function isValidDate(){
/////////alert("ssssssssssssssssssssss");
 var date1 = ''; 
 var date2 = '';
 var date1 = document.forms['surveyListForm'].elements['date1'].value;
 var date2 = document.forms['surveyListForm'].elements['date2'].value; 
 if (date1 != ''){
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = month+"-"+day+"-"+year;
   }
   
   if (date2 != ''){
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
   } 
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  
  //// date1 = finalDate.split("-");
 /// var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
   sDate.setDate(sDate.getDate() + 7);
   var df1 = sDate.format("dd-mmm-yy");
  if(date1 !='' && date2 =='') {
  document.forms['surveyListForm'].elements['date2'].value=df1;
  return true;
  } else if (date2 !='' && daysApart<0){
  alert("From Date should be less than To Date");
 document.forms['surveyListForm'].elements['date2'].value='';
 //// } else {
  return false;
  }  
 }



function copyFromDate(){
 var date1 = ''; 
 date1 = document.forms['surveyListForm'].elements['date1'].value; 
 if(date1 !='')
 {
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = month+"-"+day+"-"+year;
  
   date1 = finalDate.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
   sDate.setDate(sDate.getDate() + 7);
   var df1 = sDate.format("dd-mmm-yy");
  document.forms['surveyListForm'].elements['date2'].value=df1;
  }
 }


function confirmSubmit(targetElement){
	var consult = document.forms['surveyListForm'].elements['consult'].value;
	var surveyFrom = document.forms['surveyListForm'].elements['surveyFrom'].value;
	var surveyTo = document.forms['surveyListForm'].elements['surveyTo'].value;
	var surveyCity = document.forms['surveyListForm'].elements['surveyCity'].value;
	var surveyJob = document.forms['surveyListForm'].elements['surveyJob'].value;
	
	var agree=confirm("Are you sure you want to Delete this  Survey");
	var did = targetElement;
	if (agree){
		location.href='surveyDeleted.html?id='+did+'&consult='+consult+'&surveyFrom='+surveyFrom+'&surveyTo='+surveyTo+'&surveyCity='+surveyCity+'&surveyJob='+surveyJob+'&decorator=popup&popup=true&from=survey';
	}else{
		return false;
	}
}


//function for Survey Map

function scheduleSurvey()
           {
           var jobTypeSchedule =document.forms['surveyListForm'].elements['surveyJob'].value;
           var surveyCity = document.forms['surveyListForm'].elements['surveyCity'].value;
           var surveyFrom = document.forms['surveyListForm'].elements['surveyFrom'].value;
	       var surveyTo = document.forms['surveyListForm'].elements['surveyTo'].value;
           var targetAddress="";
           var surveyTime="";
	       var surveyTime2="";
	       var estimated= document.forms['surveyListForm'].elements['consult'].value;
	       if(estimated!=''){           
           window.open("surveyListSchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&surveyCity="+surveyCity+"&fromDate="+surveyFrom+"&toDate="+surveyTo+"&decorator=popup&popup=true","forms","height=550,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }else{
           alert("Please select the Consultant to proceed.");
           }
           }
//
//

//

	$(document).ready(function() {
	
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var fdt='${surveyFrom}';
		 //alert(fdt);
		var tdt='${surveyTo}';
		//alert(tdt);
		var d1 = new Date(Date.parse(fdt));
		// alert(Date.parse(fdt));
	    var fdateString = d1.getFullYear()+"-"+(d1.getMonth()+1)+"-"+d1.getDate();
	    var d2 = new Date(Date.parse(tdt));
	    var tdateString = d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+d2.getDate();
	   // alert(fdateString);
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			timeFormat: '',
			editable: true,
			events: "CalendarJsonServletDemo?surveyFrom=${surveyFrom}&surveyTo=${surveyTo}&consult=${consult}&surveyCity=${surveyCity}&surveyJob=${surveyJob}",
			 eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

			       /** alert(
			            event.title + " was moved " +
			            dayDelta + " days and " +
			            minuteDelta + " minutes."+"start date:"+
			            event.start+"end date"+event.end+"id:"+event.id
			        );*/

			        if (allDay) {
			            //alert("Event is now all-day");			           
			        }else{
			            //alert("Event has a time-of-day");
			        }

			        if (confirm("Are you sure about this change?")) {
			            //revertFunc();
			        	//var url = "CalendarJsonServletDemo?start="+event.start+"&end="+event.end+"&allday="+allDay;
					    //alert(url);
					    var dd=event.start;
					    //var d=new Date(dd);
					   // alert("updated....");
					   // alert(d);
					    var d = new Date(Date.parse(dd));
					    var dateString = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
					    //alert(dateString);
					    var url="updatecustomCalanderSec.html?start="+dateString+"&end="+event.end+"&allday="+allDay+"&calId="+event.id;
					$.post(
					        url, 
					        function(data){ //callback function retrieving the response sent by the controller/action
					            //event is the object that will actually be rendered to the calendar
					           // var event = jQuery.parseJSON(data.substr(1,data.length-2));
					            //actually rendering the new event to the calendar
					            //calendar.fullCalendar('renderEvent', event, false);
					            //calendar.fullCalendar('unselect');
					            //alert("Processed...");
					            if(data!=null){
					            	location.reload();
					            }
					        	
					        }
					    );
					//location.reload();
			        }else{
			        //alert("asqw");
			        	location.reload(); 
			        }

			    }
		});
		
	});

</script>
<style>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}

	#calendar {
		width: 900px;
		margin: 0 auto;
		}

</style>
</head>
<body>
<div id="Layer5" style="width:100%"> 
<s:hidden name="fileID" id ="fileID" value="" />     
<s:form id="surveyListForm" action="customCalanderSec.html?decorator=popup&popup=true&from=search" method="post" >
<c:set var="from" value="<%=request.getParameter("from") %>" />
<s:hidden name="from" value="<%=request.getParameter("from") %>" />
<c:set var="id1" value="<%=request.getParameter("id1") %>" />
<s:hidden name="id1" value="<%=request.getParameter("id1") %>" />


<%-- <c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:65px; height:25px"  
        onclick="document.forms['surveyListForm'].action='editMyEvent.html?decorator=popup&popup=true&from1=Survey';document.forms['surveyListForm'].submit();"  
        value="Add Event"/>   
</c:set> --%>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" key="button.search" onclick="return isDate();" /> 
	 <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set> 
  
<c:if test="${not empty id1}" >
		<div id="newmnav">
		  <ul>
		    <li><a href="editCustomerFile.html?id=<%=request.getParameter("id1") %> "><span>Customer File</span></a></li>
		    <li><a href="customerServiceOrders.html?id=<%=request.getParameter("id1") %> "><span>Service Orders</span></a></li>
		    <li><a href="customerRateOrders.html?id=<%=request.getParameter("id1") %>"><span>Rate Request</span></a></li>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Surveys<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="window.open('subModuleReports.html?id=<%=request.getParameter("id1") %>&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=Survey&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
		
		
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>


<div id="Layer1" style="width:93%;">

<table class="" cellspacing="1" cellpadding="0"
	border="0" width="100%">
	<tbody>
		<tr>
			<td>   
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
				
						<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:-20px;!margin-top:-5px;"><span></span></div>
<div class="center-content">
				
				<table class="table" width="90%" >
				<thead>
				<tr>
				<th><fmt:message key="customerFile.estimator"/></th>
				<th>From&nbsp;Date</th>
				<th>To&nbsp;Date</th>
				<th><fmt:message key="customerFile.originCity"/></th>
				<th><fmt:message key="customerFile.job"/></th>
				
				</tr></thead>	
						<tbody>
						<tr>
							<td>
							    <s:select name="consult" list="%{saleConsultant}" cssStyle="width:125px" cssClass="list-menu" headerKey="" headerValue="" />
							</td>
							<%--
							<td class="listwhitetext"><s:select cssClass="list-menu"  name="consult" list="%{sale}" headerKey="" headerValue=""/></td>
							--%>
							<td>
							    <s:textfield name="surveyFrom" cssClass="input-text" id="date1" cssStyle="width:75px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="isValidDate();"/><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
							<td>
							    <s:textfield name="surveyTo" cssClass="input-text" id="date2" cssStyle="width:75px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
							<td>
							    <s:textfield name="surveyCity" cssStyle="width:65px" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
							</td>
							<td>
							    <s:select name="surveyJob" cssStyle="width:165px" list="%{jobs}" cssClass="list-menu" headerKey="" headerValue=""/>
							</td>
							</tr>
							<tr>
							<td colspan="4"></td>
							<td style="border-left: hidden;">
							    <c:out value="${searchbuttons}" escapeXml="false" />   
							</td>
						</tr>
						</tbody>
					</table>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
				<s:set name="scheduledSurveys" value="scheduledSurveys" scope="request"/> 
				<div id="calendar" style="position:absolute"></div> 
		<%-- <div id="newmnav">
				  <ul>
				  	<c:if test="${not empty id1}" >
				  	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   	 <!--<li><a onclick="myEvents();"><span>Events</span></a></li>
				   	--></c:if>
				   	<c:if test="${empty id1}" >
				   	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   	</c:if>
				   	<li><a onclick="scheduleSurvey();"><span>Survey Map</span></a></li>
				  </ul>
		</div> --%><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>				
			</td>
			
		</tr>
	</tbody>
</table>

</div>
<s:hidden name="popup" value="true"/>
<%-- <authz:authorize ifAnyGranted="ROLE_SALE">
<c:out value="${buttons}" escapeXml="false" />   
</authz:authorize>--%>
<% if(userRole.equalsIgnoreCase("ROLE_SALE") || userRole.equalsIgnoreCase("ROLE_CONSULTANT") || userRole.equalsIgnoreCase("ROLE_COORD")  || userRole.equalsIgnoreCase("ROLE_ADMIN")){ %>
<c:out value="${buttons}" escapeXml="false" /> 
<%} %>  
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<!-- <div id="calendar" style="position:absolute"></div> -->

</s:form>
<c:if test="${hitFlag == 1}" >
	
		<c:redirect url="/searchSurveysList.html?consult=${consult}&surveyFrom=${surveyFrom}&surveyTo=${surveyTo}&surveyCity=${surveyCity}&surveyJob=${surveyJob}&decorator=popup&popup=true&from=search"/>
	</c:if>
	<script type="text/javascript">  
try{
  Form.focusFirstElement($("surveyListForm")); 
}catch(e){} 
  try{
  if(document.forms['surveyListForm'].elements['from'].value=='survey') { 
    autoPopulate_Date(this);
    autoPopulate_DateTo(this);
    copyFromDate();
   }
   }
   catch(e){}
 </script>
 
 <script type="text/javascript">
	setOnSelectBasedMethods(["isValidDate()"]);
	setCalendarFunctionality();
</script>
</div>
<!-- <div id='calendar'></div> -->
</body>
</html>
