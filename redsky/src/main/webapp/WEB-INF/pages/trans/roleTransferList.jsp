<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Role Transfer</title>
<meta name="heading" content="Role Transfer" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>

<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
function enableOrdisableRoleTransferButton(){
	var selObj1 = document.getElementById('transferFrom');
	var selObj2 = document.getElementById('roleTransfer');
	var selObj3 = document.getElementById('transferTo');
    var len1 = selObj1.options.length;
    var len2 = selObj2.options.length;
    var len3 = selObj3.options.length;	
    var i1 = 0;
    var chosen1 = ""
 	   for (i1 = 0; i1 < len1; i1++) {
		   if (selObj1.options[i1].selected) {
			   if(chosen1=="")
			   {
				   chosen1=selObj1.options[i1].value;
		   		}else{
	   		   		chosen1 = chosen1+","+selObj1.options[i1].value;
		   		}
	   		}
	   }
    var i2 = 0;
    var chosen2 = ""
 	   for (i2 = 0; i2 < len2; i2++) {
		   if (selObj2.options[i2].selected) {
			   if(chosen2=="")
			   {
				   chosen2=selObj2.options[i2].value;
		   		}else{
	   		   		chosen2 = chosen2+","+selObj2.options[i2].value;
		   		}
	   		}
	   }
    var i3 = 0;
    var chosen3 = ""
 	   for (i3 = 0; i3 < len3; i3++) {
		   if (selObj3.options[i3].selected) {
			   if(chosen3=="")
			   {
				   chosen3=selObj3.options[i3].value;
		   		}else{
	   		   		chosen3 = chosen3+","+selObj3.options[i3].value;
		   		}
	   		}
	   }	   
	if((chosen1!="")&&(chosen2!="")&&(chosen3!=""))
	{
		document.getElementById('roleTransferId').disabled=false;
	}else{
		document.getElementById('roleTransferId').disabled=true;
	}
} 
function GetSelectedItem() {
	var selObj = document.getElementById('transferFrom');
	 var status=document.getElementById('activeStatus').value;
     var len = selObj.options.length;
    var i = 0;
    var chosen = ""
 	   for (i = 0; i < len; i++) {
		   if (selObj.options[i].selected) {
			   if(chosen=="")
			   {
				   chosen=selObj.options[i].value;
		   		}else{
	   		   		chosen = chosen+","+selObj.options[i].value;
		   		}
	   		}
	   }
	  if(chosen != '')
	  { 
	   var url="pickAllRole.html?ajax=1&decorator=simple&popup=true&activeStatus="+status+"&userList="+chosen.trim();
	   http1.open("GET",url,true);	  
	   http1.onreadystatechange = handleHttpResponse;
	   http1.send(null);
	  }	   
}
function handleHttpResponse(){
    if (http1.readyState == 4)
    {
		 var results = http1.responseText
	        results = results.trim();
	         results=results.substring(0,parseInt(results.length)-1);
         res = results.split("~");
         targetElement = document.forms['roleTransferForm'].elements['roleTransfer'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++)
				{
				if(res[i] == ''){
				document.forms['roleTransferForm'].elements['roleTransfer'].options[i].text = '';
				document.forms['roleTransferForm'].elements['roleTransfer'].options[i].value = '';
				}else{				
				document.forms['roleTransferForm'].elements['roleTransfer'].options[i].text = res[i];
				document.forms['roleTransferForm'].elements['roleTransfer'].options[i].value = res[i];
				}
				}
			GetSelectedItem1();
    }
}     
function GetSelectedItem1() {	
	var selObj = document.getElementById('roleTransfer');
    var len = selObj.options.length;
    var i = 0;
    var chosen = ""
 	   for (i = 0; i < len; i++) {
		   if (selObj.options[i].selected) {
			   if(chosen=="")
			   {
				   chosen=selObj.options[i].value;
		   		}else{
	   		   		chosen = chosen+","+selObj.options[i].value;
		   		}
	   		}
	   }

	var selObj11 = document.getElementById('transferFrom');
	var len11 = selObj11.options.length;
	var status=document.getElementById('activeStatus').value;
	var i11 = 0;
    var chosen11 = ""
 	   for (i11 = 0; i11 < len11; i11++) {
		   if (selObj11.options[i11].selected) {
			   if(chosen11=="")
			   {
				   chosen11=selObj11.options[i11].value;
		   		}else{
	   		   		chosen11 = chosen11+","+selObj11.options[i11].value;
		   		}
	   		}
	   }

	       
	  if(chosen != '')
	  {
		var url="pickAllUser.html?ajax=1&decorator=simple&popup=true&activeStatus="+status+"&roleList="+encodeURI(chosen)+"&userList="+encodeURI(chosen11).trim();
	   http2.open("GET",url,true);	  
	   http2.onreadystatechange = handleHttpResponse1;
	   http2.send(null);
	  }	   
}
function handleHttpResponse1(){
    if (http2.readyState == 4)
    {
        var results = http2.responseText
	        results = results.trim();
	         results=results.substring(0,parseInt(results.length)-1);
	         
         res = results.split("~");
         targetElement = document.forms['roleTransferForm'].elements['transferTo'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++)
				{
				if(res[i] == ''){
				document.forms['roleTransferForm'].elements['transferTo'].options[i].text = '';
				document.forms['roleTransferForm'].elements['transferTo'].options[i].value = '';
				}else{				
				document.forms['roleTransferForm'].elements['transferTo'].options[i].text = res[i];
				document.forms['roleTransferForm'].elements['transferTo'].options[i].value = res[i];
				}
				}
    }
}     
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http1 = getHTTPObject();
var http2 = getHTTPObject();
var http21 = getHTTPObject();
</script>
</head>

<s:form name="roleTransferForm" id="roleTransferForm" action="updateUserWithRoles" method="post" validate="true">
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden  name="activeStatus" />
<div id="layer1" style="width:100%;">
<div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Role Transfer</span></a></li>
		  </ul>
		</div> 
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">	
	<tr><td height="5px">&nbsp;</td></tr>	
	<tr><td><s:checkbox id="activeStatus" name="activeStatus" onclick="checkValue(this);"/><b>Show Inactive Users</b></td></tr>              
	<tr><td height="5px">&nbsp;</td></tr>	
	<tr>
	<td align="center" class="listwhitetext" ><b>User To Transfer From:</b></td>
	<td height="5px">&nbsp;</td>
	<td align="center" class="listwhitetext" ><b>Role To Transfer:</b></td>
	<td height="5px">&nbsp;</td>
	<td align="center" class="listwhitetext" ><b>User To Transfer To:</b></td>
	</tr>	
	<tr>
	<td><s:select cssClass="list-menu"  id="transferFrom" name="transferFrom" list="%{totalUser}" size="2" onclick="GetSelectedItem();enableOrdisableRoleTransferButton();" cssStyle="width:230px; height:100px" /></td>
	<td height="5px">&nbsp;</td>
	<td><s:select cssClass="list-menu" id="roleTransfer" name="roleTransfer" list="%{userRoleListData}" size="2" onclick="GetSelectedItem1();enableOrdisableRoleTransferButton();" cssStyle="width:230px; height:100px" /></td>
	<td height="5px">&nbsp;</td>
	<td><s:select cssClass="list-menu" id="transferTo" name="transferTo" list="%{remainUser}" size="2" onclick="enableOrdisableRoleTransferButton();" cssStyle="width:230px; height:100px" /></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>	
		<td align="center" colspan="5"><s:submit cssClass="cssbutton" id="roleTransferId" align="top" value="Make Transfer" /></td>		
		
	</tr>
	<tr><td height="10"></td></tr>	 		
			

</table> 

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>    
</s:form>
     <script type="text/javascript">
     try{
    	 enableOrdisableRoleTransferButton();
    	document.getElementById('activeStatus').value="false";
     }
    	 catch(e){}
	
	
function checkValue(chk)
{
	document.getElementById('activeStatus').value=chk.checked;

	 var satusValue = chk.checked; 
	 var url="InactiveUserList.html?ajax=1&decorator=simple&popup=true&activeStatus="+encodeURI(satusValue);
     http21.open("GET", url, true);
     http21.onreadystatechange = handleHttpResponse21;
     http21.send(null);

	
	}
function handleHttpResponse21() { 
    if (http21.readyState == 4){
    	 var results = http21.responseText
    	  results = results.trim();
       //alert(results);
        //results=results.substring(0,parseInt(results.length)-1);
        results = results.replace('[','');
        results=results.replace(']','');
        results = results.split(",");
        targetElement = document.forms['roleTransferForm'].elements['transferFrom'];
			targetElement.length = results.length;
			for(i=0;i<results.length;i++)
				{
				if(results[i] == ''){
				document.forms['roleTransferForm'].elements['transferFrom'].options[i].text = '';
				document.forms['roleTransferForm'].elements['transferFrom'].options[i].value = '';
				}else{	 
				document.forms['roleTransferForm'].elements['transferFrom'].options[i].text = results[i].trim();
				document.forms['roleTransferForm'].elements['transferFrom'].options[i].value = results[i].trim();
				}
				}
      
}}
     </script>
     <script type="text/javascript">
   function getHTTPObject()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http21 = getHTTPObject();   
</script>  
