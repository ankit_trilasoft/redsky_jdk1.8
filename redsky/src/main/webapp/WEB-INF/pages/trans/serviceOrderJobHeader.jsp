<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
<table  cellspacing="1" cellpadding="0" border="0" width="100%">
   <tbody>
    <tr><td align="left" class="listwhitebox">
       <table class="detailTabLabel" border="0" width="" cellspacing="0" cellpadding="3">
         <tbody>                  
             <tr>
             <td align="right"><fmt:message key="billing.shipper"/></td>
             <td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" size="21"  cssClass="input-textUpper" readonly="true"/>
             <td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true"/></td>
             <td align="right"><fmt:message key="billing.originCountry"/></td>
             <td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="12" readonly="true"/></td>
             <td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="4" readonly="true"/></td>
             <td align="right"><fmt:message key="billing.Type"/></td>
             <td align="left"><s:textfield name="serviceOrder.job" cssClass="input-textUpper" size="4" readonly="true"/></td>
             <td align="right" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
             <td><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="26" readonly="true"/></td>
             <td align="right"><fmt:message key='customerFile.status'/></td>
           	 <td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" key="serviceOrder.status" size="5" readonly="true"/> </td>
             </tr>
             <tr>
             <td align="right"><fmt:message key="billing.jobNo"/></td>
             <td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="21" readonly="true"/></td>
             <td align="right"><fmt:message key="billing.registrationNo"/></td>
             <td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true"/></td>
             <td align="right"><fmt:message key="billing.destination"/></td>
             <td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="12" readonly="true"/></td>
             <td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="4" readonly="true"/></td>
             <c:if test="${serviceOrder.job!='RLO'}">
             <td align="right"><fmt:message key="billing.mode"/></td>             
             <td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="4" readonly="true"/></td>
             </c:if>
             <td align="right"><fmt:message key="billing.AccName"/></td>
             <td align="left" colspan="5"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="52" cssStyle="width: 277px;" readonly="true"/></td>
             </tr>
             <tr>
             <td align="left" height="5px"></td>
             </tr>
            </tbody>
     		</table>
    		</td>
    		</tr> 
           <tr>
        <td align="left" colspan="18" class="vertlinedata"></td>
       </tr>
   </tbody>
</table>
</div>
<div class="bottom-header" style="!margin-top:50px;margin-top:31px;"><span style="margin-left:50px; padding-top:5px">
	<tr>  <c:if test="${billing.cod}">
        <td width="100%" >
         <div style="position:absolute; width:95%; text-align:center; font-size:14px;padding-top:3px; font-family:Tahoma,Calibri,Verdana,Geneva,sans-serif; font-weight:bold; color:#fe0303 "> COD </div>
         <img id="cod1" src="${pageContext.request.contextPath}/images/cod-blue.png" width="95%" /></td>                              
      </c:if>
   </tr>
</span></div>

</div>
</div>

