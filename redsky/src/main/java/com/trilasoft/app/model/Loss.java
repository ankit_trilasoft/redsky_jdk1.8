package com.trilasoft.app.model;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.appfuse.model.BaseObject;   

import javax.persistence.Column;
import javax.persistence.Entity;   
import javax.persistence.GenerationType;   
import javax.persistence.Id;   
import javax.persistence.GeneratedValue;   
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder; 
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="loss")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class Loss extends BaseObject implements ListLinkData
{
	 private String corpID;
	 private Long id;
	 private Integer idNumber1;
	  private String sequenceNumber;
	  private String ship;
	  private String shipNumber;
	  private Long claimNumber;
	  private String lossNumber;
	  private String lossType;
	  private String damageByAction;
	  private String lossAction;
	  private BigDecimal paidCompensationCustomer;
	  private BigDecimal paidCompensation3Party; 
	  private String lossComment;
	  private String warehouse;
	  private Long ticket;
	  private BigDecimal ticket2;
	  private BigDecimal ticket3;
	  private String itemDescription;
	  private BigDecimal weightDamagedItem;
	  private String inventory;
	  private String chargeBackTo;
	  private BigDecimal chargeBackAmount;
	  private BigDecimal requestedAmount;
	  private String whoWorkCDE;
	  private String whoWorkName;
	  private String notesLoss;
	  private String chequeNumberCustmer;
	  private String chequeNumber3Party;
	  private String claimId;
	  private String jobNumber;
	  private Date paidCompensationCustomerDate;
	  private Date paidCompensation3PartyDate;
	  private String suffix;
	  private BigDecimal reserve;
	  private BigDecimal tk1pc;
	  private BigDecimal tk2pc;
	  private BigDecimal tk3pc;
	  private String createdBy;
	  private Date createdOn;
	  private String updatedBy;
	  private Date updatedOn;
	  private Date datePurchased;
	  //private String age;
	  private BigDecimal originalCost;
	  private Boolean cartonDamaged;
	// Added field based on claim integration
	  private String itmItemCode;
	  private String itmClmsItemSeqNbr;
	  private String itmDnlRsnCode;
	  private String itmAltItemDesc;
	  private String itrRespExcpCode;
	  private BigDecimal itrAssessdLiabilityAmt;
	  private BigDecimal itrTotalLiabilityAmt;
	  private BigInteger itrPmntId;
	  
	  private Claim claim;
	  private String originalCostCurrency;
	  private String requestedAmountCurrency;
	  private String paidCompensationCustomerCurrency;
	  private String paidCompensation3PartyCurrency;
	  private String chargeBackCurrency;
	  private String selfBlame;
	  private String idNumber;// Add for link and sync
	  
	//change request for payment settlement
	  private String chargeBack;
	  private String agentLiable;
	  private BigDecimal chargeBackAmountDest;
	  private String chargeBackCurrencyDest;
	  
	  private String integrationId;
		
		@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("id", id)
				.append("idNumber1", idNumber1)
				.append("sequenceNumber", sequenceNumber)
				.append("ship", ship)
				.append("shipNumber", shipNumber)
				.append("claimNumber", claimNumber)
				.append("lossNumber", lossNumber)
				.append("lossType", lossType)
				.append("damageByAction", damageByAction)
				.append("lossAction", lossAction)
				.append("paidCompensationCustomer", paidCompensationCustomer)
				.append("paidCompensation3Party", paidCompensation3Party)
				.append("lossComment", lossComment)
				.append("warehouse", warehouse)
				.append("ticket", ticket)
				.append("ticket2", ticket2)
				.append("ticket3", ticket3)
				.append("itemDescription", itemDescription)
				.append("weightDamagedItem", weightDamagedItem)
				.append("inventory", inventory)
				.append("chargeBack", chargeBack)
				.append("chargeBackTo", chargeBackTo)
				.append("chargeBackAmount", chargeBackAmount)
				.append("requestedAmount", requestedAmount)
				.append("whoWorkCDE", whoWorkCDE)
				.append("whoWorkName", whoWorkName)
				.append("notesLoss", notesLoss)
				.append("chequeNumberCustmer", chequeNumberCustmer)
				.append("chequeNumber3Party", chequeNumber3Party)
				.append("claimId", claimId)
				.append("jobNumber", jobNumber)
				.append("paidCompensationCustomerDate",
						paidCompensationCustomerDate)
				.append("paidCompensation3PartyDate",
						paidCompensation3PartyDate)
				.append("suffix", suffix)
				.append("reserve", reserve)
				.append("tk1pc", tk1pc)
				.append("tk2pc", tk2pc)
				.append("tk3pc", tk3pc)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("datePurchased", datePurchased)
				.append("originalCost", originalCost)
				.append("cartonDamaged", cartonDamaged)
				.append("itmItemCode", itmItemCode)
				.append("itmClmsItemSeqNbr", itmClmsItemSeqNbr)
				.append("itmDnlRsnCode", itmDnlRsnCode)
				.append("itmAltItemDesc", itmAltItemDesc)
				.append("itrRespExcpCode", itrRespExcpCode)
				.append("itrAssessdLiabilityAmt", itrAssessdLiabilityAmt)
				.append("itrTotalLiabilityAmt", itrTotalLiabilityAmt)
				.append("itrPmntId", itrPmntId)
				.append("originalCostCurrency", originalCostCurrency)
				.append("requestedAmountCurrency", requestedAmountCurrency)
				.append("paidCompensationCustomerCurrency",
						paidCompensationCustomerCurrency)
				.append("paidCompensation3PartyCurrency",
						paidCompensation3PartyCurrency)
				.append("chargeBackCurrency", chargeBackCurrency)
				.append("selfBlame", selfBlame).append("idNumber", idNumber)
				.append("agentLiable", agentLiable)
				.append("chargeBackAmountDest", chargeBackAmountDest)
				.append("chargeBackCurrencyDest", chargeBackCurrencyDest)
				.append("integrationId", integrationId)
				.toString();
	}
		@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Loss))
			return false;
		Loss castOther = (Loss) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(idNumber1, castOther.idNumber1)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(claimNumber, castOther.claimNumber)
				.append(lossNumber, castOther.lossNumber)
				.append(lossType, castOther.lossType)
				.append(damageByAction, castOther.damageByAction)
				.append(lossAction, castOther.lossAction)
				.append(paidCompensationCustomer,
						castOther.paidCompensationCustomer)
				.append(paidCompensation3Party,
						castOther.paidCompensation3Party)
				.append(lossComment, castOther.lossComment)
				.append(warehouse, castOther.warehouse)
				.append(ticket, castOther.ticket)
				.append(ticket2, castOther.ticket2)
				.append(ticket3, castOther.ticket3)
				.append(itemDescription, castOther.itemDescription)
				.append(weightDamagedItem, castOther.weightDamagedItem)
				.append(inventory, castOther.inventory)
				.append(chargeBack, castOther.chargeBack)
				.append(chargeBackTo, castOther.chargeBackTo)
				.append(chargeBackAmount, castOther.chargeBackAmount)
				.append(requestedAmount, castOther.requestedAmount)
				.append(whoWorkCDE, castOther.whoWorkCDE)
				.append(whoWorkName, castOther.whoWorkName)
				.append(notesLoss, castOther.notesLoss)
				.append(chequeNumberCustmer, castOther.chequeNumberCustmer)
				.append(chequeNumber3Party, castOther.chequeNumber3Party)
				.append(claimId, castOther.claimId)
				.append(jobNumber, castOther.jobNumber)
				.append(paidCompensationCustomerDate,
						castOther.paidCompensationCustomerDate)
				.append(paidCompensation3PartyDate,
						castOther.paidCompensation3PartyDate)
				.append(suffix, castOther.suffix)
				.append(reserve, castOther.reserve)
				.append(tk1pc, castOther.tk1pc)
				.append(tk2pc, castOther.tk2pc)
				.append(tk3pc, castOther.tk3pc)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(datePurchased, castOther.datePurchased)
				.append(originalCost, castOther.originalCost)
				.append(cartonDamaged, castOther.cartonDamaged)
				.append(itmItemCode, castOther.itmItemCode)
				.append(itmClmsItemSeqNbr, castOther.itmClmsItemSeqNbr)
				.append(itmDnlRsnCode, castOther.itmDnlRsnCode)
				.append(itmAltItemDesc, castOther.itmAltItemDesc)
				.append(itrRespExcpCode, castOther.itrRespExcpCode)
				.append(itrAssessdLiabilityAmt,
						castOther.itrAssessdLiabilityAmt)
				.append(itrTotalLiabilityAmt, castOther.itrTotalLiabilityAmt)
				.append(itrPmntId, castOther.itrPmntId)
				.append(originalCostCurrency, castOther.originalCostCurrency)
				.append(requestedAmountCurrency,
						castOther.requestedAmountCurrency)
				.append(paidCompensationCustomerCurrency,
						castOther.paidCompensationCustomerCurrency)
				.append(paidCompensation3PartyCurrency,
						castOther.paidCompensation3PartyCurrency)
				.append(chargeBackCurrency, castOther.chargeBackCurrency)
				.append(selfBlame, castOther.selfBlame)
				.append(idNumber, castOther.idNumber)
				.append(agentLiable, castOther.agentLiable)
				.append(chargeBackAmountDest, castOther.chargeBackAmountDest)
				.append(chargeBackCurrencyDest, castOther.chargeBackCurrencyDest).append(integrationId, castOther.integrationId).isEquals();
	}
		@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(idNumber1).append(sequenceNumber).append(ship)
				.append(shipNumber).append(claimNumber).append(lossNumber)
				.append(lossType).append(damageByAction).append(lossAction)
				.append(paidCompensationCustomer)
				.append(paidCompensation3Party).append(lossComment)
				.append(warehouse).append(ticket).append(ticket2)
				.append(ticket3).append(itemDescription)
				.append(weightDamagedItem).append(inventory).append(chargeBack)
				.append(chargeBackTo).append(chargeBackAmount)
				.append(requestedAmount).append(whoWorkCDE).append(whoWorkName)
				.append(notesLoss).append(chequeNumberCustmer)
				.append(chequeNumber3Party).append(claimId).append(jobNumber)
				.append(paidCompensationCustomerDate)
				.append(paidCompensation3PartyDate).append(suffix)
				.append(reserve).append(tk1pc).append(tk2pc).append(tk3pc)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).append(datePurchased).append(originalCost)
				.append(cartonDamaged).append(itmItemCode)
				.append(itmClmsItemSeqNbr).append(itmDnlRsnCode)
				.append(itmAltItemDesc).append(itrRespExcpCode)
				.append(itrAssessdLiabilityAmt).append(itrTotalLiabilityAmt)
				.append(itrPmntId).append(originalCostCurrency)
				.append(requestedAmountCurrency)
				.append(paidCompensationCustomerCurrency)
				.append(paidCompensation3PartyCurrency)
				.append(chargeBackCurrency).append(selfBlame).append(idNumber)
				.append(agentLiable).append(chargeBackAmountDest).append(chargeBackCurrencyDest).append(integrationId)
				.toHashCode();
	}
		@ManyToOne
		@JoinColumn(name="claimId", nullable=false, updatable=false,
		referencedColumnName="id")
		public Claim getClaim() {
		        return claim;
		    }
		    
			 public void setClaim(Claim claim) {
					this.claim = claim;
			} 
		@Column
		 public String getChargeBack() {
			return chargeBack;
		}
		public void setChargeBack(String chargeBack) {
		     this.chargeBack = chargeBack;
		}
		@Column(length=20)
		public BigDecimal getChargeBackAmount() {
			return chargeBackAmount;
		}
		public void setChargeBackAmount(BigDecimal chargeBackAmount) {
			this.chargeBackAmount = chargeBackAmount;
		}
		@Column(length=25)
		public String getChargeBackTo() {
			return chargeBackTo;
		}
		public void setChargeBackTo(String chargeBackTo) {
			this.chargeBackTo = chargeBackTo;
		}
		@Column(length=10)
		public String getChequeNumber3Party() {
			return chequeNumber3Party;
		}
		public void setChequeNumber3Party(String chequeNumber3Party) {
			this.chequeNumber3Party = chequeNumber3Party;
		}
		@Column(length=10)
		public String getChequeNumberCustmer() {
			return chequeNumberCustmer;
		}
		public void setChequeNumberCustmer(String chequeNumberCustmer) {
			this.chequeNumberCustmer = chequeNumberCustmer;
		}
		@Column(length=20, insertable=false, updatable=false)
		public String getClaimId() {
			return claimId;
		}
		public void setClaimId(String claimId) {
			this.claimId = claimId;
		}
		@Column(length=10)
		public Long getClaimNumber() {
			return claimNumber;
		}
		public void setClaimNumber(Long claimNumber) {
			this.claimNumber = claimNumber;
		}
		
		@Column(length=15)
		public String getCorpID() {
			return corpID;
		}
		public void setCorpID(String corpID) {
			this.corpID = corpID;
		}
		@Column(length=82)
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		@Column
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		@Column(length=4)
		public String getDamageByAction() {
			return damageByAction;
		}
		public void setDamageByAction(String damageByAction) {
			this.damageByAction = damageByAction;
		}
		
		@Id@GeneratedValue(strategy = GenerationType.AUTO)
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		
		public Integer getIdNumber1() {
			return idNumber1;
		}
		public void setIdNumber1(Integer idNumber1) {
			this.idNumber1 = idNumber1;
		}
		@Column(length=20)
		public String getInventory() {
			return inventory;
		}
		public void setInventory(String inventory) {
			this.inventory = inventory;
		}
		@Column(length=50)
		public String getItemDescription() {
			return itemDescription;
		}
		public void setItemDescription(String itemDescription) {
			this.itemDescription = itemDescription;
		}
		@Column(length=12)
		public String getJobNumber() {
			return jobNumber;
		}
		public void setJobNumber(String jobNumber) {
			this.jobNumber = jobNumber;
		}
		@Column(length=1)
		public String getLossAction() {
			return lossAction;
		}
		public void setLossAction(String lossAction) {
			this.lossAction = lossAction;
		}
		@Column
		public String getLossComment() {
			return lossComment;
		}
		public void setLossComment(String lossComment) {
			this.lossComment = lossComment;
		}
		@Column(length=10)
		public String getLossNumber() {
			return lossNumber;
		}
		public void setLossNumber(String lossNumber) {
			this.lossNumber = lossNumber;
		}
		@Column(length=4)
		public String getLossType() {
			return lossType;
		}
		public void setLossType(String lossType) {
			this.lossType = lossType;
		}
		public String getNotesLoss() {
			return notesLoss;
		}
		public void setNotesLoss(String notesLoss) {
			this.notesLoss = notesLoss;
		}
		@Column(length=20)
		public BigDecimal getPaidCompensation3Party() {
			return paidCompensation3Party;
		}
		public void setPaidCompensation3Party(BigDecimal paidCompensation3Party) {
			this.paidCompensation3Party = paidCompensation3Party;
		}
		@Column
		public Date getPaidCompensation3PartyDate() {
			return paidCompensation3PartyDate;
		}
		public void setPaidCompensation3PartyDate(Date paidCompensation3PartyDate) {
			this.paidCompensation3PartyDate = paidCompensation3PartyDate;
		}
		@Column(length=20)
		public BigDecimal getPaidCompensationCustomer() {
			return paidCompensationCustomer;
		}
		public void setPaidCompensationCustomer(BigDecimal paidCompensationCustomer) {
			this.paidCompensationCustomer = paidCompensationCustomer;
		}
		@Column(length=20)
		public Date getPaidCompensationCustomerDate() {
			return paidCompensationCustomerDate;
		}
		public void setPaidCompensationCustomerDate(Date paidCompensationCustomerDate) {
			this.paidCompensationCustomerDate = paidCompensationCustomerDate;
		}
		@Column(length=20)
		public BigDecimal getRequestedAmount() {
			return requestedAmount;
		}
		public void setRequestedAmount(BigDecimal requestedAmount) {
			this.requestedAmount = requestedAmount;
		}
		@Column(length=20)
		public BigDecimal getReserve() {
			return reserve;
		}
		public void setReserve(BigDecimal reserve) {
			this.reserve = reserve;
		}
		@Column(length=15)
		public String getSequenceNumber() {
			return sequenceNumber;
		}
		public void setSequenceNumber(String sequenceNumber) {
			this.sequenceNumber = sequenceNumber;
		}
		@Column(length=2)
		public String getShip() {
			return ship;
		}
		public void setShip(String ship) {
			this.ship = ship;
		}
		@Column(length=15)
		public String getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}
		@Column(length=2)
		public String getSuffix() {
			return suffix;
		}
		public void setSuffix(String suffix) {
			this.suffix = suffix;
		}
	
		public Long getTicket() {
			return ticket;
		}
		public void setTicket(Long ticket) {
			this.ticket = ticket;
		}
		@Column(length=20)
		public BigDecimal getTicket2() {
			return ticket2;
		}
		public void setTicket2(BigDecimal ticket2) {
			this.ticket2 = ticket2;
		}
		@Column(length=20)
		public BigDecimal getTicket3() {
			return ticket3;
		}
		public void setTicket3(BigDecimal ticket3) {
			this.ticket3 = ticket3;
		}
		@Column(length=20)
		public BigDecimal getTk1pc() {
			return tk1pc;
		}
		public void setTk1pc(BigDecimal tk1pc) {
			this.tk1pc = tk1pc;
		}
		@Column(length=20)
		public BigDecimal getTk2pc() {
			return tk2pc;
		}
		public void setTk2pc(BigDecimal tk2pc) {
			this.tk2pc = tk2pc;
		}
		@Column(length=20)
		public BigDecimal getTk3pc() {
			return tk3pc;
		}
		public void setTk3pc(BigDecimal tk3pc) {
			this.tk3pc = tk3pc;
		}
		@Column(length=82)
		public String getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}
		@Column
		public Date getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}
		@Column(length=3)
		public String getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(String warehouse) {
			this.warehouse = warehouse;
		}
		@Column(length=20)
		public BigDecimal getWeightDamagedItem() {
			return weightDamagedItem;
		}
		public void setWeightDamagedItem(BigDecimal weightDamagedItem) {
			this.weightDamagedItem = weightDamagedItem;
		}
		@Column(length=8)
		public String getWhoWorkCDE() {
			return whoWorkCDE;
		}
		public void setWhoWorkCDE(String whoWorkCDE) {
			this.whoWorkCDE = whoWorkCDE;
		}
		@Column(length=65)
		public String getWhoWorkName() {
			return whoWorkName;
		}
		public void setWhoWorkName(String whoWorkName) {
			this.whoWorkName = whoWorkName;
		}
		
		@Transient
	    public String getListCode() {       
	        return whoWorkCDE;  //return the code that needs to be set on the parent page
	    }

	    @Transient
	    public String getListDescription() {
	        return this.whoWorkName; //return the description that needs to be set on the parent page
	    } 
		
		@Transient
		public String getListSecondDescription() {
			return null;
		}
		
		@Transient
		public String getListThirdDescription() {
			return null;
		}
		
		@Transient
		public String getListFourthDescription() {
			return null;
		}
		
		@Transient
		public String getListFifthDescription() {
			return null;
		}
		
		@Transient
		public String getListSixthDescription() {
			return null;
		}
		@Transient
		public String getListSeventhDescription() {
			// TODO Auto-generated method stub
			return null;
		}
		@Transient
		public String getListEigthDescription() {
			// TODO Auto-generated method stub
			return null;
		}
		@Transient
		public String getListNinthDescription() {
			// TODO Auto-generated method stub
			return null;
		}
		@Transient
		public String getListTenthDescription() {
			// TODO Auto-generated method stub
			return null;
		}
		/**
		 * @return the age
		 *//*
		@Column(length=40)
		public String getAge() {
			return age;
		}
		*//**
		 * @param age the age to set
		 *//*
		public void setAge(String age) {
			this.age = age;
		}*/
		/**
		 * @return the cartonDamaged
		 */
		@Column
		public Boolean getCartonDamaged() {
			return cartonDamaged;
		}
		/**
		 * @param cartonDamaged the cartonDamaged to set
		 */
		public void setCartonDamaged(Boolean cartonDamaged) {
			this.cartonDamaged = cartonDamaged;
		}
		/**
		 * @return the datePurchased
		 */
		@Column
		public Date getDatePurchased() {
			return datePurchased;
		}
		/**
		 * @param datePurchased the datePurchased to set
		 */
		public void setDatePurchased(Date datePurchased) {
			this.datePurchased = datePurchased;
		}
		/**
		 * @return the originalCost
		 */
		@Column(length=20)
		public BigDecimal getOriginalCost() {
			return originalCost;
		}
		/**
		 * @param originalCost the originalCost to set
		 */
		public void setOriginalCost(BigDecimal originalCost) {
			this.originalCost = originalCost;
		}
			@Column
		 	public String getOriginalCostCurrency() {
				return originalCostCurrency;
			}
			public void setOriginalCostCurrency(String originalCostCurrency) {
				this.originalCostCurrency = originalCostCurrency;
			}
			@Column
			public String getRequestedAmountCurrency() {
				return requestedAmountCurrency;
			}
			public void setRequestedAmountCurrency(String requestedAmountCurrency) {
				this.requestedAmountCurrency = requestedAmountCurrency;
			}
			@Column
			public String getPaidCompensationCustomerCurrency() {
				return paidCompensationCustomerCurrency;
			}
			public void setPaidCompensationCustomerCurrency(
					String paidCompensationCustomerCurrency) {
				this.paidCompensationCustomerCurrency = paidCompensationCustomerCurrency;
			}
			@Column
			public String getPaidCompensation3PartyCurrency() {
				return paidCompensation3PartyCurrency;
			}
			public void setPaidCompensation3PartyCurrency(
					String paidCompensation3PartyCurrency) {
				this.paidCompensation3PartyCurrency = paidCompensation3PartyCurrency;
			}
			@Column
			public String getChargeBackCurrency() {
				return chargeBackCurrency;
			}
			public void setChargeBackCurrency(String chargeBackCurrency) {
				this.chargeBackCurrency = chargeBackCurrency;
			}
			public String getSelfBlame() {
				return selfBlame;
			}
			public void setSelfBlame(String selfBlame) {
				this.selfBlame = selfBlame;
			}
			@Column
			public String getIdNumber() {
				return idNumber;
			}
			public void setIdNumber(String idNumber) {
				this.idNumber = idNumber;
			}
			public String getItmItemCode() {
				return itmItemCode;
			}
			public void setItmItemCode(String itmItemCode) {
				this.itmItemCode = itmItemCode;
			}
			public String getItmClmsItemSeqNbr() {
				return itmClmsItemSeqNbr;
			}
			public void setItmClmsItemSeqNbr(String itmClmsItemSeqNbr) {
				this.itmClmsItemSeqNbr = itmClmsItemSeqNbr;
			}
			public String getItmDnlRsnCode() {
				return itmDnlRsnCode;
			}
			public void setItmDnlRsnCode(String itmDnlRsnCode) {
				this.itmDnlRsnCode = itmDnlRsnCode;
			}
			public String getItmAltItemDesc() {
				return itmAltItemDesc;
			}
			public void setItmAltItemDesc(String itmAltItemDesc) {
				this.itmAltItemDesc = itmAltItemDesc;
			}
			public String getItrRespExcpCode() {
				return itrRespExcpCode;
			}
			public void setItrRespExcpCode(String itrRespExcpCode) {
				this.itrRespExcpCode = itrRespExcpCode;
			}
			public BigDecimal getItrAssessdLiabilityAmt() {
				return itrAssessdLiabilityAmt;
			}
			public void setItrAssessdLiabilityAmt(BigDecimal itrAssessdLiabilityAmt) {
				this.itrAssessdLiabilityAmt = itrAssessdLiabilityAmt;
			}
			public BigDecimal getItrTotalLiabilityAmt() {
				return itrTotalLiabilityAmt;
			}
			public void setItrTotalLiabilityAmt(BigDecimal itrTotalLiabilityAmt) {
				this.itrTotalLiabilityAmt = itrTotalLiabilityAmt;
			}
			public BigInteger getItrPmntId() {
				return itrPmntId;
			}
			public void setItrPmntId(BigInteger itrPmntId) {
				this.itrPmntId = itrPmntId;
			}
			/**
			 * @return the chargeBackAmountDest
			 */
			@Column
			public BigDecimal getChargeBackAmountDest() {
				return chargeBackAmountDest;
			}
			/**
			 * @param chargeBackAmountDest the chargeBackAmountDest to set
			 */
			public void setChargeBackAmountDest(BigDecimal chargeBackAmountDest) {
				this.chargeBackAmountDest = chargeBackAmountDest;
			}
			/**
			 * @return the chargeBackCurrencyDest
			 */
			@Column
			public String getChargeBackCurrencyDest() {
				return chargeBackCurrencyDest;
			}
			/**
			 * @param chargeBackCurrencyDest the chargeBackCurrencyDest to set
			 */
			public void setChargeBackCurrencyDest(String chargeBackCurrencyDest) {
				this.chargeBackCurrencyDest = chargeBackCurrencyDest;
			}
			/**
			 * @return the agentLiable
			 */
			@Column
			public String getAgentLiable() {
				return agentLiable;
			}
			/**
			 * @param agentLiable the agentLiable to set
			 */
			public void setAgentLiable(String agentLiable) {
				this.agentLiable = agentLiable;
			}
			/**
			 * @return the integrationId
			 */
			@Column
			public String getIntegrationId() {
				return integrationId;
			}
			/**
			 * @param integrationId the integrationId to set
			 */
			public void setIntegrationId(String integrationId) {
				this.integrationId = integrationId;
			}
	
}
