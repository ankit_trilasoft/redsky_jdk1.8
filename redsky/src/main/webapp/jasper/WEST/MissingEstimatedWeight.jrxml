<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MissingEstimatedWeight" pageWidth="792" pageHeight="612" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="756" leftMargin="18" rightMargin="18" topMargin="18" bottomMargin="18" uuid="12da1aa6-9567-497c-b195-25529aecddb0">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="begindate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="enddate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	'MissingEstimatedWeight-R-PDF-WEST.jrxml',
	serviceorder.shipNumber AS serviceorder_shipNumber,
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate,
	if(serviceorder.companyDivision is null || serviceorder.companyDivision='','',serviceorder.companyDivision) AS serviceorder_companyDivision,
   	serviceorder.job AS serviceorder_job,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=''),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName)))AS Shipper,
	DATE_FORMAT(workticket.date1,'%d-%b-%y') AS workticket_date1,
	workticket.shipNumber AS workticket_shipNumber,
	workticket.service AS workticket_service,
	workticket.ticket AS workticket_ticket,
	workticket.warehouse AS workticket_warehouse,
	serviceorder.coordinator AS serviceorder_coordinator,
	DATE_FORMAT(customerfile.survey,'%d-%b-%y') AS customerfile_survey,
	customerfile.estimator AS customerfile_estimator
FROM workticket workticket
	INNER JOIN serviceorder serviceorder ON workticket.serviceOrderId = serviceorder.id and serviceorder.corpID = $P{Corporate ID}
	INNER JOIN customerfile customerfile ON serviceorder.customerFileId = customerfile.id and customerfile.corpID = $P{Corporate ID}
WHERE workticket.corpID = $P{Corporate ID}
	AND workticket.targetActual <> 'C'
	AND (workticket.estimatedWeight = 0 or workticket.estimatedWeight IS null)
AND (
       (workticket.date1 between $P{begindate}  AND  $P{enddate})
    OR (workticket.date2 between $P{begindate}  AND $P{enddate})
    OR (workticket.date1 < $P{begindate} AND workticket.date2 > $P{enddate})
)
order by serviceorder.companyDivision]]>
	</queryString>
	<field name="MissingEstimatedWeight-R-PDF-WEST.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="workticket_date1" class="java.lang.String"/>
	<field name="workticket_shipNumber" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="customerfile_survey" class="java.lang.String"/>
	<field name="customerfile_estimator" class="java.lang.String"/>
	<sortField name="workticket_date1"/>
	<sortField name="Shipper"/>
	<sortField name="serviceorder_shipNumber"/>
	<variable name="tktcount" class="java.lang.Integer" resetType="Group" resetGroup="date1" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{workticket_ticket}]]></variableExpression>
	</variable>
	<group name="date1">
		<groupExpression><![CDATA[$F{workticket_date1}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="27" splitType="Stretch">
				<textField pattern="#,##0" isBlankWhenNull="true">
					<reportElement key="textField" x="249" y="8" width="64" height="18" forecolor="#0000CC" uuid="2b99dd6e-e926-4dba-9ae0-cf0f18dedf3c"/>
					<textElement>
						<font isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{tktcount}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-22" x="33" y="8" width="148" height="18" forecolor="#0000CC" uuid="207c4d7b-af6c-4d90-923f-9186d7ecdcfc"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<text><![CDATA[Number of tickets for ]]></text>
				</staticText>
				<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
					<reportElement key="textField-4" x="184" y="8" width="65" height="18" forecolor="#0000CC" uuid="a52c3d24-4556-451d-a594-8f3375942756"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{workticket_date1}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement key="rectangle-1" x="0" y="7" width="756" height="0" uuid="aec0e0c8-850c-48fa-8e87-6146e6f6aa31"/>
				</rectangle>
				<line>
					<reportElement key="line-2" x="0" y="4" width="756" height="1" uuid="1db3b401-65a2-47da-8a66-50dbbfd40337"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="20" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="200" height="20" uuid="865fc881-1a5a-4d0c-86dc-b63d4c8194bc"/>
					<textElement>
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-24" x="0" y="0" width="102" height="20" uuid="4b70539f-3534-430f-bee9-843d8574359c"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="57" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="185" y="0" width="412" height="23" forecolor="#663300" uuid="de1df2da-3ec4-4eea-8775-120a05429b6e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="18" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[WorkTicket Missing Estimated Weight]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-2" x="464" y="23" width="133" height="21" uuid="8527d5be-85b7-45e4-961b-c091dec0bf00"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MMM-yy").format($P{enddate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-3" x="324" y="23" width="98" height="21" uuid="e3cb14fc-b1de-43a0-b7d5-3ea0d9eb16f2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MMM-yy").format($P{begindate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-19" x="428" y="23" width="36" height="21" uuid="3a597f17-ac4e-4e8d-9911-4d28f68ad33e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<text><![CDATA[To]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" x="197" y="23" width="147" height="21" uuid="819ec63e-0362-4525-9fcc-f9f86b41bfeb"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<text><![CDATA[WorkTicket Date  ]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-4" x="1" y="2" width="92" height="16" uuid="a0d6fa0c-bc41-49e9-acf1-a0abdd45f578"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="539" y="2" width="58" height="16" uuid="6fc16baa-3876-4bbf-8019-db154cfb4c1e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Service]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="268" y="2" width="52" height="14" uuid="94afcf3d-0bea-4343-88b0-62dc6b83dafa"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[S/O #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="384" y="2" width="71" height="16" uuid="c066929e-a097-4a6d-9443-8553d677d4bc"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Ticket #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="451" y="2" width="43" height="16" uuid="755d0c86-ac1b-49a7-8981-e03285431a6f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[WH]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" x="497" y="2" width="34" height="16" uuid="d8587f48-1d74-42c0-89c7-b6c3dffd4ae1"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Job]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="611" y="2" width="64" height="16" uuid="ac73d83a-af06-4bcf-ba12-6b36ff4e073f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Coordinator]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" x="675" y="2" width="81" height="16" uuid="f8e37a44-3c35-46a7-8370-576ab6f72082"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Sales Person]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" x="188" y="2" width="69" height="15" uuid="0f96771a-43ba-43cf-9fdf-bafc4b4f0f63"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Survey Date]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="19" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="539" y="0" width="58" height="18" uuid="fb0e9dbb-e9b5-45a2-80d2-3dc596d942bf"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_service}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="267" y="0" width="102" height="18" uuid="a5097bd9-682a-420c-a15a-2c21203ac4f0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="384" y="0" width="61" height="18" uuid="d179da36-f70a-403f-911c-5b7c38aba910"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_ticket}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="451" y="0" width="27" height="18" uuid="67148900-c783-4216-8fb3-2c34aa52c468"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_warehouse}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="0" width="171" height="18" uuid="fc42c495-f66e-46b2-9381-d22b06a16308"/>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="497" y="0" width="33" height="18" uuid="c1fcda07-afc0-420a-abc3-3a45ae43e987"/>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="193" y="0" width="75" height="18" uuid="601cebc0-6873-4032-ada1-68fe347dde10"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_survey}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="674" y="0" width="81" height="18" uuid="57b7f20f-be72-4374-9e1a-5a4d9558abbd"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_estimator}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="596" y="0" width="78" height="18" uuid="5d66547f-d61c-443d-bef2-43ac13188fa2"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_coordinator}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="18" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="602" y="4" width="115" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="82e4864c-af0f-41ba-a66f-44e4844a6fb5"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="720" y="4" width="36" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="fddf779c-0a3f-48a2-8fee-ed49f1ad1d31"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="4" width="209" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="eebf8b24-7e97-4e1b-8e6b-112925a1382b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-20" x="266" y="4" width="224" height="14" uuid="116aecda-4fad-403d-b374-9c482b19378f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="false" isItalic="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[MissingEstimatedWeight.jrxml]]></text>
			</staticText>
			<line>
				<reportElement key="line-3" x="0" y="1" width="754" height="1" uuid="6e392581-c2c1-4973-8b9f-f13787c52202"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
