<%@ include file="/common/taglibs.jsp"%>
<head>
	<title>Inventory&nbsp;Search</title>
	<meta name="heading" content="Inventory&nbsp;Search" />
	<STYLE type=text/css>
	.InvHead td{
	background: url("images/bg_listheader.png") repeat-x scroll 0 0 #BCD2EF;
    border-color: #3DAFCB;
    border-style: solid;
    border-width: 1px;
    color: #15428B;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;
    height: 25px;
    padding: 2px 3px 3px 5px;
    text-decoration: none;
    }
    .close_btn
    {
    
    z-index:1000;
    float:right;
    }
   
    .lightbox{margin-top:75px}

#overlayMarket {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
	</style>

	<link href="styles/lightbox.css" rel="stylesheet"  media="screen"/>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="scripts/lightbox-2.6.min.js"></script>
	<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	
	
	

	<script type="text/javascript">
function syncWithMobileMover(){
	var overlay = document.getElementById("overlay");
	overlay.style.visibility = "";
	try{
		document.getElementById("layerLoading").style.display ="none";
	}catch(e){}
	overlay.innerHTML = '<div><table cellspacing="0" cellpadding="0" border="0" width="100%" ><tr><td align="center"><table cellspacing="0" cellpadding="3" align="center"><tr><td height="200px"></td></tr><tr><td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="400px"><font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please Wait........ Syncing with Mobile Mover Sync Server</font></td></tr><tr><td  style="background-color:#DFDFDF;"  class="rounded-bottom" width="400px" align="center" height="35px"><iframe style="margin-left:126px; height:34px" id="spinner" src="${pageContext.request.contextPath}/images/ajax-loader-hr.gif"  scrolling="no" frameborder="0"></iframe></td></tr></table></td></tr></table></div>';
		var cid = <%= request.getParameter("cid")%> ;
		var url = "syncSoWithMobileMover.html?cid="+cid;
		$("#spinner").attr("src", "${pageContext.request.contextPath}/images/ajax-loader-hr.gif");
		//window.location.href = url;
		window.location.assign(url);
		$("#spinner").attr("src", "${pageContext.request.contextPath}/images/ajax-loader-hr.gif");
	}
	function mylightbox(target){
		var overlay = document.getElementById("overlay");
		overlay.style.display = "block";
		overlay.style.opacity = "0.9";
		overlay.style.background = "#000000";		
		overlay.style.visibility = "visible";
	try{
		document.getElementById("layerLoading").style.display ="none";
	}catch(e){}

		overlay.innerHTML = '<div class="lb-closeContainer close_btn" ><a href="javascript:;" onclick="closelight()" class="lb-close"><img src="images/close.png" onclick="closelight()" /></a></div><div class="lightbox"><img onclick="closelight()" src="UserImage?location='+target+'" style="width:300px" /></div>';
	} 
	function closelight(){
		var overlay = document.getElementById("overlay");
		overlay.style.display = "none";
	}
	
	function showHideMyDiv(targetDiv){
		
		$("div[id='"+targetDiv+"']").toggle();
		var shipno=targetDiv;
		shipno = shipno.replace('Pre_','');

		/* $.get("syncSoWithMobileMover.html?ajax=1&decorator=simple&popup=true", 
				{ shipno: shipno},
				function(data){
				}); */

				$.ajax({
					type: "GET",
					url: "inventoryPreCondition.html?ajax=1&decorator=simple&popup=true",
					beforeSend: function(){
					
					document.getElementById("overlay").style.visibility = "visible";

					},
				    data: { shipno: shipno}			
					})
					.done(function( msg ) {
					//alert( "Data Saved: " + msg );
					$("div[id='"+targetDiv+"'] #preResult").html(msg);
					document.getElementById("overlay").style.visibility = "hidden";
					});
		
		/*$.get("inventoryPreCondition.html?ajax=1&decorator=simple&popup=true", 
				{ shipno: shipno},
				function(data){
					$("div[id='"+targetDiv+"'] #preResult").html(data);
				}); */

		/*$.get("inventoryRoomCondition.html?ajax=1&decorator=simple&popup=true", 
				{ shipno: shipno},
				function(data){
					$("div[id='"+targetDiv+"'] #roomResult").html(data);
				}); */
		 
		/*$.get("ticketInventoryData.html?ajax=1&decorator=simple&popup=true", 
				{ shipno: shipno},
				function(data){
					 
				//	$("div[id='"+targetDiv+"'] div table div[id='inventory_"+targetDiv+"']").html(data);
					 
					
				}); */
				
		$.ajax({
			type: "GET",
			url: "ticketInventoryData.html?ajax=1&decorator=simple&popup=true",
			beforeSend: function(){
			
			document.getElementById("overlay").style.visibility = "visible";

			},
		    data: { shipno: shipno}			
			})
			.done(function( msg ) {
			//alert( "Data Saved: " + msg );
			$("div[id='"+targetDiv+"'] div table div[id='inventory_"+targetDiv+"']").html(msg);
			document.getElementById("overlay").style.visibility = "hidden";
			});
		
		
	}

	
	window.onload=function(){
		<c:forEach var="shipNumber" items="${resultSOList }">
			var targetDiv ="${shipNumber}";
			document.getElementById(targetDiv).style.display = "none";
		</c:forEach>
	};
	
	function imageList(locId,position,fieldName){
		var url="toolTipInvImageList.html?id="+locId+"&fieldName="+fieldName+"&decorator=simple&popup=true";
		ajax_SoTooltip(url,position);	
	}
	
</script>
	
</head>
<div id="Layer5" style="width:100%">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<c:if test="${Quote!='y'}">
<s:hidden id="forQuotation" name="forQuotation" value=""/>
</c:if>
<c:if test="${Quote=='y'}">
<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
</c:if>	 
<s:hidden name="fileID"  id= "fileID" value="%{customerFile.id}"/>  
<s:hidden name="ppType" id ="ppType" value="" />
 <c:set var="idOfTasks" value="${customerFile.id}" scope="session"/>
  <c:set var="quantitysum" value="0" />
  <c:set var="volumesum" value="0.00" />
  <c:set var="totalsum" value="0.00" />
  <c:set var="tableName" value="customerfile" scope="session"/>
<s:hidden name="SOflag" value="${SOflag}"/>
<c:set var="ppType" value=""/>
<s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
		<div id="newmnav">		  
		  <ul>
		  <c:if test="${Quote!='y'}">
		    <li><a href="editCustomerFile.html?id=<%=request.getParameter("cid") %>" ><span>Customer File</span></a></li>
			<li><a href="customerServiceOrders.html?id=<%=request.getParameter("cid") %> " ><span>Service Orders</span></a></li>
			<li ><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Inventory Details</span></a></li>
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li>
			<li><a onclick="window.open('subModuleReports.html?id=<%=request.getParameter("cid")%>&custID=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=Rate Request&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>			
		   </c:if>
		   <c:if test="${Quote=='y'}">
		    <li ><a href="QuotationFileForm.html?from=list&id=<%=request.getParameter("cid") %>"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a href="quotationServiceOrders.html?id=<%=request.getParameter("cid") %>&forQuotation=QC"><span>Quotes</span></a></li>		    
		    <li ><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Inventory Details</span></a></li>
		    <li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quotation&reportSubModule=quotation&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
            <li><a onclik="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>             
		   </c:if>
		</ul>
		</div><div class="spn">&nbsp;</div> 
			<div style="padding-bottom:0px;"></div>		
	<div id="content" align="center" >
  <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:100%">
		<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="5" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" onfocus="onLoad();"/></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="25" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="22" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.originCountryCode" required="true" size="5" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="25" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox"></td>
						<td align="right" class="listwhitebox"></td>
						<td align="right" class="listwhitebox"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>
	</div>
	
<div class="bottom-header"><span></span></div>


</div>
</div> </div>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>


<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round-top" style="margin-bottom:10px;">
    <div class="top" style="!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%;margin-top:2px;" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th><fmt:message key="serviceOrder.shipNumber"/></th>
			<th>Item#</th>
			<th>Item Description</th>
			<th>Carton Content</th>
			<th>Room</th>
			<th>Damaged</th>
			<th></th>
		</tr>
	</thead>	
	<tbody>
			<tr>			
				<td width=""><s:select name="shipnumber" list="%{soList}" cssClass="list-menu"  headerKey="" headerValue="" /></td>
				<td> <s:textfield name="itemNumber" size="15"  cssClass="input-text"/></td>
				<td> <s:textfield name="itemDesc" size="25"  cssClass="input-text"/></td>
				<td> <s:textfield name="cartonContent" size="15"  cssClass="input-text"/></td>
				<td> <s:textfield name="room" size="25"  cssClass="input-text"/></td>	
				<td width=""><s:select name="damaged" list="{'Y','N'}" cssClass="list-menu"  headerKey="" headerValue="" cssStyle="width: 60px;"/></td>
				<td style="border:none;vertical-align:bottom;text-align:right;">
					<c:out value="${searchbuttons}" escapeXml="false" />
				</td>
			</tr>			
	</tbody>
</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div align="left" style="padding-left:27px;">
	<%-- <input type="button" name="mmSync" value="Sync With Mobile Mover" class="cssbutton1" style="width:180px;margin-bottom:2px;" onclick="syncWithMobileMover()"/>--%>
	<img  title="Sync With Mobile Mover" src="${pageContext.request.contextPath}/images/sync-mobile.png" border="0" alt="Sync With Mobile Mover" style="vertical-align: middle;" onclick="syncWithMobileMover()"/>
	<c:if test = "${ not empty  customerFile.lastRunMMTime  }">
			<b>Last Synched :<fmt:timeZone value="${sessionTimeZone}" >
					<fmt:formatDate  pattern="${displayDateTimeFormat}" value="${customerFile.lastRunMMTime}"  />
				</fmt:timeZone>
			 </b>
	</c:if>
		<c:if test = "${ empty  customerFile.lastRunMMTime  }">
			<b>Never synched. Please click to get latest information.</b>
		</c:if>
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin: 0px;">
	<c:forEach var="shipNumber" items="${resultSOList }">
		<tr>
			<td class="listwhitetext" height="10" align="left" style="margin: 0px;">
				<div style="margin: 0px" onClick="showHideMyDiv('${shipNumber }')">
				<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin: 0px;">
					<tbody>
						<tr>
							<td class="headtab_left"> </td>
							<td class="headtab_center"> ${shipNumber }</td>
							<td class="headtab_bg" width="28" valign="top"></td>
							<td class="headtab_bg_special">  </td>
							<td class="headtab_right"> </td>
						</tr>
					</tbody>
				</table>
				</div>
				<div id="${shipNumber }">
				<div width="100%">
					<table width="98%" cellspacing="0" cellpadding="0" align="right" border="0" style="margin: 0px;">
						<tr>
							<td class="listwhitetext" height="10" align="left" style="margin: 0px;">
								<div style="margin: 0px" onClick="showHideMyDiv('Pre_${shipNumber }')">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin: 0px;">
										<tbody>
											<tr>
												<td class="headtab_left"> </td>
												<td class="headtab_center">Details at Origin and Destination Locations</td>
												<td class="headtab_bg" width="28" valign="top"></td>
												<td class="headtab_bg_special">  </td>
												<td class="headtab_right"> </td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="Pre_${shipNumber }"  width="95%"  align="right"   >								
								<div id="preResult">
								<div id="otabs" style="margin-top:-30px;">
		  							<ul>
		    							<li><a class="current"><span> Location and Room details</span></a></li>
		  							</ul>
								</div>
								<div class="spn"></div>
									<display:table class="table" requestURI=""  pagesize="10" style="width:98%;margin-left:10px;" >
 										<display:column title="Ticket #" style="width:100px;" />
										<display:column title="Location " style="width:100px;" />
										<display:column title="Address" />
										<display:column title="City , State" />
										<display:column title="Country" />
										<display:column title="Zip" />
										<display:column title="Rooms" />
										<display:column title="Floors" />
										<display:column title="Damaged" />
										<display:column title="Comments" />
									</display:table> 
								</div>
								
								<%-- <div id="roomResult">
								<div id="otabs" style="margin-top:-30px;">
		  							<ul>
		    							<li><a class="current"><span> Room Details</span></a></li>
		  							</ul>
								</div>
								<div class="spn"></div>
									<display:table class="table" requestURI="" export="true"  pagesize="10" style="width:98%;margin-left:10px;" >  
										<display:column title="Ticket #"></display:column>
										<display:column title="Location"></display:column>
										<display:column title="Rooms"></display:column>
										<display:column title="Floors"></display:column>
										<display:column title="Damaged"></display:column>
										<display:column title="Comments"></display:column>
										<display:column title="Photos"></display:column>
									</display:table>
								</div>--%>
							</div>
							</td>
						</tr>
					</table> 
				</div>
				
				<div >
					<table width="98%" cellspacing="0" cellpadding="0" border="0" align="right" style="margin: 0px;">
						<tr>
							<td class="listwhitetext" height="10" align="left" style="margin: 0px;">
								<div style="margin: 0px" onClick="showHideMyDiv('inventory_${shipNumber }')">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin: 0px;">
										<tbody>
											<tr>
												<td class="headtab_left"> </td>
												<td class="headtab_center">Inventory Details</td>
												<td class="headtab_bg" width="28" valign="top"></td>
												<td class="headtab_bg_special">  </td>
												<td class="headtab_right"> </td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="inventory_${shipNumber }"  width="95%"  align="right">
									<table name="inventroDetailTab" width="98%" border="0" cellspacing="0" cellpadding="0">
										<tr class="InvHead">
											<td rowspan="2" style="text-align:center">Ticket #</td>
											<td rowspan="2" style="text-align:center">Location</td>
											<td rowspan="2" style="text-align:center;width:10%;">Item # </td>
											<td rowspan="2" style="text-align:center;width:3%;">Quantity </td>
											<td rowspan="2" style="text-align:center">Item Description</td>
											<td rowspan="2" style="text-align:center">Packing Code</td>
											<td rowspan="2" style="text-align:center">Carton Content</td>
											<td style="text-align: center; width: 28%;" colspan="3">Origin</td>
											<td style="text-align: center; width: 28%;" colspan="1">Destination</td>
											<td rowspan="2" style="text-align:center">Delivered</td>
										</tr>
										<tr class="InvHead">
											<td>Room</td>
											<td>Item Condition</td>
											<td>Comments</td>
											<td>Item Condition</td>
										</tr>
									</table>
							</div>
							</td>
						</tr>
					</table> 
				</div>
				
				</div>
			</td>
		</tr
	</c:forEach>
</table> 
	
	
	