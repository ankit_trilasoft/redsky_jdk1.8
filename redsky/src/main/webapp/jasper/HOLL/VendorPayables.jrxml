<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="VendorPayables"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="960"
		 pageHeight="842"
		 columnWidth="900"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Company Division" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT "VendorPayables-HOLL.jrxml",
serviceorder.companyDivision AS CompanyDivision, serviceorder.job AS Job, serviceorder.shipNumber AS ShipNumber, accountline.estimatevendorname AS Estimatevendorname, accountline.actgCode AS ActgCode, accountline.vendorCode AS VendorCode, accountline.invoiceNumber AS InvoiceNumber, accountline.actualExpense AS ActualExpense, accountline.invoiceDate AS InvoiceDate, accountline.receivedDate AS 'Recd Date', accountline.payGl AS PayGl, left(serviceorder.coordinator,10) AS Coordinator, sum(accountline.actualExpense) AS Subtotal
FROM
    serviceorder serviceorder
    inner join billing billing on serviceorder.id=billing.id and billing.corpId=$P{Corporate ID}
    inner join accountline accountline on accountline.serviceOrderId=serviceorder.id and accountline.corpId=$P{Corporate ID}
    left outer join partnerprivate partner on accountline.vendorCode=partner.partnerCode and partner.corpId=$P{Corporate ID}
Where serviceorder.corpId=$P{Corporate ID}
and serviceorder.companyDivision like $P{Company Division}
and accountline.actualExpense <> 0
and accountline.payingStatus = 'A'
and (accountline.vendorCode is not null or accountline.vendorCode <> '')
and accountline.updatedOn >= '2008-12-31'
and accountline.status=true and accountline.payPostDate >='2008-12-31'
and accountline.actgCode != 'temp'
and partner.payableUploadCheck is not true and (accountline.invoiceNumber <> '' or accountline.invoiceNumber is not null ) and accountline.payAccDate is null
GROUP BY accountline.id
Order by serviceorder.CompanyDivision,accountline.vendorCode,accountline.invoicenumber;]]></queryString>

	<field name="VendorPayables-HOLL.jrxml" class="java.lang.String"/>
	<field name="CompanyDivision" class="java.lang.String"/>
	<field name="Job" class="java.lang.String"/>
	<field name="ShipNumber" class="java.lang.String"/>
	<field name="Estimatevendorname" class="java.lang.String"/>
	<field name="ActgCode" class="java.lang.String"/>
	<field name="VendorCode" class="java.lang.String"/>
	<field name="InvoiceNumber" class="java.lang.String"/>
	<field name="ActualExpense" class="java.math.BigDecimal"/>
	<field name="InvoiceDate" class="java.sql.Timestamp"/>
	<field name="Recd Date" class="java.sql.Timestamp"/>
	<field name="PayGl" class="java.lang.String"/>
	<field name="Coordinator" class="java.lang.String"/>
	<field name="Subtotal" class="java.math.BigDecimal"/>

	<variable name="Sum" class="java.math.BigDecimal" resetType="Group" resetGroup="invoiceNumber" calculation="Sum">
		<variableExpression><![CDATA[$F{ActualExpense}]]></variableExpression>
	</variable>

		<group  name="invoiceNumber" >
			<groupExpression><![CDATA[$F{InvoiceNumber}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="18"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="577"
						y="0"
						width="71"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-13"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Sub Total]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="648"
						y="0"
						width="90"
						height="18"
						key="textField"/>
					<box rightPadding="7"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{Sum}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="50"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="437"
						y="3"
						width="463"
						height="26"
						forecolor="#666666"
						key="staticText-9"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="18" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Vendor Payables Not uploaded]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="49"
						width="900"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="31"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="28"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-1"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Div]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="28"
						y="0"
						width="98"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-2"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Ship #]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="126"
						y="0"
						width="52"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-3"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Vendor]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="178"
						y="0"
						width="160"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-4"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Vendor Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="338"
						y="0"
						width="70"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-5"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Actg Code]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="408"
						y="0"
						width="91"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-6"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Invoice Number]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="565"
						y="0"
						width="71"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-7"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Invoice Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="636"
						y="0"
						width="90"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-8"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Actual Expense]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="726"
						y="0"
						width="56"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-10"/>
					<box leftPadding="9"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Paygl]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="782"
						y="0"
						width="32"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-11"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Job]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="814"
						y="0"
						width="86"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-12"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Coordinator]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="499"
						y="0"
						width="66"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-17"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Recd Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="15"
						width="28"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-18"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="28"
						y="15"
						width="98"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-19"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="126"
						y="15"
						width="52"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-20"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Code]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="178"
						y="15"
						width="160"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-21"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="338"
						y="15"
						width="70"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-22"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="408"
						y="15"
						width="91"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-23"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="565"
						y="15"
						width="71"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-24"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="636"
						y="15"
						width="90"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-25"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[$]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="726"
						y="15"
						width="56"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-26"/>
					<box leftPadding="9"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="782"
						y="15"
						width="32"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-27"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="814"
						y="15"
						width="86"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-28"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="499"
						y="15"
						width="66"
						height="16"
						backcolor="#CCCCCC"
						key="staticText-29"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="25"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="0"
						y="1"
						width="28"
						height="24"
						backcolor="#E9E7E7"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{CompanyDivision}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="28"
						y="1"
						width="98"
						height="24"
						backcolor="#E9E7E7"
						key="textField-2"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ShipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="178"
						y="1"
						width="160"
						height="24"
						backcolor="#E9E7E7"
						key="textField-3"
						isRemoveLineWhenBlank="true"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Estimatevendorname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="126"
						y="1"
						width="52"
						height="24"
						backcolor="#E9E7E7"
						key="textField-4"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{VendorCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="338"
						y="1"
						width="70"
						height="24"
						backcolor="#E9E7E7"
						key="textField-5"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ActgCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="408"
						y="1"
						width="91"
						height="24"
						backcolor="#E9E7E7"
						key="textField-6"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{InvoiceNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="565"
						y="1"
						width="71"
						height="24"
						backcolor="#E9E7E7"
						key="textField-7"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{InvoiceDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="636"
						y="1"
						width="90"
						height="24"
						backcolor="#FFFFFF"
						key="textField-8"/>
					<box rightPadding="7"></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{ActualExpense}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="726"
						y="1"
						width="56"
						height="24"
						backcolor="#E9E7E7"
						key="textField"/>
					<box leftPadding="9"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{PayGl}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="782"
						y="1"
						width="32"
						height="24"
						backcolor="#FFFFFF"
						key="textField"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="814"
						y="1"
						width="86"
						height="24"
						backcolor="#E9E7E7"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Coordinator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="499"
						y="1"
						width="66"
						height="24"
						backcolor="#E9E7E7"
						key="textField-12"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Recd Date}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="766"
						y="2"
						width="50"
						height="15"
						key="textField-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="816"
						y="2"
						width="84"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="2"
						width="125"
						height="15"
						key="textField-11"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times-Roman" size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="900"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
