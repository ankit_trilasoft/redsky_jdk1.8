package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ErrorLogDao;
import com.trilasoft.app.model.ErrorLog;



public class ErrorLogDaoHibernate extends GenericDaoHibernate<ErrorLog,Long> implements ErrorLogDao{

	public ErrorLogDaoHibernate() {
		super(ErrorLog.class);
		// TODO Auto-generated constructor stub
	}
	 public List findDistinct() {
			return getHibernateTemplate().find(" SELECT distinct corpID FROM Company ");
		}

	 public List searchErrorLogFile(String corpid,String module,String createdBy,Date createdOn){
		  StringBuilder formatedDate1=new StringBuilder() ;
			try{
				if(createdOn!=null){
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( createdOn ) );
				}
			}catch(Exception ex){}
			if(corpid== null) corpid="";
			if(module== null) module="";
			if(createdBy== null) createdBy="";
			
			String query="from ErrorLog where (corpId like '%"+corpid+"%') and (module like '%"+module+"%' ) and (createdBy like '%"+createdBy+"%') ";
			if(createdOn!=null){
				query = query +" and createdOn like '"+formatedDate1+"%'";	
			}
			
		 return getSession().createQuery(query).list();
	  }
}
