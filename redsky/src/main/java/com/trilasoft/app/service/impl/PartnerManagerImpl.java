package com.trilasoft.app.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerDao;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.service.PartnerManager;

import java.util.Map;

public class PartnerManagerImpl extends GenericManagerImpl<Partner, Long> implements PartnerManager {   
	    PartnerDao partnerDao;   
	  
	    public PartnerManagerImpl(PartnerDao partnerDao) {   
	        super(partnerDao);   
	        this.partnerDao = partnerDao;   
	    }   
	  
	    public List<Partner> findByLastName(String lastName) {   
	        return partnerDao.findByLastName(lastName);   
	    }   
	    public List<Partner> findForPartner(String lastName, String partnerCode, String billingCountryCode, String findFor) {   
	        return partnerDao.findForPartner(lastName, partnerCode, billingCountryCode, findFor);   
	    }
	    
	    public List<Partner> searchForPartner(String lastName, String partnerCode, String billingCountryCode, String billingStateCode, String billingCountry, String findFor) {   
	        return partnerDao.searchForPartner(lastName, partnerCode, billingCountryCode,billingStateCode, billingCountry, findFor);   
	    }
	    public List<Partner> findByBillingCountryCode(String billingCountryCode) {   
	        return partnerDao.findByBillingCountryCode(billingCountryCode);   
	    }
	    
	    public List<Partner> findByAccount(){
	    	return partnerDao.findByAccount();
	    }
	    
	    public List<Partner> findByBroker() {   
	        return partnerDao.findByBroker();   
	    }
	    public List<Partner> findByCarriers(String sessionCorpID) {   
	        return partnerDao.findByCarriers(sessionCorpID);   
	    }

	    public List<Partner> findByAgent() {   
	        return partnerDao.findByAgent();   
	    }

	    public List<Partner> findByOrigin(String origin) {   
	        return partnerDao.findByOrigin(origin);   
	    }
	    public List<Partner> findByFreight(String origin) {   
	        return partnerDao.findByFreight(origin);   
	    }

	    public List<Partner> findByDestination(String destination,String userType) {   
	        return partnerDao.findByDestination(destination,userType);   
	    } 
	    public List findMaximum() {
	    	return partnerDao.findMaximum();
	    }

	    public List findPartnerCode(String partnerCode){
	    	return partnerDao.findPartnerCode(partnerCode);
	    }
	    
	    public List getPartnerPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag){
	    	return partnerDao.getPartnerPopupList(partnerType,corpID, lastName,aliasName, partnerCode, billingCountryCode,billingCountry,billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag);
	    }
	    public List	getCrewPartnerPopupList(String corpID, String lastName,String aliasName, String partnerCode, String billingCountry, String billingStateCode,String extReference){
	    	return partnerDao.getCrewPartnerPopupList(corpID, lastName, aliasName, partnerCode, billingCountry, billingStateCode, extReference);
	    }
	    public List findMaxByCode(){
	    	return partnerDao.findMaxByCode();
		    }
	    public List<Partner> findByBillToCode(String shipNumber){
	    	return partnerDao.findByBillToCode(shipNumber);
	    }
	     
	    public List partnerExtract(String billToCode,String corpID){
	    	return partnerDao.partnerExtract(billToCode,corpID);
	    }

		public List<Partner> findByOwner() {
			
			return partnerDao.findByOwner();
		}
		public String findByOwnerOpLastNameByCode(String perId){
			return partnerDao.findByOwnerOpLastNameByCode(perId);
		}
		public List<Partner> findByCrewDrivers(){
			return partnerDao.findByCrewDrivers();
		}
		public List<Partner> findByOwnerOp(String perId) {
			return partnerDao.findByOwnerOp(perId); 
		}

		public List<Partner> findMultiAuthorization(String accPartnerCode) {
			
			return partnerDao.findMultiAuthorization(accPartnerCode);
		}

		public List findPartnerForActgCode(String partnerCode,String corpID,String companyDivision) {
			
			return partnerDao.findPartnerForActgCode(partnerCode,corpID,companyDivision);
		}
	    public List findPartnerForActgCodeLine(String partnerCode,String corpID,String companyDivision,String actgCodeFlag, Boolean cmmDmmFlag) {
			
			return partnerDao.findPartnerForActgCodeLine(partnerCode,corpID,companyDivision,actgCodeFlag,cmmDmmFlag);
		}
		public List<Partner> findByOriginAccRef(String origin) {
			return partnerDao.findByOriginAccRef(origin);	
		}

		public List<Partner> findByDestAccRef(String destination) {
			
			return partnerDao.findByDestAccRef(destination);
		}
        /*public List<Partner> findByBrokerAccRef() {
			
			return partnerDao.findByBrokerAccRef();
		}

		public List<Partner> findByCarriersAccRef() {
			
			return partnerDao.findByCarriersAccRef(); 
		}  */
		public List getPartnerPopupListAdmin(String partnerType,String corpID,String firstName,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgInactive, String externalRef,String vanlineCode,String typeOfVendor, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA){
			return partnerDao.getPartnerPopupListAdmin(partnerType, corpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, isPP, isAcc, isAgt, isVndr, isCarr, isOO, isIgInactive, externalRef,vanlineCode,typeOfVendor, fidiNumber, OMNINumber, AMSANumber,WERCNumber, IAMNumber, utsNumber , eurovanNetwork , PAIMA, LACMA);
		}
		
		public List getPartnerGeoMapDataList(String corpID, Boolean isAgt, Boolean isVndr, Boolean isCarr,String minLatitude,String maxLatitude,String minLogitude,String maxLogitude,Boolean isUts,Boolean isFidi,Boolean isOmniNo,Boolean isAmsaNo,Boolean isWercNo,Boolean isIamNo,Boolean isAct,String country,String lastName){
			return partnerDao.getPartnerGeoMapDataList(corpID, isAgt, isVndr, isCarr, minLatitude, maxLatitude, minLogitude, maxLogitude, isUts, isFidi, isOmniNo, isAmsaNo, isWercNo, isIamNo, isAct, country, lastName);
		}
		public List<Partner> findByOwnerOps(String perId){
			return partnerDao.findByOwnerOps(perId);
		}
		public List<Partner> checkValidNationalCode(String vlCode,String sessionCorpID){
			return partnerDao.checkValidNationalCode(vlCode,sessionCorpID);
		}
		public List findSubContExtractSeq(String corpID) {
			
			return partnerDao.findSubContExtractSeq(corpID);
		}

		public int updateSubContrcExtractSeq(String subConExtractSeq, String corpID) {
			return partnerDao.updateSubContrcExtractSeq(subConExtractSeq, corpID);
		}
		
		public List getPartnerPopupListCompDiv(String partnerType,String corpID,String compDiv,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag){
			return partnerDao.getPartnerPopupListCompDiv(partnerType, corpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag);
		}

		public List getCountryCode(String mailingCountry) {
			return partnerDao.getCountryCode(mailingCountry);
		}
		public List financePartnerExtract(String beginDate,String endDate, String companyDivision,String corpID) {
			return partnerDao.financePartnerExtract(beginDate, endDate,companyDivision,corpID);
		}
		public List financePartnerDataExtract(String corpID,String beginDate,String endDate) {
			return partnerDao.financePartnerDataExtract(corpID,beginDate,endDate);
		}

		public void updatePrtExtractFlag(String sessionCorpID, String pcode) {
			partnerDao.updatePrtExtractFlag(sessionCorpID, pcode);
			
		}
		
		public void disableUser(Long id, String sessionCorpID, String status) {
			partnerDao.disableUser(id, sessionCorpID, status);
			
		}
		public List getPartnerChildAgentsList(String partnerCode, String sessionCorpID) {
			return partnerDao.getPartnerChildAgentsList(partnerCode, sessionCorpID);
		}
		public List getPartnerIncludeChildAgentsList(String partnerCode, String sessionCorpID,String pageListType){
			return partnerDao.getPartnerIncludeChildAgentsList(partnerCode, sessionCorpID,pageListType);
		}
		public List synInvoicePartnerExtract(String sysInvoiceDates, String companyDivision, String corpID) {
			
			return partnerDao.synInvoicePartnerExtract(sysInvoiceDates, companyDivision, corpID);
		}

		public List getAllAgent(String sessionUserName) {
			return partnerDao.getAllAgent(sessionUserName);
		}

		public List searchAgentList(String firstName, String lastName, String partnerCode, String countryCode, String stateCode, String country, String status, String partnerIds, String sessionCorpID) {
			return partnerDao.searchAgentList(firstName, lastName, partnerCode, countryCode, stateCode, country, status, partnerIds,  sessionCorpID);
		}
		public List findAcctRefNum(String code, String corpId){
			return partnerDao.findAcctRefNum(code, corpId);
		}
		public List findPartnerProfile(String code, String corpId){
			return partnerDao.findPartnerProfile(code, corpId);
		}

		public List<Partner> financeAgentPartnerExtract(String beginDate, String endDate, String companyDivision, String corpID) {
			return partnerDao.financeAgentPartnerExtract(beginDate, endDate, companyDivision, corpID);
		}

		public List<Partner> findVendorCode(Long sid, String sessionCorpID, String jobType,Long cid ) {
			
			return partnerDao.findVendorCode(sid, sessionCorpID,jobType, cid);
		}

		public List searchDefaultVandor(Long sid, String job, String partnerType, String sessionCorpID, String lastName,String aliasName,String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, Long cid,  String externalRef) {
			
			return partnerDao.searchDefaultVandor(sid, job, partnerType, sessionCorpID, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,cid, externalRef);
		}

		public List findBookingAgentCode(String companyDivision, String sessionCorpID) {
			
			return partnerDao.findBookingAgentCode(companyDivision, sessionCorpID);
		}

		public List searchBookingAgent(String companyDivision, String sessionCorpID, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode) {
			
			return partnerDao.searchBookingAgent(companyDivision, sessionCorpID, lastName, partnerCode, billingCountryCode, billingCountry, billingStateCode);
		}
		
		public List ratingList(String partnerCode, String sessionCorpID){
			return partnerDao.ratingList(partnerCode, sessionCorpID);
		}

		public List getFeedbackList(String partnerCode, String year, String corpID) {
			return partnerDao.getFeedbackList(partnerCode, year, corpID);
		}
		
		public List getPartnerActivityList(String partnerCode, String sessionCorpID){
			return partnerDao.getPartnerActivityList(partnerCode, sessionCorpID);
		}
		
		public List getPartnerSSCWActivityList(String partnerCode, String corpID){
			return partnerDao.getPartnerSSCWActivityList(partnerCode, corpID);
		}
		
		public List getPartnerListNew(String lastName, String countryCode, String sessionCorpID, String billingCity){
			return partnerDao.getPartnerListNew(lastName, countryCode, sessionCorpID,  billingCity);
		}

		public List getPartnerPopupListAccount(String partnerType,String corpID,String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, String externalRef, Boolean cmmDmmFlag) {
			
			return partnerDao.getPartnerPopupListAccount(partnerType, corpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, isPP, isAcc, isAgt, isVndr, isCarr, isOO, externalRef, cmmDmmFlag);
		}
		
		public List getPartnerVanLinePopupList(String partnerType, String corpID, String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String vanLineCODE, Boolean isPrivateParty, Boolean isAccount, Boolean isAgent, Boolean isVendor, Boolean isCarrier, Boolean isOwnerOp, Boolean cmmDmmFlag){
			return partnerDao.getPartnerVanLinePopupList(partnerType, corpID, firstName, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, vanLineCODE, isPrivateParty, isAccount, isAgent, isVendor, isCarrier, isOwnerOp, cmmDmmFlag);
		}

		public List getAgentCompanyDivisionVanline(String string, String sessionCorpID, String firstName, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String vanLineCODE,String aliasName, String companyDivision) {
			return partnerDao.getAgentCompanyDivisionVanline(string, sessionCorpID, firstName, lastName, partnerCode, billingCountryCode, billingCountry, billingStateCode, vanLineCODE,aliasName, companyDivision);
		}

		public List findPartnerId(String partnerCode, String sessionCorpID) {
			
			return partnerDao.findPartnerId(partnerCode, sessionCorpID);
		}
		public List<Partner> findByCarriersForHeavy(){
			return partnerDao.findByCarriersForHeavy();
		}
		public List getCarrierListForHeavy(String corpID,String lastName, String partnerCode){
			return partnerDao.getCarrierListForHeavy(corpID, lastName, partnerCode);
		}
		public List getPayToForHeavy(String corpID,String partnerCode){
			return partnerDao.getPayToForHeavy(corpID, partnerCode);
		}
		public List ratingListOneYr(String partnerCode, String sessionCorpID) {
			return partnerDao.ratingListOneYr(partnerCode, sessionCorpID);
		}

		public List ratingListSixMonths(String partnerCode, String sessionCorpID) {
			return partnerDao.ratingListSixMonths(partnerCode, sessionCorpID);
		}

		public List ratingListTwoYr(String partnerCode, String sessionCorpID) {
			return partnerDao.ratingListTwoYr(partnerCode, sessionCorpID);
		}

		public List accountRatingList(String partnerCode, String sessionCorpID, String days) {
			return partnerDao.accountRatingList(partnerCode, sessionCorpID, days);
		}

		public List getAccountFeedbackList(String partnerCode, String year, String sessionCorpID) {
			return partnerDao.getAccountFeedbackList(partnerCode, year, sessionCorpID);
		}

		public List getPartnerAccountActivityList(String partnerCode, String sessionCorpID) {
			return partnerDao.getPartnerAccountActivityList(partnerCode, sessionCorpID);
		}

		public List<Partner> findByNetwork() {
			return partnerDao.findByNetwork();
		}

		public List findByDestinationRelo(String destination, String jobRelo,String jobReloName ,String sessionCorpID) {
			return partnerDao.findByDestinationRelo(destination, jobRelo,jobReloName,sessionCorpID);
		}

		public List findPartnerRelo(String lastName, String partnerCode, String billingCountryCode, String billingState, String billingCountry, String jobRelo,String jobReloName,String searchOfVendorCode,String sessionCorpID,boolean cmmdmmflag) {
			return partnerDao.findPartnerRelo(lastName, partnerCode, billingCountryCode, billingState, billingCountry, jobRelo,jobReloName,searchOfVendorCode,sessionCorpID,cmmdmmflag);
		}

		public List vendorNameForUniqueActgCode(String partnerCode, String sessionCorpID, Boolean cmmDmmFlag) {
			return partnerDao.vendorNameForUniqueActgCode(partnerCode, sessionCorpID, cmmDmmFlag);
		}
		public String getAccountCrossReference(String vendorCode,String companyDiv ,String sessionCorpID){
			return partnerDao.getAccountCrossReference(vendorCode, companyDiv, sessionCorpID);
		}
		public String getAccountCrossReferenceUnique(String vendorCode,String sessionCorpID){
			return partnerDao.getAccountCrossReferenceUnique(vendorCode, sessionCorpID);
		}
		public List getDriverLocationList( String corpID){
			return partnerDao.getDriverLocationList(corpID);
		}
		public List getAllDriverLocation(String driverTypeValue,String corpID){
			return partnerDao.getAllDriverLocation(driverTypeValue,corpID);
		}
		public List getAllInvolvementOfDriver(String driverId,String sessionCorpID){
			return partnerDao.getAllInvolvementOfDriver(driverId, sessionCorpID);
		}
		public String getDriverHomeAddress(String driverId){
			return partnerDao.getDriverHomeAddress(driverId);
		}
		public List partnerSAPExtract(String billToCode, String corpID){
			return partnerDao.partnerSAPExtract(billToCode,corpID);
		}
		public List partnerSAPExtractForUGCN(String billToCode, String corpID){
			return partnerDao.partnerSAPExtractForUGCN(billToCode,corpID);
		}
		public List financeSAPPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID){
			return partnerDao.financeSAPPartnerExtract(beginDate,endDate,companyDivision,corpID);
		}
		public List synSAPInvoicePartnerExtract(String sysInvoiceDates, String companyDivision, String corpID){
			return partnerDao.synSAPInvoicePartnerExtract(sysInvoiceDates,  companyDivision,corpID);
		}
		public List<Partner> financeSAPAgentPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID){
			return partnerDao.financeSAPAgentPartnerExtract(beginDate, endDate,companyDivision,corpID);
		}
		
		public List trackingInfoByShipNumber(String sessionCorpID,String sequenceNumber){
			return partnerDao.trackingInfoByShipNumber(sessionCorpID, sequenceNumber);
		}
		public Map<String, String> driverDropDownList(String sessionCorpID){
			return partnerDao.driverDropDownList(sessionCorpID);
		}
		public List nextDriverLocationList(String driverId,String sessionCorpID){
			return partnerDao.nextDriverLocationList(driverId, sessionCorpID);
		}

		public List findActgCodeForVendorCode(String vendorCode,String sessionCorpID, String companyDivision, String companyDivisionAcctgCodeUnique) {
			return partnerDao.findActgCodeForVendorCode(vendorCode, sessionCorpID, companyDivision,companyDivisionAcctgCodeUnique);
		}
		public List getAllInvolvementOfDriverMap(String driverId,String sessionCorpID){
			return partnerDao.getAllInvolvementOfDriverMap(driverId, sessionCorpID);
		}
		public Map<String, String> findDriverTypeList(String sessionCorpID){
			return partnerDao.findDriverTypeList(sessionCorpID);
		}
		public List getAmmount(String partnerCode, String sessionCorpID){
			return partnerDao.getAmmount(partnerCode, sessionCorpID);
		}
		public List getPartnerPopupListForContractAgentForm(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor){
			return partnerDao.getPartnerPopupListForContractAgentForm(partnerType, corpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, extReference, customerVendor);
		}

		public List getPartnerNetworkGroupPopupList(String partnerType, String corpID, String lastName, String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String customerVendor,String vanlineCode) {
			return partnerDao.getPartnerNetworkGroupPopupList(partnerType, corpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, extReference, customerVendor,vanlineCode);
		}
		 public List partnerExtractUTSI(String billToCode,String corpID){
		    	return partnerDao.partnerExtractUTSI(billToCode,corpID);
		    }
		public List financePartnerExtractUTSI(String beginDate,String endDate, String companyDivision,String corpID){
			return partnerDao.financePartnerExtractUTSI(beginDate, endDate, companyDivision, corpID);
		}
		public List synInvoicePartnerExtractUTSI(String sysInvoiceDates, String companyDivision, String corpID){
			return partnerDao.synInvoicePartnerExtractUTSI(sysInvoiceDates, companyDivision, corpID);
		}
		public List<Partner> financeAgentPartnerExtractUTSI(String beginDate, String endDate, String companyDivision,String corpID){
			return partnerDao.financeAgentPartnerExtractUTSI(beginDate, endDate, companyDivision, corpID);
		}
		public List basedAtName(String partnerCode, String sessionCorpID){
			return partnerDao.basedAtName(partnerCode,sessionCorpID);
		}

		public List getPartnerPopupAllList(String partnerType, String corpID,
				String lastName, String aliasName, String partnerCode,
				String billingCountryCode, String billingCountry,
				String billingStateCode, String extReference) {
			
			return partnerDao.getPartnerPopupAllList(partnerType, corpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, extReference);
		}

		public List<Partner> findByAllOwner() {
			return partnerDao.findByAllOwner();
		}

		public String checkPartnerType(String billToCode) {
			return partnerDao.checkPartnerType(billToCode);
		}
		public List getQuotationPartnerPopupListCompDiv(String partnerType,String corpID,String compDiv,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag){
			return partnerDao.getQuotationPartnerPopupListCompDiv(partnerType, corpID, compDiv, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode,extReference,vanlineCode,cmmDmmFlag);
		}
	    public List getQuotationPartnerPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag){
	    	return partnerDao.getQuotationPartnerPopupList(partnerType,corpID, lastName,aliasName, partnerCode, billingCountryCode,billingCountry,billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag);
	    }
	    public String checkDriverIdAjax(String driverCode,String corpId){
	    	return partnerDao.checkDriverIdAjax(driverCode, corpId);
	    }
	    public List financeAgentPartnerExtractHoll(String beginDate, String endDate, String companyDivision,String corpID){
	    	return partnerDao.financeAgentPartnerExtractHoll(beginDate, endDate, companyDivision, corpID);
	    }
	    public List synInvoicePartnerExtractHoll(String sysInvoiceDates, String companyDivision, String corpID){
	    	return partnerDao.synInvoicePartnerExtractHoll(sysInvoiceDates, companyDivision, corpID);
	    }
	    public List partnerExtractHoll(String billToCode, String corpID){
	    	return partnerDao.partnerExtractHoll(billToCode, corpID);
	    }
	    public List financePartnerExtractHoll(String beginDate, String endDate, String companyDivision,String corpID){
	    	return partnerDao.financePartnerExtractHoll(beginDate, endDate, companyDivision, corpID);
	    }

		public List findPartnerForActgCodeVanLine(String vendorCode, String sessionCorpID) {
			return partnerDao.findPartnerForActgCodeVanLine(vendorCode, sessionCorpID);
		}

		public List<String> getPartnerTypeList() {
			return partnerDao.getPartnerTypeList();
		}
		public Set getVanlineJobDetails(){
			return partnerDao.getVanlineJobDetails();
		}
		public List AFASPartnerExtract(String beginDate, String endDate, String corpID) {
			return partnerDao.AFASPartnerExtract(beginDate, endDate, corpID);
		}

		public Map<String, AccountContact> accountContactMap(String sessionCorpID) {
			return partnerDao.accountContactMap(sessionCorpID);
		}
		
		public List getPartnerExtract(String billToCode,String corpID){
	    	return partnerDao.getPartnerExtract(billToCode,corpID);
	    }

		public List<Partner> findByNetworkPartnerCode(String networkPartnerCode, String sessionCorpID, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String aliasName, String vanlineCode) {
			return partnerDao.findByNetworkPartnerCode(networkPartnerCode, sessionCorpID,  lastName,  partnerCode,  billingCountryCode,  billingCountry, billingStateCode,  extReference,  aliasName,  vanlineCode);
		}

		public List findByNetworkBillToName(String networkPartnerCode, String sessionCorpID, String networkBillToCode) {
			return partnerDao.findByNetworkBillToName(networkPartnerCode, sessionCorpID, networkBillToCode);
		}

		public List findNetworkPartnerDetailsForAutoComplete(String partnerName, String networkPartnerCode, String sessionCorpID) {
			return partnerDao.findNetworkPartnerDetailsForAutoComplete(partnerName,networkPartnerCode, sessionCorpID);
		}
		public String codeCheckPartner(String partnerCode,String corpId,String partnerType){
	    	return partnerDao.codeCheckPartner(partnerCode, corpId,partnerType);
	    }

		public List getPartnerPopupListWithBookingAgentSet(String partnerType, String corpID, String lastName, String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String customerVendor, String vanlineCode, Boolean cmmDmmFlag) {
			return partnerDao.getPartnerPopupListWithBookingAgentSet(partnerType, corpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, extReference, customerVendor, vanlineCode, cmmDmmFlag);
		}
		public List findByOriginforClientDirective(String origin, String corpID, String billToCode) {
			return partnerDao.findByOriginforClientDirective(origin, corpID, billToCode);
		}
		public List findPartnerDetailsForAutoCompleteNew(String partnerName,String partnerNameType,String corpid,Boolean cmmDmmFlag,String billToCode){
			return partnerDao.findPartnerDetailsForAutoCompleteNew(partnerName,partnerNameType ,corpid,cmmDmmFlag,billToCode);
		}
		public List getPartnerVanLinePopupListNew(String partnerType, String corpID, String firstName, String lastName,String aliasName ,String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String vanLineCODE, Boolean isPrivateParty, Boolean isAccount, Boolean isAgent, Boolean isVendor, Boolean isCarrier, Boolean isOwnerOp, Boolean cmmDmmFlag,String billToCode){
			return partnerDao.getPartnerVanLinePopupListNew(partnerType,corpID ,firstName,lastName,aliasName, partnerCode,  billingCountryCode,  billingCountry,  billingStateCode,  vanLineCODE,  isPrivateParty,  isAccount,  isAgent,  isVendor,  isCarrier,  isOwnerOp,  cmmDmmFlag, billToCode);
		}
		public List getPartnerPopupListNew(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag,String billToCode){
			return partnerDao.getPartnerPopupListNew(partnerType,corpID, lastName,aliasName, partnerCode, billingCountryCode,billingCountry,billingStateCode,extReference,customerVendor,vanlineCode,cmmDmmFlag,billToCode);
		}
		
		public List<Partner> vendorNameAutocompleteAjaxlist(String partnerNameAutoCopmlete) {   
	        return partnerDao.vendorNameAutocompleteAjaxlist(partnerNameAutoCopmlete);   
	    }
		
		public List basedAtNameForInvoicing(String partnerCode, String sessionCorpID){
			return partnerDao.basedAtNameForInvoicing(partnerCode,sessionCorpID);
		}
		public List getAgentPopupListCompDiv(String partnerType,String corpID,String compDiv,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag,String agentGroup){
			return partnerDao.getAgentPopupListCompDiv(partnerType, corpID, compDiv, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, extReference, vanlineCode, cmmDmmFlag, agentGroup);
		}
		public List getAgentPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag,String agentGroup){
			return partnerDao.getAgentPopupList(partnerType, corpID, lastName, aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, extReference, customerVendor, vanlineCode, cmmDmmFlag, agentGroup);
		}
		
		public String checkParnerCodeForInv(String billName, String sessionCorpID){
			return partnerDao.checkParnerCodeForInv(billName, sessionCorpID);
		}
		
		public List findByOriginAgent(String origin){
			return partnerDao.findByOriginAgent(origin);
		}
		
		public List agentRatingList(String partnerCode, String sessionCorpID, String days){
			return partnerDao.agentRatingList(partnerCode, sessionCorpID, days);
		}
		public List getAgentRatingFeedbackList(String partnerCode, String days, String corpID){
			return partnerDao.getAgentRatingFeedbackList(partnerCode, days, corpID);
		}

		public List synInvoiceMasBilltoPartnerExtract(String sysInvoiceDates, String corpID) {
			return partnerDao.synInvoiceMasBilltoPartnerExtract(sysInvoiceDates, corpID);
		}

		public List synInvoiceMasPartnerVendorExtract(String sysInvoiceDates, String corpID) {
			return partnerDao.synInvoiceMasPartnerVendorExtract(sysInvoiceDates, corpID);
		}
		public int updateBillToExtractAccess(String sessionCorpID,List partnerList,  String userName) {
			return partnerDao.updateBillToExtractAccess(sessionCorpID, partnerList,userName);
		}
		public int updateVendorExtractAccess(String sessionCorpID,List partnerList,  String userName) {
			return partnerDao.updateVendorExtractAccess(sessionCorpID, partnerList,userName);
		}
		public Map<String, String> checkBilliToCode(String sessionCorpID){
			return partnerDao.checkBilliToCode(sessionCorpID);
			
		}
		public List getOADACode(String partnerCode)
		{
			return partnerDao.getOADACode(partnerCode);
			
		}
	

}