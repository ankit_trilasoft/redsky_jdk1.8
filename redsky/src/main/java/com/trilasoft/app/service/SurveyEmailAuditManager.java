package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.SurveyEmailAudit;

public interface SurveyEmailAuditManager extends GenericManager<SurveyEmailAudit, Long>{
	public Long getSurveyEmailAuditSession(String shipNumber,String accountId,String corpId);
}
