<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineList.heading'/>"/> 
 <script type="text/javascript">      
      function pickLocation(aref,id11){
    	  //window.open('editVanLine.html?id=${vanLineRegNumList.id}')
    	  var chkComp="";
    	 
    	   <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
    	  chkComp="YES";
    	  </configByCorp:fieldVisibility>
    	  
    	  window.open('editWithOutVanLine.html?id='+id11+'&glCodeFlag='+chkComp);
    	 
      }
     
      
      </script>  
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=1;i++){
			document.forms['searchForm'].elements['vanLine.agent'].value = "";
			document.forms['searchForm'].elements['vanLine.weekEnding'].value = "";			
			document.forms['searchForm'].elements['vanLine.glType'].value = "";
		}
}

//function vanLineRegNumList(regNum,controlFlag,position){
	//var agent=document.forms['searchForm'].elements['agent'].value
	//var weekEnding=document.forms['searchForm'].elements['weekEnding'].value
	//alert(weekEnding);
	//var url="vanLineRegNum.html?vanRegNum="+regNum+"&agent="+agent+"&weekEnding="+weekEnding+"&controlFlag="+controlFlag+"&decorator=simple&popup=true";
	//var url="serviceOrderBySeqNumber.html?sequenceNumber="+sequenceNumber+"&controlFlag="+controlFlag+"&decorator=simple&popup=true";
	//ajax_SoTooltip(url,position);	
//}
</script>
<script type="text/javascript">
var flagValue = 0;
var val = 0;
var lastId ='';
function displayResult(regNum,controlFlag,position,id)
{
	if(flagValue==0){
		var agent=document.forms['searchForm'].elements['agent'].value;
		var weekEnding=document.forms['searchForm'].elements['weekEnding'].value;	
		var table=document.getElementById("vanLineList");
		var rownum = document.getElementById(id);
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);
		
		new Ajax.Request('vanLineWithoutRegNum.html?vanRegNum='+regNum+'&agent='+agent+'&weekEnding='+weekEnding+'&controlFlag='+controlFlag+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				    //  alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					     }
				  });
		
		     row.innerHTML = "<td colspan=\"7\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		     return 0;
	}
	if(flagValue==1){
		document.getElementById(val).parentNode.removeChild(document.getElementById(val));
		flagValue = 0;
		var agent=document.forms['searchForm'].elements['agent'].value;
		var weekEnding=document.forms['searchForm'].elements['weekEnding'].value;	
		var table=document.getElementById("vanLineList");
		var rownum = document.getElementById(id);
		if(lastId!=rownum.id){
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);
		
		new Ajax.Request('vanLineWithoutRegNum.html?vanRegNum='+regNum+'&agent='+agent+'&weekEnding='+weekEnding+'&controlFlag='+controlFlag+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				    //  alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){ alert('Something went wrong...') }
				  });
		
		     row.innerHTML = "<td colspan=\"7\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		}
	     return 1;
	}
			
}

</script>
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="JavaScript" type="text/javascript">
	var cal = new CalendarPopup(); 
	cal.showYearNavigation(); 
	cal.showYearNavigationInput();  
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
!width:95%;
}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editWithOutVanLine.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="searchVanLine" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

     
<s:form id="searchForm" action="searchVanLine" method="post" validate="true" >
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="totalamt" value="0.00"/>
<c:set var="totalamt1" value="0.00"/>
<c:set var="totalamt111" value="0.00"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="agent" value="<%=request.getParameter("agent") %>"/>
<c:set var="agent" value="<%=request.getParameter("agent") %>"/>
<s:hidden name="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
<c:set var="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
<c:set var="chkWith" value="${chkWith}"/>
	
		<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current"><span>Order Settlement<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<li><a href="miscellaneousWithoutSettlementList.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}"><span>Miscellaneous Settlement</span></a></li>
			    <li><a href="categoryWithSettlementList.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}"><span>Category Settlement</span></a></li>
		  </ul>
</div><div class="spn" style="!margin-bottom:0px;">&nbsp;</div>
	
      <!--<input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/orderSettlementList.html"/>'"  value="Order Settlement"/> 
        <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/miscellaneousSettlementList.html"/>'"  value="Miscellaneous Settlement"/> 
   <s:set name="vanLineList" value="vanLineList" scope="request"/>   
     -->
     
<display:table name="vanLineList" class="table" requestURI="" id="vanLineList" export="false" defaultsort="2" pagesize="50" style="width:100%;margin:0px;" >   
<c:if test ="${not empty vanLineList}">	
	<display:column  style="width:10px">
	
	<img id="${vanLineList_rowNum}" onclick ="displayResult('${vanLineList.regNum}','C',this,this.id);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<display:column property="agent" sortable="true" titleKey="vanLine.agent"    style="width:90px"/>
	<c:if test="${vanLineList.soId!=null && vanLineList.soId!=''}">
	<c:choose>
    <c:when test="${newAccountline=='Y'}">
    <display:column  sortable="true" title="Reg #">
	<a href="javascript:;" onclick="window.open('pricingList.html?sid=${vanLineList.soId}')" ><c:out value="${vanLineList.regNum}" /></a> 
	</display:column>
    </c:when>
    <c:otherwise>
	<display:column  sortable="true" title="Reg #">
	<a href="javascript:;" onclick="window.open('accountLineList.html?sid=${vanLineList.soId}')" ><c:out value="${vanLineList.regNum}" /></a> 
	</display:column>
	</c:otherwise>
	</c:choose>
	</c:if>
	<c:if test="${vanLineList.soId==null || vanLineList.soId==''}">
	<display:column  sortable="true" title="Reg #">
      <c:out value="${vanLineList.regNum}" />
	</display:column>
	</c:if>
	<display:column property="weekEnding" sortable="true" titleKey="vanLine.weekEnding" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>	
	<display:column property="shipper" sortable="true" titleKey="vanLine.shipper"/>
	<display:column property="reconcileStatus" sortable="true" title="Reconcile Status" />
	<display:column property="amtDueAgent" sortable="true" titleKey="vanLine.amtDueAgent" style="text-align: right; width:100px;">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineList.amtDueAgent}"  /> </display:column>
    
     <c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	 <c:set var="totalamt" value="${(vanLineList.totalAllStatusAmount)}"/>
	 </c:if> 
	 <c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	<c:set var="totalamt1" value="${(vanLineList.reconcileStatusAmount)}"/> 
	</c:if>
	<c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	<c:set var="totalamt111" value="${(vanLineList.suspenseStatusAmount)}"/> 
	</c:if> 
	<display:column property="reconcileAmount" sortable="true" titleKey="vanLine.reconcileAmount" style="text-align: right; width:90px;"/>		

	<display:setProperty name="paging.banner.item_name" value="vanLine"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLines"/>    
    <display:setProperty name="export.excel.filename" value="Van Line List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line List.pdf"/>
    </c:if>   
</display:table>  
<!-- Modification Start For #7921 29 Aprl -->
<div style="background:url(images/bg_listheader.png)repeat-x scroll 0 0 #BCD2EF;height:50px;font-size:13px;font-weight:bold;margin-bottom:5px;padding-bottom:2px;border:2px solid #74B3DC;border-top:none;">
 	<table width="100%">
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:81%;" class="tdheight-footer">Amount(All Statuses)&nbsp;</td>
     <td class="tdheight-footer" width="11%" align="right" style="padding-right:10px;">
     <%-- <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt}" /> --%>
             <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:81%;" class="tdheight-footer">Amount(Reconciled/Approved)&nbsp;</td>
     <td class="tdheight-footer" width="11%" align="right" style="padding-right:10px;">
    <%--  <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt1}" /> --%>
            <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt1}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:81%;" class="tdheight-footer">Amount(Suspense)&nbsp;</td>
     <td class="tdheight-footer" width="11%" align="right" style="padding-right:10px;">
    <%--  <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt1}" /> --%>
            <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt111}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
   </table> 
    </div>
<!-- Modification End -->
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
 
<script type="text/javascript">   
    highlightTableRows("vanLineList");  
</script> 