package com.trilasoft.app.dao;

import java.util.List;

import com.trilasoft.app.model.CustomerFile;

public interface CustomerFileServiceDao {
	
	public  String saveCustomerFile(CustomerFile customerFile);
	public List findMaximum(String corpId);
	public List getSurveyExternalIPsList(String corpId);

}
