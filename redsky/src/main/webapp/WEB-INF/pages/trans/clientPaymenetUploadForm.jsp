<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Client Payment Upload</title>   
    <meta name="heading" content="Client Payment Upload"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>

</head> 

<s:form id="paymentUploadForm" action="processPayment.html" enctype="multipart/form-data" method="post" validate="true">   

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="Layer3" style="width:90%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Client Payment Upload</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">

				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>					
						
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">Bill To Code</td>
							<td> <s:textfield cssClass="input-text" name="uploadBillToCode" size="10" maxlength="10"/></td>
							<!-- 
								<s:textfield cssClass="input-text" name="agentBase.baseCode" size="10" maxlength="10" onchange="findAgentBaseName()"/>
							 	<td><img class="openpopup" width="17" height="20" onclick="changeStatus();openAgentBasePopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							-->
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext">Processing Date<font color="red" size="2">*</font></td>
							
							<c:if test="${not empty processingDate}">
								<s:text id="processingDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="processingDate"/></s:text>
								<td><s:textfield cssClass="input-text" id="processingDate" name="processingDate" value="%{processingDateFormattedValue}" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="processingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty processingDate}">
								<td><s:textfield cssClass="input-text" id="processingDate" name="processingDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="processingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="6" height="5px"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4">
								<s:file name="file" size="25"/>
							</td>
						</tr>
						<tr><td height="20px"></td></tr>
					</tbody>
				</table>

	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<s:submit cssClass="cssbutton1" value="Process" cssStyle="width:55px; height:27px" onclick="return validateUpload();"/>  
<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:27px" key="Reset"/>
 </s:form>
 <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script>
function validateUpload(){
	var fileValue = document.forms['paymentUploadForm'].elements['file'].value;
	var billToCode = document.forms['paymentUploadForm'].elements['uploadBillToCode'].value;
	var processDate = document.forms['paymentUploadForm'].elements['processingDate'].value;
	
	if(processDate==null || processDate==''){
		alert("Please select Processing Date.");
		return false;
	}
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	}
	
	fileValue = fileValue.indexOf(".csv");
	if(fileValue>0){
		return true;
	}else{
		alert("Please select only CSV file.");
		return false;
	}
}
</script>
<script type="text/javascript">   
    Form.focusFirstElement($("paymentUploadForm"));   
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays(),calculateDeliveryDate()"]);
	setCalendarFunctionality();
</script>