<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head>   
    <title>Great Plains Pmt Update</title>   
    <meta name="heading" content="Great Plains Pmt Update"/>  
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script>
function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
function payablesValidateUpload(){
	var fileValue = document.forms['payablesUploadForm'].elements['file'].value; 
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	} 
	fileValue = fileValue.indexOf(".csv");
	if(fileValue>0){
		progressBar('1');
		return true;
	
	}else{
		alert("Please select only CSV file.");
		return false;
	}
}

function recValidateUpload(){ 
	var fileValue = document.forms['receivablesUploadForm'].elements['file'].value; 
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	} 
	fileValue = fileValue.indexOf(".csv");
	if(fileValue>0){
		progressBar('1');
		return true;
	
	}else{
		alert("Please select only CSV file.");
		return false;
	}
}

</script> 
<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:800px;
}
</style>

</head> 
 <DIV ID="layerH" style="position:top;width:350px;height:10px;left:250px;top:320px; background-color:white;z-index:150;">
<td align="justify"  class="listwhitetext" valign="middle"><font size="4" color="#1666C9"><b><blink>Uploading...</blink></b></font></td>
</DIV> 
<table><tr><td>
<s:form id="payablesUploadForm" action="gpProcessPayPayment.html" enctype="multipart/form-data" method="post" validate="true">   
 <div id="Layer3" style="width:100%; margin:0px; padding:0px">
 <div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Payables Upload</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4"> <s:file name="file" size="25"/> </td>
						</tr> 
						<tr><td height="20px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>
						<td></td>
						<td>
						<s:submit cssClass="cssbutton1" name="payables" value="Process" cssStyle="width:55px; height:25px" onclick="return payablesValidateUpload();"/>  
                        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset"/>
                        <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="findPayableLogFile()" value="Show Log" />
                        </td>
                        </tr>
                        <tr><td height="10px"></td></tr>  
					</tbody>
				</table> 
	    </div>

       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       </div> 
</s:form></td>
 <td>
 <s:form id="receivablesUploadForm" action="gpProcessRecPayment.html" enctype="multipart/form-data" method="post" validate="true">   
 <div id="Layer4" style="width:100%; margin-left:25px; padding:0px">
 <div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Receivables Upload</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4"> <s:file name="file" size="25"/> </td>
						</tr>
						<tr><td height="20px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>
						<td></td>
						<td>
						<s:submit cssClass="cssbutton1" value="Process" name="receivables" cssStyle="width:55px; height:25px" onclick="return recValidateUpload();"/>  
                        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset"/>
                        <input type="button" class="cssbutton" style="width:70px;height:25px" onclick="findReceivableLogFile()" value="Show Log" />
                        </td>
                        </tr>
                        <tr><td height="10px"></td></tr>  
					</tbody>
				</table> 
	    </div>
       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       </div> 
           
</s:form></td></tr></table>

<script type="text/javascript">  
 showOrHide(0);
 
function findPayableLogFile(){
	var moduleName= "Great Plains Pay Pmt Update";
	javascript:openWindow('findExtractedFileLogListByModule.html?moduleName='+moduleName+'&decorator=popup&popup=true',800,450);
} 
 
function findReceivableLogFile(){
	var moduleName= "Great Plains Rec Pmt Update";
	javascript:openWindow('findExtractedFileLogListByModule.html?moduleName='+moduleName+'&decorator=popup&popup=true',800,450);
}

</script>