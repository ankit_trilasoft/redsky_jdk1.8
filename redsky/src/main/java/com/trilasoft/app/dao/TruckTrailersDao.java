package com.trilasoft.app.dao;

import java.util.List;

import com.trilasoft.app.model.TruckTrailers;
import org.appfuse.dao.GenericDao;
public interface TruckTrailersDao extends GenericDao<TruckTrailers, Long>{

	public List getPrimaryFlagStatus(String truckNumber, String corpId);
	public List  findTruckTrailersList(String truckNumber,String corpId);
	public List trailersPopup();
	public List searchTrailerPopupList(String truckNumber,String ownerPayTo, String corpId);
}
