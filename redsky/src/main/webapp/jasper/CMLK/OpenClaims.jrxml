<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="OpenClaims" pageWidth="736" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="676" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="25c51219-798d-405d-9852-d750598a04fd">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	"OpenClaims-R-PDF-CMLK.jrxml",
	if(billing.billToCode is null || billing.billToCode='','',billing.billToCode) AS billing_billToCode,
	billing.`billToName` AS billing_billToName,
	claim.claimNumber AS claim_claimNumber,
	if(serviceorder.companyDivision is null || serviceorder.companyDivision='','',serviceorder.companyDivision) AS serviceorder_companyDivision,
	if((serviceorder.`firstName` is null || trim(serviceorder.`firstName`)=''),trim(serviceorder.`lastName`),concat(trim(serviceorder.`firstName`)," ",trim(serviceorder.`lastName`))) AS shipper_name,
	DATE_FORMAT(claim.requestForm,'%d %b %Y') AS claim_requestForm,
	DATE_FORMAT(claim.formSent,'%d %b %Y') AS claim_formSent,
	DATE_FORMAT(claim.formRecived,'%d %b %Y') AS claim_formRecived,
	DATE_FORMAT(claim.insuranceCompanyPaid,'%d %b %Y') AS claim_insuranceCompanyPaid,
	DATE_FORMAT(claim.insurerNotification,'%d %b %Y') AS claim_insurerNotification,
	DATE_FORMAT(now(),'%d %b %Y') AS currentdate
FROM	claim claim
	LEFT OUTER JOIN billing billing on claim.serviceorderid=billing.id AND billing.corpId=$P{Corporate ID}
	INNER JOIN serviceorder serviceorder on billing.id=serviceorder.id AND serviceorder.corpId=$P{Corporate ID}

WHERE claim.`corpId`=$P{Corporate ID}
	AND claim.`cancelled` is null
	AND claim.`closeDate` is null
	AND serviceorder.`status` not in ('CNCL')
ORDER BY serviceorder.companyDivision, billing.`billToCode`, claim.`claimNumber`]]>
	</queryString>
	<field name="OpenClaims-R-PDF-CMLK.jrxml" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="billing_billToName" class="java.lang.String"/>
	<field name="claim_claimNumber" class="java.lang.Long"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="shipper_name" class="java.lang.String"/>
	<field name="claim_requestForm" class="java.lang.String"/>
	<field name="claim_formSent" class="java.lang.String"/>
	<field name="claim_formRecived" class="java.lang.String"/>
	<field name="claim_insuranceCompanyPaid" class="java.lang.String"/>
	<field name="claim_insurerNotification" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="18" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="156" height="18" uuid="6ab5c1d0-b00a-4d98-a094-24dc29249c52"/>
					<textElement>
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-13" x="0" y="0" width="102" height="18" uuid="d3edbfcc-d7e3-46a9-a85f-44ab8872ecf4"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="64" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="0" y="35" width="64" height="29" uuid="74e7a853-757c-49bc-b97b-e1f78867915a"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="64" y="35" width="113" height="29" uuid="d0fff036-3e59-4bfc-990f-2f1fc1cda5ee"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="177" y="35" width="44" height="29" uuid="068b6d42-e445-41d5-b8dd-194cd2d7fa47"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Claim #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="221" y="35" width="128" height="29" uuid="6455ce84-ac3f-4b36-a093-607630c48a4f"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="349" y="35" width="64" height="29" uuid="19e7b10b-394a-4599-9fdd-a2305055e945"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Req. Form]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="413" y="35" width="64" height="29" uuid="dd79c8d1-71cc-4992-be7a-f741d68d31ac"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Form Sent]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="477" y="35" width="64" height="29" uuid="cb29259d-55e5-4993-9595-e9e1da94a7a0"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Form Recd.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="541" y="35" width="64" height="29" uuid="27156b3c-8c0c-4950-b723-f9345397eba6"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Insurer Notification]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="0" y="7" width="676" height="24" uuid="88d3161a-becd-4139-9015-4f23c97cd6b9"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="13" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Open Claims]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="605" y="35" width="71" height="29" uuid="03785efc-d4dd-4c07-a97d-00ff68cbb5db"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Insurance Comp Paid]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="26" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="1" width="64" height="25" uuid="a80714ae-d0c4-444f-8eec-8ae086bf47c6"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="64" y="1" width="113" height="25" uuid="03c4ae15-4c5d-46a7-84aa-8ce592f0e2fc"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToName}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="177" y="1" width="44" height="25" uuid="b92b9abd-ea64-48a9-98de-164827b8c92f"/>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_claimNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="221" y="1" width="128" height="25" uuid="58444c14-c627-43c3-abbd-7b4928b648f4"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{shipper_name}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="349" y="1" width="64" height="25" uuid="e400ebb9-91a1-4365-8415-75729a845ef4"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_requestForm}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="413" y="1" width="64" height="25" uuid="fda4541b-3c3b-48af-ab19-9624772d28dd"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_formSent}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="477" y="1" width="64" height="25" uuid="d482d694-2554-4701-97d7-a21024de3d71"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_formRecived}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="541" y="1" width="64" height="25" uuid="86a6b341-4599-4005-951a-a64293c4b959"/>
				<textElement textAlignment="Center">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_insurerNotification}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-4" x="605" y="1" width="71" height="25" uuid="65f9c074-1d02-4723-a4f4-7ad5af2a544a"/>
				<textElement textAlignment="Center">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_insuranceCompanyPaid}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="17" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-1" x="505" y="2" width="130" height="15" uuid="43c647f9-2c41-4c0e-8dda-785e1d261d65"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-2" x="635" y="2" width="41" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="c71269d7-711b-4a34-bfb5-4ac0516ae727"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-3" x="1" y="2" width="142" height="15" uuid="703c59b9-3f20-4514-96f7-407d7a4a0e0e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-10" x="200" y="1" width="276" height="15" uuid="13a5ab7e-e6cd-4d59-b0c0-e12ca9b789ca"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[OpenClaims.jrxml]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="0" y="1" width="676" height="1" uuid="47307083-bd81-449b-9703-5c9221266852"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
