/*File Name:SubcontractorChargesAction 
 * Created By:Ashish Mishra 
 * Created Date:08-Aug-2008
 * Summary: This action is used to manage the miscellaneous charges .
 * */
package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException; 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTO;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOForPayable;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOPayableExtractLevel;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOPayableLevel1;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOPayableLevel4; 
import com.trilasoft.app.dao.hibernate.SubcontractorChargesDoaHibernate.DTOForSubContcExtract;
import com.trilasoft.app.dao.hibernate.SubcontractorChargesDoaHibernate.DTOForSubContcExtractLevel1;
import com.trilasoft.app.dao.hibernate.SubcontractorChargesDoaHibernate.DTOForSubContcExtractLevel4;
import com.trilasoft.app.dao.hibernate.SubcontractorChargesDoaHibernate.DTOGPSubContcExtract;
import com.trilasoft.app.dao.hibernate.SubcontractorChargesDoaHibernate.DTOSubcontrACCCompanyCodeDataList;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.SubcontractorChargesManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.VanLineGLTypeManager;
import com.trilasoft.app.service.VanLineManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class SubcontractorChargesAction extends BaseAction implements Preparable{
	private SubcontractorChargesManager subcontractorChargesManager;
	private VanLine vanLine;
	private VanLineManager vanLineManager;	 
	private SubcontractorCharges subcontractorCharges;
	private List<SubcontractorCharges>subcontractorChargesList ;
	private Long id;
    private Long IdMax;
    private String sessionCorpID;
    private RefMasterManager refMasterManager;
    private Map<String, String> glCode;
    private ExtendedPaginatedList subcontractorChargesExt;
    private PagingLookupManager pagingLookupManager;
    private PaginateListFactory paginateListFactory;
    private List regNoList=new ArrayList();
    private List regNoList1=new ArrayList();
   	private PartnerManager partnerManager;
    private String perId;
    private String errorValidator;
    private int parentId;
    private long vid;
    private double dueFormAmount;
    private String systemDate;
    private Date systemDateFormat;
    private  List BranchList;
    private  List subcontrCompanyCodeList;
    private String subConExtractSeq;
    private String subcontrCompanyCode;
    private String serviceOrder;
    private Map<String, String> job;
    private List<SystemDefault> sysDefaultDetail;
    private String miscellaneousdefaultdriverId;
    private String companyDivision;
    private String companyCode;
    private Map<String,String> division;
    private String hitFlag;
    private String companies;
    private CompanyManager companyManager;
    private SystemDefaultManager systemDefaultManager;
    private SystemDefault systemDefault;
    private String glCodeFlag;
    private String jspPageFlag;
	private AccountLineManager accountLineManager;
	private List accInvoicePostDateList;
    private CustomerFileManager customerFileManager;
    private CompanyDivisionManager companyDivisionManager;
    private List companyCodeList;
    
    public List getCompanyCodeList() {
		return companyCodeList;
	}
	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}
	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}
	public String getErrorValidator() {
		return errorValidator;
	}
	public void setErrorValidator(String errorValidator) {
		this.errorValidator = errorValidator;
	}
	
	static final Logger logger = Logger.getLogger(SubcontractorChargesAction.class);
	Date currentdate = new Date();

	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
			if(getRequest().getParameter("ajax") == null){
				getComboList(sessionCorpID);
				job = refMasterManager.findByParameter(sessionCorpID, "JOB");
				companies = companyManager.findByCompanyDivis(sessionCorpID);
		 	}
			sysDefaultDetail=systemDefaultManager.findByCorpID(sessionCorpID);
	 		if(sysDefaultDetail!=null && (!(sysDefaultDetail.isEmpty())) && sysDefaultDetail.get(0)!=null){
	 			systemDefault=sysDefaultDetail.get(0);
	 		}
	}
	public String save() throws Exception {
    	
		if(!(subcontractorCharges.getServiceOrder().equals(""))){ 
		List miscellanousStatus=subcontractorChargesManager.findMiscellaneousStatus(subcontractorCharges.getServiceOrder());
		if(miscellanousStatus.isEmpty())
		{
			errorMessage("Service Order Entered has been settled. Please select appropriate Service Order");
			return INPUT;
			
		}
		}
		if (cancel != null) 
		 {
		   return "cancel";
		 }
		//getComboList(sessionCorpID);
		BranchList = subcontractorChargesManager.findBranchList(sessionCorpID);
	   boolean isNew = (subcontractorCharges.getId() == null);
	   if(isNew)
		{
		   subcontractorCharges.setCreatedOn(new Date());
		}
	   else
		{
		   subcontractorCharges.setCreatedOn(subcontractorCharges.getCreatedOn());
		   
		}
	   if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "Subcontractor Charges has been added successfully" : "Subcontractor Charges has been updated successfully";
			saveMessage(getText(key));
		}
	   
	   subcontractorCharges.setUpdatedOn(new Date());
	   subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser());
	   subcontractorCharges.setCorpID(sessionCorpID);
	   /*if(subcontractorCharges.getPersonalEscrow()==true && subcontractorCharges.getSource().equalsIgnoreCase("")){
		   subcontractorCharges.setSource("Standard Deduction");
		   subcontractorCharges.setDeductTempFlag(true);
	   }*/
	   subcontractorCharges=subcontractorChargesManager.save(subcontractorCharges);
	   hitFlag="1";
	   //getComboList(sessionCorpID);
	   if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
	   if (!isNew) { 
		   if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
				 }
       	return SUCCESS;   
       } else {  
    	   if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
  			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
  			 }
    	   IdMax = Long.parseLong(subcontractorChargesManager.findMaxId().get(0).toString());
    	   subcontractorCharges = subcontractorChargesManager.get(IdMax);
        return SUCCESS;
       } 
	   
	  }
   	
	private  Map<String, String> personTypeList;
	private Company company; 
	public String getComboList(String corpId){
		personTypeList = refMasterManager.findByParameter(corpId, "TYPEPERS");
		glCode = refMasterManager.findByParameterOrderByCode(corpId, "GLCODES");
		if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
		subcontrCompanyCodeList=subcontractorChargesManager.getsubcontrCompanyCode(corpId); 
		BranchList = subcontractorChargesManager.findBranchList(corpId);
		division=refMasterManager.findMapList(corpId,"JOB");
		company=companyManager.findByCorpID(corpId).get(0);
		return SUCCESS;
	}
	 public SubcontractorChargesAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	public String list(){
		//getComboList(sessionCorpID);
		subcontractorChargesExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		//subcontractorChargesList=subcontractorChargesManager.getAll();
		//System.out.println(subcontractorChargesList+"subcontractorChargesList in Action");
		String key = "Please enter your search criteria below";
		saveMessage(getText(key));
		return SUCCESS;	
	}
	 public String edit() {  
	    	//getComboList(sessionCorpID); 
		 if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
	    	BranchList = subcontractorChargesManager.findBranchList(sessionCorpID);
	        if (id != null) {   
	        	subcontractorCharges = subcontractorChargesManager.get(id); 
	        	try{
					if(subcontractorCharges.getCompanyDivision()!=null && (!(subcontractorCharges.getCompanyDivision().equals("")))){	
					if(subcontrCompanyCodeList.contains(subcontractorCharges.getCompanyDivision())){
						
					}else{
						subcontrCompanyCodeList.add(subcontractorCharges.getCompanyDivision());	
					}
					}
					}catch(Exception e){
						
					}
	        } 
	        else { 
	        	subcontractorCharges=new SubcontractorCharges(); 
	            try{
	        	companyCodeList=companyDivisionManager.getInvCompanyCode(sessionCorpID); 
	            if(companyCodeList !=null && (!(companyCodeList.isEmpty())) && (companyCodeList.get(0)!=null) && (!(companyCodeList.get(0).toString().equals("")))  && companyCodeList.size()==1){
	            	subcontractorCharges.setCompanyDivision(companyCodeList.get(0).toString());	
	            }
	            subcontractorCharges.setPersonType("D");
	            }catch(Exception e){
	            	
	            }
	        	subcontractorCharges.setCreatedOn(new Date());
	        	subcontractorCharges.setUpdatedOn(new Date()); 
	        }   
	    
	        return SUCCESS;   
	    }
	 private boolean ownbillingVanline = false;
	 private boolean automaticReconcile=false;
	 @SkipValidation
	 public String editSplitSub(){
		// getComboList(sessionCorpID); 
		 Date payDate=null;
		 sysDefaultDetail=systemDefaultManager.findByCorpID(sessionCorpID);
 		if(sysDefaultDetail!=null && (!(sysDefaultDetail.isEmpty())) && sysDefaultDetail.get(0)!=null){
 			systemDefault=sysDefaultDetail.get(0);
 		}
 		if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
			ownbillingVanline=systemDefault.getOwnbillingVanline();
		}
		 if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
		 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
		 }
		 BranchList = subcontractorChargesManager.findBranchList(sessionCorpID);
	        if (id != null) {    
	        	subcontractorCharges = subcontractorChargesManager.get(id); 
	        	try{
					if(subcontractorCharges.getCompanyDivision()!=null && (!(subcontractorCharges.getCompanyDivision().equals("")))){	
					if(subcontrCompanyCodeList.contains(subcontractorCharges.getCompanyDivision())){
						
					}else{
						subcontrCompanyCodeList.add(subcontractorCharges.getCompanyDivision());	
					}
					}
					}catch(Exception e){
						
					}
	        } 
	        else { 
	        	subcontractorCharges=new SubcontractorCharges();  
	        	vanLine=vanLineManager.get(vid);
	        	subcontractorCharges.setParentId(vid); 
	        	subcontractorCharges.setDescription(vanLine.getDescription());
	        	subcontractorCharges.setCorpID(vanLine.getCorpID());
	        	subcontractorCharges.setBranch(vanLine.getAgent());
	        	subcontractorCharges.setRegNumber(vanLine.getRegNum()); 
	        	subcontractorCharges.setCreatedOn(new Date());
	        	subcontractorCharges.setUpdatedOn(new Date());
	        	subcontractorCharges.setGlCode(vanLine.getGlCode());
	        	subcontractorCharges.setCompanyDivision(vanLine.getCompanyDivision());
	        	subcontractorCharges.setApproved(vanLine.getWeekEnding()); 
	        	if(ownbillingVanline){
	        		try{
	        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
	        		String strDate = sm.format(new Date());
	        		Date dt = sm.parse(strDate);
		        	subcontractorCharges.setApproved(dt);
	        		}catch(Exception e){
	        			
	        		}
			    }
	        	subcontractorCharges.setAdvanceDate(vanLine.getWeekEnding());
	        	subcontractorCharges.setDivision(vanLine.getDivision()); 
	        	subcontractorCharges.setDeductTempFlag(false);
	 			subcontractorCharges.setIsSettled(false);
	 			subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
	 			subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
	        	try{
	        	payDate=systemDefault.getPostDate1();
	    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
	    			Date dt1=vanLine.getWeekEnding();
		    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		String dt11=sdfDestination.format(dt1);
		    		Date dt2 =null;
		    		try{
		    		dt2 = new Date();
		    		}catch(Exception e){
		    		}				    		
		    		
		    		dt2=payDate;
		    		if(dt2!=null){
			    		if(dt1.compareTo(dt2)>0){
			    			payDate=dt2;
			    		}else{
			    			payDate=dt1;
			    		}
		    		}
	    		}
		}catch(Exception e){
			e.printStackTrace();
		}  	 
			//}
			if(payDate!=null){ 
				subcontractorCharges.setPost(payDate); 
			} 
	        	if(vanLine.getDriverID()==null||vanLine.getDriverID().equalsIgnoreCase(""))
	         	{
	         		 
	        		
	        			if(systemDefault.getMiscellaneousdefaultdriver()!=null && (!systemDefault.getMiscellaneousdefaultdriver().equalsIgnoreCase(""))){
	        				subcontractorCharges.setPersonId(sysDefaultDetail.get(0).getMiscellaneousdefaultdriver());
	        				String driverLastName=partnerManager.findByOwnerOpLastNameByCode(sysDefaultDetail.get(0).getMiscellaneousdefaultdriver());
	        				if(driverLastName!=null){
	        					subcontractorCharges.setPersonName(driverLastName);	
	        				}
	        				subcontractorCharges.setPersonType("D");
	        			}else{
	        				subcontractorCharges.setPersonId("");
	        				subcontractorCharges.setPersonName("");	
	    	         		subcontractorCharges.setPersonType("D");
	        			}
	        		
	         	}
	         	else if(vanLine.getDriverID()!=null && (!vanLine.getDriverID().equalsIgnoreCase("")))
	         	{
	         		subcontractorCharges.setPersonId(vanLine.getDriverID());
	         		subcontractorCharges.setPersonName(vanLine.getDriverName());
	         		subcontractorCharges.setPersonType("D");
	         	}
	        }  
		 return SUCCESS;
		 
	 }
	 public String editChargeSub(){
		 //getComboList(sessionCorpID);
		 BranchList = subcontractorChargesManager.findBranchList(sessionCorpID);
	        if (id != null) {    
	        	subcontractorCharges = subcontractorChargesManager.get(id);
	        	try{
					if(subcontractorCharges.getCompanyDivision()!=null && (!(subcontractorCharges.getCompanyDivision().equals("")))){	
					if(subcontrCompanyCodeList.contains(subcontractorCharges.getCompanyDivision())){
						
					}else{
						subcontrCompanyCodeList.add(subcontractorCharges.getCompanyDivision());	
					}
					}
					}catch(Exception e){
						
					}
	        } 
	        else { 
	        	subcontractorCharges=new SubcontractorCharges();  
	        	vanLine=vanLineManager.get(vid);
	        	subcontractorCharges.setParentId(vid); 
	        	subcontractorCharges.setDescription(vanLine.getDescription());
	        	subcontractorCharges.setCorpID(vanLine.getCorpID());
	        	subcontractorCharges.setBranch(vanLine.getAgent());
	        	subcontractorCharges.setAmount(vanLine.getAmtDueAgent());
	        	subcontractorCharges.setGlCode(vanLine.getGlCode());
	        	subcontractorCharges.setCompanyDivision(vanLine.getCompanyDivision());
	        	subcontractorCharges.setRegNumber(vanLine.getRegNum()); 
	        	subcontractorCharges.setCreatedOn(new Date());
	        	subcontractorCharges.setUpdatedOn(new Date()); 
	        }  
		 return SUCCESS;
		 
	 }
	 
	 @SkipValidation
	 public String chargeAllocationUpdate()
	 {
	   // getComboList(sessionCorpID);
		 if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
		 sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					miscellaneousdefaultdriverId=systemDefault.getMiscellaneousdefaultdriver();
					if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
						ownbillingVanline=systemDefault.getOwnbillingVanline();
					}
					automaticReconcile=	systemDefault.getAutomaticReconcile();
			}
			}
			vanLine=vanLineManager.get(vid);
			List list=subcontractorChargesManager.getCompanyCode(vanLine.getAgent(),sessionCorpID);
			if(!list.isEmpty()){
			CompanyDivision companyDivision  = (CompanyDivision)list.get(0);
			companyCode=companyDivision.getCompanyCode();
				
			}
	    BranchList = subcontractorChargesManager.findBranchList(sessionCorpID);
	    subcontractorCharges=new SubcontractorCharges();  
     	subcontractorCharges.setParentId(vid); 
     	subcontractorCharges.setDescription(vanLine.getDescription());
     	subcontractorCharges.setCorpID(vanLine.getCorpID());
     	subcontractorCharges.setBranch(vanLine.getAgent());
     	if(ownbillingVanline){
        	if(vanLine.getAmtDueAgent()!=null){ 
        		subcontractorCharges.setAmount(vanLine.getAmtDueAgent());
        	}
        	}else{
        		if(automaticReconcile){
        		BigDecimal  multiplyValue = new BigDecimal(-1.0);
	        	if(vanLine.getAmtDueAgent()!=null){ 
	        	subcontractorCharges.setAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
	        	}
        		}else{ 
    	        	if(vanLine.getAmtDueAgent()!=null){ 
    	        		subcontractorCharges.setAmount(vanLine.getAmtDueAgent());
    	        	} 
        		}
        	}
     	subcontractorCharges.setGlCode(vanLine.getGlCode());
     	subcontractorCharges.setRegNumber(vanLine.getRegNum());
     	subcontractorCharges.setCompanyDivision(companyCode);
     	
     	
     	/*if(vanLine.getDriverID()==null||vanLine.getDriverID()=="" || vanLine.getDriverID().toString().trim().equals(""))
     	{
     		subcontractorCharges.setPersonId(miscellaneousdefaultdriverId);
     		subcontractorCharges.setPersonType("A");
     	}
     	else if(vanLine.getDriverID()!=null||vanLine.getDriverID()!="")
     	{
     		subcontractorCharges.setPersonId(vanLine.getDriverID());
     		subcontractorCharges.setPersonType("D");
     	}*/
     	if(vanLine.getDriverID()==null||vanLine.getDriverID().equalsIgnoreCase(""))
     	{
     		//subcontractorCharges.setPersonId("");
     		//subcontractorCharges.setPersonType("A");
    		sysDefaultDetail=systemDefaultManager.findByCorpID(sessionCorpID);
    		if(sysDefaultDetail!=null && (!(sysDefaultDetail.isEmpty())) && sysDefaultDetail.get(0)!=null){
    			SystemDefault sd=sysDefaultDetail.get(0);
    			if(sd.getMiscellaneousdefaultdriver()!=null && (!sd.getMiscellaneousdefaultdriver().equalsIgnoreCase(""))){
    				subcontractorCharges.setPersonId(sd.getMiscellaneousdefaultdriver());
    				String driverLastName=partnerManager.findByOwnerOpLastNameByCode(sd.getMiscellaneousdefaultdriver());
    				if(driverLastName!=null){
    					subcontractorCharges.setPersonName(driverLastName);	
    				}
    				subcontractorCharges.setPersonType("D");
    			}else{
    				subcontractorCharges.setPersonId("");
    				subcontractorCharges.setPersonName("");	
	         		subcontractorCharges.setPersonType("D");
    			}
    		}
     	}
     	else if(vanLine.getDriverID()!=null && (!vanLine.getDriverID().equalsIgnoreCase("")))
     	{
     		subcontractorCharges.setPersonId(vanLine.getDriverID());
     		subcontractorCharges.setPersonName(vanLine.getDriverName());
     		subcontractorCharges.setPersonType("D");
     	}
     	subcontractorCharges.setCreatedOn(new Date());
     	subcontractorCharges.setUpdatedOn(new Date());
     	subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
     	subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
     	subcontractorChargesManager.save(subcontractorCharges);
		 return SUCCESS; 
	 }
	    private Boolean settled;
	 
	 @SkipValidation
	 public String search(){
		 
		 //getComboList(sessionCorpID);
		        boolean id = (subcontractorCharges.getId() == null);
		        boolean personId = (subcontractorCharges.getPersonId() == null);
				boolean personType = (subcontractorCharges.getPersonType() == null);
				boolean branch = (subcontractorCharges.getServiceOrder() == null);
				boolean regNumber = (subcontractorCharges.getRegNumber() == null);
				boolean approved = (subcontractorCharges.getApproved() == null);
				boolean advanceDate = (subcontractorCharges.getAdvanceDate()==null);
				boolean description= (subcontractorCharges.getDescription()==null);
				String statusDateNew="";
				String statusAdvanceDate="";
				
				if (!personId || !personType || !branch || !regNumber || !approved ||!advanceDate || !description || !id) {
					//System.out.println("\n\n\n Inside List workTicketsExt-->>>");
					subcontractorChargesExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
					List<SearchCriterion> searchCriteria = new ArrayList();
				    	searchCriteria.add(new SearchCriterion("personId", subcontractorCharges.getPersonId(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				    	searchCriteria.add(new SearchCriterion("personType", subcontractorCharges.getPersonType(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				    	searchCriteria.add(new SearchCriterion("serviceOrder", subcontractorCharges.getServiceOrder(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				    	searchCriteria.add(new SearchCriterion("description", subcontractorCharges.getDescription(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				    	searchCriteria.add(new SearchCriterion("regNumber", subcontractorCharges.getRegNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				    	searchCriteria.add(new SearchCriterion("isSettled", settled, SearchCriterion.FIELD_TYPE_BIT, SearchCriterion.OPERATOR_EQUALS));
				    	searchCriteria.add(new SearchCriterion("id", subcontractorCharges.getId(), SearchCriterion.FIELD_TYPE_BIGINT, SearchCriterion.OPERATOR_EQUALS));
				    	try {
						 		SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
						 		StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(subcontractorCharges.getApproved()) );
						 		statusDateNew = nowYYYYMMDD.toString();
						 	}catch (Exception e) {
					 			
					 			e.printStackTrace();
					 		}
					 		try{
					 			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
					 			StringBuilder dateYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(subcontractorCharges.getAdvanceDate()) );
					 			statusAdvanceDate = dateYYYYMMDD.toString();
					 		}catch(Exception e){}
					 		searchCriteria.add(new SearchCriterion("approved", statusDateNew, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					 		searchCriteria.add(new SearchCriterion("advanceDate", statusAdvanceDate, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					 		searchCriteria.add(new SearchCriterion("deductTempFlag", false, SearchCriterion.FIELD_TYPE_BIT, SearchCriterion.OPERATOR_EQUALS));
					 		pagingLookupManager.getAllRecordsPage(SubcontractorCharges.class, subcontractorChargesExt, searchCriteria);	
					 		//	ls = workTicketManager.findByTicket(workTicket.getTicket(), workTicket.getLastName(), workTicket.getFirstName(), workTicket.getShipNumber(), workTicket.getWarehouse(),
							//	workTicket.getService(), workTicket.getDate1(), workTicket.getDate2());
				    } 
		 
		 
		return SUCCESS; 
	 }
	       
		private String gotoPageString;
		private String validateFormNav;
		public String saveOnTabChange() throws Exception { 
			validateFormNav = "OK";
	    	String s = save();
	    	return gotoPageString;
	    }

	 
	 
	 
	 		private String regNo;
	 		@SkipValidation
			public String checkRegNo()
			{   
	 			 //getComboList(sessionCorpID);
				//System.out.println("Checking");
		        if(regNo!=null)
		        {
		        	regNoList=subcontractorChargesManager.registrationNo(regNo);
		        	//System.out.println("Checking"+regNoList);
		        }
		        else
		        {
		        }
				return SUCCESS; 
			}
	 		
	 		@SkipValidation
			public String findServiceOrder()
			{   
	 			//ComboList(sessionCorpID);
				//System.out.println("Checking");
		        if(regNo!=null)
		        {
		        	regNoList=subcontractorChargesManager.findServiceOrder(regNo);
		        	//System.out.println("Checking"+regNoList);
		        }
				return SUCCESS; 
			}
	 	private String regNo1;
	 		@SkipValidation
	 		public String findRegNo() {
	 			if(serviceOrder!=""){
	 				regNoList1=subcontractorChargesManager.findRegNo(serviceOrder);
	 				/*if(regNoList1!=null && !regNoList1.isEmpty()){
	 					String[] str1=regNoList1.get(0).toString().split("~");
	 					regNo1=str1[0]+","+str1[1];
	 				}*/
	 				
	 			}
	 			return SUCCESS;
				
			}
	 		private String approvedDate;
		
	 		@SkipValidation
	 		public String findSystemDate()
	 		{
	 			Date payDate=null;
	        	List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	 			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
	 				for (SystemDefault systemDefault : sysDefaultDetail) {
	 					payDate=systemDefault.getPostDate1();
	 				}
	 			}
	 			try{
					Company company=companyManager.findByCorpID(sessionCorpID).get(0);
		    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
			    		Date dt1=new Date();
			    		SimpleDateFormat sdfOrigin = new SimpleDateFormat("dd-MMM-yy");
			    		dt1=sdfOrigin.parse(approvedDate);
			    		Date dt2 =null;
			    		try{
			    		dt2 = new Date();
			    		}catch(Exception e){
			    		}				    		
			    		
			    		dt2=payDate;
			    		if(dt2!=null){
				    		if(dt1.compareTo(dt2)>0){
				    			payDate=dt2;
				    		}else{
				    			payDate=dt1;
				    		}
			    		}
		    		}
			}catch(Exception e){}
				if(payDate!=null){
		 			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
		 			try{ 
		 	            systemDate= sdf.format(payDate);
		 			}catch(Exception e){e.printStackTrace();} 
 				}else{
 					systemDate=new String("");
 				}
	 			return SUCCESS; 
	 		}
	 		
	 		@SkipValidation
	 		public String subcontrCompanyCode()
	 		{ 	
	 			subcontrCompanyCodeList=subcontractorChargesManager.getsubcontrCompanyCode(sessionCorpID); 
	 			return SUCCESS;
	 		}
	 		
	 		@SkipValidation
	 		public String subcontrCompanyCodeSSCW(){
	 			//subcontrCompanyCodeList=subcontractorChargesManager.getsubcontrCompanyCode(sessionCorpID); 
	 			//InvoicePostDateList=subcontractorChargesManager.getSubcontrCompanyCodeDataList(sessionCorpID);
	 			//accInvoicePostDateList=accountLineManager.findDriverPayableExtractList(sessionCorpID);
	 			InvoicePostDateList=subcontractorChargesManager.getSubcontrACCCompanyCodeDataList(sessionCorpID);
	 			Iterator iterator = InvoicePostDateList.iterator();
	 			Map <String, Integer> subcontrACCCompanyCode = new LinkedHashMap<String, Integer>();
	 			int count =0;
	 			String postDate="";
 				String invoiceDate="";
 				String invoiceDateEnd="";
 				String companyDivision="";
	 			String key = new String (""); 
	 			while(iterator.hasNext()){
	 				 postDate="";
	 				 invoiceDate="";
	 				 invoiceDateEnd="";
	 				 companyDivision="";
	 				Object subExtract=iterator.next();
	 				try{
 	 	    		    SimpleDateFormat postformat = new SimpleDateFormat("yyyy-MM-dd"); 
 	 	                Date du1 = new Date();
 	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getRecPostDate().toString());
                        postDate = postformat.format(du1);
                        
                        du1 = new Date();
 	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getInvoiceDate().toString());
 	 	                invoiceDate = postformat.format(du1);
 	 	                 
 	 	                du1 = new Date();
	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getInvoiceDateEnd().toString());
	 	                invoiceDateEnd = postformat.format(du1);
 	 	    	        
	 	               companyDivision =	((DTOSubcontrACCCompanyCodeDataList)subExtract).getCompanyDivision().toString().trim();
	 	               
	 	              key=postDate+"~"+invoiceDate+"~"+invoiceDateEnd+"~"+companyDivision;
	 	               
	 	              if(subcontrACCCompanyCode.containsKey(key)){
	 	            	 count= subcontrACCCompanyCode.get(key);
	 	            	count=count+1;
	 	            	subcontrACCCompanyCode.put(key, count);
	 					} else{
	 						count=1;
	 					subcontrACCCompanyCode.put(key, count);
	 					}
	 				}
 	 	            catch( Exception e)
 	 	    		{
 	 	    		  e.printStackTrace();
 	 	    		}
	 			}
	 			//System.out.println("\n\n\n\n\n subcontrACCCompanyCode"+subcontrACCCompanyCode);
	 			accInvoicePostDateList = new ArrayList();
	 			Iterator mapIterator1 = subcontrACCCompanyCode.entrySet().iterator();
				while (mapIterator1.hasNext()) {
					DTOSubcontrACCList dTOSubcontrACCList =new DTOSubcontrACCList();	
					Map.Entry entry = (Map.Entry) mapIterator1.next();
					String Key = (String) entry.getKey(); 
					int Value =  (Integer)entry.getValue();
					 String[] str1 = Key.split("~");
					 try{
					 SimpleDateFormat postformat = new SimpleDateFormat("yyyy-MM-dd"); 
					 SimpleDateFormat postformat1 = new SimpleDateFormat("dd-MMM-yyyy");
	 	             Date postDateFormat = new Date();
	 	             postDateFormat = postformat.parse(str1[0]); 
	 	             String postDate1= postformat1.format(postDateFormat); 
	 	             dTOSubcontrACCList.setRecPostDate(postDate1);
	 	             Date du1 = new Date();
	 	             du1 = postformat.parse(str1[1]);
	 	             String  startInvoiceDate=  postformat.format(du1);
	 	             
	 	             startInvoiceDate=  postformat1.format(du1);
	 	             Date du2 = new Date();
	 	             du2 = postformat.parse(str1[2]);
	 	             String  endInvoiceDate=  postformat.format(du2);
	 	             endInvoiceDate= postformat1.format(du2);
	 	             dTOSubcontrACCList.setInvoiceDate(startInvoiceDate+"  To  "+endInvoiceDate);
	 	             dTOSubcontrACCList.setCompanyDivision(str1[3]);
	 	             dTOSubcontrACCList.setRecPostDateCount(Value);
	 	             accInvoicePostDateList.add(dTOSubcontrACCList);
					 }catch( Exception e)
	 	 	    		{
	 	 	    		  e.printStackTrace();
	 	 	    		}
				}
				
	 			return SUCCESS;	
	 		}
	 		
	 		
	 		@SkipValidation
	 		public String subcontrCompanyCodeSSCWGP(){

	 			//subcontrCompanyCodeList=subcontractorChargesManager.getsubcontrCompanyCode(sessionCorpID); 
	 			//InvoicePostDateList=subcontractorChargesManager.getSubcontrCompanyCodeDataList(sessionCorpID);
	 			//accInvoicePostDateList=accountLineManager.findDriverPayableExtractList(sessionCorpID);
	 			InvoicePostDateList=subcontractorChargesManager.getSSCWGPSubcontrACCCompanyCodeDataList(sessionCorpID);
	 			Iterator iterator = InvoicePostDateList.iterator();
	 			Map <String, Integer> subcontrACCCompanyCode = new LinkedHashMap<String, Integer>();
	 			Map <String, Set> subcontrMergeACCCompanyCode = new LinkedHashMap<String, Set>();
	 			int count =0;
	 			String postDate="";
 				String invoiceDate="";
 				String invoiceDateEnd="";
 				String companyDivision="";
 				String companyDivisionGroup="";
	 			String key = new String (""); 
	 			//Set<String> companyDivisionSet = new HashSet<String>();
	 			while(iterator.hasNext()){
	 				 postDate="";
	 				 invoiceDate="";
	 				 invoiceDateEnd="";
	 				 companyDivision="";
	 				 companyDivisionGroup="";
	 				Set<String> companyDivisionSet = new HashSet<String>();
	 				Object subExtract=iterator.next();
	 				try{
 	 	    		    SimpleDateFormat postformat = new SimpleDateFormat("yyyy-MM-dd"); 
 	 	                Date du1 = new Date();
 	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getRecPostDate().toString());
                        postDate = postformat.format(du1);
                        
                        du1 = new Date();
 	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getInvoiceDate().toString());
 	 	                invoiceDate = postformat.format(du1);
 	 	                 
 	 	                du1 = new Date();
	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getInvoiceDateEnd().toString());
	 	                invoiceDateEnd = postformat.format(du1);
 	 	    	        
	 	               companyDivision =	((DTOSubcontrACCCompanyCodeDataList)subExtract).getCompanyDivision().toString().trim();
	 	               companyDivisionGroup =	((DTOSubcontrACCCompanyCodeDataList)subExtract).getCompanyDivisionGroup().toString().trim();
	 	              key=postDate+"~"+invoiceDate+"~"+invoiceDateEnd+"~"+companyDivisionGroup;
	 	               
	 	              if(subcontrACCCompanyCode.containsKey(key)){
	 	            	 count= subcontrACCCompanyCode.get(key);
	 	            	count=count+1;
	 	            	subcontrACCCompanyCode.put(key, count);
	 					} else{
	 						count=1;
	 					subcontrACCCompanyCode.put(key, count);
	 					}
	 	              
	 	             if(subcontrMergeACCCompanyCode.containsKey(key)){
	 	            	 companyDivisionSet = subcontrMergeACCCompanyCode.get(key);
	 	            	 companyDivisionSet.add(companyDivision);
	 	            	subcontrMergeACCCompanyCode.put(key, companyDivisionSet);
	 					} else{
	 						companyDivisionSet.add(companyDivision);
	 						subcontrMergeACCCompanyCode.put(key, companyDivisionSet);
	 					}
	 	              
	 				}
 	 	            catch( Exception e)
 	 	    		{
 	 	    		  e.printStackTrace();
 	 	    		}
	 			}
	 			//System.out.println("\n\n\n\n\n subcontrACCCompanyCode"+subcontrACCCompanyCode);
	 			//System.out.println("\n\n\n\n\n subcontrACCCompanyCode"+subcontrMergeACCCompanyCode);
	 			accInvoicePostDateList = new ArrayList();
	 			Iterator mapIterator1 = subcontrACCCompanyCode.entrySet().iterator();
				while (mapIterator1.hasNext()) {
					DTOSubcontrACCList dTOSubcontrACCList =new DTOSubcontrACCList();	
					Map.Entry entry = (Map.Entry) mapIterator1.next();
					String Key = (String) entry.getKey(); 
					int Value =  (Integer)entry.getValue();
					 String[] str1 = Key.split("~");
					 try{
					 SimpleDateFormat postformat = new SimpleDateFormat("yyyy-MM-dd"); 
					 SimpleDateFormat postformat1 = new SimpleDateFormat("dd-MMM-yyyy");
	 	             Date postDateFormat = new Date();
	 	             postDateFormat = postformat.parse(str1[0]); 
	 	             String postDate1= postformat1.format(postDateFormat); 
	 	             dTOSubcontrACCList.setRecPostDate(postDate1);
	 	             Date du1 = new Date();
	 	             du1 = postformat.parse(str1[1]);
	 	             String  startInvoiceDate=  postformat.format(du1);
	 	             
	 	             startInvoiceDate=  postformat1.format(du1);
	 	             Date du2 = new Date();
	 	             du2 = postformat.parse(str1[2]);
	 	             String  endInvoiceDate=  postformat.format(du2);
	 	             endInvoiceDate= postformat1.format(du2);
	 	             dTOSubcontrACCList.setInvoiceDate(startInvoiceDate+"  To  "+endInvoiceDate);
	 	             if(subcontrMergeACCCompanyCode.containsKey(Key)){
	 	            	Set<String> companyDivisionSet = new HashSet<String>();
	 	            	companyDivisionSet = subcontrMergeACCCompanyCode.get(Key);
	 	            	dTOSubcontrACCList.setCompanyDivision(companyDivisionSet.toString().replace("[", "").replace("]", "").replaceAll(" ", ""));
	 	             }else{
	 	             dTOSubcontrACCList.setCompanyDivision("");
	 	             }
	 	             dTOSubcontrACCList.setRecPostDateCount(Value);
	 	             accInvoicePostDateList.add(dTOSubcontrACCList);
					 }catch( Exception e)
	 	 	    		{
	 	 	    		  e.printStackTrace();
	 	 	    		}
				}
				
	 			return SUCCESS;	
	 			
	 		}
	 		
	 		@SkipValidation
	 		public String mergeSubcontrCompanyCodeSSCW(){

	 			//subcontrCompanyCodeList=subcontractorChargesManager.getsubcontrCompanyCode(sessionCorpID); 
	 			//InvoicePostDateList=subcontractorChargesManager.getSubcontrCompanyCodeDataList(sessionCorpID);
	 			//accInvoicePostDateList=accountLineManager.findDriverPayableExtractList(sessionCorpID);
	 			InvoicePostDateList=subcontractorChargesManager.getMergeSubcontrACCCompanyCodeDataList(sessionCorpID);
	 			Iterator iterator = InvoicePostDateList.iterator();
	 			Map <String, Integer> subcontrACCCompanyCode = new LinkedHashMap<String, Integer>();
	 			Map <String, Set> subcontrMergeACCCompanyCode = new LinkedHashMap<String, Set>();
	 			int count =0;
	 			String postDate="";
 				String invoiceDate="";
 				String invoiceDateEnd="";
 				String companyDivision="";
 				String companyDivisionGroup="";
	 			String key = new String (""); 
	 			//Set<String> companyDivisionSet = new HashSet<String>();
	 			while(iterator.hasNext()){
	 				 postDate="";
	 				 invoiceDate="";
	 				 invoiceDateEnd="";
	 				 companyDivision="";
	 				 companyDivisionGroup="";
	 				Set<String> companyDivisionSet = new HashSet<String>();
	 				Object subExtract=iterator.next();
	 				try{
 	 	    		    SimpleDateFormat postformat = new SimpleDateFormat("yyyy-MM-dd"); 
 	 	                Date du1 = new Date();
 	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getRecPostDate().toString());
                        postDate = postformat.format(du1);
                        
                        du1 = new Date();
 	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getInvoiceDate().toString());
 	 	                invoiceDate = postformat.format(du1);
 	 	                 
 	 	                du1 = new Date();
	 	                du1 = postformat.parse(((DTOSubcontrACCCompanyCodeDataList)subExtract).getInvoiceDateEnd().toString());
	 	                invoiceDateEnd = postformat.format(du1);
 	 	    	        
	 	               companyDivision =	((DTOSubcontrACCCompanyCodeDataList)subExtract).getCompanyDivision().toString().trim();
	 	               companyDivisionGroup =	((DTOSubcontrACCCompanyCodeDataList)subExtract).getCompanyDivisionGroup().toString().trim();
	 	              key=postDate+"~"+invoiceDate+"~"+invoiceDateEnd+"~"+companyDivisionGroup;
	 	               
	 	              if(subcontrACCCompanyCode.containsKey(key)){
	 	            	 count= subcontrACCCompanyCode.get(key);
	 	            	count=count+1;
	 	            	subcontrACCCompanyCode.put(key, count);
	 					} else{
	 						count=1;
	 					subcontrACCCompanyCode.put(key, count);
	 					}
	 	              
	 	             if(subcontrMergeACCCompanyCode.containsKey(key)){
	 	            	 companyDivisionSet = subcontrMergeACCCompanyCode.get(key);
	 	            	 companyDivisionSet.add(companyDivision);
	 	            	subcontrMergeACCCompanyCode.put(key, companyDivisionSet);
	 					} else{
	 						companyDivisionSet.add(companyDivision);
	 						subcontrMergeACCCompanyCode.put(key, companyDivisionSet);
	 					}
	 	              
	 				}
 	 	            catch( Exception e)
 	 	    		{
 	 	    		  e.printStackTrace();
 	 	    		}
	 			}
	 			//System.out.println("\n\n\n\n\n subcontrACCCompanyCode"+subcontrACCCompanyCode);
	 			//System.out.println("\n\n\n\n\n subcontrACCCompanyCode"+subcontrMergeACCCompanyCode);
	 			accInvoicePostDateList = new ArrayList();
	 			Iterator mapIterator1 = subcontrACCCompanyCode.entrySet().iterator();
				while (mapIterator1.hasNext()) {
					DTOSubcontrACCList dTOSubcontrACCList =new DTOSubcontrACCList();	
					Map.Entry entry = (Map.Entry) mapIterator1.next();
					String Key = (String) entry.getKey(); 
					int Value =  (Integer)entry.getValue();
					 String[] str1 = Key.split("~");
					 try{
					 SimpleDateFormat postformat = new SimpleDateFormat("yyyy-MM-dd"); 
					 SimpleDateFormat postformat1 = new SimpleDateFormat("dd-MMM-yyyy");
	 	             Date postDateFormat = new Date();
	 	             postDateFormat = postformat.parse(str1[0]); 
	 	             String postDate1= postformat1.format(postDateFormat); 
	 	             dTOSubcontrACCList.setRecPostDate(postDate1);
	 	             Date du1 = new Date();
	 	             du1 = postformat.parse(str1[1]);
	 	             String  startInvoiceDate=  postformat.format(du1);
	 	             
	 	             startInvoiceDate=  postformat1.format(du1);
	 	             Date du2 = new Date();
	 	             du2 = postformat.parse(str1[2]);
	 	             String  endInvoiceDate=  postformat.format(du2);
	 	             endInvoiceDate= postformat1.format(du2);
	 	             dTOSubcontrACCList.setInvoiceDate(startInvoiceDate+"  To  "+endInvoiceDate);
	 	             if(subcontrMergeACCCompanyCode.containsKey(Key)){
	 	            	Set<String> companyDivisionSet = new HashSet<String>();
	 	            	companyDivisionSet = subcontrMergeACCCompanyCode.get(Key);
	 	            	dTOSubcontrACCList.setCompanyDivision(companyDivisionSet.toString().replace("[", "").replace("]", "").replaceAll(" ", ""));
	 	             }else{
	 	             dTOSubcontrACCList.setCompanyDivision("");
	 	             }
	 	             dTOSubcontrACCList.setRecPostDateCount(Value);
	 	             accInvoicePostDateList.add(dTOSubcontrACCList);
					 }catch( Exception e)
	 	 	    		{
	 	 	    		  e.printStackTrace();
	 	 	    		}
				}
				
	 			return SUCCESS;	
	 			
	 		}
	 		
	 		public  class DTOSubcontrACCList
	 	   {
	 		private String recPostDate; 
	 		private String invoiceDate; 
	 		private String companyDivision;
	 		private Integer recPostDateCount;
			 
			
			public String getRecPostDate() {
				return recPostDate;
			}
			public void setRecPostDate(String recPostDate) {
				this.recPostDate = recPostDate;
			}
			public String getInvoiceDate() {
				return invoiceDate;
			}
			public void setInvoiceDate(String invoiceDate) {
				this.invoiceDate = invoiceDate;
			}
			public String getCompanyDivision() {
				return companyDivision;
			}
			public void setCompanyDivision(String companyDivision) {
				this.companyDivision = companyDivision;
			}
			public Integer getRecPostDateCount() {
				return recPostDateCount;
			}
			public void setRecPostDateCount(Integer recPostDateCount) {
				this.recPostDateCount = recPostDateCount;
			}


	 		
	 	   }
	 		
	 		@SkipValidation
	 		public void subcontractorExtract() throws Exception,EOFException
	 		{/*
	 			
	 			Date recPostingDateFormat = new Date();
	 			String postDate="";
	 			try{  
	 				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
	 	            recPostingDateFormat = df.parse(recPostingDate);
	 	           postDate=df.format(recPostingDateFormat);
	 	        }catch (ParseException e){
	 	          e.printStackTrace();
	 	        } 
	 			List subExtractSeqList=systemDefaultManager.findPayExtractSeq(sessionCorpID);
	 			if(subExtractSeqList.isEmpty())
	 			{ 
	 				subConExtractSeq= new String("00001"); 
	 			}
	 			else if(!subExtractSeqList.isEmpty())
	 			{
	 				subConExtractSeq=subExtractSeqList.get(0).toString();
	 			} 
	 			HttpServletResponse response = getResponse();
	 			ServletOutputStream outputStream = response.getOutputStream(); 
	 			StringBuffer batchDates=new StringBuffer("PAY");
	 			batchDates.append(subConExtractSeq);
	 	        File file=new File(batchDates.toString());
	 	        if(subcontrCompanyCode==null){
	 	        	subcontrCompanyCode="";
	 	        }
	 	        List  list=subcontractorChargesManager.getSubContcExtract(subcontrCompanyCode,sessionCorpID,postDate);  
	 			response.setContentType("text/csv");
	 			//response.setHeader("Cache-Control", "no-cache");
	 			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".txt");
	 			response.setHeader("Pragma", "public");
	 			response.setHeader("Cache-Control", "max-age=0");
	 			out.println("filename="+file.getName()+".txt"+"\t The number of line  \t"+list.size()+"\t is extracted");
	 			Iterator its=list.iterator();
	 			String subIdList="";
	 			String glSubAcc = new String();
	 			String shipNum = new String();
	 			BigDecimal batchTotalPay=new BigDecimal(0); 
	 			//List personIdList=new ArrayList();
	 			List glCodeList=new ArrayList();
	 			//Set personIdset=new TreeSet(); 
	 			Set glCodeSet=new TreeSet(); 
	 			Set glCodeSetForExpNiv=new TreeSet();  
	 			while(its.hasNext())
	 			{ 
	 				Object subExtract=its.next();
	 				if(((DTOForSubContcExtract)subExtract).getAmount()!=null ) 
	 				{
	 					BigDecimal amountForExtract=(BigDecimal)(((DTOForSubContcExtract)subExtract).getAmount());
	 					batchTotalPay=batchTotalPay.add(amountForExtract);
	 				}
	 				else
	 				{
	 					batchTotalPay=new BigDecimal(0);
	 					batchTotalPay=batchTotalPay.add(batchTotalPay);
	 				}
	 				//personIdList.add(((DTOForSubContcExtract)subExtract).getPersonId());
	 			}  
	 			outputStream.write("LEVEL0".getBytes());
	 			outputStream.write((",").getBytes());
	 			try{
	 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
	 	                Date du1 = new Date();
	 	                du1 = postformatLevel11.parse(postDate.toString());
	 	                postformatLevel11 = new SimpleDateFormat("yyyyMM");
	 	                String  approvedDateString = postformatLevel11.format(du1);  
	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
	 	    	        }
	 	                catch( Exception e)
	 	    			{
	 	                SimpleDateFormat postformats = new SimpleDateFormat("yyyyMM"); 
	 	   	 	        StringBuilder postDates = new StringBuilder(postformats.format(new Date()));
	 	   	 	        outputStream.write(("\""+postDates.toString()+"\",").getBytes()); 
	 	    			} 
	 	        
	 	        outputStream.write(("\""+batchTotalPay.toString()+"\"").getBytes());
	 	        outputStream.write("\r\n".getBytes());
	 	        //personIdset.addAll(personIdList);
	 			
	 	       Iterator it=list.iterator();
	 			while(it.hasNext())
	 			{ 
	 				Object subExtract=it.next();
	 				
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
	 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				String subAmount="0.00";
	 				String subActualAmount="0.00";
	 				 boolean positiveExpense=false;
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
	 				}
	 				BigDecimal amount = new BigDecimal(subAmount);
	 				
	 				if(amount.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				subActualAmount=amount.abs().toString();
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				Date du = new Date();
					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString = postformatLevel1.format(du);
					outputStream.write(("\"" + accDateString + "\",").getBytes());
	 				//String s1="S"; 
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}  
	 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				// enhancement end
	 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				

	 				
	 				if((((DTOForSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOForSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
	 	 				try{
	 	 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
	 	 	                Date du1 = new Date();
	 	 	                du1 = postformatLevel11.parse(((DTOForSubContcExtract)subExtract).getApproved().toString());
	 	 	              postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
	 	 	                String  approvedDateString = postformatLevel11.format(du1);  
	 	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
	 	 	    	        }
	 	 	                catch( Exception e)
	 	 	    			{
	 	 	    				e.printStackTrace();
	 	 	    			}
	 	 				}else
	 					{
	 						String s="\""+"\",";
	 						outputStream.write(s.getBytes());
	 					}
	 				Date du2 = new Date();
					SimpleDateFormat postformatLevel12 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString1 = postformatLevel12.format(du2);
					outputStream.write(("\"" + accDateString1 + "\"").getBytes());
					outputStream.write("\r\n".getBytes());
	 	                
	 	                
					outputStream.write("LEVEL4".getBytes());
	 				outputStream.write((",").getBytes()); 
	 				String accountingcode="00";
	 				if(((DTOForSubContcExtract)subExtract).getAccountingCode()!=null) { 
	 					accountingcode= ((DTOForSubContcExtract)subExtract).getAccountingCode().toString() ; 
	 				}
	 				String bucket2="00";
	 				if(((DTOGPSubContcExtract)subExtract).getJob()!=null && (!((((DTOGPSubContcExtract)subExtract).getJob()).toString().equals(""))) ) { 
				    bucket2=refMasterManager.getBucket2(((DTOGPSubContcExtract)subExtract).getJob().toString(),sessionCorpID);
	 				}
	 				if(((DTOForSubContcExtract)subExtract).getDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getDivision().toString().trim().equals("")))){
	 					bucket2=((DTOForSubContcExtract)subExtract).getDivision().toString();
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDivision() + "\",").getBytes());
	 				}
	 				if(((DTOForSubContcExtract)subExtract).getGlCode()!=null) { 
	 					outputStream.write(("\""+ ((DTOForSubContcExtract)subExtract).getGlCode() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				glSubAcc="0000000000000";    
	 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				
	 				 
	 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\"").getBytes());	
	 				}else {
						String s="\""+"\"";
						outputStream.write(s.getBytes());
					} 
					outputStream.write("\r\n".getBytes());

	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	        			if(subIdList.equals("")){
	        				subIdList=(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}else{
	        				subIdList = subIdList +","+(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}
	        		}
	 			}
	 		    outputStream.flush();
		 		outputStream.close();
		 		 int j= subcontractorChargesManager.gpSubContcFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
	 			//Long subExtractSeqUpdate=Long.valueOf(subConExtractSeq); 
	 			//subExtractSeqUpdate=subExtractSeqUpdate+1;
	 			//String val = "0000";
	 			//val = val.substring(subExtractSeqUpdate.toString().length()-1); 
	 			//subConExtractSeq=val+subExtractSeqUpdate.toString(); 
	 			//System.out.println("\n\n\n\n\n payExtractSeqUpdate in action"+subConExtractSeq);
	 			//int i=partnerManager.updateSubContrcExtractSeq(subConExtractSeq,sessionCorpID); 
	 			
	 			
	 			Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			String val = "0000";
	 			val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			int i = systemDefaultManager.updatePayExtractSeq(subConExtractSeq,sessionCorpID);
	 		*/}
	 		
	 		
	 		@SkipValidation
	 		public void payableDriverSSCWGPExtract() throws Exception, EOFException {

	 			String payXferUser = getRequest().getRemoteUser();
	 			//Date payDate1=null;
	 			//sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
	 			//if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
	 				//for (SystemDefault systemDefault : sysDefaultDetail) {
	 					//payDate1=systemDefault.getPostDate1();
	 			//}
	 			//}
	 			
	 			Date payPostingDateFormat = new Date();
	 			Date startInvoiceDateFormat = new Date();
	 			Date endInvoiceDateFormat = new Date();
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				payPostingDateFormat = df.parse(recPostingDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			String[] str1 = invoiceDate.split("To"); 
	 			String startInvoiceDate = str1[0];
	 			startInvoiceDate=startInvoiceDate.trim();
	 			String endInvoiceDate=str1[1];
	 			endInvoiceDate = endInvoiceDate.trim();
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				startInvoiceDateFormat = df.parse(startInvoiceDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				endInvoiceDateFormat = df.parse(endInvoiceDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			StringBuilder startInvoiceDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
					.format(startInvoiceDateFormat));
	 			StringBuilder endInvoiceDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
					.format(endInvoiceDateFormat));
	 			//Date payPostingDateFormat = getPostingDate();
	 			StringBuilder payPostingDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
	 					.format(payPostingDateFormat));
	 			
	 			List subExtractSeqList=systemDefaultManager.findPayExtractSeq(sessionCorpID); 
	 			if(subExtractSeqList.isEmpty())
	 			{ 
	 				subConExtractSeq= new String("00001"); 
	 			}
	 			else if(!subExtractSeqList.isEmpty())
	 			{
	 				subConExtractSeq=subExtractSeqList.get(0).toString();
	 			} 
	 			HttpServletResponse response = getResponse();
	 			ServletOutputStream outputStream = response.getOutputStream(); 
	 			StringBuffer batchDates=new StringBuffer("PAY");
	 			batchDates.append(subConExtractSeq);
	 	        File file=new File(batchDates.toString());
	 	        if(subcontrCompanyCode==null){
	 	        	subcontrCompanyCode="";
	 	        }
	 	        
	 	       String tempPayCompanyCode="";
	 	        for (String object : subcontrCompanyCode.split(",")) {
	 	        	if(tempPayCompanyCode.equalsIgnoreCase("")){
	 	        		tempPayCompanyCode="'"+object+"'";
	 	        	}else{
	 	        		tempPayCompanyCode=tempPayCompanyCode+",'"+object+"'";
	 	        	}
	 			}
	 	       subcontrCompanyCode=tempPayCompanyCode;
	 	      Map<String, String> accountingParameterMap=accountLineManager.getAccountingcodeMap(sessionCorpID);
	 	       
	 	        List  subContcExtractList=subcontractorChargesManager.getSubContcSSCWGPExtract(subcontrCompanyCode,sessionCorpID,payPostingDates.toString(),startInvoiceDates.toString(),endInvoiceDates.toString());  
	 			
	 	       String  shipNumber = writeSSCWGPLevel0(response, outputStream,
	 					payPostingDateFormat, payPostingDates, file,subContcExtractList,startInvoiceDates,endInvoiceDates);
	 			
	 			List payableExtractLevel = accountLineManager.findDriverPayableSSCWGPExtractLevel(
	 					subcontrCompanyCode, sessionCorpID, payPostingDates.toString(), true,startInvoiceDates.toString(),endInvoiceDates.toString());
	 			
	 			writeSSCWGPL1L4(outputStream, payableExtractLevel, true);
	 			String subIdList="";
	 			String glSubAcc = new String();
	 			String shipNum = new String();
	 			Iterator it=subContcExtractList.iterator();
	 			while(it.hasNext())
	 			{ 
	 				Object subExtract=it.next();
	 				
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
	 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				String subAmount="0.00";
	 				String subActualAmount="0.00";
	 				 boolean positiveExpense=false;
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
	 				}
	 				BigDecimal amount = new BigDecimal(subAmount);
	 				
	 				if(amount.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				subActualAmount=amount.abs().toString();
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				Date du = new Date();
					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString = postformatLevel1.format(du);
					outputStream.write(("\"" + accDateString + "\",").getBytes());
	 				//String s1="S"; 
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}  
	 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				// enhancement end
	 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}*/ 
	 				

	 				
	 				if((((DTOForSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOForSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
	 	 				try{
	 	 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
	 	 	                Date du1 = new Date();
	 	 	                du1 = postformatLevel11.parse(((DTOForSubContcExtract)subExtract).getApproved().toString());
	 	 	              postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
	 	 	                String  approvedDateString = postformatLevel11.format(du1);  
	 	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
	 	 	    	        }
	 	 	                catch( Exception e)
	 	 	    			{
	 	 	    				e.printStackTrace();
	 	 	    			}
	 	 				}else
	 					{
	 						String s="\""+"\",";
	 						outputStream.write(s.getBytes());
	 					}
	 				Date du2 = new Date();
					SimpleDateFormat postformatLevel12 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString1 = postformatLevel12.format(du2);
					outputStream.write(("\"" + accDateString1 + "\",").getBytes());
					
					if(((DTOForSubContcExtract)subExtract).getCompanyDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getCompanyDivision().toString().trim().equals("")))){
	 					String accountingCode=((DTOForSubContcExtract)subExtract).getCompanyDivision().toString();
	 					 
	 					if(accountingParameterMap.containsKey(accountingCode)){
	 					  accountingCode=accountingParameterMap.get(accountingCode);
	 					 }
	 					outputStream.write(("\"" + accountingCode + "\"").getBytes());
	 				}else{
	 					String s="\""+"\"";
 						outputStream.write(s.getBytes());
	 				}
					outputStream.write("\r\n".getBytes());
	 	                
	 	                
					outputStream.write("LEVEL4".getBytes());
	 				outputStream.write((",").getBytes()); 
	 				if(((DTOForSubContcExtract)subExtract).getCompanyDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getCompanyDivision().toString().trim().equals("")))){
	 					String accountingCode=((DTOForSubContcExtract)subExtract).getCompanyDivision().toString();
	 					 
	 					if(accountingParameterMap.containsKey(accountingCode)){
	 					  accountingCode=accountingParameterMap.get(accountingCode);
	 					 }
	 					outputStream.write(("\"" + accountingCode + "\",").getBytes());
	 				}else{
	 					String s="\""+"\",";
 						outputStream.write(s.getBytes());
	 				}
	 				/*String accountingcode="00";
	 				if(((DTOForSubContcExtract)subExtract).getAccountingCode()!=null) { 
	 					accountingcode= ((DTOForSubContcExtract)subExtract).getAccountingCode().toString() ; 
	 				}
	 				String bucket2="00";
	 				if(((DTOGPSubContcExtract)subExtract).getJob()!=null && (!((((DTOGPSubContcExtract)subExtract).getJob()).toString().equals(""))) ) { 
				    bucket2=refMasterManager.getBucket2(((DTOGPSubContcExtract)subExtract).getJob().toString(),sessionCorpID);
	 				}
	 				if(((DTOForSubContcExtract)subExtract).getDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getDivision().toString().trim().equals("")))){
	 					bucket2=((DTOForSubContcExtract)subExtract).getDivision().toString();
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDivision() + "\",").getBytes());
	 				}*/
	 				if(((DTOForSubContcExtract)subExtract).getGlCode()!=null) { 
	 					outputStream.write(("\""+ ((DTOForSubContcExtract)subExtract).getGlCode() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				glSubAcc="0000000000000";    
	 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				
	 				 
	 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}*/
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
	 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				boolean positiveExpenseLevel4=false;
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
	 				}
	 				BigDecimal amountLevel4 = new BigDecimal(subAmount);
	 				
	 				if(amountLevel4.doubleValue()<0){
	 					positiveExpenseLevel4=false;
	 				}
	 				else {
	 					positiveExpenseLevel4=true;
	 				}
	 				outputStream.write(("\""+(positiveExpenseLevel4?"VO":"AD")+"\",").getBytes());
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\"").getBytes());
					}else {
						String s="\""+"\"";
						outputStream.write(s.getBytes());
					} 
					outputStream.write("\r\n".getBytes());

	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	        			if(subIdList.equals("")){
	        				subIdList=(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}else{
	        				subIdList = subIdList +","+(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}
	        		}
	 			}
	 			
	 			outputStream.flush();
	 			outputStream.close();
	 			if(!(subIdList.equals(""))){
	 			int k= subcontractorChargesManager.subContcSSCWGPFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
	 			}
	 	    	if(!shipNumber.equals(""))
	 	        {
	 	    		shipNumber=shipNumber.substring(0, shipNumber.length()-1);
	 	        }
	 	        else if(shipNumber.equals(""))
	 	        {
	 	        	shipNumber =new String("''");
	 	        }  
	 				int j = accountLineManager.payableDriverSSCWGPinvoiceUpdate(batchDates
	 						.toString()
	 						+ ".txt", shipNumber, payXferUser, payPostingDates
	 						.toString(),sessionCorpID); 
	 			//Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			//payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			//String val = "0000";
	 			//val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			//subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			//int i = partnerManager.updateSubContrcExtractSeq(subConExtractSeq,
	 					//sessionCorpID);
	 			Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			String val = "0000";
	 			val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			int i = systemDefaultManager.updatePayExtractSeq(subConExtractSeq,sessionCorpID);
	 		
	 			
	 		}
	 		
	 		@SkipValidation
	 		public void payableDriverMergeExtract() throws Exception, EOFException {

	 			String payXferUser = getRequest().getRemoteUser();
	 			//Date payDate1=null;
	 			//sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
	 			//if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
	 				//for (SystemDefault systemDefault : sysDefaultDetail) {
	 					//payDate1=systemDefault.getPostDate1();
	 			//}
	 			//}
	 			
	 			Date payPostingDateFormat = new Date();
	 			Date startInvoiceDateFormat = new Date();
	 			Date endInvoiceDateFormat = new Date();
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				payPostingDateFormat = df.parse(recPostingDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			String[] str1 = invoiceDate.split("To"); 
	 			String startInvoiceDate = str1[0];
	 			startInvoiceDate=startInvoiceDate.trim();
	 			String endInvoiceDate=str1[1];
	 			endInvoiceDate = endInvoiceDate.trim();
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				startInvoiceDateFormat = df.parse(startInvoiceDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				endInvoiceDateFormat = df.parse(endInvoiceDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			StringBuilder startInvoiceDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
					.format(startInvoiceDateFormat));
	 			StringBuilder endInvoiceDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
					.format(endInvoiceDateFormat));
	 			//Date payPostingDateFormat = getPostingDate();
	 			StringBuilder payPostingDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
	 					.format(payPostingDateFormat));
	 			
	 			List subExtractSeqList=systemDefaultManager.findPayExtractSeq(sessionCorpID); 
	 			if(subExtractSeqList.isEmpty())
	 			{ 
	 				subConExtractSeq= new String("00001"); 
	 			}
	 			else if(!subExtractSeqList.isEmpty())
	 			{
	 				subConExtractSeq=subExtractSeqList.get(0).toString();
	 			} 
	 			HttpServletResponse response = getResponse();
	 			ServletOutputStream outputStream = response.getOutputStream(); 
	 			StringBuffer batchDates=new StringBuffer("PAY");
	 			batchDates.append(subConExtractSeq);
	 	        File file=new File(batchDates.toString());
	 	        if(subcontrCompanyCode==null){
	 	        	subcontrCompanyCode="";
	 	        }
	 	        
	 	       String tempPayCompanyCode="";
	 	        for (String object : subcontrCompanyCode.split(",")) {
	 	        	if(tempPayCompanyCode.equalsIgnoreCase("")){
	 	        		tempPayCompanyCode="'"+object+"'";
	 	        	}else{
	 	        		tempPayCompanyCode=tempPayCompanyCode+",'"+object+"'";
	 	        	}
	 			}
	 	       subcontrCompanyCode=tempPayCompanyCode;
	 	      Map<String, String> accountingParameterMap=accountLineManager.getAccountingcodeMap(sessionCorpID);
	 	       
	 	        List  subContcExtractList=subcontractorChargesManager.getSubContcMergeExtract(subcontrCompanyCode,sessionCorpID,payPostingDates.toString(),startInvoiceDates.toString(),endInvoiceDates.toString());  
	 			
	 	       String  shipNumber = writeMergeLevel0(response, outputStream,
	 					payPostingDateFormat, payPostingDates, file,subContcExtractList,startInvoiceDates,endInvoiceDates);
	 			
	 			List payableExtractLevel = accountLineManager.findDriverPayableMergeExtractLevel(
	 					subcontrCompanyCode, sessionCorpID, payPostingDates.toString(), true,startInvoiceDates.toString(),endInvoiceDates.toString());
	 			
	 			writeMergeL1L4(outputStream, payableExtractLevel, true);
	 			String subIdList="";
	 			String glSubAcc = new String();
	 			String shipNum = new String();
	 			Iterator it=subContcExtractList.iterator();
	 			while(it.hasNext())
	 			{ 
	 				Object subExtract=it.next();
	 				
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
	 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				String subAmount="0.00";
	 				String subActualAmount="0.00";
	 				 boolean positiveExpense=false;
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
	 				}
	 				BigDecimal amount = new BigDecimal(subAmount);
	 				
	 				if(amount.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				subActualAmount=amount.abs().toString();
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				Date du = new Date();
					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString = postformatLevel1.format(du);
					outputStream.write(("\"" + accDateString + "\",").getBytes());
	 				//String s1="S"; 
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}  
	 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				// enhancement end
	 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}*/ 
	 				

	 				
	 				if((((DTOForSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOForSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
	 	 				try{
	 	 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
	 	 	                Date du1 = new Date();
	 	 	                du1 = postformatLevel11.parse(((DTOForSubContcExtract)subExtract).getApproved().toString());
	 	 	              postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
	 	 	                String  approvedDateString = postformatLevel11.format(du1);  
	 	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
	 	 	    	        }
	 	 	                catch( Exception e)
	 	 	    			{
	 	 	    				e.printStackTrace();
	 	 	    			}
	 	 				}else
	 					{
	 						String s="\""+"\",";
	 						outputStream.write(s.getBytes());
	 					}
	 				Date du2 = new Date();
					SimpleDateFormat postformatLevel12 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString1 = postformatLevel12.format(du2);
					outputStream.write(("\"" + accDateString1 + "\",").getBytes());
					
					if(((DTOForSubContcExtract)subExtract).getCompanyDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getCompanyDivision().toString().trim().equals("")))){
	 					String accountingCode=((DTOForSubContcExtract)subExtract).getCompanyDivision().toString();
	 					 
	 					if(accountingParameterMap.containsKey(accountingCode)){
	 					  accountingCode=accountingParameterMap.get(accountingCode);
	 					 }
	 					outputStream.write(("\"" + accountingCode + "\"").getBytes());
	 				}else{
	 					String s="\""+"\"";
 						outputStream.write(s.getBytes());
	 				}
					outputStream.write("\r\n".getBytes());
	 	                
	 	                
					outputStream.write("LEVEL4".getBytes());
	 				outputStream.write((",").getBytes()); 
	 				if(((DTOForSubContcExtract)subExtract).getCompanyDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getCompanyDivision().toString().trim().equals("")))){
	 					String accountingCode=((DTOForSubContcExtract)subExtract).getCompanyDivision().toString();
	 					 
	 					if(accountingParameterMap.containsKey(accountingCode)){
	 					  accountingCode=accountingParameterMap.get(accountingCode);
	 					 }
	 					outputStream.write(("\"" + accountingCode + "\",").getBytes());
	 				}else{
	 					String s="\""+"\",";
 						outputStream.write(s.getBytes());
	 				}
	 				/*String accountingcode="00";
	 				if(((DTOForSubContcExtract)subExtract).getAccountingCode()!=null) { 
	 					accountingcode= ((DTOForSubContcExtract)subExtract).getAccountingCode().toString() ; 
	 				}
	 				String bucket2="00";
	 				if(((DTOGPSubContcExtract)subExtract).getJob()!=null && (!((((DTOGPSubContcExtract)subExtract).getJob()).toString().equals(""))) ) { 
				    bucket2=refMasterManager.getBucket2(((DTOGPSubContcExtract)subExtract).getJob().toString(),sessionCorpID);
	 				}
	 				if(((DTOForSubContcExtract)subExtract).getDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getDivision().toString().trim().equals("")))){
	 					bucket2=((DTOForSubContcExtract)subExtract).getDivision().toString();
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDivision() + "\",").getBytes());
	 				}*/
	 				if(((DTOForSubContcExtract)subExtract).getGlCode()!=null) { 
	 					outputStream.write(("\""+ ((DTOForSubContcExtract)subExtract).getGlCode() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				glSubAcc="0000000000000";    
	 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				
	 				 
	 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}*/
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\"").getBytes());	
	 				}else {
						String s="\""+"\"";
						outputStream.write(s.getBytes());
					} 
					outputStream.write("\r\n".getBytes());

	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	        			if(subIdList.equals("")){
	        				subIdList=(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}else{
	        				subIdList = subIdList +","+(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}
	        		}
	 			}
	 			
	 			outputStream.flush();
	 			outputStream.close();
	 			if(!(subIdList.equals(""))){
	 			int k= subcontractorChargesManager.gpSubContcFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
	 			}
	 	    	if(!shipNumber.equals(""))
	 	        {
	 	    		shipNumber=shipNumber.substring(0, shipNumber.length()-1);
	 	        }
	 	        else if(shipNumber.equals(""))
	 	        {
	 	        	shipNumber =new String("''");
	 	        }  
	 				int j = accountLineManager.payableDriverinvoiceUpdate(batchDates
	 						.toString()
	 						+ ".txt", shipNumber, payXferUser, payPostingDates
	 						.toString(),sessionCorpID); 
	 			//Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			//payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			//String val = "0000";
	 			//val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			//subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			//int i = partnerManager.updateSubContrcExtractSeq(subConExtractSeq,
	 					//sessionCorpID);
	 			Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			String val = "0000";
	 			val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			int i = systemDefaultManager.updatePayExtractSeq(subConExtractSeq,sessionCorpID);
	 		
	 			
	 		}
	 		
	 		
	 		private String writeSSCWGPLevel0(HttpServletResponse response,
	 				ServletOutputStream outputStream, Date payPostingDateFormat,
	 				StringBuilder payPostingDates, File file,List subContcExtractList,StringBuilder startInvoiceDates, StringBuilder endInvoiceDates)throws Exception, EOFException {

	 			 
	 			List list = accountLineManager.getDriverPayableSSCWGPExtract(subcontrCompanyCode,
	 					sessionCorpID, payPostingDates.toString(),startInvoiceDates.toString(),endInvoiceDates.toString());
	 			response.setContentType("text/csv");
	 			response.setHeader("Content-Disposition", "attachment; filename="
	 					+ file.getName() + ".txt");
	 			response.setHeader("Pragma", "public");
	 			response.setHeader("Cache-Control", "max-age=0");
	 			out
	 					.println("filename=" + file.getName() + ".txt"
	 							+ "\t The number of line  \t" + list.size()
	 							+ "\t is extracted");
	 			Iterator its = list.iterator();
	 			BigDecimal batchTotalPay = new BigDecimal(0);
	 			StringBuffer shipNumInvoice=new StringBuffer();
	 			Set ShipNumberSet = new TreeSet();

	 			while (its.hasNext()) {
	 				Object payExtract = its.next();
	 				if (((DTOForPayable) payExtract).getActualExpense() != null) {
	 					BigDecimal expenseForPExtrac = (BigDecimal) (((DTOForPayable) payExtract)
	 							.getActualExpense());
	 					batchTotalPay = batchTotalPay.add(expenseForPExtrac);
	 				} else {
	 					BigDecimal  batchTotalPay1 = new BigDecimal(0);
	 					batchTotalPay = batchTotalPay.add(batchTotalPay1);
	 				}
	 				 shipNumInvoice=shipNumInvoice.append("'");
	 				 shipNumInvoice=shipNumInvoice.append((String)(((DTOForPayable) payExtract).getId()).toString());
	 				 shipNumInvoice=shipNumInvoice.append("',");
	 				//ShipNumberList.add(((DTOForPayable) payExtract).getShipNumber());
	 			}
	 			
	 			Iterator it = subContcExtractList.iterator();
	 			while (it.hasNext()) {
	 				 
	 				Object subExtract=it.next();
	 				if(((DTOForSubContcExtract)subExtract).getAmount()!=null ) 
	 				{
	 					BigDecimal amountForExtract=(BigDecimal)(((DTOForSubContcExtract)subExtract).getAmount());
	 					batchTotalPay=batchTotalPay.add(amountForExtract);
	 				}
	 				else
	 				{
	 					BigDecimal  batchTotalPay1=new BigDecimal(0);
	 					batchTotalPay=batchTotalPay.add(batchTotalPay1);
	 				}
	 				//personIdList.add(((DTOForSubContcExtract)subExtract).getPersonId());
	 				
	 			}
	 			//ShipNumberSet.addAll(ShipNumberList);
	 			outputStream.write("LEVEL0".getBytes());
	 			outputStream.write((",").getBytes());
	 			SimpleDateFormat postformats = new SimpleDateFormat("yyyyMM");
	 			StringBuilder postDates = new StringBuilder(postformats
	 					.format(payPostingDateFormat));
	 			outputStream.write(("\"" + postDates.toString() + "\",").getBytes());
	 			outputStream.write(("\"" + batchTotalPay.toString() + "\"").getBytes());
	 			outputStream.write("\r\n".getBytes());
	 			String shipInvoiceUpdate=new String(shipNumInvoice);
	 			return shipInvoiceUpdate;
	 			
	 		}
	 		
	 		private String writeMergeLevel0(HttpServletResponse response,
	 				ServletOutputStream outputStream, Date payPostingDateFormat,
	 				StringBuilder payPostingDates, File file,List subContcExtractList,StringBuilder startInvoiceDates, StringBuilder endInvoiceDates)throws Exception, EOFException {

	 			 
	 			List list = accountLineManager.getDriverPayableMergeExtract(subcontrCompanyCode,
	 					sessionCorpID, payPostingDates.toString(),startInvoiceDates.toString(),endInvoiceDates.toString());
	 			response.setContentType("text/csv");
	 			response.setHeader("Content-Disposition", "attachment; filename="
	 					+ file.getName() + ".txt");
	 			response.setHeader("Pragma", "public");
	 			response.setHeader("Cache-Control", "max-age=0");
	 			out
	 					.println("filename=" + file.getName() + ".txt"
	 							+ "\t The number of line  \t" + list.size()
	 							+ "\t is extracted");
	 			Iterator its = list.iterator();
	 			BigDecimal batchTotalPay = new BigDecimal(0);
	 			StringBuffer shipNumInvoice=new StringBuffer();
	 			Set ShipNumberSet = new TreeSet();

	 			while (its.hasNext()) {
	 				Object payExtract = its.next();
	 				if (((DTOForPayable) payExtract).getActualExpense() != null) {
	 					BigDecimal expenseForPExtrac = (BigDecimal) (((DTOForPayable) payExtract)
	 							.getActualExpense());
	 					batchTotalPay = batchTotalPay.add(expenseForPExtrac);
	 				} else {
	 					BigDecimal  batchTotalPay1 = new BigDecimal(0);
	 					batchTotalPay = batchTotalPay.add(batchTotalPay1);
	 				}
	 				 shipNumInvoice=shipNumInvoice.append("'");
	 				 shipNumInvoice=shipNumInvoice.append((String)(((DTOForPayable) payExtract).getId()).toString());
	 				 shipNumInvoice=shipNumInvoice.append("',");
	 				//ShipNumberList.add(((DTOForPayable) payExtract).getShipNumber());
	 			}
	 			
	 			Iterator it = subContcExtractList.iterator();
	 			while (it.hasNext()) {
	 				 
	 				Object subExtract=it.next();
	 				if(((DTOForSubContcExtract)subExtract).getAmount()!=null ) 
	 				{
	 					BigDecimal amountForExtract=(BigDecimal)(((DTOForSubContcExtract)subExtract).getAmount());
	 					batchTotalPay=batchTotalPay.add(amountForExtract);
	 				}
	 				else
	 				{
	 					BigDecimal  batchTotalPay1=new BigDecimal(0);
	 					batchTotalPay=batchTotalPay.add(batchTotalPay1);
	 				}
	 				//personIdList.add(((DTOForSubContcExtract)subExtract).getPersonId());
	 				
	 			}
	 			//ShipNumberSet.addAll(ShipNumberList);
	 			outputStream.write("LEVEL0".getBytes());
	 			outputStream.write((",").getBytes());
	 			SimpleDateFormat postformats = new SimpleDateFormat("yyyyMM");
	 			StringBuilder postDates = new StringBuilder(postformats
	 					.format(payPostingDateFormat));
	 			outputStream.write(("\"" + postDates.toString() + "\",").getBytes());
	 			outputStream.write(("\"" + batchTotalPay.toString() + "\"").getBytes());
	 			outputStream.write("\r\n".getBytes());
	 			String shipInvoiceUpdate=new String(shipNumInvoice);
	 			return shipInvoiceUpdate;
	 			
	 		}
	 		
	 		private void writeSSCWGPL1L4(ServletOutputStream outputStream, List payableExtractLevel, boolean positiveExpense) throws IOException {
	 			SortedMap extractLevel1Map = new TreeMap();
	 			SortedMap extractLevel4Map = new TreeMap();
	 			BigDecimal sumActualExpense = new BigDecimal(0.0);
	 			BigDecimal sumlocalAmount = new BigDecimal(0.0);
	 			String shipNumberLevel4 = new String();
	 			String payGlLevel4 = new String();
	 			String actualExpenseLevel4 = new String();
	 			String bucketLevel4 = new String();
	 			String key = new String("");
	 			String goupkey = new String("");
	 			String samekey = new String("");
	 			String bucket = new String();
	 			String payGl = new String();
	 			String glSubAcc = new String();
	 			String expense = new String();
	 			String shipNum = new String();
	 			String companyDivision = new String();
	 			String companyDivisionExtract2Group= new String("SSC,DUL,FOR,LAU,LAM,GAI,BAL,INT,WAS,ALE");
	 			String accCompanyDivision = new String();
	 			//String shipmentNumber = new String();
	 			//String partnerType = new String();
	 			//String stage = new String();
	 			//String job = new String();
	 			//String wareHouse = new String();
	 			
	 			Map<String, String> accountingParameterMap=accountLineManager.getAccountingcodeMap(sessionCorpID);
	 			
	 			Iterator levelIterator = payableExtractLevel.iterator();
	 			while (levelIterator.hasNext()) {
	 				Object levelObject = levelIterator.next();
	 				String invoiceNumber = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getInvoiceNumber();
	 				String actgCode = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getActgCode();			
	 				BigDecimal actualExpense = (BigDecimal) ((DTOPayableExtractLevel) levelObject)
	 						.getActualExpense();
	 				Date invoiceDate = (Date) ((DTOPayableExtractLevel) levelObject)
	 						.getInvoiceDate();
	 				payGl = (String) ((DTOPayableExtractLevel) levelObject).getPayGl();
	 				String shipNumber = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getShipNumber();
	 				BigDecimal localAmount = (BigDecimal) ((DTOPayableExtractLevel) levelObject)
	 				.getLocalAmount();
	 				
	 				 // add for #6938 - AP file changes for Solmon start
	 				Date receivedDate = new Date();
	 				if(((DTOPayableExtractLevel) levelObject).getReceivedDate() != null){
	 						receivedDate = (Date) ((DTOPayableExtractLevel) levelObject).getReceivedDate();
	 				}else{
	 					receivedDate = (Date) ((DTOPayableExtractLevel) levelObject).getInvoiceDate();
	 				}
	 				
	 				//bucket = extractBucket(levelObject);
	 				// New Code added
	 				//shipmentNumber = ((DTOPayableExtractLevel) levelObject).getShipmentType()!=null ?((DTOPayableExtractLevel) levelObject).getShipmentType().toString():"";
	 				//partnerType = ((DTOPayableExtractLevel) levelObject).getPartnerType()!=null ?((DTOPayableExtractLevel) levelObject).getPartnerType().toString():"";
	 				//stage = ((DTOPayableExtractLevel) levelObject).getStage()!=null ?((DTOPayableExtractLevel) levelObject).getStage().toString():"";
	 				//job = ((DTOPayableExtractLevel) levelObject).getJob()!=null ?((DTOPayableExtractLevel) levelObject).getJob().toString():"";
	 				//wareHouse = ((DTOPayableExtractLevel) levelObject).getWareHouse()!=null ?((DTOPayableExtractLevel) levelObject).getWareHouse().toString():"";
	 				// ends
	 				accCompanyDivision = ((DTOPayableExtractLevel) levelObject).getAccCompanyDivision()!=null ?((DTOPayableExtractLevel) levelObject).getAccCompanyDivision().toString():"";
	 				companyDivision = ((DTOPayableExtractLevel) levelObject).getCompanyDivision()!=null ?((DTOPayableExtractLevel) levelObject).getCompanyDivision().toString():"";
	 				key = invoiceNumber.trim() + "==" + actgCode.trim() + "==" +accCompanyDivision ;

	 				if (key.equals(samekey)) {
	 					sumActualExpense = actualExpense.add(sumActualExpense);
	 					sumlocalAmount=localAmount.add(sumlocalAmount);
	 				} else if (!key.equals(samekey)) {
	 					sumActualExpense = actualExpense;
	 					sumlocalAmount=localAmount;
	 				}

	 				samekey = invoiceNumber.trim() + "==" + actgCode.trim() + "==" +accCompanyDivision ;
	 				List level4List = (List)extractLevel4Map.get(key);
	 				if (level4List == null || level4List.isEmpty()) {
	 					level4List = new ArrayList();
	 				}			
	 				try {
	 					String Level1MapValue = sumActualExpense.toString() + "=="
	 							+ invoiceDate.toString() + "==" + actgCode + "=="
	 							+ invoiceNumber +"==" + sumlocalAmount.toString() +"=="+ receivedDate.toString() + "==" +accCompanyDivision +  "==" +shipNumber ;
	 					String Level4MapValue = shipNumber + "==" + payGl
	 							+ "==" + actualExpense.toString() + "==" + bucket +"=="+ localAmount.toString() + "==" +accCompanyDivision+ "==" +actgCode + "=="+invoiceNumber
	 							;
	 					level4List.add(Level4MapValue);
	 					if(extractLevel1Map.containsKey(key)){
	 						String detailLevel1 = (String) extractLevel1Map.get(key);
	 						String arraydetailLevel1[] = detailLevel1.split("==");
	 						String soOldCompanyDivision=arraydetailLevel1[6];
	 						String soOldShipNumber=arraydetailLevel1[7];
	 						if(soOldShipNumber!=null && (!(soOldShipNumber.trim().equals(""))) && shipNumber!=null && (!(shipNumber.trim().equals(""))) && (!(shipNumber.trim().equals(soOldShipNumber.trim())))){
	 						if(soOldCompanyDivision!=null && (!(soOldCompanyDivision.trim().equals(""))) && accCompanyDivision!=null && (!(accCompanyDivision.trim().equals(""))) && (!(accCompanyDivision.trim().equals(soOldCompanyDivision.trim()))) && companyDivisionExtract2Group.indexOf(soOldCompanyDivision)>=0 && companyDivisionExtract2Group.indexOf(accCompanyDivision)>=0 ){
	 							goupkey=key; 
	 							
	 						}
	 						}
	 						if(key.equals(goupkey)){
	 							accCompanyDivision="FOR";
	 							Level1MapValue = sumActualExpense.toString() + "=="
	 		 							+ invoiceDate.toString() + "==" + actgCode + "=="
	 		 							+ invoiceNumber +"==" + sumlocalAmount.toString() +"=="+ receivedDate.toString() + "==" +accCompanyDivision +  "==" +shipNumber ;		
	 						}
	 					}
	 					extractLevel1Map.put(key, Level1MapValue);
	 					extractLevel4Map.put(key, level4List);
	 				} catch (Exception e) {
	 					e.printStackTrace();
	 				}

	 			} 
	 			Iterator mapIterator = extractLevel1Map.entrySet().iterator();
	 			while (mapIterator.hasNext()) {
	 				Map.Entry entry = (Map.Entry) mapIterator.next();
	 				String invoiceActgCode = (String) entry.getKey();
	 				String detailLevel1 = (String) extractLevel1Map
	 						.get(invoiceActgCode);
	 				String arraydetailLevel1[] = detailLevel1.split("==");
	 				String totalexpShip = arraydetailLevel1[0];
	 				BigDecimal ActualExpense = new BigDecimal(totalexpShip);
	 				if(ActualExpense.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				totalexpShip=ActualExpense.abs().toString();
	 				String invoiceDate = arraydetailLevel1[1];
	 				String actgCode = arraydetailLevel1[2];
	 				String invoiceNumber = arraydetailLevel1[3];
	 				String sumlocalExpense=arraydetailLevel1[4];			
	 				BigDecimal actualLocalExpSum = new BigDecimal(sumlocalExpense);
	 				sumlocalExpense=actualLocalExpSum.abs().toString();
	 				String recivedDate ="";
	 				if(arraydetailLevel1[5] !=null && (!(arraydetailLevel1[5].toString().trim().equals("")))){
	 				 recivedDate = arraydetailLevel1[5];
	 				}else{
	 				 recivedDate = arraydetailLevel1[1];	
	 				}
	 				String accountingCode=arraydetailLevel1[6];
	 				accountingCode=accountingParameterMap.get(accountingCode);
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				outputStream.write(("\"" + actgCode + "\",").getBytes());
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				outputStream.write(("\"" + totalexpShip.toString() + "\",")
	 						.getBytes());
	 				try {
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat(
	 							"yyyy-MM-dd");
	 					Date du = new Date();
	 					du = postformatLevel1.parse(invoiceDate.toString());
	 					postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String invoiceDatesString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + invoiceDatesString + "\",")
	 							.getBytes());
	 				} catch (Exception e) {
	 					outputStream.write(("\"" + "" + "\",")
	 							.getBytes());
	 					e.printStackTrace();
	 				}
	 				outputStream.write(("\"" + invoiceNumber + "\",").getBytes());
	 				outputStream.write(("\"" + sumlocalExpense + "\",").getBytes());
	 				try {
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd");
	 					Date du = new Date(); 
	 					du = postformatLevel1.parse(recivedDate.toString());
	 					postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String recivedDateString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + recivedDateString + "\",").getBytes());
	 				} catch (Exception e) {
	 					outputStream.write(("\"" + "" + "\",")
	 							.getBytes());
	 					System.out.println("Recived Date is not found");
	 				}
	 				
	 				try {
	 					Date du = new Date();
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String accDateString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + accDateString + "\",").getBytes());
	 				} catch (Exception e) {
	 					outputStream.write(("\"" + "" + "\",")
	 							.getBytes());
	 					System.out.println("ACC Date is not found");
	 				}
	 				outputStream.write(("\"" + accountingCode + "\"").getBytes());
	 				outputStream.write("\r\n".getBytes()); 
	 				
	 				List detail4List = (List) extractLevel4Map
	 				.get(invoiceActgCode);
	 			
	 				for (int i = 0; i < detail4List.size(); i++) {
	 					String level4String = (String)detail4List.get(i);
	 					
	 					String arrayDetailLevel4[] = level4String.split("==");
	 					shipNum = arrayDetailLevel4[0];
	 					payGl = arrayDetailLevel4[1];
	 					expense = arrayDetailLevel4[2];
	 					BigDecimal ActualExpenseLevel4 = new BigDecimal(expense);
	 					boolean positiveExpenseLevel4=true;
	 					if(ActualExpenseLevel4.doubleValue()<0){
	 						positiveExpenseLevel4=false;
		 				}
		 				else {
		 					positiveExpenseLevel4=true;
		 				}
	 					if(positiveExpense){
	 						expense=expense; 
	 					}
	 					else if(!positiveExpense){
	 						expense=ActualExpenseLevel4.abs().toString();			
	 					}
	 					accountingCode=arrayDetailLevel4[5];
	 					accountingCode=accountingParameterMap.get(accountingCode);
	 					String actgcodeLevel4="";
	 					actgcodeLevel4=arrayDetailLevel4[6];
	 					String invoiceNoLevel4="";
	 					invoiceNoLevel4=arrayDetailLevel4[7];
	 					glSubAcc = "0000000000000"; 
	 					String localExpense=arrayDetailLevel4[4];
	 					BigDecimal actualLocalExpense = new BigDecimal(localExpense);
	 					localExpense=actualLocalExpense.abs().toString();
	 					outputStream.write("LEVEL4".getBytes());
	 					outputStream.write((",").getBytes());
	 					outputStream.write(("\"" + accountingCode + "\",").getBytes());
	 					outputStream.write(("\"" + payGl + "\",").getBytes());
	 					outputStream.write(("\"" + glSubAcc + "\",").getBytes());
	 					outputStream.write(("\"" + expense + "\",").getBytes());
	 					outputStream.write(("\"" + shipNum + "\",").getBytes());
	 					outputStream.write(("\"" + localExpense + "\",").getBytes());
	 					outputStream.write(("\"" + actgcodeLevel4 + "\",").getBytes());
	 					outputStream.write(("\""+(positiveExpenseLevel4?"VO":"AD")+"\",").getBytes());
	 					outputStream.write(("\"" + invoiceNoLevel4 + "\"").getBytes());
	 					outputStream.write("\r\n".getBytes());
	 				}
	 			}
	 		}
	 		
	 		private void writeMergeL1L4(ServletOutputStream outputStream, List payableExtractLevel, boolean positiveExpense) throws IOException {
	 			SortedMap extractLevel1Map = new TreeMap();
	 			SortedMap extractLevel4Map = new TreeMap();
	 			BigDecimal sumActualExpense = new BigDecimal(0.0);
	 			BigDecimal sumlocalAmount = new BigDecimal(0.0);
	 			String shipNumberLevel4 = new String();
	 			String payGlLevel4 = new String();
	 			String actualExpenseLevel4 = new String();
	 			String bucketLevel4 = new String();
	 			String key = new String("");
	 			String goupkey = new String("");
	 			String samekey = new String("");
	 			String bucket = new String();
	 			String payGl = new String();
	 			String glSubAcc = new String();
	 			String expense = new String();
	 			String shipNum = new String();
	 			String companyDivision = new String();
	 			String companyDivisionExtract2Group= new String("SSC,DUL,FOR,LAU,LAM,GAI,BAL,INT,WAS,ALE");
	 			String accCompanyDivision = new String();
	 			//String shipmentNumber = new String();
	 			//String partnerType = new String();
	 			//String stage = new String();
	 			//String job = new String();
	 			//String wareHouse = new String();
	 			
	 			Map<String, String> accountingParameterMap=accountLineManager.getAccountingcodeMap(sessionCorpID);
	 			
	 			Iterator levelIterator = payableExtractLevel.iterator();
	 			while (levelIterator.hasNext()) {
	 				Object levelObject = levelIterator.next();
	 				String invoiceNumber = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getInvoiceNumber();
	 				String actgCode = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getActgCode();			
	 				BigDecimal actualExpense = (BigDecimal) ((DTOPayableExtractLevel) levelObject)
	 						.getActualExpense();
	 				Date invoiceDate = (Date) ((DTOPayableExtractLevel) levelObject)
	 						.getInvoiceDate();
	 				payGl = (String) ((DTOPayableExtractLevel) levelObject).getPayGl();
	 				String shipNumber = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getShipNumber();
	 				BigDecimal localAmount = (BigDecimal) ((DTOPayableExtractLevel) levelObject)
	 				.getLocalAmount();
	 				
	 				 // add for #6938 - AP file changes for Solmon start
	 				Date receivedDate = new Date();
	 				if(((DTOPayableExtractLevel) levelObject).getReceivedDate() != null){
	 					 receivedDate = (Date) ((DTOPayableExtractLevel) levelObject).getReceivedDate();
	 				}else{
	 					 receivedDate = (Date) ((DTOPayableExtractLevel) levelObject).getInvoiceDate();
	 				}
	 				
	 				//bucket = extractBucket(levelObject);
	 				// New Code added
	 				//shipmentNumber = ((DTOPayableExtractLevel) levelObject).getShipmentType()!=null ?((DTOPayableExtractLevel) levelObject).getShipmentType().toString():"";
	 				//partnerType = ((DTOPayableExtractLevel) levelObject).getPartnerType()!=null ?((DTOPayableExtractLevel) levelObject).getPartnerType().toString():"";
	 				//stage = ((DTOPayableExtractLevel) levelObject).getStage()!=null ?((DTOPayableExtractLevel) levelObject).getStage().toString():"";
	 				//job = ((DTOPayableExtractLevel) levelObject).getJob()!=null ?((DTOPayableExtractLevel) levelObject).getJob().toString():"";
	 				//wareHouse = ((DTOPayableExtractLevel) levelObject).getWareHouse()!=null ?((DTOPayableExtractLevel) levelObject).getWareHouse().toString():"";
	 				// ends
	 				accCompanyDivision = ((DTOPayableExtractLevel) levelObject).getAccCompanyDivision()!=null ?((DTOPayableExtractLevel) levelObject).getAccCompanyDivision().toString():"";
	 				companyDivision = ((DTOPayableExtractLevel) levelObject).getCompanyDivision()!=null ?((DTOPayableExtractLevel) levelObject).getCompanyDivision().toString():"";
	 				key = invoiceNumber.trim() + "==" + actgCode.trim() + "==" +accCompanyDivision ;

	 				if (key.equals(samekey)) {
	 					sumActualExpense = actualExpense.add(sumActualExpense);
	 					sumlocalAmount=localAmount.add(sumlocalAmount);
	 				} else if (!key.equals(samekey)) {
	 					sumActualExpense = actualExpense;
	 					sumlocalAmount=localAmount;
	 				}

	 				samekey = invoiceNumber.trim() + "==" + actgCode.trim() + "==" +accCompanyDivision ;;
	 				List level4List = (List)extractLevel4Map.get(key);
	 				if (level4List == null || level4List.isEmpty()) {
	 					level4List = new ArrayList();
	 				}			
	 				try {
	 					String Level1MapValue = sumActualExpense.toString() + "=="
	 							+ invoiceDate.toString() + "==" + actgCode + "=="
	 							+ invoiceNumber +"==" + sumlocalAmount.toString() +"=="+ receivedDate.toString() + "==" +accCompanyDivision +  "==" +shipNumber ;
	 					String Level4MapValue = shipNumber + "==" + payGl
	 							+ "==" + actualExpense.toString() + "==" + bucket +"=="+ localAmount.toString() + "==" +accCompanyDivision
	 							;
	 					level4List.add(Level4MapValue);
	 					if(extractLevel1Map.containsKey(key)){
	 						String detailLevel1 = (String) extractLevel1Map.get(key);
	 						String arraydetailLevel1[] = detailLevel1.split("==");
	 						String soOldCompanyDivision=arraydetailLevel1[6];
	 						String soOldShipNumber=arraydetailLevel1[7];
	 						if(soOldShipNumber!=null && (!(soOldShipNumber.trim().equals(""))) && shipNumber!=null && (!(shipNumber.trim().equals(""))) && (!(shipNumber.trim().equals(soOldShipNumber.trim())))){
	 						if(soOldCompanyDivision!=null && (!(soOldCompanyDivision.trim().equals(""))) && accCompanyDivision!=null && (!(accCompanyDivision.trim().equals(""))) && (!(accCompanyDivision.trim().equals(soOldCompanyDivision.trim()))) && companyDivisionExtract2Group.indexOf(soOldCompanyDivision)>=0 && companyDivisionExtract2Group.indexOf(accCompanyDivision)>=0 ){
	 							goupkey=key; 
	 							
	 						}
	 						}
	 						if(key.equals(goupkey)){
	 							accCompanyDivision="FOR";
	 							Level1MapValue = sumActualExpense.toString() + "=="
	 		 							+ invoiceDate.toString() + "==" + actgCode + "=="
	 		 							+ invoiceNumber +"==" + sumlocalAmount.toString() +"=="+ receivedDate.toString() + "==" +accCompanyDivision +  "==" +shipNumber ;		
	 						}
	 					}
	 					extractLevel1Map.put(key, Level1MapValue);
	 					extractLevel4Map.put(key, level4List);
	 				} catch (Exception e) {
	 					e.printStackTrace();
	 				}

	 			} 
	 			Iterator mapIterator = extractLevel1Map.entrySet().iterator();
	 			while (mapIterator.hasNext()) {
	 				Map.Entry entry = (Map.Entry) mapIterator.next();
	 				String invoiceActgCode = (String) entry.getKey();
	 				String detailLevel1 = (String) extractLevel1Map
	 						.get(invoiceActgCode);
	 				String arraydetailLevel1[] = detailLevel1.split("==");
	 				String totalexpShip = arraydetailLevel1[0];
	 				BigDecimal ActualExpense = new BigDecimal(totalexpShip);
	 				if(ActualExpense.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				totalexpShip=ActualExpense.abs().toString();
	 				String invoiceDate = arraydetailLevel1[1];
	 				String actgCode = arraydetailLevel1[2];
	 				String invoiceNumber = arraydetailLevel1[3];
	 				String sumlocalExpense=arraydetailLevel1[4];			
	 				BigDecimal actualLocalExpSum = new BigDecimal(sumlocalExpense);
	 				sumlocalExpense=actualLocalExpSum.abs().toString();
	 				String recivedDate ="";
	 				if(arraydetailLevel1[5] !=null && (!(arraydetailLevel1[5].toString().trim().equals("")))){
	 				 recivedDate = arraydetailLevel1[5];
	 				}else{
	 				 recivedDate = arraydetailLevel1[1];	
	 				} 
	 				String accountingCode=arraydetailLevel1[6];
	 				accountingCode=accountingParameterMap.get(accountingCode);
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				outputStream.write(("\"" + actgCode + "\",").getBytes());
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				outputStream.write(("\"" + totalexpShip.toString() + "\",")
	 						.getBytes());
	 				try {
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat(
	 							"yyyy-MM-dd");
	 					Date du = new Date();
	 					du = postformatLevel1.parse(invoiceDate.toString());
	 					postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String invoiceDatesString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + invoiceDatesString + "\",")
	 							.getBytes());
	 				} catch (Exception e) {
	 					outputStream.write(("\"" + "" + "\",")
	 							.getBytes());
	 					e.printStackTrace();
	 				}
	 				outputStream.write(("\"" + invoiceNumber + "\",").getBytes());
	 				outputStream.write(("\"" + sumlocalExpense + "\",").getBytes());
	 				try {
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd");
	 					Date du = new Date(); 
	 					du = postformatLevel1.parse(recivedDate.toString());
	 					postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String recivedDateString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + recivedDateString + "\",").getBytes());
	 				} catch (Exception e) {
	 					outputStream.write(("\"" + "" + "\",")
	 							.getBytes());
	 					System.out.println("Recived Date is not found");
	 				}
	 				
	 				try {
	 					Date du = new Date();
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String accDateString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + accDateString + "\",").getBytes());
	 				} catch (Exception e) {
	 					outputStream.write(("\"" + "" + "\",")
	 							.getBytes());
	 					System.out.println("ACC Date is not found");
	 				}
	 				outputStream.write(("\"" + accountingCode + "\"").getBytes());
	 				outputStream.write("\r\n".getBytes()); 
	 				
	 				List detail4List = (List) extractLevel4Map
	 				.get(invoiceActgCode);
	 			
	 				for (int i = 0; i < detail4List.size(); i++) {
	 					String level4String = (String)detail4List.get(i);
	 					
	 					String arrayDetailLevel4[] = level4String.split("==");
	 					shipNum = arrayDetailLevel4[0];
	 					payGl = arrayDetailLevel4[1];
	 					expense = arrayDetailLevel4[2];
	 					BigDecimal ActualExpenseLevel4 = new BigDecimal(expense);
	 					if(positiveExpense){
	 						expense=expense; 
	 					}
	 					else if(!positiveExpense){
	 						expense=ActualExpenseLevel4.abs().toString();			
	 					}
	 					accountingCode=arrayDetailLevel4[5];
	 					accountingCode=accountingParameterMap.get(accountingCode);
	 					glSubAcc = "0000000000000"; 
	 					String localExpense=arrayDetailLevel4[4];
	 					BigDecimal actualLocalExpense = new BigDecimal(localExpense);
	 					localExpense=actualLocalExpense.abs().toString();
	 					outputStream.write("LEVEL4".getBytes());
	 					outputStream.write((",").getBytes());
	 					outputStream.write(("\"" + accountingCode + "\",").getBytes());
	 					outputStream.write(("\"" + payGl + "\",").getBytes());
	 					outputStream.write(("\"" + glSubAcc + "\",").getBytes());
	 					outputStream.write(("\"" + expense + "\",").getBytes());
	 					outputStream.write(("\"" + shipNum + "\",").getBytes());
	 					outputStream.write(("\"" + localExpense + "\"").getBytes());
	 					outputStream.write("\r\n".getBytes());
	 				}
	 			}
	 		}
	 		
	 		@SkipValidation
	 		public void payableDriverExtract() throws Exception, EOFException {
	 			String payXferUser = getRequest().getRemoteUser();
	 			//Date payDate1=null;
	 			//sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
	 			//if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
	 				//for (SystemDefault systemDefault : sysDefaultDetail) {
	 					//payDate1=systemDefault.getPostDate1();
	 			//}
	 			//}
	 			
	 			Date payPostingDateFormat = new Date();
	 			Date startInvoiceDateFormat = new Date();
	 			Date endInvoiceDateFormat = new Date();
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				payPostingDateFormat = df.parse(recPostingDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			String[] str1 = invoiceDate.split("To"); 
	 			String startInvoiceDate = str1[0];
	 			startInvoiceDate=startInvoiceDate.trim();
	 			String endInvoiceDate=str1[1];
	 			endInvoiceDate = endInvoiceDate.trim();
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				startInvoiceDateFormat = df.parse(startInvoiceDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			try {
	 				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	 				endInvoiceDateFormat = df.parse(endInvoiceDate);
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}
	 			StringBuilder startInvoiceDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
					.format(startInvoiceDateFormat));
	 			StringBuilder endInvoiceDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
					.format(endInvoiceDateFormat));
	 			//Date payPostingDateFormat = getPostingDate();
	 			StringBuilder payPostingDates = new StringBuilder(new SimpleDateFormat("yyyy-MM-dd")
	 					.format(payPostingDateFormat));
	 			
	 			List subExtractSeqList=systemDefaultManager.findPayExtractSeq(sessionCorpID); 
	 			if(subExtractSeqList.isEmpty())
	 			{ 
	 				subConExtractSeq= new String("00001"); 
	 			}
	 			else if(!subExtractSeqList.isEmpty())
	 			{
	 				subConExtractSeq=subExtractSeqList.get(0).toString();
	 			} 
	 			HttpServletResponse response = getResponse();
	 			ServletOutputStream outputStream = response.getOutputStream(); 
	 			StringBuffer batchDates=new StringBuffer("PAY");
	 			batchDates.append(subConExtractSeq);
	 	        File file=new File(batchDates.toString());
	 	        if(subcontrCompanyCode==null){
	 	        	subcontrCompanyCode="";
	 	        }
	 	       
	 	        List  subContcExtractList=subcontractorChargesManager.getSubContcExtract(subcontrCompanyCode,sessionCorpID,payPostingDates.toString(),startInvoiceDates.toString(),endInvoiceDates.toString());  
	 			
	 	       String  shipNumber = writeLevel0(response, outputStream,
	 					payPostingDateFormat, payPostingDates, file,subContcExtractList,startInvoiceDates,endInvoiceDates);
	 			
	 			List payableExtractLevel = accountLineManager.findDriverPayableExtractLevel(
	 					subcontrCompanyCode, sessionCorpID, payPostingDates.toString(), true,startInvoiceDates.toString(),endInvoiceDates.toString());
	 			
	 			writeL1L4(outputStream, payableExtractLevel, true);
	 			String subIdList="";
	 			String glSubAcc = new String();
	 			String shipNum = new String();
	 			Iterator it=subContcExtractList.iterator();
	 			while(it.hasNext())
	 			{ 
	 				Object subExtract=it.next();
	 				
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
	 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				String subAmount="0.00";
	 				String subActualAmount="0.00";
	 				 boolean positiveExpense=false;
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
	 				}
	 				BigDecimal amount = new BigDecimal(subAmount);
	 				
	 				if(amount.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				subActualAmount=amount.abs().toString();
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				Date du = new Date();
					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString = postformatLevel1.format(du);
					outputStream.write(("\"" + accDateString + "\",").getBytes());
	 				//String s1="S"; 
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}  
	 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				// enhancement end
	 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}*/ 
	 				

	 				
	 				if((((DTOForSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOForSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
	 	 				try{
	 	 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
	 	 	                Date du1 = new Date();
	 	 	                du1 = postformatLevel11.parse(((DTOForSubContcExtract)subExtract).getApproved().toString());
	 	 	              postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
	 	 	                String  approvedDateString = postformatLevel11.format(du1);  
	 	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
	 	 	    	        }
	 	 	                catch( Exception e)
	 	 	    			{
	 	 	    				e.printStackTrace();
	 	 	    			}
	 	 				}else
	 					{
	 						String s="\""+"\",";
	 						outputStream.write(s.getBytes());
	 					}
	 				Date du2 = new Date();
					SimpleDateFormat postformatLevel12 = new SimpleDateFormat("MM/dd/yyyy");
					String accDateString1 = postformatLevel12.format(du2);
					outputStream.write(("\"" + accDateString1 + "\"").getBytes());
					outputStream.write("\r\n".getBytes());
	 	                
	 	                
					outputStream.write("LEVEL4".getBytes());
	 				outputStream.write((",").getBytes()); 
	 				/*String accountingcode="00";
	 				if(((DTOForSubContcExtract)subExtract).getAccountingCode()!=null) { 
	 					accountingcode= ((DTOForSubContcExtract)subExtract).getAccountingCode().toString() ; 
	 				}
	 				String bucket2="00";
	 				if(((DTOGPSubContcExtract)subExtract).getJob()!=null && (!((((DTOGPSubContcExtract)subExtract).getJob()).toString().equals(""))) ) { 
				    bucket2=refMasterManager.getBucket2(((DTOGPSubContcExtract)subExtract).getJob().toString(),sessionCorpID);
	 				}
	 				if(((DTOForSubContcExtract)subExtract).getDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getDivision().toString().trim().equals("")))){
	 					bucket2=((DTOForSubContcExtract)subExtract).getDivision().toString();
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDivision() + "\",").getBytes());
	 				}*/
	 				if(((DTOForSubContcExtract)subExtract).getGlCode()!=null) { 
	 					outputStream.write(("\""+ ((DTOForSubContcExtract)subExtract).getGlCode() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				glSubAcc="0000000000000";    
	 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}
	 				
	 				 
	 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
	 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
						outputStream.write(("\""+desc.trim()+"\",").getBytes());
	 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
	 				}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					}*/
	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 					//String temp ="                    ";
		 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
		 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
					}else {
						String s="\""+"\",";
						outputStream.write(s.getBytes());
					} 
	 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
	 					outputStream.write(("\"" + subActualAmount + "\"").getBytes());	
	 				}else {
						String s="\""+"\"";
						outputStream.write(s.getBytes());
					} 
					outputStream.write("\r\n".getBytes());

	 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	        			if(subIdList.equals("")){
	        				subIdList=(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}else{
	        				subIdList = subIdList +","+(((DTOForSubContcExtract)subExtract).getSubId().toString());
	        			}
	        		}
	 			}
	 			
	 			outputStream.flush();
	 			outputStream.close();
	 			if(!(subIdList.equals(""))){
	 			int k= subcontractorChargesManager.gpSubContcFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
	 			}
	 	    	if(!shipNumber.equals(""))
	 	        {
	 	    		shipNumber=shipNumber.substring(0, shipNumber.length()-1);
	 	        }
	 	        else if(shipNumber.equals(""))
	 	        {
	 	        	shipNumber =new String("''");
	 	        }  
	 				int j = accountLineManager.payableDriverinvoiceUpdate(batchDates
	 						.toString()
	 						+ ".txt", shipNumber, payXferUser, payPostingDates
	 						.toString(),sessionCorpID); 
	 			//Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			//payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			//String val = "0000";
	 			//val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			//subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			//int i = partnerManager.updateSubContrcExtractSeq(subConExtractSeq,
	 					//sessionCorpID);
	 			Long payExtractSeqUpdate = Long.valueOf(subConExtractSeq);
	 			payExtractSeqUpdate = payExtractSeqUpdate + 1;
	 			String val = "0000";
	 			val = val.substring(payExtractSeqUpdate.toString().length() - 1);
	 			subConExtractSeq = val + payExtractSeqUpdate.toString();
	 			int i = systemDefaultManager.updatePayExtractSeq(subConExtractSeq,sessionCorpID);
	 		}
	 		
	 		private String writeLevel0(HttpServletResponse response,
	 				ServletOutputStream outputStream, Date payPostingDateFormat,
	 				StringBuilder payPostingDates, File file,List subContcExtractList,StringBuilder startInvoiceDates, StringBuilder endInvoiceDates)throws Exception, EOFException {
	 			 
	 			List list = accountLineManager.getDriverPayableExtract(subcontrCompanyCode,
	 					sessionCorpID, payPostingDates.toString(),startInvoiceDates.toString(),endInvoiceDates.toString());
	 			response.setContentType("text/csv");
	 			response.setHeader("Content-Disposition", "attachment; filename="
	 					+ file.getName() + ".txt");
	 			response.setHeader("Pragma", "public");
	 			response.setHeader("Cache-Control", "max-age=0");
	 			out
	 					.println("filename=" + file.getName() + ".txt"
	 							+ "\t The number of line  \t" + list.size()
	 							+ "\t is extracted");
	 			Iterator its = list.iterator();
	 			BigDecimal batchTotalPay = new BigDecimal(0);
	 			StringBuffer shipNumInvoice=new StringBuffer();
	 			Set ShipNumberSet = new TreeSet();

	 			while (its.hasNext()) {
	 				Object payExtract = its.next();
	 				if (((DTOForPayable) payExtract).getActualExpense() != null) {
	 					BigDecimal expenseForPExtrac = (BigDecimal) (((DTOForPayable) payExtract)
	 							.getActualExpense());
	 					batchTotalPay = batchTotalPay.add(expenseForPExtrac);
	 				} else {
	 					BigDecimal  batchTotalPay1 = new BigDecimal(0);
	 					batchTotalPay = batchTotalPay.add(batchTotalPay1);
	 				}
	 				 shipNumInvoice=shipNumInvoice.append("'");
	 				 shipNumInvoice=shipNumInvoice.append((String)(((DTOForPayable) payExtract).getId()).toString());
	 				 shipNumInvoice=shipNumInvoice.append("',");
	 				//ShipNumberList.add(((DTOForPayable) payExtract).getShipNumber());
	 			}
	 			
	 			Iterator it = subContcExtractList.iterator();
	 			while (it.hasNext()) {
	 				 
	 				Object subExtract=it.next();
	 				if(((DTOForSubContcExtract)subExtract).getAmount()!=null ) 
	 				{
	 					BigDecimal amountForExtract=(BigDecimal)(((DTOForSubContcExtract)subExtract).getAmount());
	 					batchTotalPay=batchTotalPay.add(amountForExtract);
	 				}
	 				else
	 				{
	 					BigDecimal  batchTotalPay1=new BigDecimal(0);
	 					batchTotalPay=batchTotalPay.add(batchTotalPay1);
	 				}
	 				//personIdList.add(((DTOForSubContcExtract)subExtract).getPersonId());
	 				
	 			}
	 			//ShipNumberSet.addAll(ShipNumberList);
	 			outputStream.write("LEVEL0".getBytes());
	 			outputStream.write((",").getBytes());
	 			SimpleDateFormat postformats = new SimpleDateFormat("yyyyMM");
	 			StringBuilder postDates = new StringBuilder(postformats
	 					.format(payPostingDateFormat));
	 			outputStream.write(("\"" + postDates.toString() + "\",").getBytes());
	 			outputStream.write(("\"" + batchTotalPay.toString() + "\"").getBytes());
	 			outputStream.write("\r\n".getBytes());
	 			String shipInvoiceUpdate=new String(shipNumInvoice);
	 			return shipInvoiceUpdate;
	 		}
	 		
	 		private void writeL1L4(ServletOutputStream outputStream, List payableExtractLevel, boolean positiveExpense) throws IOException {
	 			SortedMap extractLevel1Map = new TreeMap();
	 			SortedMap extractLevel4Map = new TreeMap();
	 			BigDecimal sumActualExpense = new BigDecimal(0.0);
	 			BigDecimal sumlocalAmount = new BigDecimal(0.0);
	 			String shipNumberLevel4 = new String();
	 			String payGlLevel4 = new String();
	 			String actualExpenseLevel4 = new String();
	 			String bucketLevel4 = new String();
	 			String key = new String("");
	 			String samekey = new String("");
	 			String bucket = new String();
	 			String payGl = new String();
	 			String glSubAcc = new String();
	 			String expense = new String();
	 			String shipNum = new String();
	 			//String shipmentNumber = new String();
	 			//String partnerType = new String();
	 			//String stage = new String();
	 			//String job = new String();
	 			//String wareHouse = new String();
	 			
	 			
	 			
	 			Iterator levelIterator = payableExtractLevel.iterator();
	 			while (levelIterator.hasNext()) {
	 				Object levelObject = levelIterator.next();
	 				String invoiceNumber = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getInvoiceNumber();
	 				String actgCode = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getActgCode();			
	 				BigDecimal actualExpense = (BigDecimal) ((DTOPayableExtractLevel) levelObject)
	 						.getActualExpense();
	 				Date invoiceDate = (Date) ((DTOPayableExtractLevel) levelObject)
	 						.getInvoiceDate();
	 				payGl = (String) ((DTOPayableExtractLevel) levelObject).getPayGl();
	 				String shipNumber = (String) ((DTOPayableExtractLevel) levelObject)
	 						.getShipNumber();
	 				BigDecimal localAmount = (BigDecimal) ((DTOPayableExtractLevel) levelObject)
	 				.getLocalAmount();
	 				
	 				 // add for #6938 - AP file changes for Solmon start
	 				Date receivedDate = new Date();
	 				if(((DTOPayableExtractLevel) levelObject).getReceivedDate() != null)
	 						receivedDate = (Date) ((DTOPayableExtractLevel) levelObject).getReceivedDate();
	 				
	 				
	 				//bucket = extractBucket(levelObject);
	 				// New Code added
	 				//shipmentNumber = ((DTOPayableExtractLevel) levelObject).getShipmentType()!=null ?((DTOPayableExtractLevel) levelObject).getShipmentType().toString():"";
	 				//partnerType = ((DTOPayableExtractLevel) levelObject).getPartnerType()!=null ?((DTOPayableExtractLevel) levelObject).getPartnerType().toString():"";
	 				//stage = ((DTOPayableExtractLevel) levelObject).getStage()!=null ?((DTOPayableExtractLevel) levelObject).getStage().toString():"";
	 				//job = ((DTOPayableExtractLevel) levelObject).getJob()!=null ?((DTOPayableExtractLevel) levelObject).getJob().toString():"";
	 				//wareHouse = ((DTOPayableExtractLevel) levelObject).getWareHouse()!=null ?((DTOPayableExtractLevel) levelObject).getWareHouse().toString():"";
	 				// ends
	 				key = invoiceNumber.trim() + "==" + actgCode.trim();

	 				if (key.equals(samekey)) {
	 					sumActualExpense = actualExpense.add(sumActualExpense);
	 					sumlocalAmount=localAmount.add(sumlocalAmount);
	 				} else if (!key.equals(samekey)) {
	 					sumActualExpense = actualExpense;
	 					sumlocalAmount=localAmount;
	 				}

	 				samekey = invoiceNumber.trim() + "==" + actgCode.trim();
	 				List level4List = (List)extractLevel4Map.get(key);
	 				if (level4List == null || level4List.isEmpty()) {
	 					level4List = new ArrayList();
	 				}			
	 				try {
	 					String Level1MapValue = sumActualExpense.toString() + "=="
	 							+ invoiceDate.toString() + "==" + actgCode + "=="
	 							+ invoiceNumber +"==" + sumlocalAmount.toString() +"=="+ receivedDate.toString() ;
	 					String Level4MapValue = shipNumber + "==" + payGl
	 							+ "==" + actualExpense.toString() + "==" + bucket +"=="+ localAmount.toString()
	 							;
	 					level4List.add(Level4MapValue);
	 					extractLevel1Map.put(key, Level1MapValue);
	 					extractLevel4Map.put(key, level4List);
	 				} catch (Exception e) {
	 					e.printStackTrace();
	 				}

	 			} 
	 			Iterator mapIterator = extractLevel1Map.entrySet().iterator();
	 			while (mapIterator.hasNext()) {
	 				Map.Entry entry = (Map.Entry) mapIterator.next();
	 				String invoiceActgCode = (String) entry.getKey();
	 				String detailLevel1 = (String) extractLevel1Map
	 						.get(invoiceActgCode);
	 				String arraydetailLevel1[] = detailLevel1.split("==");
	 				String totalexpShip = arraydetailLevel1[0];
	 				BigDecimal ActualExpense = new BigDecimal(totalexpShip);
	 				if(ActualExpense.doubleValue()<0){
	 					positiveExpense=false;
	 				}
	 				else {
	 					positiveExpense=true;
	 				}
	 				totalexpShip=ActualExpense.abs().toString();
	 				String invoiceDate = arraydetailLevel1[1];
	 				String actgCode = arraydetailLevel1[2];
	 				String invoiceNumber = arraydetailLevel1[3];
	 				String sumlocalExpense=arraydetailLevel1[4];			
	 				BigDecimal actualLocalExpSum = new BigDecimal(sumlocalExpense);
	 				sumlocalExpense=actualLocalExpSum.abs().toString();
	 				String recivedDate = arraydetailLevel1[5];
	 				outputStream.write("LEVEL1".getBytes());
	 				outputStream.write((",").getBytes());
	 				outputStream.write(("\"" + actgCode + "\",").getBytes());
	 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
	 				outputStream.write(("\"" + totalexpShip.toString() + "\",")
	 						.getBytes());
	 				try {
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat(
	 							"yyyy-MM-dd");
	 					Date du = new Date();
	 					du = postformatLevel1.parse(invoiceDate.toString());
	 					postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String invoiceDatesString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + invoiceDatesString + "\",")
	 							.getBytes());
	 				} catch (Exception e) {
	 					e.printStackTrace();
	 				}
	 				outputStream.write(("\"" + invoiceNumber + "\",").getBytes());
	 				outputStream.write(("\"" + sumlocalExpense + "\",").getBytes());
	 				try {
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd");
	 					Date du = new Date();
	 					du = postformatLevel1.parse(recivedDate.toString());
	 					postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String recivedDateString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + recivedDateString + "\",").getBytes());
	 				} catch (Exception e) {
	 					System.out.println("Recived Date is not found");
	 				}
	 				
	 				try {
	 					Date du = new Date();
	 					SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
	 					String accDateString = postformatLevel1.format(du);
	 					outputStream.write(("\"" + accDateString + "\"").getBytes());
	 				} catch (Exception e) {
	 					System.out.println("ACC Date is not found");
	 				}
	 				outputStream.write("\r\n".getBytes()); 
	 				
	 				List detail4List = (List) extractLevel4Map
	 				.get(invoiceActgCode);
	 			
	 				for (int i = 0; i < detail4List.size(); i++) {
	 					String level4String = (String)detail4List.get(i);
	 					
	 					String arrayDetailLevel4[] = level4String.split("==");
	 					shipNum = arrayDetailLevel4[0];
	 					payGl = arrayDetailLevel4[1];
	 					expense = arrayDetailLevel4[2];
	 					BigDecimal ActualExpenseLevel4 = new BigDecimal(expense);
	 					if(positiveExpense){
	 						expense=expense; 
	 					}
	 					else if(!positiveExpense){
	 						expense=ActualExpenseLevel4.abs().toString();			
	 					} 
	 					glSubAcc = "0000000000000"; 
	 					String localExpense=arrayDetailLevel4[4];
	 					BigDecimal actualLocalExpense = new BigDecimal(localExpense);
	 					localExpense=actualLocalExpense.abs().toString();
	 					outputStream.write("LEVEL4".getBytes());
	 					outputStream.write((",").getBytes());
	 					outputStream.write(("\"" + payGl + "\",").getBytes());
	 					outputStream.write(("\"" + glSubAcc + "\",").getBytes());
	 					outputStream.write(("\"" + expense + "\",").getBytes());
	 					outputStream.write(("\"" + shipNum + "\",").getBytes());
	 					outputStream.write(("\"" + localExpense + "\"").getBytes());
	 					outputStream.write("\r\n".getBytes());
	 				}
	 			}
	 		}
	 		
	 		
 		@SkipValidation
 		public void gpSubcontractorExtract() throws Exception,EOFException
 		{
 			List subExtractSeqList=partnerManager.findSubContExtractSeq(sessionCorpID); 
 			if(subExtractSeqList.isEmpty())
 			{ 
 				subConExtractSeq= new String("00001"); 
 			}
 			else if(!subExtractSeqList.isEmpty())
 			{
 				subConExtractSeq=subExtractSeqList.get(0).toString();
 			} 
 			HttpServletResponse response = getResponse();
 			ServletOutputStream outputStream = response.getOutputStream(); 
 			StringBuffer batchDates=new StringBuffer("DRV");
 			batchDates.append(subConExtractSeq);
 	        File file=new File(batchDates.toString());
 	        if(subcontrCompanyCode==null){
 	        	subcontrCompanyCode="";
 	        }
 	        List  list=subcontractorChargesManager.getgpSubContcExtract(sessionCorpID);  
 			response.setContentType("text/csv");
 			//response.setHeader("Cache-Control", "no-cache");
 			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".txt");
 			response.setHeader("Pragma", "public");
 			response.setHeader("Cache-Control", "max-age=0");
 			out.println("filename="+file.getName()+".txt"+"\t The number of line  \t"+list.size()+"\t is extracted");
 			Iterator its=list.iterator();
 			String subIdList="";
 			String glSubAcc = new String();
 			String shipNum = new String();
 			while(its.hasNext())
 			{ 
 				Object subExtract=its.next();
 				
 				outputStream.write("LEVEL1".getBytes());
 				outputStream.write((",").getBytes());
 				if(((DTOGPSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
 					outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				String subAmount="0.00";
 				String subActualAmount="0.00";
 				 boolean positiveExpense=false;
 				if((((DTOGPSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOGPSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					subAmount=	((DTOGPSubContcExtract)subExtract).getAmount().toString().trim();
 				}
 				BigDecimal amount = new BigDecimal(subAmount);
 				
 				if(amount.doubleValue()<0){
 					positiveExpense=false;
 				}
 				else {
 					positiveExpense=true;
 				}
 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
 				subActualAmount=amount.abs().toString();
 				if((((DTOGPSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOGPSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				if((((DTOGPSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOGPSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
 				try{
 	    		    SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd"); 
 	                Date du = new Date();
 	                du = postformatLevel1.parse(((DTOGPSubContcExtract)subExtract).getApproved().toString());
 	                postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
 	                String  approvedDateString = postformatLevel1.format(du);  
 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
 	    	        }
 	                catch( Exception e)
 	    			{
 	    				e.printStackTrace();
 	    			}
 				}else
				{
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				String s1="S"; 
 				if(((DTOGPSubContcExtract)subExtract).getSubId()!=null) { 
 					s1 =s1+(((DTOGPSubContcExtract)subExtract).getSubId().toString());
				}
 				String temp ="                    ";
 				String invoiceNumberWithId= s1+""+temp.substring(s1.length()-1, temp.length())+""+((DTOGPSubContcExtract)subExtract).getSubId();
 				
				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());	
 				if((((DTOGPSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOGPSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				/* remove  in requirement of bug # 6648 - GP enhancements on the DRV file 
 				 * if((((DTOGPSubContcExtract)subExtract).getServiceOrder()!=null) && (!(((DTOGPSubContcExtract)subExtract).getServiceOrder().toString().trim().equals("")) )) {
 					outputStream.write(("\"" +((DTOGPSubContcExtract)subExtract).getServiceOrder() + "\",").getBytes());	
 				}else {
					String shipNumber="CWMS100XXXXXX";
					outputStream.write(("\"" + shipNumber + "\",").getBytes());
				}*/ 
 				// added in requirement of bug # 6648 - GP enhancements on the DRV file 
 				if(((DTOGPSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOGPSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\",").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				// enhancement end
 				if(((DTOGPSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOGPSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\",").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				

 				
 				if((((DTOGPSubContcExtract)subExtract).getPost()!=null) && (!(((DTOGPSubContcExtract)subExtract).getPost().toString().trim().equals("")) )) {
 	 				try{
 	 	    		    SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd"); 
 	 	                Date du = new Date();
 	 	                du = postformatLevel1.parse(((DTOGPSubContcExtract)subExtract).getPost().toString());
 	 	                postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
 	 	                String  postDateString = postformatLevel1.format(du);  
 	 	                outputStream.write(("\""+postDateString+"\"").getBytes());
 	 	    	        }
 	 	                catch( Exception e)
 	 	    			{
 	 	    				e.printStackTrace();
 	 	    			}
 	 				}else
 					{
 						String s="\""+"\"";
 						outputStream.write(s.getBytes());
 					}
 				
				outputStream.write("\r\n".getBytes());
 	                
 	                
				outputStream.write("LEVEL4".getBytes());
 				outputStream.write((",").getBytes()); 
 				String accountingcode="00";
 				if(((DTOGPSubContcExtract)subExtract).getAccountingCode()!=null) { 
 					accountingcode= ((DTOGPSubContcExtract)subExtract).getAccountingCode().toString() ; 
 				}
 				String bucket2="00";
 				/*if(((DTOGPSubContcExtract)subExtract).getJob()!=null && (!((((DTOGPSubContcExtract)subExtract).getJob()).toString().equals(""))) ) { 
			    bucket2=refMasterManager.getBucket2(((DTOGPSubContcExtract)subExtract).getJob().toString(),sessionCorpID);
 				}*/
 				if(((DTOGPSubContcExtract)subExtract).getDivision()!=null && (!(((DTOGPSubContcExtract)subExtract).getDivision().toString().trim().equals("")))){
 					bucket2=((DTOGPSubContcExtract)subExtract).getDivision().toString();
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDivision() + "\",").getBytes());
 				}
 				if(((DTOGPSubContcExtract)subExtract).getGlCode()!=null) { 
 					outputStream.write(("\""+accountingcode+"-"+bucket2+"-" + ((DTOGPSubContcExtract)subExtract).getGlCode() + "-00\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				glSubAcc="";    
 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
 				if((((DTOGPSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOGPSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				
 				/*remove  in requirement of bug # 6648 - GP enhancements on the DRV file 
 				 * if((((DTOGPSubContcExtract)subExtract).getServiceOrder()!=null) && (!(((DTOGPSubContcExtract)subExtract).getServiceOrder().toString().trim().equals("")) )) {
 					outputStream.write(("\"" +((DTOGPSubContcExtract)subExtract).getServiceOrder() + "\",").getBytes());	
 				}else {
					String shipNumber="CWMS100XXXXXX";
					outputStream.write(("\"" + shipNumber + "\",").getBytes());
				} */
 				
 				if(((DTOGPSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOGPSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\",").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				
 				if((((DTOGPSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOGPSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				String s2 = "S";
 				
 				if(((DTOGPSubContcExtract)subExtract).getSubId()!=null) { 
 					 s2 =s2+(((DTOGPSubContcExtract)subExtract).getSubId().toString());
 				}
 				String temp1 ="                    ";
 				String invoiceNumberWithId1 = s2+""+temp.substring(s2.length()-1, temp.length())+""+((DTOGPSubContcExtract)subExtract).getSubId();
 				outputStream.write(("\"" + invoiceNumberWithId1 + "\",").getBytes());
 				if(((DTOGPSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
 					outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				if((((DTOGPSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOGPSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
 	 				try{
 	 	    		    SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd"); 
 	 	                Date du = new Date();
 	 	                du = postformatLevel1.parse(((DTOGPSubContcExtract)subExtract).getApproved().toString());
 	 	                postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
 	 	                String  approvedDateString = postformatLevel1.format(du);  
 	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
 	 	    	        }
 	 	                catch( Exception e)
 	 	    			{
 	 	    				e.printStackTrace();
 	 	    			}
 	 				}else
 					{
 						String s="\""+"\",";
 						outputStream.write(s.getBytes());
 					}
 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\"").getBytes());
				outputStream.write("\r\n".getBytes());

 				if(((DTOGPSubContcExtract)subExtract).getSubId()!=null) { 
        			if(subIdList.equals("")){
        				subIdList=(((DTOGPSubContcExtract)subExtract).getSubId().toString());
        			}else{
        				subIdList = subIdList +","+(((DTOGPSubContcExtract)subExtract).getSubId().toString());
        			}
        		}
 			}
 		    outputStream.flush();
	 		outputStream.close();
	 		 int j= subcontractorChargesManager.gpSubContcFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
 			Long subExtractSeqUpdate=Long.valueOf(subConExtractSeq); 
 			subExtractSeqUpdate=subExtractSeqUpdate+1;
 			String val = "0000";
 			val = val.substring(subExtractSeqUpdate.toString().length()-1); 
 			subConExtractSeq=val+subExtractSeqUpdate.toString(); 
 			//System.out.println("\n\n\n\n\n payExtractSeqUpdate in action"+subConExtractSeq);
 			int i=partnerManager.updateSubContrcExtractSeq(subConExtractSeq,sessionCorpID); 
 		}
 		private List InvoicePostDateList;
 		private String recPostingDate;
 		private String invoiceDate;
 		public String getInvoiceDate() {
			return invoiceDate;
		}
		public void setInvoiceDate(String invoiceDate) {
			this.invoiceDate = invoiceDate;
		}

		private String invoiceCompanyCode;
		@SkipValidation
 		public String processingIndicatorAccInterface(){
 			return SUCCESS;	
 		}
 		@SkipValidation
 		public String accInvoiceCompanyCode(){
 			subcontrCompanyCodeList=subcontractorChargesManager.getsubcontrCompanyCode(sessionCorpID);
 			InvoicePostDateList=subcontractorChargesManager.getAccountInvoicePostDataList(sessionCorpID);
 			return SUCCESS;
 		}
 		
 		@SkipValidation
 		public void subcontractorExtractsHoll() throws Exception,EOFException
 		{
 			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
 			
 			Date recPostingDateFormat = new Date();
 			String postDate="";
 			try{  
 				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
 	            recPostingDateFormat = df.parse(recPostingDate);
 	           postDate=df.format(recPostingDateFormat);
 	        }catch (ParseException e){
 	          e.printStackTrace();
 	        }  	        
 			List subExtractSeqList=partnerManager.findSubContExtractSeq(sessionCorpID); 
 			if(subExtractSeqList.isEmpty())
 			{ 
 				subConExtractSeq= new String("00001"); 
 			}
 			else if(!subExtractSeqList.isEmpty())
 			{
 				subConExtractSeq=subExtractSeqList.get(0).toString();
 			} 
 			HttpServletResponse response = getResponse();
 			ServletOutputStream outputStream = response.getOutputStream(); 
 			StringBuffer batchDates=new StringBuffer("DRV");
 			batchDates.append(subConExtractSeq);
 	        File file=new File(batchDates.toString());
 	        if(invoiceCompanyCode==null){
 	        	invoiceCompanyCode="";
 	        }
 	        List  list=subcontractorChargesManager.getSubContcExtractHoll(invoiceCompanyCode,sessionCorpID,postDate);  
 			response.setContentType("text/csv");
 			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".txt");
 			response.setHeader("Pragma", "public");
 			response.setHeader("Cache-Control", "max-age=0");
 			out.println("filename="+file.getName()+".txt"+"\t The number of line  \t"+list.size()+"\t is extracted");
 			Iterator its=list.iterator();
 			String subIdList="";
 			String glSubAcc = new String();
 			String shipNum = new String();
 			BigDecimal batchTotalPay=new BigDecimal(0); 
 			List glCodeList=new ArrayList();
 			Set glCodeSet=new TreeSet(); 
 			Set glCodeSetForExpNiv=new TreeSet();  
 			while(its.hasNext())
 			{ 
 				Object subExtract=its.next();
 				if(((DTOForSubContcExtract)subExtract).getAmount()!=null ) 
 				{
 					BigDecimal amountForExtract=(BigDecimal)(((DTOForSubContcExtract)subExtract).getAmount());
 					batchTotalPay=batchTotalPay.add(amountForExtract);
 				}
 				else
 				{
 					batchTotalPay=new BigDecimal(0);
 					batchTotalPay=batchTotalPay.add(batchTotalPay);
 				}
 			}  
 			outputStream.write("LEVEL0".getBytes());
 			outputStream.write((",").getBytes());
 			try{
 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
 	                Date du1 = new Date();
 	                du1 = postformatLevel11.parse(recPostingDate);
 	                postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
 	                String  approvedDateString = postformatLevel11.format(du1);  
 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
 	    	        }
 	                catch( Exception e)
 	    			{
 	                SimpleDateFormat postformats = new SimpleDateFormat("MM/dd/yyyy"); 
 	   	 	        StringBuilder postDates = new StringBuilder(postformats.format(new Date()));
 	   	 	        outputStream.write(("\""+postDates.toString()+"\",").getBytes()); 
 	    			} 
 	        
 	        outputStream.write(("\""+batchTotalPay.toString()+"\"").getBytes());
 	        outputStream.write("\r\n".getBytes());
 			
 	       Iterator it=list.iterator();
 			while(it.hasNext())
 			{ 
 				Object subExtract=it.next();
 				
 				outputStream.write("LEVEL1".getBytes());
 				outputStream.write((",").getBytes());
 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				String subAmount="0.00";
 				String subActualAmount="0.00";
 				 boolean positiveExpense=false;
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
 				}
 				BigDecimal amount = new BigDecimal(subAmount);
 				
 				if(amount.doubleValue()<0){
 					positiveExpense=false;
 				}
 				else {
 					positiveExpense=true;
 				}
 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
 				subActualAmount=amount.abs().toString();
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				Date du = new Date();
				SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
				String accDateString = postformatLevel1.format(du);
				outputStream.write(("\"" + accDateString + "\",").getBytes());
 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
	 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\"").getBytes());	
 				}else {
					String s="\""+"\"";
					outputStream.write(s.getBytes());
				}  
 				
				outputStream.write("\r\n".getBytes());
				outputStream.write("LEVEL4".getBytes());
 				outputStream.write((",").getBytes()); 
 				if(((DTOForSubContcExtract)subExtract).getGlCode()!=null) { 
 					outputStream.write(("\""+ ((DTOForSubContcExtract)subExtract).getGlCode() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				glSubAcc="0000000000000";    
 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
 					BigDecimal amount1 = new BigDecimal(subAmount);
 					subActualAmount=amount1.toString();
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 
 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
	 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
	 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
 					BigDecimal amount1 = new BigDecimal(subAmount);
 					subActualAmount=amount1.toString();
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\"").getBytes());
 				}else {
					String s="\""+"\"";
					outputStream.write(s.getBytes());
				} 
				outputStream.write("\r\n".getBytes());

 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
        			if(subIdList.equals("")){
        				subIdList=(((DTOForSubContcExtract)subExtract).getSubId().toString());
        			}else{
        				subIdList = subIdList +","+(((DTOForSubContcExtract)subExtract).getSubId().toString());
        			}
        		}
 			}
 		    outputStream.flush();
	 		outputStream.close();
	 		 int j= subcontractorChargesManager.gpSubContcFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
 			Long subExtractSeqUpdate=Long.valueOf(subConExtractSeq); 
 			subExtractSeqUpdate=subExtractSeqUpdate+1;
 			String val = "0000";
 			val = val.substring(subExtractSeqUpdate.toString().length()-1); 
 			subConExtractSeq=val+subExtractSeqUpdate.toString(); 
 			int i=partnerManager.updateSubContrcExtractSeq(subConExtractSeq,sessionCorpID);
 			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"); 
 		}
 		//Not in use 		subcontractorExtractHoll()
 		@SkipValidation
 		public void subcontractorExtractHoll() throws Exception,EOFException
 		{

 			Date payDate1=null;
 			sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
 			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
 				for (SystemDefault systemDefault : sysDefaultDetail) {
 					payDate1=systemDefault.getPostDate1();
 			}
 			}
 			List subExtractSeqList=partnerManager.findSubContExtractSeq(sessionCorpID); 
 			if(subExtractSeqList.isEmpty())
 			{ 
 				subConExtractSeq= new String("00001"); 
 			}
 			else if(!subExtractSeqList.isEmpty())
 			{
 				subConExtractSeq=subExtractSeqList.get(0).toString();
 			} 
 			HttpServletResponse response = getResponse();
 			ServletOutputStream outputStream = response.getOutputStream(); 
 			StringBuffer batchDates=new StringBuffer("DRV");
 			batchDates.append(subConExtractSeq);
 	        File file=new File(batchDates.toString());
 	        if(subcontrCompanyCode==null){
 	        	subcontrCompanyCode="";
 	        }
 	        List  list=subcontractorChargesManager.getSubContcExtractHoll(subcontrCompanyCode,sessionCorpID,"");  
 			response.setContentType("text/csv");
 			//response.setHeader("Cache-Control", "no-cache");
 			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".txt");
 			response.setHeader("Pragma", "public");
 			response.setHeader("Cache-Control", "max-age=0");
 			out.println("filename="+file.getName()+".txt"+"\t The number of line  \t"+list.size()+"\t is extracted");
 			Iterator its=list.iterator();
 			String subIdList="";
 			String glSubAcc = new String();
 			String shipNum = new String();
 			BigDecimal batchTotalPay=new BigDecimal(0); 
 			//List personIdList=new ArrayList();
 			List glCodeList=new ArrayList();
 			//Set personIdset=new TreeSet(); 
 			Set glCodeSet=new TreeSet(); 
 			Set glCodeSetForExpNiv=new TreeSet();  
 			while(its.hasNext())
 			{ 
 				Object subExtract=its.next();
 				if(((DTOForSubContcExtract)subExtract).getAmount()!=null ) 
 				{
 					BigDecimal amountForExtract=(BigDecimal)(((DTOForSubContcExtract)subExtract).getAmount());
 					batchTotalPay=batchTotalPay.add(amountForExtract);
 				}
 				else
 				{
 					batchTotalPay=new BigDecimal(0);
 					batchTotalPay=batchTotalPay.add(batchTotalPay);
 				}
 				//personIdList.add(((DTOForSubContcExtract)subExtract).getPersonId());
 			}  
 			outputStream.write("LEVEL0".getBytes());
 			outputStream.write((",").getBytes());
 			try{
 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
 	                Date du1 = new Date();
 	                du1 = postformatLevel11.parse(payDate1.toString());
 	                postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
 	                String  approvedDateString = postformatLevel11.format(du1);  
 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
 	    	        }
 	                catch( Exception e)
 	    			{
 	                SimpleDateFormat postformats = new SimpleDateFormat("MM/dd/yyyy"); 
 	   	 	        StringBuilder postDates = new StringBuilder(postformats.format(new Date()));
 	   	 	        outputStream.write(("\""+postDates.toString()+"\",").getBytes()); 
 	    			} 
 	        
 	        outputStream.write(("\""+batchTotalPay.toString()+"\"").getBytes());
 	        outputStream.write("\r\n".getBytes());
 	        //personIdset.addAll(personIdList);
 			
 	       Iterator it=list.iterator();
 			while(it.hasNext())
 			{ 
 				Object subExtract=it.next();
 				
 				outputStream.write("LEVEL1".getBytes());
 				outputStream.write((",").getBytes());
 				if(((DTOForSubContcExtract)subExtract).getAccountCrossReference()!=null) { 
 					outputStream.write(("\"" + ((DTOForSubContcExtract)subExtract).getAccountCrossReference() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				String subAmount="0.00";
 				String subActualAmount="0.00";
 				 boolean positiveExpense=false;
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					subAmount=	((DTOForSubContcExtract)subExtract).getAmount().toString().trim();
 				}
 				BigDecimal amount = new BigDecimal(subAmount);
 				
 				if(amount.doubleValue()<0){
 					positiveExpense=false;
 				}
 				else {
 					positiveExpense=true;
 				}
 				outputStream.write(("\""+(positiveExpense?"VO":"AD")+"\",").getBytes());
 				subActualAmount=amount.abs().toString();
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				Date du = new Date();
				SimpleDateFormat postformatLevel1 = new SimpleDateFormat("MM/dd/yyyy");
				String accDateString = postformatLevel1.format(du);
				outputStream.write(("\"" + accDateString + "\",").getBytes());
 				//String s1="S"; 
 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
 					//String temp ="                    ";
	 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
	 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\"").getBytes());	
 				}else {
					String s="\""+"\"";
					outputStream.write(s.getBytes());
				}  
 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\",").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				// enhancement end
 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\",").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}*/ 
 				

 				
 				/*if((((DTOForSubContcExtract)subExtract).getApproved()!=null) && (!(((DTOForSubContcExtract)subExtract).getApproved().toString().trim().equals("")) )) {
 	 				try{
 	 	    		    SimpleDateFormat postformatLevel11 = new SimpleDateFormat("yyyy-MM-dd"); 
 	 	                Date du1 = new Date();
 	 	                du1 = postformatLevel11.parse(((DTOForSubContcExtract)subExtract).getApproved().toString());
 	 	              postformatLevel11 = new SimpleDateFormat("MM/dd/yyyy");
 	 	                String  approvedDateString = postformatLevel11.format(du1);  
 	 	                outputStream.write(("\""+approvedDateString+"\",").getBytes());
 	 	    	        }
 	 	                catch( Exception e)
 	 	    			{
 	 	    				e.printStackTrace();
 	 	    			}
 	 				}else
 					{
 						String s="\""+"\",";
 						outputStream.write(s.getBytes());
 					}
 				Date du2 = new Date();
				SimpleDateFormat postformatLevel12 = new SimpleDateFormat("MM/dd/yyyy");
				String accDateString1 = postformatLevel12.format(du2);
				outputStream.write(("\"" + accDateString1 + "\"").getBytes());*/
				outputStream.write("\r\n".getBytes());
 	                
 	                
				outputStream.write("LEVEL4".getBytes());
 				outputStream.write((",").getBytes()); 
 				/*String accountingcode="00";
 				if(((DTOForSubContcExtract)subExtract).getAccountingCode()!=null) { 
 					accountingcode= ((DTOForSubContcExtract)subExtract).getAccountingCode().toString() ; 
 				}
 				String bucket2="00";
 				if(((DTOGPSubContcExtract)subExtract).getJob()!=null && (!((((DTOGPSubContcExtract)subExtract).getJob()).toString().equals(""))) ) { 
			    bucket2=refMasterManager.getBucket2(((DTOGPSubContcExtract)subExtract).getJob().toString(),sessionCorpID);
 				}
 				if(((DTOForSubContcExtract)subExtract).getDivision()!=null && (!(((DTOForSubContcExtract)subExtract).getDivision().toString().trim().equals("")))){
 					bucket2=((DTOForSubContcExtract)subExtract).getDivision().toString();
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDivision() + "\",").getBytes());
 				}*/
 				if(((DTOForSubContcExtract)subExtract).getGlCode()!=null) { 
 					outputStream.write(("\""+ ((DTOForSubContcExtract)subExtract).getGlCode() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				glSubAcc="0000000000000";    
 				outputStream.write(("\"" + glSubAcc + "\",").getBytes());   
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}
 				
 				 
 				/*if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\",").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				}*/
 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
 					//String temp ="                    ";
	 				String invoiceNumberWithId=  ((DTOForSubContcExtract)subExtract).getSubId().toString();
	 				outputStream.write(("\"" + invoiceNumberWithId + "\",").getBytes());
				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				if((((DTOForSubContcExtract)subExtract).getAmount()!=null) && (!(((DTOForSubContcExtract)subExtract).getAmount().toString().trim().equals("")) )) {
 					outputStream.write(("\"" + subActualAmount + "\",").getBytes());	
 				}else {
					String s="\""+"\",";
					outputStream.write(s.getBytes());
				} 
 				if(((DTOForSubContcExtract)subExtract).getDescription()!=null) { 
 					String desc = ((DTOForSubContcExtract)subExtract).getDescription().toString();
					outputStream.write(("\""+desc.trim()+"\"").getBytes());
 					//outputStream.write(("\"" + ((DTOGPSubContcExtract)subExtract).getDescription() + "\",").getBytes());
 				}else {
					String s="\""+"\"";
					outputStream.write(s.getBytes());
				} 
				outputStream.write("\r\n".getBytes());

 				if(((DTOForSubContcExtract)subExtract).getSubId()!=null) { 
        			if(subIdList.equals("")){
        				subIdList=(((DTOForSubContcExtract)subExtract).getSubId().toString());
        			}else{
        				subIdList = subIdList +","+(((DTOForSubContcExtract)subExtract).getSubId().toString());
        			}
        		}
 			}
 		    outputStream.flush();
	 		outputStream.close();
	 		 int j= subcontractorChargesManager.gpSubContcFileUpdate(batchDates.toString()+".txt",subIdList,sessionCorpID); 
 			Long subExtractSeqUpdate=Long.valueOf(subConExtractSeq); 
 			subExtractSeqUpdate=subExtractSeqUpdate+1;
 			String val = "0000";
 			val = val.substring(subExtractSeqUpdate.toString().length()-1); 
 			subConExtractSeq=val+subExtractSeqUpdate.toString(); 
 			//System.out.println("\n\n\n\n\n payExtractSeqUpdate in action"+subConExtractSeq);
 			int i=partnerManager.updateSubContrcExtractSeq(subConExtractSeq,sessionCorpID); 
 		
 			
 		}
 		
	private List getNonsettledAmountList;
	private String subContcPersonId;
	@SkipValidation
	public String findNonsettledAmount()
	{
	getNonsettledAmountList = subcontractorChargesManager.getNonsettledAmount(subContcPersonId, sessionCorpID);
	return SUCCESS;
	 }
	 		
	 		
	public void setSubcontractorChargesManager(
			SubcontractorChargesManager subcontractorChargesManager) {
		this.subcontractorChargesManager = subcontractorChargesManager;
	}

	public SubcontractorCharges getSubcontractorCharges() {
		return subcontractorCharges;
	}

	public void setSubcontractorCharges(SubcontractorCharges subcontractorCharges) {
		this.subcontractorCharges = subcontractorCharges;
	}
	public List<SubcontractorCharges> getSubcontractorChargesList() {
		return subcontractorChargesList;
	}
	public void setSubcontractorChargesList(
			List<SubcontractorCharges> subcontractorChargesList) {
		this.subcontractorChargesList = subcontractorChargesList;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdMax() {
		return IdMax;
	}
	public void setIdMax(Long idMax) {
		IdMax = idMax;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	 
	 
	public ExtendedPaginatedList getSubcontractorChargesExt() {
		return subcontractorChargesExt;
	}
	public void setSubcontractorChargesExt(
			ExtendedPaginatedList subcontractorChargesExt) {
		this.subcontractorChargesExt = subcontractorChargesExt;
	}
	public PagingLookupManager getPagingLookupManager() {
		return pagingLookupManager;
	}
	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}
	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}
	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public List getRegNoList() {
		return regNoList;
	}
	public void setRegNoList(List regNoList) {
		this.regNoList = regNoList;
	}
	public String getGotoPageString() {
		return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}
	public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public String getPerId() {
		return perId;
	}
	public void setPerId(String perId) {
		this.perId = perId;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public long getVid() {
		return vid;
	}
	public void setVid(long vid) {
		this.vid = vid;
	}
	
	public VanLine getVanLine() {
		return vanLine;
	}
	public void setVanLine(VanLine vanLine) {
		this.vanLine = vanLine;
	}
	public void setVanLineManager(VanLineManager vanLineManager) {
		this.vanLineManager = vanLineManager;
	}
	public double getDueFormAmount() {
		return dueFormAmount;
	}
	public void setDueFormAmount(double dueFormAmount) {
		this.dueFormAmount = dueFormAmount;
	}
	public String getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	public Date getSystemDateFormat() {
		return systemDateFormat;
	}
	public void setSystemDateFormat(Date systemDateFormat) {
		this.systemDateFormat = systemDateFormat;
	}
	 
	 
	public String getSubConExtractSeq() {
		return subConExtractSeq;
	}
	public void setSubConExtractSeq(String subConExtractSeq) {
		this.subConExtractSeq = subConExtractSeq;
	}
	public String getSubcontrCompanyCode() {
		return subcontrCompanyCode;
	}
	public void setSubcontrCompanyCode(String subcontrCompanyCode) {
		this.subcontrCompanyCode = subcontrCompanyCode;
	}
	public List getGetNonsettledAmountList() {
		return getNonsettledAmountList;
	}
	public void setGetNonsettledAmountList(List getNonsettledAmountList) {
		this.getNonsettledAmountList = getNonsettledAmountList;
	}
	public String getSubContcPersonId() {
		return subContcPersonId;
	}
	public void setSubContcPersonId(String subContcPersonId) {
		this.subContcPersonId = subContcPersonId;
	}
	public String getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(String serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public String getRegNo1() {
		return regNo1;
	}
	public void setRegNo1(String regNo1) {
		this.regNo1 = regNo1;
	}
	public List getRegNoList1() {
		return regNoList1;
	}
	public void setRegNoList1(List regNoList1) {
		this.regNoList1 = regNoList1;
	}
	public Boolean getSettled() {
		return settled;
	}
	public void setSettled(Boolean settled) {
		this.settled = settled;
	}
	public Map<String, String> getPersonTypeList() {
		return personTypeList;
	}
	public void setPersonTypeList(Map<String, String> personTypeList) {
		this.personTypeList = personTypeList;
	}
	public List getBranchList() {
		return BranchList;
	}
	public void setBranchList(List branchList) {
		BranchList = branchList;
	}
	public List getSubcontrCompanyCodeList() {
		return subcontrCompanyCodeList;
	}
	public void setSubcontrCompanyCodeList(List subcontrCompanyCodeList) {
		this.subcontrCompanyCodeList = subcontrCompanyCodeList;
	}
	public Map<String, String> getGlCode() {
		return glCode;
	}
	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}
	public Map<String, String> getJob() {
		return job;
	}
	public void setJob(Map<String, String> job) {
		this.job = job;
	}
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}
	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}
	public String getMiscellaneousdefaultdriverId() {
		return miscellaneousdefaultdriverId;
	}
	public void setMiscellaneousdefaultdriverId(String miscellaneousdefaultdriverId) {
		this.miscellaneousdefaultdriverId = miscellaneousdefaultdriverId;
	}
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public Map<String, String> getDivision() {
		return division;
	}
	public void setDivision(Map<String, String> division) {
		this.division = division;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public String getCompanies() {
		return companies;
	}
	public void setCompanies(String companies) {
		this.companies = companies;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public String getGlCodeFlag() {
		return glCodeFlag;
	}
	public void setGlCodeFlag(String glCodeFlag) {
		this.glCodeFlag = glCodeFlag;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public List getInvoicePostDateList() {
		return InvoicePostDateList;
	}
	public void setInvoicePostDateList(List invoicePostDateList) {
		InvoicePostDateList = invoicePostDateList;
	}
	public String getRecPostingDate() {
		return recPostingDate;
	}
	public void setRecPostingDate(String recPostingDate) {
		this.recPostingDate = recPostingDate;
	}
	public String getInvoiceCompanyCode() {
		return invoiceCompanyCode;
	}
	public void setInvoiceCompanyCode(String invoiceCompanyCode) {
		this.invoiceCompanyCode = invoiceCompanyCode;
	}
	public String getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getJspPageFlag() {
		return jspPageFlag;
	}
	public void setJspPageFlag(String jspPageFlag) {
		this.jspPageFlag = jspPageFlag;
	}
	public SystemDefault getSystemDefault() {
		return systemDefault;
	}
	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}
	public boolean isOwnbillingVanline() {
		return ownbillingVanline;
	}
	public void setOwnbillingVanline(boolean ownbillingVanline) {
		this.ownbillingVanline = ownbillingVanline;
	}
	public boolean isAutomaticReconcile() {
		return automaticReconcile;
	}
	public void setAutomaticReconcile(boolean automaticReconcile) {
		this.automaticReconcile = automaticReconcile;
	}
	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}
	public List getAccInvoicePostDateList() {
		return accInvoicePostDateList;
	}
	public void setAccInvoicePostDateList(List accInvoicePostDateList) {
		this.accInvoicePostDateList = accInvoicePostDateList;
	}
	

}
