<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>
<table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="width:100%;margin:0px;padding:0px;margin-left:25px;" >
	<tbody>
		<c:forEach var="outerMap" items="${resourceMap1}">
			<tr>
				<div id="resourcesDay">
					<td height="10" width="100%" align="left" style="margin:0px">  
				 
						<div onClick="setAnimatedColFocus('${outerMap.key}');" style="margin:0px">
							<table cellpadding="0" cellspacing="0"  border="0" style="width:870px;margin:0px;padding:0px;">
								<tr>
									<td class="headtab_left"></td>
									<td NOWRAP class="headtab_center" >Day<c:out value=" ${outerMap.key}"/></td>
									<td width="15" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_center">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
							</table>
						</div>
						
						<div id="Day${outerMap.key}" class="switchgroup1" >
						<div id="DayTable${outerMap.key}" style="display: none;">
					 		<table cellpadding="0" cellspacing="3"  border="0" style="width:830px;margin:0px;padding:0px;">
								<tr>
									 <td style="width:70px;padding-left:20px">
									 	<input type="button" class="cssbutton1" onclick="addResource('${outerMap.key}');" value="Add" style="width:60px; height:25px;margin-bottom:5px;margin-top:5px;" tabindex="83"/>
									 </td>
									 <td style="width:100px;">
									 <s:select cssClass="list-menu" id="catTempID${outerMap.key}" name="catTempID${outerMap.key}" list="%{resourceCategory}" />
									 	<%-- <select class="list-menu" id="catTempID${outerMap.key}" name="catTempID${outerMap.key}">
									 		<option value="">Select Category</option>
										  	<option value="C">Crew</option>
										  	<option value="E">Equipment</option>
										  	<option value="M">Material</option>
										  	<option value="V">Vehicle</option>
										</select>--%>
									 </td>
									 <td id="date${outerMap.key}">
									 	<table style="margin:0px;padding:0px;">
									 		<tr>
									 			 <td class="listwhitetext" align="right" style="width:100px;">Begin Date</td>
								   				 <td style="width:100px;"><s:textfield cssClass="input-text" onchange="dateAutoSave(this.value,'${outerMap.key}');" name="beginDt${outerMap.key}" id="beginDt${outerMap.key}" readonly="true" size="8" maxlength="12" />
								   					<img id="beginDt${outerMap.key}-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkBillingCompleteStatus(this.id,'${outerMap.key}');"/>
								   				 </td>
								 				 <td class="listwhitetext" align="right" style="width:100px;">End Date</td>
								   				 <td><s:textfield cssClass="input-text" name="endDt${outerMap.key}" id="endDt${outerMap.key}" onchange="dateAutoSave(this.value,'${outerMap.key}');" readonly="true" size="8" maxlength="12" />
								   					<img id="endDt${outerMap.key}-trigger" onclick="checkBillingCompleteStatus(this.id,'${outerMap.key}');" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
								   				 </td>
									 		</tr>
									 	</table>
									 </td>
								</tr>
							</table>
							<c:forEach items="${outerMap.value}" var="innerMap" varStatus="loop">
								<table width="100%" cellpadding="0" cellspacing="0"  border="0" style="margin:0px;padding:0px;">	
									<tr>
										<td align="left" colspan="10" style="padding: 0px 0px 0px 23px;">
											<div onClick="setAnimatedColCatFocus('cat_${innerMap.key}${outerMap.key}')" style="margin:0px">
												<table cellpadding="0" cellspacing="0"  border="0" style="width:830px;margin:0px;padding:0px;">
													<tr>
														<td class="headtab_left"></td>
														<td NOWRAP class="headtab_center" ><c:out value="${innerMap.key}"/></td>
														<td width="15" valign="top" class="headtab_bg"></td>
														<td class="headtab_bg_center">&nbsp;</td>
														<td class="headtab_right"></td>
													</tr>
												</table>
											</div>
											<div id="${innerMap.key}${outerMap.key}" class="switchgroup1">
											<div id="cat_${innerMap.key}${outerMap.key}" style="display: none;">
												<c:choose>
													<c:when test="${innerMap.value == '[]' || innerMap.value == ''}">
														<display:table name="${innerMap.value}" class="table" requestURI="" id="resourceList" style="width:700px;margin:2px 0px;">
															<display:column title="Category" style="width:70px" /> 
														    <display:column title="Resource" style="width:200px"/>
														    <display:column title="Est. Quantity" style="width:100px"/>   
														    <display:column title="Est. Hour" style="width:100px"/>   
														    <display:column title="Comments" style="width:320px"/>
														    <display:column title="Remove" style="width: 15px;"/>
														</display:table>
													</c:when>
													<c:otherwise>
														<display:table name="${innerMap.value}" class="table" requestURI="" id="resourceList" style="width:700px;margin:2px 0px;">
															<c:choose>
																<c:when test="${resourceList.revisionInvoice == 'true'}">
																	<display:column title="Category" style="width:70px">  
																		<c:forEach var="chrms" items="${resourceCategory}" varStatus="loopStatus">
																				<c:if test="${chrms.key == resourceList.type}">
																					<input type="text" id="category${resourceList.id}" name="category${resourceList.id}" value="${chrms.value}" class="input-textUpper"  readonly="true" style="width:120px" />
																				</c:if>
																           </c:forEach> 
																	</display:column>
																	<display:column title="Resource" style="width:200px">
																   		<input type="text" id="descript${resourceList.id}" name="descript${resourceList.id}" value="${resourceList.descript}" class="input-textUpper"  readonly="true" size="30" maxlength="50" />
																   	</display:column>
																   	<display:column title="Est. Quantity" style="width:100px">   
																   		<input type="text" id="cost${resourceList.id}" name="cost${resourceList.id}" value="${resourceList.qty}" class="input-textUpper" readonly="true"  size="10" maxlength="10" />
																   	</display:column>
																   	<display:column title="Est. Hour" style="width:100px">   
																   		<input type="text" id="hour${resourceList.id}" name="hour${resourceList.id}" value="${resourceList.estHour}" class="input-textUpper" readonly="true"  size="10" maxlength="10" />
																   	</display:column>
																   	<display:column title="Comments" style="width:320px">
																   		<input type="text" id="comments${resourceList.id}" name="comments${resourceList.id}" value="${resourceList.comments}" class="input-textUpper"  readonly="true" size="50" maxlength="50" />
																   	</display:column>
																  	<display:column title="Remove" style="width: 15px;">
																		<a><img align="middle" onclick="confirmDeleteResource('${resourceList.id}','${outerMap.key}','${innerMap.key}','${resourceList.ticketTransferStatus}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
																	</display:column>
																</c:when>
																<c:otherwise>
																	  	<display:column title="Category" style="width:120px;margin-top:2px;"> 
																			<select id="category${resourceList.id}" name ="category${resourceList.id}" style="width:120px" onchange="findResource(this.value,'descript${resourceList.id}');" class="list-menu"> 
																				<c:forEach var="chrms" items="${resourceCategory}" varStatus="loopStatus">
																					<c:choose>
																						<c:when test="${chrms.key == resourceList.type}">
																							<c:set var="selectedInd" value=" selected"></c:set>
																						</c:when>
																						<c:otherwise>
																							<c:set var="selectedInd" value=""></c:set>
																						</c:otherwise>
																					</c:choose>
																					<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
																						<c:out value="${chrms.value}"></c:out>
																					</option>
																		           </c:forEach> 
																			</select>
																	   	</display:column>
																	   	<display:column title="Resource" style="width:200px">
																	   		<input type="text" id="descript${resourceList.id}" name="descript${resourceList.id}" value="${resourceList.descript}" onchange="autoSaveAjax1('${resourceList.id}','${outerMap.key}','RESOURCE');" onkeyup="checkChar(this,'descript${resourceList.id}','category${resourceList.id}','choices${resourceList.id}');" onclick="setFlag();" size="30" maxlength="50" class="input-text"  />
																	   		<div id="choices${resourceList.id}" class="listwhitetext" onclick="autoSaveAjax('${resourceList.id}','${outerMap.key}','RES');" ></div>
																	   	</display:column>
																	   	<display:column title="Est. Quantity" style="width:100px">   
																	   		 <input type="text" id="cost${resourceList.id}" name="cost${resourceList.id}" value="${resourceList.qty}" onchange="validateHubLimit('${resourceList.id}','${outerMap.key}','','${resourceList.workTicketID}');" size="10" maxlength="10" class="input-text" /> 
																	   		<%-- <input type="text" id="cost${resourceList.id}" name="cost${resourceList.id}" value="${resourceList.qty}" onchange="autoSaveAjax('${resourceList.id}','${outerMap.key}');" size="10" maxlength="10" class="input-text" /> --%>
																	   	</display:column>
																	   	<display:column title="Est. Hour" style="width:100px">   
																	   		<input type="text" id="hour${resourceList.id}" name="hour${resourceList.id}" value="${resourceList.estHour}" onchange="autoSaveAjax('${resourceList.id}','${outerMap.key}');" size="10" maxlength="10" class="input-text" onkeyup="" />
																	   	</display:column>
																	   	<display:column title="Comments" style="width:320px">
																	   		<input type="text" id="comments${resourceList.id}" name="comments${resourceList.id}" value="${resourceList.comments}" onkeyup="checkSPChar('${resourceList.id}');" onchange="checkSPChar('${resourceList.id}');autoSaveAjax('${resourceList.id}','${outerMap.key}');" size="50" maxlength="50" class="input-text" />
																	   	</display:column>
																		<display:column title="Remove" style="width: 15px;">
																			<a><img align="middle" onclick="confirmDeleteResource('${resourceList.id}','${outerMap.key}','${innerMap.key}','${resourceList.ticketTransferStatus}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
																		</display:column>		
																</c:otherwise>
															</c:choose>	
														</display:table>
													</c:otherwise>
												</c:choose>
												</div>
											</div>
										</td>
									</tr> 
								</table>
							</c:forEach>
							<div style="height:5px;"></div>
							</div>
						</div>
					</td>
				</div>
			</tr>
		</c:forEach> 
		<tr><td style="height:4px;"></td></tr>
	</tbody>
</table>
