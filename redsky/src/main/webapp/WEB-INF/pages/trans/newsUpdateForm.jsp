<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
    <title><fmt:message key="newsUpdateDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='newsUpdateDetail.heading'/>"/>
    <style type="text/css">
    #mainPopup {
	padding-left:0px;
	padding-right:0px;
	
}
div#page {
margin:0;
padding:0;
text-align:center;
}
</style>
</head>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
</script>  

<script language="javascript" type="text/javascript">
       
       
       function cancel(){
       var corpID=document.forms['newsUpdateForm'].elements['corpID'].value
       	location.href="newsUpdates.html?decorator=popup&popup=true&corpID="+corpID;
       }
         
       function setFlagValue(){
		document.forms['newsUpdateForm'].elements['hitFlag'].value = '1';
	   }
	   
	   function checkFormValue(){
	   	var publishedDate=document.forms['newsUpdateForm'].elements['newsUpdate.publishedDate'].value;
	   	var effectiveDate=document.forms['newsUpdateForm'].elements['newsUpdate.effectiveDate'].value;
	   	var functionality=document.forms['newsUpdateForm'].elements['newsUpdate.functionality'].value;
	   	var details=document.forms['newsUpdateForm'].elements['newsUpdate.details'].value;
	   	var corpIds=document.forms['newsUpdateForm'].elements['newsUpdate.corpId'].value;
	   	var retention=document.forms['newsUpdateForm'].elements['newsUpdate.retention'].value;
	   	var files=document.forms['newsUpdateForm'].elements['file'];
	   		   	
	   	if(publishedDate.trim()==''){
	   		alert('Date is a required field.');
	   		return false;
	   	}else if(functionality.trim()==''){
	   		alert('Functionality is a required field.');
	   		return false;
	   	}else if(details.trim()==''){
	   		alert('Details is a required field.');
	   		return false;
	   	}else if(corpIds.trim()==''){
	   		alert('CorpId is a required field.');
	   		return false;
	   	}else if(retention.trim()==''){
	   		alert('Retention is required field.');
	   		return false;
	   	}else if(effectiveDate.trim()==''){
			alert('Display Start Date is required field.');
			return false;
	   		}
	   }
	   
	   function ge(dateObj){
			  return new Date(new Date(dateObj.getFullYear(), dateObj.getUTCMonth(),1)-1);
		  	}
	   	   
	   function changeStatus(){
		   var effDate =document.forms['newsUpdateForm'].elements['newsUpdate.effectiveDate'].value;
		   var endDate =document.forms['newsUpdateForm'].elements['newsUpdate.endDisplayDate'].value;
		   var days =document.forms['newsUpdateForm'].elements['newsUpdate.retention'].value;
		   if(days==null ||days==""){
			   	 alert("Please select the Valid Month");
			   	return false;
			    	}
			var   days1=parseInt(days);
			 //  alert(days1+1);
			var mySplitResult = effDate.split("-");
			var day = mySplitResult[0];
			var month = mySplitResult[1];
			var year = mySplitResult[2];
			  
			 if(month == 'Jan'){
			       month = "01";
			   }else if(month == 'Feb'){
			       month = "02";
			   }else if(month == 'Mar'){
			       month = "03"
			   }else if(month == 'Apr'){
			       month = "04"
			   }else if(month == 'May'){
			       month = "05"
			   }else if(month == 'Jun'){
			       month = "06"
			   }else if(month == 'Jul'){
			       month = "07"
			   }else if(month == 'Aug'){
			       month = "08"
			   }else if(month == 'Sep'){
			       month = "09"
			   }else if(month == 'Oct'){
			       month = "10"
			   }else if(month == 'Nov'){
			       month = "11"
			   }else if(month == 'Dec'){
			       month = "12";
			   }
			  year = "20"+year;
			  var dateObj = new Date(year,month-1,day);
			   // alert("previous "+dateObj);
			 if(effDate==null || effDate=="")
			 {
				endDate ="";
			 }else{
				   dateObj.setMonth(dateObj.getMonth()+(days1+1));
				   document.forms['newsUpdateForm'].elements['newsUpdate.endDisplayDate'].value = Calendar.printDate(new Date(ge(dateObj)),"%d-%b-%y"); 
			   }
	   }
	   
	  
	   function fillEndDate(){
			var effDate =document.forms['newsUpdateForm'].elements['newsUpdate.effectiveDate'].value;
			var endDate =document.forms['newsUpdateForm'].elements['newsUpdate.endDisplayDate'].value;
			var days =document.forms['newsUpdateForm'].elements['newsUpdate.retention'].value;
			var   days1=parseInt(days);
			 // alert(effDate)
			 if(effDate!=null && effDate!='' && days!=null && days!=''){
			var mySplitResult = effDate.split("-");
			var day = mySplitResult[0];
			var month = mySplitResult[1];
			var year = mySplitResult[2];
			   
			  if(month == 'Jan')
			   {
			       month = "01";
			   }
			   else if(month == 'Feb')
			   {
			       month = "02";
			   }
			   else if(month == 'Mar')
			   {
			       month = "03"
			   }
			   else if(month == 'Apr')
			   {
			       month = "04"
			   }
			   else if(month == 'May')
			   {
			       month = "05"
			   }
			   else if(month == 'Jun')
			   {
			       month = "06"
			   }
			   else if(month == 'Jul')
			   {
			       month = "07"
			   }
			   else if(month == 'Aug')
			   {
			       month = "08"
			   }
			   else if(month == 'Sep')
			   {
			       month = "09"
			   }
			   else if(month == 'Oct')
			   {
			       month = "10"
			   }
			   else if(month == 'Nov')
			   {
			       month = "11"
			   }
			   else if(month == 'Dec')
			   {
			       month = "12";
			   }
			  year = "20"+year;
			  var dateObj = new Date(year,month-1,day);
			   // alert("previous "+dateObj);
			   dateObj.setMonth(dateObj.getMonth()+(days1+1));
			   document.forms['newsUpdateForm'].elements['newsUpdate.endDisplayDate'].value = Calendar.printDate(new Date(ge(dateObj)),"%d-%b-%y"); 
			 }
	   
	   }
	   
	   
	   var formatDate = function (formatDate, formatString) {
			if(formatDate instanceof Date) {
				var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				var monthsArr = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				
				var yyyy = formatDate.getFullYear();
				var yy = yyyy.toString().substring(2);
				var m = formatDate.getMonth();
				alert(m)
				var min = monthsArr[m];
				alert(min);
				var m = m < 10 ? "0" + m : m;
				var d = formatDate.getDate();
				var dd = d < 10 ? "0" + d : d;
				
				var h = formatDate.getHours();
				var hh = h < 10 ? "0" + h : h;
				var n = formatDate.getMinutes();
				var nn = n < 10 ? "0" + n : n;
				var s = formatDate.getSeconds();
				var ss = s < 10 ? "0" + s : s;

				formatString = formatString.replace(/yyyy/i, yyyy);
				formatString = formatString.replace(/yy/i, yy);
				alert(min);
				formatString = formatString.replace(/mmm/i, min);
				alert(formatString);
			    //formatString = formatString.replace(/mm/i, mm);
				//formatString = formatString.replace(/m/i, m);
				formatString = formatString.replace(/dd/i, dd);
				formatString = formatString.replace(/d/i, d);
				formatString = formatString.replace(/hh/i, hh);
				formatString = formatString.replace(/h/i, h);
				formatString = formatString.replace(/nn/i, nn);
				formatString = formatString.replace(/n/i, n);
				formatString = formatString.replace(/ss/i, ss);
				formatString = formatString.replace(/s/i, s);
			return formatString;
			} else {
				return "";
			}
		}
	   </script>
	   <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
	   
	    <script language="javascript" type="text/javascript">
			<%@ include file="/common/formCalender.js"%>
		</script> 
	   
	    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
		<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

	<!-- Modification closed here -->
   		</script>
<s:form id="newsUpdateForm" action="saveNewsUpdate.html?true" method="post" validate="true" enctype="multipart/form-data">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="newsUpdate.id" value="%{newsUpdate.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="id1" value="<%=request.getParameter("id")%>"/>
<s:hidden name="corpID" value="<%= request.getParameter("corpID")%>"/>
<s:hidden name="formStatus" value="" />
<c:if test="${not empty newsUpdate.fileName}">
<s:hidden name="fileFileName1" value="${newsUpdate.fileName}"/>
<s:hidden name="location1" value="${newsUpdate.location}"/>
</c:if>
<c:set var="corpID" value="<%=request.getParameter("corpID") %>"/>

<div id="newmnav">
		  <ul>
		     <li id="newmnav1" style="background:#FFF"><a class="current"><span>Release&nbsp;Updates&nbsp;Detail</span></a></li>
		     <li><a href="newsUpdates.html?true&corpID=${sessionCorpID}"><span>Release&nbsp;Updates&nbsp;List</span></a></li>
		  </ul>
</div>		
<div class="spn" style="!line-height:7px;">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<c:if test="${not empty newsUpdate.id}">
</c:if>
<c:if test="${empty newsUpdate.id}">
</c:if>
<script type="text/javascript">
   document.getElementsByName('file')[0].value=fileLocation;
	alert('${fileLocation}');
</script>
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:793px;">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
	 	<tbody>  	
		  	<tr>
			<td align="right" class="listwhitetext">Release Date<font color="red" size="2">*</font></td>
			<c:if test="${not empty newsUpdate.publishedDate}">
				<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="newsUpdate.publishedDate" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="publishedDate" name="newsUpdate.publishedDate" value="%{customerFileMoveDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onblur="" onselect="fillEndDate();"/>
				<img id="publishedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""/></td>
			</c:if>
			<c:if test="${empty newsUpdate.publishedDate}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="publishedDate" name="newsUpdate.publishedDate" value="%{customerFileMoveDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onblur="" onselect="fillEndDate();"/>
				<img id="publishedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""/></td>
			</c:if>
	
			<td class="listwhitetext" align="right" width="50">Retention<font color="red" size="2">*</font><br>(months)&nbsp;&nbsp;&nbsp;</td>
			<td align="left" ><s:select cssClass="list-menu" name="newsUpdate.retention" list="{1,2,3}" onchange="changeStatus();" required="true" cssStyle="width:80px" headerKey="" headerValue=""/> </td>
			</tr>
			<tr>
			<td align="right" class="listwhitetext">Display Start Date<font color="red" size="2">*</font></td>
			<c:if test="${not empty newsUpdate.effectiveDate}">
				<s:text id="effectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="newsUpdate.effectiveDate" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="effectiveDate" name="newsUpdate.effectiveDate" value="%{effectiveDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onblur="" onselect="fillEndDate();"/>
				<img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="getId(this.retention)"/></td>
			</c:if>
			<c:if test="${empty newsUpdate.effectiveDate}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="effectiveDate" name="newsUpdate.effectiveDate" required="true" size="7" maxlength="11" readonly="true" onblur="" onkeydown="return onlyDel(event,this)" onselect="fillEndDate();"/>
				<img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="getId(this.retention)"/></td>
			</c:if>
			</tr>
			<tr>
			<td align="right" class="listwhitetext">Display End Date</td>
			<c:if test="${not empty newsUpdate.endDisplayDate}">
			<s:text id="endDisplayDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="newsUpdate.endDisplayDate"/></s:text>
			<td align="left" style=""><s:textfield cssClass="input-text" id="endDisplayDate" name="newsUpdate.endDisplayDate" value="%{endDisplayDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  onblur="" />
			<img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
			<c:if test="${empty newsUpdate.endDisplayDate}">
			<td align="left" style=""><s:textfield cssClass="input-text" id="endDisplayDate" name="newsUpdate.endDisplayDate" value="%{endDisplayDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  onblur="" />
			<img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
			</tr>
			<tr>
			<td class="listwhitetext-event" align="right"><fmt:message key="newsUpdate.module"/></td>
	  		<td align="left" width="173" ><s:select cssClass="list-menu" name="newsUpdate.module" list="%{moduleReport}" cssStyle="width:151px" headerKey="" headerValue=""/> </td>
	  		
	  		<td colspan="6">
	  		<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;paddding:0px;">
	  		<tr>
	  		<td class="listwhitetext-event" align="right">Category&nbsp;</td>
	  		<td align="left" ><s:select cssClass="list-menu" name="newsUpdate.category" list="%{categoryReport}" cssStyle="width:105px" headerKey="" headerValue=""/>&nbsp; </td>
			<td align="right" class="listwhitetext">Ticket #&nbsp;</td>
    		<td colspan="4"><s:textfield name="newsUpdate.ticketNo" size="5" maxlength="6" required="true" cssClass="input-text" onkeypress="return isNumberKey(event);"/></td>
	  		</tr>
	  		</table>
	  		</td>
			</tr>
			<tr>
			<td align="right" class="listwhitetext"><fmt:message key="newsUpdate.functionality"/><font color="red" size="2">*</font></td>
    		<td colspan="6"><s:textfield name="newsUpdate.functionality" size="80" maxlength='79' required="true" cssClass="input-text" /></td>
        	</tr>
    		<tr>
    		<td class="listwhitetext" align="right" >Description<font color="red" size="2">*</font></td>
    		<td colspan="6"><s:textarea name="newsUpdate.details" rows="5" cols="51" cssStyle="border:1px solid #219DD1;" /></td>
    		</tr>
    		<c:if test="${not empty newsUpdate.id}">
 				<%--   <tr>
    			<td class="listwhitetext" align="right">File Name</td>
	 			<td colspan="4"><s:textfield name="newsUpdate.fileName" size="30" cssStyle="border:1px solid #219DD1;" /></td>    
    			<td class="listwhitetext-event" align="right" valign="top"></td>
    			<td></td>
    			</tr>--%>
    		</c:if>
   				<%--  <c:if test="${empty newsUpdate.fileName}">--%>
      		<tr>
   				<%--  <td class="listwhitetext" align="right"><fmt:message key="newsUpdate.location"/></td>--%>
	     	<td class="listwhitetext-event" align="right" ></td>
            <tr>
			<td class="listwhitetext"  align="right" >Release Note Upload</td>
			<td colspan="6"><s:file name="file" label="%{getText('newsUpdateForm.file')}" required="true" size="40" /></td>
			
			</tr>
    		</tr>
            <tr>
    		<td class="listwhitetext" align="right" >Corp Id<font color="red" size="2">*</font></td>
    		<td align="left"><s:select cssClass="list-menu" name="newsUpdate.corpId" list="%{distinctCorpId}" cssStyle="width:120px" headerKey="" headerValue=""/></td>
			<td class="listwhitetext" align="right" colspan="2">Visible <s:checkbox name="newsUpdate.visible"  value ="${newsUpdate.visible }" cssStyle="vertical-align:middle;"/> </td>
			<td class="listwhitetext" align="right" colspan="3">Partial <s:checkbox name="newsUpdate.status"  value ="${newsUpdate.status }" cssStyle="vertical-align:middle;" /> </td>
			</tr>
     		<tr>
			<td align="left" height="10px"></td>
			</tr>  
	</tbody>
	</table></td></tr></tbody></table>
	</div>
	<div class="bottom-header"><span></span></div>
	</div>
	</div>
	<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message
					key='billing.createdOn' /></b></td>
				<td style="width:130px"><fmt:formatDate
					var="customerFileCreatedOnFormattedValue"
					value="${newsUpdate.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="newsUpdate.createdOn"
					value="${customerFileCreatedOnFormattedValue}" /> <fmt:formatDate
					value="${newsUpdate.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message
					key='billing.createdBy' /></b></td>
				<c:if test="${not empty newsUpdate.id}">
					<s:hidden name="newsUpdate.createdBy" />
					<td style="width:65px"><s:label name="createdBy"
						value="%{newsUpdate.createdBy}" /></td>
				</c:if>
				<c:if test="${empty newsUpdate.id}">
					<s:hidden name="newsUpdate.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>				
			<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message
					key='billing.updatedOn' /></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue"
					value="${newsUpdate.modifyOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="newsUpdate.modifyOn"
					value="${customerFileupdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate
					value="${newsUpdate.modifyOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='billing.updatedBy' /></b></td>	
				<c:if test="${not empty newsUpdate.id}">
								<s:hidden name="newsUpdate.modifyBy"/>
								<td style="width:85px ;font-size:1em;"><s:label name="updatedBy" value="%{newsUpdate.modifyBy}"/></td>
							</c:if>
							<c:if test="${empty newsUpdate.id}">
								<s:hidden name="newsUpdate.modifyBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px;font-size:1em;"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>	
			</tr>
		</tbody>
	</table>
    <li>
    	<c:if test="${sessionCorpID=='TSFT' }">    
            <s:submit cssClass="cssbuttonA" method="save" key="button.save" theme="simple" onclick="setFlagValue();  return checkFormValue(); "/>
         	<c:if test="${not empty newsUpdate.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editNewsUpdate.html?true&corpID=true"/>'" 
	        value="<fmt:message  key="button.add"/>" tabindex="88"/> 
         	</c:if>
         	<s:reset cssClass="cssbutton1" key="Reset" />
         	<c:if test="${not empty newsUpdate.id}">
         	<input type="button" class="cssbutton1" value="<fmt:message  key="button.addAndCopy"/>" onclick="location.href='<c:url   value="/addAndCopyNewsUpdateForm.html?true&cid=${newsUpdate.id}"/>'" style="width:100px; height:25px" />
         	</c:if>
   </c:if>
    </li>
    			<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
				<s:hidden name="hitFlag" />
				<c:if test="${hitFlag == 1}" >
					<c:redirect url="/newsUpdates.html?true&corpID=${sessionCorpID}"/>
				</c:if>	
<div id="mydiv" style="position:absolute;"></div>
</s:form>
				
<script type="text/javascript">
    Form.focusFirstElement($("newsUpdateForm"));
</script>
<script type="text/javascript">
var fieldTarget;
function getId(fieldId){
	fieldTarget = fieldId.split('_')[0];
}
function hitOnSelect(){
	if(fieldTarget == 'effectiveDate'){
		fillEndDate();	
	}
}
	setOnSelectBasedMethods(["hitOnSelect()"]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">

   function addNewsUpdateForm(){
	
   			var url = "editNewsUpdate.html";	 
			document.forms['newsUpdateForm'].action= url;	 
 			document.forms['newsUpdateForm'].submit();
			return true;
}
   
		   function isNumberKey(evt){
		         var charCode = (evt.which) ? evt.which : event.keyCode
		         if (charCode > 31 && (charCode < 48 || charCode > 57))
		          
		        	 return false;
		             return true;
		      }
	       
</script>
<script type="text/javascript">
var efffield=document.forms['newsUpdateForm'].elements['newsUpdate.effectiveDate'];
if(efffield==null || efffield.value==null || efffield.value==''){
	//alert("hiiii");
	document.forms['newsUpdateForm'].elements['newsUpdate.endDisplayDate'].value='';
}

</script>