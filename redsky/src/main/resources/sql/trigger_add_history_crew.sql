
DELIMITER $$
create trigger redsky.trigger_add_history_crew BEFORE UPDATE on redsky.crew 
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

   IF (OLD.payhour  <> NEW.payhour  ) THEN
       select CONCAT(fieldNameList,'crew.payhour ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payhour,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payhour,'~') into newValueList;
   END IF;


   IF ((OLD.classificationEffective <> NEW.classificationEffective) or (OLD.classificationEffective is null and NEW.classificationEffective is not null)
      or (OLD.classificationEffective is not null and NEW.classificationEffective is null)) THEN
       select CONCAT(fieldNameList,'crew.classificationEffective~') into fieldNameList;
       IF(OLD.classificationEffective is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.classificationEffective is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.classificationEffective is not null) THEN
        	select CONCAT(oldValueList,OLD.classificationEffective,'~') into oldValueList;
     	END IF;
      	IF(NEW.classificationEffective is not null) THEN
        	select CONCAT(newValueList,NEW.classificationEffective,'~') into newValueList;
      	END IF;
   END IF;


   IF (OLD.active  <> NEW.active) THEN
       select CONCAT(fieldNameList,'crew.active ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.active,'~') into oldValueList;
       select CONCAT(newValueList,NEW.active,'~') into newValueList;
   END IF;

   IF ((OLD.hired <> NEW.hired) or (OLD.hired is null and NEW.hired is not null)
      or (OLD.hired is not null and NEW.hired is null)) THEN
       select CONCAT(fieldNameList,'crew.hired~') into fieldNameList;
       IF(OLD.hired is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.hired is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.hired is not null) THEN
		select CONCAT(oldValueList,OLD.hired,'~') into oldValueList;
	END IF;
	IF(NEW.hired is not null) THEN
		select CONCAT(newValueList,NEW.hired,'~') into newValueList;
	END IF;
   END IF;

  IF ((OLD.unionName <> NEW.unionName) or (OLD.unionName is null and NEW.unionName is not null)
      or (OLD.unionName is not null and NEW.unionName is null)) THEN
       select CONCAT(fieldNameList,'crew.unionName~') into fieldNameList;
       IF(OLD.unionName is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.unionName is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.unionName is not null) THEN
		select CONCAT(oldValueList,OLD.unionName,'~') into oldValueList;
	END IF;
	IF(NEW.unionName is not null) THEN
		select CONCAT(newValueList,NEW.unionName,'~') into newValueList;
	END IF;
   END IF;
   IF ((OLD.beginHealthWelf <> NEW.beginHealthWelf) or (OLD.beginHealthWelf is null and NEW.beginHealthWelf is not null)
      or (OLD.beginHealthWelf is not null and NEW.beginHealthWelf is null)) THEN
       select CONCAT(fieldNameList,'crew.beginHealthWelf~') into fieldNameList;
       IF(OLD.beginHealthWelf is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.beginHealthWelf is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.beginHealthWelf is not null) THEN
		select CONCAT(oldValueList,OLD.beginHealthWelf,'~') into oldValueList;
	END IF;
	IF(NEW.beginHealthWelf is not null) THEN
		select CONCAT(newValueList,NEW.beginHealthWelf,'~') into newValueList;
	END IF;
   END IF;
   IF ((OLD.terminatedDate <> NEW.terminatedDate) or (OLD.terminatedDate is null and NEW.terminatedDate is not null)
      or (OLD.terminatedDate is not null and NEW.terminatedDate is null)) THEN
       select CONCAT(fieldNameList,'crew.terminatedDate~') into fieldNameList;
       IF(OLD.terminatedDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.terminatedDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.terminatedDate is not null) THEN
		select CONCAT(oldValueList,OLD.terminatedDate,'~') into oldValueList;
	END IF;
	IF(NEW.terminatedDate is not null) THEN
		select CONCAT(newValueList,NEW.terminatedDate,'~') into newValueList;
	END IF;
   END IF;
   IF ((OLD.begginingPension <> NEW.begginingPension) or (OLD.begginingPension is null and NEW.begginingPension is not null)
      or (OLD.begginingPension is not null and NEW.begginingPension is null)) THEN
       select CONCAT(fieldNameList,'crew.begginingPension~') into fieldNameList;
       IF(OLD.begginingPension is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.begginingPension is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.begginingPension is not null) THEN
		select CONCAT(oldValueList,OLD.begginingPension,'~') into oldValueList;
	END IF;
	IF(NEW.begginingPension is not null) THEN
		select CONCAT(newValueList,NEW.begginingPension,'~') into newValueList;
	END IF;
   END IF;

   IF ((OLD.sickHours <> NEW.sickHours) or (OLD.sickHours is null and NEW.sickHours is not null)
      or (OLD.sickHours is not null and NEW.sickHours is null)) THEN
        select CONCAT(fieldNameList,'crew.sickHours~') into fieldNameList;
        IF(OLD.sickHours is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sickHours is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.sickHours is not null) then
            select CONCAT(oldValueList,OLD.sickHours,'~') into oldValueList;
        END IF;
        IF(NEW.sickHours is not null) then
            select CONCAT(newValueList,NEW.sickHours,'~') into newValueList;
        END IF;
    END IF;

    IF ((OLD.personalDayUsed <> NEW.personalDayUsed) or (OLD.personalDayUsed is null and NEW.personalDayUsed is not null)
      or (OLD.personalDayUsed is not null and NEW.personalDayUsed is null)) THEN
        select CONCAT(fieldNameList,'crew.personalDayUsed~') into fieldNameList;
        IF(OLD.personalDayUsed is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.personalDayUsed is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.personalDayUsed is not null) then
            select CONCAT(oldValueList,OLD.personalDayUsed,'~') into oldValueList;
        END IF;
        IF(NEW.personalDayUsed is not null) then
            select CONCAT(newValueList,NEW.personalDayUsed,'~') into newValueList;
        END IF;
    END IF;


   IF ((OLD.vacationHrs <> NEW.vacationHrs) or (OLD.vacationHrs is null and NEW.vacationHrs is not null)
      or (OLD.vacationHrs is not null and NEW.vacationHrs is null)) THEN
        select CONCAT(fieldNameList,'crew.vacationHrs~') into fieldNameList;
        IF(OLD.vacationHrs is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.vacationHrs is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.vacationHrs is not null) then
            select CONCAT(oldValueList,OLD.vacationHrs,'~') into oldValueList;
        END IF;
        IF(NEW.vacationHrs is not null) then
            select CONCAT(newValueList,NEW.vacationHrs,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.sickUsed <> NEW.sickUsed) or (OLD.sickUsed is null and NEW.sickUsed is not null)
      or (OLD.sickUsed is not null and NEW.sickUsed is null)) THEN
        select CONCAT(fieldNameList,'crew.sickUsed~') into fieldNameList;
        IF(OLD.sickUsed is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sickUsed is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.sickUsed is not null) then
            select CONCAT(oldValueList,OLD.sickUsed,'~') into oldValueList;
        END IF;
        IF(NEW.sickUsed is not null) then
            select CONCAT(newValueList,NEW.sickUsed,'~') into newValueList;
        END IF;
    END IF;

     IF ((OLD.vacationUsed <> NEW.vacationUsed) or (OLD.vacationUsed is null and NEW.vacationUsed is not null)
      or (OLD.vacationUsed is not null and NEW.vacationUsed is null)) THEN
        select CONCAT(fieldNameList,'crew.vacationUsed~') into fieldNameList;
        IF(OLD.vacationUsed is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.vacationUsed is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.vacationUsed is not null) then
            select CONCAT(oldValueList,OLD.vacationUsed,'~') into oldValueList;
        END IF;
        IF(NEW.vacationUsed is not null) then
            select CONCAT(newValueList,NEW.vacationUsed,'~') into newValueList;
        END IF;
    END IF;

     IF (OLD.carryOverAllowed  <> NEW.carryOverAllowed  ) THEN
       select CONCAT(fieldNameList,'crew.carryOverAllowed~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carryOverAllowed,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carryOverAllowed,'~') into newValueList;
   END IF;

   
   
   
   
   
   IF (OLD.firstName  <> NEW.firstName  ) THEN
       select CONCAT(fieldNameList,'crew.firstName ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
   END IF;
   
    IF (OLD.lastName  <> NEW.lastName  ) THEN
       select CONCAT(fieldNameList,'crew.lastName ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
   END IF;
   
    IF (OLD.corpID  <> NEW.corpID  ) THEN
       select CONCAT(fieldNameList,'crew.corpID ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;
   
    IF (OLD.initial  <> NEW.initial  ) THEN
       select CONCAT(fieldNameList,'crew.initial ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.initial,'~') into oldValueList;
       select CONCAT(newValueList,NEW.initial,'~') into newValueList;
   END IF;
   
    IF (OLD.branch  <> NEW.branch  ) THEN
       select CONCAT(fieldNameList,'crew.branch ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.branch,'~') into oldValueList;
       select CONCAT(newValueList,NEW.branch,'~') into newValueList;
   END IF;
   
    IF (OLD.department  <> NEW.department  ) THEN
       select CONCAT(fieldNameList,'crew.department ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.department,'~') into oldValueList;
       select CONCAT(newValueList,NEW.department,'~') into newValueList;
   END IF;
   
    IF (OLD.warehouse  <> NEW.warehouse  ) THEN
       select CONCAT(fieldNameList,'crew.warehouse ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
       select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
   END IF;
   
    IF (OLD.bucket  <> NEW.bucket  ) THEN
       select CONCAT(fieldNameList,'crew.bucket ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bucket,'~') into oldValueList;
       select CONCAT(newValueList,NEW.bucket,'~') into newValueList;
   END IF;
   
   
     IF (OLD.valueDate <> NEW.valueDate ) THEN
       select CONCAT(fieldNameList,'crew.valueDate~') into fieldNameList;
       IF(OLD.valueDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.valueDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.valueDate is not null) THEN
        	select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.valueDate is not null) THEN
        	select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
      	END IF;
   END IF;
   
    IF (OLD.accntingCrossRef  <> NEW.accntingCrossRef  ) THEN
       select CONCAT(fieldNameList,'crew.accntingCrossRef ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accntingCrossRef,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accntingCrossRef,'~') into newValueList;
   END IF;
   
     IF (OLD.cdl  <> NEW.cdl  ) THEN
       select CONCAT(fieldNameList,'crew.cdl ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cdl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cdl,'~') into newValueList;
   END IF;
    
	
     IF (OLD.cdlExpires <> NEW.cdlExpires ) THEN
       select CONCAT(fieldNameList,'crew.cdlExpires~') into fieldNameList;
       IF(OLD.cdlExpires is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.cdlExpires is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.cdlExpires is not null) THEN
        	select CONCAT(oldValueList,OLD.cdlExpires,'~') into oldValueList;
     	END IF;
      	IF(NEW.cdlExpires is not null) THEN
        	select CONCAT(newValueList,NEW.cdlExpires,'~') into newValueList;
      	END IF;
   END IF;
   
   
     IF (OLD.doTPhysical <> NEW.doTPhysical ) THEN
       select CONCAT(fieldNameList,'crew.doTPhysical~') into fieldNameList;
       IF(OLD.doTPhysical is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.doTPhysical is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.doTPhysical is not null) THEN
        	select CONCAT(oldValueList,OLD.doTPhysical,'~') into oldValueList;
     	END IF;
      	IF(NEW.doTPhysical is not null) THEN
        	select CONCAT(newValueList,NEW.doTPhysical,'~') into newValueList;
      	END IF;
   END IF;
   
   
     IF (OLD.drivingClass  <> NEW.drivingClass  ) THEN
       select CONCAT(fieldNameList,'crew.drivingClass ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.drivingClass,'~') into oldValueList;
       select CONCAT(newValueList,NEW.drivingClass,'~') into newValueList;
   END IF;
   
   
     IF (OLD.employeeId  <> NEW.employeeId  ) THEN
       select CONCAT(fieldNameList,'crew.employeeId ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.employeeId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.employeeId,'~') into newValueList;
   END IF;
   
   
     IF (OLD.greenCard  <> NEW.greenCard  ) THEN
       select CONCAT(fieldNameList,'crew.greenCard ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.greenCard,'~') into oldValueList;
       select CONCAT(newValueList,NEW.greenCard,'~') into newValueList;
   END IF;
   
   
     IF (OLD.late  <> NEW.late  ) THEN
       select CONCAT(fieldNameList,'crew.late ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.late,'~') into oldValueList;
       select CONCAT(newValueList,NEW.late,'~') into newValueList;
   END IF;
   
   
     IF (OLD.licenceReview <> NEW.licenceReview ) THEN
       select CONCAT(fieldNameList,'crew.licenceReview~') into fieldNameList;
       IF(OLD.licenceReview is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.licenceReview is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.licenceReview is not null) THEN
        	select CONCAT(oldValueList,OLD.licenceReview,'~') into oldValueList;
     	END IF;
      	IF(NEW.licenceReview is not null) THEN
        	select CONCAT(newValueList,NEW.licenceReview,'~') into newValueList;
      	END IF;
   END IF;
	
	
	 IF (OLD.officeFax  <> NEW.officeFax  ) THEN
       select CONCAT(fieldNameList,'crew.officeFax ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.officeFax,'~') into oldValueList;
       select CONCAT(newValueList,NEW.officeFax,'~') into newValueList;
   END IF;
   
    IF (OLD.officePhone  <> NEW.officePhone  ) THEN
       select CONCAT(fieldNameList,'crew.officePhone ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.officePhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.officePhone,'~') into newValueList;
   END IF;
   
    IF (OLD.overtimetype  <> NEW.overtimetype  ) THEN
       select CONCAT(fieldNameList,'crew.overtimetype ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.overtimetype,'~') into oldValueList;
       select CONCAT(newValueList,NEW.overtimetype,'~') into newValueList;
   END IF;
   
   
    IF (OLD.socialSecurityNumber  <> NEW.socialSecurityNumber  ) THEN
       select CONCAT(fieldNameList,'crew.socialSecurityNumber ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.socialSecurityNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.socialSecurityNumber,'~') into newValueList;
   END IF;
   
   
    IF (OLD.suspended <> NEW.suspended ) THEN
       select CONCAT(fieldNameList,'crew.suspended~') into fieldNameList;
       IF(OLD.suspended is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.suspended is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.suspended is not null) THEN
        	select CONCAT(oldValueList,OLD.suspended,'~') into oldValueList;
     	END IF;
      	IF(NEW.suspended is not null) THEN
        	select CONCAT(newValueList,NEW.suspended,'~') into newValueList;
      	END IF;
   END IF;
   
   
    IF (OLD.typeofWork  <> NEW.typeofWork  ) THEN
       select CONCAT(fieldNameList,'crew.typeofWork ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.typeofWork,'~') into oldValueList;
       select CONCAT(newValueList,NEW.typeofWork,'~') into newValueList;
   END IF;
   
     IF (OLD.usCitizen <> NEW.usCitizen ) THEN
       select CONCAT(fieldNameList,'crew.usCitizen~') into fieldNameList;
       IF(OLD.usCitizen = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.usCitizen = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.usCitizen = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.usCitizen = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.vlCode  <> NEW.vlCode  ) THEN
       select CONCAT(fieldNameList,'crew.vlCode ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vlCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vlCode,'~') into newValueList;
   END IF;
   
   
   IF ((OLD.expiration <> NEW.expiration) or (OLD.expiration is null and NEW.expiration is not null)
      or (OLD.expiration is not null and NEW.expiration is null)) THEN
       select CONCAT(fieldNameList,'crew.expiration~') into fieldNameList;
       IF(OLD.expiration is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.expiration is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.expiration is not null) THEN
		select CONCAT(oldValueList,OLD.expiration,'~') into oldValueList;
	END IF;
	IF(NEW.expiration is not null) THEN
		select CONCAT(newValueList,NEW.expiration,'~') into newValueList;
	END IF;
   END IF;
   
   
   
   IF (OLD.title  <> NEW.title) THEN
       select CONCAT(fieldNameList,'crew.title ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.title,'~') into oldValueList;
       select CONCAT(newValueList,NEW.title,'~') into newValueList;
   END IF;
   
   IF (OLD.rank  <> NEW.rank) THEN
       select CONCAT(fieldNameList,'crew.rank ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.rank,'~') into oldValueList;
       select CONCAT(newValueList,NEW.rank,'~') into newValueList;
   END IF;
   
   IF (OLD.columnName  <> NEW.columnName) THEN
       select CONCAT(fieldNameList,'crew.columnName ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.columnName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.columnName,'~') into newValueList;
   END IF;
   
   IF (OLD.info  <> NEW.info) THEN
       select CONCAT(fieldNameList,'crew.info ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.info,'~') into oldValueList;
       select CONCAT(newValueList,NEW.info,'~') into newValueList;
   END IF;
   
   IF (OLD.userName  <> NEW.userName) THEN
       select CONCAT(fieldNameList,'crew.userName ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.userName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.userName,'~') into newValueList;
   END IF;
   
   IF (OLD.companyDivision  <> NEW.companyDivision) THEN
       select CONCAT(fieldNameList,'crew.companyDivision ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
       select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
   END IF;
   
   
   
    IF ((OLD.tenure <> NEW.tenure) or (OLD.tenure is null and NEW.tenure is not null)
      or (OLD.tenure is not null and NEW.tenure is null)) THEN
        select CONCAT(fieldNameList,'crew.tenure~') into fieldNameList;
        IF(OLD.tenure is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.tenure is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.tenure is not null) then
            select CONCAT(oldValueList,OLD.tenure,'~') into oldValueList;
        END IF;
        IF(NEW.tenure is not null) then
            select CONCAT(newValueList,NEW.tenure,'~') into newValueList;
        END IF;
    END IF;
	
	 IF ((OLD.lastAnniversaryDate <> NEW.lastAnniversaryDate) or (OLD.lastAnniversaryDate is null and NEW.lastAnniversaryDate is not null)
      or (OLD.lastAnniversaryDate is not null and NEW.lastAnniversaryDate is null)) THEN
       select CONCAT(fieldNameList,'crew.lastAnniversaryDate~') into fieldNameList;
       IF(OLD.lastAnniversaryDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.lastAnniversaryDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;

	IF(OLD.lastAnniversaryDate is not null) THEN
		select CONCAT(oldValueList,OLD.lastAnniversaryDate,'~') into oldValueList;
	END IF;
	IF(NEW.lastAnniversaryDate is not null) THEN
		select CONCAT(newValueList,NEW.lastAnniversaryDate,'~') into newValueList;
	END IF;
   END IF;
   
   IF ((OLD.sickCarriedOver <> NEW.sickCarriedOver) or (OLD.sickCarriedOver is null and NEW.sickCarriedOver is not null)
      or (OLD.sickCarriedOver is not null and NEW.sickCarriedOver is null)) THEN
        select CONCAT(fieldNameList,'crew.sickCarriedOver~') into fieldNameList;
        IF(OLD.sickCarriedOver is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sickCarriedOver is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.sickCarriedOver is not null) then
            select CONCAT(oldValueList,OLD.sickCarriedOver,'~') into oldValueList;
        END IF;
        IF(NEW.sickCarriedOver is not null) then
            select CONCAT(newValueList,NEW.sickCarriedOver,'~') into newValueList;
        END IF;
    END IF;
	
  IF ((OLD.unscheduledLeave <> NEW.unscheduledLeave) or (OLD.unscheduledLeave is null and NEW.unscheduledLeave is not null)
      or (OLD.unscheduledLeave is not null and NEW.unscheduledLeave is null)) THEN
        select CONCAT(fieldNameList,'crew.unscheduledLeave~') into fieldNameList;
        IF(OLD.unscheduledLeave is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.unscheduledLeave is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.unscheduledLeave is not null) then
            select CONCAT(oldValueList,OLD.unscheduledLeave,'~') into oldValueList;
        END IF;
        IF(NEW.unscheduledLeave is not null) then
            select CONCAT(newValueList,NEW.unscheduledLeave,'~') into newValueList;
        END IF;
    END IF;

	IF ((OLD.ignoreForTimeSheet <> NEW.ignoreForTimeSheet) or (OLD.ignoreForTimeSheet is null and NEW.ignoreForTimeSheet is not null)
      or (OLD.ignoreForTimeSheet is not null and NEW.ignoreForTimeSheet is null)) THEN
        select CONCAT(fieldNameList,'crew.ignoreForTimeSheet~') into fieldNameList;
        IF(OLD.ignoreForTimeSheet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.ignoreForTimeSheet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.ignoreForTimeSheet is not null) then
            select CONCAT(oldValueList,OLD.ignoreForTimeSheet,'~') into oldValueList;
        END IF;
        IF(NEW.ignoreForTimeSheet is not null) then
            select CONCAT(newValueList,NEW.ignoreForTimeSheet,'~') into newValueList;
        END IF;
    END IF;
	
	IF ((OLD.paidSickLeave <> NEW.paidSickLeave) or (OLD.paidSickLeave is null and NEW.paidSickLeave is not null)
      or (OLD.paidSickLeave is not null and NEW.paidSickLeave is null)) THEN
        select CONCAT(fieldNameList,'crew.paidSickLeave~') into fieldNameList;
        IF(OLD.paidSickLeave is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.paidSickLeave is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.paidSickLeave is not null) then
            select CONCAT(oldValueList,OLD.paidSickLeave,'~') into oldValueList;
        END IF;
        IF(NEW.paidSickLeave is not null) then
            select CONCAT(newValueList,NEW.paidSickLeave,'~') into newValueList;
        END IF;
    END IF;
	
	IF (OLD.partnerCode  <> NEW.partnerCode) THEN
       select CONCAT(fieldNameList,'crew.partnerCode ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.partnerCode,'~') into newValueList;
   END IF;
   
    IF (OLD.partnerName  <> NEW.partnerName) THEN
       select CONCAT(fieldNameList,'crew.partnerName ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.partnerName,'~') into newValueList;
   END IF;
   
    IF (OLD.licenceNumber  <> NEW.licenceNumber) THEN
       select CONCAT(fieldNameList,'crew.licenceNumber ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.licenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.licenceNumber,'~') into newValueList;
   END IF;
   
    IF (OLD.integrationTool  <> NEW.integrationTool) THEN
       select CONCAT(fieldNameList,'crew.integrationTool ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.integrationTool,'~') into oldValueList;
       select CONCAT(newValueList,NEW.integrationTool,'~') into newValueList;
   END IF;
   
    IF (OLD.deviceNumber  <> NEW.deviceNumber) THEN
       select CONCAT(fieldNameList,'crew.deviceNumber ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deviceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deviceNumber,'~') into newValueList;
   END IF;
   
    
    IF (OLD.supervisorName  <> NEW.supervisorName) THEN
       select CONCAT(fieldNameList,'crew.supervisorName ~') into fieldNameList;
       select CONCAT(oldValueList,OLD.supervisorName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.supervisorName,'~') into newValueList;
   END IF;
   
   
   
   
   
   CALL add_tblHistory (OLD.id,"crew", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$
