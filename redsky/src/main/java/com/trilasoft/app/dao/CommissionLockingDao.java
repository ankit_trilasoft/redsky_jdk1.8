package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CommissionLocking;

public interface CommissionLockingDao extends GenericDao<CommissionLocking, Long>{
public List getCommissionLineId(String cid,String corpId,String commisionGiven,Long aid); 
public String getCommisionLineId(String corpId,Long aid,String commisionGiven);
public String getSentToCommisionLineId(String corpId,String aidList,String commisionGiven,Long aid);
}
