<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title><fmt:message key="partnerExtractDetail.title" /></title>
<meta name="heading" content="<fmt:message key='partnerExtractDetail.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript"> 
function submitByBilltoInvoice(){
  	if(document.forms['billtoSynInvExtractForm'].elements['sysInvoiceBilltoDate'].value==''){
    	alert("Please enter the from date"); 
 		return false;
  	}
  	if(document.forms['billtoSynInvExtractForm'].elements['invCompanyDivision'].value==''){
       	alert("Please enter the company division "); 
		return false;
	}else{
       	document.forms['billtoSynInvExtractForm'].action = 'billtoMasSynInvExtracts.html';
    	document.forms['billtoSynInvExtractForm'].submit(); 
  		return true;
   	}
}

function submitByVendorInvoice(){
  	if(document.forms['vendorSynInvExtractForm'].elements['sysInvoiceVendorDate'].value==''){
    	alert("Please enter the from date"); 
 		return false;
  	}
  	if(document.forms['vendorSynInvExtractForm'].elements['invCompanyDivision'].value==''){
       	alert("Please enter the company division "); 
		return false;
	}else{
       	document.forms['vendorSynInvExtractForm'].action = 'vendorMasSynInvExtracts.html';
    	document.forms['vendorSynInvExtractForm'].submit(); 
  		return true;
   	}
}

function calcDays(){  
	var LastExtractDate ; 
	var formFromDate;
	if(document.forms['vendorSynInvExtractForm'].elements['vendorFormStatus'].value== '1') 
	{
	     formFromDate = document.forms['vendorSynInvExtractForm'].elements['sysInvoiceVendorDate'].value;	  
	     LastExtractDate = document.forms['vendorSynInvExtractForm'].elements['vendorExtractDate'].value;
	    }
    if(document.forms['billtoSynInvExtractForm'].elements['billtoFormStatus'].value== '1')
    {
        formFromDate = document.forms['billtoSynInvExtractForm'].elements['sysInvoiceBilltoDate'].value;	  
        LastExtractDate = document.forms['billtoSynInvExtractForm'].elements['billToExtractDate'].value;
    }
    if(LastExtractDate !=undefined){
    var mySplitResult = LastExtractDate.split("-");
    var day = mySplitResult[0];
    var month = mySplitResult[1];
    var year = mySplitResult[2];
  
    switch(month)
   {
       case "Jan" : month = "01";break; 
       case "Feb" : month = "02";break; 
       case "Mar" : month = "03";break; 
       case "Apr" : month = "04";break; 
       case "May" : month = "05";break; 
       case "Jun" : month = "06";break; 
       case "Jul" : month = "07";break; 
       case "Aug" : month = "08";break; 
       case "Sep" : month = "09";break; 
       case "Oct" : month = "10";break; 
       case "Nov" : month = "11";break; 
       case "Dec" : month = "12";break; 
  } 
   
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = formFromDate.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
 
   if(month2 == 'Jan'){month2 = "01";}
   else if(month2 == 'Feb'){month2 = "02";}
   else if(month2 == 'Mar'){month2 = "03";}
   else if(month2 == 'Apr'){month2 = "04";}
   else if(month2 == 'May'){month2 = "05";}
   else if(month2 == 'Jun'){month2 = "06";}
   else if(month2 == 'Jul'){month2 = "07";}
   else if(month2 == 'Aug'){month2 = "08";}
   else if(month2 == 'Sep'){month2 = "09";}
   else if(month2 == 'Oct'){month2 = "10";}
   else if(month2 == 'Nov'){month2 = "11";}
   else if(month2 == 'Dec'){month2 = "12";} 
  
   var finalDate2 = month2+"-"+day2+"-"+year2;
   LastExtractDate = finalDate.split("-");
   formFromDate = finalDate2.split("-");

  var sDate = new Date(LastExtractDate[0]+"/"+LastExtractDate[1]+"/"+LastExtractDate[2]);
  var eDate = new Date(formFromDate[0]+"/"+formFromDate[1]+"/"+formFromDate[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);

  if(daysApart > 0){ 
  
 if(document.forms['billtoSynInvExtractForm'].elements['billtoFormStatus'].value== '1')
     {
	alert("You cannot enter from date less than the last extract date. '"+document.forms['billtoSynInvExtractForm'].elements['billToExtractDate'].value+"'");
    document.forms['billtoSynInvExtractForm'].elements['sysInvoiceBilltoDate'].value=document.forms['billtoSynInvExtractForm'].elements['billToExtractDate'].value; 
    } 
  if(document.forms['vendorSynInvExtractForm'].elements['vendorFormStatus'].value== '1') 
  {
	  alert("You cannot enter from date less than the last extract date. '"+document.forms['vendorSynInvExtractForm'].elements['vendorExtractDate'].value+"'");
	  document.forms['vendorSynInvExtractForm'].elements['sysInvoiceVendorDate'].value=document.forms['vendorSynInvExtractForm'].elements['vendorExtractDate'].value; 
  }    

  
  }
  document.forms['vendorSynInvExtractForm'].elements['vendorFormStatus'].value = '';
  document.forms['billtoSynInvExtractForm'].elements['billtoFormStatus'].value = '';
}}

 function forDays(){ 
	 document.forms['billtoSynInvExtractForm'].elements['billtoFormStatus'].value ='1'; 
	}
function forDays1(){ 
	 document.forms['vendorSynInvExtractForm'].elements['vendorFormStatus'].value ='1'; 
	} 
    
</script>
</head>
<div id="Layer1" style="width:85%;">
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:10px;"><span></span></div>
<div class="center-content">
<table border="0"> 
<tr><td>
<s:form name="billtoSynInvExtractForm" id="billtoSynInvExtractForm" action="billtoMasSynInvExtracts" method="post" validate="true">
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy}"/> 
<s:hidden id="billtoFormStatus" name="billtoFormStatus" /> 
<table class="" cellspacing="1" cellpadding="3" border="0" style="">

<tr>
<td width="5px"></td>
<td colspan="7" class="bgblue">Billto Data Extract</td></tr>
             <tr>
          <td height="5px"></td>   
          </tr>
  			<tr>
           <td width=""></td>
  			<td class="listwhitetext" align="right">From Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty vendorextracdate}">
					<s:text id="sysInvoiceBilltoDate" name="${FormDateValue1}"> <s:param name="value" value="%{billToExtractDate}"  /></s:text>
			        <s:hidden cssClass="input-text" id="billToExtractDate" name="billToExtractDate" value="%{sysInvoiceBilltoDate}"  /> 
			        <td><s:textfield cssClass="input-text" id="sysInvoiceBilltoDate" name="sysInvoiceBilltoDate" value="%{sysInvoiceBilltoDate}" size="10" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onselect ="calcDays(); "   /> 
			        <img id="sysInvoiceBilltoDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20   onclick="forDays();" /></td>
					</c:if>
					<c:if test="${empty vendorextracdate}" >
						<s:hidden cssClass="input-text" id="billToExtractDate" name="billToExtractDate"  />
						<td><s:textfield cssClass="input-text" id="sysInvoiceBilltoDate" name="sysInvoiceBilltoDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="sysInvoiceBilltoDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>	
				<td align="right" class="listwhitetext"></td>
				<td align="left"></td>		 	
	  		<td width="200"></td><td></td>
	  		</tr> 
	  		 <tr>
          <td height="5px"></td>   
          </tr>
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return submitByBilltoInvoice();"/></td>
	    	
	    	</tr>	 				
	
	</table>  
</s:form>
</td></tr>

<tr><td>
<s:form name="vendorSynInvExtractForm" id="vendorSynInvExtractForm" action="vendorMasSynInvExtracts" method="post" validate="true">
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy}"/> 
    <s:hidden id="vendorFormStatus" name="vendorFormStatus" />
<table class="" cellspacing="1" cellpadding="3" border="0" style="">

<tr>
<td width="5px"></td>
<td colspan="7" class="bgblue">Vendor Data Extract</td></tr>
             <tr>
          <td height="5px"></td>   
          </tr>
  			<tr>
           <td width=""></td>
  			<td class="listwhitetext" align="right">From Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty billToExtractDate}">
						<s:text id="sysInvoiceVendorDate" name="${FormDateValue1}"> <s:param name="value" value="%{vendorextracdate}" /></s:text>
						 <s:hidden cssClass="input-text" id="vendorExtractDate" name="vendorExtractDate" value="%{sysInvoiceVendorDate}"  />  
						<td><s:textfield cssClass="input-text" id="sysInvoiceVendorDate" name="sysInvoiceVendorDate" value="%{sysInvoiceVendorDate}" size="10" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"  onselect ="calcDays();"  /> 
						<img id="sysInvoiceVendorDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=" forDays1();"/></td>
					</c:if>
					<c:if test="${empty billToExtractDate}" >
						<s:hidden cssClass="input-text" id="vendorExtractDate" name="vendorExtractDate"/>
						<td><s:textfield cssClass="input-text" id="sysInvoiceVendorDate" name="sysInvoiceVendorDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="sysInvoiceVendorDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				<td align="right" class="listwhitetext"></td>
				<td align="left"></td>		 	
	  		<td width="200"></td><td></td>
	  		</tr> 
	  		 <tr>
          <td height="5px"></td>   
          </tr>
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return submitByVendorInvoice();"/></td>
	    	
	    	</tr>	 				
	
	</table>  
</s:form>
</td></tr>



</table>
</div>
<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>

<script type="text/javascript">
setOnSelectBasedMethods(["calcDays()"]);
	setCalendarFunctionality();
</script>
