package com.keysurvey.api.v68.form.result;

public class FormResultManagementServiceProxy implements com.keysurvey.api.v68.form.result.FormResultManagementService {
  private String _endpoint = null;
  private com.keysurvey.api.v68.form.result.FormResultManagementService formResultManagementService = null;
  
  public FormResultManagementServiceProxy() {
    _initFormResultManagementServiceProxy();
  }
  
  public FormResultManagementServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initFormResultManagementServiceProxy();
  }
  
  private void _initFormResultManagementServiceProxy() {
    try {
      formResultManagementService = (new com.keysurvey.api.v68.form.result.FormResultManagementLocator()).getFormResultManagementServicePort();
      if (formResultManagementService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)formResultManagementService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)formResultManagementService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (formResultManagementService != null)
      ((javax.xml.rpc.Stub)formResultManagementService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.keysurvey.api.v68.form.result.FormResultManagementService getFormResultManagementService() {
    if (formResultManagementService == null)
      _initFormResultManagementServiceProxy();
    return formResultManagementService;
  }
  
  public com.keysurvey.api.v68.form.result.WSQuestionResponse getResponse(long respondentId, long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formResultManagementService == null)
      _initFormResultManagementServiceProxy();
    return formResultManagementService.getResponse(respondentId, questionId);
  }
  
  public com.keysurvey.api.v68.form.result.WSRespondent[] getRespondents(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formResultManagementService == null)
      _initFormResultManagementServiceProxy();
    return formResultManagementService.getRespondents(formId);
  }
  
  public com.keysurvey.api.v68.form.result.WSRespondent getRespondent(long respondentId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formResultManagementService == null)
      _initFormResultManagementServiceProxy();
    return formResultManagementService.getRespondent(respondentId);
  }
  
  public com.keysurvey.api.v68.form.result.WSQuestionResponse[] getResponses(long respondentId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formResultManagementService == null)
      _initFormResultManagementServiceProxy();
    return formResultManagementService.getResponses(respondentId);
  }
  
  public com.keysurvey.api.v68.form.result.WSFormStatistics getFormStatistics(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formResultManagementService == null)
      _initFormResultManagementServiceProxy();
    return formResultManagementService.getFormStatistics(formId);
  }
  
  
}