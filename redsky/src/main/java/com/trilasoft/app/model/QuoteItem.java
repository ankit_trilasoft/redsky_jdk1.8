package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name="quoteitem")
public class QuoteItem extends BaseObject{
	private Long id;
	private String corpID;
	private Long quoteId;
	private Long quantity;
	private String name;
	private Double weight;
	private String description;
	private Long noOfItems;
	private Double length;
	private Double width;
	private Double height;
	private Double volume;
	private Double actualWeight;
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("quoteId", quoteId)
				.append("quantity", quantity).append("name", name)
				.append("weight", weight).append("description", description)
				.append("noOfItems", noOfItems).append("length", length)
				.append("width", width).append("height", height)
				.append("volume", volume).append("actualWeight", actualWeight)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof QuoteItem))
			return false;
		QuoteItem castOther = (QuoteItem) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(quoteId, castOther.quoteId)
				.append(quantity, castOther.quantity)
				.append(name, castOther.name).append(weight, castOther.weight)
				.append(description, castOther.description)
				.append(noOfItems, castOther.noOfItems)
				.append(length, castOther.length)
				.append(width, castOther.width)
				.append(height, castOther.height)
				.append(volume, castOther.volume)
				.append(actualWeight, castOther.actualWeight)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(quoteId)
				.append(quantity).append(name).append(weight)
				.append(description).append(noOfItems).append(length)
				.append(width).append(height).append(volume)
				.append(actualWeight).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = length;
	}
	@Column
	public Long getNoOfItems() {
		return noOfItems;
	}
	public void setNoOfItems(Long noOfItems) {
		this.noOfItems = noOfItems;
	}
	@Column
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Double getVolume() {
		return volume;
	}
	public void setVolume(Double volume) {
		this.volume = volume;
	}
	@Column
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	@Column
	public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column
	public Long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}
	@Column
	public Double getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(Double actualWeight) {
		this.actualWeight = actualWeight;
	}
	
}
