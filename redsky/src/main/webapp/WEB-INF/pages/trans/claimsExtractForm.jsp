<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="Claims Extract"/> 
	<title>Claims Extract</title>
	
<style type="text/css">
h2 {background-color: #FBBFFF}		
</style>

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
	
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;
font-weight: bold;
color: #15428B;
text-decoration: none;
background:url(images/collapsebg.gif) #BCD2EF;
padding:2px 3px 2px 5px;
 height:23px;
 border:1px solid #99BBE8; 
 border-right:none; border-left:none;
  border-top:none;
  margin-top:0px} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}
</style>

</head>
<body style="background-color:#444444;">
<s:form id="claimsExtractForm" name="claimsExtractForm" action="claimsExtractForm" method="post" validate="true"  cssClass="form_magn">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<configByCorp:fieldVisibility componentId="component.field.Payroll.lossaction.condition">
<s:hidden id="lossactionCondt" name="lossactionCondt" value="YES"/>
</configByCorp:fieldVisibility>
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
	<s:hidden name="salesPortalFlag" value="Yes" />
</sec-auth:authComponent>
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<s:hidden name="claimPropertyFlag" value="Yes" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.claimExtract.recievedFormCheck">
	<s:hidden name="recievedFormCheckFlag" value="Yes" />
</configByCorp:fieldVisibility>
<div id="Layer1" style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Claims Extract</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="0" cellpadding="0" border="0" style="width:800px"> 
 	<tbody>
		  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>
			<tr>
	  	   	    <td  class="listwhitetext" align="right" height="21px">Job Type</td>
	  	   	<c:if test="${salesPortalAccess=='false'}">
	  			<td style="padding-left: 5px" colspan="7" ><s:select cssClass="list-menu" cssStyle="width:100px" id="conditionA1" name="conditionA1" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>	  			
	  		</c:if>
	  		<c:if test="${salesPortalAccess=='true'}">
	  			<td style="padding-left: 5px" colspan="7" ><s:select cssClass="list-menu" cssStyle="width:100px" id="conditionA1" name="conditionA1" list="{'Equal to'}"  /></td>	  			
	  		</c:if>
	  	   </tr>
	  	   <tr>
		  		<td align="left" height="5px"></td>
		  		<td colspan="3"></td>
		  	</tr>
	  	  <tr><td class="listwhitetext"></td>
	  	  	  <td colspan="7" style="padding-left: 5px">
	  	  	  <table class="detailTabLabel" cellspacing="0" cellpadding="0">
	  	  	  <tr>
	  	  	  <td>
	  	  	  <s:select cssClass="list-menu" cssStyle="width:253px; height:50px; !height:70px;"  name="jobTypes" list="%{jobtype}"  multiple="true"/>
	  	  	  </td>
	  	  	  <td class="listwhitetext" align="left" style="font-size:10px; font-style: italic; padding-left:15px;" >
            <b>* Use Shift / Control + Click to select multiple Job Type</b>
             </td>
	  	  	  </tr>
	  	  	  </table>
	  	  	  </td>	  	  	  	  
	  	  </tr>
	  		<tr>
		  		<td align="left" height="5px"></td>
		  		<td colspan="3"></td>
		  	</tr>
		  	<tr>		  		
		  		<td align="right" width="120px" class="listwhitetext" style="padding-bottom:5px">Open Claim    From</td>	
		  		
		  		<td align="left" colspan="7" class="listwhitetext">
		  		
		  		<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	  	  	  <tr>
	  	  	  <td width="160px" style="padding-bottom:5px; padding-left:5px;"> 	 
		  		
		  		<s:textfield cssClass="input-text" id="openfromdate" name="openfromdate" onkeydown="return onlyDel(event,this)" value="%{openfromdate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="openfromdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
				<td align="right" class="listwhitetext" style="padding-bottom:5px">To</td>	
		  		
		  		<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:textfield cssClass="input-text" id="opentodate" onkeydown="return onlyDel(event,this)" name="opentodate" value="%{opentodate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="opentodate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
			 </tr>
	  	  	  </table>
	  	  	  </td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" style="padding-bottom:5px">Closed&nbsp;Claim&nbsp;From</td>
								
				<td align="left" colspan="7" class="listwhitetext">
				
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	  	  	  <tr>
	  	  	  <td width="160px" style="padding-bottom:5px; padding-left:5px;"> 	 
				
				<s:textfield cssClass="input-text" id="closeddatefrom" name="closeddatefrom" onkeydown="return onlyDel(event,this)" value="%{closeddatefrom}" size="8" maxlength="11" readonly="true"/>
				<img id="closeddatefrom_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  	
		  		<td align="right" class="listwhitetext" style="padding-bottom:5px">To</td>	
				<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:textfield cssClass="input-text" id="closeddateto" onkeydown="return onlyDel(event,this)" name="closeddateto" value="%{closeddateto}" size="8" maxlength="11" readonly="true" />
			    <img id="closeddateto_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		  	
		  	</tr>
		  	</table>
		  	</td>
		  	</tr>
		  		
		  		<tr>
				<td align="right" class="listwhitetext" style="padding-bottom:5px">Settlement&nbsp;Form</td>
								
				<td align="left" colspan="7" class="listwhitetext">
				
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	  	  	  <tr>
	  	  	  <td width="160px" style="padding-bottom:5px; padding-left:5px;"> 	 
				
				<s:textfield cssClass="input-text" id="settlementdatefrom" name="settlementdatefrom" value="%{settlementdatefrom}" onkeydown="return onlyDel(event,this)" size="8" maxlength="11" readonly="true"/>
				<img id="settlementdatefrom_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  	
		  		<td align="right" class="listwhitetext" style="padding-bottom:5px">To</td>	
				<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:textfield cssClass="input-text" id="settlementdateto" name="settlementdateto" onkeydown="return onlyDel(event,this)" value="%{settlementdateto}" size="8" maxlength="11" readonly="true" />
			    <img id="settlementdateto_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		  	
		  	</tr>
		  	</table>
		  	</td>
		  	</tr>	  	
		  	<tr>		  	  		
	  			<td width="50px"></td>			
				<td colspan="4"  style="padding-left:4px;">
				<s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " align="top" method="claimExtractForm" value="Extract" onclick="return valbutton(claimsExtractForm);" />	  		 				           	
				<s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" />
				</td>
        	</tr>
        	<tr>
		  		<td align="left" style="height:10px; !height:20px;"></td>
		  	</tr>
        	</tbody>
        	</table>  
	
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script language="javascript" type="text/javascript">
	function valbutton(thisform) {
	var date1 = document.forms['claimsExtractForm'].elements['openfromdate'].value;
	var date2 = document.forms['claimsExtractForm'].elements['opentodate'].value;
	var date3 = document.forms['claimsExtractForm'].elements['closeddatefrom'].value;
	var date4 = document.forms['claimsExtractForm'].elements['closeddateto'].value;
	var con = document.forms['claimsExtractForm'].elements['conditionA1'].value;
	var job = document.forms['claimsExtractForm'].elements['jobTypes'].value;
	var date5 = document.forms['claimsExtractForm'].elements['settlementdatefrom'].value;
	var date6 = document.forms['claimsExtractForm'].elements['settlementdateto'].value;
	
	if(date1=='' && date2=='' && date3=='' && date4=='' && con== '' && job=='')					
	{	
	 return true;	 
	}
	// for job type
	if(con!=''&& job=='')
	{ 	
		alert('Please select job type in');
		document.forms['claimsExtractForm'].elements['conditionA1'].focus();
		return false;
	}
	
	if(con==''&& job!='')
	{ 	
		alert('Please select job type');		
		document.forms['claimsExtractForm'].elements['jobTypes'].focus();
		return false;
	}
	// for open date
	if(date1!=''&& date2=='')
	{ 	
		alert('Please enter open claim to date');
		document.forms['claimsExtractForm'].elements['opentodate'].focus();
		return false;
	}
	if(date1==''&& date2!='')
	{ 	
		alert('Please enter open claim from date');
		document.forms['claimsExtractForm'].elements['openfromdate'].focus();
		return false; 
	}
	// for close date
	if(date3!=''&& date4=='')
	{ 	
		alert('Please enter closed claim to date');		
		document.forms['claimsExtractForm'].elements['closeddateto'].focus();
		return false;
	}
	if(date3==''&& date4!='')
	{ 	
		alert('Please enter closed claim from date	');
		document.forms['claimsExtractForm'].elements['closeddatefrom'].focus();
		return false;
	}
	// for settelment date
	if(date5!=''&& date6=='')
	{ 	
		alert('Please enter Settlement to date');		
		document.forms['claimsExtractForm'].elements['settlementdateto'].focus();
		return false;
	}
	if(date5==''&& date6!='')
	{ 	
		alert('Please enter Settlement from date	');
		document.forms['claimsExtractForm'].elements['settlementdatefrom'].focus();
		return false;
	}
}
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>