package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.PartnerRates;
import java.util.List;
public interface PartnerRatesDao extends GenericDao<PartnerRates, Long>{
	public List findMaximum();
}
