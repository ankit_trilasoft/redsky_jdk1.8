<%@ include file="/common/taglibs.jsp"%>
<title>Invoive Details Form</title>
<meta name="heading" content="Invoive Details Form" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
 
</script>


<s:form id="accountLineCurrencyRecForm" name="accountLineCurrencyRecForm" action="" onsubmit="" method="post">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="aid" value="${accountLine.id}" />  
<s:hidden name="formStatus"  />
<s:hidden name="accountLine.vendorCode"/>
<s:hidden name="accountLine.category" />
<s:hidden name="changeSt" value=""/>
<div id="layer1" style="width:100%">
  
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Invoice&nbsp;Details</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    
<table border="0" style="margin:0px;padding:0px;">
<tr>
<td width="105" align="right" class="listwhitetext">Invoice#</td>
<td align="left" class="listwhitetext" colspan="2">
<input type="text" id="accountLine.invoiceNumber"  name="accountLine.invoiceNumber" onchange="checkVendorInvoice('INV');assignInvoiceDate();" onkeydown="" class="input-text" value="${accountLine.invoiceNumber}" maxlength="25" size="16" >
</td>
<td class="listwhitetext" align="right" width="77">Dated</td>
	    <c:if test="${not empty accountLine.invoiceDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.invoiceDate"/></s:text>
			 <td><s:textfield id="invoiceDate" cssClass="input-text" name="accountLine.invoiceDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="invoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty accountLine.invoiceDate}">
		<td><s:textfield id="invoiceDate" cssClass="input-text" name="accountLine.invoiceDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="invoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
 <tr><td width="1px"></td></tr>
<tr>
<td class="listwhitetext" align="right">Value Date</td>
	    <c:if test="${not empty accountLine.valueDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.valueDate"/></s:text>
			 <td><s:textfield id="valueDate" cssClass="input-text" name="accountLine.valueDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:73px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="valueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty accountLine.valueDate}">
		<td><s:textfield id="valueDate" cssClass="input-text" name="accountLine.valueDate" cssStyle="width:73px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="valueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<td class="listwhitetext" align="right">Received On</td>
	    <c:if test="${not empty accountLine.receivedDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.receivedDate"/></s:text>
			 <td><s:textfield id="receivedDate" cssClass="input-text" name="accountLine.receivedDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="receivedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty accountLine.receivedDate}">
		<td><s:textfield id="receivedDate" cssClass="input-text" name="accountLine.receivedDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="receivedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<td class="listwhitetext" width="43" align="right">Status</td>
<td align="left"><s:select  cssClass="list-menu" name="accountLine.payingStatus" cssStyle="width:125px" list="%{payingStatus}" headerKey="" headerValue="" tabindex="43" onchange="checkVendorInvoice('PAY');changeStatus();"/></td>
</tr>					
</table>
<table style="margin:0px 0px 20px 0px; padding:0px;">

</table>

</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="checkVendorOnSave()"/></td>
<td><input type="button"  value="Cancel" name="Cancel" class="cssbuttonA" style="width:55px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</s:form>
<script type="text/javascript">
</script>
<script type="text/javascript">
<c:if test="${(((!discountUserFlag.pricingActual && !discountUserFlag.pricingRevision) &&  (accountLine.revisionExpense!='0.00' || accountLine.revisionRevenueAmount!='0.00' || accountLine.actualRevenue!='0.00' || accountLine.actualExpense!='0.00' ||accountLine.distributionAmount!='0.00' || accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || ((discountUserFlag.pricingActual || discountUserFlag.pricingRevision) &&  (accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLine.createdBy == 'Networking') || not empty accountLine.payAccDate )}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${ serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">
var http1981 = getHTTPObject();
var http1982 = getHTTPObject();
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}

function changeStatus() {
	   document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value = '2'; 
	}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}	
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
}
function findExchangeRateGlobal(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function updateExchangeRate(currency,field){
	document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value);
}
function currentDateGlobal(){
	 var mydate=new Date();
    var daym;
    var year=mydate.getFullYear()
    var y=""+year;
    if (year < 1000)
    year+=1900
    var day=mydate.getDay()
    var month=mydate.getMonth()+1
    if(month == 1)month="Jan";
    if(month == 2)month="Feb";
    if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function assignInvoiceDate() {
 var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec"; 
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	var invoiceNumber=document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value;
 invoiceNumber=invoiceNumber.trim();
 document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value=invoiceNumber; 
	if(document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value!='') {
	  document.forms['accountLineCurrencyRecForm'].elements['accountLine.receivedDate'].value=datam;
  } else {
  document.forms['accountLineCurrencyRecForm'].elements['accountLine.receivedDate'].value='';
 } }
function onlyDelForInvoiceDate(evt,targetElement){
	if(document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value!='A'){
		var keyCode = evt.which ? evt.which : evt.keyCode;
		  if(keyCode==46){
		  		targetElement.value = '';
		  }else{
		  if(keyCode==8){
		  	targetElement.value = '';
		  }
		  	return false;
		  }
	}
	else{
	  	return false;
	}
}
function checkVendorInvoice(name) { 
	   var payingStatus=document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value; 
	   var invoiceNumber=document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value;
	   var vendorCode=document.forms['accountLineCurrencyRecForm'].elements['accountLine.vendorCode'].value;	   
	   document.forms['accountLineCurrencyRecForm'].elements['changeSt'].value='T';	   
	   var invoiceDate='';
	   try{
	   invoiceDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceDate'].value;
	   }catch(e){}
	   invoiceDate=invoiceDate.trim();
	   if(invoiceDate!=''){
		   invoiceDate=convertItProperDateFormate(invoiceDate);
	   }
	   invoiceNumber=invoiceNumber.trim();
	   if(payingStatus=='A' || payingStatus=='C') {
		     if(invoiceNumber==''){
	         alert("Please fill invoice# in payable detail.");
	         document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value ='';
		     }else if(invoiceDate=='') {
	  	         alert("Please fill Invoice Date in the Payable Details section.");
	  	         document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value ='';
		      }else{
		      }
	   }
}


function convertItProperDateFormate(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  	   if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}
function checkVendorOnSave(){
	var aa = document.forms['accountLineCurrencyRecForm'].elements['changeSt'].value;	
	   var invoiceNumber=document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value;
	   var vendorCode=document.forms['accountLineCurrencyRecForm'].elements['accountLine.vendorCode'].value;
	   var payingStatus=document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value;

	 if(aa=='T' && invoiceNumber!=''){
			<c:if test="${accCorpID=='SSCW'}">
			  
				<c:if test="${not empty accountLine.id}">
					    var idss='${accountLine.id}';
				        var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss="+idss;
					    http1982.open("GET", url, true);
					    http1982.onreadystatechange = handleHttpResponseVendorInvoiceEnhancement1;
					    http1982.send(null); 
				</c:if>
				<c:if test="${empty accountLine.id}">
				       var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss=null";
				       http1982.open("GET", url, true);
				       http1982.onreadystatechange = handleHttpResponseVendorInvoiceEnhancement1;
				       http1982.send(null); 
				</c:if>	
		   </c:if>
		   <c:if test="${accCorpID!='SSCW'}">
		   save();	    
		   </c:if>
	   }else{
		   save(); 
	   }
} 
function handleHttpResponseVendorInvoiceEnhancement1(){
	 if (http1982.readyState == 4){
	     var results = http1982.responseText
	     results = results.trim();
	     if(results!=''){
	     	var catVal = document.forms['accountLineCurrencyRecForm'].elements['accountLine.category'].value;
	     	if(catVal=='Internal Cost'){
	     		save();	
	     	}else{
		                alert(results);
		                /* if(agree) {
		                	save();	
		                } else{ */
		                	document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value="${accountLine.invoiceNumber}";
		                	document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value="${accountLine.payingStatus}";
		                 } 
	     	
	     	     
	     }else{save();	} }	 }
function save(){
	var aid=document.forms['accountLineCurrencyRecForm'].elements['aid'].value;
	var payingStatus=document.forms['accountLineCurrencyRecForm'].elements['accountLine.payingStatus'].value;
	var receivedDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.receivedDate'].value;
	receivedDate=convertItProperDateFormate(receivedDate);
	var invoiceNumber=document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceNumber'].value;
	var invoiceDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.invoiceDate'].value;
	invoiceDate=convertItProperDateFormate(invoiceDate);
	var valueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.valueDate'].value;
	valueDate=convertItProperDateFormate(valueDate);
	 window.opener.setFieldValue('payingStatus'+aid,payingStatus);
	 window.opener.setFieldValue('receivedDate'+aid,receivedDate);
	 window.opener.setFieldValue('invoiceNumber'+aid,invoiceNumber);
	 window.opener.setFieldValue('invoiceDate'+aid,invoiceDate);
	 window.opener.setFieldValue('valueDate'+aid,valueDate);
	 window.opener.calculateActualRevenue(aid,'','FX');
     window.close();
}

</script>
