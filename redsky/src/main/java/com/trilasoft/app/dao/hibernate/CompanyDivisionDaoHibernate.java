package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.CompanyDivisionDao;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.RefMaster;
public class CompanyDivisionDaoHibernate extends GenericDaoHibernate<CompanyDivision, Long> implements CompanyDivisionDao{
	
	public CompanyDivisionDaoHibernate() { 
		super(CompanyDivision.class);
	} 

	public List getInvCompanyCode(String corpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where corpID=?", corpID);
	}
	public List getForUGCNInvCompanyCode(String corpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where corpID=?", corpID);
	}
	public List getPayCompanyCode(String corpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where corpID=?", corpID);	
	}
	public List getPayCompanyCodeForUGCN(String corpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where corpID=?", corpID);	
	}

	public List findCompanyCodeList(String corpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where corpID=?", corpID);	
	}

	public List findCompanyDivFlag(String corpID) {
		return getHibernateTemplate().find("select companyDivisionFlag from Company where companyDivisionFlag is not null and corpID=?", corpID);	
	}
	public Map<String, String> findDefaultStateList(String bucket2, String corpId){
		if(bucket2 == null || bucket2.equals("")){
			bucket2 = "NoCountry";
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		List<RefMaster> list = getHibernateTemplate().find("from RefMaster where  bucket2 like '"+bucket2+"%' order by description");
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
			}
		return parameterMap;
	}

	public List checkBookingAgentCode(String bookingAgentCode, String corpID) {
		if(bookingAgentCode!=null){
			bookingAgentCode = bookingAgentCode.replaceAll("'", "''");
		}
		return getHibernateTemplate().find("select bookingAgentCode from CompanyDivision where bookingAgentCode='"+bookingAgentCode+"' and corpID=?", corpID);	
	}

	public List checkCompayCode(String compayCode, String corpID) {
		if(compayCode!=null){
			compayCode = compayCode.replaceAll("'", "''");
		}
		return getHibernateTemplate().find("select companyCode from CompanyDivision where companyCode='"+compayCode+"' and corpID=?", corpID);	
	}
	public String searchBaseCurrencyCompanyDivision(String compayCode ,String corpID){
		List baseCurrency=this.getSession().createSQLQuery("select baseCurrency from companydivision where companyCode='"+compayCode+"' and corpID='"+corpID+"'").list();
		if(!baseCurrency.isEmpty() && baseCurrency.get(0)!=null)
		{
		return baseCurrency.get(0).toString();
		}
		else
		{
			return "";
		}
	}

	public List getBookingAgentCode(String companyDivision, String sessionCorpID) {
		return this.getSession().createSQLQuery("select bookingAgentCode from companydivision where companyCode='"+companyDivision+"' and corpID='"+sessionCorpID+"'" ).list();	
	}
	public List findCompanyCodeByBookingAgent(String bookingAgentCode,String corpID) {
		String query="select group_concat(companyCode) from companydivision where companyCode is not null and companyCode!='' and (bookingAgentCode='"+bookingAgentCode+"' OR childAgentCode like '%"+bookingAgentCode+"%') and corpID='"+corpID+"'";
		return this.getSession().createSQLQuery(query).list();
	}

	public Map<String, String> getUTSICompanyDivision(String sessionCorpID) {
		List<CompanyDivision> list;
		list = getHibernateTemplate().find("from CompanyDivision where corpID='"+sessionCorpID+"'"); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
			for (CompanyDivision companyDivision : list) {
				parameterMap.put(companyDivision.getBookingAgentCode(), companyDivision.getCompanyCode());
			} 
		return parameterMap;
	}

	public Map<String, String> getCompanyDivisionCurrencyMap(String sessionCorpID) {
		List<CompanyDivision> list;
		list = getHibernateTemplate().find("from CompanyDivision where corpID='"+sessionCorpID+"'"); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
			for (CompanyDivision companyDivision : list) {
				parameterMap.put(companyDivision.getCompanyCode().trim(), companyDivision.getBaseCurrency().trim());
			} 
		return parameterMap;
	}

	public Map<String, String> getAgentCompanyDivisionCurrencyMap() {
		List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
		Iterator it=corpidList.iterator();
		String corpID="";
		while(it.hasNext()){
			if(corpID.equals("")){
				corpID=it.next().toString();	
			}else{
				corpID=corpID+"','"+it.next().toString();	
			}
		}
		List<CompanyDivision> list;
		list = this.getSession().createSQLQuery("select companyCode,baseCurrency , corpID from companydivision where corpID in ('"+corpID+"')")
				.addScalar("companyCode", Hibernate.STRING)
				.addScalar("baseCurrency", Hibernate.STRING)
				.addScalar("corpID", Hibernate.STRING).list(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
		Iterator iterator =list.iterator(); 
		while (iterator.hasNext()) {
			String companyCode = "";
			String baseCurrency = "";
			String corpIDs = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				companyCode = row[0].toString().trim();
			}
			if (row[1] != null) {
				baseCurrency = row[1].toString().trim();
			}
			if(row[2] != null && (!(row[2].toString().trim().equals("")))){
				corpIDs = row[2].toString().trim();
			}
			 
			 
			parameterMap.put(corpIDs.trim()+"~"+companyCode.trim(), baseCurrency.trim());
			 
		}	
		
		return parameterMap;
	}

	public Map<String, String> getAgentBookingAgentCompanyDivisionCurrencyMap() {
		List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
		Iterator it=corpidList.iterator();
		String corpID="";
		while(it.hasNext()){
			if(corpID.equals("")){
				corpID=it.next().toString();	
			}else{
				corpID=corpID+"','"+it.next().toString();	
			}
		}
		List<CompanyDivision> list;
		list = this.getSession().createSQLQuery("select companyCode,bookingAgentCode , corpID from companydivision where corpID in ('"+corpID+"')")
				.addScalar("companyCode", Hibernate.STRING)
				.addScalar("bookingAgentCode", Hibernate.STRING)
				.addScalar("corpID", Hibernate.STRING).list(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
		Iterator iterator =list.iterator(); 
		while (iterator.hasNext()) {
			String companyCode = "";
			String bookingAgentCode = "";
			String corpIDs = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				companyCode = row[0].toString().trim();
			}
			if (row[1] != null) {
				bookingAgentCode = row[1].toString().trim();
			}
			if(row[2] != null && (!(row[2].toString().trim().equals("")))){
				corpIDs = row[2].toString().trim();
			}
			 
			 
			parameterMap.put(corpIDs.trim()+"~"+companyCode.trim(), bookingAgentCode.trim());
			 
		}	
		
		return parameterMap;
	}
	public int findGlobalCompanyCode(String corpID) {
		int size=0;
		List al = new ArrayList();
		al= this.getSession().createSQLQuery("select companyCode from companydivision where corpId='"+corpID+"' and companyCode is not null and companyCode!=''").list();
		if(al!=null && !al.isEmpty() && al.get(0)!=null && !al.get(0).toString().trim().equalsIgnoreCase("")){
			size=al.size();
		}
		return size;
	}
	public Map<String,String> getComDivCodePerCorpIdList() {
		Map<String,String> mp=new HashMap<String, String>();
		List al = new ArrayList();
		String str="";
		al= this.getSession().createSQLQuery("select bookingAgentCode,corpId from companydivision where bookingAgentCode is not null and bookingAgentCode!=''").list();
		if(al!=null && !al.isEmpty() && al.get(0)!=null){
			Iterator iterator =al.iterator(); 
			while (iterator.hasNext()) {
				Object[] row = (Object[]) iterator.next();
				if(mp!=null && !mp.isEmpty() && mp.containsKey(row[1]+"".trim())){
					str=mp.get(row[1]+"".trim());
					str=str+","+row[0]+"".trim();
					mp.put(row[1]+"".trim(), str);
				}else{
					mp.put(row[1]+"".trim(), row[0]+"".trim());
				}
			}
		}
		return mp;
	}

	public List findCompanyCodeListForVanLine(String sessionCorpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where closeddivision is false and  corpID=?", sessionCorpID);	
	}

	public List findCMMDMMAgentList() {
		List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
		Iterator it=corpidList.iterator();
		String corpID="";
		while(it.hasNext()){
			if(corpID.equals("")){
				corpID=it.next().toString();	
			}else{
				corpID=corpID+"','"+it.next().toString();	
			}
		}
		List CMMDMMAgentList = new ArrayList();
		CMMDMMAgentList = this.getSession().createSQLQuery("select distinct bookingAgentCode from companydivision where corpID in ('"+corpID+"') and bookingAgentCode is not null and bookingAgentCode !='' ").list(); 
		return CMMDMMAgentList;
	}
}
