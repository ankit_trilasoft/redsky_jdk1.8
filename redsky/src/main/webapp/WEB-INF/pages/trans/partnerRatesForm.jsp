<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerRatesDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerRatesDetail.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script language="JavaScript">
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
	}
</script>
<script>
function checkdate(clickType){
	
	if(!(clickType == 'save')){
	var id = document.forms['partnerRatesForm'].elements['partnerRates.id'].value;
	var id1 = document.forms['partnerRatesForm'].elements['partner.id'].value;
	
	if (document.forms['partnerRatesForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Do you want to save the partner rate and continue? press OK to continue with save OR press CANCEL");
		if(agree){
			document.forms['partnerRatesForm'].action = 'savePartnerRates!saveOnTabChange.html';
			document.forms['partnerRatesForm'].submit();
		}else{
			if(id != ''){
				if(document.forms['partnerRatesForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
					location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType=AG';
				}
				if(document.forms['partnerRatesForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'searchPartnerAdmin.html?partnerType=AG';
					}
				}
			}
		}
	}else{
	if(id != ''){
			if(id != ''){
			if(document.forms['partnerRatesForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType=AG';
				}
			if(document.forms['partnerRatesForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'searchPartnerAdmin.html?partnerType=AG';
					}
				}
	}
	}
}
}
}
function changeStatus(){
	document.forms['partnerRatesForm'].elements['formStatus'].value = '1';
}
</script>
</head> 
<style>
.mainDetailTable {
width:795px;
}
</style>
<s:form id="partnerRatesForm" action="savePartnerRates.html" method="post" validate="true">   
<s:hidden name="partner.id" />
<s:hidden name="partnerRates.corpID" />
<s:hidden name="partner.partnerCode" />
<s:hidden name="partnerRates.id" />
<s:hidden name="partnerRates.partnerCode" />
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px"  
        onclick="location.href='<c:url value="/editPartnerRatesForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.partnerlist' }">
	<c:redirect url="/searchPartnerAdmin.html?partnerType=AG"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerAddForm.html?id=${partner.id}&partnerType=AG"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<div id="newmnav">
				  <ul>
				  	<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
					    <!--<li><a href="editPartnerAddForm.html?id=${partner.id}&partnerType=AG" ><span>Partner Detail</span></a></li>
				  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></span></a></li>
						<li><a href="searchPartner.html?partnerType=AG"><span>Partner List</span></a></li>
						--><li><a onclick="setReturnString('gototab.accountdetail');return checkdate('none');"><span>Partner Detail</span></a></li>
						<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></span></a></li>
						--><li><a onclick="setReturnString('gototab.partnerlist');return checkdate('none');"><span>Partner List</span></a></li>
				  	</sec-auth:authComponent>
				  	<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
				  		<li><a onclick="setReturnString('gototab.accountdetail');return checkdate('none');"><span>Agent Detail</span></a></li>
				  		<c:if test="${partner.partnerPortalActive == true}">
				  			<li><a href="partnerUsersList.html?id=${partner.id}"><span>Users & contacts</span></a></li>
						</c:if>
				  		<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></span></a></li>
				  		--><li><a href="partnerAgent.html"><span>Agent List</span></a></li>
				  	</sec-auth:authComponent>
				  </ul>
		</div><div class="spn">&nbsp;</div>
		
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
				<table >
					<tbody>
						<tr>
							<td align="left" class="listwhite" width="55px">Rates For</td>
							<td><s:textfield cssClass="input-textUpper" name="partner.firstName" required="true" size="10" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper" name="partner.lastName" required="true" size="36" readonly="true"/></td>
							<td align="right" class="listwhite"><fmt:message key='partnerRates.type'/></td>
							<td align="right" class="listwhite"><s:select name="partnerRates.type" list="%{ratetype}" cssStyle="width:125px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="1"/></td>
							<td align="right" class="listwhite"><fmt:message key='partnerRates.effective'/></td>
							<c:if test="${not empty partnerRates.effective}">
							<s:text id="partnerRatesEffectiveFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerRates.effective"/></s:text>
							<td><s:textfield id="effective" name="partnerRates.effective" value="%{partnerRatesEffectiveFormattedValue}" cssStyle="width:65px"maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td align="left"><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRatesForm'].effective,'calender',document.forms['partnerRatesForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty partnerRates.effective}">
							<td align="right" class="listwhite"><s:textfield id="effective" name="partnerRates.effective" cssStyle="width:65px" maxlength="10" onkeydown="return onlyDel(event,this)"/></td><td align="left"><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRatesForm'].effective,'calender',document.forms['partnerRatesForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="right" class="listwhite"><fmt:message key='partnerRates.endDate'/></td>
							<c:if test="${not empty partnerRates.endDate}">
							<s:text id="partnerRatesendDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerRates.endDate"/></s:text>
							<td><s:textfield id="endDate" name="partnerRates.endDate" value="%{partnerRatesendDateFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td align="left"><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRatesForm'].endDate,'calender2',document.forms['partnerRatesForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty partnerRates.endDate}">
							<td align="right" class="listwhite"><s:textfield id="endDate" name="partnerRates.endDate" cssStyle="width:65px" maxlength="10" onkeydown="return onlyDel(event,this)"/></td><td align="left"><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRatesForm'].endDate,'calender2',document.forms['partnerRatesForm'].dateFormat.value); return false;"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<div id="newmnav">
		  <ul>
		  	<li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    
  </ul>
		</div><div class="spn">&nbsp;</div>
		
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td>
 
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Ocean Freight
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	
<table>
	<tbody>
		
		<tr>
			<td align="center" class="listwhite" width="50px"></td>
			<td align="center" class="listwhite"></td>
			<td align="center" class="listwhite">Loose</td>
			<td align="center" class="listwhite">Liftvan</td>
			<td align="center" class="listwhite"></td>
			<td align="center" class="listwhite">Loose</td>
			<td align="center" class="listwhite">Liftvan</td>
			<td align="center" class="listwhite"></td>
			<td align="center" class="listwhite">Loose</td>
			<td align="center" class="listwhite">Liftvan</td>
		</tr>
			<td width="50px"></td>
			<td align="right" class="listwhite" width="90px"><fmt:message key='partnerRates.l1To999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l1To999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l1To999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="2"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v1To999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v1To999}" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="3"/></td>
			<td align="right" class="listwhite"width="150px"><fmt:message key='partnerRates.l7000To7999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l7000To7999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l7000To7999}"  groupingUsed="false" minFractionDigits="2" maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="4"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v7000To7999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v7000To7999}"  groupingUsed="false" minFractionDigits="2" maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="5"/></td>
			<td align="right" class="listwhite"width="150px"><fmt:message key='partnerRates.l14000To14999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l14000To14999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l14000To14999}"  groupingUsed="false" minFractionDigits="2" maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="6"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v14000To14999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v14000To14999}"  groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="7"/></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l1000To1999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l1000To1999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l1000To1999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="8"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v1000To1999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v1000To1999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="9"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l8000To8999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l8000To8999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l8000To8999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="10"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v8000To8999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v8000To8999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event) " tabindex="11"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l15000To15999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l15000To15999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l15000To15999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="12"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v15000To15999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v15000To15999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="13"/></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l2000To2999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l2000To2999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l2000To2999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="14"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v2000To2999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v2000To2999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="15"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l9000To9999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l9000To9999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l9000To9999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="16"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v9000To9999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v9000To9999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="17"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l16000To16999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l16000To16999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l16000To16999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="18"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v16000To16999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v16000To16999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="19"/></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l3000To3999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l3000To3999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l3000To3999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="20"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v3000To3999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v3000To3999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="21"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l10000To10999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l10000To10999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l10000To10999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="22"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v10000To10999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v10000To10999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="23"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l17000To17999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l17000To17999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l17000To17999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="24"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v17000To17999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v17000To17999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="25"/></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l4000To4999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l4000To4999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l4000To4999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="26"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v4000To4999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v4000To4999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="27"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l11000To11999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l11000To11999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l11000To11999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="28"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v11000To11999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v11000To11999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="29"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l18000To18999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l18000To18999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l18000To18999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="30"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v18000To18999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v18000To18999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="30"/></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l5000To5999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l5000To5999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l5000To5999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="31"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v5000To5999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v5000To5999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="32"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l12000To12999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l12000To12999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l12000To12999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="33"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v12000To12999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v12000To12999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="34"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l19000To19999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l19000To19999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l19000To19999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="35"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v19000To19999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v19000To19999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="36"/></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l6000To6999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l6000To6999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l6000To6999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="37"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v6000To6999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v6000To6999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="38"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l13000To13999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l13000To13999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l13000To13999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="39"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v13000To13999" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v13000To13999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)"tabindex="40" /></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.l20000'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.l20000" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.l20000}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="41"/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.v20000" size="6" maxlength="8" value='<fmt:formatNumber value="${partnerRates.v20000}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="42"/></td>
		</tr>
	</tbody>
</table>

<table width="100%">
	<tbody>
		<tr>
			<td align="left" class="listwhite" colspan="8"><table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Minimum Ocean Weights
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table></td>
		</tr>
		<tr>
			<td width="50px"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.c20'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.c20" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.c20}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="43"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.c40'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.c40" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.c40}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="44"/></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lclToVan'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lclToVan" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lclToVan}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="45"/></td>
		</tr>
	</tbody>
	</table>

<table width="60%">
	<tbody>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite" width="150px"><b>LCL rates</b></td>
			
			<td align="center" class="listwhite">per 100#</td>
				<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite" width="150px"><b>AIR rates</b></td>
			
			<td align="center" class="listwhite">per 100#</td>
		</tr>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl499'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl499" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl499}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="46"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air499'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air499" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air499}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="47"/></td>
		</tr>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl500To999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl500To999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl500To999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="48"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air500To999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air500To999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air500To999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="49"/></td>
		</tr>

			<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl1000To1499'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl1000To1499" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl1000To1499}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="50"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air1000To1499'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air1000To1499" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air1000To1499}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="51"/></td>
		</tr>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl1500To1999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl1500To1999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl1500To1999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="52"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air1500To1999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air1500To1999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air1500To1999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="53"/></td>
		</tr>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl2000To2999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl2000To2999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl2000To2999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="54"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air2000To2999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air2000To2999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air2000To2999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="55"/></td>
		</tr>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl3000To3999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl3000To3999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl3000To3999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="56"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air3000To3999'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air3000To3999" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air3000To3999}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="57"/></td>
		</tr>
		<tr>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.lcl4000'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.lcl4000" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.lcl4000}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="58"/></td>
			<td align="center" class="listwhite"></td>
			<td align="right" class="listwhite"><fmt:message key='partnerRates.air4000'/></td>
			<td align="right" class="listwhite"><input type="text" name="partnerRates.air4000" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.air4000}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>' onkeydown="return onlyFloatNumsAllowed(event)" tabindex="59"/></td>
		</tr>
	</tbody>
	</table>

<table width="100%">
	<tbody>
		<tr>
			<td align="left" class="listwhite" colspan="6"> 
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Automobiles
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</td>
		</tr>
		<tr>
		<td>
		<table width="55%">
		<tr>
		
			<td align="center" class="listwhite" width="150px"></td>
			<td align="center" class="listwhite" width="200px">Pickup At Residence</td>
			<td align="center" class="listwhite" width="200px">Customer To/From Warehouse</td>
		</tr>
		<tr>
			<td align="right" class="listwhite">LCL or RORO</td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.aROROToRsd" size="10" maxlength="8"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="60"/></td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.aROROToCustomer" size="10" maxlength="8" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="61"/></td>
		</tr>
		<tr>
			<td align="right" class="listwhite">In Container</td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.aContainerToRsd" size="10" maxlength="8"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="62"/></td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.aContainerToCustomer" size="10" maxlength="8" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="63"/></td>
		</tr>
		<tr>
			<td align="right" class="listwhite">Container w/ HHG</td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.aContainerHHGToRS" size="10" maxlength="8" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="64"/></td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.aContainerHHGToCU" size="10" maxlength="8" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="65"/></td>
		</tr>
		

	</table>
	</td>
	</tr>
	</tbody>
	</table>

<table width="100%">
	<tbody>
		<tr>
			<td align="left" class="listwhite" colspan="6">
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Additionals
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</td>
		</tr>
	
		<tr>
		<td>
		<table width="55%">
		<tr>
			<td align="center" class="listwhite" width="150px"></td>
			<td align="center" class="listwhite" width="200px">Rate</td>
			<td align="center" class="listwhite" width="200px">Unit Of Measure</td>
		</tr>
		<tr>
			<td align="right" class="listwhite">Warehouse Handling</td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.warehouseRate" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.warehouseRate}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="66"/></td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.warehouseUnit" size="10" maxlength="8" tabindex="67"/></td>
		</tr>
		<tr>
			<td align="right" class="listwhite">Storage Per Month</td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.storageRate" size="10" maxlength="8" value='<fmt:formatNumber value="${partnerRates.storageRate}" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="2" type="number"/>'onkeydown="return onlyFloatNumsAllowed(event)" tabindex="68"/></td>
			<td align="center" class="listwhite"><input type="text" name="partnerRates.storageUnit" size="10" maxlength="8" tabindex="69" /></td>
		</tr>

	</table>
</td>
</tr>



	</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
  
  <table>
<tbody>
					<tr>
						<td align="center" class="listwhite" ></td>
						<td align="right" class="listwhite" style="width:80px"><fmt:message key='partnerRates.createdOn'/></td>
						<s:hidden name="partnerRates.createdOn" />
						<td style="width:120px"><fmt:formatDate value="${partnerRates.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhite" style="width:80px"><fmt:message key='partnerRates.createdBy' /></td>
						<c:if test="${not empty partnerRates.id}">
								<s:hidden name="partnerRates.createdBy"/>
								<td width="85px"><s:label name="createdBy" value="%{partnerRates.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partnerRates.id}">
								<s:hidden name="partnerRates.createdBy" value="${pageContext.request.remoteUser}"/>
								<td width="100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhite" style="width:80px"><fmt:message key='partnerRates.updatedOn'/></td>
						<s:hidden name="partnerRates.updatedOn"/>
						<td style="width:120px"><fmt:formatDate value="${partnerRates.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhite" style="width:80px"><fmt:message key='partnerRates.updatedBy' /></td>
						<s:hidden name="partnerRates.updatedBy" value="${pageContext.request.remoteUser}"  />
						<td width="85px"><s:label name="partnerRates.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
</tbody>
</table>               
        <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:70px; height:25px"/>   
  
</s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("partnerRatesForm"));   
</script>