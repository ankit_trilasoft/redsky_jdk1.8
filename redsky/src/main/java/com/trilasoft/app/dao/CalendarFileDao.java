package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CalendarFile;

public interface CalendarFileDao  extends GenericDao<CalendarFile, Long> { 
	public List findByUserName(String corpID,String user);
	public List getUserFormSysDefault(String usrName);
	public List searchMyEvents(String sessionCorpID,String consult,String surveyFrom,String surveyTo);
	public List getDriverListDataSummary(String sessionCorpID);
	public String driverDetailCode(String myDriverCode);
}
