package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.StorageBillingMonitor;

public interface StorageBillingMonitorManager extends GenericManager<StorageBillingMonitor, Long>{
	public List checkBillingFlag(String corpId);
	public List getStorageBillingMonitorList(String corpId);
}
