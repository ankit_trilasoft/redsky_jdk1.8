package com.trilasoft.app.webapp.action;

/**
 * @Class Name	AccountLineUtil.java
 * @Author      Ashis Kumar Mohanty
 * @Version     V01.0
 * @Date        23-Jan-2013
 */

/**
 * This class is intentionally created to reduce the complexity of AccountLine and code reusability.
 */

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtilsBean;

import com.trilasoft.app.dao.hibernate.dto.BillingDTO;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.ServiceOrderManager;


/**
 * @author  <a>Ashis Mohanty</a>
 */
public class AccountLineUtil {
	
	/** isVATRequired flag is used for VATExclude.
	 *  isVATRequired is true  set VATExclude true otherwise false
	 * */
	public static AccountLine getChargeVATExcludeAndGLCode(AccountLineManager accountLineManager,String chargeCode,String contract,String corpId,AccountLine accountLineNew,boolean isVATRequired){
		List  agentGLList=accountLineManager.getGLList(chargeCode,contract,corpId);
		if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
			String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
			String recGl=GLarrayData[0];
			String payGl=GLarrayData[1];
			if(!(recGl.equalsIgnoreCase("NO"))){
				accountLineNew.setRecGl(recGl);
			}
			if(!(payGl.equalsIgnoreCase("NO"))){
				accountLineNew.setPayGl(payGl);
			}
			if(isVATRequired){
				String VATExclude=GLarrayData[2];
				if(VATExclude.equalsIgnoreCase("Y")){
					accountLineNew.setVATExclude(true);
				}else{
					accountLineNew.setVATExclude(false);
				}
			}
		}
		return accountLineNew;
	}
	/**
	 * This Method is used to get Charge GL Code.
	 */
	public static AccountLine getChargeGLCode(AccountLineManager accountLineManager,ServiceOrder serviceOrder,String chargeCode,String contract,String corpId,AccountLine accountLine){
		String chargeStr="";
		List glList = accountLineManager.findChargeDetailFromSO(contract,chargeCode,corpId,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
		if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
			  chargeStr= glList.get(0).toString();
		  }
		   if(!chargeStr.equalsIgnoreCase("")){
			  String [] chrageDetailArr = chargeStr.split("#");
			  accountLine.setRecGl(chrageDetailArr[1]);
			  accountLine.setPayGl(chrageDetailArr[2]);
			}else{
			  accountLine.setRecGl("");
			  accountLine.setPayGl("");
		  }
		return accountLine;
	}
	
	/**
	 * This Method is use to get the Vat Amount of Payable/Receivable.
	 */
	public static BigDecimal getVatAmount(Object obj1,Object obj2){
		if(obj1 != null && !"".equals(obj1.toString()) &&
				obj2 != null && !"".equals(obj2.toString())){
			
			return (new BigDecimal(obj1.toString()).multiply(new BigDecimal(obj2.toString()))).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
		}
		return new BigDecimal("0.0");
	}
	
	/**
	 *  This Method is use to divide two Number and return BigDecimal Number. 
	 */
	public static BigDecimal BigDecimalDivide(Object obj1,Object obj2) {
		if(obj1 != null && !"".equals(obj1.toString()) &&
				obj2 != null && !"".equals(obj2.toString())){
			return (new BigDecimal(obj1.toString()).divide(new BigDecimal(obj2.toString()), 4, BigDecimal.ROUND_HALF_UP));
		}
		return new BigDecimal("0.0");
	}
	
	/**
	 * This Method is Used to get AccountLine Number.
	 */
	public static String getAccountLineNumber(ServiceOrderManager serviceOrderManager,ServiceOrder serviceOrder){
		List maxLineNumberList = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
		String accountLineNumber = "001";
        if ( maxLineNumberList.get(0) != null ) {
         	Long autoLineNumber = Long.parseLong((maxLineNumberList).get(0).toString().trim()) + 1; 
             if((autoLineNumber.toString()).length() == 2) {
             	accountLineNumber = "0"+(autoLineNumber.toString());
             }else if((autoLineNumber.toString()).length() == 1) {
             	accountLineNumber = "00"+(autoLineNumber.toString());
             }else {
             	accountLineNumber=autoLineNumber.toString();
             }
        }
        return accountLineNumber;
	}
	
	/**
	 * This Method is used to set PayableContractCurrecy And Exchange Rate
	 */
			
	public static AccountLine setPayableContractCurrencyAndExchangeRage(AccountLine accountLine,ExchangeRateManager exchangeRateManager,String corpId,String currency){
		
		accountLine.setPayableContractCurrency(currency);
		accountLine.setEstimatePayableContractCurrency(currency);
		accountLine.setRevisionPayableContractCurrency(currency);
		
		accountLine.setCountry(currency);
		accountLine.setEstCurrency(currency);
		accountLine.setRevisionCurrency(currency);
		
		accountLine.setPayableContractValueDate(new Date());
		accountLine.setEstimatePayableContractValueDate(new Date());
		accountLine.setRevisionPayableContractValueDate(new Date());
		accountLine.setValueDate(new Date());
		accountLine.setEstValueDate(new Date());
		
		
		accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setExchangeRate(Double.parseDouble("1.0000"));
		accountLine.setEstExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
		
		List payableContractCurrencyExchangeRateList = exchangeRateManager.findAccExchangeRate(corpId,currency);
		if(payableContractCurrencyExchangeRateList != null && (!(payableContractCurrencyExchangeRateList.isEmpty())) && payableContractCurrencyExchangeRateList.get(0)!= null && (!(payableContractCurrencyExchangeRateList.get(0).toString().equals("")))){
			try{
				accountLine.setPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setExchangeRate(Double.parseDouble(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setEstExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setRevisionExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
			}catch(Exception e){e.printStackTrace();}
		}
		return accountLine;
	}
	
	/**
	 * This Method is used to set Only Payable, Estimate & Revision Currency And Exchange Rate
	 */
			
	public static AccountLine setOnlyPayableContractCurrencyAndExchangeRage(AccountLine accountLine,ExchangeRateManager exchangeRateManager,String corpId,String currency){
		
		accountLine.setPayableContractCurrency(currency);
		accountLine.setEstimatePayableContractCurrency(currency);
		accountLine.setRevisionPayableContractCurrency(currency);
		
		
		accountLine.setPayableContractValueDate(new Date());
		accountLine.setEstimatePayableContractValueDate(new Date());
		accountLine.setRevisionPayableContractValueDate(new Date());
		
		
		accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
		
		List payableContractCurrencyExchangeRateList = exchangeRateManager.findAccExchangeRate(corpId,currency);
		if(payableContractCurrencyExchangeRateList != null && (!(payableContractCurrencyExchangeRateList.isEmpty())) && payableContractCurrencyExchangeRateList.get(0)!= null && (!(payableContractCurrencyExchangeRateList.get(0).toString().equals("")))){
			try{
				accountLine.setPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return accountLine;
	}
	
	/**
	 * This Method is used to set Country,EstCurrency,Revision Currency And Exchange Rate
	 */
			
	public static AccountLine setCountryCurrencyAndExchangeRage(AccountLine accountLine,ExchangeRateManager exchangeRateManager,String corpId,String currency){
		
		accountLine.setCountry(currency);
		accountLine.setEstCurrency(currency);
		accountLine.setRevisionCurrency(currency);
		
		accountLine.setValueDate(new Date());
		accountLine.setEstValueDate(new Date());
		accountLine.setRevisionValueDate(new Date());
		
		
		accountLine.setExchangeRate(Double.parseDouble("1.0000"));
		accountLine.setEstExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
		
		List payableContractCurrencyExchangeRateList = exchangeRateManager.findAccExchangeRate(corpId,currency);
		if(payableContractCurrencyExchangeRateList != null && (!(payableContractCurrencyExchangeRateList.isEmpty())) && payableContractCurrencyExchangeRateList.get(0)!= null && (!(payableContractCurrencyExchangeRateList.get(0).toString().equals("")))){
			try{
				accountLine.setEstExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setExchangeRate(accountLine.getEstExchangeRate().doubleValue());
				accountLine.setRevisionExchangeRate(accountLine.getEstExchangeRate());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return accountLine;
	}
	
	/**
	 * This Method is used to set Country Currency And Exchange Rate for Base Currency
	 */
	public static AccountLine setCountryCurrencyAndExchangeRageForBaseCurrency(AccountLine accountLine,String currency){
		accountLine.setEstCurrency(currency);
		accountLine.setRevisionCurrency(currency);	
		accountLine.setCountry(currency);
		
		accountLine.setValueDate(new Date());
		accountLine.setEstValueDate(new Date());
		accountLine.setRevisionValueDate(new Date());
		
		accountLine.setExchangeRate(new Double(1.0000));
		accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
		accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
		
		return accountLine;
	}
	
	/**
	 * This Method is used to set RecRateCurrecy And Exchange Rate
	 */
	public static AccountLine setRecRateCurrencyAndExchangeRage(AccountLine accountLine,ExchangeRateManager exchangeRateManager,String corpId,String currency){
		accountLine.setRecRateCurrency(currency);
		accountLine.setEstSellCurrency(currency);
		accountLine.setRevisionSellCurrency(currency);
		
		accountLine.setRacValueDate(new Date());
		accountLine.setEstSellValueDate(new Date());
		accountLine.setRevisionSellValueDate(new Date());
		
		accountLine.setRecRateExchange(new BigDecimal(1.0000));
		accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
		
		List accExchangeRateList=exchangeRateManager.findAccExchangeRate(corpId,currency);
		if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
			try{
				accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
				accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return accountLine;
	}
	
	/**
	 * This Method is used to set RecRateCurrecy And Exchange Rate when company Div Curr is null
	 */
	public static AccountLine setRecRateCurrencyAndExchangeRageForBaseCurrency(AccountLine accountLine,String currency){
		accountLine.setRecRateCurrency(currency);
		accountLine.setEstSellCurrency(currency);
		accountLine.setRevisionSellCurrency(currency);
		
		accountLine.setRacValueDate(new Date());
		accountLine.setEstSellValueDate(new Date());
		accountLine.setRevisionSellValueDate(new Date());
		
		accountLine.setRecRateExchange(new BigDecimal(1.0000));
		accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
		return accountLine;
	}
	
	/**
	 * This Method is used to set ContractCurrecy And Exchange Rate.
	 */
	public static AccountLine setContractCurrencyAndExchangeRage(AccountLine accountLine,ExchangeRateManager exchangeRateManager,String corpId,String currency){
		accountLine.setContractCurrency(currency);
		accountLine.setEstimateContractCurrency(currency);
		accountLine.setRevisionContractCurrency(currency);
		
		accountLine.setContractValueDate(new Date());
		accountLine.setEstimateContractValueDate(new Date());
		accountLine.setRevisionContractValueDate(new Date());
		
		accountLine.setContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));
		accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
		
		List contractCurrencyExchangeRateList=exchangeRateManager.findAccExchangeRate(corpId,currency);
		if(contractCurrencyExchangeRateList != null && (!(contractCurrencyExchangeRateList.isEmpty())) && contractCurrencyExchangeRateList.get(0)!= null && (!(contractCurrencyExchangeRateList.get(0).toString().equals("")))){
			try{
				accountLine.setContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setEstimateContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setRevisionContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
			}catch(Exception e){e.printStackTrace();}
		}		
		return accountLine;
	}
	
	/**
	 * This Method is used to get Exchange Rate.
	 */
	public static BigDecimal getExchangeRate(ExchangeRateManager exchangeRateManager,String corpId,String currency){
        BigDecimal estimateContractExchangeRateBig=new BigDecimal("1.0000");
        try {
			List estimateContractRate=exchangeRateManager.findAccExchangeRate(corpId,currency);
			 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
			 {
				 return new BigDecimal(estimateContractRate.get(0).toString());
			 }
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return estimateContractExchangeRateBig;
	}
	
	/**
	 * This Method is used to validate null and blank. 
	 *  defaultValue is 0 or 1.0
	 */
	public static BigDecimal nullAndBlankCheck(Object obj,String defaultValue){
		BigDecimal value = new BigDecimal(defaultValue);
		if (obj != null && (!"".equals(obj.toString().trim()))) {
			value = new BigDecimal(obj.toString().trim());
		}
		return value;
	}
	
	/**
	 * This Method is used to set Map value for Build Formula Expression.
	 */
	public static Map setValueForBuildFormulaByBillingDTO(BillingDTO billingDTO,Map values){
		
		values.put("billing.onHand",nullAndBlankCheck(billingDTO.getOnHand(),"0.00"));
		values.put("billing.cycle",nullAndBlankCheck(billingDTO.getCycle(),"0.00"));
		values.put("billing.postGrate",nullAndBlankCheck(billingDTO.getPostGrate(),"0.00"));
		
		values.put("billing.insuranceBuyRate",nullAndBlankCheck(billingDTO.getInsuranceBuyRate(),"0.00"));
		values.put("billing.insuranceRate",nullAndBlankCheck(billingDTO.getInsuranceRate(),"0.00"));
		values.put("billing.insuranceValueActual",nullAndBlankCheck(billingDTO.getInsuranceValueActual(),"0.00"));
		values.put("billing.baseInsuranceValue",nullAndBlankCheck(billingDTO.getBaseInsuranceValue(),"0.00"));
		values.put("billing.insuranceValueEntitle",nullAndBlankCheck(billingDTO.getInsuranceValueEntitle(),"0.00"));
		values.put("billing.totalInsrVal",nullAndBlankCheck(billingDTO.getTotalInsrVal(),"0.00"));
		
		values.put("charges.minimum",nullAndBlankCheck(billingDTO.getMinimum(),"0.00"));
		values.put("billing.payableRate",nullAndBlankCheck(billingDTO.getPayableRate(),"0.00"));
		
		values.put("billing.storagePerMonth",nullAndBlankCheck(billingDTO.getStoragePerMonth(),"0.00"));
		values.put("billing.vendorStoragePerMonth",nullAndBlankCheck(billingDTO.getVendorStoragePerMonth(),"0.00"));
		values.put("billing.insurancePerMonth",nullAndBlankCheck(billingDTO.getInsurancePerMonth(),"0.00"));
		
		values.put("charges.perValue", nullAndBlankCheck(billingDTO.getPerValue().toString(),"0.00"));
		
		
		return values;
	}
	
	/**
	 * This Method is used to set Map value for Build Formula Expression by Billing Object.
	 */
	public static Map setValueForBuildFormulaByBilling(Billing billing,Map values){
		
		values.put("billing.onHand",nullAndBlankCheck(billing.getOnHand(),"0.00"));
		values.put("billing.cycle",nullAndBlankCheck(billing.getCycle(),"0.00"));
		values.put("billing.postGrate",nullAndBlankCheck(billing.getPostGrate(),"0.00"));
		
		values.put("billing.payableRate",nullAndBlankCheck(billing.getPayableRate(),"0.00"));
		
		values.put("billing.insuranceBuyRate",nullAndBlankCheck(billing.getInsuranceBuyRate(),"0.00"));
		values.put("billing.insuranceRate",nullAndBlankCheck(billing.getInsuranceRate(),"0.00"));
		values.put("billing.insuranceValueActual",nullAndBlankCheck(billing.getInsuranceValueActual(),"0.00"));
		values.put("billing.baseInsuranceValue",nullAndBlankCheck(billing.getBaseInsuranceValue(),"0.00"));
		values.put("billing.insuranceValueEntitle",nullAndBlankCheck(billing.getInsuranceValueEntitle(),"0.00"));
		values.put("billing.totalInsrVal",nullAndBlankCheck(billing.getTotalInsrVal(),"0.00"));
		
		values.put("billing.storagePerMonth",nullAndBlankCheck(billing.getStoragePerMonth(),"0.00"));
		values.put("billing.vendorStoragePerMonth",nullAndBlankCheck(billing.getVendorStoragePerMonth(),"0.00"));
		values.put("billing.insurancePerMonth",nullAndBlankCheck(billing.getInsurancePerMonth(),"0.00"));
		values.put("billing.storageMeasurement", nullAndBlankCheck(billing.getStorageMeasurement(),"0.00"));
		return values;
	}
	
	/**
	 * This Method is used to set Link & Sync fields.
	 */
	public static AccountLine setLinkSyncAccFields(CustomerFileManager customerFileManager,AccountLine accountLineTo,AccountLine accountLineFrom,String contractType){
		List fieldToSync=customerFileManager.findAgentAccFieldToSyncCreate("AccountLine",contractType);  // contractType should be CMM or DMM
		if(fieldToSync != null && !fieldToSync.isEmpty()){
			Iterator it1=fieldToSync.iterator();
			while(it1.hasNext()){
				String field=it1.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
					beanUtilsBean.setProperty(accountLineTo,fieldTo.trim(),beanUtilsBean.getProperty(accountLineFrom, fieldFrom.trim()));
				}catch(Exception ex){
					 ex.printStackTrace();
				}
			}
		}
		return accountLineTo;
	}
}
