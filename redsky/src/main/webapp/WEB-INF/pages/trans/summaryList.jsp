<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.lang.*"%>


<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%@page import="java.util.TreeMap"%>
<head>
    <title>Work Plan Summary List</title>
    <meta name="heading" content="Work Plan Summary List"/>
<style>
table
{
border-collapse:collapse; and empty-cells: show;
}


</style>
</head>
<s:form id="summaryForm" action="" method="post" validate="true">
<div id="newmnav" >
		  <ul>
		  		<c:if test="${empty param.popup}">
		<li><a href="workPlan.html"><span>Work Plan</span></a></li>
		</c:if>
		<c:if test="${not empty param.popup}">  
		<li><a href="workPlan.html?decorator=popup&popup=true"><span>Work Plan</span></a></li>
		</c:if>
		    <li  id="newmnav1" ><a class="current"><span>Work Planning Summary List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
</div>
<div class="spn" style="width:950px;">&nbsp;</div>

<table class="mainDetailTable" cellpadding="0" cellspacing="0" border="0" style="margin:0px; padding:0px; width:1000px;!margin-top:3px;">
	<tr style="margin: 0px; padding: 0px;">
		<td class="heading-bg" align="center">Date</td>
		<td class="heading-bg" align="center">Warehouse</td>
		<td class="heading-bg" align="center" width="100px">Count & Sum</td>
		<td width="600px" style="margin: 0px; padding: 0px;">
			<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px;">
				<tr>
					<td colspan="6" class="heading-bg" align="center">Job Count / Avg. Weight Total</td>
				</tr>
				<tr>
					<td class="heading-bg" width="100px" align="center">DL</td>
					<td class="heading-bg" width="100px" align="center">DU</td>
					<td class="heading-bg" width="100px" align="center">LD</td>
					<td class="heading-bg" width="100px" align="center">PK</td>
					<td class="heading-bg" width="100px" align="center">PL</td>
					<td class="heading-bg" width="100px" align="center">UP</td>
				</tr>
			</table>
		</td>
		<td class="heading-bg" width="90px" align="center">Sub Total</td>
		 <configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
		<td class="heading-bg" width="90px" align="center">Utilization</td>
		</configByCorp:fieldVisibility>
	</tr>
	<% 
		BigDecimal totalTickets = new BigDecimal("0.00");
		BigDecimal totalAmounts = new BigDecimal("0.00");
		
		SortedMap <String, Map> dateMap = (SortedMap <String, Map>)request.getAttribute("dateMap");
		SortedMap <String, String> utilizationMap = (SortedMap <String, String>)request.getAttribute("utilizationMap");
		Map <Map, Map> crewWHAndHubVal = (Map <Map, Map>)request.getAttribute("crewWHAndHubVal");
		SortedMap <String, String> hubMap = (SortedMap <String, String>)request.getAttribute("hubMap");
		
		Iterator dateIterator = dateMap.entrySet().iterator(); 
		while (dateIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) dateIterator.next();
			String date = (String) entry.getKey();
	%>	
	<tr height="10px" style="margin: 0px;  padding: 0px;" >
			<td class="list-columnmain" style="border-right:2px solid #D1D1D1;"><%=date%></td>
		<% 
			SortedMap <String, Map> warehouseMap = (SortedMap <String, Map>)dateMap.get(date);
			Iterator whIterator = warehouseMap.entrySet().iterator(); 
			int wh = 0;
			
			BigDecimal dlSum = new BigDecimal(0);
			BigDecimal duSum = new BigDecimal(0);
			BigDecimal ldSum = new BigDecimal(0);
			BigDecimal pkSum = new BigDecimal(0);
			BigDecimal upSum = new BigDecimal(0);
			BigDecimal plSum = new BigDecimal(0);
			
			BigDecimal dlCrew = new BigDecimal(0);
			BigDecimal duCrew = new BigDecimal(0);
			BigDecimal ldCrew = new BigDecimal(0);
			BigDecimal pkCrew = new BigDecimal(0);
			BigDecimal upCrew = new BigDecimal(0);
			BigDecimal plCrew = new BigDecimal(0);
			
			
			BigDecimal warehouseUtilTicket = new BigDecimal(0);
			BigDecimal warehouseUtilWeight = new BigDecimal(0);
			BigDecimal wareHouseCrewVal = new BigDecimal(0);
			BigDecimal divideVal = new BigDecimal(100);
			BigDecimal wareHouseCrewPercentage = new BigDecimal(0);
			BigDecimal hubCrewVal = new BigDecimal(0);
			BigDecimal hubCrewPercentage = new BigDecimal(0);
			String wareHsCrewTemp="";
			String hubCrewTemp="";
			double wareHsCrewTempInt;
			double hubCrewTempInt;

			Double wh1=null;
			Double wh2=null;
			Double h1=null;
			Double h2=null;
			
			Integer serviceCountTotal = 0;
			BigDecimal serviceSumTotal = new BigDecimal(0);
			BigDecimal serviceCrewTotal = new BigDecimal(0);
			
			
			String grandWHTotal="";
			String fGrandWHTotal="";
			String totalHbWt ="";
			String warehouseUtilA="";
			String warehouseUtilB="";
			
			Integer dlCount = 0;
			Integer duCount = 0;
			Integer ldCount = 0;
			Integer pkCount = 0;
			Integer upCount = 0;
			Integer plCount = 0;
			
			String dlString = "";
			String duString = "";
			String ldString = "";
			String pkString = "";
			String upString = "";
			String plString = "";
			
			String fDLString = "";
			String fDUString = "";
			String fLDString = "";
			String fPKString = "";
			String fUPString = "";
			String fPLString = "";
			
			String fUtilizationHbWt = "";
			String tempAllValue="";
			String dailyHubLimitTemp="";
			String dailyWtLimitTemp="";
			double dailyHubLimitTempInt;
			double dailyWtLimitTempInt;
			Integer wHb = 0;
			Integer wWt = 0;
			
			Double d1=null;
			Double d2=null;
			Double d3=null;
			Double d4=null;
			Double w1=null;
			Double w2=null;
			Double w3=null;
			Double w4=null;
			
			String dailyOpValue="";
			String daliyWtValue="";
			String keyValue = "";
			
			while (whIterator.hasNext()) {
				Map.Entry entryWH = (Map.Entry) whIterator.next();
				String wareHouse = (String) entryWH.getKey();
				wh = wh+1;	
				if(wh>1){%>
					<td class="list-columndate" style="border-right:2px solid #D1D1D1;">&nbsp;</td>
				<%}%>
				<td class="list-columnmain" style="border-right:2px solid #D1D1D1;"><%=wareHouse%></td>
				<td class="list-columnmain" style="border-right:2px solid #D1D1D1; ">Ticket Count :<BR>Avg.Wgt.Sum :
				 	<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
						<BR>Crew Required :
					</configByCorp:fieldVisibility>
				</td>
				<td style="width:600px;!width:598px; margin-top:0px; margin-bottom:0px; padding: 0px; border-right:1px solid #D1D1D1;">
					<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px;">
						<tr>
						<% 
						if(crewWHAndHubVal!=null && !crewWHAndHubVal.isEmpty()){
							for (Map.Entry<Map, Map> entry5 : crewWHAndHubVal.entrySet()) {
								Map <String, String> crewHub =entry5.getValue();
								for (Map.Entry<String, String> entry2 :crewHub.entrySet()) {
									String hubDate = (String)entry2.getKey();
									if(hubDate.equalsIgnoreCase(date)){
										String hubCrew = (String)entry2.getValue();
								    	hubCrewVal = new BigDecimal(hubCrew);
									}
								}
							    Map <String, String> crewWR = entry5.getKey();
							    for (Map.Entry<String, String> entry1 :crewWR.entrySet()) {
							    	String wareHouseCodeAndDate = (String)entry1.getKey();
							    	String wareHouseDate = wareHouseCodeAndDate.split("~")[0];
							    	if(wareHouseDate.equalsIgnoreCase(date)){
							    		String wareHouseCode = wareHouseCodeAndDate.split("~")[1];
							    	if(wareHouseCode.equalsIgnoreCase(wareHouse)){
							    		String wareHouseCrewValString = (String)entry1.getValue();							    		
							    		wareHouseCrewVal = new BigDecimal(wareHouseCrewValString);
								    	}						    	
							    	}						    
								}
							}
						}
						%>						
						<%
						SortedMap <String, Map> serviceMap = (SortedMap <String, Map>)warehouseMap.get(wareHouse);
						SortedMap sumCountDisplayMap = new TreeMap <String, String>();
						
						
						Integer serviceCount = 0;
						BigDecimal serviceSum = new BigDecimal(0);
						BigDecimal serviceCrewDL = new BigDecimal(0);
						BigDecimal serviceCrewDU = new BigDecimal(0);
						BigDecimal serviceCrewLD = new BigDecimal(0);
						BigDecimal serviceCrewPK = new BigDecimal(0);
						BigDecimal serviceCrewPL = new BigDecimal(0);
						BigDecimal serviceCrewUP = new BigDecimal(0);
						BigDecimal serviceCrewAll = new BigDecimal(0);
						
						Map dlMap = serviceMap.get("DL");
						sumCountDisplayMap.put("DL", dlMap.get("count") + "<BR>" + dlMap.get("sum") + "<BR>" + dlMap.get("crew"));
						
						dlSum = dlSum.add(new BigDecimal(dlMap.get("sum").toString()));
						dlCount = dlCount + new Integer(dlMap.get("count").toString());
						dlCrew = dlCrew.add(new BigDecimal(dlMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(dlMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(dlMap.get("count").toString());
						serviceCrewDL = serviceCrewDL.add(new BigDecimal(dlMap.get("crew").toString()));
						
						Map duMap = serviceMap.get("DU");			
						sumCountDisplayMap.put("DU", duMap.get("count") + "<BR>" + duMap.get("sum") + "<BR>" + duMap.get("crew"));
						
						duSum = duSum.add(new BigDecimal(duMap.get("sum").toString()));
						duCount = duCount + new Integer(duMap.get("count").toString());
						duCrew = duCrew.add(new BigDecimal(duMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(duMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(duMap.get("count").toString());
						serviceCrewDU = serviceCrewDU.add(new BigDecimal(duMap.get("crew").toString()));
						
						Map ldMap = serviceMap.get("LD");	 
						sumCountDisplayMap.put("LD", ldMap.get("count") + "<BR>" + ldMap.get("sum") + "<BR>" + ldMap.get("crew"));
						
						ldSum = ldSum.add(new BigDecimal(ldMap.get("sum").toString()));
						ldCount = ldCount + new Integer(ldMap.get("count").toString());
						ldCrew = ldCrew.add(new BigDecimal(ldMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(ldMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(ldMap.get("count").toString());
						serviceCrewLD = serviceCrewLD.add(new BigDecimal(ldMap.get("crew").toString()));
						
						Map pkMap = serviceMap.get("PK");
						sumCountDisplayMap.put("PK", pkMap.get("count") + "<BR>" + pkMap.get("sum") + "<BR>" + pkMap.get("crew"));
						
						pkSum = pkSum.add(new BigDecimal(pkMap.get("sum").toString()));
						pkCount = pkCount + new Integer(pkMap.get("count").toString());
						pkCrew = pkCrew.add(new BigDecimal(pkMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(pkMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(pkMap.get("count").toString());
						serviceCrewPK = serviceCrewPK.add(new BigDecimal(pkMap.get("crew").toString()));
						
						Map plMap = serviceMap.get("PL");
						sumCountDisplayMap.put("PL", plMap.get("count") + "<BR>" + plMap.get("sum") + "<BR>" + plMap.get("crew"));
						
						plSum = plSum.add(new BigDecimal(plMap.get("sum").toString()));
						plCount = plCount + new Integer(plMap.get("count").toString());
						plCrew = plCrew.add(new BigDecimal(plMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(plMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(plMap.get("count").toString());
						serviceCrewPL = serviceCrewPL.add(new BigDecimal(plMap.get("crew").toString()));
						
						Map upMap = serviceMap.get("UP");
						sumCountDisplayMap.put("UP", upMap.get("count") + "<BR>" + upMap.get("sum") + "<BR>" + upMap.get("crew"));
						
						upSum = upSum.add(new BigDecimal(upMap.get("sum").toString()));
						upCount = upCount + new Integer(upMap.get("count").toString());
						upCrew = upCrew.add(new BigDecimal(upMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(upMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(upMap.get("count").toString());
						serviceCrewUP = serviceCrewUP.add(new BigDecimal(upMap.get("crew").toString()));
						serviceCrewAll = serviceCrewDL.add(serviceCrewUP).add(serviceCrewPL).add(serviceCrewPK).add(serviceCrewDU).add(serviceCrewLD);
						
						wh1=serviceCrewAll.doubleValue();
						wh2=wareHouseCrewVal.doubleValue();
						try{
						wareHsCrewTempInt = (wh1/wh2)*100;	
						wareHsCrewTempInt = ( new Float( Math.round(wareHsCrewTempInt)) );
						}catch(Exception e){
							wareHsCrewTempInt = new Double(0);
						}										
						if(wareHsCrewTempInt > 100){
							wareHsCrewTemp = "<font color='red'>"+wareHsCrewTempInt+"</font>";
						}else{											
							wareHsCrewTemp = "<font color='003366'>"+wareHsCrewTempInt+"</font>";
						}
						wareHsCrewTemp = wareHsCrewTemp.substring(0,wareHsCrewTemp.indexOf("."))+"%";
						
						serviceCountTotal = dlCount + duCount + ldCount + pkCount + plCount + upCount;
						serviceSumTotal = dlSum.add(duSum).add(ldSum).add(pkSum).add(plSum).add(upSum);	
						serviceCrewTotal = dlCrew.add(duCrew).add(ldCrew).add(pkCrew).add(plCrew).add(upCrew);	
						
						h1=serviceCrewTotal.doubleValue();
						h2=hubCrewVal.doubleValue();
						try{
							hubCrewTempInt = (h1/h2)*100;	
							hubCrewTempInt = ( new Float( Math.round(hubCrewTempInt)) );							
							}catch(Exception e){
								hubCrewTempInt = new Double(0);
							}
						if(hubCrewTempInt > 100){
							hubCrewTemp = "<font color='red'>"+hubCrewTempInt+"</font>";
						}else{											
							hubCrewTemp = "<font color='003366'>"+hubCrewTempInt+"</font>";
						}
						hubCrewTemp = hubCrewTemp.substring(0,hubCrewTemp.indexOf("."))+"%";						
												
						String grandTotal = grandTotal = serviceCount+"<BR>"+serviceSum;
						String fGrandTotal = "";
						if(grandTotal.contains(".")){
							fGrandTotal = grandTotal.substring(0, grandTotal.indexOf("."));
						}else{
							fGrandTotal = grandTotal;
						}						
												
						if(utilizationMap!=null && !utilizationMap.isEmpty()){
						tempAllValue = utilizationMap.get(wareHouse);
						dailyHubLimitTemp = tempAllValue.split("~")[0];
						dailyWtLimitTemp = tempAllValue.split("~")[1];
						d1=Double.parseDouble(dailyHubLimitTemp);
						d2=Double.parseDouble(dailyWtLimitTemp);
						d3=Double.parseDouble(serviceCount.toString());
						d4=Double.parseDouble(serviceSum.toString());
						dailyHubLimitTempInt = (d3/d1)*100;
						dailyWtLimitTempInt = (d4/d2)*100;
						dailyHubLimitTempInt = ( new Float( Math.round(dailyHubLimitTempInt)) );
						dailyWtLimitTempInt = ( new Float( Math.round(dailyWtLimitTempInt)) );
						
						if(dailyHubLimitTempInt > 100){
							dailyHubLimitTemp = "<font color='red'>"+dailyHubLimitTempInt+"</font>";
						}else{
							dailyHubLimitTemp = "<font color='#003366'>"+dailyHubLimitTempInt+"</font>";
						}
						
						if(dailyWtLimitTempInt > 100){
							dailyWtLimitTemp = "<font color='red'>"+dailyWtLimitTempInt+"</font>";
						}else{
							dailyWtLimitTemp = "<font color='#003366'>"+dailyWtLimitTempInt+"</font>";
						}
						
						dailyHubLimitTemp = dailyHubLimitTemp.substring(0,dailyHubLimitTemp.indexOf("."))+"%";
						dailyWtLimitTemp = dailyWtLimitTemp.substring(0,dailyWtLimitTemp.indexOf("."))+"%";						
						
						fUtilizationHbWt = dailyHubLimitTemp+"<BR>"+dailyWtLimitTemp;
						
						for (Map.Entry<String, String> entry1 : hubMap.entrySet()) {
						    dailyOpValue = entry1.getKey();
						    daliyWtValue = entry1.getValue();						    
						}
						
						warehouseUtilTicket = new BigDecimal(dailyOpValue);
						warehouseUtilWeight = new BigDecimal(daliyWtValue);
						w1=(warehouseUtilTicket).doubleValue();
						w2=(warehouseUtilWeight).doubleValue();
						w3=(double)serviceCountTotal;
						w4=(serviceSumTotal).doubleValue();
						warehouseUtilTicket = BigDecimal.valueOf((w3/w1)*100);
						warehouseUtilWeight = BigDecimal.valueOf((w4/w2)*100);						
						
						warehouseUtilA = warehouseUtilTicket.setScale(0,warehouseUtilTicket.ROUND_HALF_UP).toPlainString();
						warehouseUtilB = warehouseUtilWeight.setScale(0,warehouseUtilWeight.ROUND_HALF_UP).toPlainString();
						
						wHb = Integer.parseInt(warehouseUtilA);
						if(wHb > 100 ){
							warehouseUtilA = "<font color='red'>"+warehouseUtilA+"%"+"</font>";
						}else{
							warehouseUtilA = "<font color='#F37A03'>"+warehouseUtilA+"%"+"</font>";
						}
						
						wWt = Integer.parseInt(warehouseUtilB);
						if(wWt > 100 ){
							warehouseUtilB = "<font color='red'>"+warehouseUtilB+"%"+"</font>";
						}else{
							warehouseUtilB = "<font color='#F37A03'>"+warehouseUtilB+"%"+"</font>";
						}
						
						totalHbWt = warehouseUtilA+"<BR>"+warehouseUtilB;						
						}
						
						grandWHTotal = serviceCountTotal+"<BR>"+serviceSumTotal;
						if(grandWHTotal.contains(".")){
							fGrandWHTotal = grandWHTotal.substring(0, grandWHTotal.indexOf("."));
						}else{
							fGrandWHTotal = grandWHTotal;
						}
						
						totalTickets = totalTickets.add(new BigDecimal(serviceCount));
						totalAmounts = totalAmounts.add(serviceSum);
					
						dlString = dlCount+"<BR>"+dlSum;
						if(dlString.contains(".")){
							fDLString = dlString.substring(0, dlString.indexOf("."));
						}else{
							fDLString = dlString;
						}
						
						duString = duCount+"<BR>"+duSum;
						if(duString.contains(".")){
							fDUString = duString.substring(0, duString.indexOf("."));
						}else{
							fDUString = duString;
						}
						
						ldString = ldCount+"<BR>"+ldSum;
						if(ldString.contains(".")){
							fLDString = ldString.substring(0, ldString.indexOf("."));
						}else{
							fLDString = ldString;
						}
						
						pkString = pkCount+"<BR>"+pkSum;
						if(pkString.contains(".")){
							fPKString = pkString.substring(0, pkString.indexOf("."));
						}else{
							fPKString = pkString;
						}
						
						plString = plCount+"<BR>"+plSum;
						if(plString.contains(".")){
							fPLString = plString.substring(0, plString.indexOf("."));
						}else{
							fPLString = plString;
						}
						
						upString = upCount+"<BR>"+upSum;
						if(upString.contains(".")){
							fUPString = upString.substring(0, upString.indexOf("."));
						}else{
							fUPString = upString;
						}
						
						
												
						Iterator sumCountDisplayMapIter = sumCountDisplayMap.entrySet().iterator(); 
						while (sumCountDisplayMapIter.hasNext()) {
							Map.Entry entryVal = (Map.Entry) sumCountDisplayMapIter.next();
							String iterateKey = (String) entryVal.getKey();
							String iterateValue = (String) sumCountDisplayMap.get(iterateKey); 
							String fValue = "";
							if(iterateValue.contains(".")){
								fValue = iterateValue.substring(0, iterateValue.indexOf("."));
							}else{
								fValue = iterateValue;
							}
							
							if(iterateValue.equalsIgnoreCase("0<BR>0<BR>0")){%>
								<td class="list-columnmain" width="100px"></td>	
							<%}else{%>
								<td align="right" class="list-columnmain" width="100px" style="background-color: #ededed;"><%=fValue%><br>
								<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
								<%if(iterateKey.equalsIgnoreCase("DL")){%>
									<%=serviceCrewDL%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("LD")){%>
									<%=serviceCrewLD%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("PK")){%>
									<%=serviceCrewPK%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("UP")){%>
									<%=serviceCrewUP%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("PL")){%>
									<%=serviceCrewPL%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("DU")){%>
									<%=serviceCrewDU%>									
								<%}%>
								</configByCorp:fieldVisibility>								
								</td>
									
							<%}
						}%>	
						</tr>
					</table>
				</td>
				<%if(grandTotal.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_last_bg" style="background-color: #d2e1f4"></td>	
				<%}else{%>
					<td align="right" class="list_last_bg" style=""><b><%=fGrandTotal%></b>
						<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
						<br><b><%=serviceCrewAll%> </b>
						</configByCorp:fieldVisibility>
					</td>		
				<%}%>	
				 <configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">			
					<td align="right" class="list_last_bg" style=""><b><%=fUtilizationHbWt%></b><br>
						<b><%=wareHsCrewTemp%></b>
					</td>
			</configByCorp:fieldVisibility>	
			
	</tr>
	<%}%>	
	<tr>
		<td class="list-column" style="border-right:2px solid #D1D1D1;"></td>
		<td colspan="2" class="list_sum_bg" style="border-right:2px solid #D1D1D1;"><b>Warehouse Ticket Count<br>Warehouse Weight Total</b><br>
		<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
		<b>Hub Crew Total</b>
		</configByCorp:fieldVisibility>
		</td>
		<td style="width:600px;!width:598px;margin-top:0px; margin-bottom:0px; padding: 0px; border-right:1px solid #D1D1D1;">
			<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px; ">
				<tr>
				
				<%if(dlString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fDLString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=dlCrew%></b>
					</configByCorp:fieldVisibility>
					</td>	
				<%}%>
				<%if(duString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fDUString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=duCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(ldString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fLDString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=ldCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(pkString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fPKString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=pkCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(plString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fPLString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=plCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(upString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fUPString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=upCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				
				</tr>
			</table>
		</td>
		<%if(grandWHTotal.equalsIgnoreCase("0<BR>0<BR>0")){%>
				<td class="list_sum_bg" style=""></td>	
		<%}else{%>
				<td align="right" class="list_grand_bg" style=""><b><%=fGrandWHTotal%></b>
				<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
				<br><b><%=serviceCrewTotal %></b>
				</configByCorp:fieldVisibility>
				</td>
		<%}%>
		 <configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
		<td align="right" class="list_grand_bg" style=""><b><%=totalHbWt%></b><br>
		<b><%=hubCrewTemp%></b>
		</td>
		</configByCorp:fieldVisibility>
	</tr>
	<%}%>	
	<%
		String totalTKT = totalTickets.toString();
		String totalAMT = totalAmounts.toString();
		
		if(totalTKT.contains(".")){
			totalTKT = totalTKT.substring(0, totalTKT.indexOf("."));
		}
		
		if(totalAMT.contains(".")){
			totalAMT = totalAMT.substring(0, totalAMT.indexOf("."));
		}
		
	%>
	<tr>
		<td colspan="4" class="heading-bg" align="right"><b>Total Tickets&nbsp;&nbsp;</b></td>
		<td align="right" class="list_grand_bg" style=""><b><%=totalTKT%></b></td>
		<td align="right" class="list_grand_bg" style=""></td>
	</tr>
	<tr>
		<td colspan="4" class="heading-bg" align="right"><b>Grand Total&nbsp;&nbsp;</b></td>
		<td align="right" class="list_grand_bg" style=""><b><%=totalAMT%></b></td>
		<td align="right" class="list_grand_bg" style=""></td>
	</tr>
</table>
<configByCorp:fieldVisibility componentId="component.field.reportoninterface" >
<table>
<tr><td height="5"></td></tr>
<tr>
<td align="center" style="margin: 0px; padding: 0px"><img alt="XLS File" src="<c:url value='/images/excel32.png'/>" onclick="javascript:openWindow('viewReportWithParam.html?id=2977&reportName=Work Plan Summary&reportParameter_From Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${fromDate}" />&reportParameter_To Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${toDate}" />&reportParameter_Hub ID=${hubID}&reportParameter_Corporate ID=SSCW&fileType=XLS&formReportFlag=R')" /><a> XLS </a></td>
<td width="5"></td>
<td align="center" style="margin: 0px; padding: 0px"><img alt="PDF File" src="<c:url value='/images/pdf32.png'/>" onclick="javascript:openWindow('viewReportWithParam.html?id=2977&reportName=Work Plan Summary&reportParameter_From Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${fromDate}" />&reportParameter_To Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${toDate}" />&reportParameter_Hub ID=${hubID}&reportParameter_Corporate ID=SSCW&fileType=PDF&formReportFlag=R')" /><a> PDF </a></td>
</tr>
</table>
</configByCorp:fieldVisibility>
</s:form>

