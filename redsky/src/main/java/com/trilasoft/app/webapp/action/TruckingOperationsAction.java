package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.IntegrationLogInfo;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.TruckingOperations;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.TruckingOperationsManager;
import com.trilasoft.app.service.WorkTicketManager;

public class TruckingOperationsAction extends BaseAction implements Preparable{
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private  Miscellaneous  miscellaneous;
	private CustomerFile customerFile;
	private TruckingOperations truckingOperations;
	private ServiceOrderManager serviceOrderManager;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	private TruckingOperationsManager truckingOperationsManager;
		
	private String sessionCorpID;
	private String localTruckNumber;
	private static Date createdon;
	private List truckLineList;
	private List truckingOperationsList;

	private Long id;

	private String gotoPageString;
	private String hitFlag="";
	private String validateFormNav;
	private Long ticket;
	private Long sid;
	private Long tid;
	private WorkTicketManager workTicketManager;
	private WorkTicket workTicket;
	private Company company;
	private CompanyManager companyManager;
	private String voxmeIntergartionFlag;
    private String oiJobList;
	 
	Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(TruckingOperationsAction.class);

	public TruckingOperationsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	
	 public void prepare() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				company=companyManager.findByCorpID(sessionCorpID).get(0);
				if(company!=null && company.getOiJob()!=null){
					oiJobList=company.getOiJob();  
				}
				if(company!=null){
					if(company.getVoxmeIntegration()!=null){
						voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
						}
					}
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} 
	
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String s = save();
		validateFormNav = "OK";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}
	private Billing billing;
	private BillingManager billingManager;
	private String surveyTool;
	private int moveCount;
	private String emailSetupValue;
	@SkipValidation
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceOrder=serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(sid); 
    	trackingStatus=trackingStatusManager.get(sid);
    	billing=billingManager.get(sid);
		customerFile=serviceOrder.getCustomerFile();
	    workTicket = workTicketManager.get(tid);
	    surveyTool = truckingOperationsManager.findSurveyTool(serviceOrder.getCorpID(),serviceOrder.getJob(),serviceOrder.getCompanyDivision());
	    surveyTool=surveyTool.substring(1,surveyTool.length()-1);
	    emailSetupValue=truckingOperationsManager.findSendMoveCloudStatusCrew(sessionCorpID, customerFile.getSequenceNumber());
		if(emailSetupValue!= null && !emailSetupValue.equals("")){
			moveCount=1;
		}
		truckingOperationsList = truckingOperationsManager.workTicketTruck(ticket, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private String message="";
	private Long cid;
	/**
	 * Crew data needs to sent move cloud
	 * @return
	 */
	public String sendDataMoveToCloudTruck(){
		message=createDataTruckForMoveCloud(cid);
		return SUCCESS;
	}
	public String createDataTruckForMoveCloud(Long cid)
	{
		workTicket = workTicketManager.get(tid);
		//customerFile=workTicket.getServiceOrder().getCustomerFile();
		//String crewEmail = workTicketManager.findCrewEmail(workTicket.getWarehouse(), workTicket.getDate1(), sessionCorpID, workTicket.getTicket());
		List<TruckingOperations> truckingOperationsList = (List<TruckingOperations>)truckingOperationsManager.workTicketTruck(workTicket.getTicket(), sessionCorpID);
		
		for (TruckingOperations truckingOperations : truckingOperationsList) {
			IntegrationLogInfo inloginfo= new IntegrationLogInfo();
		    inloginfo.setSource("RedSky");
		    inloginfo.setDestination("MoveCloud");
		    inloginfo.setIntegrationId(cid.toString());
		    inloginfo.setSourceOrderNum(workTicket.getSequenceNumber());
		    inloginfo.setDestOrderNum(workTicket.getService());
		    inloginfo.setOperation("CREATE");
		    inloginfo.setOrderComplete("N");
		    inloginfo.setStatusCode("S");
		    inloginfo.setEffectiveDate(workTicket.getDate1());
		    inloginfo.setDetailedStatus("Ready For Sending");
		    inloginfo.setCreatedBy(truckingOperations.getDriver());
		    inloginfo.setCreatedOn(new Date());
		    inloginfo.setUpdatedBy(truckingOperations.getDriver());
		    inloginfo.setUpdatedOn(new Date());
		    inloginfo.setCorpID(sessionCorpID);
		    inloginfo =  integrationLogInfoManager.save(inloginfo);
		}
		return "Y";
	}
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}

	/**
	 * End Method
	 * @return
	 */
	
	@SkipValidation
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {
			truckingOperations = truckingOperationsManager.get(id);
			serviceOrder = serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
        	miscellaneous = miscellaneousManager.get(sid);
        	trackingStatus=trackingStatusManager.get(sid);
        	billing=billingManager.get(sid);
        	customerFile = serviceOrder.getCustomerFile();
		} else {
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
      	    miscellaneous = miscellaneousManager.get(sid);
            trackingStatus=trackingStatusManager.get(sid);
            billing=billingManager.get(sid);
  	        customerFile = serviceOrder.getCustomerFile();
			truckingOperations = new TruckingOperations();
			truckingOperations.setCorpID(sessionCorpID);
			truckingOperations.setTicket(ticket);
			truckingOperations.setCreatedOn(new Date());
			truckingOperations.setUpdatedOn(new Date());
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(truckingOperations.getLocalTruckNumber() != null && (!truckingOperations.getLocalTruckNumber().equals(""))){
    		if(truckNameValid() == null){
    			String key = "Selected Truck Number is not valid, Please select another.";
    			errorMessage(getText(key));
    			serviceOrder=serviceOrderManager.get(sid);
    			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    			miscellaneous = miscellaneousManager.get(sid); 
    	    	trackingStatus=trackingStatusManager.get(sid);
    	    	billing=billingManager.get(sid);
    			customerFile=serviceOrder.getCustomerFile(); 
                return SUCCESS;
    		} 
    	} 
		if(truckingOperations.getLocalTruckNumber()!=null && truckingOperations.getDescription()!=null){
		try{
		List validList=truckingOperationsManager.findWorkTicketTruck(truckingOperations.getLocalTruckNumber(),truckingOperations.getCorpID());
		if(validList.size()>0 && !validList.isEmpty()){
			truckingOperations.setDescription(validList.get(0).toString());
		}
		}catch (NullPointerException nullExc){
			nullExc.printStackTrace();						
		}
		catch (Exception e){
			e.printStackTrace();
		}
		}
		truckingOperations.setCorpID(sessionCorpID);
		boolean isNew = (truckingOperations.getId() == null);
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    	miscellaneous = miscellaneousManager.get(sid);
    	trackingStatus=trackingStatusManager.get(sid);
    	billing=billingManager.get(sid);
    	customerFile = serviceOrder.getCustomerFile();
		if (isNew) {
			truckingOperations.setCreatedOn(new Date());
		} 
		truckingOperations.setUpdatedOn(new Date());
		truckingOperations.setUpdatedBy(getRequest().getRemoteUser());
		truckingOperationsManager.save(truckingOperations);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "truckingOperations.added" : "truckingOperations.updated";
			saveMessage(getText(key));
		}
		hitFlag="1";
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	public String truckName(){   
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(localTruckNumber!=null){
        	truckLineList=truckingOperationsManager.findWorkTicketTruck(localTruckNumber,sessionCorpID);
        }
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	public String truckNameValid(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  	List validList=truckingOperationsManager.findWorkTicketTruck(truckingOperations.getLocalTruckNumber(),truckingOperations.getCorpID());
    	if(! validList.isEmpty()){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return "valid" ;
        }else{
        	
        	return null;
        }
	
       }
	public String updateTruckingOperations()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String key = "Truck has been deleted successfully.";   
        saveMessage(getText(key));
        truckingOperationsManager.updateDeleteStatus(id);
        hitFlag="1";
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	public static Date getCreatedon() {
		return createdon;
	}

	public static void setCreatedon(Date createdon) {
		TruckingOperationsAction.createdon = createdon;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public List getTruckingOperationsList() {
		return truckingOperationsList;
	}

	public void setTruckingOperationsList(List truckingOperationsList) {
		this.truckingOperationsList = truckingOperationsList;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the truckingOperations
	 */
	public TruckingOperations getTruckingOperations() {
		return truckingOperations;
	}

	/**
	 * @param truckingOperations the truckingOperations to set
	 */
	public void setTruckingOperations(TruckingOperations truckingOperations) {
		this.truckingOperations = truckingOperations;
	}

	/**
	 * @param truckingOperationsManager the truckingOperationsManager to set
	 */
	public void setTruckingOperationsManager(
			TruckingOperationsManager truckingOperationsManager) {
		this.truckingOperationsManager = truckingOperationsManager;
	}

	/**
	 * @return the ticket
	 */
	public Long getTicket() {
		return ticket;
	}

	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	
	/**
	 * @return the customerFile
	 */
	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	/**
	 * @param customerFile the customerFile to set
	 */
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	/**
	 * @return the miscellaneous
	 */
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	/**
	 * @param miscellaneous the miscellaneous to set
	 */
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	/**
	 * @return the serviceOrder
	 */
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	/**
	 * @param serviceOrder the serviceOrder to set
	 */
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	/**
	 * @return the trackingStatus
	 */
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	/**
	 * @param trackingStatus the trackingStatus to set
	 */
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	/**
	 * @return the sid
	 */
	public Long getSid() {
		return sid;
	}

	/**
	 * @param sid the sid to set
	 */
	public void setSid(Long sid) {
		this.sid = sid;
	}

	/**
	 * @param miscellaneousManager the miscellaneousManager to set
	 */
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	/**
	 * @param serviceOrderManager the serviceOrderManager to set
	 */
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	/**
	 * @param trackingStatusManager the trackingStatusManager to set
	 */
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	/**
	 * @return the tid
	 */
	public Long getTid() {
		return tid;
	}

	/**
	 * @param tid the tid to set
	 */
	public void setTid(Long tid) {
		this.tid = tid;
	}

	/**
	 * @return the truckLineList
	 */
	public List getTruckLineList() {
		return truckLineList;
	}

	/**
	 * @param truckLineList the truckLineList to set
	 */
	public void setTruckLineList(List truckLineList) {
		this.truckLineList = truckLineList;
	}

	/**
	 * @return the localTruckNumber
	 */
	public String getLocalTruckNumber() {
		return localTruckNumber;
	}

	/**
	 * @param localTruckNumber the localTruckNumber to set
	 */
	public void setLocalTruckNumber(String localTruckNumber) {
		this.localTruckNumber = localTruckNumber;
	}

	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}

	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}


	public Company getCompany() {
		return company;
	}


	public CompanyManager getCompanyManager() {
		return companyManager;
	}


	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}


	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}


	public String getOiJobList() {
		return oiJobList;
	}


	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}


	public String getSurveyTool() {
		return surveyTool;
	}


	public void setSurveyTool(String surveyTool) {
		this.surveyTool = surveyTool;
	}


	public int getMoveCount() {
		return moveCount;
	}


	public void setMoveCount(int moveCount) {
		this.moveCount = moveCount;
	}


	public String getEmailSetupValue() {
		return emailSetupValue;
	}


	public void setEmailSetupValue(String emailSetupValue) {
		this.emailSetupValue = emailSetupValue;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Long getCid() {
		return cid;
	}


	public void setCid(Long cid) {
		this.cid = cid;
	}

}
