package com.trilasoft.mss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;
import org.apache.log4j.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.trilasoft.app.dao.hibernate.MssDaoHibernate.MssDTO;
import com.trilasoft.mss.webservices.ArrayOfMSSCustomerContact;
import com.trilasoft.mss.webservices.ArrayOfMSSOrderCrate;
import com.trilasoft.mss.webservices.ArrayOfMSSOrderPhoneNumber;
import com.trilasoft.mss.webservices.ArrayOfMSSOrderService;
import com.trilasoft.mss.webservices.ArrayOfString;
import com.trilasoft.mss.webservices.MSSCustomerContact;
import com.trilasoft.mss.webservices.MSSOrder;
import com.trilasoft.mss.webservices.MSSOrderCrate;
import com.trilasoft.mss.webservices.MSSOrderPhoneNumber;
import com.trilasoft.mss.webservices.MSSOrderService;

public class MssXmlDto {
	 Logger logger = Logger.getLogger(MssXmlDto.class);
	public Object creatXMLRequest(List<MssDTO> objMss,List<MssDTO> objcrating,List<MssDTO> objorigin,List<MssDTO> objdestination,List<MssDTO> originNotesList,List<MssDTO> destinationNotesList,List<MssDTO> originPhoneList,List<MssDTO> destinationPhoneList,String transportTypeRadio ,List<MssDTO> customerContact) throws JAXBException{
		//GregorianCalendar gregory = new GregorianCalendar();
		MSSOrder mssOrder=new MSSOrder();
		mssOrder.setCustomerContactID(Integer.parseInt(objMss.get(0).getCustomerContactID().toString()));
		mssOrder.setCustomerContactEmailAddress(objMss.get(0).getCustomerContactEmailAddress().toString());
		mssOrder.setAffiliateID(Integer.parseInt(objMss.get(0).getAffiliateId().toString()));
		mssOrder.setBillToID(Integer.parseInt(objMss.get(0).getBillToId().toString().trim()));
		mssOrder.setPurchaseOrderNumber(objMss.get(0).getPurchaseOrderNumber().toString());
		mssOrder.setEstimatedWeight(Integer.parseInt(objMss.get(0).getEstimatedWeight().toString()));
		try{
			if(transportTypeRadio.equalsIgnoreCase("O") || transportTypeRadio.equalsIgnoreCase("B"))
			{
			mssOrder.setPackDateStart(getXMLdate(objMss.get(0).getPackDateStart()));
			mssOrder.setPackDateEnd(getXMLdate(objMss.get(0).getPackingEndDate()));
			mssOrder.setLoadDateStart(getXMLdate(objMss.get(0).getLoadingStartDate()));
			mssOrder.setLoadDateEnd(getXMLdate(objMss.get(0).getLoadingEndDate()));
			mssOrder.setOriginRequestDateStart(getXMLdate(objMss.get(0).getOriginRequestedDateStart()));
			}if(transportTypeRadio.equalsIgnoreCase("D") || transportTypeRadio.equalsIgnoreCase("B")){
				mssOrder.setDeliveryDateStart(getXMLdate(objMss.get(0).getDeliveryDateStart()));
				mssOrder.setDeliveryDateEnd(getXMLdate(objMss.get(0).getDeliveryEndDate()));				
				mssOrder.setDestinationRequestDateStart(getXMLdate(objMss.get(0).getDestinationRequestedDate()));	
			}
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
	
			mssOrder.setShipperFirstName(objMss.get(0).getShipperFirstName().toString());
			mssOrder.setShipperLastName(objMss.get(0).getShipperLastName().toString());
			for (MssDTO mssDTO : customerContact) {
	            if(mssDTO.getSoEmail()!=null && !mssDTO.getSoEmail().equals("")){
	             mssOrder.setShipperEmailAddress(mssDTO.getSoEmail().toString());
	            }else{
	             mssOrder.setShipperEmailAddress(mssDTO.getSoDestinationemail().toString());
	            }
	   }
	
		if(transportTypeRadio.equalsIgnoreCase("O") || transportTypeRadio.equalsIgnoreCase("B"))
		{	
		mssOrder.setShipperOriginAddress1(objMss.get(0).getShipperOriginAddress1().toString());
		mssOrder.setShipperOriginAddress2(objMss.get(0).getShipperOriginAddress2().toString());
		mssOrder.setShipperOriginCity(objMss.get(0).getShipperOriginCity().toString());
		mssOrder.setShipperOriginState(objMss.get(0).getShipperOriginState().toString());
		mssOrder.setShipperOriginZip(objMss.get(0).getShipperOriginZip().toString());
		}
		if(transportTypeRadio.equalsIgnoreCase("D") || transportTypeRadio.equalsIgnoreCase("B")){
			mssOrder.setShipperDestinationAddress1(objMss.get(0).getShipperDestinationAddress1().toString());
			mssOrder.setShipperDestinationAddress2(objMss.get(0).getShipperDestinationAddress2().toString());
			mssOrder.setShipperDestinationCity(objMss.get(0).getShipperDestinationCity().toString());
			mssOrder.setShipperDestinationState(objMss.get(0).getShipperDestinationState().toString());
			mssOrder.setShipperDestinationZip(objMss.get(0).getShipperDestinationZip().toString());
		}
		
		
		mssOrder.setUrgent(Boolean.parseBoolean(objMss.get(0).getUrgent().toString()));
		mssOrder.setVIP(Boolean.parseBoolean(objMss.get(0).getVip().toString()));
	/*	Iterator it=objcrating.iterator();
		while(it.hasNext()){
			Object[] obj=(Object[]) it.next();
			MSSOrderCrate MSSK=new MSSOrderCrate();
			MSSK.setOorDOnly(obj[0].toString());
			MSSK.setDescription(obj[1].toString());
			MSSK.setLength(Double.parseDouble(obj[2].toString()));
			MSSK.setWidth(Double.parseDouble(obj[3].toString()));
			MSSK.setHeight(Double.parseDouble(obj[4].toString()));
			MSSK.setCOD(Boolean.parseBoolean(obj[5].toString()));
			MSSK.setPlyWood(Boolean.parseBoolean(obj[6].toString()));
			MSSK.setBugWood(Boolean.parseBoolean(obj[7].toString()));
			MSSK.setPendingAuthorization(Boolean.parseBoolean(obj[8].toString()));
			mssOrder.getMSSOrderCrates().mssOrderCrate.add(MSSK);
		}*/
		
		////Order Crate.....
		
		ArrayOfMSSOrderCrate tempObj = new ArrayOfMSSOrderCrate();
		try {
			for (MssDTO mssDTO : objcrating) {
				MSSOrderCrate MSSK=new MSSOrderCrate();
				MSSK.setOorDOnly(mssDTO.getTransportType().toString());
				MSSK.setDescription(mssDTO.getDescription().toString());
				MSSK.setLength(Double.parseDouble(mssDTO.getLength().toString()));
				MSSK.setWidth(Double.parseDouble(mssDTO.getWidth().toString()));
				MSSK.setHeight(Double.parseDouble(mssDTO.getHeight().toString()));
				MSSK.setCOD(Boolean.parseBoolean(mssDTO.getCod().toString()));
				MSSK.setPlyWood(Boolean.parseBoolean(mssDTO.getPly().toString()));
				MSSK.setBugWood(Boolean.parseBoolean(mssDTO.getIntlIspmis().toString()));
				//MSSK.setPendingAuthorization(Boolean.parseBoolean(mssDTO.getIntlIspmis().toString()));
				if(mssDTO.getAppr().equals(true)){
					MSSK.setPendingAuthorization(false);
				}else{
					MSSK.setPendingAuthorization(true);
				}
				tempObj.getMSSOrderCrate().add(MSSK);
			}
			mssOrder.setMSSOrderCrates(tempObj);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			ArrayOfMSSCustomerContact newcontact=new ArrayOfMSSCustomerContact();
			for (MssDTO mssDTO : customerContact) {
				MSSCustomerContact MCC=new MSSCustomerContact();
				if((transportTypeRadio.equalsIgnoreCase("O") || transportTypeRadio.equalsIgnoreCase("B")) && mssDTO.getSoEmail()!=null)
				{					
					MCC.setEmailAddress(mssDTO.getSoEmail().toString());
				}
				if((transportTypeRadio.equalsIgnoreCase("D")) && mssDTO.getSoDestinationemail()!=null ){
					MCC.setEmailAddress(mssDTO.getSoDestinationemail().toString());
				}
				newcontact.getMSSCustomerContact().add(MCC);
			}
			mssOrder.setMssCustomerContact(newcontact);
		try {
			//////Origin Services....
			ArrayOfMSSOrderService orgtemp = new ArrayOfMSSOrderService();
			for (MssDTO mssDTO : objorigin) {
				MSSOrderService MSSO = new MSSOrderService();
				MSSO.setServiceNumber(Integer.parseInt(mssDTO
						.getServiceNumber().toString()));
				MSSO.setCOD(Boolean.parseBoolean(mssDTO.getOricod().toString()));
				if(mssDTO.getOriappr().equals(true)){
					MSSO.setPendingAuthorization(false);	
				}else{
					MSSO.setPendingAuthorization(true);
				}
				orgtemp.getMSSOrderService().add(MSSO);
			}
			mssOrder.setMSSOrderOriginServices(orgtemp);
			
			/////Destination Services....
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			ArrayOfMSSOrderService desttemp = new ArrayOfMSSOrderService();
			if(objdestination!=null){
			for (MssDTO mssDTO : objdestination) {
				MSSOrderService MSSO = new MSSOrderService();
				MSSO.setServiceNumber(Integer.parseInt(mssDTO
						.getDestServiceNumber().toString()));
				MSSO.setCOD(Boolean.parseBoolean(mssDTO.getDestinationCod()
						.toString()));
				if(mssDTO.getDestinationAppr().equals(true)){
					MSSO.setPendingAuthorization(false);
				}else{
					MSSO.setPendingAuthorization(true);
				}
				
				desttemp.getMSSOrderService().add(MSSO);
			}
			mssOrder.setMSSOrderDestinationServices(desttemp);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		try {
			////origin Notes
			ArrayOfString orgNote = new ArrayOfString();
			for (MssDTO mssDTO : originNotesList) {
				orgNote.getString().add(mssDTO.getNote().toString());
			}
			mssOrder.setMSSOrderOriginNotes(orgNote);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			////Destination Notes
			ArrayOfString destNote = new ArrayOfString();
			if(destinationNotesList!=null){
			for (MssDTO mssDTO : destinationNotesList) {
				destNote.getString().add(mssDTO.getNote().toString());
			}
			mssOrder.setMSSOrderDestinationNotes(destNote);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			///origin Phone...
			ArrayOfMSSOrderPhoneNumber orderPhone = new ArrayOfMSSOrderPhoneNumber();
			for (MssDTO mssDTO : originPhoneList) {
				MSSOrderPhoneNumber MssPhone = new MSSOrderPhoneNumber();
				MssPhone.setMSSPhoneNumberTypeID(Integer.parseInt(mssDTO.getPhoneTypeId().toString()));
				MssPhone.setPhoneNumber(mssDTO.getHomePhone().toString());
				orderPhone.getMSSOrderPhoneNumber().add(MssPhone);
			}
			mssOrder.setMSSOrderOriginPhoneNumbers(orderPhone);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			///Destination Phone
			ArrayOfMSSOrderPhoneNumber destPhone = new ArrayOfMSSOrderPhoneNumber();
			if(destinationPhoneList!=null){
			for (MssDTO mssDTO : destinationPhoneList) {
				MSSOrderPhoneNumber MssPhone1 = new MSSOrderPhoneNumber();
				MssPhone1.setMSSPhoneNumberTypeID(Integer.parseInt(mssDTO.getPhoneTypeId().toString()));
				MssPhone1.setPhoneNumber(mssDTO.getDestinationHomePhone()
						.toString());
				destPhone.getMSSOrderPhoneNumber().add(MssPhone1);
			}
			mssOrder.setMSSOrderDestinationPhoneNumbers(destPhone);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	/*logger.getAllAppenders();
	logger.getAppender("AXIS");
	logger.debug("Mss Order Object "+mssOrder);
	logger.warn(mssOrder);*/
	return mssOrder;
	}

public XMLGregorianCalendar getXMLdate(Object date) {
	  if(date!=null){
	   GregorianCalendar gregory = new GregorianCalendar();
	   gregory.setTime((Date) date);
	   try {
	    return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
	   } catch (DatatypeConfigurationException e) {
	    //logger.info(e.getMessage() +"---"+(e.getCause()!= null?e.getCause().getMessage():""));
	    return null;
	   }
	  }
	  return null;
	 }
}