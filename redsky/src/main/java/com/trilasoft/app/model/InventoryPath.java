package com.trilasoft.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="inventorypath")
public class InventoryPath {
private Long id;
private Long inventoryDataId;
private String path;
private String corpID;
private Long accessInfoId;
private String accessInfoType;
private Long inventoryPackingId;
private Long workticketInventoryDataId;
private Long roomId;
private Long locationId;
private Long networkSynchedId;


@Override
public String toString() {
	return new ToStringBuilder(this).append("id", id)
			.append("inventoryDataId", inventoryDataId)
			.append("path", path).append("corpID", corpID)
			.append("accessInfoId", accessInfoId)
			.append("accessInfoType", accessInfoType)
			.append("inventoryPackingId", inventoryPackingId)
			.append("workticketInventoryDataId", workticketInventoryDataId)
			.append("roomId", roomId).append("locationId", locationId)
			.append("networkSynchedId", networkSynchedId).toString();
}
@Override
public boolean equals(final Object other) {
	if (!(other instanceof InventoryPath))
		return false;
	InventoryPath castOther = (InventoryPath) other;
	return new EqualsBuilder()
			.append(id, castOther.id)
			.append(inventoryDataId, castOther.inventoryDataId)
			.append(path, castOther.path)
			.append(corpID, castOther.corpID)
			.append(accessInfoId, castOther.accessInfoId)
			.append(accessInfoType, castOther.accessInfoType)
			.append(inventoryPackingId, castOther.inventoryPackingId)
			.append(workticketInventoryDataId,
					castOther.workticketInventoryDataId)
			.append(roomId, castOther.roomId)
			.append(locationId, castOther.locationId)
			.append(networkSynchedId, castOther.networkSynchedId)
			.isEquals();
}
@Override
public int hashCode() {
	return new HashCodeBuilder().append(id).append(inventoryDataId)
			.append(path).append(corpID).append(accessInfoId)
			.append(accessInfoType).append(inventoryPackingId)
			.append(workticketInventoryDataId).append(roomId)
			.append(locationId).append(networkSynchedId).toHashCode();
}
@Id @GeneratedValue(strategy = GenerationType.AUTO)
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
@Column
public Long getInventoryDataId() {
	return inventoryDataId;
}
public void setInventoryDataId(Long inventoryDataId) {
	this.inventoryDataId = inventoryDataId;
}
@Column
public String getPath() {
	return path;
}
public void setPath(String path) {
	this.path = path;
}
@Column
public String getCorpID() {
	return corpID;
}
public void setCorpID(String corpID) {
	this.corpID = corpID;
}
@Column
public Long getAccessInfoId() {
	return accessInfoId;
}
public void setAccessInfoId(Long accessInfoId) {
	this.accessInfoId = accessInfoId;
}
@Column
public String getAccessInfoType() {
	return accessInfoType;
}
public void setAccessInfoType(String accessInfoType) {
	this.accessInfoType = accessInfoType;
}
@Column
public Long getInventoryPackingId() {
	return inventoryPackingId;
}
public void setInventoryPackingId(Long inventoryPackingId) {
	this.inventoryPackingId = inventoryPackingId;
}
/**
 * @return the workticketInventoryDataId
 */
@Column
public Long getWorkticketInventoryDataId() {
	return workticketInventoryDataId;
}
/**
 * @param workticketInventoryDataId the workticketInventoryDataId to set
 */
public void setWorkticketInventoryDataId(Long workticketInventoryDataId) {
	this.workticketInventoryDataId = workticketInventoryDataId;
}
/**
 * @return the roomId
 */
@Column
public Long getRoomId() {
	return roomId;
}
/**
 * @param roomId the roomId to set
 */
public void setRoomId(Long roomId) {
	this.roomId = roomId;
}
/**
 * @return the locationId
 */
@Column
public Long getLocationId() {
	return locationId;
}
/**
 * @param locationId the locationId to set
 */

public void setLocationId(Long locationId) {
	this.locationId = locationId;
}
@Column
public Long getNetworkSynchedId() {
	return networkSynchedId;
}
public void setNetworkSynchedId(Long networkSynchedId) {
	this.networkSynchedId = networkSynchedId;
}

}
