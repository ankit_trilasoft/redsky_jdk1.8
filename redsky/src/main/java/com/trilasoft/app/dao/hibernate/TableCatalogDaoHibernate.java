package com.trilasoft.app.dao.hibernate;



import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.TableCatalogDao;
import com.trilasoft.app.model.TableCatalog;


public class TableCatalogDaoHibernate extends GenericDaoHibernate<TableCatalog, Long> implements TableCatalogDao {

	public TableCatalogDaoHibernate() {
		super(TableCatalog.class);
		
	}
	public List getTableListValue(String tableN,String tableD){
		return getHibernateTemplate().find("from TableCatalog where tableName like '"+tableN+"%' and tableDescription like '"+tableD+"%'");
	}
	public List getData(String tableName,String sessionCorpID){
		return this.getSession().createSQLQuery("select * from datacatalog where lower(tableName)='"+tableName.trim().toLowerCase()+"' and corpID='"+sessionCorpID+"'").list();
   }
}
