package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.GlCodeRateGrid;

public interface GlCodeRateGridDao  extends GenericDao<GlCodeRateGrid, Long> {
public List getAllRateGridList(String sessionCorpID,String costId,String jobType,String routing1,String recGl,String payGl);
}
