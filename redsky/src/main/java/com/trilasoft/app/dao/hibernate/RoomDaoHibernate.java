/**
 * 
 */
package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.RoomDao;
import com.trilasoft.app.model.MoversDTO;
import com.trilasoft.app.model.Room;

/**
 * @author GVerma
 *
 */
public class RoomDaoHibernate extends GenericDaoHibernate<Room, Long> implements RoomDao {

	public RoomDaoHibernate() {
		super(Room.class);
		// TODO Auto-generated constructor stub
	}

	public List getAllRoom(String sessionCorpID, String shipno) {

		List <MoversDTO> movValueList = new ArrayList<MoversDTO>(); 
		MoversDTO moverDto = null;
		List list = getSession()
		.createSQLQuery("select " +
				"l.locationType ," + //0
				"l.workTicket," + //1
				"r.roomDesc," + //2
				"r.floor ," + //3
				"if(r.hasDamage,'Y','N') as damage ," + //4
				"r.comment ," + //5
				"ip.roomId " + //6
				"from room r left outer join inventorypath ip on ip.roomId=r.id and ip.corpid='"+sessionCorpID+"' " +
				",inventorylocation l where r.corpId='"+sessionCorpID+"' and r.shipNumber='"+shipno+"' and l.id=r.locationId " +
				" and l.shipNumber='"+shipno+"' and l.corpId='"+sessionCorpID+"'  ") 
				.addScalar("l.locationType", Hibernate.STRING)
				.addScalar("l.workTicket", Hibernate.STRING)
				.addScalar("r.roomDesc", Hibernate.STRING)
				.addScalar("r.floor", Hibernate.STRING)
				.addScalar("damage", Hibernate.STRING)
				.addScalar("r.comment", Hibernate.STRING)
				.addScalar("ip.roomId", Hibernate.LONG)
				.list(); 
		Iterator it=list.iterator();
		  while(it.hasNext()) {
			  Object [] row=(Object[])it.next();
			  moverDto = new MoversDTO();
			  moverDto.setLocation(row[0]!=null  ? row[0].toString() :"" );
			  moverDto.setTicket(row[1]!=null  ? row[1].toString() :"" );
			  moverDto.setRooms(row[2]!=null  ? row[2].toString() :"");
			  moverDto.setFloors(row[3]!=null  ? row[3].toString() :"");
			  moverDto.setHasDamage(row[4]!=null ? row[4].toString() :"");
			  moverDto.setComments(row[5]!=null ? row[5].toString() :"");
			  moverDto.setPhotos(row[6]!=null ? row[6].toString() :"");
			  movValueList.add(moverDto);
		  }
		  return movValueList;
	
	}
	
	public List checkRoom(String corpID, int roomCode, String shipNumber){
		return getHibernateTemplate().find(" from Room where corpId='"+corpID+"' and room='"+roomCode+"' and shipNumber='"+shipNumber+"' ");
	}

}
