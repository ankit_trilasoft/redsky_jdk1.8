
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mss1.webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mss1.webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetQuotePricesResponse }
     * 
     */
    public GetQuotePricesResponse createGetQuotePricesResponse() {
        return new GetQuotePricesResponse();
    }

    /**
     * Create an instance of {@link GetPhoneNumberTypes }
     * 
     */
    public GetPhoneNumberTypes createGetPhoneNumberTypes() {
        return new GetPhoneNumberTypes();
    }

    /**
     * Create an instance of {@link OrderNote }
     * 
     */
    public OrderNote createOrderNote() {
        return new OrderNote();
    }

    /**
     * Create an instance of {@link GetOrderPricesResponse }
     * 
     */
    public GetOrderPricesResponse createGetOrderPricesResponse() {
        return new GetOrderPricesResponse();
    }

    /**
     * Create an instance of {@link GetOriginServicesResponse }
     * 
     */
    public GetOriginServicesResponse createGetOriginServicesResponse() {
        return new GetOriginServicesResponse();
    }

    /**
     * Create an instance of {@link ValidateLoginResponse }
     * 
     */
    public ValidateLoginResponse createValidateLoginResponse() {
        return new ValidateLoginResponse();
    }

    /**
     * Create an instance of {@link MSSBillTo }
     * 
     */
    public MSSBillTo createMSSBillTo() {
        return new MSSBillTo();
    }

    /**
     * Create an instance of {@link PlaceOrder }
     * 
     */
    public PlaceOrder createPlaceOrder() {
        return new PlaceOrder();
    }

    /**
     * Create an instance of {@link ArrayOfOrderNote }
     * 
     */
    public ArrayOfOrderNote createArrayOfOrderNote() {
        return new ArrayOfOrderNote();
    }

    /**
     * Create an instance of {@link GetOrderURLByPONum }
     * 
     */
    public GetOrderURLByPONum createGetOrderURLByPONum() {
        return new GetOrderURLByPONum();
    }

    /**
     * Create an instance of {@link GetQuotePdf2 }
     * 
     */
    public GetQuotePdf2 createGetQuotePdf2() {
        return new GetQuotePdf2();
    }

    /**
     * Create an instance of {@link GetQuoteCo }
     * 
     */
    public GetQuoteCo createGetQuoteCo() {
        return new GetQuoteCo();
    }

    /**
     * Create an instance of {@link ArrayOfMSSOrderService }
     * 
     */
    public ArrayOfMSSOrderService createArrayOfMSSOrderService() {
        return new ArrayOfMSSOrderService();
    }

    /**
     * Create an instance of {@link GetQuotePdf2Response }
     * 
     */
    public GetQuotePdf2Response createGetQuotePdf2Response() {
        return new GetQuotePdf2Response();
    }

    /**
     * Create an instance of {@link ArrayOfMSSPricedCrate }
     * 
     */
    public ArrayOfMSSPricedCrate createArrayOfMSSPricedCrate() {
        return new ArrayOfMSSPricedCrate();
    }

    /**
     * Create an instance of {@link ArrayOfMSSCustomerContact }
     * 
     */
    public ArrayOfMSSCustomerContact createArrayOfMSSCustomerContact() {
        return new ArrayOfMSSCustomerContact();
    }

    /**
     * Create an instance of {@link MSSPricedService }
     * 
     */
    public MSSPricedService createMSSPricedService() {
        return new MSSPricedService();
    }

    /**
     * Create an instance of {@link GetReadOnlyOrderURLResponse }
     * 
     */
    public GetReadOnlyOrderURLResponse createGetReadOnlyOrderURLResponse() {
        return new GetReadOnlyOrderURLResponse();
    }

    /**
     * Create an instance of {@link MSSPricedCrate }
     * 
     */
    public MSSPricedCrate createMSSPricedCrate() {
        return new MSSPricedCrate();
    }

    /**
     * Create an instance of {@link MSSService }
     * 
     */
    public MSSService createMSSService() {
        return new MSSService();
    }

    /**
     * Create an instance of {@link GetOrderPrices }
     * 
     */
    public GetOrderPrices createGetOrderPrices() {
        return new GetOrderPrices();
    }

    /**
     * Create an instance of {@link MSSOrderPhoneNumber }
     * 
     */
    public MSSOrderPhoneNumber createMSSOrderPhoneNumber() {
        return new MSSOrderPhoneNumber();
    }

    /**
     * Create an instance of {@link PlaceOrderCoResponse }
     * 
     */
    public PlaceOrderCoResponse createPlaceOrderCoResponse() {
        return new PlaceOrderCoResponse();
    }

    /**
     * Create an instance of {@link GetDestinationServices }
     * 
     */
    public GetDestinationServices createGetDestinationServices() {
        return new GetDestinationServices();
    }

    /**
     * Create an instance of {@link ValidateLogin }
     * 
     */
    public ValidateLogin createValidateLogin() {
        return new ValidateLogin();
    }

    /**
     * Create an instance of {@link MSSPhoneNumberType }
     * 
     */
    public MSSPhoneNumberType createMSSPhoneNumberType() {
        return new MSSPhoneNumberType();
    }

    /**
     * Create an instance of {@link ArrayOfMSSPricedService }
     * 
     */
    public ArrayOfMSSPricedService createArrayOfMSSPricedService() {
        return new ArrayOfMSSPricedService();
    }

    /**
     * Create an instance of {@link ArrayOfMSSCustomerAffiliate }
     * 
     */
    public ArrayOfMSSCustomerAffiliate createArrayOfMSSCustomerAffiliate() {
        return new ArrayOfMSSCustomerAffiliate();
    }

    /**
     * Create an instance of {@link MSSOrder }
     * 
     */
    public MSSOrder createMSSOrder() {
        return new MSSOrder();
    }

    /**
     * Create an instance of {@link GetMSSDateTimeStringResponse }
     * 
     */
    public GetMSSDateTimeStringResponse createGetMSSDateTimeStringResponse() {
        return new GetMSSDateTimeStringResponse();
    }

    /**
     * Create an instance of {@link GetServiceCategoryTypesResponse }
     * 
     */
    public GetServiceCategoryTypesResponse createGetServiceCategoryTypesResponse() {
        return new GetServiceCategoryTypesResponse();
    }

    /**
     * Create an instance of {@link GetCustomerMobileAffiliates }
     * 
     */
    public GetCustomerMobileAffiliates createGetCustomerMobileAffiliates() {
        return new GetCustomerMobileAffiliates();
    }

    /**
     * Create an instance of {@link GetPhoneNumberTypesResponse }
     * 
     */
    public GetPhoneNumberTypesResponse createGetPhoneNumberTypesResponse() {
        return new GetPhoneNumberTypesResponse();
    }

    /**
     * Create an instance of {@link GetReadOnlyOrderURL }
     * 
     */
    public GetReadOnlyOrderURL createGetReadOnlyOrderURL() {
        return new GetReadOnlyOrderURL();
    }

    /**
     * Create an instance of {@link GetQuoteCoResponse }
     * 
     */
    public GetQuoteCoResponse createGetQuoteCoResponse() {
        return new GetQuoteCoResponse();
    }

    /**
     * Create an instance of {@link GetCustomerAffiliatesResponse }
     * 
     */
    public GetCustomerAffiliatesResponse createGetCustomerAffiliatesResponse() {
        return new GetCustomerAffiliatesResponse();
    }

    /**
     * Create an instance of {@link GetCustomerMobileAffiliatesResponse }
     * 
     */
    public GetCustomerMobileAffiliatesResponse createGetCustomerMobileAffiliatesResponse() {
        return new GetCustomerMobileAffiliatesResponse();
    }

    /**
     * Create an instance of {@link PlaceOrderResponse }
     * 
     */
    public PlaceOrderResponse createPlaceOrderResponse() {
        return new PlaceOrderResponse();
    }

    /**
     * Create an instance of {@link GetQuote }
     * 
     */
    public GetQuote createGetQuote() {
        return new GetQuote();
    }

    /**
     * Create an instance of {@link MSSCustomerAffiliate }
     * 
     */
    public MSSCustomerAffiliate createMSSCustomerAffiliate() {
        return new MSSCustomerAffiliate();
    }

    /**
     * Create an instance of {@link GetOriginServices }
     * 
     */
    public GetOriginServices createGetOriginServices() {
        return new GetOriginServices();
    }

    /**
     * Create an instance of {@link MSSOrderService }
     * 
     */
    public MSSOrderService createMSSOrderService() {
        return new MSSOrderService();
    }

    /**
     * Create an instance of {@link MSSOrderCrate }
     * 
     */
    public MSSOrderCrate createMSSOrderCrate() {
        return new MSSOrderCrate();
    }

    /**
     * Create an instance of {@link ArrayOfMSSBillTo }
     * 
     */
    public ArrayOfMSSBillTo createArrayOfMSSBillTo() {
        return new ArrayOfMSSBillTo();
    }

    /**
     * Create an instance of {@link GetQuotePdfCo }
     * 
     */
    public GetQuotePdfCo createGetQuotePdfCo() {
        return new GetQuotePdfCo();
    }

    /**
     * Create an instance of {@link GetOrderNotes }
     * 
     */
    public GetOrderNotes createGetOrderNotes() {
        return new GetOrderNotes();
    }

    /**
     * Create an instance of {@link PlaceOrderCo }
     * 
     */
    public PlaceOrderCo createPlaceOrderCo() {
        return new PlaceOrderCo();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link GetMSSDateTimeString }
     * 
     */
    public GetMSSDateTimeString createGetMSSDateTimeString() {
        return new GetMSSDateTimeString();
    }

    /**
     * Create an instance of {@link MSSPricedOrder }
     * 
     */
    public MSSPricedOrder createMSSPricedOrder() {
        return new MSSPricedOrder();
    }

    /**
     * Create an instance of {@link GetQuotePdfResponse }
     * 
     */
    public GetQuotePdfResponse createGetQuotePdfResponse() {
        return new GetQuotePdfResponse();
    }

    /**
     * Create an instance of {@link GetCustomerAffiliates }
     * 
     */
    public GetCustomerAffiliates createGetCustomerAffiliates() {
        return new GetCustomerAffiliates();
    }

    /**
     * Create an instance of {@link ArrayOfMSSOrderCrate }
     * 
     */
    public ArrayOfMSSOrderCrate createArrayOfMSSOrderCrate() {
        return new ArrayOfMSSOrderCrate();
    }

    /**
     * Create an instance of {@link GetOrderURLResponse }
     * 
     */
    public GetOrderURLResponse createGetOrderURLResponse() {
        return new GetOrderURLResponse();
    }

    /**
     * Create an instance of {@link GetCustomerBillTosResponse }
     * 
     */
    public GetCustomerBillTosResponse createGetCustomerBillTosResponse() {
        return new GetCustomerBillTosResponse();
    }

    /**
     * Create an instance of {@link GetDestinationServicesResponse }
     * 
     */
    public GetDestinationServicesResponse createGetDestinationServicesResponse() {
        return new GetDestinationServicesResponse();
    }

    /**
     * Create an instance of {@link GetOrderURLByPONumResponse }
     * 
     */
    public GetOrderURLByPONumResponse createGetOrderURLByPONumResponse() {
        return new GetOrderURLByPONumResponse();
    }

    /**
     * Create an instance of {@link MSSServiceCategoryType }
     * 
     */
    public MSSServiceCategoryType createMSSServiceCategoryType() {
        return new MSSServiceCategoryType();
    }

    /**
     * Create an instance of {@link GetCustomerContactsResponse }
     * 
     */
    public GetCustomerContactsResponse createGetCustomerContactsResponse() {
        return new GetCustomerContactsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMSSServiceCategoryType }
     * 
     */
    public ArrayOfMSSServiceCategoryType createArrayOfMSSServiceCategoryType() {
        return new ArrayOfMSSServiceCategoryType();
    }

    /**
     * Create an instance of {@link ArrayOfMSSOrderPhoneNumber }
     * 
     */
    public ArrayOfMSSOrderPhoneNumber createArrayOfMSSOrderPhoneNumber() {
        return new ArrayOfMSSOrderPhoneNumber();
    }

    /**
     * Create an instance of {@link GetCustomerContacts }
     * 
     */
    public GetCustomerContacts createGetCustomerContacts() {
        return new GetCustomerContacts();
    }

    /**
     * Create an instance of {@link GetServiceCategoryTypes }
     * 
     */
    public GetServiceCategoryTypes createGetServiceCategoryTypes() {
        return new GetServiceCategoryTypes();
    }

    /**
     * Create an instance of {@link ArrayOfMSSService }
     * 
     */
    public ArrayOfMSSService createArrayOfMSSService() {
        return new ArrayOfMSSService();
    }

    /**
     * Create an instance of {@link GetQuotePdf }
     * 
     */
    public GetQuotePdf createGetQuotePdf() {
        return new GetQuotePdf();
    }

    /**
     * Create an instance of {@link MSSCustomerContact }
     * 
     */
    public MSSCustomerContact createMSSCustomerContact() {
        return new MSSCustomerContact();
    }

    /**
     * Create an instance of {@link GetOrderNotesResponse }
     * 
     */
    public GetOrderNotesResponse createGetOrderNotesResponse() {
        return new GetOrderNotesResponse();
    }

    /**
     * Create an instance of {@link GetCustomerBillTos }
     * 
     */
    public GetCustomerBillTos createGetCustomerBillTos() {
        return new GetCustomerBillTos();
    }

    /**
     * Create an instance of {@link GetQuoteResponse }
     * 
     */
    public GetQuoteResponse createGetQuoteResponse() {
        return new GetQuoteResponse();
    }

    /**
     * Create an instance of {@link GetOrderURL }
     * 
     */
    public GetOrderURL createGetOrderURL() {
        return new GetOrderURL();
    }

    /**
     * Create an instance of {@link GetQuotePrices }
     * 
     */
    public GetQuotePrices createGetQuotePrices() {
        return new GetQuotePrices();
    }

    /**
     * Create an instance of {@link ArrayOfMSSPhoneNumberType }
     * 
     */
    public ArrayOfMSSPhoneNumberType createArrayOfMSSPhoneNumberType() {
        return new ArrayOfMSSPhoneNumberType();
    }

    /**
     * Create an instance of {@link GetQuotePdfCoResponse }
     * 
     */
    public GetQuotePdfCoResponse createGetQuotePdfCoResponse() {
        return new GetQuotePdfCoResponse();
    }

}
