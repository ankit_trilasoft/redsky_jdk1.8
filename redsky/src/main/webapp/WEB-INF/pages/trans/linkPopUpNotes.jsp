<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" height="250" width="747" style="!margin-top:5px;">
	<tbody>
	<tr class="subcontent-tab"><td colspan="20"><font style="padding-left:10px ">Notes</font></td></tr>
		<tr>
			<td style="margin:0; ">
				<table cellspacing="0" border="0" cellpadding="1" style="margin:0;padding: 0px;">
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key="notes.notesId"/></td>
						<td align="left" class="listwhitetext" > <s:textfield name="notes.notesId" id="editorValue" value="${trackingStatus.shipNumber}" cssClass="input-text" cssStyle="width:104px" maxlength="15" readonly="true" /></td>
						
						<td align="right" class="listwhitetext" width=""><fmt:message key="notes.noteStatus"/></td>
						<td align="left" class="listwhitetext" colspan="5"> 		
						<s:select name="notes.noteStatus" list="%{notestatus}" value="${notes.noteStatus}" cssClass="list-menu" cssStyle="width:105px" headerKey="" headerValue=""/>
						</td>
						
					</tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:42px;"><fmt:message key="notes.noteType"/><font color="red" size="2">*</font></td>
						<td align="left" class="listwhitetext" width="75px"> 
						<s:textfield name="notes.noteType" id="notes.noteType" value="Service Order" cssClass="input-text" cssStyle="width:104px" maxlength="15" readonly="true" />
						<%--<s:select name="notes.noteType" list="%{notetype}" value="${notes.noteType}" cssClass="list-menu" cssStyle="width:105px;height:20px"/>  --%> 
						</td>
							
						<td align="right" class="listwhitetext" width="55px" style="!padding:0px"><fmt:message key="notes.noteSubType"/></td>
						<td align="left" class="listwhitetext" > <s:select name="notes.noteSubType" list="%{notesubtype}" value="${notes.noteSubType}" cssStyle="width:105px;"  headerKey="" headerValue="" cssClass="list-menu" /></td>
					</tr>
					 
				</table>
			</td>
			  
			<td id="common" width="" align="right">
				<fieldset style="!padding-top:4px; !height:55px;margin:0px;">
					<legend>Display On</legend>
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="3" width="100%" style="margin-bottom: 0px;">
						<tr>					
							<td width="8px"style="padding:0px"><s:checkbox key="notes.displaySurvey" value="${isSurveyFlag}" fieldValue="true" onclick="changeStatus();"/></td>
							<td align="left" class="listwhitetext" ><fmt:message key='notes.displaySurvey'/></td>					
							<td width="8px" style="padding:0px"><s:checkbox key="notes.displayPortal" value="${isPortalFlag}" fieldValue="true" onclick="changeStatus();"/></td>
							<td align="left" class="listwhitetext" ><fmt:message key='notes.displayPortal'/></td>
						</tr>
					
						<tr>	
							<td width="8px" style="padding:0px"><s:checkbox key="notes.displayTicket" value="${isTicketFlag}" fieldValue="true" onclick="changeStatus();"/></td>
							<td align="left" class="listwhitetext" ><fmt:message key='notes.displayTicket'/></td>				
							<td width="8px" style="padding:0px"><s:checkbox key="notes.accPortal" value="${isAccPortal}" fieldValue="true"/></td>
							<td align="left" width="80px" class="listwhitetext" >Account&nbsp;Portal&nbsp;&nbsp;</td>				
							<td width="8px" style="padding:0px"><s:checkbox key="notes.partnerPortal" value="${isPartnerPortal}" fieldValue="true"/></td>
							<td align="left" width="80px" class="listwhitetext" >Partner&nbsp;Portal&nbsp;&nbsp;</td>
						</tr>				
					</table>
				</fieldset>	
				
				<fieldset style=" !padding-top:2px; !height:55px;">
					<legend>Network Access</legend>
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="3" width="100%" style="margin-bottom: 0px;">							
					<tr>
				<c:if test="${originAgentFlag=='OA'}">				
					<td width="8px" style="padding:0px"><s:checkbox name="notes.originAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
			    <c:if test="${originAgentFlag==''}">
			    	<td width="8px" style="padding:0px"><s:checkbox name="notes.originAgent" value="${isOriginAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagOA(this,'OA');"/></td>
				</c:if>
				<td align="left" class="listwhitetext" >Origin&nbsp;Agent</td>
												
				<c:if test="${destAgentFlag=='DA'}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.destinationAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${destAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.destinationAgent" value="${isDestAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagDA(this,'DA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Dest.&nbsp;Agent</td>
													
				<c:if test="${bookingAgentFlag=='BA'}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.bookingAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${bookingAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.bookingAgent" value="${isBookingAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagBA(this,'BA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Booking&nbsp;Agent</td>				
				</tr>
				
				<tr>
				<c:if test="${subOriginAgentFlag=='SOA'}">	
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subOriginAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${subOriginAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subOriginAgent" value="${isSubOriginAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagSOA(this,'SOA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Sub&nbsp;Origin&nbsp;Agent</td>
					
				<c:if test="${subDestAgentFlag=='SDA'}">			
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subDestinationAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();" /></td>
				</c:if>
				<c:if test="${subDestAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subDestinationAgent" value="${isSubDestAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagSDA(this,'SDA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Sub&nbsp;Dest.&nbsp;Agent</td>	
							
				<c:if test="${networkAgentFlag=='NA'}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.networkAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${networkAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.networkAgent" value="${isNetworkAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagNA(this,'NA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Network&nbsp;Agent</td>
				</tr>
					
					</table>
				</fieldset>	
			</td>
			
		</tr>
		
		<tr>
			<td colspan="10">
				<table align="center">
					<tr>
						<td align="right" class="listwhitetext" style="width:50px; !width:55px; " ><fmt:message key="notes.subject"/></td>
						<td align="left" class="listwhitetext"  style="padding-bottom: 2px;"><s:textfield cssClass="list-menu"  cssStyle="height:16px;width:500px;!width:625px;" id="subject" name="notes.subject" maxlength="100" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext" valign="top"><fmt:message key="notes.note"/></td>
						<td align="left" class="listwhitetext" width="20px"><s:textarea name="notes.note" id="noteTextArea" value="${notes.note}" cols="93" rows="4"  cssClass="textarea" /></td>														
					</tr>
				</table>
			</td>
		</tr>
		
	</tbody>
</table>

			
