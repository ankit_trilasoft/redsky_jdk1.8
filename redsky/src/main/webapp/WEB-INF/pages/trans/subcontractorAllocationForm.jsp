<!-- *File Name:subcontractorCharges.jsp 
 * Created By:Ashish Mishra 
 * Created Date:08-Aug-2008
 * Summary: This jsp is created for showing Detail Subcontractor Charges.
 -->
<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
    <title><fmt:message key="subcontractorCharges.title"/></title>   
    <meta name="heading" content="<fmt:message key='subcontractorCharges.heading'/>"/>   
    
    
<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
			var cal = new CalendarPopup(); 
			cal.showYearNavigation(); 
			cal.showYearNavigationInput();  
	</script>
	<script language="JavaScript">
	 function changeStatus(){
	document.forms['subcontractorChargesForm'].elements['formStatus'].value = '1';
}
	
	function autoSave(clickType){
	if ('${autoSavePrompt}' == 'No'){	
	document.forms['subcontractorChargesForm'].action = 'saveSubcontractorCharges!saveOnTabChange.html';
	document.forms['subcontractorChargesForm'].submit();
	
	}else{
	if(!(clickType == 'save')){
	var id1 = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.id'].value;
	
	if (document.forms['subcontractorChargesForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the subcontractorCharges Form");
		if(agree){
			document.forms['subcontractorChargesForm'].action = 'saveSubcontractorCharges!saveOnTabChange.html';
			document.forms['subcontractorChargesForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['subcontractorChargesForm'].elements['gotoPageString'].value == 'gototab.auditList'){
				location.href = 'subcontractorChargesList.html';
				}
			
					
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['auditSetupForm'].elements['gotoPageString'].value == 'gototab.auditList'){
				location.href = 'subcontractorChargesList.html';
				}
			
			
	}
	}
}
}
}
	
	
	
	function chkSelect()
	{
	if (checkFloatWithArth('subcontractorChargesForm','subcontractorCharges.amount','Invalid data in amount') == false)
           {
              document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].value='0';
              
              return false
           }
	}
	function chkSelectSave()
	{
	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
	if (checkFloatWithArth('subcontractorChargesForm','subcontractorCharges.amount','Invalid data in amount') == false)
           {
              document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].value='0';
              
              return false
           }
	}
	</script>
	<script language="JavaScript">
	
	function openAccountPopWindow(){
	var persId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
	javascript:openWindow('carrierCodeSub.html?&partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=subcontractorCharges.personId');
	}
	
	

	function checkRegNo()
	{
	var regNo = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value;
	if(regNo!="")
	{
    //alert(vendorId);
    var url="checkSubcontractRegNo.html?ajax=1&decorator=simple&popup=true&regNo=" + encodeURI(regNo);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    
	}
	}
	
	function checkRegNoSave()
	{
	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
	var regNo = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value;
	if(regNo!="")
	{
    //alert(vendorId);
    var url="checkSubcontractRegNo.html?ajax=1&decorator=simple&popup=true&regNo=" + encodeURI(regNo);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    
	}
	}
        function handleHttpResponse2()
        {
			//alert("a");
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                //alert("a"+results);
                results = results.trim();
                //alert(results);
                if(results.length>0)
                {
                 	//alert("a"); 
                  document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].select(); 					 				
                  
                
                 }else{
                 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
                     alert("Registration# does not exist in ServiceOrder");
                     document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value="";
					 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].select();
					 return false;
                      }
             }
        }
        
     function checkPerId()
	{
    var perId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
    if(perId!=""){
    //alert(vendorId);
    var url="matchCarrierCode.html?ajax=1&decorator=simple&popup=true&perId=" + encodeURI(perId);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
    
	}
	}
	
	
	function checkPerIdSave()
	{
	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
    var perId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
    if(perId!=""){
    //alert(vendorId);
    var url="matchCarrierCode.html?ajax=1&decorator=simple&popup=true&perId=" + encodeURI(perId);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
    
	}
	}
	
     function handleHttpResponse4()
        {
             
             if (http4.readyState == 4)
             {
              var results = http4.responseText
                results = results.trim();
                if(results.length>0)
                {
                 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].select(); 					 				
                  
                 }else{
                     document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
                     alert("Person ID does not exist ");
                     document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value="";
					 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].select();
                      return false;
                      }
             }
        }        
        
  function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  var http2 = getHTTPObject();
  var http4 = getHTTPObject();
  
  
  function checkPersionId()
  {

   var personType= document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].value;
   
   var personID=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
   
   if (personType!='A' && personID=='')
   {
   
   	alert('Please fill personId')
   	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].focus;
   	return false;
   }
  }
 
 function pick() { 
  		window.close();
	}
	
function displayRow()
{
	//alert('hi');
	var notRed = document.getElementById("captionRow1");
	var red = document.getElementById("captionRow2");
	var type=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].value;
	if(type=='A' || type=='')
	{
	red.style.display = 'none';
	notRed.style.display = '';
	}
	if(type!='A' && type!='')
	{
		red.style.display = '';
		notRed.style.display = 'none';
	}

}
	
 
	</script>
	
</head> 
<s:form id="subcontractorChargesForm" action="saveChargeAllocationSub.html?&btntype=yes" method="post" validate="true"> 
     <s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
    <c:set var="btntype"  value="<%=request.getParameter("btntype") %>"/>
    <s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 	<s:hidden  name="subcontractorCharges.id"/>
 	<s:hidden  name="subcontractorCharges.vanLineNumber"/>  
 	<s:hidden name="dueFormAmount" />
 	
 	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
    <s:hidden name="formStatus" value=""/>
    
    <c:if test="${errorValidator == 'errorMethod' && validateFormNav != 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.subcontractList' }">
    <c:redirect url="/saveSubcontractorCharges.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
    
   <c:if test="${validateFormNav == 'OK' && errorValidator != 'errorMethod'} " >
<c:choose>
<c:when test="${gotoPageString == 'gototab.subcontractList' }">
    <c:redirect url="/subcontractorChargesList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
    
   
 	<div id="Layer1" style="width:721px" onkeydown="changeStatus()">
<div id="newmnav">
	<ul> 
	<li id="newmnav1" style="background:#FFF "> <a  class="current"><span>Subcontractor Charges Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	<li><a href="chargeAllocation.html?id=${subcontractorCharges.parentId}&decorator=popup&popup=true"><span>Subcontractor Charges List</span></a></li> 
</ul>
</div>
	
<div class="spn">&nbsp;</div> 
    <table class="mainDetailTable" cellspacing="0" cellpadding="1" border="0" style="width:721px"> 
  <tr>
    <td align="right" class="listwhitetext" style="padding-right:10px;" width="170px" ><fmt:message key="subcontractorCharges.branch"/></td>
    <td align="left" height="30px" class="listwhitetext" width="20"><s:textfield cssClass="input-text" key="subcontractorCharges.branch" size="13" maxlength="20" /></td>
     <td align="right" class="listwhitetext" style="padding-right:10px;" width="170px" ><fmt:message key="subcontractorCharges.parentID"/></td>
    <td align="left" height="30px" class="listwhitetext" width="20"><s:textfield cssClass="input-text" size="13" maxlength="20" name="subcontractorCharges.parentId" readonly="true"/></td>
    
    
  </tr> 
  <tr>
    <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.regNumber"/><!--<font color="red" size="2">*</font></td>--></td>
    <td align="left"  height="30px" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.regNumber" size="20" maxlength="25" onchange="checkRegNo();" /></td>
  </tr>
  
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.personType"/></td>
   <td height="30px"><s:select cssClass="list-menu" name="subcontractorCharges.personType" list="%{personTypeList}" cssStyle="width:100px" headerKey="" headerValue=" " onchange="displayRow();changeStatus();"/></td> 
  </tr> 
  <tr>
   <td align="right" id="captionRow2" class="listwhitetext" style="padding-right:10px;" ><fmt:message key="subcontractorCharges.personId"/><font color="red" size="2">*</font></td>
   <td align="right" id="captionRow1" class="listwhitetext" style="padding-right:10px;" ><fmt:message key="subcontractorCharges.personId"/></td>
   <td align="left" height="30px" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.personId" size="13" maxlength="14" onchange="checkPerId();" />
   <img class="openpopup" width="17" height="20" onclick="openAccountPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
  </tr>
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.glCode"/></td>
   <td height="30px" colspan="3"><s:select cssClass="list-menu" name="subcontractorCharges.glCode" list="%{glCode}" cssStyle="width:215px" headerKey="" headerValue=" " onchange="changeStatus();"/></td> 
  </tr> 
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.amount"/></td>
   <td align="left" height="30px" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.amount" size="13" maxlength="14" cssStyle="text-align:right" onblur="chkSelect();"/></td>
  </tr>
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.description"/></td>
   <td align="left" colspan="3" height="30px" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.description" size="37" maxlength="225"  /></td>
  </tr>
  <tr>
  <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.approved" /></td> 
  <c:if test="${not empty subcontractorCharges.approved}">
	<s:text id="subcontractorChargesApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.approved"/></s:text>
	<td height="30px"><s:textfield cssClass="input-textUpper" id="approved" name="subcontractorCharges.approved" value="%{subcontractorChargesApprovedDateFormattedValue}" readonly="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="approved-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
	</c:if>
	<c:if test="${empty subcontractorCharges.approved}">
	<td height="30px"><s:textfield cssClass="input-textUpper" id="approved" name="subcontractorCharges.approved"  size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus=""/><img id="approved-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
	</c:if></tr>
  <tr> 
   <td align="right" class="listwhitetext" style="padding-right:10px;"><s:checkbox cssClass="input-text" key="subcontractorCharges.flag1099"   /></td>
  <td align="left" height="30px" class="listwhitetext" ><fmt:message key="subcontractorCharges.flag1099"/></td>
  </tr>
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.post"/></td>
   <c:if test="${not empty subcontractorCharges.post}">
	 <s:text id="subcontractorChargesApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.post" /></s:text>
	 <td height="30px"><s:textfield cssClass="input-textUpper" id="post" name="subcontractorCharges.post" value="%{subcontractorChargesApprovedDateFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" readonly="true"/><img id="post-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
   </c:if>
   <c:if test="${empty subcontractorCharges.post}">
	<td height="30px"><s:textfield cssClass="input-textUpper" id="post" name="subcontractorCharges.post"  size="7" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/> <img id="post-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
  </c:if> 
</tr>
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.accountSent"/></td>
     <c:if test="${not empty subcontractorCharges.accountSent}">
		<s:text id="subcontractorChargesApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.accountSent" /></s:text>
		<td height="30px"><s:textfield cssClass="input-textUpper" id="accountSent" name="subcontractorCharges.accountSent" value="%{subcontractorChargesApprovedDateFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" readonly="true"/><img id="accountSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
	</c:if>
	<c:if test="${empty subcontractorCharges.accountSent}">
		<td height="30px"><s:textfield cssClass="input-textUpper" id="accountSent" name="subcontractorCharges.accountSent" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/><img id="accountSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
	</c:if> 
	<td align="right" class="listwhitetext" style="padding-right:10px;"><fmt:message key="subcontractorCharges.sentToAccountVia"/></td>
   <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.sentToAccountVia" size="30" maxlength="100"/></td>
  <td></td>
  </tr> 
  </table>
  <table border="0" style="width:721px">
	<tbody>
	  <tr><td align="left" rowspan="1"></td></tr>
	  <tr>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='subcontractorCharges.createdOn'/></td>
		<fmt:formatDate var="containerCreatedOnFormattedValue" value="${subcontractorCharges.createdOn}"  pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="subcontractorCharges.createdOn" value="${containerCreatedOnFormattedValue}" />
		 <td><fmt:formatDate value="${subcontractorCharges.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
		 <td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='subcontractorCharges.createdBy' /></td>
		  <c:if test="${not empty subcontractorCharges.id}">
		    <s:hidden name="subcontractorCharges.createdBy"/>
		    <td><s:label name="createdBy" value="%{subcontractorCharges.createdBy}"/></td>
		 </c:if>
		 <c:if test="${empty subcontractorCharges.id}">
			 <s:hidden name="subcontractorCharges.createdBy" value="${pageContext.request.remoteUser}"/>
			 <td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='subcontractorCharges.updatedOn'/></td>
		<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${subcontractorCharges.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="subcontractorCharges.updatedOn" value="${containerUpdatedOnFormattedValue}" />
		<td><fmt:formatDate value="${subcontractorCharges.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
		<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='subcontractorCharges.updatedBy' /></td>
		<s:hidden name="subcontractorCharges.updatedBy" value="${pageContext.request.remoteUser}" />
	    <td><s:label name="subcontractorCharges.updatedBy" value="${pageContext.request.remoteUser}" /></td>
    </tr>
   </tbody>
   </table>
   <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onmouseover="return chkSelectSave();" onclick="return checkPersionId();" />   
        <s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px" />
  </div> 
<c:if test="${btntype=='yes'}"> 
<c:redirect url="/chargeAllocation.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&decorator=popup&popup=true"></c:redirect>
</c:if> 
  </s:form>   
  
<script type="text/javascript"> 
//if(document.forms['subcontractorChargesForm'].elements['btntype'].value=='yes'){
//pick();

//}
try{
displayRow();
}
catch(e){}
</script>  
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>