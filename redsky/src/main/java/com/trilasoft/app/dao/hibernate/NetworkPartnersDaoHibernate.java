package com.trilasoft.app.dao.hibernate;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate; 
import com.trilasoft.app.dao.NetworkPartnersDao; 
import com.trilasoft.app.model.NetworkPartners;

public class NetworkPartnersDaoHibernate extends GenericDaoHibernate<NetworkPartners, Long> implements NetworkPartnersDao {

	public NetworkPartnersDaoHibernate() {
		super(NetworkPartners.class);
	}

	public List getCorpidList(String sessionCorpID) {
	List corpidList=new ArrayList();
	corpidList=this.getSession().createSQLQuery("select distinct corpid from companydivision where corpid <> '"+sessionCorpID+"'").list();
    return corpidList;
	}

	public List findNetworkPartnerList(String sessionCorpID, String selectedCorpId) {
		return getHibernateTemplate().find("from NetworkPartners where agentCorpId='"+selectedCorpId+"' and corpId='"+sessionCorpID+"' ");
	}

	public List findByCorpId(String sessionCorpID, String selectedCorpId){
		 List compDivList=this.getSession().createSQLQuery("select distinct '"+sessionCorpID+"' as corpId, cd.id as id, cd.corpId as agentCorpId, '' as agentPartnerCode, cd.bookingAgentCode as agentLocalPartnerCode, cd.companyCode as agentCompanyDivision,cd.description as agentName from companydivision cd where corpId = '"+selectedCorpId+"'").list();
		 Iterator it = compDivList.iterator();
		 List nwkPartnerList=new ArrayList();
			while(it.hasNext())
			{		 
				Object []obj=(Object[]) it.next();
				NetworkPartnerByCompDivDTO nwkPartnetCompdiv = new NetworkPartnerByCompDivDTO();
				nwkPartnetCompdiv.setCorpId(obj[0]);
				nwkPartnetCompdiv.setId(obj[1]);
				nwkPartnetCompdiv.setAgentCorpId(obj[2]);
				if(obj[3] == null){nwkPartnetCompdiv.setAgentPartnerCode("");}else{nwkPartnetCompdiv.setAgentPartnerCode(obj[3]);}
				nwkPartnetCompdiv.setAgentLocalPartnerCode(obj[4]);
				nwkPartnetCompdiv.setAgentCompanyDivision(obj[5]);
				nwkPartnetCompdiv.setAgentName(obj[6]);
				nwkPartnerList.add(nwkPartnetCompdiv);				
			}		
		return nwkPartnerList;
	}

	public Integer countNetworkPartnerByCorpId(String sessionCorpID,String selectedCorpId) {
		return getHibernateTemplate().find("from NetworkPartners where agentCorpId='"+selectedCorpId+"' and corpId='"+sessionCorpID+"'").size();
	}
	
	public void removeByCorpId(String sessionCorpID, String selectedCorpId) {
		getHibernateTemplate().bulkUpdate("delete from NetworkPartners where corpId='"+sessionCorpID+"' and agentCorpId='"+selectedCorpId+"'");		
	}
	
	public Integer countCompanyDivisionByCorpId(String sessionCorpID,String selectedCorpId) {
		Integer value=null;
		List ls= this.getSession().createSQLQuery("select count(*) from companydivision where corpID='"+selectedCorpId+"'").list();
		value=Integer.parseInt(ls.get(0).toString());
		return value;
	}

	public class NetworkPartnerByCompDivDTO{
		private Object corpId;
    	private Object id;
    	private Object agentCorpId; 
    	private Object agentPartnerCode; 
    	private Object agentLocalPartnerCode;
    	private Object agentCompanyDivision;
    	private Object agentName;
		
    	public Object getCorpId() {
			return corpId;
		}
		public void setCorpId(Object corpId) {
			this.corpId = corpId;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getAgentCorpId() {
			return agentCorpId;
		}
		public void setAgentCorpId(Object agentCorpId) {
			this.agentCorpId = agentCorpId;
		}
		public Object getAgentPartnerCode() {
			return agentPartnerCode;
		}
		public void setAgentPartnerCode(Object agentPartnerCode) {
			this.agentPartnerCode = agentPartnerCode;
		}
		public Object getAgentLocalPartnerCode() {
			return agentLocalPartnerCode;
		}
		public void setAgentLocalPartnerCode(Object agentLocalPartnerCode) {
			this.agentLocalPartnerCode = agentLocalPartnerCode;
		}
		public Object getAgentName() {
			return agentName;
		}
		public void setAgentName(Object agentName) {
			this.agentName = agentName;
		}
		public Object getAgentCompanyDivision() {
			return agentCompanyDivision;
		}
		public void setAgentCompanyDivision(Object agentCompanyDivision) {
			this.agentCompanyDivision = agentCompanyDivision;
		}
    	
	}

	
	
}
