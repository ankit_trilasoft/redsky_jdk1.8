package com.trilasoft.app.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.directwebremoting.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="dshomerental")

	public class DsHomeRental  extends BaseObject{
		private Long id;
		private Long serviceOrderId;
		private String corpID;
		private String createdBy;
		private String updatedBy;
		private Date updatedOn;
		private Date createdOn;
		private String vendorCode;
		private String vendorName;
		private String vendorContact;
		private String vendorEmail;
	    private Date leaseStartDate;
	    private Date leaseExpireDate;
	    private String monthlyRentalAllowance;
	    private String propertyAvailableForViewing;
	    private String moveInInventoryAndConditionReport;
	    private String leaseReview;
	    private String securityDeposit;
	    private Date reminderDate;
	    private Date serviceStartDate;
		private Date serviceEndDate;
		@Override
		public String toString() {
			return new ToStringBuilder(this).append("id", id).append(
					"serviceOrderId", serviceOrderId).append("corpID", corpID)
					.append("createdBy", createdBy).append("updatedBy", updatedBy)
					.append("updatedOn", updatedOn).append("createdOn", createdOn)
					.append("vendorCode", vendorCode).append("vendorName",
							vendorName).append("vendorContact", vendorContact)
					.append("vendorEmail", vendorEmail).append("leaseStartDate",
							leaseStartDate).append("leaseExpireDate",
							leaseExpireDate).append("monthlyRentalAllowance",
							monthlyRentalAllowance).append(
							"propertyAvailableForViewing",
							propertyAvailableForViewing).append(
							"moveInInventoryAndConditionReport",
							moveInInventoryAndConditionReport).append(
							"leaseReview", leaseReview).append("securityDeposit",
							securityDeposit).append("reminderDate", reminderDate)
					.append("serviceStartDate", serviceStartDate).append(
							"serviceEndDate", serviceEndDate).toString();
		}
		@Override
		public boolean equals(final Object other) {
			if (!(other instanceof DsHomeRental))
				return false;
			DsHomeRental castOther = (DsHomeRental) other;
			return new EqualsBuilder().append(id, castOther.id).append(
					serviceOrderId, castOther.serviceOrderId).append(corpID,
					castOther.corpID).append(createdBy, castOther.createdBy)
					.append(updatedBy, castOther.updatedBy).append(updatedOn,
							castOther.updatedOn).append(createdOn,
							castOther.createdOn).append(vendorCode,
							castOther.vendorCode).append(vendorName,
							castOther.vendorName).append(vendorContact,
							castOther.vendorContact).append(vendorEmail,
							castOther.vendorEmail).append(leaseStartDate,
							castOther.leaseStartDate).append(leaseExpireDate,
							castOther.leaseExpireDate).append(
							monthlyRentalAllowance,
							castOther.monthlyRentalAllowance).append(
							propertyAvailableForViewing,
							castOther.propertyAvailableForViewing).append(
							moveInInventoryAndConditionReport,
							castOther.moveInInventoryAndConditionReport).append(
							leaseReview, castOther.leaseReview).append(
							securityDeposit, castOther.securityDeposit).append(
							reminderDate, castOther.reminderDate).append(
							serviceStartDate, castOther.serviceStartDate).append(
							serviceEndDate, castOther.serviceEndDate).isEquals();
		}
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(serviceOrderId).append(
					corpID).append(createdBy).append(updatedBy).append(updatedOn)
					.append(createdOn).append(vendorCode).append(vendorName)
					.append(vendorContact).append(vendorEmail).append(
							leaseStartDate).append(leaseExpireDate).append(
							monthlyRentalAllowance).append(
							propertyAvailableForViewing).append(
							moveInInventoryAndConditionReport).append(leaseReview)
					.append(securityDeposit).append(reminderDate).append(
							serviceStartDate).append(serviceEndDate).toHashCode();
		}
		@Column(length=30)
		public String getCorpID() {
			return corpID;
		}
		public void setCorpID(String corpID) {
			this.corpID = corpID;
		}
		
		@Column(length=82)
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		@Column
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		@Id
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		
		@Column
		public Date getLeaseExpireDate() {
			return leaseExpireDate;
		}
		public void setLeaseExpireDate(Date leaseExpireDate) {
			this.leaseExpireDate = leaseExpireDate;
		}
		@Column
		public Date getLeaseStartDate() {
			return leaseStartDate;
		}
		public void setLeaseStartDate(Date leaseStartDate) {
			this.leaseStartDate = leaseStartDate;
		}
		@Column
		public Date getServiceEndDate() {
			return serviceEndDate;
		}
		public void setServiceEndDate(Date serviceEndDate) {
			this.serviceEndDate = serviceEndDate;
		}
		@Column(length=20)
		public Long getServiceOrderId() {
			return serviceOrderId;
		}
		public void setServiceOrderId(Long serviceOrderId) {
			this.serviceOrderId = serviceOrderId;
		}
		@Column
		public Date getServiceStartDate() {
			return serviceStartDate;
		}
		public void setServiceStartDate(Date serviceStartDate) {
			this.serviceStartDate = serviceStartDate;
		}
		@Column(length=82)
		public String getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}
		@Column
		public Date getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}
		@Column(length=25)
		public String getVendorCode() {
			return vendorCode;
		}
		public void setVendorCode(String vendorCode) {
			this.vendorCode = vendorCode;
		}
		@Column(length=225)
		public String getVendorContact() {
			return vendorContact;
		}
		public void setVendorContact(String vendorContact) {
			this.vendorContact = vendorContact;
		}
		@Column(length=65)
		public String getVendorEmail() {
			return vendorEmail;
		}
		public void setVendorEmail(String vendorEmail) {
			this.vendorEmail = vendorEmail;
		}
		@Column(length=255)
		public String getVendorName() {
			return vendorName;
		}
		public void setVendorName(String vendorName) {
			this.vendorName = vendorName;
		}
		@Column 
		public String getLeaseReview() {
			return leaseReview;
		}
		public void setLeaseReview(String leaseReview) {
			this.leaseReview = leaseReview;
		}
		@Column (length =10)
		public String getMonthlyRentalAllowance() {
			return monthlyRentalAllowance;
		}
		public void setMonthlyRentalAllowance(String monthlyRentalAllowance) {
			this.monthlyRentalAllowance = monthlyRentalAllowance;
		}
		@Column 
		public String getPropertyAvailableForViewing() {
			return propertyAvailableForViewing;
		}
		public void setPropertyAvailableForViewing(String propertyAvailableForViewing) {
			this.propertyAvailableForViewing = propertyAvailableForViewing;
		}
		@Column 
		public Date getReminderDate() {
			return reminderDate;
		}
		public void setReminderDate(Date reminderDate) {
			this.reminderDate = reminderDate;
		}
		@Column(length =10) 
		public String getSecurityDeposit() {
			return securityDeposit;
		}
		public void setSecurityDeposit(String securityDeposit) {
			this.securityDeposit = securityDeposit;
		}
		@Column
		public String getMoveInInventoryAndConditionReport() {
			return moveInInventoryAndConditionReport;
		}
		public void setMoveInInventoryAndConditionReport(
				String moveInInventoryAndConditionReport) {
			this.moveInInventoryAndConditionReport = moveInInventoryAndConditionReport;
		}
		
		

	}


