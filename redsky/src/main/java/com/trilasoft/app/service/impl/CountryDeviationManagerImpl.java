package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.service.CountryDeviationManager;
import com.trilasoft.app.dao.CountryDeviationDao;

public class CountryDeviationManagerImpl extends GenericManagerImpl<CountryDeviation, Long> implements CountryDeviationManager{

	CountryDeviationDao countryDeviationDao;
	public CountryDeviationManagerImpl(CountryDeviationDao countryDeviationDao) {
		super(countryDeviationDao);
		this.countryDeviationDao=countryDeviationDao;
	}
	
	public List findDeviation(String charge , String deviationType){
		return countryDeviationDao.findDeviation(charge,deviationType);
	}
	public List findMaximumId(){
		return countryDeviationDao.findMaximumId();
	}
	public void delSelectedDevitaion(String delId){
		countryDeviationDao.delSelectedDevitaion(delId);
	}
	public List findAllDeviation(String chargeId, String sessionCorpID,	String contract){
		return countryDeviationDao.findAllDeviation(chargeId, sessionCorpID, contract);
	}
}
