<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>FAQ</title>   
    <meta name="heading" content="FAQ"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>
    .headtab_bg_center {
     width: 70%;
	}
	div#mydiv{margin-top: -45px;}
</style>
<style>
.textareaUpper {
    background-color: #E3F1FE;
    border: 1px solid #ABADB3;
    color: #000000;
    font-family: arial,verdana;
    font-size: 12px;   
    text-decoration: none;
}
.table th.sorted {
    background-color: #BCD2EF;
    color: #15428B;
}
</style>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript"> 
animatedcollapse.addDiv('faq', 'fade=1,hide=0')
animatedcollapse.init()
</script>
<script type="text/javascript">
function textLimit(field,maxlen) {
	if(field.value.length > maxlen){
	while(field.value.length > maxlen){
	field.value=field.value.replace(/.$/,'');
	}
	}
	}

</script>	
<script type="text/javascript">
function cancelForm(){
	var lName="${lastName}";
		location.href="frequentlyAskedList.html";		
}

function checkField(){
	var  question=document.forms['frequentlyAskedQuestionsForm'].elements['frequentlyAskedQuestions.question'].value;
    var question1=trim(question);
	var  answer= document.forms['frequentlyAskedQuestionsForm'].elements['frequentlyAskedQuestions.answer'].value;
	var answer1=trim(answer);
	var  language=document.forms['frequentlyAskedQuestionsForm'].elements['frequentlyAskedQuestions.language'].value;
	var language1=trim(language);
	var  seqNumber=document.forms['frequentlyAskedQuestionsForm'].elements['frequentlyAskedQuestions.sequenceNumber'].value;
	var seqNumber1=trim(seqNumber);
	if(question1=='' && answer1=='' && language1==''){
	     alert("Question , Answer and Language are required fields.");
		return false;
	 }else if(question1=='' && answer1=='' ){
	  alert("Question and Answer are required fields.");
	 return false;
	}else if(answer1=='' && language1==''){
		alert(" Answer and Language are required fields.");
	  return false;
	}else if(question1==''&& language1==''){
		alert("Question and Language are required fields.");
		return false;	
	}else if(question1==''){
	   alert("Question is required field.");
	 return false;
	}else if(answer1==''){
	  alert("Answer is a required field.");
	 return false;
	}else if(language1==''){
	  alert("Language is a required field.");
	return false;
	}
    <c:if test="${not empty frequentlyAskedQuestions.id }">
     if(seqNumber1==''){
          alert("Sequence Number is a required field.");
    	return false;
     }
	  </c:if>
	return true;
	}
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
</script>
	</head>  
		<s:form id="frequentlyAskedQuestionsForm" action="saveAdminQuestions" onsubmit="" method="post" validate="true">

	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		<ul>		
		<li><a href="cPortalResourceMgmts.html"><span>Cportal Docs</span></a></li>
			
		<li><a href="contractFilePolicy.html"><span>Policy</span></a></li>
					
		<li id="newmnav1" style="background:#FFF "><a class="current" style="margin-bottom:0px;"><span>FAQ</span></a></li>
		</ul>
	</div>
	<div class="spn">&nbsp;</div>
	</div>
<div id="Layer1" style="width: 100%;">

    <s:hidden name="dummQuestion"/>
    <s:hidden name="dummAnswer"/>
    <s:hidden name="dummLanguage"/>
    <s:hidden name="dummSeqNumber" />
    <s:hidden key="frequentlyAskedQuestions.partnerCode"  />
	<s:hidden name="id" value="${frequentlyAskedQuestions.id}" />
	<s:hidden name="frequentlyAskedQuestions.id" />
	<s:hidden name="partnerType" value="AC"/>	
	 <c:set var="partnerId" value="${partnerId}" /> 	
	 <c:set var="lastName" value="${lastName}" />
	  <c:set var="status" value="${status}" />  	 
	<c:if test="${frequentlyAskedQuestions.partnerId != '' && frequentlyAskedQuestions.partnerId != 0 && frequentlyAskedQuestions.partnerId != null}">
	<s:hidden name="frequentlyAskedQuestions.partnerId"/>
	<c:set var="partnerId" value="${frequentlyAskedQuestions.partnerId}" />
	</c:if>
	<c:if test="${frequentlyAskedQuestions.partnerId == '' or frequentlyAskedQuestions.partnerId == null or frequentlyAskedQuestions.partnerId == 0}">
	<s:hidden name="frequentlyAskedQuestions.partnerId" value="${partnerId}"/>
	</c:if>
	<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
   
   <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%" >
<tr>
	<td height="0" width="100%" align="left" >		
<div  onClick="javascript:animatedcollapse.toggle('faq')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"> FAQ
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="faq">
	<table><!--
	<c:if test="${frequentlyAskedQuestions.partnerCode != '' && frequentlyAskedQuestions.partnerCode != null}">
	<tr><td></td><td style="width:150px;"><s:hidden key="frequentlyAskedQuestions.partnerCode" required="true" cssClass="input-textUpper"  /></td></tr>
	<c:set var="partnerCode" value="${frequentlyAskedQuestions.partnerCode}" />
	</c:if>
	<c:if test="${frequentlyAskedQuestions.partnerCode == '' or frequentlyAskedQuestions.partnerCode == null}">
	<tr><td></td><td style="width:150px;"><s:hidden key="frequentlyAskedQuestions.partnerCode"  required="true" cssClass="input-textUpper" /></td></tr>
	</c:if>-->
	<tr><td class="listwhitetext" align="right"> Question<c:if test="${usertype!='ACCOUNT'}"><font color="red" size="2">*</font></c:if></td>
	<td style="width:150px;" colspan="6"><s:textfield cssClass="input-text" key="frequentlyAskedQuestions.question" maxlength="1000" size="133"/></td></tr>
	
	<tr><td valign="top" class="listwhitetext" align="right" > Answer<c:if test="${usertype!='ACCOUNT'}"><font color="red" size="2">*</font></c:if></td>
	<td colspan="6">
	<c:if test="${usertype!='ACCOUNT'}">
	<s:textarea name="frequentlyAskedQuestions.answer" cols="130" rows="4" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"/>
	</c:if>
	<c:if test="${usertype=='ACCOUNT'}">
	<s:textarea name="frequentlyAskedQuestions.answer" cols="130" rows="4" cssClass="textareaUpper"/>
	</c:if>
	</td></tr>
	<c:if test="${usertype!='ACCOUNT'}">
	<tr>
	<td valign="" class="listwhitetext"> Language<font color="red" size="2">*</font></td>
	<td width="65px"><s:select cssClass="list-menu"	key="frequentlyAskedQuestions.language"	list="%{language}" headerKey="" headerValue="" cssStyle="width:105px" /></td>
	<c:if test="${empty frequentlyAskedQuestions.id}">
	<td align="right" class="listwhitetext" width="100px"> Sequence #</td>
	<td><s:textfield cssClass="input-text" key="frequentlyAskedQuestions.sequenceNumber" onkeypress="return isNumberKey(event)" maxlength="5" size="5"/></td>
    <td  align="right" class="listwhitetext">Job&nbsp;Type</td>
    <td align="left"><s:textfield cssClass="input-text" name="frequentlyAskedQuestions.jobType" size="29" maxlength="65"/></td> 
   </c:if>
   <c:if test="${not empty frequentlyAskedQuestions.id}">
	<td align="right" class="listwhitetext" width="100px"> Sequence #<font color="red" size="2">*</font></td>
	<td width=""><s:textfield cssClass="input-text" key="frequentlyAskedQuestions.sequenceNumber" onkeypress="return isNumberKey(event)" maxlength="5" size="5"/></td>
	<td align="right" class="listwhitetext">Job&nbsp;Type</td>
	<td ><s:textfield cssClass="input-text" name="frequentlyAskedQuestions.jobType" size="29" maxlength="65"/></td> 
	</c:if>
	</tr>
	</c:if>
	</table>
	</div>		
	</td></tr></table>
	<div style="height:20px;"></div>
	</div><div class="bottom-header"><span></span></div></div></div>	
	<table width="70%">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${frequentlyAskedQuestions.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="frequentlyAskedQuestions.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${frequentlyAskedQuestions.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty frequentlyAskedQuestions.id}">
								<s:hidden name="frequentlyAskedQuestions.createdBy"/>
								<td ><s:label name="createdBy" value="%{frequentlyAskedQuestions.createdBy}"/></td>
							</c:if>
							<c:if test="${empty frequentlyAskedQuestions.id}">
								<s:hidden name="frequentlyAskedQuestions.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${frequentlyAskedQuestions.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="frequentlyAskedQuestions.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${frequentlyAskedQuestions.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty frequentlyAskedQuestions.id}">
							<s:hidden name="frequentlyAskedQuestions.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{frequentlyAskedQuestions.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty frequentlyAskedQuestions.id}">
							<s:hidden name="frequentlyAskedQuestions.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
<c:if test="${usertype!='ACCOUNT'}">
<c:url value="frequentlyAskedQuestionsList.html" var="url">
<c:param name="partnerCode" value="${partnerCode}"/>
<c:param name="partnerType" value="AC"/>
<c:param name="partnerId" value="${partnerId}"/>
<c:param name="lastName" value="${lastName}"/>
<c:param name="status" value="${status}"/>
</c:url>
	
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="saveAdminQuestions" key="button.save" theme="simple" onclick="return checkField();"/>
        <input type="button" class="cssbutton1" style="width:55px; height:25px" value="Cancel" style="width:87px; height:26px" onclick="cancelForm();" />
</c:if>        																																							 
        <input id="gotoFaq"type="button" class="cssbutton1" name="goToFaqList" style="width:95px; height:25px" value="Go To FAQ List" style="width:87px; height:26px" onclick="cancelForm();" />
        
	</s:form>
	
