<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head> 
    <title><fmt:message key="formsList.title"/></title> 
    <meta name="heading" content="<fmt:message key='formsList.heading'/>"/> 
    
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['searchForm'].elements['reports.menu'].value = "";
	document.forms['searchForm'].elements['reports.description'].value = "";
}	

function userStatusCheck(target){
		var targetElement = target;
		var ids = targetElement.value;
		
		if(targetElement.checked){
      		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['searchForm'].elements['userCheck'].value = ids;
      		}else{
      			var userCheckStatus=	document.forms['searchForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
      			document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
     		document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
     	}
}

function checkAll(){
	document.forms['searchForm'].elements['userCheck'].value = "";
	var len = document.forms['searchForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['searchForm'].elements['DD'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['searchForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['searchForm'].elements['DD'][i]);
	}
	document.forms['searchForm'].elements['userCheck'].value="";
}


function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}
function printForms(){
	var checkBoxId = document.forms['searchForm'].elements['userCheck'].value;
	var jobNumber = document.forms['searchForm'].elements['jobNumber'].value;
	var claimNumber = document.forms['searchForm'].elements['claimNumber'].value;
	var noteID = document.forms['searchForm'].elements['noteID'].value;
	var custID = document.forms['searchForm'].elements['custID'].value;
	var regNumber = document.forms['searchForm'].elements['regNumber'].value;
	var invoiceNumber = document.forms['searchForm'].elements['invoiceNumber'].value;
	var bookNumber = document.forms['searchForm'].elements['bookNumber'].value;
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select one or more document to print.');
	}else{
		var url = 'printForms.html?rId='+checkBoxId+'&jobNumber='+jobNumber+'&claimNumber='+claimNumber+'&noteID='+noteID+'&custID='+custID+'&regNumber='+regNumber+'&invoiceNumber='+invoiceNumber+'&bookNumber='+bookNumber;
		location.href=url;
	}
}
</script>

<style type="text/css">
div#page {
	margin:15px 0px 0px 10px;
	padding:0pt;
	text-align:center;
}
#mainPopup {
	padding-left:0px;
	padding-right:0px;
}
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	margin-top:-18px;
	padding:2px 0;
	text-align:right;
	width:100%;
}
</style>
	
</head>


<s:form id="searchForm" name="searchForm" action="searchRelatedReportsModule.html?decorator=popup&popup=true" method="post" validate="true">   
<s:hidden name="userCheck"/> 
<s:hidden name="billing.id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="reportModule" value="<%=request.getParameter("reportModule")%>"/>
<s:hidden name="reportSubModule" value="<%=request.getParameter("reportSubModule")%>"/>

<s:hidden name="customerFile.id" />
<s:hidden name="billing.shipNumber" value="%{serviceOrder.shipNumber}"/> 
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>

<s:hidden name="jobNumber" value="<%=request.getParameter("jobNumber")%>"/>
<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />

<s:hidden name="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
<c:set var="claimNumber" value="<%=request.getParameter("claimNumber") %>" />

<s:set name="reportss" value="reportss" scope="request"/> 
<c:set var="for" value="<%=request.getParameter("for") %>" />

<s:hidden name="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
<c:set var="bookNumber" value="<%=request.getParameter("bookNumber") %>" />

<s:hidden name="noteID" value="<%=request.getParameter("noteID") %>" />
<c:set var="noteID" value="<%=request.getParameter("noteID") %>" />

<s:hidden name="custID" value="<%=request.getParameter("custID") %>" />
<c:set var="custID" value="<%=request.getParameter("custID") %>" />

<s:hidden name="regNumber" value="<%=request.getParameter("regNumber")%>"/>
<c:set var="regNumber" value="<%=request.getParameter("regNumber") %>" />

<s:hidden name="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />
<c:set var="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />

<s:hidden name="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
<c:set var="bookNumber" value="<%=request.getParameter("bookNumber") %>" />

<div id="Layer1" align="left" style="width:720px;border:1px solid #fff;">
<div id="otabs" style="margin-left:40px;">
	<ul>
		<li><a class="current"><span>Search</span></a></li>
	</ul>
</div>
<div class="spnblk"></div>
<div style="padding-bottom: 0px;!padding-bottom: 7px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:-8px;!margin-top:0px; "><span></span></div>
<div class="center-content">
<table class="table" style="width:98%;position: relative;" >
	<thead>
		<tr>
			<th align="center">Form Category</th>
			<th align="center">Form Name</th>	
		</tr>
	</thead> 
	<tbody>
	 	<tr>
		   <td><s:textfield name="reports.menu" required="true" cssClass="input-text" size="20"/></td>
		   <td><s:textfield name="reports.description" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr>
		   <td></td>
		   <td style="border-left: hidden;">
		       <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" method="" key="button.search"/>  
		       <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px; " onclick="clear_fields();"/> 
		   </td>
	  	</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" />
<!-- 
<s:hidden name="jobNumber" value="%{customerFile.sequenceNumber}"/>
<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />
 -->
<s:set name="reportss" value="reportss" scope="request"/>

<div id="Layer1" style="width:720px">

	<div id="newmnav">
		<ul>
		    <li><a href="javascript:window.open('subModuleReports.html?id=${id}&claimNumber=${claimNumber}&jobNumber=${jobNumber}&regNumber=${regNumber}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&bookNumber=${bookNumber}&decorator=popup&popup=true','forms',740,800)" ><span>Forms List</span></a></li>
		    <li id="newmnav1" style="background:#FFF "><a href="javascript:window.open('relatedForms.html?id=${id}&claimNumber=${claimNumber}&jobNumber=${jobNumber}&regNumber=${regNumber}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&bookNumber=${bookNumber}&decorator=popup&popup=true','forms',740,800)" class="current"><span>Related Forms<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</ul>
	</div>
	<div id="chkAllButton"  class="listwhitetext" style="display:none;" >
		<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
		<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
	</div>
	<div class="spn">&nbsp;</div>
		
			
	<display:table name="reportss" class="table" requestURI="" id="reportsList" defaultsort="2" pagesize="10" style="width:720px">
	    <display:column title="">
		    <c:if test="${reportsList.pdf}">
		    	<input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${reportsList.id}" onclick="userStatusCheck(this)"/>
		    </c:if>
		    <c:if test="${(reportsList.pdf == 'false')}">
		    	<input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${reportsList.id}" disabled/>
		    </c:if>
	    </display:column>
	    <display:column sortable="true" title="Form Category" style="width:170px"><a href="javascript:window.open('viewFormParam.html?id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)"><c:out value="${reportsList.menu}" /></display:column>
	    <display:column property="module" sortable="true" titleKey="reports.module"/>
	    <display:column property="subModule" sortable="true" titleKey="reports.subModule"/>
	    <display:column property="description" sortable="true" title="Form Name" style="width:150px"/>
	    <display:column property="reportComment" sortable="true" title="Form Description" style="width:230px"/>
	
	    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
	    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
	    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 
	</display:table>
</div> 
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="printBtn"  value="Print" onclick="printForms();"/>
</s:form>
<script type="text/javascript"> 
	//var len = document.forms['searchForm'].elements['DD'].length;
    //if(len>1){
    //	show('chkAllButton');
    //}
    highlightTableRows("reportsList"); 
</script> 