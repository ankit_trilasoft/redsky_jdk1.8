package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.MssGridDao;
import com.trilasoft.app.model.MssGrid;

public class MssGridDaoHibernate extends GenericDaoHibernate<MssGrid, Long>implements MssGridDao{
	
	public MssGridDaoHibernate() {
		super(MssGrid.class);
	}
	
	public List findRecord(Long id, String sessionCorpID) {		
		List list = getHibernateTemplate().find("from MssGrid where mssId='"+id+"' and corpId='"+sessionCorpID+"'");
		return list;
	}
	public void deleteDataOfInventoryData(String inventoryFlag){
		getHibernateTemplate().bulkUpdate("delete from MssGrid where inventoryFlag='"+inventoryFlag+"'");
	}
}
