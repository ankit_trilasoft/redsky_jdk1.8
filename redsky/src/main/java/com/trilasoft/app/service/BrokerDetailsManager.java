package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.BrokerDetails;

public interface BrokerDetailsManager extends GenericManager<BrokerDetails, Long> {
	public List getByShipNumber(String shipNumber);

}
