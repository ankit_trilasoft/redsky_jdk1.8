<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>

<s:form name="monthlySalesGoalForm">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
<s:hidden name="msgFieldValueChanged" id="msgFieldValueChanged"/>
<s:hidden name="monthlySalesGoal.id" value="%{monthlySalesGoal.id}"/>
<s:hidden name="monthlySalesGoal.corpId" value="%{monthlySalesGoal.corpId}"/>
<s:hidden name="monthlySalesGoal.createdBy" value="%{monthlySalesGoal.createdBy}"/>
<s:hidden name="monthlySalesGoal.updatedBy" value="%{monthlySalesGoal.updatedBy}"/>
<s:hidden name="monthlySalesGoal.partnerCode" value="%{monthlySalesGoal.partnerCode}"/>
<c:if test="${not empty monthlySalesGoal.createdOn}">
	<s:text id="monthlySalesGoalCreatedOn" name="${FormDateValue}"> <s:param name="value" value="monthlySalesGoal.createdOn" /></s:text>
	<s:hidden  name="monthlySalesGoal.createdOn" value="%{monthlySalesGoalCreatedOn}" /> 
</c:if>
<c:if test="${not empty monthlySalesGoal.updatedOn}">
	<s:text id="monthlySalesGoalUpdatedOn" name="${FormDateValue}"> <s:param name="value" value="monthlySalesGoal.updatedOn" /></s:text>
	<s:hidden  name="monthlySalesGoal.updatedOn" value="%{monthlySalesGoalUpdatedOn}" /> 
</c:if>
<sec-auth:authComponent componentId="module.partner.section.monthlySalesGoalView.edit" >
	<%;
	String readOnlyVal="true";
	int permissionVal  = (Integer)request.getAttribute("module.partner.section.monthlySalesGoalView.edit" + "Permission");
 	if (permissionVal > 2 ){
 		readOnlyVal = "false";
 	}
  %>
  
	<table class="table" cellpadding="0" cellspacing="0" style="margin:3px 0px 5px;width:50%;">
			  	<thead>
			  	<tr>
				<th colspan="4" class="centeralign listwhitetext">YEAR&nbsp;
					<configByCorp:customDropDown listType="map" list="${yearList}" fieldValue="${monthlySalesGoal.year}"
    				attribute=" class=list-menu name=monthlySalesGoal.year id=year  style=height:19px;width:60px; onchange='findMonthlySalesGoalDetailsByYear();'"/>
				</th>						
				</tr>
				<tr>						
				<th class="centeralign msalesHead">MONTH</th>
				<th class="centeralign msalesHead">IMPORT</th>
				<th class="centeralign msalesHead">EXPORT</th>
				<th class="centeralign msalesHead">TOTAL</th>
				</tr>
				</thead>
				<tr class="even">
				<td style="width:150px;" class="txt-center largetext-ltblue"><b>January</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.januaryImport" id="januaryImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('januaryImport','januaryExport','januaryTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.januaryExport" id="januaryExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('januaryImport','januaryExport','januaryTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.januaryTotal" id="januaryTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="odd">
				<td class="txt-center largetext-ltblue"><b>February</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.februaryImport" id="februaryImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('februaryImport','februaryExport','februaryTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.februaryExport" id="februaryExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('februaryImport','februaryExport','februaryTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.februaryTotal" id="februaryTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="even">
				<td class="txt-center largetext-ltblue"><b>March</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.marchImport" id="marchImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('marchImport','marchExport','marchTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.marchExport" id="marchExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('marchImport','marchExport','marchTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.marchTotal" id="marchTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="odd">
				<td class="txt-center largetext-ltblue"><b>April</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.aprilImport" id="aprilImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('aprilImport','aprilExport','aprilTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.aprilExport" id="aprilExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('aprilImport','aprilExport','aprilTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.aprilTotal" id="aprilTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="even">
				<td class="txt-center largetext-ltblue"><b>May</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.mayImport" id="mayImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('mayImport','mayExport','mayTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.mayExport" id="mayExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('mayImport','mayExport','mayTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.mayTotal" id="mayTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="odd">
				<td class="txt-center largetext-ltblue"><b>June</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.juneImport" id="juneImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('juneImport','juneExport','juneTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.juneExport" id="juneExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('juneImport','juneExport','juneTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.juneTotal" id="juneTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="even">
				<td class="txt-center largetext-ltblue"><b>July</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.julyImport" id="julyImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('julyImport','julyExport','julyTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.julyExport" id="julyExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('julyImport','julyExport','julyTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.julyTotal" id="julyTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="odd">
				<td class="txt-center largetext-ltblue"><b>August</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.augustImport" id="augustImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('augustImport','augustExport','augustTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.augustExport" id="augustExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('augustImport','augustExport','augustTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.augustTotal" id="augustTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="even">
				<td class="txt-center largetext-ltblue"><b>September</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.septemberImport" id="septemberImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('septemberImport','septemberExport','septemberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.septemberExport" id="septemberExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('septemberImport','septemberExport','septemberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.septemberTotal" id="septemberTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="odd">
				<td class="txt-center largetext-ltblue"><b>October</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.octoberImport" id="octoberImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('octoberImport','octoberExport','octoberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.octoberExport" id="octoberExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('octoberImport','octoberExport','octoberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.octoberTotal" id="octoberTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="even">
				<td class="txt-center largetext-ltblue"><b>November</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.novemberImport" id="novemberImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('novemberImport','novemberExport','novemberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.novemberExport" id="novemberExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('novemberImport','novemberExport','novemberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.novemberTotal" id="novemberTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr class="odd">
				<td class="txt-center largetext-ltblue"><b>December</b></td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.decemberImport" id="decemberImport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('decemberImport','decemberExport','decemberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-text" name="monthlySalesGoal.decemberExport" id="decemberExport" readonly="<%=readOnlyVal%>" cssStyle="text-align:right;width:130px" maxlength="10" onkeypress="return onlyRateAllowed(event)" onchange="calculateMonthlyTotal('decemberImport','decemberExport','decemberTotal');findMsgFieldValueChanged();"/>
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.decemberTotal" id="decemberTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
				<tr>
				<td style="text-align:center;font-size:13px;"><b>Total</b></td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.allMonthImportTotal" id="allMonthImportTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.allMonthExportTotal" id="allMonthExportTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>
				<td>
					<s:textfield cssClass="input-textUpper" name="monthlySalesGoal.allMonthTotal" id="allMonthTotal" cssStyle="text-align:right;width:130px" readonly="true" />
				</td>						
				</tr>
	</table>
	</sec-auth:authComponent>
</s:form>
<script type="text/javascript"> 

</script>