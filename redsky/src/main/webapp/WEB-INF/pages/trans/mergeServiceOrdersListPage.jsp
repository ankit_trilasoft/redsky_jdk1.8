<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>



<display:table name="linkedSOMerge" class="table" id="linkedSOMerge">
<display:column title=""><input type="radio" name="soId" value="${linkedSOMerge.id }" id="soId" onclick="setSOIdvalue('${linkedSOMerge.id }')"/> </display:column>
<display:column property="shipNumber" sortable="true" title="SO#"/>
<display:column property="mode" sortable="true" title="Mode"/>
<display:column property="commodity" sortable="true" title="Commodity"/>
<display:column property="shipper" sortable="true" title="Shipper"/>
</display:table>
<div style="text-align:center;"> <input type="button" name="Merge" id="merge" value="Merge" onclick ="mergeSO()" class="cssbutton" disabled="true" /></div>

