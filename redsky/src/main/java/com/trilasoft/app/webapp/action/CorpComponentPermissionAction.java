package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CorpComponentPermission;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.RoleBasedComponentPermission;
import com.trilasoft.app.service.CorpComponentPermissionManager;
import com.trilasoft.app.service.RoleBasedComponentPermissionManager;
import com.trilasoft.app.service.RefMasterManager;

public class CorpComponentPermissionAction extends BaseAction{
	
	private Long id;
	private Long maxId;
	private String sessionCorpID;
	private CorpComponentPermission corpComponentPermission;
	private CorpComponentPermissionManager corpComponentPermissionManager;
	private List corpComponentPermissionList;
	private RefMasterManager refMasterManager;
	private static Map<String, String> display;
	private static List role;
	private static List globalCorpID;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CorpComponentPermissionAction.class);

		
	public CorpComponentPermissionAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		display = refMasterManager.findByParameter(corpId, "DISPLAY");
		globalCorpID=corpComponentPermissionManager.findCorpID();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	 }
	@SkipValidation
	public String permissionList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
				corpComponentPermissionList=corpComponentPermissionManager.getAll();
			}else{
		corpComponentPermissionList=corpComponentPermissionManager.findPermissionsByCorpId(sessionCorpID);
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	@SkipValidation
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null)
		{
			corpComponentPermission=corpComponentPermissionManager.get(id);
		}
		else
		{
			corpComponentPermission=new CorpComponentPermission();
			corpComponentPermission.setCreatedOn(new Date());
			corpComponentPermission.setUpdatedOn(new Date());
		
		}
		//corpComponentPermission.setCorpID(sessionCorpID);
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String save() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		boolean isNew = (corpComponentPermission.getId() == null);
		
		if(isNew)
	    {
			corpComponentPermission.setCreatedOn(new Date());
	    }
	     
		//corpComponentPermission.setCorpID(sessionCorpID);				
		corpComponentPermission.setUpdatedOn(new Date());
		corpComponentPermission.setUpdatedBy(getRequest().getRemoteUser());
		corpComponentPermission= corpComponentPermissionManager.save(corpComponentPermission);
		try{
			corpComponentPermissionManager.updateCurrentFieldVisibilityMap();
			}catch (Exception e) {}	
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Corp Component Permission has been added successfully" : "Corp Component Permission has been updated successfully";
		saveMessage(getText(key));
		}
		if (!isNew) { 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(corpComponentPermissionManager.findMaximumId().get(0).toString());
        	corpComponentPermission = corpComponentPermissionManager.get(maxId);
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
          return SUCCESS;
        }
		
	}
	@SkipValidation
	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		corpComponentPermissionManager.remove(id);
		try{
			corpComponentPermissionManager.updateCurrentFieldVisibilityMap();
			}catch (Exception e) {}	
		saveMessage(getText("Tab Permission is deleted sucessfully"));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String search(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean componenetId=corpComponentPermission.getComponentId()==null;
		boolean description=corpComponentPermission.getDescription()==null;
		if(!componenetId || !description) 
		        {
				corpComponentPermissionList=corpComponentPermissionManager.searchcompanyPermission(corpComponentPermission.getComponentId(), corpComponentPermission.getDescription(),sessionCorpID);	
				}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	
	private String gotoPageString;
	private String validateFormNav;
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
    	String s = save();
    	return gotoPageString;
    }
	
	
	

	public Long getMaxId() {
		return maxId;
	}
	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getDisplay() {
		return display;
	}

	public void setDisplay(Map<String, String> display) {
		this.display = display;
	}

	

	public List getRole() {
		return role;
	}

	public void setRole(List role) {
		this.role = role;
	}

	public CorpComponentPermission getCorpComponentPermission() {
		return corpComponentPermission;
	}

	public void setCorpComponentPermission(
			CorpComponentPermission corpComponentPermission) {
		this.corpComponentPermission = corpComponentPermission;
	}

	public List getCorpComponentPermissionList() {
		return corpComponentPermissionList;
	}

	public void setCorpComponentPermissionList(List corpComponentPermissionList) {
		this.corpComponentPermissionList = corpComponentPermissionList;
	}

	public void setCorpComponentPermissionManager(
			CorpComponentPermissionManager corpComponentPermissionManager) {
		this.corpComponentPermissionManager = corpComponentPermissionManager;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	/**
	 * @return the globalCorpID
	 */
	public List getGlobalCorpID() {
		return globalCorpID;
	}

	/**
	 * @param globalCorpID the globalCorpID to set
	 */
	public void setGlobalCorpID(List globalCorpID) {
		this.globalCorpID = globalCorpID;
	}

}
