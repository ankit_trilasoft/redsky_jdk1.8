package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "monthlysalesgoal")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class MonthlySalesGoal extends BaseObject{

	private Long id;
	private String corpId;
	private String updatedBy;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;
	private String partnerCode;
	private String year;
	private BigDecimal januaryImport = new BigDecimal(0);
	private BigDecimal januaryExport = new BigDecimal(0);
	private BigDecimal januaryTotal = new BigDecimal(0);
	private BigDecimal februaryImport = new BigDecimal(0);
	private BigDecimal februaryExport = new BigDecimal(0);
	private BigDecimal februaryTotal = new BigDecimal(0);
	private BigDecimal marchImport = new BigDecimal(0);
	private BigDecimal marchExport = new BigDecimal(0);
	private BigDecimal marchTotal = new BigDecimal(0);
	private BigDecimal aprilImport = new BigDecimal(0);
	private BigDecimal aprilExport = new BigDecimal(0);
	private BigDecimal aprilTotal = new BigDecimal(0);
	private BigDecimal mayImport = new BigDecimal(0);
	private BigDecimal mayExport = new BigDecimal(0);
	private BigDecimal mayTotal = new BigDecimal(0);
	private BigDecimal juneImport = new BigDecimal(0);
	private BigDecimal juneExport = new BigDecimal(0);
	private BigDecimal juneTotal = new BigDecimal(0);
	private BigDecimal julyImport = new BigDecimal(0);
	private BigDecimal julyExport = new BigDecimal(0);
	private BigDecimal julyTotal = new BigDecimal(0);
	private BigDecimal augustImport = new BigDecimal(0);
	private BigDecimal augustExport = new BigDecimal(0);
	private BigDecimal augustTotal = new BigDecimal(0);
	private BigDecimal septemberImport = new BigDecimal(0);
	private BigDecimal septemberExport = new BigDecimal(0);
	private BigDecimal septemberTotal = new BigDecimal(0);
	private BigDecimal octoberImport = new BigDecimal(0);
	private BigDecimal octoberExport = new BigDecimal(0);
	private BigDecimal octoberTotal = new BigDecimal(0);
	private BigDecimal novemberImport = new BigDecimal(0);
	private BigDecimal novemberExport = new BigDecimal(0);
	private BigDecimal novemberTotal = new BigDecimal(0);
	private BigDecimal decemberImport = new BigDecimal(0);
	private BigDecimal decemberExport = new BigDecimal(0);
	private BigDecimal decemberTotal = new BigDecimal(0);
	private BigDecimal allMonthTotal = new BigDecimal(0);
	private BigDecimal allMonthImportTotal = new BigDecimal(0);
	private BigDecimal allMonthExportTotal = new BigDecimal(0);
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("updatedBy", updatedBy)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("partnerCode", partnerCode)
				.append("year", year)
				.append("januaryImport", januaryImport).append("januaryExport", januaryExport).append("januaryTotal", januaryTotal)
				.append("februaryImport", februaryImport).append("februaryExport", februaryExport).append("februaryTotal", februaryTotal)
				.append("marchImport", marchImport).append("marchExport", marchExport).append("marchTotal", marchTotal)
				.append("aprilImport", aprilImport).append("aprilExport", aprilExport).append("aprilTotal", aprilTotal)
				.append("mayImport", mayImport).append("mayExport", mayExport).append("mayTotal", mayTotal)
				.append("juneImport", juneImport).append("juneExport", juneExport).append("juneTotal", juneTotal)
				.append("julyImport", julyImport).append("julyExport", julyExport).append("julyTotal", julyTotal)
				.append("augustImport", augustImport).append("augustExport", augustExport).append("augustTotal", augustTotal)
				.append("septemberImport", septemberImport).append("septemberExport", septemberExport).append("septemberTotal", septemberTotal)
				.append("octoberImport", octoberImport).append("octoberExport", octoberExport).append("octoberTotal", octoberTotal)
				.append("novemberImport", novemberImport).append("novemberExport", novemberExport).append("novemberTotal", novemberTotal)
				.append("decemberImport", decemberImport).append("decemberExport", decemberExport).append("decemberTotal", decemberTotal)
				.append("allMonthTotal", allMonthTotal).append("allMonthImportTotal", allMonthImportTotal).append("allMonthExportTotal", allMonthExportTotal)
				.toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MonthlySalesGoal))
			return false;
		MonthlySalesGoal castOther = (MonthlySalesGoal) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId).append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy).append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).append(partnerCode, castOther.partnerCode).append(year, castOther.year)
				.append(januaryImport, castOther.januaryImport).append(januaryExport, castOther.januaryExport).append(januaryTotal, castOther.januaryTotal)
				.append(februaryImport, castOther.februaryImport).append(februaryExport, castOther.februaryExport).append(februaryTotal, castOther.februaryTotal)
				.append(marchImport, castOther.marchImport).append(marchExport, castOther.marchExport).append(marchTotal, castOther.marchTotal)
				.append(aprilImport, castOther.aprilImport).append(aprilExport, castOther.aprilExport).append(aprilTotal, castOther.aprilTotal)
				.append(mayImport, castOther.mayImport).append(mayExport, castOther.mayExport).append(mayTotal, castOther.mayTotal)
				.append(juneImport, castOther.juneImport).append(juneExport, castOther.juneExport).append(juneTotal, castOther.juneTotal)
				.append(julyImport, castOther.julyImport).append(julyExport, castOther.julyExport).append(julyTotal, castOther.julyTotal)
				.append(augustImport, castOther.augustImport).append(augustExport, castOther.augustExport).append(augustTotal, castOther.augustTotal)
				.append(septemberImport, castOther.septemberImport).append(septemberExport, castOther.septemberExport).append(septemberTotal, castOther.septemberTotal)
				.append(octoberImport, castOther.octoberImport).append(octoberExport, castOther.octoberExport).append(octoberTotal, castOther.octoberTotal)
				.append(novemberImport, castOther.novemberImport).append(novemberExport, castOther.novemberExport).append(novemberTotal, castOther.novemberTotal)
				.append(decemberImport, castOther.decemberImport).append(decemberExport, castOther.decemberExport).append(decemberTotal, castOther.decemberTotal)
				.append(allMonthTotal, castOther.allMonthTotal).append(allMonthImportTotal, castOther.allMonthImportTotal).append(allMonthExportTotal, castOther.allMonthExportTotal)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(updatedBy).append(createdBy).append(createdOn)
				.append(updatedOn).append(partnerCode).append(year)
				.append(januaryImport).append(januaryExport).append(januaryTotal)
				.append(februaryImport).append(februaryExport).append(februaryTotal)
				.append(marchImport).append(marchExport).append(marchTotal)
				.append(aprilImport).append(aprilExport).append(aprilTotal)
				.append(mayImport).append(mayExport).append(mayTotal)
				.append(juneImport).append(juneExport).append(juneTotal)
				.append(julyImport).append(julyExport).append(julyTotal)
				.append(augustImport).append(augustExport).append(augustTotal)
				.append(septemberImport).append(septemberExport).append(septemberTotal)
				.append(octoberImport).append(octoberExport).append(octoberTotal)
				.append(novemberImport).append(novemberExport).append(novemberTotal)
				.append(decemberImport).append(decemberExport).append(decemberTotal)
				.append(allMonthTotal).append(allMonthImportTotal).append(allMonthExportTotal)
				.toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public BigDecimal getJanuaryImport() {
		return januaryImport;
	}

	public void setJanuaryImport(BigDecimal januaryImport) {
		this.januaryImport = januaryImport;
	}

	public BigDecimal getJanuaryExport() {
		return januaryExport;
	}

	public void setJanuaryExport(BigDecimal januaryExport) {
		this.januaryExport = januaryExport;
	}

	public BigDecimal getJanuaryTotal() {
		return januaryTotal;
	}

	public void setJanuaryTotal(BigDecimal januaryTotal) {
		this.januaryTotal = januaryTotal;
	}

	public BigDecimal getFebruaryImport() {
		return februaryImport;
	}

	public void setFebruaryImport(BigDecimal februaryImport) {
		this.februaryImport = februaryImport;
	}

	public BigDecimal getFebruaryExport() {
		return februaryExport;
	}

	public void setFebruaryExport(BigDecimal februaryExport) {
		this.februaryExport = februaryExport;
	}

	public BigDecimal getFebruaryTotal() {
		return februaryTotal;
	}

	public void setFebruaryTotal(BigDecimal februaryTotal) {
		this.februaryTotal = februaryTotal;
	}

	public BigDecimal getMarchImport() {
		return marchImport;
	}

	public void setMarchImport(BigDecimal marchImport) {
		this.marchImport = marchImport;
	}

	public BigDecimal getMarchExport() {
		return marchExport;
	}

	public void setMarchExport(BigDecimal marchExport) {
		this.marchExport = marchExport;
	}

	public BigDecimal getMarchTotal() {
		return marchTotal;
	}

	public void setMarchTotal(BigDecimal marchTotal) {
		this.marchTotal = marchTotal;
	}

	public BigDecimal getAprilImport() {
		return aprilImport;
	}

	public void setAprilImport(BigDecimal aprilImport) {
		this.aprilImport = aprilImport;
	}

	public BigDecimal getAprilExport() {
		return aprilExport;
	}

	public void setAprilExport(BigDecimal aprilExport) {
		this.aprilExport = aprilExport;
	}

	public BigDecimal getAprilTotal() {
		return aprilTotal;
	}

	public void setAprilTotal(BigDecimal aprilTotal) {
		this.aprilTotal = aprilTotal;
	}

	public BigDecimal getMayImport() {
		return mayImport;
	}

	public void setMayImport(BigDecimal mayImport) {
		this.mayImport = mayImport;
	}

	public BigDecimal getMayExport() {
		return mayExport;
	}

	public void setMayExport(BigDecimal mayExport) {
		this.mayExport = mayExport;
	}

	public BigDecimal getMayTotal() {
		return mayTotal;
	}

	public void setMayTotal(BigDecimal mayTotal) {
		this.mayTotal = mayTotal;
	}

	public BigDecimal getJuneImport() {
		return juneImport;
	}

	public void setJuneImport(BigDecimal juneImport) {
		this.juneImport = juneImport;
	}

	public BigDecimal getJuneExport() {
		return juneExport;
	}

	public void setJuneExport(BigDecimal juneExport) {
		this.juneExport = juneExport;
	}

	public BigDecimal getJuneTotal() {
		return juneTotal;
	}

	public void setJuneTotal(BigDecimal juneTotal) {
		this.juneTotal = juneTotal;
	}

	public BigDecimal getJulyImport() {
		return julyImport;
	}

	public void setJulyImport(BigDecimal julyImport) {
		this.julyImport = julyImport;
	}

	public BigDecimal getJulyExport() {
		return julyExport;
	}

	public void setJulyExport(BigDecimal julyExport) {
		this.julyExport = julyExport;
	}

	public BigDecimal getJulyTotal() {
		return julyTotal;
	}

	public void setJulyTotal(BigDecimal julyTotal) {
		this.julyTotal = julyTotal;
	}

	public BigDecimal getAugustImport() {
		return augustImport;
	}

	public void setAugustImport(BigDecimal augustImport) {
		this.augustImport = augustImport;
	}

	public BigDecimal getAugustExport() {
		return augustExport;
	}

	public void setAugustExport(BigDecimal augustExport) {
		this.augustExport = augustExport;
	}

	public BigDecimal getAugustTotal() {
		return augustTotal;
	}

	public void setAugustTotal(BigDecimal augustTotal) {
		this.augustTotal = augustTotal;
	}

	public BigDecimal getSeptemberImport() {
		return septemberImport;
	}

	public void setSeptemberImport(BigDecimal septemberImport) {
		this.septemberImport = septemberImport;
	}

	public BigDecimal getSeptemberExport() {
		return septemberExport;
	}

	public void setSeptemberExport(BigDecimal septemberExport) {
		this.septemberExport = septemberExport;
	}

	public BigDecimal getSeptemberTotal() {
		return septemberTotal;
	}

	public void setSeptemberTotal(BigDecimal septemberTotal) {
		this.septemberTotal = septemberTotal;
	}

	public BigDecimal getOctoberImport() {
		return octoberImport;
	}

	public void setOctoberImport(BigDecimal octoberImport) {
		this.octoberImport = octoberImport;
	}

	public BigDecimal getOctoberExport() {
		return octoberExport;
	}

	public void setOctoberExport(BigDecimal octoberExport) {
		this.octoberExport = octoberExport;
	}

	public BigDecimal getOctoberTotal() {
		return octoberTotal;
	}

	public void setOctoberTotal(BigDecimal octoberTotal) {
		this.octoberTotal = octoberTotal;
	}

	public BigDecimal getNovemberImport() {
		return novemberImport;
	}

	public void setNovemberImport(BigDecimal novemberImport) {
		this.novemberImport = novemberImport;
	}

	public BigDecimal getNovemberExport() {
		return novemberExport;
	}

	public void setNovemberExport(BigDecimal novemberExport) {
		this.novemberExport = novemberExport;
	}

	public BigDecimal getNovemberTotal() {
		return novemberTotal;
	}

	public void setNovemberTotal(BigDecimal novemberTotal) {
		this.novemberTotal = novemberTotal;
	}

	public BigDecimal getDecemberImport() {
		return decemberImport;
	}

	public void setDecemberImport(BigDecimal decemberImport) {
		this.decemberImport = decemberImport;
	}

	public BigDecimal getDecemberExport() {
		return decemberExport;
	}

	public void setDecemberExport(BigDecimal decemberExport) {
		this.decemberExport = decemberExport;
	}

	public BigDecimal getDecemberTotal() {
		return decemberTotal;
	}

	public void setDecemberTotal(BigDecimal decemberTotal) {
		this.decemberTotal = decemberTotal;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public BigDecimal getAllMonthTotal() {
		return allMonthTotal;
	}

	public void setAllMonthTotal(BigDecimal allMonthTotal) {
		this.allMonthTotal = allMonthTotal;
	}

	public BigDecimal getAllMonthImportTotal() {
		return allMonthImportTotal;
	}

	public void setAllMonthImportTotal(BigDecimal allMonthImportTotal) {
		this.allMonthImportTotal = allMonthImportTotal;
	}

	public BigDecimal getAllMonthExportTotal() {
		return allMonthExportTotal;
	}

	public void setAllMonthExportTotal(BigDecimal allMonthExportTotal) {
		this.allMonthExportTotal = allMonthExportTotal;
	}

	
}
