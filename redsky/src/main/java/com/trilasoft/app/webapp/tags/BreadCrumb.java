package com.trilasoft.app.webapp.tags;

import java.io.Serializable;

public class BreadCrumb implements Serializable{
	private String url;

	private String label;

	public BreadCrumb(String url, String label) {
		this.url = url;
		this.label = label;
	}

	public String getUrl() {
		return this.url;
	}

	public String getLabel() {
		return this.label;
	}

	public int hashCode() {
		return this.url.hashCode() & this.label.hashCode();
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof BreadCrumb)) {
			return false;
		}

		BreadCrumb b = (BreadCrumb) obj;

		// this is all to make the equality of a url not include the query
		// string
		// Could just use java.net.URL
		String u1 = this.url;
		String u2 = b.url;
		int idx1 = u1.indexOf("?");
		if (idx1 != -1) {
			u1 = u1.substring(0, idx1);
		}
		int idx2 = u2.indexOf("?");
		if (idx2 != -1) {
			u2 = u2.substring(0, idx2);
		}

		return u1.equals(u2) && this.label.equals(b.label);
	}

	public String toString() {
		return "[" + this.label + "|" + this.url + "]";
	}

}
