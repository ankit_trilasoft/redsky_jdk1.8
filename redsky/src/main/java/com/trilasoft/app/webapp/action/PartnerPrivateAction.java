package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.PartnerPrivateDaoHibernate.UserDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.model.MonthlySalesGoal;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.PartnerBankInformation;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.PartnerBankInformation;
import com.trilasoft.app.service.AccountAssignmentTypeManager;
import com.trilasoft.app.service.AccountContactManager;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.EntitlementManager;
import com.trilasoft.app.service.MonthlySalesGoalManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.PartnerBankInfoManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;

public class PartnerPrivateAction extends BaseAction implements Preparable{
	
	private static final PartnerPrivate ParentPartnerPrivate = null;

	private Long id;

	private List partnerPrivateList;
	
	private PartnerPrivate partnerPrivate; 
	
	private PartnerPublic partnerPublic;
	
	private PartnerPublicManager partnerPublicManager;
	
	private PartnerPrivateManager partnerPrivateManager;
	
	private RefMasterManager refMasterManager;

	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;

	private String hitflag;
	
	private Map<String, String> partnerStatus;
	
	private String partnerType;
	
	private String partnerCode;
	
	private String partnerId;
	
	private PartnerManager partnerManager;
	
	private String popupval;
	
	private String exList;
	
	private Map<String, String> billgrp;

	private Map<String, String> billinst;

	private Map<String, String> paytype;

	private Map<String, String> payopt;
	
	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	
	private  Map<String, String> pricing = new LinkedHashMap<String, String>();
	
	private  Map<String, String> billing = new LinkedHashMap<String, String>();
	
	private  Map<String, String> payable = new LinkedHashMap<String, String>();
	
	private  Map<String, String> claimsUser = new LinkedHashMap<String, String>();
	
	private  Map<String, String> auditor = new LinkedHashMap<String, String>();
	
	private  Map<String, String> coordinator = new LinkedHashMap<String, String>();
	
	private  Map<String, String> internalBillingPersonList = new LinkedHashMap<String, String>();
	
	
	private Map<String, String> classcode;
	
	private Map<String, String> creditTerms;
	
	private AccountContactManager accountContactManager;

	private AccountContact accountContact;

	private AccountProfileManager accountProfileManager;

	private AccountProfile accountProfile;
	
	private DataSecurityFilterManager dataSecurityFilterManager;

	private DataSecuritySetManager dataSecuritySetManager;
	
	private DataSecurityFilter dataSecurityFilter;

	private DataSecuritySet dataSecuritySet;
	
	private List multiplequalityCertifications;
	
	private List multiplVanlineAffiliations;
	
	private List multiplServiceLines;
	
	private List isoCodeList;
	
	private String partnerNotes;
	
	private NotesManager notesManager;
	
	private  Map<String,String> currency;
	private  Map<String,String> bankCode;
	 private AccountLineManager accountLineManager;
	 private String  defaultCurrency;
	 private String agentCountryCode;
	 private String justSayYes;
	private String qualitySurvey;
	private Boolean noTransfereeEvaluation;
	private Boolean oneRequestPerCF;

	private List ratingScale;
	private List customerFeedback;
	private String rateScale;
	private TrackingStatus trackingStatus;
	private ServiceOrder serviceOrder;
	 
	private List entitlementList1=new ArrayList();
	private List<Entitlement> entitlementList =new ArrayList<Entitlement>();
	private Map<String, String> entitlementMap=new HashMap<String, String>();
	private EntitlementManager entitlementManager;
	 Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(PartnerPrivateAction.class);
	private List partnerContractList;
	private Map<String, String> lead;
	private  Map<String, String> hasval;
	private  Map<String, String> insopt;
	private  Map<String, String> vatBillingGroups = new LinkedHashMap<String, String>();
	private  Map<String,String>euVatList;
	private CustomerFileManager customerFileManager;
	private PartnerAccountRefManager partnerAccountRefManager;
	private PartnerAccountRef partnerAccountRef;
	private Boolean agentCheck;
	private Map <String,String> billingCurrency;
	private List partnerBankInfoList;
	private String bankPartnerCode;
	private String bankNoList;
    private String currencyList;
    private String checkList;
    private String newlineid;
    private PartnerBankInformation partnerBankInformation;
    private PartnerBankInfoManager partnerBankInfoManager;
    private String partnerbanklist;
    private  Map<String, String> agentClassification = new LinkedHashMap<String, String>();
    private  Map<String,String>rddBasedList;
    private String containsData;
	private String userName;
	private Map<String, String> storageEmailType;
	private Map<String, String> billingMomentList;
	private List companyDivisionList = new ArrayList();
	private List multipleCompanyDivisionList = new ArrayList();
	private Map<String, String> printOptions;
	private Map<String, String> billCycle;
	private String billToCode;
    private String  description;
    private Long prId;
    private String selectedVatDec;
    private List refMasterList;
    private String vatdec;
    private String  flexcode ;
	private List vatDescription;
	private List newVatDescription;
	private String  defaultVat;
	private String   vatBillingCode;
	private  Map<String, String> vatBillingGroupPartnerData = new LinkedHashMap<String, String>();
	private String   parterCodeList;
	
	public String getVatBillingCode() {
		return vatBillingCode;
	}

	public void setVatBillingCode(String vatBillingCode) {
		this.vatBillingCode = vatBillingCode;
	}

	public String getDefaultVat() {
		return defaultVat;
	}

	public void setDefaultVat(String defaultVat) {
		this.defaultVat = defaultVat;
	}

	private String vatBillimngGroupCode;

	public String getVatBillimngGroupCode() {
		return vatBillimngGroupCode;
	}

	public void setVatBillimngGroupCode(String vatBillimngGroupCode) {
		this.vatBillimngGroupCode = vatBillimngGroupCode;
	}

	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String s = save();
		validateFormNav = "OK";		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}

	public PartnerPrivateAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	private Company company;
	private CompanyManager companyManager;
	private Boolean checkTransfereeInfopackage;
	private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
	private Map<String, String> collectionList;
	private List<SystemDefault> sysDefaultDetail;
	private String baseCurrency;
	private String checkCodefromDefaultAccountLine;
	private Map <String, String> defaultContact = new LinkedHashMap<String, String>();
	private  Map<String,String> yearList;
	private  Map<String,String>flexCodeList;

	public Map<String, String> getFlexCodeList() {
		return flexCodeList;
	}

	public void setFlexCodeList(Map<String, String> flexCodeList) {
		this.flexCodeList = flexCodeList;
	}

	public void prepare() throws Exception {		

		ratingScale = new ArrayList();		
		ratingScale.add("1 - 10 (10 is the best)");
		ratingScale.add("1 � 6 (1 is the best)");		
		
		customerFeedback=new ArrayList();
		customerFeedback.add("Just Say Yes");
		customerFeedback.add("Quality Survey");		 
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
		/*if(!partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode).isEmpty() && (partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode).get(0)!=null)){
			partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode).get(0);
			partnerContractList = customerFileManager.findCustJobContract(partnerPrivate.getPartnerCode(), "", partnerCreatedOn, partnerPrivate.getCorpID(),"");
		}*/
		sysDefaultDetail = partnerPrivateManager.findsysDefault(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				baseCurrency=systemDefault.getBaseCurrency();
			}
			
		}
		
	}	
	
	@SkipValidation
	public String list() {
		/*getComboList(sessionCorpID);
		partnerPublic = partnerPublicManager.get(id);
		String firstName;
		String lastName;
		String partnerCode;
		String status;

		if (partnerPrivate != null) {
			if (partnerPrivate.getFirstName() == null) {
				firstName = "";
			} else {
				firstName = partnerPrivate.getFirstName();
			}
			if (partnerPrivate.getLastName() == null) {
				lastName = "";
			} else {
				lastName = partnerPrivate.getLastName();
			}
			if (partnerPrivate.getPartnerCode() == null) {
				partnerCode = "";
			} else {
				partnerCode = partnerPrivate.getPartnerCode();
			}
			if (partnerPrivate.getStatus() == null) {
				status = "";
			} else {
				status = partnerPrivate.getStatus();
			}
																																								
			partnerPrivateList = partnerPrivateManager.getPartnerPrivateList(partnerType, sessionCorpID,firstName, lastName, partnerCode, status);
		} else {
			partnerPrivateList = partnerPrivateManager.getPartnerPrivateList("", sessionCorpID, "", "", "", "");
		}*/
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerPrivateList = partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public void createPartnerAccountRef(PartnerPublic partnerPublic,PartnerPrivate partnerPrivate , String refType){
		try{
			String permKey = sessionCorpID +"-"+"component.AcctRef.autoGenerateAccRefWithPartner";
			boolean visibilityForPUTT=false;
			 visibilityForPUTT=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
			 if(visibilityForPUTT){
			 List tempCount = partnerAccountRefManager.countChkAccountRef(partnerPrivate.getPartnerCode(),refType,sessionCorpID);
			 if(tempCount.get(0).toString().trim().equalsIgnoreCase("0")){
				    partnerAccountRef = new PartnerAccountRef();
		        	partnerAccountRef.setCorpID(sessionCorpID);    
		        	partnerAccountRef.setRefType(refType);
		        	String actg="";
		        	String PartnerCode=(partnerPrivate.getPartnerCode());
		        	if(PartnerCode.substring(0, 1).matches("[0-9]")){
						actg=PartnerCode;	
					}else{
					actg=PartnerCode.substring(1, PartnerCode.length());
					}
		        	String val = "000000";
					if(actg.toString().length()<=6){
					val = val.substring(actg.toString().length()); 
					actg=val+actg;
					}
					if(partnerAccountRef.getRefType()!=null && partnerAccountRef.getRefType().toString().trim().equalsIgnoreCase("R")){
					    actg="D"+actg;
					}else{
						actg="C"+actg;	
					}
					partnerAccountRef.setAccountCrossReference(actg);
		        	partnerAccountRef.setPartnerCode(partnerPrivate.getPartnerCode());
		        	partnerAccountRef.setUpdatedOn(new Date());
		        	partnerAccountRef.setCreatedOn(new Date());
		        	partnerAccountRef.setCreatedBy(getRequest().getRemoteUser());
			        partnerAccountRef.setUpdatedBy(getRequest().getRemoteUser());
			        //partnerAccountRef.setPartner(partner);  	
			        partnerAccountRef = partnerAccountRefManager.save(partnerAccountRef);
			 }
			 }

			}catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
		    	 e.printStackTrace(); 
			}
	}
	
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		getComboList(sessionCorpID);
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		agentCountryCode=partnerPublicManager.getCountryCode(partnerPublic.getBillingCountry(),"COUNTRY",sessionCorpID);
		if(!partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode).isEmpty() && (partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode).get(0)!=null)){
			partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode).get(0);
			containsData=partnerPrivateManager.getPartnerCodeFromDefaultTemplate(sessionCorpID,partnerCode);
			if(partnerPrivate.getGrpAllowed()==null)
			{
				company= companyManager.findByCorpID(sessionCorpID).get(0);
				String grpDefaultVal=company.getGrpDefault();
				if(grpDefaultVal!=null)
				{
					if(grpDefaultVal.equalsIgnoreCase("YES"))
					{
					partnerPrivate.setGrpAllowed(true);
					}
					else
					{
					partnerPrivate.setGrpAllowed(false);
					}
				}
				else
				{
					partnerPrivate.setGrpAllowed(false);
				}
				
			}
			if(partnerPrivate.getStatus()==null || partnerPrivate.getStatus().equalsIgnoreCase("")){
				partnerPrivate.setStatus("New");
			}
			if(partnerPublic.getStatus()!=null &&(partnerPublic.getStatus().equalsIgnoreCase("Inactive") || partnerPublic.getStatus().equalsIgnoreCase("BlackListed"))){
				partnerPrivate.setStatus(partnerPublic.getStatus());
			}
			if(partnerPrivate.getMultipleCompanyDivision()!=null && !partnerPrivate.getMultipleCompanyDivision().equals("")){
				multipleCompanyDivisionList = new ArrayList();
				String[] companyDivision = partnerPrivate.getMultipleCompanyDivision().split(",");
				for (String compDiv : companyDivision){
					multipleCompanyDivisionList.add(compDiv.trim());
				}
			}
			String permKey = sessionCorpID +"-"+"component.partner.vatBillingGroup.edit";
			boolean visibilityForCorpId=false;
			 visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
			 if(visibilityForCorpId){
				 String flagb="";
				 String vatBillimngGroupDes="";
				 String partnerVatDec="";
					if(vatBillingGroups.containsKey(partnerPrivate.getVatBillingGroup())){
					vatBillimngGroupDes =vatBillingGroups.get(partnerPrivate.getVatBillingGroup());	
				}
					flexcode  = refMasterManager.findPartnerVatBillingFlexCode(sessionCorpID, vatBillimngGroupDes);
				    if(vatBillimngGroupDes!=null && !vatBillimngGroupDes.equals("") && flexcode!=null && (!(flexcode.trim().equals(""))) ){
					euVatList = refMasterManager.findPartnerVatDescription(sessionCorpID, "EUVAT", flexcode, partnerPrivate.getDefaultVat(), "Y");
				    }
				   
			 }
			 
		}
		else{
			partnerPrivate = new PartnerPrivate();
			partnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
			partnerPrivate.setLastName(partnerPublic.getLastName());
			if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
				partnerPrivate.setCorpID("VOER");
			}else{
				partnerPrivate.setCorpID(sessionCorpID);
			}
			
			partnerPrivate.setCreatedOn(new Date());
			partnerPrivate.setUpdatedOn(new Date());
			partnerPrivate.setUpdatedBy(getRequest().getRemoteUser());
		}
		getNotesForIconChange();

		if (partnerPublic != null) {
		   if(partnerPrivate.getCustomerFeedback()==null || partnerPrivate.getCustomerFeedback().equalsIgnoreCase("")){			
			  if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) && company.getJustSayYes()==true && company.getQualitySurvey()==true){
				    	justSayYes="checked";
			  }else if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) && company.getJustSayYes()==true && company.getQualitySurvey()==false){
				    	justSayYes="checked";
			  }else if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) && company.getJustSayYes()==false && company.getQualitySurvey()==true){
				    	qualitySurvey="checked";
			  }else{	
				qualitySurvey="checked";
				List parentAgentlist = partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getAgentParent());
				if(parentAgentlist!=null && !parentAgentlist.isEmpty() && (parentAgentlist.get(0)!=null)){
					PartnerPrivate partnerPrivateParent = (PartnerPrivate) parentAgentlist.get(0);
					partnerPrivate.setNoTransfereeEvaluation(partnerPrivateParent.getNoTransfereeEvaluation());
					partnerPrivate.setOneRequestPerCF(partnerPrivateParent.getOneRequestPerCF());
					partnerPrivate.setRatingScale(partnerPrivateParent.getRatingScale());
					partnerPrivate.setDescription1(partnerPrivateParent.getDescription1());
					partnerPrivate.setDescription2(partnerPrivateParent.getDescription2());
					partnerPrivate.setDescription3(partnerPrivateParent.getDescription3());
					partnerPrivate.setLink1(partnerPrivateParent.getLink1());
					partnerPrivate.setLink2(partnerPrivateParent.getLink2());
					partnerPrivate.setLink3(partnerPrivateParent.getLink3());	
					partnerPrivate=partnerPrivateManager.save(partnerPrivate);	
					
			  }
			}
		  }else{
			  if(partnerPrivate.getCustomerFeedback().equalsIgnoreCase("Just Say Yes")){
				justSayYes="checked";
			  }else{
				qualitySurvey="checked";
			  }
		   }
		}
		if(partnerPrivate.getCoordinator()!=null && !(partnerPrivate.getCoordinator().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getCoordinator());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getCoordinator().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!coordinatorList.containsValue(coordCode)){
					coordinatorList.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getClaimsUser()!=null && !(partnerPrivate.getClaimsUser().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getClaimsUser());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getClaimsUser().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!claimsUser.containsValue(coordCode)){
					claimsUser.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getAuditor()!=null && !(partnerPrivate.getAuditor().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getAuditor());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getAuditor().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!auditor.containsValue(coordCode)){
					auditor.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getPricingUser()!=null && !(partnerPrivate.getPricingUser().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getPricingUser());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getPricingUser().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!pricing.containsValue(coordCode)){
					pricing.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getPayableUser()!=null && !(partnerPrivate.getPayableUser().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getPayableUser());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getPayableUser().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!payable.containsValue(coordCode)){
					payable.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getAccountManager()!=null && !(partnerPrivate.getAccountManager().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getAccountManager());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getAccountManager().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!coordinator.containsValue(coordCode)){
					coordinator.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getAccountHolder()!=null && !(partnerPrivate.getAccountHolder().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getAccountHolder());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getAccountHolder().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!sale.containsValue(coordCode)){
					sale.put(coordCode,alias );
				}
			}
		}
		if(partnerPrivate.getBillingUser()!=null && !(partnerPrivate.getBillingUser().equalsIgnoreCase(""))){
			List aliasNameList=userManager.findUserByUsername(partnerPrivate.getBillingUser());
			if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
				String coordCode=partnerPrivate.getBillingUser().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!billing.containsValue(coordCode)){
					billing.put(coordCode,alias );
				}
			}
		}
		
		//accountAssignmentTypeList=accountAssignmentTypeManager.getAll();
		accountAssignmentTypeList=accountAssignmentTypeManager.getAssignmentTypeList(partnerCode, Long.parseLong(partnerId), sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
		getComboList(sessionCorpID);
		boolean isNew = (partnerPrivate.getId() == null);
		
		if(partnerPrivate.getFirstName() == null){
			partnerPrivate.setFirstName("");
		}
		if(partnerPrivate.getAgentClassification() == null){
			partnerPrivate.setAgentClassification("");
		}
		if(partnerPrivate.getMultipleCompanyDivision()!=null && !partnerPrivate.getMultipleCompanyDivision().equals("")){
			String[] companyDivision = partnerPrivate.getMultipleCompanyDivision().split(",");
			for (String compDivVal : companyDivision) {
				multipleCompanyDivisionList.add(compDivVal.trim());
			}
		}
		
		/*if(partnerPrivate.getUsaFlagRequired()!=null || partnerPrivate.getUsaFlagRequired()!=false)
		{
		trackingStatus.setFlagCarrier("USA");
		serviceOrder.setServiceOrderFlagCarrier("USA");
		}*/
		
		/*if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ")){
			partnerPrivate.setCorpID("VOER");
		}else{
			partnerPrivate.setCorpID(sessionCorpID);
		}*/
		partnerPrivate.setCorpID(company.getParentCorpId());
		partnerPrivate.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			partnerPrivate.setCreatedOn(new Date());
			
			String maxPartnerCode;
			if(partnerPrivate.getPartnerCode()== null || partnerPrivate.getPartnerCode()==""){
				try {
					if ((partnerPrivateManager.findPartnerCode("P10001")).isEmpty()) {
						maxPartnerCode = "P10001";
					} else {
						List maxCode = partnerPrivateManager.findMaxByCode();
	
						maxPartnerCode = "P" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
						if (!(partnerPrivateManager.findPartnerCode(maxPartnerCode) == null || (partnerPrivateManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
							maxPartnerCode = "P" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
						}
					}
				} catch (Exception ex) {
					maxPartnerCode = "P10001";
					ex.printStackTrace();
				}
	
				partnerPrivate.setPartnerCode(maxPartnerCode);
			}
			
		}
		
		partnerPrivate.setUpdatedOn(new Date());
		
		try {
			boolean portalIdActive = (partnerPrivate.getreloPartnerPortal() == (true));
			if (portalIdActive) {
				String prefix = "DATA_SECURITY_FILTER_SO_" + partnerPrivate.getPartnerCode();
				String prefixDataSet = "DATA_SECURITY_SET_SO_RLOVENDORCODE_" + partnerPrivate.getPartnerCode();
				List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
				List agentFilterSetList = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
				if ((agentFilterList ==null || agentFilterList.isEmpty()) && (agentFilterSetList == null || agentFilterSetList.isEmpty())) {
					List nameList = new ArrayList();
					nameList.add("CAR_vendorCode");
					nameList.add("COL_vendorCode");
					nameList.add("TRG_vendorCode");
					nameList.add("HOM_vendorCode");
					nameList.add("RNT_vendorCode");
					nameList.add("SET_vendorCode");
					
					nameList.add("LAN_vendorCode");
					nameList.add("MMG_vendorCode");
					nameList.add("ONG_vendorCode");
					nameList.add("PRV_vendorCode");
					nameList.add("EXP_vendorCode");
					nameList.add("RPT_vendorCode");
					
					nameList.add("SCH_schoolSelected");
					nameList.add("TAX_vendorCode");
					nameList.add("TAC_vendorCode");
					nameList.add("TEN_vendorCode");
					nameList.add("VIS_vendorCode");
					nameList.add("WOP_vendorCode");
					
					nameList.add("REP_vendorCode");
					nameList.add("CAT_vendorCode");
					nameList.add("CLS_vendorCode");
					nameList.add("CHS_vendorCode");
					nameList.add("DPS_vendorCode");
					nameList.add("HSM_vendorCode");
					
					
					
					
					nameList.add("PDT_vendorCode");
					nameList.add("RCP_vendorCode");
					nameList.add("SPA_vendorCode");
					nameList.add("TCS_vendorCode");
					nameList.add("MTS_vendorCode");
					nameList.add("DSS_vendorCode");
					
					nameList.add("HOB_vendorCode");
					nameList.add("FRL_vendorCode");
					nameList.add("APU_vendorCode");
					nameList.add("FLB_vendorCode");
					nameList.add("INS_vendorCode");
					nameList.add("INP_vendorCode");
					
					
					nameList.add("EDA_vendorCode");
					nameList.add("TAS_vendorCode");
					nameList.add("AIO_vendorCode");
					nameList.add("RLS_vendorCode");
					dataSecuritySet = new DataSecuritySet();
					Iterator it = nameList.iterator();
					while (it.hasNext()) {
						String prefixdatasecfilter="DATA_SECURITY_FILTER_SO_";
						String fieldName = it.next().toString();
						String postFix = prefix;
						String fieldNamedata = postFix.replace("DATA_SECURITY_FILTER_SO_", "");
						String postFixData= prefixdatasecfilter+fieldName+"_"+fieldNamedata;
						dataSecurityFilter = new DataSecurityFilter();
						dataSecurityFilter.setName(postFixData);
						dataSecurityFilter.setTableName("serviceorder");
						dataSecurityFilter.setFieldName(fieldName);
						dataSecurityFilter.setFilterValues(partnerPrivate.getPartnerCode().toString());
						dataSecurityFilter.setCorpID(sessionCorpID);
						dataSecurityFilter.setCreatedOn(new Date());
						dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
						dataSecurityFilter.setUpdatedOn(new Date());
						dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
						dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
						dataSecuritySet.addFilters(dataSecurityFilter);
					}
					dataSecuritySet.setName(prefixDataSet);
					dataSecuritySet.setDescription(prefixDataSet);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySet.setCreatedOn(new Date());
					dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
					dataSecuritySet.setUpdatedOn(new Date());
					dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
				}
				List associatedList = new ArrayList();
				boolean childViewActive = (partnerPrivate.getViewChild() == (true));
				if (childViewActive) {
					getChildAgentlist(partnerPrivate.getPartnerCode(), associatedList);
					partnerPublic = partnerPublicManager.get(new Long(partnerId));
					associatedList.add(partnerPublic.getAgentParent());
					Iterator it2 = associatedList.iterator();
					while (it2.hasNext()) {
						String pcode = it2.next().toString().trim();
						if (!pcode.equalsIgnoreCase("")) {
							String prefix1 = "DATA_SECURITY_FILTER_SO_" + pcode;
							String prefixDataSet1 = "DATA_SECURITY_SET_SO_RLOVENDORCODE_" + pcode;
							List agentFilterList1 = dataSecurityFilterManager.getAgentFilterList(prefix1, sessionCorpID);
							List agentFilterSetList1 = dataSecuritySetManager.getDataSecuritySet(prefixDataSet1, sessionCorpID);
							if ((agentFilterList1 ==null || agentFilterList1.isEmpty()) && (agentFilterSetList1 == null || agentFilterSetList1.isEmpty())) {
								List nameList = new ArrayList();
								nameList.add("CAR_vendorCode");
								nameList.add("COL_vendorCode");
								nameList.add("TRG_vendorCode");
								nameList.add("HOM_vendorCode");
								nameList.add("RNT_vendorCode");
								nameList.add("SET_vendorCode");
								
								nameList.add("LAN_vendorCode");
								nameList.add("MMG_vendorCode");
								nameList.add("ONG_vendorCode");
								nameList.add("PRV_vendorCode");
								nameList.add("EXP_vendorCode");
								nameList.add("RPT_vendorCode");
								
								nameList.add("SCH_schoolSelected");
								nameList.add("TAX_vendorCode");
								nameList.add("TAC_vendorCode");
								nameList.add("TEN_vendorCode");
								nameList.add("VIS_vendorCode");
								nameList.add("WOP_vendorCode");
								
								nameList.add("REP_vendorCode");
								nameList.add("CAT_vendorCode");
								nameList.add("CLS_vendorCode");
								nameList.add("CHS_vendorCode");
								nameList.add("DPS_vendorCode");
								nameList.add("HSM_vendorCode");
								
								
								
								
								nameList.add("PDT_vendorCode");
								nameList.add("RCP_vendorCode");
								nameList.add("SPA_vendorCode");
								nameList.add("TCS_vendorCode");
								nameList.add("MTS_vendorCode");
								nameList.add("DSS_vendorCode");
								
								nameList.add("HOB_vendorCode");
								nameList.add("FRL_vendorCode");
								nameList.add("APU_vendorCode");
								nameList.add("FLB_vendorCode");
								nameList.add("INS_vendorCode");
								nameList.add("INP_vendorCode");
								
								
								nameList.add("EDA_vendorCode");
								nameList.add("TAS_vendorCode");
								nameList.add("AIO_vendorCode");
								nameList.add("RLS_vendorCode");
								dataSecuritySet = new DataSecuritySet();
								Iterator it3 = nameList.iterator();
								while (it3.hasNext()) {
									String fieldName = it3.next().toString();
									String prefixdata="DATA_SECURITY_FILTER_SO_";
									String postFix = prefix;
									String fieldNamedata = postFix.replace("DATA_SECURITY_FILTER_SO_", "");
									String postFixtemp= prefixdata+fieldName+"_"+fieldNamedata;
									dataSecurityFilter = new DataSecurityFilter();
									dataSecurityFilter.setName(postFixtemp);
									dataSecurityFilter.setTableName("serviceorder");
									dataSecurityFilter.setFieldName(fieldName);
									dataSecurityFilter.setFilterValues(pcode);
									dataSecurityFilter.setCorpID(sessionCorpID);
									dataSecurityFilter.setCreatedOn(new Date());
									dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
									dataSecurityFilter.setUpdatedOn(new Date());
									dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
									dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
									dataSecuritySet.addFilters(dataSecurityFilter);
								}
								dataSecuritySet.setName(prefixDataSet1);
								dataSecuritySet.setDescription(prefixDataSet1);
								dataSecuritySet.setCorpID(sessionCorpID);
								dataSecuritySet.setCreatedOn(new Date());
								dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
								dataSecuritySet.setUpdatedOn(new Date());
								dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
								dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
							}
						}

					}

				}

			}
		} catch (Exception ex) {

		}
		
		partnerPrivate.setPartnerPublicId(new Long(partnerId));

	/*	if(isvalidIsoCode() == null){
			String key = "VAT # is not correct, please enter first 2 letters of ISO country code.";    			
			errorMessage(getText(key));
            return INPUT;
		} 
		else
		{
			partnerPrivate=partnerPrivateManager.save(partnerPrivate);			
		}*/
		
		
		partnerPrivate.setRatingScale(rateScale);
		if(partnerType.equalsIgnoreCase("AC") || partnerType.equalsIgnoreCase("AG")){		
		   if(partnerPrivate.getCustomerFeedback()==null || partnerPrivate.getCustomerFeedback().equalsIgnoreCase("")){			
			  if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) && company.getJustSayYes()==true && company.getQualitySurvey()==true){
				    	justSayYes="checked";
						partnerPrivate.setCustomerFeedback("Just Say Yes");
			  }else if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) && company.getJustSayYes()==true && company.getQualitySurvey()==false){
				    	justSayYes="checked";
						partnerPrivate.setCustomerFeedback("Just Say Yes");
			  }else if(partnerPrivate.getPartnerCode()!=null && partnerPrivate.getPartnerCode().equalsIgnoreCase(partnerPublic.getAgentParent()) && company.getJustSayYes()==false && company.getQualitySurvey()==true){
				    	qualitySurvey="checked";
					    partnerPrivate.setCustomerFeedback("Quality Survey");	
			  }else{	
				qualitySurvey="checked";
				partnerPrivate.setCustomerFeedback("Quality Survey");
				List parentAgentlist = partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerPublic.getAgentParent());
				if(parentAgentlist!=null && !parentAgentlist.isEmpty() && (parentAgentlist.get(0)!=null)){
					PartnerPrivate partnerPrivateParent = (PartnerPrivate) parentAgentlist.get(0);
					partnerPrivate.setNoTransfereeEvaluation(partnerPrivateParent.getNoTransfereeEvaluation());
					partnerPrivate.setOneRequestPerCF(partnerPrivateParent.getOneRequestPerCF());
					partnerPrivate.setRatingScale(partnerPrivateParent.getRatingScale());
					partnerPrivate.setDescription1(partnerPrivateParent.getDescription1());
					partnerPrivate.setDescription2(partnerPrivateParent.getDescription2());
					partnerPrivate.setDescription3(partnerPrivateParent.getDescription3());
					partnerPrivate.setLink1(partnerPrivateParent.getLink1());
					partnerPrivate.setLink2(partnerPrivateParent.getLink2());
					partnerPrivate.setLink3(partnerPrivateParent.getLink3());				
			  }
			}
		  }else{
			  if(partnerPrivate.getCustomerFeedback().equalsIgnoreCase("Just Say Yes")){
				justSayYes="checked";
				partnerPrivate.setCustomerFeedback("Just Say Yes");  
			  }else{
				qualitySurvey="checked";
				partnerPrivate.setCustomerFeedback("Quality Survey");	 
			  }
		   }
		}
		
		
		String permKey = sessionCorpID +"-"+"component.partner.vatBillingGroup.edit";
		boolean visibilityForCorpId=false;
		 visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
		 if(visibilityForCorpId){
			 String flagb="";
			 String vatBillimngGroupDes="";
			 String partnerVatDec="";
				if(vatBillingGroups.containsKey(partnerPrivate.getVatBillingGroup())){
				vatBillimngGroupDes =vatBillingGroups.get(partnerPrivate.getVatBillingGroup());	
			}
				flexcode  = refMasterManager.findPartnerVatBillingFlexCode(sessionCorpID, vatBillimngGroupDes);
			    if(vatBillimngGroupDes!=null && !vatBillimngGroupDes.equals("") && flexcode!=null && (!(flexcode.trim().equals("")))){
				euVatList = refMasterManager.findPartnerVatDescription(sessionCorpID, "EUVAT", flexcode, partnerPrivate.getDefaultVat(), "Y");
			    }
			   
		 }
		partnerPrivate=partnerPrivateManager.save(partnerPrivate); 
		accountAssignmentTypeList=accountAssignmentTypeManager.getAssignmentTypeList(partnerPrivate.getPartnerCode(), Long.parseLong(partnerId), sessionCorpID);
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		try{
			if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
			createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
		    createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
			}
	    }catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
	    	 e.printStackTrace(); 
		}
		/*if(partnerPublic.getStatus()!=null && (partnerPublic.getStatus().equalsIgnoreCase("Inactive")||partnerPublic.getStatus().equalsIgnoreCase("Blacklisted")))
		{
			partnerPrivate.setStatus(partnerPublic.getStatus());
		}*/
		if (!popupval.equalsIgnoreCase("true")) {
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "partner.added" : "partner.updated";
				saveMessage(getText(key));
			}
		}
		if(checkCodefromDefaultAccountLine!=null && !checkCodefromDefaultAccountLine.equalsIgnoreCase("")){
			try{
			String[] templateDetail = checkCodefromDefaultAccountLine.split("~");
       	 	String checkFromTemplate= templateDetail[0];
       	 	String partnerType= templateDetail[1];
       	 	String userName=getRequest().getRemoteUser();
       	 	if(checkFromTemplate.equalsIgnoreCase("Y")){
       	 		partnerPublicManager.updateDefaultAccountLine(sessionCorpID,partnerPrivate.getPartnerCode(),partnerType,userName);
       	 	}
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		getNotesForIconChange();
		try{
			if((msgFieldValueChanged!=null && !msgFieldValueChanged.equalsIgnoreCase("")) && msgFieldValueChanged.equalsIgnoreCase("Yes")){
				monthlySalesGoal = monthlySalesGoalManager.save(monthlySalesGoal);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String msgFieldValueChanged;
	private AccountAssignmentTypeManager accountAssignmentTypeManager;
	private List accountAssignmentTypeList;
	public String getChildAgentlist(String parentAgentCode, List agentList) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		List associatedAgentsList = dataSecuritySetManager.getAssociatedAgents(parentAgentCode, sessionCorpID);
		if (!associatedAgentsList.isEmpty()) {

			if (associatedAgentsList.get(0) != null) {
				String ag = associatedAgentsList.toString();
				ag = ag.replace("[", "");
				ag = ag.replace("]", "");
				String[] arrayid1 = ag.split(",");
				int arrayLength1 = arrayid1.length;
				for (int i = 0; i < arrayLength1; i++) {
					agentList.add(arrayid1[i]);
					if (getChildAgentlist(arrayid1[i], agentList) == null) {
						break;
					}

				}
			}

		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	
	public String savePrivateBankInfo(){
		Map<String,String> bankInfo=new HashMap<String,String>();
		
		String[] idandValue=null;
		
		
		String key="";
		String value="";
		String tempVal="";
		if(bankNoList.trim().toString().length()>1 && currencyList.trim().toString().length()>1)
		{
		String[] arrayStr =bankNoList.split("~");
		   for (String string : arrayStr) {
			   idandValue= string.split(":");
			   key=string.split(":")[0].toString().trim().replace("ban", "");
			   value=string.split(":")[1].toString().trim();
			   if(bankInfo.containsKey(key)){
				   tempVal=bankInfo.get(key)+"";
				   value=tempVal+"~"+value;
				   bankInfo.put(key, value);
			   }else{
				   bankInfo.put(key, value);
			   }
		   }
		   arrayStr =currencyList.split("~");
		   for (String string : arrayStr) {
			   idandValue= string.split(":");
			   key=string.split(":")[0].toString().trim().replace("cur", "");
			   value=string.split(":")[1].toString().trim();
			   if(bankInfo.containsKey(key)){
				   tempVal=bankInfo.get(key)+"";
				   value=tempVal+"~"+value;
				   bankInfo.put(key, value);
			   }else{
				   bankInfo.put(key, value);
			   }	   
		   }
		   arrayStr =checkList.split("~");
		   for (String string : arrayStr) {
			   idandValue= string.split(":");
			   key=string.split(":")[0].toString().trim().replace("chk", "");
			   value=string.split(":")[1].toString().trim();
			   if(bankInfo.containsKey(key)){
				   tempVal=bankInfo.get(key)+"";
				   value=tempVal+"~"+value;
				   bankInfo.put(key, value);
			   }else{
				   bankInfo.put(key, value);
			   }	   
		   }
		
		   
		}
		   Set st= new HashSet<String>();
		   for(String str:newlineid.split("~")){
			   st.add(str);
		   }
		   if(st.isEmpty()){
			   st=null;
		   }
		 for(Map.Entry<String, String> rec:bankInfo.entrySet()){
			 if(rec.getValue().split("~")[1]!=null && !rec.getValue().split("~")[1].toString().equalsIgnoreCase("")){
			 if(st!=null && !st.isEmpty() && st.contains(rec.getKey())){
				 partnerBankInformation = new PartnerBankInformation(); 
				 partnerBankInformation.setCreatedBy(getRequest().getRemoteUser());
				 partnerBankInformation.setCreatedOn(new Date());
				 
			 }else{
				 partnerBankInformation = partnerBankInfoManager.get(Long.parseLong(rec.getKey()));
			 }
				partnerBankInformation.setCorpId(sessionCorpID);		        		
				partnerBankInformation.setUpdatedBy(getRequest().getRemoteUser());
				partnerBankInformation.setUpdatedOn(new Date());
				partnerBankInformation.setBankAccountNumber(rec.getValue().split("~")[0]);
				partnerBankInformation.setCurrency(rec.getValue().split("~")[1]);
				partnerBankInformation.setStatus(Boolean.parseBoolean(rec.getValue().split("~")[2]));
				partnerBankInformation.setPartnerCode(bankPartnerCode);
				partnerBankInfoManager.save(partnerBankInformation);
			 }
			 
		 }
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerStatus = refMasterManager.findByParameter(corpId, "PARTNER_STATUS");
		billgrp = refMasterManager.findByParameter(corpId, "BILLGRP");
		billingCurrency = refMasterManager.findCodeOnleByParameter(corpId, "DEFAULT_BILLING_CURRENCY");
		sale = refMasterManager.findUser(corpId, "ROLE_SALE");
		paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
		billinst = refMasterManager.findByParameter(corpId, "BILLINST");
		billCycle = refMasterManager.findByParameter(corpId, "BILLCYCLE");
		payopt = refMasterManager.findByParameter(corpId, "PAYOPT");
		pricing = refMasterManager.findUser(corpId, "ROLE_PRICING");
		billing = refMasterManager.findUser(corpId, "ROLE_BILLING");
		payable = refMasterManager.findUser(corpId, "ROLE_PAYABLE");
		claimsUser = refMasterManager.findUser(corpId, "ROLE_CLAIM_HANDLER");
		auditor = refMasterManager.findUser(corpId, "ROLE_AUDITOR");
		coordinator = refMasterManager.findUser(corpId, "ROLE_COORD");
		classcode =  refMasterManager.findByParameter(corpId, "CLASSCODE");
		creditTerms = refMasterManager.findByParameter(corpId, "CrTerms");
		internalBillingPersonList = refMasterManager.findUser(corpId,"ROLE_INTERNAL_BILLING");
		List list=partnerPrivateManager.getDefaultContactUsersList(partnerCode, corpId);
		storageEmailType = refMasterManager.findByParameter(corpId, "STORAGEEMAILTYPE");
		billingMomentList = refMasterManager.findByParameter(corpId, "BILLINGMOMENT");
		printOptions = refMasterManager.findByParameter(sessionCorpID, "INVOICEFORMINPUTPARM");			
  	     if(list.size()>0){
			Iterator it=list.iterator();
			while (it.hasNext()){
				Object listObject = it.next();
	            String aliasName= (String) ((UserDTO)listObject).getAlias();
	            String userName= (String) ((UserDTO)listObject).getUsername();
	            
	            if(aliasName==null || aliasName.equals(""))
	            {
	            	
	            defaultContact.put(userName, userName);
	            }
	            else 
	            {
            	defaultContact.put(userName, aliasName);	
	            }
     	}}
	
		try{
		//creditTerms=CommonUtil.sortByKey(creditTerms);
		}catch(Exception e){e.printStackTrace();}
		currency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		bankCode= refMasterManager.findByParameter(corpId, "bankCode");
		collectionList = refMasterManager.findByParameter(sessionCorpID,"COLLECTION");
		lead=refMasterManager.findByParameter(sessionCorpID, "LEAD");
		hasval = refMasterManager.findByParameter(sessionCorpID, "HASVAL");
		insopt = refMasterManager.findByParameter(sessionCorpID, "INSOPT");
		vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
		euVatList = refMasterManager.findByParameterWithoutParent (sessionCorpID, "EUVAT");
//		entitlementList = entitlementManager.findEntitlementByPartnerId(corpId, Long.valueOf(partnerId));
		partnerContractList = partnerPrivateManager.findContract(sessionCorpID);
		agentClassification=refMasterManager.findByParameter(sessionCorpID, "AGENTCLASSIFICATION");
		rddBasedList = refMasterManager.findByParameter(sessionCorpID, "RDDBASED");
		companyDivisionList = customerFileManager.findCompanyDivision(sessionCorpID);
		flexCodeList=refMasterManager.findPartnerRecVatDescriptionList(sessionCorpID, "vatBillingGroup");
		getEntitle(corpId);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
		
	public void getEntitle(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		if(partnerPublic.getPartnerType()!=null && (!(partnerPublic.getPartnerType().toString().trim().equals("")))  && partnerPublic.getStatus().equalsIgnoreCase("Approved") && (partnerPublic.getPartnerType().equalsIgnoreCase("CMM") || partnerPublic.getPartnerType().equalsIgnoreCase("DMM"))){
			entitlementList1 = entitlementManager.findLinkEntitlementByPartner(Long.parseLong(partnerId));	
		}else{
		entitlementList1 = entitlementManager.findEntitlementByPartnerId(corpId, Long.parseLong(partnerId));
		}
		// Collections.sort(entitlementList);
		for (int i = 0; i < entitlementList1.size(); i++) {
			Entitlement e = (Entitlement)entitlementList1.get(i);
			entitlementList.add(e);
		}
		
		Collections.sort(entitlementList, new Comparator(){
			 
            public int compare(Object o1, Object o2) {
            	Entitlement p1 = (Entitlement) o1;
            	Entitlement p2 = (Entitlement) o2;
               return p1.geteOption().compareToIgnoreCase(p2.geteOption());
            }
 
        });
		 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	@SkipValidation
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		List notePartner = notesManager.countForPartnerNotes(partnerPrivate.getPartnerCode());
		
		if (notePartner.isEmpty()) {
			partnerNotes = "0";
		} else {
			if ((notePartner.get(0)) == "0") {
				partnerNotes = "0";
			} else {
				partnerNotes = ((notePartner).get(0)).toString();
			}
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String vatNumber;
	@SkipValidation
	public String validIsoCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		isoCodeList=partnerPrivateManager.getIsoCode(vatNumber);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String usedAmount;	
	

	@SkipValidation
	public String pickAvailableCredit(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
		if(defaultCurrency!=null && (!(defaultCurrency.trim().equalsIgnoreCase(""))))
		{ 	
		}else{
			defaultCurrency =accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();		
		}
		
		} catch(Exception e ){
			e.printStackTrace();
		}
		usedAmount=partnerPrivateManager.getUsedAmount(partnerCode,defaultCurrency,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String updatePartnerPrivateSection()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerPrivate=partnerPrivateManager.get(id);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String updatePartnerPublicSection()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		partnerPrivate=partnerPrivateManager.get(id);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String check;
	private List<User> users;
	public String findDefaultContactInfo()
	{ check="";
		 if(userName!=null){
			
		 List list=partnerPrivateManager.loadUserByUsername(userName);
		 if(list.size()>0){
				Iterator it=list.iterator();
				while (it.hasNext()){
					Object listObject = it.next();
					
						  String  firstName= (String) ((UserDTO)listObject).getFirst_name()	;
						   String  lastName= (String) ((UserDTO)listObject).getLast_name();
						   String contactName=firstName+" "+lastName;
						   if(lastName==null && lastName=="" ) 
						   {
							   contactName=contactName.trim();
							}
						    String  email= (String) ((UserDTO)listObject).getEmail();
				            String  phone= (String) ((UserDTO)listObject).getPhoneNumber();
				             check = contactName+"~"+email +"~"+phone;
					}
		   
		 }}
		return SUCCESS;
	}
private MonthlySalesGoal monthlySalesGoal;
private MonthlySalesGoalManager monthlySalesGoalManager;
private String yearVal;
@SkipValidation
	public String findMonthlySalesGoalDetails() {
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" findMonthlySalesGoalDetails() Start");
				yearList = new LinkedHashMap<String, String>();
				List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,"'YEAR'");
				for (RefMasterDTO refObj : allParamValue) {
						yearList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
				}
				Calendar now = Calendar.getInstance();
				int currentYear = now.get(Calendar.YEAR);
				if(yearVal==null || yearVal.equalsIgnoreCase("")){
					yearVal = String.valueOf(currentYear);
				}
				monthlySalesGoal = monthlySalesGoalManager.getMonthlySalesGoalDetails(partnerCode, sessionCorpID, yearVal);
				if(monthlySalesGoal.getId()==null){
					monthlySalesGoal = new MonthlySalesGoal();
					monthlySalesGoal.setCreatedOn(new Date());
					monthlySalesGoal.setCreatedBy(getRequest().getRemoteUser());
					monthlySalesGoal.setUpdatedBy(getRequest().getRemoteUser());
					monthlySalesGoal.setUpdatedOn(new Date());
					monthlySalesGoal.setCorpId(sessionCorpID);
					monthlySalesGoal.setPartnerCode(partnerCode);
					monthlySalesGoal.setYear(String.valueOf(currentYear));
				}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" findMonthlySalesGoalDetails() End");
			}catch (Exception e) {
				e.printStackTrace();
			}
		return SUCCESS;
	}

@SkipValidation
	public String  vatBillingDesc() {
	          try {
	        	  String flag="";
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
				 
				if(description!=null && !description.isEmpty() && (!(description.trim().equals("")))){
			     flexcode  = refMasterManager.findPartnerVatBillingFlexCode(sessionCorpID, description);
				 vatDescription=refMasterManager.findPartnerVatCodeDescription(flexcode,defaultVat,sessionCorpID,"Y");
				}
				else{

				     flexcode  = refMasterManager.findPartnerVatBillingFlexCode(sessionCorpID,description);
					vatDescription=refMasterManager.findPartnerVatCodeDescription(flexcode,defaultVat,sessionCorpID,flag);
				}
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
	    	return SUCCESS;
	}

@SkipValidation
public String  getSaveVatBillingGroup(){
	 try {
		 vatBillingGroupPartnerData = partnerPrivateManager.getVatBillingGroupMap(sessionCorpID, parterCodeList) ;
	 } catch (Exception e) {
			e.printStackTrace();
		} 
	return SUCCESS;
}
/*	public String isvalidIsoCode(){
		
		String vatNumber1=partnerPrivate.getVatNumber();
		vatNumber1=vatNumber1.trim();
		String vatNumber2= new String("");
		if(!vatNumber1.equalsIgnoreCase(""))
		{
			 vatNumber2=vatNumber1.substring(0, 2);	
		}
		isoCodeList=partnerPrivateManager.getIsoCode(vatNumber2);
		if(!isoCodeList.isEmpty()){
	    	return "valid" ;
	    }else{
	    	return null;
	    }		
	}*/
private List billingCycle;
public List getBillingCycle() {
	return billingCycle;
}

public void setBillingCycle(List billingCycle) {
	this.billingCycle = billingCycle;
}

public String findBillingCycle()
{
	 billingCycle=partnerPrivateManager.getBillingCycle(billToCode, sessionCorpID);
	return SUCCESS;
	

}
	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public Map<String, String> getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(Map<String, String> partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public String getExList() {
		return exList;
	}

	public void setExList(String exList) {
		this.exList = exList;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public List getPartnerPrivateList() {
		return partnerPrivateList;
	}

	public void setPartnerPrivateList(List partnerPrivateList) {
		this.partnerPrivateList = partnerPrivateList;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public Map<String, String> getBillgrp() {
		return billgrp;
	}

	public void setBillgrp(Map<String, String> billgrp) {
		this.billgrp = billgrp;
	}

	public Map<String, String> getBilling() {
		return billing;
	}

	public void setBilling(Map<String, String> billing) {
		this.billing = billing;
	}

	public Map<String, String> getBillinst() {
		return billinst;
	}

	public void setBillinst(Map<String, String> billinst) {
		this.billinst = billinst;
	}

	public Map<String, String> getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(Map<String, String> creditTerms) {
		this.creditTerms = creditTerms;
	}

	public Map<String, String> getPayable() {
		return payable;
	}

	public void setPayable(Map<String, String> payable) {
		this.payable = payable;
	}

	public Map<String, String> getClaimsUser() {
		return claimsUser;
	}

	public void setClaimsUser(Map<String, String> claimsUser) {
		this.claimsUser = claimsUser;
	}

	public Map<String, String> getPayopt() {
		return payopt;
	}

	public void setPayopt(Map<String, String> payopt) {
		this.payopt = payopt;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public void setPaytype(Map<String, String> paytype) {
		this.paytype = paytype;
	}

	public Map<String, String> getPricing() {
		return pricing;
	}

	public void setPricing(Map<String, String> pricing) {
		this.pricing = pricing;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public AccountContact getAccountContact() {
		return accountContact;
	}

	public void setAccountContact(AccountContact accountContact) {
		this.accountContact = accountContact;
	}

	public AccountProfile getAccountProfile() {
		return accountProfile;
	}

	public void setAccountProfile(AccountProfile accountProfile) {
		this.accountProfile = accountProfile;
	}

	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}

	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}

	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}

	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}

	public List getMultiplequalityCertifications() {
		return multiplequalityCertifications;
	}

	public void setMultiplequalityCertifications(List multiplequalityCertifications) {
		this.multiplequalityCertifications = multiplequalityCertifications;
	}

	public List getMultiplServiceLines() {
		return multiplServiceLines;
	}

	public void setMultiplServiceLines(List multiplServiceLines) {
		this.multiplServiceLines = multiplServiceLines;
	}

	public List getMultiplVanlineAffiliations() {
		return multiplVanlineAffiliations;
	}

	public void setMultiplVanlineAffiliations(List multiplVanlineAffiliations) {
		this.multiplVanlineAffiliations = multiplVanlineAffiliations;
	}

	public void setAccountContactManager(AccountContactManager accountContactManager) {
		this.accountContactManager = accountContactManager;
	}

	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}

	public void setDataSecurityFilterManager(
			DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}

	public void setDataSecuritySetManager(
			DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}

	public String getPartnerNotes() {
		return partnerNotes;
	}

	public void setPartnerNotes(String partnerNotes) {
		this.partnerNotes = partnerNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public Map<String, String> getCurrency() {
		return currency;
	}

	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}

	public Map<String, String> getAuditor() {
		return auditor;
	}

	public void setAuditor(Map<String, String> auditor) {
		this.auditor = auditor;
	}

	public Map<String, String> getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(Map<String, String> coordinator) {
		this.coordinator = coordinator;
	}

	public Map<String, String> getClasscode() {
		return classcode;
	}

	public void setClasscode(Map<String, String> classcode) {
		this.classcode = classcode;
	}

	public List getIsoCodeList() {
		return isoCodeList;
	}

	public void setIsoCodeList(List isoCodeList) {
		this.isoCodeList = isoCodeList;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getUsedAmount() {
		return usedAmount;
	}

	public void setUsedAmount(String usedAmount) {
		this.usedAmount = usedAmount;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public String getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public String getAgentCountryCode() {
		return agentCountryCode;
	}

	public void setAgentCountryCode(String agentCountryCode) {
		this.agentCountryCode = agentCountryCode;
	}

	public Map<String, String> getBankCode() {
		return bankCode;
	}

	public void setBankCode(Map<String, String> bankCode) {
		this.bankCode = bankCode;
	}	
	
	
	

	public Map<String, String> getEntitlementMap() {
		return entitlementMap;
	}

	public void setEntitlementMap(Map<String, String> entitlementMap) {
		this.entitlementMap = entitlementMap;
	}

	public EntitlementManager getEntitlementManager() {
		return entitlementManager;
	}

	public void setEntitlementManager(EntitlementManager entitlementManager) {
		this.entitlementManager = entitlementManager;
	}
	
	public List getRatingScale() {
		return ratingScale;
	}

	public void setRatingScale(List ratingScale) {
		this.ratingScale = ratingScale;
	}
	
	public List getCustomerFeedback() {
		return customerFeedback;
	}

	public void setCustomerFeedback(List customerFeedback) {
		this.customerFeedback = customerFeedback;
	}
	
	public String getJustSayYes() {
		return justSayYes;
	}

	public void setJustSayYes(String justSayYes) {
		this.justSayYes = justSayYes;
	}

	public String getQualitySurvey() {
		return qualitySurvey;
	}

	public void setQualitySurvey(String qualitySurvey) {
		this.qualitySurvey = qualitySurvey;
	}


	public String getRateScale() {
		return rateScale;
	}


	public void setRateScale(String rateScale) {
		this.rateScale = rateScale;
	}

	public Boolean getNoTransfereeEvaluation() {
		return noTransfereeEvaluation;
	}


	public void setNoTransfereeEvaluation(Boolean noTransfereeEvaluation) {
		this.noTransfereeEvaluation = noTransfereeEvaluation;
	}


	public Boolean getOneRequestPerCF() {
		return oneRequestPerCF;
	}


	public void setOneRequestPerCF(Boolean oneRequestPerCF) {
		this.oneRequestPerCF = oneRequestPerCF;
	}

	public List getEntitlementList1() {
		return entitlementList1;
	}

	public void setEntitlementList1(List entitlementList1) {
		this.entitlementList1 = entitlementList1;
	}

	public List<Entitlement> getEntitlementList() {
		return entitlementList;
	}

	public void setEntitlementList(List<Entitlement> entitlementList) {
		this.entitlementList = entitlementList;
	}

	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}
	public void setAccountAssignmentTypeManager(
			AccountAssignmentTypeManager accountAssignmentTypeManager) {
		this.accountAssignmentTypeManager = accountAssignmentTypeManager;
	}

	public List getAccountAssignmentTypeList() {
		return accountAssignmentTypeList;
	}

	public void setAccountAssignmentTypeList(List accountAssignmentTypeList) {
		this.accountAssignmentTypeList = accountAssignmentTypeList;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public Map<String, String> getCollectionList() {
		return collectionList;
	}

	public void setCollectionList(Map<String, String> collectionList) {
		this.collectionList = collectionList;
	}
	
	public Map<String, String> getLead() {
		return lead;
	}

	public void setLead(Map<String, String> lead) {
		this.lead = lead;
	}

	public Map<String, String> getHasval() {
		return hasval;
	}

	public void setHasval(Map<String, String> hasval) {
		this.hasval = hasval;
	}

	public Map<String, String> getInsopt() {
		return insopt;
	}

	public void setInsopt(Map<String, String> insopt) {
		this.insopt = insopt;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Map<String, String> getEuVatList() {
		return euVatList;
	}

	public void setEuVatList(Map<String, String> euVatList) {
		this.euVatList = euVatList;
	}

	public List getPartnerContractList() {
		return partnerContractList;
	}

	public void setPartnerContractList(List partnerContractList) {
		this.partnerContractList = partnerContractList;
	}

	public void setPartnerAccountRefManager(
			PartnerAccountRefManager partnerAccountRefManager) {
		this.partnerAccountRefManager = partnerAccountRefManager;
	}

	public PartnerAccountRef getPartnerAccountRef() {
		return partnerAccountRef;
	}

	public void setPartnerAccountRef(PartnerAccountRef partnerAccountRef) {
		this.partnerAccountRef = partnerAccountRef;
	}

	public Map<String, String> getVatBillingGroups() {
		return vatBillingGroups;
	}

	public void setVatBillingGroups(Map<String, String> vatBillingGroups) {
		this.vatBillingGroups = vatBillingGroups;
	}

	public Boolean getAgentCheck() {
		return agentCheck;
	}

	public void setAgentCheck(Boolean agentCheck) {
		this.agentCheck = agentCheck;
	}

	public Map<String, String> getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(Map<String, String> billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	public List getPartnerBankInfoList() {
		return partnerBankInfoList;
	}

	public void setPartnerBankInfoList(List partnerBankInfoList) {
		this.partnerBankInfoList = partnerBankInfoList;
	}

	public String getBankPartnerCode() {
		return bankPartnerCode;
	}

	public void setBankPartnerCode(String bankPartnerCode) {
		this.bankPartnerCode = bankPartnerCode;
	}

	public String getBankNoList() {
		return bankNoList;
	}

	public void setBankNoList(String bankNoList) {
		this.bankNoList = bankNoList;
	}

	public String getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(String currencyList) {
		this.currencyList = currencyList;
	}

	public String getCheckList() {
		return checkList;
	}

	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}

	public String getNewlineid() {
		return newlineid;
	}

	public void setNewlineid(String newlineid) {
		this.newlineid = newlineid;
	}

	public String getPartnerbanklist() {
		return partnerbanklist;
	}

	public void setPartnerbanklist(String partnerbanklist) {
		this.partnerbanklist = partnerbanklist;
	}

	public PartnerBankInformation getPartnerBankInformation() {
		return partnerBankInformation;
	}

	public void setPartnerBankInformation(
			PartnerBankInformation partnerBankInformation) {
		this.partnerBankInformation = partnerBankInformation;
	}

	public PartnerBankInfoManager getPartnerBankInfoManager() {
		return partnerBankInfoManager;
	}

	public void setPartnerBankInfoManager(
			PartnerBankInfoManager partnerBankInfoManager) {
		this.partnerBankInfoManager = partnerBankInfoManager;
	}

	public Map<String, String> getAgentClassification() {
		return agentClassification;
	}

	public void setAgentClassification(Map<String, String> agentClassification) {
		this.agentClassification = agentClassification;
	}

	public Map<String, String> getRddBasedList() {
		return rddBasedList;
	}

	public void setRddBasedList(Map<String, String> rddBasedList) {
		this.rddBasedList = rddBasedList;
	}

	public Map<String, String> getInternalBillingPersonList() {
		return internalBillingPersonList;
	}

	public void setInternalBillingPersonList(
			Map<String, String> internalBillingPersonList) {
		this.internalBillingPersonList = internalBillingPersonList;
	}

	public String getContainsData() {
		return containsData;
	}

	public void setContainsData(String containsData) {
		this.containsData = containsData;
	}

	public String getCheckCodefromDefaultAccountLine() {
		return checkCodefromDefaultAccountLine;
	}

	public void setCheckCodefromDefaultAccountLine(
			String checkCodefromDefaultAccountLine) {
		this.checkCodefromDefaultAccountLine = checkCodefromDefaultAccountLine;
	}

	public Map<String, String> getDefaultContact() {
		return defaultContact;
	}

	public void setDefaultContact(Map<String, String> defaultContact) {
		this.defaultContact = defaultContact;
	}

	

	public String getCheck() {
		return check;
	}

	public void setCheck(String check) {
		this.check = check;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Map<String, String> getStorageEmailType() {
		return storageEmailType;
	}

	public void setStorageEmailType(Map<String, String> storageEmailType) {
		this.storageEmailType = storageEmailType;
	}

	public Map<String, String> getBillingMomentList() {
		return billingMomentList;
	}

	public void setBillingMomentList(Map<String, String> billingMomentList) {
		this.billingMomentList = billingMomentList;
	}

	public Map<String, String> getYearList() {
		return yearList;
	}

	public void setYearList(Map<String, String> yearList) {
		this.yearList = yearList;
	}

	public String getYearVal() {
		return yearVal;
	}

	public void setYearVal(String yearVal) {
		this.yearVal = yearVal;
	}

	public MonthlySalesGoal getMonthlySalesGoal() {
		return monthlySalesGoal;
	}

	public void setMonthlySalesGoal(MonthlySalesGoal monthlySalesGoal) {
		this.monthlySalesGoal = monthlySalesGoal;
	}

	public void setMonthlySalesGoalManager(
			MonthlySalesGoalManager monthlySalesGoalManager) {
		this.monthlySalesGoalManager = monthlySalesGoalManager;
	}

	public String getMsgFieldValueChanged() {
		return msgFieldValueChanged;
	}

	public void setMsgFieldValueChanged(String msgFieldValueChanged) {
		this.msgFieldValueChanged = msgFieldValueChanged;
	}

	public List getCompanyDivisionList() {
		return companyDivisionList;
	}

	public void setCompanyDivisionList(List companyDivisionList) {
		this.companyDivisionList = companyDivisionList;
	}

	public List getMultipleCompanyDivisionList() {
		return multipleCompanyDivisionList;
	}

	public void setMultipleCompanyDivisionList(List multipleCompanyDivisionList) {
		this.multipleCompanyDivisionList = multipleCompanyDivisionList;
	}

	public Map<String, String> getPrintOptions() {
		return printOptions;
	}

	public void setPrintOptions(Map<String, String> printOptions) {
		this.printOptions = printOptions;
	}

	public Map<String, String> getBillCycle() {
		return billCycle;
	}

	public void setBillCycle(Map<String, String> billCycle) {
		this.billCycle = billCycle;
	}

	public String getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public String getVatdec() {
		return vatdec;
	}

	public void setVatdec(String vatdec) {
		this.vatdec = vatdec;
	}
	public String getFlexcode() {
		return flexcode;
	}

	public void setFlexcode(String flexcode) {
		this.flexcode = flexcode;
	}
	public List getRefMasterList() {
	return refMasterList;
}

public void setRefMasterList(List refMasterList) {
	this.refMasterList = refMasterList;
}

	public String getSelectedVatDec() {
	return selectedVatDec;
}

public void setSelectedVatDec(String selectedVatDec) {
	this.selectedVatDec = selectedVatDec;
}

	public Long getPrId() {
	return prId;
}

    public void setPrId(Long prId) {
	this.prId = prId;
    }
	public List getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(List vatDescription) {
		this.vatDescription = vatDescription;
	}
	public List getNewVatDescription() {
		return newVatDescription;
	}

	public void setNewVatDescription(List newVatDescription) {
		this.newVatDescription = newVatDescription;
	}

	public String getDescription() {
	return description;
     }

     public void setDescription(String description) {
	  this.description = description;
     }

	public String getParterCodeList() {
		return parterCodeList;
	}

	public void setParterCodeList(String parterCodeList) {
		this.parterCodeList = parterCodeList;
	}

	public Map<String, String> getVatBillingGroupPartnerData() {
		return vatBillingGroupPartnerData;
	}

	public void setVatBillingGroupPartnerData(
			Map<String, String> vatBillingGroupPartnerData) {
		this.vatBillingGroupPartnerData = vatBillingGroupPartnerData;
	}

}


