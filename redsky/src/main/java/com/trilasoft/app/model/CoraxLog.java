package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="coraxlog")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class CoraxLog {
	
	
	private String corpID; 
	private Long id;
	private String  updatedBy;
	private Date updatedOn;
	private String  createdBy;
	private Date createdOn;
	private String message; 
	private String actionName; 
	private String actionStatus; 
	private Long ticketNo;
	private Long workTicketId;
	private String shipNumber; 
	private String typeOfFlow; 
	
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("message", message)
				.append("actionName", actionName)
				.append("actionStatus", actionStatus)
				.append("ticketNo", ticketNo)
				.append("workTicketId", workTicketId)
				.append("shipNumber", shipNumber)
				.append("typeOfFlow", typeOfFlow)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.toString();
	}
		
		
		@Override
		public boolean equals(final Object other) {
			CoraxLog castOther = (CoraxLog) other;
			return new EqualsBuilder()
			.append(id, castOther.id)
			.append(corpID, castOther.corpID)
			.append(message, castOther.message)
			.append(actionName, castOther.actionName)
			.append(actionStatus, castOther.actionStatus)
			.append(ticketNo, castOther.ticketNo)
			.append(workTicketId, castOther.workTicketId)
			.append(shipNumber, castOther.shipNumber)
			.append(typeOfFlow, castOther.typeOfFlow)
			.append(updatedBy, castOther.updatedBy)
			.append(updatedOn, castOther.updatedOn)
			.append(createdBy,castOther.createdBy)
			.append(createdOn,castOther.createdOn)
			.isEquals();
		}
		
		
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(corpID)
					.append(message).append(actionName).append(actionStatus)
					.append(ticketNo).append(workTicketId).append(shipNumber)
					.append(typeOfFlow).append(updatedBy).append(updatedOn)
					.append(createdBy).append(createdOn)
					.toHashCode();
		}

		@Column
		public String getCorpID() {
			return corpID;
		}


		public void setCorpID(String corpID) {
			this.corpID = corpID;
		}

		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}

		@Column
		public String getUpdatedBy() {
			return updatedBy;
		}


		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		@Column
		public Date getUpdatedOn() {
			return updatedOn;
		}


		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}

		@Column
		public String getCreatedBy() {
			return createdBy;
		}


		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		@Column
		public Date getCreatedOn() {
			return createdOn;
		}


		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		@Column
		public String getMessage() {
			return message;
		}


		public void setMessage(String message) {
			this.message = message;
		}

		@Column
		public String getActionName() {
			return actionName;
		}

		
		public void setActionName(String actionName) {
			this.actionName = actionName;
		}

		@Column
		public String getActionStatus() {
			return actionStatus;
		}

		
		public void setActionStatus(String actionStatus) {
			this.actionStatus = actionStatus;
		}

		@Column
		public Long getTicketNo() {
			return ticketNo;
		}


		public void setTicketNo(Long ticketNo) {
			this.ticketNo = ticketNo;
		}

		@Column
		public Long getWorkTicketId() {
			return workTicketId;
		}


		public void setWorkTicketId(Long workTicketId) {
			this.workTicketId = workTicketId;
		}

		@Column
		public String getShipNumber() {
			return shipNumber;
		}


		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}

		@Column
		public String getTypeOfFlow() {
			return typeOfFlow;
		}


		public void setTypeOfFlow(String typeOfFlow) {
			this.typeOfFlow = typeOfFlow;
		}
	

}
