
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOriginServicesResult" type="{http://www.mss1.com/webservices/}ArrayOfMSSService" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOriginServicesResult"
})
@XmlRootElement(name = "GetOriginServicesResponse")
public class GetOriginServicesResponse {

    @XmlElement(name = "GetOriginServicesResult")
    protected ArrayOfMSSService getOriginServicesResult;

    /**
     * Gets the value of the getOriginServicesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSService }
     *     
     */
    public ArrayOfMSSService getGetOriginServicesResult() {
        return getOriginServicesResult;
    }

    /**
     * Sets the value of the getOriginServicesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSService }
     *     
     */
    public void setGetOriginServicesResult(ArrayOfMSSService value) {
        this.getOriginServicesResult = value;
    }

}
