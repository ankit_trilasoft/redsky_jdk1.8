<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<meta name="heading" content="insurance Detailed"/> 
<title>Insurance Detailed</title> 
<script type="text/javascript">
window.onload=function collectingRoomNew(){
	try{
		<c:forEach var="transfereeListVlaue" items="${itemInsuranceList}">
		<c:if test="${empty transfereeListVlaue.valuation}">
		var aid="newRoom${transfereeListVlaue.id}";
		document.getElementById(aid).value = '${transfereeListVlaue.room}';
		</c:if>
		</c:forEach>
	}catch(e){
	}

}
</script>
</head>
<body >
<c:set var="Valuationsum" value="0" />
<c:set var="volumesum" value="0.00" />
<s:form id="insuranceDetailed" name="insuranceDetailed"  method="post">
<s:hidden name="checkData" />
<div id="Layer1" style="width: 400px">
 <div id="otabs" style="margin:0px 0px 0px 10px;">
  <ul>
	<li><a class="current"><span>Detailed List</span></a></li>
  </ul>
 </div>
 <div class="spn"></div>
 <s:set name="itemInsuranceList" value="itemInsuranceList" scope="request"/> 
 <c:if test="${itemInsuranceList!='[]'}">
 <display:table name="itemInsuranceList" class="table" requestURI="" id="itemInsuranceList"    pagesize="20" style="margin:0px 0px 5px 0px;" >
 <display:column  title="Article">
 <s:textfield  name="newAtricle" id="newAtricle${itemInsuranceList.id}" cssClass="input-text"  value="${itemInsuranceList.atricle}"  onchange="updateInsurance('${itemInsuranceList.id}','atricle',this);"  cssStyle="width:100px;"></s:textfield>
 </display:column>
  <display:column  title="Room">
  <s:select cssClass="list-menu" cssStyle="width:100px;" id="newRoom${itemInsuranceList.id}"  list="%{roomType}"    value="'${itemInsuranceList.room}'"  onchange="updateInsurance('${itemInsuranceList.id}','room',this);"   headerKey="" headerValue=""/>
 <%-- <s:textfield cssClass="input-text" name="newRoom" id="newRoom"  value="${itemInsuranceList.room}" cssStyle="width:90px;"></s:textfield> --%>
 </display:column>
  <display:column  title="Quantity">
 <s:textfield cssClass="input-text" name="newQuantity${itemInsuranceList.id}" id="newQuantity${itemInsuranceList.id}" onchange="updateInsurance('${itemInsuranceList.id}','quantity',this),calculateTotal('${itemInsuranceList.id}',this);onlyNumeric(this);" value="${itemInsuranceList.quantity}" cssStyle="width:60px;"></s:textfield>
 </display:column>
  <display:column  title="Volume">
 <s:textfield cssClass="input-text" name="newCft${itemInsuranceList.id}" id="newCft${itemInsuranceList.id}" onchange="updateInsurance('${itemInsuranceList.id}','cft',this),calculateTotal('${itemInsuranceList.id}',this);onlyFloat(this);" value="${itemInsuranceList.cft}" cssStyle="width:60px;"></s:textfield>
 </display:column>
  <display:column  title="Total">
 <s:textfield cssClass="input-text" name="newTotal${itemInsuranceList.id}" id="newTotal${itemInsuranceList.id}" onchange="updateInsurance('${itemInsuranceList.id}','total',this);onlyFloat(this)" value="${itemInsuranceList.total}" cssStyle="width:60px;"></s:textfield>
 </display:column>
  <c:if test="${invListflag ne null && invListflag=='1'}">
    <c:set var="volumesum" value="${itemInsuranceList.total+volumesum }" /> 
    </c:if>
 <display:column  title="Valuation">
 <s:textfield cssClass="input-text" name="newValuation" id="newValuation${itemInsuranceList.id}" onchange="updateInsurance('${itemInsuranceList.id}','valuation',this);onlyFloat(this);" value="${itemInsuranceList.valuation}" cssStyle="width:80px;text-align:right;"></s:textfield>
 </display:column>
   <c:if test="${invListflag ne null && invListflag=='1'}">
    <c:set var="Valuationsum" value="${itemInsuranceList.valuation+Valuationsum }" /> 
    </c:if>
 <display:column  title="Comment">
 <s:textfield cssClass="input-text" name="newComment" id="newComment${itemInsuranceList.id}" onchange="updateInsurance('${itemInsuranceList.id}','comment',this);" value="${itemInsuranceList.comment}" cssStyle="width:150px;text-align:right;"></s:textfield>
 </display:column>
 <display:column title="Remove" style="width: 15px;" >
	<a><img align="middle" title="Remove" onclick="confirmSubmitArticle('${itemInsuranceList.serviceOrderID}','${itemInsuranceList.id}');"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
	</display:column>
 <display:footer>
	          <tr>                     
                 <td colspan="3" align="right"  class="tdheight-footer">Total Valuation</td>
                 <td colspan="1"></td>
                   <td align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volumesum}"  /> </td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${Valuationsum}"  /></td>
                     <td></td>
                     <td></td>
               </tr>
	         </display:footer> 
 </display:table>
 </c:if>
 <c:if test="${itemInsuranceList=='[]'}">
 <display:table name="itemInsuranceList" class="table" requestURI="" id="itemInsuranceList"    pagesize="20" style="margin:0px 0px 5px 0px;" >
 <display:column title="Atricle" style="width:100px;" />
 <display:column title="Room" style="width:90px;"/>
 <display:column title="Quantity" style="width:60px;"/>
 <display:column  title="Volume" style="width:30px;"/>
 <display:column title="Total" style="width:60px;"/>
 <display:column  title="Valuation"  style="width:80px;text-align:right;"/>
 <display:column  title="Comment"  style="width:150px;text-align:right;"/>
  </display:table>
 </c:if>
 <table class="detailTabLabel" border="0" style="margin-bottom:20px;">	
  <tr>
  	<c:if test="${itemInsuranceList!='[]'}">		  	
	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Add" onclick="addInsuranceDetailedRow('${itemInsuranceList.id}')" /> </td> 
   </c:if>
   <c:if test="${itemInsuranceList=='[]'}">		  	
	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Add" onclick="addInsuranceDetailedRow('')" /> </td> 
   </c:if>
</tr>
</tbody>
  </table>
  </div>
</s:form>  
<div class="spnblk"></div>
  <c:if test="${commodity=='AUTO'|| commodity=='BOAT' || commodity=='HHG/A'|| commodity=='VEH' || commodity=='MTRCY'}">
  <s:form id="vehicleDetailed" name="vehicleDetailed"  method="post">
  <s:hidden name="checkDataVehicle" />
  <div id="Layer1" style="width: 400px">
  <div id="otabs" style="margin:0px 0px 0px 10px;">
<ul>
	<li><a class="current"><span>Vehicle Detail</span></a></li>
	</ul>
	</div>	
  <div class="spn"></div> 
<display:table name="vehicleList" class="table" requestURI="" id="vehicleList"    pagesize="20" style="margin:0px 0px 5px 0px;" >
 <c:if test="${vehicleList!='[]' }"> 
  <c:if test="${vehicleList.idNumber!=vehicleIdNumber}">
	<display:column  title="Vehicle Type" >
	<c:choose>
	<c:when test="${vehicleList.vehicleType=='' || vehicleList.model=='' || vehicleList.year=='0'  || vehicleList.valuation=='0'}">
		<s:select cssClass="list-menu"  name="newVehicleType${vehicleList.id}" id="newVehicleType${vehicleList.id}" list="%{vehicleTypeList}"  headerKey="" headerValue=" "  onchange="UpdateVehicle('${vehicleList.id}','vehicleType','${vehicleList.idNumber}',this);" cssStyle="width:80px" />				
	</c:when>
	<c:otherwise>
	<c:out value="${vehicleList.vehicleType}"/>
	<input type="hidden" name="newVehicleType${vehicleList.id}" id="newVehicleType${vehicleList.id}" value="${vehicleList.vehicleType}" readonly="readonly"/>	
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column  title="Make"> 
	<c:choose>
	<c:when test="${vehicleList.vehicleType=='' || vehicleList.model=='' || vehicleList.year=='0'  || vehicleList.valuation=='0'}">
		<s:textfield cssStyle="text-align:right" name="newMake${vehicleList.id}" id="newMake${vehicleList.id}" required="true"   cssClass="input-text"    onchange="UpdateVehicle('${vehicleList.id}','make','${vehicleList.idNumber}',this);" maxlength="25" size="10"  />				
	</c:when>
	<c:otherwise>
	<c:out value="${vehicleList.make}"/>
	<input type="hidden" name="newMake${vehicleList.id}" id="newMake${vehicleList.id}" value="${vehicleList.make}" readonly="readonly"/>	
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column  title="Model"> 
	<c:choose>
	<c:when test="${vehicleList.vehicleType=='' || vehicleList.model=='' || vehicleList.year=='0'  || vehicleList.valuation=='0'}">
		<s:textfield cssStyle="text-align:right" name="newModel${vehicleList.id}" id="newModel${vehicleList.id}" required="true"   cssClass="input-text"    onchange="UpdateVehicle('${vehicleList.id}','model','${vehicleList.idNumber}',this);" maxlength="25" size="10"  />				
	</c:when>
	<c:otherwise>
	<c:out value="${vehicleList.model}"/>
	<input type="hidden" name="newModel${vehicleList.id}" id="newModel${vehicleList.id}" value="${vehicleList.model}" readonly="readonly"/>	
	</c:otherwise>
	</c:choose>
	</display:column>	
	
	<display:column  title="Year" > 
	<c:choose>
	<c:when test="${vehicleList.vehicleType=='' || vehicleList.model=='' || vehicleList.year=='0'  || vehicleList.valuation=='0'}">
		<s:textfield cssStyle="text-align:right" name="newYear${vehicleList.id}" id="newYear${vehicleList.id}" required="true" cssClass="input-text"  onchange="UpdateVehicle('${vehicleList.id}','Year','${vehicleList.idNumber}',this);onlyNumeric(this);" maxlength="8" size="10"  />				
	</c:when>
	<c:otherwise>
	<c:out value="${vehicleList.year}"/>
		<input type="hidden" name="newYear${vehicleList.id}" id="newYear${vehicleList.id}"  value="${vehicleList.year}" readonly="readonly"/>
	</c:otherwise>
	</c:choose>	
	</display:column>
	<display:column   title="Value/EUR" > 
	<c:choose>
	<c:when test="${vehicleList.vehicleType=='' || vehicleList.model=='' || vehicleList.year=='0'  || vehicleList.valuation=='0'}">
		<s:textfield cssStyle="text-align:right" name="newValueEUR${vehicleList.id}" id="newValueEUR${vehicleList.id}" required="true"  onchange="UpdateVehicle('${vehicleList.id}','valuation','${vehicleList.idNumber}',this);onlyFloat(this);"  cssClass="input-text" maxlength="8" size="10"  />				
	</c:when>
	<c:otherwise>
	<c:out value="${vehicleList.valuation}"/>
		<input type="hidden"  name="newValueEUR${vehicleList.id}" id="newValueEUR${vehicleList.id}"  value="${vehicleList.valuation}" readonly="readonly"/>
	</c:otherwise>
	</c:choose>	
	</display:column>
	<display:column title="Status" style="width: 15px;" >
	<c:if test="${vehicleList.status}">
          <input type="checkbox"  checked="checked" onclick="confirmSubmit('${vehicleList.serviceOrderId}','${vehicleList.id}',this);" />
        </c:if>
       <c:if test="${vehicleList.status==false}">
		<input type="checkbox"   onclick="confirmSubmit('${vehicleList.serviceOrderId}','${vehicleList.id}',this);" />
       </c:if>
	 
	</display:column>
 </c:if> 
 <c:if test="${vehicleList.idNumber==vehicleIdNumber}">
	<display:column title="Vehicle Type">
		<s:select cssClass="list-menu"  name="newVehicleType${vehicleList.id}" id="newVehicleType${vehicleList.id}" list="%{vehicleTypeList}"  headerKey="" headerValue=" " onchange="UpdateVehicle('${vehicleList.id}','vehicleType','${vehicleList.idNumber}',this);" cssStyle="width:80px" /> 
	</display:column>
		<display:column title="Make"> 
		<s:textfield cssStyle="text-align:right" name="newModel${vehicleList.id}" id="newModel${vehicleList.id}" required="true"   cssClass="input-text" onchange="UpdateVehicle('${vehicleList.id}','make','${vehicleList.idNumber}',this);" maxlength="25" size="10"  />
	</display:column>	
	<display:column title="Model"> 
		<s:textfield cssStyle="text-align:right" name="newModel${vehicleList.id}" id="newModel${vehicleList.id}" required="true"   cssClass="input-text" onchange="UpdateVehicle('${vehicleList.id}','model','${vehicleList.idNumber}',this);" maxlength="25" size="10"  />
	</display:column>	

	<display:column title="Year"> 
		<s:textfield cssStyle="text-align:right" name="newYear${vehicleList.id}" id="newYear${vehicleList.id}" required="true" cssClass="input-text" onchange="UpdateVehicle('${vehicleList.id}','year','${vehicleList.idNumber}',this);onlyNumeric(this);" maxlength="8" size="10"  />
	</display:column>
	<display:column title="Value/EUR"> 
		<s:textfield cssStyle="text-align:right" name="newValueEUR${vehicleList.id}" id="newValueEUR${vehicleList.id}" required="true" onchange="UpdateVehicle('${vehicleList.id}','valuation','${vehicleList.idNumber}',this);onlyFloat(this);"  cssClass="input-text" maxlength="8" size="10"  />
	</display:column>
	<display:column title="Status" style="width: 15px;" >
	<c:if test="${vehicleList.status}">
          <input type="checkbox"  checked="checked" onclick="confirmSubmit('${vehicleList.serviceOrderId}','${vehicleList.id}',this);" />
        </c:if>
       <c:if test="${vehicleList.status==false}">
		<input type="checkbox"   onclick="confirmSubmit('${vehicleList.serviceOrderId}','${vehicleList.id}',this);" />
       </c:if>
	</display:column>
</c:if> 
 
 </c:if> 
 <c:if test="${vehicleList=='[]'}">
<display:column title="Vehicle Type" />
<display:column  title="Make" />
<display:column  title="Model" />
<display:column  title="Year" />  
<display:column  title="Value/EUR" /> 
</c:if> 
</display:table>
<table class="detailTabLabel" border="0" style="margin-bottom:20px;">	
  <tr>	
  <c:if test="${vehicleList!='[]'}">	  	
	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Add" onclick="addVehicleRow('${vehicleList.id}')" /> </td> 
</c:if>
<c:if test="${vehicleList=='[]'}">	  	
	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Add" onclick="addVehicleRow('')" /> </td> 
</c:if>
</tr>
</tbody>
  </table>
  </div>
  </s:form>
  </c:if>
  <script type="text/javascript">
  function addInsuranceDetailedRow(id){
		var sid='<%=request.getParameter("sid")%>';
		var commodity='<%=request.getParameter("commodity")%>';
		if(id==''){
		var url ="addRowToDetailedInsurance.html?sid="+sid+"&commodity="+commodity+"&decorator=popup&popup=true";
		document.forms['insuranceDetailed'].action = url;
		document.forms['insuranceDetailed'].submit();
		}else{
			  var newAtricle=document.getElementById('newAtricle'+id).value;
			  var newRoom=document.getElementById('newRoom'+id).value;
			  var newQuantity=document.getElementById('newQuantity'+id).value;
			  var newCft=document.getElementById('newCft'+id).value;
			  var newTotal=document.getElementById('newTotal'+id).value;
			  var newValuation=document.getElementById('newValuation'+id).value;
			  var newComment=document.getElementById('newComment'+id).value;
			if(newAtricle=='' || newRoom==''|| newQuantity==''|| newCft==''|| newTotal==''|| newValuation==''|| newComment==''){
				 alert('Please Select Inventory Item Detail');
				 return false;			
			}else{
				var url ="addRowToDetailedInsurance.html?sid="+sid+"&commodity="+commodity+"&decorator=popup&popup=true";
				document.forms['insuranceDetailed'].action = url;
				document.forms['insuranceDetailed'].submit();				
			}			
		}
	}
  function addVehicleRow(id){
		var sid='<%=request.getParameter("sid")%>';
		var commodity='<%=request.getParameter("commodity")%>';
		 if(id==''){
			 var url ="addRowToVehicleDetailed.html?sid="+sid+"&commodity="+commodity+"&decorator=popup&popup=true";
				document.forms['vehicleDetailed'].action = url;
				document.forms['vehicleDetailed'].submit();
				}
		 else{
			 var targetVehicleType=document.getElementById('newVehicleType'+id).value;
			 var targetModel=document.getElementById('newModel'+id).value;
			 var targetYear=document.getElementById('newYear'+id).value;
			 var targetVehicleValuation=document.getElementById('newValueEUR'+id).value;
			if(targetVehicleType=='' || targetModel==''|| targetYear==''|| targetVehicleValuation==''){
				alert('Please Select Vehicle Detail');
				 return false;			
			}else{
				var url ="addRowToVehicleDetailed.html?sid="+sid+"&commodity="+commodity+"&decorator=popup&popup=true";
				document.forms['vehicleDetailed'].action = url;
				document.forms['vehicleDetailed'].submit();				
			}			
		
	}
  }
	function saveVehicleRow(){
		var sid='<%=request.getParameter("sid")%>';
		var commodity='<%=request.getParameter("commodity")%>';
		var url = "saveVehicleDetailed.html?sid="+sid+"&commodity="+commodity+"&saveBntType=yes&decorator=popup&popup=true";
		document.forms['vehicleDetailed'].action = url;
		document.forms['vehicleDetailed'].submit();
	}
	
	function confirmSubmitArticle(serviceOrderId,targetElement){
		var agree=confirm("Are you sure you wish to remove this Article Details?");
		var sid='<%=request.getParameter("sid")%>';
		var commodity='<%=request.getParameter("commodity")%>';
		var did = targetElement;
		if (agree){
			location.href="documentFileArticle.html?id="+did+"&sid="+sid+"&commodity="+commodity+"&saveBntType=yes&decorator=popup&popup=true";
		
		}else{
			return false;
		}
	}
	function confirmSubmit(serviceOrderId,targetElement,targetElementStatus){
		var agree=confirm("Are you sure you wish to decative this Item?");
		var sid='<%=request.getParameter("sid")%>';
		var commodity='<%=request.getParameter("commodity")%>';
		var did = targetElement;
		var vehicleStatus=false;
		if(targetElementStatus.checked==false){
			vehicleStatus=false;	
		}else{
			vehicleStatus=true;
		}
		if (agree){
			location.href="documentVehicleFileDelete.html?id="+did+"&sid="+sid+"&commodity="+commodity+"&vehicleStatus="+vehicleStatus+"&saveBntType=yes&decorator=popup&popup=true";

		}else{
			return false;	
		}
	}

	function calculateTotal(id,target){
		//var targetValue=target.value;
		var newQuantity='newQuantity'+id;
		var newCft='newCft'+id;
		var newTotal='newTotal'+id;
		var dfd1 = document.forms['insuranceDetailed'].elements[newQuantity].value;
		//alert("newQuantity"+dfd1)
		var dfd2 = document.forms['insuranceDetailed'].elements[newCft].value;
		//alert("newCft"+dfd2)
		if(dfd1!='' && dfd2!=''){
		var totalValue=dfd1*dfd2;
		//alert(totalValue);
		document.forms['insuranceDetailed'].elements[newTotal].value=totalValue;
		updateTotalInsurance(id,'total',totalValue);	
		}
	}
	function updateTotalInsurance(id,field,target){
		var targetValue=target;
		var id=id;
		var field=field;
		var sid='<%=request.getParameter("sid")%>';
		var url="updateInsurance.html?decorator=simple&popup=true&newField="+encodeURI(field)+"&fieldValue="+targetValue+"&sid="+sid+"&id="+id;
		http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponsePlanValue;
	    http2.send(null);
	}
	function handleHttpResponsePlanValue(){
		
	}
	/*function traceVehicleData(szDivID){
		var dfd = document.forms['vehicleDetailed'].elements['checkDataVehicle'].value;
		if(dfd == ''){
			document.forms['vehicleDetailed'].elements['checkDataVehicle'].value = szDivID;
		}else{
			if(dfd.indexOf(szDivID) >-1){
			var splitDFD = dfd.split(',');
			var dfd = '';
			for(i = 0; i<splitDFD.length ; i++){
				if(splitDFD[i].indexOf(szDivID) > -1){
					splitDFD[i] = szDivID
				}
				if(dfd == ''){
					dfd = splitDFD[i];
				}else{
					dfd = dfd+','+splitDFD[i];
				}
			}
		}
		else{
			dfd=document.forms['vehicleDetailed'].elements['checkDataVehicle'].value = dfd + ',' + szDivID;
		}
				document.forms['vehicleDetailed'].elements['checkDataVehicle'].value = dfd.replace( ',,' , ',' );
		}
	}
	function traceData(szDivID){
		var dfd = document.forms['insuranceDetailed'].elements['checkData'].value;
		if(dfd == ''){
			document.forms['insuranceDetailed'].elements['checkData'].value = szDivID;
		}else{
			if(dfd.indexOf(szDivID) >-1){
			var splitDFD = dfd.split(',');
			var dfd = '';
			for(i = 0; i<splitDFD.length ; i++){
				if(splitDFD[i].indexOf(szDivID) > -1){
					splitDFD[i] = szDivID
				}
				if(dfd == ''){
					dfd = splitDFD[i];
				}else{
					dfd = dfd+','+splitDFD[i];
				}
			}
		}
		else{
			dfd=document.forms['insuranceDetailed'].elements['checkData'].value = dfd + ',' + szDivID;
		}
				document.forms['insuranceDetailed'].elements['checkData'].value = dfd.replace( ',,' , ',' );
		}
	}*/
	function updateInsurance(id,field,target){
		var targetValue=target.value;
		var id=id;
		var field=field;
		var sid='<%=request.getParameter("sid")%>';
		var url="updateInsurance.html?decorator=simple&popup=true&newField="+encodeURI(field)+"&fieldValue="+targetValue+"&sid="+sid+"&id="+id;
		http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponsePlanValue;
	    http2.send(null);
	}
	function handleHttpResponsePlanValue(){
		
	}
	function UpdateVehicle(id,field,vehicleIdNumber,target){
		var targetValue=target.value;
		var id=id;
		var field=field;
		var vehicleIdNumber=vehicleIdNumber;
		var sid='<%=request.getParameter("sid")%>';
		var url="updateVehicle.html?decorator=simple&popup=true&newField="+encodeURI(field)+"&fieldValue="+targetValue+"&vehicleIdNumber="+vehicleIdNumber+"&sid="+sid+"&id="+id;
		http3.open("GET", url, true);
	    http3.onreadystatechange = handleHttpResponseVehicle;
	    http3.send(null);
	}
	function handleHttpResponseVehicle(){
		
	}
	var http2 = getHTTPObject();
	var http3 = getHTTPObject();
	function getHTTPObject()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}

  </script>

</body>
 