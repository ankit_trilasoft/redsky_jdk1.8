<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
    <title><fmt:message key="discountDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='discountDetail.heading'/>"/>
</head>
<s:form id="discountForm" name="discountForm"  action="saveDiscount" method="post" validate="true" enctype="multipart/form-data">
<div id="newmnav">
  <ul>
    <li id="newmnav1" style="background:#FFF"><a class="current"><span>Discount&nbsp;Detail</span></a></li>
     <li><a href="discountList.html?contract=${discounts.contract}&chargeCode=${discounts.chargeCode}&chargeId=${chargeId}&contractId=${contractId}"><span>Discount&nbsp;List</span></a></li>
  
  </ul>
</div>
<s:hidden name="discounts.id"/>
<s:hidden name="chargeCode" />
<s:hidden name="contract"/>
<s:hidden name="chargeId" />
<s:hidden name="contractId" />
<div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">  
	<table class=""  cellspacing="0" cellpadding="2" border="0" style="width:580px">  	
 	  <tr>
 			<td height="5px" colspan="4"></td>
 		</tr>
 	   <tr>
 	   	    <td  class="listwhitetext" align="right"  >Contract</td>
 	   		<td  width="2px"></td>
 			<td ><s:textfield cssClass="input-textUpper"  name="discounts.contract" size="35" maxlength="11" readonly="true" tabindex="1" /></td>	  		
 				  			
 			<td width="120px" class="listwhitetext" align="right"  >Charge Code</td>
 			<td  width="2px"></td>
 			<td ><s:textfield cssClass="input-textUpper" name="discounts.chargeCode" size="20" maxlength="11" readonly="true" tabindex="2" /></td>
 			  				  		
		</tr>
 		<tr>
 			<td  class="listwhitetext" align="right"  >Discount%</td>
 			<td  width="2px"></td>
 			<td><s:textfield cssClass="input-text" name="discounts.discount"  size="10" maxlength="11"  tabindex="3" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
 		
 			<td  class="listwhitetext" align="right"  >Discount&nbsp;Description</td>
 			<td  width="2px"></td>	  					
 			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" name="discounts.description" size="15" maxlength="11"  tabindex="4" /> 
  					
 		</tr>
 		</tbody>
 		</table>
 		</div>
 		<div class="bottom-header"><span></span></div>
</div>
</div>
<table width="780px">
<tbody>
	<tr>
	      <td align="right" class="listwhitetext" width="90"><b><fmt:message key='discounts.createdOn'/></b></td>
	<s:hidden name="discounts.createdOn"/>
	<td><s:date name="quotationCosting.createdOn" format="dd-MMM-yyyy HH:mm"/></td>
	<td width="130"><fmt:formatDate value="${discounts.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
	
	
	<td align="right" class="listwhitetext" width="100"><b><fmt:message key='discounts.createdBy' /></b></td>
	
	<c:if test="${not empty discounts.id}">
		<s:hidden name="discounts.createdBy"/>
		<td ><s:label name="createdBy" value="%{discounts.createdBy}"/></td>
	</c:if>
	<c:if test="${empty discounts.id}">
		<s:hidden name="discounts.createdBy" value="${pageContext.request.remoteUser}"/>
		<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
	</c:if>
	<td align="right" class="listwhitetext" width="90"><b><fmt:message key='discounts.updatedOn'/></b></td>
	<s:hidden name="discounts.updatedOn"/>
	<td width="130"><fmt:formatDate value="${discounts.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
	<td align="right" class="listwhitetext" width="90"><b><fmt:message key='discounts.updatedBy' /></b></td>
	<c:if test="${not empty discounts.id}">
				<s:hidden name="discounts.updatedBy"/>
				<td><s:label name="updatedBy" value="%{discounts.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty discounts.id}">
				<s:hidden name="discounts.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
	</tr>
</tbody>
</table>  
<s:submit cssClass="cssbuttonA" method="save" key="button.save"  theme="simple" />
<s:reset cssClass="cssbutton1" key="Reset" />
</s:form>
<script type="text/javascript">
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
}
</script>
	