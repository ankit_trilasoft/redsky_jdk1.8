package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.PartnerAccountRefDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.RefMaster;

public class PartnerAccountRefDaoHibernate extends GenericDaoHibernate<PartnerAccountRef, Long> implements PartnerAccountRefDao{
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	public PartnerAccountRefDaoHibernate(){
		super(PartnerAccountRef.class);
	}
	public List getPartnerAccountReferenceList(String corpId,String partnerCodeForRef){
		//return getHibernateTemplate().find("from PartnerAccountRef where corpID='"+corpId+"' and partnerCode ='"+partnerCodeForRef+"' ");
		List reflist=new ArrayList();
		List list= this.getSession().createSQLQuery("select id,companyDivision,accountCrossReference,refType from partneraccountref where corpID  in ('"+corpId+"') and partnerCode ='"+partnerCodeForRef+"' ").list();
		Iterator it=list.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			RefDTO refDTO=new RefDTO();
			refDTO.setId(Long.parseLong(row[0].toString()));
	        if(row[1] == null){refDTO.setCompanyDivision("");}else{refDTO.setCompanyDivision(row[1].toString());} 
	        if(row[2] == null){refDTO.setAccountCrossReference("");}else{refDTO.setAccountCrossReference(row[2].toString());}
	        if(row[3]==null){refDTO.setRefType("");}else{refDTO.setRefType(row[3].toString());}
	        reflist.add(refDTO);
		}
		return reflist;
		
	}
	public List findCompanyDivisionByCorpId(String corpId){
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where corpID='"+corpId+"'");
	}
	public List getPartnerList(String partnerCode, String sessionCorpID) {
		List l=getHibernateTemplate().find("FROM PartnerPublic WHERE partnerCode='"+partnerCode+"'  group by partnercode");
		return l;
	}
	public List checkCompayCode(String companyDivision, String corpID,String partnerCode ){
		return getHibernateTemplate().find("select companyDivision from PartnerAccountRef where companyDivision='"+companyDivision+"' and partnerCode='"+partnerCode+"' ");
	}
	public List checkCompayCodeAccount(String companyDivision, String corpID,String partnerCode,String refType){
		return getHibernateTemplate().find("select count(companyDivision) from PartnerAccountRef where companyDivision='"+companyDivision+"' and partnerCode='"+partnerCode+"' and refType='"+refType+"' ");
	}
	public List checkAccountReference(String AccountReference, String corpID, String partnerCode, String refType){
		return getHibernateTemplate().find("select count(accountCrossReference) from PartnerAccountRef where accountCrossReference='"+AccountReference+"' and partnerCode='"+partnerCode+"' and refType='"+refType+"' ");
	}
	public int updateDeleteStatus(Long ids){
		return getHibernateTemplate().bulkUpdate("delete PartnerAccountRef where id='"+ids+"'");
	}
	
	public class RefDTO{
		
		private Object id;
		private Object companyDivision;
		private Object accountCrossReference;
		private Object refType;
		
		public Object getRefType() {
			return refType;
		}
		public void setRefType(Object refType) {
			this.refType = refType;
		}
		public Object getAccountCrossReference() {
			return accountCrossReference;
		}
		public void setAccountCrossReference(Object accountCrossReference) {
			this.accountCrossReference = accountCrossReference;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		
	}

	public String findAccountInterface(String sessionCorpID) {
		String accountInterface="";
		List accountInterfaceList= getHibernateTemplate().find("select companyDivisionAcctgCodeUnique from SystemDefault where corpid='"+sessionCorpID+"'");
		if(accountInterfaceList.get(0)!=null){
			accountInterface=accountInterfaceList.get(0).toString();
		}
		return accountInterface;
	}
	public Map<String, String> getActgCodeMap(String sessionCorpID) {

		//List<PartnerAccountRef> list;
		List list = this.getSession().createSQLQuery("select concat(partnerCode,'~',if(accountCrossReference is not null or accountCrossReference<>'' ,accountCrossReference , '')) from partneraccountref where  refType='R' and accountCrossReference is not null and accountCrossReference <>'' and  corpID  in ('"+sessionCorpID+"')").list();

		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator itr = list.iterator();
		while(itr.hasNext()){
			String temp = itr.next().toString();
			String ref [] = temp.split("~");
			if(!(parameterMap.containsKey(ref[0]))){
				if(ref.length >1){
					parameterMap.put(ref[0],ref[1]);
				}else{
					parameterMap.put(ref[0],"");
				}
			}
		}
		/*for (PartnerAccountRef partnerAccountRef : list) {
			if(!(parameterMap.containsKey(partnerAccountRef.getPartnerCode()))){
			parameterMap.put(partnerAccountRef.getPartnerCode(), partnerAccountRef.getAccountCrossReference());
			}
		}*/
		return parameterMap;
	
	}
	
	public Map<String, String> getPayableReffNumber(String sessionCorpID) {

		//List<PartnerAccountRef> list;
		List list = this.getSession().createSQLQuery("select concat(partnerCode,'~',if(accountCrossReference is not null or accountCrossReference<>'' ,accountCrossReference , '')) from partneraccountref where  refType='P' and accountCrossReference is not null and accountCrossReference <>'' and corpID  in ('"+sessionCorpID+"')").list();

		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator itr = list.iterator();
		while(itr.hasNext()){
			String temp = itr.next().toString();
			String ref [] = temp.split("~");
			if(!(parameterMap.containsKey(ref[0]))){
				if(ref.length >1){
					parameterMap.put(ref[0],ref[1]);
				}else{
					parameterMap.put(ref[0],"");
				}
			}
		}
		/*for (PartnerAccountRef partnerAccountRef : list) {
			if(!(parameterMap.containsKey(partnerAccountRef.getPartnerCode()))){
			parameterMap.put(partnerAccountRef.getPartnerCode(), partnerAccountRef.getAccountCrossReference());
			}
		}*/
		return parameterMap;
	
	}
	public List countChkAccountRef(String partnerCode, String refType,String sessionCorpID){
		return  getHibernateTemplate().find("select count(accountCrossReference) from PartnerAccountRef where partnerCode='"+partnerCode+"' and refType='"+refType+"' ");
	}
	
	public String getAccountingReffCode(String sessionCorpID,String billToCode, String currencyCode, String reftype){
		String accRefCode ="";
		List l1 = getHibernateTemplate().find("select accountCrossReference from PartnerAccountRef where corpID in ('"+sessionCorpID+"') and partnerCode='"+billToCode+"' and refType='"+reftype+"' and accountCrossReference like '"+currencyCode+"%' ");
		if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
			accRefCode = l1.get(0).toString();
			if(accRefCode.contains(currencyCode+"-")){
				accRefCode = accRefCode.replace(currencyCode+"-", "");
			}
		}
		return accRefCode;
	}
	public List getAccrefSameParentForChild(String corpId){
		List list = this.getSession().createSQLQuery("select concat(corpid,',',parentcorpid) from company where corpId<>parentcorpId and acctRefSameParent is true and parentcorpId='"+corpId+"' ").list();
		return list;
	}
	
	public void deletePartnerAccountRefFromChild(String accountcrossReference,String childCorpId,String reftype,String partnerCode){
			this.getSession().createSQLQuery("delete from partneraccountref where accountCrossReference='"+accountcrossReference+"' and corpid='"+childCorpId+"' and partnercode='"+partnerCode+"' and refType='"+reftype+"'").executeUpdate();
	}
	
	public List checkAccountReferenceForChild(String AccountReference, String corpID, String partnerCode, String refType){
		List list = this.getSession().createSQLQuery("select count(accountCrossReference) from partneraccountref where accountCrossReference='"+AccountReference+"' and partnerCode='"+partnerCode+"' and refType='"+refType+"' and corpid='"+corpID+"' ").list();
		return list;
	}
	
	
	
	
}
