package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountAssignmentType;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.service.AccountAssignmentTypeManager;

	public class AccountAssignmentTypeAction extends BaseAction implements Preparable{
		
		private Long id;
		private String sessionCorpID;
		private AccountAssignmentType accountAssignmentType;
		private AccountAssignmentTypeManager accountAssignmentTypeManager;
		private Long partnerPrivateId;
		private String partnerCode;
		private String partnerPrivateURL;
		
	public void prepare() throws Exception {		
	}

	public AccountAssignmentTypeAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	}
	
	public String edit() {
		if (id != null) {
			accountAssignmentType = accountAssignmentTypeManager.get(id);
		} else {
			accountAssignmentType = new AccountAssignmentType(); 
			accountAssignmentType.setCreatedOn(new Date());
			accountAssignmentType.setUpdatedOn(new Date());
			accountAssignmentType.setPartnerPrivateId(partnerPrivateId);
			accountAssignmentType.setPartnerCode(partnerCode);
		} 
		return SUCCESS;
	}
	private String hitFlag="0";
	public String save() throws Exception {		
		boolean isNew = (accountAssignmentType.getId() == null);  
				if (isNew) { 
					accountAssignmentType.setCreatedOn(new Date());
					accountAssignmentType.setCreatedBy(getRequest().getRemoteUser());
		        }
				accountAssignmentType.setUpdatedOn(new Date());
				accountAssignmentType.setUpdatedBy(getRequest().getRemoteUser()); 
				accountAssignmentType.setCorpId(sessionCorpID);
				accountAssignmentType=accountAssignmentTypeManager.save(accountAssignmentType);				
		    	String key = (isNew) ? "Assignment Type has been added." : "Assignment Type has been updated";
			    saveMessage(getText(key));
			    hitFlag="1";
				return SUCCESS;
	     }
	
	private List accountAssignmentTypeList;
	public String list(){ 
		//accountAssignmentTypeList=accountAssignmentTypeManager.getAll();
		return SUCCESS;
	}
	
	public String delete(){		
		accountAssignmentTypeManager.remove(id);  
		partnerPrivateURL = "?partnerId="+partnerPrivateId+"&partnerCode="+partnerCode+"&partnerType=AC";
	      return SUCCESS;		    
	}
	
	private List assignmentList;
	private String checkDup;
	private String assignment;
	@SkipValidation
	 public String checkAssignmentDuplicate(){
		if(partnerCode!=null && !"".equals(partnerCode.trim())){
	        assignmentList = accountAssignmentTypeManager.checkAssignmentDuplicate(partnerCode,sessionCorpID,assignment);
	        if(assignmentList != null && assignmentList.size()>0){
	        	checkDup = "YES";
	        }else{
	        	checkDup = "NO";
	        }
		}
		return SUCCESS; 
	  }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public AccountAssignmentType getAccountAssignmentType() {
		return accountAssignmentType;
	}

	public void setAccountAssignmentType(AccountAssignmentType accountAssignmentType) {
		this.accountAssignmentType = accountAssignmentType;
	}

	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public List getAccountAssignmentTypeList() {
		return accountAssignmentTypeList;
	}

	public void setAccountAssignmentTypeList(List accountAssignmentTypeList) {
		this.accountAssignmentTypeList = accountAssignmentTypeList;
	}

	public void setAccountAssignmentTypeManager(
			AccountAssignmentTypeManager accountAssignmentTypeManager) {
		this.accountAssignmentTypeManager = accountAssignmentTypeManager;
	}

	public String getPartnerPrivateURL() {
		return partnerPrivateURL;
	}

	public void setPartnerPrivateURL(String partnerPrivateURL) {
		this.partnerPrivateURL = partnerPrivateURL;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	public List getAssignmentList() {
		return assignmentList;
	}

	public void setAssignmentList(List assignmentList) {
		this.assignmentList = assignmentList;
	}

	public String getCheckDup() {
		return checkDup;
	}

	public void setCheckDup(String checkDup) {
		this.checkDup = checkDup;
	}
	
}
