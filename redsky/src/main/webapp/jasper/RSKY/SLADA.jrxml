<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="SLADA" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="2968ce3c-59b5-4237-b86b-8c51884d1ec9">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Service Order Number" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT "SLADA-RSKY-pdf.jrxml",
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     app_user.phone_number AS app_user_phone_number,
     app_user.usertitle  AS app_user_title ,
     CONCAT(if(trim(app_user.first_name)="" || app_user.first_name is null,"",trim(app_user.first_name)),if(trim(app_user.last_name)="" || app_user.last_name is null,"",if((trim(app_user.first_name)="" || app_user.first_name is null),trim(app_user.last_name),concat(" ",trim(app_user.last_name))))) AS serviceorder_coordinator
FROM serviceorder serviceorder
LEFT OUTER JOIN `app_user` app_user ON serviceorder.`coordinator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}
WHERE serviceorder.corpID = $P{Corporate ID}
AND serviceorder.`shipNumber` = $P{Service Order Number}]]>
	</queryString>
	<field name="SLADA-RSKY-pdf.jrxml" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="app_user_phone_number" class="java.lang.String"/>
	<field name="app_user_title" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="467" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-1" x="5" y="332" width="530" height="55" forecolor="#0019A9" uuid="6fab1a94-fdba-4e58-8182-8b8d4621629f"/>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" x="249" y="5" width="284" height="34" uuid="44e0f61e-c53a-4a9e-9b77-c955a7c7ea78"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="18" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["Service Level Agreement"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-2" x="259" y="40" width="274" height="34" uuid="600033e7-c669-4197-8f7c-2c90941c3074"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["Reg # "+($F{serviceorder_shipNumber}==null ? "" : $F{serviceorder_shipNumber})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-3" x="0" y="53" width="250" height="34" uuid="8bcbf603-2e9f-4ca4-bbb1-7676ffb50fdc"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["815 South Main Street, 4th Floor Jacksonville, Florida 32207"+"\n"+
"Phone 800-874-3833 Fax 904-858-1201"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-4" x="5" y="122" width="530" height="41" uuid="25065ccd-a1b7-46f7-9df8-7042ced3ac24"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["To ensure that our client receives the highest level of service, our supplier provider commits to provide the"+"\n"+
"following service, and to adhere to the following guidelines:"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-8" x="5" y="149" width="530" height="34" uuid="aa6ca590-cb8d-4542-8cea-555025f78dd1"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" size="18" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["Service Commitments – Destination Agents will:"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-1" x="5" y="176" width="530" height="189" uuid="68e8c075-72d9-425e-82b0-283f67f9d40b"/>
				<textElement textAlignment="Justified"/>
				<text><![CDATA[• Confirm to Suddath International 24 hours before pack date to confirm crew arrival and start time.
• Must be in uniform.
• Minimum of two crew members must be assigned to every shipment.
• Contact customer 24 hours before delivery date to confirm crew arrival and start time.
• Provide only Internationally trained crews on all Suddath/Sentry International jobs.
• Crew must bring basic tools to residence for assembly and/or dismantling of normal items.
• Call Suddath International counselor from residence if client refuses full unpack or if they require
  additional services not previously authorized.
• Provide Signed Inventories and Suddath Bill of Lading within 48 hour of delivery.
• Invoice Suddath/Sentry International within 5-7 business days after delivery.
• Offer client full unpacking and basic reassembly on all Suddath/Sentry International jobs.
• Remove debris from residence same day as unpacking.
• Remove debris from shipping containers same day or morning after delivery.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-2" x="5" y="330" width="530" height="73" forecolor="#0019A9" uuid="301375b4-3f3f-4c00-8e75-c46202309ec2"/>
				<box topPadding="5" leftPadding="3">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font size="10"/>
				</textElement>
				<text><![CDATA[• Seal must remain unbroken to show client at residence. Walkboards should be attached to the bottom of the chassis
  and all other materials must be carried on the back of the tractor or inside the cab, space provided.
• After unloading and removing debris from container, a 7-point inspection must be done. Please see
  instructions and form attached.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="5" y="392" width="530" height="26" uuid="d57e4725-9df6-4f3f-a1ca-01ce426903d3"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font size="12" isItalic="true" pdfFontName="Helvetica-Oblique"/>
				</textElement>
				<text><![CDATA[Please call me with any questions regarding this shipment. Thank you!!!]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-9" x="5" y="411" width="530" height="56" uuid="5ab48ab1-b3ed-42ad-86af-9dc8ec321968"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font size="12" isItalic="true" pdfFontName="Helvetica-Oblique"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{serviceorder_coordinator}==null ? "" : $F{serviceorder_coordinator})+"\n"+($F{app_user_title}==null ? "" : $F{app_user_title})]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Left" vAlign="Top" onErrorType="Blank">
				<reportElement key="image-1" x="1" y="1" width="230" height="50" uuid="22504397-8fcb-4f67-bf13-8fe972780530"/>
				<imageExpression><![CDATA["../../images/SuddathLogo.jpg"]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
