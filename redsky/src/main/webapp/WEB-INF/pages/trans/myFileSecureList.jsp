<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="<fmt:message key='myFileList.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>

<script>
function performAction(targetElement,myFileFor,myFileListId,fileId, fileNameFor,secure,forQuotation,ppType,fileContentType, from){
	var action = targetElement.value;
	if(action=='Edit'){
		var agree=confirm("Are you sure you wish to edit this file?");
		if (agree){
			if(myFileFor!='PO'){
				location.href="editFileUpload.html?fid="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&secure="+secure+"&forQuotation="+forQuotation;
			}
			if(myFileFor=='PO'){
				location.href="editFileUpload.html?fid="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&secure="+secure+"&ppType="+ppType;
			}
		}else{
			return false;
		}
	}else if(action=='Split'){
		var agree=confirm("Are you sure you wish to split this file?");
		if (agree){
			if(fileContentType == 'application/pdf' && myFileFor!='PO'){
	           location.href="docSplit.html?fileId="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&from=main&relatedDocs=No&secure="+secure+"&forQuotation="+forQuotation;
			}else{
				alert('Only pdf documents can be split to TransDoc.');
			}
		}else{
			return false;
		}
	}else if(action=='Remove'){
			var agree=confirm("Are you sure you wish to remove this file?");
			if (agree){
				confirmSubmit(myFileListId);
			}else{
				return false;
			}
	}else{
		return false;
	}
	
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this file?");
	var cid = document.forms['myFileForm'].elements['customerFile.id'].value;
	var sid = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var did = targetElement;
    if(sid == ''){
		location.href="deleteDoc.html?id="+cid+"&did="+did+"&myFileFor=CF&myFileFrom=CF&relatedDocs=No&active=true&secure=true&forQuotation=${forQuotation}";
    }else{
		location.href="deleteDoc.html?id="+sid+"&did="+did+"&myFileFor=SO&myFileFrom=SO&relatedDocs=No&active=true&secure=true&forQuotation=${forQuotation}";
	}
}

function generatePortalId() {
		var daReferrer = document.referrer; 
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to e-mail.');
		}else{
			window.open('attachMail.html?checkBoxId='+checkBoxId+'&decorator=popup&popup=true&from=file','','width=650,height=170');
		}
} 

function checkStatusId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

function checkStatusPartnerId(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	


var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

// 1 visible, 0 hidden   
function userStatusCheck(target){
		var targetElement = target;
		var tempValue=targetElement.value.split("`");
		var ids=tempValue[0];		
		var fileName=tempValue[1];
		var transDoc = tempValue[2];

		if(targetElement.checked){
      		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['myFileForm'].elements['userCheck'].value = ids;
      		}else{
      			var userCheckStatus=	document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
      			document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
      		
      		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
      		if(userCheckFileName == ''){
	  			document.forms['myFileForm'].elements['userCheckFile'].value = fileName;
      		}else{
      			var userCheckFileName =	document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName + ',' + fileName;
      			document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( ',,' , ',' );
      		}
      		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
      		if(userCheckTransDoc == ''){
	  			document.forms['myFileForm'].elements['userTransDocStatus'].value = transDoc;
      		}else{
      			var userCheckTransDoc =	document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc + ',' + transDoc;
      			document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( ',,' , ',' );
      		}

    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
     		
     		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
     		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( fileName , '' );
     		document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( ',,' , ',' );

     		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
     		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( transDoc , '' );
     		document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( ',,' , ',' );
       		}
}

function downloadDoc(){
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
		}
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo='+seqNo;
			location.href=url;
		}
} 
function emailDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=EMAIL';
			location.href=url;
		}
}

function checkAll(){
	document.forms['myFileForm'].elements['userCheck'].value = "";
	document.forms['myFileForm'].elements['userCheckFile'].value = "";
	
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	document.forms['myFileForm'].elements['userCheck'].value="";
	document.forms['myFileForm'].elements['userCheckFile'].value="";
}
function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}


function conbinedDoc(){
	var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
	var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
	
	var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	if(seqNo == '' ){
		seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
	}
	
	var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
	name +=	' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
	
	if(checkBoxFile != ''){
		checkBoxFile = checkBoxFile.trim();
		if (checkBoxFile.indexOf(",") == 0) {
			checkBoxFile = checkBoxFile.substring(1);
		}
		if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
			checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
		}
		
		var arrayid = checkBoxFile.split(",");
		var arrayLength = arrayid.length;
		
		for (var i = 0; i < arrayLength; i++) {
			var fName = arrayid[i];
			if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf' && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
				alert('Please select only PDF Files.');
				return false;
			}
		}
	}
	
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select one or more document to merge.');
	}else{
		var url = 'conbinedDoc.html?rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
		location.href=url;
	}
}

function transDoc(){
var id = document.forms['myFileForm'].elements['fileId'].value;
var fileNameFor = document.forms['myFileForm'].elements['fileNameFor'].value;
var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
var transDocStatus = document.forms['myFileForm'].elements['userTransDocStatus'].value;
var transDocArray = transDocStatus.split(',');
var count=0;
for(var i=0;i<transDocArray.length ; i++)
{
	if(transDocArray[i]=='UPLOADED'){
		count++;
	}
}
if(count>0){
	    var agree = confirm("You have chosen to upload files that were previously sent. Do you wish to upload them again?");
	if(agree){
		}else{return false;}
}
var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
if(seqNo == '' ){
	seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
}

var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
name +=	' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;

if(checkBoxFile != ''){
	checkBoxFile = checkBoxFile.trim();
	if (checkBoxFile.indexOf(",") == 0) {
		checkBoxFile = checkBoxFile.substring(1);
	}
	if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
		checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
	}
	
	var arrayid = checkBoxFile.split(",");
	var arrayLength = arrayid.length;
	for (var i = 0; i < arrayLength; i++) {
		var fName = arrayid[i];
		if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf' && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
			alert('Only pdf documents can be transferred to TransDoc.');
			return false;
		}
	}
}

if(checkBoxId =='' || checkBoxId ==','){
	alert('Please select document to transfer.');
}else{
	var url = 'docTransferSecure.html?id='+id+'&myFileFor='+fileNameFor+'&rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
	location.href=url;
}
}

</script>

   
<style>
  

#mainPopup {
padding-left:10px;
padding-right:10px;
}

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>


<s:form id="myFileForm" action="searchMyFiles" method="post" >  
<c:set var="soCoordinator" value="${fn:trim(serviceOrder.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(user1.username)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />
<c:set var="appUserCorpid" value="${fn:trim(user1.corpID)}" />

<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>

<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />

<s:hidden name="userCheck"/> 
<s:hidden name="userCheckFile"/>
<s:hidden name="customerFile.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="workTicket.ticket" />
<c:set var="salesPortalAccess" value="false" />
<s:hidden name="myFileForVal" value="<%=request.getParameter("myFileFor")%>" />
<s:hidden name="noteForVal" value="<%=request.getParameter("noteFor")%>" />
<s:hidden name="activeVal" value="<%=request.getParameter("active")%>" />
<s:hidden name="secureVal" value="<%=request.getParameter("secure")%>" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden id="forQuotation" name="forQuotation" />
<s:hidden name="userTransDocStatus" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<c:if test="${myFileFor =='CF'}">
	<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
	<c:set var="noteID" value="${TempnotesId}" scope="session"/>
	<c:set var="custID" value="" scope="session"/>
	<c:set var="noteFor" value="${noteFor}" scope="session"/>
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="buttons">   
	<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&active=true&secure=${secure}&forQuotation=${forQuotation}"/>'" value="<fmt:message key="button.upload"/>"/>
</c:set> 
<c:if test="${myFileFor!='CF'}"> 
<s:hidden name="customerFile.firstName" />
<s:hidden name="customerFile.lastName" />
<s:hidden name="customerFile.sequenceNumber" />
<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
<c:set var="noteID" value="${TempnotesId}" scope="session"/>
<c:set var="noteFor" value="${noteFor}" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:if test="${myFileFor!='PO'}">
<div id="layer6" style="width:100%; ">

<div id="newmnav" style="float:left; ">
            <c:choose>
	        <c:when test="${forQuotation!='QC'}">
            <ul>
               <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
				<s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
				<c:set var="relocationServicesKey" value="" />
				<c:set var="relocationServicesValue" value="" /> 
			    <c:forEach var="entry" items="${relocationServices}">
					<c:if test="${relocationServicesKey==''}">
					<c:if test="${entry.key==serviceOrder.serviceType}">
					<c:set var="relocationServicesKey" value="${entry.key}" />
					<c:set var="relocationServicesValue" value="${entry.value}" /> 
					</c:if>
					</c:if> 
               </c:forEach>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	           <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
	            	</c:if>
	            <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
	            	</c:if>
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		              <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			        <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	              </c:if>
	              </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         	 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         	 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         	 </sec-auth:authComponent>
	          	 </c:if>
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			     <c:if test="${serviceOrder.job !='RLO'}"> 
			     		<c:if test="${forwardingTabVal!='Y'}"> 
	   						<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
	  					<c:if test="${forwardingTabVal=='Y'}">
	  						<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
			     </c:if>
			     </c:if>	
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             <c:if test="${serviceOrder.job !='RLO'}"> 
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>	
		             </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>

	              <c:if test="${serviceOrder.job =='RLO'}">  
                  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                 </c:if>
                 <c:if test="${serviceOrder.job !='RLO'}"> 
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	              <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </c:if>
	             </c:if>	
	             </sec-auth:authComponent>
	              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             	</c:if>
	             	</c:if>
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	       </ul>
	        </c:when>
	       <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		</c:otherwise></c:choose>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
      </c:if>
</div>
</div>
 </c:if>
 <c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:95%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
	<div id="newmnav">
		   <ul>
		  <c:if test="${customerFile.controlFlag=='A'}">
		    <li><a href="editOrderManagement.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		    </c:if> 
		    <c:if test="${customerFile.controlFlag!='A'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		     <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li> 
		    </c:if>
		    <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		    </c:if>
		    <c:if test="${salesPortalAccess=='false'}"> 
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		    </c:if>
		    </c:if> 
          </ul>
		</div>
	 </c:when>
	 <c:otherwise>
		<div id="newmnav">
		   <ul>
            <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 
          </ul>
		</div></c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top:3px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:90%">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>  
<div id="Layer1" style="width:100%;">
		<div id="fc-newmnav">
			<ul>
				<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}"><span>Document List</span></a></li>
				<li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}"><span>Related Docs</span></a></li>
				<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&noteFor=${noteFor}&forQuotation=${forQuotation}"><span>Waste Basket</span></a></li>
				<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=true&forQuotation=${forQuotation}"><span>Secure List<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
			    <li><a href="checkListFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Check List</span></a></li>
			
			<c:if test="${myFileFor=='PO'}">
				        <c:if test="${!param.popup}"> 
						<li><a href="editPartnerPublic.html?id=${fileId}&partnerType=AG" ><span>Agent Detail</span></a></li>
					</c:if>
				    </c:if>	
				<%-- <li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> --%>
			</ul>
		</div>
		<div id="chkAllButton"  class="listwhitetext" style="display:none;float: left; !position: absolute; !margin-left: 450px; !margin-top: -25px;" >
			<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
			<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
		</div>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>



<table class="" cellspacing="0" cellpadding="0" border="0" style="width:1050px;!padding-top:3px;">
	<tbody>
		<tr>
			<td>
				<s:set name="myFiles" value="myFiles" scope="request"/> 
				<c:set var="myFilesView" value="N" />
				<display:table name="myFiles" class="table-fc" requestURI="" id="myFileList" export="true" defaultsort="${fieldName}"  defaultorder="${sortOrder}" pagesize="100" style="width:100%;" >
				<c:choose>
				<c:when test="${appUserName eq soCoordinator && myFileFor =='SO' && appUserCorpid !='UNIP'}">
				   
				    <display:column title=" " style="width:30px;"><input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/></display:column>
				    <display:column property="fileType" sortable="true" title="Document&nbsp;Type" style="width:150px;"/>
				    <display:column sortable="true" sortProperty="description" maxLength="40" title="Description" style="width:180px;">
						<a onclick="downloadSelectedFile('${myFileList.id}');">
						<c:out value="${myFileList.description}" escapeXml="false"/></a>
						</display:column>	
				    <display:column property="fileSize" sortable="true" title="Size" style="width:60px;"></display:column>
				    <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					         <display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus">
					         <c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOADED'}">
					    	Received&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='READY_TO_UPLOAD'}">
					    	Ready&nbsp;to&nbsp;transfer&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />				    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOADED'}">
					    	Sent&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOAD_FAILED'}">
					    	Send&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOAD_FAILED'}">
					    	Receive&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if></display:column>
					   	 	</c:if>
				    <display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:85px;"/>
				    <display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" style="width:100px;" />
				    
				    
				    <display:column title="Email&nbsp;Status" style="width:25px;">
						<c:url value="openMailView.html" var="url1" >
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
							<c:param name="emailStatusVal" value="${myFileList.emailStatus}"/>
						</c:url>
						<c:if test="${myFileList.emailStatus==null || myFileList.emailStatus=='' || myFileList.emailStatus==' '}">
						</c:if>						
						<c:if test="${myFileList.emailStatus!=null && myFileList.emailStatus!=''}">
							<c:forEach var="entry" items="${emailStatusList}">
							<c:if test="${myFileList.emailStatus==entry.key}">
								<c:set var="str" value="${entry.value}" />
							</c:if>
							</c:forEach>
							
						    <c:forEach var="num1" items="${fn:split(str, '^')}" varStatus="count">
						    <c:choose> 
							<c:when test="${count.last}">
 								<c:set var="strqq1" value="${num1}" />
 							</c:when>
							<c:otherwise>
								<c:set var="strqq2" value="${num1}" />
							</c:otherwise>			
							</c:choose>			    
				            </c:forEach>	
				            <c:choose> 
				            <c:when test="${strqq2 < 3}">
     				        <c:set var="strqq" value="Ready For Sending" />
				            <c:set var="emailSt" value="pendingState" />
				            </c:when>
				            <c:when test="${strqq2 > 2 && fn:indexOf(strqq1,'SaveForEmail')>-1}">
				            <c:set var="ste" value="${fn:replace(strqq1,'SaveForEmail','')}"/>
				             <c:set var="strqq" value="Email sending has failed ${strqq2} times,${ste}" />
				             <c:set var="emailSt" value="failed" />
				            </c:when>
				            <c:otherwise>
				            </c:otherwise>
				            </c:choose>
							 <c:set var="str1" value="SaveForEmail"/>
							 <c:if test="${fn:indexOf(str,str1)>-1}">
								 <c:if test="${emailSt=='pendingState'}">
								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
								 </c:if>
								 <c:if test="${emailSt!='pendingState'}">
 								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>				
								 </c:if>
							 </c:if>
							 <c:if test="${fn:indexOf(str,str1)<0}">
 							 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>				
                           </c:if>
							 
							 
							 
						</c:if>	
						 &nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/> 	
						</display:column>
				    
				    
				    <display:column title="Cust&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isCportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Acc&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isAccportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Partner&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isPartnerPortal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    <display:column title="Action" style="width:45px;font-size:9px;">
						<s:select cssClass="list-menu" cssStyle="width:72px" name="action" list="{'','Edit','Split','Remove'}" onchange="return performAction(this, '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" />
				    </display:column>
				    </c:when>
					<c:otherwise>
					<c:choose>
					<c:when test="${myFileFor !='SO'}">
					<display:column title=" " style="width:30px;"><input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/></display:column>
				    <display:column property="fileType" sortable="true" title="Document&nbsp;Type" style="width:150px;"/>
				    <display:column sortable="true" sortProperty="description" maxLength="40" title="Description" style="width:180px;">
						<a onclick="downloadSelectedFile('${myFileList.id}');">
						<c:out value="${myFileList.description}" escapeXml="false"/></a>
						</display:column>	
				    <display:column property="fileSize" sortable="true" title="Size" style="width:60px;"></display:column>
				    <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					         <display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus">
					         <c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOADED'}">
					    	Received&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='READY_TO_UPLOAD'}">
					    	Ready&nbsp;to&nbsp;transfer&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />				    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOADED'}">
					    	Sent&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOAD_FAILED'}">
					    	Send&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOAD_FAILED'}">
					    	Receive&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if></display:column>
					   	 	</c:if>
				    <display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:85px;"/>
				    <display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" style="width:100px;" />
				    
				    
				    <display:column title="Email&nbsp;Status" style="width:25px;">
						<c:url value="openMailView.html" var="url1" >
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
							<c:param name="emailStatusVal" value="${myFileList.emailStatus}"/>
						</c:url>
						<c:if test="${myFileList.emailStatus==null || myFileList.emailStatus=='' || myFileList.emailStatus==' '}">
						</c:if>						
						<c:if test="${myFileList.emailStatus!=null && myFileList.emailStatus!=''}">
							<c:forEach var="entry" items="${emailStatusList}">
							<c:if test="${myFileList.emailStatus==entry.key}">
								<c:set var="str" value="${entry.value}" />
							</c:if>
							</c:forEach>
							
						    <c:forEach var="num1" items="${fn:split(str, '^')}" varStatus="count">
						    <c:choose> 
							<c:when test="${count.last}">
 								<c:set var="strqq1" value="${num1}" />
 							</c:when>
							<c:otherwise>
								<c:set var="strqq2" value="${num1}" />
							</c:otherwise>			
							</c:choose>			    
				            </c:forEach>	
				            <c:choose> 
				            <c:when test="${strqq2 < 3}">
     				        <c:set var="strqq" value="Ready For Sending" />
				            <c:set var="emailSt" value="pendingState" />
				            </c:when>
				            <c:when test="${strqq2 > 2 && fn:indexOf(strqq1,'SaveForEmail')>-1}">
				            <c:set var="ste" value="${fn:replace(strqq1,'SaveForEmail','')}"/>
				             <c:set var="strqq" value="Email sending has failed ${strqq2} times,${ste}" />
				             <c:set var="emailSt" value="failed" />
				            </c:when>
				            <c:otherwise>
				            </c:otherwise>
				            </c:choose>
							 <c:set var="str1" value="SaveForEmail"/>
							 <c:if test="${fn:indexOf(str,str1)>-1}">
								 <c:if test="${emailSt=='pendingState'}">
								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
								 </c:if>
								 <c:if test="${emailSt!='pendingState'}">
 								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>				
								 </c:if>
							 </c:if>
							 <c:if test="${fn:indexOf(str,str1)<0}">
 							 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>				
                           </c:if>
							 
							 
							 
						</c:if>	
						 &nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/> 	
						</display:column>
				    
				    
				    <display:column title="Cust&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isCportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Acc&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isAccportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Partner&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isPartnerPortal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    <display:column title="Action" style="width:45px;font-size:9px;">
						<s:select cssClass="list-menu" cssStyle="width:72px" name="action" list="{'','Edit','Split','Remove'}" onchange="return performAction(this, '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" />
				    </display:column>
					</c:when>
					<c:when test="${myFileFor =='SO' && myFilesView =='N' && appUserCorpid !='UNIP'}">
					<c:set var="myFilesView" value="Y" />
					<display:column title=" " style="width:30px;">
					</display:column>
				    <display:column  sortable="true" title="Document&nbsp;Type" style="width:150px;">
				    You&nbsp;are&nbsp;not&nbsp;authorize&nbsp;to&nbsp;view&nbsp;Details.
				    </display:column>
				    <display:column sortable="true" sortProperty="description" maxLength="40" title="Description" style="width:180px;">
					</display:column>	
				    <display:column  sortable="true" title="Size" style="width:60px;"></display:column>
				    <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					         <display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus">
					    	</display:column>
					 </c:if>
				    <display:column  sortable="true" title="Uploaded&nbsp;On"  style="width:85px;"/>
				    <display:column  sortable="true" title="Uploaded&nbsp;By" style="width:100px;" />
				    
				    
				    <display:column title="Email&nbsp;Status" style="width:25px;">
					</display:column>
				    
				    
				    <display:column title="Cust&nbsp;Portal" style="width:25px;">
				    
				    </display:column>
				    
				    <display:column title="Acc&nbsp;Portal" style="width:25px;">
				    
				    </display:column>
				    
				    <display:column title="Partner&nbsp;Portal" style="width:25px;">
				    
					
				    </display:column>
				    <display:column title="Action" style="width:45px;font-size:9px;">
						
				    </display:column>
					</c:when>
					
					<c:when test="${myFileFor =='SO' && appUserCorpid =='UNIP'}">
				   
				    <display:column title=" " style="width:30px;"><input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/></display:column>
				    <display:column property="fileType" sortable="true" title="Document&nbsp;Type" style="width:150px;"/>
				    <display:column sortable="true" sortProperty="description" maxLength="40" title="Description" style="width:180px;">
						<a onclick="downloadSelectedFile('${myFileList.id}');">
						<c:out value="${myFileList.description}" escapeXml="false"/></a>
						</display:column>	
				    <display:column property="fileSize" sortable="true" title="Size" style="width:60px;"></display:column>
				    <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					         <display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus">
					         <c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOADED'}">
					    	Received&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='READY_TO_UPLOAD'}">
					    	Ready&nbsp;to&nbsp;transfer&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />				    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOADED'}">
					    	Sent&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOAD_FAILED'}">
					    	Send&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOAD_FAILED'}">
					    	Receive&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if></display:column>
					   	 	</c:if>
				    <display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:85px;"/>
				    <display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" style="width:100px;" />
				    
				    
				    <display:column title="Email&nbsp;Status" style="width:25px;">
						<c:url value="openMailView.html" var="url1" >
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
							<c:param name="emailStatusVal" value="${myFileList.emailStatus}"/>
						</c:url>
						<c:if test="${myFileList.emailStatus==null || myFileList.emailStatus=='' || myFileList.emailStatus==' '}">
						</c:if>						
						<c:if test="${myFileList.emailStatus!=null && myFileList.emailStatus!=''}">
							<c:forEach var="entry" items="${emailStatusList}">
							<c:if test="${myFileList.emailStatus==entry.key}">
								<c:set var="str" value="${entry.value}" />
							</c:if>
							</c:forEach>
							
						    <c:forEach var="num1" items="${fn:split(str, '^')}" varStatus="count">
						    <c:choose> 
							<c:when test="${count.last}">
 								<c:set var="strqq1" value="${num1}" />
 							</c:when>
							<c:otherwise>
								<c:set var="strqq2" value="${num1}" />
							</c:otherwise>			
							</c:choose>			    
				            </c:forEach>	
				            <c:choose> 
				            <c:when test="${strqq2 < 3}">
     				        <c:set var="strqq" value="Ready For Sending" />
				            <c:set var="emailSt" value="pendingState" />
				            </c:when>
				            <c:when test="${strqq2 > 2 && fn:indexOf(strqq1,'SaveForEmail')>-1}">
				            <c:set var="ste" value="${fn:replace(strqq1,'SaveForEmail','')}"/>
				             <c:set var="strqq" value="Email sending has failed ${strqq2} times,${ste}" />
				             <c:set var="emailSt" value="failed" />
				            </c:when>
				            <c:otherwise>
				            </c:otherwise>
				            </c:choose>
							 <c:set var="str1" value="SaveForEmail"/>
							 <c:if test="${fn:indexOf(str,str1)>-1}">
								 <c:if test="${emailSt=='pendingState'}">
								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
								 </c:if>
								 <c:if test="${emailSt!='pendingState'}">
 								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>				
								 </c:if>
							 </c:if>
							 <c:if test="${fn:indexOf(str,str1)<0}">
 							 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>				
                           </c:if>
							 
							 
							 
						</c:if>	
						 &nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/> 	
						</display:column>
				    
				    
				    <display:column title="Cust&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isCportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Acc&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isAccportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Partner&nbsp;Portal" style="width:25px;">
				    <c:if test="${myFileList.isPartnerPortal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" checked/>
				    </c:if>
					<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)"/>
					</c:if>
				    </display:column>
				    <display:column title="Action" style="width:45px;font-size:9px;">
						<s:select cssClass="list-menu" cssStyle="width:72px" name="action" list="{'','Edit','Split','Remove'}" onchange="return performAction(this, '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" />
				    </display:column>
				    </c:when>
					<c:otherwise>
					
					<display:column title=" " style="width:30px;padding:0.0em">
					</display:column>
				    <display:column  sortable="true" title="Document&nbsp;Type" style="width:150px;padding:0.0em">
				    </display:column>
				    <display:column sortable="true" sortProperty="description" maxLength="40" title="Description" style="width:180px;padding:0.0em">
					</display:column>	
				    <display:column  sortable="true" title="Size" style="width:60px;padding:0.0em"></display:column>
				    <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					         <display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus">
					    	</display:column>
					 </c:if>
				    <display:column  sortable="true" title="Uploaded&nbsp;On"  style="width:85px;padding:0.0em"/>
				    <display:column  sortable="true" title="Uploaded&nbsp;By" style="width:100px;padding:0.0em" />
				    
				    
				    <display:column title="Email&nbsp;Status" style="width:25px;padding:0.0em">
					</display:column>
				    
				    
				    <display:column title="Cust&nbsp;Portal" style="width:25px;padding:0.0em">
				    
				    </display:column>
				    
				    <display:column title="Acc&nbsp;Portal" style="width:25px;padding:0.0em">
				    
				    </display:column>
				    
				    <display:column title="Partner&nbsp;Portal" style="width:25px;padding:0.0em">
				    
					
				    </display:column>
				    <display:column title="Action" style="width:45px;font-size:9px;padding:0.0em">
						
				    </display:column>
					
					
					</c:otherwise>
					</c:choose>
				    </c:otherwise>  
				     </c:choose>
				    <display:setProperty name="paging.banner.item_name" value="document"/>   
				    <display:setProperty name="paging.banner.items_name" value="documents"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Document List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Document List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Document List.pdf"/>
				</display:table>  
  
  
			</td>
		</tr>
	</tbody>
</table>
</div>
	<c:out value="${buttons}" escapeXml="false" />
	<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" value="Email" onclick="emailDoc();"/> --%>
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="combBtn"  value="Merge PDF" onclick="return conbinedDoc();"/>
<c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
	<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:95px; font-size: 15" name="mailBtn"  value="TransDoc Xfer" onclick="return transDoc();"/>
</c:if>
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15"  value="Email" onclick=" attachDocInEmail();"/>
</s:form>

<script type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.0.3.min.js"></script>

<script>
$(document).ready(function(){
$("#Layer1 div.exportlinks").addClass('CFEXP_left');
$("#Layer1 div.exportlinks").after('<div class="CFEXP_right"></div>');
$("#Layer1 div.exportlinks a").first().css('margin-left','65px');
$("#Layer1 div.exportlinks").css('padding-top','7px'); 
$("#Layer1 div.exportlinks").css('text-transform','uppercase');
$("#Layer1 div.exportlinks a").css('text-transform','capitalize'); 
})
</script>

<script type="text/javascript"> 
try{
<c:if test="${hitFlag == 5}" >
	alert('The selected document type is not mapped to TransDoc so it will not be transferred.');
	location.href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=true";
</c:if>
<c:if test="${hitFlag == 1}" >
location.href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=true";
</c:if>
  }
  catch(e){}
  try{
    var len = document.forms['myFileForm'].elements['DD'].length;
    }
    catch(e){}
   try{
    if(len>1){
    	show('chkAllButton');
    }
    }
    catch(e){}
</script>
<script type="text/javascript">
 function recipientWithEmailStatus(position,myFileId) {
	 var myFileFor='${fileNameFor}';
		if(myFileFor=='SO')
	{
	    var jobNumber=document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
	}
		if(myFileFor=='CF')
	{
			 var jobNumber=document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	}
 var url="recipientWithEmailStatus.html?ajax=1&decorator=simple&popup=true&jobNumber="+jobNumber+"&noteFor=${noteFor}&myFileId="+myFileId;
 ajax_showTooltip(url,position);
 }
</script>
<script type="text/javascript">
function attachDocInEmail()
{
	/* var fileid = '${fn:substring(myFileList.fileId,11,14)}'; */
	var myFileFor='${fileNameFor}';
	if(myFileFor=='SO')
{
    var jobNumber=document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
}
	if(myFileFor=='CF')
{
		 var jobNumber=document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
}
var checkBoxEmailId = document.forms['myFileForm'].elements['userCheck'].value;
if(checkBoxEmailId ==''){
	alert('Please select the one or more document to e-mail.');
}else{                           
	var url = 'sendEmailFile.html?decorator=popup&popup=true&checkBoxEmailId='+checkBoxEmailId+'&jobNumber='+jobNumber+"&noteFor=${noteFor}&companyDivision=${customerFile.companyDivision}";
	window.openWindow(url,height=570,width=520);
}
}

function downloadSelectedFile(id){
	var fileIdVal = document.forms['myFileForm'].elements['fileId'].value;
	var fieldVal = document.forms['myFileForm'].elements['myFileForVal'].value;
	var fieldVal1 = document.forms['myFileForm'].elements['noteForVal'].value;
	var fieldVal2 = document.forms['myFileForm'].elements['activeVal'].value;
	var fieldVal3 = document.forms['myFileForm'].elements['secureVal'].value;
	var myFileJspName = "secureFiles";
	if(fieldVal=='SO'){
		var url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"";
	}else if(fieldVal=='CF'){
		var url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"";
	}else{}
	location.href=url;
}
var fileVal = '${resultType}';
if ((fileVal!=null && fileVal!='') && (fileVal=='errorNoFile')){
	alert('This File is Temporarily Unavailable.')
	
	var myFileForVal =  document.forms['myFileForm'].elements['myFileForVal'].value;
	var noteForVal = document.forms['myFileForm'].elements['noteForVal'].value;
	var activeVal = document.forms['myFileForm'].elements['activeVal'].value;
	var secureVal = document.forms['myFileForm'].elements['secureVal'].value;
	
	var fileIdVal = document.forms['myFileForm'].elements['fileId'].value;
	var replaceURL = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"";
	window.location.replace(replaceURL);
}
</script>
