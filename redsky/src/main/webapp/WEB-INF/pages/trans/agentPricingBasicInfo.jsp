<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>
<title>Pricing Engine</title>
	<meta name="heading" content="Pricing Wizard"/>
    <meta name="menu" content="Pricing Wizard"/>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
	<style><%@ include file="/common/calenderStyle.css"%></style>


</head>

<s:form id="agentPricing" name="agentPricing" action='saveAgentPricingBasicInfo' method="post" validate="true">
<s:hidden name="latitude" />
<s:hidden name="logtitude" />
<s:hidden name="countCheck" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" /> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden key="pricingControl.id"/>
<s:hidden key="pricingControl.pricingID"/>
<s:hidden key="pricingControl.shipNumber"/>
<s:hidden key="pricingControl.sequenceNumber"/>
<s:hidden key="tariffApplicability"/>
<s:hidden key="pricingControl.isOrigin"/>
<s:hidden key="pricingControl.isFreight"/>
<s:hidden key="pricingControl.isDestination"/>
<c:set var="serviceOrderId" value="<%=request.getParameter("serviceOrderId") %>" />
<s:hidden name="serviceOrderId" value="<%=request.getParameter("serviceOrderId") %>" /> 
<div id="newmnav" style="float:left;">
		  <ul>
		  <li id="newmnav1" ><a class="current"><span>Basic Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  <c:if test="${!param.popup}">
		    <c:if test="${not empty pricingControl.id}">
		  	<li><a onclick="return checkOriginScope();" href="agentPricingOriginInfo.html?id=${pricingControl.id}"><span>Origin Options</span></a></li>
		  	<li><a onclick="return checkFreightScope();" href="agentPricingFreightInfo.html?id=${pricingControl.id}"><span>Freight Options</span></a></li>
		  	<li><a onclick="return checkDestinationScope();" href="agentPricingDestinationInfo.html?id=${pricingControl.id}"><span>Destination Options</span></a></li>
		  	<li><a  href="agentPricingSummaryInfo.html?id=${pricingControl.id}"><span>Pricing Summary</span></a></li>
		  	<li><a href="agentPricingAllPricingInfo.html?id=${pricingControl.id}"><span>Pricing Options</span></a></li>
		  	<li><a  href="agentPricingList.html"><span>Quote List</span></a></li>
			<sec-auth:authComponent componentId="module.tab.pricingWizard.accountingTab">
			 <c:if test="${not empty pricingControl.serviceOrderId}">
			  	<li><a onclick="return checkControlFlag();"><span>Accounting</span></a></li>
		   	</c:if>	
		  </sec-auth:authComponent>
		   </c:if>
		   <c:if test="${empty pricingControl.id}">
		  	<li><a onclick="notExists()"><span>Origin Options</span></a></li>
		  	<li><a onclick="notExists()"><span>Freight Options</span></a></li>
		  	<li><a onclick="notExists()"><span>Destination Options</span></a></li>
		  	<li><a onclick="notExists()"><span>Pricing Summary</span></a></li>
		  	<li><a onclick="notExists()"><span>Pricing Options</span></a></li>
		  	<li><a href="agentPricingList.html"><span>Quote List</span></a></li>
		   </c:if>
		  </c:if>	
		   <c:if test="${param.popup}">
		  	<li><a href="agentPricingList.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"><span>Pricing Engine List</span></a></li>
		  </c:if>
		  	
		  </ul>
		</div>
		<!--<div style="float: left; font-size: 13px; margin-left: 50px;margin-top:-3px;">
		<a href="http://www.sscw.com/movingservices/international_move.html" target="_blank" style="text-decoration:underline;">
		
		<a href="#"  style="color:#330000;" onclick="javascript:window.open('http://www.sscw.com/redsky/agentportal/','help','width=1004,height=500,scrollbars=yes,left=100,top=50');">
		<img src="images/wizard-button.png" border="0"/></a></div>
		--><div class="spn">&nbsp;</div>
<table width="100%" cellpadding="0" cellspacing="0" style="margin: 0px">
<tr>
<td  height="30" style=" background-image:url(images/bg.png); background-repeat:repeat-x;">
<div style="background-image: url(images/scope_bg.png); background-repeat:no-repeat; text-align:left;">

</div>
    </td>
  </tr>
</table>
</td>
</tr>
</table>

<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;">
  <tbody>
    <tr>
      <td width="20%" valign="top" class="pleft-bg">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0px;padding: 0px;">
          <tbody>
            <tr>
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img1);">
                <div onClick="javascript:animatedcollapse.toggle('info');" style="margin:0px;" >                
                  
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img1" src="images/tab_open.png" border="0" /></a></div><div class="price_tright">&nbsp;Shipment Information</div></div>
                </div>
                </span>
                <div id="info" style="margin:0px;padding:0px;">
                  <table width="" style="margin:0px;padding:0px;">
                  	<tr>
                      <td align="left" class="listwhitetext" width="">Contract:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2"><s:select cssClass="list-menu" name="pricingControl.contract" list="%{contract}" headerKey="" headerValue="" cssStyle="width:170px"/></td>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext" width="">Base&nbsp;Currency:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2"><s:select cssClass="list-menu" name="pricingControl.baseCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:170px"/></td>
                    </tr>
                    <!--<tr>
                      <td align="left" class="listwhitetext" width="">Scope:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2"><s:select cssClass="list-menu" name="pricingControl.scope" list="%{scope}" headerKey="" headerValue="" cssStyle="width:170px"/></td>
                    </tr>
                    --><tr>
                      <td align="left" class="listwhitetext" width="">Customer&nbsp;Name:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2"><s:textfield name="pricingControl.customerName"  maxlength="45" cssStyle="width:165px" cssClass="input-text" readonly="false"/></td>
                    </tr>
                    <tr>
                      <td align="left"  class="listwhitetext" colspan="2">Expected&nbsp;Load&nbsp;Date</td>
                    </tr>
                    <tr>
                     <c:if test="${not empty pricingControl.expectedLoadDate}">
						<s:text id="expectedLoadDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="pricingControl.expectedLoadDate"/></s:text>
						<td width="20px"><s:textfield cssClass="input-text" id="expectedLoadDate" name="pricingControl.expectedLoadDate" value="%{expectedLoadDateFormattedValue}" required="true" cssStyle="width:75px;"  maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
						<td><img id="expectedLoadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty pricingControl.expectedLoadDate}">
						<td colspan="" width="20px"><s:textfield cssClass="input-text" id="expectedLoadDate" name="pricingControl.expectedLoadDate" required="true" cssStyle="width:75px;"  maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/></td>
						<td><img id="expectedLoadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext" width="">Mode:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2"><s:select cssClass="list-menu" name="pricingControl.mode" list="{'Air','Sea'}" headerKey="" headerValue="" cssStyle="width:170px" onchange="selectMode(),changeText();openSection();"/></td>
                    </tr>
                    <tr>
                      <td class="listwhitetext" colspan="5"><table style="margin: 0px;padding:0px;" width="100%">
                          <tr>
                            <c:choose>
								<c:when test="${pricingControl.unitType == 'Lbs'}" >
							   		<td class="listwhitetext" style="font-weight: bold;">(lbs/cft)<INPUT type="radio" name="pricingControl.unitType" checked="checked" value="Lbs" >(metric)<INPUT type="radio" name="pricingControl.unitType"  value="Kgs" ></td>
							   	</c:when>
							   	<c:when test="${pricingControl.unitType == 'Kgs'}" >
							   		<td class="listwhitetext" style="font-weight: bold;">(lbs/cft)<INPUT type="radio" name="pricingControl.unitType"  value="Lbs" >(metric)<INPUT type="radio" name="pricingControl.unitType" checked="checked" value="Kgs" ></td>
							   	</c:when>
							   	<c:otherwise>
							   		<td class="listwhitetext" style="font-weight: bold;">(lbs/cft)<INPUT type="radio" name="pricingControl.unitType"  value="Lbs" checked="checked" >(metric)<INPUT type="radio" name="pricingControl.unitType"  value="Kgs" ></td>
							   	</c:otherwise>
					   	</c:choose>
                              
                          </tr>
                         
                        </table></td>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext" id='boldStuff'>Weight:</td>
                      <td align="left" class="listwhitetext" id='boldStuff1'>Volume:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan=""><s:textfield name="pricingControl.weight"  maxlength="100" size="10" cssClass="input-text" readonly="false" onchange="checkDimensionalWeight(),onlyFloat(this);"/></td>
                      <td align="left" colspan="" width="120px"><s:textfield name="pricingControl.volume"  maxlength="100" size="11" cssClass="input-text" onchange="checkVolume();checkDimensionalWeight(),onlyFloat(this);"/></td>
                    </tr>
                    <tr>
                      <td colspan="6" style="margin: 0px;padding: 0px;width: 100%">
                      <div id="hid1" style="margin: 0px;padding:0px;">
                                                <table border="0" style="margin: 0px;padding: 0px;">
                            <tr>
                              <td align="left" class="listwhitetext" width="">Packing&nbsp;Mode:</td>
                            </tr>
                            <tr>
                              <td align="left"><s:select cssClass="list-menu" name="pricingControl.packing" list="%{pkmode}" cssStyle="width:165px;" onchange="openRadioSection();"/></td>
                            </tr>
                        <tr>
		  		
					  		<td align="left" class="listwhitetext">
					  		<div id="hid2" style="margin: 0px;padding:0px;">
					  		<c:choose>
								<c:when test="${pricingControl.lclFclFlag == 'lcl'}" >
						  				LCL<INPUT type="radio" name="pricingControl.lclFclFlag" checked="checked" value="lcl" >FCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="fcl" >
						  		</c:when>
						  		<c:when test="${pricingControl.lclFclFlag == 'fcl'}" >
						  				LCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="lcl" >FCL<INPUT type="radio" name="pricingControl.lclFclFlag" checked="checked" value="fcl">
						  		</c:when>
						  		<c:otherwise>
						  				LCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="lcl" checked="checked" >FCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="fcl" >
						  		</c:otherwise>
					  		</c:choose>
					  		</div>
					  		</td>
					  		</div>
		  			</tr>
                            <tr>
                              <td align="left" class="listwhitetext">Container&nbsp;Size:</td>
                            </tr>
                            <tr>
                              <td align="left" colspan=""><s:select cssClass="list-menu" name="pricingControl.containerSize" list="{'20','40','40H'}" headerKey="" headerValue="" cssStyle="" onchange="checkVolume()"/></td>
                            </tr>
                            
                          </table>
                        </div></td>
                    </tr>
                  </table>
                </div></td>
            </tr>
            <tr><td height="1px"></td></tr>
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img2);">
                <div onClick="javascript:animatedcollapse.toggle('origin');" style="margin:0px;">
                  <!--<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
                    <tr>
                      <td class="probg_left"></td>
                      <td NOWRAP class="probg_center"><a href="javascript:;"><img id="img2" src="images/129.png" border="0"/></a>&nbsp;Origin Address</td>
                      <td class="probg_right"></td>
                    </tr>
                  </table>
                  --><div class="pricetab_bg topsection">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img2" src="images/tab_close.png" border="0"/></a></div><div class="price_tright">&nbsp;Origin Address</div></div>
                </div>
                </span>
                <div id="origin" style="margin:0px;padding:0px; ">
                  <table width="100%" style="margin:0px;padding:0px;">
                    <tr>
                      <td align="left" class="listwhitetext"  width="90" colspan="4">Address:</td>
                    <tr>
                      <td align="left" colspan="4"><s:textfield name="pricingControl.originAddress1"  maxlength="100" size="32" cssClass="input-text" readonly="false" /></td>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext">City:</td>
                      <td align="left" class="listwhitetext">Postal:</td>
                    </tr>
                    <tr>
                      <td align="left" width="100"><s:textfield name="pricingControl.originCity"  maxlength="100" size="17" cssClass="input-text" readonly="false" /></td>
                      <td align="left"><s:textfield name="pricingControl.originZip"  maxlength="100" size="8" cssClass="input-text" readonly="false" /></td>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext">Country:</td>
                      <td align="left" class="listwhitetext">State:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan=""><s:select cssClass="list-menu" name="pricingControl.originCountry" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:115px"/></td>
                      <td align="left" width=""><s:textfield name="pricingControl.originState"  maxlength="100" size="8" cssClass="input-text" readonly="false" /></td>
                    </tr>
                    
                    <tr>
                      <td align="left" colspan="4"><input type="button" class="cssbutton" onclick="return  loadGoogleGeoCode('origin');" name="Find Geo Code" value="Confirm Location on Map" style="height:25px;width:183px;"/></td>
                    </tr>
                    
                    <tr>
                    <td colspan="2" align="left">
                    <table class="deatailTabLabel" border="0" style="margin: 0px;padding: 0px;">
                    <tr>
                      <td class="listwhitetext" align="left">Latitude</td>
                      <td align="left" class="listwhitetext">Longitude</td>
                    </tr>
                    <tr>
                      <td align="left" width="90"><s:textfield name="pricingControl.originLatitude"  maxlength="100" size="12" cssClass="input-textUpper" readonly="false" /></td>
                      <td><s:textfield name="pricingControl.originLongitude"  maxlength="100" size="12" cssClass="input-textUpper" readonly="false" /></td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    
                    <tr>
                      <td colspan="2" align="left"><table class="deatailTabLabel" border="0" style="margin: 0px;padding: 0px;">
                                    <tr>
                                      <td align="left" class="listwhitetext" width="">Port&nbsp;Of&nbsp;Lading:</td>
                                    </tr>
                                    <tr>
	                                  	<td align="left"><s:textfield name="pricingControl.originPOE"  maxlength="100" size="8" cssClass="input-text" readonly="true" onblur="changeName('Orig');disablePortRange('Origin');" onchange="disablePortRange('Origin')" onkeydown="return onlyDel(event,this);"/>
			  							<img align="top" class="openpopup" width="17" height="20" onclick="putPortName('origin');" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                    	<td align="right"><s:textfield name="pricingControl.originPOEName"  maxlength="100" size="13" cssClass="input-text" readonly="true"/></td>
                                    </tr>
                                    <tr>
	                                  	<td align="left" class="listwhitetext" width="" colspan="2">Choose&nbsp;Ports&nbsp;Within:</td>
			  						 </tr>
			  						 <tr>
			  						   	<td align="left"><s:select cssClass="list-menu" name="pricingControl.originPortRange" list="{'25','50','100','200','300','400','500','600','700','800','900','1000','1100'}" headerKey="" headerValue="" cssStyle="width:80px" onchange=""/></td>
                                    </tr>
                                  </table></td>
                    </tr>
                  </table>
              </div>
              </td>
           
            </tr>
            <tr><td height="1px"></td></tr> 
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img3);">
                <div onClick="javascript:animatedcollapse.toggle('destination');" style="margin:0px;">
                  <!--<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
                    <tr>
                      <td class="probg_left"></td>
                      <td NOWRAP class="probg_center"><a href="javascript:;"><img id="img3" src="images/129.png" border="0"/></a>&nbsp;Destination Address</td>
                      <td class="probg_right"></td>
                    </tr>
                  </table>
                  --><div class="pricetab_bg topsection">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img3" src="images/tab_close.png" border="0"/></a></div><div class="price_tright">&nbsp;Destination Address</div></div>
                </div>
                </span>
                <div id="destination" style="margin:0px;padding:0px; ">
                  <table width="100%" style="margin:0px;padding:0px;">
                    <tr>
                      <td align="left" class="listwhitetext"  width="90" colspan="4">Address:</td>
                    <tr>
                      <td align="left" colspan="4"><s:textfield name="pricingControl.destinationAddress1"  maxlength="100" size="32" cssClass="input-text" readonly="false" /></td>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext">City:</td>
                      <td align="left" class="listwhitetext">Postal:</td>
                    </tr>
                    <tr>
                      <td align="left" width="100"><s:textfield name="pricingControl.destinationCity"  maxlength="100" size="17" cssClass="input-text" readonly="false" /></td>
                      <td align="left"><s:textfield name="pricingControl.destinationZip"  maxlength="100" size="8" cssClass="input-text" readonly="false" /></td>
                    </tr>
                    <tr>
                      <td align="left" class="listwhitetext">Country:</td>
                      <td align="left" class="listwhitetext">State:</td>
                    </tr>
                    <tr>
                      <td align="left" colspan=""><s:select cssClass="list-menu" name="pricingControl.destinationCountry" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:115px"/></td>
                      <td align="left" width=""><s:textfield name="pricingControl.destinationState"  maxlength="100" size="8" cssClass="input-text" readonly="false" /></td>
                    </tr>
                    
                    <tr>
                      <td align="left" colspan="4"><input type="button" class="cssbutton" onclick="return loadGoogleGeoCode('destination');" name="Find Geo Code" value="Confirm Location on Map" style="height:25px;width:183px;"/></td>
                    </tr>
                    
                    <tr>
                    <td colspan="2" align="left">
                    <table class="deatailTabLabel" border="0" style="margin: 0px;padding: 0px;">                    
                    <tr>
                      <td class="listwhitetext" align="left">Latitude</td>
                      <td align="left" class="listwhitetext">Longitude</td>
                    </tr>
                    <tr>
                      <td align="left" width="90"><s:textfield name="pricingControl.destinationLatitude"  maxlength="100" size="12" cssClass="input-textUpper" readonly="false" /></td>
                      <td><s:textfield name="pricingControl.destinationLongitude"  maxlength="100" size="12" cssClass="input-textUpper" readonly="false" /></td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    
                    <tr>
                      <td colspan="2" align="left"><table border="0" style="margin: 0px;padding: 0px;">
                                    <tr>
                                      <td align="left" class="listwhitetext" width="">Port&nbsp;OfEntry:</td>
                                    </tr>
                                    <tr>
						                                    
								  		<td align="left"><s:textfield name="pricingControl.destinationPOE"  maxlength="100" size="8" cssClass="input-text" readonly="true"  onblur="changeName('Dest'); disablePortRange('Destination');" onchange="disablePortRange('Destination');" onkeydown="return onlyDel(event,this);"/>
								  		<img align="top" class="openpopup" width="17" height="20" onclick="putPortName('destination');" id="openpopup2.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                    	<td align="right"><s:textfield name="pricingControl.destinationPOEName"  maxlength="100" size="13" cssClass="input-text" readonly="true"/></td>
                                    </tr>
                                    <tr>
	                                  	<td align="left" class="listwhitetext" width="" colspan="2">Choose&nbsp;Ports&nbsp;Within:</td>
			  						 </tr>
			  						 <tr>
			  						   	<td align="left"><s:select cssClass="list-menu" name="pricingControl.destinationPortRange" list="{'25','50','100','200','300','400','500','600','700','800','900','1000','1100'}" headerKey="" headerValue="" cssStyle="width:80px" onchange=""/></td>
                                    </tr>
                                  </table></td>
                    </tr>
                  </table>
              </div></td>
            </tr>
            
          </tbody>
        </table></td>
      <td width="80%" valign="top" style="margin:0px;padding: 0px;"><table border="0" cellpadding="2" cellspacing="1" width="100%" style="margin:0px;padding: 0px;">
          <tbody>
            <tr>
              <td valign="top" style="margin:0px;padding: 0px;width: 74%;border:1px solid #969694;"><div id="map" style="width: 100%; height: 500px;margin:0px;padding: 0px;"></div></td><B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
            </tr>
          
			<td colspan="7" style="margin:0px;">
			<DIV ID="layerH" style="position:absolute;left:400px;top:140px;!left:120px;!top:120px; ">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Calculating&nbsp;Pricing&nbsp;Scenarios.</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
			</td>
									
            <tr>
              <td height="15"></td>
            </tr>
            <tr>
              <td colspan="" align="center"><table class="detailTabLabel"  border="0" style="margin: 0px;padding: 0px;" >
                  <tbody>
                    <tr>
                      
                      <td align="left" width="30"><input type="button" class="cssbutton1" onclick="reset();" value="Reset" style="height:25px"/></td>
                      <td align="left">	<s:submit cssClass="cssbutton1" type="button" cssStyle="width:105px"  value="Get Pricing"  onclick="return selectScope(),getPricing()"/> </td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>


<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="pricingControlCreatedOnFormattedValue" value="${pricingControl.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="pricingControl.createdOn" value="${pricingControlCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${pricingControl.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty pricingControl.id}">
								<s:hidden name="pricingControl.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{pricingControl.createdBy}"/></td>
							</c:if>
							<c:if test="${empty pricingControl.id}">
								<s:hidden name="pricingControl.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="pricingControlupdatedOnFormattedValue" value="${pricingControl.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="pricingControl.updatedOn" value="${pricingControlupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${pricingControl.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty pricingControl.id}">
								<s:hidden name="pricingControl.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{pricingControl.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty pricingControl.id}">
								<s:hidden name="pricingControl.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

</div>
</s:form>

<%-- Script Shifted from Top to Botton on 11-Sep-2012 By Kunal --%>

<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"djConfig="parseOnLoad:true, isDebug:false"></script>
<%--
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&visible=100&amp;key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRtKhpDtLX6R5MagO3ZOu0_GHeYthTZmolg4KuvVTJhWVj_M3YuGSUoeA" type="text/javascript"></script>
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
--%>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=0')
animatedcollapse.addDiv('origin', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('destination', 'fade=0,persist=0,hide=1')
animatedcollapse.init()
</script>

<script language="javascript" type="text/javascript"><!--

var namesVec = new Array("tab_open.png", "tab_close.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

var geocoder;
var map;
var address;
window.onload = function() { 
		 if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
		 trap();
		 var elementsLen=document.forms['agentPricing'].elements.length;
		 for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['agentPricing'].elements[i].type=='text')
					{
						document.forms['agentPricing'].elements[i].readOnly =true;
						document.forms['agentPricing'].elements[i].className = 'input-textUpper';
					}
					else
					{
						document.forms['agentPricing'].elements[i].disabled=true;
					}
		}
		 }
		  
	}
	function test()
		{
			return false;
		}
	function trap() 
		  {
		  if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('calender.png')>0)
						{
							
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0)
						{ 
							
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
							    document.images[i].src = 'images/navarrow.gif';
						}
						
					
					}
		    }
		  }
window.onunload = function() { 
		 map = new GMap2(document.getElementById("map"));
		  geocoder = new GClientGeocoder();
	}
function checkFields(){

var originScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].checked;
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isDestination'].checked;
var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;

	if(document.forms['agentPricing'].elements['pricingControl.expectedLoadDate'].value==''){
	  			alert("Please enter the expected date");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.expectedLoadDate'].focus();
	  			return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.mode'].value==''){
	  			alert("Please select the mode");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.mode'].focus();
	  			return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.weight'].value==''){
	  			alert("Please select the weight");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.weight'].focus();
	  			return false;
	}
	if(mode=='Sea'){
	if(document.forms['agentPricing'].elements['pricingControl.packing'].value==''){
	  			alert("Please select the packing mode");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.packing'].focus();
	  			return false;
	}
	var packingMode=document.forms['agentPricing'].elements['pricingControl.packing'].value;
	if(packingMode=='LVCO' || packingMode=='BKBLK')
	{
		if(document.forms['agentPricing'].elements['pricingControl.lclFclFlag'].checked==false){
	  			alert("Please select the lcl or fcl");
	  			return false;
	}
	}
	}
	if(document.forms['agentPricing'].elements['pricingControl.containerSize'].value==''){
	  			alert("Please select the container size");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.containerSize'].focus();
	  			return false;
	}
	if(originScope)
	{
		if(document.forms['agentPricing'].elements['pricingControl.originCountry'].value==''){
	  			alert("Please select the origin country");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.originCountry'].focus();
	  			return false;
		}
			
	}
	if(destinationScope)
	{
		if(document.forms['agentPricing'].elements['pricingControl.destinationCountry'].value==''){
	  			alert("Please select the origin country");
	  			document.forms['partnerRequestAccess'].elements['pricingControl.originCountry'].focus();
	  			return false;
		}
			
	}
	 <c:if test='${!param.popup}'>
		 	document.forms['agentPricing'].action="saveAgentPricingOriginInfo.html"
	  </c:if>	
	 <c:if test='${param.popup}'>
	 	document.forms['agentPricing'].action="saveAgentPricingOriginInfo.html?decorator=popup&popup=true"
	  </c:if>	
		document.forms['agentPricing'].submit();
}

function openSection()
{
	
		var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;
		var packing=document.forms['agentPricing'].elements['pricingControl.packing'].value;
	
	  	var e1 = document.getElementById('hid1');
	  	var e2 = document.getElementById('hid2');
	
		if(mode == 'Sea' && packing!='BKBLK' ){
		e1.style.display = 'block';
		e2.style.display = 'none';
		
		}else if(mode == 'Sea' && packing=='BKBLK'){
		e1.style.display = 'block';
		e2.style.display = 'block';
	
		}else if(mode == 'Sea' && packing=='LOOSE'){
		e1.style.display = 'block';
		e2.style.display = 'none';
		}
		else{
		e1.style.display = 'none';
		e2.style.display = 'none';
	
		}
		
	
}

function openRadioSection()
{
	
		var packing=document.forms['agentPricing'].elements['pricingControl.packing'].value;
	  	var e2 = document.getElementById('hid2');
	  	if(packing == 'BKBLK'){
		e2.style.display = 'block';
		}else if(packing == 'LOOSE'){
		e2.style.display = 'none';
		}
		else{
		e2.style.display = 'none';
		}
}
function initLoader() { 
  	 var script = document.createElement("script");
       script.type = "text/javascript";
       script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadGoogleGeoCode";
       document.body.appendChild(script);
        setTimeout('',900000);
       }

function loadGoogleGeoCode(target)
 {
	if(target==undefined){
		target='origin';
	}
	  	if(document.forms['agentPricing'].elements['pricingControl.mode'].value=='')
 	{
 		alert('Please select the mode');
 		return false;
 	}
 	//setTimeout('',6000);
 	 if(target=='origin')
   {
   	var address1=document.forms['agentPricing'].elements['pricingControl.originAddress1'].value;
	var city=document.forms['agentPricing'].elements['pricingControl.originCity'].value;
	var zip=document.forms['agentPricing'].elements['pricingControl.originZip'].value;
	var state=document.forms['agentPricing'].elements['pricingControl.originState'].value;
	var dd = document.forms['agentPricing'].elements['pricingControl.originCountry'].selectedIndex;
	var country = document.forms['agentPricing'].elements['pricingControl.originCountry'].options[dd].text;
	address = address1+","+city+" - "+zip+","+state+","+country;
	//alert(address);
	}
	if(target=='destination'){
  	var address1=document.forms['agentPricing'].elements['pricingControl.destinationAddress1'].value;
	var city=document.forms['agentPricing'].elements['pricingControl.destinationCity'].value;
	var zip=document.forms['agentPricing'].elements['pricingControl.destinationZip'].value;
	var state=document.forms['agentPricing'].elements['pricingControl.destinationState'].value;
	var dd = document.forms['agentPricing'].elements['pricingControl.destinationCountry'].selectedIndex;
	var country = document.forms['agentPricing'].elements['pricingControl.destinationCountry'].options[dd].text;
	address = address1+","+city+" - "+zip+","+state+","+country;
	//alert(address);
	}
      // Create new map object
     map = new GMap2(document.getElementById("map"));

      // Create new geocoding object
     geocoder = new GClientGeocoder();

      // Retrieve location information, pass it to addToMap()
      if(target=='origin')
   	  {
      geocoder.getLocations(address, addToMapOrigin);
      }
      if(target=='destination'){
       geocoder.getLocations(address, addToMapDestination);
      }
   }
   function addToMapOrigin(response)
   {
      // Retrieve the object
      place = response.Placemark[0];

      // Retrieve the latitude and longitude
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
		//alert(point);
		document.forms['agentPricing'].elements['pricingControl.originLatitude'].value = place.Point.coordinates[1];
		document.forms['agentPricing'].elements['pricingControl.originLongitude'].value = place.Point.coordinates[0];
      // Center the map on this point
      map.setCenter(point, 8);
	
	  addBasicInfoToMap(point, null); 
	  map.setUIToDefault();
	   map.disableScrollWheelZoom();
	  getPortsPoint('Origin');
  }
  
  function addToMapDestination(response)
   {
      // Retrieve the object
      place = response.Placemark[0];

      // Retrieve the latitude and longitude
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
		//alert(point);
		document.forms['agentPricing'].elements['pricingControl.destinationLatitude'].value = place.Point.coordinates[1];
		document.forms['agentPricing'].elements['pricingControl.destinationLongitude'].value = place.Point.coordinates[0];
      // Center the map on this point
      map.setCenter(point, 8);
	  addBasicInfoToMap(point, null); 
	  map.setUIToDefault();
	  map.disableScrollWheelZoom();
	  map.zoomIn();
	  getPortsPoint('Destination');
	}
   
   function getAddress(){
   		var serviceOrderId= document.forms['agentPricing'].elements['serviceOrderId'].value;
   		var tariffApplicability=document.forms['agentPricing'].elements['pricingControl.tariffApplicability'].value;
	 	var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
		document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text='';
		document.forms['agentPricing'].elements['pricingControl.country'].options[dd].value='';
   		if(tariffApplicability!='')
   		{
   			var url="getAddressForPricing.html?ajax=1&decorator=simple&popup=true&serviceOrderId="+encodeURI(serviceOrderId)+"&tariffApplicability=" + encodeURI(tariffApplicability);
			http2.open("GET", url, true);
	    	http2.onreadystatechange = handleHttpResponseAddress;
	     	http2.send(null);
   		}else{
 			document.forms['agentPricing'].elements['pricingControl.address1'].value='';
			document.forms['agentPricing'].elements['pricingControl.city'].value='';
			document.forms['agentPricing'].elements['pricingControl.state'].value='';
			document.forms['agentPricing'].elements['pricingControl.zip'].value='';
			var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
			document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text='';
			document.forms['agentPricing'].elements['pricingControl.country'].options[dd].value='';
   		
   		}
   }
   
   function handleHttpResponseAddress()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                res = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.trim();
                res = res.trim();
                res = res.split("@");
               		document.forms['agentPricing'].elements['pricingControl.address1'].value=res[0];
					document.forms['agentPricing'].elements['pricingControl.city'].value=res[1];
					document.forms['agentPricing'].elements['pricingControl.state'].value=res[2];
					document.forms['agentPricing'].elements['pricingControl.zip'].value=res[3];
					var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
					document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text=res[4];
					document.forms['agentPricing'].elements['pricingControl.country'].options[dd].value=res[5];
					//document.forms['agentPricing'].elements['pricingControl.country'].options[0].text = res[4];
					//document.forms['agentPricing'].elements['pricingControl.country'].options[0].value = res[5];
             }
        }
	
	String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();


function putPortName(target){ 
	var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;
	if(target=='origin'){
	var countryCode=document.forms['agentPricing'].elements['pricingControl.originCountry'].value;
	window.open('searchHaulingPortList.html?portCode=&portName=&country='+countryCode+'&modeType='+mode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=latitude&fld_secondDescription=logtitude&fld_description=pricingControl.originPOEName&fld_code=pricingControl.originPOE','port','scrollbars=1,width=900,height=450');
	}
	if(target=='destination'){
	var countryCode=document.forms['agentPricing'].elements['pricingControl.destinationCountry'].value;
	window.open('searchHaulingPortList.html?portCode=&portName=&country='+countryCode+'&modeType='+mode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=latitude&fld_secondDescription=logtitude&fld_description=pricingControl.destinationPOEName&fld_code=pricingControl.destinationPOE','port','scrollbars=1,width=900,height=450');
	}
} 

function getCount(){
		var id= document.forms['agentPricing'].elements['pricingControl.id'].value;
		var url="getDeatilsCount.html?ajax=1&decorator=simple&popup=true&pricingControlID=" + encodeURI(id);
		http2.open("GET", url, true);
    	http2.onreadystatechange = handleHttpResponseCount;
     	http2.send(null);
}

function handleHttpResponseCount()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results>0){
                	document.forms['agentPricing'].elements['countCheck'].value = 'true';
                }
				else{
					document.forms['agentPricing'].elements['countCheck'].value = 'false';
				}	
             }
        }
function getPricing(){
		selectScope();
		 //openFreightSection();
		var originScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].value;
		var destinationScope=document.forms['agentPricing'].elements['pricingControl.isDestination'].value;
			
	if(document.forms['agentPricing'].elements['pricingControl.expectedLoadDate'].value==''){
		alert('Please enter the expected Load Date');
		return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.mode'].value==''){
		alert('Please enter the mode');
		return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.weight'].value*1 <= 0){
		alert('Please enter the weight');
		return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.volume'].value*1 <= 0){
		alert('Please enter the volume');
		return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.packing'].value==''){
		alert('Please enter the packing mode');
		return false;
	}
	if(document.forms['agentPricing'].elements['pricingControl.mode'].value=='Sea' && document.forms['agentPricing'].elements['pricingControl.packing'].value=='LOOSE')
	{
	if(document.forms['agentPricing'].elements['pricingControl.containerSize'].value==''){
		alert('Please enter the container Size');
		return false;
	}
	}
	
	//return checkVolume();	
	var originLatitude=document.forms['agentPricing'].elements['pricingControl.originLatitude'].value;
	var originPOE=document.forms['agentPricing'].elements['pricingControl.originPOE'].value;
	var destinationLatitude=document.forms['agentPricing'].elements['pricingControl.destinationLatitude'].value;
	var destinationPOE=document.forms['agentPricing'].elements['pricingControl.destinationPOE'].value;
	var originPortRange=document.forms['agentPricing'].elements['pricingControl.originPortRange'].value;
	var destinationPortRange=document.forms['agentPricing'].elements['pricingControl.destinationPortRange'].value;
	if(originLatitude=='' && (originPOE=='' && originPortRange=='') && destinationLatitude=='' && (destinationPOE=='' && destinationPortRange==''))
	{
		alert('Please confirm at least one address');
		return false;
	}
	
	if(originLatitude!='' && (originPOE=='' && originPortRange==''))
	{
		alert('Please select the Port Of Lading');
		return false;
	}
	
	if(originLatitude=='' && (originPOE!='' && originPortRange!=''))
	{
		alert('Map location not confirmed for origin address');
		return false;
	}
	
	if(destinationLatitude!='' && (destinationPOE=='' &&  destinationPortRange==''))
	{
		alert('Please select the Port Of Entry');
		return false;
	}
	
	if(destinationLatitude=='' && (destinationPOE!='' && destinationPortRange!=''))
	{
		alert('Map location not confirmed for destination address');
		return false;
	}
	 return submitForm();
	
	//return getCount();

} 

function submitForm(){
	var countCheck = document.forms['agentPricing'].elements['countCheck'].value;
	if(countCheck == 'true'){
	var agree=confirm("Previous Pricing Scenarios will be replaced, Hit OK to Continue or Cancel to retain the scenarios");
		if (agree){
			showOrHide(1);
			return true;
			
		}else{
			return false;
		}
		
	}else if(countCheck == 'false'){
		showOrHide(1);
		return true;
	}

}

function locatePartners(){
	// post some data, ignore the response:
	   dojo.xhrPost({
	       form: "agentPricing", // read the url: from the action="" of the <form>
	       timeout: 3000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	        	var partnerListDiv = document.getElementById('partnerList');
		   		var partnerTable = document.createElement('TABLE');
		   		partnerTable.setAttribute('class', 'table');
		   		partnerListDiv.appendChild(partnerTable);
		   		
		   		var partnerHeader =  document.createElement('THEAD');
		   		partnerTable.appendChild(partnerHeader);
	        	var partnerHeaderRow = document.createElement('TR');
	        	partnerHeader.appendChild(partnerHeaderRow);

	        	var partnerNameHeader = document.createElement('TH');
	        	partnerHeaderRow.appendChild(partnerNameHeader);
	        	var partnerNameHeaderText = document.createTextNode('Name');
	        	partnerNameHeader.appendChild(partnerNameHeaderText);

	        	var partnerQuoteHeader = document.createElement('TH');
	        	partnerHeaderRow.appendChild(partnerQuoteHeader);
	        	var partnerQuoteHeaderText = document.createTextNode('Quote');
	        	partnerQuoteHeader.appendChild(partnerQuoteHeaderText);
	        	
	           for(var i=0; i < jsonData.partners.length; i++){
	           	   var partnerRow = document.createElement('TR');
	        	   partnerTable.appendChild(partnerRow);
	        	  
	        	   var partnerNameCol = document.createElement('TD');
	        	   partnerRow.appendChild(partnerNameCol);
	        	   var partnerName = document.createTextNode(jsonData.partners[i].name);
	        	   partnerNameCol.appendChild(partnerName);
	        	 
	        	 
	        	   var partnerQuoteCol = document.createElement('TD');
	        	   partnerRow.appendChild(partnerQuoteCol);
	        	   var partnerQuote = document.createTextNode(jsonData.partners[i].rate);
	        	   partnerQuoteCol.appendChild(partnerQuote);	
	        	   
	        	    var partnerRadioCol = document.createElement('TD');
	        	    partnerRow.appendChild(partnerRadioCol);
	        	    var newElem= document.createElement("input");
	        	    newElem.type = "radio";
	        	   	partnerRadioCol.appendChild(newElem);
	        	    
	        	   console.log("lat: " + jsonData.partners[i].latitude + ", long: " +  jsonData.partners[i].longitude);
	        	   point = new GLatLng(jsonData.partners[i].latitude, jsonData.partners[i].longitude);
	        	   addCoordsToMap(point, jsonData.partners[i]);
	        	   var delme = 0;
	        	}
	       }	       
	   });
   }
   
   function addBasicInfoToMap(point, partnerDetails){

	      // Create a marker
	      //marker = new GMarker(point);
		// Create our "tiny" marker icon
		var yellowIcon = new GIcon(G_DEFAULT_ICON);
		yellowIcon.image = "${pageContext.request.contextPath}/images/target.gif";
		                
		// Set up our GMarkerOptions object
		markerOptions = { icon:yellowIcon };
	     marker = new GMarker(point, markerOptions); 
		 map.addOverlay(marker, markerOptions);
		  map.setCenter(point, 8);
		  map.setUIToDefault();
	 	
	  }
   
   function addPortsCoordsToMap(point, portDetails, tariffApplicability){

	      // Create a marker
	     // marker = new GMarker(point);

	      // Add the marker to map
	      //map.addOverlay(marker);
	      
		// Create our "tiny" marker icon
		var yellowIcon = new GIcon(G_DEFAULT_ICON);
		yellowIcon.image = "${pageContext.request.contextPath}/images/markerP-yellow.png";
		                
		// Set up our GMarkerOptions object
		markerOptions = { icon:yellowIcon };
	    var marker = new GMarker(point, markerOptions);
	      
		 map.addOverlay(marker, markerOptions);
	     // GEvent.addListener(marker, "click", function()
		//{marker.openInfoWindowHtml(''+portDetails.portcode+', '+portDetails.name+'');});
		
		 var fn = markerClickFnPorts(point, portDetails, tariffApplicability);
	      GEvent.addListener(marker, "click", fn);

	  }
	  
	  function markerClickFnPorts(point, portDetails, tariffApplicability) {
	   return function() {
		   if (!portDetails) return;
	       var name = portDetails.name;
	       var code = portDetails.portcode;
	       
	       var infoHtml = '<div style="width:100%;"><h3>'+code+', '+name+'</h3>'+
	       '<td><a style="text-decoration: underline; cursor: pointer;" onclick=putSelectedPortName("'+code+'","'+encodeURI(name)+'","'+tariffApplicability+'")>select this port</a></td>';
	            
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }
   
   function putSelectedPortName(code, name, tariffApplicability){
   		//alert(code+'---'+name+'---'+tariffApplicability);
   		if(tariffApplicability=='Origin'){
   			if(document.forms['agentPricing'].elements['pricingControl.originPortRange'].value == ''){
   				document.forms['agentPricing'].elements['pricingControl.originPOE'].value=code;
   		    	document.forms['agentPricing'].elements['pricingControl.originPOEName'].value=name;
   		    	document.forms['agentPricing'].elements['pricingControl.originPOE'].focus();
   			}else{
   				alert('Origin port range has already been selected.');
   			}
   		}
   		if(tariffApplicability=='Destination'){
   			if(document.forms['agentPricing'].elements['pricingControl.destinationPortRange'].value == ''){
   				document.forms['agentPricing'].elements['pricingControl.destinationPOE'].value=code;
   				document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].value=name;
   			 	document.forms['agentPricing'].elements['pricingControl.destinationPOE'].focus();
   			}else{
   				alert('Destination port range has already been selected.');
   			}
   		}
  	}
   
   function addCoordsToMap(point, partnerDetails){

	      // Create a marker
	      marker = new GMarker(point);

	      // Add the marker to map
	      map.addOverlay(marker);
	  
	      // Add address information to marker
	      //marker.openInfoWindowHtml(place.address);
	     var fn = markerClickFn(point, partnerDetails);
	      GEvent.addListener(marker, "click", fn);
	      
	      

	  }
   
    function markerClickFn(point, partnerDetails) {
	   return function() {
		   if (!partnerDetails) return;
	       var title = partnerDetails.name;
	       var url = partnerDetails.url;
	       var fileurl = partnerDetails.locationPhotoUrl;
	       var infoHtml = '<div style="width:210px;"><h3>' + title
	         + '</h3><div style="width:200px;height:200px;line-height:200px;margin:2px 0;text-align:center;">'
	         + '<a id="infoimg" href="' + fileurl + '" target="_blank">Loading...</a></div><br/>'
	         + '<h5>Quote: $'+partnerDetails.rate+'</h5>'
	         + '<h6>'+partnerDetails.address1+'</h6>'
	         + '<h6>'+partnerDetails.address2+'</h6><br/>'
	         + '<h6>'+partnerDetails.phone+'</h6>';
	       var img = document.createElement("img");
	       GEvent.addDomListener(img, "load", function() {
	                               if ($("infoimg") == null) {
	                                 return;
	                               }
	                               img = adjustImage(img, 200, 200);
	                               img.style.cssText = "vertical-align:middle;padding:1px;border:1px solid #EAEAEA;";
	                               $("infoimg").innerHTML = "";
	                               $("infoimg").appendChild(img);
	                             });
	       img.src = "UserImage?location=" + fileurl;
	       if(img.readyState == "complete" || img.readyState == "loaded") {
	         img = adjustImage(img, 280, 200);
	         infoHtml += '<img width=' + img.width + ' height=' + img.height
	           + ' style="vertical-align:middle;padding:1px;border:1px solid #aAaAaA"></img>';
	       }
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }
   
     
   function adjustImage(img, maxwidth, maxheight) {
	     var wid = img.width;
	     var hei = img.height;
	     var newwid = wid;
	     var newhei = hei;
	     if(wid / maxwidth > hei / maxheight){
	       if(wid > maxwidth){
	         newwid = maxwidth;
	         newhei = parseInt(hei * newwid / wid);
	       }
	     } else {
	       if(hei > maxheight) {
	         newhei = maxheight;
	         newwid = parseInt(wid * newhei / hei);
	       }
	     }
	     var src = img.src;
	     img = document.createElement("img");
	     img.src = src;
	     img.width = newwid;
	     img.height = newhei;
	     return img;
	   }
	   
function openOriginSection(){
	var originValue=document.forms['agentPricing'].elements['pricingControl.isOrigin'].checked;
		  	var e1 = document.getElementById('origin');
	  	if(originValue){
		e1.style.display = 'block';
		}else{
		e1.style.display = 'none';
		}
}

function openDestinationsection(){

		var destinationValue=document.forms['agentPricing'].elements['pricingControl.isDestination'].checked;
		var e1 = document.getElementById('destination');
	  	if(destinationValue){
		e1.style.display = 'block';
		}else{
		e1.style.display = 'none';
		}
}

function openFreightSection(){
	var freightValue=document.forms['agentPricing'].elements['pricingControl.isFreight'].checked;
	if(freightValue){
		document.forms['agentPricing'].elements['pricingControl.isDestination'].checked=true;
		document.forms['agentPricing'].elements['pricingControl.isOrigin'].checked=true;
		//alert('Please fill all the details in the origin and destination section ');
		//document.forms['agentPricing'].elements['pricingControl.isFreight'].checked=false;
	}
}
--></script>

<script language="JavaScript" type="text/JavaScript">


function notExists(){
	alert("The pricing wizard information has not been saved yet, please click get Pricing to continue");
}

function changeText(){
var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;

if(mode=='Air')
{
	document.getElementById('boldStuff').innerHTML = 'Gross Weight';
	document.getElementById('boldStuff1').innerHTML = 'Gross Volume';
}
if(mode=='Sea')
{
	document.getElementById('boldStuff').innerHTML = 'Net Weight';
	document.getElementById('boldStuff1').innerHTML = 'Net Volume';
}
	
}

function checkDimensionalWeight(){
var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;
var volume=document.forms['agentPricing'].elements['pricingControl.volume'].value;
var weight=document.forms['agentPricing'].elements['pricingControl.weight'].value;
var dimensionalWeight;
if(volume>0){
if(mode=='Air'){
	dimensionalWeight=volume*7;
}
if(mode=='Sea'){
	dimensionalWeight=volume*6.5;
}
if(weight<dimensionalWeight){
	alert('Dimension Weight of '+dimensionalWeight+' has been taken.');
	document.forms['agentPricing'].elements['pricingControl.weight'].value=dimensionalWeight;
}
}
}

function getPackingMode(){
		var mode= document.forms['agentPricing'].elements['pricingControl.mode'].value;
  		var url="getPackingMode.html?decorator=simple&popup=true&modeName="+encodeURI(mode);
		http2.open("GET", url, true);
    	http2.onreadystatechange = handleHttpResponseMode;
     	http2.send(null);
}

  function handleHttpResponseMode()
        {
            
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                res = results.trim();
                //alert(res);
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.trim();
                res = res.trim();
                res = res.split("@");
               		var dd = document.forms['agentPricing'].elements['pricingControl.packing'].selectedIndex;
					document.forms['agentPricing'].elements['pricingControl.packing'].options[dd].text=res[4];
					document.forms['agentPricing'].elements['pricingControl.packing'].options[dd].value=res[5];
					//document.forms['agentPricing'].elements['pricingControl.country'].options[0].text = res[4];
					//document.forms['agentPricing'].elements['pricingControl.country'].options[0].value = res[5];
				
             }
           
        }
</script>


<SCRIPT LANGUAGE="JavaScript">
	var mode = new Array('','Air','Sea');
	var modeids = new Array('','Air','Sea');
	var packingMode = new Array('','Air Vans','Triwalls','Loose in container','Liftvans (LCL)','Liftvans (FCL)');
	var packingModeIds = new Array('','A/V','T/W','LOOSE','BKBLK','LVCO');
	var ModePackingModeIds = new Array('','Air','Air','Sea','Sea','Sea');

	var modeValue = 1
	var centervalue = 1

	function selectMode()
	{
		modeValue = document.forms['agentPricing'].elements['pricingControl.mode'].value
		changePackingMode(modeValue)
	}

	function changePackingMode(modeValue)
	{
		var x=0
		var temp = new Array()
		for(i=0;i<ModePackingModeIds.length;i++)
		{
			if(modeValue==ModePackingModeIds[i])
				{
					temp[x] = i
					x++			
				}
		}
		document.forms['agentPricing'].elements['pricingControl.packing'].length=x
		
		for(i=0;i<x;i++)
		{
			document.forms['agentPricing'].elements['pricingControl.packing'].options[i].value=packingModeIds[temp[i]]
			if(packingModeIds[temp[i]] == '${pricingControl.packing}'){
				document.forms['agentPricing'].elements['pricingControl.packing'].options[i].selected=true;
			}
		}
		for(i=0;i<x;i++)
		{
			document.forms['agentPricing'].elements['pricingControl.packing'].options[i].text=packingMode[temp[i]]
		}
	}
	
	function getPortsPoint(tariffApplicability){
	if(tariffApplicability=='Origin')
	{
		document.forms['agentPricing'].elements['tariffApplicability'].value='Origin';
	}
	if(tariffApplicability=='Destination')
	{
		document.forms['agentPricing'].elements['tariffApplicability'].value='Destination';
	}
		dojo.xhrPost({
	       form: "agentPricing",
	       url:"portsPoint.html",
	       timeout: 9000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	           for(var i=0; i < jsonData.ports.length; i++){
	           	           	    
	        	   console.log("lattitude: " + jsonData.ports[i].latitude + ", longitude: " +  jsonData.ports[i].longitude);
	        	   point = new GLatLng(jsonData.ports[i].latitude, jsonData.ports[i].longitude);
	        	   addPortsCoordsToMap(point, jsonData.ports[i],tariffApplicability);
	        	   var delme = 0;
	        	}
	       }	       
	   });
	}
	
function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}

function selectScope()
{
	var originLatitude=document.forms['agentPricing'].elements['pricingControl.originLatitude'].value;
	var originPOE=document.forms['agentPricing'].elements['pricingControl.originPOE'].value;
	var destinationLatitude=document.forms['agentPricing'].elements['pricingControl.destinationLatitude'].value;
	var destinationPOE=document.forms['agentPricing'].elements['pricingControl.destinationPOE'].value;
	
	if(originLatitude!='' && originPOE!='')
	{
		document.forms['agentPricing'].elements['pricingControl.isOrigin'].value=true;
	}
	
	if(destinationLatitude!='' && destinationPOE!='')
	{
		document.forms['agentPricing'].elements['pricingControl.isDestination'].value=true;
	}
	
	if(originPOE!='' && destinationPOE!='')
	{
		document.forms['agentPricing'].elements['pricingControl.isFreight'].value=true;
	}

}

function checkVolume()
{
	var containerSize=document.forms['agentPricing'].elements['pricingControl.containerSize'].value;
	var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;
	var weight=document.forms['agentPricing'].elements['pricingControl.weight'].value;
	var volume=document.forms['agentPricing'].elements['pricingControl.volume'].value;
	
	if(mode=='Sea' && containerSize=='20' && volume > 1170)
	{
		alert('Max capacity for 20 feet container is 1170');
		document.forms['agentPricing'].elements['pricingControl.volume'].value='';
		return false;
	}
	if(mode=='Sea' && containerSize=='40' && volume > 2765)
	{
		alert('Max capacity for 40 feet container is 2765');
		document.forms['agentPricing'].elements['pricingControl.volume'].value='';
		return false;
	}
}

function changeName(target){
if(target='Orig'){
	var oriPortName=document.forms['agentPricing'].elements['pricingControl.originPOEName'].value;
	for(var i=0;i<100;i++){
	  oriPortName = oriPortName.replace("%20", "");
	 }
	 document.forms['agentPricing'].elements['pricingControl.originPOEName'].value=oriPortName;
}
if(target='Dest'){
	var portName=document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].value;
	for(var i=0;i<100;i++){
	  portName = portName.replace("%20", "");
	 }
	 document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].value=portName;
}
	

}
function checkControlFlag(){
	var id= '${pricingControl.id}';
	var sequenceNumber='${pricingControl.sequenceNumber}';
	var url="checkControlFlag.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" +sequenceNumber;
	http2.open("GET", url, true);
  	http2.onreadystatechange = handleHttpResponseCheckControlFlag;
   	http2.send(null);
	
}

function handleHttpResponseCheckControlFlag(){
var sid='${pricingControl.serviceOrderId}';
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results=='C'){
                try{
                	location.href="accountLineList.html?sid="+sid;
                	}catch(e){}
                
                	return true;
                }else{
                	alert('Quote is still not accepted');
                	return false;
                }
             }
}

function checkOriginScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].value;
if(destinationScope!='true')
{
	alert('Origin information not provided.');
	return false;
}

}

function checkDestinationScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isDestination'].value;
if(destinationScope!='true')
{
	alert('Destination information not provided.');
	return false;
}

}
function checkFreightScope(){
var freightScope=document.forms['agentPricing'].elements['pricingControl.isFreight'].value;
if(freightScope!='true')
{
	alert('Origin and/or Destination Ports information not provided.');
	return false;
}
}

function disablePortRange(Scope){
	if(Scope=='Origin'){
		if(document.forms['agentPricing'].elements['pricingControl.originPOE'].value!=''){
			document.forms['agentPricing'].elements['pricingControl.originPortRange'].disabled=true;
		}else{
			document.forms['agentPricing'].elements['pricingControl.originPortRange'].disabled=false;
			document.forms['agentPricing'].elements['pricingControl.originPOEName'].value='';
		}
	}
	if(Scope=='Destination'){
		if(document.forms['agentPricing'].elements['pricingControl.destinationPOE'].value!=''){
			document.forms['agentPricing'].elements['pricingControl.destinationPortRange'].disabled=true;
		}else{
			document.forms['agentPricing'].elements['pricingControl.destinationPortRange'].disabled=false;
			document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].value='';
		}
	}
}  

function disablePortField(Scope){
	if(Scope=='Origin'){
		if(document.forms['agentPricing'].elements['pricingControl.originPortRange'].value!=''){
			document.forms['agentPricing'].elements['pricingControl.originPOE'].disabled=true;
			document.forms['agentPricing'].elements['pricingControl.originPOEName'].disabled=true;
		}else{
			document.forms['agentPricing'].elements['pricingControl.originPOE'].disabled=false;
			document.forms['agentPricing'].elements['pricingControl.originPOEName'].disabled=false;
		}
	}
	
	if(Scope=='Destination'){
		if(document.forms['agentPricing'].elements['pricingControl.destinationPortRange'].value!=''){
			document.forms['agentPricing'].elements['pricingControl.destinationPOE'].disabled=true;
			document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].disabled=true;
		}else{
			document.forms['agentPricing'].elements['pricingControl.destinationPOE'].disabled=false;
			document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].disabled=false;
		}
	}

}
</SCRIPT>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
try{
	initLoader();
    }
    catch(e){}
try{
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/agentPricingSummaryInfo.html?id=${pricingControl.id}&from=Pricing"  />
</c:if>

}
catch(e){}
try{
	var tempIsOrigin='${pricingControl.isOrigin}';
	var tempIsDestination='${pricingControl.isDestination}';
	//alert(tempIsOrigin+"---------------------"+tempIsDestination);
	//  setTimeout('',6000);
if(tempIsOrigin=='origin'){
  loadGoogleGeoCode('origin');
}
if(tempIsDestination=='destination'){
   loadGoogleGeoCode('destination');
}
}catch(e){}

openSection();
//openRadioSection();
//openOriginSection();
try{
var destinationValue=document.forms['agentPricing'].elements['pricingControl.isDestination'].checked;
 	if(destinationValue){
	animatedcollapse.addDiv('destination', 'fade=0,persist=0,hide=0')
}
}
catch(e){}
try{
var destinationValue=document.forms['agentPricing'].elements['pricingControl.isOrigin'].checked;
 	if(destinationValue){
	animatedcollapse.addDiv('origin', 'fade=0,persist=0,hide=0')
}
}
catch(e){}
try{
changeText();
}
catch(e){}
try{
selectMode();
}
catch(e){}
getCount();
 showOrHide(0);
 disablePortRange('Origin');
 disablePortRange('Destination');

</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>