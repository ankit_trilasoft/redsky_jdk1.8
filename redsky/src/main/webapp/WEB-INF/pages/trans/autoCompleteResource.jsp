<%@ include file="/common/taglibs.jsp"%>  

<%
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>

<div id="autocomplete_choices" >
<ul>
 <c:forEach var="varResourceList" items="${resourceList}">
  <li id="${varResourceList}" onclick="checkValue(this);" onmouseover="this.style.background='#E3E4FA';" class="listwhitetext" onmouseout="this.style.background='#ffffff';" style="font-size:9pt;">${varResourceList}</li>
  </c:forEach>
</ul>
</div>
