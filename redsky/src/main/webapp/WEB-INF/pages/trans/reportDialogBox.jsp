<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.*" %>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	String sessionCorpID = user.getCorpID();
%>

<head>   
	<title><fmt:message key="reportsDetail.title"/></title>   
	<meta name="heading" content="<fmt:message key='reportsDetail.heading'/>"/>   
	<c:if test="${param.popup}"> 
	    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
	</c:if>
	
	<style>
	<%@ include file="/common/calenderStyle.css"%>
	</style>
	
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->	
	<script language="javascript" type="text/javascript">
		function validatefields(val){
			<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
			var i;
			i = ${status.index}*1 + 13;
			  if(document.forms['reportForm'].elements[i].value == ''){
					document.forms['reportForm'].elements[i].focus();
					alert('Please enter values for '+"${reportParameter.name}"+'.');
				  return false;  
				}
			</c:forEach>
			selectCommodities();
		    selectCompanyDivision();
			document.forms['reportForm'].elements['fileType'].value = val;
			document.forms['reportForm'].submit();
			return true;
		}
		function refreshParent() {
			window.close();
		}
		
		function selectCompanyDivision(){
		var selectCompanyDivisions = "";
		var selObj = document.getElementById('companyDivisions');
		var i;
		var count = 0;
		if(selObj!= null){
		for (i=0; i<selObj.options.length; i++) {
			 if (selObj.options[i].selected) {
				 selectCompanyDivisions = selectCompanyDivisions +",'"+ selObj.options[i].value+"'";
			    count++;
			  }
			}
		}
		selectCompanyDivisions = selectCompanyDivisions.trim();
		document.forms['reportForm'].elements['selectCompanyDivisions'].value = selectCompanyDivisions.substring(1,selectCompanyDivisions.length);
		}
		
		function selectCommodities(){
			var selectedCommodities = "";
			var selObj = document.getElementById('commodityTypes');
			var i;
			var count = 0;
			if(selObj!= null){
			for (i=0; i<selObj.options.length; i++) {
				 if (selObj.options[i].selected) {
					 selectedCommodities = selectedCommodities +",'"+ selObj.options[i].value+"'";
				    count++;
				  }
				}
			}
			selectedCommodities = selectedCommodities.trim();
			document.forms['reportForm'].elements['selectedCommodities'].value = selectedCommodities.substring(1,selectedCommodities.length);
			}
			
	</script>
	
	<script>
	function display(){
		window.onload=displayStatusReason();
	}
	function displayStatusReason(){
	     var statusValue=document.forms['reportForm'].elements['docsxfer'].value;
	     if(statusValue=='Yes'){
	        document.getElementById('docs').style.display='block';
	     }else{
	        document.getElementById('docs').style.display='none';
	     }
	} 
	function validateFields1(){
		 var partnerValue='';
		<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
			var i;
			i = ${status.index}*1 + 9;
			  if(document.forms['reportForm'].elements[i].value == ''){
					document.forms['reportForm'].elements[i].focus();
					alert('Please enter values for '+"${reportParameter.name}"+'.');
				  return false;  
				} else{
					  if(('${reportParameter.name}'=='PartnerCode')|| '${reportParameter.name}'=='Bill to Code'|| '${reportParameter.name}'=='Vendor Code'){
                            var idvalue='${"reportParameter_"}${reportParameter.name}';		                 
		                    partnerValue=document.forms['reportForm'].elements[idvalue].value;
							//document.forms['reportForm'].elements[idvalue].value=document.forms['reportForm'].elements['BillTo'].value;
							//alert(document.forms['reportForm'].elements['BillTo'].value);
							}
						}
		</c:forEach>
		if(document.forms['reportForm'].elements['docFileType'].value==""){
			alert("Select a file type");
			return false;
		}	
		var url = "uploadReport.html?BillTo="+partnerValue+"&decorator=popup&popup=true";
		document.forms['reportForm'].action = url;
		document.forms['reportForm'].submit();
		return true;
	}

	function test()
	
	{
	 document.getElementById("mydiv1").style.visibility = "visible";
	}
	function setDescriptionDuplicate(targetElement){
		document.forms['reportForm'].elements['myFile.description'].value = targetElement.options[targetElement.selectedIndex].text;
	}
	function documentMap(targetElement){
	   	var fileType = targetElement.value;
	   	var url="docMapFolder.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
	   	http2.open("GET", url, true);
	   	http2.onreadystatechange = handleHttpResponseMap;
	   	http2.send(null);
	}

	function handleHttpResponseMap(){
	      if(http2.readyState == 4){
			var results = http2.responseText
	        results = results.trim();
	        var res = results.substring(1,results.length-1);
			document.forms['reportForm'].elements['mapFolder'].options[0].text = res; 
			document.forms['reportForm'].elements['mapFolder'].options[0].value = res;
			document.forms['reportForm'].elements['mapFolder'].options[0].selected=true;					     	 
		}
	}

	var http2 = getHTTPObject();

	function getHTTPObject(){
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}

</script>
<script type="text/javascript">   
window.onload=function driverCodeAdd(){
	 <c:if test="${usertype=='DRIVER'}">
	document.forms['reportForm'].elements['reportParameter_Driver ID'].value='${userParentAgent}';
	document.forms['reportForm'].elements['reportParameter_Driver ID'].readOnly = true;
	document.forms['reportForm'].elements['reportParameter_Driver ID'].className = 'input-textUpper';
	</c:if>
}
</script>

	
	<style type="text/css">
		/* collapse */
		
		a.dsphead{text-decoration:none;color:#000000;}
		.dspchar2{padding-left:0px;}
		
		img {
		cursor:pointer;
		}
		
		
	</style>
	
	
</head>  
<body style="background-color:#444444;">

<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>
<s:form id="reportForm" name="reportForm" target="_parent" action='${empty param.popup?"viewReportWithParam.html":"viewReportWithParam.html?decorator=popup&popup=true"}' method="post" validate="true" enctype="multipart/form-data" >
	<s:hidden name="selectedCommodities" />
	<s:hidden name="multipleCommodity" />
	<s:hidden name="selectCompanyDivisions" />
	<s:hidden name="multipleCompanyDivisions" />
	
    <s:hidden name="docsxfer" value="<%=request.getParameter("docsxfer") %>" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
	<s:hidden name="module" value="%{reports.module}"/>
	<s:hidden name="reportName" value="<%=request.getParameter("reportName") %>"/>
	<s:hidden name="jobNumber" value="<%=request.getParameter("customerFile.sequenceNumber") %>"/>
	<s:hidden name="reportModule" value="${reportModule}"/>
	<s:hidden name="reportSubModule" value="${reportSubModule}"/>
	<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />
	<s:set name="reportss" value="reportss" scope="request"/>

		<s:hidden name="cid" value="<%=request.getParameter("cid") %>" />
		<s:set name="cid" value="cid" scope="session"/>

		<s:hidden name="sid" value="<%=request.getParameter("sid") %>" />
		<s:set name="sid" value="sid" scope="session"/>
		

	<div id="Layer1">
<c:if test="${list!='main'}">	
	<div id="newmnav">
		  <ul>
		  		<li><a href="javascript:openWindow('subModuleReports.html?id=${id}&jobNumber=${jobNumber}&soList=${soList}&payableStatusList=${payableStatusList}&payingStatusList=${payingStatusList}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&OMNI=${OMNI}&Mode=${Mode}&decorator=popup&popup=true',740,800)"><span>Reports List</span></a></li>
			    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Reports Manager<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div>
		</div><div class="spn">&nbsp;</div><br>
</c:if>
	<table>
		<tbody>
			<tr>
				<td><img src="<c:url value='/images/form_ico.gif'/>"/></td>
				<td>
					<table>
					<tr><td>&nbsp;</td></tr> 
					<tr><td><b><font style="font-size:16px; font-family:Verdana, Geneva, sans-serif; font-style:italic; color: #EB8234;">Report Manager</font></b></td></tr> 
					<tr style="height: 5px;"><td></td></tr> 
					<tr><td><b><font style="color: #5D5D5D;">Report : <%=request.getParameter("reportName") %></font></b></td></tr> 
					</table>
				</td>
			</tr>	
		</tbody>
		
	</table>   
	<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:510px;">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab" style="line-height:20px;padding-left:5px;">Select Parameters and Output format for Report</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
		    				<tbody>	
		    					<tr style="height: 10px;"><td colspan="4"></td></tr> 
								<tr><td colspan="3"><b>&nbsp;&nbsp;Parameter(s) :</b></td></tr>
								<tr style="height: 10px;"><td colspan="4"></td></tr> 
								  <c:forEach var="reportParameter" items="${reportParameters}">
								   <tr>
									  <td width="123px" align="right" class="listwhitetext"><c:out value="${reportParameter.name}" /><font color="red" size="2">*</font> :&nbsp;&nbsp;</td>
											<c:if test='${reportParameter.valueClassName == "java.util.Date"}'> 
											  <td class="listwhitetext" width="">
											  	<c:set var="inputParameterName"  value='${"reportParameter_"}${reportParameter.name}'/>
												<c:if test="${(inputParameterName == 'reportParameter_Work_Date')}">
											  	<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" value="<%= (new java.text.SimpleDateFormat("MM/dd/yyyy")).format(new java.util.Date()) %>" size="15" maxlength="11" readonly="true"/>
											    </c:if>
											    <c:if test="${(inputParameterName != 'reportParameter_Work_Date')}">
											  	<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" maxlength="11" readonly="true"/>
											    </c:if>											    
											  
											  <img id="${inputParameterName}-trigger" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 style="vertical-align:top;" onclick="test(); return false;"/></td>
										
											</c:if>
											
											<c:if test='${reportParameter.valueClassName != "java.util.Date"}'>
											  <c:set var="inputParameterName" value='${"reportParameter_"}${reportParameter.name}'/>
										      <td class="listwhitetext" colspan="2" width="100px">
										     
												<c:if test="${jobNumber==null || jobNumber==''}">
													<c:if test="${(inputParameterName == 'reportParameter_Corporate ID')}">
											  			<s:textfield cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="<%=sessionCorpID %>" readonly="true" size="15"/>
											  		</c:if>
											  		
											  		<c:if test="${!(inputParameterName == 'reportParameter_Corporate ID')}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Warehouse')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{house}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${house}"/>
			    										</c:if>
			    											 <c:if test="${(inputParameterName == 'reportParameter_Service Group')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{tcktGrp}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${tcktGrp}"/>
			    				                     	</c:if> 
			    										<c:if test="${(inputParameterName == 'reportParameter_UserName')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{usrName}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${usrName}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Coordinator')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{soList}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${soList}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Estimator')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{esList}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${esList}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Payment Status')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{payableStatusList}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${payableStatusList}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Approval Status')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{payingStatusList}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${payingStatusList}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_PaymentStatus')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'All', 'Fully Paid', 'Unpaid'}" cssStyle="width:115px" />
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_ApprovalStatus')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'All', 'Approved', 'Unapproved'}" cssStyle="width:115px" />
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Payment_Status')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Fully Paid', 'Unpaid (Others)', 'All'}" cssStyle="width:130px" />
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Approval_Status')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Approved', 'Unapproved (Others)', 'All'}" cssStyle="width:130px" />
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Mode')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{Mode}" cssStyle="width:115px" headerKey="" headerValue="" value="${Mode}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_RedSky Customer')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{redskyCustomer}" cssStyle="width:175px" headerKey="" headerValue="" value="${redskyCustomer}"/>
			    										</c:if>
			    									
			    										<c:if test="${(inputParameterName == 'reportParameter_Service_Contract')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{OMNI}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${OMNI}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Commodity')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{COMMODITS}" cssStyle="width:115px" headerKey="%%" headerValue="all" value="${COMMODITS}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Commodities')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" id="commodityTypes" list="%{Commodities}" value="%{multipleCommodity}" cssStyle="width:130px; height:100px" multiple="true" headerKey="%{multipleCommodity}" headerValue="All"/><td><font style="font-size:9px;"><u>* Use Control + mouse to select multiple Commodities</u></font></td>
														</c:if>
														
														<c:if test="${(inputParameterName == 'reportParameter_Company Divisions')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" id="companyDivisions" list="%{CompanyDivisions}" value="%{multipleCompanyDivisions}" cssStyle="width:125px; height:70px" multiple="true" headerKey="%{multipleCompanyDivisions}" headerValue="All"/><td><font style="font-size:9px;"><u>* Use Control + mouse to select multiple Company Divisions</u></font></td>
														</c:if>
														
														<c:if test="${(inputParameterName == 'reportParameter_Condition')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'AND','OR'}" cssStyle="width:100px"/>
														</c:if>
														
														<c:if test="${(inputParameterName == 'reportParameter_Date Criteria')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Move Date','Survey Date'}" headerKey="%%" headerValue="All" cssStyle="width:100px"/>
														</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_User Type')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'ACCOUNT','AGENT','CUSTOMER','PARTNER','USER'}" cssStyle="width:115px" headerKey="%%" headerValue="all" value=" "/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Crew Name')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{CREWNAME}" cssStyle="width:115px"  headerKey="%%" headerValue="all" value="${CREWNAME}"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Crew Type')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{CREWTYPE}" cssStyle="width:115px"  headerKey="%%" headerValue="all" value="${CREWTYPE}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Order By')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{actionType}" cssStyle="width:115px"   value="${actionType}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Routing')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{ROUTING}" cssStyle="width:115px" headerKey="" headerValue="" value="${ROUTING}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Currency')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{CURRENCY}" cssStyle="width:115px" headerKey="%%" headerValue="All" value="${CURRENCY}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Job Type')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{JOB}" cssStyle="width:115px" headerKey="%%" headerValue="All" value="${JOB}"/>
			    										</c:if>
			    										
			    										<c:if test="${(inputParameterName == 'reportParameter_Company Division')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{COMPANYCODE}" cssStyle="width:115px" headerKey="%%" headerValue="All" value="${COMPANYCODE}"/>
			    				                     	</c:if>
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Movement')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{customsMovement}" cssStyle="width:115px" headerKey="%%" headerValue="All" value="${customsMovement}"/>
			    				                     	</c:if>
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Status')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{customStatus}" cssStyle="width:115px" headerKey="%%" headerValue="All" value="${customStatus}"/>
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Contract')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{contract}" cssStyle="width:200px" headerKey="" headerValue="" value="${contract}"/>
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Vanline Agency')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{vanLineCodeList}" cssStyle="width:80px" headerKey="%%" headerValue="All" value="${vanLineCodeList}"/>
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Origin Base')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{BaseScore}" cssStyle="width:200px" headerKey="" headerValue="" value="${BaseScore}"/>
			    				                     	</c:if>
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_SO Status')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="{'All Jobs', 'Jobs exclude cancelled'}" cssStyle="width:115px" />
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Send Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Print','Email'}" cssStyle="width:115px"  value=" "/>
															</c:if>	
			    										<c:if test="${(inputParameterName == 'reportParameter_order')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{order}" cssStyle="width:115px" />
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Sort By')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Relocation Service','City'}" cssStyle="width:150px"  value=""/>
															</c:if>	
			    										<c:if test="${(inputParameterName == 'reportParameter_Relocation Services')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{relocationServices}" cssStyle="width:150px" headerKey="%%" headerValue="All"/>
			    										</c:if>
			    										<c:if test="${(inputParameterName == 'reportParameter_Bill To')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Bill To" size="15"/>
			    										    <img class="openpopup" width="17" height="20" style="vertical-align:top;" onclick="window.open('partnersPopup.html?&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Bill To','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Account')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Account" size="18"/>
			    										   <td><img class="openpopup" width="17" height="20" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Account','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" /></td>
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_PartnerCode')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_PartnerCode" size="15" value=""/> <td><img class="openpopup" width="17" height="20" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_PartnerCode','PartnerCode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" /></td>
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Vendor Name')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Vendor Name" size="15" value="%%"/>
			    										   <img class="openpopup" width="17" height="20" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Vendor Name','vendorCode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                      				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Agent Code')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Agent Code" readonly="true" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:top;" onclick="window.open('partnerAgentsPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Agent Code','billtocode','scrollbars=yes,resizable=yes,width=980,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Sales Person')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{sale}" cssStyle="width:105px" headerKey="%%" headerValue="ALL" value=""/>
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Sales_Person')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{saleperson}" cssStyle="width:105px" headerKey="%%" headerValue="ALL" value=""/>
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Job Owner')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{jobOwner}" cssStyle="width:105px" headerKey="%%" headerValue="all" value="${jobOwner}"/>
			    				                     	</c:if>
			    				                     	
			    				                     	<%-- <c:if test="${(inputParameterName == 'reportParameter_Contractor')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{contractor}" cssStyle="width:105px" headerKey="%%" headerValue="all" value="${contractor}"/>
			    				                     	</c:if> --%>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Contractor')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Contractor" size="15"/>
			    										   <td><img class="openpopup" width="17" height="20" onclick="window.open('partnersPopup.html?&partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Contractor','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" /></td>
															<s:hidden name="secondDescription" /> 
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Sales Person / Consultant')}" >
			    				                     	<c:if test="${visibilityForSalesPortla=='false'}">
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{owner}" cssStyle="width:105px" headerKey="%%" headerValue="all" value="${owner}"/>
			    				                     	</c:if>
			    				                     	<c:if test="${visibilityForSalesPortla=='true'}">			    				                     
			    				                     	<s:textfield cssClass="input-textUpper"  name="${inputParameterName}" readonly="true"  size="15" value="${sessionUserName}"/>
			    										</c:if>
			    				                     	</c:if>
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Relationship Owner')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{owner}" cssStyle="width:105px" headerKey="%%" headerValue="all" value="${owner}"/>
			    				                     	</c:if>
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Billing Group')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{storageBillingGroup}" cssStyle="width:105px" headerKey="" headerValue="" value="${storageBillingGroup}"/>
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Agent Group')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{agentGroup}" cssStyle="width:105px"  headerKey="%%" headerValue="All" value="${agentGroup}"/>
			    				                     	</c:if>
			    										
			    										<c:if test="${reports.corpID == 'SSCW'}">
				    										<c:if test="${!(inputParameterName == 'reportParameter_Warehouse') && !(inputParameterName == 'reportParameter_Coordinator')  && !(inputParameterName == 'reportParameter_Payment Status') && !(inputParameterName == 'reportParameter_Approval Status') && !(inputParameterName == 'reportParameter_Service_Contract') && !(inputParameterName == 'reportParameter_Mode') && !(inputParameterName == 'reportParameter_RedSky Customer') && !(inputParameterName == 'reportParameter_Commodity') && !(inputParameterName == 'reportParameter_Commodities') && !(inputParameterName == 'reportParameter_Company Divisions') && !(inputParameterName == 'reportParameter_Crew Name') && !(inputParameterName == 'reportParameter_Crew Type') && !(inputParameterName == 'reportParameter_Order By') && !(inputParameterName == 'reportParameter_Routing') && !(inputParameterName == 'reportParameter_Currency') && !(inputParameterName == 'reportParameter_Job Type') && !(inputParameterName == 'reportParameter_Company Division') && !(inputParameterName == 'reportParameter_Status') && !(inputParameterName == 'reportParameter_Movement') && !(inputParameterName == 'reportParameter_Bill To') && !(inputParameterName == 'reportParameter_Account') && !(inputParameterName == 'reportParameter_Sales Person') && !(inputParameterName == 'reportParameter_Sales_Person') && !(inputParameterName == 'reportParameter_Job Owner')  && !(inputParameterName == 'reportParameter_Contractor')  && !(inputParameterName == 'reportParameter_Sales Person / Consultant') && !(inputParameterName == 'reportParameter_Relationship Owner') && !(inputParameterName == 'reportParameter_Billing Group') && !(inputParameterName == 'reportParameter_Agent Group') && !(inputParameterName == 'reportParameter_Estimator') && !(inputParameterName == 'reportParameter_Contract') && !(inputParameterName == 'reportParameter_Vanline Agency')  && !(inputParameterName == 'reportParameter_Condition') && !(inputParameterName == 'reportParameter_Date Criteria') && !(inputParameterName == 'reportParameter_User Type') && !(inputParameterName == 'reportParameter_PaymentStatus') && !(inputParameterName == 'reportParameter_ApprovalStatus') && !(inputParameterName == 'reportParameter_Payment_Status') && !(inputParameterName == 'reportParameter_Approval_Status') && !(inputParameterName == 'reportParameter_SO Status') && !(inputParameterName == 'reportParameter_UserName') && !(inputParameterName == 'reportParameter_PartnerCode') && !(inputParameterName == 'reportParameter_Vendor Name')  && !(inputParameterName == 'reportParameter_Agent Code') && !(inputParameterName == 'reportParameter_Origin Base') && !(inputParameterName == 'reportParameter_order') && !(inputParameterName == 'reportParameter_Send Option') && !(inputParameterName == 'reportParameter_Sort By') && !(inputParameterName == 'reportParameter_Relocation Services') && !(inputParameterName == 'reportParameter_Service Group')}" >
				    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
				    										</c:if>
			    										</c:if>
			    										<c:if test="${reports.corpID != 'SSCW'}">
			    											<c:if test="${!(inputParameterName == 'reportParameter_Warehouse') && !(inputParameterName == 'reportParameter_Coordinator') && !(inputParameterName == 'reportParameter_Payment Status') && !(inputParameterName == 'reportParameter_Approval Status') && !(inputParameterName == 'reportParameter_Service_Contract')&& !(inputParameterName == 'reportParameter_Mode') && !(inputParameterName == 'reportParameter_RedSky Customer') && !(inputParameterName == 'reportParameter_Commodity') && !(inputParameterName == 'reportParameter_Commodities') && !(inputParameterName == 'reportParameter_Company Divisions') && !(inputParameterName == 'reportParameter_Crew Name') && !(inputParameterName == 'reportParameter_Crew Type') && !(inputParameterName == 'reportParameter_Order By') && !(inputParameterName == 'reportParameter_Routing') && !(inputParameterName == 'reportParameter_Invoice_From') && !(inputParameterName == 'reportParameter_Invoice_To') && !(inputParameterName == 'reportParameter_Currency') && !(inputParameterName == 'reportParameter_Job Type') && !(inputParameterName == 'reportParameter_Company Division') && !(inputParameterName == 'reportParameter_Status') && !(inputParameterName == 'reportParameter_Movement') && !(inputParameterName == 'reportParameter_Bill To') && !(inputParameterName == 'reportParameter_Account') && !(inputParameterName == 'reportParameter_Sales Person') && !(inputParameterName == 'reportParameter_Sales_Person') && !(inputParameterName == 'reportParameter_Job Owner') && !(inputParameterName == 'reportParameter_Contractor') && !(inputParameterName == 'reportParameter_Sales Person / Consultant') && !(inputParameterName == 'reportParameter_Relationship Owner') && !(inputParameterName == 'reportParameter_Billing Group') && !(inputParameterName == 'reportParameter_Agent Group') && !(inputParameterName == 'reportParameter_Estimator') && !(inputParameterName == 'reportParameter_Contract') && !(inputParameterName == 'reportParameter_Vanline Agency') && !(inputParameterName == 'reportParameter_Condition') && !(inputParameterName == 'reportParameter_Date Criteria') && !(inputParameterName == 'reportParameter_User Type') && !(inputParameterName == 'reportParameter_PaymentStatus') && !(inputParameterName == 'reportParameter_ApprovalStatus') && !(inputParameterName == 'reportParameter_Payment_Status') && !(inputParameterName == 'reportParameter_Approval_Status') && !(inputParameterName == 'reportParameter_SO Status') && !(inputParameterName == 'reportParameter_UserName') && !(inputParameterName == 'reportParameter_PartnerCode') && !(inputParameterName == 'reportParameter_Vendor Name')   && !(inputParameterName == 'reportParameter_Agent Code') && !(inputParameterName == 'reportParameter_Origin Base') && !(inputParameterName == 'reportParameter_order') && !(inputParameterName == 'reportParameter_Send Option') && !(inputParameterName == 'reportParameter_Sort By') && !(inputParameterName == 'reportParameter_Relocation Services') && !(inputParameterName == 'reportParameter_Service Group')}" >
				    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
				    										</c:if>
				    										<c:if test="${inputParameterName == 'reportParameter_Invoice_From'}" >
				    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="17001"/>
				    										</c:if>
				    										<c:if test="${inputParameterName == 'reportParameter_Invoice_To'}" >
				    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="99999"/>
				    										</c:if>
				    									</c:if>
											  		</c:if>
											  	</c:if>
											  	<c:if test="${!(jobNumber==null || jobNumber=='')}">
											  		<c:if test="${(inputParameterName == 'reportParameter_Service Order Number')}">
											  			<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" value="${jobNumber}" readonly="true" size="15"/>
											  		</c:if>
											  		
											  		<c:if test="${!(sequenceNumber==null || sequenceNumber=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Customer File Number')}">
												  			<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" value="${sequenceNumber}" readonly="true" size="15"/>
												  		</c:if>
											  		</c:if>
											  		
											  		<c:if test="${!(inputParameterName == 'reportParameter_Service Order Number')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Corporate ID')}">
												  			<s:textfield cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="<%=sessionCorpID %>" readonly="true" size="15" />
												  		</c:if>
													  	<c:if test="${!(inputParameterName == 'reportParameter_Corporate ID')}">
													  	<c:if test="${reports.corpID == 'SSCW'}">
													  		<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
													  	</c:if>		
													  	
													  	<c:if test="${reports.corpID != 'SSCW'}">
													  		<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
													  	</c:if>	
													  	</c:if>			  	
											  		</c:if>
											  	</c:if>
											  </td>
											</c:if>
										</tr> 
									  <tr><td class="listwhitetext" style="height:10px" colspan="4"></td></tr>  
						            </c:forEach>
						            </tbody>
						           </table>
						           <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
						           <thead>
						           <c:if test="${usertype!='DRIVER'}">
						            <tr>
							           <td align="left"  colspan="2" style="padding-left:10px">
							           		<s:textarea name="reports.reportComment"  onkeypress="return imposeMaxLength(this,1000);" rows="4" cols="50" readonly="true" cssClass="textarea"/>
							           </td>
						            </tr>
						           </c:if>
						            <tr><td class="listwhitetext" style="height:20px"></td></tr>  
									<tr><td><b>&nbsp;&nbsp;Output Format : </b></td></tr> 
									<tr><td class="listwhitetext" style="height:10px"></td></tr> 
									<tr> 
						
									
									<td colspan="2" class="listwhitetext">&nbsp;&nbsp;
									<c:if test="${(reports.csv == true)}">
									<img alt="CSV File" src="<c:url value='/images/csv1.gif'/>" onclick="return validatefields('CSV');"/><a href="#" onClick="return validatefields('CSV');"> CSV </a>
									</c:if>
									<c:if test="${(reports.xls == true)}">
									&nbsp;&nbsp;<img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validatefields('XLS');"/><a href="#" onClick="return validatefields('XLS');"> XLS </a>
									</c:if>
									<c:if test="${(reports.html == true)}">
									&nbsp;&nbsp;<img alt="HTML File" src="<c:url value='/images/html1.gif'/>" onclick="return validatefields('HTML');"/><a href="#" onClick="return validatefields('HTML');"> HTML </a>
									</c:if>
									<c:if test="${(reports.pdf == true)}">
									&nbsp;&nbsp;<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="return validatefields('PDF');"/> <a href="#" onClick="return validatefields('PDF');"> PDF </a>
									</c:if>
									<c:if test="${(reports.rtf == true)}">
									&nbsp;&nbsp;<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="return validatefields('RTF');"/> <a href="#" onClick="return validatefields('RTF');"> DOC </a>
									</c:if>
									<c:if test="${(reports.extract == true)}">
									&nbsp;&nbsp;<img alt="EXTRACT" src="<c:url value='/images/extract-para.png'/>" onclick="return validatefields('EXTRACT');"/> <a href="#" onClick="return validatefields('EXTRACT');">EXTRACT </a>
									</c:if>
									<c:if test="${(reports.docx == true)}">
									&nbsp;&nbsp;<img alt="Docx File" src="<c:url value='/images/docx.gif'/>" onclick="return validatefields('DOCX');"/> <a href="#" onClick="return validatefields('DOCX');"> DOCX </a>
									</c:if>
									
									</td></tr>				  		
									
									
							</table>
						</td>	
		  			</tr>
		  		</tbody>
			</table>
            <table><tr><td>Note: Use % as wildcard in parameter section.</td></tr></table>
           <c:if test="${visibilityForSalesPortla=='false'}">
			<div id="docs">
				<table id="docs" class="notesDetailTable" cellspacing="0" cellpadding="0" style="width:510px;">
				<tbody>
					<tr><td colspan="15"><div class="subcontent-tab" style="line-height:20px;padding-left:5px;">Enter information below to file this report in the Docs. repository :</div></td></tr>  
								
									<tr><td class="listwhitetext" style="height:10px"></td></tr>  
									<tr>
										<td align="right" class="listwhitetext">Document Type : &nbsp;</td>
										<td align="left" style="width:187px"><s:select name="docFileType" cssClass="list-menu" list="%{docsList}" headerKey="" headerValue="" cssStyle="width:110px" onclick="setDescriptionDuplicate(this),documentMap(this);"/></td>
									    <td class="listwhitetext" align="right">Map&nbsp;</td>
									    <td align="left"><s:select name="mapFolder" cssClass="list-menu" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px"/></td>
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
									</tr>
									<tr>
										<td align="right" class="listwhitetext" style="width:120px" >Description : &nbsp;</td>
										<td align="left" colspan="5"><s:textfield name="myFile.description" value="" cssClass="input-text" size="50"/></td>
										<td align="left" class="listwhitetext" style="width:20px"></td>
										<td colspan="2" align="right" style="padding-right:48px;"><s:submit name="upload" key="button.upload" action="uploadReport"  cssClass="cssbutton" cssStyle="width:70px; height:25px" onclick="return validateFields1();"/></td>
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:30px"></td>
									</tr><!--
									<tr>
										<td align="right" class="listwhitetext">File To Upload : &nbsp;</td>
										<td align="left" class="listwhitetext"><s:file name="file" label="%{getText('uploadForm.file')}"  cssClass="text file" required="true"/></td>
									</tr>
									-->
								</tbody>
					</table> 		
			</div>
			</c:if>
	<table><tr><td></td></tr></table>
	  <c:if test="${usertype!='DRIVER'}">
	<table>
		<tbody>
		  
			<tr height="40px;"><td align="right" style="padding-left: 370px;"><strong><a href="#" onClick="return refreshParent(); abc();"><font style="color: #EB8234;font-size:11px;"><u>Close this window</u></font></a></strong></td></tr>  		
	  	</tbody>
	</table> 
	</c:if>
	<s:hidden name="fileType" />
	<div id="mydiv1" style="position:absolute; visibility:visible; top:200px; left:100px;"></div>
</s:form>
<script type="text/javascript">   
	displayStatusReason()
</script>
<script type="text/javascript">
	
    var myimage = document.getElementsByTagName("img"); 
   	for (var i = 0; i < myimage.length; i++) {
	   	var idinms=myimage[i].getAttribute('id');
	   	if(idinms != null && (idinms.split('-')[1]=="trigger")){
		   	var args1=idinms.split('-');
		//	alert(args1[0]);
		   	RANGE_CAL_1 = new Calendar({
		           inputField: args1[0],
		           dateFormat: "%m/%d/%Y",
		           trigger: idinms,
		           bottomBar: true,
		           animation:true,
		           onSelect: function() {                             
		               this.hide();
		       }
		   });
	   	}
   	}
</script>
</body>
<script type="text/javascript">   
 Form.focusFirstElement($("reportForm")); 
</script>
					
