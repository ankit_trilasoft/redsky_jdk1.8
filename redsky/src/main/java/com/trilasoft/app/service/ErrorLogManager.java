package com.trilasoft.app.service;
import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ErrorLog;

public interface ErrorLogManager extends GenericManager<ErrorLog, Long> {
	public void saveLogError(String corpID, Date createdOn ,String message , String methods , String createdBy , String module );
	 public List searchErrorLogFile(String corpid,String module,String createdBy,Date createdOn);
	 public List findDistinct();
}
