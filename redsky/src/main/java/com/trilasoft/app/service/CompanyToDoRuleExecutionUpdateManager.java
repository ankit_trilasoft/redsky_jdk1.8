package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CompanyToDoRuleExecutionUpdate;

public interface CompanyToDoRuleExecutionUpdateManager extends GenericManager<CompanyToDoRuleExecutionUpdate, Long> {

	public List<CompanyToDoRuleExecutionUpdate> findByCorpID(String corpId);
}
