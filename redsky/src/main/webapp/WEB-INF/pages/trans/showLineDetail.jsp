<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineList.heading'/>"/>   
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=1;i++){
			document.forms['searchForm'].elements['vanLine.agent'].value = "";
			document.forms['searchForm'].elements['vanLine.weekEnding'].value = "";			
			document.forms['searchForm'].elements['vanLine.glType'].value = "";
			document.forms['searchForm'].elements['vanLine.reconcileStatus'].value = "";
		}
}
</script>
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="JavaScript" type="text/javascript">
	var cal = new CalendarPopup(); 
	cal.showYearNavigation(); 
	cal.showYearNavigationInput();  
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
!width:95%;
}
</style>
</head>   
  
<s:form id="searchForm" action="searchVanLine" method="post" validate="true" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		


<c:out value="${searchresults}" escapeXml="false" />  


<s:set name="showVanLines" value="showVanLines" scope="request"/>   
<display:table name="showVanLines" class="table" requestURI="" id="showVanLines" export="true" defaultsort="2" pagesize="10" style="width:95%" >   
	<display:column property="registrationNumber" sortable="true" titleKey="vanLine.regNum"/>	
	<display:column property="shipperName" sortable="true" titleKey="vanLine.shipper"/>
	<display:column property="glType" sortable="true" titleKey="vanLine.glType"/>
	<display:column property="distributionAmount" sortable="true" titleKey="vanLine.amtDueAgent"/>	

	

       
    <display:setProperty name="paging.banner.item_name" value="vanLine"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLines"/>    
    <display:setProperty name="export.excel.filename" value="Van Line List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line List.pdf"/>   
</display:table>   
  
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
 
<script type="text/javascript">   
    highlightTableRows("vanLineList");  
    Form.focusFirstElement($("searchForm"));    
</script> 