<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<title><fmt:message key="serviceOrderDetail.title" /></title>
<meta name="heading" content="<fmt:message key='serviceOrderDetail.heading'/>" />
<style type="text/css">h2 {background-color: #FBBFFF}</style>

<script language="javascript" type="text/javascript">
function check(field)	
{
var l=field.value;
document.serviceOrderForm.characterstics1.value=l;
//alert(document.form1.characterstics1.value);
}

function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	var tim1=document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value; 
	<%--alert(document.forms['serviceOrderForm'].elements['destinationCountryCode'].value); --%>
	if(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value == "null" ){
		document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value="";
	}
	
}


</script>

<script type="text/javascript">
function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true}
}
}


function calcNetWeight1(){
	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value = document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value; 
}
function calcNetWeight2(){
	document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value = document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value - document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value;
}
function calcNetWeight3(){
	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value - document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value;
}
function calcNetWeight4(){
	document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value = document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value - document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value;
}
</script>




<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	 
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
</script>


<script type="text/javascript">
/**
 * DHTML phone number validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }
</script>






<script type="text/javascript">
	function autoPopulate_serviceOrder_originCountry(targetElement) {
		var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value=originCountryCode.substring(0,originCountryCode.indexOf(":"));
		targetElement.form.elements['serviceOrder.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+1,originCountryCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_serviceOrder_destinationCountry(targetElement) {
		var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value=destinationCountryCode.substring(0,destinationCountryCode.indexOf(":"));
		targetElement.form.elements['serviceOrder.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+1,destinationCountryCode.length);
	}
</script>
<script>
	function wrongValue(field, event) {
		var key, keyChar;
		if(window.event) {
			key = window.event.keyCode;
		}else if(event) {
			key = event.which;
		}
		else
			return true;
		
		keyChar = String.fromCharCode(key);
		
		if((key==null) || (key==0) || (key==8) ||(key== 9) || (key==13) || (key==27)) {
			window.status="";
			return true;
		}
		else {
		if((("01234567890").indexOf(keyChar)>-1)) {
		
			window.status="";
			return true;
		}else {
		
			window.status="It accepts only numeric value";
			alert("It accepts only numeric value");
			return false;
		}
		}
	}
</script>




<script type="text/javascript">
	function openOriginLocation() {
	
		var city = document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		var country = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
		var zip = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
		var address = document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
}
	function openDestinationLocation() {
		var city = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
		var zip = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
		var address = document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
}
</script>


<script type="text/javascript"> 
		function forwardToMyMessage1(){ 
		//alert("Enter valid Number");
		document.forms['serviceOrderForm'].elements['serviceOrder.actualNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.actualCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;		
			

		}
	</script>


</head>

<s:form id="serviceOrderForm" action="saveServiceOrder" method="post" validate="true">
	<s:hidden name="serviceOrder.id" />
	
<div id="Layer1">
<c:if test="${not empty serviceOrder.id}">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/head-left.jpg'/>" /></td>
	<td width="65" class="detailActiveTabLabel content-tab"><a href="customerServiceOrders.html?id=${customerFile.id} " ><font color="white">S/oDetails</font></a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/head-right.jpg'/>" /></td>
	<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="50" class="content-tab" align="center"><a href="editBilling.html?id=${serviceOrder.id}" >Bill</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="65" class="content-tab" align="center"><a href="servicePartners.html?id=${serviceOrder.id}" >Partner</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="50" class="content-tab" align="center"><a href="containers.html?id=${serviceOrder.id}" >Pack</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="50" class="content-tab" align="center">Date</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="65" class="content-tab" align="center"><a href="editMiscellaneous.html?id=${serviceOrder.id}">Domestic</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<c:if test="${serviceOrder.job =='RLO'}"> 
<td width="50" class="content-tab" align="center"><a href="editDspDetails.html?id=${serviceOrder.id}"/>Status</a></td>
</c:if>
<c:if test="${serviceOrder.job !='RLO'}"> 
<td width="50" class="content-tab" align="center"><a href="editTrackingStatus.html?id=${serviceOrder.id}"/>Status</a></td>
</c:if>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="50" class="content-tab" align="center"><a href="customerWorkTickets.html?id=${serviceOrder.id}" >Ticket</a></td>
<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="65" class="content-tab" align="center"><a href="costings.html?id=${serviceOrder.id}" >Invoicing</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="40" class="content-tab" align="center"><a href="claims.html?id=${serviceOrder.id}" >Claim</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="75" class="content-tab" align="center"><a href="editCustomerFile.html?id=${customerFile.id}" >CustomerFile</a></td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>

</tr>
</tbody>
</table>
</c:if>
<c:if test="${empty serviceOrder.id}">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/head-left.jpg'/>" /></td>
	<td width="65" class="detailActiveTabLabel content-tab"><a href="customerServiceOrders.html?id=${customerFile.id} " ><font color="white">S/oDetails</font></a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/head-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="50" class="content-tab" align="center">Bill</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="65" class="content-tab" align="center">Partner</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="50" class="content-tab" align="center">Pack</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="50" class="content-tab" align="center">Date</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="50" class="content-tab" align="center">Domestic</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="65" class="content-tab" align="center">Status</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="50" class="content-tab" align="center">Ticket</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>	
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="65" class="content-tab" align="center">Invoicing</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="40" class="content-tab" align="center">Claim</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="75" class="content-tab" align="center"><a href="editCustomerFile.html?id=${customerFile.id}">CustomerFile</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="8"></td>

</tr>
</tbody>
</table>
</c:if>





<%-- 	
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
	
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/head-left.jpg'/>" /></td>
	<td width="100" class="detailActiveTabLabel content-tab"><a href="customerServiceOrders.html?id=${customerFile.id} " ><font color="white">S/oDetails</font></a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/head-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="editBilling.html?id=${serviceOrder.id}" >Bill</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="servicePartners.html?id=${serviceOrder.id}" >Partner</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="containers.html?id=${serviceOrder.id}"  >Pack</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="editMiscellaneous.html?id=${serviceOrder.id}">Domestic</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="editTrackingStatus.html?id=${serviceOrder.id}"/>Status</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="customerWorkTickets.html?id=${serviceOrder.id}" >Ticket</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>	
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="costings.html?id=${serviceOrder.id}" >Invoicing</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="claims.html?id=${serviceOrder.id}" >Claim</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="editCustomerFile.html?id=${customerFile.id}" >CustomerFile</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
</tr>
</tbody>
</table>
--%>

<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
<tbody>
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr>
	<td align="left" class="listwhitebox"></td><td align="left" class="listwhitebox"><fmt:message key='serviceOrder.prefix'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.firstName'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.mi'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.lastName'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.suffix'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.vip'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.shipNumber'/></td>
	<td align="left" class="listwhitebox"><fmt:message key='serviceOrder.status'/></td>
</tr>
<tr>
	<td width="4" align="left" class="listwhitebox"></td>
	<td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.prefix" size="15" maxlength="15" required="true"  onfocus = "myDate();" readonly="true"/></td>
	<td align="left"><s:textfield cssClass="input-textUpper" key="serviceOrder.firstName" size="20" maxlength="30" required="true"  readonly="true"/> </td>
	<td align="left"><s:textfield cssClass="input-textUpper" key="serviceOrder.mi" cssStyle="width:13px" maxlength="1" required="true"   readonly="true" /> </td>
	<td align="left"><s:textfield cssClass="input-textUpper" key="serviceOrder.lastName"  size="20" maxlength="80" required="true" readonly="true"/> </td>
	<td align="left"><s:textfield cssClass="input-textUpper" key="serviceOrder.suffix"  size="6" maxlength="10" required="true"  readonly="true"/></td>
	<td align="left" class="listwhitetext"><input type="checkbox" name="checkbox" value="checkbox"></td>
	<td align="left"><s:textfield cssClass="input-textUpper" key="serviceOrder.shipNumber" size="10" maxlength="11"	required="true" readonly="true"  /> </td>
	<td><s:select cssClass="list-menu" name="serviceOrder.status"  list="%{JOB_STATUS}" headerKey="" headerValue="" cssStyle="width:60px"/></td>
</tr>
</tbody>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr>
										<td class="subcontent-tab" colspan="2">Jobs</td>
									</tr>
									<tr>
										<td colspan="5">
										<table class="detailTabLabel" cellspacing="3" cellpadding="0" border="0">
											<tbody>
												<tr>
											<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.registrationNumber'/></td>
    										<td><s:textfield cssClass="input-text" name="serviceOrder.registrationNumber" size="12" maxlength="12" required="true" /></td>
											<td></td><td  width="100"  align="right" class="listwhitetext"><fmt:message key='serviceOrder.job1'/></td>
											<td><s:select cssClass="list-menu" key="serviceOrder.job" list="%{job}" cssStyle="width:60px" headerKey="" headerValue=": Leave Empty"  /></td>
											<td></td><td width="200" align="right" class="listwhitetext"><fmt:message key='serviceOrder.routing'/></td>
    										<td><s:select cssClass="list-menu" key="serviceOrder.routing"  list="%{routing}" cssStyle="width:57px"/></td>
    										<td width="200px" align="right" class="listwhitetext"><fmt:message key='serviceOrder.statusDate'/></td>
    										<td><s:text id="FormatedInvoiceDate" name="FormDateValue"><s:param name="value" value="serviceOrder.statusDate"/></s:text>
                							<s:textfield cssClass="input-text" name="serviceOrder.statusDate" value="%{FormatedInvoiceDate}" required="true" size="7"  readonly="true"/></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							<td width="15" align="left" class="listwhitetext"></td>
                							</tr>
    										
                
    										
  											
  											
  											
											<tr>
											<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.estimator'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.estimator"  list="%{sale}" cssStyle="width:100px"/></td>
											<td></td><td align="right" class="listwhitetext"><fmt:message key='serviceOrder.serviceType'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.serviceType"  list="%{service}" cssStyle="width:60px"/></td>
											<td></td><td align="right" class="listwhitetext"><fmt:message key='serviceOrder.mode'/></td>
    										<td><s:select cssClass="list-menu" key="serviceOrder.mode"  list="%{mode}" cssStyle="width:57px"/></td>
    										<td width="400" align="right" class="listwhitetext"><fmt:message key='serviceOrder.quoteStatus'/></td>
    										<td><s:select cssClass="list-menu" key="serviceOrder.quoteStatus"  list="%{QUOTESTATUS}" cssStyle="width:100px"/></td>
											</tr>
    										
											
											<tr>
											<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.salesMan'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.salesMan" list="%{sale}" cssStyle="width:100px"/></td>
											<td></td><td align="right" class="listwhitetext"><fmt:message key='serviceOrder.commodity'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.commodity"   list="%{commodit}" cssStyle="width:60px" headerKey="" headerValue=": Leave Empty"  /></td>
											<td></td><td align="right" class="listwhitetext"><fmt:message key='serviceOrder.specificParameter'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.specificParameter" list="%{specific}" cssStyle="width:57px"/></td>
    										</tr>
  											
    						
											<tr>
											<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.coordinator'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.coordinator"  list="%{coord}" cssStyle="width:100px"/></td>
											<td></td><td width="200" align="right" class="listwhitetext"><fmt:message key='serviceOrder.packingMode'/></td>
    										<td><s:select cssClass="list-menu" name="serviceOrder.packingMode"  list="%{pkmode}" cssStyle="width:60px"/></td>
											<td></td><td align="right" class="listwhitetext"><fmt:message key='serviceOrder.socialSecurityNumber'/></td>
    										<td><s:textfield cssClass="input-text" name="serviceOrder.socialSecurityNumber" size="9" maxlength="11" required="true" /></td>
    										
</tbody>
</table>
</td>
</tr>

<tr>
<td height="10" align="left" class="listwhitetext">
<div class="subcontent-tab"><a href="javascript:void(0)" class="dsphead" onclick="dsp(this)">Weights & Volume&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="dspchar2">+</span></a></div>
<div class="dspcont" id="Layer5">
<table border="0">
<tbody>
<tr>
											<td></td>
											<td colspan="2" align="left" class="listwhitetext"><fmt:message key='labels.entitled'/></td>
											<td colspan="2" align="left" class="listwhitetext"><fmt:message key='labels.estimated'/></td>
    										<td colspan="2" align="left" class="listwhitetext"><fmt:message key='labels.actual1'/></td>
    										<td colspan="2" align="left" class="listwhitetext"><fmt:message key='labels.rewgh/fnl$'/></td>
</tr>
<tr>
											<td align="right" class="listwhitetext"><fmt:message key='labels.grossweight'/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.entitleGrossWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight1();" /></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.estimateGrossWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight2();" /></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.actualGrossWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight3();" /></td>
											<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.rwghGross" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight4();" /></td>
</tr>
<tr>
                                             <td align="right" class="listwhitetext"><fmt:message key='labels.tareweight'/></td>
                                             <td colspan="2"></td>
                                             <td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.estimateTareWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight2();" /></td>
                                             <td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.actualTareWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight3();" /></td>
                                             <td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.rwghTare" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" onchange="calcNetWeight4();" /></td>
</tr>
<tr>
											<td align="right" class="listwhitetext"><fmt:message key='labels.netweight'/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.entitleNetWeight" size="8" maxlength="10" required="true"  onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.estimatedNetWeight" size="8" maxlength="10" required="true"  onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.actualNetWeight" size="8" maxlength="10" required="true"  onkeydown="return onlyNumsAllowed(event)"/></td>
											<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.rwghNet" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)" /></td>
											<td align="right" class="listwhitetext"><fmt:message key='labels.units'/></td>
											<td width="200px"><s:radio name="serviceOrder.unit1" list="%{weightunits}" /></td>
</tr>
<tr>
											<td align="right" class="listwhitetext"><fmt:message key='labels.autoboat'/></td>
    										<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.entitleNumberAuto" maxlength="2" size="1" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.entitleBoatYN" maxlength="2"  size="1" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.estimateAuto" maxlength="2" size="1" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.estimateBoat" maxlength="2" size="1" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.actualAuto" maxlength="2" size="1" required="true" onkeydown="return onlyNumsAllowed(event)" /></td>
    										<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.actualBoat" maxlength="2" size="1" required="true" onkeydown="return onlyNumsAllowed(event)" /></td>
											
											<td></td>
</tr>
<tr>
											<td align="right" class="listwhitetext"><fmt:message key='labels.vehicleweight'/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.entitleAutoWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.estimatedAutoWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.actualAutoWeight" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.equipment'/></td>
    										<td colspan="2"><s:select cssClass="list-menu" name="serviceOrder.equipment" list="%{EQUIP}" cssStyle="width:75px" headerKey="" headerValue=": Leave Blank"  /></td>
    										
    										<td></td>
											</tr>

<tr>
											<td align="right" class="listwhitetext"><fmt:message key='labels.volume'/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.estimateCubicFeet" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.estimateTotalCubicFeet" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										<td align="right" colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.actualCubicFeet" size="8" maxlength="10" required="true" onkeydown="return onlyNumsAllowed(event)"/></td>
    										
    										<td align="right" class="listwhitetext"><fmt:message key='labels.volunit'/></td>
    										<td colspan="3" width="200px"><s:radio name="serviceOrder.unit2" list="%{volumeunits}" /></td>
    										</tr>
</tbody>
</table>
</div>
</td>
</tr>	
<tr>
<td colspan="7"></td>
</tr>
<tr>
<td height="10" align="left" class="listwhitetext">
<div class="subcontent-tab"><a href="javascript:void(0)" class="dsphead" onclick="dsp(this)">Origin&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="dspchar1">+</span></a></div>
<div class="dspcont" id="Layer4">
<table border="0">
<tbody>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originCompany'/></td>
	<td colspan="3"><s:textfield cssClass="input-text" name="serviceOrder.originCompany" size="25" maxlength="30"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originAddressLine1'/></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="serviceOrder.originAddressLine1" size="30" maxlength="100"/></td>
</tr>
<tr>
	<td colspan="5"><s:label label="    " /></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="serviceOrder.originAddressLine2" size="30" maxlength="100"/></td>
</tr>
<tr>
	<td colspan="5"><s:label label="    " /></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="serviceOrder.originAddressLine3" size="30" maxlength="100"/></td>
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originCity'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originCity" size="10" maxlength="30"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originArea'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originArea" size="3" maxlength="3" onkeydown="return onlyNumsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originState'/></td>
	<td><s:select cssClass="list-menu" name="serviceOrder.originState" list="%{state}" cssStyle="width:57px"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originZip'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originZip" size="11" maxlength="10" onkeydown="return onlyNumsAllowed(event)" /></td>
	</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originCounty' /></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.originCounty" size="15" maxlength="30"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originCountryCode'/></td>
	<td><s:select cssClass="list-menu" name="serviceOrder.originCountryCodeDesc" list="%{country}" cssStyle="width:55px" onchange="autoPopulate_serviceOrder_originCountry(this);"/></td><s:hidden key="serviceOrder.originCountryCode" value="%{serviceOrder.originCountryCode}"/>
	<td colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.originCountry" size="20" maxlength="25" readonly="true" /><img src="../redsky/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openOriginLocation();"/></td>
	<td></td>
	
	
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originDayPhone'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originDayPhone" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originDayExtn" size="2" maxlength="3" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originHomePhone'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originHomePhone" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originFax'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.originFax" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
    
    
    
    
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originLoadSite'/></td>
	<td colspan="2"><s:select cssClass="list-menu" name="serviceOrder.originLoadSite" list="%{loadsite}" cssStyle="width:57px"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originMilitary'/></td>
	<td><s:select cssClass="list-menu" name="serviceOrder.originMilitary" list="%{military}" cssStyle="width:57px"/></td>
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originContactName'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.originContactName" size="15" maxlength="35" onkeydown="return onlyCharsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originContactWork'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originContactWork" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.originContactExtn" size="2" maxlength="3" onkeydown="return onlyNumsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.originContactPhone'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.originContactPhone" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
</tr>
<tr>
	
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.email'/></td>
	<td colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.email" size="40" maxlength="65" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.email2'/></td>
	<td colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.email2"  size="35" maxlength="65"  /></td>
</tr>
</table>
</div>
</td>
</tr>	

<tr>
<td height="10" align="left" class="listwhitetext">
<div class="subcontent-tab"><a href="javascript:void(0)" class="dsphead" onclick="dsp(this)">Destination&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="dspchar1">+</span></a></div>
<div class="dspcont" id="Layer3">
<table border="0">
<tbody>

<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationCompany'/></td>
	<td colspan="3"><s:textfield cssClass="input-text" name="serviceOrder.destinationCompany" size="25" maxlength="30"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationAddressLine1'/></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="serviceOrder.destinationAddressLine1" size="30" maxlength="100"/></td>
</tr>
<tr>
	<td colspan="5"><s:label label="    " /></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="serviceOrder.destinationAddressLine2" size="30" maxlength="100"/></td>
</tr>
<tr>
	<td colspan="5"><s:label label="    " /></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="serviceOrder.destinationAddressLine3" size="30" maxlength="100"/></td>
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationCity'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.destinationCity" size="10" maxlength="30"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationArea'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.destinationArea" size="3" maxlength="3" onkeydown="return onlyNumsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationState'/></td>
	<td><s:select cssClass="list-menu" name="serviceOrder.destinationState" list="%{state}" cssStyle="width:57px"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationZip'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.destinationZip" size="11" maxlength="10" onkeydown="return onlyNumsAllowed(event)" /></td>
	</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationCounty' /></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.destinationCounty" size="15" maxlength="30"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationCountryCode'/></td>
	<td><s:select cssClass="list-menu" name="serviceOrder.destinationCountryCodeDesc"  list="%{country}" cssStyle="width:55px" onchange="autoPopulate_serviceOrder_destinationCountry(this);"/></td><s:hidden key="serviceOrder.destinationCountryCode" />
	<td colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.destinationCountry" size="20" maxlength="25" readonly="true" /><img src="../redsky/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openDestinationLocation();"/></td>
	<td></td>
	
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationDayPhone'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.destinationDayPhone" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.destinationDayExtn" size="2" maxlength="3" onkeydown="return onlyNumsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationHomePhone'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.destinationHomePhone" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationFax'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.destinationFax" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
    
    
    
    
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationLoadSite'/></td>
	<td colspan="2"><s:select cssClass="list-menu" name="serviceOrder.destinationLoadSite" list="%{loadsite}" cssStyle="width:57px"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.destinationMilitary'/></td>
	<td><s:select cssClass="list-menu" name="serviceOrder.destinationMilitary" list="%{military}" cssStyle="width:57px"/></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.consignee'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.consignee" size="15" maxlength="30"/></td>
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.contactName'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.contactName" size="11" maxlength="35" onkeydown="return onlyCharsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.contactPhone'/></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.contactPhone" size="11" maxlength="20" onkeydown="return onlyNumsAllowed(event)"   /></td>
	<td><s:textfield cssClass="input-text" name="serviceOrder.destinationContactExtn" size="2" maxlength="3" onkeydown="return onlyNumsAllowed(event)" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.contactFax'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.contactFax" size="15" maxlength="20"  onkeydown="return onlyNumsAllowed(event)"   /></td>
</tr>
<tr>
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.email'/></td>
	<td colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.destinationEmail" size="40" maxlength="65"  /></td>
	
	<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.email2'/></td>
	<td colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.destinationEmail2"  size="35" maxlength="65"  /></td>
	
</tr>
</tbody>
</table>
</div>
</td>
<td valign="top" align="left" class="listwhite" /> 
<td align="left" class="listwhite" />
</tr>								

<table>
					<tbody>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.createdOn'/></td>
						<s:hidden name="serviceOrder.createdOn" />
						<td><s:date name="serviceOrder.createdOn" format="dd-MMM-yyyy"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.createdBy' /></td>
						
						<c:if test="${not empty serviceOrder.id}">
								<s:hidden name="serviceOrder.createdBy"/>
								<td><s:label name="createdBy" value="%{serviceOrder.createdBy}"/></td>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
								<s:hidden name="serviceOrder.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
																	
						<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.updatedOn'/></td>
						<s:hidden name="serviceOrder.updatedOn"/>
						<td><s:date name="serviceOrder.updatedOn" format="dd-MMM-yyyy"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='serviceOrder.updatedBy' /></td>
						<s:hidden name="serviceOrder.updatedBy" value="${pageContext.request.remoteUser}" />
						<td><s:label name="serviceOrder.updatedBy" /></td>
						</tr>
						</tbody>
						</table>
						</tbody>
						</table>
						</div>
<li class="buttonBar bottom"><s:submit cssClass="button"
		method="save" key="button.save" onclick="forwardToMyMessage1();"/> <c:if
		test="${not empty serviceOrder.id}">
		
	</c:if> <s:reset cssClass="button" key="Reset" onmousemove="myDate();"/>
</li>
<s:hidden name="serviceOrder.gbl"  />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="serviceOrder.contract" value="%{customerFile.contract}" />
<s:hidden name="serviceOrder.orderBy"  />
<s:hidden name="serviceOrder.orderPhone"  />
<s:hidden name="miscellaneous.id" value="%{miscellaneous.id}"/>
<s:hidden name="miscellaneous.shipNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="serviceOrder.billToCode" />
<s:hidden name="serviceOrder.payType"  />
<s:hidden name="serviceOrder.billToName" />
<s:hidden name="customerFile.id"  />
<s:hidden name="serviceOrder.corpID" value="SSCW"/>
<s:hidden name="serviceOrder.actualNetWeight"/>
<s:hidden name="serviceOrder.actualGrossWeight"/>
<s:hidden name="serviceOrder.actualCubicFeet"/>

<s:hidden name="miscellaneous.sequenceNumber"/>
<s:hidden name="miscellaneous.ship"/>

<s:hidden name="miscellaneous.pieceNumber"/>
<s:hidden name="miscellaneous.inventoryStickerNumber"/>
<s:hidden name="miscellaneous.descriptionStorage"/>
<s:hidden name="miscellaneous.listLotNumber"/>
<s:hidden name="miscellaneous.locationStorageLot"/>

<s:hidden name="miscellaneous.estimateGrossWeightKilo"/>

<s:hidden name="miscellaneous.actualGrossWeightKilo"/>

<s:hidden name="miscellaneous.estimateTareWeightKilo"/>

<s:hidden name="miscellaneous.actualTareWeightKilo"/>

<s:hidden name="miscellaneous.entitleNetWeightKilo"/>

<s:hidden name="miscellaneous.entitleGrossWeightKilo"/>

<s:hidden name="miscellaneous.estimatedNetWeightKilo"/>

<s:hidden name="miscellaneous.actualNetWeightKilo"/>
<s:hidden name="miscellaneous.estimateGood"/>
<s:hidden name="miscellaneous.estimateStorageGood"/>
<s:hidden name="miscellaneous.actualStorage"/>
<s:hidden name="miscellaneous.entitleSIT"/>
<s:hidden name="miscellaneous.estimateSIT"/>
<s:hidden name="miscellaneous.actualSIT"/>
<s:hidden name="miscellaneous.entitleNumberAuto"/>
<s:hidden name="miscellaneous.estimateAuto"/>
<s:hidden name="miscellaneous.actualAuto"/>
<s:hidden name="miscellaneous.bookerSelfPacking"/>
<s:hidden name="miscellaneous.bookerSelfHauling"/>
<s:hidden name="miscellaneous.bookerOwnAuthority"/>
<s:hidden name="miscellaneous.packAuthorize"/>
<s:hidden name="miscellaneous.packFullPartial"/>
<s:hidden name="miscellaneous.unPack"/>
<s:hidden name="miscellaneous.packingBulky"/>
<s:hidden name="miscellaneous.packingBulkyDescription"/>
<s:hidden name="miscellaneous.codeHauling"/>
<s:hidden name="miscellaneous.tarif"/>
<s:hidden name="miscellaneous.section"/>
<s:hidden name="miscellaneous.spaceReserved"/>
<s:hidden name="miscellaneous.applicationNumber3rdParty"/>
<s:hidden name="miscellaneous.applicationDescription3rdParty"/>
<s:hidden name="miscellaneous.whatAllOnOrigin"/>
<s:hidden name="miscellaneous.whatOtherDescription"/>
<s:hidden name="miscellaneous.insuranceValue"/>
<s:hidden name="miscellaneous.insuranceValueDescription"/>
<s:hidden name="miscellaneous.insuranceOption"/>
<s:hidden name="miscellaneous.insuranceYN"/>
<s:hidden name="miscellaneous.needWaivesYN"/>
<s:hidden name="miscellaneous.needWaiveEstimate"/>
<s:hidden name="miscellaneous.needWaiveSurvey"/>
<s:hidden name="miscellaneous.needWaivePeakRate"/>
<s:hidden name="miscellaneous.needWaiveSRA"/>
<s:hidden name="miscellaneous.needWaiveSIT"/>
<s:hidden name="miscellaneous.needWaiveSITOADAYN"/>
<s:hidden name="miscellaneous.authorizeSITDays"/>
<s:hidden name="miscellaneous.needWaive619"/>
<s:hidden name="miscellaneous.vanLineOrderNumber"/>
<s:hidden name="miscellaneous.vanLineContractNumber"/>
<s:hidden name="miscellaneous.vanLineNationalCode"/>
<s:hidden name="miscellaneous.originCountyCode"/>
<s:hidden name="miscellaneous.originStateCode"/>
<s:hidden name="miscellaneous.originCounty"/>
<s:hidden name="miscellaneous.originState"/>
<s:hidden name="miscellaneous.destinationCountyCode"/>
<s:hidden name="miscellaneous.destinationStateCode"/>
<s:hidden name="miscellaneous.destinationCode"/>
<s:hidden name="miscellaneous.destinationState"/>
<s:hidden name="miscellaneous.extraStopYN"/>
<s:hidden name="miscellaneous.mile"/>
<s:hidden name="miscellaneous.rateSIT"/>
<s:hidden name="miscellaneous.ratePSTG"/>
<s:hidden name="miscellaneous.rateInsurance"/>
<s:hidden name="miscellaneous.entitleAmountDoller"/>
<s:hidden name="miscellaneous.actualInsuranceAmount"/>
<s:hidden name="miscellaneous.vanLineNumber"/>
<s:hidden name="miscellaneous.estimatedRevenue"/>
<s:hidden name="miscellaneous.noStorageInOut"/>
<s:hidden name="miscellaneous.packMaterialYN"/>
<s:hidden name="miscellaneous.warehouse"/>
<s:hidden name="miscellaneous.overflow"/>
<s:hidden name="miscellaneous.tag"/>
<s:hidden name="miscellaneous.onHandWeight"/>
<s:hidden name="miscellaneous.test"/>
<s:hidden name="miscellaneous.entitleConsignment"/>
<s:hidden name="miscellaneous.estimateConsignment"/>
<s:hidden name="miscellaneous.actualConsignment"/>
<s:hidden name="miscellaneous.entitleHouseHoldGoodPound"/>
<s:hidden name="miscellaneous.estimateHouseHoldGoodPound"/>
<s:hidden name="miscellaneous.actualHouseHoldGood"/>
<s:hidden name="miscellaneous.entitleHouseHoldGoodKilo"/>
<s:hidden name="miscellaneous.estimateHouseHoldGoodKilo"/>
<s:hidden name="miscellaneous.actualHouseHoldGoodKilo"/>
<s:hidden name="miscellaneous.estimateConsignmentKilo"/>
<s:hidden name="miscellaneous.entitleConsignmentKilo"/>
<s:hidden name="miscellaneous.actualConsignmentKilo"/>
<s:hidden name="miscellaneous.actualRevenueDollor"/>
<s:hidden name="miscellaneous.agentPerformNonperform"/>
<s:hidden name="miscellaneous.destination24HR"/>
<s:hidden name="miscellaneous.haulingStatus"/>
<s:hidden name="miscellaneous.storageInOut"/>
<s:hidden name="miscellaneous.insuranceCarrier"/>
<s:hidden name="miscellaneous.insurancePolicy"/>
<s:hidden name="miscellaneous.insuranceDate"/>
<s:hidden name="miscellaneous.storageInOutDate"/>
<s:hidden name="miscellaneous.storageInOutWeight"/>
<s:hidden name="miscellaneous.origin24Hr"/>
<s:hidden name="miscellaneous.vanLinePickupNumber"/>
<s:hidden name="miscellaneous.shipmentRegistrationDate"/>
<s:hidden name="miscellaneous.vanLineRemark1"/>
<s:hidden name="miscellaneous.vanLineRemark2"/>
<s:hidden name="miscellaneous.vanLineRemark3"/>
<s:hidden name="miscellaneous.vanLineRemark4"/>
<s:hidden name="miscellaneous.vanLineRemark5"/>
<s:hidden name="miscellaneous.selfHauling"/>
<s:hidden name="miscellaneous.specialInstructionCode"/>
<s:hidden name="miscellaneous.storageUnit"/>
<s:hidden name="miscellaneous.atlasDown"/>
<s:hidden name="miscellaneous.entitleBoatYN"/>
<s:hidden name="miscellaneous.estimateBoat"/>
<s:hidden name="miscellaneous.actualBoat"/>
<s:hidden name="miscellaneous.remarkSA"/>
<s:hidden name="miscellaneous.remarkSB"/>
<s:hidden name="miscellaneous.remarkSC"/>
<s:hidden name="miscellaneous.trailerVolumeWeight"/>
<s:hidden name="miscellaneous.gate"/>
<s:hidden name="miscellaneous.trailer"/>
<s:hidden name="miscellaneous.pendingNumber"/>
<s:hidden name="miscellaneous.settledVanLineAmount"/>
<s:hidden name="miscellaneous.vanLineCODAmount"/>
<s:hidden name="miscellaneous.prepaid"/>
<s:hidden name="miscellaneous.weightTicket"/>
<s:hidden name="miscellaneous.haulDiscusion"/>
<s:hidden name="miscellaneous.peakRate"/>
<s:hidden name="miscellaneous.createdBy"/>
<s:hidden name="miscellaneous.createdOn"/>
<s:hidden name="miscellaneous.updatedBy"/>
<s:hidden name="miscellaneous.updatedOn"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="session"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="session"/>
</c:if>
</s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("serviceOrderForm"));   
</script>
