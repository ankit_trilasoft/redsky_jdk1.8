package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.GenericDao;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.RoleBasedComponentPermissionDao;
import com.trilasoft.app.dao.UrlPatternRolesDefinitionSource;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.MenuItemPermission;
import com.trilasoft.app.model.RoleBasedComponentPermission;

public class RoleBasedComponentPermissionDaoHibernate extends GenericDaoHibernate<RoleBasedComponentPermission, Long> implements RoleBasedComponentPermissionDao {   
	
	/*private UrlPatternRolesDefinitionSource objectDefinitionSource;
	
	public UrlPatternRolesDefinitionSource getObjectDefinitionSource() {
		return objectDefinitionSource;
	}

	public void setObjectDefinitionSource(
			UrlPatternRolesDefinitionSource objectDefinitionSource) {
		this.objectDefinitionSource = objectDefinitionSource;
	}*/
	
	private static Map permMap = AppInitServlet.roleBasedComponentPerms;

	public RoleBasedComponentPermissionDaoHibernate() {   
        super(RoleBasedComponentPermission.class);   
    }
    
   /* @Override
	public RoleBasedComponentPermission save(RoleBasedComponentPermission object) {
    	RoleBasedComponentPermission rbcp = super.save(object);
		objectDefinitionSource.init(); 
		return rbcp;
	}*/

	public boolean isAccessAllowed(String componentId, String roles) {
		boolean accessAllowed = false;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
		List<Integer> searchResults = this.getSession().createSQLQuery("SELECT rcp.mask from rolebased_comp_permission rcp where rcp.role in ("+roles+") and " +
				"rcp.componentId = '" + componentId +"' and rcp.corpID='"+user.getCorpID() +"'" ).list();
		for (Integer permission :searchResults){
			if (permission >= 2) {
				accessAllowed = true;
				break;
			}
		}
			
		return accessAllowed;
	}
	
	
	/*public int getPermission(String componentId, String roles) {

		boolean accessAllowed = false;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        
        roles = roles.replaceAll("'", "");
        String[] rolesList = roles.split(",");
        List rolesAsList = Arrays.asList(rolesList);
        
        List<RoleBasedComponentPermission> rolePerms = this.getSession().createCriteria(RoleBasedComponentPermission.class)
        	.add(Restrictions.in("role", rolesAsList))
        	.add(Restrictions.eq("componentId", componentId))
        	.add(Restrictions.eq("corpID", user.getCorpID()))
        	.setCacheable(true)
        	.list();
        
        List<Integer> searchResults = new ArrayList<Integer>();
        for(RoleBasedComponentPermission permission:rolePerms){
        	searchResults.add(permission.getMask());
        }
        
        int componentPermission = 0;
		for (Integer permission :searchResults){
			if (permission > componentPermission ) componentPermission = permission;
		}
		return componentPermission;*/
        
		/*String[] roleArray = roles.split(",");
		Criteria criteria = this.getSession().createCriteria(RoleBasedComponentPermission.class)
        		.add(Restrictions.in("role", roleArray))
        		.add(Restrictions.eq("componentId", componentId))
        		.add(Restrictions.eq("corpID", user.getCorpID())).setCacheable(true);
		
        List<RoleBasedComponentPermission> searchResults = 
        		criteria.list();
        int componentPermission = 0;
		for (RoleBasedComponentPermission perm :searchResults){
			Integer permission = perm.getMask();
			if (permission > componentPermission ) componentPermission = permission;
		}
		return componentPermission;*/
	//}
	
	public int getPermission(String componentId, String roles) { 
		  boolean accessAllowed = false;
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		        User user = (User)auth.getPrincipal();
		        String corpId = user.getCorpID();
		        roles = roles.replaceAll("'", "");
		        String[] rolesList = roles.split(",");
		        String key = corpId+"-"+componentId;
		        //String tsftkey = "TSFT-"+componentId;
		        
		        //logger.warn("Getting permissions for key : "+key+" for role list "+roles);
		        //System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n                      "+permMap);
		        List <String>allowedRoles=new ArrayList<String>();
		         if(permMap.containsKey(key)){
		         allowedRoles = (List) permMap.get(key);
		         }
		         List<Integer> permList = new ArrayList<Integer>();
		         List<Integer> maskResults = new ArrayList<Integer>();
		         Map permissionMap = new HashMap();
		         for (String permission :allowedRoles){
		        	 String[] data= permission.split("~");
		        	 
		        	 if(permissionMap.containsKey(data[0])){
		        		 permList=	(List)permissionMap.get(data[0]); 
		        		 permList.add(Integer.parseInt(data[1]));
		        		 permissionMap.put(data[0], permList);
		        	 }else{
		        		 permList = new ArrayList();
		        		 permList.add(Integer.parseInt(data[1]));
		        		 permissionMap.put(data[0], permList);
		        	 }
		        	 
		         }
		         for(String role: rolesList){
		        	if( permissionMap.containsKey(role.trim())){
		        		maskResults.addAll((List)permissionMap.get(role.trim()));
		        	}
		        	 
		         } 
		         int componentPermission = 0;
		 		for (Integer permission :maskResults){
		 			if (permission > componentPermission ) componentPermission = permission;
		 		}
		         /*if(allowedRoles.contains(role)){
		          logger.warn("Users role are ["+roles+"] and allowed roles for key ["+key+"] : "+allowedRoles+" for component id "+componentId);
		          return 1000;
		         }*/
		         
		         
		        
		       // logger.warn("No access for Users role are ["+roles+"] and allowed roles for key ["+key+"] for component id "+componentId);
		  return componentPermission;
		      
		 }
	

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from RoleBasedComponentPermission");
	}

	public List searchcompanyPermission(String componentId, String description,String role) {
		return getHibernateTemplate().find("from RoleBasedComponentPermission where componentId like '%"+componentId.replaceAll("'", "''")+"%' and description like '%"+description.replaceAll("'", "''")+"%'and role like '%"+role.replaceAll("'", "''")+"%'");
	}

	public List getRole(String corpID) {
		return getHibernateTemplate().find("select name from Role where corpID='"+corpID+"'");
	}

	public String getUserRoleInTransaction(String transId, String partnerIdList) {
		List  list = this.getSession().
		createSQLQuery("select s.bookingAgentCode, s.brokerCode, s.originAgentCode, s.originSubAgentCode, " +
				"s.destinationAgentCode, s.destinationSubAgentCode " +
				"from serviceorder s where shipNumber ='"+transId+"'")
		.addScalar("s.bookingAgentCode", Hibernate.STRING)
		.addScalar("s.brokerCode", Hibernate.STRING)
		.addScalar("s.originAgentCode", Hibernate.STRING)
		.addScalar("s.originSubAgentCode", Hibernate.STRING)
		.addScalar("s.destinationAgentCode", Hibernate.STRING)
		.addScalar("s.destinationSubAgentCode", Hibernate.STRING).list();
		
		String roles = ""; //ROLE_BROKER_CODE, 
		
		Iterator it=list.iterator();
		while(it.hasNext()){
		Object [] row=(Object [])it.next();	
			if(row[0]!=null && (!row[0].toString().equalsIgnoreCase("")) )
			{
				if(partnerIdList.contains(row[0].toString()))
				{
					roles +="'ROLE_BOOKING_AGENT' , ";
				}
				
			}
			if(row[1]!=null && (!row[1].toString().equalsIgnoreCase("")))
			{
				if(partnerIdList.contains(row[1].toString()))
				{
				roles +="'ROLE_BROKER_AGENT' , ";
				}
			}
			if(row[2]!=null && (!row[2].toString().equalsIgnoreCase("")))
			{
				if(partnerIdList.contains(row[2].toString()))
				{
				roles +="'ROLE_ORIGIN_AGENT' , ";
				}
			}
			if(row[3]!=null && (!row[3].toString().equalsIgnoreCase("")))
			{
				if(partnerIdList.contains(row[3].toString()))
				{
				roles +="'ROLE_ORIGINSUB_AGENT' , ";
				}
			}
			if(row[4]!=null && (!row[4].toString().equalsIgnoreCase("")))
			{
				if(partnerIdList.contains(row[4].toString()))
				{
				roles +="'ROLE_DESTINATION_AGENT' , ";
				}
			}
			if(row[5]!=null && (!row[5].toString().equalsIgnoreCase("")))
			{
				if(partnerIdList.contains(row[5].toString()))
				{
				roles +="'ROLE_DESTINATIONSUB_AGENT' , ";
				}
			}
			
			if(!roles.equalsIgnoreCase(""))
			{
				if (roles.trim().lastIndexOf(",") == roles.trim().length() - 1) {
					roles = roles.substring(0, roles.trim().length() - 1);
				}
			}
			
			
		}
		
		//if (bookingAgentCode in (partnerIdList)) roles += ROLE_BOOKING_AGENT
		//else if (originAgentCode in (partnerIdList)) roles = ROLE_BROKER; 
		
		
		return roles; //'ROLE_BOOKING_AGENT', 'ROLE_DESTINATION_AGENT'
	}

	public List getModulePermissions(String[] tableList, String roles) {
		List searchResults= new ArrayList();
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        String moduleComponentClause ="";
        
        for (String table:tableList){
        	if (!moduleComponentClause.equals("")) moduleComponentClause += " or ";
        	moduleComponentClause += " rcp.componentId like '%.field." + table + ".%'";
        }
        moduleComponentClause = " (" + moduleComponentClause + ") ";
        //System.out.println("\n\n\n\n\n roles111---->"+roles); 
        if(roles.substring(roles.length()-1, roles.length()).equalsIgnoreCase(","))
          {
        	  roles=roles.substring(0, roles.length()-1);
          }
         // System.out.println("\n\n\n\n\n roles22---->"+roles);
        String query="select  concat(max(rcp.mask), '#', rcp.componentId) as mask from rolebased_comp_permission as rcp where rcp.role in ("+roles+") and " + moduleComponentClause + " and rcp.corpID='"+user.getCorpID() +"' group by rcp.componentId";
		searchResults = this.getSession().createSQLQuery("select  concat(max(rcp.mask), '#', rcp.componentId) as mask " + " from rolebased_comp_permission " + "as rcp where rcp.role in ("+roles+") " + "and " + moduleComponentClause + " and rcp.corpID='"+user.getCorpID() +"' group by rcp.componentId").list();
		}catch(Exception ex)
		{
			//System.err.println("\n\n\n\n\n searchResults"+ex);
		}
		return searchResults;
	}

	public String getPartnerIdList(String userName) {
		List  list = this.getSession().createSQLQuery("select distinct SUBSTRING_INDEX(dss.name, '_', -1) from userdatasecurity uds , app_user u, datasecurityset dss  where uds.user_id = u.id and dss.id = uds.userdatasecurity_id and dss.name like 'DATA_SECURITY_SET_AGENT_%' and username = '"+userName+"'").list();
		String partnerIds="";
		if(!list.isEmpty())
		{
		StringBuffer partnerIdsBuffer = new StringBuffer("");
		String pId=list.toString();
		pId=pId.replace("[", "");
		pId=pId.replace("]", "");
		String []partnerIdsArray=pId.split(",");
		int arrayLength = partnerIdsArray.length;
		for (int i = 0; i < arrayLength; i++) {
			partnerIdsBuffer.append("'");
			partnerIdsBuffer.append(partnerIdsArray[i].trim());
			partnerIdsBuffer.append("'");
			partnerIdsBuffer.append(",");
			
		}
			partnerIds = new String(partnerIdsBuffer);
		if (!partnerIds.equals("")) {
			partnerIds = partnerIds.substring(0, partnerIds.length() - 1);
		} else if (partnerIds.equals("")) {
			partnerIds = new String("''");
		}
		
		}
		
		// TODO Auto-generated method stub
		//select distinct SUBSTRING_INDEX(dss.name, '_', -1) from userdatasecurity uds , app_user u, datasecurityset dss  where uds.user_id = u.id
		//and dss.id = uds.userdatasecurity_id and dss.name like 'DATA_SECURITY_SET_AGENT_%' and username = 'gbell1'
		// 'A14634','707850','714450','714451','714453','714415','714454','714452','A05451','A06025','A12273','A12276','A12277','A12637','A15394','A18078'
		
		return partnerIds; //'12345', 'A234234'
	}
	public void updateCurrentRolePermission(String corpId){
		try {
			String query="select corpID,componentId,role,mask from rolebased_comp_permission rb where componentId is not null and componentId !='' and role is not null and role !='' and mask is not null and mask !='' and corpID in (select corpID from company where status is true)";
			List rolePermissions= this.getSession().createSQLQuery(query).list();
			List permList = null;
			Map permissionMap = new HashMap();
			Iterator it=rolePermissions.iterator();
			while(it.hasNext()){
				 Object [] row=(Object[])it.next();
				String permKey = row[0] +"-"+row[1];
				if((permList = (List) permissionMap.get(permKey))==null){
					permList = new ArrayList<String>();
					permList.add(row[2]+"~"+row[3]);
					permissionMap.put(permKey, permList);
				}else{
					permList.add(row[2]+"~"+row[3]);
					permissionMap.put(permKey, permList);
				}
			}
			permMap.clear();
			permMap.putAll(permissionMap);			
		} catch (Exception e) {
			log.error("Error creating the role based permission cache",e);
		} 
	}
	
}