// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `payableActual` VARCHAR(100),
 ADD COLUMN `receivableActual` VARCHAR(100),
 ADD COLUMN `payableAccrued` VARCHAR(100),
 ADD COLUMN `receivableAccrued` VARCHAR(100);
 
 ALTER TABLE `redsky`.`claim` ADD COLUMN `branchandsortcode` VARCHAR(180) AFTER `claimType`,
 ADD COLUMN `accountname` VARCHAR(100) AFTER `branchandsortcode`;
 
 ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `packAE` DATETIME AFTER `destinationReceivedName`,
 ADD COLUMN `loadAE` DATETIME AFTER `packAE`,
 ADD COLUMN `deliveryAE` DATETIME AFTER `loadAE`;
 
 ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `accrualReadyUpdatedBy` VARCHAR(82) ,
 ADD COLUMN `accrualReadyDate` DATETIME ;
 
 DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE ALGORITHM=UNDEFINED 
SQL SECURITY DEFINER VIEW `partner` AS select `a`.`id` AS `id`,
`a`.`parent` AS `parent`,
`a`.`middleInitial` AS `middleInitial`,
`a`.`billingAddress1` AS `billingAddress1`,
`a`.`billingAddress2` AS `billingAddress2`,
`a`.`billingAddress3` AS `billingAddress3`,
`a`.`billingAddress4` AS `billingAddress4`,
`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,
`a`.`billingCountryCode` AS `billingCountryCode`,
`a`.`billingEmail` AS `billingEmail`,
`a`.`billingFax` AS `billingFax`,
`a`.`billingPhone` AS `billingPhone`,
`a`.`billingState` AS `billingState`,
`a`.`billingTelex` AS `billingTelex`,
`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,
`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,
`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,
`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,
`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,
`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,
`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,
`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,
`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,
`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,
`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,
`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,
`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,
`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,
`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,
`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,
`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,
`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,
`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,
`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,
`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,
`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,
`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,
`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,
`a`.`companyCapabilities` AS `companyCapabilities`,
`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,
`a`.`serviceRangeKms` AS `serviceRangeKms`,
`a`.`serviceRangeMiles` AS `serviceRangeMiles`,
`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,
`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,
`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,
`a`.`qualityCertifications` AS `qualityCertifications`,
`a`.`vanLineAffiliation` AS `vanLineAffiliation`,
`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,
`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,
`a`.`createdOn` AS `createdOn`,
`a`.`updatedBy` AS `updatedBy`,if((`a`.`updatedOn` > `b`.`updatedOn`),
`a`.`updatedOn`,`b`.`updatedOn`) AS `updatedOn`,`a`.`firstName` AS `firstName`,
`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),
`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,
`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,
`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,
`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,
`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,
`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,
`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,
`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,
`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,
`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,
`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,
`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,
`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,
`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,
`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,
`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,
`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,
`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,
`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,
`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,
`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,
`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,
`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,
`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,
`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,
`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,
`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,
`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,
`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,
`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,
`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,
`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,
`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,
`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,
`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,
`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,
`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,
`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,
`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankCode` AS `bankCode`,
`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName`,
`a`.`ugwwNetworkGroup` AS `ugwwNetworkGroup`,`b`.`collection` AS `collection`,
`a`.`isPartnerExtract` AS `isPartnerExtract`,`a`.`agentGroup` AS `agentGroup`,
`a`.`partnerType` AS `partnerType`,`b`.`noInsurance` AS `noInsurance`,
`b`.`lumpSum` AS `lumpSum`,`b`.`detailedList` AS `detailedList`,
`b`.`vatBillingGroup` AS `vatBillingGroup`,
`b`.`agentClassification` AS `agentClassification`
from (`partnerpublic` `a` join `partnerprivate` `b`)
where (`a`.`id` = `b`.`partnerPublicId`);

 ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `vendorId` VARCHAR(8) AFTER `deliveryAE`,
 ADD COLUMN `vendorName` VARCHAR(255) AFTER `vendorId`,
 ADD COLUMN `grade` VARCHAR(255) AFTER `vendorName`,
 ADD COLUMN `containerType` VARCHAR(255) AFTER `grade`,
 ADD COLUMN `freeDatewithContainer` DATETIME AFTER `containerType`,
 ADD COLUMN `chasisProvide` BOOLEAN AFTER `freeDatewithContainer`,
 ADD COLUMN `containerDays` INTEGER AFTER `chasisProvide`,
 ADD COLUMN `containerFreeDays` INTEGER AFTER `containerDays`,
 ADD COLUMN `containerFreeDaysSubTotal` DOUBLE AFTER `containerFreeDays`,
 ADD COLUMN `containerFreeDaysTotal` DOUBLE AFTER `containerFreeDaysSubTotal`,
 ADD COLUMN `chasisDays` INTEGER AFTER `containerFreeDaysTotal`,
 ADD COLUMN `chasisfreeDays` INTEGER AFTER `chasisDays`,
 ADD COLUMN `chasisfreeDaysSubTotal` DOUBLE AFTER `chasisfreeDays`,
 ADD COLUMN `chasisfreeDaysTotal` DOUBLE AFTER `chasisfreeDaysSubTotal`,
 ADD COLUMN `otherChargesDesc` VARCHAR(255) AFTER `chasisfreeDaysTotal`,
 ADD COLUMN `otherChargesAmount` DOUBLE AFTER `otherChargesDesc`,
 ADD COLUMN `inLandTotalAmount` DOUBLE AFTER `otherChargesAmount`,
 ADD COLUMN `sentTOFPickUp` DATETIME AFTER `inLandTotalAmount`,
 ADD COLUMN `freeDateAtPort` DATETIME AFTER `sentTOFPickUp`,
 ADD COLUMN `outOfPort` DATETIME AFTER `freeDateAtPort`,
 ADD COLUMN `requestPickDate` DATETIME AFTER `outOfPort`,
 ADD COLUMN `actualPickDate` DATETIME AFTER `requestPickDate`,
 ADD COLUMN `requestDropDate` DATETIME AFTER `actualPickDate`,
 ADD COLUMN `actualDropDate` DATETIME AFTER `requestDropDate`,
 ADD COLUMN `sentTruckingOrderforReturn` DATETIME AFTER `actualDropDate`,
 ADD COLUMN `returntoPortActual` DATETIME AFTER `sentTruckingOrderforReturn`,
 ADD COLUMN `liveLoad` DATETIME AFTER `returntoPortActual`,
 ADD COLUMN `liveUnLoad` DATETIME AFTER `liveLoad`;
 
  create table `taskdetails`(
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`corpId` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
`createdBy` varchar(82) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `details`  varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `time` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticket` bigint(20) DEFAULT NULL,
  `workTicketId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
  );
  
  ALTER TABLE `redsky`.`trackingstatus` MODIFY COLUMN `chasisProvide` BIT(1);
  
   ALTER TABLE `redsky`.`taskdetails` ADD COLUMN `workorder` VARCHAR(10) ;
   
   ALTER TABLE `redsky`.`companydivision` ADD COLUMN `closedDivision` BIT(1) NULL DEFAULT b'0'  AFTER `inactiveEffectiveDate` ;
   
    ALTER TABLE `redsky`.`taskdetails` ADD COLUMN `shipNumber` VARCHAR(15);
    
    ALTER TABLE `redsky`.`trackingstatus` CHANGE COLUMN `packAE` `actualPackBegin` DATETIME,
 CHANGE COLUMN `loadAE` `actualLoadBegin` DATETIME,
 CHANGE COLUMN `deliveryAE` `actualDeliveryBegin` DATETIME,
 ADD COLUMN `targetPackEnding` DATETIME AFTER `liveUnLoad`,
 ADD COLUMN `targetLoadEnding` DATETIME AFTER `targetPackEnding`,
 ADD COLUMN `targetdeliveryShipper` DATETIME AFTER `targetLoadEnding`;
 
 ALTER TABLE `redsky`.`app_user` ADD COLUMN `additionalCommissionType` VARCHAR(15) AFTER `recordsForSoDashboard`;
 
 ALTER TABLE `redsky`.`accountline`
ADD COLUMN `varianceRevenueAmount` DECIMAL(19,2) DEFAULT 0.00 AFTER `networkBillToName`,
ADD COLUMN `varianceExpenseAmount` DECIMAL(19,2) DEFAULT 0.00 AFTER `varianceRevenueAmount`;

insert into extractcolumnmgmt (corpID,reportName,tableName,fieldName,displayName,columnSequenceNumber,createdBy,createdOn,updatedBy,updatedOn)
VALUES
('SUDD','shipmentAnalysis','trackingstatus','t.actualPackBegin as Actual_Packing_Begin_Date','Actual_Packing_Begin_Date',1941,'ashish', now(), 'ashish', now()),
('SUDD','shipmentAnalysis','trackingstatus','t.actualLoadBegin as Actual_Load_Begin_Date','Actual_Load_Begin_Date',1942,'ashish', now(), 'ashish', now()),
('SUDD','shipmentAnalysis','trackingstatus','t.actualDeliveryBegin as Actual_Delivery_Begin_Date','Actual_Delivery_Begin_Date',1943,'ashish', now(), 'ashish', now()),
('SUDD','shipmentAnalysis','trackingstatus','t.targetPackEnding as Target_Packing_Ending_Date','Target_Packing_Ending_Date',1944,'ashish', now(), 'ashish', now()),
('SUDD','shipmentAnalysis','trackingstatus','t.targetLoadEnding as Target_Load_Ending_Date','Target_Load_Ending_Date',1945,'ashish', now(), 'ashish', now()),
('SUDD','shipmentAnalysis','trackingstatus','t.targetdeliveryShipper as Target_Delivery_Ending_Date','Target_Delivery_Ending_Date',1946,'ashish', now(), 'ashish', now());

insert into extractcolumnmgmt (corpID,reportName,tableName,fieldName,displayName,columnSequenceNumber,createdBy,createdOn,updatedBy,updatedOn)
VALUES
('SUDD','domesticAnalysis','trackingstatus','t.actualPackBegin as Actual_Packing_Begin_Date','Actual_Packing_Begin_Date',1950,'ashish', now(), 'ashish', now()),
('SUDD','domesticAnalysis','trackingstatus','t.actualLoadBegin as Actual_Load_Begin_Date','Actual_Load_Begin_Date',1951,'ashish', now(), 'ashish', now()),
('SUDD','domesticAnalysis','trackingstatus','t.actualDeliveryBegin as Actual_Delivery_Begin_Date','Actual_Delivery_Begin_Date',1952,'ashish', now(), 'ashish', now()),
('SUDD','domesticAnalysis','trackingstatus','t.targetPackEnding as Target_Packing_Ending_Date','Target_Packing_Ending_Date',1953,'ashish', now(), 'ashish', now()),
('SUDD','domesticAnalysis','trackingstatus','t.targetLoadEnding as Target_Load_Ending_Date','Target_Load_Ending_Date',1954,'ashish', now(), 'ashish', now()),
('SUDD','domesticAnalysis','trackingstatus','t.targetdeliveryShipper as Target_Delivery_Ending_Date','Target_Delivery_Ending_Date',1955,'ashish', now(), 'ashish', now());

insert into extractcolumnmgmt (corpID,reportName,tableName,fieldName,displayName,columnSequenceNumber,createdBy,createdOn,updatedBy,updatedOn)
VALUES
('SUDD','dynamicFinancialSummary','trackingstatus','t.actualPackBegin as Actual_Packing_Begin_Date','Actual_Packing_Begin_Date',1953,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialSummary','trackingstatus','t.actualLoadBegin as Actual_Load_Begin_Date','Actual_Load_Begin_Date',1954,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialSummary','trackingstatus','t.actualDeliveryBegin as Actual_Delivery_Begin_Date','Actual_Delivery_Begin_Date',1955,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialSummary','trackingstatus','t.targetPackEnding as Target_Packing_Ending_Date','Target_Packing_Ending_Date',1956,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialSummary','trackingstatus','t.targetLoadEnding as Target_Load_Ending_Date','Target_Load_Ending_Date',1957,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialSummary','trackingstatus','t.targetdeliveryShipper as Target_Delivery_Ending_Date','Target_Delivery_Ending_Date',1958,'ashish', now(), 'ashish', now());

insert into extractcolumnmgmt (corpID,reportName,tableName,fieldName,displayName,columnSequenceNumber,createdBy,createdOn,updatedBy,updatedOn)
VALUES
('SUDD','storageBillingAnalysis','trackingstatus','t.actualPackBegin as Actual_Packing_Begin_Date','Actual_Packing_Begin_Date',1960,'ashish', now(), 'ashish', now()),
('SUDD','storageBillingAnalysis','trackingstatus','t.actualLoadBegin as Actual_Load_Begin_Date','Actual_Load_Begin_Date',1961,'ashish', now(), 'ashish', now()),
('SUDD','storageBillingAnalysis','trackingstatus','t.actualDeliveryBegin as Actual_Delivery_Begin_Date','Actual_Delivery_Begin_Date',1962,'ashish', now(), 'ashish', now()),
('SUDD','storageBillingAnalysis','trackingstatus','t.targetPackEnding as Target_Packing_Ending_Date','Target_Packing_Ending_Date',1963,'ashish', now(), 'ashish', now()),
('SUDD','storageBillingAnalysis','trackingstatus','t.targetLoadEnding as Target_Load_Ending_Date','Target_Load_Ending_Date',1964,'ashish', now(), 'ashish', now()),
('SUDD','storageBillingAnalysis','trackingstatus','t.targetdeliveryShipper as Target_Delivery_Ending_Date','Target_Delivery_Ending_Date',1965,'ashish', now(), 'ashish', now());

insert into extractcolumnmgmt (corpID,reportName,tableName,fieldName,displayName,columnSequenceNumber,createdBy,createdOn,updatedBy,updatedOn)
VALUES
('SUDD','dynamicFinancialDetails','trackingstatus','t.actualPackBegin as Actual_Packing_Begin_Date','Actual_Packing_Begin_Date',1968,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialDetails','trackingstatus','t.actualLoadBegin as Actual_Load_Begin_Date','Actual_Load_Begin_Date',1969,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialDetails','trackingstatus','t.actualDeliveryBegin as Actual_Delivery_Begin_Date','Actual_Delivery_Begin_Date',1970,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialDetails','trackingstatus','t.targetPackEnding as Target_Packing_Ending_Date','Target_Packing_Ending_Date',1971,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialDetails','trackingstatus','t.targetLoadEnding as Target_Load_Ending_Date','Target_Load_Ending_Date',1972,'ashish', now(), 'ashish', now()),
('SUDD','dynamicFinancialDetails','trackingstatus','t.targetdeliveryShipper as Target_Delivery_Ending_Date','Target_Delivery_Ending_Date',1973,'ashish', now(), 'ashish', now());



ALTER TABLE `redsky`.`preferredagentforaccount` ADD COLUMN `agentGroup` VARCHAR(255) AFTER `updatedOn`;

update extractcolumnmgmt set displayName='ACTUAL_PACKING_BEGIN_DATE'  where
reportName='shipmentAnalysis' and corpid='SUDD' and
columnSequenceNumber ='1941'
and tableName='trackingstatus';

update extractcolumnmgmt set displayName='ACTUAL_LOAD_BEGIN_DATE'  where
reportName='shipmentAnalysis' and corpid='SUDD' and
columnSequenceNumber ='1942'
and tableName='trackingstatus';

update extractcolumnmgmt set displayName='ACTUAL_DELIVERY_BEGIN_DATE'  where
reportName='shipmentAnalysis' and corpid='SUDD' and
columnSequenceNumber ='1943'
and tableName='trackingstatus';

update extractcolumnmgmt set displayName='TARGET_PACKING_ENDING_DATE'  where
reportName='shipmentAnalysis' and corpid='SUDD' and
columnSequenceNumber ='1944'
and tableName='trackingstatus';

update extractcolumnmgmt set displayName='TARGET_LOAD_ENDING_DATE'  where
reportName='shipmentAnalysis' and corpid='SUDD' and
columnSequenceNumber ='1945'
and tableName='trackingstatus';

update extractcolumnmgmt set displayName='TARGET_DELIVERY_ENDING_DATE'  where
reportName='shipmentAnalysis' and corpid='SUDD' and
columnSequenceNumber ='1946'
and tableName='trackingstatus';

delete from extractcolumnmgmt where reportName='domesticAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1950';
 delete from extractcolumnmgmt where reportName='domesticAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1951';
 delete from extractcolumnmgmt where reportName='domesticAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1952';
 delete from extractcolumnmgmt where reportName='domesticAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1953';
 delete from extractcolumnmgmt where reportName='domesticAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1954';
 delete from extractcolumnmgmt where reportName='domesticAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1955';


 delete from extractcolumnmgmt where reportName='dynamicFinancialSummary' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1953';
 delete from extractcolumnmgmt where reportName='dynamicFinancialSummary' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1954';
 delete from extractcolumnmgmt where reportName='dynamicFinancialSummary' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1955';
 delete from extractcolumnmgmt where reportName='dynamicFinancialSummary' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1956';
 delete from extractcolumnmgmt where reportName='dynamicFinancialSummary' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1957';
 delete from extractcolumnmgmt where reportName='dynamicFinancialSummary' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1958';


 delete from extractcolumnmgmt where reportName='storageBillingAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1960';
 delete from extractcolumnmgmt where reportName='storageBillingAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1961';
 delete from extractcolumnmgmt where reportName='storageBillingAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1962';
 delete from extractcolumnmgmt where reportName='storageBillingAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1963';
 delete from extractcolumnmgmt where reportName='storageBillingAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1964';
 delete from extractcolumnmgmt where reportName='storageBillingAnalysis' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1965';


 delete from extractcolumnmgmt where reportName='dynamicFinancialDetails' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1968';
 delete from extractcolumnmgmt where reportName='dynamicFinancialDetails' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1969';
 delete from extractcolumnmgmt where reportName='dynamicFinancialDetails' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1970';
 delete from extractcolumnmgmt where reportName='dynamicFinancialDetails' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1971';
 delete from extractcolumnmgmt where reportName='dynamicFinancialDetails' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1972';
 delete from extractcolumnmgmt where reportName='dynamicFinancialDetails' and corpid='SUDD' and
 tableName='trackingstatus' and columnSequenceNumber='1973';
