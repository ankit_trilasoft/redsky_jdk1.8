<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Tenancy Billing</title>   
    <meta name="heading" content="Tenancy Billing"/>   
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<style>
#loader {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.listwhitetextWhite {
    background-color: #FFFFFF;
    color: #003366;
    font-family: arial,verdana;
    font-size: 10.5px;
   }
</style>

<script language="javascript" type="text/javascript">


</script>

</head>

<s:form cssClass="form_magn" id="tenancyBillingListForm" action="" method="post" > 
<div id="Layer1" style="width:100%;">

<s:hidden name="tenancyBillingGrpList" id="tenancyBillingGrpList" />
<s:hidden name="isPrivatePartyFlag" id="isPrivatePartyFlag" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Tenancy Billing</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:11px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table style="width:600px;" cellspacing="2" cellpadding="0" border="0">
	
		<tr>
			<td class="listwhitetext" align="right" >Bill Through From</td>			
				<c:if test="${not empty billThroughFrom}">
						<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"> <s:param name="value" value="billThroughFrom" /></s:text>
						<td width="130px" style="padding-left:2px"><s:textfield cssClass="input-text" id="date1" name="billThroughFrom" value="%{customerFiledate1FormattedValue}" size="9" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
						</td>
				</c:if>
				<c:if test="${empty billThroughFrom}" >
						<td width="130px" style="padding-left:2px"><s:textfield cssClass="input-text" id="date1" name="billThroughFrom" required="true" size="9" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
						</td>
			   </c:if>
			<td class="listwhitetext" align="right" >Bill Through To</td>
			   <c:if test="${not empty billThroughTo}">
						<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"> <s:param name="value" value="billThroughTo" /></s:text>
						<td  style="padding-left:2px;padding-bottom:3px;"><s:textfield cssClass="input-text" id="date2" name="billThroughTo" value="%{customerFiledate1FormattedValue}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
						</td>
			</c:if>
			<c:if test="${empty billThroughTo}" >
						<td style="padding-left:2px;padding-bottom:3px;"><s:textfield cssClass="input-text" id="date2" name="billThroughTo" required="true" size="9" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
						</td>
			</c:if>
		</tr>
			<tr>
		  		<td class="listwhitetext" align="right" >Bill To Code</td>
		  		<td class="listwhitetext" style="padding-left:2px"><s:textfield  cssClass="input-text" name="billToCode" size="9" maxlength="7" onchange="findBillToCode()"/>
		  		<img class="openpopup" align="absmiddle" width="17" height="20" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
		  		<td class="listwhitetext" align="right" >Billing Group</td>
		  		<td style="padding-left:2px">
		  			<s:select cssClass="list-menu" cssStyle="width:130px; height:70px; overflow-y:scroll;"  id="tenancyBillingGrp" name="tenancyBillingGrp" list="%{tenancyBillingGroup}"  multiple="true" />
		  		</td>
	  		</tr>
</table>
 
<div style="!margin-top:7px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<tr>
	  	<td><input type="button" class="cssbutton" style="width:140px; margin-left:40px;" onclick="return findPreviewBillingReport()" name="billing.preview" value="Preview Billing Report" /></td>
	  
	 	<td><input type="button" class="cssbutton" style="width:140px;" onclick="createAccountlineByTenancyBilling()" name="billing.previewVat" value="Create Invoice Lines" /></td>
	  
	 </tr>
</div>

<c:if test="${tenancyBillingPreviewList!='[]'}">
<div class="spnblk">&nbsp;</div>
 <div id="otabs" style="margin-bottom:0px;">
	<ul>
		<li><a class="current"><span>Tenancy Invoice Preview</span></a></li>
	</ul>
</div> 
 
<s:set name="tenancyBillingPreviewList" value="tenancyBillingPreviewList" scope="request"/>

<display:table name="tenancyBillingPreviewList" class="table"  requestURI="" id="tenancyBillingPreviewList" >
	
	<c:choose>
	
	<c:when test="${tenancyBillingPreviewList.notFound!='' && tenancyBillingPreviewList.notFound!=null}">
		
		<display:column property="shipNumber" title="S/O#" style="color:red;"></display:column>
		<display:column property="shipperName" title="Shipper" style="color:red;"></display:column>
		<display:column property="billToCode"  title="BillTo" style="color:red;"/>
		<display:column property="billToName"  title="Account Name" style="color:red;"/>
		<display:column  title="Amount" style="color:red;">
			<div align="right">
				<c:out value="${tenancyBillingPreviewList.rentAmount}" />&nbsp;<c:out value="${tenancyBillingPreviewList.contractCurrency}" />
			</div>
		 </display:column>
		 <display:column property="vatPercent"  title="Vat%" style="color:red;"/>
		<display:column property="rentVat"  title="Vat" style="color:red;"/>
		<display:column property="rentCycle"  title="Cycle" style="color:red;"/>
		<display:column property="chargeCode"  title="Charge" style="color:red;"/>
		<display:column property="chargeDescription"  title="Description" style="color:red;"/>
		<display:column property="vendorCode"  title="Vendor Code" style="color:red;"/>
		<display:column property="vendorName"  title="Vendor Name" style="color:red;"/>
		<display:column  title="Amount" style="color:red;">
		<c:if test="${tenancyBillingPreviewList.chargeCode!='RENT' && tenancyBillingPreviewList.chargeCode!='TEN' && tenancyBillingPreviewList.chargeCode!='EMG'}">
			<div align="right">
				<c:out value="${tenancyBillingPreviewList.rentAmount}" />&nbsp;<c:out value="${tenancyBillingPreviewList.contractCurrency}" />
			</div>
		</c:if>
		</display:column>
		<display:column  title="Vat" style="color:red;">
		<c:if test="${tenancyBillingPreviewList.chargeCode!='RENT' && tenancyBillingPreviewList.chargeCode!='TEN' && tenancyBillingPreviewList.chargeCode!='EMG'}">
			<div align="right">
				<c:out value="${tenancyBillingPreviewList.rentVat}" />
			</div>
		</c:if>
		 </display:column>
		<display:column property="notFound"  title="Validation" style="color:red;"/>
	
	</c:when>
	<c:otherwise>
		<display:column property="shipNumber" title="S/O#" style="color:black;" ></display:column>
		<display:column property="shipperName" title="Shipper" style="color:black;"></display:column>
		<display:column property="billToCode"  title="BillTo" style="color:black;"/>
		<display:column property="billToName"  title="Account Name" style="color:black;"/>
		<display:column  title="Amount" style="color:black;">
			<div align="right">
				<c:out value="${tenancyBillingPreviewList.rentAmount}" />&nbsp;<c:out value="${tenancyBillingPreviewList.contractCurrency}" />
			</div>
		 </display:column>
		 <display:column property="vatPercent"  title="Vat%" style="color:black;"/>
		<display:column property="rentVat"  title="Vat" style="color:black;"/>
		<display:column property="rentCycle"  title="Cycle" style="color:black;"/>
		<display:column property="chargeCode"  title="Charge" style="color:black;"/>
		<display:column property="chargeDescription"  title="Description" style="color:black;"/>
		<display:column property="vendorCode"  title="Vendor Code" style="color:black;"/>
		<display:column property="vendorName"  title="Vendor Name" style="color:black;"/>
		<display:column title="Pay Amount" style="color:black;">
		<c:if test="${tenancyBillingPreviewList.chargeCode!='RENT' && tenancyBillingPreviewList.chargeCode!='TEN' && tenancyBillingPreviewList.chargeCode!='EMG'}">
			<div align="right">
				<c:out value="${tenancyBillingPreviewList.rentAmount}" />&nbsp;<c:out value="${tenancyBillingPreviewList.contractCurrency}" />
			</div>
		</c:if>
		 </display:column>
		<display:column title="Vat" style="color:black;">
		<c:if test="${tenancyBillingPreviewList.chargeCode!='RENT' && tenancyBillingPreviewList.chargeCode!='TEN' && tenancyBillingPreviewList.chargeCode!='EMG'}">
			<div align="right">
				<c:out value="${tenancyBillingPreviewList.rentVat}" />
			</div>
		</c:if>
		 </display:column>
		<display:column property="notFound" title="Validation" style="color:black;"/>
	
	</c:otherwise>
</c:choose>
</display:table>
</c:if>
<c:if test="${tenancyBillingPreviewList=='[]'}">
<table class="table" id="dataTable" style="width:100%;margin-top:0px;"><!-- sandeep -->
					 <thead>					 
					 <tr>
					<th width="">S/O#</th>
					 <th width="">Shipper</th>
					 <th width="">BillTo</th>
					 <th width="">Account Name</th>
					 <th width="">Amount</th>
					 <th width="">Vat</th>
					 <th width="">Vat%</th>
					 <th width="">Cycle</th>
					 <th width="">Charge</th>
					 <th width="">Description</th>
					 <th width="">Vendor Code</th>
					 <th width="">Vendor Name</th>
					 <th width="">Amount</th>
					 <th width="">Vat</th>
					 <th width="">Validation</th>
					 </tr>
					 </thead>
					 		 		  
					 
<td  class="listwhitetextWhite" colspan="15">Nothing Found To Display.</td>
</table>
</c:if>

<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription"/>
<s:hidden name="eigthDescription" />
<s:hidden name="ninthDescription" />
<s:hidden name="tenthDescription" />

<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>
</s:form>
<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
function showHide(action){
	document.getElementById("loader").style.display = action;
}
function findBillToCode(){
    var billToCode = document.forms['tenancyBillingListForm'].elements['billToCode'].value;
    billToCode=billToCode.trim();
    if(billToCode!=''){
    showHide("block");
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse4;
    http2.send(null);
    }
}
function handleHttpResponse4(){
        if (http2.readyState == 4){
           var results = http2.responseText
           results = results.trim();
           if(results.length>=1){
        	   showHide("none");
			}else{
				showHide("none");
                alert("Invalid Bill To code, please select another");
                document.forms['tenancyBillingListForm'].elements['billToCode'].value="";
            }
        } 
}
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http2 = getHTTPObject();
function openBookingAgentPopWindow(){		
	javascript:openWindow('partnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billToCode');
}

function findPreviewBillingReport(){
	
	   var date1 = document.forms['tenancyBillingListForm'].elements['billThroughFrom'].value;	 
	   var date2 = document.forms['tenancyBillingListForm'].elements['billThroughTo'].value; 
	   var daysApart = getDateCompare(date1,date2);
	   
	  if(daysApart<0){
	    alert("Bill Through To should be greater than  or equal to Bill Through From");
	    document.forms['tenancyBillingListForm'].elements['billThroughTo'].value='';
	    return false;
	  }
	  
	  var fld = document.getElementById('tenancyBillingGrp');
      var tenancyBillingGrpList = [];
      for (var i = 0; i < fld.options.length; i++) {
        if (fld.options[i].selected) {
        	tenancyBillingGrpList.push(fld.options[i].value);
        }
      }
    document.forms['tenancyBillingListForm'].elements['tenancyBillingGrpList'].value = tenancyBillingGrpList;
    var billToCode = document.forms['tenancyBillingListForm'].elements['billToCode'].value;
    if(date1==''){
    	alert('Please select Bill Through From.');
    }else if(date2==''){
    	alert('Please select Bill Through To.');
    }else if(billToCode!='' && tenancyBillingGrpList!=''){
    	alert('Please choose either Bill To Code or Billing Group.');
    }else{
    	showHide("block");
		document.forms['tenancyBillingListForm'].action = 'tenancyBillingPreviewSearch.html';
		document.forms['tenancyBillingListForm'].submit();
    }
}

function getDateCompare(date1,date2){
	 var mySplitResult = date1.split("-");
	   var day = mySplitResult[0];
	   var month = mySplitResult[1];
	   var year = mySplitResult[2];
	  if(month == 'Jan'){
	       month = "01";
	   }else if(month == 'Feb'){
	       month = "02";
	   }else if(month == 'Mar'){
	       month = "03"
	   }else if(month == 'Apr'){
	       month = "04"
	   }else if(month == 'May'){
	       month = "05"
	   }else if(month == 'Jun'){
	       month = "06"
	   }else if(month == 'Jul'){
	       month = "07"
	   }else if(month == 'Aug'){
	       month = "08"
	   }else if(month == 'Sep'){
	       month = "09"
	   }else if(month == 'Oct'){
	       month = "10"
	   }else if(month == 'Nov'){
	       month = "11"
	   }else if(month == 'Dec'){
	       month = "12";
	   }
	  
	   var finalDate = month+"-"+day+"-"+year;
	   var mySplitResult2 = date2.split("-");
	   var day2 = mySplitResult2[0];
	   var month2 = mySplitResult2[1];
	   var year2 = mySplitResult2[2];
	   
	   if(month2 == 'Jan'){
	       month2 = "01";
	   }else if(month2 == 'Feb'){
	       month2 = "02";
	   }else if(month2 == 'Mar'){
	       month2 = "03"
	   }else if(month2 == 'Apr'){
	       month2 = "04"
	   }else if(month2 == 'May'){
	       month2 = "05"
	   }else if(month2 == 'Jun'){
	       month2 = "06"
	   }else if(month2 == 'Jul'){
	       month2 = "07"
	   }else if(month2 == 'Aug'){
	       month2 = "08"
	   }else if(month2 == 'Sep'){
	       month2 = "09"
	   }else if(month2 == 'Oct'){
	       month2 = "10"
	   }else if(month2 == 'Nov'){
	       month2 = "11"
	   }else if(month2 == 'Dec'){
	       month2 = "12";
	   }
	  var finalDate2 = month2+"-"+day2+"-"+year2;
	  date1 = finalDate.split("-");
	  date2 = finalDate2.split("-");
	  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
	  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
	  var daysApart = Math.round((eDate-sDate)/86400000);
	  
	  return daysApart;
}

function createAccountlineByTenancyBilling(){
		var date1 = document.forms['tenancyBillingListForm'].elements['billThroughFrom'].value;	 
	   var date2 = document.forms['tenancyBillingListForm'].elements['billThroughTo'].value; 
	   var daysApart = getDateCompare(date1,date2);
	   
	  if(daysApart<0){
	    alert("Bill Through To should be greater than  or equal to Bill Through From");
	    document.forms['tenancyBillingListForm'].elements['billThroughTo'].value='';
	    return false;
	  }
	  
	  var fld = document.getElementById('tenancyBillingGrp');
   		var tenancyBillingGrpList = [];
	   for (var i = 0; i < fld.options.length; i++) {
	     if (fld.options[i].selected) {
	     	tenancyBillingGrpList.push(fld.options[i].value);
	     }
	   }
 	document.forms['tenancyBillingListForm'].elements['tenancyBillingGrpList'].value = tenancyBillingGrpList;
 	var billToCode = document.forms['tenancyBillingListForm'].elements['billToCode'].value;
	 if(date1==''){
	 	alert('Please select Bill Through From.');
	 }else if(date2==''){
	 	alert('Please select Bill Through To.');
	 }else if(billToCode!='' && tenancyBillingGrpList!=''){
	 	alert('Please choose either Bill To Code or Billing Group.');
	 }else if(${tenancyBillingPreviewList=='[]'}){
		 alert('Please preview billing report first.');
	 }else{
		 showHide("block");
		document.forms['tenancyBillingListForm'].action = 'createAccountlineByTenancyBilling.html';
		document.forms['tenancyBillingListForm'].submit();
	 }
}
</script> 
  