package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.BookStorage;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.Storage;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.BookStorageManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.LocationManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.StorageLibraryManager;
import com.trilasoft.app.service.StorageManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketManager;
//import org.appfuse.service.GenericManager; 
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BookStorageAction extends BaseAction implements Preparable{

	private NotesManager notesManager;
	
	private BookStorageManager bookStorageManager;

	private StorageManager storageManager;
	private ServiceOrder serviceOrderToRecods;
	private TrackingStatus trackingStatusToRecods;
	private  TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private   Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private  CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private StorageLibraryManager storageLibraryManager;
	private StorageLibrary storageLibrary;
	private List bookStorages;

	private BookStorage bookStorage;
	


	private Long id;
	
	private Long BID;
	
	private Long ticket;
	
	private Long id1;
	private Long wtId;
	private Long soId;

	private List storages;
	
	private  List weightunits;
	private List volumeunits;
	private Storage storage;

	private Location location;

	private WorkTicket workTicket;

	private WorkTicketManager workTicketManager;

	private ServiceOrderManager serviceOrderManager;

	private ServiceOrder serviceOrder;

	private LocationManager locationManager;

	private Long autoSequenceNumber;

	private String sessionCorpID;
	
	private String countTicketLocationNotes;
	
	private String hitFlag;
	private String addedUtilizedVolCft;
	private String addedUtilizedVolCbm;
	private String subAvailVolCft;
	private String subAvailVolCbm;
	Date currentdate = new Date();
	private Company company;
	private CompanyManager companyManager;
	private String voxmeIntergartionFlag;
	private RefMasterManager refMasterManager;
	private Map<String,String> house;	
	private String oiJobList;
	static final Logger logger = Logger.getLogger(BookStorageAction.class);
	
	public String getCountTicketLocationNotes() {
		return countTicketLocationNotes;
	}

	public void setCountTicketLocationNotes(String countTicketLocationNotes) {
		this.countTicketLocationNotes = countTicketLocationNotes;
	}

	public BookStorageAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		 if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				}
			}
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	} 
	public void setBookStorageManager(BookStorageManager bookStorageManager) {
		this.bookStorageManager = bookStorageManager;
	}

	public void setStorageManager(StorageManager storageManager) {
		this.storageManager = storageManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public List getBookStorages() {
		return bookStorages;
	}

	public List getStorages() {
		return storages;
	}

	public String list() {
		bookStorages = bookStorageManager.getAll();
		return SUCCESS;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public BookStorage getBookStorage() {
		return bookStorage;
	}

	public void setBookStorage(BookStorage bookStorage) {
		this.bookStorage = bookStorage;
	}

	public WorkTicket getWorkTicket() { 
		return( workTicket!=null )?workTicket:workTicketManager.get(id1);
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location workTicket) {
		this.location = location;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String delete() {
		bookStorageManager.remove(bookStorage.getId());
		saveMessage(getText("bookStorage.deleted"));

		return SUCCESS;
	}

	public String getComboList(String corpID){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		weightunits = new ArrayList();
		weightunits.add("Lbs");
		weightunits.add("Kgs");
		volumeunits = new ArrayList();
	  	volumeunits.add("Cft");
	  	volumeunits.add("Cbm");
	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		workTicket = workTicketManager.get(id1);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	billing=billingManager.get(serviceOrder.getId());
    	customerFile = serviceOrder.getCustomerFile();
    	house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		//getComboList(sessionCorpID);
		if (id != null) {
			bookStorage = bookStorageManager.get(id);
			storage = bookStorage.getStorage();
		} else {
			bookStorage = new BookStorage();
			bookStorage.setCreatedOn(new Date());
			bookStorage.setUpdatedOn(new Date());
		}
		getActionList();
		getNotesForIconChange();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String oldUnit; 
	private BigDecimal oldPersistedVolume=new BigDecimal("0");
	public String editUnit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		workTicket = workTicketManager.get(id1);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	billing=billingManager.get(serviceOrder.getId());
    	customerFile = serviceOrder.getCustomerFile();
		//getComboList(sessionCorpID);
		
		if (id != null) {
			bookStorage = bookStorageManager.get(id);
			storage = bookStorage.getStorage();
			oldUnit=storage.getVolUnit();
			oldPersistedVolume=storage.getVolume();
			storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
		} else {
			bookStorage = new BookStorage();
			bookStorage.setCreatedOn(new Date());
			bookStorage.setUpdatedOn(new Date());
		}
		getActionList();
		getNotesForIconChange();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");

		List m = notesManager.countTicketLocationNotes(ticket);
		//System.out.println("\n\n id1===============>"+id1);
		//System.out.println("\n\n ticket===============>"+ticket);

		if (m.isEmpty()) {
			countTicketLocationNotes = "0";
		}else{
			countTicketLocationNotes = ((m).get(0)).toString();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
			workTicket = workTicketManager.get(id1);			
			storage.setServiceOrderId(workTicket.getServiceOrderId());			
			storage.setCorpID(sessionCorpID);			
			storage.setTicket(workTicket.getTicket());			
			boolean isNew = (storage.getId() == null);
			if (isNew) {
				storage.setCreatedOn(new Date());				
				bookStorage=new BookStorage();
				bookStorage.setCreatedOn(new Date());
				bookStorage.setWhat("L");
				bookStorage.setDated(new Date());
				bookStorage.setServiceOrderId(workTicket.getServiceOrderId());
				bookStorage.setCorpID(sessionCorpID);
				bookStorage.setTicket(workTicket.getTicket());
				bookStorage.setCreatedBy(getRequest().getRemoteUser());
				bookStorage.setType("");
				bookStorage.setDescription(storage.getDescription());
				bookStorage.setSequenceNumber(workTicket.getSequenceNumber());
				bookStorage.setShip("");
				bookStorage.setShipNumber(workTicket.getShipNumber());
				bookStorage.setContainerId(storage.getContainerId());
				bookStorage.setItemTag(storage.getItemTag());
				bookStorage.setPrice(storage.getPrice());
				bookStorage.setLocationId(storage.getLocationId());
				bookStorage.setOldLocation("");
				bookStorage.setWarehouse(storage.getWarehouse());
				location = locationManager.get(id);
				List autoNumList = storageManager.findMaximum();
				if(autoNumList.get(0) != null){
					autoSequenceNumber = Long.parseLong(autoNumList.get(0).toString()) + 1;
				}else{
					autoSequenceNumber = Long.parseLong("1");
				}
				//autoSequenceNumber = Long.parseLong((storageManager.findMaximum()).get(0).toString()) + 1;
				storage.setIdNum(autoSequenceNumber);
				bookStorage.setIdNum(autoSequenceNumber);
			} 
			storage.setUpdatedOn(new Date());
			storage.setUpdatedBy(getRequest().getRemoteUser());
			
			bookStorage.setUpdatedOn(new Date());
			bookStorage.setUpdatedBy(getRequest().getRemoteUser());
			
			if(bookStorage.getWhat().equalsIgnoreCase("L")){
				location = locationManager.getByLocation(bookStorage.getLocationId());
				location.setOccupied("X");			
			}else{
				location = locationManager.getByLocation(bookStorage.getLocationId());
				location.setOccupied("");
			}
			
			//getComboList(sessionCorpID);					
			storage=storageManager.save(storage);
			bookStorage.setStorage(storage);
			bookStorageManager.save(bookStorage);
			locationManager.save(location);
			String key = (isNew) ? "storage.added" : "bookStorage.updated";
			saveMessage(getText(key));
			//workTicket = workTicketManager.get(id1);
			serviceOrder = workTicket.getServiceOrder();
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		    	billing=billingManager.get(serviceOrder.getId());
		    	customerFile = serviceOrder.getCustomerFile();
				//System.out.println("\n\n\n workTicket.getTicket"+workTicket.getTicket());
		    	bookStorages = bookStorageManager.getStorageList(workTicket.getTicket().toString(), sessionCorpID);
		    	hitFlag="1"; 
		    	if((serviceOrder.getJob().equals("STO")|| serviceOrder.getJob().equals("TPS") || serviceOrder.getJob().equals("STF")|| serviceOrder.getJob().equals("STL"))){
		    		List StorageDataList = serviceOrderManager.getStorageData(serviceOrder.getId(),sessionCorpID); 
		    		int StorageDataListCount=Integer.parseInt(StorageDataList.get(0).toString());
		     	if(serviceOrder.getStatusNumber().intValue()<80){
		     		  String status="STG";
		     		  int statusNumber=80;
		     		  int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser());
		     		 try{
		 			  if(trackingStatus.getSoNetworkGroup()!=null && trackingStatus.getSoNetworkGroup()){ 
						List linkedShipNumber=findLinkerShipNumber(serviceOrder);
						List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						Iterator  it=serviceOrderRecords.iterator();
						while(it.hasNext()){
							try{
							serviceOrderToRecods=(ServiceOrder)it.next(); 
							trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
							if(trackingStatusToRecods.getSoNetworkGroup() && (!(serviceOrder.getShipNumber().toString().equalsIgnoreCase(serviceOrderToRecods.getShipNumber().toString())))){
								 serviceOrderManager.updateStorageJob(serviceOrderToRecods.getId(),status,statusNumber,serviceOrderToRecods.getCorpID(),serviceOrder.getCorpID()+":"+getRequest().getRemoteUser()); 	
					    	}
							}catch(Exception e){
								e.printStackTrace();
							}
						} 
		 			}
		     		}catch(Exception e){
		     			e.printStackTrace();
					}
		     	}
		    	}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	@SkipValidation
    private List findLinkerShipNumber(ServiceOrder serviceOrder) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List linkedShipNumberList= new ArrayList();
    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	}else{
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return linkedShipNumberList;
    }
	@SkipValidation
    private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
    	List<Object> recordList= new ArrayList();
    	Iterator it =linkedShipNumber.iterator();
    	while(it.hasNext()){
    		String shipNumber= it.next().toString();
    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    		recordList.add(serviceOrderRemote);
    	}
    	return recordList;
    	}
	
	String locId;
	private String storageModeValue;
	private String locationByStorage;
	public String saveUnit() throws Exception {
		try{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			workTicket = workTicketManager.get(id1);			
			storage.setServiceOrderId(workTicket.getServiceOrderId());					
			storage.setCorpID(sessionCorpID);			
			storage.setTicket(workTicket.getTicket());			
			boolean isNew = (storage.getId() == null);
			if (isNew) {
				storage.setCreatedOn(new Date());
				bookStorage=new BookStorage();
				bookStorage.setCreatedOn(new Date());
				bookStorage.setWhat("L");
				bookStorage.setDated(new Date());
				bookStorage.setServiceOrderId(workTicket.getServiceOrderId());
				bookStorage.setCorpID(sessionCorpID);
				bookStorage.setTicket(workTicket.getTicket());
				bookStorage.setCreatedBy(getRequest().getRemoteUser());
				bookStorage.setType("");
				bookStorage.setDescription(storage.getDescription());
				bookStorage.setSequenceNumber(workTicket.getSequenceNumber());
				bookStorage.setShip("");
				bookStorage.setShipNumber(workTicket.getShipNumber());
				bookStorage.setContainerId(storage.getContainerId());
				bookStorage.setItemTag(storage.getItemTag());
				bookStorage.setPrice(storage.getPrice());
				bookStorage.setLocationId(storage.getLocationId());
				if(storageModeValue==null){
					storageModeValue="";
				}
				if(locationByStorage==null){
					locationByStorage="";
				}
				if(!(locationByStorage.equalsIgnoreCase(storage.getLocationId().toString())) && !(storageModeValue.equalsIgnoreCase("5"))){
					bookStorage.setOldLocation(locationByStorage);
					
				}
				else{
				   bookStorage.setOldLocation("");
				}
				bookStorage.setStorageId(storage.getStorageId());
				List autoNumList = storageManager.findMaximum();
				if(autoNumList.get(0) != null){
					autoSequenceNumber = Long.parseLong(autoNumList.get(0).toString()) + 1;
				}else{
					autoSequenceNumber = Long.parseLong("1");
				}
				storage.setIdNum(autoSequenceNumber);
				bookStorage.setIdNum(autoSequenceNumber);
			} 
			storage.setShipNumber(workTicket.getShipNumber());
			storage.setUpdatedOn(new Date());
			storage.setUpdatedBy(getRequest().getRemoteUser());			
			bookStorage.setUpdatedOn(new Date());
			bookStorage.setUpdatedBy(getRequest().getRemoteUser());			
			/*if(bookStorage.getWhat().equalsIgnoreCase("L")){
				location = locationManager.getByLocation(storage.getLocationId());
				storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
				location.setOccupied("X");
				BigDecimal cuMultiplyer=new BigDecimal(35.314);
                BigDecimal cuDivisor=new BigDecimal(35.314);
                BigDecimal currentVolume=new BigDecimal("0");
                BigDecimal availcbm1=new BigDecimal("0");
                BigDecimal availcft1=new BigDecimal("0");
                BigDecimal cft = new BigDecimal("0");
                BigDecimal cbm = new BigDecimal("0");
                if(location.getUtilizedVolCft()!=null && !(location.getUtilizedVolCft().equals(""))){
                	cft=location.getUtilizedVolCft();
                }
                if(location.getUtilizedVolCbm()!=null && !(location.getUtilizedVolCbm().equals(""))){
                	cbm = location.getUtilizedVolCbm();
                }  
                if(oldUnit!=null && oldUnit.equalsIgnoreCase("Cft")){                	
                	BigDecimal availcbm111=new BigDecimal("0");
                    BigDecimal availcft111=new BigDecimal("0");
                    availcft111=oldPersistedVolume;
                    availcbm111=oldPersistedVolume.divide(cuDivisor, 3, 0);
                	BigDecimal curravailcbm111=new BigDecimal("0");
                    BigDecimal curravailcft111=new BigDecimal("0");
                    curravailcft111=storage.getVolume().subtract(availcft111);
                    curravailcbm111=storage.getVolume().subtract(availcbm111);
                	BigDecimal avalcft=cft.add(curravailcft111);
                    BigDecimal avalcbm=cbm.add(curravailcbm111.divide(cuDivisor, 3, 0));
                    availcft1=avalcft;                  
                    availcbm1=avalcbm;
                }else if(oldUnit!=null && oldUnit.equalsIgnoreCase("Cbm")){
                	BigDecimal availcbm111=new BigDecimal("0");
                    BigDecimal availcft111=new BigDecimal("0");                	
                	availcbm111=oldPersistedVolume;
                	availcft111=oldPersistedVolume.multiply(cuMultiplyer);
                	BigDecimal curravailcbm111=new BigDecimal("0");
                    BigDecimal curravailcft111=new BigDecimal("0");
                    curravailcft111=storage.getVolume().subtract(availcft111);
                    curravailcbm111=storage.getVolume().subtract(availcbm111);
                	BigDecimal avalcft=cft.add(curravailcft111.multiply(cuMultiplyer));
                    BigDecimal avalcbm=cbm.add(curravailcbm111);
                    availcft1=avalcft;                  
                    availcbm1=avalcbm;  
                }else{
                	if(storage.getVolUnit().equalsIgnoreCase("Cft")){ 
                		currentVolume=storage.getVolume();
                        BigDecimal avalcft=cft.add(currentVolume);
                        BigDecimal avalcbm=cbm.add(currentVolume.divide(cuDivisor, 3, 0));
                        availcft1=avalcft;                  
                        availcbm1=avalcbm;                    
                    }else{
                    	currentVolume=storage.getVolume();
                        BigDecimal avalcft=cft.add(currentVolume.multiply(cuMultiplyer));
                        BigDecimal avalcbm=cbm.add(currentVolume);
                        availcft1=avalcft;                  
                        availcbm1=avalcbm; 
                    }
                }             
				location.setUtilizedVolCft(availcft1);
				location.setUtilizedVolCbm(availcbm1);
				//storageLibrary.setStorageLocked(true);
				storageLibrary.setStorageAssigned(true);
				if(addedUtilizedVolCft!=null && !(addedUtilizedVolCft.equalsIgnoreCase(""))){
					storageLibrary.setUsedVolumeCft(new BigDecimal(addedUtilizedVolCft));
				}
				if(addedUtilizedVolCbm!=null && !(addedUtilizedVolCbm.equalsIgnoreCase(""))){
					storageLibrary.setUsedVolumeCbm(new BigDecimal(addedUtilizedVolCbm));
				}
				if(subAvailVolCft!=null && !(subAvailVolCft.equalsIgnoreCase(""))){
					storageLibrary.setAvailVolumeCft(new BigDecimal(subAvailVolCft));
				}
				if(subAvailVolCbm!=null && !(subAvailVolCbm.equalsIgnoreCase(""))){
					storageLibrary.setAvailVolumeCbm(new BigDecimal(subAvailVolCbm));
				}
			}else{
				location = locationManager.getByLocation(bookStorage.getLocationId());
				storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
				location.setOccupied("");
			}*/
			
			//getComboList(sessionCorpID);
			List locIdList=storageManager.getLocationId(storage.getStorageId());
			if((locIdList!=null)&& !(locIdList.isEmpty())&& locIdList.get(0)!=null){
			 locId=locIdList.get(0).toString();
			}
			storage=storageManager.save(storage);
			bookStorage.setStorage(storage);
			bookStorageManager.save(bookStorage);			
			//location=locationManager.save(location);
			//storageLibraryManager.save(storageLibrary);
			location = locationManager.getByLocation(storage.getLocationId());
			storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
			if(!(storageLibrary.getStorageMode().equalsIgnoreCase("5"))){
			if(!(locId.equalsIgnoreCase(storage.getLocationId().toString()))){
			List li=storageManager.getRecordsForBookSto(sessionCorpID,storage.getStorageId(),storage.getTicket());
			
			if((li!=null) && !(li.isEmpty())){
				Iterator it=li.iterator();
				while(it.hasNext()){
					Long stoId=Long.parseLong(it.next().toString());
					Storage sto=storageManager.get(stoId);
					bookStorage=new BookStorage();
					bookStorage.setCreatedOn(new Date());
					bookStorage.setWhat("M");
					bookStorage.setDated(new Date());
					bookStorage.setServiceOrderId(sto.getServiceOrderId());
					bookStorage.setCorpID(sessionCorpID);
					bookStorage.setTicket(sto.getTicket());
					bookStorage.setCreatedBy(getRequest().getRemoteUser());
					bookStorage.setType("");
					bookStorage.setDescription(sto.getDescription());
				//	bookStorage.setSequenceNumber(sto.getSequenceNumber());
					bookStorage.setShip("");
					bookStorage.setShipNumber(sto.getShipNumber());
					bookStorage.setContainerId(sto.getContainerId());
					bookStorage.setItemTag(sto.getItemTag());
					bookStorage.setPrice(sto.getPrice());
					bookStorage.setLocationId(location.getLocationId());
					bookStorage.setOldLocation(sto.getLocationId());
					bookStorage.setStorageId(sto.getStorageId());
					bookStorage.setIdNum(sto.getIdNum());
					bookStorage.setUpdatedBy(getRequest().getRemoteUser());
					bookStorage.setUpdatedOn(new Date());
					bookStorageManager.save(bookStorage);
					
				}
				
			}
			     storageManager.updateStorageForLocation(sessionCorpID,storage.getStorageId(),location.getLocationId());
			}
			}
			List dummyStorageUnit=storageManager.dummyStorageList(storage.getStorageId(), sessionCorpID);
			if(dummyStorageUnit!=null && (!(dummyStorageUnit.isEmpty())) && dummyStorageUnit.size()>1){
				storageManager.deleteDummyUsedRecord(storage.getStorageId(), sessionCorpID);
			}
			String key = (isNew) ? "storage.added" : "bookStorage.updated";
			saveMessage(getText(key));			
			serviceOrder = workTicket.getServiceOrder();
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		    	billing=billingManager.get(serviceOrder.getId());
		    	customerFile = serviceOrder.getCustomerFile();
				//System.out.println("\n\n\n workTicket.getTicket"+workTicket.getTicket());
		    	bookStorages = bookStorageManager.getStorageList(workTicket.getTicket().toString(), sessionCorpID);
		    	hitFlag="1"; 
		    	if((serviceOrder.getJob().equals("STO")|| serviceOrder.getJob().equals("TPS") || serviceOrder.getJob().equals("STF")|| serviceOrder.getJob().equals("STL"))){
		    		List StorageDataList = serviceOrderManager.getStorageData(serviceOrder.getId(),sessionCorpID); 
		    		int StorageDataListCount=Integer.parseInt(StorageDataList.get(0).toString());
		     	if(serviceOrder.getStatusNumber().intValue()<80){
		     		  String status="STG";
		     		  int statusNumber=80;
		     		  int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser());
		     		 try{
			 			  if(trackingStatus.getSoNetworkGroup()!=null && trackingStatus.getSoNetworkGroup()){ 
							List linkedShipNumber=findLinkerShipNumber(serviceOrder);
							List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
							Iterator  it=serviceOrderRecords.iterator();
							while(it.hasNext()){
								try{
								serviceOrderToRecods=(ServiceOrder)it.next(); 
								trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
								if(trackingStatusToRecods.getSoNetworkGroup() && (!(serviceOrder.getShipNumber().toString().equalsIgnoreCase(serviceOrderToRecods.getShipNumber().toString())))){
									 serviceOrderManager.updateStorageJob(serviceOrderToRecods.getId(),status,statusNumber,serviceOrderToRecods.getCorpID(),serviceOrder.getCorpID()+":"+getRequest().getRemoteUser()); 	
						    	}
								}catch(Exception e){
									e.printStackTrace();
								}
							} 
			 			}
			     		}catch(Exception e){
			     			e.printStackTrace();
						}
		     	}
		    	}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	private Billing billing;
	private BillingManager billingManager;
	@SkipValidation
	public String bookStoragesList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	billing=billingManager.get(serviceOrder.getId());
    	customerFile = serviceOrder.getCustomerFile();
    	//System.out.println("\n\n\n bookStorages"+workTicket.getTicket());
		bookStorages = bookStorageManager.getStorageList(workTicket.getTicket().toString(), sessionCorpID);
		//bookStorages = bookStorageManager.getStorageList(workTicket.getShipNumber());
		//System.out.println("\n\n\n bookStorages"+bookStorages.size());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String bookStorageLibraryList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	billing=billingManager.get(serviceOrder.getId());
    	customerFile = serviceOrder.getCustomerFile();
    	//System.out.println("\n\n\n bookStorages"+workTicket.getTicket());
		bookStorages = bookStorageManager.getStorageLibraryList(workTicket.getTicket().toString(), sessionCorpID);
		//bookStorages = bookStorageManager.getStorageList(workTicket.getShipNumber());
		//System.out.println("\n\n\n bookStorages"+bookStorages.size());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private  Map<String, String> actionType;
	
	public  Map<String, String> getActionType() {
		return actionType;
	}
	
	public String getActionList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		actionType = new LinkedHashMap<String, String>();
		actionType.put("L", "L" + " : " + "Load");
		actionType.put("A", "A" + " : " + "Access");
		actionType.put("R", "R" + " : " + "Release");
		actionType.put("M", "M" + " : " + "Move");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public  List getWeightunits() {
		return weightunits;
	}

	public  void setWeightunits(List weightunits) {
		this.weightunits = weightunits;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public Long getBID() {
		return BID;
	}

	public void setBID(Long bid) {
		BID = bid;
	}

	public Long getId1() {
		return id1;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getSoId() {
		return soId;
	}

	public void setSoId(Long soId) {
		this.soId = soId;
	}

	public Long getWtId() {
		return wtId;
	}

	public void setWtId(Long wtId) {
		this.wtId = wtId;
	}

	public List getVolumeunits() {
		return volumeunits;
	}

	public void setVolumeunits(List volumeunits) {
		this.volumeunits = volumeunits;
	}

	public void setStorageLibraryManager(StorageLibraryManager storageLibraryManager) {
		this.storageLibraryManager = storageLibraryManager;
	}

	public StorageLibrary getStorageLibrary() {
		return storageLibrary;
	}

	public void setStorageLibrary(StorageLibrary storageLibrary) {
		this.storageLibrary = storageLibrary;
	}

	public String getAddedUtilizedVolCft() {
		return addedUtilizedVolCft;
	}

	public void setAddedUtilizedVolCft(String addedUtilizedVolCft) {
		this.addedUtilizedVolCft = addedUtilizedVolCft;
	}

	public String getAddedUtilizedVolCbm() {
		return addedUtilizedVolCbm;
	}

	public void setAddedUtilizedVolCbm(String addedUtilizedVolCbm) {
		this.addedUtilizedVolCbm = addedUtilizedVolCbm;
	}

	public String getSubAvailVolCft() {
		return subAvailVolCft;
	}

	public void setSubAvailVolCft(String subAvailVolCft) {
		this.subAvailVolCft = subAvailVolCft;
	}

	public String getSubAvailVolCbm() {
		return subAvailVolCbm;
	}

	public void setSubAvailVolCbm(String subAvailVolCbm) {
		this.subAvailVolCbm = subAvailVolCbm;
	}

	public String getOldUnit() {
		return oldUnit;
	}

	public void setOldUnit(String oldUnit) {
		this.oldUnit = oldUnit;
	}

	public BigDecimal getOldPersistedVolume() {
		return oldPersistedVolume;
	}

	public void setOldPersistedVolume(BigDecimal oldPersistedVolume) {
		this.oldPersistedVolume = oldPersistedVolume;
	}

	public String getLocationByStorage() {
		return locationByStorage;
	}

	public void setLocationByStorage(String locationByStorage) {
		this.locationByStorage = locationByStorage;
	}

	public String getStorageModeValue() {
		return storageModeValue;
	}

	public void setStorageModeValue(String storageModeValue) {
		this.storageModeValue = storageModeValue;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Company getCompany() {
		return company;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	
}
