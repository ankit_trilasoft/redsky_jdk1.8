ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `agentGroup` VARCHAR(15) AFTER `ugwwNetworkGroup`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `doNotEmail` BIT(1) DEFAULT b'0' AFTER `newsUpdateFlag`;

ALTER TABLE `redsky`.`contractpolicy` ADD COLUMN `docSequenceNumber` BIGINT(20) DEFAULT NULL AFTER `parentId`;

ALTER TABLE `redsky`.`policydocument` ADD COLUMN `docSequenceNumber` BIGINT(20) DEFAULT NULL AFTER `parentId`;

update networkdatafields set toModelName='AccountLine',tofieldname='estimateDeviation' ,fieldname ='estimateSellDeviation'  where fieldname ='estimateDeviation' and modelName='AccountLine';

update networkdatafields set toModelName='AccountLine',tofieldname='revisionDeviation' ,fieldname ='revisionSellDeviation'  where fieldname ='revisionDeviation' and modelName='AccountLine';

update networkdatafields set toModelName='AccountLine',tofieldname='payDeviation' ,fieldname ='receivableSellDeviation'  where fieldname ='payDeviation' and modelName='AccountLine';

ALTER TABLE `redsky`.`cportalresourcemgmt` ADD COLUMN `docSequenceNumber` BIGINT(20) DEFAULT null AFTER `fileSize`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `storageEmail` VARCHAR(60);





