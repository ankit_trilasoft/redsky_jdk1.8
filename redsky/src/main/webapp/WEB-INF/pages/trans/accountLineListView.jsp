<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>

<%-- <%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%> --%>
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/>
<style>
span.pagelinks {display: block;font-size: 0.85em;margin-bottom: 2px;margin-top: -20px;padding: 2px 0 2px 22%;text-align: left; width: 100%; }
.tdheight-footer {border: 1px solid #e0e0e0; padding: 0;}
</style>
<style type="text/css">
.listwhitetextWhite {
    background-color: #FFFFFF;
    color: #003366;
    font-family: arial,verdana;
    font-size: 10.5px;
   }
</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>">
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
</head>

<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="ServiceOrder" />
<s:form id="pricingListForm" name="pricingListForm" action=""  onsubmit="return submit_form()" method="post">
<c:set var="rolesAssign" value="${roles}" />
<c:set var="soCoordinator" value="${fn:trim(serviceOrder.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(userName)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />
<c:set var="modeWeight" value="" />
<c:set var="modeVolumeDesc" value="" />
<c:if test="${fn:contains(rolesAssign, 'ROLE_SUPERVISOR')}">
<c:set var="rolesAssignFlag" value="Y" />
</c:if>
<c:if test="${serviceOrder.mode=='Air'}">
<c:set var="modeWeight" value="${miscellaneous.actualGrossWeight}" />
<c:set var="modeVolumeDesc" value="${miscellaneous.actualCubicFeet}" />
</c:if>
<c:if test="${serviceOrder.mode!='Air' && serviceOrder.mode!=''}">
<c:set var="modeWeight" value="${miscellaneous.actualNetWeight}" />
<c:set var="modeVolumeDesc" value="${miscellaneous.netActualCubicFeet}" />
</c:if>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<%--  New Field Added Start --%>
<s:hidden name="ratePaybleHid" />
<s:hidden name="localAmountPaybleHid" /> 
<s:hidden name="accEstimateQuantity" />
<s:hidden name="accEstimateRate" />
<s:hidden name="accEstimateExpense" />
<s:hidden name="accEstimateSellRate" />
<s:hidden name="accEstimateRevenueAmount" />
<s:hidden name="accEstSellLocalAmount" />
<s:hidden name="formStatus" />
<s:hidden name="checkComputeDescription" />
<%--  New Field Added End --%> 
<s:hidden name="accUpdateAjax" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/> 
<s:hidden name="cfContract" value="${billing.contract}" />
<s:hidden name="inComptetive" value="${customerFile.comptetive}"/>
 <s:hidden name="buttonType" />
	<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
	<s:hidden name="tempId1"></s:hidden>
	<s:hidden name="tempId2"></s:hidden>
	<s:hidden name="transferDateFlag" />
	<s:hidden name="firstDescription" />
	<s:hidden name="salesCommisionRate" value="${salesCommisionRate}"/>
	<s:hidden name="grossMarginThreshold" value="${grossMarginThreshold}"/>
	<s:hidden name="inEstimatedTotalExpense" value="${serviceOrder.estimatedTotalExpense}"/>
	<s:hidden name="inEstimatedTotalRevenue" value="${serviceOrder.estimatedTotalRevenue}"/>
	<s:hidden name="inRevisedTotalExpense" value="${serviceOrder.revisedTotalExpense}"/>
	<s:hidden name="inRevisedTotalRevenue" value="${serviceOrder.revisedTotalRevenue}"/>
	<s:hidden name="inActualExpense" value="${serviceOrder.actualExpense}"/>
	<s:hidden name="inActualRevenue" value="${serviceOrder.actualRevenue}"/>
		<s:hidden name="routingSO" value="${serviceOrder.routing}" />
		<s:hidden name="modeSO" value="${serviceOrder.mode}"/>
		  <s:hidden name="serviceOrder.revisedTotalExpense" />  
				<s:hidden name="serviceOrder.contract" /> 
				<s:hidden name="serviceOrder.originCity" id="originCity"/>
				<s:hidden name="serviceOrder.destinationCity" id="destinationCity"/>
				<s:hidden name="serviceOrder.projectedActualExpense" />
				<s:hidden name="serviceOrder.packingMode" />
				<s:hidden name="serviceOrder.serviceType" />
				<s:hidden name="miscellaneous.netEstimateCubicMtr" />
				<s:hidden name="miscellaneous.estimatedNetWeight" />
				<s:hidden name="serviceOrder.orderBy" value="%{customerFile.orderBy}" /> 
			   <s:hidden name="serviceOrder.orderPhone" value="%{customerFile.orderPhone}" />
			    <s:hidden name="serviceOrder.payType" value="%{customerFile.billPayMethod}" /> 
				<s:hidden name="serviceOrder.actualNetWeight" value="%{miscellaneous.actualNetWeight}" /> 
				<s:hidden name="serviceOrder.actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" /> 
				<s:hidden name="serviceOrder.actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" /> 
				<s:hidden name="serviceOrder.estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" /> 
				<s:hidden name="serviceOrder.estimatedNetWeight" value="%{miscellaneous.estimatedNetWeight}" /> 
				<s:hidden name="serviceOrder.estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" />
				<s:hidden name="miscellaneous.netEstimateCubicFeet" /> 
				<s:hidden name="miscellaneous.netActualCubicMtr" />
				<s:hidden name="miscellaneous.netActualCubicFeet" />
				<s:hidden name="miscellaneous.estimatedNetWeightKilo" />
				<s:hidden name="accountIdCheck"/> 
				<s:hidden name="serviceOrder.revisedTotalRevenue" /> 
				<s:hidden name="serviceOrder.revisedGrossMargin" />
				<s:hidden name="serviceOrder.revisedGrossMarginPercentage" /> 
				<s:hidden name="serviceOrder.entitledTotalAmount" /> 
				<s:hidden name="serviceOrder.estimatedTotalExpense" /> 
				<s:hidden name="serviceOrder.estimatedTotalRevenue" /> 
				<s:hidden name="serviceOrder.estimatedGrossMargin" /> 
				<s:hidden name="serviceOrder.distributedTotalAmount" />  
				<s:hidden name="serviceOrder.estimatedGrossMarginPercentage"/> 
				<s:hidden name="serviceOrder.actualExpense"/>
			    <s:hidden name="serviceOrder.actualRevenue"/> 
				<s:hidden name="serviceOrder.actualGrossMargin"/> 
				<s:hidden name="serviceOrder.actualGrossMarginPercentage"/>
				<s:hidden name="serviceOrder.projectedGrossMarginPercentage" />
				<s:hidden name="defaultTemplate" id="defaultTemplate"/>
				<s:hidden name="serviceOrder.projectedActualRevenue" />
				<s:hidden name="serviceOrder.projectedGrossMargin" />
				<s:hidden name="serviceOrder.actualAuto"/>
				<s:hidden name="serviceOrder.actualBoat"/>				
				<s:hidden name="serviceOrder.controlFlag" />
				<s:hidden name="serviceOrder.isSOExtract"/>
				<s:hidden name="isSOExtract"/> 
				<s:hidden name="secondDescription" />
			    <s:hidden name="thirdDescription" /> 
			    <s:hidden name="fourthDescription" /> 
				<s:hidden name="fifthDescription" />
				 <s:hidden name="sixthDescription" />
				 <s:hidden name="seventhDescription" /> 
				 <s:hidden name="eigthDescription" />
	             <s:hidden name="ninthDescription" />
	             <s:hidden name="tenthDescription" />
	             <s:hidden name="oldStatus" value="%{serviceOrder.status}" /> 
	             	<s:hidden name="oldStatusNumber" value="%{serviceOrder.statusNumber}" /> 
	             	<c:if test="${not empty serviceOrder.id}">
	<c:if test="${not empty billing.billComplete}">
	 <s:text id="billingBillingComplete" name="${FormDateValue}"> <s:param name="value" value="billing.billComplete" /></s:text>
	 <s:hidden  name="billing.billComplete" value="%{billingBillingComplete}" />  
	</c:if>
	<c:if test="${empty billing.billComplete}">
	<s:hidden  name="billing.billComplete" />
	</c:if>
	<s:hidden name="billing.billCompleteA" value="%{billing.billCompleteA}" />
	<c:if test="${not empty trackingStatus.deliveryA}">
	<s:text id="trackingStatusDeliveryA" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.deliveryA" /></s:text>
	<s:hidden  name="trackingStatus.deliveryA" value="%{trackingStatusDeliveryA}" />  
	</c:if>
	<c:if test="${empty trackingStatus.deliveryA}">
	<s:hidden  name="trackingStatus.deliveryA" />  
	</c:if>
	<s:hidden  name="claimCount" value="${claimCount}"/> 
	<s:hidden name="invoiceCount" value="${invoiceCount}"/> 
	</c:if> 
<s:hidden name="vanLineAccountView" id ="vanLineAccountView"/>
<s:hidden name="generateMassage" /> 
<s:hidden name="activateAccPortal" id="activateAccPortalID"/> 
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="accountInterface" value="${accountInterface}"/>
<s:hidden name="multiCurrency" value="${multiCurrency}"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.shipNumber" /> 
<s:hidden name="customerFile.id"/>
<s:hidden name="billToCodeForTicket"/>
<s:hidden name="companyDivisionForTicket"/>
<s:hidden name="billing.billToAuthority"/>
<s:hidden name="billing.billToCode"/>
<s:hidden name="approvedPayingStatusDisableStatus" value=""/>
<c:set var="approvedPayingStatusDisableStatus" value="no"/>
<s:hidden name="billingDMMContractTypePage" value="${billingDMMContractType}" />
<s:hidden name="userRoleExecutive" value="${userRoleExecutive}"/>
<s:hidden name="chargeCodeValidationVal" id="chargeCodeValidationVal" />
<s:hidden name="checkChargeCodePopup" value=""/>
<s:hidden name="chargeCostElement" value="" />
<configByCorp:fieldVisibility componentId="component.accountLine.approvedPayingStatus.disableStatus">
	<c:set var="approvedPayingStatusDisableStatus" value="yes"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
		<s:hidden name="actgCodeFlag" value="YES" />
	</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
	<s:hidden name="rollUpInvoiceFlag" value="True" />
</configByCorp:fieldVisibility> 
<s:hidden name="emptyList"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
<c:set var="tableName" value="accountline" scope="session"/>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if> 
<s:hidden name="systemDefaultmiscVl" value="%{systemDefaultmiscVl}" />
<s:hidden name="systemDefaultstorage" value="%{systemDefaultstorage}" />
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<c:set var="btntype"  value="<%=request.getParameter("btntype") %>"/>
<c:set var="billingFlag" value="${billingFlag}" scope="request"/> 
<s:hidden name="billingContractFlag" value="${billingContractFlag}"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/> 
<div id="newmnav" style="float: left;">
  <ul>
  <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
  <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
  <c:set var="relocationServicesKey" value="" />
  <c:set var="relocationServicesValue" value="" /> 
  <c:forEach var="entry" items="${relocationServices}">
         <c:if test="${relocationServicesKey==''}">
         <c:if test="${entry.key==serviceOrder.serviceType}">
         <c:set var="relocationServicesKey" value="${entry.key}" />
         <c:set var="relocationServicesValue" value="${entry.value}" /> 
         </c:if>
         </c:if> 
     </c:forEach>   
    <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
     <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
 
  <li id="newmnav1" style="background:#FFF "><a href="pricingList.html?sid=${serviceOrder.id}" class="current" ><span>Accounting<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
     <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
     			<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 	
      
	  <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	  </sec-auth:authComponent>
	  </c:if>  
<c:if test="${serviceOrder.job !='RLO'}">
 <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">
	   <c:if test="${forwardingTabVal!='Y'}"> 
	   		<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  </c:if>
	  <c:if test="${forwardingTabVal=='Y'}">
	  		<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  </c:if>
  </c:if>
  </c:if>
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS' || billing.billToCode =='P4071'}">
  <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </c:if> 
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	     <c:if test="${serviceOrder.job =='INT'}">
	      <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	     </c:if>
  </sec-auth:authComponent>
  <c:if test="${serviceOrder.job =='RLO'}"> 
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>	
 <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab"> 
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  </c:if>
  	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		 <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li> 
	</c:if>
	</sec-auth:authComponent>
	<configByCorp:fieldVisibility componentId="component.standard.claimTab">	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent>
	</configByCorp:fieldVisibility>
   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
			</sec-auth:authComponent>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  		<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}"><span>Critical Dates</span></a></li>
	</sec-auth:authComponent>
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  <%-- <configByCorp:fieldVisibility componentId="component.field.Alternative.pricingWizardTab">
  <li><a href="pricingWizardSummary.html?from=Accounting&shipnumber=${serviceOrder.id}" ><span>Pricing</span></a></li>
  </configByCorp:fieldVisibility> --%>
  </ul>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;"><tr>
	<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if> 
		</tr></table>
<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;!padding-bottom:6px;"></div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
	<tr> 
	<td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0" width="">
		  <tbody> 		  
		  <tr>
               <td align="left" colspan="16">
             <table style="margin:0px;padding:0px;">          	  	
		  	<tr><td align="right" width="52">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td>
		  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"   size="22"  cssClass="input-textUpper" readonly="true"/>
		  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="15" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="4" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="4" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.routing"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="4" readonly="true"/></td>
		  	<td align="right"><fmt:message key='customerFile.status'/></td>
            <td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" key="serviceOrder.status" size="4" readonly="true"/> </td>
		  	</tr>
		  	<tr>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td>
		  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="17" readonly="true"/></td>
		  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="15" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="3" readonly="true"/></td>
			 <c:if test="${serviceOrder.job!='RLO'}">
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="4" readonly="true"/></td>
		  	</c:if>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td>
		  	<td align="left" colspan="5"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="43" readonly="true"/></td>
		  	</tr>
		 	</table>
		 	</td>
		 	</tr>
			<tr>
	            <td align="right" class="listwhitetext" width="56">Bkg. Agent&nbsp;</td>
	            <td><s:textfield cssClass="input-textUpper" name="serviceOrder.bookingAgentCode" size="14" maxlength="8" readonly="true"/></td>
				<td align="right" class="listwhitetext" >Division</td>
				<td><s:textfield cssClass="input-textUpper" name="serviceOrder.companyDivision" size="6" maxlength="3" readonly="true"/></td>
				
				<td align="left" class="listwhitetext" colspan="11">
				<c:if test="${from=='rule'}">
				<c:if test="${field=='serviceorder.revised'}">
				<font color="red">Revised</font>
				</c:if></c:if>
				<c:if test="${from=='rule'}">
				<c:if test="${field!='serviceorder.revised'}">
				Revised
				</c:if></c:if>
				<c:if test="${from!='rule'}">
				Revised
				</c:if>
				<c:if test="${serviceOrder.revised == true}">
				    <input type="checkbox" style="margin-left:0px;vertical-align:bottom;" id="checkboxId" value="${serviceOrder.id}" onclick="checkRevisedId(${serviceOrder.id},this)" checked/>
				    </c:if>
					<c:if test="${serviceOrder.revised == false || serviceOrder.revised == null}">
					<input type="checkbox" style="margin-left:0px;vertical-align:bottom;" id="checkboxId" value="${serviceOrder.id}" onclick="checkRevisedId(${serviceOrder.id},this)"/>
					</c:if>
					
				<c:if test="${from=='rule'}">
				<c:if test="${field=='serviceorder.est'}">
				<font color="red">Est</font>
				</c:if></c:if>
				<c:if test="${from=='rule'}">
				<c:if test="${field!='serviceorder.est'}">
				&nbsp;Est
				</c:if></c:if>
				<c:if test="${from!='rule'}">
				&nbsp;Est
				</c:if>
				<c:if test="${serviceOrder.est == true}">
				    <input type="checkbox" style="margin-left:0px;vertical-align:bottom;" value="${serviceOrder.id}" onclick="checkEstId(${serviceOrder.id},this)" checked/>
				    </c:if>
					<c:if test="${serviceOrder.est == false || serviceOrder.est == null}">
					<input type="checkbox" style="margin-left:0px;vertical-align:bottom;" value="${serviceOrder.id}" onclick="checkEstId(${serviceOrder.id},this)"/>
					</c:if>
					
				&nbsp;Contract&nbsp;<s:textfield cssClass="input-textUpper" name="billing.contract" size="28"  readonly="true" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instruction&nbsp;<s:textfield cssClass="input-textUpper" name="billing.billingInstruction" size="43"  readonly="true" /></td>   
			</tr>
			</table>
			</td>
			</tr>
			<tr>
<td align="left" colspan="18" class="vertlinedata"></td>
</tr>
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="">
<tbody>
	<tr>
	  <c:if test="${serviceOrder.job!='RLO'}">
	<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
                                 <td width="57" align="right" class="listwhitetext">Est&nbsp;Wght</td>
                     
                                 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
                               </c:if>
                               <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 
                                 <td align="right" class="listwhitetext">Act&nbsp;Wght</td>
                                 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                  <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true"/></td>
                                 </configByCorp:fieldVisibility>
                                 </c:if>
                                   <c:if test="${serviceOrder.job!='RLO'}">
                                 <configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
                                 <td height="25" align="right" class="listwhitetext">Est&nbsp;Vol.</td>
                     				 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                 </c:if>
                                <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicMtr}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicMtr}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicFeet}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 
                                 <td align="right" class="listwhitetext">Act&nbsp;Vol.</td>
                                 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicMtr}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                  <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicMtr}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicFeet}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit2" value="%{miscellaneous.unit2}" size="3"  maxlength="10" readonly="true"/></td>
                                 </configByCorp:fieldVisibility>			
                                 </c:if>
      <configByCorp:fieldVisibility componentId="customerFile.survey">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
	<c:if test="${not empty customerFile.survey}">
	<s:text id="customerFileSurveyFormattedValues" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="Survey" value="${customerFileSurveyFormattedValues}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
	</c:if>
	<c:if test="${empty customerFile.survey}">
	<td><s:textfield cssClass="input-textUpper" name="Survey" value="${customerFileSurveyFormattedValues}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
	</c:if>
	</configByCorp:fieldVisibility> 
	 <configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
	<c:if test="${not empty trackingStatus.surveyDate}">
	<s:text id="trackingStatusSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.surveyDate"/></s:text>
	<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<c:if test="${empty trackingStatus.surveyDate}">
	<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	</configByCorp:fieldVisibility> 
	  <c:if test="${serviceOrder.job!='RLO'}">	
	 <td align="right" class="listwhitetext">Pack&nbsp;Date</td>                                            
     <c:if test="${empty trackingStatus.packA}">
     <c:if test="${empty trackingStatus.beginPacking}">
     <td><s:textfield cssClass="input-textUpper" name="packingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
     </c:if>
     <c:if test="${not empty trackingStatus.beginPacking}">
     <td width="10px"><font color="red" size="1">(T)</font></td>
     <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginPacking"/></s:text>
     <td><s:textfield cssClass="input-textUpper"  name="packingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
     </c:if>  
                     </c:if>
     <c:if test="${not empty trackingStatus.packA}">
     <td width="10px"><font color="red" size="1">(A)</font></td>
     <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packA"/></s:text>
     <td><s:textfield cssClass="input-textUpper"  name="packingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
     </c:if>
     
     <td align="right" class="listwhitetext">Load&nbsp;Date</td>
	<c:if test="${empty trackingStatus.loadA}">
	<c:if test="${empty trackingStatus.beginLoad}"> 
	<td><s:textfield cssClass="input-textUpper" name="loadingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<c:if test="${not empty trackingStatus.beginLoad}">
	<td width="10px"><font color="red" size="1">(T)</font></td>
	<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>  
	</c:if>
	<c:if test="${not empty trackingStatus.loadA}">
	<td width="10px"><font color="red" size="1">(A)</font></td>
	<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<td align="right" class="listwhitetext">Delivery</td>
	<c:if test="${empty trackingStatus.deliveryA}">
	<c:if test="${empty trackingStatus.deliveryShipper}"> 
	<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<c:if test="${not empty trackingStatus.deliveryShipper}">
	<td width="10px"><font color="red" size="1">(T)</font></td>
	<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>  
	</c:if>
	<c:if test="${not empty trackingStatus.deliveryA}">
	<td width="10px"><font color="red" size="1">(A)</font></td>
	<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if> 
	</c:if>
	<td align="right" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
	<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="20"  maxlength="250" readonly="true"/></td>
       <configByCorp:fieldVisibility componentId="component.field.serviceOrder.coordinator">
         <td align="right" class="listwhitetext"><fmt:message key="serviceOrder.coordinator"/></td>
            <td align="left"><s:textfield name="serviceOrder.coordinator" cssClass="input-textUpper" size="18" readonly="true"/></td>
           </configByCorp:fieldVisibility>
	</tr>
</tbody>
</table>
</td>
</tr> 
<configByCorp:fieldVisibility componentId="component.accountline.accrualbutton.show">
<tr>
<td height="4px" align="left" colspan="18" style="!padding-bottom:4px;" class="vertlinedata"></td>
</tr>
<tr> 
    <td align="left" class="listwhitebox">
        <table class="detailTabLabel" border="0" width="">
          <tbody>   
<tr>
<c:choose>
<c:when test="${appUserName==soCoordinator || rolesAssignFlag=='Y'}">
    <td align="right" class="listwhitetext"></td>
    <td align="left"> <div id="hideTd"><input type="button" id="accrualReady" value="Accrual Ready" style="width:100px;margin-left:20px;height:22px;border-radius:4px;" class="cssbutton1"  onclick="checkAccrualReadyValidation();" theme="simple"/></div></td>
</c:when>
<c:otherwise>
<td align="right" class="listwhitetext"></td>
    <td align="left"> <div id="hideTd"><input type="button" id="accrualReady" value="Accrual Ready" style="width:100px;margin-left:20px;height:22px;border-radius:4px;" class="pricelistbtn-dis"  theme="simple" disabled="disabled"/></div></td>
</c:otherwise>
</c:choose>
    <td align="right" class="listwhitetext">Accrual Ready</td>
    <c:if test="${not empty serviceOrder.accrualReadyDate}">
    <s:text id="accrualReadyDate" name="${FormDateValue}"><s:param name="value" value="serviceOrder.accrualReadyDate"/></s:text>
    <c:if test="${rolesAssignFlag!='Y'}">
    <td><s:textfield id="accrualReadyDate" cssClass="input-textUpper" name="serviceOrder.accrualReadyDate" value="${accrualReadyDate}" cssStyle="width:65px" maxlength="13"  readonly="true"/></td>
    </c:if>
    <c:if test="${rolesAssignFlag=='Y'}">
    <td><s:textfield id="accrualReadyDate" cssClass="input-textUpper" name="serviceOrder.accrualReadyDate" value="${accrualReadyDate}" cssStyle="width:65px" maxlength="13" onkeydown="return accrualDateDel(event,this)" /></td>
    </c:if>
    <c:if test="${rolesAssignFlag=='Y'}">
		<td><img id="accrualReadyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id);"/></td>
    </c:if>
    </c:if>
     <c:if test="${empty serviceOrder.accrualReadyDate}">
    <td><s:textfield cssClass="input-textUpper" id="accrualReadyDate" name="serviceOrder.accrualReadyDate" cssStyle="width:65px" maxlength="11" onkeydown="return accrualDateDel(event,this)"  readonly="true"/></td>
    <c:if test="${rolesAssignFlag=='Y'}">
    <td><img id="accrualReadyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id);"/></td>
    </c:if> 
    </c:if>
    <td align="right" class="listwhitetext" width="5px"></td>
    <td align="right" class="listwhitetext">Accrual Updated By</td>
    <td align="left"><s:textfield id="accrualReadyUpdatedBy" name="serviceOrder.accrualReadyUpdatedBy" cssClass="input-textUpper" cssStyle="width:65px"  readonly="true"/></td>
	<td align="right" class="listwhitetext" width="5px"></td>
    <td align="right" class="listwhitetext">Accrual Checked</td>
    <c:if test="${not empty serviceOrder.accrueChecked}">
    <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.accrueChecked"/></s:text>
    <td><s:textfield cssClass="input-textUpper" name="serviceOrder.accrueChecked" value="%{accountLineFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/></td>                    
    </c:if>
    <c:if test="${empty serviceOrder.accrueChecked}">
	<td ><s:textfield  cssClass="input-textUpper" id="accrueChecked" name="serviceOrder.accrueChecked" readonly="true" cssStyle="width:65px;" maxlength="11" /> </td>
	</c:if>
</tr>
</tbody>
</table>
</td>
</tr> 
<tr>
<td height="4px" align="left" colspan="18" style="!padding-bottom:4px;" class="vertlinedata"></td>
</tr>
</configByCorp:fieldVisibility>    

</tbody>
</table> 
</div>
<div class="bottom-header" style="margin-top:35px;!margin-top:44px;">
<span style="margin-left:50px; padding-top:5px">
<c:if test="${billing.cod}">
  <div style="position:absolute; width:95%; text-align:center; font-size:14px;padding-top:3px; font-family:Tahoma,Calibri,Verdana,Geneva,sans-serif; font-weight:bold; color:#fe0303;"> COD </div>
<img id="cod1" src="${pageContext.request.contextPath}/images/cod-blue.png" width="95%" />                              
</c:if>
</span></div></div> </div>
<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	<tr><td>
 				<div id="generateInv" class="message" style="display:none;width:300px;background:none;border:none;">
 				<table style="margin:0px;padding:0px;"><tr>
 				<td>
				<font color="red" style="text-align:center;width:100%;font:12px arial,verdana;"><b>
					<script type="text/javascript"> 
					document.forms['pricingListForm'].elements['generateMassage'].value = getCookie('generateMassage');
					if(document.forms['pricingListForm'].elements['generateMassage'].value == "invoiceMassage"){
						document.getElementById('generateInv').style.display = 'block' 
						document.write("Invoice has been Generated successfully.");
						document.forms['pricingListForm'].elements['accUpdateAjax'].value="T";
						document.forms['pricingListForm'].elements['generateMassage'].value="";
					}
					   document.cookie = 'generateMassage' + "=; expires=" +  new Date().toGMTString();
					</script>
					</b></font></td></tr></table>
				</div>
			</td>
	</tr>
</sec-auth:authComponent>

<div id="para1" >
	<jsp:include flush="true" page="accountLineListForm.jsp"></jsp:include>	
	</div>
</s:form>

<jsp:include flush="true" page="accountLineListViewJs.jsp"></jsp:include>
<jsp:include flush="true" page="accountLineListViewJsSecond.jsp"></jsp:include>
<jsp:include flush="true" page="accountLineListViewPaginationJs.jsp"></jsp:include>
<script type="text/javascript">
	setOnSelectBasedMethods(["checkAccrualReadyValidationOnDate();"]);
	setCalendarFunctionality();
   	setTimeout(function() {
	    $('#generateInv').hide();
	},15000);
</script>
<script type="text/javascript">

$(document).ready(
		function (){
			var $j = jQuery.noConflict();
			$j(".tablesorter")
				 .collapsible("td.collapsible", {
					collapse: true
				})
				 .tablesorter({
				// set default sort column
				/* sortList: [[2,0]],
				// don't sort by first column
				headers: {0: {sorter: false}}
				// set the widgets being used - zebra stripping
				, widgets: ['zebra']
				, onRenderHeader: function (){
					this.wrapInner("<span></span>");
				}
				, debug: false */
			})
			.tablesorterPager({container: $("#pager"), positionFixed: false});
		});
</script>
