package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.EquipMaterialsCostDao;
import com.trilasoft.app.model.EquipMaterialsCost;


public class EquipMaterialsCostDaoHibernate extends GenericDaoHibernate<EquipMaterialsCost, Long> implements EquipMaterialsCostDao {
	public EquipMaterialsCostDaoHibernate(){   
        super(EquipMaterialsCost.class);   
    }  
	public EquipMaterialsCost getCostById(long id){
		EquipMaterialsCost cost=null;
		List list=getHibernateTemplate().find("from EquipMaterialsCost where id="+id);
		if(list!=null && !(list.isEmpty()) && (list.get(0)!=null)){
			 cost=(EquipMaterialsCost)list.get(0);
		}else{
			cost=new EquipMaterialsCost();
		}
		return cost;
	}
    public EquipMaterialsCost getByMaterailId(long id){
		EquipMaterialsCost cost=null;
		List list=getHibernateTemplate().find("from EquipMaterialsCost where equipMaterialsId="+id);
		if(list!=null && !(list.isEmpty()) && (list.get(0)!=null)){
			 cost=(EquipMaterialsCost)list.get(0);
		}else{
			cost=new EquipMaterialsCost();
		}
		return cost;
	}
  
}
