package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.CreditCardDao;
import com.trilasoft.app.model.CreditCard;
import com.trilasoft.app.service.CreditCardManager;

public class CreditCardManagerImpl extends GenericManagerImpl<CreditCard, Long> implements CreditCardManager  {

	CreditCardDao creditCardDao;   
	  
    public CreditCardManagerImpl(CreditCardDao creditCardDao) {   
        super(creditCardDao);   
        this.creditCardDao = creditCardDao;   
    }   
    public List findMaxId() {
    	
		return creditCardDao.findMaxId();
	} 
    public List findByShipNumber(String shipNumber)
    {
    	return creditCardDao.findByShipNumber(shipNumber);
    }
	public List getPrimaryFlagStatus(String shipNumber, String sessionCorpID) {
		
		return creditCardDao.getPrimaryFlagStatus(shipNumber, sessionCorpID);
	}
	public List getCreditCardMasking(String number) {
	
		return creditCardDao.getCreditCardMasking(number);
	}
	public List getCreditcardNumber(Long id) {
		return creditCardDao.getCreditcardNumber(id);
	}
	public List getServiceOrder(String shipNumber, String sessionCorpID) {
		return creditCardDao.getServiceOrder(shipNumber, sessionCorpID);
	}
    
}
