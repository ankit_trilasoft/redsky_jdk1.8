package com.trilasoft.app.dao;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.model.User;

import com.trilasoft.app.model.PasswordOTP;

public interface PasswordOTPDao extends GenericDao<PasswordOTP, Long> {
		
		public List checkById(Long id);
		public List<User> loadUserByUsername(String username) ;
		public List checkUserOTP(Long id, String verificationCode);
}
