package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;  
import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import org.hibernate.Hibernate;
import com.trilasoft.app.model.Storage;   
import com.trilasoft.app.dao.StorageDao;   
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
  
public class StorageDaoHibernate extends GenericDaoHibernate<Storage, Long> implements StorageDao {   
	private HibernateUtil hibernateUtil;
    private List storages;

	public StorageDaoHibernate() {   
        super(Storage.class);   
    } 
	
    public List findMaximum() {
    	List list=new ArrayList();
    	list=this.getSession().createSQLQuery("select MAX(idNum) from storage").list();
        return list;
    }
  
    public List<Storage> findByItemNumber(String itemNumber) {   
        return getHibernateTemplate().find("from Storage where itemNumber=?", itemNumber);   
    } 
    
    

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public List<Storage> findByItemNumber(String itemNumber, String jobNumber, String locationId, Long ticket) {
    	
    	if(ticket == null || ticket.toString().equalsIgnoreCase("")){
    		storages = getHibernateTemplate().find("from Storage where itemNumber like '" + itemNumber.replaceAll("'", "''").replaceAll(":", "''") + "%' AND jobNumber like '" + jobNumber.replaceAll("'", "''").replaceAll(":", "''")+"%' AND locationId like '" + locationId.replaceAll("'", "''").replaceAll(":", "''")+"%' " );
    	}else{
    		storages = getHibernateTemplate().find("from Storage where itemNumber like '" + itemNumber.replaceAll("'", "''").replaceAll(":", "''") + "%' AND jobNumber like '" + jobNumber.replaceAll("'", "''").replaceAll(":", "''")+"%' AND locationId like '" + locationId.replaceAll("'", "''").replaceAll(":", "''")+"%' AND ticket =?",ticket);
    	}
    	return storages;
    }
	public List<Storage> findByJobNumber(String jobNumber) {
		return getHibernateTemplate().find("from Storage where jobNumber=?", jobNumber);   
	}
	
	public List<Storage> findByLocation(String locationId) {
		return getHibernateTemplate().find("from Storage where locationId=?", locationId);
	}
	
	public List<Storage> findByLocationRearrange(String storageId,String locationId) {
		return getHibernateTemplate().find("from Storage where locationId='"+locationId+"' and storageId='"+storageId+"' and storageId is not null and storageId!='' and locationId is not null and locationId!='' and releaseDate is null ");
	}
	
	public List<Storage> findByMeasQuantity(Double measQuantity) {
		return getHibernateTemplate().find("from Storage where measQuantity=?", measQuantity);
	}
	
	public List getByIdNum(Long idNum){
    	return getHibernateTemplate().find("from BookStorage where idNum=?", idNum);
    }
	
	public List getTraceStorageList(){
    	List list= getHibernateTemplate().find("SELECT s.id, s.locationId, s.itemNumber, s.jobNumber, s.releaseDate, s.ticket, s.toRelease, s.description, s.containerId, s.itemTag, s.pieces, s.price, w.firstName, w.lastName FROM Storage s , WorkTicket w where s.ticket=w.ticket");
    	List storages= new ArrayList();
    	StroageDTO stroageDTO;
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			stroageDTO=new StroageDTO();
			stroageDTO.setId(obj[0]);
			stroageDTO.setLocationId(obj[1]);
			stroageDTO.setItemNumber(obj[2]);
			stroageDTO.setJobNumber(obj[3]);
			stroageDTO.setReleaseDate(obj[4]);
			stroageDTO.setTicket(obj[5]);
			stroageDTO.setToRelease(obj[6]);
			stroageDTO.setDescription(obj[7]);
			stroageDTO.setContainerId(obj[8]);
			stroageDTO.setItemTag(obj[9]);
			stroageDTO.setPieces(obj[10]);
			stroageDTO.setPrice(obj[11]);
			stroageDTO.setFirstname(obj[12]);
			stroageDTO.setLastName(obj[13]);
			storages.add(stroageDTO);
		}
		return storages;
    }
	
	public class StroageDTO
	{
		private Object id; 
		private Object locationId;
		private Object itemNumber;
		private Object shipNumber;
		private Object releaseDate;
		private Object ticket;
		private Object toRelease;
		private Object description;
		private Object containerId;
		private Object itemTag;
		private Object pieces;
		private Object price;
		private Object firstname;
		private Object lastName;
		private Object jobNumber;
		private Object storageId;
		private Object measQuantity;
		private Object unit;
		private Object volume;
		private Object volUnit;
		private Object warehouse;
		public Object getContainerId() {
			return containerId;
		}
		public void setContainerId(Object containerId) {
			this.containerId = containerId;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getFirstname() {
			return firstname;
		}
		public void setFirstname(Object firstname) {
			this.firstname = firstname;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getItemNumber() {
			return itemNumber;
		}
		public void setItemNumber(Object itemNumber) {
			this.itemNumber = itemNumber;
		}
		public Object getItemTag() {
			return itemTag;
		}
		public void setItemTag(Object itemTag) {
			this.itemTag = itemTag;
		}
		public Object getJobNumber() {
			return jobNumber;
		}
		public void setJobNumber(Object jobNumber) {
			this.jobNumber = jobNumber;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getLocationId() {
			return locationId;
		}
		public void setLocationId(Object locationId) {
			this.locationId = locationId;
		}
		public Object getPieces() {
			return pieces;
		}
		public void setPieces(Object pieces) {
			this.pieces = pieces;
		}
		public Object getPrice() {
			return price;
		}
		public void setPrice(Object price) {
			this.price = price;
		}
		public Object getReleaseDate() {
			return releaseDate;
		}
		public void setReleaseDate(Object releaseDate) {
			this.releaseDate = releaseDate;
		}
		public Object getTicket() {
			return ticket;
		}
		public void setTicket(Object ticket) {
			this.ticket = ticket;
		}
		public Object getToRelease() {
			return toRelease;
		}
		public void setToRelease(Object toRelease) {
			this.toRelease = toRelease;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getStorageId() {
			return storageId;
		}
		public void setStorageId(Object storageId) {
			this.storageId = storageId;
		}
		public Object getMeasQuantity() {
			return measQuantity;
		}
		public void setMeasQuantity(Object measQuantity) {
			this.measQuantity = measQuantity;
		}
		public Object getUnit() {
			return unit;
		}
		public void setUnit(Object unit) {
			this.unit = unit;
		}
		public Object getVolume() {
			return volume;
		}
		public void setVolume(Object volume) {
			this.volume = volume;
		}
		public Object getVolUnit() {
			return volUnit;
		}
		public void setVolUnit(Object volUnit) {
			this.volUnit = volUnit;
		}
		public Object getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(Object warehouse) {
			this.warehouse = warehouse;
		}
	}
	public List<Storage> search(String itemNumber, String jobNumber, String locationId, String storageId, Long ticket, String firstName, String lastName, String tag, String corpID) {
		
		List list= new ArrayList();
		if(ticket == null || ticket.toString().equalsIgnoreCase("")){		
			list = this.getSession().createSQLQuery("SELECT s.id, s.locationId, s.itemNumber, s.shipNumber, s.releaseDate, s.ticket, s.toRelease, replace(s.description,'\r\t','') , s.containerId, s.itemTag, s.pieces, s.price, w.firstName, w.lastName, s.storageId, concat(s.volume,' ',s.volUnit) as volume FROM storage s , workticket w WHERE s.ticket=w.ticket AND w.firstName like '%" + firstName.replaceAll("'", "''").replaceAll(":", "''").trim()+"%' AND w.lastName like  '%" + lastName.replaceAll("'", "''").replaceAll(":", "''").trim()+"%' AND s.locationId like  '%" + locationId.replaceAll("'", "''").trim()+"%' AND s.storageId like  '%" + storageId.replaceAll("'", "''").trim()+"%' AND s.shipNumber like  '%" + jobNumber.replaceAll("'", "''").replaceAll(":", "''").trim()+"%' AND s.description like  '%" + itemNumber.replaceAll("'", "''").replaceAll(":", "''").trim() + "%' AND s.itemTag like  '%" + tag.replaceAll("'", "''").replaceAll(":", "''").trim() + "%' AND s.corpID ='"+corpID+"' AND s.corpID = w.corpID").list();
		}else{
			list = this.getSession().createSQLQuery("SELECT s.id, s.locationId, s.itemNumber, s.shipNumber, s.releaseDate, s.ticket, s.toRelease, replace(s.description,'\r\t','') , s.containerId, s.itemTag, s.pieces, s.price, w.firstName, w.lastName, s.storageId, concat(s.volume,' ',s.volUnit) as volume FROM storage s , workticket w WHERE s.ticket=w.ticket AND w.firstName like '%" + firstName.replaceAll("'", "''").replaceAll(":", "''").trim()+"%' AND w.lastName like  '%" + lastName.replaceAll("'", "''").replaceAll(":", "''").trim()+"%' AND s.locationId like  '%" + locationId.replaceAll("'", "''").trim()+"%' AND s.storageId like  '%" + storageId.replaceAll("'", "''").trim()+"%' AND s.shipNumber like  '%" + jobNumber.replaceAll("'", "''").replaceAll(":", "''").trim()+"%' AND s.description like  '%" + itemNumber.replaceAll("'", "''").replaceAll(":", "''").trim() + "%' AND s.itemTag like  '%" + tag.replaceAll("'", "''").replaceAll(":", "''").trim() + "%' AND s.ticket like '%"+ticket+"%' AND s.corpID ='"+corpID+"' AND s.corpID = w.corpID").list();
		}
		List storages= new ArrayList();
		StroageDTO stroageDTO;
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			stroageDTO=new StroageDTO();
			stroageDTO.setId(obj[0]);
			stroageDTO.setLocationId(obj[1]);
			stroageDTO.setItemNumber(obj[2]);
			stroageDTO.setShipNumber(obj[3]);
			stroageDTO.setReleaseDate(obj[4]);
			stroageDTO.setTicket(obj[5]);
			stroageDTO.setToRelease(obj[6]);
			stroageDTO.setDescription(obj[7]);
			stroageDTO.setContainerId(obj[8]);
			stroageDTO.setItemTag(obj[9]);
			stroageDTO.setPieces(obj[10]);
			stroageDTO.setPrice(obj[11]);
			stroageDTO.setFirstname(obj[12]);
			stroageDTO.setLastName(obj[13]);
			stroageDTO.setStorageId(obj[14]);
			stroageDTO.setVolume(obj[15]);
			storages.add(stroageDTO);
		}
		return storages;
	}
	public List findStorageByShipNumber(String shipNumber){
		///List list= getHibernateTemplate().find("SELECT s.id, s.locationId, s.itemNumber, s.jobNumber, s.releaseDate, s.ticket, s.toRelease, s.description, s.containerId, s.itemTag, s.pieces, s.price, w.firstName, w.lastName FROM Storage s , WorkTicket w where s.ticket=w.ticket");
		///List list= getHibernateTemplate().find("select s.id, s.ticket, s.description, s.locationId, s.pieces, s.releaseDate, s.containerid, s.itemNumber from storage as s,workticket as w where s.ticket=w.ticket and w.shipNumber=?",shipNumber);
		List list= getHibernateTemplate().find("select w.id, s.ticket, s.description, s.locationId, s.pieces, s.releaseDate, s.containerId, s.itemNumber, s.storageId,s.measQuantity, s.unit, s.volume, s.volUnit, s.warehouse from Storage as s, WorkTicket as w where s.ticket=w.ticket and w.shipNumber='"+shipNumber+"' order by s.releaseDate");
		List storages= new ArrayList();
    	StroageDTO stroageDTO;
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			stroageDTO=new StroageDTO();
			stroageDTO.setId(obj[0]);
			stroageDTO.setTicket(obj[1]);
			stroageDTO.setDescription(obj[2]);
			stroageDTO.setLocationId(obj[3]);
			stroageDTO.setPieces(obj[4]);
			stroageDTO.setReleaseDate(obj[5]);
			stroageDTO.setContainerId(obj[6]);
			stroageDTO.setItemNumber(obj[7]);
			stroageDTO.setStorageId(obj[8]);
			stroageDTO.setMeasQuantity(obj[9]);
			stroageDTO.setUnit(obj[10]);
			stroageDTO.setVolume(obj[11]);
			stroageDTO.setVolUnit(obj[12]);
			stroageDTO.setWarehouse(obj[13]);
			storages.add(stroageDTO);
		}
		return storages;
		
	}

	public List storageExtract(String corpId,String jobSTLFlag ,String salesPortalStorageReport) {
		//List storageExtractList = new ArrayList();
		//String query="select s.shipnumber 'Shipnumber', concat(s.firstname, ' ',s.lastname)  'Shipper Name', t.originagent 'Origin Services Perfomed by', po.client5 'Origin Services Agents OMNI Member No', t.destinationagent 'Destination Services performed By', pd.client5 'Destination Services  Agents OMNI Member No.', m.actualnetweight 'Net Weight (in Pounds)',    	'1' as'No of Shipmnets',    	if(s.mode = 'Air','A~','S~') Mode    	from serviceorder s , trackingstatus t, partner po, partner pd, miscellaneous m    	where s.id = t.id    	and s.id   = m.id    	and s.job in ( 'INT','GST')    	and s.bookingagentcode in (select bookingagentcode from companydivision where corpID = '"+corpId+"' )   and t.omnireport is null   and s.commodity <> 'AUTO' and t.beginload between '"+beginDates+"' and '"+endDates+"'	and s.status not in ('CNCL','DWNLD','CANCEL')    	and s.createdon >= '2007-01-01'    	and t.originagentcode = po.partnercode    	and t.destinationagentcode = pd.partnercode    	and (po.client5  <> '' or pd.client5 <> '') and s.corpID = '"+corpId+"' ";
    	//String query="select s.shipnumber, concat(s.firstname, ' ',s.lastname), t.originagent, po.client5, t.destinationagent, pd.client5, m.actualnetweight, '1', if(s.mode = 'Air','A~','S~') from serviceorder s , trackingstatus t, partner po, partner pd, miscellaneous m	where s.id = t.id and s.id = m.id and s.job in ( 'INT','GST') and s.bookingagentcode in (select bookingagentcode from companydivision where corpID = '"+corpId+"' ) and t.omnireport is null and s.commodity <> 'AUTO' and t.beginload between '"+beginDates+"' and '"+endDates+"'	and s.status not in ('CNCL','DWNLD','CANCEL') and s.createdon >= '2007-01-01' and t.originagentcode = po.partnercode and t.destinationagentcode = pd.partnercode and (po.client5  <> '' or pd.client5 <> '') and s.corpID = '"+corpId+"' ";
		String query="select s.shipNumber 'ShipNumber', ";
				query=query+ "s.firstName as 'First Name', ";
				query=query+ "s.lastName as 'Last Name', ";
				query=query+ "if(b.billToCode=' ','',concat_ws('','`',b.billToCode)) 'Bill To', ";
				query=query+ "b.billToName 'Bill To Name', ";
				query=query+ "b.locationAgentCode 'LocationAgentCode',";
				query=query+ "b.locationAgentName 'LocationAgentName',";
				query=query+ "b.billToAuthority 'PO #', ";
				query=query+ "b.billToReference 'Ref #', ";
				query=query+ "CAST(DATE_FORMAT(b.taExpires,'%d-%b-%Y') AS CHAR) 'PO Auth Expires', ";
				query=query+ "s.registrationNumber 'Reg.Number', ";
				query=query+ "s.commodity 'Commodity', ";
				query=query+ "m.actualNetWeight 'ActualWeight', ";
				query=query+ "CAST(DATE_FORMAT(t.beginLoad,'%d-%b-%Y') AS CHAR) 'Load (T)', ";
				query=query+ "CAST(DATE_FORMAT(t.loadA,'%d-%b-%Y') AS CHAR) 'Load (A)', ";
				query=query+ "CAST(DATE_FORMAT(t.deliveryA,'%d-%b-%Y') AS CHAR) 'DeliveryA', ";
				query=query+ "CAST(DATE_FORMAT(b.storageOut,'%d-%b-%Y') AS CHAR) 'Storage Out Date', ";
				query=query+ "b.insuranceValueEntitle 'Insurance Entitlement', ";
				query=query+ "CAST(DATE_FORMAT(b.billThrough,'%d-%b-%Y') AS CHAR) 'Billed Through', ";
				query=query+ "if(b.insuranceValueActual is null || b.insuranceValueActual='','',b.currency) 'Currency', ";
				query=query+ "b.insuranceValueActual 'Insval_Ac', ";
				query=query+ "b.onHand 'OnHand', ";
				query=query+ "b.storagePerMonth 'StoPerMonth', ";
				query=query+ "b.insurancePerMonth 'InsPerMonth', ";
				query=query+ "t.originAgent 'OA Name', ";
				query=query+ "s.bookingAgentName 'BA Name', ";
				query=query+ "p.terminalAddress1 'OA Address1', ";
				query=query+ "p.terminalCity 'OA City', ";
				query=query+ "p.terminalState 'OA State',";
				query=query+ "p.terminalZip 'OA Zip', ";
				query=query+ "p.terminalCountry 'OA Country', ";
				query=query+ "p.terminalPhone 'OA Phone', ";
				query=query+ "t.destinationAgent 'DA Name',";
				query=query+ "ppp.terminalAddress1 'DA Address1',";
				query=query+ "ppp.terminalCity 'DA City',";
				query=query+ "ppp.terminalState 'DA State',";
				query=query+ "ppp.terminalZip 'DA Zip',";
				query=query+ "ppp.terminalCountry 'DA Country',";
				query=query+ "ppp.terminalPhone 'DA Phone',";
				query=query+ "s.job 'Job', ";
				query=query+ "s.companyDivision 'CompanyDivision', ";
				query=query+ "r.description 'Warehouse',";
				query=query+ "b.personBilling 'Person Billing',";
				query=query+ "s.coordinator 'Coordinator',";
				query=query+ "t.originSubAgentCode  'OriginSubAgentCode',";
				query=query+ "t.originSubAgent  'OriginSubAgentName',";
				query=query+ "t.destinationSubAgentCode 'Sub DA Code',";
				query=query+ "t.destinationSubAgent 'Sub DA Name',";
				query=query+ "(select group_concat(st.locationid order by st.locationid asc separator ', ') from storage st ";
				query=query+ "where st.shipnumber=s.shipnumber and st.releaseDate is null) as 'Location',";
				query=query+ "if(b.noCharge,'Yes','No') as noCharge,";
				query=query+ "c.charge as 'Charge',";
				query=query+ "c.description as 'Description',";
				query=query+ "b.vendorName1 'VendorName',";
				query=query+ "b.postGrate 'PostGrate',";
				query=query+ "c1.accountName 'AccountName',";
				query=query+ "b.storageEmail 'Storage Email Address',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingAddress1,'\n', ' '), '\r', ' '), '\t', ' ')) 'MaiAddress1',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingAddress2,'\n', ' '), '\r', ' '), '\t', ' ')) 'MailAddress2',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingAddress3,'\n', ' '), '\r', ' '), '\t', ' ')) 'Mailingaddress3',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingCity,'\n', ' '), '\r', ' '), '\t', ' ')) 'City',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingState,'\n', ' '), '\r', ' '), '\t', ' ')) 'State',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingCountry,'\n', ' '), '\r', ' '), '\t', ' ')) 'Country',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingZip,'\n', ' '), '\r', ' '), '\t', ' ')) 'Zip',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingEmail,'\n', ' '), '\r', ' '), '\t', ' ')) 'MailEmailaddress',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.mailingPhone,'\n', ' '), '\r', ' '), '\t', ' ')) 'MailPhonenumbers',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingAddress1,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingaddress1',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingAddress2,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingaddress2',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingAddress3,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingaddress3',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingCity,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingCity',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingState,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingState',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingCountry,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingCountry',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingZip,'\n', ' '), '\r', ' '), '\t', ' ')) 'billingZip',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingEmail,'\n', ' '), '\r', ' '), '\t', ' ')) 'Billing Email address',";
				query=query+ "TRIM(REPLACE(REPLACE(REPLACE(pp.billingPhone,'\n', ' '), '\r', ' '), '\t', ' ')) 'Billing Phone numbers' ";
				query=query+ "from serviceorder s ";
				query=query+ "inner join ";
				query=query+ "billing b on s.id=b.id and b.corpId = '"+ corpId+ "' ";
				query=query+ "inner join customerfile c1  ON c1.id = s.customerfileid and c1.corpid = '" + corpId+ "'  ";
				query=query+ "inner join trackingstatus t ON s.id=t.id and t.corpId = '"+ corpId+ "'  ";
				query=query+ "inner join miscellaneous m on s.id=m.id and m.corpId = '"+ corpId+ "' ";
				query=query+ "left outer join partner p ON t.originAgentcode = p.partnerCode and p.corpId in ('"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId+ "') ";
				query=query+ "left outer join partner pp ON b.billToCode = pp.partnerCode and pp.corpId in ('"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId+ "')  ";
				query=query+ "left outer join partner ppp ON t.destinationAgentCode = ppp.partnerCode and ppp.corpId in ('"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId+ "')";
				query=query+ "left outer join refmaster r ON b.warehouse = r.code and parameter = 'HOUSE' and r.corpId in ('"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId + "') ";
				query=query+ " left outer join charges c on b.contract=c.contract and b.charge=c.charge and  c.corpID='"+corpId+"' where ";
				if(jobSTLFlag.equalsIgnoreCase("No")){
					query = query + " s.job IN('STO','STF','TPS','OIS','SCS') ";
				}else if(!salesPortalStorageReport.equals("")){
					query = query + " "+salesPortalStorageReport+" ";
				}else{
					query = query + " s.job IN('STO','STF','STL','TPS','OIS') ";
				}
				query = query + "and s.status not in ('CNCL','CLSD') and b.storageOut is null and s.corpId = '" +corpId+"' and s.controlFlag='C' and s.moveType='BookedMove'  group by s.shipnumber order by b.billtocode ,s.firstName";
		return this.getSession().createSQLQuery(query)
		.addScalar("ShipNumber", Hibernate.STRING)
		.addScalar("First Name", Hibernate.STRING)
		.addScalar("Last Name", Hibernate.STRING)
		.addScalar("Bill To", Hibernate.STRING)
		.addScalar("Bill To Name", Hibernate.STRING)
		.addScalar("LocationAgentCode", Hibernate.STRING)
		.addScalar("LocationAgentName", Hibernate.STRING)
		.addScalar("PO #", Hibernate.STRING)
		.addScalar("Ref #", Hibernate.STRING)
		.addScalar("PO Auth Expires", Hibernate.STRING)
		.addScalar("Reg.Number", Hibernate.STRING)
		.addScalar("Commodity", Hibernate.STRING)
		.addScalar("ActualWeight", Hibernate.BIG_DECIMAL)
		.addScalar("Load (T)", Hibernate.STRING)
		.addScalar("Load (A)", Hibernate.STRING)
		.addScalar("DeliveryA", Hibernate.STRING)
		.addScalar("Storage Out Date", Hibernate.STRING)
		.addScalar("Insurance Entitlement", Hibernate.BIG_DECIMAL)
		.addScalar("Billed Through", Hibernate.STRING) 
		.addScalar("Currency", Hibernate.STRING)
		.addScalar("Insval_Ac", Hibernate.BIG_DECIMAL)
		.addScalar("OnHand", Hibernate.BIG_DECIMAL)
		.addScalar("StoPerMonth", Hibernate.BIG_DECIMAL)
		.addScalar("InsPerMonth", Hibernate.BIG_DECIMAL)
		.addScalar("OA Name", Hibernate.STRING)
		.addScalar("BA Name", Hibernate.STRING)
		.addScalar("OA Address1", Hibernate.STRING)
		.addScalar("OA City", Hibernate.STRING)
		.addScalar("OA State", Hibernate.STRING)
		.addScalar("OA Zip", Hibernate.STRING)
		.addScalar("OA Country", Hibernate.STRING)
		.addScalar("OA Phone", Hibernate.STRING)
		.addScalar("DA Name", Hibernate.STRING)
		.addScalar("DA Address1", Hibernate.STRING)
		.addScalar("DA City", Hibernate.STRING)
		.addScalar("DA State", Hibernate.STRING)
		.addScalar("DA Zip", Hibernate.STRING)
		.addScalar("DA Country", Hibernate.STRING)
		.addScalar("DA Phone", Hibernate.STRING)
		.addScalar("Job", Hibernate.STRING)
		.addScalar("CompanyDivision", Hibernate.STRING)		
		.addScalar("Warehouse", Hibernate.TEXT)
		.addScalar("Person Billing", Hibernate.STRING)
		.addScalar("Coordinator", Hibernate.STRING)		
		.addScalar("OriginSubAgentCode", Hibernate.STRING)
		.addScalar("OriginSubAgentName", Hibernate.STRING)
		.addScalar("Sub DA Code", Hibernate.STRING)
		.addScalar("Sub DA Name", Hibernate.STRING)
		.addScalar("Location", Hibernate.STRING)
		.addScalar("noCharge", Hibernate.STRING)
		.addScalar("Charge",Hibernate.STRING)
		.addScalar("Description",Hibernate.STRING)
		.addScalar("VendorName", Hibernate.STRING)
		.addScalar("postGrate",Hibernate.BIG_DECIMAL)
		.addScalar("AccountName",Hibernate.STRING)
		.addScalar("Storage Email Address",Hibernate.STRING)
		.addScalar("MaiAddress1",Hibernate.STRING)
		.addScalar("MailAddress2",Hibernate.STRING)
		.addScalar("Mailingaddress3",Hibernate.STRING)
		.addScalar("City",Hibernate.STRING)
		.addScalar("State",Hibernate.STRING)
		.addScalar("Country",Hibernate.STRING)
		.addScalar("Zip",Hibernate.STRING)
		.addScalar("MailEmailaddress",Hibernate.STRING)
		.addScalar("MailPhonenumbers",Hibernate.STRING)
		.addScalar("billingaddress1",Hibernate.STRING)
		.addScalar("billingaddress2",Hibernate.STRING)
		.addScalar("billingaddress3",Hibernate.STRING)
		.addScalar("billingCity",Hibernate.STRING)
		.addScalar("billingState",Hibernate.STRING)
		.addScalar("billingCountry",Hibernate.STRING)
		.addScalar("billingZip",Hibernate.STRING)
		.addScalar("Billing Email address",Hibernate.STRING)
		.addScalar("Billing Phone numbers",Hibernate.STRING)
		.list();
		//String query="select s.shipNumber, CONCAT_WS(', 's.lastName,NULL,s.firstName), b.billToCode, b.billToReference, b.billToAuthority, s.registrationNumber, s.commodity, b.billToAuthority,b.billThrough,b.insuranceValueActual, b.onHand,b.storagePerMonth,b.taExpires,t.beginLoad from serviceorder s,billing b,trackingstatus t,miscellaneous m where s.id=b.id and s.id=t.id and s.id=m.id and s.job IN('STO','STF','STL','TPS')and b.storageOut is null and t.deliveryA is not null";
		//storageExtractList = this.getSession().createSQLQuery(query).list();	
    	//System.out.println("\n\nQUERY::"+query);
		//return storageExtractList;
	}

	public List findLocationByStorage(String storageId, String sessionCorpID) {
		return getHibernateTemplate().find("select locationId from Storage where storageId='"+storageId+"' and corpId='"+sessionCorpID+"' and releaseDate is null");
	}

	public List storageLibraryVolumeList(String storageId) {
		return getHibernateTemplate().find("from StorageLibrary where storageId='"+storageId+"'");
	}
	public List dummyStorageList(String storageId,String corpId){
		return this.getSession().createSQLQuery("select * from storage where storageId='"+storageId+"' and corpID ='"+corpId+"' ").list();
	}
	public void deleteDummyUsedRecord(String storageId, String sessionCorpID){
		getHibernateTemplate().bulkUpdate("delete from Storage where storageId='"+storageId+"' and ticket=0 ");
	}
	public List handOutList(String ticket, String sessionCorpID){
		return getHibernateTemplate().find("from Storage where hoTicket='"+ticket+"' and handoutStatus='A' and releaseDate is null");
	}
	public void updateStorageForLocation(String sessionCorpID,String storageId,String locationId){
		getHibernateTemplate().bulkUpdate("update Storage set locationId ='"+locationId+"' where storageId='"+storageId+"' and corpId='"+sessionCorpID+"' ");
	}
	 public List getRecordsForBookSto(String sessionCorpID, String storageId,Long ticket){
		 return getHibernateTemplate().find("select id from Storage where storageId='"+storageId+"' and ticket!='"+ticket+"' and corpId='"+sessionCorpID+"'");
		 
	 }
	 public List getLocationId(String storageId){
		 return getHibernateTemplate().find("select distinct locationId from Storage where storageId='"+storageId+"'");
		 
	 }
	 
	 public List<Storage> findByLocationInternalRearrange(String storageId,String locationId, Long ticket, String mode) {
			if(mode.equalsIgnoreCase("5")){
				
				return getHibernateTemplate().find("from Storage where locationId='"+locationId+"' and storageId='"+storageId+"' and storageId is not null and storageId!='' and locationId is not null and locationId!='' and releaseDate is null and ticket='"+ticket+"'");
			}
			else{
			return getHibernateTemplate().find("from Storage where locationId='"+locationId+"' and storageId='"+storageId+"' and storageId is not null and storageId!='' and locationId is not null and locationId!='' and releaseDate is null  ");
			}
		}
	 
	 public List<Storage> findStoragesByLocation(String locationId) {
			return getHibernateTemplate().find("from Storage where locationId=? and releaseDate is null", locationId);
		}
}  
