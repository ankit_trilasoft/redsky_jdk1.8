<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<title>Ikea Order Detail</title>
<meta name="heading"
	content="Ikea Order Detail" />

<style type="text/css">	
.OverFlowPr { overflow-x:auto;overflow-y:auto; }
.upper-case { text-transform:capitalize; }
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
	<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>

</head> 

<s:form id="customerFileForm" name="customerFileForm" validate="true" >
        <s:hidden name="customerFile.comptetive"/>
        
		<s:hidden name="customerFile.id" value="%{customerFile.id}" /> 
		<s:hidden name="id" value="<%=request.getParameter("id") %>" /> 
		<s:hidden name="ikeaOrderForDaysClick" id="ikeaOrderForDaysClick"/>
		<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	    <s:hidden name="customerFile.parentAgent" />
	    <s:hidden name="invoiceEmailAddress" />
	    <s:hidden name="contactPerson" />
	    <s:hidden name="customerFile.orderAction"  value="IO"/>
	    <s:hidden name="customerFile.companyDivision" />
	    <s:hidden name="customerFile.portalIdActive" />
	    <s:hidden name="customerFile.coordinator" />
	    <s:hidden  key="customerFile.customerEmployer" />
	    <s:hidden id="billPayMethod"  name="customerFile.billPayMethod" />
	    <s:hidden  name="customerFile.source"  /> 
		<s:hidden name="customerFile.accountCode"  /> 
		<s:hidden name="customerFile.accountName" />
		<s:hidden name="customerFile.status" />  
	    <s:hidden name="customerFile.statusNumber" />
	    <s:hidden name="customerFile.surveyTime" /> 
		<s:hidden name="customerFile.surveyTime2" />
		<s:hidden name="customerFile.prefSurveyTime1" /> 
		<s:hidden name="customerFile.prefSurveyTime2" />
		<s:hidden name="customerFile.prefSurveyTime3" /> 
		<s:hidden name="customerFile.prefSurveyTime4" />
	    <s:hidden name="customerFile.quotationStatus" />
	    <s:hidden name="customerFile.personPricing"   /> 
	  	<s:hidden name="customerFile.personBilling" /> 
	  	<s:hidden name="customerFile.personPayable"  /> 
	  	<s:hidden name="customerFile.auditor" />
	  	<s:hidden name="customerFile.sequenceNumber" /> 
	  	<s:hidden name="customerFile.entitled" />
	    <s:hidden name="customerFile.orderIntiationStatus"  />
	    <s:hidden name="customerFile.controlFlag" />
	     
	    <s:hidden name="customerFile.originCityCode" />
		<s:hidden name="customerFile.originCountryCode" /> 
		<s:hidden name="customerFile.destinationCityCode" /> 
		<s:hidden name="customerFile.destinationCountryCode" />
		<s:hidden name="customerFile.homeCountryCode" />
		<s:hidden name="benefitTypeCheckedVal" id="benefitTypeCheckedVal"/>
		<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
		<s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		<s:hidden name="customerFile.firstInternationalMove" />
		 <s:hidden name="customerFile.custPartnerLastName" />
		 <s:hidden name="customerFile.custPartnerFirstName" />
		 <s:hidden name="customerFile.custPartnerPrefix" />
		 <s:hidden name="customerFile.custPartnerContact" />
      	<c:if test="${not empty customerFile.statusDate}">
        <s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
        <s:hidden  name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" />
      	</c:if>
      	<s:hidden  name="customerFile.contract" value="${customerFile.contract}" />
      	<c:if test="${empty customerFile.statusDate}">
      	<s:hidden   name="customerFile.statusDate"  />
      	</c:if>
		<c:if test="${not empty customerFile.systemDate}">
		<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate"/></s:text>
	    <s:hidden id="dateSystemDate" name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" />
	  	</c:if>
	  	<c:if test="${empty customerFile.systemDate}">
		<s:hidden id="dateSystemDate" name="customerFile.systemDate" /> 
	  	</c:if>
	  	<c:if test="${not empty customerFile.bookingDate}">
		<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
		<s:hidden id="bookingDate" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" />
		</c:if>
	    <c:if test="${empty customerFile.bookingDate}">
	    <s:hidden id="bookingDate"  name="customerFile.bookingDate" />
	    </c:if>

     	<s:hidden name="secondDescription" />
     	<s:hidden name="thirdDescription" />
     	<s:hidden name="fourthDescription" />
     	<s:hidden name="fifthDescription" />
     	<s:hidden name="sixthDescription" />

	  <c:if test="${not empty customerFile.id}">
		<div id="newmnav">
		  <ul> 
		    <li id="newmnav1" style="background:#FFF "><a href="editAsmlOrder.html?id=${customerFile.id}" class="current"><span>Order Detail</span></a></li>
		    <li><a href="accountPortalFiles.html?id=${customerFile.id}&asmlFlag=ikea" /><span>File Documents</span></a></li>
		    <li><a href="ikeaOrders.html" /><span>Order List</span></a></li>
		    <li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=accPortal&reportSubModule=${partnerAbbreviation}&jobType=${customerFile.job}&preferredLanguage=${customerFile.customerLanguagePreference}&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		 </div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
	  </c:if>	
	  <c:if test="${empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a onmouseover="" onclick=" " class="current"><span>Order Detail</span></a></li>
		    <li><a onmouseover="" onclick=" "><span>File Documents</span></a></li> 
		    <li><a href="ikeaOrders.html" /><span>Order List</span></a></li>  
		    <li><a onclick=" "><span>Forms</span></a></li>
		 </div><div class="spn">&nbsp;</div>
	  </c:if> 

      <div id="content" align="center">
       <div id="liquid-round">
       <div class="top"><span></span></div>
       <div class="center-content">
<table class="no-mp" cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td> 
 <div  style="margin:0px;cursor:hand;">
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;" >
        <tr>
        <td class="headtab_left"></td>
	    <td NOWRAP class="headtab_center" style="cursor:hand; ">&nbsp;&nbsp;Transferee Details </td>
        <td width="28" valign="top" class="headtab_bg" style="cursor:hand; "></td>
        <td class="headtab_bg_center" style="cursor:hand;"> <a href="javascript:animatedcollapse.hide(['presentaddress', 'ciinfo', 'benefits', 'family', 'hr'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show([ 'presentaddress', 'ciinfo', 'benefits', 'family', 'hr'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a></td>
        <td class="headtab_right"></td>
	    </tr>
</table>
</div>
<div id="employee" class="switchgroup1">

		<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;padding:0px;" >
			 <tbody>			 
			  <tr>
			  <td colspan="15">
			  <table class="no-mp" cellspacing="3" cellpadding="1" border="0" width="">
			  <tr>
			 	<td style="width:135px;">&nbsp;</td>
				<td align="left" width="" class="listwhitetext" valign="bottom" ><fmt:message key='customerFile.prefix'/><font color="red" size="2">*</font></td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' />&nbsp;(as per passport)<font color="red" size="2">*</font></td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom">Last&nbsp;Name&nbsp;(as per passport)<font color="red" size="2">*</font></td>
			 </tr>
			 <tr> 
			 
			 <td class="listwhitetext" align="right"><b>Name&nbsp;of&nbsp;Co-worker</b></td>
				<td align="left" class="listwhitetext" valign="top"> <s:select cssClass="list-menu" name="customerFile.prefix" cssStyle="width:55px" list="%{preffix}" headerKey="" headerValue="" tabindex="1" /></td>
				
				<td align="left" class="listwhitetext" valign="top"> <s:textfield cssClass="input-text upper-case" name="customerFile.firstName" onchange="" onblur="titleCase(this)" onkeypress="" required="true" cssStyle="width:204px" maxlength="80" tabindex="3" /> </td>
				
				<td align="left" class="listwhitetext" valign="top"> <s:textfield cssClass="input-text" key="customerFile.middleInitial" 	required="true" cssStyle="width:19px" maxlength="1" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="4" /> </td>
				
				<td align="left" class="listwhitetext" valign="top"> <s:textfield cssClass="input-text upper-case" key="customerFile.lastName" onchange="" onblur="titleCase(this)" onkeypress="" required="true" cssStyle="width:204px" maxlength="80" tabindex="5" /> </td>
				<c:if test="${empty customerFile.id}">
				<td align="right" style="width:190px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
				</c:if>
				<c:if test="${not empty customerFile.id}"> 
				<c:choose>
					<c:when test="${countAccountPortalNotes == '0' || countAccountPortalNotes == '' || countAccountPortalNotes == null}">
					<td align="right" style="width:190px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);" ></a></td>
					</c:when>
					<c:otherwise>
					<td align="right" style="width:190px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);" ></a></td>
					</c:otherwise>
				</c:choose> 
				</c:if>
			 </tr>
			  
			
			 <tr>
			    <td align="right" class="listwhitetext">Mobile&nbsp;Phone<font color="red">*</font></td>
			    <td colspan="2"><s:textfield cssClass="input-text" name="customerFile.originMobile" cssStyle="width:160px;" maxlength="25" tabindex="6" /></td>
			    
			    <td align="right" class="listwhitetext">Primary&nbsp;Email<font color="red" size="2">*</font></td>
			    <td colspan="2"><s:textfield cssClass="input-text" name="customerFile.email" cssStyle="width:240px;" maxlength="65" tabindex="9" onchange="validateEmail(this);" />
			    
			    <a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.email'].value)">
			    <img class="openpopup" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email}"/></a></td>
             </tr>
                
             <tr> 
			    <td align="right" class="listwhitetext">Office Phone</td>
			    <td colspan="2"><s:textfield cssClass="input-text" name="customerFile.originDayPhone" cssStyle="width:160px;" maxlength="20" tabindex="7" /></td>
			    
			    <td align="right" class="listwhitetext">Secondary Email</td>
			    <td colspan="2"><s:textfield cssClass="input-text" name="customerFile.email2" cssStyle="width:240px;" maxlength="65" tabindex="10" onchange="validateEmail(this);"/>
			    
			    <a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.email2'].value)">
			    <img class="openpopup" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email2}"/></a></td>
	         </tr>
	         
	         <tr>
			    <td align="right" class="listwhitetext">Home Phone</td>
			    <td colspan="2"><s:textfield cssClass="input-text" name="customerFile.originHomePhone" cssStyle="width:160px;" maxlength="20" tabindex="8" /></td>
			    
			    <td align="right" class="listwhitetext" >
						<div id="spousePartnerEmailNotRequired">Spouse/Partner Email</div>
						<div id="spousePartnerEmailRequired">Spouse/Partner Email<font color="red" size="2">*</font></div>
				</td>
			    <td colspan="2" align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.custPartnerEmail"  cssStyle="width:240px;" maxlength="65" tabindex="11" onchange="validateEmail(this);"/>
			    
			    <a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.custPartnerEmail'].value)">
			    <img class="openpopup" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.custPartnerEmail}"/></a></td>
             </tr>
	         <tr>
			    <td align="right" class="listwhitetext">Job Function<font color="red">*</font></td>
			    <td colspan="2"><s:textfield cssClass="input-text" name="customerFile.jobFunction" cssStyle="width:160px;" maxlength="65" tabindex="8" /></td>
			    </tr>
	         <tr>
			     <td align="right" class="listwhitetext">Start&nbsp;Date&nbsp;of&nbsp;Contract/Stay<font color="red" size="2">*</font></td> 
			     <c:if test="${not empty customerFile.contractStart}">
					<s:text id="customerFileContractStartFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractStart"/></s:text>
					<td colspan="2"> <s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" value="%{customerFileContractStartFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
					 	<img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
			    	</td>
			    </c:if>
			    <c:if test="${empty customerFile.contractStart}">
				<td colspan="2"><s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/>
					<img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
				</td>
				</c:if>
				
				<td align="right" class="listwhitetext" >
						<div id="endDateNotRequired">End&nbsp;Date&nbsp;of&nbsp;Contract/Stay</div>
						<div id="endDateRequired">End&nbsp;Date&nbsp;of&nbsp;Contract/Stay<font color="red" size="2">*</font></div>
			    </td>
				<td colspan="4">
				<table class="no-mp" cellspacing="0" cellpadding="0">
				<tr>
				 <c:if test="${not empty customerFile.contractEnd}">
					 <s:text id="customerFileContractEndFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractEnd"/></s:text>
					<td width="110px"> <s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" value="%{customerFileContractEndFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
					 <img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
			    </td>
			    </c:if>
			    <c:if test="${empty customerFile.contractEnd}">
					 <td width="110px"><s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
					<img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
				</td>
				</c:if>
				<td width="74" align="right" class="listwhitetext" >Duration&nbsp;</td>
			    <td width="108" ><s:textfield cssClass="input-text" name="customerFile.duration" id="ikeaOrderDuration" cssStyle="width:87px" maxlength="5" readonly="true" onkeydown="return onlyNumsAllowed(event)"  onselect="" onfocus="calcDays();" /></td>
				<td colspan="4" align="left">
		      	<table class="no-mp" cellspacing="0" cellpadding="0" border="0">
		      	<tr>
				<td class="listwhitetext" align="right" >Assignment&nbsp;End&nbsp;Reason<font color="red" size="2"></font>&nbsp;</td>
		        <td style="padding-left: 4px;"><configByCorp:customDropDown listType="map" 	list="${assignmentEndReason}" fieldValue="${customerFile.assignmentEndReason}"
					attribute="class=list-menu style=width:120px;  name=customerFile.assignmentEndReason headerKey='' headerValue='' " />
					</td>
				</tr>
				</table>
				</td>
		        </tr>
				</table>
				</td>			    
			 </tr>
			 
			<tr>
		      <td class="listwhitetext" align="right">Mobility&nbsp;Type<font color="red" size="2">*</font></td>
		      <td colspan="2"><s:select cssClass="list-menu" name="customerFile.assignmentType" list="%{assignmentList}" cssStyle="width:163px" headerKey="" headerValue="" tabindex="13" onchange="changeEndDate();spousePartnerEmailRequiredMethod();" /></td>
		      
		      <td align="right" class="listwhitetext">Original&nbsp;IKEA&nbsp;Employing&nbsp;Entity<font color="red">*</font></td>
		      	<td colspan="4" align="left">
		      	<table class="no-mp" cellspacing="0" cellpadding="0" border="0">
		      	<tr>
	 			<td width=""><s:textfield cssClass="input-text"	id="originalCompanyCodeId" name="customerFile.originalCompanyCode" cssStyle="width:65px;" tabindex="14" maxlength="8"
							onchange="valid(this,'special');findOrderInitiationBillToCode();" />
				<img style="vertical-align:top;padding-right:5px;" class="openpopup" id="originalCompanyCode.popupImage" width="17" height="20" onclick="openOriginalCompanyPopUp();document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
				</td>
	 
	 			<td align="left" width=""><s:textfield	cssClass="input-text" id="originalCompanyNameId" name="customerFile.originalCompanyName" cssStyle="width:160px;" tabindex="15" maxlength="250"
									onkeyup="findIkeaPartnerDetails('originalCompanyNameId','originalCompanyCodeId','originalCompanyNameDiv',event);"
									onchange="findPartnerDetailsByName('originalCompanyCodeId','originalCompanyNameId');" /> 
				<img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'originalCompanyCodeId');" src="<c:url value='/images/address2.png'/>" />
				</td>
				<div id="originalCompanyNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 560px;"></div>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
			<td align="right" class="listwhitetext">Current&nbsp;Country&nbsp;of&nbsp;Residence<font color="red" size="2">*</font></td>
				<td colspan="2"><s:select cssClass="list-menu" name="dummyOriginCountry" list="%{ocountry}" id="dummyOriginCountry" onchange="copyCountryValue('dummy');requiredOriginState();" cssStyle="width:163px" tabindex="16" headerKey="" headerValue="" /></td>
			
			<td align="right" class="listwhitetext">Original&nbsp;Company&nbsp;Hiring&nbsp;Date<font color="red">*</font></td>
				<c:if test="${not empty customerFile.originalCompanyHiringDate}">
					<s:text id="customerFileOriginalCompanyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.originalCompanyHiringDate"/></s:text>
					<td colspan="4"> <s:textfield cssClass="input-text" id="originalCompanyHiringDate" name="customerFile.originalCompanyHiringDate" value="%{customerFileOriginalCompanyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
						<img id="originalCompanyHiringDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
			    	</td>
			    </c:if>
			    <c:if test="${empty customerFile.originalCompanyHiringDate}">
					 <td colspan="4"><s:textfield cssClass="input-text" id="originalCompanyHiringDate" name="customerFile.originalCompanyHiringDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
						<img id="originalCompanyHiringDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
					</td>
				</c:if>
			</tr>
			<tr>
			<td  align="right"  class="listwhitetext" >Family&nbsp;Situation<font color="red" size="2">*</font></td>  
             <td colspan="2"><s:select cssClass="list-menu" name="customerFile.familySitiation" list="%{familySitiationList}" onchange="spousePartnerEmailRequiredMethod();" cssStyle="width:163px" tabindex="17" headerKey="" headerValue="" /></td>
             <td align="right" class="listwhitetext">Current company of employment<font color="red">*</font></td>
             
             <td colspan="4" align="left">
		      	<table class="no-mp" cellspacing="0" cellpadding="0" border="0">
		      	<tr>
	         <td width=""><s:textfield cssClass="input-text"	id="currentEmpCode" name="customerFile.currentEmploymentCode" tabindex="18"cssStyle="width:65px;" maxlength="8"
							onchange="valid(this,'special');findCurrentEmployCode();" />
		    <img style="vertical-align:top;padding-right:5px;" class="openpopup" id="currentEmpCode.popupImage" width="17" height="20" onclick="opencurrentEmployeeCodePopUp();document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	         <td align="left" width=""><s:textfield	cssClass="input-text" id="curretEmpNameId"	name="customerFile.currentEmploymentName" cssStyle="width:163px;" tabindex="19" maxlength="250"
									onkeyup="findIkeaPartnerDetails('curretEmpNameId','currentEmpCode','currentEmployeeNameDiv',event);"
									onchange="findPartnerDetailsByName('currentEmpCode','curretEmpNameId');" /> 
		<img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'currentEmpCode');" src="<c:url value='/images/address2.png'/>" /></td>
		<div id="currentEmployeeNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
		 
            
             <td align="right" class="listwhitetext">&nbsp;&nbsp;1st International move<font color="red" size="2">*</font>&nbsp;</td>
             <td style="vertical-align:top;"><s:select cssClass="list-menu" id="firstInternationalMoveDummy" name="firstInternationalMoveDummy" value="${firstInternationalMoveValue}" list="{'','Yes','No'}" cssStyle="width:50px"  onchange="selectValue(this);" tabindex="20" /></td>
			</tr>
			</table>
			</td>
			<%-- 	
             
             <s:checkbox id="customerFile.firstInternationalMove" cssStyle="margin:0px;" tabindex="20" key="customerFile.firstInternationalMove" value="${isVipFlag}" /></td>
			 --%></tr>            
         	</table>
         	</td>
         	</tr>           
            
	         <tr>
	         <td colspan="6">
	         <div onClick="javascript:animatedcollapse.toggle('presentaddress')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">                   
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Addresses </td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center listblacktextBig"> </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>
            
	         <div id="presentaddress">               
				<table class="no-mp" cellspacing="3" cellpadding="1">
				<tr>
				<td style="width:40px;"></td>
				<td></td>
				<td colspan="3" class="subcontenttabChild" style="text-align: center;height:18px;">Present&nbsp;Address</td>
				<td colspan="2"></td>
				<td colspan="3" class="subcontenttabChild" style="text-align: center;height:18px;">Destination&nbsp;Address</td>
	            </tr>
	            <tr>
	            <td style="width:40px;"></td>
	            <td align="right" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/><font color="red" size="2">*</font></td>
	           	<td align="left" colspan="3"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress1" onblur="titleCase(this);" cssStyle="width:255px;" maxlength="100" tabindex="20" /></td>
	           	<td style="width:20px;"></td>	           	
	           	<td align="right" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
	           	<td align="left" colspan="3"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress1" onblur="titleCase(this);" cssStyle="width:255px;" maxlength="100" tabindex="26" /></td>
	            </tr>
	            
	            <tr>
	            <td style="width:40px;"></td>
	            <td align="right" class="listwhitetext"><font color="gray">Address2</font></td>
			    <td align="left" colspan="3"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress2" onblur="titleCase(this);" cssStyle="width:255px;" maxlength="100" tabindex="21" /></td>
			    <td style="width:20px;"></td>
			    <td align="right" class="listwhitetext"><font color="gray">Address2</font></td>
			    <td align="left" colspan="3"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress2" onblur="titleCase(this);" cssStyle="width:255px;" maxlength="100" tabindex="27" /></td>
				</tr>
				<tr>
	            <td style="width:40px;"></td>
	            <td align="right" class="listwhitetext"><font color="gray">Address3</font></td>
	           	<td align="left" colspan="3"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress3" onblur="titleCase(this);" cssStyle="width:255px;" maxlength="100" tabindex="20" /></td>
	           	<td style="width:20px;"></td>	           	
	           	<td align="right" class="listwhitetext"><font color="gray">Address3</font></td>
	           	<td align="left" colspan="3"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress3" onblur="titleCase(this);" cssStyle="width:255px;" maxlength="100" tabindex="26" /></td>
	            </tr>				
				<tr>
				<td style="width:40px;"></td>
				<td align="right" class="listwhitetext"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
				<td><s:textfield cssClass="input-text upper-case" name="customerFile.originCity" cssStyle="width:129px" maxlength="30"  tabindex="22" onkeypress="" onblur="titleCase(this);autoPopulate_customerFile_originCityCode(this,'special');" /></td>
				
				<td align="right" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /><font color="red" size="2">*</font></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originZip" cssStyle="width:50px" maxlength="10" tabindex="23" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special')"  /></td>
				<td width="20"></td>
				<td align="right" class="listwhitetext"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
				<td><s:textfield cssClass="input-text upper-case" name="customerFile.destinationCity" cssStyle="width:129px;!width:auto;" maxlength="30" tabindex="28" onkeypress="" onblur="titleCase(this);autoPopulate_customerFile_destinationCityCode(this,'special');" /></td>
				
				<td align="right" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
				<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" cssStyle="width:50px;" maxlength="10" tabindex="29" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special')" /></td>
				</tr>
				
				<tr>
					<td style="width:40px;"></td>
					<td align="right" class="listwhitetext" >
						<div id="originStateNotRequired"><fmt:message key='customerFile.originState'/></div>
						<div id="originStateRequired"><fmt:message key='customerFile.originState'/><font color="red" size="2">*</font></div>
					</td>
					<td colspan="3"><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:259px"  tabindex="24" onchange="autoPopulate_customerFile_originCityCode(this); " headerKey="" headerValue="" /></td>
				
					<td align="right" colspan="2" class="listwhitetext" >
						<div id="destinationStateNotRequired"><fmt:message key='customerFile.destinationState'/></div>
						<div id="destinationStateRequired"><fmt:message key='customerFile.destinationState'/><font color="red" size="2">*</font></div>
					</td>
					<td colspan="3"><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:259px" tabindex="30" value="%{customerFile.destinationState}"  onchange="autoPopulate_customerFile_destinationCityCode(this);" headerKey="" headerValue="" /></td>
				</tr>
				
				<tr>
				<td style="width:40px;"></td>
				<td align="right" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
				<td colspan="3"><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" id="ocountry" cssStyle="width:259px"  tabindex="25" onchange="requiredOriginState();getOriginCountryCode();getState(this);enableStateListOrigin();copyCountryValue('origin');"  headerKey="" headerValue="" /></td>
				<td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>
				
				<td align="right" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
				<td colspan="3"><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" id="dcountry" cssStyle="width:259px"  tabindex="32" onchange="requiredDestinState();getDestinationCountryCode();getDestinationState(this);enableStateListDestin();"  headerKey="" headerValue="" /></td>
				<td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openDestinationLocation();"/></td>				
				</tr> 		
				</table>
				</div>
				</td>
				</tr>
								
				

<tr>
<td> 
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;">
<tr><td height="10" width="100%" align="left" style="margin: 0px">
<div  onClick="javascript:animatedcollapse.toggle('ciinfo')" style="margin: 0px">
 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
 <tr>
 <td class="headtab_left"></td>
 <td NOWRAP  class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;Company Invoicing Information </td>
 <td width="28" valign="top" class="headtab_bg"></td>
 <td class="headtab_bg_center">&nbsp; </td>
 <td class="headtab_right"> </td>
 </tr>
 </table>
 </div>
 <div id="ciinfo" class="switchgroup1">
     <table border="0" class="detailTabLabel">
	 <tbody>
	 <tr>
	 <td align="right" class="listwhitetext">Company&nbsp;Paying</td>
	 <td colspan="5">
	 <table class="no-mp" cellpadding="0" cellspacing="0">
	 <tr>	 
	 <td width=""><s:textfield cssClass="input-text"	id="billToCodeId" name="customerFile.billToCode" tabindex="33" cssStyle="width:80px;" maxlength="8"
							onchange="valid(this,'special');findBillToCode();" />
		<img style="vertical-align:top;padding-right:5px;" class="openpopup" id="billToCode.popupImage" width="17" height="20" onclick="openBillToCodePopUp();document.forms['customerFileForm'].elements['customerFile.billToCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	 
	 <td align="left" width=""><s:textfield	cssClass="input-text" id="billToNameId"	name="customerFile.billToName" cssStyle="width:235px;" tabindex="34" maxlength="250"
									onkeyup="findIkeaPartnerDetails('billToNameId','billToCodeId','billToNameDiv',event);"
									onchange="findPartnerDetailsByName('billToCodeId','billToNameId');" /> 
		<img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'billToCodeId');" src="<c:url value='/images/address2.png'/>" /></td>
		<div id="billToNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
		
		<td align="right" width="177" class="listwhitetext">&nbsp;&nbsp;Cost Centre Field<font color="red">*</font>&nbsp;</td>   
		<td><s:textfield cssClass="input-text" name="customerFile.billToReference"  cssStyle="width:120px" maxlength="20"  tabindex="40"/></td>	<td align="left" style="width: 20px"></td>
		<td align="left" class="listwhitetext" width="136px" colspan="1">Account PO#/SAP Number</a></td>
					<td><s:textfield id="billToAuthorization" cssClass="input-text" name="customerFile.billToAuthorization"cssStyle="width:250px;" maxlength="50" onblur="valid(this,'special')" /></td>
		</tr>		
		</table>
		</td>
	 </tr>
	 <tr>
	 <td align="right" class="listwhitetext">Invoicing email address</td>
	 <td colspan="5">
	 <table class="no-mp" cellpadding="0" cellspacing="0">
	 <tr>	 
	 <%-- <td width=""><s:textfield cssClass="input-text" name="invoiceEmailAddressDummy"  value="${invoiceEmailAddress}" tabindex="34" cssStyle="width:233px;" maxlength="200" onchange="changeValue1(this);validateEmail(this);"/> --%>
	 <td><s:textfield cssClass="input-text" name="customerFile.contactEmail" cssStyle="width:233px;" maxlength="65" onchange="validateEmail(this);" />
      <a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.contactEmail'].value)">
		<img class="openpopup" style="vertical-align:top;" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.contactEmail}"/></a></td>
	 <td style="width:207px;"></td>
	 <td align="right" width="102" class="listwhitetext">Contact person<font color="red">*</font>&nbsp;</td>
	 <%-- <td width=""><s:textfield cssClass="input-text" name="contactPersonDummy" value="${contactPerson}" tabindex="41" cssStyle="width:120px;" maxlength="200" onchange="changeValue2(this);"/> --%>
	 <td><s:textfield cssClass="input-text" id="contactName" name="customerFile.contactName" cssStyle="width:120px;" maxlength="95" onkeydown="return onlyCharsAllowed(event, this, 'special')" onblur="valid(this,'special')" /></td>
	</tr>
	</table>
	</td>
	</tr>
	 <tr>
	 <td colspan="5" >
		 <table width="100%" style="margin:0px;padding:0px;">
		 <tr> 
		  <td align="center"  class="listwhitetext" style="width:30%;color:#007a8d;"><b><u>Origin/Home Country</u></b></td>
		  <td align="center"  class="listwhitetext" style="padding-left:5%;width:36%;color:#007a8d;"><b><u>Destination/Host Country</u></b></td> 
		 </tr>
		 </table>
	 </td>
	 </tr>
	 
	 <tr>
	 <td colspan="8" >
		 <table width="" style="margin:0px;padding:0px;">
		 <tr> 
	  <td align="right" class="listwhitetext" style="width:158px;">Company<font color="red">*</font></td>
	   <td align="left" class="listwhitetext"  ><s:textfield cssClass="input-text"	id="originCompanyCodeId" name="customerFile.originCompanyCode" tabindex="35" cssStyle="width:60px;" maxlength="8"
							onchange="valid(this,'special');findOriginCompanyCode();" />
		<img style="vertical-align:top;padding-right:5px;" class="openpopup" id="originCompanyCode.popupImage" width="17" height="20" onclick="openOriginCompanyCodePopUp();document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="originCompanyId" name="customerFile.originCompany"  cssStyle="width:235px;" maxlength="280" tabindex="36" onkeyup="findIkeaPartnerDetails('originCompanyId','originCompanyCodeId','originCompanyNameNameDiv',event);"
									onchange="findPartnerDetailsByName('originCompanyCodeId','originCompanyId');" />							
            <img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'originCompanyCodeId');" src="<c:url value='/images/address2.png'/>" />
	    </td>
									
									</td>
	    <td align="right" class="listwhitetext" style="width:8px;"></td>
	   <td align="right" class="listwhitetext" style="padding-left: 220px;">Company<font color="red">*</font></td>
	    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"	id="destCompanyCodeID" name="customerFile.destinationCompanyCode" tabindex="42" cssStyle="width:60px;" maxlength="8"
							onchange="valid(this,'special');findDestinationCompanyCode();" />
		<img style="vertical-align:top;padding-right:5px;" class="openpopup" id="destCompanyCode.popupImage" width="17" height="20" onclick="openDestinationCompanyCodePopUp();document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	   <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="destinationCompanyId" name="customerFile.destinationCompany" cssStyle="width:225px;" maxlength="280" tabindex="43" onkeyup="findIkeaPartnerDetails('destinationCompanyId','destCompanyCodeID','destCompanyNameNameDiv',event);"
									onchange="findPartnerDetailsByName('destCompanyCodeID','destinationCompanyId');" />
   <img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'destCompanyCodeID');" src="<c:url value='/images/address2.png'/>" />	
									</td>
	    <div id="originCompanyNameNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
	    <div id="destCompanyNameNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
	  </tr>
		 </table>
		 </td>
		 <tr> 
	 </tr>
	 <tr>
        <td align="right" class="listwhitetext">Name of Mobility/HR responsible</td>
		<td><s:textfield cssClass="input-text" name="customerFile.authorizedBy" cssStyle="width:235px" tabindex="37" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)"  /></td>
		
		<td align="right" class="listwhitetext">Name of Mobility/HR responsible<font color="red" size="2">*</font></td>
		<td colspan="2"><s:textfield cssClass="input-text" name="customerFile.localHr" cssStyle="width:225px" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="44" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext">Phone</td>   
		<td><s:textfield cssClass="input-text" name="customerFile.authorizedPhone"  cssStyle="width:120px" maxlength="20"  tabindex="38"/></td>
		
		<td align="right" class="listwhitetext">Phone<font color="red" size="2">*</font></td>   
		<td colspan="2"><s:textfield cssClass="input-text" name="customerFile.localHrPhone"  cssStyle="width:120px" maxlength="20"  tabindex="45"/></td>
	</tr>
	<tr>
        <td align="right" class="listwhitetext">Email Address</td>
		<td ><s:textfield cssClass="input-text" name="customerFile.authorizedEmail" cssStyle="width:235px" maxlength="65" tabindex="39"  onkeydown="" onchange="validateEmail(this);"/>
		<a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.authorizedEmail'].value)">
		<img class="openpopup" style="vertical-align:top;" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.authorizedEmail}"/></a></td>
		
		<td align="right" class="listwhitetext">Email Address<font color="red" size="2">*</font></td>
		<td colspan="2"><s:textfield cssClass="input-text" name="customerFile.localHrEmail" cssStyle="width:225px" maxlength="65" tabindex="46"  onkeydown="" onchange="validateEmail(this);"/>
		<a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.localHrEmail'].value)">
		<img class="openpopup" style="vertical-align:top;" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.localHrEmail}"/></a></td>
	</tr>
	 <tr>
        <td align="right" class="listwhitetext"></td>
     </tr>
	 </tbody>
	 </table> 
 </div>
 </td>
 </tr>
 
 </table>
 </td>
 </tr>

<tr>
<td> 
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;">
<tr><td height="10" width="100%" align="left" style="margin: 0px">
<div  onClick="javascript:animatedcollapse.toggle('benefits')" style="margin: 0px">
 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
 <tr>
 <td class="headtab_left"></td>
 <td NOWRAP  class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;Benefits Type </td>
 <td width="28" valign="top" class="headtab_bg"></td>
 <td class="headtab_bg_center">&nbsp; </td>
 <td class="headtab_right"> </td>
 </tr>
 </table>
 </div>
 	<div id="benefits" class="switchgroup1">
     <table border="0" class="detailTabLabel" >
	 <tbody>
	<tr>
	<td style="width:96px;"></td>
        <td colspan="2">
			 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tr>
                      <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
                      <td></td>
                      </tr>
                      <c:if test="${serviceRelosCheck==''}">
                       <s:hidden  name="customerFile.serviceRelo" value="${customerFile.serviceRelo}"/>
                     </c:if>
                      <c:if test="${serviceRelosCheck!=''}">
                      <tr> 
                      <s:hidden  name="customerFile.serviceRelo"   />
                     <td colspan="2" class="listwhitetext" valign="middle"><input type="checkbox" style="margin-left:0px " name="checkAllRelo"  onclick="checkAll(this)" tabindex="8" />Check&nbsp;All&nbsp;</td>
                     </tr>
                     <tr> 
                     <td colspan="2" valign="top">
                     <fieldset style="margin:3px 0 0 0; padding:2px 0 0 2px; ">                      
                     <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                     <tr>
                      <td width="53%" valign="top">
                        <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          
                          <c:forEach var="entry" items="${serviceRelos1}">
                           <c:if test="${entry.key!='MMG'}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="9"/> </td> 
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td> 
                          </tr>
                          </c:if>
                          </c:forEach>
                          
                          <c:forEach var="entry" items="${serviceRelos2}">
                           <c:if test="${entry.key!='MMG'}"> 
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="9"/> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:if> 
                          </c:forEach>
                          
                           <c:forEach var="entry" items="${serviceRelos1}">
                           <c:if test="${entry.key=='MMG'}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="9"/> </td> 
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td> 
                          </tr>
                          </c:if>
                          </c:forEach>
                          
                          <c:forEach var="entry" items="${serviceRelos2}">
                           <c:if test="${entry.key=='MMG'}"> 
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="9"/> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:if> 
                          </c:forEach>
                          
                          </table>                    
                       </td>
                       </tr>
                       </table>
                          </fieldset>
                       </td>
                        
                       </tr>
                       </c:if>
                       <tr>
	                      <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
	                      <td></td>
                        </tr>
                      </table>
		</td>
	
		<td align="right" class="listwhitetext" style="padding-left:47px;vertical-align:bottom;padding-bottom:22px;">Preferred removal date</td>
	     <c:if test="${not empty customerFile.moveDate}">
			 <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
			<td style="vertical-align: bottom;padding-bottom:19px;"> <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" tabindex="49" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
			<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
	    	</td>
	    </c:if>
	    <c:if test="${empty customerFile.moveDate}">
			<td style="vertical-align: bottom;padding-bottom:19px;"><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="49" onkeydown="return onlyDel(event,this)"/>
				<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
			</td>
		</c:if>
	</tr>
	<tr>
		<td class="listwhitetext" align="right" >Comment</td>
		 <td colspan="4"><s:textarea cssClass="textarea"  cssStyle="width:447px;height:60px;" tabindex="48" name="customerFile.orderComment" /> 
   </tr>
	</tbody>
	</table> 
 	</div>
 </td>
 </tr>
 </table>
 </td>
 </tr>

<c:if test="${not empty customerFile.id}">
	<tr> 
             <td height="10" width="100%" colspan="6" align="left" style="margin:0px">
               <div onClick="javascript:animatedcollapse.toggle('family')" style="margin:0px;">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Family&nbsp;Details </td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>           
       
                <div id="family" class="OverFlowPr" style="margin:5px 0px 5px 0px;padding:0px;width:100%;">
                <s:set name="dsFamilyDetailsList" value="dsFamilyDetailsList" scope="request"/>
                  <display:table name="dsFamilyDetailsList" class="table" requestURI="" id="dsFamilyDetailsList"  defaultsort="1" pagesize="10" style="width:100%;margin-bottom:10px;">
                     <c:if test="${customerFile.orderIntiationStatus=='Submitted'}">
                     <display:column title="First Name"> 
                     <c:out value="${dsFamilyDetailsList.firstName}"></c:out>
                     </display:column> 
                     </c:if>
                     <c:if test="${customerFile.orderIntiationStatus!='Submitted'}">
                 <display:column title="First Name">
                     <a href="javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,510)">
                     <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
                     </display:column>  
                     </c:if>
                  <display:column property="lastName"  title="Last Name"/>
			      <display:column property="relationship"  title="Relationship"/>   
			      <display:column property="cellNumber"  title="Mobile Phone"  />
			      <display:column property="email" title="Email"/>
			      <display:column property="dateOfBirth" title="Date of Birth" format="{0,date,dd-MMM-yyyy}" />
			      <display:column property="age" title="Age"/>
			      <display:column property="countryOfBirth" title="Country of Birth"/>
			      <display:column property="passportNumber" title="Passport #"/> 
				  <display:column property="expiryDate" title="Expiry Date" format="{0,date,dd-MMM-yyyy}" />
				  <display:column property="countryOfIssue"  title="Country of Issue"/> 
				  <display:column property="nationality"  title="Nationality"/>
				  </display:table>
				  
	              <c:if test="${not empty customerFile.id}">
	               <input type="button" class="cssbutton1" onclick="window.open('editDsFamilyDetails.html?isOrderInitiation=true&decorator=popup&popup=true&customerFileId=${customerFile.id}','sqlExtractInputForm','height=520,width=1350,top=0, scrollbars=yes,resizable=yes').focus();" 
	                value="Add Family Details" style="width:120px;margin-bottom:10px;!margin-bottom:0px;" /> 
                  </c:if>
                  <c:if test="${empty customerFile.id}">
	                <input type="button" class="cssbutton1" onclick="massageFamily();"  value="Add Family Details" style="width:120px;margin-bottom:10px;!margin-bottom:0px;" /> 
                   </c:if>
			     <span style="padding-left:20px;" class="listwhitetext">Family Size <s:textfield cssClass="input-text" name="customerFile.familysize" readonly="true" size="2" maxlength="2"   /></span>
                </div>
	         </td> 
	</tr>
</c:if>
	
</table>
</div>
</td>
</tr>
</table>

</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
<div class="modal fade" id="showFileErrorModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				    <div class="modal-body" style="font-weight:600;">
					    <div class="alert alert-danger" id="getFileErrorMsg">
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
				    </div>
			</div>
		</div>
	</div>	
</div>
	
	<table border="0">
		<tbody>
			
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>							
				<td valign="top"></td>
				<td style="width:120px">
				<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
				pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
				</td>		
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
				<c:if test="${not empty customerFile.id}">
					<s:hidden name="customerFile.createdBy"/>
					<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
				</c:if>
				<c:if test="${empty customerFile.id}">
					<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
				
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
				pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
				<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
				<c:if test="${not empty customerFile.id}">
					<s:hidden name="customerFile.updatedBy"/>
					<td style="width:85px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
				</c:if>
				<c:if test="${empty customerFile.id}">
					<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
			</tr>
		</tbody>
	</table>
<c:if test="${empty customerFile.id}">
	<input type="button" class="cssbuttonA"  name="save" value="Next" onclick="return saveIkeaOrder('save');"/>
</c:if>
<c:if test="${not empty customerFile.id}">		 
	<!-- <input type="button" class="cssbuttonA"  name="save" value="Save" onclick="return saveIkeaOrder('save') ;"/> -->
	<input type="button" class="cssbuttonA"  name="submit1" value="Submit" onclick="return saveIkeaOrder('submit') " />
</c:if>
<input type="button" class="cssbuttonA"  name="Cancel" value="Cancel" onclick="location.href='<c:url value="/ikeaOrders.html"/>' " />	
</s:form>

<script type="text/javascript">
try{
	checkColumn();
	}
	catch(e){}
try{
	checkAllRelo();
	}
catch(e){}
animatedcollapse.addDiv('presentaddress', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('ciinfo', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('benefits', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('family', 'fade=1,hide=0,show=0')
animatedcollapse.addDiv('hr', 'fade=1,hide=0,show=1')
animatedcollapse.init()


setOnSelectBasedMethods(['forDays(),calcDays()']);
setCalendarFunctionality();

var checkedItems=document.getElementsByName('checkV');
var selectedItems="";
for(var i=0; i<checkedItems.length; i++){
	if(checkedItems[i].type=='checkbox'){
		 if(selectedItems == '' ){
			 selectedItems = checkedItems[i].value;
		 }else{
			 selectedItems = selectedItems+"@"+checkedItems[i].value;
		 }
	}
}
document.getElementById('benefitTypeCheckedVal').value = selectedItems;
function getCheckedValues(idVal){
	var checkedVal = document.forms['customerFileForm'].elements['benefitTypeCheckedVal'].value;
    
    if(checkedVal == ''){
   	 	document.forms['customerFileForm'].elements['benefitTypeCheckedVal'].value = idVal;
    }else{
   		var check = checkedVal.indexOf(idVal);
		if(check > -1){
			var values = checkedVal.split("@");
		   for(var i = 0 ; i < values.length ; i++) {
		      if(values[i]== idVal) {
		    	values.splice(i, 1);
		    	checkedVal = values.join("@");
		    	document.forms['customerFileForm'].elements['benefitTypeCheckedVal'].value = checkedVal;
		      }
		   }
		}else{
			checkedVal = checkedVal + "@" +idVal;
			document.forms['customerFileForm'].elements['benefitTypeCheckedVal'].value = checkedVal.replace('@ @',"@");
		}
    }
}
function notExists(){
	var $j = jQuery.noConflict();
	$j('#getFileErrorMsg').html("<p>The customer information has not been saved yet, please save customer information to continue.</p>");
	$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
}
function saveIkeaOrder(temp){
	var originCountryVal = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var destCountryVal = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
    var assignType = document.forms['customerFileForm'].elements['customerFile.assignmentType'].value; 
	var $j = jQuery.noConflict();
	if(document.forms['customerFileForm'].elements['customerFile.prefix'].value==""){
		$j('#getFileErrorMsg').html("<p>Prefix is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.firstName'].value.trim()==""){
		$j('#getFileErrorMsg').html("<p>First Name is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.lastName'].value==""){
		$j('#getFileErrorMsg').html("<p>Last Name is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originMobile'].value==""){
		$j('#getFileErrorMsg').html("<p>Mobile Phone is Required Field in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.email'].value==""){
		$j('#getFileErrorMsg').html("<p>Primary Email is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.jobFunction'].value==""){
		$j('#getFileErrorMsg').html("<p>JobFunction  is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.contractStart'].value==null || document.forms['customerFileForm'].elements['customerFile.contractStart'].value==''){
		$j('#getFileErrorMsg').html("<p>Start Date of Contract/Stay is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if((assignType=="STA"||assignType=="Commuter" ||assignType=="IA International Assignment" ) && (document.forms['customerFileForm'].elements['customerFile.contractEnd'].value=="")){
		$j('#getFileErrorMsg').html("<p>End Date Of Contract/Stay is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value==""){
		$j('#getFileErrorMsg').html("<p>Mobility Type is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].value==""){
		$j('#getFileErrorMsg').html("<p>Original IKEA Employing Entity  is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['dummyOriginCountry'].value==""){
		$j('#getFileErrorMsg').html("<p>Current Country of Residence is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originalCompanyHiringDate'].value==""){
		$j('#getFileErrorMsg').html("<p>Origin Company Hiring Date  is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.familySitiation'].value==""){
		$j('#getFileErrorMsg').html("<p>Family Situation is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value==""){
		$j('#getFileErrorMsg').html("<p>Current company of Employment  is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['firstInternationalMoveDummy'].value==""){
		$j('#getFileErrorMsg').html("<p>firstInternationalMove is required in in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if((document.forms['customerFileForm'].elements['customerFile.familySitiation'].value=="Married" || document.forms['customerFileForm'].elements['customerFile.familySitiation'].value=="Married with children" || document.forms['customerFileForm'].elements['customerFile.familySitiation'].value=="Couple" || document.forms['customerFileForm'].elements['customerFile.familySitiation'].value=="Couple with children") && (document.forms['customerFileForm'].elements['customerFile.custPartnerEmail'].value=="")){
		$j('#getFileErrorMsg').html("<p>Spouse/Partner Email is required in Transferee Details.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originAddress1'].value==""){
		$j('#getFileErrorMsg').html("<p>Transferee Address is required in Present Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originCity'].value==""){
		$j('#getFileErrorMsg').html("<p>City is required in Present Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originZip'].value==""){
		$j('#getFileErrorMsg').html("<p>Postal Code is required in Present Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(originCountryVal==""){
		$j('#getFileErrorMsg').html("<p>Country is required in Present Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if((originCountryVal=="United States" || originCountryVal=="Canada" || originCountryVal=="India") && (document.forms['customerFileForm'].elements['customerFile.originState'].value=='')){
		$j('#getFileErrorMsg').html("<p>State is required in Present Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value==""){
		$j('#getFileErrorMsg').html("<p>City is required in Destination Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(destCountryVal==""){
		$j('#getFileErrorMsg').html("<p>Country is required in Destination Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if((destCountryVal=="United States" || destCountryVal=="Canada" || destCountryVal=="India") && (document.forms['customerFileForm'].elements['customerFile.destinationState'].value=='')){
		$j('#getFileErrorMsg').html("<p>State is required in Destination Address.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.billToReference'].value==""){
		$j('#getFileErrorMsg').html("<p>Cost Centre Field  is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.contactName'].value==""){
		$j('#getFileErrorMsg').html("<p>Contact Person is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value==""){
		$j('#getFileErrorMsg').html("<p>Origin Company Code is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value==""){
		$j('#getFileErrorMsg').html("<p>Destination Company Code is Required Field.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.localHr'].value==""){
		$j('#getFileErrorMsg').html("<p>Name of Mobility/HR Responsible is required in Company Invoicing Information.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.localHrPhone'].value==""){
		$j('#getFileErrorMsg').html("<p>Phone is required in Company Invoicing Information.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}else if(document.forms['customerFileForm'].elements['customerFile.localHrEmail'].value==""){
		$j('#getFileErrorMsg').html("<p>Email Address is required in Company Invoicing Information.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	}<c:if test="${serviceRelosCheck!=''}">
	 else if(document.forms['customerFileForm'].elements['benefitTypeCheckedVal'].value==""){
		$j('#getFileErrorMsg').html("<p>Please select any Benefits type.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		return false;
	} </c:if>

	else{
		if(temp=="save"){
			saveIkeaOrderDetail(); 
		}
		if(temp=="submit"){
			submitIkeaOrderInitiation(); 
		}
	}
}
function saveIkeaOrderDetail(){
	document.forms['customerFileForm'].action ='saveIkeaOrderInitiation.html';
	document.forms['customerFileForm'].submit();
}
function submitIkeaOrderInitiation(){
	var cid=document.forms['customerFileForm'].elements['customerFile.id'].value;
	new Ajax.Request('getDsFamilyDetailsBlankFieldIdAjax.html?ikeaflag=1&ajax=1&decorator=simple&popup=true&cid='+cid,
			{
			method:'get',
			onSuccess: function(transport){
				var response = transport.responseText || "";
				response = response.trim();
				if(response=='Y'){
					alert('Please fill the mandatory fields in family details.');
					return false;
				}else if(response=='N'){
					var familySituation =document.forms['customerFileForm'].elements['customerFile.familySitiation'].value;
			  		var familySize = '${dsFamilyDetailsSize}';
			  		if((familySituation=='Couple' || familySituation=='Married'||familySituation=='Single with children') && familySize < 2){
			  		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [2] lines of family details</p>");
						$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			  		  	return false;
			  	  	}else if ((familySituation=='Married with children' || familySituation=='Couple with children') && familySize < 3){
			  		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [3] lines of family details</p>");
						$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			  		  	return false;
			  	  	}else{
			  	  		document.forms['customerFileForm'].action ='submitIkeaOrderInitiation.html';
						document.forms['customerFileForm'].submit();
			  	  	}
				}else{
					document.forms['customerFileForm'].action ='submitIkeaOrderInitiation.html';
					document.forms['customerFileForm'].submit();
				}
			},
			onFailure: function(){ 
			}
		});
} 

function titleCase(element){ 
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
		upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
		upstring+=spl[i].substring(1, spl[i].length); 
		upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
}

function validateEmail(target){
	   var reg = /^([A-Za-z0-9_\-\.\'])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	   var address = target.value;
	   if(address!=''){
		    if(reg.test(address) == false) {		 
		      alert('Invalid Email Address');
		      target.value='';
		      return false;
		    } 
	   } 
  /* 	var x = target.value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if(target.value!=null && target.value!=''){
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
	    	//alert('Invalid Email Address');
	    	var $j = jQuery.noConflict();
	    	$j('#getFileErrorMsg').html("<p>Invalid Email Address</p>");
	    	$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		    target.value='';
	        return false;
	    }
    } */
}

function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	
function calcDays() {
	document.forms['customerFileForm'].elements['customerFile.duration'].value="";
	var date2 = document.forms['customerFileForm'].elements['customerFile.contractStart'].value;	 
	var date1 = document.forms['customerFileForm'].elements['customerFile.contractEnd'].value; 
	var mySplitResult = date1.split("-");
	var day = mySplitResult[0];
	var month = mySplitResult[1];
	var year = mySplitResult[2];
	if(month == 'Jan'){
	     month = "01";
	 } else if(month == 'Feb'){
	     month = "02";
	 } else if(month == 'Mar'){
	     month = "03"
	 } else if(month == 'Apr'){
	     month = "04"
	 }  else if(month == 'May'){
	     month = "05"
	 } else if(month == 'Jun'){
	     month = "06"
	 }  else if(month == 'Jul'){
	     month = "07"
	 } else if(month == 'Aug'){
	     month = "08"
	 }  else if(month == 'Sep'){
	     month = "09"
	 }  else if(month == 'Oct'){
	     month = "10"
	 }  else if(month == 'Nov'){
	     month = "11"
	 }  else if(month == 'Dec'){
	     month = "12";
	 }
	 var finalDate = month+"-"+day+"-"+year;
	 var mySplitResult2 = date2.split("-");
	 var day2 = mySplitResult2[0];
	 var month2 = mySplitResult2[1];
	 var year2 = mySplitResult2[2];
	 if(month2 == 'Jan'){
	     month2 = "01";
	 }else if(month2 == 'Feb'){
	     month2 = "02";
	 }else if(month2 == 'Mar'){
	     month2 = "03"
	 }  else if(month2 == 'Apr'){
	     month2 = "04"
	 }  else if(month2 == 'May'){
	     month2 = "05"
	 }  else if(month2 == 'Jun'){
	     month2 = "06"
	 }   else if(month2 == 'Jul'){
	     month2 = "07"
	 }  else if(month2 == 'Aug'){
	     month2 = "08"
	 }else if(month2 == 'Sep'){
	     month2 = "09"
	 }  else if(month2 == 'Oct'){
	     month2 = "10"
	 }  else if(month2 == 'Nov'){
	     month2 = "11"
	 }  else if(month2 == 'Dec'){
	     month2 = "12";
	 }
	var finalDate2 = month2+"-"+day2+"-"+year2;
	date1 = finalDate.split("-");
	date2 = finalDate2.split("-");
	var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
	var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
	var diff = Math.round((sDate-eDate)/86400000);
	var  daysApart=0;
	if(diff<0){
	  alert("Contract Start Date must be less than Contract End Date");
	  document.forms['customerFileForm'].elements['customerFile.duration'].value='';
	  document.forms['customerFileForm'].elements['customerFile.contractEnd'].value="";
	}else{
		 daysApart=qryHowOld(sDate, eDate);
	}
	document.forms['customerFileForm'].elements['customerFile.duration'].value = daysApart; 
	if(document.forms['customerFileForm'].elements['customerFile.duration'].value=='NaN'){
	   document.forms['customerFileForm'].elements['customerFile.duration'].value = '';
	} 
	document.forms['customerFileForm'].elements['ikeaOrderForDaysClick'].value = '';
}

function qryHowOld(varAsOfDate, varBirthDate)
{
var dtAsOfDate;
var dtBirth;
var dtAnniversary;
var intSpan;
var intYears;
var intMonths;
var intWeeks;
var intDays;
var intHours;
var intMinutes;
var intSeconds;
var strHowOld;

// get born date
dtBirth = new Date(varBirthDate);
// get as of date
dtAsOfDate = new Date(varAsOfDate);
// if as of date is on or after born date
if ( dtAsOfDate >= dtBirth ){
   // get time span between as of time and birth time
   intSpan = ( dtAsOfDate.getUTCHours() * 3600000 +
               dtAsOfDate.getUTCMinutes() * 60000 +
               dtAsOfDate.getUTCSeconds() * 1000    ) -
             ( dtBirth.getUTCHours() * 3600000 +
               dtBirth.getUTCMinutes() * 60000 +
               dtBirth.getUTCSeconds() * 1000       )

   // start at as of date and look backwards for anniversary 

   // if as of day (date) is after birth day (date) or
   //    as of day (date) is birth day (date) and
   //    as of time is on or after birth time
   if ( dtAsOfDate.getUTCDate() > dtBirth.getUTCDate() ||
        ( dtAsOfDate.getUTCDate() == dtBirth.getUTCDate() && intSpan >= 0 ) ) {

      // most recent day (date) anniversary is in as of month
      dtAnniversary = 
         new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                             dtAsOfDate.getUTCMonth(),
                             dtBirth.getUTCDate(),
                             dtBirth.getUTCHours(),
                             dtBirth.getUTCMinutes(),
                             dtBirth.getUTCSeconds() ) );

      }

   // if as of day (date) is before birth day (date) or
   //    as of day (date) is birth day (date) and
   //    as of time is before birth time
   else
      {

      // most recent day (date) anniversary is in month before as of month
      dtAnniversary = 
         new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                             dtAsOfDate.getUTCMonth() - 1,
                             dtBirth.getUTCDate(),
                             dtBirth.getUTCHours(),
                             dtBirth.getUTCMinutes(),
                             dtBirth.getUTCSeconds() ) );

      // get previous month
      intMonths = dtAsOfDate.getUTCMonth() - 1;
      if ( intMonths == -1 )
         intMonths = 11;

      // while month is not what it is supposed to be (it will be higher)
      while ( dtAnniversary.getUTCMonth() != intMonths )

         // move back one day
         dtAnniversary.setUTCDate( dtAnniversary.getUTCDate() - 1 );

      }

   // if anniversary month is on or after birth month
   if ( dtAnniversary.getUTCMonth() >= dtBirth.getUTCMonth() )
      {

      // months elapsed is anniversary month - birth month
      intMonths = dtAnniversary.getUTCMonth() - dtBirth.getUTCMonth();

      // years elapsed is anniversary year - birth year
      intYears = dtAnniversary.getUTCFullYear() - dtBirth.getUTCFullYear();

      }

   // if birth month is after anniversary month
   else
      {

      // months elapsed is months left in birth year + anniversary month
      intMonths = (11 - dtBirth.getUTCMonth()) + dtAnniversary.getUTCMonth() + 1;

      // years elapsed is year before anniversary year - birth year
      intYears = (dtAnniversary.getUTCFullYear() - 1) - dtBirth.getUTCFullYear();

      }

   // to calculate weeks, days, hours, minutes and seconds
   // we can take the difference from anniversary date and as of date

   // get time span between two dates in milliseconds
   intSpan = dtAsOfDate - dtAnniversary;

   // get number of weeks
  // intWeeks = Math.floor(intSpan / 604800000);

   // subtract weeks from time span
 //  intSpan = intSpan - (intWeeks * 604800000);
   
   // get number of days
   intDays = Math.floor(intSpan / 86400000);

   // subtract days from time span
   intSpan = intSpan - (intDays * 86400000);

   // get number of hours
   intHours = Math.floor(intSpan / 3600000);
 
   // subtract hours from time span
   intSpan = intSpan - (intHours * 3600000);

   // get number of minutes
   intMinutes = Math.floor(intSpan / 60000);

   // subtract minutes from time span
   intSpan = intSpan - (intMinutes * 60000);

   // get number of seconds
   intSeconds = Math.floor(intSpan / 1000);

   // create output string     
   if ( intYears >= 0 )
      if ( intYears > 1 )
         strHowOld = intYears.toString() + 'Y';
      else
         strHowOld = intYears.toString() + 'Y';
   else
      strHowOld = '';

   if ( intMonths > 0 )
      if ( intMonths > 1 )
         strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
      else
         strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
        
  // if ( intWeeks > 0 )
   //   if ( intWeeks > 1 )
    //     strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';
    //  else
     //    strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';

   if ( intDays > 0 )
      if ( intDays > 1 )
         strHowOld = strHowOld + '-' + intDays.toString() + 'D';
      else
         strHowOld = strHowOld + '-' + intDays.toString() + 'D';
   }
else
   strHowOld = ''

// return string representation
return strHowOld
}

function forDays(){
	document.forms['customerFileForm'].elements['ikeaOrderForDaysClick'].value ='1';
}
function massageFamily(){
	var $j = jQuery.noConflict();
	$j('#getFileErrorMsg').html("<p>Please Save Ikea Order Initiation Detail first before adding Family members.</p>");
	$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
}
function sendEmail(target){
 	var originEmail = target;
	var subject = 'C/F# ${customerFile.sequenceNumber}';
	subject = subject+" "+document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	subject = subject+" "+ document.forms['customerFileForm'].elements['customerFile.lastName'].value; 
   	var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);				
	//win = window.open(mailto_link,'emailWindow');
	//if (win && win.open &&!win.closed) win.close(); 
   	window.location.href = mailto_link;
}

function copyCountryValue(type){
	if(type=='dummy'){
		document.forms['customerFileForm'].elements['customerFile.originCountry'].value = document.forms['customerFileForm'].elements['dummyOriginCountry'].value;
		requiredOriginState();
		getOriginCountryCode();
		getState(document.forms['customerFileForm'].elements['customerFile.originCountry'].value);
		enableStateListOrigin();
	}else{
		document.forms['customerFileForm'].elements['dummyOriginCountry'].value = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	}
}

function autoPopulate_customerFile_originCityCode(targetElement, w) {
	var r={
	 'special':/[\W]/g,
	 'quotes':/['\''&'\"']/g,
	 'notnumbers':/[^\d]/g
	};
	if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != ''){
		if(document.forms['customerFileForm'].elements['customerFile.originState'].value != ''){
			document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+','+document.forms['customerFileForm'].elements['customerFile.originState'].value;
		}else{
			document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
			
		}
	}
}

function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
	var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+','+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
		}
	}

function findCityStateOfZipCode(targetElement,targetField){
    var zipCode = targetElement.value;
    if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
      if(document.forms['customerFileForm'].elements['originCountryFlex3Value'].value=='Y'){
       var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
        http33.open("GET", url, true);
        http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
        http33.send(null);
      }
    }else if(targetField=='DZ'){
      var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
        if( document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
            http33.open("GET", url, true);
            http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
            http33.send(null);
           }
      }  
} 

function handleHttpResponseCityState(targetField){
           if (http33.readyState == 4){
             var results = http33.responseText
             results = results.trim();
             var resu = results.replace("[",'');
             resu = resu.replace("]",'');
             resu = resu.split(",");
			for(var i = 0; i < resu.length+1; i++) {
             var res = resu[i];
             res = res.split("#");
             if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.originState'].value=res[2];
		            if (document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=='') {
		           	 	document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=(res[4].substring(0,19));
		            } 
	             } 
           	 document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
           	}else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           		document.forms['customerFileForm'].elements['customerFile.destinationCity'].value=res[0];
	           	 	document.forms['customerFileForm'].elements['customerFile.destinationState'].value=res[2];
		            if(document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=='') {
		           	 	document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=(res[4].substring(0,19));
		           	}
	           	}	
           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
           	} 
      } } else { } }

function getOriginCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
function handleHttpResponseCountryName(){
    if (http4.readyState == 4){
       var results = http4.responseText
       results = results.trim();
       var res=results.split('#');
       if(res.length>=1){
	       	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
	       	if(res[1]=='Y'){
	       		document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
			}else{
			 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
			}try{
			 	document.forms['customerFileForm'].elements['customerFile.originCountry'].select();
			}catch (e){}		     	 
		}else{}
    }
}

function getDestinationCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}

function httpDestinationCountryName(){
         if (http4.readyState == 4){
            var results = http4.responseText
            results = results.trim();
            var res = results.split("#");
            if(res.length>=1){
				document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
				if(res[1]=='Y'){
					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
				}else{
					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
				}try{	
					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].select();
				}catch (e){}
			}else{}
         }
}

function getState(targetElement) {
	var country = '';
	if(targetElement.value==undefined){
		country = targetElement;
	}else{
		country = targetElement.value;
	}
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}

function handleHttpResponse5(){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       res = results.split("@");
       targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
				
				if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
				   document.getElementById("originState").options[i].defaultSelected = true;
				}
			}
		}
		if ('${stateshitFlag}'=="1"){
			document.getElementById("originState").value = '';
		}else{ 
			document.getElementById("originState").value == '${customerFile.originState}';
		}
    }
}
	
function getDestinationState(targetElement){
	var country = targetElement.value; 
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
}
function handleHttpResponse6(){
	if (http3.readyState == 4){
            var results = http3.responseText
            results = results.trim();
            res = results.split("@");
            targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
						if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
						   document.getElementById("destinationState").options[i].defaultSelected = true;
						}
					}
				}
			if ('${stateshitFlag}'=="1"){
				document.getElementById("destinationState").value = '';
			}else{ 
				document.getElementById("destinationState").value == '${customerFile.destinationState}';
			}
    }
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
	  }	        
}
function enableStateListDestin(){ 
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
	  }	        
}
function viewPartnerDetailsForBillToCode(position,codeId) {
    var partnerCode = "";
    var originalCorpID='${customerFile.corpID}';
    partnerCode =  document.getElementById(codeId).value;
    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
    ajax_showTooltip(url,position);
    return false
}
function openBillToCodePopUp(){
	window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_seventhDescription=customerFile.contactEmail&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
}
function openOriginCompanyCodePopUp(){
	window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originCompany&fld_code=customerFile.originCompanyCode');
}
function openDestinationCompanyCodePopUp(){
	window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.destinationCompany&fld_code=customerFile.destinationCompanyCode');
}
function opencurrentEmployeeCodePopUp(){
	window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.currentEmploymentName&fld_code=customerFile.currentEmploymentCode');
}
function openOriginalCompanyPopUp(){
	window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originalCompanyName&fld_code=customerFile.originalCompanyCode');
}
function findIkeaPartnerDetails(e, t, n,s) {
	var $j = jQuery.noConflict();
	var o = s.which ? s.which : s.keyCode;
	if(o != 9){
		var u = document.getElementById(e).value;
		if(u.length <= 3) {
			document.getElementById(t).value = ""
		}
		if(u.length >= 3) {
			$j.get("ikeaOrderPartnerDetailsAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
				partnerNameAutoCopmlete: u,
				partnerNameId: e,
				paertnerCodeId: t,
				autocompleteDivId: n
			}, function(e) {
				document.getElementById(n).style.display = "block";
				$j("#" + n).html(e)
			})
		} else {
			document.getElementById(n).style.display = "none"
		}
	} else {
		document.getElementById(n).style.display = "none"
	}
}
function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";
	if(paertnerCodeId=='billToCodeId'){
		findBillToCode();		
	}
	else
		{
	findOrderInitiationBillToCode(autocompleteDivId);
		}
}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
}
function openOriginLocation() {
    var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
    var country = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
    var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
    var address = "";
    address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
    address = address.replace("#","-");
    var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
}
 function openDestinationLocation() {
    var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
    var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
    var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
   	var address = "";
    address = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
   	address = address.replace("#","-");
    var state = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
}
 function findOrderInitiationBillToCode(autocompleteDivId){
		var bCode = '';
		if(autocompleteDivId=='billToNameDiv'){
			bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		}
	    else{
			bCode = document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].value;
		}
		
		bCode=bCode.trim();
		if(bCode!='') {
		    var url="findOrderInitiationBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		    http5551.open("GET", url, true); 
		    http5551.onreadystatechange = function(){findOrderInitiationBillToCodeHttp(autocompleteDivId);};
		    http5551.send(null);	     
	     }else{
	    	if(autocompleteDivId=='billToNameDiv'){
	    		document.forms['customerFileForm'].elements['customerFile.billToName'].value ="";
	    	}
	    	else{
	    		document.forms['customerFileForm'].elements['customerFile.originalCompanyName'].value =""; 
	    	 }
	     }
	 }
function findOrderInitiationBillToCodeHttp(autocompleteDivId){
    if (http5551.readyState == 4){    	
    	var results = http5551.responseText
    	results = results.trim();
    	results=results.replace("[","");
        results=results.replace("]","");
    	if(results.length >1){
    		if(autocompleteDivId=='billToNameDiv'){
		 		document.forms['customerFileForm'].elements['customerFile.billToName'].value = results;
    		}
    		else{
    			document.forms['customerFileForm'].elements['customerFile.originalCompanyName'].value = results;
    		}
    	}else{
    		if(autocompleteDivId=='billToNameDiv'){
	    	    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	    		alert("You are not authorize to select bill to code "+billToCode+ ".")
	    		document.forms['customerFileForm'].elements['customerFile.billToCode'].value="${customerFile.billToCode}";
	    		document.forms['customerFileForm'].elements['customerFile.billToName'].value="${customerFile.billToName}";
    		}
    		else{
    			var billToCode = document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].value;
	    		alert("You are not authorize to select bill to code "+billToCode+ ".")
	    		document.forms['customerFileForm'].elements['customerFile.billToCode'].value="${customerFile.originalCompanyCode}";
	    		document.forms['customerFileForm'].elements['customerFile.billToName'].value="${customerFile.originalCompanyName}";
    		}
    	}
    }
}
function allReadOnly(){ 
    if(document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value=="Submitted"){ 
       var numOfElements = document.forms['customerFileForm'].elements.length;  
       for(var i=0; i<numOfElements;i++){
        	document.forms['customerFileForm'].elements[i].disabled=true; 
        }
    }
    if(document.images) {
	    var totalImages = document.images.length;
	    	for (var i=0;i<totalImages;i++) {
					if(document.images[i].src.indexOf('calender.png')>0) { 
						var el = document.getElementById(document.images[i].id);  
						document.images[i].src = 'images/navarrow.gif'; 
						if((el.getAttribute("id")).indexOf('trigger')>0){ 
							el.removeAttribute('id');
						}
					}
					if(document.images[i].src.indexOf('open-popup.gif')>0){  
						var el = document.getElementById(document.images[i].id); 
						try{
							el.onclick = false;
						}catch(e){}
					    	document.images[i].src = 'images/navarrow.gif';
					}
					if(document.images[i].src.indexOf('notes_empty1.jpg')>0) {
						var el = document.getElementById(document.images[i].id);
						try{
							el.onclick = false;
						}catch(e){}
				        	document.images[i].src = 'images/navarrow.gif';
					}
					if(document.images[i].src.indexOf('notes_open1.jpg')>0) {
						var el = document.getElementById(document.images[i].id);
						try{
							el.onclick =false;
						}catch(e){}
							document.images[i].src = 'images/navarrow.gif';
					} 				
					if(document.images[i].src.indexOf('images/nav')>0) {
						var el = document.getElementById(document.images[i].id);
						try{
							el.onclick = false;
						}catch(e){}
							document.images[i].src = 'images/navarrow.gif';
					}
					if(document.images[i].src.indexOf('images/image')>0) {
						var el = document.getElementById(document.images[i].id);
						try{
							el.onclick = false;
						}catch(e){}
							document.images[i].src = 'images/navarrow.gif';
					}
					if(document.images[i].src.indexOf('email_small.gif')>0) {
						var el = document.getElementById(document.images[i].id);
						try{
							el.onclick =false;
						}catch(e){}
							document.images[i].src = 'images/navarrow.gif';
					}
					/* if(document.images[i].src.indexOf('globe.png')>0) {
						var el = document.getElementById(document.images[i].id);
						try{
							el.onclick =false;
						}catch(e){}
							document.images[i].src = 'images/navarrow.gif';
					} */
				} 
		 	}
	//document.forms['customerFileForm'].elements['Cancel'].disabled=false; 
}
function autoPopulate_customerFile_originCountry(targetElement) {		
 	var oriCountry = targetElement.value;
 	var countryCode="";
	<c:forEach var="entry" items="${countryCod}">
	if(oriCountry=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
    var enbState = '${enbState}';
	var index = (enbState.indexOf(oriCountry)> -1);
	if(index != ''){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
			if(oriCountry == 'United States'){
			setTimeout(function() {document.forms['customerFileForm'].elements['customerFile.originZip'].focus(); }, 50);
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
			autoPopulate_customerFile_originCityCode(countryCode,'special');
		}
}

function autoPopulate_customerFile_destinationCountry(targetElement) {   
	var dCountry = targetElement.value;
	var countryCode="";
	<c:forEach var="entry" items="${countryCod}">
	if(dCountry=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
    var enbState = '${enbState}';
	var index = (enbState.indexOf(dCountry)> -1);
	if(index != ''){
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;			
		if(dCountry == 'United States'){
			setTimeout(function() { document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus(); }, 50);
		}
	}else{
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
		document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
		autoPopulate_customerFile_destinationCityCode(countryCode,'special');
	}
}

function requiredDestinState(){
    var destinCountry = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value; 
	var r2=document.getElementById('destinationStateNotRequired');
	var r1=document.getElementById('destinationStateRequired');
	if(destinCountry == 'United States' || destinCountry == 'Canada' || destinCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(destinCountry)> -1);
		if(index != ''){
			r1.style.display = 'block';
			r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}

function requiredOriginState(){
    var originCountry = document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 
	var r2=document.getElementById('originStateNotRequired');
	var r1=document.getElementById('originStateRequired');
	if(originCountry == 'United States' || originCountry == 'Canada' || originCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(originCountry)> -1);
		if(index != ''){
			r1.style.display = 'block';
			r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}

function spousePartnerEmailRequiredMethod(){
    var assignType = document.forms['customerFileForm'].elements['customerFile.assignmentType'].value;
    var familySituationVal = document.forms['customerFileForm'].elements['customerFile.familySitiation'].value; 
	var r2=document.getElementById('spousePartnerEmailNotRequired');
	var r1=document.getElementById('spousePartnerEmailRequired');
	
	
	if (assignType!='STA' && (familySituationVal == 'Married' || familySituationVal == 'Married with children'||familySituationVal == 'Couple'||familySituationVal=='Couple with children')){	
		r1.style.display = 'block';
		r2.style.display = 'none';
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}

var http3 = getHTTPObject1();
var http33 = getHTTPObject33();
var http4 = getHTTPObject3();
var httpState = getHTTPObjectState();
var http5551 = getHTTPObject51();
var http6661=getHTTPObject61();
var http7771=getHTTPObject71();
var http8881=getHTTPObject81();
var http9991=getHttpObject91();
var httpPortalAccess = getHTTPObjectPortalAccess();

function getHTTPObjectPortalAccess()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject61(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject71(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject81(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHttpObject91(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject51(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject33(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject3(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
function getHTTPObjectState(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject1(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
try{
	spousePartnerEmailRequiredMethod();
	changeEndDate();
	document.forms['customerFileForm'].elements['dummyOriginCountry'].value = document.forms['customerFileForm'].elements['customerFile.originCountry'].value
	requiredOriginState();	
	autoPopulate_customerFile_originCountry(document.forms['customerFileForm'].elements['customerFile.originCountry']);
}catch(e){}
try{
 	var enbState = '${enbState}';
 	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
 		if(oriCon=="") {
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
 		}else{	
			if(enbState.indexOf(oriCon)> -1){
				document.getElementById('originState').disabled = false; 
				document.getElementById('originState').value='${customerFile.originState}';									
			}else{
				document.getElementById('originState').disabled = true;
				document.getElementById('originState').value ="";						
		}
	}
 }catch(e){}
 try{
	requiredDestinState();	
	autoPopulate_customerFile_destinationCountry(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
	}catch(e){}
 
try{
 	var enbState = '${enbState}';
 	var oriCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
 		if(oriCon=="") {
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
 		}else{	
			if(enbState.indexOf(oriCon)> -1){
				document.getElementById('destinationState').disabled = false; 
				document.getElementById('destinationState').value='${customerFile.destinationState}';									
			}else{
				document.getElementById('destinationState').disabled = true;
				document.getElementById('destinationState').value ="";						
		}
	}
 }catch(e){}
 try{
	if(document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value=="Submitted"){
		allReadOnly();
	}
}catch(e){}
<c:if test="${not empty customerFile.id}">
 	var $j = jQuery.noConflict();
	var familySituationVal ="<%=request.getAttribute("familySituationValue")%>";
	familySituationVal = familySituationVal.trim();
	var firstTimeSaved = '${newData}';
	if((familySituationVal==null || familySituationVal=='' || familySituationVal=='null') && (firstTimeSaved=='Yes')){
		var familySituation ='${customerFile.familySitiation}';
		var familySize = '${dsFamilyDetailsSize}';
		if((familySituation=='Couple' || familySituation=='Married') && familySize < 2){
		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [2] lines of family details.<BR>Please add details of the co-worker.</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  	}
		if((familySituation=='Single with children') && familySize < 2){
		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [2] lines of family details.</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  	}
		if((familySituation=='Married with children' || familySituation=='Couple with children') && familySize < 3){
		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [3] lines of family details.<BR>Please add details of the co-worker.</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  	}
		var dsId = '${dsFamilyDetailsId}';
		if((dsId!=null && dsId!='') && (firstTimeSaved=='Yes')){
			javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsId}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,510);
		}
	}
</c:if>
var familySituationVal ="<%=request.getAttribute("familySituationValue")%>";
familySituationVal = familySituationVal.trim();
var familySize = '${dsFamilyDetailsSize}';
var firstTimeSaved = '${newData}';
if((familySituationVal!=null && familySituationVal!='' && familySituationVal!='null') && (firstTimeSaved=='Yes')){
	  if((familySituationVal=='Couple' || familySituationVal=='Married') && familySize < 2){
		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituationVal+"]. For this status we expect [2] lines of family details.<BR>Please add details of the co-worker.</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  }
	  if((familySituationVal=='Single with children') && familySize < 2){
		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituationVal+"]. For this status we expect [2] lines of family details.</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  }
	  if((familySituationVal=='Married with children' || familySituation=='Couple with children') && familySize < 3){
		  	$j('#getFileErrorMsg').html("<p>The Family Status is ["+familySituationVal+"]. For this status we expect [3] lines of family details.<BR>Please add details of the co-worker.</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  }
	  var dsId = '${dsFamilyDetailsId}';
	  if((dsId!=null && dsId!='') && (firstTimeSaved=='Yes')){
		  javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsId}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,510);
	  }
}
var field = 'newData';
var url = window.location.href;
if(url.indexOf('&' + field + '=') != -1){
	  removeParam("newData");
}
function removeParam(parameter){
	  var url=document.location.href;
	  var urlparts= url.split('?');

	 if (urlparts.length>=2){
	  var urlBase=urlparts.shift(); 
	  var queryString=urlparts.join("?"); 

	  var prefix = encodeURIComponent(parameter)+'=';
	  var pars = queryString.split(/[&;]/g);
	  for (var i= pars.length; i-->0;)               
	      if (pars[i].lastIndexOf(prefix, 0)!==-1)   
	          pars.splice(i, 1);
	  url = urlBase+'?'+pars.join('&');
	  window.history.pushState('',document.title,url);

	}
}

<c:if test="${not empty customerFile.id}"> 
<c:if test="${customerFile.firstInternationalMove}">
document.forms['customerFileForm'].elements['firstInternationalMoveDummy'].value='Yes'
</c:if>
<c:if test="${!customerFile.firstInternationalMove}">
document.forms['customerFileForm'].elements['firstInternationalMoveDummy'].value='No'
</c:if>
</c:if>
function selectValue(val)
{ 
	if(document.forms['customerFileForm'].elements['firstInternationalMoveDummy'].value=='Yes'){
	document.forms['customerFileForm'].elements['customerFile.firstInternationalMove'].value=true;
	}
	if(document.forms['customerFileForm'].elements['firstInternationalMoveDummy'].value=='No'){
		document.forms['customerFileForm'].elements['customerFile.firstInternationalMove'].value=false;
	}
}
function changeValue1(val)
{ 
	if(val.value!=null)
		{
		document.forms['customerFileForm'].elements['invoiceEmailAddress'].value=val.value;
		}
}
function changeValue2(val)
{ 
	if(val.value!=null)
		{
		document.forms['customerFileForm'].elements['contactPerson'].value=val.value;
		}
}
function getPortalDefaultPartnerData(){
	var partnerCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="partnerPrivatePortalDefaultData.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	httpPortalAccess.open("GET", url, true);
	httpPortalAccess.onreadystatechange = httpPortalDefaultPartnerData;
	httpPortalAccess.send(null);
}
function httpPortalDefaultPartnerData(){
     if (httpPortalAccess.readyState == 4){
                var results = httpPortalAccess.responseText
                results = results.trim();
                if(results!=""){
                var res = results.split("~"); 
                if(res[0]!="NODATA"){
                 document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0]; 
                }
                if(res[1]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value = res[1]; 
                }
                if(res[2]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personPricing'].value = res[2]; 
                }
                if(res[3]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personBilling'].value = res[3]; 
                }
                if(res[4]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personPayable'].value = res[4]; 
                }
                if(res[5]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.auditor'].value = res[5]; 
                }
                /* if(res[6]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.coordinator'].value = res[6]; 
                } */
                if(res[7]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.source'].value = res[7]; 
                }
                if(res[8]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[8]; 
                }
                if(res[9]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.contract'].value = res[9]; 
                }
                if(res[10]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.accountCode'].value = res[10]; 
                }
                if(res[11]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.accountName'].value = res[11]; 
                }
                }
     }
}

function changeEndDate(){
    var assignType = document.forms['customerFileForm'].elements['customerFile.assignmentType'].value; 
	var r2=document.getElementById('endDateNotRequired');
	var r1=document.getElementById('endDateRequired');
	if(assignType == 'STA'||assignType == 'Commuter'|| assignType=='IA International Assignment'){	
	       r1.style.display = 'block';
			r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
         }
         
         
function findCurrentEmployCode(){
	var bCode = '';
	bCode = document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value;
	bCode=bCode.trim();
    if(bCode!='') {
	    var url="findOrderInitiationBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http6661.open("GET", url, true); 
	    http6661.onreadystatechange = function(){findCurrentEmployCodeHttp();};
	    http6661.send(null);	     
     }else{
    		document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value =""; 
    }
 }
function findCurrentEmployCodeHttp(){
if (http6661.readyState == 4){    	
	var results = http6661.responseText
	results = results.trim();
	results=results.replace("[","");
    results=results.replace("]","");
	if(results.length >1){
		
			document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value = results;
		
	}else{
		var billToCode = document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value;
    		alert("You are not authorize to select Current Employement Code "+billToCode+ ".")
    		document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value="${customerFile.currentEmploymentCode}";
    		document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value="${customerFile.currentEmploymentName}";
		}
	}
}

function findOriginCompanyCode(){
	var bCode = '';
	bCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
	bCode=bCode.trim();
	if(bCode!='') {
	    var url="findOrderInitiationBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http7771.open("GET", url, true); 
	    http7771.onreadystatechange = function(){findOriginCompanyCodeHttp();};
	    http7771.send(null);	     
     }else{
    	
    		document.forms['customerFileForm'].elements['customerFile.originCompany'].value =""; 
    	
     }
 }
function findOriginCompanyCodeHttp(){
if (http7771.readyState == 4){    	
	var results = http7771.responseText
	results = results.trim();
	results=results.replace("[","");
    results=results.replace("]","");
	if(results.length >1){
		
			document.forms['customerFileForm'].elements['customerFile.originCompany'].value = results;
		
	}else{
		var billToCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
    		alert("You are not authorize to select Origin Company code "+billToCode+ ".")
    		document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value="${customerFile.originCompanyCode}";
    		document.forms['customerFileForm'].elements['customerFile.originCompany'].value="${customerFile.originCompany}";
		}
	}
}



function findDestinationCompanyCode(){
	var bCode = '';
	bCode = document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value;
	bCode=bCode.trim();
	if(bCode!='') {
	    var url="findOrderInitiationBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http8881.open("GET", url, true); 
	    http8881.onreadystatechange = function(){findDestinationCompanyCodeHttp();};
	    http8881.send(null);	     
     }else{
    	
    		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value =""; 
    	
     }
 }
function findDestinationCompanyCodeHttp(){
if (http8881.readyState == 4){    	
	var results = http8881.responseText
	results = results.trim();
	results=results.replace("[","");
    results=results.replace("]","");
	if(results.length >1){
		
			document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value = results;
		
	}else{
		var billToCode = document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value;
    		alert("You are not authorize to select Destination Company code "+billToCode+ ".")
    		document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value="${customerFile.destinationCompanyCode}";
    		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value="${customerFile.destinationCompany}";
		}
	}
}



function findBillToCode(){
	var bCode = '';
	bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	bCode=bCode.trim();
	if(bCode!='') {
		var url="findIkeaInitiationEmail.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		http9991.open("GET", url, true); 
	    http9991.onreadystatechange = function(){findBillToCodeHttp();};
	    http9991.send(null);	     
     }else{
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value =""; 
    }
 }
function findBillToCodeHttp(){
if (http9991.readyState == 4){
	var results = http9991.responseText
	results = results.trim();
	results=results.replace("[","");
    results=results.replace("]","");
    if(results!=''){
	results = results.split("#");
    document.forms['customerFileForm'].elements['customerFile.billToName'].value = results[0];
    document.forms['customerFileForm'].elements['customerFile.contactEmail'].value = results[1];
    getPortalDefaultPartnerData();
  }else{
		var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    		alert("You are not authorize to select bill to code "+billToCode+ ".")
    		document.forms['customerFileForm'].elements['customerFile.billToCode'].value="${customerFile.billToCode}";
    		document.forms['customerFileForm'].elements['customerFile.billToName'].value="${customerFile.billToName}";
		}
	}
}
function checkAll(temp){
    document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = "";
    var len = document.forms['customerFileForm'].elements['checkV'].length;
    if(temp.checked){
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = true ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
    else{
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = false ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
}
function checkAllRelo(){ 
	<c:if test="${serviceRelosCheck!=''}"> 
	var len = document.forms['customerFileForm'].elements['checkV'].length;
	var check=true;
	for (i = 0; i < len; i++){ 
	        if(document.forms['customerFileForm'].elements['checkV'][i].checked == false ){
	        check=false;
	        i=len+1
	        }
	    } 
	    if(check){
	    document.forms['customerFileForm'].elements['checkAllRelo'].checked=true;
	    }else{
	    document.forms['customerFileForm'].elements['checkAllRelo'].checked=false;
	    }
	</c:if>    
	}
function checkColumn(){
	var userCheckStatus ="";
	  userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
	if(userCheckStatus!=""){ 
	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value="";
	var column = userCheckStatus.split(","); 
	for(i = 0; i<column.length ; i++)
	{    
	     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
	     var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' +column[i];   
	     document.getElementById(column[i]).checked=true;
	} 
	}
	if(userCheckStatus==""){

	}
	}
function selectColumn(targetElement) 
{  
var rowId=targetElement.value;  
if(targetElement.checked)
  {
   var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
   if(userCheckStatus == '')
   {
	  	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = rowId;
   }
   else
   {
    var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' + rowId;
   document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
   }
 }
if(targetElement.checked==false)
 {
  var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
  var userCheckStatus=document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( rowId , '' );
  document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
  } 
 }
</script>   


