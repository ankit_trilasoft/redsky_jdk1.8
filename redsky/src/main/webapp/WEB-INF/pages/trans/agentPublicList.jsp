<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title>Agent List </title>   
    <meta name="heading" content="<fmt:message key='agentList.title'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
      <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
   
<script language="javascript" type="text/javascript">
 
function clear_fields(){
	     document.forms['agentListForm'].elements['agentRequest.aliasName'].value = '';
		document.forms['agentListForm'].elements['agentRequest.lastName'].value = '';
		
		document.forms['agentListForm'].elements['agentRequest.partnerCode'].value = '';
		document.forms['agentListForm'].elements['countryCodeSearch'].value = '';
		document.forms['agentListForm'].elements['stateSearch'].value = '';
		document.forms['agentListForm'].elements['countrySearch'].value = '';
		document.forms['agentListForm'].elements['agentRequest.status'].value = '';
		document.forms['agentListForm'].elements['agentRequest.isPrivateParty'].checked = false;
		document.forms['agentListForm'].elements['agentRequest.isAccount'].checked = false;
		document.forms['agentListForm'].elements['agentRequest.isAgent'].checked = false;
		document.forms['agentListForm'].elements['agentRequest.isVendor'].checked = false;
		document.forms['agentListForm'].elements['agentRequest.isCarrier'].checked = false;
		document.forms['agentListForm'].elements['agentRequest.isOwnerOp'].checked = false;
		document.forms['agentListForm'].elements['isIgnoreInactive'].checked = false;
		document.forms['agentListForm'].elements['partnerPrivate.extReference'].value = '';
		document.forms['agentListForm'].elements['agentRequest.fidiNumber'].value = '';
		document.forms['agentListForm'].elements['agentRequest.OMNINumber'].value = '';
		document.forms['agentListForm'].elements['agentRequest.AMSANumber'].value = '';
		document.forms['agentListForm'].elements['agentRequest.WERCNumber'].value = '';
		document.forms['agentListForm'].elements['agentRequest.IAMNumber'].value = '';
		document.forms['agentListForm'].elements['agentRequest.utsNumber'].value = '';
		document.forms['agentListForm'].elements['agentRequest.eurovanNetwork'].value = '';
		document.forms['agentListForm'].elements['agentRequest.PAIMA'].value = '';
		document.forms['agentListForm'].elements['agentRequest.LACMA'].value = '';
}
function activBtn(temp){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
    var len = temp.length;
    var val="" ;
    if(len == undefined){
		if(temp.checked){
			val= temp.value;
		}
    }
		
	  for(var i = 0; i < len; i++) {
		if(temp[i].checked) {
			val = temp[i].value;
		}
	}

	if(val=='AG'){
		document.forms['addPartner'].elements['chkAgentTrue'].value='Y';
	}else{
		document.forms['addPartner'].elements['chkAgentTrue'].value='';	
	}
}

function seachValidate(){
		var pp =  document.forms['agentListForm'].elements['agentRequest.isPrivateParty'].checked;
		var ac =  document.forms['agentListForm'].elements['agentRequest.isAccount'].checked;
		var ag =  document.forms['agentListForm'].elements['agentRequest.isAgent'].checked;
		var vd =  document.forms['agentListForm'].elements['agentRequest.isVendor'].checked;
		var cr =  document.forms['agentListForm'].elements['agentRequest.isCarrier'].checked;
		var oo =  document.forms['agentListForm'].elements['agentRequest.isOwnerOp'].checked;
		if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
			alert('Please select atleast one agentRequest type.');
			return false;
		} 
		
}


function findVanLineCode(partnerCode,position){
	var url="findVanLineCodeList.html?ajax=1&decorator=simple&popup=true&partnerVanLineCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);
}

</script>
<style>
  span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
.btn-group-sm > .btn, .btn-sm {    
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12) !important;
}
.btn.focus, .btn:focus, .btn:hover {outline:none !important;}

.alert { margin-bottom: 0px;}

#modalAlert{
    padding: 10px!important;
    overflow-y: inherit!important;
    margin-top: 0px!important;
}
.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
   .key_so_dashboard { background: url("images/key_partner_list.jpg") no-repeat scroll 0 0 transparent; cursor: default; height: 22px; margin-left:22%; }
   
.key_partner_list_sm {
    background: url(images/key_partner_list_sm.jpg) no-repeat scroll 0 0 transparent;
    cursor: default;
    height: 23px;
    margin-left: 62%;
}
 #ajax_tooltipObj .ajax_tooltip_content {background-color:#FFF !important; 
 } 
 .update-btn-OI {background:-moz-linear-gradient(top,#e6f8ff,#fff); color:#5e5e5e;
}
.table td, .table th, .tableHeaderTable td {border:1px solid #d8d8d8;}
span.pagelinks {margin-top:-5px !important;font-size:0.9em !important;}
.beta {color:#ee0909;font-style:italic;font-size:11px;font-family:arial,verdana;font-weight:bold;}
.radiobtn {margin:0px 0px 2px 0px;!padding-bottom:5px;!margin-bottom:5px;}
form {margin-top:-10px;!margin-top:0px;}

div#main {margin:-5px 0 0;}
input[type="checkbox"] {vertical-align:middle;}
.br-n-ctnr {border:none !important;text-align:center !important;}
.br-n-rgt {border:none !important;text-align:right !important;}



</style>
</head>
<c:set var="buttons">     
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editPartnerPublic.html"/>'" value="<fmt:message key="button.add"/>"/>         
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="agentListForm" action="searchAgent.html" method="post" >   
     <c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" /> 
	
<s:hidden name="counter" id="counter" />
<div id="layer5" style="width:100%">
<div id="newmnav">
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
		<li><a href="geoCodeMaps.html?partnerOption=notView" ><span>Geo Search</span></a></li>					  	
	  </ul>
</div>
<div class="spnblk" style="margin-top:-10px;">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-2px;"><span></span></div>
<div class="center-content">
<table class="table" border="0" style="width:99%;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th>Last/Company Name</th>
			<th>Alias Name</th>
			<th>Country Code</th>
		
			<th>State Code</th>
			<th><fmt:message key="partner.status"/></th>
			<th></th>
			</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="agentRequest.partnerCode" size="13" cssClass="input-text"/></td>
			<td><s:textfield name="agentRequest.lastName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="agentRequest.aliasName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="13" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="13" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="agentRequest.status" list="%{agentRequestStatus}" cssStyle="width:95px" headerKey="" headerValue=""/></td>
			<s:hidden name="agentRequest.isAgent" value="true"/>
			<s:hidden key="isIgnoreInactive" value="true"/>
			<td width="220px" style="border:none;"><c:out value="${searchbuttons}" escapeXml="false"/></td> 
		</tr> 
	</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:35px;!margin-top:45px;"><span></span></div>
</div>
</div>
</div>
</s:form>
<div id="layer1" style="width:100%">

		<div style="float:left; " id="newmnav">
		  <ul>
		   <c:if test="${paramValue == 'View'}">
					<li><a href="partnerView.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
		    <li style="background:#FFF" id="newmnav1"><a class="current"><span>Agent Change Request</span></a></li>	    
		  </ul>		  
		</div>
		<div id="Key" class="key_partner_list_sm" style="float:left;width: 392px;margin-left:15px;">&nbsp;</div>
		<div class="spnblk" style="width:100%">&nbsp;</div>
<s:set name="agentList" value="agentList" scope="request"/> 
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility> 
<display:table name="agentList" class="table" requestURI="" id="agentLists" export="false" pagesize="10" style="width:100%;margin-left:5px;">  		
<display:column titleKey="partner.name" sortable="true" ><c:out value="${agentLists.lastName}" /></display:column>		
<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" />
<display:column property="stateName" sortable="true"  titleKey="partner.billingState" />
<display:column property="cityName" sortable="true" titleKey="partner.billingCity" />
<display:column title="Status" sortable="true" style="${agentLists.status == 'Rejected' ? 'background-color: #f2dede' : ''} ${agentLists.status == 'Approved' ? 'background-color: #d9edf7' : ''} ${agentLists.status == 'New' ? 'background-color: #dff0d8' : ''} ${agentLists.status == 'Edit' ? 'background-color: #dff0d8' : ''}">
    <c:out value="${agentLists.status}"/>
    <c:if test="${agentLists.status == 'Rejected'}">
         <a><img src="images/viewAgent.png" onclick="findCustomInfo(this,'${agentLists.id}');" alt="Agent Request Message" title="Agent Request Message" /></a					 
   </c:if> 
</display:column>
<display:column title="Action" style="width:155px" >
		<c:if test="${agentLists.isAgent == true}">  
        <c:if test="${agentLists.status != 'Approved'}">
    	 <c:if test="${paramValue == 'View'}">
    	 <button type="button" title="Edit" alt="Edit" class="btn btn-success btn-xs" id="edit${agentLists.id}" onclick="window.open('agentRequestFinal.html?partnerId=${agentLists.partnerId}&id=${agentLists.id}&partnerType=AG&paramView=View')" ><i class='fa fa-pencil'></i>&nbsp;Edit</button>
    	 </c:if>
    	 <c:if test="${paramValue != 'View'}">
    	<button type="button" title="Edit" alt="Edit" class="btn btn-success btn-xs" id="edit${agentLists.id}" onclick="window.open('agentRequestFinal.html?partnerId=${agentLists.partnerId}&id=${agentLists.id}&partnerType=AG')" ><i class='fa fa-pencil'></i>&nbsp;Edit</button>
	   </c:if>
	    </c:if>
	    <c:if test="${agentLists.status == 'Approved'}">
	    <c:if test="${paramValue == 'View'}">
    	 <button type="button" title="View" alt="View" class="btn btn-primary btn-xs" id="edit${agentLists.id}" onclick="window.open('agentRequestFinal.html?partnerId=${agentLists.partnerId}&id=${agentLists.id}&partnerType=AG&paramView=View')" ><i class='fa fa-eye'></i>&nbsp;View</button>
    	 </c:if>
    	 <c:if test="${paramValue != 'View'}">
    	<button type="button" title="View" alt="View" class="btn btn-primary btn-xs" id="edit${agentLists.id}" onclick="window.open('agentRequestFinal.html?partnerId=${agentLists.partnerId}&id=${agentLists.id}&partnerType=AG')" ><i class='fa fa-eye'></i>&nbsp;View</button>
	   </c:if>
	    </c:if>
	    </c:if>
	  <div id="agentDetailList${agentLists.id}" class="modal fade" ></div>
          <c:if test="${agentLists.status == 'Rejected'}">
		  <button type="button" class="btn btn-primary btn-xs" id="editProcess${agentLists.id}" onclick="resendRequest('${agentLists.id}')" ><i class='fa fa-history'></i> Resend</button>	
         </c:if> 
         
</display:column>


    <display:setProperty name="paging.banner.item_name" value="Partner Public"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Public"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>
</div>

<c:set var="isTrue" value="false" scope="session"/>
<script type="text/javascript">
try{
    document.forms['addPartner'].elements['addBtn'].disabled = true;	
    function submit_addPartner()
	{
    	
	verify_radio("addPartner","partnerType");
	}
	
	function verify_radio(frmName, rbGroupName)
    {
	var radios = document[frmName].elements[rbGroupName];
			for (var i=0; i <radios.length; i++) {
			if (radios[i].checked) {
				
				if(radios[i].value=='AG')
					{
					  document.forms['addPartner'].action ='agentRequestFinal.html';
					document.forms['addPartner'].submit();
					
					}
				else{
					
					 document.forms['addPartner'].action ='editPartnerPublic.html';
						document.forms['addPartner'].submit();
				}
				
			   return true;
			  }
			 }
			 return false;
			}
}
catch(e){}


function handleHttpResponse444412(target)
{

if (http2.readyState == 4)
{
	var results = http2.responseText
	results = results.trim();
}}


var http2 = getHTTPObject2(); 
function getHTTPObject2() {
	var xmlhttp;
	if(window.XMLHttpRequest)  {
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp) {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
	}
	return xmlhttp;
}
function ajax_showTooltip1(e,t){if(!ajax_tooltipObj){ajax_tooltipObj=document.createElement("DIV");ajax_tooltipObj.style.position="absolute";ajax_tooltipObj.id="ajax_tooltipObj";document.body.appendChild(ajax_tooltipObj);var n=document.createElement("DIV");n.className="ajax_tooltip_arrow";n.id="ajax_tooltip_arrow";ajax_tooltipObj.appendChild(n);var r=document.createElement("DIV");r.className="ajax_tooltip_content";ajax_tooltipObj.appendChild(r);r.id="ajax_tooltip_content";if(ajax_tooltip_MSIE){ajax_tooltipObj_iframe=document.createElement('<IFRAME frameborder="0">');ajax_tooltipObj_iframe.style.position="absolute";ajax_tooltipObj_iframe.border="0";ajax_tooltipObj_iframe.frameborder=0;ajax_tooltipObj_iframe.style.backgroundColor="#FFF";ajax_tooltipObj_iframe.src="about:blank";r.appendChild(ajax_tooltipObj_iframe);ajax_tooltipObj_iframe.style.left="0px";ajax_tooltipObj_iframe.style.top="0px"}}ajax_tooltipObj.style.display="block";ajax_loadContent("ajax_tooltip_content",e);if(ajax_tooltip_MSIE){ajax_tooltipObj_iframe.style.width=ajax_tooltipObj.clientWidth+"px";ajax_tooltipObj_iframe.style.height=ajax_tooltipObj.clientHeight+"px"}ajax_positionTooltip(t)}function ajax_positionTooltip(e){var t=ajaxTooltip_getLeftPos(e)+e.offsetWidth;var n=ajaxTooltip_getTopPos(e);var r=document.getElementById("ajax_tooltip_content").offsetWidth+document.getElementById("ajax_tooltip_arrow").offsetWidth;ajax_tooltipObj.style.left=t-20+"px";ajax_tooltipObj.style.width=100+"%";ajax_tooltipObj.style.top=n-25+"px"}function ajaxTooltip_getTopPos(e){var t=e.offsetTop;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetTop}return t}function ajaxTooltip_getLeftPos(e){var t=e.offsetLeft;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetLeft}return t}function ajax_hideTooltip(){if(ajax_tooltipObj){ajax_tooltipObj.style.display="none"}if(ajax_tooltipObjBig){ajax_tooltipObjBig.style.display="none"}}function ajax_showBigTooltip(e,t){if(!ajax_tooltipObjBig){ajax_tooltipObjBig=document.createElement("DIV");ajax_tooltipObjBig.style.position="absolute";ajax_tooltipObjBig.id="ajax_big_tooltipObj";document.body.appendChild(ajax_tooltipObjBig);var n=document.createElement("DIV");n.className="ajax_big_tooltip_arrow";n.id="ajax_big_tooltip_arrow";ajax_tooltipObjBig.appendChild(n);var r=document.createElement("DIV");r.className="ajax_big_tooltip_content";ajax_tooltipObjBig.appendChild(r);r.id="ajax_big_tooltip_content";if(ajax_tooltipBig_MSIE){ajax_tooltipObjBig_iframe=document.createElement('<IFRAME frameborder="0">');ajax_tooltipObjBig_iframe.style.position="absolute";ajax_tooltipObjBig_iframe.border="0";ajax_tooltipObjBig_iframe.frameborder=0;ajax_tooltipObjBig_iframe.style.backgroundColor="#FFF";ajax_tooltipObjBig_iframe.src="about:blank";r.appendChild(ajax_tooltipObj_iframe);ajax_tooltipObjBig_iframe.style.left="0px";ajax_tooltipObjBig_iframe.style.top="0px"}}ajax_tooltipObjBig.style.display="block";ajax_loadContent("ajax_big_tooltip_content",e);if(ajax_tooltipBig_MSIE){ajax_tooltipObjBig_iframe.style.width=ajax_tooltipObjBig.clientWidth+"px";ajax_tooltipObjBig_iframe.style.height=ajax_tooltipObjBig.clientHeight+"px"}ajax_positionBigTooltip(t)}function ajax_positionBigTooltip(e){var t=ajaxTooltip_getLeftPos(e)+e.offsetWidth;var n=ajaxTooltip_getTopPos(e);var r=document.getElementById("ajax_big_tooltip_content").offsetWidth+document.getElementById("ajax_big_tooltip_arrow").offsetWidth;ajax_tooltipObjBig.style.left=t-20+"px";ajax_tooltipObjBig.style.top=n-25+"px"}function ajax_SoTooltip(e,t){if(!ajax_tooltipObjSo){ajax_tooltipObjSo=document.createElement("DIV");ajax_tooltipObjSo.style.position="absolute";ajax_tooltipObjSo.id="ajaxtooltipObj";document.body.appendChild(ajax_tooltipObjSo);var n=document.createElement("DIV");n.className="ajaxtooltip_arrow";n.id="ajaxtooltip_arrow";ajax_tooltipObjSo.appendChild(n);var r=document.createElement("DIV");r.className="ajaxtooltip_content";ajax_tooltipObjSo.appendChild(r);r.id="ajaxtooltip_content";if(ajax_tooltipSo_MSIE){ajax_tooltipObjSo_iframe=document.createElement('<IFRAME frameborder="0">');ajax_tooltipObjSo_iframe.style.position="absolute";ajax_tooltipObjSo_iframe.border="0";ajax_tooltipObjSo_iframe.frameborder=0;ajax_tooltipObjSo_iframe.style.backgroundColor="#FFF";ajax_tooltipObjSo_iframe.src="about:blank";r.appendChild(ajax_tooltipObj_iframe);ajax_tooltipObjSo_iframe.style.left="0px";ajax_tooltipObjSo_iframe.style.top="0px"}}ajax_tooltipObjSo.style.display="block";ajax_loadContent("ajaxtooltip_content",e);if(ajax_tooltipSo_MSIE){ajax_tooltipObjSo_iframe.style.width=ajax_tooltipObjSo.clientWidth+"px";ajax_tooltipObjSo_iframe.style.height=ajax_tooltipObjSo.clientHeight+"px"}ajax_positionSoTooltip(t)}function ajax_positionSoTooltip(e){var t=ajaxTooltip_getLeftPos(e)+e.offsetWidth;var n=ajaxTooltip_getTopPos(e);var r=document.getElementById("ajaxtooltip_content").offsetWidth+document.getElementById("ajaxtooltip_arrow").offsetWidth;ajax_tooltipObjSo.style.left=t-20+"px";ajax_tooltipObjSo.style.top=n-25+"px"}function ajaxTooltip_getTopPos(e){var t=e.offsetTop;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetTop}return t}function ajaxTooltip_getLeftPos(e){var t=e.offsetLeft;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetLeft}return t}function hideTooltip(){if(ajax_tooltipObjSo){ajax_tooltipObjSo.style.display="none"}}var x_offset_tooltip=5;var y_offset_tooltip=0;var ajax_tooltipObj=false;var ajax_tooltipObj_iframe=false;var ajax_tooltip_MSIE=false;if(navigator.userAgent.indexOf("MSIE")>=0)ajax_tooltip_MSIE=true;var ajax_tooltipObjBig=false;var ajax_tooltipObjBig_iframe=false;var ajax_tooltipBig_MSIE=false;if(navigator.userAgent.indexOf("MSIE")>=0)ajax_tooltipBig_MSIE=true;var x_offset_tooltip=5;var y_offset_tooltip=0;var ajax_tooltipObjSo=false;var ajax_tooltipObjSo_iframe=false;var ajax_tooltipSo_MSIE=false;if(navigator.userAgent.indexOf("MSIE")>=0)ajax_tooltipSo_MSIE=true

function findCustomInfo(position,value,corp) {
	
	 
	 var url="agentInfo.html?ajax=1&decorator=simple&popup=true&agentRequestId=" + encodeURI(value)
	 ajax_showTooltip1(url,position);
	  }
	  
	  function resendRequest(id)
	  {
		 
		  var $j = jQuery.noConflict();
		  	$j.ajax({
		  		 url: 'editAgentRequestReason.html?agentId='+id+'&agentRequestId='+id+'&decorator=modal&popup=true',
		            success: function(data){
		            	
		            	 $j("#agentDetailList"+id).modal({show:true,backdrop: 'static',keyboard: false});
						$j("#agentDetailList"+id).html(data);
	           },
	            error: function () {
	                alert('Some error occurs');
	                $j("#agentDetailList"+id).modal("hide");
	             }   
		});  

		  	 
	  }
	  function userStatusCheck(target)
	  {
		  
		  alert(target.value)
	  }
</script>