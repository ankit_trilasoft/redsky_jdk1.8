 <%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="agentDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentDetail.heading'/>"/> 
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>   
</head>   
<c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/> 
<s:form id="agentForm" action="saveAgent" method="post" validate="true">   
<s:hidden name="agent.id" value="<%=request.getParameter("id")%>"/>  
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/> 
<s:hidden name="agent.shipNumber" /> 
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" />ServiceOrder</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td> 
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editBilling.html?id=<%=request.getParameter("id")%>"/>Bill</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editAgent.html?id=${serviceOrder.id}"/>Partner</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="containers.html?id=<%=request.getParameter("id")%>"/>Pack</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Date</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Long</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editInternational.html?id=<%=request.getParameter("id")%>"/>International</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="customerWorkTickets.html?id=${serviceOrder.id}" >Ticket</a></td>
			<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="receivables.html?id=${serviceOrder.id}" >Costing</a></td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="claims.html?id=${serviceOrder.id}"/>Claim</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editCustomerFile.html?id=${customerFile.id}"/>CustomerFile</td>
			<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
		</tr>
	</tbody>
</table>

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
	<tr><td align="left" class="listwhite">
		<table>
		 
		  <tbody>  	
		  	<tr><td align="left" class="listwhite"><fmt:message key="billing.shipper"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.firstName"  size="14" readonly="true"  onfocus="checkDate();"/><td align="left" class="listwhite"><s:textfield name="serviceOrder.lastName"  size="11" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.originCity"  size="10" readonly="true"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.originCountryCode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.Job"   size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.commodity"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.commodity"  size="6" readonly="true"/></td><td align="left" class="listwhite"><fmt:message key="billing.routing"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.routing"   size="4" readonly="true" /></td></tr>
		  </tbody>
		 </table>
 	</td></tr>
 	<tr><td align="left" class="listwhite">
		 <table>
		  <tbody>  	
		  	<tr><td align="right" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left" class="listwhite"><s:textfield name="customerFileNumber" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.registrationNo"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.registrationNumber"  size="11" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.destination"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.destinationCity"  size="10" readonly="true"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.destinationCountryCode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.mode"   size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.billTo"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.billToCode"   size="12" readonly="true"/></td></tr>
		  </tbody>
		 
		 </table>
	 </td></tr>
   </tbody>
 </table>
 
<c:if test="${not empty agent.bookingCode}">   
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
		    <td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Agents</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="servicePartners.html?id=<%=request.getParameter("id") %>">Carriers</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${empty agent.bookingCode}">  
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
		    <td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Agents</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Carriers</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
		</tr>
	</tbody>
</table>
</c:if>
 
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
			<table cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
                     <tr>
      <td align="left" class="listwhite"><fmt:message key="agent.bookingCode"/><font color="red" size="2">*</font></td>
  <td align="left" class="listwhite"><s:textfield name="agent.bookingCode" size="17" maxlength="10" readonly="true" /></td>
  <td align="left" class="listwhite"><s:textfield name="agent.bookingAgentLastName" size="17" readonly="true" /></td>
  <td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=agent.bookingCity&fld_description=agent.bookingAgentLastName&fld_code=agent.bookingCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
  <td align="left" class="listwhite"><fmt:message key="agent.freightForworderCode" /></td>
  <td align="left" class="listwhite"><s:textfield name="agent.freightForworderCode" size="17" maxlength="10" readonly="true" /></td>
  <td align="left" class="listwhite"><s:textfield name="agent.freightForworderLastName" size="17" readonly="true" /></td>
  <td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=agent.freightForworderCity&fld_description=agent.freightForworderLastName&fld_code=agent.freightForworderCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
</tr>  
  <tr>
  <td></td>
  <td align="left" class="listwhite"><s:textfield name="agent.bookingCity"   size="17" readonly="true"/></td>
  <td colspan="3"></td>
  <td align="left" class="listwhite"><s:textfield name="agent.freightForworderCity" size="17" readonly="true"/></td>
  </tr>
  <tr>
 <td align="left" class="listwhite"><fmt:message key="agent.bookingCoord"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.bookingCoord" size="17" maxlength="35" onkeydown="return onlyCharsAllowed(event)"/></td>
<td></td><td align="left" class="listwhite" colspan="2"><fmt:message key="agent.freightForworderCoord"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.freightForworderCoord" size="17" maxlength="35" onkeydown="return onlyCharsAllowed(event)" /></td>
 </tr>
 <tr>
  <td align="left" class="listwhite"><fmt:message key="agent.bookingEmail"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.bookingEmail" size="17" maxlength="65"/></td>
<td></td><td align="left" class="listwhite" colspan="2"><fmt:message key="agent.freightForworderEmail"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.freightForworderEmail" size="17" maxlength="65"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="agent.originAgentCode"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.originAgentCode" size="17" maxlength="10" readonly="true" /></td>
<td align="left" class="listwhite"><s:textfield name="agent.originAgentLastName" size="17" readonly="true" /></td>
<td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=agent.originAgentCity&fld_description=agent.originAgentLastName&fld_code=agent.originAgentCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
<td align="left" class="listwhite"><fmt:message key="agent.destinationAgentCode" /></td>
<td align="left" class="listwhite"><s:textfield name="agent.destinationAgentCode" size="17" maxlength="10" readonly="true" /></td>
<td align="left" class="listwhite"><s:textfield name="agent.destinationAgentLastName" size="17" readonly="true" /></td>
<td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=agent.destinationAgentCity&fld_description=agent.destinationAgentLastName&fld_code=agent.destinationAgentCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
<tr>
<td></td>
<td align="left" class="listwhite"><s:textfield name="agent.originAgentCity" size="17" readonly="true" /></td>
<td colspan="3"></td>
<td align="left" class="listwhite"><s:textfield name="agent.destinationAgentCity" size="17" readonly="true" /></td>
<td></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="agent.originAgentCoord"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.originAgentCoord" size="17" maxlength="35" onkeydown="return onlyCharsAllowed(event)" /></td>
<td></td><td align="left" class="listwhite" colspan="2"><fmt:message key="agent.destinationAgentCoord"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.destinationAgentCoord" size="17" maxlength="35" onkeydown="return onlyCharsAllowed(event)" /></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="agent.originAgentEmail"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.originAgentEmail" size="17" maxlength="65"/></td>
<td></td><td align="left" class="listwhite" colspan="2"><fmt:message key="agent.destinationAgentEmail"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.destinationAgentEmail" size="17" maxlength="65"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="agent.pickupCode"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.pickupCode" size="17" maxlength="10" readonly="true" /></td>
<td align="left" class="listwhite"><s:textfield name="agent.pickupDesc" size="17" readonly="true" /></td>
<td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agent.pickupDesc&fld_code=agent.pickupCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
<td align="left" class="listwhite"><fmt:message key="agent.setoffCode"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.setoffCode" size="17" maxlength="10" readonly="true" /></td>
<td align="left" class="listwhite"><s:textfield name="agent.setoffDesc" size="17" readonly="true" /></td>
<td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agent.setoffDesc&fld_code=agent.setoffCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="agent.tranship"/></td>
<td align="left" class="listwhite"><s:select name="agent.tranship" list="{'N:  No','Y:  Yes'}"  cssStyle="width:57px" /></td>
<td></td><td align="left" class="listwhite" colspan="2"><fmt:message key="agent.deliveryCode"/></td>
<td align="left" class="listwhite"><s:textfield name="agent.deliveryCode" size="17" maxlength="10" readonly="true" /></td>
<td align="left" class="listwhite"><s:textfield name="agent.deliveryDesc" size="17" readonly="true" /></td>
<td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agent.deliveryDesc&fld_code=agent.deliveryCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
					<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='agent.createdOn'/></td>
						<s:hidden name="agent.createdOn" />
						<td><s:date name="agent.createdOn" format="dd-MMM-yyyy"/></td>		
						<td align="right" class="listwhite"><fmt:message key='agent.createdBy' /></td>
						
						<c:if test="${not empty agent.id}">
								<s:hidden name="agent.createdBy"/>
								<td><s:label name="createdBy" value="%{agent.createdBy}"/></td>
							</c:if>
							<c:if test="${empty agent.id}">
								<s:hidden name="agent.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
						<td align="right" class="listwhite"><fmt:message key='agent.updatedOn'/></td>
						<s:hidden name="agent.updatedOn"/>
						<td><s:date name="agent.updatedOn" format="dd-MMM-yyyy"/></td>
						<td align="right" class="listwhite"><fmt:message key='agent.updatedBy' /></td>
						<s:hidden name="agent.updatedBy" value="${pageContext.request.remoteUser}" />
						<td><s:label name="agent.updatedBy" /></td>
					</tr>
				</tbody>
			</table>
									</td>
									<td background="<c:url value='/images/bg-right.jpg'/>"
										align="left" class="listwhite"></td>
								</tr>
								<tr>
									<td></td>
									</tr>
						<td valign="top" align="left" class="listwhite" />
						<td align="left" class="listwhite" />
								<tr>
									<td colspan="5"></td>
								</tr>
						<td align="left" class="listwhite"></td>
					<tr>
						<td align="left" class="listwhite" />
						<td valign="top" align="left" class="listwhite">
						<div id="layer" />
						</td>
						<td align="left" class="listwhite" />
					</tr>
					<tr>
						<td align="left" class="listwhite"></td>
						<td valign="top" align="left" class="listwhite" />
						<td align="left" class="listwhite" />
					</td>
					</tr>
									</tbody>
									</table>						
									</div>
    <li class="buttonBar bottom">            
        <s:submit cssClass="button" method="save" key="button.save"/> 
        <s:reset type="button" key="Reset"/>  
        <c:if test="${not empty agent.id}">    
        </c:if>   
    </li>  
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />    
</s:form>   

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<script language="JavaScript">
	function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function isNumeric(targetElement){   
		var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
		        alert("Enter valid Number");
		        targetElement.value="";
		        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script> 
<%-- Shifting Closed Here --%>
  
<script type="text/javascript">   
    Form.focusFirstElement($("agentForm"));   
</script>  

  
