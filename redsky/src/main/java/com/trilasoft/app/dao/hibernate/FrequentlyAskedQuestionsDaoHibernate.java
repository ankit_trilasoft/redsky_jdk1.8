package com.trilasoft.app.dao.hibernate;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.FrequentlyAskedQuestionsDao;
import com.trilasoft.app.dao.hibernate.QualitySurveySettingsDaoHibernate.SurveyEmailDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.FrequentlyAskedQuestions;


public class FrequentlyAskedQuestionsDaoHibernate extends GenericDaoHibernate<FrequentlyAskedQuestions, Long> implements FrequentlyAskedQuestionsDao {
	private HibernateUtil hibernateUtil;
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	public FrequentlyAskedQuestionsDaoHibernate() {
		super(FrequentlyAskedQuestions.class);
		
	}
	
	public List getFAQByCMMDMMPartnerCode(String partnerCode,String corpId){
		List l2= getSession().createSQLQuery("select id,corpId,partnerCode,question,answer,createdOn,createdBy,updatedOn,updatedBy,partnerId,language,parentId,sequenceNumber from frequentlyaskedquestions where (partnerId  is not null) and partnerCode like '"+partnerCode+"%' and corpId like '"+corpId+"%' group by answer,language,question").list();
		      
		/**if(l2!=null && l2.size()<=0){
		    	   l2= getSession().createSQLQuery("select id,corpId,partnerCode,question,answer,createdOn,createdBy,updatedOn,updatedBy,partnerId,language,parentId,sequenceNumber from frequentlyaskedquestions where (partnerId  is not null) and (partnerCode='' or partnerCode is null) and corpId like '"+corpId+"%'").list();  
		       }*/
		List fAQDTOList= new ArrayList();
		Iterator it=l2.iterator();
		while(it.hasNext())
		{
			FrequentlyAskedQuestions faq = new FrequentlyAskedQuestions();
			Object []obj=(Object[]) it.next();
			faq.setId((obj[0] != null )?Long.valueOf(obj[0].toString()):0);
			faq.setCorpId((obj[1] != null )?obj[1].toString():"");
			faq.setPartnerCode((obj[2] != null )?obj[2].toString():"");
			faq.setQuestion((obj[3] != null )?obj[3].toString():"");
			faq.setAnswer((obj[4] != null )?obj[4].toString():"");
			faq.setCreatedOn((obj[5] != null )?(Date)obj[5]:new Date());
			faq.setCreatedBy((obj[6] != null )?obj[6].toString():"");
			faq.setUpdatedOn((obj[7] != null )?(Date)obj[7]:new Date());
			faq.setUpdatedBy((obj[8] != null )?obj[8].toString():"");
			faq.setPartnerId((obj[9] != null )?Long.valueOf(obj[9].toString()):0);
			faq.setLanguage((obj[10] != null )?obj[10].toString():"");
			faq.setParentId((obj[11] != null )?Long.valueOf(obj[11].toString()):0);
			faq.setSequenceNumber((obj[12] != null )?Long.valueOf(obj[12].toString()):0);
			fAQDTOList.add(faq);
		}
		return fAQDTOList;
	}
	
	public List getFAQByPartnerCode(String partnerCode,String corpId){
		List l2= getSession().createSQLQuery("select id,corpId,partnerCode,question,answer,createdOn,createdBy,updatedOn,updatedBy,partnerId,language,parentId,sequenceNumber from frequentlyaskedquestions where (partnerId  is not null) and partnerCode like '"+partnerCode+"%' and corpId like '"+corpId+"%' group by answer,language,question").list();
		/**      
		if(l2!=null && l2.size()<=0){
		    	   l2= getSession().createSQLQuery("select id,corpId,partnerCode,question,answer,createdOn,createdBy,updatedOn,updatedBy,partnerId,language,parentId,sequenceNumber from frequentlyaskedquestions where (partnerId  is not null) and (partnerCode='' or partnerCode is null) and corpId like '"+corpId+"%'").list();  
		       }*/
		List fAQDTOList= new ArrayList();
		Iterator it=l2.iterator();
		while(it.hasNext())
		{
			FrequentlyAskedQuestions faq = new FrequentlyAskedQuestions();
			Object []obj=(Object[]) it.next();
			faq.setId((obj[0] != null )?Long.valueOf(obj[0].toString()):0);
			faq.setCorpId((obj[1] != null )?obj[1].toString():"");
			faq.setPartnerCode((obj[2] != null )?obj[2].toString():"");
			faq.setQuestion((obj[3] != null )?obj[3].toString():"");
			faq.setAnswer((obj[4] != null )?obj[4].toString():"");
			faq.setCreatedOn((obj[5] != null )?(Date)obj[5]:new Date());
			faq.setCreatedBy((obj[6] != null )?obj[6].toString():"");
			faq.setUpdatedOn((obj[7] != null )?(Date)obj[7]:new Date());
			faq.setUpdatedBy((obj[8] != null )?obj[8].toString():"");
			faq.setPartnerId((obj[9] != null )?Long.valueOf(obj[9].toString()):0);
			faq.setLanguage((obj[10] != null )?obj[10].toString():"");
			faq.setParentId((obj[11] != null )?Long.valueOf(obj[11].toString()):0);
			faq.setSequenceNumber((obj[12] != null )?Long.valueOf(obj[12].toString()):0);
			fAQDTOList.add(faq);
		}
		return fAQDTOList;
	}
	   public List getFAQByAdmin(String corpId){
		
		return getHibernateTemplate().find("from FrequentlyAskedQuestions where partnerCode='' and  corpId like '"+corpId+"%')  order by sequenceNumber");
		}
		
		public List getNewFaqByPartnerCode(String partnerCode,String corpId){
			
			List l1= getHibernateTemplate().find("from FrequentlyAskedQuestions where partnerCode like '"+partnerCode+"%' and corpId like '"+corpId+"%' group by answer,language,question  order by sequenceNumber");
			      /**   if(l1.size()<=0){
			        	 l1= getHibernateTemplate().find("from FrequentlyAskedQuestions where (partnerCode='' or partnerCode is null) and corpId like '"+corpId+"%')  order by sequenceNumber");    	 
			         }*/
			List l2= getSession().createSQLQuery("select id,corpId,partnerCode,question,answer,createdOn,createdBy,updatedOn,updatedBy,partnerId,language,parentId from frequentlyaskedquestions where partnerCode like '"+partnerCode+"%' and corpId like '"+corpId+"%' group by answer,language,question").list();
			/**if(l2.size()<=0){
				l2= getSession().createSQLQuery("select id,corpId,partnerCode,question,answer,createdOn,createdBy,updatedOn,updatedBy,partnerId,language,parentId from frequentlyaskedquestions where (partnerCode='' or partnerCode is null) and corpId like '"+corpId+"%'").list();	
			}*/
			List fAQDTOList= new ArrayList();
			FAQDTO fAQDTO=null;
			Iterator it=l2.iterator();
			while(it.hasNext())
			{
				Object []obj=(Object[]) it.next();
				fAQDTO=new FAQDTO();
				fAQDTO.setId(obj[0]);
				fAQDTO.setCorpId(obj[1]);
				fAQDTO.setPartnerCode(obj[2]);
				fAQDTO.setQuestion(obj[3]);
				fAQDTO.setAnswer(obj[4]);
				fAQDTO.setCreatedOn(obj[5]);
				fAQDTO.setCreatedBy(obj[6]);
				fAQDTO.setUpdatedOn(obj[7]);
				fAQDTO.setUpdatedBy(obj[8]);
				fAQDTO.setPartnerId(obj[9]);
				fAQDTO.setLanguage(obj[10]);
				fAQDTO.setParentId(obj[11]);
				fAQDTOList.add(fAQDTO);
			}
				return l1;
		}
	public List getFaqByPartnerCode(String partnerCode,String corpId){
			return getHibernateTemplate().find("from FrequentlyAskedQuestions where partnerCode like '"+partnerCode+"%' and corpId like '"+corpId+"%'  group by answer,language,question order by sequenceNumber");
	}
	@SuppressWarnings("unchecked")
	public Boolean getExcludeFPU(String partnerCode,String corpId){
		Boolean FPU=false;
		List al=getSession().createSQLQuery("select if(excludeFromParentFaqUpdate,'TRUE','FALSE') from partnerprivate where partnerCode='"+partnerCode+"' AND corpID in('"+corpId+"','"+hibernateUtil.getParentCorpID(corpId)+"')").list();
		if(!al.isEmpty())
		{
			if(al.get(0).toString().equalsIgnoreCase("TRUE"))
			{
				FPU=true;
			}else{
				FPU=false;
			}
		}
		return FPU;
	}
	public int checkFrequentlyAskedQuestions(String question,String answer,String language,String partnerCode,String sessionCorpID){
		if(question==null) question="";
		if(answer==null) answer="";
		if(language==null) language="";
		String query ="select count(*) from frequentlyaskedquestions where partnerCode='"+partnerCode+"' and question='"+StringEscapeUtils.escapeSql(question)+"' and answer='"+StringEscapeUtils.escapeSql(answer)+"' and language='"+language+"' and  corpId = '"+sessionCorpID+"'";
		String temp=this.getSession().createSQLQuery(query).list().get(0).toString();
		int countValue=Integer.parseInt(temp);
		return countValue;

	}
	public void deleteagentParentPolicyFile(String oldAgentParent,String partnerCode,String sessionCorpID){
		 String tempPartnerId="";
			String policyDocumentId="";
			List tempParent= this.getSession().createSQLQuery("select id from frequentlyaskedquestions where partnerCode='"+oldAgentParent+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list();
			if(tempParent.size()>0){
				Iterator it=tempParent.iterator();
				while(it.hasNext()){
					if(tempPartnerId.equals("")){
						tempPartnerId = "'"+it.next().toString()+"'";
					}else{
						tempPartnerId =tempPartnerId+ ",'"+it.next().toString()+"'";
					}
				}
			}
			if(!tempPartnerId.equals("")){
				this.getSession().createSQLQuery("delete FROM frequentlyaskedquestions where parentid in ("+tempPartnerId+") and partnerCode='"+partnerCode+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").executeUpdate();	
			}
	}
	public List getFaqReference(String partnerCode, String corpID,Long parentId){
		List al=getSession().createSQLQuery("select id from frequentlyaskedquestions where partnerCode='"+ partnerCode +"' and corpID = '"+ corpID +"' and parentId='"+parentId+"' limit 1").list();
		return al;
	}

	public List findMaximum(String language,String partnerCode,String corpID) {
    	return this.getSession().createSQLQuery("select max(sequenceNumber) from frequentlyaskedquestions where partnerCode='"+partnerCode+"' and language='"+language+"' and  corpId = '"+corpID+"'").list();
    }
	public List findMaximumNumber(String language,String corpID){
		
		return this.getSession().createSQLQuery("select max(sequenceNumber) from frequentlyaskedquestions where ( partnerCode=''or partnerCode is null ) and language='"+language+"' and  corpId = '"+corpID+"'").list();
	}
	public int checkFrequentlySequence(String language,String partnerCode,Long sequenceNumber,String sessionCorpID){
		if(language==null) language="";
		if(sequenceNumber==null) sequenceNumber=0L;
		String tempValue=this.getSession().createSQLQuery("select count(*) from frequentlyaskedquestions where partnerCode='"+partnerCode+"' and sequenceNumber='"+sequenceNumber+"' and language='"+language+"' and  corpId = '"+sessionCorpID+"' ").list().get(0).toString();
		int valuecount=Integer.parseInt(tempValue);
		return valuecount;
	}
	public void deleteChildDocument(Long id, String sessionCorpID){
		String query="";
		query="delete from frequentlyaskedquestions where parentId ="+id+" and corpID in ('TSFT', '"+sessionCorpID+"')";
		this.getSession().createSQLQuery(query).executeUpdate();
	}
	public int deletedChildFAQ(String partnerCode, String sessionCorpID) {
		return this.getSession().createSQLQuery("delete FROM frequentlyaskedquestions where  partnerCode='"+partnerCode+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").executeUpdate();	
		 
	}
}

class FAQDTO
{
	
	private Object id;
	private Object corpId;	
	private Object partnerCode;
	private Object question;
	private Object answer;
	private Object createdOn;
	private Object createdBy;
	private Object updatedOn;
	private Object updatedBy;
	private Object partnerId;
	private Object language;
	private Object parentId;
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public Object getCorpId() {
		return corpId;
	}
	public void setCorpId(Object corpId) {
		this.corpId = corpId;
	}
	public Object getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(Object partnerCode) {
		this.partnerCode = partnerCode;
	}
	public Object getQuestion() {
		return question;
	}
	public void setQuestion(Object question) {
		this.question = question;
	}
	public Object getAnswer() {
		return answer;
	}
	public void setAnswer(Object answer) {
		this.answer = answer;
	}
	public Object getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Object createdOn) {
		this.createdOn = createdOn;
	}
	public Object getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Object createdBy) {
		this.createdBy = createdBy;
	}
	public Object getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Object updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Object getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Object updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Object getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Object partnerId) {
		this.partnerId = partnerId;
	}
	public Object getLanguage() {
		return language;
	}
	public void setLanguage(Object language) {
		this.language = language;
	}
	public Object getParentId() {
		return parentId;
	}
	public void setParentId(Object parentId) {
		this.parentId = parentId;
	}	
}