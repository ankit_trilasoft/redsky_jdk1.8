package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;   

import javax.management.Query;

import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import com.trilasoft.app.model.NewsUpdate;   
import com.trilasoft.app.dao.NewsUpdateDao; 
import com.trilasoft.app.dao.hibernate.InventoryPackingDaoHibernate.InventoryPieceDTO;
import com.trilasoft.app.model.RefMaster;

  
public class NewsUpdateDaoHibernate extends GenericDaoHibernate<NewsUpdate, Long> implements NewsUpdateDao {   
  
    public NewsUpdateDaoHibernate() {   
        super(NewsUpdate.class);   
    }   
   
    public List<NewsUpdate> findByDetail(String details) {   
        return getHibernateTemplate().find("from NewsUpdate where details=?", details);   
    }   
    public List findUpdatedDate(String corpId){
    	   	   	
    //	return getHibernateTemplate().find("from NewsUpdate where datediff(sysdate(),effectiveDate) <= 4 and corpId in('TSFT','"+corpId+"') and visible=true");
           return getSession().createSQLQuery("select * from newsupdate where corpId in('TSFT','"+corpId+"') and visible=true and case dayname(effectiveDate) when 'Friday' then effectiveDate between effectiveDate and effectiveDate + interval 3 day when 'Saturday' then effectiveDate between effectiveDate and effectiveDate + interval 3 day when 'Sunday' then effectiveDate between effectiveDate and effectiveDate + interval 2 day else effectiveDate between effectiveDate and effectiveDate + interval 1 day end and datediff(sysdate(),effectiveDate) <=3 and datediff(sysdate(),effectiveDate) >=0").list();
    }

	public List findDistinct() {
		return getHibernateTemplate().find(" SELECT distinct corpID FROM Company order by corpID");
	}

	public List findListOnCorpId(String corpId,Boolean addValues) {
		String condition=" and datediff(sysdate(),effectiveDate) >=0 and datediff(sysdate(),endDisplayDate) <=0";
		return getSession().createQuery("from NewsUpdate where corpId in('TSFT','"+corpId+"') and visible=true"+(!addValues?condition:"")).list();
	}
	
	public List findListByModuleCorp(String module,String corpid, Date publishedDate,String isVisible,Boolean activeStatus){
		if(!activeStatus){
			module= (module == null || "".equals(module))?"(module like '%%' or module is null)":"module like '%"+module+"%'";
			
				corpid= (corpid == null || "".equals(corpid))?"like '%%'": "in ('"+corpid.replaceAll(", ", "','")+"')";
			
			
			StringBuilder formatedDate1=new StringBuilder() ;
			try{
				if(publishedDate!=null){
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( publishedDate ) );
				}
			}catch(Exception ex){}
			return getHibernateTemplate().find("from NewsUpdate where "+module+" AND corpId "+corpid+" AND publishedDate like '"+formatedDate1+"%' and visible is false ");
		//	return getHibernateTemplate().find("from NewsUpdate where visible is false");
		}
		if((module == null || "".equals(module)) && (corpid == null || "".equals(corpid)) && (publishedDate == null || "".equals(publishedDate))&& activeStatus ){
			return getHibernateTemplate().find("from NewsUpdate where visible is true");
		}
		module= (module == null || "".equals(module))?"(module like '%%' or module is null)":"module like '"+module+"'";
		if("".equals(isVisible)){
			corpid= (corpid == null || "".equals(corpid))?"like '%%'": "in ('"+corpid.replaceAll(", ", "','")+"')";
		}else{
			corpid= (corpid == null || "".equals(corpid))?"like '%%'": "in ('TSFT','"+corpid.replaceAll(", ", "','")+"')";
		}
		
		StringBuilder formatedDate1=new StringBuilder() ;
		try{
			if(publishedDate!=null){
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( publishedDate ) );
			}
		}catch(Exception ex){}
		return getHibernateTemplate().find("from NewsUpdate where "+module+" AND corpId "+corpid+" AND publishedDate like '"+formatedDate1+"%' "+isVisible);
	}
	
  public List findByModule(String module,String corpid,Date publishedDate){
	  StringBuilder formatedDate1=new StringBuilder() ;
		try{
			if(publishedDate!=null){
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( publishedDate ) );
			}
		}catch(Exception ex){}
	  return getSession().createQuery("from NewsUpdate where (module like '"+module+"%' or module is null) and visible is true and corpId in('TSFT','"+corpid+"') and publishedDate like '"+formatedDate1+"%'").list();
  }
  
  public String findNewsUpdateDescForToolTip(Long id, String corpId){
		String newsUpdateList="";
		if(corpId.equals("TSFT")){
			List list = this.getSession().createSQLQuery("SELECT details FROM newsupdate where id='"+id+"'").list();
			if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
				newsUpdateList=list.get(0).toString();
			}
		}else{
			List list = this.getSession().createSQLQuery("SELECT details FROM newsupdate where id='"+id+"' and corpID ='"+corpId+"'").list();
			if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
				newsUpdateList=list.get(0).toString();
			}
		}
		return newsUpdateList;
  }
}

