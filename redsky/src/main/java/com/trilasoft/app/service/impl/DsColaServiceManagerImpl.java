package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsColaServiceDao;
import com.trilasoft.app.model.DsColaService;
import com.trilasoft.app.service.DsColaServiceManager;


public class DsColaServiceManagerImpl extends GenericManagerImpl<DsColaService, Long> implements DsColaServiceManager { 
	DsColaServiceDao dsColaServiceDao;
	public DsColaServiceManagerImpl(DsColaServiceDao dsColaServiceDao) {
		super(dsColaServiceDao); 
        this.dsColaServiceDao = dsColaServiceDao; 
		// TODO Auto-generated constructor stub
	}
	
	public List getListById(Long id) {
		return dsColaServiceDao.getListById(id);
	}

}


