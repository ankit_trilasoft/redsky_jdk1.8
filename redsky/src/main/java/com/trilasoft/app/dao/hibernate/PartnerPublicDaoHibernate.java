package com.trilasoft.app.dao.hibernate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.PartnerPublicDao;
import com.trilasoft.app.dao.hibernate.dto.AgentUpdateSchedularDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.model.AgentBaseSCAC;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.model.FrequentlyAskedQuestions;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefMaster;

public class PartnerPublicDaoHibernate extends GenericDaoHibernate<PartnerPublic, Long> implements PartnerPublicDao {
	
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	
	public PartnerPublicDaoHibernate() {
		super(PartnerPublic.class);
	}
	
	
	public List getPartnerPublicListByPartnerView(String partnerType, String corpID,String firstName, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgnoreInactive, String extRefernece) {
		
	List <Object> partnerList = new ArrayList<Object>();
	List <Object> popupList = new ArrayList<Object>();
	getHibernateTemplate().setMaxResults(500);
	String tempExtRef = "";
	if(extRefernece.trim().length() <= 0){
		tempExtRef =  "OR p.extReference is null ";
	}
	
	String query1 = "";
	String query = "select distinct p.id, p.partnerCode, p.lastName, p.firstName, p.aliasName, p.isAgent, p.isAccount, p.isPrivateParty, p.isCarrier, p.isVendor, p.isOwnerOp, p.status, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, p.extReference " +
					"FROM partner p "+
					"WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
					"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
					"AND (p.extReference like '%" + extRefernece.replaceAll("'", "''") + "%' "+tempExtRef+") " +
					"AND p.status like '"+ status +"%' AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
	if(isIgnoreInactive){
		query += "AND p.status <> 'Inactive' ";
	}
	
	if(isPP){
		query1 = query1+",isPrivateParty=true";
	}
	if(isAgt){
		query1 = query1+",isAgent=true";
	}
	if(isVndr){
		query1 = query1+",isVendor=true";
	}
	if(isCarr){
		query1 = query1+",isCarrier=true";
	}
	if(isAcc){
		query1 = query1+",isAccount=true";
	}
	if(isOO){
		query1 = query1+",isOwnerOp=true";
	}
	
	if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
		query1 = query1.substring(1, query1.length());
		String tempString[] = query1.split(",");
		int totalLength = tempString.length;
		
		if(totalLength == 1){
			query +=  " AND "+query1;
		}
		if(totalLength > 1){
			String tmpStart = " AND (";
  		   	String tmpEnd = ")";
  		   	String tempQuery = "";
			for (int i = 0 ; i < totalLength ; i++) {
		     	   
		     	  tempQuery += tempString[i]+" or ";
				}
			tempQuery = tempQuery.substring(0,tempQuery.length()-3);
			query += tmpStart.concat(tempQuery).concat(tmpEnd);
		}	
	}else{
		query += " AND isAgent = false AND isVendor = false AND isCarrier= false AND isPrivateParty = false AND isAccount = false AND isOwnerOp = false";
	}
	
	System.out.println("----------->"+query);
	popupList = this.getSession().createSQLQuery(query).list(); 
	Iterator it = popupList.iterator();
	PartnerDTO dto = null;
	while(it.hasNext()){
		Object [] row = (Object[])it.next();
		dto = new PartnerDTO();
		
		dto.setId(row[0]);
		dto.setPartnerCode(row[1]);
		dto.setLastName(row[2]);
		if(row[3] == null){
			dto.setFirstName("");
		}else{
			dto.setFirstName(row[3]);
		}if(row[4] == null){
			dto.setAliasName("");
		}else{
			dto.setAliasName(row[4]);
		}
		
		if(row[5]==null){
			dto.setIsAgent("");
		}else{
			dto.setIsAgent(row[5]);
		}
		
		if(row[6]==null){
			dto.setIsAccount("");
		}else{
			dto.setIsAccount(row[6]);
		}
		
		if(row[7]==null){
			dto.setIsPrivateParty("");
		}else{
			dto.setIsPrivateParty(row[7]);
		}
		
		if(row[8]==null){
			dto.setIsCarrier("");
		}else{
			dto.setIsCarrier(row[8]);
		}
		
		if(row[9]==null){
			dto.setIsVendor("");
		}else{
			dto.setIsVendor(row[9]);
		}
		
		if(row[10]==null){
			dto.setIsOwnerOp("");
		}else{
			dto.setIsOwnerOp(row[10]);
		}
		
		if(row[11]==null){
			dto.setStatus("");
		}else{
			dto.setStatus(row[11]);
		}
		
		if(row[12] == null){
			dto.setCountryName("");
		}else{
			dto.setCountryName(row[12]);
		}
		
		if(row[13] == null){
			dto.setCityName("");
		}else{
			dto.setCityName(row[13]);
		}
		
		if(row[14] == null){
			dto.setStateName("");
		}else{
			dto.setStateName(row[14]);
		} 
		if(row[15]==null){
			dto.setExtReference("");
		}else{
			dto.setExtReference(row[15]);
		}
		
		partnerList.add(dto);
	}
	
	return partnerList;
}

	
	public List getPartnerPublicList(String partnerType, String corpID,String firstName, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgnoreInactive, String extRefernece,String vanlineCode,String typeOfVendor, boolean cmmdmmflag, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA) {
		List <Object> partnerList = new ArrayList<Object>();
		List <Object> popupList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(500);
		String tempExtRef = "";
		if(extRefernece.trim().length() <= 0){
			tempExtRef =  "OR pp.extReference is null ";
		}
		
		String query1 = "";
		String query="";
		if(corpID.equalsIgnoreCase("TSFT")){
		if(vanlineCode!=null && !vanlineCode.equals("")){
			query = "select distinct p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp,if(pp.status='' or pp.status is null ,'New',pp.status)," +
			"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
			"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
			"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, pp.extReference,pv.vanLineCode , if(p.status='' or p.status is null ,'New',p.status) as publicStatus, p.corpID,p.agentGroup,pp.agentClassification  " +
			//",ref.description   " +
			"FROM partnerpublic p left outer join partnerprivate pp on pp.partnerpublicid = p.id  AND pp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" +
			//"left outer join refmaster ref on ((if(isAgent=true or isVendor=true or isCarrier=true, terminalState =ref.code, billingState =ref.code))) and ref.corpid='TSFT' and ref.parameter='STATE' " +
			//"and (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode =ref.bucket2, billingCountryCode =ref.bucket2)) " +
			"inner join partnervanlineref pv on p.partnerCode = pv.partnerCode and pv.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')"+
			"WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND(p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
			"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND  pv.vanLineCode like '%" + vanlineCode.replaceAll("'", "''") + "%' " +
			"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
			"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
			"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
			"AND (pp.extReference like '%" + extRefernece.replaceAll("'", "''") + "%' "+tempExtRef+") " +
			"AND p.status like '"+ status +"%' AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
		}else{
		 query = "select distinct p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, if(pp.status='' or pp.status is null ,'New',pp.status) as status ," +
						"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
						"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
						"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, pp.extReference,'', if(p.status='' or p.status is null ,'New',p.status) as publicStatus, p.corpID,p.agentGroup,pp.agentClassification  " +
						//",ref.description " +
						"FROM partnerpublic p left outer join partnerprivate pp on pp.partnerpublicid = p.id  AND pp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" +
						//"left outer join refmaster ref on ((if(isAgent=true or isVendor=true or isCarrier=true, terminalState =ref.code, billingState =ref.code))) and ref.corpid='TSFT' and ref.parameter='STATE' " +
						//"and (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode =ref.bucket2, billingCountryCode =ref.bucket2)) " +
						"WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND(p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
						"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'" +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
						"AND (pp.extReference like '%" + extRefernece.replaceAll("'", "''") + "%' "+tempExtRef+") " +
						"AND p.status like '"+ status +"%' AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
		}
		}else{
			if(vanlineCode!=null && !vanlineCode.equals("")){
				query = "select distinct p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp,if(pp.status='' or pp.status is null ,'New',pp.status) as status," +
				"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
				"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
				"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, pp.extReference,pv.vanLineCode , if(p.status='' or p.status is null ,'New',p.status) as publicStatus , p.corpID,p.agentGroup,pp.agentClassification  " +
				//",ref.description  " +
				"FROM partnerpublic p left outer join partnerprivate pp on pp.partnerpublicid = p.id  AND pp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') AND pp.status like '"+ status +"%'"+
				//"left outer join refmaster ref on ((if(isAgent=true or isVendor=true or isCarrier=true, terminalState =ref.code, billingState =ref.code))) and ref.corpid='TSFT' and ref.parameter='STATE' " +
				//"and (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode =ref.bucket2, billingCountryCode =ref.bucket2)) " +
				"inner join partnervanlineref pv on p.partnerCode = pv.partnerCode and pv.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')"+
				"WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND(p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
				"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND  pv.vanLineCode like '%" + vanlineCode.replaceAll("'", "''") + "%' " +
				"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
				"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
				"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
				"AND (pp.extReference like '%" + extRefernece.replaceAll("'", "''") + "%' "+tempExtRef+") " +
				"AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
			}else{
			 query = "select distinct p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, if(pp.status='' or pp.status is null ,'New',pp.status) as status ," +
							"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
							"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
							"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, pp.extReference,'', if(p.status='' or p.status is null ,'New',p.status) as publicStatus, p.corpID,p.agentGroup,pp.agentClassification  " +
							//",ref.description " +
							"FROM partnerpublic p left outer join partnerprivate pp on pp.partnerpublicid = p.id  AND pp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') AND pp.status like '"+ status +"%'" +
							//"left outer join refmaster ref on ((if(isAgent=true or isVendor=true or isCarrier=true, terminalState =ref.code, billingState =ref.code))) and ref.corpid='TSFT' and ref.parameter='STATE' " +
							//"and (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode =ref.bucket2, billingCountryCode =ref.bucket2)) " +
							"WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND(p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
							"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'" +
							"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
							"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
							"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
							"AND (pp.extReference like '%" + extRefernece.replaceAll("'", "''") + "%' "+tempExtRef+") " +
							"AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
			}
		}
		String additionalSearch=""; 
		if(fidiNumber!=null && (!(fidiNumber.trim().equals(""))) && fidiNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.fidiNumber ='"+fidiNumber+"' or p.fidiNumber is null or p.fidiNumber='')  ";
		}
		if(fidiNumber!=null && (!(fidiNumber.trim().equals(""))) && fidiNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.fidiNumber ='"+fidiNumber+"'   ";
		}
		if(OMNINumber!=null && (!(OMNINumber.trim().equals(""))) && OMNINumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.OMNINumber is null or p.OMNINumber='') ";
		}
		if(OMNINumber!=null && (!(OMNINumber.trim().equals(""))) && OMNINumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.OMNINumber !='' and p.OMNINumber is not null  ";
		}
		if(AMSANumber!=null && (!(AMSANumber.trim().equals(""))) && AMSANumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.AMSANumber ='"+AMSANumber+"'  or p.AMSANumber is null or p.AMSANumber='') ";
		}
		if(AMSANumber!=null && (!(AMSANumber.trim().equals(""))) && AMSANumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.AMSANumber ='"+AMSANumber+"' ";
		}
		if(WERCNumber!=null && (!(WERCNumber.trim().equals(""))) && WERCNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.WERCNumber ='"+WERCNumber+"'  or p.WERCNumber is null or p.WERCNumber='') ";
		}
		if(WERCNumber!=null && (!(WERCNumber.trim().equals(""))) && WERCNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.WERCNumber ='"+WERCNumber+"' ";
		}
		if(IAMNumber!=null && (!(IAMNumber.trim().equals(""))) && IAMNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.IAMNumber ='"+IAMNumber+"'  or p.IAMNumber is null or p.IAMNumber='') ";
		}
		if(IAMNumber!=null && (!(IAMNumber.trim().equals(""))) && IAMNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.IAMNumber ='"+IAMNumber+"'  ";
		}
		if(utsNumber!=null && (!(utsNumber.trim().equals(""))) && utsNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.utsNumber ='"+utsNumber+"'  or p.utsNumber is null or p.utsNumber='') ";
		}
		if(utsNumber!=null && (!(utsNumber.trim().equals(""))) && utsNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.utsNumber ='"+utsNumber+"' ";
		}
		if(eurovanNetwork!=null && (!(eurovanNetwork.trim().equals(""))) && eurovanNetwork.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.eurovanNetwork ='"+eurovanNetwork+"'  or p.eurovanNetwork is null or p.eurovanNetwork='') ";
		}
		if(eurovanNetwork!=null && (!(eurovanNetwork.trim().equals(""))) && eurovanNetwork.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.eurovanNetwork ='"+eurovanNetwork+"'  ";
		}
		if(PAIMA!=null && (!(PAIMA.trim().equals(""))) && PAIMA.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.PAIMA ='"+PAIMA+"'  or p.PAIMA is null or p.PAIMA='') ";
		}
		if(PAIMA!=null && (!(PAIMA.trim().equals(""))) && PAIMA.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.PAIMA ='"+PAIMA+"' ";
		}
		if(LACMA!=null && (!(LACMA.trim().equals(""))) && LACMA.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.LACMA ='"+LACMA+"'  or p.LACMA is null or p.LACMA='') ";
		}
		if(LACMA!=null && (!(LACMA.trim().equals(""))) && LACMA.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.LACMA ='"+LACMA+"' ";
		}
		System.out.println("\n\n\n\n additionalSearch "+additionalSearch);
		query=query+additionalSearch;
		if((status!=null)&&(!status.equalsIgnoreCase(""))&&(!status.equalsIgnoreCase("New"))){
			query=query.replaceAll("left outer", "inner");
		}
		if(isIgnoreInactive){
			query += "AND p.status <> 'Inactive' ";
		}
		if( isVndr && typeOfVendor!=null && !typeOfVendor.equals("")){
			query +="AND p.typeOfVendor like '%"+typeOfVendor.replaceAll("'", "''") +"%'";	
		}
		if(isPP){
			query1 = query1+",isPrivateParty=true";
		}
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		if(isVndr){
			query1 = query1+",isVendor=true";
		}
		if(isCarr){
			query1 = query1+",isCarrier=true";
		}
		if(isAcc){
			query1 = query1+",isAccount=true";
		}
		if(isOO){
			query1 = query1+",isOwnerOp=true";
		}
		
		if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND isAgent = false AND isVendor = false AND isCarrier= false AND isPrivateParty = false AND isAccount = false AND isOwnerOp = false";
		}
		query += " limit 500 ";
		//System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list(); 
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			if(isIgnoreInactive)
			{
				if((!row[11].toString().equalsIgnoreCase("Inactive")) && (!row[17].toString().equalsIgnoreCase("Inactive")))
				{
					dto.setId(row[0]);
					dto.setPartnerCode(row[1]);
					dto.setLastName(row[2]);
					if(row[3] == null){
						dto.setFirstName("");
					}else{
						dto.setFirstName(row[3]);
					}if(row[4] == null){
						dto.setAliasName("");
					}else{
						dto.setAliasName(row[4]);
					}
					if(row[5]==null){
						dto.setIsAgent("");
					}else{
						dto.setIsAgent(row[5]);
					}
					if(row[6]==null){
						dto.setIsAccount("");
					}else{
						dto.setIsAccount(row[6]);
					}
					if(row[7]==null){
						dto.setIsPrivateParty("");
					}else{
						dto.setIsPrivateParty(row[7]);
					}
					if(row[8]==null){
						dto.setIsCarrier("");
					}else{
						dto.setIsCarrier(row[8]);
					}
					if(row[9]==null){
						dto.setIsVendor("");
					}else{
						dto.setIsVendor(row[9]);
					}
					if(row[10]==null){
						dto.setIsOwnerOp("");
					}else{
						dto.setIsOwnerOp(row[10]);
					}
					if(row[11]==null && row[17]==null){
						dto.setStatus("");
					}else if(corpID.equalsIgnoreCase("TSFT")){
						dto.setStatus(row[17]);
					}else if(row[17]!=null && (row[17].toString().equalsIgnoreCase("Inactive")||row[17].toString().equalsIgnoreCase("Blacklisted"))){
						dto.setStatus(row[17]);
					}
					else{
						dto.setStatus(row[11]);	
					}
					if(row[12] == null){
						dto.setCountryName("");
					}else {
						dto.setCountryName(row[12]);
					}
					if(row[13] == null){
						dto.setCityName("");
					}else{
						dto.setCityName(row[13]);
					}
					if(row[14] == null){
						dto.setStateName("");
					}else{
						List descName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+row[14]+"' and parameter='STATE' and bucket2='"+row[12]+"' ").addScalar("r.description", Hibernate.STRING).list();
							if(descName!=null && !descName.isEmpty() && descName.get(0)!=null ){
								dto.setStateName(descName.get(0).toString());	
							}else{
								dto.setStateName("");
							}
					} 
					if(row[15]==null){
						dto.setExtReference("");
					}else{
						dto.setExtReference(row[15]);
					}
					if(row[16]==null){
						dto.setVanLineCode("");
					}else{
						dto.setVanLineCode(row[16]);
					}
					if(row[19]==null){
						dto.setAgentGroup("");
					}else{
						dto.setAgentGroup(row[19]);
					}
					if(row[20]==null){
						dto.setAgentClassification("");
					}else{
						dto.setAgentClassification(row[20]);
					}
					
					partnerList.add(dto);
					
			}}else{
				dto.setId(row[0]);
				dto.setPartnerCode(row[1]);
				dto.setLastName(row[2]);
				if(row[3] == null){
					dto.setFirstName("");
				}else{
					dto.setFirstName(row[3]);
				}if(row[4] == null){
					dto.setAliasName("");
				}else{
					dto.setAliasName(row[4]);
				}
				if(row[5]==null){
					dto.setIsAgent("");
				}else{
					dto.setIsAgent(row[5]);
				}
				if(row[6]==null){
					dto.setIsAccount("");
				}else{
					dto.setIsAccount(row[6]);
				}
				if(row[7]==null){
					dto.setIsPrivateParty("");
				}else{
					dto.setIsPrivateParty(row[7]);
				}
				if(row[8]==null){
					dto.setIsCarrier("");
				}else{
					dto.setIsCarrier(row[8]);
				}
				if(row[9]==null){
					dto.setIsVendor("");
				}else{
					dto.setIsVendor(row[9]);
				}
				if(row[10]==null){
					dto.setIsOwnerOp("");
				}else{
					dto.setIsOwnerOp(row[10]);
				}
				if(row[11]==null && row[17]==null){
					dto.setStatus("");
				}else if(corpID.equalsIgnoreCase("TSFT")){
					dto.setStatus(row[17]);
				}else if(row[17]!=null && (row[17].toString().equalsIgnoreCase("Inactive")||row[17].toString().equalsIgnoreCase("Blacklisted"))){
					dto.setStatus(row[17]);
				}
				else{
					dto.setStatus(row[11]);	
				}
				if(row[12] == null){
					dto.setCountryName("");
				}else {
					dto.setCountryName(row[12]);
				}
				if(row[13] == null){
					dto.setCityName("");
				}else{
					dto.setCityName(row[13]);
				}
				if(row[14] == null){
					dto.setStateName("");
				}else{
					List descName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+row[14]+"' and parameter='STATE' and bucket2='"+row[12]+"' ").addScalar("r.description", Hibernate.STRING).list();
						if(descName!=null && !descName.isEmpty() && descName.get(0)!=null ){
							dto.setStateName(descName.get(0).toString());	
						}else{
							dto.setStateName("");
						}
				} 
				if(row[15]==null){
					dto.setExtReference("");
				}else{
					dto.setExtReference(row[15]);
				}
				if(row[16]==null){
					dto.setVanLineCode("");
				}else{
					dto.setVanLineCode(row[16]);
				}
				if(row[19]==null){
					dto.setAgentGroup("");
				}else{
					dto.setAgentGroup(row[19]);
				}
				if(row[20]==null){
					dto.setAgentClassification("");
				}else{
					dto.setAgentClassification(row[20]);
				}
				partnerList.add(dto);
				
			}
		}
		
		return partnerList;
	}
	
	
	public List getPartnerPublicMergeList(String partnerType, String corpID,String firstName, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, String city) {
		List <Object> partnerList = new ArrayList<Object>();
		List <Object> popupList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(500);
		String query1 = "";
		String ch=hibernateUtil.getSpecialCorpIDs(corpID);
		String query = "select p.id, p.partnerCode, lastName, firstName, isAgent,aliasName, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, status, " +
						"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
						"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
						"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, corpID, mergedCode, doNotMerge " +
						"FROM partnerpublic p " +
						"WHERE (firstName like '%" + firstName.replaceAll("'", "''") +"%' or aliasName like '%" + firstName.replaceAll("'", "''") +"%') AND( aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND (lastName like '%" + lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") +"%')" +
						"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
						"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCity like '%" + city.replaceAll("'", "''") + "%', billingCity like '%" + city.replaceAll("'", "''") + "%')) " +
						"AND status like '"+ status +"%' " +
						"AND corpID IN ('"+corpID+"','TSFT','"+hibernateUtil.getSpecialCorpIDs(corpID)+"')";
		
		if(isPP){
			query1 = query1+",isPrivateParty=true";
		}
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		if(isVndr){
			query1 = query1+",isVendor=true";
		}
		if(isCarr){
			query1 = query1+",isCarrier=true";
		}
		if(isAcc){
			query1 = query1+",isAccount=true";
		}
		if(isOO){
			query1 = query1+",isOwnerOp=true";
		}
		
		if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND isAgent = false AND isVendor = false AND isCarrier= false AND isPrivateParty = false AND isAccount = false AND isOwnerOp = false";
		}
		
		//System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list();
		
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			
			dto.setId(row[0]);
			dto.setPartnerCode(row[1]);
			dto.setLastName(row[2]);
			
			
			if(row[3] == null){
				dto.setFirstName("");
			}else{
				dto.setFirstName(row[3]);
			}
			
			if(row[4]==null){
				dto.setIsAgent("");
			}else{
				dto.setIsAgent(row[4]);
			}
			if(row[5]==null){
				dto.setAliasName("");
			}else{
				dto.setAliasName(row[5]);
			}
			
			if(row[6]==null){
				dto.setIsAccount("");
			}else{
				dto.setIsAccount(row[6]);
			}
			
			if(row[7]==null){
				dto.setIsPrivateParty("");
			}else{
				dto.setIsPrivateParty(row[7]);
			}
			
			if(row[8]==null){
				dto.setIsCarrier("");
			}else{
				dto.setIsCarrier(row[8]);
			}
			
			if(row[9]==null){
				dto.setIsVendor("");
			}else{
				dto.setIsVendor(row[9]);
			}
			
			if(row[10]==null){
				dto.setIsOwnerOp("");
			}else{
				dto.setIsOwnerOp(row[10]);
			}
			
			if(row[11]==null){
				dto.setStatus("");
			}else{
				dto.setStatus(row[11]);
			}
			
			if(row[12] == null){
				dto.setCountryName("");
			}else{
				dto.setCountryName(row[12]);
			}
			
			if(row[13] == null){
				dto.setCityName("");
			}else{
				dto.setCityName(row[13]);
			}
			
			if(row[14] == null){
				dto.setStateName("");
			}else{
				dto.setStateName(row[14]);
			}
			
			if(row[15] == null){
				dto.setCorpID("");
			}else{
				dto.setCorpID(row[15]);
			}
			
			if(row[16] == null){
				dto.setMergedCode("");
			}else{
				dto.setMergedCode(row[16]);
			}
			
			if(row[17] == null){
				dto.setDoNotMerge("");
			}else{
				dto.setDoNotMerge(row[17]);
			}
			
			partnerList.add(dto);
		}
		
		return partnerList;
	}
	
	public class PartnerDTO {
		private Object id;
		private Object partnerCode;
		private Object lastName;
		private Object firstName;
		private Object aliasName;
		private Object isAgent;
		private Object isAccount;
		private Object isPrivateParty;
		private Object isCarrier;
		private Object isVendor;
		private Object isOwnerOp;
		private Object status;
		private Object countryName;
		private Object stateName;
		private Object cityName;
		private Object stage;
		private Object vanLineCode;
		private Object add1;
		private Object add2;
		private Object zip;
		private Object companyDiv;
		private Object corpID;
		private Object mergedCode;
		private Object doNotMerge;
		private Object extReference;
		private Object publicStatus;
		private Object agentGroup;
		private Object agentClassification;
			
		
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getIsAccount() {
			return isAccount;
		}
		public void setIsAccount(Object isAccount) {
			this.isAccount = isAccount;
		}
		public Object getIsAgent() {
			return isAgent;
		}
		public void setIsAgent(Object isAgent) {
			this.isAgent = isAgent;
		}
		public Object getIsCarrier() {
			return isCarrier;
		}
		public void setIsCarrier(Object isCarrier) {
			this.isCarrier = isCarrier;
		}
		public Object getIsOwnerOp() {
			return isOwnerOp;
		}
		public void setIsOwnerOp(Object isOwnerOp) {
			this.isOwnerOp = isOwnerOp;
		}
		public Object getIsPrivateParty() {
			return isPrivateParty;
		}
		public void setIsPrivateParty(Object isPrivateParty) {
			this.isPrivateParty = isPrivateParty;
		}
		public Object getIsVendor() {
			return isVendor;
		}
		public void setIsVendor(Object isVendor) {
			this.isVendor = isVendor;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getCityName() {
			return cityName;
		}
		public void setCityName(Object cityName) {
			this.cityName = cityName;
		}
		public Object getCountryName() {
			return countryName;
		}
		public void setCountryName(Object countryName) {
			this.countryName = countryName;
		}
		public Object getStateName() {
			return stateName;
		}
		public void setStateName(Object stateName) {
			this.stateName = stateName;
		}
		public Object getStage() {
			return stage;
		}
		public void setStage(Object stage) {
			this.stage = stage;
		}
		public Object getVanLineCode() {
			return vanLineCode;
		}
		public void setVanLineCode(Object vanLineCode) {
			this.vanLineCode = vanLineCode;
		}

		public Object getAdd1() {
			return add1;
		}

		public void setAdd1(Object add1) {
			this.add1 = add1;
		}

		public Object getAdd2() {
			return add2;
		}

		public void setAdd2(Object add2) {
			this.add2 = add2;
		}

		public Object getZip() {
			return zip;
		}

		public void setZip(Object zip) {
			this.zip = zip;
		}

		public Object getCompanyDiv() {
			return companyDiv;
		}

		public void setCompanyDiv(Object companyDiv) {
			this.companyDiv = companyDiv;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getMergedCode() {
			return mergedCode;
		}
		public void setMergedCode(Object mergedCode) {
			this.mergedCode = mergedCode;
		}
		public Object getDoNotMerge() {
			return doNotMerge;
		}
		public void setDoNotMerge(Object doNotMerge) {
			this.doNotMerge = doNotMerge;
		}
	
		public Object getExtReference() {
			return extReference;
		}
	
		public void setExtReference(Object extReference) {
			this.extReference = extReference;
		}
		public Object getAliasName() {
			return aliasName;
		}
		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}
		public Object getPublicStatus() {
			return publicStatus;
		}
		public void setPublicStatus(Object publicStatus) {
			this.publicStatus = publicStatus;
		}
		public Object getAgentGroup() {
			return agentGroup;
		}
		public void setAgentGroup(Object agentGroup) {
			this.agentGroup = agentGroup;
		}
		public Object getAgentClassification() {
			return agentClassification;
		}
		public void setAgentClassification(Object agentClassification) {
			this.agentClassification = agentClassification;
		}
	}

	public List findMaxByCode(String initial) {
		Long l = Long.parseLong((this.getSession().createSQLQuery("select max(id) from partnerpublic where partnerCode like '"+ initial +"%'")).list().get(0).toString());
		List pList = this.getSession().createSQLQuery("select partnerCode from partnerpublic where id  ='"+ l +"'" ).list();
		return pList;
	}

	public List findPartnerCode(String partnerCode) {
		List pList = this.getSession().createSQLQuery("select id from partnerpublic where partnerCode='"+partnerCode+"' ").list();
		return pList;
		//return getHibernateTemplate().find("from PartnerPublic where partnerCode=?", partnerCode);
	}
	public List findPartnerpublicCode(String partnerCode) {
		/*List pList = this.getSession().createSQLQuery("select partnerCode from partnerpublic where partnerCode='"+partnerCode+"' ").list();
		return pList;*/
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("from PartnerPublic where partnerCode=?", partnerCode);
	}
	public List findPartnerId(String partnerCode, String sessionCorpID) {
		return getHibernateTemplate().find("select id from PartnerPublic where partnerCode='"+partnerCode+"'");
	}

	public PartnerPublic getByID(Long id) {
		return (PartnerPublic)getHibernateTemplate().find("from PartnerPublic where id=?", id);
	}

	public PartnerPublic getPartnerByCode(String partnerCode) {
		//List pList = getHibernateTemplate().find("from PartnerPublic where partnerCode='"+partnerCode+"'");
		List pList=getSession().createSQLQuery("select * from partnerpublic where partnerCode='"+partnerCode+"'").addEntity(PartnerPublic.class).list();
		if(pList!=null && !pList.isEmpty())
		{
			return (PartnerPublic)pList.get(0);
		}
		else
		{ 
			PartnerPublic p=new PartnerPublic();
				return p;
		}
	}

	public List getAgentBaseByPartnerCode(String partnerCode, String corpID) {
		List baseList = new ArrayList();
		String columns = "corpID, baseCode, description, partnerCode, createdBy, updatedBy, createdOn, updatedOn";
		baseList = getSession().createSQLQuery("select "+columns+" from agentbase where partnerCode='"+partnerCode+"' and corpID IN ('"+corpID+"','"+hibernateUtil.getSpecialCorpIDs(corpID)+"') ").list();
		try {
			baseList = hibernateUtil.convertScalarToComponent(columns, baseList, AgentBase.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return baseList;
	}

	public List getAgentBaseSCACByPartnerCode(String partnerCode, String baseCode, String corpID) {
		List baseSCACList = new ArrayList();
		String columns = "corpID, baseCode, SCACCode, partnerCode, createdBy, updatedBy, createdOn, updatedOn, internationalLOIflag, domesticLOIflag";
		baseSCACList = getSession().createSQLQuery("select "+columns+" from agentbasescac where baseCode = '"+baseCode+"' and partnerCode='"+partnerCode+"'").list();
		try {
			baseSCACList = hibernateUtil.convertScalarToComponent(columns, baseSCACList, AgentBaseSCAC.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return baseSCACList;
	}

	public List getAccLineRefByPartnerCode(String partnerCode, String corpID) {
		List accLineRefList = new ArrayList();
		String columns = "corpID, partnerCode, createdOn, updatedOn, updatedBy, createdBy, companyDivision, accountCrossReference, refType";
		accLineRefList = getSession().createSQLQuery("select "+columns+" from partneraccountref where partnerCode='"+partnerCode+"' and corpID IN ('"+corpID+"','TSFT','"+hibernateUtil.getSpecialCorpIDs(corpID)+"')").list();
		try {
			accLineRefList = hibernateUtil.convertScalarToComponent(columns, accLineRefList, PartnerAccountRef.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return accLineRefList;
	}

	public List getAccContactByPartnerCode(String partnerCode, String corpID) {
		List accContractList = new ArrayList();
		String columns = "corpID, createdOn,createdBy, updatedOn, updatedBy, jobType, jobTypeOwner, contractType, contactName, department, primaryPhone, primaryPhoneType, secondaryPhone, secondaryPhoneType, thirdPhone, thirdPhoneType, contactEmail, partnerCode, contactFrequency, title, dateOfBirth, doNotCall, salutation, contactLastName, leadSource, reportTo, assistant, assistantPhone, email2, emailType, optedOut1, invalid1, optedOut2, invalid2, address, country, state, city, zip";
		accContractList = getSession().createSQLQuery("select "+columns+" from accountcontact where partnerCode='"+partnerCode+"' ").list();
		try {
			accContractList = hibernateUtil.convertScalarToComponent(columns, accContractList, AccountContact.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return accContractList;
	}

	public List getAccProfileByPartnerCode(String partnerCode, String corpID) {
		List accProfileList = new ArrayList();
		String columns = "corpID, createdOn, createdBy, updatedOn, updatedBy, stage, type, industry, employees, revenue, country, source, owner, partnerCode, currency";
		accProfileList = getSession().createSQLQuery("select "+columns+" from accountprofile where partnerCode='"+partnerCode+"'").list();
		try {
			accProfileList = hibernateUtil.convertScalarToComponent(columns, accProfileList, AccountProfile.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return accProfileList;
	}

	public List getContPolicyByPartnerCode(String partnerCode, String corpID) {
		List contPolicyList = new ArrayList();
		String columns = "corpID, createdBy, createdOn, updatedBy, updatedOn, policy, partnerCode";
		contPolicyList = getSession().createSQLQuery("select "+columns+" from contractpolicy where partnerCode='"+partnerCode+"' ").list();
		try {
			contPolicyList = hibernateUtil.convertScalarToComponent(columns, contPolicyList, ContractPolicy.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return contPolicyList;
	}
	
	public List getFAQByPartnerCode(String partnerCode, String corpID){
		List faqList = new ArrayList();
		faqList = this.getHibernateTemplate().find("from FrequentlyAskedQuestions where partnerCode='" + partnerCode + "' and corpId='" + corpID + "'");
		return faqList;
	}
	
	public List getPolicyByPartnerCode(String partnerCode, String corpID){
		List policyList = new ArrayList();
		policyList = this.getHibernateTemplate().find("from ContractPolicy where partnerCode='" + partnerCode + "' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')");
		return policyList;
	}

	public StringBuffer updateMaregedParter(String userCheck, String mergedCode, String corpID){
		StringBuffer logQuery  = new StringBuffer();
		
		String partnerCodes = userCheck.replaceAll(",", "','");
		logQuery = logQuery.append("update partnerpublic set status = 'Inactive', updatedOn = now(), updatedBy = 'MERG_WIZ', mergedCode='"+mergedCode+"' where partnerCode in ('"+partnerCodes+"')");
		getSession().createSQLQuery("update partnerpublic set status = 'Inactive', updatedOn = now(), updatedBy = 'MERG_WIZ', mergedCode='"+mergedCode+"' where partnerCode in ('"+partnerCodes+"')").executeUpdate();
		
		logQuery = logQuery.append("<BR>").append("update partnerprivate set status = 'Inactive', updatedOn = now(), updatedBy = 'MERG_WIZ' where partnerCode in ('"+partnerCodes+"')");
		getSession().createSQLQuery("update partnerprivate set status = 'Inactive', updatedOn = now(), updatedBy = 'MERG_WIZ' where partnerCode in ('"+partnerCodes+"')").executeUpdate();
		return logQuery;
	}

	public StringBuffer updatePartnerVanLineRef(String userCheck, String partnerCode, String corpID, String partnerCodewithoutMaster, String masterPartnerCode) {
		StringBuffer logQuery  = new StringBuffer();
		
		String oldPartnerCodes = userCheck.replaceAll(",", "','");
		logQuery = logQuery.append("<BR>").append("update partnervanlineref set partnerCode='"+partnerCode+"',corpID='TSFT' where partnerCode in ('"+masterPartnerCode+"')");
		getSession().createSQLQuery("update partnervanlineref set partnerCode='"+partnerCode+"',corpID='TSFT' where partnerCode in ('"+masterPartnerCode+"')").executeUpdate();
		return logQuery;
	}

	public StringBuffer updateCodes(String userCheck, String partnerCode, String lastNameOld, String corpID) {
		StringBuffer logQuery  = new StringBuffer();
		String lastName = lastNameOld.replaceAll("'", "\\\\'");
		String oldPartnerCodes = userCheck.replaceAll(",", "','");
		
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		
		//Update Partner Rate Grid
		logQuery = logQuery.append("<BR>").append("update partnerrategrid set partnerCode = '"+partnerCode+"', partnerName = '"+lastName+"', corpID='TSFT' where partnerCode in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update partnerrategrid set partnerCode = '"+partnerCode+"', partnerName = '"+lastName+"', corpID='TSFT' where partnerCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Update Partner Agent
		logQuery = logQuery.append("<BR>").append("update partnerpublic set agentParent = '"+partnerCode+"' where agentParent in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update partnerpublic set agentParent = '"+partnerCode+"' where agentParent in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Customer File
		logQuery = logQuery.append("<BR>").append("update customerfile set bookingAgentCode = '"+partnerCode+"', bookingAgentName = '"+lastName+"' where bookingAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update customerfile set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update customerfile set accountCode = '"+partnerCode+"', accountName = '"+lastName+"' where accountCode in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update customerfile set bookingAgentCode = '"+partnerCode+"', bookingAgentName = '"+lastName+"' where bookingAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update customerfile set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update customerfile set accountCode = '"+partnerCode+"', accountName = '"+lastName+"' where accountCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//ServiceOrder
		logQuery = logQuery.append("<BR>").append("update serviceorder set bookingAgentCode = '"+partnerCode+"', bookingAgentName = '"+lastName+"' where bookingAgentCode in ('"+oldPartnerCodes+"')");
		logQuery = logQuery.append("<BR>").append("update serviceorder set originAgentCode = '"+partnerCode+"' where originAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update serviceorder set originSubAgentCode = '"+partnerCode+"' where originSubAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update serviceorder set destinationAgentCode = '"+partnerCode+"' where destinationAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update serviceorder set destinationSubAgentCode = '"+partnerCode+"' where destinationSubAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update serviceorder set brokerCode = '"+partnerCode+"' where brokerCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update serviceorder set forwarderCode = '"+partnerCode+"' where forwarderCode in ('"+oldPartnerCodes+"') ");
		
		
		getSession().createSQLQuery("update serviceorder set bookingAgentCode = '"+partnerCode+"', bookingAgentName = '"+lastName+"' where bookingAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update serviceorder set originAgentCode  = '"+partnerCode+"' where originAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update serviceorder set originSubAgentCode  = '"+partnerCode+"' where originSubAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update serviceorder set destinationAgentCode  = '"+partnerCode+"' where destinationAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update serviceorder set destinationSubAgentCode  = '"+partnerCode+"' where destinationSubAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update serviceorder set brokerCode  = '"+partnerCode+"' where brokerCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update serviceorder set forwarderCode  = '"+partnerCode+"' where forwarderCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		
		//TrackingStatus
		logQuery = logQuery.append("<BR>").append("update trackingstatus set originAgentCode = '"+partnerCode+"', originAgent = '"+lastName+"' where originAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update trackingstatus set destinationAgentCode = '"+partnerCode+"', destinationAgent = '"+lastName+"' where destinationAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update trackingstatus set originSubAgentCode = '"+partnerCode+"', originSubAgent = '"+lastName+"' where originSubAgentCode in ('"+oldPartnerCodes+"') ");				
		logQuery = logQuery.append("<BR>").append("update trackingstatus set destinationSubAgentCode = '"+partnerCode+"', destinationSubAgent = '"+lastName+"' where destinationSubAgentCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update trackingstatus set brokerCode = '"+partnerCode+"', brokerName = '"+lastName+"' where brokerCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update trackingstatus set forwarderCode = '"+partnerCode+"', forwarder = '"+lastName+"' where forwarderCode in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update trackingstatus set originAgentCode = '"+partnerCode+"', originAgent = '"+lastName+"' where originAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update trackingstatus set destinationAgentCode = '"+partnerCode+"', destinationAgent = '"+lastName+"' where destinationAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update trackingstatus set originSubAgentCode = '"+partnerCode+"', originSubAgent = '"+lastName+"' where originSubAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update trackingstatus set destinationSubAgentCode = '"+partnerCode+"', destinationSubAgent = '"+lastName+"' where destinationSubAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update trackingstatus set brokerCode = '"+partnerCode+"', brokerName = '"+lastName+"' where brokerCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update trackingstatus set forwarderCode = '"+partnerCode+"', forwarder = '"+lastName+"' where forwarderCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Billing
		logQuery = logQuery.append("<BR>").append("update billing set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' , updatedBy='MERG_WIZ', updatedOn= now() where billToCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update billing set billTo2Code = '"+partnerCode+"', billTo2Name = '"+lastName+"', updatedBy='MERG_WIZ', updatedOn= now() where billTo2Code in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update billing set privatePartyBillingCode = '"+partnerCode+"', privatePartyBillingName = '"+lastName+"' , updatedBy='MERG_WIZ', updatedOn= now() where privatePartyBillingCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update billing set billingId = '"+partnerCode+"' , updatedBy='MERG_WIZ', updatedOn= now() where billingId in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update billing set vendorCode = '"+partnerCode+"', vendorName = '"+lastName+"' , updatedBy='MERG_WIZ', updatedOn= now() where vendorCode in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update billing set billToCode = '"+partnerCode+"', billToName = '"+lastName+"', updatedBy='MERG_WIZ', updatedOn= now() where billToCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update billing set billTo2Code = '"+partnerCode+"', billTo2Name = '"+lastName+"' , updatedBy='MERG_WIZ', updatedOn= now() where billTo2Code in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update billing set privatePartyBillingCode = '"+partnerCode+"', privatePartyBillingName = '"+lastName+"' , updatedBy='MERG_WIZ', updatedOn= now() where privatePartyBillingCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update billing set billingId = '"+partnerCode+"' , updatedBy='MERG_WIZ', updatedOn= now() where billingId in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update billing set vendorCode = '"+partnerCode+"', vendorName = '"+lastName+"' , updatedBy='MERG_WIZ', updatedOn= now() where vendorCode in ('"+oldPartnerCodes+"')  ").executeUpdate();
		
		//AccountLine
		for(int i = 0; i < arrayLength; i++) {
			String code = arrayid[i];
			logQuery = logQuery.append("<BR>").append("update accountline set oldBillToCode = '"+code+"' where billToCode = '"+code+"' ");
			getSession().createSQLQuery("update accountline set oldBillToCode = '"+code+"' where billToCode = '"+code+"' ").executeUpdate();
		}
		
		logQuery = logQuery.append("<BR>").append("update accountline set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update accountline set vendorCode = '"+partnerCode+"', estimateVendorName = '"+lastName+"' where vendorCode in ('"+oldPartnerCodes+"') ");
						
		getSession().createSQLQuery("update accountline set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update accountline set vendorCode = '"+partnerCode+"', estimateVendorName = '"+lastName+"' where vendorCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//WorkTicket
		logQuery = logQuery.append("<BR>").append("update workticket set account = '"+partnerCode+"', accountName = '"+lastName+"' where account in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update workticket set vendorCode = '"+partnerCode+"', vendorName = '"+lastName+"' where vendorCode in ('"+oldPartnerCodes+"') ");
						
		getSession().createSQLQuery("update workticket set account = '"+partnerCode+"', accountName = '"+lastName+"' where account in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update workticket set vendorCode = '"+partnerCode+"', vendorName = '"+lastName+"' where vendorCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//ServicePartner
		logQuery = logQuery.append("<BR>").append("update servicepartner set carrierCode = '"+partnerCode+"', carrierName = '"+lastName+"' where carrierCode in ('"+oldPartnerCodes+"') ");
				
		getSession().createSQLQuery("update servicepartner set carrierCode = '"+partnerCode+"', carrierName = '"+lastName+"' where carrierCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Claim
		logQuery = logQuery.append("<BR>").append("update claim set insurerCode = '"+partnerCode+"', insurer = '"+lastName+"' where insurerCode in ('"+oldPartnerCodes+"') ");
				
		getSession().createSQLQuery("update claim set insurerCode = '"+partnerCode+"', insurer = '"+lastName+"' where insurerCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Loss
		logQuery = logQuery.append("<BR>").append("update loss set whoWorkCDE = '"+partnerCode+"', whoWorkName = '"+lastName+"' where whoWorkCDE in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update loss set whoWorkCDE = '"+partnerCode+"', whoWorkName = '"+lastName+"' where whoWorkCDE in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Miscellaenous
		logQuery = logQuery.append("<BR>").append("update miscellaneous set bookerSelfPacking = '"+partnerCode+"' where bookerSelfPacking in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update miscellaneous set haulingAgentCode = '"+partnerCode+"', haulerName = '"+lastName+"' where haulingAgentCode in ('"+oldPartnerCodes+"') ");
				
		getSession().createSQLQuery("update miscellaneous set bookerSelfPacking = '"+partnerCode+"' where bookerSelfPacking in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update miscellaneous set haulingAgentCode = '"+partnerCode+"', haulerName = '"+lastName+"' where haulingAgentCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//SubcontractorCharges
		logQuery = logQuery.append("<BR>").append("update subcontractorcharges set personId = '"+partnerCode+"' where personId in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update subcontractorcharges set personId = '"+partnerCode+"' where personId in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//CPortalResourceMgmt
		logQuery = logQuery.append("<BR>").append("update cportalresourcemgmt set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update cportalresourcemgmt set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ").executeUpdate();

		//DefaultAccountLine
		logQuery = logQuery.append("<BR>").append("update defaultaccountline set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ");
		logQuery = logQuery.append("<BR>").append("update defaultaccountline set vendorCode = '"+partnerCode+"', vendorName = '"+lastName+"' where vendorCode in ('"+oldPartnerCodes+"') ");
						
		getSession().createSQLQuery("update defaultaccountline set billToCode = '"+partnerCode+"', billToName = '"+lastName+"' where billToCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		getSession().createSQLQuery("update defaultaccountline set vendorCode = '"+partnerCode+"', vendorName = '"+lastName+"' where vendorCode in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//PricingControl
		logQuery = logQuery.append("<BR>").append("update pricingcontrol set parentAgent = '"+partnerCode+"' where parentAgent in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update pricingcontrol set parentAgent = '"+partnerCode+"' where parentAgent in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//User
		logQuery = logQuery.append("<BR>").append("update app_user set parentAgent = '"+partnerCode+"' where parentAgent in ('"+oldPartnerCodes+"') ");
		
		getSession().createSQLQuery("update app_user set parentAgent = '"+partnerCode+"' where parentAgent in ('"+oldPartnerCodes+"') ").executeUpdate();
		
		//Update DataSecurityFilter & DataSecuritySet
		for(int i = 0; i < arrayLength; i++) {
			String code = arrayid[i];
			
			logQuery = logQuery.append("<BR>").append("update datasecurityfilter set name = REPLACE(name,'"+code+"','"+partnerCode+"'), filterValues = '"+partnerCode+"' where filterValues = '"+code+"' ");
			logQuery = logQuery.append("<BR>").append("update datasecurityset set name = 'DATA_SECURITY_SET_AGENT_"+partnerCode+"', description = 'DATA_SECURITY_SET_AGENT_"+partnerCode+"' where name = 'DATA_SECURITY_SET_AGENT_"+code+"' ");
			logQuery = logQuery.append("<BR>").append("update datasecurityset set name = 'DATA_SECURITY_SET_SO_BILLTOCODE_"+partnerCode+"', description = 'DATA_SECURITY_SET_SO_BILLTOCODE_"+partnerCode+"' where name = 'DATA_SECURITY_SET_SO_BILLTOCODE_"+code+"' ");
			
			getSession().createSQLQuery("update datasecurityfilter set name = REPLACE(name,'"+code+"','"+partnerCode+"'), filterValues = '"+partnerCode+"' where filterValues = '"+code+"' ").executeUpdate();
			getSession().createSQLQuery("update datasecurityset set name = 'DATA_SECURITY_SET_AGENT_"+partnerCode+"', description = 'DATA_SECURITY_SET_AGENT_"+partnerCode+"' where name = 'DATA_SECURITY_SET_AGENT_"+code+"' ").executeUpdate();
			getSession().createSQLQuery("update datasecurityset set name = 'DATA_SECURITY_SET_SO_BILLTOCODE_"+partnerCode+"', description = 'DATA_SECURITY_SET_SO_BILLTOCODE_"+partnerCode+"' where name = 'DATA_SECURITY_SET_SO_BILLTOCODE_"+code+"' ").executeUpdate();
		}
		return logQuery;
	}

	public List isExistAccContact(String corpID, String partnerCode) {
		return getSession().createSQLQuery("select * from accountcontact where partnerCode='"+partnerCode+"' AND corpID = '"+corpID+"'").list();
	}

	public List isExistAccProfile(String corpID, String partnerCode) {
		return getSession().createSQLQuery("select * from accountprofile where partnerCode='"+partnerCode+"' AND corpID = '"+corpID+"'").list();
	}

	public List isExistContractPolicy(String corpID, String partnerCode) {
		return getSession().createSQLQuery("select * from contractpolicy where partnerCode='"+partnerCode+"' AND corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')").list();
	}

	public void updateStatus(String partnerCode) {
		getHibernateTemplate().bulkUpdate("update PartnerPublic set status = 'Prospect' where partnerCode = '" + partnerCode + "' ");
		getHibernateTemplate().bulkUpdate("update PartnerPrivate set status = 'Prospect' where partnerCode = '" + partnerCode + "' ");
	}
	public List  findOwnerOperatorList(String parentId,String corpId){
		return getHibernateTemplate().find("from ControlExpirations where parentId='"+parentId+"' and corpID='"+corpId+"' ");
	}

	public StringBuffer updateMaregedParterPrivate(String userCheck, String partnerCode, Long id) {
		StringBuffer logQuery  = new StringBuffer();
		String partnerCodes = userCheck.replaceAll(",", "','");
		logQuery = logQuery.append("<BR>").append("update partnerprivate set partnerPublicId='"+id+"',partnerCode='"+partnerCode+"' ,updatedOn = now(), updatedBy = 'MERG_WIZ' where partnerCode in ('"+partnerCodes+"')");
		getSession().createSQLQuery("update partnerprivate set partnerPublicId='"+id+"',partnerCode='"+partnerCode+"', updatedOn = now(), updatedBy = 'MERG_WIZ' where partnerCode in ('"+partnerCodes+"')").executeUpdate();
		return logQuery;
	}

	public StringBuffer updateMaregedParterSameCorpid(String userCheck, String partnerCode, String corpID) {
		StringBuffer logQuery  = new StringBuffer();
		String partnerCodes = userCheck.replaceAll(",", "','");
		logQuery = logQuery.append("<BR>").append("update partnerprivate set status = 'Inactive', updatedOn = now(), updatedBy = 'MERG_WIZ' where partnerCode in ('"+partnerCodes+"')");
		getSession().createSQLQuery("update partnerprivate set status = 'Inactive', updatedOn = now(), updatedBy = 'MERG_WIZ' where partnerCode in ('"+partnerCodes+"')").executeUpdate();
		return logQuery;
	}

	public int updateConvertPartnerCorpid(String userCheck) {
		getHibernateTemplate().bulkUpdate("update PartnerVanLineRef set corpID='TSFT' where partnerCode ='"+userCheck+"'");
		getHibernateTemplate().bulkUpdate("update PartnerRateGrid set  corpID='TSFT' where partnerCode ='"+userCheck+"' ");
		getHibernateTemplate().bulkUpdate("update PartnerPublic set updatedOn = now(), updatedBy = 'MERG_WIZ', corpID='TSFT' where partnerCode ='"+userCheck+"'");
		return 0;
	}

	public String findPartnerCodeCount(String partnerCode) {
		String count="0";
		List pList = new ArrayList();
		pList = this.getSession().createSQLQuery("select count(id) from partnerpublic where partnerCode='"+partnerCode+"' ").list();
		if(pList!=null && (!(pList.isEmpty())) && pList.get(0)!=null){
			count=pList.get(0).toString();
		}
		return count;
	}
	public List getFindDontNerge(String corpId,String partnerCode){
		return getSession().createSQLQuery("select bookingAgentCode from companydivision where bookingAgentCode='"+partnerCode+"'").list();
	}
	public String getCountryCode(String billingCountryCode, String parameter,String sessionCorpID){
		String Query="Select concat(if(bucket2 is null,' ',bucket2),'~',if(branch is null,' ',branch)) from refmaster where parameter='"+parameter+"' and description='"+billingCountryCode+"' and (language='en' or language='' or language is null )  ";
		String str="";
		List l1 = this.getSession().createSQLQuery(Query).list();
		if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
			str= l1.get(0).toString();
		}
		return str;
	}
	public List getUserNameList(String sessionCorpID, String partnerCode){
		String Query="select userName from app_user where corpID='"+sessionCorpID+"' and parentAgent='"+partnerCode+"' and account_enabled is true";
		return getSession().createSQLQuery(Query).list();
	}

	public Map<String, String> getPartnerChildMap(String sessionCorpID, Long partnerCode) {
		
		List list;
		Map<String,String> modes =new TreeMap<String, String>();
		String Query2 =  "select distinct dsf.filterValues "+
						"from userdatasecurity uds, datasecurityset  dss,  datasecuritypermission dsp, datasecurityfilter dsf "+
						"where uds.userdatasecurity_id=dss.id "+
						"and dss.corpid in('"+sessionCorpID+"','TSFT') "+
						"and uds.user_id=" + partnerCode +
						" and dss.id=dsp.datasecurityset_id"+
						" and dsf.id=dsp.datasecurityfilter_id"+
						" and dsf.filterValues!='true' and dss.name!='DATA_SECURITY_SET_SO_JOB_Agent' ";
		//String Query2 = "select substring(name,Length('DATA_SECURITY_SET_SO_BILLTOCODE_')+1,Length(name)) from datasecurityset where id in(select userdatasecurity_id from userdatasecurity where user_id="+ partnerCode +") and corpid in('"+sessionCorpID+"','TSFT') and name like 'DATA_SECURITY_SET_SO_BILLTOCODE_%'";
	     list = getSession().createSQLQuery(Query2).list();
	     int j = list.size();
	     for (int i = 0; i <j; i++) {
	    	 String query = "select partnercode from partnerpublic where agentparent = '"+list.get(i)+"' and corpid in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and status ='Approved' and partnercode <> '"+list.get(i)+"' ";
	    	 List temp =  this.getSession().createSQLQuery(query).list();
	    	 list.addAll(temp);
	     }
	     Map<String, String> linkMap = new HashMap<String, String>();
	     String lastName="";	
		for (int i = 0; i < list.size(); i++) {
			List lastNameList  = new ArrayList();
			lastNameList = getSession().createSQLQuery("select lastName from partnerpublic where partnercode='"+list.get(i)+"'").list();	
			if(!lastNameList.isEmpty())
				lastName = lastNameList.get(0).toString();
			linkMap.put(lastName + " - " + list.get(i).toString(), list.get(i).toString());
		}
		
	/*	
		String Query1="select concat(partnerCode,'_',lastName) from PartnerPublic where corpID='"+sessionCorpID+"' and partnerCode='"+partnerCode+"'";
		String Query = "select concat(partnerCode,'_',lastName) from PartnerPublic p  where p.corpId in ('TSFT', 'UTSI') and p.agentParent ='"+partnerCode+"'"+
		" or p.agentParent in (select pp.partnercode from PartnerPublic pp where pp.corpId in ('TSFT', '"+sessionCorpID+"') and pp.agentParent = '"+partnerCode+"')";
		
		String Query2 = "select p.partnerCode,'~',p.lastName from partnerpublic p  where p.corpID in ('TSFT', 'UTSI') and p.agentParent ='"+partnerCode+"'"+
		" or p.agentParent in (select pp.partnerCode from partnerpublic pp where pp.corpID in ('TSFT', '"+sessionCorpID+"') and pp.agentParent = '"+partnerCode+"')";
		
		
		
		Map<String, String> linkMap = new HashMap<String, String>();
		List codeList = getSession().createSQLQuery(Query2).list();
        String abc=codeList.get(0).toString();
        System.out.println(abc);
		for (PartnerPublic partnerPublic : codeList) {
			
			linkMap.put(partnerPublic.getLastName() ,partnerPublic.getPartnerCode());
		}
		
		String Query="FROM PartnerPublic where corpID in ('TSFT', 'UTSI') and agentParent ='"+partnerCode+"' or agentParent in
		return getHibernateTemplate().find(Query).get(0).toString();
	*/	
		return linkMap;
	}
	public Boolean getExcludeFPU(String partnerCode,String corpId){
		Boolean FPU=false;
		List al=getSession().createSQLQuery("select if(excludePublicReloSvcsParentUpdate,'TRUE','FALSE') " +
				"from partnerprivate where partnerCode='"+partnerCode+"' AND corpID in ('"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') ").list();
		if((al!=null) && (al.get(0)!=null) &&(!al.isEmpty()))
		{
			if(al.get(0).toString().equalsIgnoreCase("TRUE"))
			{
				FPU=true;
			}else{
				FPU=false;
			}
		}
		return FPU;
	}
	public List getPartnerChildList(String sessionCorpID, String partnerCode) {
		String qu = "select concat(p.partnerCode,'_',p.lastName) from PartnerPublic p  where p.corpId in ('TSFT', '"+sessionCorpID+"') and p.agentParent ='"+partnerCode+"'";
		String Query1="select concat(partnerCode,'_',lastName) from PartnerPublic where corpID='"+sessionCorpID+"' and partnerCode='"+partnerCode+"'";
		
		String Query = "select concat(partnerCode,'_',lastName) from PartnerPublic p  where p.corpId in ('TSFT', 'UTSI') and p.agentParent ='"+partnerCode+"'"+
		" or p.agentParent in (select pp.partnercode from PartnerPublic pp where pp.corpId in ('TSFT', '"+sessionCorpID+"') and pp.agentParent = '"+partnerCode+"')";
		
		String Query2 = "select p.partnerCode, p.lastName from PartnerPublic p  where p.corpID in ('TSFT', 'UTSI') and p.agentParent ='"+partnerCode+"'"+
		" or p.agentParent in (select pp.partnerCode from PartnerPublic pp where pp.corpID in ('TSFT', '"+sessionCorpID+"') and pp.agentParent = '"+partnerCode+"')";
		
//		List list = getSession().createSQLQuery(Query).list();
		
		List list = getSession().createSQLQuery(qu).list();
		
		return list;
	}
		public List getPartnerPublicChildAccount(String partnerCode,String corpId){
		return getHibernateTemplate().find("FROM PartnerPublic where status='Approved' and corpID in ('TSFT', '"+corpId+"') and partnerCode=?",partnerCode);
	     }
		public List getPartnerPublicDefaultAccount(String partnerCode,String corpId){
			return getHibernateTemplate().find("FROM PartnerPublic where  corpID in ('TSFT', '"+corpId+"') and partnerCode=?",partnerCode);
		}
		public List getVanLine(String partnerVanLineCode, String sessionCorpID){
			/*List vanList=new ArrayList();
			String Query="select vanLineCode from partnervanlineref where partnercode='"+partnerVanLineCode+"'  and corpID in ('TSFT', '"+sessionCorpID+"')";
			vanList=getSession().createSQLQuery(Query).list();*/
			return getHibernateTemplate().find("from PartnerVanLineRef where partnerCode='"+partnerVanLineCode+"'  and corpID in ('TSFT', '"+sessionCorpID+"')");
		}
		public void deleteAgentParent(Long id,String partnerCode,String corpID){
			String tempPartnerId="";
			List tempParent= this.getSession().createSQLQuery("select id from entitlement where partnerPrivateId='"+id+"' and corpID='"+corpID+"' ").list();
			if(tempParent.size()>0){
				Iterator it=tempParent.iterator();
				while(it.hasNext()){
					if(tempPartnerId.equals("")){
						tempPartnerId = "'"+it.next().toString()+"'";
					}else{
						tempPartnerId =tempPartnerId+ ",'"+it.next().toString()+"'";
					}
				}
			}
			if(!tempPartnerId.equals("")){
				this.getSession().createSQLQuery("delete FROM entitlement where parentid in ("+tempPartnerId+") and partnerCode='"+partnerCode+"' and corpID='"+corpID+"'").executeUpdate();	
			}
			
		}
		public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID){			
			List <String>list  = this.getSession().createSQLQuery("Select concat(if(description is null,' ',description),'~',if((branch is null or branch='' or branch='No'),'No','Yes'),'~',if((bucket2 is null or bucket2=''),'NoBucket',bucket2)) from refmaster where parameter='"+parameter+"' and (language='en' or language='' or language is null )").list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for(String pageL: list){
				 String arr[]=pageL.split("~");
				 if((arr[0]!=null)&&(!arr[0].toString().equalsIgnoreCase(""))){
				 parameterMap.put(arr[0].toString(), arr[1].toString()+"#"+arr[2].toString());
				 }
			}
			return parameterMap;		
		}
		
		public List agentUpdatedIn24Hour(){
			List agentUpdated = new ArrayList();
			List DTO = new ArrayList();
			agentUpdated = this.getSession().createSQLQuery("select partnercode,lastname,updatedon,updatedby from partnerpublic where ((select DATEDIFF(updatedon, now())) = -1 or (select DATEDIFF(updatedon, now())) = 0  or (select DATEDIFF(createdon, now())) = -1 or (select DATEDIFF(createdon, now())) = 0)and corpid='TSFT' and isAgent=true").list();
			Iterator it = agentUpdated.iterator();
			//agentUpdated.clear();
			AgentUpdateSchedularDTO dto = null;
			while(it.hasNext()){
				dto = new AgentUpdateSchedularDTO();
				Object[] object = (Object[])it.next();
				dto.setPartnerCode(object[0].toString());
				dto.setLastName(object[1].toString());
				dto.setUpdatedOn(object[2].toString());
				dto.setUpdatedBy(object[3].toString());
				DTO.add(dto);
			}
			return DTO;
		}
		
		public Boolean getPrivatePartyStatus(String billToCode, String corpId){
			Boolean flag = false;
			try{
			String query = "SELECT isPrivateParty FROM partnerpublic where partnerCode = '"+billToCode+"' and corpId='"+corpId+"' and isPrivateParty=true";
			List l1 = getSession().createSQLQuery(query).list();
			if(l1.size()>0){
				flag = true;
			}
			}catch(Exception e){
				e.printStackTrace();
				return flag;
			}
			return flag;
		}
		
		public void updateDefaultAccountLine(String corpId,String partnerCode,String partnerType,String userName){
			String columnName1="";
			String columnName2="";
			try{
			if(partnerType.equals("B")){
				columnName1="billToCode";
				columnName2="billToName";
			}
			if(partnerType.equals("V")){
				columnName1="vendorCode";
				columnName2="vendorName";
			}
			getSession().createSQLQuery("update defaultaccountline set "+columnName1+" = '',"+columnName2+" = '', updatedOn = now(), updatedBy = '"+userName+"' where "+columnName1+"='"+partnerCode+"' and corpId='"+corpId+"'").executeUpdate();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	public PartnerPublic checkById(Long id)
		{List pList=getSession().createSQLQuery("select * from partnerpublic where id='"+id+"'").addEntity(PartnerPublic.class).list();
			if(pList!=null && !pList.isEmpty())
			{
				return (PartnerPublic)pList.get(0);
			}
			else
			{ 
				PartnerPublic p=new PartnerPublic();
					return p;
			}
		}
	public int updateAgentParentName(String lastName,String partnerCode,String updatedBy) {
		lastName = lastName.replaceAll("'", "''");
		getHibernateTemplate().bulkUpdate("update PartnerPublic set updatedOn = now(), updatedBy = '"+updatedBy+"',agentParentName='"+lastName+"'  where agentParent ='"+partnerCode+"'  ");
		return 0;
	}

	public void updatePartnerPortal(String partnerCode) {
		getSession().createSQLQuery("update partnerpublic set partnerPortalActive=true where partnerCode ='"+partnerCode+"'  ").executeUpdate();
		
	}

}
