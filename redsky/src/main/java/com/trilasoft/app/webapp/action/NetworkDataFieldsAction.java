package com.trilasoft.app.webapp.action;




import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.NetworkDataFields;
import com.trilasoft.app.service.NetworkDataFieldsManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;



public class NetworkDataFieldsAction extends BaseAction implements Preparable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List nlist;
	private List  combo1Map;
	NetworkDataFields networkDataFields;
	private ExtendedPaginatedList networkDataFieldsExt;
	NetworkDataFieldsManager networkDataFieldsManager;
	private PaginateListFactory paginateListFactory;
	private PagingLookupManager pagingLookupManager;
	private Long id;
	public String hitflag;
	private String modelName;
	private String fieldName;
	private String mName;
	private String transactionType;
	private  List trType;
	private  List type;
	private String sessionCorpID;
	private List fieldList = new ArrayList();
	private  String tranType;
	private  String types;
	public String networkDataFieldListURL;
	public String paginationNum;
	
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getHitflag() {
		return hitflag;
	}
	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public NetworkDataFieldsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	public void prepare() throws Exception {
	    
    }
	
	public String list() {
		getComboList();
		nlist = networkDataFieldsManager.getAll();
		return SUCCESS;
	}
	
	  
	  @SkipValidation
	  public String search(){ 
		 getComboList();
		 if(types==null){
			 types="";
		 }
		 if(tranType==null){
			 tranType="";
		 }
		 nlist=networkDataFieldsManager.search(mName,fieldName,types,tranType);
		 return SUCCESS;     
	    } 
	  
	  public String edit(){
		getComboList();
		if (id != null) {
			networkDataFields = networkDataFieldsManager.get(id);
			networkDataFields.setFieldName(networkDataFields.getFieldName().trim());
			fieldList=networkDataFieldsManager.getFieldList(networkDataFields.getModelName());
			}else{
				networkDataFields=new NetworkDataFields();
				networkDataFields.setCreatedOn(new Date());
		}
		return SUCCESS;
	}

	public String save() throws Exception{  
		getComboList();
		//validation
		fieldList=networkDataFieldsManager.getFieldList(networkDataFields.getModelName());
		try {
			if (networkDataFields.getModelName().trim() == null
					|| networkDataFields.getModelName().trim().isEmpty()) {
				errorMessage("Please select Model Name.");
				return INPUT;
			}

			if (networkDataFields.getFieldName().trim() == null
					|| networkDataFields.getFieldName().trim().isEmpty()) {
				errorMessage("Please select Field Name.");
				return INPUT;
			}

			if (networkDataFields.getTransactionType().trim() == null
					|| networkDataFields.getTransactionType().trim().isEmpty()) {
				errorMessage("Please select Transaction Type.");
				return INPUT;
			}
		} catch (NullPointerException n) {
			n.printStackTrace();
			if(!(sessionCorpID.equalsIgnoreCase("TSFT"))){
				errorMessage("You are not authorized for save.");
			}else{
				errorMessage("Please select mandatory field.");
			}
			return INPUT;
		}
		boolean isNew = (networkDataFields.getId() == null);
		if (isNew) {
			networkDataFields.setCreatedOn(new Date());
			//CHECK FOR DUPLICATE RECORD
			try {
				boolean flag = networkDataFieldsManager.checkDuplicateRecord(
						networkDataFields.getModelName(),
						networkDataFields.getFieldName(),
						networkDataFields.getTransactionType());
				if (flag) {
					networkDataFields = networkDataFieldsManager.save(networkDataFields);
					String key = (isNew) ? "Record has been added successfully" : "Record has been Updated successfully";
					saveMessage(getText(key));
				} else {
					errorMessage("Not to save, because record all ready exist.");
					return INPUT;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			NetworkDataFields networkDataFields1 = networkDataFieldsManager.get(networkDataFields.getId());
			networkDataFields.setUpdatedOn(new Date());
			networkDataFields.setUpdatedBy(getRequest().getRemoteUser());
			
			if(networkDataFields.getModelName().equalsIgnoreCase(networkDataFields1.getModelName()) && networkDataFields.getFieldName().equalsIgnoreCase( networkDataFields1.getFieldName()) && networkDataFields.getTransactionType().equalsIgnoreCase(networkDataFields1.getTransactionType())){
				networkDataFields = networkDataFieldsManager.save(networkDataFields);
				String key = (isNew) ? "Record has been added successfully" : "Record has been Updated successfully";
				saveMessage(getText(key));
			}else{
				try {
					boolean flag = networkDataFieldsManager.checkDuplicateRecord(
							networkDataFields.getModelName(),
							networkDataFields.getFieldName(),
							networkDataFields.getTransactionType());
					if (flag) {
						networkDataFields = networkDataFieldsManager.save(networkDataFields);
						String key = (isNew) ? "Record has been added successfully" : "Record has been Updated successfully";
						saveMessage(getText(key));
					} else {
						errorMessage("Not to save, because record all ready exist.");
						return INPUT;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		return SUCCESS;
	}
	
	public String getComboList() { 
		combo1Map=networkDataFieldsManager.findForCombo1Map();
		
		trType = new ArrayList();
		trType.add("Create");
		trType.add("Update");
		
		type = new ArrayList();
		type.add("CMM");
		type.add("DMM");
		type.add("CMM/DMM");
        return SUCCESS;   
    }
	
	public String delete(){
		networkDataFieldsManager.remove(id);
		saveMessage(getText("Record has been deleted successfully"));
		if((fieldName != null && fieldName.toString().trim().length()>0) || (mName != null && mName.toString().trim().length()>0)){
			networkDataFieldListURL = "?d-148826-p="+paginationNum+"&mName="+mName+"&fieldName="+fieldName+"&method:search=Search";
			   return "SEARCH";
		 }
		if (paginationNum != null && !"".equals(paginationNum)) {
			networkDataFieldListURL = "?d-148826-p="+paginationNum;
		}
		return SUCCESS;
	}
	
	public List getNlist() {
		return nlist;
	}

	public void setNlist(List nlist) {
		this.nlist = nlist;
	}

	public void setNetworkDataFieldsManager(
			NetworkDataFieldsManager networkDataFieldsManager) {
		this.networkDataFieldsManager = networkDataFieldsManager;
	}

	public NetworkDataFields getNetworkDataFields() {
		return networkDataFields;
	}
	public void setNetworkDataFields(NetworkDataFields networkDataFields) {
		this.networkDataFields = networkDataFields;
	}
	public List getCombo1Map() {
		return combo1Map;
	}
	public void setCombo1Map(List combo1Map) {
		this.combo1Map = combo1Map;
	}
	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}
	public ExtendedPaginatedList getNetworkDataFieldsExt() {
		return networkDataFieldsExt;
	}
	public void setNetworkDataFieldsExt(ExtendedPaginatedList networkDataFieldsExt) {
		this.networkDataFieldsExt = networkDataFieldsExt;
	}
	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}
	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public List getTrType() {
		return trType;
	}
	public void setTrType(List trType) {
		this.trType = trType;
	}
	public List getType() {
		return type;
	}
	public void setType(List type) {
		this.type = type;
	}
	
	public String getPaginationNum() {
		return paginationNum;
	}
	public void setPaginationNum(String paginationNum) {
		this.paginationNum = paginationNum;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getFieldList() {
		return fieldList;
	}
	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTranType() {
		return tranType;
	}
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	public String getTypes() {
		return types;
	}
	public void setTypes(String types) {
		this.types = types;
	}
	public void setNetworkDataFieldListURL(String networkDataFieldListURL) {
		this.networkDataFieldListURL = networkDataFieldListURL;
	}
	
}
