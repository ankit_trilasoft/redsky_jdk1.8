package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AccountProfileDao;
import com.trilasoft.app.model.AccountProfile;

public class AccountProfileDaoHibernate extends GenericDaoHibernate<AccountProfile, Long> implements AccountProfileDao {

	public AccountProfileDaoHibernate() {   
        super(AccountProfile.class);   
    }
	
	public List<AccountProfile> findAccountProfileByPC(String partnerCode, String corpID){
		return getHibernateTemplate().find("from AccountProfile where corpID = '"+corpID+"' AND partnerCode=?", partnerCode);
	}
	
	public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from AccountProfile");
    }

	public List getRelatedNotesForProfile(String corpID, String partnerCode) {
		return getHibernateTemplate().find("from Notes where notetype='AccountContact' and corpID='"+corpID+"' and notesId='"+partnerCode+"'");
	}
	public List findAccountProfile(String corpID, String partnerCode){
		return getHibernateTemplate().find("from AccountProfile  where corpID='"+corpID+"' and PartnerCode=?",partnerCode);
	}

	public List getAccountOwner(String corpID) {
		return this.getSession().createSQLQuery("SELECT distinct (alias) FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE and role.name='ROLE_SALE' and app_user.corpid in ('" + corpID + "') order by username").list();
		//return getHibernateTemplate().find("select alias from User where corpID='"+corpID+"'");
	}
	
}
