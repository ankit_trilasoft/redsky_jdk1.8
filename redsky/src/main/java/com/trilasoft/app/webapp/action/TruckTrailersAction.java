package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.webapp.action.BaseAction;

import org.appfuse.model.User;
import org.acegisecurity.Authentication;

import com.trilasoft.app.model.TruckTrailers;
import com.trilasoft.app.service.TruckTrailersManager;

public class TruckTrailersAction extends BaseAction{
	
	private Long id;
	private String sessionCorpID;
	private TruckTrailers truckTrailers;
	private TruckTrailersManager truckTrailersManager;
	private List truckTrailerss;
	private String truckNumber;
	private List primaryFlagStatus = new ArrayList();
	private String primaryFlags;
	private String primaryFlagId;
	private String hitFlag;
	private List trailerPopupList;
	private String localNumber;
	private String ownerPayTo;
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(TruckTrailersAction.class);

	
	public String getPrimaryFlagId() {
		return primaryFlagId;
	}
	public void setPrimaryFlagId(String primaryFlagId) {
		this.primaryFlagId = primaryFlagId;
	}
	public String getPrimaryFlags() {
		return primaryFlags;
	}
	public void setPrimaryFlags(String primaryFlags) {
		this.primaryFlags = primaryFlags;
	}
	public List getPrimaryFlagStatus() {
		return primaryFlagStatus;
	}
	public void setPrimaryFlagStatus(List primaryFlagStatus) {
		this.primaryFlagStatus = primaryFlagStatus;
	}
	public String getTruckNumber() {
		return truckNumber;
	}
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}
	public TruckTrailersAction(){
		Authentication auth=SecurityContextHolder.getContext().getAuthentication();
		User user=(User)auth.getPrincipal();
		this.sessionCorpID=user.getCorpID();
		
	}
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		truckTrailerss=truckTrailersManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String trailerList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		truckTrailerss = truckTrailersManager.findTruckTrailersList(truckNumber, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
	}
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if(id != null){
			truckTrailers=truckTrailersManager.get(id);
		}
		else{
			truckTrailers=new TruckTrailers();
			truckTrailers.setCreatedOn(new Date());
			truckTrailers.setCorpID(sessionCorpID);
			truckTrailers.setTruckNumber(truckNumber);
			truckTrailers.setUpdatedOn(new Date());
		}
		primaryFlagStatus=truckTrailersManager.getPrimaryFlagStatus(truckNumber, sessionCorpID);
		if(!primaryFlagStatus.isEmpty()){
			  Iterator it= primaryFlagStatus.iterator();
				while(it.hasNext()){
					Object [] row=(Object[])it.next();
					primaryFlagId=row[0].toString();
					primaryFlags=row[1].toString();
				}
		}
		  if(primaryFlagStatus.isEmpty()){
			  primaryFlags="show";
		  }
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
		
	}
	public String save(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (truckTrailers.getId()==null);
		truckTrailers.setCorpID(sessionCorpID);
		truckTrailers.setUpdatedOn(new Date());
		truckTrailers.setUpdatedBy(getRequest().getRemoteUser());
		truckTrailers = truckTrailersManager.save(truckTrailers);
		primaryFlagStatus=truckTrailersManager.getPrimaryFlagStatus(truckNumber, sessionCorpID);
		if(!primaryFlagStatus.isEmpty()){
			  Iterator it= primaryFlagStatus.iterator();
				while(it.hasNext()){
					Object [] row=(Object[])it.next();
					primaryFlagId=row[0].toString();
					primaryFlags=row[1].toString();
				}
		}
		  if(primaryFlagStatus.isEmpty()){
			  primaryFlags="show";
		  }
		
		String key = (isNew) ? "Truck Trailer has been added successfully" : "Truck Trailer has been updated successfully";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      	return SUCCESS;
		
	}
	@SkipValidation
	public String trailersPopup(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		trailerPopupList=truckTrailersManager.trailersPopup();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       return SUCCESS;
	}
	public String searchTrailerPopupList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		trailerPopupList=truckTrailersManager.searchTrailerPopupList(localNumber,ownerPayTo,sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
	}
	
	public String delete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		truckTrailersManager.remove(id);
		hitFlag="1";
		String key2 ="Truck Trailer has been removed successfully.";
		saveMessage(getText(key2));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public TruckTrailers getTruckTrailers() {
		return truckTrailers;
	}
	public void setTruckTrailers(TruckTrailers truckTrailers) {
		this.truckTrailers = truckTrailers;
	}
	public List getTruckTrailerss() {
		return truckTrailerss;
	}
	public void setTruckTrailerss(List truckTrailerss) {
		this.truckTrailerss = truckTrailerss;
	}
	public void setTruckTrailersManager(TruckTrailersManager truckTrailersManager) {
		this.truckTrailersManager = truckTrailersManager;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public List getTrailerPopupList() {
		return trailerPopupList;
	}
	public void setTrailerPopupList(List trailerPopupList) {
		this.trailerPopupList = trailerPopupList;
	}
	public String getLocalNumber() {
		return localNumber;
	}
	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}
	public String getOwnerPayTo() {
		return ownerPayTo;
	}
	public void setOwnerPayTo(String ownerPayTo) {
		this.ownerPayTo = ownerPayTo;
	}
	

}
