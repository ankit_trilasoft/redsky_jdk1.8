package com.trilasoft.app.service;

import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.PartnerRates;

import java.util.List; 

public interface PartnerRatesManager extends GenericManager<PartnerRates, Long> {
	public List findMaximum();
}
