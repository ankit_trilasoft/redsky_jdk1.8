package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.HolidayMaintenance;

public interface HolidayMaintenanceManager extends GenericManager<HolidayMaintenance, Long>{
	public List checkHoliDayList(String type,Date hDayDate,String corpID);
	public List getHoliDayMaintenanceList(String corpID);
}
