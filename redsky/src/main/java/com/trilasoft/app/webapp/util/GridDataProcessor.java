package com.trilasoft.app.webapp.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.PropertyUtils;

import com.trilasoft.app.webapp.json.JSONArray;
import com.trilasoft.app.webapp.json.JSONException;

public class GridDataProcessor {
	
	public static List<Map> getRowData (String listData, String listFieldNames, String listFieldTypes, String listFieldEditability, String listIdFieldName) throws JSONException{
			
		JSONArray rows = new JSONArray(listData);
		String[] fields = listFieldNames.split(",");
		String[] fieldTypes = listFieldTypes.split(",");
		String[] editability = listFieldEditability.split(",");
		// need to support edit in list
		List<Map> rowValues = new ArrayList<Map>();
		for (int i = 0; i < rows.length(); i++) {
			JSONArray row = (JSONArray)rows.get(i);
			Map values = new HashMap();
			for (int j = 0; j < row.length(); j++) {
				String value = row.get(j).toString();
				if(value.equalsIgnoreCase("")|| value.equalsIgnoreCase("undefined"))
				{
					value="0";
				}
				if (fields[j].equals(listIdFieldName)){
					values.put(fields[j], Long.parseLong(value)); //assuming the id field is of type long
				} 				
				if (editability[j].equals("editable")) {
					if (fieldTypes[j].equals("int")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], 0);
						}else{
							values.put(fields[j], Integer.parseInt(value));
						}
					} else if (fieldTypes[j].equals("long")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], 0);
						}else{
							values.put(fields[j], Long.parseLong(value));
						}
					} else if (fieldTypes[j].equals("double")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], Double.parseDouble("0.0"));
						}else{
							values.put(fields[j], Double.parseDouble(value));
						}
					} else if (fieldTypes[j].equals("float")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], Double.parseDouble("0.0"));
						}else{
							values.put(fields[j], Double.parseDouble(value));
						}
					} else if (fieldTypes[j].equals("text")) {
						values.put(fields[j], value);
					} else if (fieldTypes[j].equals("string")) {
						values.put(fields[j], value);
					} else {
						values.put(fields[j], Double.parseDouble(value));
					}
				}
			}
			rowValues.add(values);
		}
		return rowValues;
	}
	//
	//Updated for #8612
public static List<Map> getRowDataMapForSave(String listData, String listFieldNames, String listFieldTypes, String listFieldEditability, String listIdFieldName) throws JSONException{
		
		JSONArray rows = new JSONArray(listData);
		String[] fields = listFieldNames.split(",");
		String[] fieldTypes = listFieldTypes.split(",");
		String[] editability = listFieldEditability.split(",");
		// need to support edit in list
		List<Map> rowValues = new ArrayList<Map>();
		for (int i = 0; i <rows.length() ; i++) {
			JSONArray row = (JSONArray)rows.get(i);
			Map values = new HashMap();
			for (int j = 0; j <row.length(); j++) {
				String value = row.get(j).toString();
				if(value.equalsIgnoreCase("")|| value.equalsIgnoreCase("undefined"))
				{
					value="0";
				}
				if (fields[j].equals(listIdFieldName)){
					values.put(fields[j], Long.parseLong(value)); //assuming the id field is of type long
				} 				
				if (editability[j].equals("editable")) {
					if (fieldTypes[j].equals("int")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], 0);
						}else{
							values.put(fields[j], Integer.parseInt(value));
						}
					} else if (fieldTypes[j].equals("long")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], 0);
						}else{
							values.put(fields[j], Long.parseLong(value));
						}
					} else if (fieldTypes[j].equals("double")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], Double.parseDouble("0.0"));
						}else{
							values.put(fields[j], Double.parseDouble(value));
						}
					} else if (fieldTypes[j].equals("float")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], Double.parseDouble("0.0"));
						}else{
							values.put(fields[j], Double.parseDouble(value));
						}
					} else if (fieldTypes[j].equals("text")) {
						values.put(fields[j], value);
					} else if (fieldTypes[j].equals("string")) {
						values.put(fields[j], value);
					} else {
						values.put(fields[j], Double.parseDouble(value));
					}
				}
			}
			rowValues.add(values);
		}
		return rowValues;
	}
//
public static List<Map> getRowDataMapForUpdate(String listData, String listFieldNames, String listFieldTypes, String listFieldEditability, String listIdFieldName) throws JSONException{
	
	JSONArray rows = new JSONArray(listData);
	String[] fields = listFieldNames.split(",");
	String[] fieldTypes = listFieldTypes.split(",");
	String[] editability = listFieldEditability.split(",");
	// need to support edit in list
	List<Map> rowValues = new ArrayList<Map>();
	for (int i = 0; i <rows.length() ; i++) {
		JSONArray row = (JSONArray)rows.get(i);
		Map values = new HashMap();
		for (int j = 0; j <=7; j++) {
			String value = row.get(j).toString();
			if(value.equalsIgnoreCase("")|| value.equalsIgnoreCase("undefined"))
			{
				value="0";
			}
			if (fields[j].equals(listIdFieldName)){
				values.put(fields[j], Long.parseLong(value)); //assuming the id field is of type long
			} 				
			if (editability[j].equals("editable")) {
				if (fieldTypes[j].equals("int")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], 0);
					}else{
						values.put(fields[j], Integer.parseInt(value));
					}
				} else if (fieldTypes[j].equals("long")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], 0);
					}else{
						values.put(fields[j], Long.parseLong(value));
					}
				} else if (fieldTypes[j].equals("double")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], Double.parseDouble("0.0"));
					}else{
						values.put(fields[j], Double.parseDouble(value));
					}
				} else if (fieldTypes[j].equals("float")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], Double.parseDouble("0.0"));
					}else{
						values.put(fields[j], Double.parseDouble(value));
					}
				} else if (fieldTypes[j].equals("text")) {
					values.put(fields[j], value);
				} else if (fieldTypes[j].equals("string")) {
					values.put(fields[j], value);
				} else {
					values.put(fields[j], Double.parseDouble(value));
				}
			}
		}
		rowValues.add(values);
	}
	return rowValues;
}
//
public static List<Map> getRowDataMiscMap (String listData, String listFieldNames, String listFieldTypes, String listFieldEditability, String listIdFieldName) throws JSONException{
	
	JSONArray rows = new JSONArray(listData);
	String[] fields = listFieldNames.split(",");
	String[] fieldTypes = listFieldTypes.split(",");
	String[] editability = listFieldEditability.split(",");
	// need to support edit in list
	List<Map> rowValues = new ArrayList<Map>();
	for (int i = 0; i < rows.length(); i++) {
		JSONArray row = (JSONArray)rows.get(i);
		Map values = new HashMap();
		for (int j = 0; j <=5; j++) {
			String value="";
			if(row.get(j)!=null){
			 value = row.get(j).toString();
			}
			if(value.equalsIgnoreCase("")|| value.equalsIgnoreCase("undefined"))
			{
				value="0";
			}
			if (fields[j].equals(listIdFieldName)){
				values.put(fields[j], Long.parseLong(value)); //assuming the id field is of type long
			} 				
			if (editability[j].equals("editable")) {
				if (fieldTypes[j].equals("int")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], 0);
					}else{
						values.put(fields[j], Integer.parseInt(value));
					}
				} else if (fieldTypes[j].equals("long")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], 0);
					}else{
						values.put(fields[j], Long.parseLong(value));
					}
				} else if (fieldTypes[j].equals("double")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], Double.parseDouble("0.0"));
					}else{
						values.put(fields[j], Double.parseDouble(value));
					}
				} else if (fieldTypes[j].equals("float")) {
					if(value.equalsIgnoreCase("NaN")){
						values.put(fields[j], Double.parseDouble("0.0"));
					}else{
						values.put(fields[j], Double.parseDouble(value));
					}
				} else if (fieldTypes[j].equals("text")) {
					values.put(fields[j], value);
				} else if (fieldTypes[j].equals("string")) {
					values.put(fields[j], value);
				} else {
					values.put(fields[j], Double.parseDouble(value));
				}
			}
		}
		rowValues.add(values);
	}
	return rowValues;
}
	
	public static List<Map> getRowDataMap (String listData, String listFieldNames, String listFieldTypes, String listFieldEditability, String listIdFieldName) throws JSONException{
		
		JSONArray rows = new JSONArray(listData);
		String[] fields = listFieldNames.split(",");
		String[] fieldTypes = listFieldTypes.split(",");
		String[] editability = listFieldEditability.split(",");
		// need to support edit in list
		List<Map> rowValues = new ArrayList<Map>();
		for (int i = 0; i < rows.length(); i++) {
			JSONArray row = (JSONArray)rows.get(i);
			Map values = new HashMap();
			for (int j = 0; j < row.length(); j++) {
				String value = row.get(j).toString();
				if(value.equalsIgnoreCase("")|| value.equalsIgnoreCase("undefined"))
				{
					value="0";
				}
				if (fields[j].equals(listIdFieldName)){
					values.put(fields[j], Long.parseLong(value)); //assuming the id field is of type long
				} 				
				if (editability[j].equals("editable")) {
					if (fieldTypes[j].equals("int")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], 0);
						}else{
							values.put(fields[j], Integer.parseInt(value));
						}
					} else if (fieldTypes[j].equals("long")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], 0);
						}else{
							values.put(fields[j], Long.parseLong(value));
						}
					} else if (fieldTypes[j].equals("double")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], Double.parseDouble("0.0"));
						}else{
							values.put(fields[j], Double.parseDouble(value));
						}
					} else if (fieldTypes[j].equals("float")) {
						if(value.equalsIgnoreCase("NaN")){
							values.put(fields[j], Double.parseDouble("0.0"));
						}else{
							values.put(fields[j], Double.parseDouble(value));
						}
					} else if (fieldTypes[j].equals("text")) {
						values.put(fields[j], value);
					} else if (fieldTypes[j].equals("string")) {
						values.put(fields[j], value);
					} else {
						values.put(fields[j], Double.parseDouble(value));
					}
				}
			}
			rowValues.add(values);
		}
		return rowValues;
	}
	//
	
	public static String processHtml(String htmlCode, Object bean, String pattern) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Pattern fiedDelimiterPattern = Pattern.compile(pattern);
		StringBuffer processedHtml = new StringBuffer();
		Matcher matcher = fiedDelimiterPattern.matcher(htmlCode);
		while (matcher.find()) {
			String paramFieldName = htmlCode.substring(matcher.start() + 8, matcher
					.end() -1);
			//System.out.println("************paramFieldName *****************"+paramFieldName );
			String paramValue = PropertyUtils.getProperty(bean, paramFieldName).toString();
			matcher.appendReplacement(processedHtml,paramValue);
		}
		matcher.appendTail(processedHtml);		
		return processedHtml.toString();
	}
	public static List<Map> getRowDatas (String listData, String listFieldNames, String listFieldTypes, String listFieldEditability, String listIdFieldName) throws JSONException{
		
		JSONArray rows = new JSONArray(listData);
		String[] fields = listFieldNames.split(",");
		String[] fieldTypes = listFieldTypes.split(",");
		String[] editability = listFieldEditability.split(",");
		// need to support edit in list
		List<Map> rowValues = new ArrayList<Map>();
		for (int i = 0; i < rows.length(); i++) {
			JSONArray row = (JSONArray)rows.get(i);
			Map values = new HashMap();
			for (int j = 0; j < row.length(); j++) {
				String value = row.get(j).toString();
				if(value.equalsIgnoreCase("undefined")||value.equalsIgnoreCase(""))
				{
					value="0";
				}
				if (fields[j].equals(listIdFieldName)){
					values.put(fields[j], Long.parseLong(value)); //assuming the id field is of type long
				} 				
				if (editability[j].equals("editable")) {
					if (fieldTypes[j].equals("int")) {
						values.put(fields[j], Integer.parseInt(value));
					} else if (fieldTypes[j].equals("long")) {
						values.put(fields[j], Long.parseLong(value));
					} else if (fieldTypes[j].equals("double")) {
						values.put(fields[j], Double.parseDouble(value));
					} else if (fieldTypes[j].equals("text")) {
						values.put(fields[j], value);
					}else if (fieldTypes[j].equals("string")) {
						values.put(fields[j], value);
					}
					else
					{
						values.put(fields[j], Double.parseDouble(value));
					}
				}
			}
			rowValues.add(values);
		}
		return rowValues;
	}
}

