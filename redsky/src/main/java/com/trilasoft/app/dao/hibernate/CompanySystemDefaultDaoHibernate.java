package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CompanySystemDefaultDao;
import com.trilasoft.app.model.CompanySystemDefault;


public class CompanySystemDefaultDaoHibernate extends GenericDaoHibernate<CompanySystemDefault, Long> implements CompanySystemDefaultDao {

	public CompanySystemDefaultDaoHibernate() {
		super(CompanySystemDefault.class);
	}

	/*public List<CompanySystemDefault> findByCorpID(String sessionCorpID) {
		System.out.println("from CompanySystemDefault where corpID='"+sessionCorpID+"'" );
		List CompanySystemDefaultList= getHibernateTemplate().find("from CompanySystemDefault where corpID=?", sessionCorpID);
		System.out.println("\n CompanySystemDefaultList Value" +CompanySystemDefaultList );
		return CompanySystemDefaultList;
	}*/

}
