package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerPrivateDao;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.PartnerPrivateManager;

public class PartnerPrivateManagerImpl extends GenericManagerImpl<PartnerPrivate, Long> implements PartnerPrivateManager {   
	PartnerPrivateDao partnerPrivateDao;   
	  
    public PartnerPrivateManagerImpl(PartnerPrivateDao partnerPrivateDao) {   
        super(partnerPrivateDao);   
        this.partnerPrivateDao = partnerPrivateDao;   
    }
    
    public PartnerPrivate findPartnerPrivateByCode(String partnerCode,String sessionCorpID){
    	return partnerPrivateDao.findPartnerPrivateByCode(partnerCode, sessionCorpID);
    }

	public List findMaxByCode() {
		return partnerPrivateDao.findMaxByCode();
	}

	public List findPartnerCode(String partnerCode) {
		return partnerPrivateDao.findPartnerCode(partnerCode);
	}

	public List getPartnerPrivateList(String partnerType, String sessionCorpID, String firstName, String lastName, String partnerCode, String status) {
		return partnerPrivateDao.getPartnerPrivateList(partnerType, sessionCorpID, firstName, lastName, partnerCode, status);
	}

	public List getPartnerPrivateListByPartnerCode(String sessionCorpID, String partnerCode) {
		return partnerPrivateDao.getPartnerPrivateListByPartnerCode(sessionCorpID, partnerCode);
	}
	
	public List getPartnerPrivate(String partnerCode, String corpID) {
		return partnerPrivateDao.getPartnerPrivate(partnerCode, corpID);
	}

	public List isExistPartnerPrivate(String corpID, String partnerCode) {
		return partnerPrivateDao.isExistPartnerPrivate(corpID, partnerCode);
	}
	
	public List getIsoCode(String vatNumber){
		return partnerPrivateDao.getIsoCode(vatNumber);
	}

	public List getPartnerPrivateId(String partnerCode) {
		return partnerPrivateDao.getPartnerPrivateId(partnerCode);
	}
	public String getTheCreditTerm(String partnerCode, String corpID){
		return partnerPrivateDao.getTheCreditTerm(partnerCode, corpID);
	}
	 public List getGroupAgePermission(String billToCode,String corpID){
		 return partnerPrivateDao.getGroupAgePermission(billToCode,corpID);
		 
	 }
	public String getUsedAmount(String partnerCode,String tempCurrency,String corpID){
		return partnerPrivateDao.getUsedAmount(partnerCode,tempCurrency,corpID);
	}
	public List findprivateParty(String partnerCode,String corpID){
		return partnerPrivateDao.findprivateParty(partnerCode,corpID);
	}
	public String getBillMc(String billToCode, String sessionCorpID){
		return partnerPrivateDao.getBillMc(billToCode, sessionCorpID);
	}

	public Map<String, String> getPartnerPrivateDescLinkMap(String partnerCode,String CorpID) {
		return partnerPrivateDao.getPartnerPrivateDescLinkMap(partnerCode, CorpID);
	}
	public int updatePartnerPrivate(String excludeFPU,String sessionCorpID,String partnerCode,String excludeOption){
		return partnerPrivateDao.updatePartnerPrivate(excludeFPU, sessionCorpID, partnerCode,excludeOption);
	}
	public List getPartnerPrivateDefaultAccount(String partnerCode,String corpId){
		return partnerPrivateDao.getPartnerPrivateDefaultAccount(partnerCode, corpId);
	}
	public List getPartnerPrivateChildAccount(String partnerCode,String corpId){
		return partnerPrivateDao.getPartnerPrivateChildAccount(partnerCode, corpId);
	}
	public void updateLastName(String sessionCorpID, String lName, Long ppId){
		partnerPrivateDao.updateLastName(sessionCorpID,lName, ppId);
	}
	public List<SystemDefault> findsysDefault(String corpID){
		return partnerPrivateDao.findsysDefault(corpID);
	}
	public List getPartnerPrivateByPartnerCode(String otherCorpId,	String partnerCode){
		return partnerPrivateDao.getPartnerPrivateByPartnerCode(otherCorpId, partnerCode);
	}
	public List findContract(String sessionCorpID){
		return partnerPrivateDao.findContract(sessionCorpID);
	}
	public Double getminMargin(String billToCode){
		return partnerPrivateDao.getminMargin(billToCode);
	}
	public String getPartnerCodeFromDefaultTemplate(String sessionCorpID,String partnerCode){
		return partnerPrivateDao.getPartnerCodeFromDefaultTemplate(sessionCorpID,partnerCode);
	}
	public int getParentCompensationYear(String corpId,String agentParent){
		return partnerPrivateDao.getParentCompensationYear(corpId,agentParent);
	}

	public List findDoNotInvoicePartnerList(String sessionCorpID) {
		return partnerPrivateDao.findDoNotInvoicePartnerList(sessionCorpID);
	}
	
	public List getDefaultContactUsersList(String partnerCode, String sessionCorpID)
	{
		return partnerPrivateDao.getDefaultContactUsersList(partnerCode, sessionCorpID);
	}
	 public List loadUserByUsername(String userName) 
	 {
		 return partnerPrivateDao.loadUserByUsername(userName);
		 
	 }
	 public String getClassifiedAgentValue(String agentCode){
		 return partnerPrivateDao.getClassifiedAgentValue(agentCode);
	 }
     public PartnerPrivate checkById(Long id){
		 return partnerPrivateDao.checkById(id);
		 
	 }
     public List getBillingCycle(String billToCode,String sessionCorpID){
  		 return partnerPrivateDao.getBillingCycle(billToCode, sessionCorpID);
  		 
  	 }
	public List findVatBillimngGroup(String sessionCorpID, String partnerCode) {
		// TODO Auto-generated method stub
		return partnerPrivateDao.findVatBillimngGroup(sessionCorpID, partnerCode);
	}

	public Map<String, String> getVatBillingGroupMap(String sessionCorpID, String parterCodeList) {
		return partnerPrivateDao.getVatBillingGroupMap(sessionCorpID, parterCodeList);
	}

	

	
}
