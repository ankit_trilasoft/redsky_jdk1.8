<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>IntelLead&nbsp;Form</title>
<meta name="heading" content="IntelLead&nbsp;Form" />
</head>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>   


<script language="javascript" type="text/javascript">
function orderCreated(target,id,authType){
    if(target.value=='Accept'){
        var agree= confirm("Are you sure you want to Accept this Lead?");
        if(agree){
             $.get("orderCreatedFromIntel.html?ajax=1&decorator=simple&popup=true",
                        {id:id,authType:authType},
                        function(data){
                        alert("Order is created successfuly. ")
                       self.opener.location.reload(); 
                        window.close();
                        /* setTimeout(function(){
                            refreshParent();
                              }, 2000);  */
                         
                            
                    });
           
        }else{
           
        }
    }
}


function refreshParent() {
window.close();
}
</script>
<s:form id="intelLeadFormId" name="intelLeadForm" action="intelLeadForm" onsubmit="" method="post" validate="true"> 
 <fmt:formatDate value="${intelLead.authDate}" var="authDate" pattern="dd-MMM-yyyy"/>
 <fmt:formatDate value="${intelLead.assignmentStartDate}" var="assignmentStartDate" pattern="dd-MMM-yyyy"/>
 <fmt:formatDate value="${intelLead.assignmentProjectedReturnDate}" var="assignmentProjectedReturnDate" pattern="dd-MMM-yyyy"/>
 <fmt:formatDate value="${intelLead.serviceStartDate}" var="serviceStartDate" pattern="dd-MMM-yyyy"/>    
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content"> 
    <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
          <tbody>
          <tr><td align="center" class="listwhitetext" style="font-size:14px;padding-bottom:5px;"><b>Intel VENAUTHs</b></td></tr>
           <tr>
            <td align="center" class="listblacktext" style="font-size:12px;padding-bottom:5px;"><b><i>Report Run on:</i></b></td>       
      		</tr>
          </tbody>
        </table>
      <table cellspacing="0" cellpadding="0" style="margin: 0px;border:2px solid #74b3dc;width:100%;"> 
      <tr>
         <td height="" align="left" class="listwhitetext" colspan="10" style="margin: 0px;">
        <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;background:url('images/price_tab_bg.png');line-height:26px;font-size:12px;">
          <tbody>
          <tr>
           <td align="right" style="color:#f38621;font-size:12px;" class="listblacktext" ><b>Supplier:&nbsp;</b></td>
            <td align="left" width=""><b><c:out value="${intelLead.supplierCode}"></c:out></b></td>
            <td width="10px"></td>
            <td align="right" style="color:#f38621;font-size:12px;"  class="listblacktext" ><b>Supplier Nm:&nbsp;</b></td>
            <td align="left" width=""><b><c:out value="${intelLead.supplierName}"></c:out></b></td>
            <td width="10px"></td>
            <td align="right" style="color:#f38621;font-size:12px;"  class="listblacktext" ><b>Svc Country:&nbsp;</b></td>
            <td align="left" width=""><b><c:out value="${intelLead.serviceCountryName}"></c:out>&nbsp; <c:out value="${intelLead.serviceCountryID}"></c:out></b></td>
            <td width="10px"></td>
           <%--  <td align="right" style="color:#f38621;font-size:12px;"  class="listblacktext" ><b>Moves ID:&nbsp;</b></td>
            <td align="left" width=""><b><c:out value="${intelLead.employeeName}"></c:out></b></td> --%>            
          </tr>
          </tbody>
        </table>
        </td>
        </tr>
        
      <tr>
   	 <td  width="100%" align="left">
        <table class="detailTabLabel" border="0" width="100%">
          <tbody> 
          <tr>
             <c:if test="${not empty intelLead.customerFileNumber }">
                <td align="right" class="listwhitetext" width="120px"> Customer&nbsp;File&nbsp;Number<b>:</b></td>
                <td align="left" class="listblacktext" width="140px"><b><c:out value="${intelLead.customerFileNumber}"></b></c:out></td> 
                </c:if>
                <c:if test="${not empty intelLead.serviceOrderNumber }">
                <td align="right" class="listwhitetext">Service&nbsp;Order&nbsp;Number<b>:</b></td>
              <td align="left" class="listblacktext"   width="200px"><b><c:out value="${intelLead.serviceOrderNumber}"></c:out></b></td></td>
              </c:if>             
            </tr>
                 
            <tr>
                <td align="right" class="listwhitetext" width="120px"> Employee&nbsp;<b>:</b></td>
                <td align="left" class="listblacktext" width="140px"><b><c:out value="${intelLead.employeeName}"></b></c:out></td> 
                <td align="right" class="listwhitetext">Emp&nbsp;ID&nbsp;<b>:</b></td>
              <td align="left" class="listblacktext"   width="200px"><b><c:out value="${intelLead.emplid}"></c:out></b></td></td>
            </tr>
            
              <tr>
                <td align="right" class="listwhitetext" >Assign&nbsp;Name&nbsp;<b>:</b></td>
                <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.assignmentName}"></c:out></b></td>  
                <td align="right" class="listwhitetext" >Assign&nbsp;Type&nbsp;<b>:</b></td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.assignmentType}"></c:out></b></td>
            </tr>
            
             <tr>
                <td align="right" class="listwhitetext" >Acct&nbsp;Name&nbsp;<b>:</b></td>
                <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.accountName}"></c:out></b></td>  
                <td align="right" class="listwhitetext" >Acct&nbsp;Desc&nbsp;<b>:</b></td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.accountDescription}"></c:out></b></td>
            </tr>
            
             <tr>
                <td align="right" class="listwhitetext" ><b>Auth&nbsp;ID&nbsp;</b><b>:</b></td>
                <td align="left" class="listblacktext"  ><s:textfield id="" name="intelLead.authID" required="true" readonly="true" cssClass="input-textUpper pr-f11" cssStyle="width:65px;background:none;" maxlength="8" /></td>    
                <td align="right" class="listwhitetext" ><b>Auth&nbsp;Type&nbsp;</b><b>:</b></td>
              	<td align="left" class="listblacktext"  ><b><c:out value="${intelLead.authType}"></c:out></b></td>              
              <td align="right" class="listwhitetext" >Auth&nbsp;Date&nbsp;<b>:</b></td>
              <td align="left" class="listblacktext"  width="125px"><b><c:out value="${authDate}"></c:out></b></td>            
            </tr>
            
             <tr>
                <td align="right" class="listwhitetext" >How&nbsp;Many&nbsp;<b>:</b></td>
                <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.howMany}"></c:out></b></td>    
                <td align="right" class="listwhitetext" >How&nbsp;Many&nbsp;Desc&nbsp;<b>:</b></td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.howManyDesc}"></c:out></b></td>              
            </tr>
            
            <tr>
                <td align="right" width="120px" class="listwhitetext" >Assign Start&nbsp;<b>:</b></td>
                <td align="left" class="listblacktext"  ><b><c:out value="${assignmentStartDate}"></c:out></b></td>  
                <td align="right" class="listwhitetext" >Assign&nbsp;Proj&nbsp;Return&nbsp;<b>:</b></td>
              	<td align="left" class="listblacktext" ><b><c:out value="${assignmentProjectedReturnDate}"></c:out></b></td>           
            	<td align="right" width="120px" class="listwhitetext" >Svc Start Date&nbsp;<b>:</b> </td>
                <td align="left" class="listblacktext" ><b><c:out value="${serviceStartDate}"></c:out></b></td>  
                <td align="right" class="listwhitetext" >Svc&nbsp;Proj&nbsp;End&nbsp;<b>:</b> </td>
              	<td align="left" class="listblacktext"  width="120px"><b><c:out value="${intelLead.serviceProjectedEndDate}"></c:out></b></td>  
            </tr>
            
            <tr>
                <td align="right" class="listwhitetext" >Duration&nbsp;<b>:</b> </td>
                <td align="left" class="listblacktext" ><b><c:out value="${intelLead.duration}"></c:out></b></td>    
                <td align="right" class="listwhitetext" >Flexibility&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.flexibility}"></c:out></b></td>
             <td align="right" class="listwhitetext" >Currency ID&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.currencyCode}"></c:out></b></td>
            </tr>
            
          <tr>
            <td align="left" class="listwhitetext" colspan="10" style="margin: 0px;padding: 0px;">
	        <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
	          <tbody>
	          <tr><td align="left" class="vertlinedata"></td></tr>
	          <tr class="subcontenttabChild" style="font-size:11px;"><td><b>&nbsp;Email / Phone</b></td></tr>
	          </tbody>
	        </table>
        	</td>
          </tr>           
          
          <tr>
          	<td align="right" width="120px" class="listwhitetext" >Email&nbsp;<b>:</b> </td>
            <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.emplid}"></c:out></b></td>
          </tr>
              
              <tr>                
                <td align="right" class="listwhitetext" >Home&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.homePhone}"></c:out></b></td>           
             <td align="right" class="listwhitetext" >Work&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.workPhone}"></c:out></b></td>           
            	<td align="right" class="listwhitetext" >Other&nbsp;<b>:</b> </td>
              <td align="left"class="listblacktext"  ><b><c:out value="${intelLead.otherPhone}"></c:out></b></td>          
            </tr> 
            
           <tr>
            <td align="left" class="listwhitetext" colspan="10" style="margin: 0px;padding: 0px;">
	        <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
	          <tbody>
	          <tr><td align="left" class="vertlinedata"></td></tr>
	          <tr class="subcontenttabChild" style="font-size:11px;"><td><b>&nbsp;Origin</b></td></tr>
	          </tbody>
	        </table>
        	</td>
          </tr>       
         
          <tr>
                <td align="right" class="listwhitetext" >Address1&nbsp;<b>:</b> </td>
                <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.fromAddress1}"></c:out></b></td>                
                <td align="right" class="listwhitetext" >Address2&nbsp;<b>:</b> </td>
                <td align="left" class="listblacktext"  ><b><c:out value="${intelLead.fromAddress2}"></c:out></b></td>                
              </tr>
              
              <tr>
                  <td align="right"  class="listwhitetext" >Address3&nbsp;<b>:</b> </td>
                <td align="left"  class="listblacktext" ><b><c:out value="${intelLead.fromAddress3}"></c:out></b></td>
              </tr>
              
              <tr> 
                <td align="right" class="listwhitetext" >City&nbsp;<b>:</b> </td>
              <td align="left"  class="listblacktext" ><b><c:out value="${intelLead.fromCity}"></c:out></b></td>           
             <td align="right" class="listwhitetext" >State&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext" ><b><c:out value="${intelLead.fromState}"></c:out></b></td>           
            <td align="right" class="listwhitetext" >Postal Code&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext" ><b><c:out value="${intelLead.fromPostalCode}"></c:out></b></td>           
            </tr> 
            
            <tr>
                <td align="right" class="listwhitetext" >Country&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext" ><b><c:out value="${intelLead.fromCountry}"></c:out></b></td>
            </tr> 
            
      
         <tr>
            <td align="left" class="listwhitetext" colspan="10" style="margin: 0px;padding: 0px;">
	        <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
	          <tbody>
	          <tr><td align="left" class="vertlinedata"></td></tr>
	          <tr class="subcontenttabChild" style="font-size:11px;"><td><b>&nbsp;Destination</b></td></tr>
	          </tbody>
	        </table>
        	</td>
          </tr> 
              
              <tr>
                <td align="right" class="listwhitetext" >City&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext" ><b><c:out value="${intelLead.toCityToState}"></c:out></b></td>           
             <td align="right" class="listwhitetext" >State&nbsp;<b>:</b> </td>
              <td align="left" class="listblacktext" ><b><c:out value=""></c:out></b></td>             
              <td align="right" class="listwhitetext" >Country&nbsp;<b>:</b></td>
              <td align="left" class="listblacktext" ><b><c:out value="${intelLead.toCountry}"></c:out></b></td>
            </tr> 
            
            <tr> 
                <td align="right" class="listwhitetext" >Comments&nbsp;<b>:</b></td>
              <td align="left" width="125px" colspan="3"> <s:textarea cssStyle="width:270px;height:70px;background:none; " cssClass="input-textUpper pr-f11" id="comments" value="${intelLead.comments}" readonly="true" name="comments" onkeydown="" onkeyup=""> 
               </s:textarea> 
               </td>               
                <c:if test="${intelLead.action eq 'Completed' && intelLead.authType eq 'NEW'}">
                 <td align="right" class="listwhitetext" >Action&nbsp;<b>:</b></td>
	            <td align="left" colspan="2" class="listwhitetext">
	   			<s:select list="{'Accept'}" name="action"  onchange="orderCreated(this,'${intelLead.id }','${intelLead.authType}');" disabled="true" cssClass="list-menu" cssStyle="width:65px;"/> 
	   			</td>
	   			</c:if>   
	   			<c:if test="${intelLead.action ne 'Completed' || intelLead.authType ne 'NEW' }">
	   			<td align="right" class="listwhitetext" >Action&nbsp;<b>:</b></td>
	            <td align="left" colspan="2" class="listwhitetext">
	    		<s:select list="{'','Accept','Hold'}" name="action"  onchange="orderCreated(this,'${intelLead.id }','${intelLead.authType}');"  cssClass="list-menu" cssStyle="width:65px;"></s:select> 
	   			</td>
	   			</c:if>
            </tr> 
            
      </table>
      
      </td>
      </tr>
     
       <tr>
        <td align="left" height="8px"></td>
        </tr>
      </table>     
      <table class="detailTabLabel" border="0" style="width: 850px;margin:0px;">
                            <tbody>
                                <tr>
                                    <td align="left" class="listwhitetext" width="30px"></td>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="listwhitetext" width="30px"></td>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="listwhitetext" style="width: 50px"><fmt:formatDate
                                            var="cartonCreatedOnFormattedValue"
                                            value="${intelLead.createdOn}"
                                            pattern="${displayDateTimeEditFormat}" /></td>
                                    <td align="right" class="listwhitetext" style="width: 50px"><b><fmt:message
                                                key='carton.createdOn' /></td>
                                    <s:hidden name="intelLead.createdOn"
                                        value="${cartonCreatedOnFormattedValue}" />
                                    <td style="width: 190px"><fmt:formatDate
                                            value="${intelLead.createdOn}"
                                            pattern="${displayDateTimeFormat}" /></td>


                                    <td align="right" class="listwhitetext" style="width: 50px"><b><fmt:message
                                                key='customerFile.createdBy' /></b></td>
                                    <c:if test="${not empty intelLead.id}">
                                        <s:hidden name="intelLead.createdBy" />
                                        <td style="width: 85px"><s:label name="createdBy"
                                                value="%{intelLead.createdBy}" /></td>
                                    </c:if>
                                    <c:if test="${empty intelLead.id}">
                                        <s:hidden name="intelLead.createdBy"
                                            value="${pageContext.request.remoteUser}" />
                                        <td style="width: 85px"><s:label name="createdBy"
                                                value="${pageContext.request.remoteUser}" /></td>
                                    </c:if>
                                    
                                    <!-- <td align="right" class="listwhitetext" style="width: 20px"> --><fmt:formatDate
                                            var="cartonCreatedOnFormattedValue"
                                            value="${intelLead.updatedOn}"
                                            pattern="${displayDateTimeEditFormat}" /><!-- </td> -->
                                    <td align="right" class="listwhitetext" style="width: 110px"><b><fmt:message
                                                key='carton.updatedOn' /></td>
                                    <s:hidden name="intelLead.updatedOn"
                                        value="${cartonCreatedOnFormattedValue}" />
                                    <td style="width: 200px"><fmt:formatDate
                                            value="${intelLead.updatedOn}"
                                            pattern="${displayDateTimeFormat}" /></td>

                                    <td align="right" class="listwhitetext" style="width: 75px"><b><fmt:message
                                                key='customerFile.updatedBy' /></b></td>
                                    <c:if test="${not empty intelLead.id}">
                                        <s:hidden name="intelLead.updatedBy" />
                                        <td style="width: 85px"><s:label name="updatedBy"
                                                value="%{intelLead.updatedBy}" /></td>
                                    </c:if>
                                    <c:if test="${empty intelLead.id}">
                                        <s:hidden name="intelLead.updatedBy"
                                            value="${pageContext.request.remoteUser}" />
                                        <td style="width: 85px"><s:label name="updatedBy"
                                                value="${pageContext.request.remoteUser}" /></td>
                                    </c:if>
                                </tr>
                                <tr>
                                    <td align="left" height="5px"></td>
                                </tr>
                            </tbody>
                        </table>
      
      </div>
	<div class="bottom-header" style="margin-top:40px;"><span></span></div>
	</div>
	</div>
</s:form>
    