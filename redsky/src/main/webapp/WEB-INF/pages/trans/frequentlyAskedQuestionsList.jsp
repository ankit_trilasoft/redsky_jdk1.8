<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>FAQ List</title>   
    <meta name="heading" content="FAQ List"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-16px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;

}
</style> 
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript"> 
animatedcollapse.addDiv('faq', 'fade=1,hide=0')
animatedcollapse.init()
</script>
<script type="text/javascript">
function disabledAll(){
	var elementsLen=document.forms['frequentlyAskedQuestionsListPage'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['frequentlyAskedQuestionsListPage'].elements[i].type=='text'){
			document.forms['frequentlyAskedQuestionsListPage'].elements[i].readOnly =true;
			document.forms['frequentlyAskedQuestionsListPage'].elements[i].className = 'input-textUpper';
		}else{
			document.forms['frequentlyAskedQuestionsListPage'].elements[i].disabled=true;
		} 
	}
}

</script>
<script type="text/javascript"> 

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this FAQ?");
	var id = targetElement;
	var lName="${lastName}";
	if (agree){
		location.href="deleteFrequentlyAskedQuestions.html?partnerCode=${partnerCode}&partnerType=AC&partnerId=${partnerId}&lastName="+encodeURIComponent(lName)+"&status=${status}&id="+id;		
	}else{
		return false;
	}
}
function addForm(){
	var lName="${lastName}";
		location.href="editFrequentlyAskedQuestions.html?partnerCode=${partnerCode}&partnerType=AC&partnerId=${partnerId}&lastName="+encodeURIComponent(lName)+"&status=${status}";		
}
function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");	
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=FAQ&excludeFPU=TRUE&partnerCode=${partnerCode}";
		  http10.open("GET", url, true); 
		  http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=FAQ&excludeFPU=FALSE&partnerCode=${partnerCode}";
		  http10.open("GET", url, true); 
	      http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }
}
function handleHttpResponse(){
    if (http10.readyState == 4)
    {
    			  var results = http10.responseText
	               results = results.trim();					               
				if(results=="")	{
				
				}
				else{
				
				} 
    } 
}
function updateChildFromParent()
{
		  var url="updateChildFromParent.html?ajax=1&decorator=simple&popup=true&partnerCode=${partnerCode}&pageListType=FAQ";
		  http11.open("GET", url, true); 
		  http11.onreadystatechange = handleHttpResponse1; 
	      http11.send(null); 
}
function handleHttpResponse1(){
    if (http11.readyState == 4)
    {
    			  var results = http11.responseText
	               results = results.trim();					               
				if(results!="")	{
				alert(results);				
				} 
    } 
}
var http11 = getHTTPObject();
var http10 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>
</head>
<s:form id="frequentlyAskedQuestionsListPage" action="" method="post" validate="true">


	<s:hidden name="partnerType" value="AC"/>
	<s:hidden name="partnerCode" value="${partnerCode}"/>
	<s:hidden name="partnerId" value="${partnerId}"/>
	<c:set var="partnerCode" value="${partnerCode}"/>
	<c:set var="partnerId" value="${partnerId}"/>
	<c:set var="lastName" value="${lastName}" />
<c:url value="editFrequentlyAskedQuestions.html" var="url">
<c:param name="partnerCode" value="${partnerCode}"/>
<c:param name="partnerType" value="AC"/>
<c:param name="partnerId" value="${partnerId}"/>
<c:param name="lastName" value="${lastName}"/>
<c:param name="status" value="${status}"/>
</c:url>
	
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		<ul>
		<c:if test="${usertype!='ACCOUNT'}">		
		<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
		<li><a href="editNewAccountProfile.html?id=${partnerId}&partnerType=AC&partnerCode=${partnerCode}"><span>Account Profile</span></a></li>
		 <c:if test="${sessionCorpID!='TSFT' }">
		<li><a href="editPartnerPrivate.html?partnerId=${partnerId}&partnerCode=${partnerCode}&partnerType=AC"><span>Additional Info</span></a></li>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
		<c:if test="${partnerType == 'AC'}">
		<li><a href="editProfileInfo.html?partnerId=${partnerId}&partnerCode=${partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
		</c:if>
		</configByCorp:fieldVisibility>
		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerCode}&partnerType=AC"><span>Acct Ref</span></a></li>
		<configByCorp:fieldVisibility componentId="component.standard.accountContactTab"><li><a href="accountContactList.html?id=${partnerId}&partnerType=AC"><span>Account Contact</span></a></li></configByCorp:fieldVisibility>
		<c:if test="${checkTransfereeInfopackage==true}">
			<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=AC"><span>Policy</span></a></li>
		</c:if>
		<%-- Added Portal Users Tab By kunal for ticket number: 6176 --%>
		<c:if test="${partner.partnerPortalActive == true}">
	  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
			<c:if test="${partnerType == 'AG'}">
						<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
					</c:if>
					<c:if test="${partnerType == 'AC'}">
					<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">	<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
					</c:if>
			</sec-auth:authComponent>
		</c:if>
		<%-- Modification closed here for ticket number: 6176 --%>
		<li><a href="partnerPublics.html?partnerType=AC"><span>Partner List</span></a></li>
		<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=AC"><span>Services</span></a></li>
		<li id="newmnav1" style="background:#FFF "><a class="current" style="margin:0px;"><span>FAQ</span></a></li>
		<c:if test="${partner.partnerPortalActive == true}">
		<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
		</c:if>
		</c:if>					
		<c:if test="${usertype=='ACCOUNT'}">
		<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
		<c:if test="${checkTransfereeInfopackage==true}">
		<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=AC"><span>Policy</span></a></li>
		</c:if>
		<li id="newmnav1" style="background:#FFF "><a class="current" style="margin:0px;"><span>FAQ</span></a></li>
		 <li><a href="accountPortalPartnerFiles.html?id=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}&status=${partner.status}&lastName=${partner.lastName}"><span>Partner File Cabinet</span></a></li>
		</c:if>	
		</ul>
	</div>
	<div class="spn">&nbsp;</div>
	</div>


   <div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table style="margin-bottom: 3px">
		<tr>
	<td align="right" class="listwhitetext" width="70px">Name</td>
	<td><s:textfield key="lastName" value="${lastName}" required="true" size="35" readonly="true" cssClass="input-textUpper"/></td>
		  	<td align="right" class="listwhitetext" width="40">Code</td>
	<td><s:textfield key="partnerCode" value="${partnerCode}" required="true" readonly="true" cssClass="input-textUpper"/></td>
	<td align="right" class="listwhitetext" width="50px" style="padding-right:2px">Status</td>
	<td><s:textfield key="status" value="${status}" required="true" size="35" readonly="true" cssClass="input-textUpper"/></td>
	</tr>
	</table>
	
	</div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
</div>

<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%" >
<tr>
	<td height="0" width="100%" align="left" >		
<div   style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"> FAQ
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div>
  	
  	<div id="content" align="center">
<div id="Layer1" style="width: 100%;">

<table  width="100%" cellspacing="1" cellpadding="0" border="0" class="detailTabLabel">
	<tbody>
	<tr><td height="5"></td></tr>
		<tr>
			<td>				
<display:table name="frequentlyAskedQuestionsList" defaultsort="4" class="table" id="frequentlyAskedQuestionsList" requestURI="" pagesize="25" style="width:100%;">
<display:column sortable="true" sortProperty="sequenceNumber" title="Sequence#" style="width:70px;">
<c:url value="editFrequentlyAskedQuestions.html" var="url1" >
<c:param name="partnerCode" value="${frequentlyAskedQuestionsList.partnerCode}"/>
<c:param name="partnerType" value="AC"/>
<c:param name="partnerId" value="${partnerId}"/>
<c:param name="id" value="${frequentlyAskedQuestionsList.id}"/>
<c:param name="lastName" value="${lastName}"/>
<c:param name="status" value="${status}"/>
</c:url>
 
               
               	<c:set var="partnerCode" value="${frequentlyAskedQuestionsList.partnerCode}"/>
				<c:set var="partnerId" value="${frequentlyAskedQuestionsList.partnerId}"/>
                 <%-- <c:out value="${frequentlyAskedQuestionsList.sequenceNumber}" /> --%>
                 <c:out value="${frequentlyAskedQuestionsList_rowNum}"/>
                  </display:column>
 <display:column sortable="true" style="width:350px" title="Question"  maxLength="80" >
 <a href="${url1}"><c:out value="${frequentlyAskedQuestionsList.question}"  /></a>
 </display:column> 	
	<display:column property="answer" sortable="true" title="Answer" maxLength="80"/>
	<display:column property="jobType" sortable="true" title="Job&nbsp;Type" />
	<display:column property="language" sortable="true" title="Language" style="width:70px"/>
	<c:if test="${usertype!='ACCOUNT'}">
		<c:choose>
			<c:when test="${partner.partnerType == 'DMM' or partner.partnerType == 'CMM'}">
				<configByCorp:fieldVisibility componentId="component.field.CmmDmm.visibility">
					<display:column title="Remove" style="width: 15px;" >
						<a><img align="middle" onclick="confirmSubmit(${frequentlyAskedQuestionsList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
					</display:column>
				</configByCorp:fieldVisibility>
			</c:when>
			<c:otherwise>
				<display:column title="Remove" style="width: 15px;" >
					<a><img align="middle" onclick="confirmSubmit(${frequentlyAskedQuestionsList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
				</display:column>
			</c:otherwise>
		</c:choose>
	</c:if>
</display:table>
			</td>
		</tr>
	</tbody>
</table> 
</div>
</div>

  	</div>
  	</td>
  	</tr>
  	</table>
<c:set var="buttons"> 	
<c:if test="${usertype!='ACCOUNT'}">	
    <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="addForm();" value="<fmt:message key="button.add"/>"/>
    </c:if> 
</c:set> 
<c:if test="${usertype!='ACCOUNT'}">
<table width="100%">
											<tr align="right">
											<td align="right"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="updateChildFromParent();"/></td><td></td>
											</tr>
											<tr align="right">
											<td align="right" class="listwhitetext">Exclude from Parent updates</td>
											<td class="listwhitetext" width="8px" align="right"><s:checkbox name="excludeFromParent" id="excludeFPU" value="${excludeFPU}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>
														 		  	   	 	               	
											</tr>												
											</table>
											</c:if>
<c:out value="${buttons}" escapeXml="false" /> 
  	
  	

</s:form>
<script type="text/javascript">
try{
	 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
disabledAll();
	</sec-auth:authComponent>
}
catch(e){}
</script>
<script type="text/javascript">
		  	<c:if test="${excludeFPU==true}">
							document.getElementById("excludeFPU").checked=true;
			</c:if>
		  	<c:if test="${excludeFPU==false}">
			document.getElementById("excludeFPU").checked=false;
			</c:if>


function checkCmmDmmDisable(sessionCorpID){
	if(sessionCorpID != 'TSFT'){
		if( sessionCorpID != 'UTSI'){
			if('${partner.partnerType}' == 'DMM' || '${partner.partnerType}' == 'CMM'){
				var elementsLen=document.forms['frequentlyAskedQuestionsListPage'].elements.length;
				for(i=0;i<=elementsLen-1;i++){
					if(document.forms['frequentlyAskedQuestionsListPage'].elements[i].type=='text'){
						document.forms['frequentlyAskedQuestionsListPage'].elements[i].readOnly =true;
						document.forms['frequentlyAskedQuestionsListPage'].elements[i].className = 'input-textUpper';
					}else if(document.forms['frequentlyAskedQuestionsListPage'].elements[i].type=='textarea'){
						document.forms['frequentlyAskedQuestionsListPage'].elements[i].readOnly =true;
						document.forms['frequentlyAskedQuestionsListPage'].elements[i].className = 'textareaUpper';
					}else if(document.forms['frequentlyAskedQuestionsListPage'].elements[i].type=='submit' || document.forms['frequentlyAskedQuestionsListPage'].elements[i].type=='reset' || document.forms['frequentlyAskedQuestionsListPage'].elements[i].type=='button'){
						document.forms['frequentlyAskedQuestionsListPage'].elements[i].style.display='none';
					}else{
						document.forms['frequentlyAskedQuestionsListPage'].elements[i].disabled=true;
					} 
				}
			}
		}
	}
}

<c:if test="${not empty partner.id && partnerType=='AC'}">
		checkCmmDmmDisable('${sessionCorpID}');
</c:if>

</script>