package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;   
import com.trilasoft.app.model.Person;   
  
import java.util.List;   
  
public interface PersonManager extends GenericManager<Person, Long> {   
    public List<Person> findByLastName(String lastName);   
}  
