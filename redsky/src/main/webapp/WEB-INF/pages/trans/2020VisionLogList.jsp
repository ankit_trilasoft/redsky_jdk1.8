<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>2020&nbsp;Vision&nbsp;Log&nbsp;Info&nbsp;List</title>    
    <meta name="heading" content="2020&nbsp;Vision&nbsp;Log&nbsp;Info&nbsp;"/>   
    <script language="javascript" type="text/javascript">
    function reloadList(){
    var ll=document.forms['searchForm'].elements['status'];
    for(var i=0;i<ll.options.length;i++){
    	//alert(i);
    	if(ll.options[i].value=='${status}'){
    		//alert(i);
    		ll.options[i].selected="selected";	
    	}
    }
    }
  function clear_fields(){
   document.forms['searchForm'].elements['entryNumber'].value = "";
  document.forms['searchForm'].elements['invoiceNumber'].value = "";
     document.forms['searchForm'].elements['orderNumber'].value = "";
      document.forms['searchForm'].elements['orderNumberLine'].value = ""; 
      document.forms['searchForm'].elements['status'].value = ""; 
}
  
  function goToEditAccountLinePage(sid,id){
	  var ssid=sid;
	  var iid=id;
	  window.location="editAccountLine.html?sid="+ssid+"&id="+iid+"&checkLHF=''";
	 /*  var getCheckLHF=document.forms['serviceForm1'].elements['checkLHF'].value;
	  var idForCheckLHF=document.forms['serviceForm1'].elements['idForCheckLHF'].value;
	  if(idForCheckLHF==id){
	  window.location="editAccountLine.html?sid="+ssid+"&id="+iid+"&checkLHF=";
	  }
	  else{
	  window.location="editAccountLine.html?sid="+ssid+"&id="+iid+"&checkLHF="+getCheckLHF;
	  } */
	  }
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:0px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
form {
margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
</style> 
<script>
function goToCustomerDetail(targetValue){
        /* document.forms['searchForm'].elements['id'].value = targetValue;
        document.forms['searchForm'].action = 'editRefZipGeoCodeMap.html?from=list';
        document.forms['searchForm'].submit(); */
}

</script>  
</head>
<c:set var="buttons">   
    <%-- <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="editRefZipGeoCodeMap.html"/>'" value="<fmt:message key="button.add"/>"/> --%> 
</c:set> 
<s:form id="searchForm" action="searcht20VisionLogInfo.html" method="post" validate="true" >
<div id="layer1" style="width:100%; ">

<div id="otabs" style="">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
  <div class="spnblk">&nbsp;</div>
</div>

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;"  method="searcht20VisionLogInfoList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:-20px;!margin-top:-2px; "><span></span></div>
    <div class="center-content">
		<table class="table" style="width:100%">
		<thead>
		<tr>
		<th>Entry Number</th>
		<th>Invoice Number</th>
		<th>Order Number</th>
		<th>Account Line Number</th>
		<th>Status</th>
		<th></th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="entryNumber" required="true" cssClass="input-text" size="24" value="${entryNumber}"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="invoiceNumber" required="true" cssClass="input-text" size="24" value="${invoiceNumber}"/>
					</td>
					
					<td width="" align="left">
					    <s:textfield name="orderNumber" required="true" cssClass="input-text" size="24" value="${orderNumber}"/>
					</td>
					
					<td width="" align="left">
					    <s:textfield name="orderNumberLine" required="true" cssClass="input-text" size="29" value="${orderNumberLine}"/>
					</td>
					
					<td width="" align="left">
					   <%--  <s:textfield name="status" required="true" cssClass="input-text" size="30"/> --%>
					    <s:select name="status" required="true" cssClass="list-menu" list="{'','Success','Failure'}" cssStyle="width:80px" value="${status}"></s:select>
					</td>
					
					<td width="" align="right">
					    <c:out value="${searchbuttons}" escapeXml="false" />
					</tr>
				</tbody>
			</table>
			
				</div>
<div class="bottom-header" style="!margin-top:45px;" ><span></span></div>
</div>
</div> 
<s:set name="refZipGeoCodeMaps" value="refZipGeoCodeMaps" scope="request"/>  
<div id="otabs" style="margin-top: -15px;">
  <ul>
    <li><a class="current"><span>2020&nbsp;Vision&nbsp;Log&nbsp;Info&nbsp;List</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<display:table name="t20VisionLogList" class="table" requestURI="" id="t20VisionLogIdList" pagesize="20" style="width:99%; margin-left:5px;margin-top: 0px;!margin-top:-5px;">
    <display:column  sortable="true" property="entryNumber" title="Entry Number"/>      
    <%-- <a onclick="goToCustomerDetail(${refZipGeoCodeMapList.id});" style="cursor:pointer">
    <c:out value="${refZipGeoCodeMapList.zipcode}" /></a></display:column> --%>
    <display:column   property="invoiceNumber" sortable="true" title="Invoice Number" />    
    <display:column  sortable="true" title="Order Number">
    <a href="editServiceOrderUpdate.html?id=${t20VisionLogIdList.serviceOrderId}" style="cursor:pointer">
    <c:out value="${t20VisionLogIdList.orderNumber}" /></a>
    </display:column>
    <display:column  sortable="true" title="Account Line Number">
    <a href="editAccountLine.html?sid=${t20VisionLogIdList.serviceOrderId}&id=${t20VisionLogIdList.orderNumberLine}&checkLHF=${t20VisionLogIdList.checkLHF}"  style="cursor:pointer">
    <c:out value="${t20VisionLogIdList.orderNumberLine}" />
    </a>
    </display:column>
    <display:column property="status" sortable="true" title="Status"/>  
    <display:column property="error" sortable="true" title="Processed Through"/> 
    <display:column property="notProcess" sortable="true" title="Reason"/> 
     <display:column property="createdOn" sortable="true" title="Processed On"/>  
</display:table> 
</div>
<c:out value="${buttons}" escapeXml="false" />  
<%-- <s:hidden name="id"/> --%>
</s:form>
<script> 
reloadList();
		/* <c:if test="${hitFlag == 1}" >
			<c:redirect url="/refZipGeoCodeMaps.html" ></c:redirect>
		</c:if>
		<c:if test="${detailPage == true}" >
			   <c:redirect url="/editRefZipGeoCodeMap.html?id=${refZipGeoCodeMapList.id}"/>
	   </c:if> */
</script>