package com.trilasoft.app.service;


import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.SurveyResponse;

public interface SurveyResponseManager extends GenericManager<SurveyResponse, Long>{

}
