<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head>
<meta name="heading" content="<fmt:message key='timeSheet.heading'/>"/> 
<title><fmt:message key="timeSheet.title"/></title> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="JavaScript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>

<script language="javascript" type="text/javascript"> 
function autoChange_fromCalcCode(targetElement){
	<c:if test="${!disablePayroll}">
	if(document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value == '' || document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value =='H'){
		document.forms['workTicketCrewsList'].elements['payRollProcess'].disabled = true;
	}else{
		document.forms['workTicketCrewsList'].elements['payRollProcess'].disabled = false;
	}
	</c:if>
	if(document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value == 'H' ){
		document.forms['workTicketCrewsList'].elements['SaveHourly'].disabled = false;
	}else{
		document.forms['workTicketCrewsList'].elements['SaveHourly'].disabled = true;
	}
}
function autoChange_calcCode(targetElement){
	document.forms['workTicketCrewsList'].elements['calcCode'].value = targetElement.value;
}

function editTimeSheets(targetElement){
	var id = targetElement;
	var whid=document.forms['workTicketCrewsList'].elements['workTicket.id'].value;
	document.forms['workTicketCrewsList'].action = 'editTimeSheet.html?from=list&id='+id+'&whoticket='+'ticketCrews'+'&whid='+whid;
	document.forms['workTicketCrewsList'].submit();
}
 </script>
 <SCRIPT LANGUAGE="JavaScript">


function processPayroll(){
	var actWeight = document.forms['workTicketCrewsList'].elements['workTicket.actualWeight'].value;
	var calcCode = document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value;
	var rev = document.forms['workTicketCrewsList'].elements['workTicket.revenue'].value;
	var wrkDate = document.forms['workTicketCrewsList'].elements['workTicket.date1'].value;
	var id = document.forms['workTicketCrewsList'].elements['workTicket.id'].value;
	var url="processPayrolls.html?ajax=1&decorator=simple&popup=true&calcCode=" + encodeURI(calcCode) +"&actWeight=" +encodeURI(actWeight) +"&rev=" +encodeURI(rev)+"&wrkDate=" +encodeURI(wrkDate)+"&id=" +encodeURI(id);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse2;
    http2.send(null);
    
}

function processPayrollRevised(){
	var ticket = document.forms['workTicketCrewsList'].elements['workTicket.ticket'].value;
	var id = document.forms['workTicketCrewsList'].elements['workTicket.id'].value;
	var url="processPayrollsRevised.html?ajax=1&decorator=simple&popup=true&ticket=" + encodeURI(ticket)+"&id="+id;
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponseChkRevenue;
    http2.send(null);
    
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

 function handleHttpResponse2(){
     if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        document.forms['workTicketCrewsList'].elements['totalRevenue'].value = results;
     }
 }
        
 function handleHttpResponseChkRevenue(){
           if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                if(results == 'false'){
                	var agree = confirm("Clicking OK will recalculate the payroll and overwrite the data");
                	if(agree){
                	    document.forms['workTicketCrewsList'].elements['hitFlag'].value='';
                		document.forms['workTicketCrewsList'].elements['hitFlag'].value='payRoll';
                		document.forms['workTicketCrewsList'].submit();
                	}
                }else{
                	document.forms['workTicketCrewsList'].elements['hitFlag'].value='payRoll';
                	document.forms['workTicketCrewsList'].submit();
                }
                
             }
        }
  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http2222=getHTTPObject();
    var httpVoxme = getHTTPObject();
    var httpMoveCloud = getHTTPObject();
    var httpMailcheck = getHTTPObject();
    
function processSaveHourly(){
	document.forms['workTicketCrewsList'].elements['hitFlag'].value='';
	document.forms['workTicketCrewsList'].elements['hitFlag'].value='Hourly';
	var id = document.forms['workTicketCrewsList'].elements['workTicket.id'].value;
	document.forms['workTicketCrewsList'].action="workTicketCrews.html?id="+id;
	document.forms['workTicketCrewsList'].submit();

}
function TransferDetails(){
var url="createXMLForAccessInfo.html?ajax=1&decorator=simple&popup=true&cid=${serviceOrder.customerFileId}";
    http2222.open("GET", url, true);
    showOrHide(1);
    http2222.onreadystatechange = handleHttpResponseForXML;
    http2222.send(null);
}  
function handleHttpResponseForXML()
        {
             if (http2222.readyState == 4)
             {
                var results = http2222.responseText
                results = results.trim(); 
                if(results=='Y'){
                alert("Mail has been sent");
                }
                if(results=='N'){
                 alert("No Data In Access Info");
                }
                showOrHide(0);
             }
        }
function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} }  
   	
 function sendMailCrewFromVoxme(){
 var tempCrewDevice = document.forms['workTicketCrewsList'].elements['crewDevice'].value
 var crewMailId = document.forms['workTicketCrewsList'].elements['crewMailId'].value 
 if(tempCrewDevice==''){
	 alert('Please Select the Crew-Device, where the information needs to be sent.');
	 return false;
 }else{
	 if(crewMailId==''){
		 alert('Crew-Device does not have an email id associated with it.');
		 return false;
	 }else{
	 var url="createXMLForCrew.html?decorator=simple&popup=true&cid=${serviceOrder.customerFileId}&crewMailID="+crewMailId+"&sid=${serviceOrder.id}&wrkTckId=${workTicket.id}";
	    httpVoxme.open("GET", url, true);
	    showOrHide(1);
	    httpVoxme.onreadystatechange = handleHttpResponseForVoxmeXML;
	    httpVoxme.send(null);
	 }
 } 
}
function handleHttpResponseForVoxmeXML()
 {
       if (httpVoxme.readyState == 4)
        { 
           var crewMailId = document.forms['workTicketCrewsList'].elements['crewMailId'].value 
             var results = httpVoxme.responseText
             results = results.trim(); 
           //  if(results=='Y'){
            	 window.open('sendCrewMail.html?results='+results+'&mailId='+crewMailId+'&decorator=popup&popup=true','','width=550,height=170,top=200,left=200') ;
             //alert("Mail has been sent");
             //}
            // if(results=='N'){
            //   alert("Mail not sent");
           //   }
           showOrHide(0);
        }
 }
 
function sendDataMoveCloud(){
		 var url="sendMoveToCloudCrew.html?decorator=simple&popup=true&cid=${serviceOrder.customerFileId}&wrkTckId=${workTicket.id}";
		    httpMoveCloud.open("GET", url, true);
		    showOrHide(1);
		    httpMoveCloud.onreadystatechange = handleHttpResponseForMoveCloud;
		    httpMoveCloud.send(null);
	}
	function handleHttpResponseForMoveCloud()
	 {
	       if (httpMoveCloud.readyState == 4)
	        { 
	             var results = httpMoveCloud.responseText
	             results = results.trim();
	           if(results=='Y'){
               alert("Request is scheduled for MoveCloud.");
              
               var id = document.forms['workTicketCrewsList'].elements['workTicket.id'].value;
           	   document.forms['workTicketCrewsList'].action="workTicketCrews.html?id="+id;
           	   document.forms['workTicketCrewsList'].submit();
               
	           }
	           showOrHide(0);
	        }
	 }

function checkMailId(){
	var tempCrewDevice = document.forms['workTicketCrewsList'].elements['crewDevice'].value
	var url="checkCrewMailID.html?decorator=simple&popup=true&code="+tempCrewDevice;
	httpMailcheck.open("GET", url, true);
	httpMailcheck.onreadystatechange = handleHttpResponseForMailId;
	httpMailcheck.send(null);
}

function handleHttpResponseForMailId()
{
      if (httpMailcheck.readyState == 4)
       {
            var results = httpMailcheck.responseText
            results = results.trim(); 
            document.forms['workTicketCrewsList'].elements['crewMailId'].value = results;
       }
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}

</style>
</head>

<s:form name="workTicketCrewsList" action="workTicketCrews.html?id=${workTicket.id}" method="post">
<s:hidden name="crewMailId" />
<s:hidden name="editable" value="${editable}"/>
<s:hidden name="rev" value="${workTicket.revenue}"/>
<s:hidden name="tkt" value="${workTicket.ticket}"/>
<s:hidden name="calcCode" value="${payrollAllocation.calculation}"/>
<s:hidden name="actWeight" value="${workTicket.actualWeight}"/>
<c:set var="disablePayroll" value="${disablePayroll}" />
<s:hidden name="workTicket.id" />
<s:hidden name="hitFlag" value="${hitFlag}" />
<c:set var="sid" value="${serviceOrder.id}" />
<s:hidden name="sid" value="${serviceOrder.id}" />
<s:hidden name="workTicket.revenueCalculation" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="workTicket.date1" /></s:text>
<td align="left"><s:hidden cssClass="input-text" id="wrkDate" name="wrkDate" value="%{customerFileSurveyFormattedValue}" />
<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="workTicket.date1" /></s:text>
<td align="left"><s:hidden id="date1" name="workTicket.date1" value="%{customerFileSurveyFormattedValue}" />
<c:set var="totalRevenue" value="${totalRevenue}" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<div id="newmnav">
		  
		  
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			   <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
			  <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
			  <c:if test="${forwardingTabVal!='Y'}"> 
	   			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	 			</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                 <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
              </sec-auth:authComponent> 
			<sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			  <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if> 
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">			
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  </sec-auth:authComponent>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			 <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>
	
	
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;"><%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table>

<div id="Layer6" style="width:100%">
<div id="newmnav">
		    <ul>
			  <li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
			  <sec-auth:authComponent componentId="module.tab.workTicket.materialEquipmentTab">
			  			<li><a href="itemsJbkEquipExtraInfo.html?wid=${workTicket.id}&sid=${serviceOrder.id}&itemType=E"><span>Equipment&nbsp;And&nbsp;Material</span></a></li>
					</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.materialTab">
			  <li><a href="itemsJbkEquips.html?id=${workTicket.id}&itemType=M&sid=${sid}"><span>Material</span></a></li>
			  </sec-auth:authComponent>
			  <!--<li><a href="itemsJbkResourceList.html?id=${workTicket.id}&sid=${sid}"><span>Resource</span></a></li>
			  -->
			  <sec-auth:authComponent componentId="module.tab.workTicket.equipmentTab">
			  <li><a href="itemsJbkResourceList.html?id=${workTicket.id}&itemType=E&sid=${sid}"><span>Resource</span></a></li>
			  </sec-auth:authComponent>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Crew<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		      <li><a href="truckingOperationsList.html?ticket=${workTicket.ticket}&sid=${serviceOrder.id}&tid=${workTicket.id}"><span>Truck</span></a></li>
		      <configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
		      <li><a href="bookStorages.html?id=${workTicket.id}"><span>Whse/Mgmt</span></a></li>
		      </configByCorp:fieldVisibility>
		    <sec-auth:authComponent componentId="module.tab.workTicket.formsTab">
			<li><a onclick="window.open('subModuleReports.html?id=${workTicket.id}&jobNumber=${serviceOrder.shipNumber}&noteID=${workTicket.ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			</sec-auth:authComponent>
			</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
		<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" style="width:85% !width:65%; margin-bottom:8px;">
	<tbody>
		<tr>
			<td align="right" class="listwhitetext">Revenue&nbsp;Calculation&nbsp;Code&nbsp;</td>
			<td class="listwhitetext" colspan="3"><s:select cssClass="list-menu" cssStyle="min-width:50px;" name="payrollAllocation.calculation"  list="%{revcalc}" headerKey=""  headerValue=""  onchange="autoChange_fromCalcCode(this);autoChange_calcCode(this);"/></td>
			<td align="right" class="listwhitetext">Ticket&nbsp;#&nbsp;</td>
			<td class="listwhitetext"><s:textfield cssClass="input-textUpper" size="18" name="workTicket.ticket" readonly="true"/></td>
			 <td width="100px"></td>
		</tr>
		<tr>
			<td align="right" class="listwhitetext" >Total&nbsp;Rev&nbsp;Earned&nbsp;By&nbsp;Crew&nbsp;</td>
			<c:if test="${empty workTicket.revenue}">
				<td class="listwhitetext" ><s:textfield cssClass="input-text"  size="15" name="totalRevenue" value="%{totalRevenue}"/></td>
			</c:if>
			<c:if test="${not empty workTicket.revenue}">
				<td class="listwhitetext"><s:textfield cssClass="input-text"  size="15" name="totalRevenue" value="${workTicket.crewRevenue}"/></td>
			</c:if>
			<td align="right" class="listwhitetext">Ticket&nbsp;Revenue</td>
									<td><s:textfield name="workTicket.revenue" size="17"
										cssClass="input-textUpper" readonly="true" /></td>
			<td align="right" class="listwhitetext" >Extra&nbsp;Payroll</td>
									<td><s:textfield name="workTicket.extraPayroll" size="18"
										cssClass="input-textUpper" readonly="true" /></td>
			<td align="right" class="listwhitetext">Actual&nbsp;Weight&nbsp;</td>
			<td class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" size="12" name="workTicket.actualWeight" readonly="true"/></td>
		    <td width="100px"></td>
		</tr>
		<tr>
		<td></td>
		<c:if test="${disablePayroll}">
			<td colspan="2" align="left"><input type="button" class="cssbutton1" name="payRollProcess" value="Process Payroll" style="width:125px;" onclick="return processPayrollRevised()" disabled="disabled" /></td>
		</c:if>
		<c:if test="${!disablePayroll}">
			<td colspan="2" align="left"><input type="button" class="cssbutton1" name="payRollProcess" value="Process Payroll" style="width:125px;" onclick="return processPayrollRevised()"/></td>
		</c:if>
			<td align="left" colspan="2"><input type="button" class="cssbutton1" name="SaveHourly" value="Save Hourly" style="width:125px;" onclick="processSaveHourly()"/></td>
		</tr>
		<tr height="15px"></tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<table style="width:100%;">
<tbody>
<tr>
<td align="left">
	<div id="otabs" style="width:85%;">
		  <ul>
		    <li><a class="current"><span>Days Distribution For Ticket</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<display:table name="dayDistributionHoursDTOs" class="table" requestURI="workTicketCrews.html" id="dayDistributionHoursList" defaultsort="1" style="width:100%;margin-top:-18px;!margin-top:0px;margin-bottom:0px;">
		<display:column property="workDate"  sortable="true" title="Date" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
		<display:column sortable="true" headerClass="containeralign" title="Hours" style="text-align:right"><fmt:formatNumber type="number"  
            value="${dayDistributionHoursList.totalHours}"  maxFractionDigits="2" groupingUsed="true"/></display:column>
       	<c:if test="${dayDistributionHoursList.percentHours!='NaN'}">
       	<display:column sortable="true" property="percentHours"  title="%Hours"  style="text-align:right">       	
        </display:column>
        </c:if>
        <c:if test="${dayDistributionHoursList.percentHours=='NaN'}">
       	<display:column sortable="true" value="0"  title="%Hours"  style="text-align:right">       	
        </display:column>
        </c:if>
        <c:if test="${dayDistributionHoursList.ticketRevenueShare!='NaN'}">
        <display:column sortable="true" property="ticketRevenueShare"  title="Tkt Share" style="text-align:right"></display:column>
        </c:if>
        <c:if test="${dayDistributionHoursList.ticketRevenueShare=='NaN'}">
        <display:column sortable="true" title="Tkt Share" style="text-align:right"></display:column>
        </c:if>       
        <c:if test="${dayDistributionHoursList.weightShare!='NaN'}">   
        <display:column sortable="true" property="weightShare"  title="Weight" style="text-align:right">
        </display:column>
        </c:if>
         <c:if test="${dayDistributionHoursList.weightShare=='NaN'}"> 
         <display:column sortable="true" value="0"  title="Weight" style="text-align:right"></display:column>
         </c:if>
        <display:column sortable="true" title="Share" style="text-align:right">
        <fmt:formatNumber type="number" value="${dayDistributionHoursList.revenueShare}"  maxFractionDigits="4" groupingUsed="true"/>
         </display:column>
        <c:if test="${dayDistributionHoursList.perHour!='NaN'}"> 
         <display:column sortable="true" title="Per&nbsp;Hour"style="text-align:right">
         <fmt:formatNumber type="number" value="${dayDistributionHoursList.perHour}"  maxFractionDigits="4" groupingUsed="true"/>
         </display:column>
         </c:if>
         <c:if test="${dayDistributionHoursList.perHour=='NaN'}"> 
         <display:column sortable="true" value="0"  title="Per&nbsp;Hour"style="text-align:right">
         </display:column>
         </c:if>
       	<display:column sortable="true" headerClass="containeralign" title="OT&nbsp;Hour"style="text-align:right"><fmt:formatNumber type="number"  
       value="${dayDistributionHoursList.otHours}"  maxFractionDigits="2" groupingUsed="true"/></display:column>
        <display:column sortable="true" headerClass="containeralign" title="Total&nbsp;Hours"style="text-align:right"><fmt:formatNumber type="number"  
       value="${dayDistributionHoursList.totalHours + dayDistributionHoursList.otHours}"  maxFractionDigits="2" groupingUsed="true"/></display:column>
	</display:table>
</td>
</tr>

</tbody>
</table>
<table style="width:100%;">
<tbody>
<tr>
<td align="left">
	<div id="otabs" style="width:85%;">
		  <ul>
		    <li><a class="current"><span>Crew List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<display:table name="timeSheets" class="table" id="timeSheetList" requestURI="workTicketCrews.html" defaultsort="1" style="width:100%;margin-top:-18px;!margin-top:0px;">
		<display:column property="workDate" sortable="true" titleKey="timeSheet.workDate" style="width:100px;" format="{0,date,dd-MMM-yyyy}"/>
		
		<display:column sortable="true" style="text-align:left" titleKey="timeSheet.crewName">
		<c:if test="${editable=='Yes'}">
		<a style="cursor:pointer" onclick="editTimeSheets(${timeSheetList.id})">
		</c:if>
		<c:out value="${timeSheetList.crewName}" /></a></display:column>
		
		<display:column property="crewType" sortable="true" title="Type" />
		<display:column property="action" sortable="true" titleKey="timeSheet.action" />
		<display:column property="calculationCode" sortable="true" title="Calc Code" />
		
		<display:column property="beginHours" headerClass="containeralign" sortable="true" titleKey="timeSheet.beginHours" style="text-align:right"/>
		<display:column property="endHours" headerClass="containeralign" sortable="true" titleKey="timeSheet.endHours" style="text-align:right"/>
		<display:column title="Reg&nbsp;Hrs" headerClass="containeralign"><div style="text-align:right">
		<fmt:formatNumber type="number"  value="${timeSheetList.regularHours}"  maxFractionDigits="2" groupingUsed="true"/></div></display:column>
        
        <display:column titleKey="timeSheet.overTime" headerClass="containeralign"><div style="text-align:right">
        <fmt:formatNumber type="number"  value="${timeSheetList.overTime}"  maxFractionDigits="2" groupingUsed="true"/>
        </div></display:column>
		
		<display:column sortable="true" titleKey="timeSheet.paidRevenueShare" headerClass="containeralign" ><div style="text-align:right"><fmt:formatNumber type="number"
			value="${timeSheetList.paidRevenueShare}" maxFractionDigits="2" groupingUsed="true"/></div></display:column>
			<c:if test="${totalRevenue == '0.0'}">
			<display:column title="Share&nbsp;%" headerClass="containeralign"></display:column>
			</c:if>
        <c:if test="${totalRevenue != '0.0'}">
        <display:column title="Share&nbsp;%" headerClass="containeralign"><div style="text-align:right"><fmt:formatNumber type="number"  
            value="${timeSheetList.paidRevenueShare*100 / totalRevenue}"  maxFractionDigits="2" groupingUsed="true"/></div></display:column> 
        </c:if>
    </display:table>
</td>
</tr>

</tbody>
</table>

<table>
<tr>
<!--<td><input type="button"  class="cssbutton" style="width:150px; height:25px;margin-left:10px;"  value="Transfer Details" onclick="TransferDetails();"  /></td>-->
							<c:if test="${surveyTool=='Voxme' }">	
							<c:if test="${count > 0}">
						<!-- <td align="left" width="15px"></td> -->
						<td align="left" class="listwhitetext" style="width:88px;"><span>Email Status</span> <img class="openpopup" id="openpopup.img1" style="vertical-align:sub;"  src="<c:url value='/images/email_small.gif'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='SaveForEmail'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'SaveForEmail')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetupValue,'SaveForEmail','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							<td align="right" class="listwhitetext">Crew Device</td>
                           <td><s:select cssClass="list-menu" name="crewDevice"  list="%{crewDeviceList}"  cssStyle="width:100px;"  onchange="checkMailId()" /></td>
                            <td><input type="button" value="Send To Voxme" class="cssbuttonB" onclick="sendMailCrewFromVoxme()" /></td>
							</c:if>
							<c:if test="${surveyTool=='MoveCloud' && checkDriver =='DR' && (workTicket.service=='DL' || workTicket.service=='PK')}">	
							<c:if test="${moveCount > 0}">						
							<td align="left" class="listwhitetext" width="50"><img class="openpopup" id="openpopup.img1" style="vertical-align:text-bottom;margin-left:17px; margin-top:1px;" title="Move Cloud" src="<c:url value='/images/move4u.png'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='Ready For Sending'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'Ready For Sending')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" title="${fn:replace(emailSetupValue,'Ready For Sending','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							<td><input type="button" value="Send To MoveCloud" class="cssbuttonB" onclick="sendDataMoveCloud()" /></td>
							</c:if>

</tr>
</table>
<s:hidden name="emailSetupValue" />
<s:hidden name="moveCount" />
</s:form>
<script language="javascript" type="text/javascript"> 
try{
	/*if(document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value == '' )
		{
		document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value='H';
	}*/
	
<c:if test="${!disablePayroll}">
	if(document.forms['workTicketCrewsList'].elements['payrollAllocation.calculation'].value == '' ){
		document.forms['workTicketCrewsList'].elements['payRollProcess'].disabled = true;
	}else{
		document.forms['workTicketCrewsList'].elements['payRollProcess'].disabled = false;
	}
</c:if>
}
catch(e){}
try{
document.forms['workTicketCrewsList'].elements['SaveHourly'].disabled = true;
}
catch(e){}
try{
document.forms['workTicketCrewsList'].elements['totalRevenue'].value = Math.round(document.forms['workTicketCrewsList'].elements['totalRevenue'].value*100)/100;
}
catch(e){}
try{
<c:if test="${editable=='No'}">
	for(i=0;i<=20;i++)
			{
					document.forms['workTicketCrewsList'].elements[i].disabled = true;
			}
</c:if>
}
catch(e){}

 </script>