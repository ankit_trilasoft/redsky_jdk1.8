package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.SurveyResponseDtl;

public interface SurveyResponseDtlManager extends GenericManager<SurveyResponseDtl, Long>{

}
