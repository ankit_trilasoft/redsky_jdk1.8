<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="QuotationVsGrossMargin" pageWidth="927" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="867" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="a993de97-1928-42b0-8f94-ccc6f1fa1f17">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Begin_Date" class="java.util.Date"/>
	<parameter name="End_Date" class="java.util.Date"/>
	<queryString>
		<![CDATA[SELECT Distinct "QuotationVsGrossMargin-R-Extract-VORU.jrxml",
	customerfile.sequenceNumber AS 'Quotation',
	serviceorder.shipNumber AS 'Quote File',
	customerfile.comptetive AS 'Compete',
	customerfile.billtoname AS 'Bill to Name',
	trim(customerfile.originCompany) AS 'Employer Name',
	Concat(if(customerfile.lastName is null || trim(customerfile.lastName)='',"",trim(customerfile.lastName)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='',"",if((customerfile.lastName is null || trim(customerfile.lastName)=''),trim(customerfile.firstName),concat(" ",trim(customerfile.firstName))))) AS 'Client Name',
	DATE_FORMAT(customerfile.survey,'%d %b %Y') AS 'Survey Date',
	DATE_FORMAT(customerfile.moveDate,'%d %b %Y') AS 'Move Date',
	serviceorder.job AS 'Job',
	concat(if(serviceorder.originCity is not null,concat(serviceorder.`originCity`,","),"")," ",if(!(trim(refmaster_so.`description`) is null || trim(refmaster_so.`description`)=""),concat(trim(refmaster_so.`description`),","),"")," ",if(serviceorder.`originCountry` is not null,serviceorder.`originCountry`,"")) AS 'From',
	concat(if(serviceorder.`destinationCity` is not null,concat(serviceorder.`destinationCity`,","),"")," ",if(!(trim(refmaster_sd.`description`) is null || trim(refmaster_sd.`description`)=""),concat(trim(refmaster_sd.`description`),","),"")," ",if(serviceorder.`destinationCountry` is not null,serviceorder.`destinationCountry`,"")) AS 'To',
	miscellaneous.netEstimateCubicMtr AS 'Volume',
	serviceorder.estimatedGrossMargin AS 'Gross Margin',
	serviceorder.estimatedGrossMarginPercentage AS 'Gross Margin %',
	DATE_FORMAT(now(),'%d %b %Y') 'Dates',
	if(company.`corpID`='VOER','A',(if(company.`corpID`='VORU','B',(if(company.`corpID`='VOCZ','C',""))))) 'CorpID',
	customerfile.`lastName`  'Last Name',
	customerfile.`firstName`  'First Name',
	serviceorder.`originCity`  'Origin City',
	trim(refmaster_so.`description`)  'Origin State',
	serviceorder.`originCountry`  'Origin Country', 
	serviceorder.`destinationCity`  'Destination City',
	trim(refmaster_sd.`description`)  'Destination State',
	serviceorder.`destinationCountry`  'Destination Country',
	if(serviceorder.`estimatedGrossMarginPercentage`>=25,serviceorder.`shipNumber`,"") 'ShipNumber',
	if(serviceorder.`estimatedGrossMarginPercentage`<25,serviceorder.`shipNumber`,"") 'ShipNumber1'
FROM `customerfile` customerfile 
	left outer join `company` company on company.parentCorpId = $P{Corporate ID}
	left outer join `serviceorder` serviceorder on serviceorder.`customerFileId` = customerfile.id and serviceorder.corpid = $P{Corporate ID}
	inner join `miscellaneous` miscellaneous on miscellaneous.id = serviceorder.id and miscellaneous.corpid = $P{Corporate ID}
	left outer join `refmaster` refmaster_so on serviceorder.`originState`= refmaster_so.`code` and refmaster_so.`parameter` = 'STATE' and refmaster_so.`corpID` in ($P{Corporate ID},'TSFT','VOER')
	left outer join `refmaster` refmaster_sd on serviceorder.`destinationState`= refmaster_sd.`code` and refmaster_sd.`parameter` = 'STATE' and refmaster_sd.`corpID` in ($P{Corporate ID},'TSFT','VOER')
where customerfile.corpid = $P{Corporate ID}
AND (customerfile.`controlFlag` ='C' AND customerfile.`movetype`='Quote')
and serviceorder.`shipNumber`!=''
and serviceorder.`shipNumber` is not null
and customerfile.`survey` between $P{Begin_Date} AND $P{End_Date}
and customerfile.`quotationStatus`!='Rejected']]>
	</queryString>
	<field name="QuotationVsGrossMargin-R-Extract-VORU.jrxml" class="java.lang.String"/>
	<field name="Quotation" class="java.lang.String"/>
	<field name="Quote File" class="java.lang.String"/>
	<field name="Compete" class="java.lang.String"/>
	<field name="Bill to Name" class="java.lang.String"/>
	<field name="Employer Name" class="java.lang.String"/>
	<field name="Client Name" class="java.lang.String"/>
	<field name="Survey Date" class="java.lang.String"/>
	<field name="Move Date" class="java.lang.String"/>
	<field name="Job" class="java.lang.String"/>
	<field name="From" class="java.lang.String"/>
	<field name="To" class="java.lang.String"/>
	<field name="Volume" class="java.math.BigDecimal"/>
	<field name="Gross Margin" class="java.math.BigDecimal"/>
	<field name="Gross Margin %" class="java.math.BigDecimal"/>
	<field name="Dates" class="java.lang.String"/>
	<field name="CorpID" class="java.lang.String"/>
	<field name="Last Name" class="java.lang.String"/>
	<field name="First Name" class="java.lang.String"/>
	<field name="Origin City" class="java.lang.String"/>
	<field name="Origin State" class="java.lang.String"/>
	<field name="Origin Country" class="java.lang.String"/>
	<field name="Destination City" class="java.lang.String"/>
	<field name="Destination State" class="java.lang.String"/>
	<field name="Destination Country" class="java.lang.String"/>
	<field name="ShipNumber" class="java.lang.String"/>
	<field name="ShipNumber1" class="java.lang.String"/>
	<sortField name="CorpID"/>
	<sortField name="Quotation"/>
	<sortField name="Survey Date"/>
	<group name="Company">
		<groupExpression><![CDATA[$F{CorpID}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="CustNumber">
		<groupExpression><![CDATA[$F{Quotation}]]></groupExpression>
		<groupHeader>
			<band height="19" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-31" mode="Transparent" x="0" y="1" width="61" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="d1294eb9-64cc-43db-8ffe-79a9de35e087"/>
					<box leftPadding="1">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font size="10" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Quotation:]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement key="textField" x="61" y="1" width="100" height="18" uuid="29401e0d-239b-41f6-b2cf-810a03592d83"/>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{Quotation}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="43" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="1" width="867" height="42" uuid="90bb73d0-78f1-421e-bb28-86e75198011b"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="18" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Quotation Vs Gross Margin Report]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="37" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="541" y="1" width="26" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="bf5c9b07-3d41-4bd2-bc0a-87cd7a3b15d8"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Job ]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Opaque" x="471" y="1" width="70" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="7eb0824d-e39d-4066-8253-29baf1591651"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Move by]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" mode="Opaque" x="567" y="1" width="75" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="bdeb2231-add5-4984-ae6b-ec48a322e767"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[From]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" mode="Opaque" x="297" y="1" width="104" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="8f9e1c16-44e2-4922-8d3e-4b264fa9b832"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Client Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" mode="Opaque" x="401" y="1" width="70" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="86ec489e-8905-4235-aa37-cc314e50d2d8"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Survey]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" mode="Opaque" x="0" y="1" width="74" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="0d9c47ae-dd24-465a-9d85-5200d09b1a30"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Quote File ]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="74" y="1" width="48" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="d7934c35-8d64-495a-9796-bbec9a05f4ee"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Compete]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" mode="Opaque" x="122" y="1" width="95" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="ffce5677-52bb-43d1-93b0-71094aeb54c1"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Bill to Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" mode="Opaque" x="217" y="1" width="80" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="219e5a37-a4ee-48b8-aa9b-8ced76e30f82"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Employer Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" mode="Opaque" x="642" y="1" width="75" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="b089edf7-688e-4e4b-badb-f09946d14730"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[To]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" mode="Opaque" x="717" y="1" width="50" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="821c0338-03df-4f5e-ab2a-8d3363bdb420"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Volume]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" mode="Opaque" x="767" y="1" width="50" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="957596e8-9f0f-44b8-a67f-b12d68480067"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Gross]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" mode="Opaque" x="817" y="1" width="50" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="706c0b07-bbe9-4d16-859a-1642a203d0a6"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Gross]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-17" mode="Opaque" x="541" y="19" width="26" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="7fd70f05-a2bf-46e9-a07e-3a9bd432f28a"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-18" mode="Opaque" x="471" y="19" width="70" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="17ac2cd5-ffa1-43a6-9112-38bf2d0becbe"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-19" mode="Opaque" x="567" y="19" width="75" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="30c6a86c-caba-4bfb-8a1d-04749a922081"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-20" mode="Opaque" x="297" y="19" width="104" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="c4231d2c-1a37-45fd-b7c2-bc3bef9b5779"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" mode="Opaque" x="401" y="19" width="70" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="94b5d06b-ef77-4185-84c1-25efdddb8896"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" mode="Opaque" x="0" y="19" width="74" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="9276e3ba-3e7b-48ff-bd5a-41f5fea2c8f8"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" mode="Opaque" x="74" y="19" width="48" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="52e4adef-69ed-4e55-ad57-1aafd9270a83"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" mode="Opaque" x="122" y="19" width="95" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="1a9ce8d6-51f2-43fb-9eed-78e52943cd4a"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-25" mode="Opaque" x="217" y="19" width="80" height="18" isRemoveLineWhenBlank="true" forecolor="#000033" backcolor="#CCCCCC" uuid="056be70c-ee29-4af6-b5f6-88383302c3f3"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-26" mode="Opaque" x="642" y="19" width="75" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="60716e9b-6cf8-4b0e-9668-d2a8543aac68"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-27" mode="Opaque" x="717" y="19" width="50" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="98e1e9b5-194c-4afe-b6ab-f1f8ae30ae18"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-28" mode="Opaque" x="767" y="19" width="50" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="0ccbab23-b560-454f-83c2-fc680a0b6e41"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Margin]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-29" mode="Opaque" x="817" y="19" width="50" height="18" forecolor="#000033" backcolor="#CCCCCC" uuid="7c297f2b-ef68-45db-8533-6b9afedebe77"/>
				<box leftPadding="1">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Margin %]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="27" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="0" width="89" height="18" uuid="e14c5533-5270-47c2-b344-53c81744c5ba"/>
				<box leftPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ShipNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="89" y="0" width="33" height="18" uuid="48dce8cf-1585-454f-960a-dbfac8c8325b"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Compete}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="122" y="0" width="95" height="18" uuid="b2d67466-60a2-4f27-960d-64b1af1150da"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Bill to Name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="217" y="0" width="80" height="18" uuid="1051a3bd-57d7-4150-9b82-e22b065c3e93"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Employer Name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="297" y="0" width="104" height="18" uuid="b0afb0d5-e658-416a-ab2f-812cc92cc10f"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{Last Name}!=null ? $F{Last Name} : "")+" "+($F{First Name}!=null ? $F{First Name} : "")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="401" y="0" width="70" height="18" uuid="d94b90f4-39a0-433c-83f3-978ec442bc57"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Survey Date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="471" y="0" width="70" height="18" uuid="660df74d-59ad-461a-9e11-ccbe3650f594"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Move Date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="541" y="0" width="26" height="18" uuid="98e09bc1-d0ff-46a0-815a-55a9da612f11"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Job}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="567" y="0" width="75" height="18" uuid="7f236ca1-2116-4920-804e-6b6d8298e6f2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{Origin City}!=null ? $F{Origin City} : "")+(($F{Origin City}.equals("")) ? "" : (", "+ $F{Origin State}))+"\n"+($F{Origin Country}!=null ? $F{Origin Country} : "")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="642" y="0" width="75" height="18" uuid="31b7fe22-3378-4451-b53e-37880b7eb232"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{Destination City}!=null ? $F{Destination City}:"")+(($F{Destination State}.equals("")) ? "" : (", "+ $F{Destination State}))+"\n"+($F{Destination Country}!=null ? $F{Destination Country} : "")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="717" y="0" width="50" height="18" uuid="f805416a-1874-4c97-9646-21b62c3212df"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Volume}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="767" y="0" width="50" height="18" uuid="ce436a29-1b13-4008-b87b-2af0a0135fc7"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Gross Margin}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="817" y="0" width="50" height="18" uuid="9b07de97-0e50-4bd9-ab21-4dd598e75b5a"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{Gross Margin %}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-4" x="0" y="0" width="89" height="18" forecolor="#990033" uuid="7326d0d1-046f-44c9-bc3c-6ec9e4d83fe6"/>
				<box leftPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ShipNumber1}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="16" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-30" x="138" y="1" width="629" height="15" uuid="b8193edb-c5d4-496d-a2e5-d56a52d1bf55"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="10"/>
				</textElement>
				<text><![CDATA[QuotationVsGrossMargin.jrxml]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="767" y="1" width="50" height="15" uuid="86516475-9f67-4dbf-b0ae-14731bd787a2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Helvetica" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="817" y="1" width="50" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="bceac110-c656-4bee-8051-5feafa667489"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd MMM yyyy" isBlankWhenNull="true">
				<reportElement key="textField-3" x="1" y="1" width="137" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="6a60568b-d38b-4c41-9987-f549b7e0e5e5"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Helvetica" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Dates}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
