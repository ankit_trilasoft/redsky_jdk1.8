<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<meta name="heading" content="Schedule Resource"/> 
<title>Schedule Resource</title> 
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<style>
.invredsch{
	font-family: arial,verdana; 
	text-decoration: none; border-radius: 3px; 
	padding: 1px 2px; color: rgb(254, 46, 46); 
	background: #ffffff; /* Old browsers */
	background: -moz-linear-gradient(top, #ffffff 0%, #fcf2f2 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top, #ffffff 0%,#fcf2f2 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom, #ffffff 0%,#fcf2f2 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#fcf2f2',GradientType=0 );
}
 .btbg1-big{
 background:url(images/countbg-big.gif) no-repeat scroll 0 0;
 font-size:12px;
 padding-top:3px;
 text-align:center;
 width:160px;height:24px;
 }
 .btbg1-middle{
 background:url(images/bg-hide.png) no-repeat scroll 0 0;
 font-size:12px;
 padding-top:3px;
 text-align:center;width:83px;
 height:24px; 
 cursor: pointer;
 }
 .btbg1-small{
 background:url(images/countbg.gif) no-repeat scroll 0 0;
 font-size:12px;
 padding-top:3px;
 text-align:center;
 width:79px;
 height:24px;
 }
.bleft{border-left:none !important;border-right:none !important;}
.bright{border-left:1px solid #74b3dc !important;border-right:1px solid #74b3dc !important;}
 </style>
<script type="text/JavaScript">
window.onload = function() {
	
	if (screen.width > 1600 ){
		 var elem = document.getElementById("ResorceCenDiv");				
		 elem.style.overflow = 'auto';
		 elem.style.width = '58%';
		 
}else if (screen.width == 1600 ){
		 var elem = document.getElementById("ResorceCenDiv");				
		 elem.style.overflow = 'auto';
		 elem.style.width = '58%';
		 
}else if (screen.width == 1440 ){
		 var elem = document.getElementById("ResorceCenDiv");				
		 elem.style.overflow = 'auto';
		 elem.style.width = '58%';
		 
}else if (screen.width == 1366 ){
	 var elem = document.getElementById("ResorceCenDiv");				
	 elem.style.overflow = 'auto';
	 elem.style.width = '58%';
	 
}else if (screen.width == 1360 ){
	 var elem = document.getElementById("ResorceCenDiv");				
	 elem.style.overflow = 'auto';
	 elem.style.width = '58%';
	 
 }else if(screen.width == 1280) {
	  var elem = document.getElementById("ResorceCenDiv");
	  elem.style.overflow = 'auto';
	  elem.style.width = '58%';
	  
 }else if(screen.width == 1152) {
	  var elem = document.getElementById("ResorceCenDiv");
	  elem.style.overflow = 'auto';
	  elem.style.width = '58%';
	  
 }else if(screen.width == 1024){
	  var elem = document.getElementById("ResorceCenDiv");
	  elem.style.overflow = 'auto';
	  elem.style.width = '58%';
 }
 else {
	  var elem = document.getElementById("ResorceCenDiv");
	  elem.style.overflow = 'auto';
	  elem.style.width = '58%';
}
}

</script>


<script language="javascript" type="text/javascript"> 

function showMMMessage(chkObj,target,mmDevice){
	<c:if test= "${mmValidation =='Yes' }" >
	var deviceName='';
		<c:forEach var="entry" items="${mmDeviceMap}">
   				if('${entry.key}'== mmDevice){
   					deviceName =  '${entry.value}';
   				}
		</c:forEach>
		var divObj = document.getElementById("mmMessage");
		if(target == 'MM'){
			if(chkObj.checked ){
				if(document.forms['timeSheetList'].elements['mobileMoverCheck'].value==''){
					document.forms['timeSheetList'].elements['mobileMoverCheck'].value = 'ok';
					divObj.innerHTML = '<font>(Dispatch Email will be sent to Mobile Mover Device Name -'+deviceName+'('+mmDevice+'))</font>';
				}else{
					alert("You have already chose a mobile mover crew.");
					chkObj.checked = false;
				}
			}else{
				document.forms['timeSheetList'].elements['mobileMoverCheck'].value ='';
				divObj.innerHTML = '';
			}
		}
	</c:if>
}

function sendToMobileMover(){
	var overlay = document.getElementById("overlay");
	var requestedCW = document.forms['timeSheetList'].elements['requestedCrews'].value;
	var requestedWT = document.forms['timeSheetList'].elements['requestedWorkTkt'].value;
	//overlay.style.visibility = "";
	//try{
	//	document.getElementById("layerLoading").style.display ="none";
	///}catch(e){}
//	overlay.innerHTML = '<div><table cellspacing="0" cellpadding="0" border="0" width="100%" ><tr><td align="center"><table cellspacing="0" cellpadding="3" align="center"><tr><td height="200px"></td></tr><tr><td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="400px"><font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please Wait........ Syncing with Mobile Mover Sync Server</font></td></tr><tr><td  style="background-color:#DFDFDF;"  class="rounded-bottom" width="400px" align="center" height="35px"><iframe style="margin-left:126px; height:34px" id="spinner" src="${pageContext.request.contextPath}/images/ajax-loader-hr.gif"  scrolling="no" frameborder="0"></iframe></td></tr></table></td></tr></table></div>';
		var url = "sendTicketToMobileMover.html?requestedCW="+requestedCW+"&requestedWT="+requestedWT+"&buttonType=MM";
	//	$("#spinner").attr("src", "${pageContext.request.contextPath}/images/ajax-loader-hr.gif");
		window.location.assign(url);
	///	$("#spinner").attr("src", "${pageContext.request.contextPath}/images/ajax-loader-hr.gif");
	}


function setServiceGroup() { 
	  var serviceGroup = document.forms['timeSheetList'].elements['workticketServiceGroup'].value; 
	  document.forms['timeSheetList'].elements['serviceGroup'].value=serviceGroup;
	  var url="serviceType.html?ajax=1&decorator=simple&popup=true&serviceGroup="+encodeURI(serviceGroup);
	  http6.open("GET", url, true);
	  http6.onreadystatechange = handleHttpResponse4;
	  http6.send(null);
	 }		 
	function handleHttpResponse4()  {
			 if (http6.readyState == 4)  {
	         var results = http6.responseText
	         results = results.trim();
	         resu = results.replace("{",'');
	         resu = resu.replace("}",'');
			 var res = resu.split(",");
			 document.forms['timeSheetList'].elements['workticketService'].options.length = res.length;		 
	         for(i=0;i<res.length;i++) {
	         if(res[i] == ''){
				document.forms['timeSheetList'].elements['workticketService'].options[i].text = '';
				document.forms['timeSheetList'].elements['workticketService'].options[i].value = '';
			 }else{
			var resd = res[i].split("=");
			document.forms['timeSheetList'].elements['workticketService'].options[i].text = resd[1].trim();
			document.forms['timeSheetList'].elements['workticketService'].options[i].value = resd[0].trim();
		}
	   } 
	  }
	 }
	              
	var http6 = getHTTPObject2();
	function getHTTPObject2(){
	   var xmlhttp;
	   if(window.XMLHttpRequest){
	       xmlhttp = new XMLHttpRequest();
	   }else if (window.ActiveXObject){
	       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	       if (!xmlhttp){
	           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	       }
	   }
	   return xmlhttp;
	  }




function requestedWorkCrews(targetElement, checkUsed){
	//checkWorkDate();
	var truckRequired='${truckRequired}';
	var chkbtn = document.forms['timeSheetList'].elements['checkbtn'].value;
	var checkUsed=checkUsed+targetElement.id;
	var requestedCW = document.forms['timeSheetList'].elements['requestedCrews'].value;
	var checkUsedCW = document.forms['timeSheetList'].elements['checkUsed'].value;
	if(targetElement.checked){
		if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' ){
			document.forms['timeSheetList'].elements['requestedCrews'].value = targetElement.id;
			document.forms['timeSheetList'].elements['checkUsed'].value = checkUsed;
		}else{
			if(requestedCW.indexOf(targetElement.id)>=0){}
			else{
				requestedCW = requestedCW + ',' +targetElement.id;
				checkUsedCW = checkUsedCW + ',' +checkUsed;
			}
			document.forms['timeSheetList'].elements['requestedCrews'].value = requestedCW.replace(',,',",");
			document.forms['timeSheetList'].elements['checkUsed'].value = checkUsedCW.replace(',,',",");
		}
	}else{
		if(requestedCW.indexOf(targetElement.id)>=0){
			requestedCW = requestedCW.replace(targetElement.id,"");
			checkUsedCW = checkUsedCW.replace(checkUsed,"");
		}
		document.forms['timeSheetList'].elements['requestedCrews'].value = requestedCW.replace(',,',",");
		document.forms['timeSheetList'].elements['checkUsed'].value = checkUsedCW.replace(',,',",");
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedCrews'].value = '';
			document.forms['timeSheetList'].elements['checkUsed'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedCrews'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedCrews'].value = (document.forms['timeSheetList'].elements['requestedCrews'].value).substring(1);
			document.forms['timeSheetList'].elements['checkUsed'].value = (document.forms['timeSheetList'].elements['checkUsed'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedCrews'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedCrews'].value.length -1){
			document.forms['timeSheetList'].elements['requestedCrews'].value = (document.forms['timeSheetList'].elements['requestedCrews'].value).substring(0,document.forms['timeSheetList'].elements['requestedCrews'].value.length-1)
			document.forms['timeSheetList'].elements['checkUsed'].value = (document.forms['timeSheetList'].elements['checkUsed'].value).substring(0,document.forms['timeSheetList'].elements['checkUsed'].value.length-1)
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' || document.forms['timeSheetList'].elements['requestedTrucks'].value == ''){
		//alert(truckRequired);
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['absent'].disabled = true;
		document.forms['timeSheetList'].elements['present'].disabled = true;
		if(document.forms['timeSheetList'].elements['requestedCrews'].value != '' && document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' && document.forms['timeSheetList'].elements['requestedTrucks'].value == ''){            
			if(chkbtn=='A'){
			document.forms['timeSheetList'].elements['absent'].disabled = false;
			}else{
			document.forms['timeSheetList'].elements['present'].disabled = false;
			}
		}else{
			document.forms['timeSheetList'].elements['absent'].disabled = true;
			document.forms['timeSheetList'].elements['present'].disabled = true;
		}
		if(document.forms['timeSheetList'].elements['requestedCrews'].value != '' && document.forms['timeSheetList'].elements['requestedWorkTkt'].value != '' && ( document.forms['timeSheetList'].elements['requestedTrucks'].value == '' && truckRequired=='N')){
			if(chkbtn=='A'){
				//alert(1+"k1"+  truckRequired);
				document.forms['timeSheetList'].elements['assign'].disabled = false;
			}
		}
		
	}else{
		if(chkbtn=='A'){
			document.forms['timeSheetList'].elements['assign'].disabled = false;
		}
		
	}
}
 </script>

<script language="javascript" type="text/javascript">  
function requestedWorkTickets(targetElement){
	//checkWorkDate();
	var truckRequired='${truckRequired}';
	var chkbtn = document.forms['timeSheetList'].elements['checkbtn'].value;
	var requestedWT = document.forms['timeSheetList'].elements['requestedWorkTkt'].value;
	if(targetElement.checked){
		if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' ){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = targetElement.id;
		}else{
			if(requestedWT.indexOf(targetElement.id)>=0){}
			else{
				requestedWT = requestedWT + ',' +targetElement.id;
			}
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = requestedWT.replace(',,',",");
		}
	}else{
		if(requestedWT.indexOf(targetElement.id)>=0){
			requestedWT = requestedWT.replace(targetElement.id,"");
		}
		document.forms['timeSheetList'].elements['requestedWorkTkt'].value = requestedWT.replace(',,',",");
	}
	
	if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedWorkTkt'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = (document.forms['timeSheetList'].elements['requestedWorkTkt'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedWorkTkt'].value.length -1){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = (document.forms['timeSheetList'].elements['requestedWorkTkt'].value).substring(0,document.forms['timeSheetList'].elements['requestedWorkTkt'].value.length-1)
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' || document.forms['timeSheetList'].elements['requestedTrucks'].value == ''){
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['absent'].disabled = true;
		if(document.forms['timeSheetList'].elements['requestedCrews'].value != '' && document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' && document.forms['timeSheetList'].elements['requestedTrucks'].value == ''){
			if(chkbtn=='A'){
				document.forms['timeSheetList'].elements['absent'].disabled = false;
				}else{
				document.forms['timeSheetList'].elements['present'].disabled = false;
			}
		}else{
			document.forms['timeSheetList'].elements['absent'].disabled = true;
			document.forms['timeSheetList'].elements['present'].disabled = true;
		}
		if(document.forms['timeSheetList'].elements['requestedCrews'].value != '' && document.forms['timeSheetList'].elements['requestedWorkTkt'].value != '' && ( document.forms['timeSheetList'].elements['requestedTrucks'].value == '' && truckRequired=='N')){
			if(chkbtn=='A'){
				//alert(1+"k1"+  truckRequired);
				document.forms['timeSheetList'].elements['assign'].disabled = false;
			}
		}
	}else{
		if(chkbtn=='A'){
			document.forms['timeSheetList'].elements['assign'].disabled = false;
		}
		
	}
	//if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value != ''){
	//	document.forms['timeSheetList'].elements['sendToMM'].disabled = false;
	//}else{
	//	document.forms['timeSheetList'].elements['sendToMM'].disabled = true;
	//}
	
}

function requestedTrucksss(targetElement,name){
	//checkWorkDate();
	//alert(targetElement);
	var agree60 = "This vehicle is due in 60 days. Please contact warehouse manager for renewal.";
	var agree30 = "This vehicle is due in 30 days. Please contact warehouse manager for renewal.";
	if(name<=30 && name>=0 && targetElement.checked){
		alert(agree30);
	}if(name>30 && name<=60 && targetElement.checked){
		alert(agree60);
	}
	if(name<0 && targetElement.checked){
		alert("This vehicle is over due "+(-(name))+" days. Please contact warehouse manager for renewal.");
		return false;
	}
	var truckRequired='${truckRequired}';
	var requestedTruc = document.forms['timeSheetList'].elements['requestedTrucks'].value;
	var chkbtn = document.forms['timeSheetList'].elements['checkbtn'].value;
	if(targetElement.checked){
		if(document.forms['timeSheetList'].elements['requestedTrucks'].value == '' ){
			document.forms['timeSheetList'].elements['requestedTrucks'].value = targetElement.id;
		}else{
			
			if(requestedTruc.indexOf(','+targetElement.value)>=0){}
			else{
				
				requestedTruc = requestedTruc + ',' +targetElement.id;
			}
			document.forms['timeSheetList'].elements['requestedTrucks'].value = requestedTruc.replace(',,',",");
		}
	}else{
		if(requestedTruc.indexOf(',')>=0)
		{
			requestedTruc = requestedTruc.replace(','+targetElement.id,"");
			requestedTruc = requestedTruc.replace(targetElement.id+',',"");
		}
		else{
			requestedTruc = requestedTruc.replace(targetElement.id,"");
		}
	
		document.forms['timeSheetList'].elements['requestedTrucks'].value = requestedTruc.replace(',,',",");
		
	}
	
	if(document.forms['timeSheetList'].elements['requestedTrucks'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedTrucks'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedTrucks'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedTrucks'].value = (document.forms['timeSheetList'].elements['requestedTrucks'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedTrucks'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedTrucks'].value.length -1){
			document.forms['timeSheetList'].elements['requestedTrucks'].value = (document.forms['timeSheetList'].elements['requestedTrucks'].value).substring(0,document.forms['timeSheetList'].elements['requestedTrucks'].value.length-1)
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' || document.forms['timeSheetList'].elements['requestedTrucks'].value == ''){
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['absent'].disabled = true;
		if(document.forms['timeSheetList'].elements['requestedCrews'].value != '' && document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' && document.forms['timeSheetList'].elements['requestedTrucks'].value == ''){
			if(chkbtn=='A'){
				document.forms['timeSheetList'].elements['absent'].disabled = false;
				}else{
				document.forms['timeSheetList'].elements['present'].disabled = false;
			}
		}else{
			document.forms['timeSheetList'].elements['absent'].disabled = true;
			document.forms['timeSheetList'].elements['present'].disabled = true;
		}
		if(document.forms['timeSheetList'].elements['requestedCrews'].value != '' && document.forms['timeSheetList'].elements['requestedWorkTkt'].value != '' && ( document.forms['timeSheetList'].elements['requestedTrucks'].value == '' && truckRequired=='N')){
			if(chkbtn=='A'){
				//alert(1+"k1"+  truckRequired);
				document.forms['timeSheetList'].elements['assign'].disabled = false;
			}
		}
	}else{
		if(chkbtn=='A'){
			document.forms['timeSheetList'].elements['assign'].disabled = false;
		}
		
	}
	
}
 </script>
<script language="javascript" type="text/javascript"> 
function removeTime(timeSheetId,truckingOpsId,ticket,id,element){
	var timeSheetIdValue=timeSheetId+"--"+truckingOpsId+"--"+ticket+"--"+id;
	removeTimeSheets(element,timeSheetIdValue);
	
}

function removeTimeSheets(timeSheetId,element){
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		timeSheetId.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew warehouse to continue.....");
 		timeSheetId.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
		alert("Select Ticket WareHouse  to continue.....");
		timeSheetId.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
		alert("Select Truck WareHouse  to continue.....");
		timeSheetId.checked = false;
 		return false;
 	}
	var requestedTS = document.forms['timeSheetList'].elements['requestedSchedule'].value;
	if(timeSheetId.checked){
		if(document.forms['timeSheetList'].elements['requestedSchedule'].value == '' ){
			document.forms['timeSheetList'].elements['requestedSchedule'].value = element;
		}else{
			if(requestedTS.indexOf(element)>=0){}
			else{
				requestedTS = requestedTS + ',' +element;
			}
			document.forms['timeSheetList'].elements['requestedSchedule'].value = requestedTS.replace(',,',",");
		}
	}else{
		if(requestedTS.indexOf(element)>=0){
			requestedTS = requestedTS.replace(element,"");
		}
		document.forms['timeSheetList'].elements['requestedSchedule'].value = requestedTS.replace(',,',",");
	}
	
	
	
	if(document.forms['timeSheetList'].elements['requestedSchedule'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedSchedule'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedSchedule'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedSchedule'].value = (document.forms['timeSheetList'].elements['requestedSchedule'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedSchedule'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedSchedule'].value.length -1){
			document.forms['timeSheetList'].elements['requestedSchedule'].value = (document.forms['timeSheetList'].elements['requestedSchedule'].value).substring(0,document.forms['timeSheetList'].elements['requestedSchedule'].value.length-1)
	}
	if(document.forms['timeSheetList'].elements['requestedSchedule'].value == ''){
		document.forms['timeSheetList'].elements['remove'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['remove'].disabled = false;
	}
}

 function updateDrivers(chk){
		var driverId = 0;
		if(chk.checked == true){
			driverId = chk.value;
			url="updateDriver.html?ajax=1&decorator=simple&popup=true&driverId="+encodeURI(driverId)+"&driverChecked="+chk.checked;
		}
		if(chk.checked == false){
			driverId = chk.value;
			url="updateDriver.html?ajax=1&decorator=simple&popup=true&driverId="+encodeURI(driverId)+"&driverChecked="+chk.checked;
		}
		httpDriver.open("GET", url, true);
		httpDriver.onreadystatechange = handleHttpResponseDriver;
		httpDriver.send(null);				 			 
	}
 function handleHttpResponseDriver()  {
	    if (httpDriver.readyState == 4)  {  }
     }
 var httpDriver = getHTTPObjectDriver();
 function getHTTPObjectDriver(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
 }
 function buildScheduleResource(){
 	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		return false;
 	}else{
 		document.forms['timeSheetList'].action = 'buildScheduleResource.html';
 		document.forms['timeSheetList'].submit();
		submit_form();
 	}
 }
 
 function hideSelectedTicket(){
	 if(document.forms['timeSheetList'].elements['workDt'].value == ""){
	 		alert("Select work date to continue.....");
	 		document.forms['timeSheetList'].elements['workDt'].select();
	 		return false;
	 }
	 if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
	 		alert("Select Crew WareHouse to continue.....");
	 		return false;
	 }
	 if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
			alert("Select Ticket WareHouse  to continue.....");
	 		return false;
	 }
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
			alert("Select Truck WareHouse  to continue.....");
	 		return false;
	 }
	if(document.forms['timeSheetList'].elements['wareHse'].value != "" && document.forms['timeSheetList'].elements['wareHse'].value != ""){
		document.forms['timeSheetList'].elements['checkbtn'].value='A';
	 	document.forms['timeSheetList'].action = 'hideSelectedTicket.html';
	 	document.forms['timeSheetList'].submit();	
	} 
 }
 
 
 function groupAssignTimeSheet(){
 if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		return false;
 	}else{
 		submit_form();
 		document.forms['timeSheetList'].action = 'groupAssignTimeSheet.html';
 		document.forms['timeSheetList'].submit();
 	}
 
 }
 function removeTimeSheet(){
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
		alert("Select work date to continue.....");
		document.forms['timeSheetList'].elements['workDt'].select();
		return false;
	}else{
		document.forms['timeSheetList'].elements['checkbtn'].value='A';
	 	document.forms['timeSheetList'].action = 'removeScheduleResources.html';
	 	document.forms['timeSheetList'].submit();
	 	//self.document.location.reload(true);
	 	//document.location.reload();
	}
 }
 
 function absentsTimeSheet(){
 	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		return false;
 	}

 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
		alert("Select Ticket WareHouse  to continue.....");
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
		alert("Select Truck WareHouse  to continue.....");
 		return false;
 	}
 	var check=document.forms['timeSheetList'].elements['checkUsed'].value;
 	if(check.indexOf('t')>=0){
 		alert('One or more crew selected has already been scheduled, please check again.');
 		return false;
 	}else{
 		document.forms['timeSheetList'].elements['checkbtn'].value='P';
 		document.forms['timeSheetList'].action = 'absentScheduleResource.html?filter=hidden&showAbsent=Absent';
 		document.forms['timeSheetList'].submit();
 	}
 }

 function presentTimeSheet(){
	 	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
	 		alert("Select work date to continue.....");
	 		document.forms['timeSheetList'].elements['workDt'].select();
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
	 		alert("Select Crew WareHouse to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
			alert("Select Ticket WareHouse  to continue.....");
	 		return false;
	 	}
		if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
			alert("Select Truck WareHouse  to continue.....");
	 		return false;
	 	}
	 	var check=document.forms['timeSheetList'].elements['checkUsed'].value;
	 	if(check.indexOf('t')>=0){
	 		alert('One or more crew selected has already been scheduled, please check again.');
	 		return false;
	 	}else{
	 		document.forms['timeSheetList'].elements['checkbtn'].value='A';
	 		document.forms['timeSheetList'].action = 'presentScheduleResource.html?filter=hidden&showAbsent=None';
	 		document.forms['timeSheetList'].submit();
	 	}
	 }
 </script>
 
 <script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['timeSheetList'].elements['workDt'].value = "";
			document.forms['timeSheetList'].elements['wareHse'].value = "";
			document.forms['timeSheetList'].elements['tktWareHse'].value = "";
			document.forms['timeSheetList'].elements['truckWareHse'].value = "";
			document.forms['timeSheetList'].elements['workticketService'].value = "";
			document.forms['timeSheetList'].elements['workticketServiceGroup'].value = "";
			document.forms['timeSheetList'].elements['workticketStatus'].value = "";
			setServiceGroup();
}

function editTimeSheets(targetElement){
	var id = targetElement;
	document.forms['timeSheetList'].action = 'editTimeSheet.html?from=list&id='+id;
	document.forms['timeSheetList'].submit();
}

function isInteger()
{   var i;
	var s = document.forms['timeSheetList'].elements['tkt'].value;
	
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Only numbers are allowed in ticket field");
        return false;
        }
    }
    return true;
}

function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}

function checkCondition(){
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}
 	
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
 		alert("Select Ticket WareHouse  to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
 		alert("Select Truck WareHouse  to continue.....");
 		return false;
 	}
 	
}
function checkNavigation(temp){
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}	
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
 		alert("Select Ticket WareHouse  to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
 		alert("Select Truck WareHouse  to continue.....");
 		return false;
 	}
 	if(temp=='less'){
 		goPrevNext('lessThanOneDay');
 	 }else{
 	 	goPrevNext('greaterThanOneDay');	
 	 }	
}

function checkAll(targetElement){
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		targetElement.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew warehouse to continue.....");
 		targetElement.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
		alert("Select Ticket WareHouse  to continue.....");
 		targetElement.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
		alert("Select Truck WareHouse  to continue.....");
 		targetElement.checked = false;
 		return false;
 	}
}

function ShowAll()
{
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
		alert("Select Ticket WareHouse  to continue.....");
 		//targetElement.checked = false;
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
		alert("Select Truck WareHouse  to continue.....");
 		//targetElement.checked = false;
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value != "" && document.forms['timeSheetList'].elements['wareHse'].value != ""){
 		document.forms['timeSheetList'].elements['checkbtn'].value='A';
 		document.forms['timeSheetList'].action = 'searchScheduleResouce.html?filter=ShowAll';
 		document.forms['timeSheetList'].submit();
 	}
}
function ShowAllCrews()
{
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['checkbtn'].value='P';
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		document.forms['timeSheetList'].elements['checkbtn'].value='P';
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
		alert("Select Ticket WareHouse  to continue.....");
		document.forms['timeSheetList'].elements['checkbtn'].value='P';
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
		alert("Select Truck WareHouse  to continue.....");
		document.forms['timeSheetList'].elements['checkbtn'].value='P';
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value != "" && document.forms['timeSheetList'].elements['wareHse'].value != ""){
 		document.forms['timeSheetList'].elements['checkbtn'].value='A';
 		document.forms['timeSheetList'].action = 'searchScheduleResouce.html?showAbsent=None';
 		document.forms['timeSheetList'].submit();
 	}
 	
}
function ShowAbsentCrews()
{
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
		alert("Select Ticket WareHouse  to continue.....");
 		return false;
 	}
	if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
		alert("Select Truck WareHouse  to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value != "" && document.forms['timeSheetList'].elements['wareHse'].value != ""){
 		document.forms['timeSheetList'].elements['checkbtn'].value='P';
 		document.forms['timeSheetList'].action = 'searchScheduleResouce.html?showAbsent=Absent';
 		document.forms['timeSheetList'].submit();
 	}
}
function excludeEearningOppertunity(){
 
 		document.forms['timeSheetList'].action = 'excludeEearningOppertunity.html?filter=excludeEearningOppertunity';
 		document.forms['timeSheetList'].submit();
 }
 
 function doneForDay(){
 
 		document.forms['timeSheetList'].action = 'doneForDay.html?filter=doneForDay';
 		document.forms['timeSheetList'].submit();
 }
 
 function filterLunch(){
 
 		document.forms['timeSheetList'].action = 'filterLunch.html?filter=filterLunch';
 		document.forms['timeSheetList'].submit();
 }
 
 function openTimeSheets(){
 
 	document.forms['timeSheetList'].action = 'openTimeSheets.html?filter=openTimeSheets';
 	document.forms['timeSheetList'].submit();
}

function setFlagValue(){
	document.forms['timeSheetList'].elements['hitFlag'].value = '1';
}

function selectWarehouse(){
	document.forms['timeSheetList'].elements['wareHse'].value=document.forms['timeSheetList'].elements['tktWareHse'].value;
	document.forms['timeSheetList'].elements['truckWareHse'].value=document.forms['timeSheetList'].elements['tktWareHse'].value;
}

function selectTicketWarehouse(){
	document.forms['timeSheetList'].elements['tktWareHse'].value=document.forms['timeSheetList'].elements['wareHse'].value;
	document.forms['timeSheetList'].elements['truckWareHse'].value=document.forms['timeSheetList'].elements['wareHse'].value;
}

function openTicket(workTickeTID){
	 var newWindow = window.open('editWorkTicketUpdate.html?id='+workTickeTID, '_blank');
	 newWindow.focus();
	
}

function openTicketCrews(workTickeTID){
	 var newWindow = window.open('workTicketCrews.html?id='+workTickeTID, '_blank');
	 newWindow.focus();
}

function openTicketTrucks(workTickeTID,ticket,sid){
	 var newWindow = window.open('truckingOperationsList.html?ticket='+ticket+'&sid='+sid+'&tid='+workTickeTID, '_blank');
	 newWindow.focus();
}

function findWorkTicketForms(ticket,jobNumber,position){
	var url="reportBySubModuleWorkTicket.html?ajax=1&decorator=simple&popup=true&ticket=" + encodeURI(ticket)+"&id="+ticket+"&jobNumber="+jobNumber+"&reportModule=workTicket&reportSubModule=workTicket";
	ajax_showTooltip(url,position);	
}
function showAttachedDocList(id,position){
	var url="ticketDocs.html?ajax=1&decorator=simple&popup=true&id="+id;
	ajax_showTooltip(url,position);	
	var divscroll = document.getElementById("ResorceCenDiv").scrollLeft;
  	var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;	 
  document.getElementById("ajax_tooltipObj").style.left = ((parseInt(myposition))-25)+'px';	  
  var divscroll2 = document.getElementById("ResorceCenDiv").scrollTop;
  	var myposition2 = document.getElementById("ajax_tooltipObj").offsetTop-divscroll2 ;	 
  document.getElementById("ajax_tooltipObj").style.top =myposition2+'px';
}
function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer){
	window.open('viewFormParam.html?id='+id+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID='+invNum+'&custID=${custID}&reportModule=workTicket&reportSubModule==workTicket&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
	//&reportName=${reportsList.description}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&
	//window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
}

function goPrevNext(typeChange) {
	var myDateValue1=document.forms['timeSheetList'].elements['workDt'].value;
	if(myDateValue1!=''){
		var mdate111= new Date();
	    var mdate11 = myDateValue1.split("-");         
	    var day = mdate11[0];
	    var month = mdate11[1];
		var year = mdate11[2];			
	    if(month == 'Jan'){
		       month = "01";
		   	}
		if(month == 'Feb'){
		       month = "02";
		   	}
		if(month == 'Mar'){
		       month = "03";
		   	}
		if(month == 'Apr'){
		       month = "04";
		   	}
		if(month == 'May'){
		       month = "05";
		   	}
		if(month == 'Jun'){
		       month = "06";
		   	}
		if(month == 'Jul'){
		       month = "07";
		   	}
		if(month == 'Aug'){
		       month = "08";
		   	}
		if(month == 'Sep'){
		       month = "09";
		   	}
		if(month == 'Oct'){
		       month = "10";
		  	}
		if(month == 'Nov'){
		       month = "11";
		   	}
		if(month == 'Dec'){
		       month = "12";
		   	}
		
	     if(parseInt(year)>=00 && parseInt(year)<50)
	     {
	         year=2000+parseInt(year)+"";
	     }
	     
	     if(parseInt(year)>=50)
	     {
	         year=1900+parseInt(year)+"";
	     }
	
	   		
	    mdate11=year+"/"+month+"/"+day;
		mdate111= new Date(mdate11);
		if(typeChange=='lessThanOneDay'){ 
			var mydate = subtractDate(mdate111, 1);		   	 	
		}
		if(typeChange=='greaterThanOneDay'){ 
			var mydate = addDate(mdate111, 1);			   	 	
		}
		var year1=mydate.getFullYear();
		var y=""+year1;
		if (year1 < 1000)
		year1+=1900;
		var month1=mydate.getMonth()+1;
		if(month1 == 1)month1="Jan";
		if(month1 == 2)month1="Feb";
		if(month1 == 3)month1="Mar";
		if(month1 == 4)month1="Apr";
		if(month1 == 5)month1="May";
		if(month1 == 6)month1="Jun";
		if(month1 == 7)month1="Jul";
		if(month1 == 8)month1="Aug";
		if(month1 == 9)month1="Sep";
		if(month1 == 10)month1="Oct";
		if(month1 == 11)month1="Nov";
		if(month1 == 12)month1="Dec";
	    var daym=mydate.getDate();
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month1+"-"+y.substring(2,4);
		document.forms['timeSheetList'].elements['workDt'].value=datam;
		location.href="searchScheduleResouce.html?filter=hidden";
		document.forms['timeSheetList'].submit();
	}else{
		alert("Select work date to continue.....");
		return false;
    }
  }

   function addDate(dateObject, numDays) {
	dateObject.setDate(dateObject.getDate() + numDays);
	return dateObject;
   }
	 
  function subtractDate(dateObject, numDays) {
	dateObject.setDate(dateObject.getDate() - numDays);
	return dateObject;
  }
  
  function searchScheduleResource(){
	  document.forms['timeSheetList'].elements['flag'].value='';
	  if(document.forms['timeSheetList'].elements['workDt'].value == ""){
	 		alert("Select work date to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
	 		alert("Select Crew WareHouse to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
			alert("Select Ticket WareHouse  to continue.....");
	 		targetElement.checked = false;
	 		return false;
	 	}
		if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
			alert("Select Truck WareHouse  to continue.....");
	 		targetElement.checked = false;
	 		return false;
	 	}
		document.forms['timeSheetList'].action = 'searchScheduleResouce.html?filter=hidden&checkbtn=A';
 		document.forms['timeSheetList'].submit();
		
  }
  
  function findResourceList(){
	  if(document.forms['timeSheetList'].elements['workDt'].value == ""){
	 		alert("Select work date to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
	 		alert("Select Crew WareHouse to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
			alert("Select Ticket WareHouse  to continue.....");
	 		targetElement.checked = false;
	 		return false;
	 	}
		if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
			alert("Select Truck WareHouse  to continue.....");
	 		targetElement.checked = false;
	 		return false;
	 	}
		document.forms['timeSheetListResource'].elements['truckWareHse'].value=document.forms['timeSheetList'].elements['truckWareHse'].value;
		document.forms['timeSheetListResource'].elements['wareHse'].value=document.forms['timeSheetList'].elements['wareHse'].value;
		document.forms['timeSheetListResource'].elements['workticketServiceGroup'].value=document.forms['timeSheetList'].elements['workticketServiceGroup'].value;
		document.forms['timeSheetListResource'].elements['workticketService'].value=document.forms['timeSheetList'].elements['workticketService'].value;
		document.forms['timeSheetListResource'].elements['workticketStatus'].value=document.forms['timeSheetList'].elements['workticketStatus'].value;
		document.forms['timeSheetListResource'].elements['tktWareHse'].value=document.forms['timeSheetList'].elements['tktWareHse'].value;
		
		document.forms['timeSheetListResource'].action = 'searchResourceItems.html?checkbtn=A&flag=Y';
 		document.forms['timeSheetListResource'].submit();
  }
  function findToolTipResource(ticket,type,position){
	  var url="findToolTipResourceDetail.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket)+"&type="+encodeURI(type);
	  ajax_showTooltip(url,position);

	  var divscroll = document.getElementById("ResorceCenDiv").scrollLeft;
	  	var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;	 
	  document.getElementById("ajax_tooltipObj").style.left = myposition+'px';
	  
	  var divscroll2 = document.getElementById("ResorceCenDiv").scrollTop;
	  	var myposition2 = document.getElementById("ajax_tooltipObj").offsetTop-divscroll2 ;	 
	  document.getElementById("ajax_tooltipObj").style.top =myposition2+'px';//((parseInt(myposition2))-25)+'px';
	  	
	  }
 
  function findToolTipService(shipNumber,ocity,position){  	
  	var url="findToolTipServiceForCity.html?ajax=1&decorator=simple&popup=true&shipNumberForCity="+shipNumber+"&ocityForCity="+ocity;  	
  	ajax_showTooltip(url,position);  	
  	var divscroll = document.getElementById("ResourceDiv").scrollLeft;
  	var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;	 
    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(myposition))-20)+'px';
  	
  }  
  
  function findToolTipDestCityService(shipNumber,dcity,position){	
	  	var url="findToolTipServiceForDestinationCity.html?ajax=1&decorator=simple&popup=true&shipNumberForCity="+shipNumber+"&dcityForCity="+dcity;
	  	ajax_showTooltip(url,position);
	  	var containerdiv =  document.getElementById("ResourceDiv").scrollLeft;
	  	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
	    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))-25)+'px';	  	
	  }
  
  function findToolTipService1(shipNumber,ocity,position){	  
	  	var url="findToolTipServiceForWorkListCity.html?ajax=1&decorator=simple&popup=true&shipNumberForCity="+shipNumber+"&ocityForCity="+ocity;
	  	ajax_showTooltip(url,position);	  	
	 	var divscroll = document.getElementById("ResorceCenDiv").scrollLeft;
	  	var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;	 
	  document.getElementById("ajax_tooltipObj").style.left = ((parseInt(myposition))-25)+'px';	  
	  var divscroll2 = document.getElementById("ResorceCenDiv").scrollTop;
	  	var myposition2 = document.getElementById("ajax_tooltipObj").offsetTop-divscroll2 ;	 
	  document.getElementById("ajax_tooltipObj").style.top =myposition2+'px';
	  	
	  }
  
  function findToolTipDestCityService1(shipNumber,dcity,position){	  	
	  	var url="findToolTipServiceForDestinationWorkListCity.html?ajax=1&decorator=simple&popup=true&shipNumberForCity="+shipNumber+"&dcityForCity="+dcity;
	  	ajax_showTooltip(url,position);
	  	var containerdiv =  document.getElementById("ResorceCenDiv").scrollLeft;
	  var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
	   document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))-25)+'px';
	   
		var containerdiv1 =  document.getElementById("ResorceCenDiv").scrollTop;
		  var newposition1 = document.getElementById("ajax_tooltipObj").offsetTop-containerdiv1 ;	
		   document.getElementById("ajax_tooltipObj").style.top =newposition1+'px'; 	  	
	  }
  function findToolTipCoordinatorSO(shipNumber,position){
		var url="findToolTipCoordinatorByShipNum.html?ajax=1&decorator=simple&popup=true&shipNumber=" + encodeURI(shipNumber);
		ajax_showTooltip(url,position);
		var divscroll = document.getElementById("ResorceCenDiv").scrollLeft;
	  	var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;	 
	    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(myposition))-20)+'px';
	}
  
  function findResourceDetails(ticket,position){
	  	var url="findResourceDetails.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket);
	  	ajax_showTooltip(url,position);
	  	var leftPosition = document.getElementById("ajax_tooltipObj").offsetLeft ;	
	 	document.getElementById("ajax_tooltipObj").style.left = ((parseInt(leftPosition))+10)+'px';
	 	var topPosition = document.getElementById("ajax_tooltipObj").offsetTop;
	 	document.getElementById("ajax_tooltipObj").style.top = ((parseInt(topPosition))+5)+'px';
	}
  
  function validatefields(id,ticket,jobNumber,reportName,docsxfer,jobType,val){
		var url='viewReportWithParam.html?formFrom=list&id='+id+'&noteID='+ticket+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'';
		location.href=url;	  
	}
  </script>

<style>

div#main {
 margin: 0;
 text-align: left;
 width:99% !important;
}
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}
div#inner-content {
margin-left:20px;
}
table {
font-size:1em;
margin:0pt 0pt 0em;
padding:0pt;
}
.table td, .table th, .tableHeaderTable td {
border:1px solid #E0E0E0;
padding:1px 4px;
}

.listwhitetext-crew {
color:#000000;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
background-color:none;

}

.listwhitetext-hcrew {
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
text-decoration:none;
background-color:none;
padding:0px 0px 2px 0px;
!padding-bottom:3px;
}

.radiobtn {
margin:0px 0px 2px 0px;
!padding-bottom:5px;
!margin-bottom:5px;
}
.table th.sorted {
    background-color: #BCD2EF;
    color: #15428B;
}
</style>
<script>

</script>
</head>

<s:form name="timeSheetList" action="searchScheduleResouce.html?filter=hidden&checkbtn=A" onsubmit="return submit_form()" >
<s:hidden name="mobileMoverCheck" />
<s:hidden name="requestedWorkTkt" value=""/>
<s:hidden name="requestedCrews" value=""/>
<s:hidden name="requestedTrucks" value=""/>
<s:hidden name="requestedSchedule" value=""/>
<s:hidden name="checkUsed" value=""/>
<s:hidden name="deleteTruckNumber" />
<s:hidden name="deleteCrewName" />
<s:hidden name="crewNameCheckBox" />
<s:hidden name="truckNumberCheckBox" />
<s:hidden name="whereClauseSecheDuledResourceMap" value=""/>
<s:hidden name="whereClauseSecheDuledResourceMap1" value=""/>
<s:hidden name="fieldValueName" value=""/>
<s:hidden name="ticketNumber" value=""/>
<s:hidden name="workDateValue" value=""/>
<s:hidden name="editable" value="${editable}"/>
<s:hidden name="orderByValue" value="${orderByValue}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="checkbtn" value="<%= request.getParameter("checkbtn")%>"/>
<s:hidden name="flag" value="<%= request.getParameter("flag")%>"/>
<c:if test="${flag!=null && flag=='Y'}">
	<s:hidden name="wareHse" value="${wareHse}"/>
	<s:hidden name="truckWareHse" value="${truckWareHse}"/>
	<s:hidden name="workticketServiceGroup" value="${workticketServiceGroup}"/>
	<s:hidden name="workticketService" value="${workticketService}"/>
	<s:hidden name="workticketStatus" value="${workticketStatus}"/>
</c:if>
<c:set var="separateCrewDriverList" value="No"/>
<configByCorp:fieldVisibility componentId="component.scheduleResouce.separateCrewDriverList">
	<c:set var="separateCrewDriverList" value="Yes"/>
</configByCorp:fieldVisibility>
   <!--<configByCorp:fieldVisibility componentId="component.field.scheduleResource.crewTypeAndClass"> -->
	<s:hidden name="defaultSortValueFlag" value="typeofWork"/>
<!--</configByCorp:fieldVisibility>-->
<div id="otabs" style="margin-top:-20px;">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		  
		</div>
		<div class="spnblk">&nbsp;</div>
	<table class="table" style="width:100%;clear:both;">
		<thead>
			<tr>
				<th>Work Date</th>
					<th>Crew&nbsp;WareHouse</th>
					<th>Ticket&nbsp;WareHouse</th>
					<th>Truck WareHouse</th>
					<th>Service Group</th>
					<th>Service</th>
					<th>Status</th>				
			<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
					<td align="left"><s:textfield cssClass="input-text" id="date1" name="workDt" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" cssStyle="width:222px;" /><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					<a><img align="middle" style="vertical-align:top;margin-top:1px;" onclick="return checkNavigation('less'); " alt="Previous" title="Previous" src="images/s-left.png"/></a>
					<a><img align="middle" style="vertical-align:top;margin-top:1px;" onclick="return checkNavigation('greater'); " alt="Next" title="Next" src="images/s-right.png"/></a>
					</td>
					<!--<td><s:textfield name="serv" required="true" cssClass="input-text" size="7"/></td>-->
					<td><s:select name="wareHse" list="%{wareHouse}" cssStyle="width:110px" cssClass="list-menu" headerKey="" headerValue="" onchange="selectTicketWarehouse();"/></td>
					<!--<td><s:textfield name="crewNm" required="true" cssClass="input-text" size="7"/></td>
					    <td><s:textfield name="shipper" required="true" cssClass="input-text" size="7"/></td>-->
					<td><s:select name="tktWareHse" list="%{wareHouse}" cssStyle="width:110px" cssClass="list-menu" headerKey="" headerValue=""/></td>
					<td><s:select name="truckWareHse" list="%{wareHouse}" cssStyle="width:110px" cssClass="list-menu" headerKey="" headerValue="" /></td>
		            <td><s:select name="workticketServiceGroup" list="%{tcktGrp}" cssStyle="width:100px" cssClass="list-menu" onchange="return setServiceGroup();" headerKey="" headerValue="" /></td>
					<td><s:select name="workticketService" list="%{serviceType}" cssStyle="width:120px" cssClass="list-menu" /></td>
					<td><s:select name="workticketStatus" list="%{tcktactn}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" /></td>
				<td><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.search" onclick="return checkCondition();"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/></td> 
				
			</tr>
			
		</tbody>
	</table>
	<div class="spnblk">&nbsp;</div>
	<table style="margin:0px;padding:0px;width:100%;" cellpadding="0" cellspacing="0">
	<tr>
	<td>
<div style="float: left; width: 100%; clear: both; margin: 0px; padding:3px 0px;">
<div style="float: left; width: 100%; clear: both; margin: 0px; padding:3px 0px;">
<div style="!margin-top:0px; width:20%; height:250px;overflow-x:auto;float:left; overflow-y:auto;"><!-- sandeep -->
		<div id="otabs" style="margin-left:1px;">
		  <ul>
		  <c:if test="${checkbtn=='A' || checkbtn=='A, A' || checkbtn=='A, P'}">
		    <li style="background:none;"><dt class="btbg1-middle" id="crewListBtn" style="cursor: pointer;color:red;" onclick="return ShowAllCrews()"><b>Crew&nbsp;List</b></dt></li>
		    <li style="background:none;"><dt class="btbg1-middle" id="absentCrewBtn" style="cursor: pointer;" onclick="return ShowAbsentCrews()"><b>Absent&nbsp;Crew</b></dt></li>
		  </c:if>
		  <c:if test="${checkbtn=='P'}">
		    <li style="background:none;"><dt class="btbg1-middle" id="crewListBtn" style="cursor: pointer;" onclick="return ShowAllCrews()"><b>Crew&nbsp;List</b></dt></li>
		    <li style="background:none;"><dt class="btbg1-middle" id="absentCrewBtn" style="cursor: pointer;color:red;" onclick="return ShowAbsentCrews()"><b>Absent&nbsp;Crew</b></dt></li>
		  </c:if>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<s:set name="availableCrews" value="availableCrews" scope="request"/>   
		
   <display:table name="availableCrews" class="table" requestURI="searchCrewTickets.html" id="PayrollssList" style="margin-top: -19px; !margin-top:0px" >   
   	  
   <c:if test="${PayrollssList.isCrewUsed==false}">
   		  <display:column title="Select" style="text-align: center" ><input type="checkbox" name="check1" id="${PayrollssList.id}" onclick="return checkAll(this),showMMMessage(this,'${PayrollssList.integrationTool}','${PayrollssList.deviceNumber}'),requestedWorkCrews(this,'f');"/></display:column>
  	   	  <configByCorp:fieldVisibility componentId="component.field.scheduleResource.crew">
  	   	  <display:column title="Crew" maxLength="6" style="color:black;width:10px;"><c:out value="${PayrollssList.userName}" /></display:column>
  	      </configByCorp:fieldVisibility>
  	      <display:column title="Crew Name" style="color:black">
  	      	<c:out value="${PayrollssList.lastName}" />,&nbsp;<c:out value="${PayrollssList.firstName}" />
  	     	<c:if test="${PayrollssList.integrationTool=='MM' }">
   		 		<img id="mmIntegration" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/MM_12.png"  width="23" height="9"/>
   		 	</c:if>
  	      </display:column>
  	  <%--    <configByCorp:fieldVisibility componentId="component.field.scheduleResource.crewTypeAndClass"> --%>
	  	      <display:column title="Crew&nbsp;Type" style="color:black">
	  	      	<c:out value="${PayrollssList.typeofWork}" />  	     	
	  	      </display:column>
  	   <%--   </configByCorp:fieldVisibility>--%>
   </c:if>
   
   <c:if test="${PayrollssList.isCrewUsed==true}">
  		 <display:column title="Select" style="text-align: center" ><input type="checkbox" name="check1"  id="${PayrollssList.id}" onclick="return checkAll(this),showMMMessage(this,'${PayrollssList.integrationTool}','${PayrollssList.deviceNumber}'),requestedWorkCrews(this,'t');"/></display:column>
   		 <configByCorp:fieldVisibility componentId="component.field.scheduleResource.crew">
   		 <display:column title="Crew" maxLength="6" style="color:red;width:10px;"><c:out value="${PayrollssList.userName}" /></display:column>
   		 </configByCorp:fieldVisibility>
   		 <display:column title="Crew Name" style="color:red">
   		 	<c:out value="${PayrollssList.lastName}" />,&nbsp;<c:out value="${PayrollssList.firstName}" />
   		 	<c:if test="${PayrollssList.integrationTool=='MM' }">
   		 		<img id="mmIntegration" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/MM_12.png" width="23" height="9" />
   		 	</c:if>
   		 </display:column>
   	<%--  <configByCorp:fieldVisibility componentId="component.field.scheduleResource.crewTypeAndClass">--%>	
	  	      <display:column title="Crew&nbsp;Type" style="color:red">
	  	      	<c:out value="${PayrollssList.typeofWork}" />  	     	
	  	      </display:column>
  	   <%--  </configByCorp:fieldVisibility>--%>
  </c:if>

  
</display:table>  
</div>
<div id="ResorceCenDiv" style="height:250px; float:left; margin-top:0px; margin-left:1%; margin-right:1%;"><!-- sandeep -->
<c:set var="resourceListShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForCWMS">
<div style="text-align:center1;margin-bottom:4px;"><span style="margin-left:503px;">${crewRecords} Requested</span>
 <span  style="margin-left:42px;">${vehicleRecords} Requested</span></div>
</configByCorp:fieldVisibility> 
<div id="newmnav" style="width:700px;overflow-x:auto;margin-left:5px;">
		<ul>			
		<li><a class=""><span>Work Ticket List</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForCWMS">
		  <li><a onclick="return findResourceList()" id="resourceListBtn"><span>Resource List</span></a></li>		  
		  </configByCorp:fieldVisibility>
		  <li style="background:none;"><dt class="btbg1-small"><b>&nbsp;<font color="red"> ${selectedWorkTicket } </font>&nbsp;</b></dt></li>
		  <li style="background:none;"><dt class="btbg1-middle" id="hideSelectedBtn" style="cursor: pointer;" onclick="return hideSelectedTicket()"><b>Hide Selected</b></dt></li>
		  <li style="background:none;"><dt class="btbg1-small" id="showAllBtn" style="cursor: pointer;" onclick="ShowAll()"><b>&nbsp;Show All&nbsp;</b></dt></li>			 
		  <li style="background:none;padding-top:3px" class="listwhitetext">Crew</li>
		  <li style="background:none;"><dt class="btbg1-small"><b>&nbsp;<font color="red">${selectedAvailableCrew }</font>&nbsp;</b></dt></li>
		  <li style="background:none;padding-top:3px" class="listwhitetext">Truck</li>
		  <li style="background:none;"><dt class="btbg1-small"><b>&nbsp;<font color="red">${selectedAvailableTrucks}</font>&nbsp;</b></dt></li>
		</ul>
	</div>

		<configByCorp:fieldVisibility componentId="component.field.Alternative.hideOnlyForCWMS">
	<div class="spn">&nbsp;</div>
	<s:set name="workTicketsList" value="workTicketsList" scope="request" />
		<display:table name="workTicketsList" class="table" requestURI="searchCrewTickets.html" id="workTicketList" defaultsort="1" style="width:100%;!margin-top:3px">	
		<display:column title="Select" style="width:10%"><input type="checkbox" name="check2" id="${workTicketList.id}"  onclick="return checkAll(this),requestedWorkTickets(this);"/></display:column>
   		<c:if test="${workTicketList.isCrewUsed==true}">
	   		<display:column sortable="true" sortProperty="ticket" style="text-align:right;width:10%;color:red;" titleKey="workTicket.ticket">
	   			<a onclick="openTicket('${workTicketList.id}')"><c:out value="${workTicketList.ticket}"></c:out></a>
	   			<c:if test= "${mmValidation =='Yes' }" >
	   			<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${workTicketList.id}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
	   			</c:if>
	   			<img align="top" title="Forms" onclick="findWorkTicketForms('${workTicketList.ticket}','${workTicketList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/>
	   		</display:column>
	   		<c:if test="${pageCorpID != 'VOER' and pageCorpID != 'VORU' and pageCorpID != 'VOCZ' and pageCorpID != 'VOAO' and pageCorpID != 'BONG'}">
				<display:column title="Status" maxLength="12" style="text-align:right;width:10%;color:red;" ><c:out value="${workTicketList.targetActual}" /></display:column>
			</c:if>
			<display:column title="Note" sortable="true" style="text-align: center;width:10%;color:red;" maxLength="50">
				<c:if test="${workTicketList.notesID==''}">
					<img id="open${workTicketList.id}"src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${workTicketList.notesID!=''}">
					<img id="close${workTicketList.id}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
			</display:column>
			<display:column title="Shipper" style="width:35%;color:red" maxLength="12"><c:out value="${workTicketList.lastName}" /></display:column>
			<display:column title="Job" style="width:35%;color:red"><c:out value="${workTicketList.jobType}" /></display:column>		    
			<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForSSCW">
			<display:column title="Weight" style="width:20%;color:red">
				<c:if test="${workTicketList.actualWeight!=null && workTicketList.actualWeight!='' && workTicketList.actualWeight!='0.0'}">	
					<c:out value="${workTicketList.actualWeight}" />(A)&nbsp;${workTicketList.unit1}
			    </c:if>
			    <c:if test="${workTicketList.actualWeight==null || workTicketList.actualWeight=='' || workTicketList.actualWeight=='0.0'}">	
				   <c:out value="${workTicketList.estimatedWeight}" />(E)&nbsp;${workTicketList.unit1}
			    </c:if>
			</display:column>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForOthers">
			<display:column title="Volume" style="width:20%;color:red">
				<c:if test="${workTicketList.actualVolume!=null && workTicketList.actualVolume!='' && workTicketList.actualVolume!='0.0'}">	
					<c:out value="${workTicketList.actualVolume}" />(A)&nbsp;${workTicketList.unit2}
			    </c:if>
			    <c:if test="${workTicketList.actualVolume==null || workTicketList.actualVolume=='' || workTicketList.actualVolume=='0.0'}">	
				   <c:out value="${workTicketList.estimatedCubicFeet}" />(E)&nbsp;${workTicketList.unit2}
			    </c:if>
			</display:column>
			</configByCorp:fieldVisibility>
			<display:column titleKey="workTicket.service" style="width:50%;color:red">${workTicketList.service}</display:column>
			<display:column property="date1" title="Begin" style="width:10%;color:red" format="{0,date,dd-MMM-yy}" ></display:column>
			<display:column title="Wkd" style="width:10%;color:red">
				<c:if test="${workTicketList.weekendFlag==true}">
					<img src="${pageContext.request.contextPath}/images/tick01.gif" />
				</c:if>
				<c:if test="${workTicketList.weekendFlag==false }">
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
				</c:if>
			</display:column>
			<display:column property="date2" title="End" style="width:10%;color:red" format="{0,date,dd-MMM-yy}" ></display:column>			
			<display:column  title="Origin" style="width:10%;color:red;">			
		<span align="left" onmouseover="findToolTipService1('${workTicketList.id}','${workTicketList.city}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.city}....." /></span>
				</display:column>			
			<display:column  title="Dest" style="width:10%;color:red;">			 
       <span align="left" onmouseover="findToolTipDestCityService1('${workTicketList.id}','${workTicketList.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.destinationCity}....." /></span> 
        </display:column>
			<display:column title="ShipNumber" style="width:10%;color:red">
			<div align="left" onmouseover="findToolTipCoordinatorSO('${workTicketList.shipNumber}',this);" onmouseout="ajax_hideTooltip();" >
			<c:out value="${workTicketList.shipNumber}" /></div>
		</display:column>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForICMG">
		<display:column title="&nbsp;Crew&nbsp;&nbsp;" style="width:10%;color:red;" >
				<c:if test="${workTicketList.cQty!='' && workTicketList.cQty!=null && workTicketList.cQty!='0' && workTicketList.cQty!='0.0' && workTicketList.cQty!='0.00'}" >
					 <c:out value="${workTicketList.cQty}"></c:out><img align="right" title="Forms" onclick="findToolTipResource('${workTicketList.ticket}','C',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>			
			<display:column sortable="true" title="Parking" style="width:10%;color:red">
		<c:choose>
			<c:when test="${workTicketList.parkingS!=null && workTicketList.parkingS!='' &&( workTicketList.parkingD==null || workTicketList.parkingD=='')}">	
				<c:out value="${workTicketList.parkingS}" />(O)
			</c:when>
			<c:when test="${(workTicketList.parkingS==null || workTicketList.parkingS=='') && workTicketList.parkingD!=null && workTicketList.parkingD!=''}">	
				<c:out value="${workTicketList.parkingD}" />(D)
			</c:when>
			<c:when test="${(workTicketList.parkingS!=null && workTicketList.parkingS!='') && (workTicketList.parkingD!=null && workTicketList.parkingD!='')}">	
				<c:out value="${workTicketList.parkingS}" />(O)</br>${workTicketList.parkingD}(D)
			</c:when>
			<c:when test="${workTicketList.parkingS==null || workTicketList.parkingS=='' || workTicketList.parkingD==null || workTicketList.parkingD==''}">	
				<c:out value="${workTicketList.parkingS}" />&nbsp;${workTicketList.parkingD}
			</c:when>
			<c:otherwise>				
			</c:otherwise>
		</c:choose>
		</display:column>
		<display:column sortable="true" title="Shuttle" style="width:10%;color:red">
		<c:choose>
			<c:when test="${workTicketList.farInMeters!=null && workTicketList.farInMeters!='' &&( workTicketList.dest_farInMeters==null || workTicketList.dest_farInMeters=='')}">	
				<c:out value="${workTicketList.farInMeters}" />(O)
			</c:when>
			<c:when test="${(workTicketList.farInMeters==null || workTicketList.farInMeters=='') && workTicketList.dest_farInMeters!=null && workTicketList.dest_farInMeters!=''}">	
				<c:out value="${workTicketList.dest_farInMeters}" />(D)
			</c:when>
			<c:when test="${(workTicketList.farInMeters!=null && workTicketList.farInMeters!='') && (workTicketList.dest_farInMeters!=null && workTicketList.dest_farInMeters!='')}">	
				<c:out value="${workTicketList.farInMeters}" />(O)</br>${workTicketList.dest_farInMeters}(D)
			</c:when>
			<c:when test="${workTicketList.farInMeters==null || workTicketList.farInMeters=='' || workTicketList.dest_farInMeters==null || workTicketList.dest_farInMeters==''}">	
				<c:out value="${workTicketList.farInMeters}" />&nbsp;${workTicketList.dest_farInMeters}
			</c:when>
			<c:otherwise>				
			</c:otherwise>
		</c:choose>
		</display:column>
		<display:column sortable="true" title="Which Floor" style="width:10%;color:red">
			<c:choose>
				<c:when test="${workTicketList.origMeters!=null && workTicketList.origMeters!='' &&( workTicketList.destMeters==null || workTicketList.destMeters=='')}">	
					<c:out value="${workTicketList.origMeters}" />(O)
				</c:when>
				<c:when test="${(workTicketList.origMeters==null || workTicketList.origMeters=='') && workTicketList.destMeters!=null && workTicketList.destMeters!=''}">	
					<c:out value="${workTicketList.destMeters}" />(D)
				</c:when>
				<c:when test="${(workTicketList.origMeters!=null && workTicketList.origMeters!='') && (workTicketList.destMeters!=null && workTicketList.destMeters!='')}">	
					<c:out value="${workTicketList.origMeters}" />(O)</br>${workTicketList.destMeters}(D)
				</c:when>
				<c:when test="${workTicketList.origMeters==null || workTicketList.origMeters=='' || workTicketList.destMeters==null || workTicketList.destMeters==''}">	
					<c:out value="${workTicketList.origMeters}" />&nbsp;${workTicketList.destMeters}
				</c:when>
				<c:otherwise>				
				</c:otherwise>
			</c:choose>
		</display:column>
			<display:column title="Arrival Time at sight" style="width:35%;color:red"><c:out value="${workTicketList.scheduledArrive}" /></display:column>	
   		</configByCorp:fieldVisibility>
   		</c:if>
   		<c:if test="${workTicketList.isCrewUsed==false}">
   			<display:column sortProperty="ticket" style="text-align:right;width:10%;color:black" titleKey="workTicket.ticket"><a onclick="openTicket('${workTicketList.id}')"><c:out value="${workTicketList.ticket}"></c:out></a>
				<c:if test= "${mmValidation =='Yes' }" >
	   				<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${workTicketList.id}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
	   			</c:if>
			<img align="top" title="Forms" onclick="findWorkTicketForms('${workTicketList.ticket}','${workTicketList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></display:column>
   			<c:if test="${pageCorpID != 'VOER' and pageCorpID != 'VORU' and pageCorpID != 'VOCZ' and pageCorpID != 'VOAO' and pageCorpID != 'BONG'}">
		 	  <display:column title="Status" maxLength="12" style="text-align:right;width:10%;color:black" ><c:out value="${workTicketList.targetActual}" /></display:column>
		 	</c:if>
		    <display:column title="Note" style="text-align: center;width:10%;color:black;" maxLength="50">
				<c:if test="${workTicketList.notesID==''}">
					<img id="open${workTicketList.id}"src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${workTicketList.notesID!=''}">
					<img id="close${workTicketList.id}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
			</display:column>
			<display:column title="Shipper" style="width:35%;color:black" maxLength="12"><c:out value="${workTicketList.lastName}" /></display:column>
			<display:column title="Job" style="width:35%;color:black"><c:out value="${workTicketList.jobType}" /></display:column>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForSSCW">
			<display:column title="Weight" style="width:20%;color:black">
				<c:if test="${workTicketList.actualWeight!=null && workTicketList.actualWeight!='' && workTicketList.actualWeight!='0.0'}">	
					<c:out value="${workTicketList.actualWeight}" />(A)&nbsp;${workTicketList.unit1}
			    </c:if>
			    <c:if test="${workTicketList.actualWeight==null || workTicketList.actualWeight=='' || workTicketList.actualWeight=='0.0'}">	
				   <c:out value="${workTicketList.estimatedWeight}" />(E)&nbsp;${workTicketList.unit1}
			    </c:if>
			</display:column>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForOthers">
			<display:column title="Volume" style="width:20%;color:black">
				<c:if test="${workTicketList.actualVolume!=null && workTicketList.actualVolume!='' && workTicketList.actualVolume!='0.0'}">	
					<c:out value="${workTicketList.actualVolume}" />(A)&nbsp;${workTicketList.unit2}
			    </c:if>
			    <c:if test="${workTicketList.actualVolume==null || workTicketList.actualVolume=='' || workTicketList.actualVolume=='0.0'}">	
				   <c:out value="${workTicketList.estimatedCubicFeet}" />(E)&nbsp;${workTicketList.unit2}
			    </c:if>
			</display:column>
			</configByCorp:fieldVisibility>
			<display:column titleKey="workTicket.service" style="width:50%;color:black">${workTicketList.service}</display:column>
			<display:column property="date1" title="Begin" style="width:10%;color:black" format="{0,date,dd-MMM-yy}" ></display:column>
			<display:column title="Wkd" style="width:10%;color:black">
				<c:if test="${workTicketList.weekendFlag==true}">
					<img src="${pageContext.request.contextPath}/images/tick01.gif" />
				</c:if>
				<c:if test="${workTicketList.weekendFlag==false }">
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
				</c:if>
			</display:column>
			<display:column property="date2" title="End" style="width:10%;color:black" format="{0,date,dd-MMM-yy}" ></display:column>			
			<display:column  title="Origin" style="width:10%;color:black;">		
		<span align="left" onmouseover="findToolTipService1('${workTicketList.id}','${workTicketList.city}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.city}....." /></span>
				</display:column>			
			<display:column  title="Dest" style="width:10%;color:black;">			 
       <span align="left" onmouseover="findToolTipDestCityService1('${workTicketList.id}','${workTicketList.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.destinationCity}....." /></span> 
        </display:column>
		<display:column title="ShipNumber" style="width:10%;color:black">
			<div align="left" onmouseover="findToolTipCoordinatorSO('${workTicketList.shipNumber}',this);" onmouseout="ajax_hideTooltip();" >
			<c:out value="${workTicketList.shipNumber}" /></div>
		</display:column>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForICMG">	
		<display:column title="&nbsp;Crew&nbsp;&nbsp;" style="width:10%;color:red;" >
				<c:if test="${workTicketList.cQty!='' && workTicketList.cQty!=null && workTicketList.cQty!='0' && workTicketList.cQty!='0.0' && workTicketList.cQty!='0.00'}" >
					 <c:out value="${workTicketList.cQty}"></c:out><img align="right" title="Forms" onclick="findToolTipResource('${workTicketList.ticket}','C',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>		
		<display:column sortable="true" title="Parking" style="width:10%;color:black">
		<c:choose>
			<c:when test="${workTicketList.parkingS!=null && workTicketList.parkingS!='' &&( workTicketList.parkingD==null || workTicketList.parkingD=='')}">	
				<c:out value="${workTicketList.parkingS}" />(O)
			</c:when>
			<c:when test="${(workTicketList.parkingS==null || workTicketList.parkingS=='') && workTicketList.parkingD!=null && workTicketList.parkingD!=''}">	
				<c:out value="${workTicketList.parkingD}" />(D)
			</c:when>
			<c:when test="${(workTicketList.parkingS!=null && workTicketList.parkingS!='') && (workTicketList.parkingD!=null && workTicketList.parkingD!='')}">	
				<c:out value="${workTicketList.parkingS}" />(O)</br>${workTicketList.parkingD}(D)
			</c:when>
			<c:when test="${workTicketList.parkingS==null || workTicketList.parkingS=='' || workTicketList.parkingD==null || workTicketList.parkingD==''}">	
				<c:out value="${workTicketList.parkingS}" />&nbsp;${workTicketList.parkingD}
			</c:when>
			<c:otherwise>				
			</c:otherwise>
		</c:choose>
		</display:column>
		<display:column sortable="true" title="Shuttle" style="width:10%;color:black">
		<c:choose>
			<c:when test="${workTicketList.farInMeters!=null && workTicketList.farInMeters!='' &&( workTicketList.dest_farInMeters==null || workTicketList.dest_farInMeters=='')}">	
				<c:out value="${workTicketList.farInMeters}" />(O)
			</c:when>
			<c:when test="${(workTicketList.farInMeters==null || workTicketList.farInMeters=='') && workTicketList.dest_farInMeters!=null && workTicketList.dest_farInMeters!=''}">	
				<c:out value="${workTicketList.dest_farInMeters}" />(D)
			</c:when>
			<c:when test="${(workTicketList.farInMeters!=null && workTicketList.farInMeters!='') && (workTicketList.dest_farInMeters!=null && workTicketList.dest_farInMeters!='')}">	
				<c:out value="${workTicketList.farInMeters}" />(O)</br>${workTicketList.dest_farInMeters}(D)
			</c:when>
			<c:when test="${workTicketList.farInMeters==null || workTicketList.farInMeters=='' || workTicketList.dest_farInMeters==null || workTicketList.dest_farInMeters==''}">	
				<c:out value="${workTicketList.farInMeters}" />&nbsp;${workTicketList.dest_farInMeters}
			</c:when>
			<c:otherwise>				
			</c:otherwise>
		</c:choose>
		</display:column>
		<display:column sortable="true" title="Which Floor" style="width:10%;color:black">
			<c:choose>
				<c:when test="${workTicketList.origMeters!=null && workTicketList.origMeters!='' &&( workTicketList.destMeters==null || workTicketList.destMeters=='')}">	
					<c:out value="${workTicketList.origMeters}" />(O)
				</c:when>
				<c:when test="${(workTicketList.origMeters==null || workTicketList.origMeters=='') && workTicketList.destMeters!=null && workTicketList.destMeters!=''}">	
					<c:out value="${workTicketList.destMeters}" />(D)
				</c:when>
				<c:when test="${(workTicketList.origMeters!=null && workTicketList.origMeters!='') && (workTicketList.destMeters!=null && workTicketList.destMeters!='')}">	
					<c:out value="${workTicketList.origMeters}" />(O)</br>${workTicketList.destMeters}(D)
				</c:when>
				<c:when test="${workTicketList.origMeters==null || workTicketList.origMeters=='' || workTicketList.destMeters==null || workTicketList.destMeters==''}">	
					<c:out value="${workTicketList.origMeters}" />&nbsp;${workTicketList.destMeters}
				</c:when>
				<c:otherwise>				
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="Arrival Time at sight" style="width:35%;color:black"><c:out value="${workTicketList.scheduledArrive}" /></display:column>
		</configByCorp:fieldVisibility>		
   		</c:if>
   	</display:table>   
   	</configByCorp:fieldVisibility>
   	<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForCWMS">
   	<div class="spn">&nbsp;</div>
	<s:set name="workTicketsList" value="workTicketsList" scope="request" />
	<display:table name="workTicketsList" class="table" requestURI="searchCrewTickets.html" id="workTicketList" defaultsort="1" style="width:100%;!margin-top:3px">	
		<display:column title="Select" style="width:10%"><input type="checkbox" name="check2" id="${workTicketList.id}"  onclick="return checkAll(this),requestedWorkTickets(this);"/></display:column>
   		<c:if test="${workTicketList.isCrewUsed==true}">
	   		<display:column sortable="true" sortProperty="ticket" style="text-align:right;width:10%;color:red;" titleKey="workTicket.ticket">
	   			<a onclick="openTicket('${workTicketList.id}')"><c:out value="${workTicketList.ticket}"></c:out></a>
	   			<c:if test= "${mmValidation =='Yes' }" >
	   				<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${workTicketList.id}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
	   			</c:if>
	   			<img align="top" title="Forms" onclick="findWorkTicketForms('${workTicketList.ticket}','${workTicketList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/>
	   		</display:column>
			<display:column title="Status" maxLength="12" style="text-align:right;width:10%;color:red;" ><c:out value="${workTicketList.targetActual}" /></display:column>
			<display:column title="Note" sortable="true" style="text-align: center;width:10%;color:red;" maxLength="50">
				<c:if test="${workTicketList.notesID==''}">
					<img id="open${workTicketList.id}"src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${workTicketList.notesID!=''}">
					<img id="close${workTicketList.id}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
			</display:column>
			<display:column title="Customer Name" style="width:25%;color:red;" maxLength="12"><c:out value="${workTicketList.lastName}" /></display:column>
			<display:column titleKey="workTicket.service" style="width:10%;color:red;">${workTicketList.service}</display:column>
			<display:column property="date1" title="Begin" style="width:10%;color:red;" format="{0,date,dd-MMM-yy}" ></display:column>
			<display:column title="Wkd" style="width:10%;color:red;">
				<c:if test="${workTicketList.weekendFlag==true}">
					<img src="${pageContext.request.contextPath}/images/tick01.gif" />
				</c:if>
				<c:if test="${workTicketList.weekendFlag==false }">
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
				</c:if>
			</display:column>
			<display:column property="date2" title="End" style="width:10%;color:red;" format="{0,date,dd-MMM-yy}" ></display:column>			
			<display:column title="&nbsp;Crew&nbsp;&nbsp;" style="width:10%;color:red;" >
				<c:if test="${workTicketList.cQty!='' && workTicketList.cQty!=null && workTicketList.cQty!='0' && workTicketList.cQty!='0.0' && workTicketList.cQty!='0.00'}" >
					 <c:out value="${workTicketList.cQty}"></c:out><img align="right" title="Crew" onclick="findToolTipResource('${workTicketList.ticket}','C',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>		
			<display:column title="Vehicle" style="width:10%;color:red;" >
				<c:if test="${workTicketList.vQty!='' && workTicketList.vQty!=null && workTicketList.vQty!='0' && workTicketList.vQty!='0.0' && workTicketList.vQty!='0.00'}">
					 <c:out value="${workTicketList.vQty}"></c:out><img align="right" title="Vehicle" onclick="findToolTipResource('${workTicketList.ticket}','V',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>	
			<display:column title="Equipment" style="width:10%;color:red;" >
				<c:if test="${workTicketList.eQty!='' && workTicketList.eQty!=null && workTicketList.eQty!='0' && workTicketList.eQty!='0.0' && workTicketList.eQty!='0.00'}">
					 <c:out value="${workTicketList.eQty}"></c:out><img align="right" title="Equipment" onclick="findToolTipResource('${workTicketList.ticket}','E',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>			          	
			<display:column title="Material" style="width:10%;color:red;" >
				<c:if test="${workTicketList.mQty!='' && workTicketList.mQty!=null && workTicketList.mQty!='0' && workTicketList.mQty!='0.0' && workTicketList.mQty!='0.00'}">
					<c:out value="${workTicketList.mQty}"></c:out><img align="right" title="Material" onclick="findToolTipResource('${workTicketList.ticket}','M',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>
			<display:column title="Weight" style="width:20%;color:red;">
				<c:if test="${workTicketList.actualWeight!=null && workTicketList.actualWeight!='' && workTicketList.actualWeight!='0.0'}">	
					<c:out value="${workTicketList.actualWeight}" />&nbsp;(A)&nbsp;${workTicketList.unit1}
			    </c:if>
			    <c:if test="${workTicketList.actualWeight==null || workTicketList.actualWeight=='' || workTicketList.actualWeight=='0.0'}">	
				   <c:out value="${workTicketList.estimatedWeight}" />&nbsp;(E)&nbsp;${workTicketList.unit1}
			    </c:if>
			</display:column>	
			<display:column title="Bill To" style="width:20%;">
				<c:out value="${workTicketList.billToName}" />
			</display:column>    
			<display:column title="Volume" style="width:20%;color:red;">
				<c:if test="${workTicketList.actualVolume!=null && workTicketList.actualVolume!='' && workTicketList.actualVolume!='0.0'}">	
					<c:out value="${workTicketList.actualVolume}" />(A)&nbsp;${workTicketList.unit2}
			    </c:if>
			    <c:if test="${workTicketList.actualVolume==null || workTicketList.actualVolume=='' || workTicketList.actualVolume=='0.0'}">	
				   <c:out value="${workTicketList.estimatedCubicFeet}" />(E)&nbsp;${workTicketList.unit2}
			    </c:if>
			</display:column>
			<display:column title="Time" style="width:10%;color:red;">
			<c:if test="${workTicketList.resourceReqdAm==true && workTicketList.resourceReqdPm==true}">
			<c:out value="Full Day" /></c:if>
			<c:if test="${workTicketList.resourceReqdAm==true && workTicketList.resourceReqdPm==false}">
			<c:out value="Morning" /></c:if>
			<c:if test="${workTicketList.resourceReqdAm==false && workTicketList.resourceReqdPm==true}">
			<c:out value="AfterNoon" /></c:if>		
			</display:column>			
			<display:column title="Job" style="width:5%;color:red;"><c:out value="${workTicketList.jobType}" /></display:column>			
			<display:column title="ShipNumber" style="width:10%;color:red">
			<div align="left" onmouseover="findToolTipCoordinatorSO('${workTicketList.shipNumber}',this);" onmouseout="ajax_hideTooltip();" >
				<c:out value="${workTicketList.shipNumber}" /></div>
			</display:column>
			
			<display:column property="state" title="Origin State" style="width:10%;color:red;"></display:column>			
			<display:column  title="Origin City" style="width:10%;color:red;">			
			<span align="left" onmouseover="findToolTipService1('${workTicketList.id}','${workTicketList.city}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.city}....." /></span>
			</display:column>
			<display:column property="phone" title="Origin Phone" style="width:10%;color:red;"></display:column>
			<display:column property="destinationState" title="Destination State" style="width:10%;color:red;"></display:column>			
		
			<display:column  title="Destination City" style="width:10%;color:red;">			 
	       		<span align="left" onmouseover="findToolTipDestCityService1('${workTicketList.id}','${workTicketList.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.destinationCity}....." /></span> 
	        </display:column>
			<display:column property="destinationPhone" title="Destination Phone" style="width:10%;color:red;"></display:column>
			<display:column property="registrationNumber" title="Registration Number" style="width:10%;color:red;"></display:column>		
   			</c:if>
   		<c:if test="${workTicketList.isCrewUsed==false}">
   			<display:column sortable="true" sortProperty="ticket" style="text-align:right;width:10%;color:black;" titleKey="workTicket.ticket"><a onclick="openTicket('${workTicketList.id}')"><c:out value="${workTicketList.ticket}"></c:out></a>
   			<c:if test= "${mmValidation =='Yes' }" >
	   				<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${workTicketList.id}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
	   			</c:if>
   			<img align="top" title="Forms" onclick="findWorkTicketForms('${workTicketList.ticket}','${workTicketList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></display:column>
			<display:column title="Status" maxLength="12" style="text-align:right;width:10%;color:black;" ><c:out value="${workTicketList.targetActual}" /></display:column>
			<display:column title="Note" sortable="true" style="text-align: center;width:10%;color:black;" maxLength="50">
				<c:if test="${workTicketList.notesID==''}">
					<img id="open${workTicketList.id}"src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${workTicketList.notesID!=''}">
					<img id="close${workTicketList.id}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${workTicketList.id }&notesId=${workTicketList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
			</display:column>
			<display:column title="Customer Name" style="width:25%;color:black;" maxLength="12"><c:out value="${workTicketList.lastName}" /></display:column>
			<display:column titleKey="workTicket.service" style="width:10%;color:black;">${workTicketList.service}</display:column>
			<display:column property="date1" title="Begin" style="width:10%;color:black;" format="{0,date,dd-MMM-yy}" ></display:column>
			<display:column title="Wkd" style="width:10%;color:black;">
				<c:if test="${workTicketList.weekendFlag==true}">
					<img src="${pageContext.request.contextPath}/images/tick01.gif" />
				</c:if>
				<c:if test="${workTicketList.weekendFlag==false }">
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
				</c:if>
			</display:column>
			<display:column property="date2" title="End" style="width:10%;color:black;" format="{0,date,dd-MMM-yy}" ></display:column>			
			<display:column title="&nbsp;Crew&nbsp;&nbsp;" style="width:10%;color:black;" >
				<c:if test="${workTicketList.cQty!='' && workTicketList.cQty!=null && workTicketList.cQty!='0' && workTicketList.cQty!='0.0' && workTicketList.cQty!='0.00'}" >
					 <c:out value="${workTicketList.cQty}"></c:out><img align="right" title="Crew" onclick="findToolTipResource('${workTicketList.ticket}','C',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>		
			<display:column title="Vehicle" style="width:10%;color:black;" >
				<c:if test="${workTicketList.vQty!='' && workTicketList.vQty!=null && workTicketList.vQty!='0' && workTicketList.vQty!='0.0' && workTicketList.vQty!='0.00'}">
					 <c:out value="${workTicketList.vQty}"></c:out><img align="right" title="Vehicle" onclick="findToolTipResource('${workTicketList.ticket}','V',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>	
			<display:column title="Equipment" style="width:10%;color:black;" >
				<c:if test="${workTicketList.eQty!='' && workTicketList.eQty!=null && workTicketList.eQty!='0' && workTicketList.eQty!='0.0' && workTicketList.eQty!='0.00'}">
					 <c:out value="${workTicketList.eQty}"></c:out><img align="right" title="Equipment" onclick="findToolTipResource('${workTicketList.ticket}','E',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>			          	
			<display:column title="Material" style="width:10%;color:black;" >
				<c:if test="${workTicketList.mQty!='' && workTicketList.mQty!=null && workTicketList.mQty!='0' && workTicketList.mQty!='0.0' && workTicketList.mQty!='0.00'}">
					<c:out value="${workTicketList.mQty}"></c:out><img align="right" title="Material" onclick="findToolTipResource('${workTicketList.ticket}','M',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>		 
				</c:if>
			</display:column>	
			<display:column title="Weight" style="width:20%;color:black;">
				<c:if test="${workTicketList.actualWeight!=null && workTicketList.actualWeight!='' && workTicketList.actualWeight!='0.0'}">	
					<c:out value="${workTicketList.actualWeight}" />&nbsp;(A)&nbsp;${workTicketList.unit1}
			    </c:if>
			    <c:if test="${workTicketList.actualWeight==null || workTicketList.actualWeight=='' || workTicketList.actualWeight=='0.0'}">	
				   <c:out value="${workTicketList.estimatedWeight}" />&nbsp;(E)&nbsp;${workTicketList.unit1}
			    </c:if>
			</display:column>	
			<display:column title="Bill To" style="width:20%;">
				<c:out value="${workTicketList.billToName}" />
			</display:column>		    
			<display:column title="Volume" style="width:20%;color:black;">
				<c:if test="${workTicketList.actualVolume!=null && workTicketList.actualVolume!='' && workTicketList.actualVolume!='0.0'}">	
					<c:out value="${workTicketList.actualVolume}" />(A)&nbsp;${workTicketList.unit2}
			    </c:if>
			    <c:if test="${workTicketList.actualVolume==null || workTicketList.actualVolume=='' || workTicketList.actualVolume=='0.0'}">	
				   <c:out value="${workTicketList.estimatedCubicFeet}" />(E)&nbsp;${workTicketList.unit2}
			    </c:if>
			</display:column>
			<display:column title="Time" style="width:10%;color:black;">
			<c:if test="${workTicketList.resourceReqdAm==true && workTicketList.resourceReqdPm==true}">
			<c:out value="Full Day" /></c:if>
			<c:if test="${workTicketList.resourceReqdAm==true && workTicketList.resourceReqdPm==false}">
			<c:out value="Morning" /></c:if>
			<c:if test="${workTicketList.resourceReqdAm==false && workTicketList.resourceReqdPm==true}">
			<c:out value="AfterNoon" /></c:if>	
			</display:column>			           	
			<display:column title="Job" style="width:5%;color:black;"><c:out value="${workTicketList.jobType}" /></display:column>			
			<display:column title="ShipNumber" style="width:10%;color:black">
			<div align="left" onmouseover="findToolTipCoordinatorSO('${workTicketList.shipNumber}',this);" onmouseout="ajax_hideTooltip();" >
				<c:out value="${workTicketList.shipNumber}" /></div>
			</display:column>
			
			<display:column property="state" title="Origin State" style="width:10%;color:black;"></display:column>			
			<display:column  title="Origin City" style="width:10%;color:black;">			
		<span align="left" onmouseover="findToolTipService1('${workTicketList.id}','${workTicketList.city}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.city}....." /></span>
				</display:column>       
			<display:column property="phone" title="Origin Phone" style="width:10%;color:black;"></display:column>
			<display:column property="destinationState" title="Destination State" style="width:10%;color:black;"></display:column>			
			<display:column  title="Destination City" style="width:10%;color:black;">			 
       <span align="left" onmouseover="findToolTipDestCityService1('${workTicketList.id}','${workTicketList.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.destinationCity}....." /></span> 
        </display:column>
			<display:column property="destinationPhone" title="Destination Phone" style="width:10%;color:black;"></display:column>	
			<display:column title="Reg#" style="width:10%;color:black;"><c:out value="${workTicketList.registrationNumber}"></c:out></display:column>	
   		</c:if>
   	</display:table>	
</configByCorp:fieldVisibility>
</div>
<div style="width:20%;height:250px;overflow-x:auto;float:left; !margin-top:0px;"><!-- sandeep -->

	<div id="otabs" style="margin-left:5px;">
		  <ul>
		    <li><a class="current"><span>Trucks List</span></a></li>
		    <!--<li class="listwhitetext"  style="background:url(images/countbg.gif) no-repeat scroll 0 0;font-size:12px;padding-top:3px;text-align:center;width:79px;height:24px;"><b>&nbsp;<font color="red">${selectedAvailableTrucks}</font>&nbsp;</b></li>
		  	<li style="background:none;"><dt id="btbg-small"><b>&nbsp;<font color="red">${selectedAvailableTrucks}</font>&nbsp;</b></dt></li>
		  -->
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>${availableTrucksList.inspectionDueDiff}
	<s:set name="availableTrucks" value="availableTrucks" scope="request" />
	<display:table name="availableTrucks" class="table" requestURI="searchScheduleResouces.html" id="availableTrucksList" defaultsort="2" style="width:100%;margin-top: -19px; !margin-top:0px">
		<display:column title="Select" style="text-align:center; width:1%;"><input type="checkbox" name="check3" id="${availableTrucksList.id}"  onclick="return checkAll(this),requestedTrucksss(this,'${availableTrucksList.inspectionDueDiff}');"/></display:column>
		<c:if test="${availableTrucksList.isTruckUsed==true}">
   		<display:column  style="text-align:left; width:13%; color:red" title="No." >${availableTrucksList.truckNumber}</display:column>
   		<configByCorp:fieldVisibility componentId="component.field.scheduleResource.truck">
		<display:column style="text-align:left; width:50%;color:red" title="Trucks" >${availableTrucksList.description}</display:column>
		</configByCorp:fieldVisibility>
		</c:if>
		<c:if test="${availableTrucksList.inspectionDueDiff <0}">
   		<display:column  style="text-align:left; width:13%; color:red" title="No." >${availableTrucksList.truckNumber}</display:column>
   		<configByCorp:fieldVisibility componentId="component.field.scheduleResource.truck">
		<display:column style="text-align:left; width:50%;" title="Trucks" >${availableTrucksList.description}</display:column>
		</configByCorp:fieldVisibility>
		</c:if>
		
		<c:if test="${availableTrucksList.isTruckUsed==false}">
		<display:column   style="text-align:left; width:13%;" title="No."  >${availableTrucksList.truckNumber}</display:column>
		<configByCorp:fieldVisibility componentId="component.field.scheduleResource.truck">
		<display:column style="text-align:left; width:50%;" title="Trucks" >${availableTrucksList.description}</display:column>
		</configByCorp:fieldVisibility>
		</c:if>
	</display:table>
</div>
</div>
</td>
</tr>
</table>
<table width="77%">
<tr>
<td  style="padding-left:0px; width:20%;">
<input type="button" name="absent" value="Crew Absent" style="width:100px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="return absentsTimeSheet();"/>
<input type="button" name="present" value="Crew Present" style="width:100px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="return presentTimeSheet();"/>
</td>
<td style="width:0px;">
	<c:if test= "${mmValidation =='Yes' }" >
		<div id="mmMessage"></div>
		<!--<input type="button" name="sendToMM" value="Send To Mobile Mover" style="width:200px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="sendToMobileMover();"/>
	--></c:if>
</td>
<td align="left" style="width:61%; text-align:center;"> 
<input type="button" name="assign" value="Assign to tickets" style="width:212px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="buildScheduleResource();"/>

</td >
        <configByCorp:fieldVisibility componentId="component.field.Alternative.showClipboardWithUser">
        <c:choose>
        <c:when test="${countScrachCard == '0'}">
          <td>
          <a href="javascript:;" onclick="displaySheldulerMoodal()" >
          <img id="countForImage" src="<c:url value='/images/clipboard-trans-new.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </td>
         </c:when>
         <c:otherwise>
          <td>
          <a href="javascript:;" onclick="displaySheldulerMoodal()" >
          <img id="countForImage" src="<c:url value='/images/clipnew.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </td>
          </c:otherwise>
          </c:choose>
          </configByCorp:fieldVisibility>
          <configByCorp:fieldVisibility componentId="component.field.Alternative.showClipboardWithoutUser">
       <c:choose>
        <c:when test="${countScrachCard == '0'}">
          <td>
          <a href="javascript:;" onclick="displaySheldulerMoodal()" >
          <img id="countForImage" src="<c:url value='/images/clipboard-trans-new.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </td>
         </c:when>
         <c:otherwise>
          <td>
          <a href="javascript:;" onclick="displaySheldulerMoodal()" >
          <img id="countForImage" src="<c:url value='/images/clipnew.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </td>
         </c:otherwise>
         </c:choose>
         </configByCorp:fieldVisibility>
          

</tr>
</table>         
<table style="width:100%;" border="0">
<tbody>
<tr>
<td align="left">
<div style="width:100%;overflow-x:auto; margin-top:20px" id="ResourceDiv">
	<div id="otabs" style="width:100%;!margin-top:0px;margin-bottom:29px;">
		  <ul>
		    <li><a class="current"><span>Resource Sheet</span></a></li>
		     <!--<li class="listwhitetext"  style="background:url(images/countbg-big.gif) no-repeat scroll 0 0;font-size:12px;padding-top:3px;text-align:center;width:160px;height:24px;"><b># Resource Sheets</b><b>{&nbsp;<font color="red">${countScheduleResources}</font>&nbsp;}</b></li>
		 	--><li style="background:none;"><dt class="btbg1-big" id="resourceSheetBtn"><b># Resource Sheets</b><b>{&nbsp;<font color="red">${countScheduleResources}</font>&nbsp;}</b></dt></li>
		  </ul>
		</div>
		<c:if test="${timeSheets1!='[]' && separateCrewDriverList=='Yes'}">
			  	<div style="text-align: left; margin-top:-28px;">
					<a id="expandAll">
					<img id="show" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP /></a>
					<a id="collapseAll">
					<img id="hide" src="${pageContext.request.contextPath}/images/minus1-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP /></a>
					<font class="listwhitetext"><b>Sort By :</b></font>		
						<s:select cssClass="list-menu pr-f11" id="fieldName" name="fieldName" list="%{fieldNameList}" cssStyle="width:70px" onchange="getCrewTruckList(this)" headerKey="" headerValue=""/>	 
					<font class="listwhitetext"><b>Value</font>		
						<s:select cssClass="list-menu pr-f11" id="fieldValue" name="fieldValue" list="%{fieldList}" cssStyle="width:90px" onchange="getTimeSheetList(this)" headerKey="" headerValue=""/>
					<input type="button" class="pricelistbtn" style="font-size: 11px;height:19px;padding:1px 3px;line-height:10px;border-radius:2px;" name="clear" value="Clear Sorting" onclick="clearSorting();"/>
				</div>
		</c:if>
		<div class="spnblk" style="margin-top:15px">&nbsp;</div>
			
		<c:if test="${timeSheets1!='[]' && separateCrewDriverList=='No'}">
		<display:table name="timeSheets1" class="table" defaultsort="8" requestURI="" id="timeSheetList" style="width:100%;margin-top: -20px;">
		<display:column  sortProperty="ticket" sortable="true" style="text-align:right;width:10%;color:black"  titleKey="workTicket.ticket"><a onclick="openTicket('${timeSheetList.workTicketId}')"><c:out value="${timeSheetList.ticket}"></c:out></a>
		<c:if test= "${mmValidation =='Yes' }" >
	   				<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${timeSheetList.workTicketId}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
	   			</c:if>
		<img align="top" title="Forms" onclick="findWorkTicketForms('${timeSheetList.ticket}','${timeSheetList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></display:column>
		<display:column title="Note" style="text-align: center;width:5%;" maxLength="50">
				<c:if test="${timeSheetList.notesId==''}">
					<img id="open${timeSheetList.workTicketId}" src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${timeSheetList.workTicketId}&notesId=${timeSheetList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${timeSheetList.notesId!=''}">
					<img id="close${timeSheetList.workTicketId}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${timeSheetList.workTicketId}&notesId=${timeSheetList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
		</display:column>
		<display:column sortable="true" property="shipper" titleKey="timeSheet.shipper" />
	    <configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForSSCW">
			<display:column title="Weight" style="width:10%;">
				<c:if test="${timeSheetList.actualWeight!=null && timeSheetList.actualWeight!='' && timeSheetList.actualWeight!='0.0'}">	
					<c:out value="${timeSheetList.actualWeight}" />(A)&nbsp;${timeSheetList.unit1}
			    </c:if>
			    <c:if test="${timeSheetList.actualWeight==null || timeSheetList.actualWeight=='' || timeSheetList.actualWeight=='0.0'}">	
				   <c:out value="${timeSheetList.estimatedWeight}" />(E)&nbsp;${timeSheetList.unit1}
			    </c:if>
			</display:column>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForOthers">
			<display:column title="Volume" style="width:10%;">
			<c:if test="${timeSheetList.actualVolume!=null && timeSheetList.actualVolume!='' && timeSheetList.actualVolume!='0.0'}">	
				<c:out value="${timeSheetList.actualVolume}" />(A)&nbsp;${timeSheetList.unit2}
			</c:if>
			<c:if test="${timeSheetList.actualVolume==null || timeSheetList.actualVolume==''|| timeSheetList.actualVolume=='0.0'}">	
				<c:out value="${timeSheetList.estimatedCubicFeet}" />(E)&nbsp;${timeSheetList.unit2}
			</c:if>
	    	</display:column>
		</configByCorp:fieldVisibility>
		<display:column  sortable="true" title="Service" property="service" ></display:column>
		<display:column sortProperty="crewName" sortable="true" title="Crew Name" style="width: 130px">
			<a onclick="openTicketCrews('${timeSheetList.workTicketId}')"><c:out value="${timeSheetList.crewName}" /></a>
			<c:if test="${timeSheetList.integrationTool=='MM' }">
   		 		<img id="mmIntegration" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/MM_12.png"  width="23" height="9"/>
   		 	</c:if>
		</display:column>
		<display:column title="Driver" style="width:20px; color:#15428B !important;" >
		<c:if test="${timeSheetList.isDriverUsed==true}">
			<div style="text-align:center"><input type="checkbox" name="driverCheck" value="${timeSheetList.id}" onclick="updateDrivers(this);" checked="checked" /></div>
		</c:if>
		<c:if test="${timeSheetList.isDriverUsed==false}">
			<div style="text-align:center"><input type="checkbox" name="driverCheck" value="${timeSheetList.id}" onclick="updateDrivers(this);" /></div>
		</c:if>
		</display:column>
		<display:column sortable="true" title="Truck#" sortProperty="localtruckNumber">
		<a onclick="openTicketTrucks('${timeSheetList.workTicketId}','${timeSheetList.ticket}','${timeSheetList.serviceOrderId}')"><c:out value="${timeSheetList.localtruckNumber}" /></a></display:column>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCWMSAndSSCW">
		<display:column title="ATime" property="aTime" > </display:column>
		<display:column title="DTime" property="dTime" > </display:column>
		</configByCorp:fieldVisibility>
		<display:column  sortProperty="originCity" sortable="true" title="Origin City" >
		<span align="left" onmouseover="findToolTipService('${timeSheetList.ticket}','${timeSheetList.originCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${timeSheetList.originCity}....." /></span>
		</display:column>
        <display:column  sortProperty="destinationCity" sortable="true" title="Destination City">
       <span align="left" onmouseover="findToolTipDestCityService('${timeSheetList.ticket}','${timeSheetList.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${timeSheetList.destinationCity}....." /></span> 
        </display:column>
        <display:column title="Select" style="width:20px;" ><div style="text-align:center"><input type="checkbox" name="check4"  onclick="return removeTime('${timeSheetList.timesheetId}','${timeSheetList.truckingOpsId}','${timeSheetList.ticket}','${timeSheetList.id}',this)"/></div></display:column>
        
	</display:table>
	</c:if>
	
	<c:if test="${timeSheets1!='[]' && separateCrewDriverList=='Yes'}">
		<table class="table" style="width:100%;margin-top: -20px;">
						<thead>
							 <tr>
								<th onclick="getTimeSheetSortedList('order by w.ticket')" style="cursor:pointer" id="ticketListId">Ticket#</th>
							 	<th width="">Note</th>
							 	<th onclick="getTimeSheetSortedList('order by s.shipper')" style="cursor:pointer" id="shipperListId">Shipper</th>
							 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForSSCW">
							 		<th >Weight</th>
							 	</configByCorp:fieldVisibility>
							 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForOthers">
							 		<th >Volume</th>
							 	</configByCorp:fieldVisibility>
							 	<th onclick="getTimeSheetSortedList('order by s.service')" style="cursor:pointer" id="serviceListId">Service</th>
							 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCWMSAndSSCW">
							 		<th>Resources</th>
								 	<th onclick="getTimeSheetSortedList('order by time(w.scheduledArrive)')" style="cursor:pointer" id="scheduledArriveListId">ATime</th>
								 	<th onclick="getTimeSheetSortedList('order by time(w.timeFrom)')" style="cursor:pointer" id="timeFromId">DTime</th>
							 	</configByCorp:fieldVisibility>
							 	<th onclick="getTimeSheetSortedList('order by s.originCity')" style="cursor:pointer" id="originCityId">Origin City</th>
							 	<th onclick="getTimeSheetSortedList('order by s.destinationCity')" style="cursor:pointer" id="destinationCityId">Destination City</th>
							 </tr>
						 </thead>
					 <c:forEach var="individualItem" items="${timeSheetListMap}" >
					 	
					 	<c:forEach var="scheduleResourceListMap" items="${individualItem.key}" >
							 
								 <c:forEach var="scheduleResourceListValue" items="${scheduleResourceListMap.value}" >
									
									    <tr class="">
									    <td  class="listwhitetext" style="text-align:left;width:8%;color:black;font-size:1em" >
									    	<img class="clickable" id="show" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
											<div style="display:inline-block;" onmouseover="findCrewTruckListToolTip('${scheduleResourceListValue.ticket}',this);" onmouseout="ajax_hideTooltip();" >
												<a onclick="openTicket('${scheduleResourceListValue.workTicketId}')"><c:out value="${scheduleResourceListValue.ticket}"></c:out></a>
											</div>
											<c:if test= "${mmValidation =='Yes' }" >
									   				<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${scheduleResourceListValue.workTicketId}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
									   		</c:if>
									   		<c:if test="${scheduleResourceListValue.acceptByDriver=='true'}">
									   		<div style="background:#008000; width:8px; height:8px;display: inline-block;margin-left: 5px;margin-top: 3px;border-radius: 10px;"></div>
									   		</c:if>
									   		<%-- <c:if test="${scheduleResourceListValue.acceptByDriver=='false'}">
									   		<div style="background:#f00; width:8px; height:8px;display: inline-block;margin-left: 5px;margin-top: 3px;border-radius: 10px;"></div>
									   		</c:if> --%>
									   		
											<img align="top" title="Forms" onclick="findWorkTicketForms('${scheduleResourceListValue.ticket}','${scheduleResourceListValue.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/>
									    </td>
									    <td  class="listwhitetext" align="right" style="text-align:center;width:5%;font-size:1em;">
											<c:if test="${scheduleResourceListValue.notesId==''}">
												<img id="open${scheduleResourceListValue.workTicketId}" src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${scheduleResourceListValue.workTicketId}&notesId=${scheduleResourceListValue.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
											</c:if>
											<c:if test="${scheduleResourceListValue.notesId!=''}">
												<img id="close${scheduleResourceListValue.workTicketId}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${scheduleResourceListValue.workTicketId}&notesId=${scheduleResourceListValue.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
											</c:if>
									    </td>
									    <td  class="listwhitetext" align="right" style="font-size:1em;color:black;">${scheduleResourceListValue.shipper}</td>
									    <configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForSSCW">
												<td  class="listwhitetext" align="right" style="width:10%;font-size:1em;color:black;">
												    
												    <c:choose>
														<c:when test="${(scheduleResourceListValue.actualWeight!=null && scheduleResourceListValue.actualWeight!='' && scheduleResourceListValue.actualWeight!='0.0') && (scheduleResourceListValue.estimatedWeight!=null && scheduleResourceListValue.estimatedWeight!='' && scheduleResourceListValue.estimatedWeight!='0.0')}">
											    				<c:out value="${scheduleResourceListValue.actualWeight}" />(A)&nbsp;${scheduleResourceListValue.unit1}
											    		</c:when>
											    		<c:when test="${(scheduleResourceListValue.actualWeight!=null && scheduleResourceListValue.actualWeight!='' && scheduleResourceListValue.actualWeight!='0.0') && (scheduleResourceListValue.estimatedWeight==null || scheduleResourceListValue.estimatedWeight=='' || scheduleResourceListValue.estimatedWeight=='0.0')}">
											    				<c:out value="${scheduleResourceListValue.actualWeight}" />(A)&nbsp;${scheduleResourceListValue.unit1}
											    		</c:when>
											    		<c:when test="${(scheduleResourceListValue.actualWeight==null || scheduleResourceListValue.actualWeight=='' || scheduleResourceListValue.actualWeight=='0.0') && (scheduleResourceListValue.estimatedWeight!=null && scheduleResourceListValue.estimatedWeight!='' && scheduleResourceListValue.estimatedWeight!='0.0')}">
											    				<c:out value="${scheduleResourceListValue.estimatedWeight}" />(E)&nbsp;${scheduleResourceListValue.unit1}
											    		</c:when>
											    		<c:when test="${(scheduleResourceListValue.actualWeight==null || scheduleResourceListValue.actualWeight=='' || scheduleResourceListValue.actualWeight=='0.0') && (scheduleResourceListValue.estimatedWeight==null || scheduleResourceListValue.estimatedWeight=='' || scheduleResourceListValue.estimatedWeight=='0.0')}">
											    				<c:out value="${scheduleResourceListValue.actualWeight}" />(A)&nbsp;${scheduleResourceListValue.unit1}
											    		</c:when>
											    		<c:otherwise>
											    				<c:out value="${scheduleResourceListValue.actualWeight}" />(A)&nbsp;${scheduleResourceListValue.unit1}
											    		</c:otherwise>
	    											</c:choose>
												    
												</td>
										</configByCorp:fieldVisibility>
									    <configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForOthers">
											<td  class="listwhitetext" align="right" style="width:10%;font-size:1em;color:black;">
												<c:if test="${scheduleResourceListValue.actualVolume!=null && scheduleResourceListValue.actualVolume!='' && scheduleResourceListValue.actualVolume!='0.0'}">	
													<c:out value="${scheduleResourceListValue.actualVolume}" />(A)&nbsp;${scheduleResourceListValue.unit2}
												</c:if>
												<c:if test="${scheduleResourceListValue.actualVolume==null || scheduleResourceListValue.actualVolume==''|| scheduleResourceListValue.actualVolume=='0.0'}">	
													<c:out value="${scheduleResourceListValue.estimatedCubicFeet}" />(E)&nbsp;${scheduleResourceListValue.unit2}
												</c:if>
											</td>
										</configByCorp:fieldVisibility>
										<td  class="listwhitetext" align="right" style="font-size:1em;color:black;">${scheduleResourceListValue.service}</td>
										<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCWMSAndSSCW">
											<td  class="listwhitetext" align="right" style="font-size:1em;color:black;">
											<c:if test="${scheduleResourceListValue.quantitySum!=null && scheduleResourceListValue.quantitySum!='' && scheduleResourceListValue.quantitySum!='0' && scheduleResourceListValue.quantitySum!='0.0' && scheduleResourceListValue.quantitySum!='0.00'}" >
												<div align="center">
													<img align="middle" title="" onmouseover="findResourceDetails('${scheduleResourceListValue.ticket}',this);"  src="${pageContext.request.contextPath}/images/plus-small.png" onmouseout="ajax_hideTooltip();"/>		 
												</div>
											</c:if>
											</td>
											<td  class="listwhitetext" align="right" style="font-size:1em;color:black;">${scheduleResourceListValue.aTime}</td>
											<td  class="listwhitetext" align="right" style="font-size:1em;color:black;">${scheduleResourceListValue.dTime}</td>
										</configByCorp:fieldVisibility>
										<td  class="listwhitetext" align="right" style="font-size:1em;color:black;">
											<span align="left" onmouseover="findToolTipService('${scheduleResourceListValue.ticket}','${scheduleResourceListValue.originCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${scheduleResourceListValue.originCity}....." /></span>
										</td>
								        <td  class="listwhitetext" align="right" style="font-size:1em;color:black;">
								       		<span align="left" onmouseover="findToolTipDestCityService('${scheduleResourceListValue.ticket}','${scheduleResourceListValue.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${scheduleResourceListValue.destinationCity}....." /></span> 
								        </td>
									    </tr>
									
									</c:forEach>
									
								 <c:forEach var="crewTruckList" items="${individualItem.value}" >
								 <tr class="expand-child">
								 <td colspan="20">
								 <table class="table" style="margin-left:99px;float:left;padding:0px;border:none;width:60%" cellpadding="0" cellspacing="0">
								 <tr>
								  <td colspan="8" style="padding:0px;margin:0px;vertical-align:top;">
								   <table style="float:left;padding:0px;border:none;width:100%" cellpadding="0" cellspacing="0">
									 <thead>
										 <tr>
											<th width="">Crew Name</th>
										 	<th width="">Additional Information</th>
										 	<th width="">Select</th>
										 	<th width="">Project&nbsp;Lead&nbsp;Name</th>
                                             <th width="">Project&nbsp;Lead&nbsp;Number</th>	
										 	
										 </tr>
									</thead>
									<c:forEach var="crewListValue" items="${crewTruckList.key}" >
										<tr>
										<c:choose> 
									        <c:when test="${fieldValueName == crewListValue.crewName }">
									        	<td  class="listwhitetext" align="right" style="width:125px;font-size:0.9em;color:black;"><font class="invredsch">${crewListValue.crewName}</font></td>
									        </c:when> 
									        <c:otherwise>
										         <td  class="listwhitetext" align="right" style="width:125px;font-size:0.9em;color:black;">${crewListValue.crewName}</td>
										     </c:otherwise>
										</c:choose>
										 	<td></td>
										 	<c:set var="crewName" value="${crewNameCheckBox}"/>
											 <c:set var="crewNameVal" value="${crewListValue.crewName}"/>
										   		<c:choose>
												    <c:when test="${fn:contains(crewName, crewNameVal)}">
												        <td><div style="text-align:center"><input type="checkbox" name="checkCrew" checked="checked" onclick="setValues('${crewListValue.crewName}',this,'Crew','${crewListValue.ticket}','${crewListValue.timesheetId}','${crewListValue.truckingOpsId}','${crewListValue.id}')"/></div></td>
												    </c:when>
												    <c:otherwise>
													 	<td><div style="text-align:center"><input type="checkbox" name="checkCrew"  onclick="setValues('${crewListValue.crewName}',this,'Crew','${crewListValue.ticket}','${crewListValue.timesheetId}','${crewListValue.truckingOpsId}','${crewListValue.id}')"/></div></td>
													</c:otherwise>
										    	</c:choose>
										 <td  class="listwhitetext" align="right" style="width:100px;font-size:0.9em;color:black;"><c:out value="${crewListValue.projectLeadName}" /></td>
										  <td  class="listwhitetext" align="right" style="width:100px;font-size:0.9em;color:black;"><c:out value="${crewListValue.projectLeadNumber}" /></td>
										  
										 </tr>
									</c:forEach>
									</table>
								  </td>
								  
								  <td colspan="8" style="padding:0px;margin:0px;vertical-align:top;">
								   <table class="table" style="float:left;padding:0px;border:none;width:100%;vertical-align:top;" cellpadding="0" cellspacing="0">
									 <thead>
										 <tr>
										 	<th width="">Truck#</th>
											 <th width="" style="text-align:center;">Driver</th>
											 <th width="">Select</th>
										 </tr>
									</thead>
									<c:forEach var="truckListValue" items="${crewTruckList.value}" >
										<tr>
										<c:choose> 
									        <c:when test="${fieldValueName == truckListValue.localtruckNumber }">
									        	<td  class="listwhitetext" align="right" style="width:100px;font-size:0.9em;color:black;"><font class="invredsch">${truckListValue.localtruckNumber}</font></td>
									        </c:when> 
									        <c:otherwise>
										         <td  class="listwhitetext" align="right" style="width:100px;font-size:0.9em;color:black;">${truckListValue.localtruckNumber}</td>
										     </c:otherwise>
										</c:choose>
										 <td  class="listwhitetext" style="text-align:center;">
										 <c:if test="${truckListValue.crewNameList!=''}">					 
											  <select name="assignedCrewNameForTruck" onchange="updateAssignedCrewForTruck('${truckListValue.localtruckNumber}','${truckListValue.ticket}',this);" class="list-menu" >
											  	<option selected value=""></option>
					                            	<c:forEach var="item" items="${truckListValue.crewNameList}">
														   <c:choose>
															    <c:when test="${item == truckListValue.assignedCrewNameForTruck}">
															        <c:set var="selectedInd" value=" selected"></c:set>
															    </c:when>
															    <c:otherwise>
																 	<c:set var="selectedInd" value=""></c:set>
																</c:otherwise>
														    </c:choose>
														    <option value="<c:out value='${item}' />" <c:out value='${selectedInd}' />>
														     	<c:out value="${item}"></c:out>
														    </option>
													  </c:forEach>
											  </select>
										  </c:if>
										  </td>
										  
										  
										  <c:set var="truckNo" value="${truckNumberCheckBox}"/>
										  <c:set var="truckNoVal" value="${truckListValue.localtruckNumber}"/>
										   	<c:choose>
											    <c:when test="${fn:contains(truckNo, truckNoVal)}">
											        <td><div style="text-align:center"><input type="checkbox" name="checkTruck" checked="checked" onclick="setValues('${truckListValue.localtruckNumber}',this,'Truck','${truckListValue.ticket}','${truckListValue.timesheetId}','${truckListValue.truckingOpsId}','${truckListValue.id}')"/></div></td>
											    </c:when>
											    <c:otherwise>
												 	<td><div style="text-align:center"><input type="checkbox" name="checkTruck"  onclick="setValues('${truckListValue.localtruckNumber}',this,'Truck','${truckListValue.ticket}','${truckListValue.timesheetId}','${truckListValue.truckingOpsId}','${truckListValue.id}')"/></div></td>
												</c:otherwise>
										    </c:choose>
										 </tr>
									</c:forEach>
									</table>
								  </td>
								  <td style="border:none;">
								  <table style="float:left;">
									<tr>
									<td style="border:none;"><input type="button" name="removeTruckCrew" class="pricelistbtn" value="Remove" style="font-size: 11px; height: 19px; padding: 1px 3px;margin-top:20%; line-height: 10px;" onclick="removeCrewTruck();"/></td>
									</tr>
									</table>
								  </td>
								 </tr>
								 </table>
								</td>							
								</tr>
							</c:forEach>
								
						</c:forEach>
					</c:forEach>
			</table>
	</c:if>
	
   <c:if test="${timeSheets1=='[]'}">
   <display:table name="timeSheets1" class="table" requestURI="" id="timeSheetList" style="width:100%;margin-top: -20px;">
		<display:column style="text-align:right;width:10%;color:black"  titleKey="workTicket.ticket"><a onclick="openTicket('${timeSheetList.workTicketId}')"><c:out value="${timeSheetList.ticket}"></c:out></a>
		<c:if test= "${mmValidation =='Yes' }" >
	   				<img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${timeSheetList.workTicketId}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/>
	   			</c:if>
		<img align="top" title="Forms" onclick="findWorkTicketForms('${timeSheetList.ticket}','${timeSheetList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></display:column>
		<display:column title="Note" style="text-align: center;width:5%;" maxLength="50">
				<c:if test="${timeSheetList.notesId==''}">
					<img id="open${timeSheetList.workTicketId}" src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${timeSheetList.workTicketId}&notesId=${timeSheetList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${timeSheetList.notesId!=''}">
					<img id="close${timeSheetList.workTicketId}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${timeSheetList.workTicketId}&notesId=${timeSheetList.ticket}&noteFor=WorkTicket&subType=Scheduling&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
		</display:column>
		<display:column property="shipper" titleKey="timeSheet.shipper" />
	    <configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForSSCW">
			<display:column title="Weight" style="width:10%;">
				<c:if test="${timeSheetList.actualWeight!=null && timeSheetList.actualWeight!='' && timeSheetList.actualWeight!='0.0'}">	
					<c:out value="${timeSheetList.actualWeight}" />(A)&nbsp;${timeSheetList.unit1}
			    </c:if>
			    <c:if test="${timeSheetList.actualWeight==null || timeSheetList.actualWeight=='' || timeSheetList.actualWeight=='0.0'}">	
				   <c:out value="${timeSheetList.estimatedWeight}" />(E)&nbsp;${timeSheetList.unit1}
			    </c:if>
			</display:column>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForOthers">
			<display:column title="Volume" style="width:10%;">
			<c:if test="${timeSheetList.actualVolume!=null && timeSheetList.actualVolume!='' && timeSheetList.actualVolume!='0.0'}">	
				<c:out value="${timeSheetList.actualVolume}" />(A)&nbsp;${timeSheetList.unit2}
			</c:if>
			<c:if test="${timeSheetList.actualVolume==null || timeSheetList.actualVolume==''|| timeSheetList.actualVolume=='0.0'}">	
				<c:out value="${timeSheetList.estimatedCubicFeet}" />(E)&nbsp;${timeSheetList.unit2}
			</c:if>
	    </display:column>
			</configByCorp:fieldVisibility>
		<display:column  title="Service" property="service" ></display:column>
		<display:column  sortProperty="crewName" title="Crew Name" style="width: 130px"><c:out value="${timeSheetList.crewName}" /></display:column>
		<display:column title="Driver" style="width:20px; color:#15428B !important;" >
		<c:if test="${timeSheetList.isDriverUsed==true}">
			<div style="text-align:center"><input type="checkbox" name="driverCheck" value="${timeSheetList.id}" onclick="updateDrivers(this);" checked="checked" /></div>
		</c:if>
		<c:if test="${timeSheetList.isDriverUsed==false}">
			<div style="text-align:center"><input type="checkbox" name="driverCheck" value="${timeSheetList.id}" onclick="updateDrivers(this);" /></div>
		</c:if>
		</display:column>
		<display:column  title="Truck#" property="localtruckNumber"></display:column>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCWMSAndSSCW">
		<display:column title="ATime" property="aTime" > </display:column>
		<display:column title="DTime" property="dTime" > </display:column>
		</configByCorp:fieldVisibility>
		<display:column   title="Origin City" >
		<span align="left" onmouseover="findToolTipService('${timeSheetList.ticket}','${timeSheetList.originCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${timeSheetList.originCity}....." /></span>
		</display:column>
        <display:column   title="Destination City">
       <span align="left" onmouseover="findToolTipDestCityService('${timeSheetList.ticket}','${timeSheetList.destinationCity}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${timeSheetList.destinationCity}....." /></span> 
        </display:column>
        <display:column title="Select" style="width:20px;" ><div style="text-align:center"><s:checkbox name="check4" fieldValue="${timeSheetList.timesheetId}--${timeSheetList.truckingOpsId}--${timeSheetList.ticket}--${timeSheetList.id}" onclick="return removeTimeSheets(this)"/></div></display:column>
        </display:table>
   </c:if>
	</div>
</td>
</tr>
<c:if test="${separateCrewDriverList=='No'}">
<tr>
<td align="left" valign="middle">
<table>
<tr>
<td><input type="button" name="remove" value="Remove" style="width:70px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="removeTimeSheet();"/></td>
</tr>
</table>
</td>
</tr>
</c:if>
</tbody>
</table>
</s:form>
<s:form name="timeSheetListResource" action="searchScheduleResouce.html?filter=hidden&checkbtn=A">
<s:hidden name="requestedWorkTkt" value=""/>
<s:hidden name="requestedCrews" value=""/>
<s:hidden name="requestedTrucks" value=""/>
<s:hidden name="requestedSchedule" value=""/>
<s:hidden name="checkUsed" value=""/>
<s:hidden name="editable" value="${editable}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="checkbtn" value="<%= request.getParameter("checkbtn")%>"/>
<s:hidden name="flag" value="<%= request.getParameter("flag")%>"/>
	<s:hidden name="wareHse" value="${wareHse}"/>
	<s:hidden name="tktWareHse"/>
	<s:hidden name="workDt"/>
	<s:hidden name="truckWareHse" value="${truckWareHse}"/>
	<s:hidden name="workticketServiceGroup" value="${workticketServiceGroup}"/>
	<s:hidden name="workticketService" value="${workticketService}"/>
	<s:hidden name="workticketStatus" value="${workticketStatus}"/>

</s:form>
<jsp:include flush="true" page="accountLineListViewPaginationJs.jsp"></jsp:include>
<script>
try{
if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' ){
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['assign'].disabled = false;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = false;
	}
}
catch(e){}	
try{
if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' ){
		document.forms['timeSheetList'].elements['absent'].disabled = true;
		document.forms['timeSheetList'].elements['present'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['absent'].disabled = false;
		document.forms['timeSheetList'].elements['present'].disabled = true;
	}
	}
	catch(e){}
try{
if(document.forms['timeSheetList'].elements['requestedSchedule'].value == ''){
		document.forms['timeSheetList'].elements['remove'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['remove'].disabled = false;
	}
	}
	catch(e){}
try{
	//if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value == ''){
	//	document.forms['timeSheetList'].elements['sendToMM'].disabled = true;
	//}else{
	//	document.forms['timeSheetList'].elements['sendToMM'].disabled = false;
	//}
}catch(e){}
try{
<c:if test="${editable=='No'}">

	document.forms['timeSheetList'].elements['remove'].disabled = true;
	document.forms['timeSheetList'].elements['assign'].disabled = true;
	document.forms['timeSheetList'].elements['groupAssign'].disabled = true;
	document.forms['timeSheetList'].elements['absent'].disabled = true;
	//document.forms['timeSheetList'].elements['sendToMM'].disabled = true;
	
</c:if>
}
catch(e){}

function setValues(nameNumber,targetElement,type,ticketNo,timeSheetId,truckingOpsId,id){
	document.forms['timeSheetList'].elements['ticketNumber'].value = ticketNo
	var timeSheetIdValue=timeSheetId+"#"+truckingOpsId+"#"+ticketNo+"#"+id;
	if(type=='Truck'){
		var truckNoId = document.forms['timeSheetList'].elements['deleteTruckNumber'].value;
		var truckNumber = document.forms['timeSheetList'].elements['truckNumberCheckBox'].value;
		if(targetElement.checked){
			if(truckNoId == '' ){
				document.forms['timeSheetList'].elements['deleteTruckNumber'].value = timeSheetIdValue;
			}else{
				if(truckNoId.indexOf(timeSheetIdValue)>=0){
				}else{
					truckNoId = truckNoId + "~" +timeSheetIdValue;
				}
				document.forms['timeSheetList'].elements['deleteTruckNumber'].value = truckNoId.replace('~ ~',"~");
			}
			if(truckNumber == '' ){
				document.forms['timeSheetList'].elements['truckNumberCheckBox'].value = nameNumber;
			}else{
				if(truckNumber.indexOf(nameNumber)>=0){
				}else{
					truckNumber = truckNumber + "~" +nameNumber;
				}
				document.forms['timeSheetList'].elements['truckNumberCheckBox'].value = truckNumber.replace('~ ~',"~");
			}
		}else{
		     if(truckNoId == ''){
		    	 document.forms['timeSheetList'].elements['deleteTruckNumber'].value = timeSheetIdValue;
		     }else{
		    	 var check = truckNoId.indexOf(timeSheetIdValue);
		 		if(check > -1){
		 			var values = truckNoId.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== timeSheetIdValue) {
		 		    	 values.splice(i, 1);
		 		    	truckNoId = values.join("~");
		 		    	document.forms['timeSheetList'].elements['deleteTruckNumber'].value = truckNoId;
		 		      }
		 		   }
		 		}else{
		 			truckNoId = truckNoId + "~" +timeSheetIdValue;
		 			document.forms['timeSheetList'].elements['deleteTruckNumber'].value = truckNoId.replace('~ ~',"~");
		 		}
		     }
		     if(truckNumber == ''){
		    	 document.forms['timeSheetList'].elements['truckNumberCheckBox'].value = nameNumber;
		     }else{
		    	 var check = truckNumber.indexOf(nameNumber);
		 		if(check > -1){
		 			var values = truckNumber.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== nameNumber) {
		 		    	values.splice(i, 1);
		 		    	truckNumber = values.join("~");
		 		    	document.forms['timeSheetList'].elements['truckNumberCheckBox'].value = truckNumber;
		 		      }
		 		   }
		 		}else{
		 			truckNumber = truckNumber + "~" +nameNumber;
		 			document.forms['timeSheetList'].elements['truckNumberCheckBox'].value = truckNumber.replace('~ ~',"~");
		 		}
		     }
		}
	}
	if(type=='Crew'){
		var crewNameId = document.forms['timeSheetList'].elements['deleteCrewName'].value;
		var crewName = document.forms['timeSheetList'].elements['crewNameCheckBox'].value;
		if(targetElement.checked){
			if(crewNameId == '' ){
				document.forms['timeSheetList'].elements['deleteCrewName'].value = timeSheetIdValue;
			}else{
				if(crewNameId.indexOf(timeSheetIdValue)>=0){
				}else{
					crewNameId = crewNameId + "~" +timeSheetIdValue;
				}
				document.forms['timeSheetList'].elements['deleteCrewName'].value = crewNameId.replace('~ ~',"~");
			}
			if(crewName == '' ){
				document.forms['timeSheetList'].elements['crewNameCheckBox'].value = nameNumber;
			}else{
				if(crewName.indexOf(nameNumber)>=0){
				}else{
					crewName = crewName + "~" +nameNumber;
				}
				document.forms['timeSheetList'].elements['crewNameCheckBox'].value = crewName.replace('~ ~',"~");
			}
		}else{
			if(crewNameId == ''){
		    	 document.forms['timeSheetList'].elements['deleteCrewName'].value = timeSheetIdValue;
		     }else{
		    	 var check = crewNameId.indexOf(timeSheetIdValue);
		 		if(check > -1){
		 			var values = crewNameId.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== timeSheetIdValue) {
		 		    	 values.splice(i, 1);
		 		    	crewNameId = values.join("~");
		 		    	document.forms['timeSheetList'].elements['deleteCrewName'].value = crewNameId;
		 		      }
		 		   }
		 		}else{
		 			crewNameId = crewNameId + "~" +timeSheetIdValue;
		 			document.forms['timeSheetList'].elements['deleteCrewName'].value = crewNameId.replace('~ ~',"~");
		 		}
		     }
			if(crewName == ''){
		    	 document.forms['timeSheetList'].elements['crewNameCheckBox'].value = nameNumber;
		     }else{
		    	 var check = crewName.indexOf(nameNumber);
		 		if(check > -1){
		 			var values = crewName.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== nameNumber) {
		 		    	values.splice(i, 1);
		 		    	crewName = values.join("~");
		 		    	document.forms['timeSheetList'].elements['crewNameCheckBox'].value = crewName;
		 		      }
		 		   }
		 		}else{
		 			crewName = crewName + "~" +nameNumber;
		 			document.forms['timeSheetList'].elements['crewNameCheckBox'].value = crewName.replace('~ ~',"~");
		 		}
		     }
		}
	}
}

function removeCrewTruck(){
	var truckDetail = document.forms['timeSheetList'].elements['deleteTruckNumber'].value;
	var crewDetail = document.forms['timeSheetList'].elements['deleteCrewName'].value;
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
		alert("Select work date to continue.....");
		document.forms['timeSheetList'].elements['workDt'].select();
		return false;
	}else if(truckDetail=='' && crewDetail==''){
		alert('Nothing found to Remove.');
		return false;
	}else{
		document.forms['timeSheetList'].elements['checkbtn'].value='A';
		document.forms['timeSheetList'].action = 'deleteCrewTruckFromScheduleResource.html';
	 	document.forms['timeSheetList'].submit();
	 	submit_form();
	}
}
function findCrewTruckListToolTip(ticket,position){
	if(ticket!=null && !ticket==''){
		var newWrkDt = document.forms['timeSheetList'].elements['workDt'].value;
		var wareHse = document.forms['timeSheetList'].elements['wareHse'].value;
		var crewName = document.forms['timeSheetList'].elements['crewNameCheckBox'].value;
		var truckNo = document.forms['timeSheetList'].elements['truckNumberCheckBox'].value;
		var url='scheduleResourceSeparateCrewTruckListAjax.html?ajax=1&tkt='+ticket+'&workDt='+newWrkDt+'&wareHse='+wareHse+'&crewNameCheckBox='+crewName+'&truckNumberCheckBox='+truckNo+'&decorator=simple&popup=true';
		ajax_showTooltip(url,position);
	}
}

function updateAssignedCrewForTruck(truckNumber,ticket,targetElement){
		var updateQuery = "update ScheduleResourceOperation set assignedCrewForTruck ='"+targetElement.value+"' where ticket='"+ticket+"' and localtruckNumber='"+truckNumber+"' ";
		new Ajax.Request('/redsky/updateAssignedCrewForTruckAjax.html?ajax=1&updateQuery='+updateQuery+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			    var response = transport.responseText || "no response text";
			    },
			    onFailure: function(){ 
				    }
			  });
	}
	
function getTimeSheetList(target){
	if(target.value!='' && target.value!=' '){
		  document.forms['timeSheetList'].elements['flag'].value='';
		  if(document.forms['timeSheetList'].elements['workDt'].value == ""){
		 		alert("Select work date to continue.....");
		 		return false;
		 	}
		 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
		 		alert("Select Crew WareHouse to continue.....");
		 		return false;
		 	}
		 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
				alert("Select Ticket WareHouse  to continue.....");
		 		return false;
		 	}
			if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
				alert("Select Truck WareHouse  to continue.....");
		 		return false;
		 	}
			var fieldName = document.forms['timeSheetList'].elements['fieldName'].value;
			var whereClause = "and s."+fieldName+" = '"+target.value+"'";
			var whereClause1 = "and s."+fieldName+" != '"+target.value+"'";
		 	document.forms['timeSheetList'].elements['whereClauseSecheDuledResourceMap'].value=whereClause;
		 	document.forms['timeSheetList'].elements['whereClauseSecheDuledResourceMap1'].value=whereClause1;
		 	document.forms['timeSheetList'].elements['fieldValueName'].value=target.value;
		 	document.forms['timeSheetList'].elements['deleteTruckNumber'].value='';
			document.forms['timeSheetList'].elements['deleteCrewName'].value='';
			document.forms['timeSheetList'].elements['crewNameCheckBox'].value='';
			document.forms['timeSheetList'].elements['truckNumberCheckBox'].value='';
			document.forms['timeSheetList'].action = 'searchScheduleResouce.html?filter=hidden&checkbtn=A';
			document.forms['timeSheetList'].submit();
			submit_form();
		}
	}
	
function getTimeSheetSortedList(sortVal){
	  document.forms['timeSheetList'].elements['flag'].value='';
	  if(document.forms['timeSheetList'].elements['workDt'].value == ""){
	 		alert("Select work date to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
	 		alert("Select Crew WareHouse to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
			alert("Select Ticket WareHouse  to continue.....");
	 		return false;
	 	}
		if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
			alert("Select Truck WareHouse  to continue.....");
	 		return false;
	 	}
	 	var orderByVal= document.forms['timeSheetList'].elements['orderByValue'].value;
		if(orderByVal==null || orderByVal=='' || orderByVal=='asc'){
			orderByVal = document.forms['timeSheetList'].elements['orderByValue'].value = 'desc';
			document.forms['timeSheetList'].elements['whereClauseSecheDuledResourceMap'].value=sortVal+" "+orderByVal;
		}else{
			orderByVal = document.forms['timeSheetList'].elements['orderByValue'].value = 'asc';
			document.forms['timeSheetList'].elements['whereClauseSecheDuledResourceMap1'].value=sortVal+" "+orderByVal;
		}
		document.forms['timeSheetList'].action = 'searchScheduleResouce.html?filter=hidden&checkbtn=A';
		document.forms['timeSheetList'].submit();
		submit_form();
	}
	
	var $j = jQuery.noConflict();
	$j(".clickable").click(function() {
	    $j(this).closest('tr').next('tr').toggle();
	});
	
	(function ($) {
		$('#expandAll')
	    .unbind('click')
	    .click( function() {
	        $("tr.expand-child").css("display","table-row").show();
	    })
	    
	    $('#collapseAll')
	    .unbind('click')
	    .click( function() {
	        $("tr.expand-child").css("display","none").hide();
	    })
    })(jQuery);
	
	function getCrewTruckList(target){
		var newWrkDt = document.forms['timeSheetList'].elements['workDt'].value;
		var wareHse = document.forms['timeSheetList'].elements['wareHse'].value;
		document.forms['timeSheetList'].elements['fieldValue'].value='';
			new Ajax.Request('getCrewTruckListAjax.html?ajax=1&decorator=simple&popup=true&fieldName='+target.value+'&workDt='+newWrkDt+'&wareHse='+wareHse,
					{
				method:'get',
				onSuccess: function(transport){
					var response = transport.responseText || "";
					response = response.trim();
					response = response.replace("[",' ~');
					response = response.replace("]",'');
					var res = response.split("~");
		            targetElement = document.getElementById('fieldValue');
					targetElement.length = res.length;
						for(i=0;i<res.length;i++){
							if(res[i] == '' || res[i]== 'null'){
								document.getElementById('fieldValue').options[i].text = '';
								document.getElementById('fieldValue').options[i].value = '';
							}else{
								document.getElementById('fieldValue').options[i].text = res[i];
								document.getElementById('fieldValue').options[i].value = res[i];
								document.getElementById('fieldValue').options[i].selected=false;
							}
						}
				},
				onFailure: function(){ 
				}
			});
	}
	
	function clearSorting(){
		var fieldName = document.forms['timeSheetList'].elements['fieldName'].value;
		var fieldValue = document.forms['timeSheetList'].elements['fieldValue'].value;
		if(fieldName=='' && fieldValue==''){
			alert('Nothing found to Clear.');
			return false;
		}else{
			document.forms['timeSheetList'].elements['flag'].value='';
		  	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
		 		alert("Select work date to continue.....");
		 		return false;
		 	}
		 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
		 		alert("Select Crew WareHouse to continue.....");
		 		return false;
		 	}
		 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
				alert("Select Ticket WareHouse  to continue.....");
		 		return false;
		 	}
			if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
				alert("Select Truck WareHouse  to continue.....");
		 		return false;
		 	}
			document.forms['timeSheetList'].elements['deleteTruckNumber'].value='';
			document.forms['timeSheetList'].elements['deleteCrewName'].value='';
			document.forms['timeSheetList'].elements['crewNameCheckBox'].value='';
			document.forms['timeSheetList'].elements['truckNumberCheckBox'].value='';
			document.forms['timeSheetList'].elements['fieldName'].value='';
			document.forms['timeSheetList'].elements['fieldValue'].value='';
			document.forms['timeSheetList'].action = 'searchScheduleResouce.html?filter=hidden&checkbtn=A';
			document.forms['timeSheetList'].submit();
			submit_form();
		}
	}
	function disableForm(){
		if(document.images) {
		    var totalImages = document.images.length;
		    	for (var i=0;i<totalImages;i++) {
						if(document.images[i].src.indexOf('calender.png')>0) { 
							var el = document.getElementById(document.images[i].id);  
							document.images[i].src = 'images/navarrow.gif'; 
							if((el.getAttribute("id")).indexOf('trigger')>0){ 
								el.removeAttribute('id');
							}
						}
						if(document.images[i].src.indexOf('s-right.png')>0) {
							var el = document.getElementById(document.images[i].id);
							try{
								el.onclick = false;
							}catch(e){}
					        	document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('s-left.png')>0) {
							var el = document.getElementById(document.images[i].id);
							try{
								el.onclick = false;
							}catch(e){}
					        	document.images[i].src = 'images/navarrow.gif';
						}
					} 
			 	}
		var elementsLen=document.forms['timeSheetList'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['timeSheetList'].elements[i].type=='text'){
				document.forms['timeSheetList'].elements[i].readOnly =true;
				document.forms['timeSheetList'].elements[i].className = 'input-textUpper';
				document.forms['timeSheetList'].elements[i].onkeydown=false;
			}else{
			 	document.forms['timeSheetList'].elements[i].disabled=true;
			}
			
			if(document.forms['timeSheetList'].elements[i].type=='button'){
				document.forms['timeSheetList'].elements[i].className ='pricelistbtn-dis';
			}
			var ticketLink = document.getElementById("ticketListId");
			ticketLink.onclick = '';
			var shipperListLink = document.getElementById("shipperListId");
			shipperListLink.onclick = '';
			var serviceListLink = document.getElementById("serviceListId");
			serviceListLink.onclick = '';
			var scheduledArriveListLink = document.getElementById("scheduledArriveListId");
			scheduledArriveListLink.onclick = '';
			var timeFromLink = document.getElementById("timeFromId");
			timeFromLink.onclick = '';
			var originCityLink = document.getElementById("originCityId");
			originCityLink.onclick = '';
			var destinationCityLink = document.getElementById("destinationCityId");
			destinationCityLink.onclick = '';
			var crewListBtnLink = document.getElementById("crewListBtn");
			crewListBtnLink.onclick = '';
			var absentCrewBtnLink = document.getElementById("absentCrewBtn");
			absentCrewBtnLink.onclick = '';
			var resourceSheetBtnLink = document.getElementById("resourceSheetBtn");
			resourceSheetBtnLink.onclick = '';
			var hideSelectedBtnLink = document.getElementById("hideSelectedBtn");
			hideSelectedBtnLink.onclick = '';
			var showAllBtnLink = document.getElementById("showAllBtn");
			showAllBtnLink.onclick = '';
			var resourceListBtnLink = document.getElementById("resourceListBtn");
			resourceListBtnLink.onclick = '';
			
		}  
	} 
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	<c:if test="${timeSheets1!='[]' && separateCrewDriverList=='Yes'}">
		var $j = jQuery.noConflict();
		$j("tr.expand-child").css("display","none").hide();
		
		var response = '${fieldList}';
		response = response.trim();
		response = response.replace("[",' ~');
		response = response.replace("]",'');
		var res = response.split("~");
	    targetElement = document.getElementById('fieldValue');
		targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.getElementById('fieldValue').options[i].text = '';
					document.getElementById('fieldValue').options[i].value = '';
				}else{
					document.getElementById('fieldValue').options[i].text = res[i];
					document.getElementById('fieldValue').options[i].value = res[i];
				}
			}
		
		var x=document.getElementById("fieldValue")
		if(x!=undefined && x!=null && x!=''){
			x.value='${fieldValueName}';
		}
	</c:if>

	<c:if test="${separateCrewDriverList=='Yes'}">
		<sec-auth:authComponent componentId="module.scheduleResouce.edit">
			<%;
				String scheduleResouceDisable="true";
				int permissionCheck  = (Integer)request.getAttribute("module.scheduleResouce.edit" + "Permission");
			 	if (permissionCheck > 2 ){
			 		scheduleResouceDisable = "false";
			 	}
  			%>
		 <% if (scheduleResouceDisable.equalsIgnoreCase("true")){%>
		 	disableForm();
		  <%} %>
		</sec-auth:authComponent>
	</c:if>
	function displaySheldulerMoodal()
	{
		var workDt=document.forms['timeSheetList'].elements['workDt'].value;
		if(workDt!="")
			{
		window.open('shedulerResourcEdit.html?workDate='+workDt+'&imageId=countForImage&decorator=popup&popup=true','test','height=700,width=725,top=0');   
	      }
	    else
		{
		alert("Select work date to continue.....")
		}}
	

		
			
	
	
</script>
