package com.trilasoft.app.webapp.action;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompensationSetup;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.ContractAccount;
import com.trilasoft.app.model.CostElement;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.model.PartnerBankInformation;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CompensationSetupManager;
import com.trilasoft.app.service.ContractAccountManager;
import com.trilasoft.app.service.ContractManager;
import com.trilasoft.app.service.CostElementManager;
import com.trilasoft.app.service.CountryDeviationManager;
import com.trilasoft.app.service.ItemsJEquipManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RateGridManager;
import com.trilasoft.app.service.RefMasterManager;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class ContractAction extends BaseAction implements Preparable {
	private Set contracts;

	private Contract contract;

	private ContractManager contractManager;
	
	private RateGridManager rateGridManager;
	
	private CountryDeviationManager countryDeviationManager; 

	private Long id;

	private String sessionCorpID;

	private List refMasters;

	private RefMasterManager refMasterManager;

	private static Map<String, String> billinginstruction;

	private ContractAccountManager contractAccountManager;

	private ContractAccount contractAccount;

	private Long maxId;
	
	private Long delAccountId;
	
	private List contractAccountList;

	private Set contractAccountSet;

	private ItemsJEquipManager itemsJEquipManager;

	private ItemsJEquip itemsJEquip;

	private List materialsList;

	private String itemType;

	private String cont;

	private static Map<String, String> job;
	
	private  Map<String, String> paytype;
	
	private String gotoPageString;

	private ChargesManager chargesManager;

	private List chargess;

	private String validateFormNav;
	
	private Boolean activeCheck;
	
	private String contractid;
	
	private Date openfromdate;
	
	private Date opentodate;
	private int countContractAccount;
	private List contractAccountByContract;

	private List copyContractAccount;

	private ContractAccount contractAccountCopy;

	private String hitFlag;

	private String contractCopy;

	private String contractA;
	private Company company;
	private CompanyManager companyManager;
	private String currencySign;
	private Charges chargesObj;
	private List agentList;
	private Map<String ,String> agentType1;
	private String agentTypeValue;
	private String dummyAgentCode;
	private String flag = "";
	private Boolean checkUTSI;
	private String contactAgentType;
	private AccountLineManager accountLineManager;
	private String baseCurrency;
	private  Map<String,String>currency;
	private String publishContractId;
	private CostElementManager costElementManager;
	public String contractOrgId; 
	private String parentCode;
	private String childContract;
	private String contractEnding;
	private String agentCorpId;
	private  Map<String,String>euVatList; 
	private  Map<String,String>euVatPercentList;
	private String companyCode;
	private List cmmDmmAgentList = new ArrayList();
	private List compensationList=new ArrayList();
	private CompensationSetupManager compensationSetupManager;
	private CompensationSetup compensationSetup;
	private Integer compensationyear;
	private BigDecimal  commissionpercentage = new BigDecimal("0.00");
	private boolean checkFieldVisibility=false;
	private boolean tenancyBillingContractType=false;
	private List<SystemDefault> sysDefaultDetail;
	private PartnerPrivateManager partnerPrivateManager;
	private String baseCurrencyValue;
	private Map<String, String> lead;
	private  Map<String, String> insopt;
	private List ratingScaleList;
	private List customerFeedbackList;
	private Map<String, String> creditTermsList;
	private Map<String, String> billgrp;
	private Map<String, String> billingMomentList;
	private boolean networkAgentFlag=false;
	private boolean cmmDmmAgentFlag=false;
	private  Map<String, String> printOptions;
	public String getDummyAgentCode() {
	return dummyAgentCode;
}

public void setDummyAgentCode(String dummyAgentCode) {
	this.dummyAgentCode = dummyAgentCode;
}

	private List companyDivis = new ArrayList();
	
	Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(ContractAction.class); 

	
	public ContractAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		companyCode=user.getCompanyDivision();
			}
	
	public void prepare() throws Exception {
		contractOrgId = sessionCorpID;
		getComboList(sessionCorpID);
		euVatList = refMasterManager.findByParameterWithoutParent(sessionCorpID, "EUVAT");
	    euVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "EUVAT");
	    cmmDmmAgentList = contractManager.findCmmDmmAgentDromCompany();
	    sysDefaultDetail = partnerPrivateManager.findsysDefault(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				baseCurrencyValue=systemDefault.getBaseCurrency();
			}
			
		}
		ratingScaleList = new ArrayList();		
		ratingScaleList.add("1 - 10 (10 is the best)");
		ratingScaleList.add("1 � 6 (1 is the best)");		
		
		customerFeedbackList=new ArrayList();
		customerFeedbackList.add("Just Say Yes");
		customerFeedbackList.add("Quality Survey");
	}
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		activeCheck = true;
		job = refMasterManager.findByParameter(sessionCorpID, "JOB");
		companyDivis=contractManager.findComanyDivision(sessionCorpID);
		contracts = new HashSet(contractManager.findAllContract(sessionCorpID));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contractManager.remove(contract.getId());
		saveMessage(getText("contract.deleted"));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		company= companyManager.findByCorpID(corpID).get(0);
		currencySign=company.getCurrencySign();		
		agentType1= refMasterManager.findByParameter(sessionCorpID, "AgentType");
		billinginstruction = refMasterManager.findByParameter(corpID, "BILLINST");
		job = refMasterManager.findByParameter(corpID, "JOB");
		companyDivis=contractManager.findComanyDivision(sessionCorpID);
		paytype = refMasterManager.findByParameter(corpID, "PAYTYPE");
		baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
		currency=refMasterManager.findCodeOnleByParameter(corpID, "CURRENCY");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		String permKey = sessionCorpID +"-"+"component.contract.reloJob.tenancyBilling";
		checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		printOptions = refMasterManager.findByParameter(sessionCorpID, "INVOICEFORMINPUTPARM");		

		if(checkFieldVisibility){
			tenancyBillingContractType = true;
		}
		lead=refMasterManager.findByParameter(sessionCorpID, "LEAD");
		insopt = refMasterManager.findByParameter(sessionCorpID, "INSOPT");
		creditTermsList = refMasterManager.findByParameter(sessionCorpID, "CrTerms");
		billgrp = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
		billingMomentList = refMasterManager.findByParameter(sessionCorpID, "BILLINGMOMENT");
		if(company.getUTSI()!=null && company.getUTSI()){
			networkAgentFlag = true;
		}
		if(company.getCmmdmmAgent()!=null && company.getCmmdmmAgent()){
	  		cmmDmmAgentFlag = true;
	  	}
		return SUCCESS;
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		if (id != null) {
			contract = contractManager.get(id);
		} else {
			contract = new Contract();
			contract.setCreatedOn(new Date());
			contract.setUpdatedOn(new Date());
			contract.setTimeTo("00:00");
			contract.setTimeFrom("00:00");
			contract.setContractCurrency(baseCurrency);
			contract.setPayableContractCurrency(baseCurrency);
			contract.setOwner(sessionCorpID);
		}
		
		checkUTSI=company.getUTSI();
		contract.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (contract.getId() == null);
		if (isNew) {
			contract.setCreatedOn(new Date());
		}

		String contractNew =contract.getContract();
		contractNew=contractNew.trim();
		contract.setContract(contractNew);
		contract.setCorpID(sessionCorpID);
		contract.setUpdatedOn(new Date());
		contract.setUpdatedBy(getRequest().getRemoteUser());
		contract.setOwner(sessionCorpID);
		contractManager.save(contract);
		//getComboList(sessionCorpID);
		checkUTSI=company.getUTSI();
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "contract.added" : "contract.updated";
			saveMessage(getText(key));
		}
		if (!isNew) {
			hitFlag="1";
			return INPUT;
		} else {
			maxId = Long.parseLong(contractManager.findMaximumId().get(0).toString());
			contract = contractManager.get(maxId);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		hitFlag="1";	
		return SUCCESS;
		}
	}
	
	private String cCon;
	private String cDes;
	private List checkContract;
	@SkipValidation
    public String findOldContract(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	checkContract=contractManager.checkContract(sessionCorpID,cCon);            	      		
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    return SUCCESS;
    }
	
	@SkipValidation
	public String searchContract() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			if (contract != null) {
				boolean contractType = contract.getContract() == null;
				boolean description = contract.getDescription() == null;
				boolean jobType = contract.getJobType() == null;
				boolean companyDivision = contract.getCompanyDivision() == null;
				job = refMasterManager.findByParameter(sessionCorpID, "JOB");
				companyDivis=contractManager.findComanyDivision(sessionCorpID);
				getRequest().getSession().setAttribute("contractTypeSession", contract.getContract());
				getRequest().getSession().setAttribute("descriptionSession", contract.getDescription());
				getRequest().getSession().setAttribute("jobTypeSession", contract.getJobType());
				getRequest().getSession().setAttribute("companyDivisionSession", contract.getCompanyDivision());
				getRequest().getSession().setAttribute("activeCheckSession", activeCheck);

				if (!contractType || !description || !jobType || !companyDivision) {
					contracts = new HashSet(contractManager.findContracts(contract.getContract(), contract.getDescription(), contract.getJobType(),contract.getCompanyDivision(), activeCheck, sessionCorpID));
				}
			} else {
				String contractSS = (String) getRequest().getSession().getAttribute("contractTypeSession");
				String descSS = (String) getRequest().getSession().getAttribute("descriptionSession");
				String jobTypeSS = (String) getRequest().getSession().getAttribute("jobTypeSession");
				String companyDivisionSS = (String) getRequest().getSession().getAttribute("companyDivisionSession");
				Boolean activeCheckSS = (Boolean) getRequest().getSession().getAttribute("activeCheckSession");
				contracts = new HashSet(contractManager.findContracts(contractSS, descSS, jobTypeSS,companyDivisionSS, activeCheckSS, sessionCorpID));
			}
		} catch (Exception e) {
			list();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	@SkipValidation
	public String deleteContractAccountIDMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contractAccountManager.remove(delAccountId);
		contractAccountList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String deleteContractAgentMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contractAccountManager.remove(delAccountId);
		 agentContractList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String updateChildCode() throws Exception {
		ArrayList al = new ArrayList(); 
		ArrayList a2 = new ArrayList(); 
		ArrayList a3 = new ArrayList(); 
		List childAccountCodeList=contractAccountManager.findChildAccountCode(parentCode,childContract, sessionCorpID);
		if(!childAccountCodeList.isEmpty()&& childAccountCodeList !=null){
			Iterator resc=childAccountCodeList.iterator();
			while(resc.hasNext()){
				String resc1=resc.next().toString();
				String[] resc2= resc1.split("~");
				List checkParentCode=contractAccountManager.checkPublicParentCode(resc2[0],parentCode,sessionCorpID);
				if(!checkParentCode.isEmpty()&& checkParentCode !=null){
				//a2.add(resc2[0]);	
				}else{
					contractAccountManager.remove(new Long(resc2[1]));	
				}
			}
		}
		//String k1=al.toString();
		//String [] arrStr = new String[];
		List childCodeList=contractAccountManager.findChildCode(parentCode,sessionCorpID);
		if(!childCodeList.isEmpty()&&childCodeList!=null){
		Iterator res=childCodeList.iterator();
		while(res.hasNext()){
		    String res2=res.next().toString();
			String[] res1= res2.split("~");
	       if(childCodeList.size()==1 && parentCode.equalsIgnoreCase(res1[1])){
	    	   a2.add(res1[1]);		
			}
			int i=contractAccountManager.countPartnerCode(res1[1],parentCode,childContract,sessionCorpID);
	        if(i>0){
	        	a3.add(res1[1]);
	        }else if(!parentCode.equalsIgnoreCase(res1[1])){
			ContractAccount contractAccount=new ContractAccount();
			contractAccount.setAccountCode(res1[1]);
			contractAccount.setAccountName(res1[0]);
			contractAccount.setCorpID(sessionCorpID);
			contractAccount.setContract(childContract);
			contractAccount.setAccountType("Account");
			contractAccount.setParentAccountCode(parentCode);
			contractAccount.setCreatedBy(getRequest().getRemoteUser());
			contractAccount.setCreatedOn(new Date());
			contractAccount.setUpdatedBy(getRequest().getRemoteUser());
			contractAccount.setUpdatedOn(new Date());
			al.add(res1[1]);
			contractAccountManager.save(contractAccount);
	        }
		}
		if(al.size()>0){
			String key="";
			int count = 0;
			Iterator arr=al.iterator();
			while(arr.hasNext()){
				if(count==0){
					key = arr.next().toString();
				}else{
					key = key +" , " +arr.next();
				}
				count++;
			}
			String key1 = "The child accounts " + key + " has been added successfully.";
		  	saveMessage(getText(key1));
		}
		if(a3.size()>0 && al.size()==0 && a2.size()==0){
		
		String key2 = "Child account related to this account is already available in the list.";
	  	saveMessage(getText(key2));
		}
		if(a2.size()>0){
			String key4 = "No child account is available for this account.";
		  	saveMessage(getText(key4));
		}
		}else{
		String key3 = "No child account is available for this account.";
	  	saveMessage(getText(key3));
		}
		contractAccountList();
		return SUCCESS;
	}
	@SkipValidation
	public String contractAccountList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contractOrgId = sessionCorpID;
		contract = contractManager.get(id);
		contract.setContract(contract.getContract());
		contractAccountList = contractAccountManager.findContractAccountList(contract.getContract(), sessionCorpID);
		countContractAccount=contractAccountList.size();
		contractAccountSet = new HashSet(contractAccountList);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String addAndCopyContractAccount() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 contractAccountByContract = contractAccountManager.findContractAccountByContract(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	@SkipValidation
	public String copyContractAccount() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		copyContractAccount = contractAccountManager.findContractAccountList(contractA,sessionCorpID);
		for (int x=0; x<copyContractAccount.size(); x++){
			contractAccountCopy = (ContractAccount)contractAccountManager.findContractAccountList(contractA,sessionCorpID).get(x);
					if (id == null) {
					//getComboList(sessionCorpID);
					ContractAccount contractAccount=new ContractAccount();
					contractAccount.setCorpID(contractAccountCopy.getCorpID());
					contractAccount.setAccountCode(contractAccountCopy.getAccountCode());
					contractAccount.setAccountName(contractAccountCopy.getAccountName());
					contractAccount.setValueDate(contractAccountCopy.getValueDate());
					contractAccount.setAccountType(contractAccountCopy.getAccountType());
					contractAccount.setContract(contractCopy);
					contractAccount.setCreatedOn(new Date());
					contractAccount.setCreatedBy(getRequest().getRemoteUser());
					contractAccount.setUpdatedOn(new Date());
					contractAccount.setUpdatedBy(getRequest().getRemoteUser());
					contractAccountManager.save(contractAccount);
				}
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	} 
	@SkipValidation
     public String agentContractList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contract = contractManager.get(id);
   		contract.setContract(contract.getContract());
   		SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
   		contractEnding="";
   		String strCrrDate="";
		StringBuilder tempCurrentDate = new StringBuilder(dateformatnewDateBuilder.format(currentdate));
		strCrrDate = tempCurrentDate.toString();
		Date tempCurrDate = new Date();
		try {
			tempCurrDate = dateformatnewDateBuilder.parse(strCrrDate);
		} catch (ParseException e) {
			logger.warn("Exception: "+strCrrDate+" ## : "+e.toString());

		}
		if(contract.getEnding()!=null){
			Date tempEnding = contract.getEnding();
			String endDate="";
			StringBuilder newTempEnding = new StringBuilder(dateformatnewDateBuilder.format(tempEnding));
			endDate = newTempEnding.toString();
			Date tempEndDate=new Date();
			try {
				tempEndDate = dateformatnewDateBuilder.parse(endDate);
			} catch (ParseException e) {
				logger.warn("Exception: "+tempEndDate+" ## : "+e.toString());

			}
			if(tempEndDate.compareTo(tempCurrDate)< 0){
				contractEnding = "Y";
			}
   		}
		if(contract.getBegin()!=null){
			Date tempBegin = contract.getBegin();
			String beginDate = "";
			StringBuilder newTempBegin = new StringBuilder(dateformatnewDateBuilder.format(tempBegin));
			beginDate = newTempBegin.toString();
			Date tempBeginDate=new Date();
			try {
				tempBeginDate = dateformatnewDateBuilder.parse(beginDate);
			} catch (ParseException e) {
				logger.warn("Exception: "+tempBeginDate+" ## : "+e.toString());

			}
			if(tempBeginDate.compareTo(tempCurrDate)> 0){
				contractEnding = "Y";
			}
		}
	
	agentList=contractManager.agentContract(sessionCorpID,contract.getContract());
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
}
	@SkipValidation
	public String findToolTipPublishAgents(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		agentList=contractManager.findAgentCodeList(agentCorpId); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	/*@SkipValidation
	 public String publishContractMethod() throws IllegalAccessException, InvocationTargetException{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" publishContractMethod() Start");
		
		//String checkDel="";
		List<String> contractArray = new ArrayList();
		contract = contractManager.get(Long.parseLong(contractid));
		contract.setPublished("Y");
		contractManager.save(contract);
		List tempCharegs = chargesManager.findChargesRespectToContract(contract.getContract(),sessionCorpID);
		List tempContactAccount = contractAccountManager.findContractAccountRespectToContract(contract.getContract(),sessionCorpID);
		//List ownerCostElement = contractManager.findCostElement(sessionCorpID);
		String[] agentsCorpId =publishContractId.split(",");
		//int tempCountChk = 0;
		//String invalidContractCorpId="";
		for(int i=0; i< agentsCorpId.length; i++){
			agentCorpIdName = agentsCorpId[i];
		int chkContractCount = contractManager.findContractCount(sessionCorpID,contract.getContract(),agentsCorpId[i],contract.getId());
		if(chkContractCount>0){
			if(tempCountChk==0){
				invalidContractCorpId=agentsCorpId[i];
			}else{
				invalidContractCorpId = invalidContractCorpId+" , "+agentsCorpId[i];
			}
			tempCountChk++;
			}
		//}

			if(tempCountChk > 0){
				errorMessage("Contract with the same name is available for the CorpID "+invalidContractCorpId+" .\n You can not publish this contract.");
			}else{
				contract.setPublished("Y");
				contractManager.save(contract);
			//for(int i=0; i< agentsCorpId.length; i++){
			// copy of contract 

				List count = contractManager.countRowOwner(sessionCorpID,contract.getId(),agentsCorpId[i]);
				if(count.size()>0){
					int countSize=0;
					Iterator itrContract = count.iterator();
					while(itrContract.hasNext()){
						String tempStr= itrContract.next().toString().trim();
						String tempIdArray[] = tempStr.split("~");
						contractArray.add(tempIdArray[1]);
						String  contract1=contractManager.getOwnerContract(Long.parseLong(tempIdArray[0].toString()));
						if(!contract1.equals("")){
						String[] tempArray = contract1.split("~");
						if((tempArray[0].equalsIgnoreCase(sessionCorpID))&&(tempArray[1].equalsIgnoreCase(contract.getId().toString()))){
							//contractManager.deleteRecords(new Long(count.get(countSize).toString()),agentsCorpId[i],"contract");
							contractManager.removeForOtherCorpid(Long.parseLong(tempIdArray[0].toString()));
							checkDel="yes";
						}}
						countSize++;
					}
				}
			  contractManager.findDeleteContractOfOtherCorpId(contract.getContract(),agentsCorpId[i]);
			  Contract newContract = new Contract();
			  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
			  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			  beanUtilsContract.copyProperties(newContract, contract);
			  newContract.setCorpID(agentsCorpId[i]);
			  newContract.setOwner(contract.getCorpID());
			  newContract.setOrigContract(contract.getId());
			  newContract.setCompanyDivision("");
			  newContract.setId(null);
			  newContract.setUpdatedOn(new Date());
			  contractManager.save(newContract);
			  
			  
			  //---------- Cost Element Copy  start------------\\
			  Iterator itrCost = ownerCostElement.iterator();
			  while(itrCost.hasNext()){
				CostElement cElementOwner = costElementManager.get(Long.parseLong(itrCost.next().toString()));
				List tempAgentCost = costElementManager.findAgentCostElement(cElementOwner.getCostElement(),agentsCorpId[i]);
				if(tempAgentCost.size()==0){
					CostElement cElementAgent = new CostElement();
					BeanUtilsBean beanUtilsCostElement = BeanUtilsBean.getInstance();
					beanUtilsCostElement.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					beanUtilsCostElement.copyProperties(cElementAgent, cElementOwner);
					cElementAgent.setPayGl("");
					cElementAgent.setRecGl("");
					cElementAgent.setCorpId(agentsCorpId[i]);
					cElementAgent.setId(null);
					costElementManager.save(cElementAgent);
				} 
			  }
			  
			  List agentCostElement = costElementManager.findAgentCostElementData(sessionCorpID,agentsCorpId[i]);
			  if(agentCostElement!=null && !agentCostElement.isEmpty()){
				  Iterator itrCost = agentCostElement.iterator();
					  while(itrCost.hasNext()){
						CostElement cElementOwner = costElementManager.get(Long.parseLong(itrCost.next().toString()));
						CostElement cElementAgent = new CostElement();
						BeanUtilsBean beanUtilsCostElement = BeanUtilsBean.getInstance();
						beanUtilsCostElement.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						beanUtilsCostElement.copyProperties(cElementAgent, cElementOwner);
						cElementAgent.setPayGl("");
						cElementAgent.setRecGl("");
						cElementAgent.setCorpId(agentsCorpId[i]);
						cElementAgent.setId(null);
						costElementManager.save(cElementAgent);
					}
			  }
			  
			  //---------- Cost Element Copy  ends------------\\ 
			  
			// deletion of change ,rate grid, and country deviation  of charge 
			 for(int j=0 ;j<contractArray.size() ;j++){
			  List countCharge = chargesManager.findChargesRespectToContract(contractArray.get(j).toString(),agentsCorpId[i]);
			  if(countCharge.size()>0 && checkDel.equals("yes")){
				  Iterator itr = countCharge.iterator();
				  while(itr.hasNext()){
					 // contractManager.deleteRecords(new Long(itr.next().toString()),agentsCorpId[i],"charges");
					 Long chargeid=Long.parseLong(itr.next().toString());
					  chargesManager.removeForOtherCorpid(chargeid);
					 List tempGrid = chargesManager.getAllGridForCharges(chargeid.toString(),agentsCorpId[i],contract.getContract());
					 if(tempGrid.size()>0 && checkDel.equals("yes")){
						  Iterator itrr = tempGrid.iterator();
						  while(itrr.hasNext()){
							  Long tempId  = Long.parseLong(itrr.next().toString());
							  //contractManager.deleteRecords(tempId,agentsCorpId[i],"rategrid");
							 rateGridManager.removeForOtherCorpid(tempId);
						  }
					  }
					 List agentDeviation = countryDeviationManager.findAllDeviation(chargeid.toString(),agentsCorpId[i], contract.getContract()); 
						if(agentDeviation.size()>0 && checkDel.equals("yes")){
							Iterator itrDeviation = agentDeviation.iterator();
							while(itrDeviation.hasNext()){
								Long tempDevId = Long.parseLong(itrDeviation.next().toString());
								 // contractManager.deleteRecords(tempDevId,agentsCorpId[i],"countrydeviation");
								countryDeviationManager.removeForOtherCorpid(tempDevId);
							}
						}
				  }
			  }
		}
			 
			 
			chargesManager.findDeleteChargeRateGridOfOtherCorpId(contract.getContract(),agentsCorpId[i]);
			 
			 
			 Map<String,String> costElementData = costElementManager.costElementDataMap(agentsCorpId[i]);
			  Iterator itrChagres = tempCharegs.iterator();
				while(itrChagres.hasNext()){
					String temp = itrChagres.next().toString();
					chargesObj = chargesManager.get(Long.parseLong(temp));
					Charges newCharges = new Charges();
					BeanUtilsBean beanUtilsCharges = BeanUtilsBean.getInstance();
				    beanUtilsCharges.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					beanUtilsCharges.copyProperties(newCharges, chargesObj);
					//List tempCostElementList = costElementManager.findCostElementList(chargesObj.getCostElement(), agentsCorpId[i],"","","");
					if(!tempCostElementList.isEmpty()){
						String tempCostElement = tempCostElementList.get(0).toString();
						if(!(tempCostElement.trim().equals("~"))){
							String tempCostElementArray[] = tempCostElement.split("~");
							try{
								newCharges.setGl(tempCostElementArray[0]);
								newCharges.setExpGl(tempCostElementArray[1]);
							}catch(Exception exception){
								newCharges.setGl("");
								newCharges.setExpGl("");
							}
						}else{
							newCharges.setGl("");
							newCharges.setExpGl("");
						}
					}else{
						newCharges.setGl("");
						newCharges.setExpGl("");
					}
					if(!costElementData.isEmpty()){
						try{
							String val = costElementData.get(chargesObj.getCostElement());
							if(val!=null && !val.equalsIgnoreCase("") && !val.equalsIgnoreCase("~")){
								String	costElementDataTemp[] = val.split("~");
								if(costElementDataTemp[0] !=null && (!(costElementDataTemp[0].toString().trim().equalsIgnoreCase("NODATA")))){
									newCharges.setGl(costElementDataTemp[0]);	
								}else{
									newCharges.setGl("");
								}
								if(costElementDataTemp[1] !=null && (!(costElementDataTemp[1].toString().trim().equalsIgnoreCase("NODATA")))){
									newCharges.setExpGl(costElementDataTemp[1]);
								}else{
									newCharges.setExpGl("");
								}
							}else{
								newCharges.setGl("");
								newCharges.setExpGl("");
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}else{
						newCharges.setGl("");
						newCharges.setExpGl("");
					}
					newCharges.setCorpID(agentsCorpId[i]);
					newCharges.setId(null);
					try{
						newCharges=chargesManager.save(newCharges);
					}catch(Exception e){
			        	e.printStackTrace();
			        }
					
				//}
				
				
			//	Iterator itrChagres1 = tempCharegs.iterator();
			//	while(itrChagres1.hasNext()){
				//	String temp = itrChagres1.next().toString();
					// copy of rate grid
					List tempParentGrid = chargesManager.getAllGridForCharges(temp, sessionCorpID, contract.getContract());
					 Iterator itrGrid = tempParentGrid.iterator();
					 while(itrGrid.hasNext()){
						 Long rateGridId = Long.parseLong(itrGrid.next().toString());
						 RateGrid grid = rateGridManager.get(rateGridId);
						 RateGrid newGrid = new RateGrid();
						 BeanUtilsBean beanUtilsGrid = BeanUtilsBean.getInstance();
						 beanUtilsGrid.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						 beanUtilsGrid.copyProperties(newGrid, grid);
						 newGrid.setCorpID(agentsCorpId[i]);
						 newGrid.setCharge(newCharges.getId().toString());
						 newGrid.setId(null);
						 rateGridManager.save(newGrid);
					 }
					// copy of CountryDeviation
					List tempCountryDeviation = countryDeviationManager.findAllDeviation(temp,sessionCorpID, contract.getContract());  
					 Iterator itrDev = tempCountryDeviation.iterator();
					 while(itrDev.hasNext()){
						 Long devId = Long.parseLong(itrDev.next().toString());
						 CountryDeviation countryDev = countryDeviationManager.get(devId);
						 CountryDeviation countryDevNew = new CountryDeviation();
						 BeanUtilsBean beanUtilsDev = BeanUtilsBean.getInstance();
						 beanUtilsDev.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						 beanUtilsDev.copyProperties(countryDevNew, countryDev);
						 countryDevNew.setCorpID(agentsCorpId[i]);
						 countryDevNew.setCharge(newCharges.getId().toString());
						 countryDevNew.setId(null);
						 countryDeviationManager.save(countryDevNew);
					 }
		     }
				// copy of Contract Account 
				  List countContractAccount = contractAccountManager.findContractAccountRespectToContract(contract.getContract(),agentsCorpId[i]);
				  if(countContractAccount.size()>0 && checkDel.equals("yes")){
					  Iterator itr = countContractAccount.iterator();
					  while(itr.hasNext()){
						  //contractManager.deleteRecords(Long.parseLong(itr.next().toString()),agentsCorpId[i],"contractaccount");
						  contractAccountManager.removeForOtherCorpid(Long.parseLong(itr.next().toString()));
					  }
				  }
				contractAccountManager.findDeleteContractAccountOfOtherCorpId(contract.getContract(),agentsCorpId[i]);
				  Iterator itrContractAccount = tempContactAccount.iterator();
					while(itrContractAccount.hasNext()){
						String temp = itrContractAccount.next().toString();
						ContractAccount contractAccountObj = contractAccountManager.get(Long.parseLong(temp));
						if(contractAccountObj.getAccountType()!= null && contractAccountObj.getAccountType().equals("Agent") && contractAccountObj.getCorpID().equals(sessionCorpID) && contractAccountObj.getAgentCorpID().equals(agentsCorpId[i])  ){
							contractAccountObj.setPublished("Y");
						}ContractAccount newContractAccount = new ContractAccount();
						BeanUtilsBean beanUtilsCharges = BeanUtilsBean.getInstance();
					    beanUtilsCharges.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						beanUtilsCharges.copyProperties(newContractAccount, contractAccountObj);
						newContractAccount.setCorpID(agentsCorpId[i]);
						newContractAccount.setId(null);
						contractAccountManager.save(newContractAccount);						
					}
			//}
		}
		
			// code for checking contract expiry
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
	   		contractEnding="";
	   		String strCrrDate="";
			StringBuilder tempCurrentDate = new StringBuilder(dateformatnewDateBuilder.format(currentdate));
			strCrrDate = tempCurrentDate.toString();
			Date tempCurrDate = new Date();
			try {
				tempCurrDate = dateformatnewDateBuilder.parse(strCrrDate);
			} catch (ParseException e) {
				logger.warn("Exception: "+strCrrDate+" ## : "+e.toString());

			}
			if(contract.getEnding()!=null){
				Date tempEnding = contract.getEnding();
				String endDate="";
				StringBuilder newTempEnding = new StringBuilder(dateformatnewDateBuilder.format(tempEnding));
				endDate = newTempEnding.toString();
				Date tempEndDate=new Date();
				try {
					tempEndDate = dateformatnewDateBuilder.parse(endDate);
				} catch (ParseException e) {
					logger.warn("Exception: "+tempEndDate+" ## : "+e.toString());

				}
				if(tempEndDate.compareTo(tempCurrDate)< 0){
					contractEnding = "Y";
				}
	   		}
			if(contract.getBegin()!=null){
				Date tempBegin = contract.getBegin();
				String beginDate = "";
				StringBuilder newTempBegin = new StringBuilder(dateformatnewDateBuilder.format(tempBegin));
				beginDate = newTempBegin.toString();
				Date tempBeginDate=new Date();
				try {
					tempBeginDate = dateformatnewDateBuilder.parse(beginDate);
				} catch (ParseException e) {
					logger.warn("Exception: "+tempBeginDate+" ## : "+e.toString());

				}
				if(tempBeginDate.compareTo(tempCurrDate)> 0){
					contractEnding = "Y";
				}
			}
			
			// expiry code end here
		agentList=contractManager.agentContract(sessionCorpID,contract.getContract());
		publishContractId="";
		if(tempCountChk==0){
			String key = "The contract has been published successfully";
			saveMessage(getText(key));
		}
		String key = "The contract has been published successfully";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" publishContractMethod() End");
		return SUCCESS ;
	 }*/
	
	@SkipValidation
	 public String publishContractMethod() throws IllegalAccessException, InvocationTargetException{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" publishContractMethod() Start");
		
		contract = contractManager.get(Long.parseLong(contractid));
		contract.setPublished("Y");
		contractManager.save(contract);
		
		String[] agentsCorpId =publishContractId.split(",");
		for(int i=0; i< agentsCorpId.length; i++){
			String returnVal = contractManager.callPublishContractProcedure(contract.getContract(),sessionCorpID,agentsCorpId[i],getRequest().getRemoteUser());
		}
		
			// code for checking contract expiry
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
	   		contractEnding="";
	   		String strCrrDate="";
			StringBuilder tempCurrentDate = new StringBuilder(dateformatnewDateBuilder.format(currentdate));
			strCrrDate = tempCurrentDate.toString();
			Date tempCurrDate = new Date();
			try {
				tempCurrDate = dateformatnewDateBuilder.parse(strCrrDate);
			} catch (ParseException e) {
				logger.warn("Exception: "+strCrrDate+" ## : "+e.toString());

			}
			if(contract.getEnding()!=null){
				Date tempEnding = contract.getEnding();
				String endDate="";
				StringBuilder newTempEnding = new StringBuilder(dateformatnewDateBuilder.format(tempEnding));
				endDate = newTempEnding.toString();
				Date tempEndDate=new Date();
				try {
					tempEndDate = dateformatnewDateBuilder.parse(endDate);
				} catch (ParseException e) {
					logger.warn("Exception: "+tempEndDate+" ## : "+e.toString());

				}
				if(tempEndDate.compareTo(tempCurrDate)< 0){
					contractEnding = "Y";
				}
	   		}
			if(contract.getBegin()!=null){
				Date tempBegin = contract.getBegin();
				String beginDate = "";
				StringBuilder newTempBegin = new StringBuilder(dateformatnewDateBuilder.format(tempBegin));
				beginDate = newTempBegin.toString();
				Date tempBeginDate=new Date();
				try {
					tempBeginDate = dateformatnewDateBuilder.parse(beginDate);
				} catch (ParseException e) {
					logger.warn("Exception: "+tempBeginDate+" ## : "+e.toString());

				}
				if(tempBeginDate.compareTo(tempCurrDate)> 0){
					contractEnding = "Y";
				}
			}
			
			// expiry code end here
		agentList=contractManager.agentContract(sessionCorpID,contract.getContract());
		publishContractId="";
		
		String key = "The contract has been published successfully";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" publishContractMethod() End");
		return SUCCESS ;
	 }
	
	
	private String presentCount;
	private String totalCount;
	private String contractName;
	private String agentCorpIdName;
	@SkipValidation
	public String publishContractProgressbar(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" publishContractProgressbar() Start");
			List totalChargeCount = chargesManager.getTotalChargeCount(contractName,sessionCorpID);
			if(totalChargeCount.get(0)!=null){
				totalCount=totalChargeCount.get(0).toString();
			}
			List presentChargeCount = chargesManager.getPresentChargeCountInAgentInstance(contractName,agentCorpIdName);
			if(presentChargeCount.get(0)!=null){
				presentCount=presentChargeCount.get(0).toString();
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" publishContractProgressbar() End");
		return SUCCESS ;
	}
	
	@SkipValidation
	public String agentContractAdds(){
		try{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
           
		if (id != null) {
			contract = contractManager.get(Long.parseLong(contractid));
			contactAgentType=contract.getContractType();
			contractAccount = contractAccountManager.get(id);
			// agentType1= refMasterManager.findByParameter(sessionCorpID, "AgentType");
			dummyAgentCode=contractAccount.getAccountCode();
			//contactAgentType=contractAccount.getAgentType();
		} else {
			// agentType1= refMasterManager.findByParameter(sessionCorpID, "AgentType");
			contractAccount = new ContractAccount();
			contract = contractManager.get(Long.parseLong(contractid));
			contractAccount.setContract(contract.getContract());
			contractAccount.setCreatedOn(new Date());
			contractAccount.setUpdatedOn(new Date());
			contactAgentType=contract.getContractType();
		}
		contractAccount.setCorpID(sessionCorpID);
		// contactAgentType=contract.getContractType();
		}catch(Exception ex){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		ex.getStackTrace();
		}
		return SUCCESS;	
	}
	@SkipValidation
  public String saveAgentContractForms()throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		// agentType1= refMasterManager.findByParameter(sessionCorpID, "AgentType");
		contract = contractManager.get(Long.parseLong(contractid));
	    boolean isNew = (contractAccount.getId() == null);
	if (isNew) {
		contractAccount.setCreatedOn(new Date());
	    contractAccount.setCreatedBy(getRequest().getRemoteUser());
	    contactAgentType=contract.getContractType();
	}
	contractAccount.setAccountType("Agent");
	contractAccount.setContract(contract.getContract());
	contractAccount.setCorpID(sessionCorpID);
	contractAccount.setUpdatedOn(new Date());
	contractAccount.setUpdatedBy(getRequest().getRemoteUser());
	contactAgentType=contract.getContractType();
	List isValid=contractAccountManager.validateAccountcode(contractAccount.getAccountCode(),sessionCorpID);
	if(isValid.isEmpty()){
		errorMessage("Agent code is not valid");
		//contractAccount.setAccountCode("");
		contractAccount.setAccountName("");
		return SUCCESS;
	}
	else{
		String lastName=isValid.get(0).toString();
		contractAccount.setAccountName(lastName);
	}
		
	List isexisted = contractAccountManager.isExisted(contractAccount.getAccountCode(), contractAccount.getContract(), sessionCorpID);
	if (!isexisted.isEmpty() && isNew) {
		errorMessage("Agent Code already exist for this Contract");
		contractAccount.setCreatedOn(new Date());
		contractAccount.setUpdatedOn(new Date());
		return SUCCESS;
	} 
	String dupDummyAgentCode=contractAccount.getAccountCode();
	if(!dummyAgentCode.equals(dupDummyAgentCode)){
		if (!isexisted.isEmpty()) {
			errorMessage("Agent Code already exist for this Contract");
			contractAccount.setCreatedOn(new Date());
			contractAccount.setUpdatedOn(new Date());
			return SUCCESS;
		} 
	}
		if(contract.getContractType()!=null && (!contract.getContractType().equalsIgnoreCase("CMM"))){
			contractAccount.setCmmFeeThreshold(0.0);
			contractAccount.setCmmFeeMax(0.0);
			contractAccount.setCmmFeeMin(0.0);
			contractAccount.setCmmFeePCT(0.0);
			}
		
		contractAccountManager.save(contractAccount);
		String key = (isNew) ? "contractAccount.added" : "contractAccount.updated";
		saveMessage(getText(key));
		if (!isNew) {
			flag = "true";
			return INPUT;
		} else {
			maxId = Long.parseLong(contractAccountManager.findMaximumId().get(0).toString());
			contractAccount = contractAccountManager.get(maxId);
			flag = "true";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	
	

	
	@SkipValidation
	public String editcontractAccountForm() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contract = contractManager.get(Long.parseLong(contractid));
		if (id != null) {
			contractAccount = contractAccountManager.get(id);

		} else {
			contractAccount = new ContractAccount();
			contractAccount.setContract(contract.getContract());
			contractAccount.setCreatedOn(new Date());
			contractAccount.setUpdatedOn(new Date());
		}
		contractAccount.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String saveContractAccountForm() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contract = contractManager.get(Long.parseLong(contractid));
		boolean isNew = (contractAccount.getId() == null);
		if (isNew) {
			contractAccount.setCreatedOn(new Date());
		}

		contractAccount.setCorpID(sessionCorpID);
		contractAccount.setAccountType("Account");
		contractAccount.setUpdatedOn(new Date());
		contractAccount.setUpdatedBy(getRequest().getRemoteUser());
		List isValid=contractAccountManager.validateAccountcode(contractAccount.getAccountCode(),sessionCorpID);
		if(isValid.isEmpty()){
			errorMessage("Account code is not valid");
			//contractAccount.setAccountCode("");
			contractAccount.setAccountName("");
			return SUCCESS;
		}
		else{
			String lastName=isValid.get(0).toString();
			contractAccount.setAccountName(lastName);
		}
			
		List isexisted = contractAccountManager.isExisted(contractAccount.getAccountCode(), contractAccount.getContract(), sessionCorpID);
		if (!isexisted.isEmpty() && isNew) {
			errorMessage("Account Code already exist for this Contract");
			contractAccount.setCreatedOn(new Date());
			contractAccount.setUpdatedOn(new Date());
			return SUCCESS;
		} else {
			contractAccountManager.save(contractAccount);
			String key = (isNew) ? "contractAccount.added" : "contractAccount.updated";
			saveMessage(getText(key));
			if (!isNew) {
				return INPUT;
			} else {
				maxId = Long.parseLong(contractAccountManager.findMaximumId().get(0).toString());
				contractAccount = contractAccountManager.get(maxId);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		}
	}

	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		validateFormNav = "OK";
		String s = save();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}
	
	@SkipValidation
	public String domSalesComm() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public void calculateComm() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List calcCommission = contractManager.calculateCommission(openfromdate, opentodate, sessionCorpID);
		
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(openfromdate));
		String fromDate = nowYYYYMMDD1.toString();
			
		StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD1.format(opentodate));
		String toDate = nowYYYYMMDD2.toString();
		
		String fileName = "DomesticSalesCommission"+fromDate+"_"+toDate+" .xls";
		try {
			if (!calcCommission.isEmpty()) {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename = "+ fileName );
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header = " Ship Number" + "\t" + " Shipper" + "\t" + "Job" + "\t" + "Estimator" + "\t" + "Booking Agent" + "\t"
				+ "Source"+ "\t" + "Charge"+ "\t" + "Distribution"+ "\t" + "Commission %"+ "\t" + "Commission"+ "\t" + "Description"+ "\t" + "Actual Load Date"+ "\t" 
				+ "Estimated Weight" + "\t" + "Actual Weight" + "\n";
				outputStream.write(header.getBytes());
				
				Iterator it = calcCommission.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					if (row[0] != null) {
						outputStream.write(("'" +row[0].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[1] != null) {
						outputStream.write((row[1].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[2] != null) {
						outputStream.write((row[2].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					/*if (row[3] != null) {
						outputStream.write((row[3].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}*/

					if (row[4] != null) {
						outputStream.write((row[4].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					/*if (row[5] != null) {
						outputStream.write((row[5].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					*/
					if (row[6] != null) {
						outputStream.write(("'" +row[6].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[7] != null) {
						outputStream.write((row[7].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[8] != null) {
						outputStream.write((row[8].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[9] != null) {
						outputStream.write((row[9].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[10] != null) {
						outputStream.write((row[10].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[11] != null) {
						outputStream.write((row[11].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[12] != null) {
						outputStream.write((row[12].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[13] != null) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
						StringBuilder date = new StringBuilder(sdf.format(row[13]));
						outputStream.write((date.toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[14] != null) {
						outputStream.write((row[14].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[15] != null) {
						outputStream.write((row[15].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					outputStream.write("\r\n".getBytes());
				}
			}else{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename = "+ fileName );
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header = "NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	@SkipValidation
	public String editCompensationSetup(){
		compensationList=compensationSetupManager.getListFromCompensationSetup(sessionCorpID,Long.parseLong(contractid),contractName);
		return SUCCESS;
	}
	@SkipValidation
	public String saveCompensationSetupLine(){
		
		try{
				 compensationSetup = new CompensationSetup(); 
				 compensationSetup.setCreatedBy(getRequest().getRemoteUser());
				 compensationSetup.setCreatedOn(new Date());
				 compensationSetup.setCorpid(sessionCorpID);		        		
				 compensationSetup.setUpdatedBy(getRequest().getRemoteUser());
				 compensationSetup.setUpdatedOn(new Date());
				 compensationSetup.setContract(contractName);
				 compensationSetup.setContractid(Long.parseLong(contractid));
				 compensationSetupManager.save(compensationSetup);
				 hitFlag="1";
		}
		catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	@SkipValidation
	public String saveUpdatesCompensationSetupLine(){
		try{
				compensationSetup=compensationSetupManager.get(id);
				String createdBy=compensationSetup.getCreatedBy();
				 Date CreatedOn=compensationSetup.getCreatedOn();
				 compensationSetup = new CompensationSetup();
				 compensationSetup.setCreatedBy(createdBy);
				 compensationSetup.setCreatedOn(CreatedOn);
				 compensationSetup.setCorpid(sessionCorpID);		        		
				 compensationSetup.setUpdatedBy(getRequest().getRemoteUser());
				 compensationSetup.setUpdatedOn(new Date());
				 compensationSetup.setContract(contractName);
				 compensationSetup.setContractid(Long.parseLong(contractid));
				 compensationSetup.setCommissionpercentage(commissionpercentage);
				 compensationSetup.setCompensationyear(compensationyear);
				 compensationSetup.setId(id);
				 compensationSetupManager.save(compensationSetup);
		}
		catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	
	
	public void setId(Long id) {
		this.id = id;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	public static Map<String, String> getJob() {
		return job;
	}

	public static void setJob(Map<String, String> job) {
		ContractAction.job = job;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setContractManager(ContractManager contractManager) {
		this.contractManager = contractManager;
	}

	public Set getcontracts() {
		return contracts;
	}

	public Map<String, String> getBillinginstruction() {
		return billinginstruction;
	}

	public void setBillinginstruction(Map<String, String> billinginstruction) {
		this.billinginstruction = billinginstruction;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public ContractAccount getContractAccount() {
		return contractAccount;
	}

	public void setContractAccount(ContractAccount contractAccount) {
		this.contractAccount = contractAccount;
	}

	public void setContractAccountManager(ContractAccountManager contractAccountManager) {
		this.contractAccountManager = contractAccountManager;
	}

	public String getContractid() {
		return contractid;
	}

	public void setContractid(String contractid) {
		this.contractid = contractid;
	}

	public List getContractAccountList() {
		return contractAccountList;
	}

	public void setContractAccountList(List contractAccountList) {
		this.contractAccountList = contractAccountList;
	}

	public Set getContractAccountSet() {
		return contractAccountSet;
	}

	public void setContractAccountSet(Set contractAccountSet) {
		this.contractAccountSet = contractAccountSet;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

	public List getChargess() {
		return chargess;
	}

	public void setChargess(List chargess) {
		this.chargess = chargess;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public Boolean getActiveCheck() {
		return activeCheck;
	}

	public void setActiveCheck(Boolean activeCheck) {
		this.activeCheck = activeCheck;
	}

	public List getMaterialsList() {
		return materialsList;
	}

	public void setMaterialsList(List materialsList) {
		this.materialsList = materialsList;
	}

	public void setItemsJEquipManager(ItemsJEquipManager itemsJEquipManager) {
		this.itemsJEquipManager = itemsJEquipManager;
	}

	public String getCont() {
		return cont;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public ItemsJEquip getItemsJEquip() {
		return itemsJEquip;
	}

	public void setItemsJEquip(ItemsJEquip itemsJEquip) {
		this.itemsJEquip = itemsJEquip;
	}

	public Date getOpenfromdate() {
		return openfromdate;
	}

	public void setOpenfromdate(Date openfromdate) {
		this.openfromdate = openfromdate;
	}

	public Date getOpentodate() {
		return opentodate;
	}

	public void setOpentodate(Date opentodate) {
		this.opentodate = opentodate;
	}

	public List getContractAccountByContract() {
		return contractAccountByContract;
	}

	public void setContractAccountByContract(List contractAccountByContract) {
		this.contractAccountByContract = contractAccountByContract;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public ContractAccount getContractAccountCopy() {
		return contractAccountCopy;
	}

	public void setContractAccountCopy(ContractAccount contractAccountCopy) {
		this.contractAccountCopy = contractAccountCopy;
	}

	public List getCopyContractAccount() {
		return copyContractAccount;
	}

	public void setCopyContractAccount(List copyContractAccount) {
		this.copyContractAccount = copyContractAccount;
	}

	public String getContractCopy() {
		return contractCopy;
	}

	public void setContractCopy(String contractCopy) {
		this.contractCopy = contractCopy;
	}

	public String getContractA() {
		return contractA;
	}

	public void setContractA(String contractA) {
		this.contractA = contractA;
	}

	public int getCountContractAccount() {
		return countContractAccount;
	}

	public void setCountContractAccount(int countContractAccount) {
		this.countContractAccount = countContractAccount;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the currencySign
	 */
	public String getCurrencySign() {
		return currencySign;
	}

	/**
	 * @param currencySign the currencySign to set
	 */
	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}

	/**
	 * @param companyManager the companyManager to set
	 */
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public List getAgentList() {
		return agentList;
	}

	public void setAgentList(List agentList) {
		this.agentList = agentList;
	}
	public Map<String, String> getAgentType1() {
		return agentType1;
	}

	public void setAgentType1(Map<String, String> agentType1) {
		this.agentType1 = agentType1;
	}
	public String getAgentTypeValue() {
		return agentTypeValue;
	}

	public void setAgentTypeValue(String agentTypeValue) {
		this.agentTypeValue = agentTypeValue;
	}
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Boolean getCheckUTSI() {
		return checkUTSI;
	}

	public void setCheckUTSI(Boolean checkUTSI) {
		this.checkUTSI = checkUTSI;
	}

	public String getContactAgentType() {
		return contactAgentType;
	}

	public void setContactAgentType(String contactAgentType) {
		this.contactAgentType = contactAgentType;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	
	public Map<String, String> getCurrency() {
		return currency;
	}

	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}

	public String getPublishContractId() {
		return publishContractId;
	}

	public void setPublishContractId(String publishContractId) {
		this.publishContractId = publishContractId;
	}

	public Charges getChargesObj() {
		return chargesObj;
	}

	public void setChargesObj(Charges chargesObj) {
		this.chargesObj = chargesObj;
	}

	public void setRateGridManager(RateGridManager rateGridManager) {
		this.rateGridManager = rateGridManager;
	}

	public void setCountryDeviationManager(
			CountryDeviationManager countryDeviationManager) {
		this.countryDeviationManager = countryDeviationManager;
	}

	public void setCostElementManager(CostElementManager costElementManager) {
		this.costElementManager = costElementManager;
	}

	public Long getDelAccountId() {
		return delAccountId;
	}

	public void setDelAccountId(Long delAccountId) {
		this.delAccountId = delAccountId;
	}

	public String getContractOrgId() {
		return contractOrgId;
	}

	public void setContractOrgId(String contractOrgId) {
		this.contractOrgId = contractOrgId;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getChildContract() {
		return childContract;
	}

	public void setChildContract(String childContract) {
		this.childContract = childContract;
	}

	public String getContractEnding() {
		return contractEnding;
	}

	public void setContractEnding(String contractEnding) {
		this.contractEnding = contractEnding;
	}

	public String getAgentCorpId() {
		return agentCorpId;
	}

	public void setAgentCorpId(String agentCorpId) {
		this.agentCorpId = agentCorpId;
	}

	public String getcCon() {
		return cCon;
	}

	public void setcCon(String cCon) {
		this.cCon = cCon;
	}

	public String getcDes() {
		return cDes;
	}

	public void setcDes(String cDes) {
		this.cDes = cDes;
	}

	public List getCheckContract() {
		return checkContract;
	}

	public void setCheckContract(List checkContract) {
		this.checkContract = checkContract;
	}

	public Map<String, String> getEuVatList() {
		return euVatList;
	}

	public void setEuVatList(Map<String, String> euVatList) {
		this.euVatList = euVatList;
	}

	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}

	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getPresentCount() {
		return presentCount;
	}

	public void setPresentCount(String presentCount) {
		this.presentCount = presentCount;
	}

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getAgentCorpIdName() {
		return agentCorpIdName;
	}

	public void setAgentCorpIdName(String agentCorpIdName) {
		this.agentCorpIdName = agentCorpIdName;
	}

	public List getCmmDmmAgentList() {
		return cmmDmmAgentList;
	}

	public void setCmmDmmAgentList(List cmmDmmAgentList) {
		this.cmmDmmAgentList = cmmDmmAgentList;
	}

	public List getCompensationList() {
		return compensationList;
	}

	public void setCompensationList(List compensationList) {
		this.compensationList = compensationList;
	}

	public void setCompensationSetupManager(
			CompensationSetupManager compensationSetupManager) {
		this.compensationSetupManager = compensationSetupManager;
	}

	public Integer getCompensationyear() {
		return compensationyear;
	}

	public void setCompensationyear(Integer compensationyear) {
		this.compensationyear = compensationyear;
	}

	public BigDecimal getCommissionpercentage() {
		return commissionpercentage;
	}

	public void setCommissionpercentage(BigDecimal commissionpercentage) {
		this.commissionpercentage = commissionpercentage;
	}

	public boolean isCheckFieldVisibility() {
		return checkFieldVisibility;
	}

	public void setCheckFieldVisibility(boolean checkFieldVisibility) {
		this.checkFieldVisibility = checkFieldVisibility;
	}

	public boolean isTenancyBillingContractType() {
		return tenancyBillingContractType;
	}

	public void setTenancyBillingContractType(boolean tenancyBillingContractType) {
		this.tenancyBillingContractType = tenancyBillingContractType;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getBaseCurrencyValue() {
		return baseCurrencyValue;
	}

	public void setBaseCurrencyValue(String baseCurrencyValue) {
		this.baseCurrencyValue = baseCurrencyValue;
	}

	public Map<String, String> getLead() {
		return lead;
	}

	public void setLead(Map<String, String> lead) {
		this.lead = lead;
	}

	public Map<String, String> getInsopt() {
		return insopt;
	}

	public void setInsopt(Map<String, String> insopt) {
		this.insopt = insopt;
	}

	public List getRatingScaleList() {
		return ratingScaleList;
	}

	public void setRatingScaleList(List ratingScaleList) {
		this.ratingScaleList = ratingScaleList;
	}

	public List getCustomerFeedbackList() {
		return customerFeedbackList;
	}

	public void setCustomerFeedbackList(List customerFeedbackList) {
		this.customerFeedbackList = customerFeedbackList;
	}

	public Map<String, String> getCreditTermsList() {
		return creditTermsList;
	}

	public void setCreditTermsList(Map<String, String> creditTermsList) {
		this.creditTermsList = creditTermsList;
	}

	public Map<String, String> getBillgrp() {
		return billgrp;
	}

	public void setBillgrp(Map<String, String> billgrp) {
		this.billgrp = billgrp;
	}

	public Map<String, String> getBillingMomentList() {
		return billingMomentList;
	}

	public void setBillingMomentList(Map<String, String> billingMomentList) {
		this.billingMomentList = billingMomentList;
	}

	public boolean isNetworkAgentFlag() {
		return networkAgentFlag;
	}

	public void setNetworkAgentFlag(boolean networkAgentFlag) {
		this.networkAgentFlag = networkAgentFlag;
	}

	public boolean isCmmDmmAgentFlag() {
		return cmmDmmAgentFlag;
	}

	public void setCmmDmmAgentFlag(boolean cmmDmmAgentFlag) {
		this.cmmDmmAgentFlag = cmmDmmAgentFlag;
	}

	public Map<String, String> getPrintOptions() {
		return printOptions;
	}

	public void setPrintOptions(Map<String, String> printOptions) {
		this.printOptions = printOptions;
	}
}