
delimiter $$

CREATE trigger redsky.trigger_add_history_reports
BEFORE UPDATE on redsky.reports
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   
   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'reports.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;







   IF (OLD.reportId <> NEW.reportId ) THEN
      select CONCAT(fieldNameList,'reports.reportId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportId,'~') into newValueList;
  END IF;







   IF (OLD.menu <> NEW.menu ) THEN
      select CONCAT(fieldNameList,'reports.menu~') into fieldNameList;
      select CONCAT(oldValueList,OLD.menu,'~') into oldValueList;
      select CONCAT(newValueList,NEW.menu,'~') into newValueList;
  END IF;






   IF (OLD.module <> NEW.module ) THEN
      select CONCAT(fieldNameList,'reports.module~') into fieldNameList;
      select CONCAT(oldValueList,OLD.module,'~') into oldValueList;
      select CONCAT(newValueList,NEW.module,'~') into newValueList;
  END IF;






     IF (OLD.enabled <> NEW.enabled ) THEN
      select CONCAT(fieldNameList,'reports.enabled~') into fieldNameList;
      select CONCAT(oldValueList,OLD.enabled,'~') into oldValueList;
      select CONCAT(newValueList,NEW.enabled,'~') into newValueList;
  END IF;





   IF (OLD.reportComment <> NEW.reportComment ) THEN
      select CONCAT(fieldNameList,'reports.reportComment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportComment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportComment,'~') into newValueList;
  END IF;





   
   IF (OLD.docsxfer <> NEW.docsxfer ) THEN
      select CONCAT(fieldNameList,'reports.docsxfer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.docsxfer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.docsxfer,'~') into newValueList;
  END IF;





   IF (OLD.location <> NEW.location ) THEN
      select CONCAT(fieldNameList,'reports.location~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location,'~') into newValueList;
  END IF;

    IF (OLD.fileContentType <> NEW.fileContentType ) THEN
      select CONCAT(fieldNameList,'reports.fileContentType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileContentType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileContentType,'~') into newValueList;
  END IF;
IF (OLD.agentRoles <> NEW.agentRoles ) THEN
      select CONCAT(fieldNameList,'reports.agentRoles~') into fieldNameList;
      select CONCAT(oldValueList,OLD.agentRoles,'~') into oldValueList;
      select CONCAT(newValueList,NEW.agentRoles,'~') into newValueList;
  END IF;
  IF (OLD.sequenceNo <> NEW.sequenceNo ) THEN
      select CONCAT(fieldNameList,'reports.sequenceNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sequenceNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sequenceNo,'~') into newValueList;
  END IF;
  IF (OLD.routing <> NEW.routing ) THEN
      select CONCAT(fieldNameList,'reports.routing~') into fieldNameList;
      select CONCAT(oldValueList,OLD.routing,'~') into oldValueList;
      select CONCAT(newValueList,NEW.routing,'~') into newValueList;
  END IF;
  
  IF (OLD.attachedURL <> NEW.attachedURL ) THEN
      select CONCAT(fieldNameList,'reports.attachedURL~') into fieldNameList;
      select CONCAT(oldValueList,OLD.attachedURL,'~') into oldValueList;
      select CONCAT(newValueList,NEW.attachedURL,'~') into newValueList;
  END IF;
  
  IF (OLD.formCondition <> NEW.formCondition ) THEN
      select CONCAT(fieldNameList,'reports.formCondition~') into fieldNameList;
      select CONCAT(oldValueList,OLD.formCondition,'~') into oldValueList;
      select CONCAT(newValueList,NEW.formCondition,'~') into newValueList;
  END IF;
 
  IF (OLD.formFieldName <> NEW.formFieldName ) THEN
      select CONCAT(fieldNameList,'reports.formFieldName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.formFieldName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.formFieldName,'~') into newValueList;
  END IF;
 
  IF (OLD.mode <> NEW.mode ) THEN
      select CONCAT(fieldNameList,'reports.mode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mode,'~') into newValueList;
  END IF;
  IF (OLD.billToCode <> NEW.billToCode ) THEN
      select CONCAT(fieldNameList,'reports.billToCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
  END IF;
  IF (OLD.job <> NEW.job ) THEN
      select CONCAT(fieldNameList,'reports.job~') into fieldNameList;
      select CONCAT(oldValueList,OLD.job,'~') into oldValueList;
      select CONCAT(newValueList,NEW.job,'~') into newValueList;
  END IF;
  
  IF (OLD.companyDivision <> NEW.companyDivision ) THEN
      select CONCAT(fieldNameList,'reports.companyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
  END IF;
  IF (OLD.runTimeParameterReq <> NEW.runTimeParameterReq ) THEN
      select CONCAT(fieldNameList,'reports.runTimeParameterReq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.runTimeParameterReq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.runTimeParameterReq,'~') into newValueList;
  END IF;
  IF (OLD.emailOut <> NEW.emailOut ) THEN
      select CONCAT(fieldNameList,'reports.emailOut~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailOut,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailOut,'~') into newValueList;
  END IF;
  IF (OLD.secureForm <> NEW.secureForm ) THEN
      select CONCAT(fieldNameList,'reports.secureForm~') into fieldNameList;
      select CONCAT(oldValueList,OLD.secureForm,'~') into oldValueList;
      select CONCAT(newValueList,NEW.secureForm,'~') into newValueList;
  END IF;
  IF (OLD.extract <> NEW.extract ) THEN
      select CONCAT(fieldNameList,'reports.extract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extract,'~') into newValueList;
  END IF;
  IF (OLD.rtf <> NEW.rtf ) THEN
      select CONCAT(fieldNameList,'reports.rtf~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rtf,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rtf,'~') into newValueList;
  END IF;
  IF (OLD.pdf <> NEW.pdf ) THEN
      select CONCAT(fieldNameList,'reports.pdf~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pdf,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pdf,'~') into newValueList;
  END IF;
  IF (OLD.html <> NEW.html ) THEN
      select CONCAT(fieldNameList,'reports.html~') into fieldNameList;
      select CONCAT(oldValueList,OLD.html,'~') into oldValueList;
      select CONCAT(newValueList,NEW.html,'~') into newValueList;
  END IF;
  IF (OLD.xls <> NEW.xls ) THEN
      select CONCAT(fieldNameList,'reports.xls~') into fieldNameList;
      select CONCAT(oldValueList,OLD.xls,'~') into oldValueList;
      select CONCAT(newValueList,NEW.xls,'~') into newValueList;
  END IF;
  IF (OLD.csv <> NEW.csv ) THEN
      select CONCAT(fieldNameList,'reports.csv~') into fieldNameList;
      select CONCAT(oldValueList,OLD.csv,'~') into oldValueList;
      select CONCAT(newValueList,NEW.csv,'~') into newValueList;
  END IF;
  IF (OLD.location <> NEW.location ) THEN
      select CONCAT(fieldNameList,'reports.location~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location,'~') into newValueList;
  END IF;
  IF (OLD.fileFileName <> NEW.fileFileName ) THEN
      select CONCAT(fieldNameList,'reports.fileFileName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileFileName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileFileName,'~') into newValueList;
  END IF;
  IF (OLD.fileContentType <> NEW.fileContentType ) THEN
      select CONCAT(fieldNameList,'reports.fileContentType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileContentType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileContentType,'~') into newValueList;
  END IF;
  IF (OLD.fileType <> NEW.fileType ) THEN
      select CONCAT(fieldNameList,'reports.fileType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileType,'~') into newValueList;
  END IF;
  IF (OLD.file <> NEW.file ) THEN
      select CONCAT(fieldNameList,'reports.file~') into fieldNameList;
      select CONCAT(oldValueList,OLD.file,'~') into oldValueList;
      select CONCAT(newValueList,NEW.file,'~') into newValueList;
  END IF;
  IF (OLD.subModule <> NEW.subModule ) THEN
      select CONCAT(fieldNameList,'reports.subModule~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subModule,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subModule,'~') into newValueList;
  END IF;
  IF (OLD.formReportFlag <> NEW.formReportFlag ) THEN
      select CONCAT(fieldNameList,'reports.formReportFlag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.formReportFlag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.formReportFlag,'~') into newValueList;
  END IF;
  

  IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'reports.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;
  IF (OLD.menu <> NEW.menu ) THEN
      select CONCAT(fieldNameList,'reports.menu~') into fieldNameList;
      select CONCAT(oldValueList,OLD.menu,'~') into oldValueList;
      select CONCAT(newValueList,NEW.menu,'~') into newValueList;
  END IF;
  IF (OLD.emailBody <> NEW.emailBody ) THEN
      select CONCAT(fieldNameList,'reports.emailBody~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailBody,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailBody,'~') into newValueList;
  END IF;
  IF (OLD.reportComment <> NEW.reportComment ) THEN
      select CONCAT(fieldNameList,'reports.reportComment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportComment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportComment,'~') into newValueList;
  END IF;
  IF (OLD.reportName <> NEW.reportName ) THEN
      select CONCAT(fieldNameList,'reports.reportName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportName,'~') into newValueList;
  END IF;
  
  
 
  IF (OLD.reportHits <> NEW.reportHits ) THEN
      select CONCAT(fieldNameList,'reports.reportHits~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportHits,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportHits,'~') into newValueList;
  END IF;




     IF (OLD.formReportFlag <> NEW.formReportFlag ) THEN
      select CONCAT(fieldNameList,'reports.formReportFlag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.formReportFlag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.formReportFlag,'~') into newValueList;
  END IF;


 

   
  

   IF (OLD.extract <> NEW.extract ) THEN
      select CONCAT(fieldNameList,'reports.extract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extract,'~') into newValueList;
  END IF;


   IF (OLD.targetCabinet <> NEW.targetCabinet ) THEN
      select CONCAT(fieldNameList,'reports.targetCabinet~') into fieldNameList;
      select CONCAT(oldValueList,OLD.targetCabinet,'~') into oldValueList;
      select CONCAT(newValueList,NEW.targetCabinet,'~') into newValueList;
  END IF;


  
      IF (OLD.formValue <> NEW.formValue ) THEN
      select CONCAT(fieldNameList,'reports.formValue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.formValue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.formValue,'~') into newValueList;
  END IF;
  
   IF (OLD.extractTheme <> NEW.extractTheme ) THEN
      select CONCAT(fieldNameList,'reports.extractTheme~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extractTheme,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extractTheme,'~') into newValueList;
  END IF;
  IF (OLD.autoupload <> NEW.autoupload ) THEN
       select CONCAT(fieldNameList,'reports.autoupload~') into fieldNameList;
       IF(OLD.autoupload = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.autoupload = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.autoupload = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.autoupload = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.excludeFromForm <> NEW.excludeFromForm ) THEN
       select CONCAT(fieldNameList,'reports.excludeFromForm~') into fieldNameList;
       IF(OLD.excludeFromForm = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFromForm = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFromForm = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFromForm = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.isMultiLingual <> NEW.isMultiLingual ) THEN
       select CONCAT(fieldNameList,'reports.isMultiLingual~') into fieldNameList;
       IF(OLD.isMultiLingual = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isMultiLingual = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isMultiLingual = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isMultiLingual = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     
  
   IF (OLD.docx <> NEW.docx ) THEN
      select CONCAT(fieldNameList,'reports.docx~') into fieldNameList;
      select CONCAT(oldValueList,OLD.docx,'~') into oldValueList;
      select CONCAT(newValueList,NEW.docx,'~') into newValueList;
  END IF;

 CALL add_tblHistory (OLD.id,"reports", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;