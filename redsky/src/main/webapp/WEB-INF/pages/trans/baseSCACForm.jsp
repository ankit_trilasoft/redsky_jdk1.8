<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="agentBaseSCACDetails.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentBaseSCACDetails.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>

</head> 
<style>
.mainDetailTable {
	width:650px;
}
</style>
<s:form id="agentBaseForm" action="saveBaseSCAC.html" method="post" validate="true">   
<s:hidden name="agentBaseSCAC.id" />
<s:hidden name="agentBaseSCAC.corpID" />
<s:hidden name="agentBaseSCAC.partnerCode" />
<s:hidden name="agentBaseSCAC.baseCode" />
<s:hidden name="baseId" value="${agentBase.id}"/>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" onclick="location.href='<c:url value="/editPartnerRatesForm.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<div id="newmnav">
	<ul>
		<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
		<c:if test="${sessionCorpID!='TSFT' }">
		<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=AG&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
		</c:if>
		<li><a href="baseList.html?id=${partner.id}"><span>Base List</span></a></li>
	  	<li><a href="editAgentBase.html?partnerId=${partner.id}&id=${agentBase.id}"><span>Base Details</span></a></li>
		<li><a href="baseSCACList.html?id=${agentBase.id}"><span>Agent Base SCAC List</span></a></li>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Base SCAC Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		<c:if test='${paramValue == "View"}'>
			<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
		</c:if>
		<c:if test='${paramValue != "View"}'>
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
	</ul>
</div>
<div style="width:100%">
<div class="spn">&nbsp;</div>
</div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhite" colspan="7"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
					  		<td align="right" class="listwhitetext">Partner Code</td>
							<td><s:textfield cssClass="input-textUpper" key="partner.partnerCode" required="true" readonly="true"/></td>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext">Partner Name</td>
							<td><s:textfield key="partner.lastName" cssClass="input-textUpper" required="true" size="50" readonly="true"/></td>
						</tr>
						
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='agentBase.baseCode'/></td>
							<td><s:textfield cssClass="input-textUpper" key="agentBase.baseCode" required="true" readonly="true"/></td>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='agentBase.description'/></td>
							<td><s:textfield cssClass="input-textUpper" name="agentBase.description"  size="50" readonly="true"/></td>
						</tr>
						
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='agentBaseSCAC.SCACCode'/></td>
							<td><s:select name="agentBaseSCAC.SCACCode" list="%{SCACList}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:95px"/></td>
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<c:set var="ischeckedInt" value="false"/>
							<c:if test="${agentBaseSCAC.internationalLOIflag}">
							<c:set var="ischeckedInt" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext"><fmt:message key='agentBaseSCAC.internationalLOIflag'/></td>
							<td align="left" class="listwhitetext" colspan="4">
							<table style="margin:0px;padding:0px;" cellspacing="0" cellpadding="0" >
							<tr>							
							<td><s:checkbox key="agentBaseSCAC.internationalLOIflag" value="${ischeckedInt}" fieldValue="true" onclick="changeStatus();" cssStyle="margin:0px;"/></td>
							<c:set var="ischeckedDom" value="false"/>
							<c:if test="${agentBaseSCAC.domesticLOIflag}">
							<c:set var="ischeckedDom" value="true"/>
							</c:if>	
							<td align="right" class="listwhitetext" width="185px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='agentBaseSCAC.domesticLOIflag'/><s:checkbox key="agentBaseSCAC.domesticLOIflag" value="${ischeckedDom}" fieldValue="true" onclick="changeStatus();" cssStyle="vertical-align:middle;" /></td>
						</tr>
						</table>
						</td>
						<tr>
						<td align="left" height="15"></td>
						</tr>	
					</tbody>
				</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
  
<table>
	<tbody>
		<tr>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdOn'/></b></td>
			<td valign="top"></td>
			<td style="width:120px">
				<fmt:formatDate var="agentBaseCreatedOnFormattedValue" value="${agentBaseSCAC.createdOn}" pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="agentBaseSCAC.createdOn" value="${agentBaseCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${agentBaseSCAC.createdOn}" pattern="${displayDateTimeFormat}"/>
			</td>		
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdBy' /></b></td>
			<c:if test="${not empty agentBaseSCAC.id}">
				<s:hidden name="agentBaseSCAC.createdBy"/>
				<td style="width:85px"><s:label name="createdBy" value="%{agentBaseSCAC.createdBy}"/></td>
			</c:if>
			<c:if test="${empty agentBaseSCAC.id}">
				<s:hidden name="agentBaseSCAC.createdBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
			
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedOn'/></b></td>
			<fmt:formatDate var="agentBaseupdatedOnFormattedValue" value="${agentBaseSCAC.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="agentBaseSCAC.updatedOn" value="${agentBaseupdatedOnFormattedValue}"/>
			<td style="width:120px"><fmt:formatDate value="${agentBaseSCAC.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedBy' /></b></td>
			<c:if test="${not empty agentBaseSCAC.id}">
				<s:hidden name="agentBaseSCAC.updatedBy"/>
				<td style="width:85px"><s:label name="updatedBy" value="%{agentBaseSCAC.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty agentBaseSCAC.id}">
				<s:hidden name="agentBaseSCAC.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
</table>               
        <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px"/>   
		
  
 
</s:form>

<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript">
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
}
</script>
<script type="text/javascript">    
    <c:if test="${hitflag == 1}" >
			<c:redirect url="/baseSCACList.html?id=${agentBase.id}"  />
	</c:if>
</script>