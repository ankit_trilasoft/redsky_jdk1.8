/**
 * Implementation of ExchangeRateManager interface.
 * Set the Dao for communication with the data layer.
 * @param 		dao
 * This class represents the basic "ExchangeRate" object in Redsky.
 * @Class Name	ExchangeRateManagerImpl
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        08-Dec-2008
 */

package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ExchangeRateDao;
import com.trilasoft.app.model.ExchangeRate;
import com.trilasoft.app.service.ExchangeRateManager;

public class ExchangeRateManagerImpl  extends GenericManagerImpl<ExchangeRate, Long> implements ExchangeRateManager { 
	ExchangeRateDao exchangeRateDao; 
    public ExchangeRateManagerImpl(ExchangeRateDao exchangeRateDao) { 
        super(exchangeRateDao); 
        this.exchangeRateDao = exchangeRateDao; 
    }
    		
	public List searchBaseCurrency(String corpID){
		return exchangeRateDao.searchBaseCurrency(corpID);
	}	
	public List findMaximumId(){
	    return exchangeRateDao.findMaximumId(); 
	}
	public List findAccExchangeRate(String corpId, String country) {
		return exchangeRateDao.findAccExchangeRate(corpId, country);
	}	
	public List searchAll(String currency){
		return exchangeRateDao.searchAll(currency);
	}  
	public List searchID(){
		return exchangeRateDao.searchID();
	}
	public Map<String, String> getExchangeRateWithCurrency(String corpID){
		return exchangeRateDao.getExchangeRateWithCurrency(corpID);
	}

	public Map<String, String> getAgentExchangeRateWithCurrency() {
		return exchangeRateDao.getAgentExchangeRateWithCurrency();
	}
	public List getExchangeHistoryList(String corpID)
	{
		return exchangeRateDao.getExchangeHistoryList(corpID);
	}
	
	public List searchAll(String currency ,String valueDate ){
		return exchangeRateDao.searchAll(currency , valueDate);
	}
	public List getHistoryFromid(Long id)
	{
		return exchangeRateDao.getHistoryFromid(id);
	}
	public Map<String, String> getExchangeHistoryListByDate(String corpID,String fxValueDate){
		return exchangeRateDao.getExchangeHistoryListByDate(corpID,fxValueDate);
	}
}
