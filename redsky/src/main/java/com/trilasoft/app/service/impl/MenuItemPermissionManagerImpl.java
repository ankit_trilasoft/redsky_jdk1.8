package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Set;


import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.MenuItemPermissionDao;
import com.trilasoft.app.model.MenuItemPermission;
import com.trilasoft.app.service.MenuItemPermissionManager;

public class MenuItemPermissionManagerImpl extends GenericManagerImpl<MenuItemPermission, Long> implements MenuItemPermissionManager {
	MenuItemPermissionDao menuItemPermissionDao; 
    
	 
    public MenuItemPermissionManagerImpl(MenuItemPermissionDao menuItemPermissionDao) { 
        super(menuItemPermissionDao); 
        this.menuItemPermissionDao = menuItemPermissionDao; 
    }

    public List<MenuItemPermission> findByCorpID(String corpId){
    	return menuItemPermissionDao.findByCorpID(corpId);
    }
   public List checkById(Long id) {
		return menuItemPermissionDao.checkById(id);
}
	public List<MenuItemPermission> findByUser(User user, Set<Role> roles) {
		return menuItemPermissionDao.findByUser(user, roles);
	}
	public List<User> findByUserPermission(String userName){
		return menuItemPermissionDao.findByUserPermission(userName);
	}

	public List<Long> findMenuIdsByUser(User user, Set<Role> roles) {
		return menuItemPermissionDao.findMenuIdsByUser(user, roles);
	}
}
