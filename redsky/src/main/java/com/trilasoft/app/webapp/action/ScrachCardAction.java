package com.trilasoft.app.webapp.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ScrachCard;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ScrachCardManager;

public class ScrachCardAction  extends BaseAction {

	private Long id;
	private String sessionCorpID;
	private ScrachCardManager scrachCardManager;
	private ScrachCard scrachCard;
	private CompanyManager companyManager;
	
	public ScrachCardAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ScrachCard getScrachCard() {
		return scrachCard;
	}

	public void setScrachCard(ScrachCard scrachCard) {
		this.scrachCard = scrachCard;
	}

	public void setScrachCardManager(ScrachCardManager scrachCardManager) {
		this.scrachCardManager = scrachCardManager;
	}
	
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	
	public String edit(){
		List scrachcardList = scrachCardManager.getScrachCardByUser(getRequest().getRemoteUser());
		if(scrachcardList == null || scrachcardList.isEmpty() || scrachcardList.get(0) == null || scrachcardList.get(0).toString().equalsIgnoreCase("")){
			scrachCard = new ScrachCard();
		}else{
			scrachCard = (ScrachCard) scrachCardManager.getScrachCardByUser(getRequest().getRemoteUser()).get(0);
		}
		return SUCCESS;
	}
	private String workDate;
	private String countScrachCard;
	private boolean checkFieldVisibility1=false;
	private boolean checkFieldVisibility2=false;
	public String shedulerResourcEdit(){
		Date date1 = null;
		if(!workDate.equalsIgnoreCase("")){
			 try {
			    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDate);
			    } catch (ParseException e) {
					
					e.printStackTrace();
				}
			}
		try{
			String permKey1 = sessionCorpID +"-"+"component.field.Alternative.showClipboardWithUser";
			checkFieldVisibility1=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey1) ;
			String permKey2 = sessionCorpID +"-"+"component.field.Alternative.showClipboardWithoutUser";
			checkFieldVisibility2=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey2) ;
				if(checkFieldVisibility1){
					List scrachcardList = scrachCardManager.getSchedulerScrachCardByUser(getRequest().getRemoteUser(),date1);
					
					if(scrachcardList == null || scrachcardList.isEmpty() || scrachcardList.get(0) == null || scrachcardList.get(0).toString().equalsIgnoreCase("")){
						countScrachCard="0";
					}else{
						scrachCard = (ScrachCard) scrachCardManager.getSchedulerScrachCardByUser(getRequest().getRemoteUser(),date1).get(0);
						countScrachCard="1";
					}
				}
				else if(checkFieldVisibility2)
				{
					List scrachcardList = scrachCardManager.getSchedulerScrachCardByDate(date1);
					
					if(scrachcardList == null || scrachcardList.isEmpty() || scrachcardList.get(0) == null || scrachcardList.get(0).toString().equalsIgnoreCase("")){
						countScrachCard="0";
					}else{
						scrachCard = (ScrachCard) scrachCardManager.getSchedulerScrachCardByDate(date1).get(0);
						countScrachCard="1";
					}
				}
		
		}catch(Exception e){
				e.printStackTrace();
			}	
		
		return SUCCESS;
	}
	public String save() throws Exception{
		boolean isNew = (scrachCard.getId() == null);  
		scrachCard.setCorpID(sessionCorpID);
		scrachCard.setUpdatedOn(new Date());
		/*Company company= companyManager.findByCorpID(sessionCorpID).get(0);
		TimeZone timeZones = TimeZone.getTimeZone( company.getTimeZone() );
        SimpleDateFormat timeZoneFormate = new SimpleDateFormat( "dd-MMM-yyyy hh:mm:ss" );
        timeZoneFormate.setTimeZone( timeZones );*/
		
		scrachCard.setNoteDetail(scrachCard.getNoteDetail());
		scrachCardManager.save(scrachCard);
		//String key = (isNew) ? "Event added successfully" : "Event updated successfully";   
        //saveMessage(getText(key));   
		if(!isNew){
			return INPUT;
		}else{
			return SUCCESS;
		}
	}

	public String getWorkDate() {
		return workDate;
	}

	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}

	public String getCountScrachCard() {
		return countScrachCard;
	}

	public void setCountScrachCard(String countScrachCard) {
		this.countScrachCard = countScrachCard;
	}

	

	public boolean isCheckFieldVisibility1() {
		return checkFieldVisibility1;
	}

	public void setCheckFieldVisibility1(boolean checkFieldVisibility1) {
		this.checkFieldVisibility1 = checkFieldVisibility1;
	}

	public boolean isCheckFieldVisibility2() {
		return checkFieldVisibility2;
	}

	public void setCheckFieldVisibility2(boolean checkFieldVisibility2) {
		this.checkFieldVisibility2 = checkFieldVisibility2;
	}

}
