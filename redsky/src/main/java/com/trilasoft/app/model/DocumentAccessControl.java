package com.trilasoft.app.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table ( name="documentaccesscontrol")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class DocumentAccessControl extends BaseObject {
	
	private Long id;
	private String corpID;
	private String fileType;
    private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private Boolean isCportal;
	private Boolean isAccportal;
	private Boolean isPartnerPortal;  
	private Boolean isBookingAgent;

	private Boolean isNetworkAgent;

	private Boolean isOriginAgent;

	private Boolean isSubOriginAgent;

	private Boolean isDestAgent;
	
	private Boolean isSubDestAgent;
	private Boolean isServiceProvider;
	private Boolean invoiceAttachment;
	private Boolean isvendorCode;
	private Boolean isPaymentStatus;
	private String fileDescription;
    
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("fileType", fileType)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("isCportal", isCportal)
				.append("isAccportal", isAccportal)
				.append("isPartnerPortal", isPartnerPortal)
				.append("isBookingAgent", isBookingAgent)
				.append("isNetworkAgent", isNetworkAgent)
				.append("isOriginAgent", isOriginAgent)
				.append("isSubOriginAgent", isSubOriginAgent)
				.append("isDestAgent", isDestAgent)
				.append("isSubDestAgent", isSubDestAgent)
				.append("isServiceProvider", isServiceProvider)
				.append("invoiceAttachment", invoiceAttachment)
				.append("isvendorCode", isvendorCode)
				.append("isPaymentStatus", isPaymentStatus)
				.append("fileDescription", fileDescription)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DocumentAccessControl))
			return false;
		DocumentAccessControl castOther = (DocumentAccessControl) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(fileType, castOther.fileType)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(isCportal, castOther.isCportal)
				.append(isAccportal, castOther.isAccportal)
				.append(isPartnerPortal, castOther.isPartnerPortal)
				.append(isBookingAgent, castOther.isBookingAgent)
				.append(isNetworkAgent, castOther.isNetworkAgent)
				.append(isOriginAgent, castOther.isOriginAgent)
				.append(isSubOriginAgent, castOther.isSubOriginAgent)
				.append(isDestAgent, castOther.isDestAgent)
				.append(isSubDestAgent, castOther.isSubDestAgent)
				.append(isServiceProvider, castOther.isServiceProvider)
				.append(invoiceAttachment, castOther.invoiceAttachment)
				.append(isvendorCode, castOther.isvendorCode)
				.append(isPaymentStatus, castOther.isPaymentStatus)
				.append(fileDescription, castOther.fileDescription)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(fileType)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(isCportal).append(isAccportal)
				.append(isPartnerPortal).append(isBookingAgent)
				.append(isNetworkAgent).append(isOriginAgent)
				.append(isSubOriginAgent).append(isDestAgent)
				.append(isSubDestAgent).append(isServiceProvider).append(invoiceAttachment)
				.append(isvendorCode)
				.append(isPaymentStatus)
				.append(fileDescription)
				.toHashCode();
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length=25 )
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	@Column( length=15 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column( length=1)
	public Boolean getIsCportal() {
		return isCportal;
	}
	public void setIsCportal(Boolean isCportal) {
		this.isCportal = isCportal;
	}
	@Column( length=1)
	public Boolean getIsAccportal() {
		return isAccportal;
	}
	public void setIsAccportal(Boolean isAccportal) {
		this.isAccportal = isAccportal;
	}
	@Column( length=1)
	public Boolean getIsPartnerPortal() {
		return isPartnerPortal;
	}
	public void setIsPartnerPortal(Boolean isPartnerPortal) {
		this.isPartnerPortal = isPartnerPortal;
	}
	@Column
	public Boolean getIsBookingAgent() {
		return isBookingAgent;
	}

	public void setIsBookingAgent(Boolean isBookingAgent) {
		this.isBookingAgent = isBookingAgent;
	}
	@Column
	public Boolean getIsNetworkAgent() {
		return isNetworkAgent;
	}

	public void setIsNetworkAgent(Boolean isNetworkAgent) {
		this.isNetworkAgent = isNetworkAgent;
	}
	@Column
	public Boolean getIsOriginAgent() {
		return isOriginAgent;
	}

	public void setIsOriginAgent(Boolean isOriginAgent) {
		this.isOriginAgent = isOriginAgent;
	}
	@Column
	public Boolean getIsSubOriginAgent() {
		return isSubOriginAgent;
	}

	public void setIsSubOriginAgent(Boolean isSubOriginAgent) {
		this.isSubOriginAgent = isSubOriginAgent;
	}
	@Column
	public Boolean getIsDestAgent() {
		return isDestAgent;
	}

	public void setIsDestAgent(Boolean isDestAgent) {
		this.isDestAgent = isDestAgent;
	}
	@Column
	public Boolean getIsSubDestAgent() {
		return isSubDestAgent;
	}

	public void setIsSubDestAgent(Boolean isSubDestAgent) {
		this.isSubDestAgent = isSubDestAgent;
	}

	public Boolean getIsServiceProvider() {
		return isServiceProvider;
	}

	public void setIsServiceProvider(Boolean isServiceProvider) {
		this.isServiceProvider = isServiceProvider;
	}

	public Boolean getInvoiceAttachment() {
		return invoiceAttachment;
	}

	public void setInvoiceAttachment(Boolean invoiceAttachment) {
		this.invoiceAttachment = invoiceAttachment;
	}
	@Column
	public Boolean getIsvendorCode() {
		return isvendorCode;
	}

	public void setIsvendorCode(Boolean isvendorCode) {
		this.isvendorCode = isvendorCode;
	}
	@Column
	public Boolean getIsPaymentStatus() {
		return isPaymentStatus;
	}

	public void setIsPaymentStatus(Boolean isPaymentStatus) {
		this.isPaymentStatus = isPaymentStatus;
	}

	public String getFileDescription() {
		return fileDescription;
	}

	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}
}
