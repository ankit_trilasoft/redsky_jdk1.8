package com.trilasoft.app.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.MenuItem;

public interface MenuItemManager extends GenericManager<MenuItem, Long> { 
	
	public List<MenuItem> findByCorpID(String corpId);
	public List findPermissions(String recipient,String corpId,String parentN,String menuN,String titleName,String urlN);
	public List countForMenuItem(Long id);
	public Boolean checkPermissions(Long menuItemId,int permission, String recipient,String sessionCorpID);
	public Boolean permissionExists(Long menuItemId,int permission, String recipient,String sessionCorpID,String url);
	public List <MenuItem>getMenuList(String corpId);
	public List allMenuItems();
	public List findCorpId();
	public List findMenuItem(String corp, String parName, String nm, String tit);
	public List findParent();
	public List findSeqNumList(String corp, String parent);
	public List checkTitleAvailability(String corp, String parent,String titleN);
	public List findParByCorp(String corp);
	public Collection findPageActions(String corp, String menuItemUrl);
	public List checkRolePermission(String url,String corpId);
	public List getPageActionListByUrl(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl);
	public List getPageActionListByUrlNew(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl);
	public List findPagePermissions(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl, String perRec);
	public void updatePageActionPermission(Long pageId,Long menuId,int permission,String corpId,String role,String url);
	public List getModuleActionListByURL(String module,String role,String corpId,String moduleDescr,String moduleUrl);
	public void updateModuleActionPermission(String sidL,String corpId,String oldSidL);
	public List getModuleList(String corpId);
	public void updatePageActionListByUrl(Long id,int permission,String corpId,String role);
	public Long getMenuItemPermissionId(Long menuId,String role,String corpId);
	public Boolean pagePermissionExists(Long menuItemPermissionId, int permission,String recipient,String childFlag,String corpId);
	public String getPageActionIdByUrl(String url,String menuName,String corpId);
	public void updatePageActionPermissionByList(String idList,Long menuItemPermisionid,int permisson,String role,String sessionCorpID);
	public List listAllMenuItems(String corpId,String parentN,String menuN,String titleName,String urlN,String pageUrl);
	public void updateMenuActionPermissionByList(Long menuId,String role,String sessionCorpID,int permission,String url);
	public Boolean isSecurityEnabled(String corpId);
	public void updatePageActionUrlList(String pageDescription,String sessionCorpId,String pageMethod,String pageClass,String pageModule,String pageUrl,String menuId,String pageId);
	public String getPageRecById(String pageId);
	public void removePageActionUrlList(String pageId);
	public void updateCurrentUrlPermission(String sessionCorpID);
}
