<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<SCRIPT LANGUAGE="JavaScript">
function findExchangeRateRateOnActualization(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
   var daym;
   var year=mydate.getFullYear()
   var y=""+year;
   if (year < 1000)
   year+=1900
   var day=mydate.getDay()
   var month=mydate.getMonth()+1
   if(month == 1)month="Jan";
   if(month == 2)month="Feb";
   if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function rateOnActualizationDate(field1,field2,field3){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
    <c:if test="${contractType}">	
	var currency=""; 
		var currentDate=currentDateRateOnActualization();
		try{
		currency=document.forms['accountLineForms'].elements[field1.trim().split('~')[0]].value;
		document.forms['accountLineForms'].elements[field2.trim().split('~')[0]].value=currentDate;
		document.forms['accountLineForms'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}

		try{
		currency=document.forms['accountLineForms'].elements[field1.trim().split('~')[1]].value;
		document.forms['accountLineForms'].elements[field2.trim().split('~')[1]].value=currentDate;
		document.forms['accountLineForms'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}		
	</c:if>
	</c:if>
}
function checkPayAcc() {
		alert('You can not change the Status as Sent To Acc has been already filled');
		document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].focus();
		document.getElementById("accountLineForms").reset(); 
}
function checkPayAccPayableContractCurrency(){
	alert('You can not change the Contract Currency as Sent To Acc has been already filled');
	document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].focus();
	document.getElementById("accountLineForms").reset();
}
function  checkPayAccCountry(){
	alert('You can not change the Currency as Sent To Acc has been already filled');
	document.forms['accountLineForms'].elements['accountLine.valueDate'].focus();
	document.getElementById("accountLineForms").reset();
}
function checkVendorName(){ 
    var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
	vendorId=vendorId.trim();
	if(vendorId == ''){
	    var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value 
	    if(actualExpense>0){
	    alert("You can not remove Partner Code as there is value in the payable details section");
		<c:if test="${not empty accountLine.id}">
		document.forms['accountLineForms'].elements['accountLine.vendorCode'].value='${accountLine.vendorCode}';
		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="${accountLine.estimateVendorName}";
		</c:if>
		<c:if test="${empty accountLine.id}">
		document.forms['accountLineForms'].reset();
		</c:if> 
	    }else {
		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
		} }
	if(vendorId != ''){ 
    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=vendorId;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    }  }
function checkVendorNameBranch(){ 
    var vendorId = document.forms['accountLineForms'].elements['accountLine.branchCode'].value;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse12;
    http2.send(null);
}
function handleHttpResponse12(){
	if (http2.readyState == 4){
           var results = http2.responseText
           results = results.trim();
           var res = results.split("#"); 
           if(res.length >= 2){ 
           		if(res[2] == 'Approved'){
           			document.forms['accountLineForms'].elements['accountLine.branchName'].value = res[1];
           		}else{
           			alert("Branch code is not approved" ); 
				    document.forms['accountLineForms'].elements['accountLine.branchName'].value="";
				    document.forms['accountLineForms'].elements['accountLine.branchCode'].value="";
			   		document.forms['accountLineForms'].elements['accountLine.branchCode'].select();
           		}			
           }else{
               alert("Branch code not valid");    
			   document.forms['accountLineForms'].elements['accountLine.branchName'].value="";
			   document.forms['accountLineForms'].elements['accountLine.branchCode'].value="";
			   document.forms['accountLineForms'].elements['accountLine.branchCode'].select();
		   }  } }
function findRadioValue(){
		var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
	<c:if test="${(accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0  &&  not empty accountLine.networkSynchedId) && !(trackingStatus.accNetworkGroup) )) || accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' || serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
    document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
	document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
	document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
	document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
	document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
</c:if>
<c:if test="${(accountLine.createdBy != 'Networking' && ( !(((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 ) &&  not empty accountLine.networkSynchedId ) && !(trackingStatus.accNetworkGroup) ))) && accountLine.chargeCode != 'DMMFEE' && accountLine.chargeCode != 'DMMFXFEE' &&  serviceOrder.status != 'CNCL' &&  serviceOrder.status != 'DWND' &&  serviceOrder.status != 'DWNLD'}">
     var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     if(rev == "Internal Cost"){
     if(charge=="SALESCM" || charge=="VANPAY"|| charge=="DOMCOMM") {
     activeIntCompute();
     }  else {
     var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value 
     var url="findRadioValueInternalCost.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision);
     http22.open("GET", url, true);
     http22.onreadystatechange = handleHttpResponse5555;
     http22.send(null);
     }
     } else{
     var url="invoiceExtracts2.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract);
     http22.open("GET", url, true);
     http22.onreadystatechange = handleHttpResponse55;
     http22.send(null);
     } </c:if> }}
function resetDisableCategory(){
var rev1='${accountLine.category}';
    if(rev1 == "Internal Cost"){
    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=0;
    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=0;
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=0;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recRate'].value=0; 
    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=0;
    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=0; 
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=0;
    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
    document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value=0;
    document.forms['accountLineForms'].elements['accountLine.distributionAmount'].readOnly=true;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=0; 
    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=0;
    <c:if test="${multiCurrency=='Y'}"> 
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=0; 
    </c:if>  
    document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='I';
    }
    <c:if test="${accountInterface=='Y'}">
    else if ((document.forms['accountLineForms'].elements['accountLine.recAccDate'].value!='' || (document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!='' && ${accNonEditFlag})) || ${utsiRecAccDateFlag} || ${utsiPayAccDateFlag} )  { 
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.itemNew'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.basisNew'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.basisNewType'].readOnly=true;  
    document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recXfer'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recXferUser'].readOnly=true; 
    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
    document.forms['accountLineForms'].elements['accountLine.distributionAmount'].readOnly=true;
    </c:if>
    <c:if test="${multiCurrency=='Y'}">
    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=true;
    </c:if>
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.contractCurrency'].disabled=true;
    document.forms['accountLineForms'].elements['accountLine.contractRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].readOnly=true;
    </c:if>
    } 
    </c:if>
    else{
    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=false; 
    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].readOnly=false; 
    <c:if test="${multiCurrency=='Y'}"> 
     document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=false;
     document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false;
     document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=false;
    </c:if> 
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.contractCurrency'].disabled=false;
    document.forms['accountLineForms'].elements['accountLine.contractRate'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].readOnly=false;
    </c:if>
    }
    document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='${accountLine.payingStatus}';
    document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}'; 
    accrualQuantitycheck('1');
    setVatExcluded('2') ;
}
function activeIntCompute() { 
 document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=false;
 document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=false;
 document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false;
 document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true; 
}
function findInternalCostVendorCode(temp){
     var corpid = document.forms['accountLineForms'].elements['accountLine.corpID'].value;
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value; 
     if(rev == "Internal Cost"){
     document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
     document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=""; 
     var url="findInternalCostVendorCode.html?ajax=1&decorator=simple&popup=true&companyDivision="+encodeURI(companyDivision);
     http222.open("GET", url, true);
     http222.onreadystatechange = function(){ handleHttpResponse555(temp);};
     http222.send(null);
    } }
function findRevisedQuantitys(){ 
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
     if(rev == "Internal Cost"){
     if(charge=="VANPAY" ){
      getVanPayCharges("Revision");
      } else if(charge=="DOMCOMM" ){
      getDomCommCharges("Revision");
      }
     else if(charge=="SALESCM" && comptetive=="Y") { 
     var totalExpense = eval(document.forms['accountLineForms'].elements['inRevisedTotalExpense'].value);
     var totalRevenue =eval(document.forms['accountLineForms'].elements['inRevisedTotalRevenue'].value); 
     var commisionRate =eval(document.forms['accountLineForms'].elements['salesCommisionRate'].value); 
     var grossMargin = eval(document.forms['accountLineForms'].elements['grossMarginThreshold'].value);
     var commision=((totalRevenue*commisionRate)/100);
     var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
     calGrossMargin=calGrossMargin*100;
     var calGrossMargin=Math.round(calGrossMargin);
     if(calGrossMargin>=grossMargin){
      document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=Math.round(commision*10000)/10000; 
      document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
      document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
      <c:if test="${contractType}">
      document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
      </c:if>	
     }else{ 
     commision=((.8*totalRevenue)-totalExpense);
     commision=Math.round(commision*10000)/10000;
     if(commision<0){
     document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=0;
     document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1; 
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
     </c:if>
     }else{
      document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=Math.round(commision*10000)/10000; 
      document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
      document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
      <c:if test="${contractType}">
      document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
      </c:if>
     } }  
     } else if(charge=="SALESCM" && comptetive!="Y") {
     var agree =confirm("This job is not competitive; do you still want to calculate commission?");
     if(agree) {
     var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value  
     var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
     http99.open("GET", url, true);
     http99.onreadystatechange = handleHttpResponse300;
     http99.send(null); 
     } else {
     return false;
     }
     } else {
     var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value  
     var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
     http99.open("GET", url, true);
     http99.onreadystatechange = handleHttpResponse300;
     http99.send(null);
     }
     }  else{
     var url="invoiceExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
     http99.open("GET", url, true);
     http99.onreadystatechange = handleHttpResponse3;
     http99.send(null); 
} }
function findRevisedQuantityLocalAmounts(){ 
	 var buildFormulaActualRevenue = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
     if(rev == "Internal Cost"){
     if(charge=="VANPAY" ){
      getVanPayCharges("Actual");
      } else if(charge=="DOMCOMM" ){
      getDomCommCharges("Actual");
      }
     else if(charge=="SALESCM" && comptetive=="Y") { 
     var totalExpense = eval(document.forms['accountLineForms'].elements['inActualExpense'].value);
     var totalRevenue =eval(document.forms['accountLineForms'].elements['inActualRevenue'].value); 
     var commisionRate =eval(document.forms['accountLineForms'].elements['salesCommisionRate'].value); 
     var grossMargin = eval(document.forms['accountLineForms'].elements['grossMarginThreshold'].value);
     var commision=((totalRevenue*commisionRate)/100);
     var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
     calGrossMargin=calGrossMargin*100;
     var calGrossMargin=Math.round(calGrossMargin);
     if(calGrossMargin>=grossMargin) {
     document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=Math.round(commision*10000)/10000; 
     document.forms['accountLineForms'].elements['accountLine.localAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
     document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=1; 
     }  else  { 
     commision=((.8*totalRevenue)-totalExpense);
     commision=Math.round(commision*10000)/10000;
     if(commision<0) {
     document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=0;
     document.forms['accountLineForms'].elements['accountLine.localAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
     document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=1;
     }  else {
     document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=Math.round(commision*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.localAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value; 
     document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=1;
     }  }  
     } else if(charge=="SALESCM" && comptetive!="Y") {
     var agree =confirm("This job is not competitive; do you still want to calculate commission?");
     if(agree)  { 
         var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value  
         var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
         http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse301;
	     http2.send(null); 
     } else {
     return false;
     }
     } else { 
         var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value  
         var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
         http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse301;
	     http2.send(null);
	     } }else{
           var url="invoiceExtractsPayable.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+"&buildFormulaActualRevenue=" + encodeURI(buildFormulaActualRevenue)+ "&sid=" +${serviceOrder.id}  ;
           http2.open("GET", url, true);
           http2.onreadystatechange = handleHttpResponsePayable;
           http2.send(null);
	     }  }
	 function handleHttpResponsePayable() {
	  if (http2.readyState == 4) {
                var results = http2.responseText
                 results = results.trim(); 
                if(document.forms['accountLineForms'].elements['accountLine.recAccDate'].value==''){
                <c:if test="${!contractType}"> 
                if(results !='') {
                var res = results.split("#");
                <configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">
                if(res[9]=="AskUser2"){
                var reply = prompt("Please Enter Quantity", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.')  {
                document.forms['accountLineForms'].elements['accountLine.payQuantity'].value =Math.round(reply*100)/100; 
              	} else {
              		alert("Please Enter legitimate Quantity");
              	} 
                }   else{
                document.forms['accountLineForms'].elements['accountLine.payQuantity'].value = "${accountLine.recQuantity}";
                }
                }    else{
                  if(res[7] == undefined){
                  document.forms['accountLineForms'].elements['accountLine.payQuantity'].value ="";
                  }else{ 
                document.forms['accountLineForms'].elements['accountLine.payQuantity'].value =Math.round(res[7]*100)/100;
               }  }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.')
                 {
                  //document.forms['accountLineForms'].elements['accountLine.payRate'].value = Math.round(reply*10000)/10000;
              	 } else {
              		alert("Please Enter legitimate Rate");
              	}
              	} else{
              	//document.forms['accountLineForms'].elements['accountLine.recRate'].value ="${accountLine.recRate}"; 
              	} 
                }  else{ 
                 if(res[8] == undefined){
                 //document.forms['accountLineForms'].elements['accountLine.recRate'].value =""; 
                 }else{
                //document.forms['accountLineForms'].elements['accountLine.recRate'].value =Math.round(res[8]*10000)/10000 ;
                }  }
               if(res[10]=="BuildFormula"){
               if(res[11] == undefined){
              // document.forms['accountLineForms'].elements['accountLine.recRate'].value = ""; 
               }else{ 
              // document.forms['accountLineForms'].elements['accountLine.recRate'].value = Math.round(res[11]*10000)/10000;
                }  }
               </configByCorp:fieldVisibility>
               var Q3 = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
               var Q4 = document.forms['accountLineForms'].elements['accountLine.recRate'].value;
               if(res[10]=="BuildFormula" && res[11] != undefined){
            	 Q4 =res[11];
               }
               var E1=Q3*Q4; 
               var Q5 = res[5];
               if(res[5] == undefined){
               //document.forms['accountLineForms'].elements['accountLine.basisNew'].value="";
               }else{
               //document.forms['accountLineForms'].elements['accountLine.basisNew'].value=Q5; 
               }
               var type = res[3];//document.forms['accountLineForms'].elements['accountLine.checkNew'].value;
               var E2= "";
               var E3= "";
               if(type == 'Division') {
               	 if(Q5==0) 	 {
               	   E2=0*1;
               	   //document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E2;
                }  else {	
               		E2=(Q3*Q4)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		//document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3;
               	 }    }
               if(type == ' ' || type == '')   {
               		E2=(Q3*Q4);
               		E3=Math.round(E2*10000)/10000;
               		//document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3;
               }
               if(type == 'Multiply')   {
               		E2=(Q3*Q4*Q5);
               		E3=Math.round(E2*10000)/10000;
               		//document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3; 
               } 
               if(res[2]=='') {
               } else {
               if(res[2] == undefined){
               ///document.forms['accountLineForms'].elements['accountLine.itemNew'].value = "";
               }else{
               //document.forms['accountLineForms'].elements['accountLine.itemNew'].value = res[2];
               }  }
               if(res[3]==' ') { 
               //document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
               } else{ 
               //document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
               }
               if(document.forms['accountLineForms'].elements['accountLine.description'].value.trim()=='') {
               if(res[4]=='')  {
               //fillDescription();
               } else{ 
                var chk = res[4];
				var chkReplace=(chk.replace('"','').replace('"',''));
				if(res[12]=='')
				{
					//document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace;
				}else{
               	///document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace+" ("+res[12]+")";
               	}  } }
               if(res[5]=='') {
               } else{
               if(res[5] == undefined)  {
               //document.forms['accountLineForms'].elements['accountLine.basisNew'].value = "";
               }  else{
               //document.forms['accountLineForms'].elements['accountLine.basisNew'].value = Q5;
               }  }
               if(res[6]=='') {
               } else{
               //document.forms['accountLineForms'].elements['accountLine.basisNewType'].value = res[6];
               }
               document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13]; 
               if(res[19]=='Y'){
                	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
                }else{document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; }
                setVatExcluded('1') ; 
               var chargedeviation=res[18];
               document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation; 
               if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
               //document.getElementById("receivableDeviationShow").style.display="block"; 
               document.getElementById("payableDeviationShow").style.display="block";   
               var buyDependSellAccount=res[15]
               var actualRevenue=0;
               var finalActualRevenue=0; 
               var finalActualExpenceAmount=0;
               var sellDeviation=0;  
               sellDeviation=res[16] 
               var buyDeviation=0; 
               buyDeviation=res[17] 
               actualRevenue=document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;  
               finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*10000)/10000;
               if(buyDependSellAccount=="Y"){
                finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*10000)/10000;
               }else{
                finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*10000)/10000;
               }
               //document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=finalActualRevenue; 
               ///document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=sellDeviation;
               //document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=sellDeviation;
               document.forms['accountLineForms'].elements['oldPayDeviation'].value=buyDeviation;
               document.forms['accountLineForms'].elements['accountLine.payDeviation'].value=buyDeviation;
               document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=finalActualExpenceAmount;
               convertLocalAmount('none');
                }else {
               //document.getElementById("receivableDeviationShow").style.display="none";
               document.getElementById("payableDeviationShow").style.display="none";
              // document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=0;
               document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=0; 
               var bayRate=0
               bayRate = res[20] ;
               if(bayRate!=0){
               var recQuantityPay=0;
               var payRateCheck=true;
               <configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">
               payRateCheck=false;
               recQuantityPay= document.forms['accountLineForms'].elements['accountLine.payQuantity'].value;
               document.forms['accountLineForms'].elements['accountLine.payRate'].value=bayRate;
               </configByCorp:fieldVisibility>
               if(payRateCheck){
               recQuantityPay= document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
               }
               Q3=recQuantityPay;
               Q4=bayRate;
               if(type == 'Division') {
               	 if(Q5==0) 	 {
               	   E2=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E2;
                }  else {	
               		E2=(Q3*Q4)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               	 }    }
               if(type == ' ' || type == '')   {
               		E2=(Q3*Q4);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               }
               if(type == 'Multiply')   {
               		E2=(Q3*Q4*Q5);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3; 
               } 
               convertLocalAmount('none'); 
               } } 
               <c:if test="${multiCurrency=='Y'}"> 
               var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
               var recRate=document.forms['accountLineForms'].elements['accountLine.recRate'].value
               var actualRevenueCal= document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
               var recCurrencyRate=recRate*recRateExchange;
               var roundValue=Math.round(recCurrencyRate*10000)/10000;
               //document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=roundValue ;
               var actualRevenueForeignCal=actualRevenueCal*recRateExchange; 
               actualRevenueForeignCal=Math.round(actualRevenueForeignCal*10000)/10000;
               //document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeignCal;
               
               var typeConversion01 = res[5];             
               var type01 = res[3];
               </c:if>               						                   
               //calculateVatAmt();
               //calculateVatAmtRevision();
               //calculateVatAmtEst();        
                 }     
                 </c:if>
                 <c:if test="${contractType}">
                 var type01='';
                 var typeConversion01=0;
                 if(results !='') {
                     var res = results.split("#"); 
                     <c:choose>
      		         <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
      		          if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value==res[21]){
      		    	   
      		       }else{
      		    	 if(res[21]!=undefined || res[21]!="undefined"){
      		    		document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[21];	
      		  		}else{
      		  		document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value="";
      		  		}
      		    	 
      		    	 //rateOnActualizationDate('accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');
      		       } 
      		       </c:when>
      		       <c:otherwise> 
                    // document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[21];
                     //findExchangeContractRateByCharges('receivable');
                     </c:otherwise>
        		     </c:choose>
                     if(res[9]=="AskUser2"){
                     var reply = prompt("Please Enter Quantity", "");
                     if(reply){
                     if((reply>0 || reply<9)|| reply=='.')  {
                     //document.forms['accountLineForms'].elements['accountLine.recQuantity'].value =Math.round(reply*100)/100; 
                   	} else {
                   		alert("Please Enter legitimate Quantity");
                   	} 
                     }   else{
                     //document.forms['accountLineForms'].elements['accountLine.recQuantity'].value = "${accountLine.recQuantity}";
                     }
                     }    else{
                       if(res[7] == undefined){
                       //document.forms['accountLineForms'].elements['accountLine.recQuantity'].value ="";
                       }else{ 
                     //document.forms['accountLineForms'].elements['accountLine.recQuantity'].value =Math.round(res[7]*100)/100;
                    }  }
                    if(res[10]=="AskUser"){
                     ///var reply = prompt("Please Enter Rate", "");
                     //if(reply){
                     ///if((reply>0 || reply<9)|| reply=='.')
                      //{
                      // document.forms['accountLineForms'].elements['accountLine.contractRate'].value = Math.round(reply*10000)/10000;
                   	 //} else {
                   		//alert("Please Enter legitimate Rate");
                   	//}
                   	//} else{
                   	//document.forms['accountLineForms'].elements['accountLine.contractRate'].value ="${accountLine.recRate}"; 
                   	//} 
                     }  else{ 
                      if(res[8] == undefined){
                      //document.forms['accountLineForms'].elements['accountLine.contractRate'].value =""; 
                      }else{
                     //document.forms['accountLineForms'].elements['accountLine.contractRate'].value =Math.round(res[8]*10000)/10000 ;
                     }  }
                    if(res[10]=="BuildFormula"){
                    if(res[11] == undefined){
                    //document.forms['accountLineForms'].elements['accountLine.contractRate'].value = ""; 
                    }else{ 
                    //document.forms['accountLineForms'].elements['accountLine.contractRate'].value = Math.round(res[11]*10000)/10000;
                     }  }
                    var Q3 = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
                    var Q4 = document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
                    if(res[10]=="BuildFormula" && res[11] != undefined){
                    	Q4 = res[11];
                    }
                    var E1=Q3*Q4; 
                    var Q5 = res[5];
                    if(res[5] == undefined){
                    //document.forms['accountLineForms'].elements['accountLine.basisNew'].value="";
                    }else{
                    //document.forms['accountLineForms'].elements['accountLine.basisNew'].value=Q5; 
                    }
                    var type = res[3];//document.forms['accountLineForms'].elements['accountLine.checkNew'].value;
                    var E2= "";
                    var E3= "";
                    if(type == 'Division') {
                    	 if(Q5==0) 	 {
                    	   E2=0*1;
                    	   //document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E2;
                     }  else {	
                    		E2=(Q3*Q4)/Q5;
                    		E3=Math.round(E2*10000)/10000;
                    		//document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E3;
                    	 }    }
                    if(type == ' ' || type == '')   {
                    		E2=(Q3*Q4);
                    		E3=Math.round(E2*10000)/10000;
                    		//document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E3;
                    }
                    if(type == 'Multiply')   {
                    		E2=(Q3*Q4*Q5);
                    		E3=Math.round(E2*10000)/10000;
                    		//document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E3; 
                    } 
                    if(res[2]=='') {
                    } else {
                    if(res[2] == undefined){
                    //document.forms['accountLineForms'].elements['accountLine.itemNew'].value = "";
                    }else{
                    //document.forms['accountLineForms'].elements['accountLine.itemNew'].value = res[2];
                    }  }
                    if(res[3]==' ') { 
                    //document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
                    } else{ 
                   // document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
                    } 
                    if(document.forms['accountLineForms'].elements['accountLine.description'].value.trim()=='') {
                    if(res[4]=='')  {
                    //fillDescription();
                    } else{ 
                     var chk = res[4];
     				var chkReplace=(chk.replace('"','').replace('"',''));
     				if(res[12]=='')
     				{
     					//document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace;
     				}else{
                    	//document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace+" ("+res[12]+")";
                    	}  } }
                    if(res[5]=='') {
                    } else{
                    if(res[5] == undefined)  {
                    //document.forms['accountLineForms'].elements['accountLine.basisNew'].value = "";
                    }  else{
                    //document.forms['accountLineForms'].elements['accountLine.basisNew'].value = Q5;
                    }  }
                    if(res[6]=='') {
                    } else{
                    //document.forms['accountLineForms'].elements['accountLine.basisNewType'].value = res[6];
                    }
                    //document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13]; 
                    if(res[19]=='Y'){
                     	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
                     }else{document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; }
                     setVatExcluded('1') ; 
                    var chargedeviation=res[18];
                    document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation; 
                    if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
                    document.getElementById("receivableDeviationShow").style.display="block"; 
                    document.getElementById("payableDeviationShow").style.display="block";   
                    var buyDependSellAccount=res[15]
                    var actualRevenue=0;
                    var finalActualRevenue=0; 
                    var finalActualExpenceAmount=0;
                    var contractRateAmmount=0;
                    var finalContractRateAmmount=0;
                    var payableContractRateAmmount=0;
                    var sellDeviation=0;  
                    sellDeviation=res[16] 
                    var buyDeviation=0; 
                    buyDeviation=res[17]
                    var contractExchangeRate=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
                    contractRateAmmount =document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value
                    actualRevenue=contractRateAmmount/contractExchangeRate;
                    actualRevenue=Math.round(actualRevenue*10000)/10000;  
                    //actualRevenue=document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;  
                    finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*10000)/10000;
                    finalContractRateAmmount=Math.round((contractRateAmmount*(sellDeviation/100))*10000)/10000;
                    //document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=finalContractRateAmmount; 
                    if(buyDependSellAccount=="Y"){
                     finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*10000)/10000;
                    }else{
                     finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*10000)/10000;
                    }
                    var payableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
                    payableContractRateAmmount=finalActualExpenceAmount*payableContractExchangeRate;
                    payableContractRateAmmount=Math.round(payableContractRateAmmount*10000)/10000;
                    document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value=payableContractRateAmmount;
                    //document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=finalActualRevenue; 
                    //document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=sellDeviation;
                    //document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=sellDeviation;
                    document.forms['accountLineForms'].elements['oldPayDeviation'].value=buyDeviation;
                    document.forms['accountLineForms'].elements['accountLine.payDeviation'].value=buyDeviation;
                    document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=finalActualExpenceAmount;
                   // convertLocalAmount('none');
                     }else {
                    //document.getElementById("receivableDeviationShow").style.display="none";
                    document.getElementById("payableDeviationShow").style.display="none";
                    document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=0;
                    document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=0; 
                    var bayRate=0
                    bayRate = res[20] ;
                    if(bayRate!=0){
                    	document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value=res[22]; 
                    	findPayableContractCurrencyExchangeRateByCharges('payable');   
                    var recQuantityPay=0;
                    recQuantityPay= document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
                    Q3=recQuantityPay;
                    Q4=bayRate;
                    if(type == 'Division') {
                    	 if(Q5==0) 	 {
                    	   E2=0*1;
                    	   document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E2;
                     }  else {	
                    		E2=(Q3*Q4)/Q5;
                    		E3=Math.round(E2*10000)/10000;
                    		document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3;
                    	 }    }
                    if(type == ' ' || type == '')   {
                    		E2=(Q3*Q4);
                    		E3=Math.round(E2*10000)/10000;
                    		document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3;
                    }
                    if(type == 'Multiply')   {
                    		E2=(Q3*Q4*Q5);
                    		E3=Math.round(E2*10000)/10000;
                    		document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3; 
                    } } }
                    typeConversion01 = res[5];             
                    type01 = res[3]; 
                    } 
                 //calculateRecRateByContractRate('none');
                 //calculateActualExpenseByContractRate('none');
                 var contractCurrencyCal =document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value; 
                 if(contractCurrencyCal!=''){
                 var contractExchangeRateCal=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
                 var contractRateCal=document.forms['accountLineForms'].elements['accountLine.contractRate'].value
                 var contractRateAmmountCal = document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value 
                 var recRateCal=contractRateCal/contractExchangeRateCal;
                 recRateCal=Math.round(recRateCal*10000)/10000;
                 var actualRevCal=contractRateAmmountCal/contractExchangeRateCal;
                 actualRevCal=Math.round(actualRevCal*10000)/10000;
                 //document.forms['accountLineForms'].elements['accountLine.recRate'].value=recRateCal ;
                 //document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=actualRevCal ;  
                  }  
                 var recRateCurrencyCal =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
                 if(recRateCurrencyCal!=''){
                	 var recRateExchangeCal=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
                     var recRateCal=document.forms['accountLineForms'].elements['accountLine.recRate'].value
                     var actualRevenueCal= document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
                     var recCurrencyRateCal=recRateCal*recRateExchangeCal;
                     recCurrencyRateCal=Math.round(recCurrencyRateCal*10000)/10000;
                    // document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=recCurrencyRateCal ;
                     var actualRevenueForeignCal=actualRevenueCal*recRateExchangeCal; 
                     actualRevenueForeignCal=Math.round(actualRevenueForeignCal*10000)/10000;
                     //document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeignCal;
                 }
                 var payableContractCurrencyCal =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value; 
     	         if(payableContractCurrencyCal!=''){
     	         var payableContractExchangeRateCal=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
     	         var payableContractRateAmmountCal=document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value
     	         var actualExpenseCal=payableContractRateAmmountCal/payableContractExchangeRateCal;
     	         actualExpenseCal=Math.round(actualExpenseCal*10000)/10000;
     	         document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=actualExpenseCal ; 
     	         }
     	        var exchangeRate= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
     	        var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value ;
     	        if(exchangeRate ==0) {
     	                document.forms['accountLineForms'].elements['accountLine.localAmount'].value=0*1;
     	        } else  {
     	            var localAmount=0;
     	            localAmount=exchangeRate*actualExpense
     	            document.forms['accountLineForms'].elements['accountLine.localAmount'].value=Math.round(localAmount*10000)/10000; 
     	            }
    	                 </c:if>
    	  	           <c:if test="${systemDefaultVatCalculation=='true'}">
    		           try{
    		       				calculatePayVatAmt();
    		       		      calculateESTVatAmt();
    		       		      calculateREVVatAmt();	      
    		       				
    		           }catch(e){}
    		           try{ 
    		               //calculateVatAmt();
    		               //calculateVatAmtRevision();
    		               //calculateVatAmtEst();        
    	                 }catch(e){}  
    		           </c:if>   
                  }else{
                	  <c:if test="${!contractType}"> 
                      if(results !='') {
                      var res = results.split("#");
                      var chargedeviation=res[18];
                      if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
                           }else {
                          //document.getElementById("receivableDeviationShow").style.display="none";
                          document.getElementById("payableDeviationShow").style.display="none";
                          //document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=0;
                         // document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=0; 
                          var bayRate=0
                          bayRate = res[20] ;
                          if(bayRate!=0){
                          var recQuantityPay=0;
                          if(res[9]=="AskUser2"){
                              var reply = prompt("Please Enter Quantity", "");
                              if(reply){
                              if((reply>0 || reply<9)|| reply=='.')  {
                            	  recQuantityPay =Math.round(reply*10000)/10000; 
                            	} else {
                            		alert("Please Enter legitimate Quantity");
                            	} 
                              }   else{
                            	  recQuantityPay = "${accountLine.recQuantity}";
                              }
                              }    else{
                                if(res[7] == undefined){
                                	
                                }else{ 
                                	recQuantityPay =Math.round(res[7]*10000)/10000;
                             }  }
                          //recQuantityPay= document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
                          var Q3="";
                          var Q4="";
                          var Q5 = res[5];                           
                          Q3=recQuantityPay;
                          Q4=bayRate;
                          var type = res[3];
                          var E2= "";
                          var E3= "";
                          if(type == 'Division') {
                          	 if(Q5==0) 	 {
                          	   E2=0*1;
                          	   document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E2;
                           }  else {	
                          		E2=(Q3*Q4)/Q5;
                          		E3=Math.round(E2*10000)/10000;
                          		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
                          	 }    }
                          if(type == ' ' || type == '')   {
                          		E2=(Q3*Q4);
                          		E3=Math.round(E2*10000)/10000;
                          		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
                          }
                          if(type == 'Multiply')   {
                          		E2=(Q3*Q4*Q5);
                          		E3=Math.round(E2*10000)/10000;
                          		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3; 
                          } 
                          convertLocalAmount('none');
                          }  }   }
                      </c:if>
                      <c:if test="${contractType}"> 
                      if(results !='') {
                          var res = results.split("#"); 
                          var chargedeviation=res[18]; 
                          if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){    
                          }else {
                              //document.getElementById("receivableDeviationShow").style.display="none";
                              document.getElementById("payableDeviationShow").style.display="none";
                              //document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=0;
                              //document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=0; 
                              var bayRate=0
                              bayRate = res[20] ;
                              if(bayRate!=0){
                              	document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value=res[22]; 
                              	findPayableContractCurrencyExchangeRateByCharges('payable');   
                              var recQuantityPay=0;
                              if(res[9]=="AskUser2"){
                                  var reply = prompt("Please Enter Quantity", "");
                                  if(reply){
                                  if((reply>0 || reply<9)|| reply=='.')  {
                                	  recQuantityPay =Math.round(reply*10000)/10000; 
                                	} else {
                                		alert("Please Enter legitimate Quantity");
                                	} 
                                  }   else{
                                	  recQuantityPay = "${accountLine.recQuantity}";
                                  }
                                  }    else{
                                    if(res[7] == undefined){
                                    	
                                    }else{ 
                                    	recQuantityPay =Math.round(res[7]*10000)/10000;
                                 }  }
                             // recQuantityPay= document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
                              var Q3="";
                              var Q4="";
                              var Q5 = res[5]; 
                              Q3=recQuantityPay;
                              Q4=bayRate;
                              var type = res[3];
                              var E2= "";
                              var E3= ""; 
                              if(type == 'Division') {
                              	 if(Q5==0) 	 {
                              	   E2=0*1;
                              	   document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E2;
                               }  else {	
                              		E2=(Q3*Q4)/Q5;
                              		E3=Math.round(E2*10000)/10000;
                              		document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3;
                              	 }    }
                              if(type == ' ' || type == '')   {
                              		E2=(Q3*Q4);
                              		E3=Math.round(E2*10000)/10000;
                              		document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3;
                              }
                              if(type == 'Multiply')   {
                              		E2=(Q3*Q4*Q5);
                              		E3=Math.round(E2*10000)/10000;
                              		document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3; 
                              }  }  }
                          calculateActualExpenseByContractRate('none');
                      }
                      </c:if> 
                 }  } }
  function copyRevisionExpense(){
  if(document.forms['accountLineForms'].elements['accountLine.category'].value=='Internal Cost'){
  		    document.forms['accountLineForms'].elements['accountLine.actualExpense'].value =document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value ; 
  		    var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value
  		    var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		    var localAmount=0;
  		    localAmount=actualExpense*exchangeRate;
  		    document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;  
  		    }   } 
  function fillRevQtyLocalAmounts(){
	  var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
		if(chargeCheck=='' || chargeCheck==null){
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
     charge=charge.trim(); 
     if(charge!="") { 
     if(rev == "Internal Cost" && charge!="SALESCM" && charge!="VANPAY" && charge!="DOMCOMM" ){
         var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value  
         var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
         http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponseFillRevQty;
	     http2.send(null);
	     } else if(rev == "Internal Cost"&&charge!="SALESCM" && (charge=="VANPAY" || charge=="DOMCOMM") ){ 
	     } } }}
 function handleHttpResponseFillRevQty() { 
             if (http2.readyState == 4)
             { 
                var results = http2.responseText
                 results = results.trim(); 
                if(results !=''){
                var res = results.split("#");
                if(res[10]=="Preset"){
                if(res[9]=="AskUser2")
                {
                var reply = prompt("Please Enter Quantity", "");
                if((reply>0 || reply<9)|| reply=='.') {
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = reply;
               } 	else {
              		alert("Please Enter legitimate Quantity");
              	} 
                } else{ 
                if(res[7] == undefined){
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = "";
                }else{                                           
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = res[7];
                    }  }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if((reply>0 || reply<9)|| reply=='.')  {
              	document.forms['accountLineForms'].elements['ratePaybleHid'].value = reply;
               } else {
              		alert("Please Enter legitimate Rate");
              	} 
                } else{
                  if(res[8] == undefined){ 
                  document.forms['accountLineForms'].elements['ratePaybleHid'].value = "";
                  }else{                                          
                document.forms['accountLineForms'].elements['ratePaybleHid'].value = res[8];
               }  }
               if(res[10]=="BuildFormula"){
               if(res[11] == undefined){
               document.forms['accountLineForms'].elements['ratePaybleHid'].value ="";
               }else{
               document.forms['accountLineForms'].elements['ratePaybleHid'].value = res[11]; 
               } 
               }
	           var Q2 = document.forms['accountLineForms'].elements['localAmountPaybleHid'].value;
               var Q1 = document.forms['accountLineForms'].elements['ratePaybleHid'].value;
               var E1="";
               if(Q1==0 && Q2==0)  {
               E1=0*1;
               }
               else if(Q2==0)
               {
               E1=0*1;
               } else {
               E1=Q1*Q2;  
               var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		       var localAmount=0;
  		       localAmount=E1*exchangeRate;
  		       document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;               
               document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E1;
               }
               var Q5 = res[5];            
               var type = res[3];
               var E2= "";
               var E3= ""; 
               if(type == 'Division')  {
               	 if(Q5==0)   {
               	   E2=0*1;
                   var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		           var localAmount=0;
  		           localAmount=E2*exchangeRate;
  		           document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               	   document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E2;
                } else {	
               		E2=(E1)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		            var localAmount=0;
  		            localAmount=E3*exchangeRate;
  		            document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
                }  }
               if(type == ' ' || type == '') {
               		E2=(E1);
               		E3=Math.round(E2*10000)/10000;
               		var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		            var localAmount=0;
  		            localAmount=E3*exchangeRate;
  		            document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               }
               if(type == 'Multiply') {
               		E2=(E1)*Q5;
               		E3=Math.round(E2*10000)/10000;
               		var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		            var localAmount=0;
  		            localAmount=E3*exchangeRate;
  		            document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
                } 
              document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value ="INT";
              document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='I'
        var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		 var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value!='') {
		   document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=datam;
		   document.forms['accountLineForms'].elements['accountLine.valueDate'].value=datam;
		   document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value=datam;
	     } else {
	     document.forms['accountLineForms'].elements['accountLine.valueDate'].value='';
	     document.forms['accountLineForms'].elements['accountLine.receivedDate'].value='';
	     document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value='';
	    } 
		findRevisedEstimateQuantitys();
        findRevisedQuantitys();  
      } }  }  }
function findRevisedReceivableQuantitys(){
	 document.forms['accountLineForms'].elements['checkComputeDescription'].value='';
	 var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     if(rev == "Internal Cost"){
     var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value  
     var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse900;
     http2.send(null);
     } else{
     var url="invoiceExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id}  ;
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse9;
     http2.send(null); 
     } }
function findGlTypeList(){
	 document.forms['accountLineForms'].elements['accountLine.glType'].value='';
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var url="getGlTypeList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber) ;
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse10;
     http3.send(null);
}
function handleHttpResponse10(){
            if (http3.readyState == 4) { 
              var results = http3.responseText
                results = results.trim();
                if(results.indexOf("@")==0) {
	  	    	results=results.substring(1);
	  		    }
                var res = results.split("@");
                if(res.length > 1)  {
	                var targetElement = document.forms['accountLineForms'].elements['accountLine.glType'];
					targetElement.length = res.length;
 					for(i=0;i<=res.length;i++) { 	
						document.forms['accountLineForms'].elements['accountLine.glType'].options[i].text = res[i];
						document.forms['accountLineForms'].elements['accountLine.glType'].options[i].value = res[i];
					}
				} else {
					var targetElement = document.forms['accountLineForms'].elements['accountLine.glType'];
					targetElement.length = res.length;
 					
 						document.forms['accountLineForms'].elements['accountLine.glType'].options[0].text = res[0];
						document.forms['accountLineForms'].elements['accountLine.glType'].options[0].value = res[0]; 
				} } }
function commissionList(){  
var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var url="getCommissionList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber) ;
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse11;
     http4.send(null); 
}
function handleHttpResponse11(){
           if (http4.readyState == 4)  {
                var results = http4.responseText
                results = results.trim();  
                if(results.indexOf("@")==0)  {
	  	    	results=results.substring(1);
	  		    } 
                var res = results.split("@");           
                if(res.length > 1) {
	                var targetElement = document.forms['accountLineForms'].elements['accountLine.commission'];
					targetElement.length = (res.length);
 					for(var i=0;i<res.length;i++) {
						document.forms['accountLineForms'].elements['accountLine.commission'].options[i].text = res[i];
						document.forms['accountLineForms'].elements['accountLine.commission'].options[i].value = res[i];
					} //if('${accountLine.commission}'==''){ 
					document.forms['accountLineForms'].elements['accountLine.commission'].options[0].selected=true; //}
				 } else {
					var targetElement = document.forms['accountLineForms'].elements['accountLine.commission'];
					targetElement.length = res.length;					
						document.forms['accountLineForms'].elements['accountLine.commission'].options[0].text = res[0];
						document.forms['accountLineForms'].elements['accountLine.commission'].options[0].value = res[0]; 		
				}<c:if test="${accountInterface=='Y'}">
                 if(document.forms['accountLineForms'].elements['partnerCommisionCode'].value!=''){
                       document.forms['accountLineForms'].elements['accountLine.commission'].value =document.forms['accountLineForms'].elements['partnerCommisionCode'].value;                        
                 }else{vendorCodeForActgCode();}      
                  </c:if> 
				} }
function findRevisedEntitleQuantitys(){ 
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev=document.forms['accountLineForms'].elements['accountLine.category'].value;
     if(rev == "Internal Cost"){
      var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value 
      var url="internalCostsExtracts3.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
      http2.open("GET", url, true);
      http2.onreadystatechange = handleHttpResponse6;
      http2.send(null);
      } else{
      var url="invoiceExtracts3.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
      http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);
      } }
function getVanPayCharges(temp){
 var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
 var url="getVanPayCharges.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
      http9.open("GET", url, true);
      http9.onreadystatechange =function(){ handleHttpResponseVanPayCharges(temp);}; ;
      http9.send(null);
}
function handleHttpResponseVanPayCharges(temp){
           if (http9.readyState == 4)   {
                var results = http9.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length > 0) {
                }else{
                results=0;
                }
                if(temp=='Estimate'){
                document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=results; 
                document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=results;
                document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
                document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
                </c:if>		
                }
                if(temp=='Revision'){
                document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=results; 
                document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=results;  
                document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
                </c:if>
                }
                if(temp=='Actual'){
                document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=results; 
                document.forms['accountLineForms'].elements['accountLine.localAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
                document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=1;  
                } } }
function getDomCommCharges(temp){
var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
if(comptetive=="Y"){
 var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
 var job=document.forms['accountLineForms'].elements['serviceOrder.Job'].value;
 var url="getDomCommCharges.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&job="+encodeURI(job);
      http9.open("GET", url, true);
      http9.onreadystatechange =function(){ handleHttpResponseVanPayCharges(temp);}; ;
      http9.send(null);
      } }
function handleHttpResponseDomCommCharges(temp){
           if (http9.readyState == 4)   {
                var results = http9.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length > 0)  {
                }else{
                results=0;
                }
                if(temp=='Estimate'){
                document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=results; 
                document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=results;
                document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
                document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
                </c:if>	
                }
                if(temp=='Revision'){
                document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=results; 
                document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=results;  
                document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
                </c:if>
                }
                if(temp=='Actual'){
                document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=results; 
                document.forms['accountLineForms'].elements['accountLine.localAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
                document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=1;  
                }  }  }
function findRevisedEstimateQuantitys(){ 
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = '${billing.contract}';
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev=document.forms['accountLineForms'].elements['accountLine.category'].value;
     var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
      if(rev == "Internal Cost"){
      if(charge=="VANPAY" ){
      getVanPayCharges("Estimate");
      }else if(charge=="DOMCOMM" ){
      getDomCommCharges("Estimate");
      }
     else if(charge=="SALESCM" && comptetive=="Y") { 
     var totalExpense = eval(document.forms['accountLineForms'].elements['inEstimatedTotalExpense'].value);
     var totalRevenue =eval(document.forms['accountLineForms'].elements['inEstimatedTotalRevenue'].value); 
     var commisionRate =eval(document.forms['accountLineForms'].elements['salesCommisionRate'].value); 
     var grossMargin = eval(document.forms['accountLineForms'].elements['grossMarginThreshold'].value);
     var commision=((totalRevenue*commisionRate)/100);
     var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
     calGrossMargin=calGrossMargin*100;
     var calGrossMargin=Math.round(calGrossMargin);
     if(calGrossMargin>=grossMargin)  {
     document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=Math.round(commision*10000)/10000; 
     document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
     } else { 
     commision=((.8*totalRevenue)-totalExpense); 
     commision=Math.round(commision*10000)/10000;
     if(commision<0) { 
     document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=0;
     document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1; 
     } else {
     document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=Math.round(commision*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.basis'].value='each';
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
     } }  
     } else if(charge=="SALESCM" && comptetive!="Y") {
     var agree =confirm("This job is not competitive; do you still want to calculate commission?");
     if(agree) {
     var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value 
     var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
     http88.open("GET", url, true);
     http88.onreadystatechange = handleHttpResponse700;
     http88.send(null);
     } else {
     return false;
     }
     } else {
     var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value 
     var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
     http88.open("GET", url, true);
     http88.onreadystatechange = handleHttpResponse700;
     http88.send(null);
     }
} else{	
 var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
     http88.open("GET", url, true);
     http88.onreadystatechange = handleHttpResponse7;
     http88.send(null);
} } 
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}
function handleHttpResponse2(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
		           if(res.length >= 2){ 
		           		if(res[2] == 'Approved'){
		           			document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value = res[1];
		           			document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
		           			AddDocumentDetails(); 
			                	if((res[7]!=null)&&(res[7]!='')&&(res[7]!=' ')){
			                  <c:if test="${contractType}">		                	
				                document.forms['accountLineForms'].elements['accountLine.estCurrency'].value=res[7];
				                document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value=res[7];
				                document.forms['accountLineForms'].elements['accountLine.country'].value=res[7];
				                </c:if>
			                	}           	
		           		}else{
		           			alert("Vendor code is not approved." ); 
						    document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
						    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
						    document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
		           		}
                }else{
                     alert("Vendor code not valid." );
                     document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
				 	 document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
				 	 document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
			   } } }
function handleHttpResponse55() {     
      if (http22.readyState == 4) {
                 var results22 = http22.responseText
                 results22 = results22.trim(); 
                 var res = results22.split("#"); 
                 var chargeCode = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
                 var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;  
                if((rev != "Internal Cost")&&(rev != " ") && (chargeCode !="")) { 
                	if((res[0] != "" )&&(res[0] != undefined)&&(res[1] !=undefined) && (res[1] !="")) { 
                  		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=false;
                  		<c:if test="${accountInterface=='Y'}"> 
                  		if(((!${accNonEditFlag} && document.forms['accountLineForms'].elements['accountLine.recAccDate'].value=='') || (${accNonEditFlag} && document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value=='') )&& !${utsiRecAccDateFlag} && !${utsiPayAccDateFlag}){
                  		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=false;
                  		}
                  		</c:if>
                  		<c:if test="${accountInterface!='Y'}"> 
                  		if(document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value==''){
                  		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=false;
                  		}
                  		</c:if>
                  		 if((res[4]!='' && res[4]!='NCT' && res[4]!=undefined)|| res[6]!=0 || res[7]!=0){
                  			<c:if test="${accountInterface=='Y'}"> 
                      		if(((document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=='' || document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value=='') && (document.forms['accountLineForms'].elements['accountLine.payPostDate'].value=='' && document.forms['accountLineForms'].elements['accountLine.payingStatus'].value!='A') && ${accNonEditFlag} ) || (!${accNonEditFlag} && document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='' )){
                      		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false;
                      		}
                      		</c:if>
                      		<c:if test="${accountInterface!='Y'}">  
                      		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false; 
                      		</c:if>	  
                  		//document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false;
                  		}else{document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;}
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	} 
                	if((res[2] != "")&&(res[2] != undefined)&&(res[1] !=undefined) && (res[1] !="")) {  
                  		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                	} 
                	if((res[1] != "" )&&(res[1] != undefined)&&(res[3] !=undefined) && (res[3] != "")) {
                  		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                	} }
                else if((rev == "Internal Cost") && (chargeCode !=""))   { 
                  	if((res[0] != "" )&&(res[0] != undefined)&&(res[1] !=undefined) && (res[1] !="")) { 
                  		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=false;
                  		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                  		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	} 
                	if((res[2] != "")&&(res[2] != undefined)&&(res[1] !=undefined) && (res[1] !="")) {  
                  		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                	} 
                	if((res[1] != "" )&&(res[1] != undefined)&&(res[3] !=undefined) && (res[3] != "")) 	{
                  		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                	}
                } else {
                		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	} 
                <c:if test="${((!(trackingStatus.soNetworkGroup))  && billingCMMContractType && networkAgent )}"> 
                document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true; 
                </c:if>        
    }
    accrualQuantitycheck('1');
}
function handleHttpResponse5555() { 
             if (http22.readyState == 4) {
                 var results22 = http22.responseText
                 results22 = results22.trim();  
                 var res = results22.split("#"); 
                 var chargeCode = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
                 var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;  
                if((rev != "Internal Cost")&&(rev != " ") && (chargeCode !=""))    { 
                	if((res[0] != "" )&&(res[0] != undefined)&&(res[1] !=undefined) && (res[1] !="")) { 
                  		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=false;
                  		if(((!accNonEditFlag && document.forms['accountLineForms'].elements['accountLine.recAccDate'].value=='') || (accNonEditFlag && document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value=='') ) && !${utsiRecAccDateFlag} && ! ${utsiPayAccDateFlag}){
                  		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=false;
                  		}
                  		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	} 
                	if((res[2] != "")&&(res[2] != undefined)&&(res[1] !=undefined) && (res[1] !="")) {  
                  		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                	} 
                	if((res[1] != "" )&&(res[1] != undefined)&&(res[3] !=undefined) && (res[3] != "")) {
                  		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                	}
                	} else if((rev == "Internal Cost") && (chargeCode !=""))
                { 
                  	if((res[0] != "" )&&(res[0] != undefined)&&(res[1] !=undefined) && (res[1] !="")) { 
                  		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=false;
                  		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                  		<c:if test="${accountInterface=='Y'}"> 
                  		if(document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='' ){
                  		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false;
                  		}
                  		</c:if>
                  		<c:if test="${accountInterface!='Y'}">  
                  		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=false; 
                  		</c:if>	
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	} 
                	if((res[2] != "")&&(res[2] != undefined)&&(res[1] !=undefined) && (res[1] !="")) {  
                  		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                	} 
                	if((res[1] != "" )&&(res[1] != undefined)&&(res[3] !=undefined) && (res[3] != "")) {
                  		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=false;
                	} else {
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                	}
                } else {
                		document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute4'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	}  
            	   <c:if test="${((!(trackingStatus.soNetworkGroup))  && billingCMMContractType && networkAgent )}"> 
                	document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
                	</c:if>  }
    accrualQuantitycheck('1');
}
function handleHttpResponse555(temp) { 
             if (http222.readyState == 4)  {
                 var result= http222.responseText
                 result = result.trim();   
                 document.forms['accountLineForms'].elements['accountLine.vendorCode'].value = result;      
                 if(result !="" && temp=='2'){
                  <c:if test="${accountInterface!='Y'}">
                  checkVendorName();
                  </c:if>
                  <c:if test="${accountInterface=='Y'}">
                  vendorCodeForActgCode();
                  </c:if>
                  }  } }
function checkFloatNew(formName,fieldName,message)
{   var i;
	var s = document.forms[formName].elements[fieldName].value;
	var count = 0;
	if(s.trim()=='.')
	{
		document.forms[formName].elements[fieldName].value='0.00';
	}else{
    for (i = 0; i < s.length; i++)  {   
        var c = s.charAt(i); 
        if(c == '.') {
        	count = count+1
        }
        if ((c > '9') && (c != '.') || (count>'1')) {
	        document.forms[formName].elements[fieldName].value='0.00';
        }  }    } }
function handleHttpResponse7() { 
             if (http88.readyState == 4)  {
                 var results = http88.responseText
                 results = results.trim(); 
                 <c:if test="${!contractType}"> 
                 var res = results.split("#");
                 var multquantity=res[14]
                if(res[3]=="AskUser1"){ 
                 var reply = prompt("Please Enter Quantity", "");
                 if(reply) {
                 if((reply>0 || reply<9)|| reply=='.')  {
              	document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = Math.round(reply*10000)/10000;
              	} else {
              		alert("Please Enter legitimate Quantity");
              	} }
              	else{
              	document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = "${accountLine.estimateQuantity}";
              	}
              }else{             
              if(res[1] == undefined){
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value ="";
              }else{
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = Math.round(res[1]*10000)/10000;
              }  }
              if(res[5]=="AskUser"){ 
              var reply = prompt("Please Enter the value of Rate", "");
              if(reply)  {
              if((reply>0 || reply<9)|| reply=='.') {
              	document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value = Math.round(reply*10000)/10000;
              	} else {
              		alert("Please Enter legitimate Rate");
              	} }
              	else{
              	document.forms['accountLineForms'].elements['accountLine.estimateRate'].value = "${accountLine.estimateRate}";
              	} 
              }else{
              if(res[4] == undefined){
              document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value ="";
              }else{
              document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value =Math.round(res[4]*10000)/10000 ;
              }  }
              if(res[5]=="BuildFormula")   {
              if(res[6] == undefined){
              document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value ="";
              }else{
               document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value =Math.round(res[6]*10000)/10000 ;
              }  }
             var Q1 = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
             var Q2 = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
             if(res[5]=="BuildFormula" && res[6] != undefined)   {
            	 Q2 = res[6]
             } 
             var E1=Q1*Q2;
             var E2="";
             E2=Math.round(E1*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
             var type = res[8];
               var typeConversion=res[10];   
               if(type == 'Division')   {
               	 if(typeConversion==0) 	 {
               	   E1=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E1;
               	 } else {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
               	 } }
               if(type == ' ' || type == '') {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
               }
               if(type == 'Multiply')  {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
               } 
               document.forms['accountLineForms'].elements['accountLine.basis'].value=res[9];
               var chargedeviation=res[15];
               document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation;
               if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
               document.getElementById("estimateDeviationShow").style.display="block"; 
               document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value=res[11];
               var buyDependSellAccount=res[11];
               var estimateRevenueAmount=0;
               var finalEstimateRevenueAmount=0; 
               var finalEstimateExpenceAmount=0;
               var estimateSellRate=0; 
               var sellDeviation=0;
               var  estimatepasspercentage=0
               var estimatepasspercentageRound=0;
               var buyDeviation=0;
               sellDeviation=res[12]
               buyDeviation=res[13] 
               estimateRevenueAmount=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;  
               finalEstimateRevenueAmount=Math.round((estimateRevenueAmount*(sellDeviation/100))*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=finalEstimateRevenueAmount; 
               if(buyDependSellAccount=="Y"){
                finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*10000)/10000;
               }else{
                finalEstimateExpenceAmount= Math.round((estimateRevenueAmount*(buyDeviation/100))*10000)/10000;
               }
               document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value=sellDeviation;
               document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value=sellDeviation;
               document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value=buyDeviation;
               document.forms['accountLineForms'].elements['oldEstimateDeviation'].value=buyDeviation;
               document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=finalEstimateExpenceAmount;
               estimatepasspercentage = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
  	           estimatepasspercentageRound=Math.round(estimatepasspercentage);
  	           if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
  	   	             document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
  	           }else{
  	   	             document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
  	           } 
               calculateEstimateRate('none'); 
               }else {
               document.getElementById("estimateDeviationShow").style.display="none"; 
               document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value=0;
               document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value=0;
               document.forms['accountLineForms'].elements['oldEstimateDeviation'].value=0;
               document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value=0;
               var bayRate=0
               bayRate = res[17] ;
               if(bayRate!=0){
               var estimateQuantityPay=0;
               estimateQuantityPay= document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
               Q1=estimateQuantityPay;
               Q2=bayRate; 
               if(type == 'Division')   {
               	 if(typeConversion==0) 	 {
               	   E1=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E1;
               	 } else {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
               	 } }
               if(type == ' ' || type == '') {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
               }
               if(type == 'Multiply')  {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
               }
                 document.forms['accountLineForms'].elements['accountLine.estimateRate'].value = Q2;
                 var  estimatepasspercentage=0
                 var estimatepasspercentageRound=0;
                 var finalEstimateRevenueAmount=0; 
                 var finalEstimateExpenceAmount=0;
               finalEstimateRevenueAmount=  document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
               finalEstimateExpenceAmount=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
               estimatepasspercentage = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
  	           estimatepasspercentageRound=Math.round(estimatepasspercentage);
  	           if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
  	   	             document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
  	           }else{
  	   	             document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
  	           } 
               }else{ 
               var quantity =0;
               quantity= document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
               var rate =0;
               rate=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
               finalEstimateExpenceAmount = (quantity*rate); 
               if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")  {
      	          finalEstimateExpenceAmount = finalEstimateExpenceAmount/100; 
               }
              if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")  {
      	        finalEstimateExpenceAmount = finalEstimateExpenceAmount/1000; 
               }
               finalEstimateExpenceAmount= Math.round((finalEstimateExpenceAmount)*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=finalEstimateExpenceAmount;
               }  } 
				try{
		     	    var basisValue = document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    	    checkFloatNew('accountLineForms','accountLine.estimateQuantity','Nothing');
		    	    var quantityValue = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
				     var type01 = res[8];
				     var typeConversion01=res[10]; 
		 	        var buyRate1=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
			        var estCurrency1=document.forms['accountLineForms'].elements['accountLine.estCurrency'].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							var estimateExpenseQ1=0;
							Q1=buyRate1;
							Q2=document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*10000)/10000;
				            document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=E1;
				            var estLocalAmt1=0;
				            estimateExpenseQ1=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value	
				            estLocalAmt1=estimateExpenseQ1*Q2; 
				            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
				            document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=estLocalAmt1;     
					}
					 <c:if test="${multiCurrency=='Y'}"> 
			        buyRate1=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
			        estCurrency1=document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value;			        
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							var estimateRevenueAmountQ1=0;
							Q1=buyRate1;
							Q2=document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*10000)/10000;
				            document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=E1;
				            var estLocalAmt1=0;
				            estimateRevenueAmountQ1=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value
				            estLocalAmt1=estimateRevenueAmountQ1*Q2;	 
				            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
				            document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=estLocalAmt1;     
					}
					</c:if>
					}catch(e){}
               </c:if>
               <c:if test="${contractType}"> 
             
               var res = results.split("#"); 
               /* <c:choose>
		       <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
		       if(document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value==res[18]){
		    	   
		       }else{
		    	   document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value=res[18]; 
		    	   rateOnActualizationDate('accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');
		       }
		       document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value=res[19]; 
		       </c:when>
		       <c:otherwise>
               document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value=res[18];
               document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value=res[19]; 
               findExchangeContractRateByCharges('estimate');
               </c:otherwise>
  		       </c:choose> */
  		       document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value=res[18];
               document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value=res[19]; 
  		       findExchangeContractRateByCharges('estimate');
               findPayableContractCurrencyExchangeRateByCharges('estimate');
               var multquantity=res[14]
              if(res[3]=="AskUser1"){ 
               var reply = prompt("Please Enter Quantity", "");
               if(reply) {
               if((reply>0 || reply<9)|| reply=='.')  {
            	document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = Math.round(reply*10000)/10000;
            	<c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value= Math.round(reply*10000)/10000;
                </c:if>	
            	} else {
            		alert("Please Enter legitimate Quantity");
            	} }
            	else{
            	document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = "${accountLine.estimateQuantity}";
            	<c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value= "${accountLine.estimateSellQuantity}";
                </c:if>	
            	}
            }else{             
            if(res[1] == undefined){
            document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value ="";
            <c:if test="${contractType}">
            document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value = "";
            </c:if>	
            }else{
            document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = Math.round(res[1]*10000)/10000;
            <c:if test="${contractType}">
            document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value = Math.round(res[1]*10000)/10000;
            </c:if>	
            }  }
            if(res[5]=="AskUser"){ 
            var reply = prompt("Please Enter the value of Rate", "");
            if(reply)  {
            if((reply>0 || reply<9)|| reply=='.') {
            	document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value = Math.round(reply*10000)/10000;
            	} else {
            		alert("Please Enter legitimate Rate");
            	} }
            	else{
            	document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value = "${accountLine.estimateRate}";
            	} 
            }else{
            if(res[4] == undefined){
            document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value ="";
            }else{
            document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value =res[4] ;
            }  }
            if(res[5]=="BuildFormula")   {
            if(res[6] == undefined){
            document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value ="";
            }else{
             document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value =res[6] ;
            }  }
           var Q1 = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
           var Q2 = document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
           if(res[5]=="BuildFormula" && res[6] != undefined )   {
        	   Q2 = res[6];
           }
           var E1=Q1*Q2;
           var E2="";
           E2=Math.round(E1*10000)/10000;
           document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value = E2;
           var type = res[8];
             var typeConversion=res[10];   
             if(type == 'Division')   {
             	 if(typeConversion==0) 	 {
             	   E1=0*1;
             	   document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value = E1;
             	 } else {	
             		E1=(Q1*Q2)/typeConversion;
             		E2=Math.round(E1*10000)/10000;
             		document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value = E2;
             	 } }
             if(type == ' ' || type == '') {
             		E1=(Q1*Q2);
             		E2=Math.round(E1*10000)/10000;
             		document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value = E2;
             }
             if(type == 'Multiply')  {
             		E1=(Q1*Q2*typeConversion);
             		E2=Math.round(E1*10000)/10000;
             		document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value = E2;
             } 
             document.forms['accountLineForms'].elements['accountLine.basis'].value=res[9];
             var chargedeviation=res[15];
             document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation;
             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
             document.getElementById("estimateDeviationShow").style.display="block"; 
             document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value=res[11];
             var buyDependSellAccount=res[11];
             var estimateRevenueAmount=0;
             var finalEstimateRevenueAmount=0; 
             var finalEstimateExpenceAmount=0;
             var finalEstimateContractRateAmmount=0;
             var estimateContractRateAmmount=0;
             var estimatePayableContractRateAmmount=0;
             var estimateSellRate=0; 
             var sellDeviation=0;
             var  estimatepasspercentage=0
             var estimatepasspercentageRound=0;
             var buyDeviation=0;
             sellDeviation=res[12]
             buyDeviation=res[13] 
             var estimateContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value
             estimateContractRateAmmount =document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value
             estimateRevenueAmount=estimateContractRateAmmount/estimateContractExchangeRate;
             estimateRevenueAmount=Math.round(estimateRevenueAmount*10000)/10000;
             //estimateRevenueAmount=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;  
             finalEstimateRevenueAmount=Math.round((estimateRevenueAmount*(sellDeviation/100))*10000)/10000;
             finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount*(sellDeviation/100))*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value=finalEstimateContractRateAmmount; 
             if(buyDependSellAccount=="Y"){
              finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*10000)/10000;
             }else{
              finalEstimateExpenceAmount= Math.round((estimateRevenueAmount*(buyDeviation/100))*10000)/10000;
             }
             document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value=sellDeviation;
             document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value=sellDeviation;
             document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value=buyDeviation;
             document.forms['accountLineForms'].elements['oldEstimateDeviation'].value=buyDeviation;
             var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value
             estimatePayableContractRateAmmount=finalEstimateExpenceAmount*estimatePayableContractExchangeRate;
             estimatePayableContractRateAmmount=Math.round(estimatePayableContractRateAmmount*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value=estimatePayableContractRateAmmount;
             calEstimatePayableContractRate();
             }else {
             document.getElementById("estimateDeviationShow").style.display="none"; 
             document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value=0;
             document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value=0;
             document.forms['accountLineForms'].elements['oldEstimateDeviation'].value=0;
             document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value=0;
             var bayRate=0
             bayRate = res[17] ;
             if(bayRate!=0){
             var estimateQuantityPay=0;
             estimateQuantityPay= document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
             Q1=estimateQuantityPay;
             Q2=bayRate; 
             if(type == 'Division')   {
             	 if(typeConversion==0) 	 {
             	   E1=0*1;
             	   document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value = E1;
             	 } else {	
             		E1=(Q1*Q2)/typeConversion;
             		E2=Math.round(E1*10000)/10000;
             		document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value = E2;
             	 } }
             if(type == ' ' || type == '') { 
             		E1=(Q1*Q2);
             		E2=Math.round(E1*10000)/10000;
             		document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value = E2;
             }
             if(type == 'Multiply')  {
             		E1=(Q1*Q2*typeConversion);
             		E2=Math.round(E1*10000)/10000;
             		document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value = E2;
             }
               document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value =Q2; 
             } }
             var estimatePayableContractRatecal= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value
             var estimatePayableContractRateAmmountcal= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value
             var  estimatePayableContractCurrencycal=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value;			        
			 if(estimatePayableContractCurrencycal.trim()!=""){
			 var estimatePayableContractExchangeRatecal=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
             var estimateRatecal=estimatePayableContractRatecal/estimatePayableContractExchangeRatecal;
             estimateRatecal=Math.round(estimateRatecal*10000)/10000;
             var estimateExpensecal=estimatePayableContractRateAmmountcal/estimatePayableContractExchangeRatecal;
             estimateExpensecal=Math.round(estimateExpensecal*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRatecal;
             document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=estimateExpensecal;
		     }
			 var estimateRatecal= document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
             var estimateExpensecal= document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
		     var estCurrencycal=document.forms['accountLineForms'].elements['accountLine.estCurrency'].value;
			 if(estCurrencycal.trim()!=""){
			 var estExchangeRatecal = document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;
			 var estLocalRatecal=estimateRatecal*estExchangeRatecal;
			 estLocalRatecal=Math.round(estLocalRatecal*10000)/10000;
			 var estLocalAmountcal=estimateExpensecal*estExchangeRatecal;
			 estLocalAmountcal=Math.round(estLocalAmountcal*10000)/10000;
			 document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=estLocalRatecal;
             document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=estLocalAmountcal;
			 }
			 var estimateContractRatecal= document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value
             var estimateContractRateAmmountcal= document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value
             var  estimateContractCurrencycal=document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value;
             if(estimateContractCurrencycal.trim()!=""){
             var estimateContractExchangeRatecal=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value;
             var estimateSellRatecal=estimateContractRatecal/estimateContractExchangeRatecal;
             estimateSellRatecal=Math.round(estimateSellRatecal*10000)/10000;
             var estimateRevenueAmountcal=estimateContractRateAmmountcal/estimateContractExchangeRatecal;
             estimateRevenueAmountcal=Math.round(estimateRevenueAmountcal*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRatecal;
             document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=estimateRevenueAmountcal;
             } 
             var estimateSellRatecal= document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
             var estimateRevenueAmountcal= document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
		     var estSellCurrencycal=document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value;
			 if(estSellCurrencycal.trim()!=""){
				 var estSellExchangeRatecal = document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;
				 var estSellLocalRatecal=estimateSellRatecal*estSellExchangeRatecal;
				 estSellLocalRatecal=Math.round(estSellLocalRatecal*10000)/10000;
				 var estSellLocalAmountcal=estimateRevenueAmountcal*estSellExchangeRatecal;
				 estSellLocalAmountcal=Math.round(estSellLocalAmountcal*10000)/10000;
				 document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=estSellLocalRatecal;
	             document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=estSellLocalAmountcal;
			 }
            </c:if>
	               calculateVatAmtEst();
	            	calculateESTVatAmt();
	            	var  estimatepasspercentage=0
	            	var estimatepasspercentageRound=0;
	            	var finalEstimateRevenueAmount=0; 
	            	var finalEstimateExpenceAmount=0;
		           finalEstimateRevenueAmount=  document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
		           finalEstimateExpenceAmount=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
		           estimatepasspercentage = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
		           estimatepasspercentageRound=Math.round(estimatepasspercentage);
		           if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		   	             document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		           }else{
		   	             document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		           }
               }
           }
function handleHttpResponse700() { 
             if (http88.readyState == 4)  {
                 var results = http88.responseText
                 results = results.trim(); 
                 var res = results.split("#");
                 if(res[3]=="AskUser1"){
                 var reply = prompt("Please Enter Quantity", "");
                 if(reply)   {
                 if((reply>0 || reply<9)|| reply=='.') {
              	    document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = Math.round(reply*10000)/10000;
              	}  else  {
              		alert("Please Enter legitimate Quantity");
              	} }
              	 else{
              	 document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = "${accountLine.estimateQuantity}";
              }  }
              else{
              if(res[1] == undefined){
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value ="";
              }else{
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value =Math.round(res[1]*10000)/10000 ;
              }  }
              if(res[5]=="AskUser"){
              var reply = prompt("Please Enter the value of Rate", "");
              if(reply)  {
              if((reply>0 || reply<9)|| reply=='.')  {
              	document.forms['accountLineForms'].elements['accountLine.estimateRate'].value =Math.round(reply*10000)/10000 ;
              	} else {
              		alert("Please Enter legitimate Rate");
              	} 	}
              	 else{
              	 document.forms['accountLineForms'].elements['accountLine.estimateRate'].value = "${accountLine.estimateRate}";
              } 
              }else{
              if(res[4] == undefined){
              document.forms['accountLineForms'].elements['accountLine.estimateRate'].value ="";
              }else{
              document.forms['accountLineForms'].elements['accountLine.estimateRate'].value =Math.round(res[4]*10000)/10000 ;
              }  }
              if(res[5]=="BuildFormula")
              {
              if(res[6] == undefined){
              document.forms['accountLineForms'].elements['accountLine.estimateRate'].value ="";
              }else{
               document.forms['accountLineForms'].elements['accountLine.estimateRate'].value = Math.round(res[6]*10000)/10000;
              }  }
             var Q1 = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
             var Q2 = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
             if(res[5]=="BuildFormula" && res[6] != undefined){
            	 Q2 =res[6];    
             }
             var E1=Q1*Q2;
             var E2="";
             E2=Math.round(E1*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
             var type = res[8];
               var typeConversion=res[10];
               if(type == 'Division')  {
               	 if(typeConversion==0)  {
               	   E1=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E1;
               	 } else  {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
               	 }  }
               if(type == ' ' || type == '')  {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
               }
               if(type == 'Multiply')  {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = E2;
               } 
               document.forms['accountLineForms'].elements['accountLine.basis'].value=res[9];
               document.getElementById("estimateDeviationShow").style.display="none";
               document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value=0;
               document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value=0;
               document.forms['accountLineForms'].elements['oldEstimateDeviation'].value=0;
               document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value=0; 
       }  } 
function handleHttpResponse6() { 
             if (http2.readyState == 4) {
                var results = http2.responseText
                 results = results.trim();
                 var res = results.split("#");  
              if(res[3]=="AskUser0"){
              var reply = prompt("Please Enter Entitlement Amount", "");
              if(reply)  {
              if((reply>0 || reply<9)|| reply=='.') {
              	document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = reply;
              	} else {
              		alert("Please Enter legitimate Quantity");
              	} 
              	} else{
              	document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = "${accountLine.entitlementAmount}";
              	} }
              if(res[3]=="Preset0"){
              if(res[0] == undefined){
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = "";
              }else{
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = res[0];
              } }
             if(res[3]=="FromField0"){
             if(res[1] == undefined){
             document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = "";
             }else{
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = res[1];
              }  }
              if(res[5]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if(reply)  {
                if((reply>0 || reply<9)|| reply=='.') {
              	document.forms['accountLineForms'].elements['rateEstimate'].value = reply;
              	} else {
              		alert("Please Enter legitimate Rate");
              	}
              	} else{
              		document.forms['accountLineForms'].elements['rateEstimate'].value = 1;
              		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = "${accountLine.entitlementAmount}";
              		return false;
              	}
                }  else{
                if(res[10] == undefined){
                document.forms['accountLineForms'].elements['rateEstimate'].value = "";
                }else{
                document.forms['accountLineForms'].elements['rateEstimate'].value = res[10];
                }  } 
                if(res[5]=="RateGrid"){
                if(res[8] == undefined){
                document.forms['accountLineForms'].elements['rateEstimate'].value = "";
                }else{
                document.forms['accountLineForms'].elements['rateEstimate'].value = res[8];
                } }
               var Q1 = document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value;
               var Q2 = document.forms['accountLineForms'].elements['rateEstimate'].value;
               var E1=Q1*Q2;
               var E2="";
               E2=Math.round(E1*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value=E2;
               var type = res[4];
               var typeConversion=res[6];
               if(type == 'Division') {
               	 if(typeConversion==0)  {
               	   E1=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E1;
               	 } else  {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E2;
               	 } }
               if(type == ' ' || type == '')  {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E2;
               }
               if(type == 'Multiply')   {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E2;
               }
               document.forms['accountLineForms'].elements['accountLine.basis'].value=res[7];
       } }  
function handleHttpResponse3()  { 
             if (http99.readyState == 4)   {
                var results = http99.responseText
                <c:if test="${!contractType}">    
                results = results.trim();  
                 
                var res = results.split("#");
                if(res[9]=="AskUser2"){
                var reply = prompt("Please Enter Quantity", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.') { 
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value = Math.round(reply*10000)/10000;
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value= Math.round(reply*10000)/10000;
                </c:if>
                 } else {
              		alert("Please Enter legitimate Quantity");
              	    }
              	} else{
              	document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value= "${accountLine.revisionQuantity}";
              	<c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value= "${accountLine.revisionSellQuantity}";
                </c:if>
              	    } 
                }  else{
                if(res[7] == undefined){
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value ="";
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value= "";
                </c:if>
                }else{                                  
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value =Math.round(res[7]*10000)/10000 ;
                <c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Math.round(res[7]*10000)/10000 ;
                </c:if>
               } }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.')  { 
                document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value =Math.round(reply*10000)/10000 ;
               } else {
              		alert("Please Enter legitimate Rate");
              	} 
                }  else{
                document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value = "${accountLine.revisionSellRate}";
                }
                } else{
                  if(res[8] == undefined){
                  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value ="";
                  }else{
                document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value =Math.round(res[8]*10000)/10000 ;
               }  }
               if(res[10]=="BuildFormula"){
               if(res[11] == undefined){
                  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value ="";
                  }else{
               document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value = Math.round(res[11]*10000)/10000;
              }  }
	           var Q1 = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
               var Q2 = document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
               if(res[10]=="BuildFormula" && res[11] != undefined){
            	   Q2 = res[11]
                   }
               var E1=Q1*Q2;
               document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value = E1;
              var Q5 = res[5];             
               var type = res[3];
               var E2= "";
               var E3= ""; 
               if(type == 'Division')  {
               	 if(Q5==0) {
               	   E2=0*1; 
               	   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value = E2;
               	 } else {	
               		E2=(Q1*Q2)/Q5;
               		E3=Math.round(E2*10000)/10000; 
               		document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value = E3;
               	 } }
               if(type == ' '  || type == '') {
               		E2=(Q1*Q2);
               		E3=Math.round(E2*10000)/10000; 
               		document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value = E3;
               }
               if(type == 'Multiply') {
               		E2=(Q1*Q2*Q5);
               		E3=Math.round(E2*10000)/10000;
               	    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value = E3;
               }
              document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13];
               var chargedeviation=res[18];
               document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation;
               if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
               document.getElementById("revisionDeviationShow").style.display="block"; 
               document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value=res[15];
               var buyDependSellAccount=res[15]
               var revisionRevenueAmount=0;
               var finalRevisionRevenueAmount=0; 
               var finalRevisionExpenceAmount=0;
               var revisionSellRate=0; 
               var sellDeviation=0;
               var  revisionpasspercentage=0
               var revisionpasspercentageRound=0;
               var buyDeviation=0;
               sellDeviation=res[16]
               buyDeviation=res[17] 
               revisionRevenueAmount=document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;  
               finalRevisionRevenueAmount=Math.round((revisionRevenueAmount*(sellDeviation/100))*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=finalRevisionRevenueAmount; 
               if(buyDependSellAccount=="Y"){
                finalRevisionExpenceAmount= Math.round((finalRevisionRevenueAmount*(buyDeviation/100))*10000)/10000;
               }else{
                finalRevisionExpenceAmount= Math.round((revisionRevenueAmount*(buyDeviation/100))*10000)/10000;
               }
               document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=sellDeviation;
               document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value=sellDeviation;
               document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=buyDeviation;
               document.forms['accountLineForms'].elements['oldRevisionDeviation'].value=buyDeviation;
               document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=finalRevisionExpenceAmount;
               revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpenceAmount)*100;
  	           revisionpasspercentageRound=Math.round(revisionpasspercentage);
  	           if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
  	   	             document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
  	           }else{
  	   	             document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionpasspercentageRound;
  	           }
  	           calculateRevisionRate('none');  
  	           }else {
               document.getElementById("revisionDeviationShow").style.display="none";
               document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=0;
               document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value=0;
               document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=0;
               document.forms['accountLineForms'].elements['oldRevisionDeviation'].value=0; 
               var bayRate=0 
               bayRate = res[20] ; 
               if(bayRate!=0){
               var recQuantityPay=0;
               recQuantityPay= document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
               Q1=recQuantityPay;
               Q2=bayRate;
               if(type == 'Division')  {
               	 if(Q5==0) {
               	   E2=0*1; 
               	   document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E2;
               	 } else {	
               		E2=(Q1*Q2)/Q5;
               		E3=Math.round(E2*10000)/10000; 
               		document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E3;
               	 } }
               if(type == ' ' || type == '') {
               		E2=(Q1*Q2);
               		E3=Math.round(E2*10000)/10000; 
               		document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E3;
               }
               if(type == 'Multiply') {
               		E2=(Q1*Q2*Q5);
               		E3=Math.round(E2*10000)/10000;
               	    document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E3;
               }
                document.forms['accountLineForms'].elements['accountLine.revisionRate'].value = Q2;
               var  revisionpasspercentage=0
               var revisionpasspercentageRound=0;
               var finalRevisionRevenueAmount=0; 
               var finalRevisionExpenceAmount=0;
               finalRevisionExpenceAmount=document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value
               finalRevisionRevenueAmount= document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value
               revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpenceAmount)*100;
  	           revisionpasspercentageRound=Math.round(revisionpasspercentage);
  	           if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
  	   	             document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
  	           }else{
  	   	             document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionpasspercentageRound;
  	           }
               }else{ 
               var quantity =0;
               quantity= document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
               var rate =0;
               rate=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
               finalRevisionExpenceAmount = (quantity*rate); 
               if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")  {
      	          finalRevisionExpenceAmount = finalRevisionExpenceAmount/100; 
               }
              if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")  {
      	        finalRevisionExpenceAmount = finalRevisionExpenceAmount/1000; 
               }
               finalRevisionExpenceAmount= Math.round((finalRevisionExpenceAmount)*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=finalRevisionExpenceAmount;
               }} 
               try{
		     	    var basisValue = document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    	    checkFloatNew('accountLineForms','accountLine.revisionQuantity','Nothing');
		    	    var quantityValue = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    	    		             
		 	        var buyRate1=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
			        var estCurrency1=document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							var revisionExpenseQ1=0;
							Q1=buyRate1;
							Q2=document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*10000)/10000;
				            document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=E1;
				            var estLocalAmt1=0;
				            revisionExpenseQ1 = document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value 
				            estLocalAmt1 =revisionExpenseQ1*Q2;
				            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
				            document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=estLocalAmt1;     
					}
					<c:if test="${multiCurrency=='Y'}"> 
			        buyRate1=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
			        estCurrency1=document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							var revisionRevenueAmountQ1=0;
							Q1=buyRate1;
							Q2=document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*10000)/10000;
				            document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=E1;
				            var estLocalAmt1=0;
				            revisionRevenueAmountQ1=   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value
				            estLocalAmt1 =revisionRevenueAmountQ1*Q2;
				            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
				            document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=estLocalAmt1;     
					}
					</c:if>
		              var typeConversion01 = res[5];             
		               var type01 = res[3];
					}catch(e){}
               </c:if>
               <c:if test="${contractType}">
               results = results.trim();  
                
               var res = results.split("#");
               /* <c:choose>
		       <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
		       if(document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value==res[21]){
		    	   
		       }else{
		    	   document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value=res[21];
		    	   rateOnActualizationDate('accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');
		       }
		       document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value=res[22]; 
		       </c:when>
		       <c:otherwise> 
               document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value=res[21];
               document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value=res[22]; 
               findExchangeContractRateByCharges('revision');
               </c:otherwise>
  		       </c:choose> */
  		       document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value=res[21];
               document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value=res[22]; 
  		       findExchangeContractRateByCharges('revision');
           	   findPayableContractCurrencyExchangeRateByCharges('revision');
               if(res[9]=="AskUser2"){
               var reply = prompt("Please Enter Quantity", "");
               if(reply){
               if((reply>0 || reply<9)|| reply=='.') { 
               document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value = Math.round(reply*10000)/10000;
               <c:if test="${contractType}">
               document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Math.round(reply*10000)/10000;
               </c:if>
                } else {
             		alert("Please Enter legitimate Quantity");
             	    }
             	} else{
             	document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value= "${accountLine.revisionQuantity}";
             	<c:if test="${contractType}">
                document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value="${accountLine.revisionQuantity}";
                </c:if>
             	    } 
               }  else{
               if(res[7] == undefined){
               document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value ="";
               <c:if test="${contractType}">
               document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value="";
               </c:if>
               }else{                                  
               document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value =Math.round(res[7]*10000)/10000 ;
               <c:if test="${contractType}">
               document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Math.round(res[7]*10000)/10000 ;
               </c:if>
              } }
              if(res[10]=="AskUser"){
               var reply = prompt("Please Enter Rate", "");
               if(reply){
               if((reply>0 || reply<9)|| reply=='.')  { 
               document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value =Math.round(reply*10000)/10000 ;
              } else {
             		alert("Please Enter legitimate Rate");
             	} 
               }  else{
               document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value = "${accountLine.revisionSellRate}";
               }
               } else{
                 if(res[8] == undefined){
                 document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value ="";
                 }else{
               document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value =res[8] ;
              }  }
              if(res[10]=="BuildFormula"){
              if(res[11] == undefined){
                 document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value ="";
                 }else{
              document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value = res[11];
             }  }
	           var Q1 = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
              var Q2 = document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
              if(res[10]=="BuildFormula" && res[11] != undefined){
            	  Q2 =res[11];
              }
              var E1=Q1*Q2;
              document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value = E1;
             var Q5 = res[5];             
              var type = res[3];
              var E2= "";
              var E3= ""; 
              if(type == 'Division')  {
              	 if(Q5==0) {
              	   E2=0*1; 
              	   document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value = E2;
              	 } else {	
              		E2=(Q1*Q2)/Q5;
              		E3=Math.round(E2*10000)/10000; 
              		document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value = E3;
              	 } }
              if(type == ' ' || type == '') {
              		E2=(Q1*Q2);
              		E3=Math.round(E2*10000)/10000; 
              		document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value = E3;
              }
              if(type == 'Multiply') {
              		E2=(Q1*Q2*Q5);
              		E3=Math.round(E2*10000)/10000;
              	    document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value = E3;
              } 
             document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13];
              var chargedeviation=res[18];
              document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation;
              if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
              document.getElementById("revisionDeviationShow").style.display="block"; 
              document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value=res[15];
              var buyDependSellAccount=res[15]
              var revisionRevenueAmount=0;
              var finalRevisionRevenueAmount=0; 
              var finalRevisionExpenceAmount=0;
              var revisionSellRate=0; 
              var sellDeviation=0;
              var  revisionpasspercentage=0
              var revisionpasspercentageRound=0;
              var revisionContractRateAmmount=0;
              var finalRevisionContractRateAmmount=0;
              var revisionPayableContractRateAmmount=0;
              var buyDeviation=0;
              sellDeviation=res[16]
              buyDeviation=res[17]
              var revisionContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value
              revisionContractRateAmmount =document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value
              revisionRevenueAmount=revisionContractRateAmmount/revisionContractExchangeRate;
              revisionRevenueAmount=Math.round(revisionRevenueAmount*10000)/10000; 
              //revisionRevenueAmount=document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;  
              finalRevisionRevenueAmount=Math.round((revisionRevenueAmount*(sellDeviation/100))*10000)/10000;
              finalRevisionContractRateAmmount=Math.round((revisionContractRateAmmount*(sellDeviation/100))*10000)/10000;
              document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value=finalRevisionContractRateAmmount; 
              if(buyDependSellAccount=="Y"){
               finalRevisionExpenceAmount= Math.round((finalRevisionRevenueAmount*(buyDeviation/100))*10000)/10000;
              }else{
               finalRevisionExpenceAmount= Math.round((revisionRevenueAmount*(buyDeviation/100))*10000)/10000;
              }
              document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=sellDeviation;
              document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value=sellDeviation;
              document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=buyDeviation;
              document.forms['accountLineForms'].elements['oldRevisionDeviation'].value=buyDeviation;
              var revisionPayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value
              revisionPayableContractRateAmmount=finalRevisionExpenceAmount*revisionPayableContractExchangeRate;
              revisionPayableContractRateAmmount=Math.round(revisionPayableContractRateAmmount*10000)/10000;
              document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value=revisionPayableContractRateAmmount;
              calRevisionPayableContractRate();  
              }else {
              document.getElementById("revisionDeviationShow").style.display="none";
              document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=0;
              document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value=0;
              document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=0;
              document.forms['accountLineForms'].elements['oldRevisionDeviation'].value=0; 
              var bayRate=0 
              bayRate = res[20] ; 
              if(bayRate!=0){
              var recQuantityPay=0;
              recQuantityPay= document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
              Q1=recQuantityPay;
              Q2=bayRate;
              if(type == 'Division')  {
              	 if(Q5==0) {
              	   E2=0*1; 
              	   document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value = E2;
              	 } else {	
              		E2=(Q1*Q2)/Q5;
              		E3=Math.round(E2*10000)/10000; 
              		document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value = E3;
              	 } }
              if(type == ' ' || type == '') {
              		E2=(Q1*Q2);
              		E3=Math.round(E2*10000)/10000; 
              		document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value = E3;
              }
              if(type == 'Multiply') {
              		E2=(Q1*Q2*Q5);
              		E3=Math.round(E2*10000)/10000;
              	    document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value = E3;
              }
               document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value = Q2;
               }}                 	                 
              var revisionPayableContractRatecal= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value
              var revisionPayableContractRateAmmountcal= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value
              var  revisionPayableContractCurrencycal=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value;			        
 			 if(revisionPayableContractRatecal.trim()!=""){
 			 var revisionPayableContractExchangeRatecal=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value;
              var revisionRatecal=revisionPayableContractRatecal/revisionPayableContractExchangeRatecal;
              revisionRatecal=Math.round(revisionRatecal*10000)/10000;
              var revisionExpensecal=revisionPayableContractRateAmmountcal/revisionPayableContractExchangeRatecal;
              revisionExpensecal=Math.round(revisionExpensecal*10000)/10000;
              document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRatecal;
              document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=revisionExpensecal;
 		     }
 			 var revisionRatecal= document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
              var revisionExpensecal= document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
 		     var revisionCurrencycal=document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value;
 			 if(revisionCurrencycal.trim()!=""){
 			 var revisionExchangeRatecal = document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;
 			 var revisionLocalRatecal=revisionRatecal*revisionExchangeRatecal;
 			 revisionLocalRatecal=Math.round(revisionLocalRatecal*10000)/10000;
 			 var revisionLocalAmountcal=revisionExpensecal*revisionExchangeRatecal;
 			 revisionLocalAmountcal=Math.round(revisionLocalAmountcal*10000)/10000;
 			 document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=revisionLocalRatecal;
             document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmountcal;
 			 }
 			 var revisionContractRatecal= document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value
              var revisionContractRateAmmountcal= document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value
              var  revisionContractCurrencycal=document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value;
              if(revisionContractCurrencycal.trim()!=""){
              var revisionContractExchangeRatecal=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value;
              var revisionSellRatecal=revisionContractRatecal/revisionContractExchangeRatecal;
              revisionSellRatecal=Math.round(revisionSellRatecal*10000)/10000;
              var revisionRevenueAmountcal=revisionContractRateAmmountcal/revisionContractExchangeRatecal;
              revisionRevenueAmountcal=Math.round(revisionRevenueAmountcal*10000)/10000;
              document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRatecal;
              document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=revisionRevenueAmountcal;
              }	 
              var revisionSellRatecal= document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
              var revisionRevenueAmountcal= document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
 		     var revisionSellCurrencycal=document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value;
 			 if(revisionSellCurrencycal.trim()!=""){
 				 var revisionSellExchangeRatecal = document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;
 				 var revisionSellLocalRatecal=revisionSellRatecal*revisionSellExchangeRatecal;
 				revisionSellLocalRatecal=Math.round(revisionSellLocalRatecal*10000)/10000;
 				 var revisionSellLocalAmountcal=revisionRevenueAmountcal*revisionSellExchangeRatecal;
 				revisionSellLocalAmountcal=Math.round(revisionSellLocalAmountcal*10000)/10000;
 				 document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=revisionSellLocalRatecal;
 	             document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmountcal;
 			 } 
            </c:if>
	            calculateVatAmtRevision();  
	            calculateREVVatAmt();
	            var  revisionpasspercentage=0
	            var revisionpasspercentageRound=0;
	            var finalRevisionRevenueAmount=0; 
	            var finalRevisionExpenceAmount=0;
	           finalRevisionExpenceAmount=document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value
	           finalRevisionRevenueAmount= document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value
	           revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpenceAmount)*100;
	           revisionpasspercentageRound=Math.round(revisionpasspercentage);
	           if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
	   	             document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
	           }else{
	   	             document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionpasspercentageRound;
	           }
           }
        }
    function handleHttpResponse300() { 
             if (http99.readyState == 4) {
                var results = http99.responseText
                 results = results.trim();
                if(results !=''){
                var res = results.split("#");
                if(res[9]=="AskUser2"){
                var reply = prompt("Please Enter Quantity", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.') { 
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value =Math.round(reply*10000)/10000 ;
                   } else {
              		alert("Please Enter legitimate Quantity");
              	} 
              	} else{
              	document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value= "${accountLine.revisionQuantity}";
              	}
                }  else{
                if(res[7] == undefined){
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value ="";
                }else{                                            
                document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value = Math.round(res[7]*10000)/10000;
               }  }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                 if(reply){
                if((reply>0 || reply<9)|| reply=='.')
              { 
                document.forms['accountLineForms'].elements['accountLine.revisionRate'].value =Math.round(reply*10000)/10000 ;
               } else {
              		alert("Please Enter legitimate Rate");
              	} 
                }  else{
                document.forms['accountLineForms'].elements['accountLine.revisionRate'].value= "${accountLine.revisionRate}";
                }
                }  else{ 
                 if(res[8] == undefined){ 
                 document.forms['accountLineForms'].elements['accountLine.revisionRate'].value = "";
                 }else{                                
                document.forms['accountLineForms'].elements['accountLine.revisionRate'].value = Math.round(res[8]*10000)/10000;
              }  }
               if(res[10]=="BuildFormula"){
               if(res[11] == undefined){ 
               document.forms['accountLineForms'].elements['accountLine.revisionRate'].value ="";
               }else{
               document.forms['accountLineForms'].elements['accountLine.revisionRate'].value =Math.round(res[11]*10000)/10000 ;
              }  }
	           var Q1 = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
               var Q2 = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
               if(res[10]=="BuildFormula" && res[11] != undefined){
            	   Q2 =res[11];
               }
               var E1=Q1*Q2;
               document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E1; 
               var Q5 = res[5];           
               var type = res[3];//document.forms['accountLineForms'].elements['accountLine.checkNew'].value;
               var E2= "";
               var E3= ""; 
               if(type == 'Division') {
               	 if(Q5==0)  {
               	   E2=0*1; 
               	   document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E2;
               	 } else {	
               		E2=(Q1*Q2)/Q5;
               		E3=Math.round(E2*10000)/10000; 
               		document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E3;
               	 }    }
               if(type == ' ' || type == '')  {
               		E2=(Q1*Q2);
               		E3=Math.round(E2*10000)/10000;
               	    document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E3;
               }
               if(type == 'Multiply')   {
               		E2=(Q1*Q2*Q5);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = E3;
               }   } 
              document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13];
              document.getElementById("revisionDeviationShow").style.display="none"; 
              document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=0;
              document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value=0;
              document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=0;
              document.forms['accountLineForms'].elements['oldRevisionDeviation'].value=0;
      }
      copyRevisionExpense();
    }
    function handleHttpResponse301() { 
             if (http2.readyState == 4)  { 
                var results = http2.responseText
                 results = results.trim(); 
                if(results !=''){
                var res = results.split("#");
                if(res[9]=="AskUser2")  {
                var reply = prompt("Please Enter Quantity", "");
                if((reply>0 || reply<9)|| reply=='.')   {
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = reply;
               } 	else {
              		alert("Please Enter legitimate Quantity");
              	} 
                } else{ 
                if(res[7] == undefined){
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = "";
                }else{                                           
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = res[7];
                    }  }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if((reply>0 || reply<9)|| reply=='.')  {
              	document.forms['accountLineForms'].elements['ratePaybleHid'].value = reply;
               } else {
              		alert("Please Enter legitimate Rate");
              	} 
                } else{
                  if(res[8] == undefined){ 
                  document.forms['accountLineForms'].elements['ratePaybleHid'].value = "";
                  }else{                                          
                document.forms['accountLineForms'].elements['ratePaybleHid'].value = res[8];
               }  }
               if(res[10]=="BuildFormula"){
               if(res[11] == undefined){
               document.forms['accountLineForms'].elements['ratePaybleHid'].value ="";
               }else{
               document.forms['accountLineForms'].elements['ratePaybleHid'].value = res[11]; 
               } }
	           var Q2 = document.forms['accountLineForms'].elements['localAmountPaybleHid'].value;
               var Q1 = document.forms['accountLineForms'].elements['ratePaybleHid'].value;
               var E1="";
               if(Q1==0 && Q2==0) {
               E1=0*1;
               }
               else if(Q2==0)  {
               E1=0*1;
               } else {
               E1=Q1*Q2;  
               var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		       var localAmount=0;
  		       localAmount=E1*exchangeRate;
  		       document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;               
               document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E1;
               }
               var Q5 = res[5];            
               var type = res[3];
               var E2= "";
               var E3= ""; 
               if(type == 'Division')  {
               	 if(Q5==0)  {
               	   E2=0*1;
                   var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		           var localAmount=0;
  		           localAmount=E2*exchangeRate;
  		           document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               	   document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E2;
                } else {	
               		E2=(E1)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		            var localAmount=0;
  		            localAmount=E3*exchangeRate;
  		            document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
                } }
               if(type == ' ' || type == '')  {
               		E2=(E1);
               		E3=Math.round(E2*10000)/10000;
               		var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		            var localAmount=0;
  		            localAmount=E3*exchangeRate;
  		            document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               }
               if(type == 'Multiply')  {
               		E2=(E1)*Q5;
               		E3=Math.round(E2*10000)/10000;
               		var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		            var localAmount=0;
  		            localAmount=E3*exchangeRate;
  		            document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
                } 
              document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value ="INT";
              document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='I'
              if(res[15]=='Y'){
                	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
                }else{document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; }
                setVatExcluded('1') ;
        var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		 var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value!='') {
		   document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=datam;
		   document.forms['accountLineForms'].elements['accountLine.valueDate'].value=datam;
		   document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value=datam;
	     } else {
	     document.forms['accountLineForms'].elements['accountLine.valueDate'].value='';
	     document.forms['accountLineForms'].elements['accountLine.receivedDate'].value='';
	     document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value='';
	    }   }  }    }
    function handleHttpResponse9() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                 results = results.trim(); 
                <c:if test="${!contractType}">
                 if(results !='') {
                var res = results.split("#"); 
                if(res[9]=="AskUser2"){
                var reply = prompt("Please Enter Quantity", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.')  {
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value =Math.round(reply*10000)/10000; 
              	} else {
              		alert("Please Enter legitimate Quantity");
              	} 
                }   else{
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value = "${accountLine.recQuantity}";
                }
                }    else{
                  if(res[7] == undefined){
                  document.forms['accountLineForms'].elements['accountLine.recQuantity'].value ="";
                  }else{ 
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value =Math.round(res[7]*10000)/10000;
               }  }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.')
                 {
                  document.forms['accountLineForms'].elements['accountLine.recRate'].value = Math.round(reply*10000)/10000;
              	 } else {
              		alert("Please Enter legitimate Rate");
              	}
              	} else{
              	document.forms['accountLineForms'].elements['accountLine.recRate'].value ="${accountLine.recRate}"; 
              	} 
                }  else{ 
                 if(res[8] == undefined){
                 document.forms['accountLineForms'].elements['accountLine.recRate'].value =""; 
                 }else{
                document.forms['accountLineForms'].elements['accountLine.recRate'].value =Math.round(res[8]*10000)/10000 ;
                }  }
               if(res[10]=="BuildFormula"){
               if(res[11] == undefined){
               document.forms['accountLineForms'].elements['accountLine.recRate'].value = ""; 
               }else{ 
               document.forms['accountLineForms'].elements['accountLine.recRate'].value = Math.round(res[11]*10000)/10000;
                }  }
               var Q3 = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
               var Q4 = document.forms['accountLineForms'].elements['accountLine.recRate'].value;
               if(res[10]=="BuildFormula" && res[11] != undefined){
              	 Q4 =res[11];
                 }
               var E1=Q3*Q4; 
               var Q5 = res[5];
               if(res[5] == undefined){
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value="";
               }else{
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value=Q5; 
               }
               var type = res[3];//document.forms['accountLineForms'].elements['accountLine.checkNew'].value;
               var E2= "";
               var E3= "";
               if(type == 'Division') {
               	 if(Q5==0) 	 {
               	   E2=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E2;
                }  else {	
               		E2=(Q3*Q4)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3;
               	 }    }
               if(type == ' ' || type == '')   {
               		E2=(Q3*Q4);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3;
               }
               if(type == 'Multiply')   {
               		E2=(Q3*Q4*Q5);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3; 
               } 
               if(res[2]=='') {
               } else {
               if(res[2] == undefined){
               document.forms['accountLineForms'].elements['accountLine.itemNew'].value = "";
               }else{
               document.forms['accountLineForms'].elements['accountLine.itemNew'].value = res[2];
               }  }
               if(res[3]==' ') { 
               document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
               } else{ 
               document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
               } 
               if(document.forms['accountLineForms'].elements['accountLine.description'].value.trim()=='') {
               if(res[4]=='')  {
            	   document.forms['accountLineForms'].elements['checkComputeDescription'].value="computeButton";
            	fillDescription();
               } else{ 
                var chk = res[4];
				var chkReplace=(chk.replace('"','').replace('"',''));
				if(res[12]=='')
				{
					document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace;
				}else{
               	document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace+" ("+res[12]+")";
               	}  } }
               if(res[5]=='') {
               } else{
               if(res[5] == undefined)  {
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value = "";
               }  else{
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value = Q5;
               }  }
               if(res[6]=='') {
               } else{
               document.forms['accountLineForms'].elements['accountLine.basisNewType'].value = res[6];
               }
               document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13]; 
               if(res[19]=='Y'){
                	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
                }else{document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; }
                setVatExcluded('1') ; 
               var chargedeviation=res[18];
               document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation; 
               if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
               document.getElementById("receivableDeviationShow").style.display="block"; 
               document.getElementById("payableDeviationShow").style.display="block";   
               var buyDependSellAccount=res[15]
               var actualRevenue=0;
               var finalActualRevenue=0; 
               var finalActualExpenceAmount=0;
               var sellDeviation=0;  
               sellDeviation=res[16] 
               var buyDeviation=0; 
               buyDeviation=res[17] 
               actualRevenue=document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;  
               finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*10000)/10000;
               if(buyDependSellAccount=="Y"){
                finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*10000)/10000;
               }else{
                finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*10000)/10000;
               }
               document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=finalActualRevenue; 
               document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=sellDeviation;
               document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=sellDeviation;
               if (document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='') { 
               //document.forms['accountLineForms'].elements['oldPayDeviation'].value=buyDeviation;
               //document.forms['accountLineForms'].elements['accountLine.payDeviation'].value=buyDeviation;
               //document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=finalActualExpenceAmount;
               //convertLocalAmount('none');
               }
                }else {
               document.getElementById("receivableDeviationShow").style.display="none";
               document.getElementById("payableDeviationShow").style.display="none";
               document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=0;
               document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=0; 
               var bayRate=0
               bayRate = res[20] ;
               if(bayRate!=0 &&  document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='' ){
               var recQuantityPay=0;
               recQuantityPay= document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
               Q3=recQuantityPay;
               Q4=bayRate;
               if(type == 'Division') {
               	 if(Q5==0) 	 {
               	   E2=0*1;
               	   //document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E2;
                }  else {	
               		E2=(Q3*Q4)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		//document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               	 }    }
               if(type == ' ' || type == '')   {
               		E2=(Q3*Q4);
               		E3=Math.round(E2*10000)/10000;
               		//document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               }
               if(type == 'Multiply')   {
               		E2=(Q3*Q4*Q5);
               		E3=Math.round(E2*10000)/10000;
               		//document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3; 
               }
               //convertLocalAmount('none');
               }
               } 
               <c:if test="${multiCurrency=='Y'}"> 
               var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
               var recRate=document.forms['accountLineForms'].elements['accountLine.recRate'].value
               var actualRevenueCal= document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
               var recCurrencyRate=recRate*recRateExchange;
               var roundValue=Math.round(recCurrencyRate*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=roundValue ; 
               var actualRevenueForeignCal=actualRevenueCal*recRateExchange; 
               actualRevenueForeignCal=Math.round(actualRevenueForeignCal*10000)/10000;
               document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeignCal; 
               </c:if>
               calculateVatAmt();
               calculateVatAmtRevision();
               calculateVatAmtEst();        
 			 var type01 = res[3];
             var typeConversion01=res[5]; 
                 }      
             </c:if>
             <c:if test="${contractType}">
             var type01 = '';
             var typeConversion01=0; 
             if(results !='') {
                 var res = results.split("#"); 
                 /* <c:choose>
  		         <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
  		          if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value==res[21]){
  		    	   
  		       }else{
  		    	 document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[21];
  		    	 rateOnActualizationDate('accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');
  		       } 
  		       </c:when>
  		       <c:otherwise> 
                 document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[21];
                 </c:otherwise>
    		     </c:choose> */
                 if(res[21]!=undefined || res[21]!="undefined"){
                	 document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[21];	
            		}else{
            			document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value="";
            		}
    		      
    		     findExchangeContractRateByCharges('receivable');
                 if(res[9]=="AskUser2"){
                 var reply = prompt("Please Enter Quantity", "");
                 if(reply){
                 if((reply>0 || reply<9)|| reply=='.')  {
                 document.forms['accountLineForms'].elements['accountLine.recQuantity'].value =Math.round(reply*10000)/10000; 
               	} else {
               		alert("Please Enter legitimate Quantity");
               	} 
                 }   else{
                 document.forms['accountLineForms'].elements['accountLine.recQuantity'].value = "${accountLine.recQuantity}";
                 }
                 }    else{
                   if(res[7] == undefined){
                   document.forms['accountLineForms'].elements['accountLine.recQuantity'].value ="";
                   }else{ 
                 document.forms['accountLineForms'].elements['accountLine.recQuantity'].value =Math.round(res[7]*10000)/10000;
                }  }
                if(res[10]=="AskUser"){
                 var reply = prompt("Please Enter Rate", "");
                 if(reply){
                 if((reply>0 || reply<9)|| reply=='.')
                  {
                   document.forms['accountLineForms'].elements['accountLine.contractRate'].value = Math.round(reply*10000)/10000;
               	 } else {
               		alert("Please Enter legitimate Rate");
               	}
               	} else{
               	document.forms['accountLineForms'].elements['accountLine.contractRate'].value ="${accountLine.recRate}"; 
               	} 
                 }  else{ 
                  if(res[8] == undefined){
                  document.forms['accountLineForms'].elements['accountLine.contractRate'].value =""; 
                  }else{
                 document.forms['accountLineForms'].elements['accountLine.contractRate'].value =res[8] ;
                 }  }
                if(res[10]=="BuildFormula"){
                if(res[11] == undefined){
                document.forms['accountLineForms'].elements['accountLine.contractRate'].value = ""; 
                }else{ 
                document.forms['accountLineForms'].elements['accountLine.contractRate'].value = res[11];
                 }  }
                var Q3 = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
                var Q4 = document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
                if(res[10]=="BuildFormula" && res[11] != undefined){
                	Q4 = res[11];
                }
                var E1=Q3*Q4; 
                var Q5 = res[5];
                if(res[5] == undefined){
                document.forms['accountLineForms'].elements['accountLine.basisNew'].value="";
                }else{
                document.forms['accountLineForms'].elements['accountLine.basisNew'].value=Q5; 
                }
                var type = res[3];//document.forms['accountLineForms'].elements['accountLine.checkNew'].value;
                var E2= "";
                var E3= "";
                if(type == 'Division') {
                	 if(Q5==0) 	 {
                	   E2=0*1;
                	   document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E2;
                 }  else {	
                		E2=(Q3*Q4)/Q5;
                		E3=Math.round(E2*10000)/10000;
                		document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E3;
                	 }    }
                if(type == ' ' || type == '')   {
                		E2=(Q3*Q4);
                		E3=Math.round(E2*10000)/10000;
                		document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E3;
                }
                if(type == 'Multiply')   {
                		E2=(Q3*Q4*Q5);
                		E3=Math.round(E2*10000)/10000;
                		document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value = E3; 
                } 
                if(res[2]=='') {
                } else {
                if(res[2] == undefined){
                document.forms['accountLineForms'].elements['accountLine.itemNew'].value = "";
                }else{
                document.forms['accountLineForms'].elements['accountLine.itemNew'].value = res[2];
                }  }
                if(res[3]==' ') { 
                document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
                } else{ 
                document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
                }
                if(document.forms['accountLineForms'].elements['accountLine.description'].value.trim()=='') { 
                if(res[4]=='')  {
                	document.forms['accountLineForms'].elements['checkComputeDescription'].value="computeButton";
                fillDescription();
                } else{ 
                 var chk = res[4];
 				var chkReplace=(chk.replace('"','').replace('"',''));
 				if(res[12]=='')
 				{
 					document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace;
 				}else{
                	document.forms['accountLineForms'].elements['accountLine.description'].value = chkReplace+" ("+res[12]+")";
                	}  } }
                if(res[5]=='') {
                } else{
                if(res[5] == undefined)  {
                document.forms['accountLineForms'].elements['accountLine.basisNew'].value = "";
                }  else{
                document.forms['accountLineForms'].elements['accountLine.basisNew'].value = Q5;
                }  }
                if(res[6]=='') {
                } else{
                document.forms['accountLineForms'].elements['accountLine.basisNewType'].value = res[6];
                }
                document.forms['accountLineForms'].elements['accountLine.basis'].value=res[13]; 
                if(res[19]=='Y'){
                 	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
                 }else{document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; }
                 setVatExcluded('1') ;
                
                var chargedeviation=res[18];
                document.forms['accountLineForms'].elements['accountLine.deviation'].value=chargedeviation; 
                if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
                document.getElementById("receivableDeviationShow").style.display="block"; 
                document.getElementById("payableDeviationShow").style.display="block";   
                var buyDependSellAccount=res[15]
                var actualRevenue=0;
                var finalActualRevenue=0; 
                var finalActualExpenceAmount=0;
                var contractRateAmmount=0;
                var finalContractRateAmmount=0;
                var payableContractRateAmmount=0;
                var sellDeviation=0;  
                sellDeviation=res[16] 
                var buyDeviation=0; 
                buyDeviation=res[17]
                var contractExchangeRate=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
                contractRateAmmount =document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value
                actualRevenue=contractRateAmmount/contractExchangeRate;
                actualRevenue=Math.round(actualRevenue*10000)/10000;  
                //actualRevenue=document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;  
                finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*10000)/10000;
                finalContractRateAmmount=Math.round((contractRateAmmount*(sellDeviation/100))*10000)/10000;
                document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=finalContractRateAmmount; 
                if(buyDependSellAccount=="Y"){
                 finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*10000)/10000;
                }else{
                 finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*10000)/10000;
                }
                var payableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
                payableContractRateAmmount=finalActualExpenceAmount*payableContractExchangeRate;
                payableContractRateAmmount=Math.round(payableContractRateAmmount*10000)/10000;
                
                //document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=finalActualRevenue; 
                document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=sellDeviation;
                document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=sellDeviation;
                if (document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='') {
                //document.forms['accountLineForms'].elements['oldPayDeviation'].value=buyDeviation; 
                //document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value=payableContractRateAmmount;     
                //document.forms['accountLineForms'].elements['accountLine.payDeviation'].value=buyDeviation;
                //document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=finalActualExpenceAmount;
                }
               // convertLocalAmount('none');
                 }else {
                document.getElementById("receivableDeviationShow").style.display="none";
                document.getElementById("payableDeviationShow").style.display="none";
                document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value=0;
                document.forms['accountLineForms'].elements['oldReceivableDeviation'].value=0; 
                var bayRate=0
                bayRate = res[20] ;
                if(bayRate!=0 && document.forms['accountLineForms'].elements['accountLine.payAccDate'].value==''){
                	//document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value=res[22]; 
                	//findPayableContractCurrencyExchangeRateByCharges('payable');   
                var recQuantityPay=0;
                recQuantityPay= document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
                Q3=recQuantityPay;
                Q4=bayRate;
                if(type == 'Division') {
                	 if(Q5==0) 	 {
                	   E2=0*1;
                	   //document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E2;
                 }  else {	
                		E2=(Q3*Q4)/Q5;
                		E3=Math.round(E2*10000)/10000;
                		//document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3;
                	 }    }
                if(type == ' ' || type == '')   {
                		E2=(Q3*Q4);
                		E3=Math.round(E2*10000)/10000;
                		//document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3;
                }
                if(type == 'Multiply')   {
                		E2=(Q3*Q4*Q5);
                		E3=Math.round(E2*10000)/10000;
                		//document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value = E3; 
                }
                
                }
                }  
                type01 = res[3];
                typeConversion01=res[5];
             var contractCurrencyCal =document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value; 
             if(contractCurrencyCal!=''){
             var contractExchangeRateCal=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
             var contractRateCal=document.forms['accountLineForms'].elements['accountLine.contractRate'].value
             var contractRateAmmountCal = document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value 
             var recRateCal=contractRateCal/contractExchangeRateCal;
             recRateCal=Math.round(recRateCal*10000)/10000;
             var actualRevCal=contractRateAmmountCal/contractExchangeRateCal;
             actualRevCal=Math.round(actualRevCal*10000)/10000;
             document.forms['accountLineForms'].elements['accountLine.recRate'].value=recRateCal ;
             document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=actualRevCal ;  
              }  
             var recRateCurrencyCal =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
             if(recRateCurrencyCal!=''){
            	 var recRateExchangeCal=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
                 var recRateCal=document.forms['accountLineForms'].elements['accountLine.recRate'].value
                 var actualRevenueCal= document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
                 var recCurrencyRateCal=recRateCal*recRateExchangeCal;
                 recCurrencyRateCal=Math.round(recCurrencyRateCal*10000)/10000;
                 document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=recCurrencyRateCal ;
                 var actualRevenueForeignCal=actualRevenueCal*recRateExchangeCal; 
                 actualRevenueForeignCal=Math.round(actualRevenueForeignCal*10000)/10000;
                 document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeignCal;
             }
             if (document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='') {
             var payableContractCurrencyCal =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value; 
 	         if(payableContractCurrencyCal!=''){
 	         var payableContractExchangeRateCal=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
 	         var payableContractRateAmmountCal=document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value
 	         var actualExpenseCal=payableContractRateAmmountCal/payableContractExchangeRateCal;
 	         actualExpenseCal=Math.round(actualExpenseCal*10000)/10000;
 	         //document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=actualExpenseCal ; 
 	         }
 	        var exchangeRate= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
 	        var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value ;
 	        if(exchangeRate ==0) {
 	                //document.forms['accountLineForms'].elements['accountLine.localAmount'].value=0*1;
 	        } else  {
 	            var localAmount=0;
 	            localAmount=exchangeRate*actualExpense
 	            //document.forms['accountLineForms'].elements['accountLine.localAmount'].value=Math.round(localAmount*100)/100; 
 	            }
             }     
             }                          	                                 
             </c:if>
	           <c:if test="${systemDefaultVatCalculation=='true'}">
	           try{
	       				//calculatePayVatAmt();
	       		      //calculateESTVatAmt();
	       		     // calculateREVVatAmt();	      
	       				
	           }catch(e){}
	           try{ 
	               calculateVatAmt();
	               calculateVatAmtRevision();
	               calculateVatAmtEst();        
                 }catch(e){} 
	           </c:if>       
             
                }}
     function handleHttpResponse900() { 
             if (http2.readyState == 4) {
                var results = http2.responseText
                 results = results.trim();
                 if(results !='')  {
                var res = results.split("#"); 
                if(res[9]=="AskUser2"){
                var reply = prompt("Please Enter Quantity", "");
                if(reply){ 
                if((reply>0 || reply<9)|| reply=='.')
                {
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value = Math.round(reply*10000)/10000;
              	} else {
              		alert("Please Enter legitimate Quantity");
              	} 
                }else{
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value ="${accountLine.recQuantity}";
                }
                }   else{ 
                if(res[7] == undefined){
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value = "";
                }else{
                document.forms['accountLineForms'].elements['accountLine.recQuantity'].value = Math.round(res[7]*10000)/10000;
               }  }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if(reply){
                if((reply>0 || reply<9)|| reply=='.')  {
                document.forms['accountLineForms'].elements['accountLine.recRate'].value =Math.round(reply*10000)/10000 ;
                } else {
              		alert("Please Enter legitimate Rate");
              	} 
              	} 	else{
              	document.forms['accountLineForms'].elements['accountLine.recRate'].value= "${accountLine.recRate}";
              	}
                } else{ 
                if(res[8] == undefined){
                document.forms['accountLineForms'].elements['accountLine.recRate'].value =""; 
                }else{ 
                document.forms['accountLineForms'].elements['accountLine.recRate'].value =Math.round(res[8]*10000)/10000 ;
                } }
               if(res[10]=="BuildFormula"){ 
               if(res[11] == undefined){
                document.forms['accountLineForms'].elements['accountLine.recRate'].value =""; 
                }else{ 
               document.forms['accountLineForms'].elements['accountLine.recRate'].value =Math.round(res[11]*10000)/10000 ;
               }  }
               var Q3 = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
               var Q4 = document.forms['accountLineForms'].elements['accountLine.recRate'].value;
               if(res[10]=="BuildFormula" && res[11] != undefined){
            	   Q4 =res[11];
               }
               var E1=Q3*Q4; 
               var Q5 = res[5];
               if(res[5] == undefined)  {
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value = "";
               }   else{
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value=Q5; 
               }
               var type = res[3];//document.forms['accountLineForms'].elements['accountLine.checkNew'].value;
               var E2= "";
               var E3= "";
               if(type == 'Division')  {
               	 if(Q5==0)  {
               	   E2=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E2;
               	 } else {	
               		E2=(Q3*Q4)/Q5;
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3;
               	  } }
               if(type == ' ' || type == '')  {
               		E2=(Q3*Q4);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3;
                }
               if(type == 'Multiply')  { 
                 	E2=(Q3*Q4*Q5);
               		E3=Math.round(E2*10000)/10000;
               		document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = E3; 
               } 
               if(res[2]=='') {
               } else{
               if(res[2] == undefined){
               document.forms['accountLineForms'].elements['accountLine.itemNew'].value = "";
               }else{
               document.forms['accountLineForms'].elements['accountLine.itemNew'].value = res[2];
               }  }
               if(res[3]==' ')  { 
               document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
               } else{ 
               document.forms['accountLineForms'].elements['accountLine.checkNew'].value = res[3];
               }
               if(document.forms['accountLineForms'].elements['accountLine.description'].value.trim()=='') { 
               if(res[4]=='')
               {
            	   document.forms['accountLineForms'].elements['accountLine.checkComputeDescription'].value="computeButton";
            	fillDescription();
               } else{ 
               document.forms['accountLineForms'].elements['accountLine.description'].value = res[4]+res[12];
               } }
               if(res[5]=='') {
               } else{
               if(res[5] == undefined) {
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value = "";
               }   else{
               document.forms['accountLineForms'].elements['accountLine.basisNew'].value = Q5;
               } }
               if(res[6]=='') {
               } else{
               document.forms['accountLineForms'].elements['accountLine.basisNewType'].value = res[6];
               } } 
               if(res[15]=='Y'){
                	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
                }else{document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; }
                setVatExcluded('1') ;
                <c:if test="${contractType}"> 
                	var contractCurrency= '${accountLine.contractCurrency}'
                	if(res[16]!=contractCurrency){
                		if(res[16]!=undefined || res[16]!="undefined"){
                			document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[16];	
                			}else{
                				document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value="";
                			}
                	findExchangeContractRate('','','');
                	}
                	</c:if>
                }  } 
function getDistributedAmtFromBCode(){
	var disAmt = document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value;
	var agree = true;
	if(disAmt != 0 || disAmt != 0.00){
		agree=confirm("Continuing with this calculation will overwrite the amount obtained from XML.");
	}
		if (agree){
			document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value="0.00";
			var actualRevanue = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
		    var glType = document.forms['accountLineForms'].elements['accountLine.glType'].value;
		    var branchCode = document.forms['accountLineForms'].elements['accountLine.branchCode'].value;
		    var chargeCode = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
		    var desc = document.forms['accountLineForms'].elements['accountLine.description'].value;
		    var url="distAmount.html?ajax=1&decorator=simple&popup=true&sid=${serviceOrder.id}&actualRevanue=" + encodeURI(actualRevanue)+ "&glType=" + encodeURI(glType)+ "&branchCode=" + encodeURI(branchCode)+"&chargeCode=" + encodeURI(chargeCode)+ "&desc=" +encodeURI(desc);
		    http6.open("GET", url, true);
		    http6.onreadystatechange = handleHttpResponse18;
		    http6.send(null);
		}else{
				return false;
		}
		chechCreditInvoice();
}
function handleHttpResponse18(){
             if (http6.readyState == 4) {
                var results1 = http6.responseText
                results1 = results1.trim();                
                if(results1.length>0 && results1.indexOf("DOCTYPE html") == -1 ) {
				var distAmount = 0; 
				if(results1.indexOf(".") == -1) {
					distAmount = results1;
				} else {
					distAmount = results1.substring(0,(results1.indexOf(".")+3));
				} 
               		document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value = distAmount;
               		document.forms['accountLineForms'].elements['accountLine.distributionAmount'].focus();
                 }else{ 
	                 document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value="0.00";
					 document.forms['accountLineForms'].elements['accountLine.glType'].value="";
			   }          }     }  
function getCommissionAmtFromBCode(){
 	var commAmt = document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
	var agree = true;
	var distAmt ="0.00"
	if(commAmt != 0 || commAmt != 0.00){
		agree=confirm("Continuing with this calculation will overwrite the previous commission Amount");
	} 
 	if (agree){
		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value="0.00";
		<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		distAmt= document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value;
	   </c:if>
		var actualAmt = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
	    var glcomm = document.forms['accountLineForms'].elements['accountLine.commission'].value;
	    var url="commAmount.html?ajax=1&decorator=simple&popup=true&sid=${serviceOrder.id}&distAmt=" + encodeURI(distAmt)+ "&glcomm=" + encodeURI(glcomm)+"&actualAmt="+encodeURI(actualAmt);
	    http7.open("GET", url, true);
	    http7.onreadystatechange = handleHttpResponse22;
	    http7.send(null);
	}else{
		return false;
	}  }
function handleHttpResponse22(){
var disAmt =0
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
disAmt = document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value;
</c:if>
<c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
disAmt = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
</c:if>
 if(disAmt>0 || disAmt>0.00){
             if (http7.readyState == 4) {
                var results3 = http7.responseText
                results3 = results3.trim();               
                if(results3.length>0) {
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = results3*1;
               	    var mydate=new Date();
					var daym;
					var year=mydate.getFullYear()
					var y=""+year;
					if (year < 1000)
					year+=1900
					var day=mydate.getDay()
					var month=mydate.getMonth()+1
					if(month == 1)month="Jan";
					if(month == 2)month="Feb";
					if(month == 3)month="Mar";
					if(month == 4)month="Apr";
					if(month == 5)month="May";
					if(month == 6)month="Jun";
					if(month == 7)month="Jul";
					if(month == 8)month="Aug";
					if(month == 9)month="Sep";
					if(month == 10)month="Oct";
					if(month == 11)month="Nov";
					if(month == 12)month="Dec"; 
					var daym=mydate.getDate()
					if (daym<10)
					daym="0"+daym
					var datam = daym+"-"+month+"-"+y.substring(2,4); 
	               		document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value = datam;
	               		document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value = "1.00";
	               		document.forms['accountLineForms'].elements['accountLine.localAmount'].value = results3*1;
                 }else{
	                 document.forms['accountLineForms'].elements['accountLine.actualExpense'].value="0.00";
					 document.forms['accountLineForms'].elements['accountLine.commission'].value="";
			   }   } } }      
function myFunctionGL(){
	var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
   try{
   findGlTypeList();
   }
   catch(e){}
   try{
   commissionList();
   }
   catch(e){}}
}   
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
    var http2 = getHTTPObject(); 
    function getHTTPObjectvandor() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
  var httpvandor  = getHTTPObjectvandor();
  var httpvandor2221984 = getHTTPObjectvandor();
function getHTTPObject1() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    function getHTTPObject2() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
    var http4 = getHTTPObject2(); 
    var http5 = getHTTPObject5();
	var http1234 = getHTTPObject5();    
    var http10 = getHTTPObject();   
    var http55 = getHTTPObject55();
    var http5512 = getHTTPObject();
    var http5513 = getHTTPObject();
    var http1984 = getHTTPObject();
    var http1985 = getHTTPObject();
    var http1986 = getHTTPObject();
    var http1983 = getHTTPObject();
    var http1981 = getHTTPObject();
    var http51984 = getHTTPObject(); 
    var http5222 = getHTTPObject();
    var http52222 = getHTTPObject();
    function getHTTPObject55() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
function getHTTPObject5() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http6 = getHTTPObject6();
function getHTTPObject6(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
} 
var http7 = getHTTPObject7();
function getHTTPObject7(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}    
var http8 = getHTTPObject8(); 
function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
} 
var http22 = getHTTPObject22(); 
function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)   {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
 function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
     var http34=getHTTPObject77(); 
    var http100 = getHTTPObject77(); 
    var  httpNetworkAgent= getHTTPObject77(); 
    var http77 = getHTTPObject77(); 
    var http777 = getHTTPObject77(); 
    var http7777 = getHTTPObject77(); 
     function getHTTPObject88() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)   {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
    var http88 = getHTTPObject88();  
function getHTTPObject99() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
   var http99 = getHTTPObject99(); 
function getHTTPObject9() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
   var http9 = getHTTPObject9(); 
   var http1987 = getHTTPObject9();
   function getHTTPObject222() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
} 
  var  http222 = getHTTPObject222(); 
function expense(target) {
       var estimate=0; 
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
 	   var estimatePassPercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
 	   var revisionPassPercentage = document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
  if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt") {
      document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseCwt'].value;
        var Q1=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
        var Q2=0;
        Q2=Math.round(Q1*10000)/10000;
        document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value= Q2;
        <c:if test="${contractType}">
        document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=Q2;
        </c:if>	
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseCwt'].value;
     var Q3=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
     var Q4=0;
     Q4=Math.round(Q3*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value= Q4;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Q4;
     </c:if>
     if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0) {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseCwt'].value;
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value= Q4;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'Lbs';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '100';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= 'cwt';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= 'Division';
     }
     } else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="kg") {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseKg'].value;
     var Q5=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
     var Q6=0;
     Q6=Math.round(Q5*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=Q6;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=Q6;
     </c:if> 
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseKg'].value;
     var Q7=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
     var Q8=0;
     Q8=Math.round(Q7*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=Q8;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Q8;
     </c:if>
      if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0) {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseKg'].value;
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=Q8;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'Kgs';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }
  } else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cft") {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseCft'].value;
     var Q9=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
     var Q10=0;
     Q10=Math.round(Q9*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=Q10;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=Q10;
     </c:if>	
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseCft'].value;
     var Q11=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
     var Q12=0;
     Q12=Math.round(Q11*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value= Q12;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Q12;
     </c:if>
     if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0)  {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseCft'].value;
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=Q12;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'Cft';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }
  } else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cbm")  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseCbm'].value;
     var Q13=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
     var Q14=0;
     Q14=Math.round(Q13*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value= Q14;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=Q14;
     </c:if>
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseCbm'].value;
     var Q15=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
     var Q16=0;
     Q16=Math.round(Q15*10000)/10000;
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value= Q16;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=Q16;
     </c:if>
      if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0) {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseCbm'].value;
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=Q16;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'Cbm';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="each")  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
     </c:if>
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
     </c:if>
      if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0)  {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'Each';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     } }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="value") {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
     </c:if>
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
     </c:if>
      if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0) {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }   }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="flat")  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
     </c:if>
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     </c:if> 
     if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0)  {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }   }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="hour") {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseHour'].value;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseHour'].value;
     </c:if>
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseHour'].value;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseHour'].value;
     </c:if>  
     if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0) {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseHour'].value;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'Hrs';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }  } 
   else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")  {
    document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
    </c:if>   
    if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0) {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= '%age';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }  }
   else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")
   {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
     </c:if>
     document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     <c:if test="${contractType}">
     document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     </c:if> 
     if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0)
     {
     document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=document.forms['accountLineForms'].elements['totalRevisionExpenseFlat'].value;
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= 'per 1000';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }
   } else { 
     if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0)  {
     document.forms['accountLineForms'].elements['accountLine.itemNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNew'].value= '';
     document.forms['accountLineForms'].elements['accountLine.basisNewType'].value= '';
     document.forms['accountLineForms'].elements['accountLine.checkNew'].value= ' ';
     }  } 
  	expenseOld('all',target,'','',''); 
  	revisionExpanseOld('all',target,'','',''); 
  	return estimate;
}  
   function mockupForRevenue(target,field1,field2,field3) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
   	   var estimate=0;
       var estimateround=0;
       var sellRateround=0;
       var estimatepasspercentageRound=0;
       var  estimateDeviation=0;
       var  estimateSellDeviation=0;
       var finalEstimateRevenueAmount=0;
       var finalEstimateExpenceAmount=0;
       var buyDependSellAccount=document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value;
       estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value 
       var	estimatepasspercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
       var sellRate = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
       var quantity = eval(document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value); 
       <c:if test="${contractType}">
       quantity = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value
       </c:if>
       if(quantity>0){
       var quantity1 = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
       <c:if test="${contractType}">
       quantity1 = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
       </c:if>
       var quantityabs=Math.abs(quantity);
 	   var quantity1abs=Math.abs(quantity1);
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	   var estimateRevenue = document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value; 
   	   estimate = document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
   	   estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value 
	    if(estimateDeviation!='' && estimateDeviation>0 && estimateSellDeviation!='' && estimateSellDeviation>0){ 
         finalEstimateRevenueAmount=Math.round((estimateRevenue*(100/estimateSellDeviation))*10000)/10000; 
        if(buyDependSellAccount=="Y"){
            finalEstimateExpenceAmount= Math.round((estimateRevenue*(estimateDeviation)/100)*10000)/10000;
         }else{
            finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(estimateDeviation)/100)*10000)/10000;
         } 
	    estimate=Math.round(finalEstimateExpenceAmount*10000)/10000;
	    document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=finalEstimateExpenceAmount; 
	    }  
   	   	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       		  sellRate=(estimateRevenue/quantity)*100;
       		  sellRateround=Math.round(sellRate*10000)/10000; 
       		  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
       	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){ 
       		  sellRate=(estimateRevenue/quantity)*1000;
       		  sellRateround=Math.round(sellRate*10000)/10000;
       		  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
       	if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" && document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age" && document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000") {
  	        sellRate=estimateRevenue/quantity;
       	    sellRateround=Math.round(sellRate*10000)/10000;
  	        document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;
  	    }  
	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
	    sellRate=sellRateround*100/estimateSellDeviation; 
	    sellRateround=Math.round(sellRate*10000)/10000; 
	    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;
	    }  
  	   estimatepasspercentage = (estimateRevenue/estimate)*100;
  	   estimatepasspercentageRound=Math.round(estimatepasspercentage);
  	   if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
  	   }else{
  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
  	   }
  	 <c:if test="${!contractType}">
  	   if(quantityabs==quantity1abs && document.forms['accountLineForms'].elements['accountLine.accrueRevenue'].value==''&& document.forms['accountLineForms'].elements['accountLine.accruePayable'].value=='' ) {
   	     document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value; 
   	   }
  	 </c:if>
   	   try{
		    expenseOld('rev',target,'','','');
		    }catch(e){}	
   	   } 
   	   calculateEstimateRate(target);
   	   calculateVatAmtEst();
 }
	 function calculateRevenue() {
   	    var estimate=0;
   	    var sellRateround=0;
        var estimateround=0;
        var estimateRevenue=0;
        var estimateRevenueRound=0;
        var estimateSellDeviation=0
        var estimateDeviation=0;
       var	estimatepasspercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
       var sellRate = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
       var quantity = eval(document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value);
       var sellQuantity=0;
       <c:if test="${contractType}">
       sellQuantity = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value
       </c:if>
       if(quantity>0){
       var quantity1 = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
       var quantityabs=Math.abs(quantity);
 	   var quantity1abs=Math.abs(quantity1);
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	   estimate = (quantity*rate); 
   	   	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       		estimate = estimate/100; 	  	
       	}  
       	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       		estimate = estimate/1000; 	  	
       	}
       	estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value 
	    if(estimateDeviation!='' && estimateDeviation>0){
	    estimate=estimate*estimateDeviation/100;
	    estimate=Math.round(estimate*10000)/10000; 
	    }
       	estimateRevenue=((estimate)*estimatepasspercentage/100)
       	estimateRevenueRound=Math.round(estimateRevenue*10000)/10000;
  	    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=estimateRevenueRound; 
  	    <c:if test="${contractType}">
  	      if(sellQuantity>0){
  	    	quantity=  sellQuantity; 
  	        }else{
  	        	document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=	quantity;
  	        }
         </c:if>  
	     if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       	  sellRate=(estimateRevenue/quantity)*100;
       	  sellRateround=Math.round(sellRate*10000)/10000;
       	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
	    else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       	  sellRate=(estimateRevenue/quantity)*1000;
       	  sellRateround=Math.round(sellRate*10000)/10000;
       	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	} else if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age"||document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000")
        {
          sellRate=(estimateRevenue/quantity);
       	  sellRateround=Math.round(sellRate*10000)/10000;
          document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
	    }
	    estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value 
	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
	    sellRate=sellRateround*100/estimateSellDeviation;
	    sellRateround=Math.round(sellRate*10000)/10000;
	    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;
	    }
	    <c:if test="${!contractType}">
	    if(quantityabs==quantity1abs && document.forms['accountLineForms'].elements['accountLine.accrueRevenue'].value==''&& document.forms['accountLineForms'].elements['accountLine.accruePayable'].value=='' ) {
   	     document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
   	     document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value; 
   	   }
	    </c:if> 
	    try{
		    expenseOld('all','none','','','');
		    }catch(e){}	   	   
	   	   calculateVatAmtEst();  } }
	function mockupForRevisionRevenue(target,field1,field2,field3) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
   	    var estimate=0;
        var estimateround=0;
        var sellRateround=0;
        var revisionPassPercentageRound=0;
        var revisionSellDeviation=0
        var revisionDeviation=0;
        var finalRevisionRevenueAmount=0;
        var finalRevisionExpenceAmount=0;
        var buyDependSellAccount=document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value;
        revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value 
        var sellRate=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
        var	revisionPassPercentage = document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
        var quantity = eval(document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value);
        <c:if test="${contractType}">
        quantity = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
        </c:if>
        if(quantity>0){
        var rate = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
   	    var revisionRevenue = document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
   	     estimate = document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
   	     revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value 
	     if(revisionDeviation!='' && revisionDeviation>0 && revisionSellDeviation!='' && revisionSellDeviation>0){ 
               finalRevisionRevenueAmount=Math.round((revisionRevenue*(100/revisionSellDeviation))*10000)/10000; 
               if(buyDependSellAccount=="Y"){
                finalRevisionExpenceAmount= Math.round((revisionRevenue*(revisionDeviation/100))*10000)/10000;
               }else{
                finalRevisionExpenceAmount= Math.round((finalRevisionRevenueAmount*(revisionDeviation/100))*10000)/10000;
               }
               document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=finalRevisionExpenceAmount;  
	       estimate=Math.round(finalRevisionExpenceAmount*10000)/10000; 
	     }
   	   	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       		  sellRate=(revisionRevenue/quantity)*100;
       		  sellRateround=Math.round(sellRate*10000)/10000;
       		  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;	  	  	
       	}
       	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){ 
       		  sellRate=(revisionRevenue/quantity)*1000;
       		  sellRateround=Math.round(sellRate*10000)/10000;
       		  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;	  	  	
       	}
       	if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" && document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age" && document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000") {
  	        sellRate=revisionRevenue/quantity;
       	    sellRateround=Math.round(sellRate*10000)/10000;
  	        document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;
  	    } 
	    if(revisionSellDeviation!='' && revisionSellDeviation>0){
	    sellRate=sellRateround*100/revisionSellDeviation;
	    sellRateround=Math.round(sellRate*10000)/10000;
	    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;
	    }
   	   revisionPassPercentage = (revisionRevenue/estimate)*100;
  	   revisionPassPercentageRound=Math.round(revisionPassPercentage); 
  	   if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
  	   }else{
  	    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
  	   } 
	    try{
	    	revisionExpanseOld('rev',target,'','','');
		    }catch(e){}	
  	   }calculateRevisionRate(target);
  	   calculateVatAmtRevision();
  	    }
   function calculateRevisionRevenue() { 
   	    var estimate=0;
        var estimateround=0;
        var revisionRevenue = 0;
        var revisionRevenueRound=0;
        var sellRateround=0;
        var revisionSellDeviation=0
        var revisionDeviation=0;
        var sellRate=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
        var	revisionPassPercentage = document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
        var quantity =eval(document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value);
        
        if(quantity>0){
        var rate = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
   	    estimate = (quantity*rate); 
   	   	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       		estimate = estimate/100; 	  	
       	}  
       	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       		estimate = estimate/1000; 	  	
       	}
       	revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value 
	    if(revisionDeviation!='' && revisionDeviation>0){
	    estimate=estimate*revisionDeviation/100;
	    estimate=Math.round(estimate*10000)/10000; 
	    }
   	    revisionRevenue=((estimate)*revisionPassPercentage/100)
       	revisionRevenueRound=Math.round(revisionRevenue*10000)/10000;
   	    <c:if test="${contractType}">
          quantity = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
        </c:if>
  	    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=revisionRevenueRound;
  	     if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       	  sellRate=(revisionRevenue/quantity)*100;
       	  sellRateround=Math.round(sellRate*10000)/10000;
       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;	  	
       	}
  	     else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       	  sellRate=(revisionRevenue/quantity)*1000;
       	  sellRateround=Math.round(sellRate*10000)/10000;
       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;	  	
       	} else if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age"|| document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000")
        {
          sellRate=(revisionRevenue/quantity);
       	  sellRateround=Math.round(sellRate*10000)/10000;
          document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;	  	
	    }
	    revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value 
	    if(revisionSellDeviation!='' && revisionSellDeviation>0){
	    sellRate=sellRateround*100/revisionSellDeviation;
	    sellRateround=Math.round(sellRate*10000)/10000;
	    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=sellRateround;
	    }
	    try{
	    	revisionExpanseOld('all','none','','','');
		    }catch(e){}	    
	    calculateVatAmtRevision(); }  }
   function expenseOld(temp,target,field1,field2,field3) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }
	   
       var estimate=0;
       var estimateSell=0;
       var estimateround=0;
       var estimateSellRound=0; 
       var passPercentage=0;
       var passPercentageround=0;
       var estimateDeviation=0;
       var estimateSellDeviation=0;
       var finalEstimateRevenueAmount=0;
       var finalEstimateExpenceAmount=0;
       var buyDependSellAccount=document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value;
       estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
       estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
       if(quantity=='' ||quantity=='0' ||quantity=='0.0' ||quantity=='0.00') {
    	   quantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
           } 
       <c:if test="${contractType}">
       sellQuantity = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value
       if(sellQuantity=='' ||sellQuantity=='0' ||sellQuantity=='0.0' ||sellQuantity=='0.00') {
    	   sellQuantity =document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
           } 
       </c:if>
       var quantity1 = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
       
       var quantityabs=Math.abs(quantity);
 	   var quantity1abs=Math.abs(quantity1);
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
       var sellRate = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
   	   var estimatePassPercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
   	   estimate = (quantity*rate);
  	   estimateSell=(quantity*sellRate)
  	    <c:if test="${contractType}">
  	   estimateSell=(sellQuantity*sellRate)
  	    </c:if>
       if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")  {
      	   estimate = estimate/100;
      	   estimateSell=estimateSell/100; 	  	
       }
       if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000") {
      	   estimate = estimate/1000;
      	   estimateSell=estimateSell/1000; 	  	
       } 
       if(estimateSellDeviation!='' && estimateSellDeviation>0){
       estimateSell=estimateSell*estimateSellDeviation/100;
       }  
       if(estimateDeviation!='' && estimateDeviation>0){
       finalEstimateRevenueAmount=Math.round((estimateSell*(100/estimateSellDeviation))*10000)/10000;  
        if(buyDependSellAccount=="Y"){
            finalEstimateExpenceAmount= Math.round((estimateSell*(estimateDeviation)/100)*10000)/10000;
         }else{
            finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(estimateDeviation)/100)*10000)/10000;
         } 
	    estimate=Math.round(finalEstimateExpenceAmount*10000)/10000; 
       }  
      	    estimateround=Math.round(estimate*10000)/10000; 
      	    estimateSellRound=Math.round(estimateSell*10000)/10000;
      	    passPercentage=((estimateSell/estimate)*100);
      	    passPercentageround=Math.round(passPercentage);      	    
  		    document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = estimateround;
  		    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = estimateSellRound;
  		    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=passPercentageround; 
 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
  	   } 
   	   if(temp =='all' || temp=='exp' || ((temp =='all' || temp=='rev') && estimateDeviation!='' && estimateDeviation>0)){
        calEstLocalRate('cal',target);
   	   }
   	 if(temp =='all' || temp=='rev' || ((temp =='all' || temp=='exp') && estimateDeviation!='' && estimateDeviation>0) ){
        calEstSellLocalRate('cal',target);
   	 }
        var estimatePayableContractRate=0;
        var estimateContractRate=0;
        <c:if test="${contractType}">
        estimatePayableContractRate = eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value);
        estimateContractRate = eval(document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value);
        </c:if> 
        if(estimatePayableContractRate!=0){
        	if(temp =='all' || temp=='exp' || ((temp =='all' || temp=='rev') && estimateDeviation!='' && estimateDeviation>0) ){
        calEstimatePayableContractRateAmmount(target);
        	}
        }
        if(estimateContractRate!=0){
        	if(temp =='all' || temp=='rev' || ((temp =='all' || temp=='exp') && estimateDeviation!='' && estimateDeviation>0)){
        calEstimateContractRateAmmount(target);
        	}  }
        <c:if test="${!contractType}">
        if(quantityabs==quantity1abs && document.forms['accountLineForms'].elements['accountLine.accrueRevenue'].value==''&& document.forms['accountLineForms'].elements['accountLine.accruePayable'].value=='' ){
        	   document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
        	   document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
        	   document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
        	   document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
        	   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
        	   document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
        	   document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
       	 	<c:if test="${contractType}">
         	   document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
         	   document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value;
         	   document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
         	   document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value=document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value;
      		</c:if>
      		<c:if test="${multiCurrency=='Y'}">
         	   document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;
         	   document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value;
        	    </c:if>
        	   document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;
     	   document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value;
     	       }
        </c:if>
        driverCommission();
        calculateVatAmtEst();
       	calculateESTVatAmt();
   	   //revisionExpanseOld(); 
  	return estimate;		
}        
   function revisionExpanseOld(temp,target,field1,field2,field3) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
        var estimateround=0;
        var estimateOtherRevrnd=0;
        var estimate=0;
        var estimateSell=0;
        var estimateSellRound=0;
        var passPercentage;
        var passPercentageround;
        var revisionDeviation=0;
        var revisionSellDeviation=0;
        var finalRevisionRevenueAmount=0;
        var finalRevisionExpenceAmount=0;
        var buyDependSellAccount=document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value;
       revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
       revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
 	   var quantity = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
 	   if(quantity=='' ||quantity=='0' ||quantity=='0.0' ||quantity=='0.00') {
   	   quantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
          }
       var quantity1 = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
 	   var quantityabs=Math.abs(quantity);
 	   var quantity1abs=Math.abs(quantity1);   
 	  <c:if test="${contractType}">
 	  revisionSellquantity = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
      if(revisionSellquantity=='' ||revisionSellquantity=='0' ||revisionSellquantity=='0.0' ||revisionSellquantity=='0.00') {
    	  revisionSellquantity =document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
             } 
      </c:if> 
 	   var rate = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
 	   var sellRate = document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
   	   var revisionPassPercentage = document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
  	 	 estimate = (quantity*rate);
  	 	 estimateSell=(quantity*sellRate); 
  	 	<c:if test="${contractType}">
  	 	estimateSell=(revisionSellquantity*sellRate); 
  	 	</c:if>
      if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age") {
      	estimate = estimate/100; 
      	estimateSell=estimateSell/100;	  	
       }
       if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000") {
      	estimate = estimate/1000; 
      	estimateSell=estimateSell/1000;	  	
       }  
       if(revisionSellDeviation!='' && revisionSellDeviation>0){
       estimateSell=estimateSell*revisionSellDeviation/100;
       } 
       if(revisionDeviation!='' && revisionDeviation>0){
       finalRevisionRevenueAmount=Math.round((estimateSell*(100/revisionSellDeviation))*10000)/10000; 
        if(buyDependSellAccount=="Y"){
            finalRevisionExpenceAmount= Math.round((estimateSell*(revisionDeviation)/100)*10000)/10000;
         }else{
            finalRevisionExpenceAmount= Math.round((finalRevisionRevenueAmount*(revisionDeviation)/100)*10000)/10000;
         } 
	    estimate=Math.round(finalRevisionExpenceAmount*10000)/10000;  
       }
      	    estimateround=Math.round(estimate*10000)/10000; 
  		    estimateSellRound=Math.round(estimateSell*10000)/10000;
  		    passPercentage=((estimateSell/estimate)*100);
            passPercentageround=Math.round(passPercentage);
            document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=estimateSellRound;
  		    document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value = estimateround;
  		    if(document.forms['accountLineForms'].elements['accountLine.category'].value=='Internal Cost'){
  		    document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = estimateround; 
  		    var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value
  		    var exchangeRate=document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value;
  		    var localAmount=0;
  		    localAmount=actualExpense*exchangeRate;
  		    document.forms['accountLineForms'].elements['accountLine.localAmount'].value =Math.round(localAmount*10000)/10000;  
  		    }
  		    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=passPercentageround; 
  	        var passThrough = document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
  	    estimateround=(passThrough*estimate)/100;
  		estimateroundRev=Math.round(estimateround*10000)/10000; 
  	if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
  	   } 
  	 if(temp =='all' || temp=='exp' || ((temp =='all' || temp=='rev') && revisionDeviation!='' && revisionDeviation>0)){
  	   calRevisionLocalRate('cal',target);
  	 }
  	if(temp =='all' || temp=='rev' || ((temp =='all' || temp=='exp') && revisionDeviation!='' && revisionDeviation>0)){
   	   calRevisionSellLocalRate('cal',target);
  	} 
   	   var revisionPayableContractRate=0;
       var revisionContractRate=0;
       <c:if test="${contractType}">
       revisionPayableContractRate = eval(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value);
       revisionContractRate = eval(document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value);
       </c:if> 
       if(revisionContractRate!=0){
    	   if(temp =='all' || temp=='rev' || ((temp =='all' || temp=='exp') && revisionDeviation!='' && revisionDeviation>0)){
  	   calRevisionContractRateAmmount(target);
    	   }  }
       if(revisionPayableContractRate!=0){
    	   if(temp =='all' || temp=='exp' || ((temp =='all' || temp=='rev') && revisionDeviation!='' && revisionDeviation>0)){
  	   calRevisionPayableContractRateAmmount(target);
    	   } }
       driverCommission(); 
       calculateVatAmtRevision();
       calculateREVVatAmt();
  		return estimate; 
}        
function convertLocalAmount(target){
	if(target=='accountLine.payableContractRateAmmount'){
    var payableContractExchangeRateTemp=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
    var payableContractRateAmmountTemp=document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value
    var actualExpenseTemp=payableContractRateAmmountTemp/payableContractExchangeRateTemp; 
    var exchangeRate= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
    var actualExpense= actualExpenseTemp ;
    if(exchangeRate ==0) {
             document.forms['accountLineForms'].elements['accountLine.localAmount'].value=0*1;
         } else  {
         var localAmount=0;
         localAmount=exchangeRate*actualExpense
         document.forms['accountLineForms'].elements['accountLine.localAmount'].value=Math.round(localAmount*10000)/10000;
         calculatePayVatAmt();
	      calculateESTVatAmt();
	      calculateREVVatAmt();	 
         if(target!='accountLine.payableContractRateAmmount'){
         calPayableContractExchangeRate('none');
         }  } 
	}else{
var exchangeRate= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value ;
if(exchangeRate ==0) {
         document.forms['accountLineForms'].elements['accountLine.localAmount'].value=0*1;
     } else  {
     var localAmount=0;
     localAmount=exchangeRate*actualExpense
     document.forms['accountLineForms'].elements['accountLine.localAmount'].value=Math.round(localAmount*10000)/10000; 
     calculatePayVatAmt();
     calculateESTVatAmt();
     calculateREVVatAmt();	      

     if(target!='accountLine.payableContractRateAmmount'){
     calPayableContractExchangeRate('none');
     }  } }  }
function calculateActualExpense(){
	   var estimate=0;
	   var estimaternd=0;
	   var payableSellDeviation=0;
	       var quantity = document.forms['accountLineForms'].elements['accountLine.payQuantity'].value;
	       if(quantity=='' ||quantity=='0' ||quantity=='0.0' ||quantity=='0.00') {
	    	   quantity = document.forms['accountLineForms'].elements['accountLine.payQuantity'].value=1;
		    }
	       var rate = document.forms['accountLineForms'].elements['accountLine.payRate'].value;
	       rate = document.forms['accountLineForms'].elements['accountLine.payRate'].value=Math.round(rate*10000)/10000;
	       payableSellDeviation=document.forms['accountLineForms'].elements['accountLine.payDeviation'].value;
	       if(quantity<999999999999 && rate<999999999999) {
	  	 	estimate = (quantity*rate);
	  	 	estimaternd=Math.round(estimate*10000)/10000;
	  	 	} else { 
	  	 	 estimate=0;
	  	 	 estimaternd=Math.round(estimate*10000)/10000;
	  	 	} 
	         if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age") {
	      	    estimate = estimate/100; 
	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	      	    document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = estimaternd; 
	          }
	          else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000") {
	      	    estimate = estimate/1000; 
	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	      	    document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = estimaternd; 
	      	     	
	          } else{              
	            document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = estimaternd;
	           
	          }
	          if(payableSellDeviation!='' && payableSellDeviation>0){
	           estimaternd=estimaternd*payableSellDeviation/100;
	           estimaternd=Math.round(estimaternd*10000)/10000;
	           document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = estimaternd; 
	           } 
	          convertLocalAmount('none');
}  
function calculatePayRate(){
	<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">
	var payableSellDeviation=document.forms['accountLineForms'].elements['accountLine.payDeviation'].value;
	var actualExpense=0;
	actualExpense=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
   	
	var quantity=0;
    quantity = document.forms['accountLineForms'].elements['accountLine.payQuantity'].value; 
   	if(quantity=='' ||quantity=='0' ||quantity=='0.0' ||quantity=='0.00') {
 	   quantity = document.forms['accountLineForms'].elements['accountLine.payQuantity'].value=1;
     }
   	
   	var payRate=0;
   	var payRateRound=0
	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
		payRate=(actualExpense/quantity)*100;
		payRateRound=Math.round(payRate*10000)/10000; 
 		document.forms['accountLineForms'].elements['accountLine.payRate'].value=payRateRound;	  	
 	}
 	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){ 
 		payRate=(actualExpense/quantity)*1000;
 		payRateRound=Math.round(payRate*10000)/10000;
 		document.forms['accountLineForms'].elements['accountLine.payRate'].value=payRateRound;	  	
 	}
 	if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" && document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age" && document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000") {
 		payRate=actualExpense/quantity;
 		payRateRound=Math.round(payRate*10000)/10000;
 		document.forms['accountLineForms'].elements['accountLine.payRate'].value=payRateRound;
    } 
 	
  if(payableSellDeviation!='' && payableSellDeviation>0){
	  payRate=payRateRound*100/payableSellDeviation; 
	  payRateRound=Math.round(payRate*10000)/10000; 
	  document.forms['accountLineForms'].elements['accountLine.payRate'].value=payRateRound;
  } 
  </configByCorp:fieldVisibility>	
}
function convertedAmount(target) { 
	var contractRate=0;
    <c:if test="${contractType}">
    contractRate = document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value;
    </c:if>
    //if(contractRate==0){
   var x= document.forms['accountLineForms'].elements['accountLine.localAmount'].value ;
   var a= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
   var round=0;
   if(x<999999999999&& a<999999999999) {
  	 if(a ==0) {
         document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=0*1;
     } else  {
       amount=x/a;
       roundValue=Math.round(amount*10000)/10000;
       document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=roundValue ;
       calculatePayVatAmt();
	      calculateESTVatAmt();
	      calculateREVVatAmt();	      
        
       calPayableContractExchangeRate(target);
     }
  } else { } 
}
function convertedAmountbyExchangeRate() { 
	var contractRate=0;
    <c:if test="${contractType}">
    contractRate = document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value;
    </c:if>
    if(contractRate==0){
   var x= document.forms['accountLineForms'].elements['accountLine.localAmount'].value ;
   var a= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
   var round=0;
   if(x<999999999999&& a<999999999999) {
  	 if(a ==0) {
         document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=0*1;
     } else  {
       amount=x/a;
       roundValue=Math.round(amount*10000)/10000;
       document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=roundValue ;
       calculatePayVatAmt();
	      calculateESTVatAmt();
	      calculateREVVatAmt();	 
       calPayableContractExchangeRate('none');
     }
  } else { } 
   }else{
    	convertLocalAmount('none');
    } }
function calPayableContractExchangeRate(target){
	<c:if test="${contractType}"> 
    var country =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value; 
    if(country=='') {
    //alert("Please select Contract Currency "); 
    document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value=0; 
  } else if(country!=''){
	  <c:if test="${contractType}"> 
			   var x= document.forms['accountLineForms'].elements['accountLine.localAmount'].value ;
			   var a= document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value*1 ;
			   var amount=0.00;
			   amount=x/a;
			   var payableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value;
			   var actualExpense=amount;
			   var payableContractRateAmmount=actualExpense*payableContractExchangeRate;
			   var roundValue=Math.round(payableContractRateAmmount*10000)/10000;
			   document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value=roundValue ;
				  
		</c:if>
		 <c:if test="${!contractType}"> 
			  var payableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
			  var actualExpense=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value
			  var payableContractRateAmmount=actualExpense*payableContractExchangeRate;
			  var roundValue=Math.round(payableContractRateAmmount*10000)/10000;
			  document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value=roundValue ;
		</c:if>
  }
</c:if>
} 
function convertedDeviationAmount() { 
var actualExpense=0;
var payDeviation=0;
var oldPayDeviation=0;
var finalActualExpense=0;
actualExpense=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value ;
payDeviation=document.forms['accountLineForms'].elements['accountLine.payDeviation'].value ;
oldPayDeviation=document.forms['accountLineForms'].elements['oldPayDeviation'].value ;
if(oldPayDeviation=='' || oldPayDeviation==0){
   oldPayDeviation=100;
   } 
actualExpense=Math.round(actualExpense*100/oldPayDeviation*10000)/10000; 
if(payDeviation!='' && payDeviation!=0){
finalActualExpense=Math.round((actualExpense*(payDeviation/100))*10000)/10000;  
document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=finalActualExpense;
} 
document.forms['accountLineForms'].elements['oldPayDeviation'].value=document.forms['accountLineForms'].elements['accountLine.payDeviation'].value;  
convertLocalAmount('none');
}
function convertedAmountBilling(target,field1,field2,field3) {
	 if(field1!='' && field2!='' & field3!=''){
		 rateOnActualizationDate(field1,field2,field3);
		 }	   
   var estimate=0;
   var estimaternd=0;
   var receivableSellDeviation=0;
       var quantity = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
       if(quantity=='' ||quantity=='0' ||quantity=='0.0' ||quantity=='0.00') {
    	   quantity = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
	    }
       var rate = document.forms['accountLineForms'].elements['accountLine.recRate'].value;
       rate = document.forms['accountLineForms'].elements['accountLine.recRate'].value=Math.round(rate*10000)/10000;
       receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
       if(quantity<999999999999&& rate<999999999999) {
  	 	estimate = (quantity*rate);
  	 	estimaternd=Math.round(estimate*10000)/10000;
  	 	} else { 
  	 	 estimate=0;
  	 	 estimaternd=Math.round(estimate*10000)/10000;
  	 	} 
         if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age") {
      	    estimate = estimate/100; 
      	    estimaternd=Math.round(estimate*10000)/10000;	 
      	    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = estimaternd;
      	    <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
      	    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
             document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value = estimaternd;
             </c:if>
             </c:if> 	
          }
          else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000") {
      	    estimate = estimate/1000; 
      	    estimaternd=Math.round(estimate*10000)/10000;	 
      	    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = estimaternd; 
      	     <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
      	    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
             document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value = estimaternd;
            </c:if> 
            </c:if> 	
          } else{              
            document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = estimaternd;
           <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
            <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
            document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value = estimaternd;
            </c:if> 
            </c:if> 
          }
          if(receivableSellDeviation!='' && receivableSellDeviation>0){
           estimaternd=estimaternd*receivableSellDeviation/100;
           estimaternd=Math.round(estimaternd*10000)/10000;
           document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value = estimaternd;
           <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
           document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value = estimaternd;
           </c:if> 
           </c:if> 	 
           } 
          chechCreditInvoice();
          calFrgnCurrAmount(target); 
          calContractRateAmmount(target);
          driverCommission(); 
  		} 
function assignInvoiceDate() {
	       document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value.trim();
	    var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec"; 
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		var invoiceNumber=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value;
	    invoiceNumber=invoiceNumber.trim();
	    document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value=invoiceNumber; 
		if(document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value!='') {
		  document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=datam;
	     } else {
	     document.forms['accountLineForms'].elements['accountLine.receivedDate'].value='';
	    } }
function validNumberEntitle() { 
   var  temp=document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value;
   if(temp<9999999999) {
     document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value;
   } else { 
     document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value=0*1;
     document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value.focus();
   } }
function checkLHFValue(){
	var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
if(chargeCheck=='' || chargeCheck==null){
var checkLHFValue=document.forms['accountLineForms'].elements['checkLHF'].value;
var chargeCode=document.forms['accountLineForms'].elements['accountLine.chargeCode'].value
if(checkLHFValue=='true'&& (chargeCode.toLowerCase()=='lhf')) {
alert("Account line with charge code LHF already exists");
return false;
}else{
return true;
} }} 
function chk() { 
	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
	var billingContract = '${billing.contract}';
	var originCountry=document.forms['accountLineForms'].elements['serviceOrder.originCountryCode'].value;
	var destinationCountry=document.forms['accountLineForms'].elements['serviceOrder.destinationCountryCode'].value;
	var mode;
	var contractType=document.forms['accountLineForms'].elements['contractTypeAcc'].value; 
	var contractCurrency='tenthDescription';
	var payableContractCurrency='fourthDescription';
	if(contractType=='true'){
	if('${accountLine.recInvoiceNumber}' ==''){ 
	contractCurrency='accountLine.contractCurrency';
	}
	if(document.forms['accountLineForms'].elements['accountLine.payingStatus'].value!='A'){
	payableContractCurrency='accountLine.payableContractCurrency'
	}
	} 
	if(document.forms['accountLineForms'].elements['serviceOrder.mode']!=undefined){
	 mode=document.forms['accountLineForms'].elements['serviceOrder.mode'].value; }else{mode="";}
	var description = "";
	var note = "";
	var noteValue = ""; 
		description = document.forms['accountLineForms'].elements['accountLine.description'].value;
		note = document.forms['accountLineForms'].elements['accountLine.note'].value; 
	description=description.trim();
	note=note.trim();
	billingContract=billingContract.trim();
	descriptionValue='firstDescription';  
	if(note=='') {
	noteValue='accountLine.note';
	} else if(note!='') {
	noteValue='firstDescription';
	}
	if(document.forms['accountLineForms'].elements['accountLine.recGl'] == undefined && document.forms['accountLineForms'].elements['accountLine.payGl'] == undefined){
		recGlVal = 'secondDescription';
		payGlVal = 'thirdDescription'; 
	}else{
		recGlVal = 'accountLine.recGl';
		payGlVal = 'accountLine.payGl'; 
	} 
		if(val=='Internal Cost'){
        var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value 
		javascript:openWindow('internalCostChargess.html?accountCompanyDivision='+accountCompanyDivision+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=chargeCostElement&fld_tenthDescription='+contractCurrency+'&fld_ninthDescription=accountLine.VATExclude&fld_eigthDescription=accountLine.buyDependSell&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+payableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description='+descriptionValue+'&fld_thirdDescription='+payGlVal+'&fld_code=accountLine.chargeCode');
	   document.forms['accountLineForms'].elements['accountLine.chargeCode'].select();
	} else {
	if(billingContract=='' || billingContract==' ') {
		alert("There is no pricing contract in billing: Please select.");
	} else{ 
		javascript:openWindow('chargess.html?contract='+billingContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=chargeCostElement&fld_tenthDescription='+contractCurrency+'&fld_ninthDescription=accountLine.VATExclude&fld_eigthDescription=accountLine.buyDependSell&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+payableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description='+descriptionValue+'&fld_thirdDescription='+payGlVal+'&fld_code=accountLine.chargeCode&categoryType='+val);
	 document.forms['accountLineForms'].elements['accountLine.chargeCode'].select();     
	 }  } 
document.forms['accountLineForms'].elements['accountLine.chargeCode'].focus(); 
}
function checkChargesDataAjax(){
	var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
	var billingContract = '${billing.contract}';
    var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
    var url="checkChargesDataAjax.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&soId="+sid; 
	http51984.open("GET", url, true);
    http51984.onreadystatechange =handleHttpResponsecheckChargesDataAjax;
    http51984.send(null); 
}
function handleHttpResponsecheckChargesDataAjax(){ 
	if (http51984.readyState == 4){
	var results = http51984.responseText
     results = results.trim();  
    var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
    var corpid = document.forms['accountLineForms'].elements['accountLine.corpID'].value;
     results = results.replace('[','');
     results=results.replace(']',''); 
     if(results==''){
    	 <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
    		var contractdescriptionValue=document.forms['accountLineForms'].elements['firstDescription'].value;
    		document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=contractdescriptionValue;
    		document.forms['accountLineForms'].elements['accountLine.description'].value=contractdescriptionValue;
    		document.forms['accountLineForms'].elements['accountLine.note'].value=contractdescriptionValue;
    	   	 <configByCorp:fieldVisibility componentId="component.field.Description.NwvlChargeCode">
    	    		document.forms['accountLineForms'].elements['accountLine.description'].value=contractdescriptionValue+' - '+shipNumber;
    	    		document.forms['accountLineForms'].elements['accountLine.note'].value=contractdescriptionValue+' - '+shipNumber;
    	    		</configByCorp:fieldVisibility>
    		</configByCorp:fieldVisibility>
    		<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
    		var  note = document.forms['accountLineForms'].elements['accountLine.note'].value;
    		note= note.trim();
    		var description= document.forms['accountLineForms'].elements['accountLine.description'].value;
    		description= description.trim(); 
    		var quoteDescription= document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value;
    		quoteDescription= quoteDescription.trim(); 
    		var contractdescriptionValue=document.forms['accountLineForms'].elements['firstDescription'].value;
    		if(note=='') {  
    		document.forms['accountLineForms'].elements['accountLine.note'].value=contractdescriptionValue;
    		} 
    		if(description=='') {  
    			document.forms['accountLineForms'].elements['accountLine.description'].value=contractdescriptionValue;
    			} 
    		if(quoteDescription=='') {  
    			document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=contractdescriptionValue;
    			} 
    		</configByCorp:fieldVisibility> 
     } else{
    	 <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
 		var contractdescriptionValue=results;
 		document.forms['accountLineForms'].elements['accountLine.note'].value=contractdescriptionValue;
 		document.forms['accountLineForms'].elements['accountLine.description'].value=contractdescriptionValue;
 		document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=contractdescriptionValue;
 	   	 <configByCorp:fieldVisibility componentId="component.field.Description.NwvlChargeCode">
 		document.forms['accountLineForms'].elements['accountLine.description'].value=contractdescriptionValue+' - '+shipNumber;
 		document.forms['accountLineForms'].elements['accountLine.note'].value=contractdescriptionValue+' - '+shipNumber;
 		</configByCorp:fieldVisibility>
 		</configByCorp:fieldVisibility>
 		<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
 		var  note = document.forms['accountLineForms'].elements['accountLine.note'].value;
 		note= note.trim();
 		var description= document.forms['accountLineForms'].elements['accountLine.description'].value;
 		description= description.trim(); 
 		var quoteDescription= document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value;
 		quoteDescription= quoteDescription.trim(); 
 		var contractdescriptionValue=results;
 		if(note=='') {  
 		document.forms['accountLineForms'].elements['accountLine.note'].value=contractdescriptionValue;
 		} 
 		if(description=='') {  
 			document.forms['accountLineForms'].elements['accountLine.description'].value=contractdescriptionValue;
 			} 
 		if(quoteDescription=='') {  
 			document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=contractdescriptionValue;
 			} 
 		</configByCorp:fieldVisibility> 
     }  } }
function checkChargesData() {
	<configByCorp:fieldVisibility componentId="component.accountLine.BillingCurrencyFromCharge">
	if(document.forms['accountLineForms'].elements['tenthDescription'].value !='' && document.forms['accountLineForms'].elements['tenthDescription'].value!=undefined && document.forms['accountLineForms'].elements['tenthDescription'].value!="undefined"){
		document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value=document.forms['accountLineForms'].elements['tenthDescription'].value;
		document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value=document.forms['accountLineForms'].elements['tenthDescription'].value;
		document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value=document.forms['accountLineForms'].elements['tenthDescription'].value;
	}
	if(document.forms['accountLineForms'].elements['fourthDescription'].value !='' && document.forms['accountLineForms'].elements['fourthDescription'].value!=undefined &&  document.forms['accountLineForms'].elements['fourthDescription'].value!="undefined"){
		document.forms['accountLineForms'].elements['accountLine.estCurrency'].value=document.forms['accountLineForms'].elements['fourthDescription'].value;
		document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value=document.forms['accountLineForms'].elements['fourthDescription'].value;
		document.forms['accountLineForms'].elements['accountLine.country'].value=document.forms['accountLineForms'].elements['fourthDescription'].value;
	}
	</configByCorp:fieldVisibility>
	fillDefaultInvoiceNumber();
	<c:if test="${contractType}"> 
	rateOnActualizationDate('accountLine.estSellCurrency','accountLine.estSellValueDate','accountLine.estSellExchangeRate');
	rateOnActualizationDate('accountLine.revisionSellCurrency','accountLine.revisionSellValueDate','accountLine.revisionSellExchangeRate');
	rateOnActualizationDate('accountLine.recRateCurrency','accountLine.racValueDate','accountLine.recRateExchange');
	
	if(document.forms['accountLineForms'].elements['accountLine.payingStatus'].value!='A'){
	var contractPayableContractCurrency=document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value; 
	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value=contractPayableContractCurrency;
	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value=contractPayableContractCurrency;
	findPayableContractCurrencyExchangeRateByCharges('all');
	}
	if('${accountLine.recInvoiceNumber}' ==''){
	var contractContractCurrency=document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
	document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value=contractContractCurrency;
	document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value=contractContractCurrency;
    findExchangeContractRateByCharges('all');
	}
	</c:if>	 
}  
function checkCostElement() { 
	<c:if test="${accountInterface=='Y'}">
    <c:if test="${costElementFlag}"> 
     var chargeCostElement = document.forms['accountLineForms'].elements['chargeCostElement'].value; 
     var jobtype = document.forms['accountLineForms'].elements['serviceOrder.Job'].value; 
     var routing = document.forms['accountLineForms'].elements['serviceOrder.routing'].value; 
     var soCompanyDivision = document.forms['accountLineForms'].elements['serviceOrder.companyDivision'].value; 
     var url="findCostElement.html?ajax=1&decorator=simple&popup=true&chargeCostElement="+encodeURIComponent(chargeCostElement)+"&jobType="+encodeURIComponent(jobtype)+"&routing="+encodeURIComponent(routing)+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
	 http5.open("GET", url, true);
     http5.onreadystatechange =handleHttpResponseCostElement;
     http5.send(null); 
     </c:if> </c:if>
}
function handleHttpResponseCostElement(){ 
	if (http5.readyState == 4){
	var results = http5.responseText
     results = results.trim();  
     results = results.replace('[','');
     results=results.replace(']',''); 
     var res = results.split("~");  
     if(res[0]!=undefined || res[0]!="undefined"){
    	 document.forms['accountLineForms'].elements['accountLine.recGl'].value = res[0];	
		}else{
			document.forms['accountLineForms'].elements['accountLine.recGl'].value ="";
		}
     
     if(res[1]!=undefined || res[1]!="undefined"){
    	 document.forms['accountLineForms'].elements['accountLine.payGl'].value = res[1];	
		}else{
			document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
		}
	 var ctype= document.forms['accountLineForms'].elements['accountLine.category'].value;
	 ctype=ctype.trim();
	if(ctype==''){
		 document.forms['accountLineForms'].elements['accountLine.category'].value = res[2];
	} } }
function setVatExcluded(temp){ 
	<c:if test="${(((accountLine.createdBy != 'Networking'  && ( !(((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 ) &&  not empty accountLine.networkSynchedId) && !(trackingStatus.accNetworkGroup)) )) && (accountLine.chargeCode != 'MGMTFEE'))|| !(trackingStatus.soNetworkGroup))}">
	<c:if test="${fn1:indexOf(accountLine.createdBy,'DMM')<0}">
	<c:if test="${systemDefaultVatCalculation=='true'}"> 
	var vatExcludeFlag = document.forms['accountLineForms'].elements['accountLine.VATExclude'].value; 
	if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value!=''){ 
		if(vatExcludeFlag !='false') { 
    		var vatRec=0.00;
    		vatRec=document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value;
    		vatRec=parseFloat(vatRec);
    		var vatPay=0.00;
    		vatPay=document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value;
    		vatPay=parseFloat(vatPay);
    		if((vatRec>0)||(vatPay>0)) {
        		if(temp=='2'){
        			document.forms['accountLineForms'].elements['checkChargeCodePopup'].value="found";
        		}
    		if(temp=='1'){
			var agree = confirm("The new charge code entered is VAT Exempt. Click OK to continue or Click Cancel.");
			 if(agree) {
            	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;
	    		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
		        document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=""; 
		        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0;
			   }else { 
				   document.forms['accountLineForms'].elements['accountLine.chargeCode'].value='${accountLine.chargeCode}';	
				   document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}';	
			   }
    		} }	else{ 
			document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
			document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;	 
    		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
			document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=""; 
			document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
			document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
			document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
			document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0;
    		}
			} else { 
				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=false;  
				var rev = document.forms['accountLineForms'].elements['accountLine.category'].value; 
               if(rev != "Internal Cost"){
				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=false;
				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=false;
				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false; 
				}
				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=false;
				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=false; 
				<c:if test="${((!(trackingStatus.soNetworkGroup))  && billingCMMContractType && networkAgent )}">
				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true;
				</c:if>
			} }
	fillRecVatByChargeCode(); 
	fillPayVatByChargeCode(); 
	 </c:if>
	 </c:if> </c:if>
}
function setVatExcludeNew(){
	var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
	<c:if test="${systemDefaultVatCalculation=='true'}"> 
	var vatExcludeFlag = document.forms['accountLineForms'].elements['accountLine.VATExclude'].value;
	var flagVat=document.forms['accountLineForms'].elements['checkChargeCodePopup'].value;
	if(flagVat=="found"){ 
	if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value!=''){ 
		if(vatExcludeFlag !='false') { 
    		var vatRec=0.00;
    		vatRec=document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value;
    		vatRec=parseFloat(vatRec);
    		var vatPay=0.00;
    		vatPay=document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value;
    		vatPay=parseFloat(vatPay);
    		if((vatRec>0)||(vatPay>0)) {
			var agree = confirm("The new charge code entered is VAT Exempt. Click OK to continue or Click Cancel.");
			 if(agree) {
            	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;
	    		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
		        document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=""; 
		        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0;
			   }else { 
				   document.forms['accountLineForms'].elements['accountLine.chargeCode'].value='${accountLine.chargeCode}';	
				   document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}';	
			   }
    		}	else{ 
			document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
			document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
			document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;	 
    		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
			document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=""; 
			document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
			document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
			document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
			document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0;
    		}
			} else { 
				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=false;  
				var rev = document.forms['accountLineForms'].elements['accountLine.category'].value; 
               if(rev != "Internal Cost"){
				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=false;
				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=false;
				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false; 
				}
				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=false;
				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=false; 
			} } }
	fillRecVatByChargeCode();
	fillPayVatByChargeCode();
	 </c:if>}
}
function setLHF() {
	var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value!=''){
	var lsfValue = document.forms['accountLineForms'].elements['fifthDescription'].value;
	var inovoiceValue = document.forms['accountLineForms'].elements['sixthDescription'].value;
	<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
	if(lsfValue !='false') {
	document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=true;
	} else {
	document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=false;
	}
	</configByCorp:fieldVisibility>
	if(inovoiceValue =='true') {
	document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=true;
	} else {
	document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=false;
	}
}else{
<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=false;
</configByCorp:fieldVisibility>
document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=false;
} }}
function noQuotes(evt) {   
  var keyCode= evt.which ? evt.which : evt.keyCode;
   if(keyCode==222 || keyCode==13){
   return false;
   }
   else { return true; } 
}  
function checkChargeCode(){
	var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
	var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
	var billingContract = '${billing.contract}';
	var jobtype = document.forms['accountLineForms'].elements['serviceOrder.Job'].value; 
    var routing = document.forms['accountLineForms'].elements['serviceOrder.routing'].value; 
    var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
    var soCompanyDivision=document.forms['accountLineForms'].elements['serviceOrder.companyDivision'].value;
	chargeCode=chargeCode.trim();
	if(chargeCode!='') {
	if(chargeCode !='${accountLine.chargeCode}'){
	progressBarAutoSave('1');
	if(val=='Internal Cost'){
	var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value; 
	 var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(accountCompanyDivision)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobtype)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
	 http5.open("GET", url, true);
     http5.onreadystatechange = function(){ handleHttpResponse112("Internal");};
     http5.send(null);
	}else{
	if(billingContract=='' || billingContract==' '){
	alert("There is no pricing contract in billing: Please select.");
	document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
	}else{
	 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobtype)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
     http5.open("GET", url, true);
     http5.onreadystatechange =function(){ handleHttpResponse112("NoInternal");};
     http5.send(null);
	} } } }
	else {
		var actualRevenue = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
		if(actualRevenue>0)
		{
		alert("You can not remove charge code as there is value in the receivable details section");
		<c:if test="${not empty accountLine.id}">
		document.forms['accountLineForms'].elements['accountLine.chargeCode'].value='${accountLine.chargeCode}';
		</c:if>
		<c:if test="${empty accountLine.id}">
		document.forms['accountLineForms'].reset();
		</c:if>
		}else{
		document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
        document.forms['accountLineForms'].elements['accountLine.recGl'].value = "";
        document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
        document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}';
        <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
        document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=false;
        </configByCorp:fieldVisibility>
        document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=false;
        document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value="";
        }  } }}
function handleHttpResponse112(temp){
             if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']',''); 
                var res = results.split("#"); 
                var description1 = "";
                var note = "";  
                var wordingTemp="";
                var quoteDescription="";
                 if(results.length>6){
                	if(results.indexOf("Login")==-1)
            	    {      
                		rateOnActualizationDate('accountLine.estSellCurrency','accountLine.estSellValueDate','accountLine.estSellExchangeRate');
                		rateOnActualizationDate('accountLine.revisionSellCurrency','accountLine.revisionSellValueDate','accountLine.revisionSellExchangeRate');
                		rateOnActualizationDate('accountLine.recRateCurrency','accountLine.racValueDate','accountLine.recRateExchange');
                		
                   <c:if test="${accountInterface=='Y'}">
                   <c:if test="${costElementFlag}">
                   	document.forms['accountLineForms'].elements['chargeCostElement'].value=res[11]; 
                    var ctype= document.forms['accountLineForms'].elements['accountLine.category'].value;
               		 ctype=ctype.trim();
               		if(ctype==''){
               			document.forms['accountLineForms'].elements['accountLine.category'].value = res[12];
               		}
               		if(res[1]!=undefined || res[1]!="undefined"){
               			document.forms['accountLineForms'].elements['accountLine.recGl'].value = res[1];	
               		}else{
               			document.forms['accountLineForms'].elements['accountLine.recGl'].value = "";
               		}
               		
               		if(res[2]!=undefined || res[2]!="undefined"){
               			document.forms['accountLineForms'].elements['accountLine.payGl'].value = res[2];	
               		}else{
               			document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
               		}
	                try{
	                    if(temp=="NoInternal"){ 
	                    wordingTemp=res[14];
	                    }else{
	                    wordingTemp=res[13]; 
	                    }
	                    }catch(e){} 
	                populateCostElementData();
					</c:if>
					<c:if test="${!costElementFlag}">
						if(res[1]!=undefined || res[1]!="undefined"){
							document.forms['accountLineForms'].elements['accountLine.recGl'].value = res[1];	
						}else{
							document.forms['accountLineForms'].elements['accountLine.recGl'].value ="";
						}
						
						if(res[2]!=undefined || res[2]!="undefined"){
							document.forms['accountLineForms'].elements['accountLine.payGl'].value = res[2];	
						}else{
							document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
						}
					
	                
	                try{
	                    if(temp=="NoInternal"){ 
	                    wordingTemp=res[14];
	                    }else{
	                    wordingTemp=res[11];
	                    }
	                    }catch(e){}
	                </c:if>
	           <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
	                if(res[5]=='Y'){
	                document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=true;
	                document.forms['accountLineForms'].elements['fifthDescription'].value=true;
	                }else{
	                 document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=false;
	                 document.forms['accountLineForms'].elements['fifthDescription'].value=false;
	                }
	          </configByCorp:fieldVisibility>
	                </c:if>
	                if(res[6]=='Y'){
		                document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=true;
		                document.forms['accountLineForms'].elements['sixthDescription'].value=true;
		                }else{
		                 document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=false;
		                 document.forms['accountLineForms'].elements['sixthDescription'].value=false;
		                }  
	                <configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
	                description1 = document.forms['accountLineForms'].elements['accountLine.description'].value;
	                note = document.forms['accountLineForms'].elements['accountLine.note'].value;
	                quoteDescription = document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value; 
	                document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value=res[7];
	                note=note.trim();
	                description1=description1.trim();
	                quoteDescription=quoteDescription.trim();
	                if(description1==''){
		                if(wordingTemp==''){
		                	if(res[4]!=undefined || res[4]!="undefined"){
		                		document.forms['accountLineForms'].elements['accountLine.description'].value=res[4];	
                			}else{
                				document.forms['accountLineForms'].elements['accountLine.description'].value="";
                			}
		                }else{
		                	document.forms['accountLineForms'].elements['accountLine.description'].value=wordingTemp;
		                }  }
	                if(note==''){

	                if(wordingTemp==''){
	                	if(res[4]!=undefined || res[4]!="undefined"){
	                		document.forms['accountLineForms'].elements['accountLine.note'].value=res[4];	
            			}else{
            				document.forms['accountLineForms'].elements['accountLine.note'].value=res[4];
            			}
	                	
	                }else{
	                	document.forms['accountLineForms'].elements['accountLine.note'].value=wordingTemp;
	                } }
	                if(quoteDescription==''){
		                if(wordingTemp==''){
		                	if(res[4]!=undefined || res[4]!="undefined"){
		                		document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=res[4];	
                			}else{
                				document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value="";
                			}
		                }else{
		                	document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=wordingTemp;
		                }  }
	                </configByCorp:fieldVisibility>
	                <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
	                shipNumber = document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
	                if(wordingTemp==''){
	                	if(res[4]!=undefined || res[4]!="undefined"){
	                		document.forms['accountLineForms'].elements['accountLine.description'].value=res[4];
	                		<configByCorp:fieldVisibility componentId="component.field.Description.NwvlChargeCode">
	                		document.forms['accountLineForms'].elements['accountLine.description'].value=res[4]+' - '+shipNumber;	
	                        </configByCorp:fieldVisibility>
	                	}else{
            				document.forms['accountLineForms'].elements['accountLine.description'].value="";
            			}
	                }else{
	                	document.forms['accountLineForms'].elements['accountLine.description'].value=wordingTemp;
	                	<configByCorp:fieldVisibility componentId="component.field.Description.NwvlChargeCode">
	                	document.forms['accountLineForms'].elements['accountLine.description'].value=wordingTemp+' - '+shipNumber;
	                    </configByCorp:fieldVisibility>
	                }

	                if(wordingTemp==''){
	                	if(res[4]!=undefined || res[4]!="undefined"){
	                		document.forms['accountLineForms'].elements['accountLine.note'].value=res[4];	
	                		<configByCorp:fieldVisibility componentId="component.field.Description.NwvlChargeCode">
	                		document.forms['accountLineForms'].elements['accountLine.note'].value=res[4]+' - '+shipNumber;	
	                		 </configByCorp:fieldVisibility>
	                	}else{
            				document.forms['accountLineForms'].elements['accountLine.note'].value=res[4];
            			}
	                }else{
	                  	document.forms['accountLineForms'].elements['accountLine.note'].value=wordingTemp;
	                	<configByCorp:fieldVisibility componentId="component.field.Description.NwvlChargeCode">
	                	document.forms['accountLineForms'].elements['accountLine.note'].value=wordingTemp+' - '+shipNumber;
	                    </configByCorp:fieldVisibility>
	                }

	                if(wordingTemp==''){
	                	if(res[4]!=undefined || res[4]!="undefined"){
	                		document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=res[4];	
            			}else{
            				document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value="";
            			}
	                	
	                }else{
	                	document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=wordingTemp;
	                } 
	                </configByCorp:fieldVisibility>
                	<c:if test="${systemDefaultVatCalculation=='true'}">
                	<c:if test="${(((accountLine.createdBy != 'Networking' && ( !(((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 ) &&  not empty accountLine.networkSynchedId ) && !(trackingStatus.accNetworkGroup)) )) && (accountLine.chargeCode != 'MGMTFEE')) || !(trackingStatus.soNetworkGroup) )}">
                	if(res[8]=='Y'){
    		    		var vatRec=0.00;
    		    		vatRec=document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value;
    		    		vatRec=parseFloat(vatRec);
    		    		var vatPay=0.00;
    		    		vatPay=document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value;
    		    		vatPay=parseFloat(vatPay);
    		    		if((vatRec>0)||(vatPay>0))
    		    		{
    		    			var agree = confirm("The new charge code entered is VAT Exempt. Click OK to continue or Click Cancel.");
    		    			 if(agree) {
    		                 	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
    		    				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
    		    				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
    		    				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
    		    				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
    		    				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
    		    				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;
    				    		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
    					        document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=""; 
    					        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
    					        document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
    					        document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
    					        document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0;	
    		    			   }else { 
    		    				   document.forms['accountLineForms'].elements['accountLine.chargeCode'].value='${accountLine.chargeCode}';
    		    			   }
    		    		} else{
                	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
    				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
    				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
    				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
    				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
    				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
    				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;
		    		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
			        document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=""; 
			        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
			        document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
			        document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
			        document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0;
    		    		}
                	}else{
                	    document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; 
                		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=false; 
	    				if(temp=="NoInternal"){
	    				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=false;
	    				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=false;
	    				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false;
	    				}
	    				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=false;
	    				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=false;
                	}
                	</c:if>
                	</c:if>
                	<c:if test="${contractType}">  
                	if('${accountLine.recInvoiceNumber}' ==''){ 
                			if(res[9]!=undefined || res[9]!="undefined"){
                				document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value=res[9];	
                			}else{
                				document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value="";
                			}
                	}
                	if(document.forms['accountLineForms'].elements['accountLine.payingStatus'].value!='A'){
                	document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value=res[10]; 
                	}
                	checkChargesData();
                	</c:if> 
                	<configByCorp:fieldVisibility componentId="component.accountLine.BillingCurrencyFromCharge">
                	if(res[9] !='' && res[9]!=undefined &&  res[9]!="undefined"){
        				document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value=res[9];
        				document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value=res[9];
        				document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value=res[9];
        			}
                	if(res[10] !='' && res[10]!=undefined &&  res[10]!="undefined"){
        				document.forms['accountLineForms'].elements['accountLine.estCurrency'].value=res[10];
        				document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value=res[10];
        				document.forms['accountLineForms'].elements['accountLine.country'].value=res[10];
        			}
                	</configByCorp:fieldVisibility>
               findInternalCostVendorCode('2');}else{
		    		alert("Session has been ended, Login Again");
		    		window.location=document.location.href;  
               } 
                	 fillDefaultInvoiceNumber();
              }else{
                    if(temp=="NoInternal"){
                    document.getElementById("ChargeNameDivId").style.display = "none";
                    document.forms['accountLineForms'].elements['accountLine.chargeCode'].focus();
                    alert("Charge code does not exist according the billing contract, Please select another" );
                    document.getElementById("ChargeNameDivId").style.display = "none";
                     progressBarAutoSave('0');
                    }else{
                    document.forms['accountLineForms'].elements['accountLine.chargeCode'].focus();	
                    alert("Charge code does not exist according the Company Division, Please select another" );
                     progressBarAutoSave('0');
                    }
                    document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
                    document.forms['accountLineForms'].elements['accountLine.recGl'].value = "";
                    document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
                    document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}';
                    <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
                    document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=false;
                    </configByCorp:fieldVisibility>
	                document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=false;
	                document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value="";
				}   progressBarAutoSave('0');   }   } 
function onlyforInvoice(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;  
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110)||(keyCode==188)||(keyCode==191)||(keyCode==220)||(keyCode==111)||(keyCode==106)||(keyCode==144)||(keyCode==8)||(keyCode==96)||(keyCode==97)||(keyCode==98)||(keyCode==99)||(keyCode==100)||(keyCode==101)||(keyCode==102)||(keyCode==103)||(keyCode==104)||(keyCode==105)||(keyCode==107) ||(keyCode==173); 
	}
	function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
	} 
	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode; 
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
	}
	function onlyCharsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	function onlyAlphaNumericAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110)|| (keyCode==222); 
	}	
	function onlyRateAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode; 
		return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110|| (keyCode==173)||(keyCode==189)); 
	}
	function checkCMMAgentPIR(){
		<c:if test="${trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && accountLine.paymentStatus=='PIR' }">
		 var agree =confirm("This line already linked with Harmony as CMM contract. Would you like to chnage Payment Status ? ");
         if(agree) {
         }
		 else{
		 document.forms['accountLineForms'].elements['accountLine.paymentStatus'].value='${accountLine.paymentStatus}';
		 }
		</c:if> 
	}
	function isNumeric(targetElement){   
		var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++){   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        return false;
	        } }
	    return true;
	} 
function notExists(){
	alert("The Account Line information has not been saved yet.");
} 
function setCategoryType(){ 
 	var category = document.forms['accountLineForms'].elements['accountLine.category'].value; 
 	if(category=='Internal Cost'||oldcategory=='Internal Cost'){
 	document.forms['accountLineForms'].elements['accountLine.chargeCode'].value="";
    <c:if test="${accountInterface=='Y'}">
 	document.forms['accountLineForms'].elements['accountLine.recGl'].value="";
 	document.forms['accountLineForms'].elements['accountLine.payGl'].value="";
 	</c:if> 
 	document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value="";
 	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}';
 	}
 	<c:if test="${!(networkAgent)}"> 
 	if(category=='Origin' && document.forms['accountLineForms'].elements['accountLine.vendorCode'].value==''){
 		document.forms['accountLineForms'].elements['accountLine.vendorCode'].value='${trackingStatus.originAgentCode}'
 	    <c:if test="${accountInterface!='Y'}">
  	      checkVendorName();fillCurrencyByChargeCode();changeStatus();
  	    </c:if>
  	    <c:if test="${accountInterface=='Y'}">
  	      vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
  	    </c:if>
 	} 
 	if(category=='Destin' && document.forms['accountLineForms'].elements['accountLine.vendorCode'].value==''){
 		document.forms['accountLineForms'].elements['accountLine.vendorCode'].value='${trackingStatus.destinationAgentCode}'
 		<c:if test="${accountInterface!='Y'}">
	      checkVendorName();fillCurrencyByChargeCode();changeStatus();
	    </c:if>
	    <c:if test="${accountInterface=='Y'}">
	      vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
	    </c:if>
 	}
 	</c:if>
 	}
 function setCompanyDivision(){ 
 	var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value; 
 	if(companyDivision!=oldCompanyDivision){
 	document.forms['accountLineForms'].elements['accountLine.chargeCode'].value="";
    <c:if test="${accountInterface=='Y'}">
 	document.forms['accountLineForms'].elements['accountLine.recGl'].value="";
 	document.forms['accountLineForms'].elements['accountLine.payGl'].value="";
 	</c:if> 
 	document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value="";
 	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value='${accountLine.VATExclude}';
 	}  } 
 var oldcategory="";
 function checkCategoryType(){
	 <configByCorp:fieldVisibility componentId="component.AccountLine.Category.NonEditInvoicedLine">
	 if (document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!=''){
		 alert('Category Type can not be changed as invoice is already generated for this line.');	
	    } 
	    </configByCorp:fieldVisibility>
	 oldcategory=document.forms['accountLineForms'].elements['accountLine.category'].value;
 }
 var oldCompanyDivision="";
 function checkCompanyDivision(){
 if (document.forms['accountLineForms'].elements['accountLine.recAccDate'].value!='' || document.forms['accountLineForms'].elements['accountLine.payAccDate'].value!='') { 	 
	alert('You can not change the CompanyDivision as Sent To Acc has been already filled');
 }	
 <c:if test="${singleCompanyDivisionInvoicing}">
 if (document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!=''){
	 alert('Company division can not be changed as invoice is already generated for this line.'); 
 } 
</c:if> 
 oldCompanyDivision=document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;
 } 
 function winOpen(){
 	var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
 	  openWindow('findVendorCode.html?sid=${serviceOrder.id}&partnerType=DF&accType=ACCNew&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();	
 }
 function findPartnersAgent() {
 openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.branchName&fld_code=accountLine.branchCode');
 }
 function activeStatusCheck(target){
	 var receivableAmount = eval(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value);
	 var payableAmount = eval(document.forms['accountLineForms'].elements['accountLine.actualExpense'].value);
	 <c:if test="${accountInterface!='Y'}">
	 if(receivableAmount!='0'||payableAmount!='0')  {
	    if(document.forms['accountLineForms'].elements['accountLine.status'].checked != true) {
	    		alert("Receivable Detail Revenue or Payable Detail Expense is not zero");
	    		return false;
	    	} }
	    </c:if>
	    <c:if test="${accountInterface=='Y'}">
	    var payingStatusCheck=""
	    <configByCorp:fieldVisibility componentId="component.accountLine.approvedPayingStatus.disableStatus">
	    if(document.forms['accountLineForms'].elements['accountLine.payingStatus'].value=='A'){
	    	payingStatusCheck='yes';
	    } 
	    </configByCorp:fieldVisibility>
	     var accruePayable=document.forms['accountLineForms'].elements['accountLine.accruePayable'].value;
	     var accrueRevenue=document.forms['accountLineForms'].elements['accountLine.accrueRevenue'].value;
	     var recAccDate=document.forms['accountLineForms'].elements['accountLine.recAccDate'].value;
	     var payAccDate=document.forms['accountLineForms'].elements['accountLine.payAccDate'].value;
	     var receivedInvoiceDate=document.forms['accountLineForms'].elements['accountLine.receivedInvoiceDate'].value;
	    if(accruePayable!=''||accrueRevenue!=''||recAccDate!=''||payAccDate!=''||receivedInvoiceDate!='' || payingStatusCheck!=''){
	    if(document.forms['accountLineForms'].elements['accountLine.status'].checked != true) {
	    	if(payingStatusCheck!=''){
	    		alert("This account line cannot be deactivated as either it has been Accrued, Payable Approved, transferred to Accounting and/or Invoiced."); 
	    	}else{ 
	    	   alert("This account line cannot be deactivated as either it has been Accrued, transferred to Accounting and/or Invoiced.");
	    	}
	    	document.forms['accountLineForms'].elements['accountLine.status'].disabled=true;
	    		return false;
	    	}  }
	    </c:if> 
	    <c:if test="${((trackingStatus.accNetworkGroup) && (billingDMMContractType) && (trackingStatus.soNetworkGroup) )}">
	    <c:if test="${not empty accountLine.id}">
		 progressBarAutoSave('1');
		 var idss='${accountLine.id}';
	     var url="checkNetworkAgentInvoice.html?ajax=1&decorator=simple&popup=true&networkAgentId=" + encodeURI(idss);
	     httpNetworkAgent.open("GET", url, true);
	     httpNetworkAgent.onreadystatechange = checkNetworkAgentInvoiceResponse; 
	     httpNetworkAgent.send(null);
	     </c:if>  </c:if> 
 }
 function checkNetworkAgentInvoiceResponse(){
  if (httpNetworkAgent.readyState == 4)  { 
      var results = httpNetworkAgent.responseText
      results = results.trim(); 
      if(results=='true'){
   	   alert("This account line cannot be deactivated as either it has been transferred to Accounting and/or Invoiced in UTSI Instance ");
   	   document.forms['accountLineForms'].elements['accountLine.status'].checked = true
   	   document.forms['accountLineForms'].elements['accountLine.status'].disabled=true;
   	   return false;
      } }   
	  progressBarAutoSave('0');
}
function activeAccrueStatus(){
	editTrap();
 <c:if test="${accountInterface=='Y'}">
 document.forms['accountLineForms'].elements['accountLine.accrueRevenueManual'].disabled=false;
document.forms['accountLineForms'].elements['accountLine.accruePayableManual'].disabled=false; 
</c:if>
document.forms['accountLineForms'].elements['accountLine.paymentStatus'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.country'].disabled=false;   
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
document.forms['accountLineForms'].elements['accountLine.glType'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.commission'].disabled=false; 
</c:if>
<c:if test="${multiCurrency=='Y'}">
document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false; 
</c:if> 
<c:if test="${systemDefaultVatCalculation=='true'}">
document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false;
document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=false; 
</c:if> 
return true;
}
function autoSaveFunc(clickType) {
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	editTrap();
<c:if test="${(trackingStatus.soNetworkGroup  && (accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 &&  not empty accountLine.networkSynchedId ) && !(trackingStatus.accNetworkGroup) ))) ||(accountLine.chargeCode == 'MGMTFEE') || accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' }">
document.forms['accountLineForms'].setAttribute('bypassDirtyCheck',false);
document.forms['accountLineForms'].elements['formStatus'].value = '0'
</c:if>
<c:if test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
document.forms['accountLineForms'].setAttribute('bypassDirtyCheck',false);
document.forms['accountLineForms'].elements['formStatus'].value = '0'

</c:if>
var checkLHFValue=document.forms['accountLineForms'].elements['checkLHF'].value;
var chargeCode=document.forms['accountLineForms'].elements['accountLine.chargeCode'].value
if(checkLHFValue=='true'&& (chargeCode.toLowerCase()=='lhf')) {
alert("Account line with charge code LHF already exists");
return false;
} 
<c:if test="${accountInterface=='Y'}">
document.forms['accountLineForms'].elements['accountLine.accrueRevenueManual'].disabled=false;
document.forms['accountLineForms'].elements['accountLine.accruePayableManual'].disabled=false;  
</c:if>
document.forms['accountLineForms'].elements['accountLine.paymentStatus'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].disabled=false;
document.forms['accountLineForms'].elements['accountLine.country'].disabled=false;   
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
document.forms['accountLineForms'].elements['accountLine.glType'].disabled=false; 
document.forms['accountLineForms'].elements['accountLine.commission'].disabled=false; 
</c:if> 
<c:if test="${multiCurrency=='Y'}">
document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false; 
</c:if>
<c:if test="${systemDefaultVatCalculation=='true'}">
document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false;
document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=false; 
</c:if> 
progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){ 
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 =document.forms['accountLineForms'].elements['serviceOrder.id'].value; 
           if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.serviceorderDetails')  {
             noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.billingDetails')  {
             noSaveAction = 'editBilling.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.accountLineList')  {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.containerList') {
        	  <c:if test="${forwardingTabVal!='Y'}">
        	  		noSaveAction = 'containers.html?id='+id1;
        	  </c:if>
       		  <c:if test="${forwardingTabVal=='Y'}">
       		  	noSaveAction = 'containersAjaxList.html?id='+id1;
       		  </c:if>
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.OI')  {
              noSaveAction = 'operationResource.html?id='+id1;
            }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.domestic')  {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.trackingStatusDetails')  {
              <c:if test="${serviceOrder.job =='RLO'}">
              noSaveAction = 'editDspDetails.html?id='+id1; 
			</c:if>
          <c:if test="${serviceOrder.job !='RLO'}">
          noSaveAction =  'editTrackingStatus.html?id='+id1; 
			</c:if> 
			 }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.customerWorkTicketsList')  {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.claimsList') {
             noSaveAction = 'claims.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.CustomerFileDetails')  {
             var cidVal='${customerFile.id}';
        	  noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.criticaldate')  {
              noSaveAction ='soAdditionalDateDetails.html?sid='+id1;
            }  
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveNextAccountLine')  {
        	  noSaveAction ='editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
            }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.movePrevAccountLine')  {
        	  noSaveAction ='editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
            }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveSelectAccount')  {
        	  noSaveAction ='editAccountLine.html?sid=${serviceOrder.id}&id='+document.getElementById('setSelectedAccountId').value;
            }
           if (formIsDirty(document.forms['accountLineForms'])){
           	if(document.forms['accountLineForms'].elements['billing.billComplete'].value!=''){ 
           		if (document.forms['accountLineForms'].elements['formStatus'].value == '1' || '${formStatus}' == '1')  {
               	 var agree;
                    if(document.getElementById('setSelectedAccountId').value != ''){
                   		 agree = confirm(" Do you want to save the changes you made?");
                    }
                    else{
                    	 agree = confirm("The billing has been completed, do you want to still save accountline?");
                    }
           		}
           		<c:if test="${contractType}">
           		if(agree) { 
           		    if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
           		        	    || document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
           		        	    || document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value
           		        	    || document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value
           		        	    || document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value
           		        	    || document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value){
           		        	 if('${accountLine.invoiceNumber}' ==''){
           		        	 if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
           		        			 && (eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value) ) ) {
           		        		agree = confirm("Same contract and billing currency but different FX rate in Estimate Expense section. Are you sure you want to save the accountline?");
           		        	 }}
           		        	 if(agree){
           		        	 if('${accountLine.recInvoiceNumber}' ==''){
           		        	 if(document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
           		        			 && (eval(document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value) ) ) {		 
           		        		agree = confirm("Same contract and billing currency but different FX rate in Estimate Revenue section. Are you sure you want to save the accountline?");
           		        	  }}}
           		        	 if(agree){
           		        	if('${accountLine.invoiceNumber}' ==''){	 
           		        	 if(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value 
           		        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value) ) ) {
           		        		agree = confirm("Same contract and billing currency but different FX rate in Revision Expense section. Are you sure you want to save the accountline?");
           		        	 }}}
           		        	 if(agree){
           		        	 if('${accountLine.recInvoiceNumber}' ==''){	 
           		        	 if(document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value 
           		        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value ) != eval(document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value)  ) ) {
           		        		agree = confirm("Same contract and billing currency but different FX rate in Revision Revenue section. Are you sure you want to save the accountline?");
           		        	 }}}
           		        	 if(agree){
           		        	 if('${accountLine.recInvoiceNumber}' ==''){	 
           		        	 if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value 
           		        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value ) != eval(document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value) 	) ) {
           		        		agree = confirm("Same contract and billing currency but different FX rate in Receivable Details section. Are you sure you want to save the accountline?");
           		        	 }}}
           		        	 if(agree){
           		        	if('${accountLine.invoiceNumber}' ==''){	 
           		        	 if(document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value 
           		        			 && (eval(document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value)   ) ) {
           		        		agree = confirm("Same contract and billing currency but different FX rate in Payable Details section. Are you sure you want to save the accountline?");
           		        	 }}}
           		        	
           		        			
           		         }
           		    }
           		</c:if>
              if(agree)  {
                 processAutoSave(document.forms['accountLineForms'], 'saveAccountLine!saveOnTabChange.html', noSaveAction); 
                }else  {
               if(id1 != '')  {
                 if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.serviceorderDetails')  {
                    location.href = 'editServiceOrderUpdate.html?id='+id1;
                  }
                 if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.billingDetails')  {
                   location.href = 'editBilling.html?id='+id1;
                  }
                 if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.accountLineList')  {
                    location.href = 'accountLineList.html?sid='+id1;
                  }
                 if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.containerList')  {
                    <c:if test="${forwardingTabVal!='Y'}">
                    	location.href = 'containers.html?id='+id1;
        	  		</c:if>
       		  		<c:if test="${forwardingTabVal=='Y'}">
       					location.href = 'containersAjaxList.html?id='+id1;
       		  		</c:if>
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.OI')  {
                     location.href = 'operationResource.html?id='+id1;
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.domestic')  {
                    location.href = 'editMiscellaneous.html?id='+id1;
                 }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.trackingStatusDetails')  {
                    <c:if test="${serviceOrder.job =='RLO'}">
                    location.href = 'editDspDetails.html?id='+id1; 
				</c:if>
                <c:if test="${serviceOrder.job !='RLO'}">
                location.href =  'editTrackingStatus.html?id='+id1; 
				</c:if>
				   }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.customerWorkTicketsList') {
                   location.href = 'customerWorkTickets.html?id='+id1;
                 }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.claimsList')   {
                    location.href = 'claims.html?id='+id1;
                 }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.CustomerFileDetails') {
                   var cidVal='${customerFile.id}';
                	location.href ='editCustomerFile.html?id='+cidVal;
                }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.criticaldate')  {
                	location.href ='soAdditionalDateDetails.html?sid='+id1;
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveNextAccountLine')  {
                    location.href ='editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.movePrevAccountLine')  {
                    location.href ='editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveSelectAccount')  {
                      location.href ='editAccountLine.html?sid=${serviceOrder.id}&id='+document.getElementById('setSelectedAccountId').value;
                  } } }
        }else{
        	if (document.forms['accountLineForms'].elements['formStatus'].value == '1' || '${formStatus}' == '1')  {
              	 var agree;
                   if(document.getElementById('setSelectedAccountId').value != ''){
                  		 agree = confirm(" Do you want to save the changes you made?");
                   }else{
						agree=true;
                   }
        	}
        	<c:if test="${contractType}">
        	if(agree) { 
        	    if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
        	        	    || document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
        	        	    || document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value
        	        	    || document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value
        	        	    || document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value
        	        	    || document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value){
        	    	     if('${accountLine.invoiceNumber}' ==''){
        	        	 if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
        	        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value) ) ) {
        	        		agree = confirm("Same contract and billing currency but different FX rate in Estimate Expense section. Are you sure you want to save the accountline?");
        	        	 }}
        	        	 if(agree){
        	        	 if('${accountLine.recInvoiceNumber}' ==''){	 
        	        	 if(document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
        	        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value)  ) ) {		 
        	        	 agree = confirm("Same contract and billing currency but different FX rate in Estimate Revenue section. Are you sure you want to save the accountline?");
        	        	  }}}
        	        	 if(agree){
        	        		 if('${accountLine.invoiceNumber}' ==''){	 
        	        	 if(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value 
        	        			 && (eval(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value ) != eval(document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value)  ) ) {
        	        		agree = confirm("Same contract and billing currency but different FX rate in Revision Expense section. Are you sure you want to save the accountline?");
        	        	 }}}
        	        	 if(agree){
        	        		 if('${accountLine.recInvoiceNumber}' ==''){ 
        	        	 if(document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value 
        	        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value) ) ) {
        	        		agree = confirm("Same contract and billing currency but different FX rate in Revision Revenue section. Are you sure you want to save the accountline?");
        	        	 }}}
        	        	 if(agree){
        	        		 if('${accountLine.recInvoiceNumber}' ==''){  
        	        	 if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value 
        	        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value)   ) ) {
        	        		agree = confirm("Same contract and billing currency but different FX rate in Receivable Details section. Are you sure you want to save the accountline?");
        	        	 }}}
        	        	 if(agree){
        	        		 if('${accountLine.invoiceNumber}' ==''){	 
        	        	 if(document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value 
        	        			 && (eval(document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value)  ) ) {
        	        		agree = confirm("Same contract and billing currency but different FX rate in Payable Details section. Are you sure you want to save the accountline?");
        	        	 }}}
        	        	
        	        			
        	         }
        	    }
        	</c:if>
        	if(agree){
        		processAutoSave(document.forms['accountLineForms'], 'saveAccountLine!saveOnTabChange.html', noSaveAction);
        	}else{
        		if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveNextAccountLine')  {
                    location.href ='editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.movePrevAccountLine')  {
                    location.href ='editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
                  }
                if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveSelectAccount')  {
                      location.href ='editAccountLine.html?sid=${serviceOrder.id}&id='+document.getElementById('setSelectedAccountId').value;
                  } 
                if(id1 != '')  {
                    if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.serviceorderDetails')  {
                       location.href = 'editServiceOrderUpdate.html?id='+id1;
                     }
                    if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.billingDetails')  {
                      location.href = 'editBilling.html?id='+id1;
                     }
                    if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.accountLineList')  {
                       location.href = 'accountLineList.html?sid='+id1;
                     }
                    if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.containerList')  {
                       <c:if test="${forwardingTabVal!='Y'}">
                   			location.href = 'containers.html?id='+id1;
       	  				</c:if>
      		  			<c:if test="${forwardingTabVal=='Y'}">
      						location.href = 'containersAjaxList.html?id='+id1;
      		  			</c:if>
                     }
                     if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.OI')  {
                         location.href = 'operationResource.html?id='+id1;
                      }
                   if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.domestic')  {
                       location.href = 'editMiscellaneous.html?id='+id1;
                    }
                   if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.trackingStatusDetails')  {
                       <c:if test="${serviceOrder.job =='RLO'}">
                       location.href = 'editDspDetails.html?id='+id1; 
   				</c:if>
                   <c:if test="${serviceOrder.job !='RLO'}">
                   location.href =  'editTrackingStatus.html?id='+id1; 
   				</c:if>
   				   }
                   if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.customerWorkTicketsList') {
                      location.href = 'customerWorkTickets.html?id='+id1;
                    }
                   if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.claimsList')   {
                       location.href = 'claims.html?id='+id1;
                    }
                   if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.CustomerFileDetails') {
                      var cidVal='${customerFile.id}';
                	   location.href ='editCustomerFile.html?id='+cidVal;
                   }
                   if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.criticaldate')  {
                   		location.href ='soAdditionalDateDetails.html?sid='+id1;
                     }  } }  }	
     }  else{ 
    	 var agree;
		    	 if (document.forms['accountLineForms'].elements['formStatus'].value == '1' || '${formStatus}' == '1')  {
	                    if(document.getElementById('setSelectedAccountId').value != ''){
	                   		 agree = confirm(" Do you want to save the changes you made?");
	                    }
	              }
		    	 <c:if test="${contractType}">
		    	 if(agree) { 
		    	     if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
		    	         	    || document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
		    	         	    || document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value
		    	         	    || document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value
		    	         	    || document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value
		    	         	    || document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value){
		    	    	 if('${accountLine.invoiceNumber}' ==''){	 
		    	         	 if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
		    	         			 && (eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value)  ) ) {
		    	         		agree = confirm("Same contract and billing currency but different FX rate in Estimate Expense section. Are you sure you want to save the accountline?");
		    	         	 }}
		    	         	 if(agree){
		    	         		if('${accountLine.recInvoiceNumber}' ==''){  
		    	         	 if(document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
		    	         			 && ( eval(document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value)  ) ) {		 
		    	         	 agree = confirm("Same contract and billing currency but different FX rate in Estimate Revenue section. Are you sure you want to save the accountline?");
		    	         	  }}}
		    	         	 if(agree){
		    	         		if('${accountLine.invoiceNumber}' ==''){	 
		    	         	 if(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value 
		    	         			 && (eval(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value)  ) ) {
		    	         		agree = confirm("Same contract and billing currency but different FX rate in Revision Expense section. Are you sure you want to save the accountline?");
		    	         	 }}}
		    	         	 if(agree){
		    	         		if('${accountLine.recInvoiceNumber}' ==''){
		    	         	 if(document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value 
		    	         			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value)  ) ) {
		    	         		agree = confirm("Same contract and billing currency but different FX rate in Revision Revenue section. Are you sure you want to save the accountline?");
		    	         	 }}}
		    	         	 if(agree){
		    	         		if('${accountLine.recInvoiceNumber}' ==''){
		    	         	 if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value 
		    	         			 && ( eval(document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value) 	) ) {
		    	         		agree = confirm("Same contract and billing currency but different FX rate in Receivable Details section. Are you sure you want to save the accountline?");
		    	         	 }}}
		    	         	 if(agree){
		    	         		if('${accountLine.invoiceNumber}' ==''){	
		    	         	 if(document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value 
		    	         			 && ( eval(document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value) ) ) {
		    	         		agree = confirm("Same contract and billing currency but different FX rate in Payable Details section. Are you sure you want to save the accountline?");
		    	         	 }}}
		    	         	
		    	         			
		    	          }
		    	     }
		    	 </c:if>
			   if(agree)  {
				   document.forms['accountLineForms'].action ='saveAccountLine!saveOnTabChange.html';
		           document.forms['accountLineForms'].submit(); 
			     }else{
			    	window.location = noSaveAction; 
				  }  } 
	 } else{ 
   if(!(clickType == 'save')) {
     var id1 =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
     if (document.forms['accountLineForms'].elements['formStatus'].value == '1' || '${formStatus}' == '1')  {
    	 var agree;
         if(document.getElementById('setSelectedAccountId').value != ''){
        	 agree = confirm(" Do you want to save the changes you made?");
         }
         else{
        	 agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='accountLineDetail.heading'/>");
         }
         <c:if test="${contractType}">
         if(agree) { 
             if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
                 	    || document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
                 	    || document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value
                 	    || document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value
                 	    || document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value
                 	    || document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value){
            	 if('${accountLine.invoiceNumber}' ==''){	
                 	 if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
                 			 && (  eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value) ) ) {
                 		agree = confirm("Same contract and billing currency but different FX rate in Estimate Expense section. Are you sure you want to save the accountline?");
                 	 }}
                 	 if(agree){
                 		if('${accountLine.recInvoiceNumber}' ==''){
                 	 if(document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
                 			 && ( eval(document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value)  ) ) {		 
                 	 agree = confirm("Same contract and billing currency but different FX rate in Estimate Revenue section. Are you sure you want to save the accountline?");
                 	  }}}
                 	 if(agree){
                 		if('${accountLine.invoiceNumber}' ==''){	
                 	 if(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value 
                 			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value) ) ) {
                 		agree = confirm("Same contract and billing currency but different FX rate in Revision Expense section. Are you sure you want to save the accountline?");
                 	 }}}
                 	 if(agree){
                 		if('${accountLine.recInvoiceNumber}' ==''){
                 	 if(document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value 
                 			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value) ) ) {
                 		agree = confirm("Same contract and billing currency but different FX rate in Revision Revenue section. Are you sure you want to save the accountline?");
                 	 }}}
                 	 if(agree){
                 		if('${accountLine.recInvoiceNumber}' ==''){
                 	 if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value 
                 			 && ( eval(document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value)  ) ) {
                 		agree = confirm("Same contract and billing currency but different FX rate in Receivable Details section. Are you sure you want to save the accountline?");
                 	 }}}
                 	 if(agree){
                 		if('${accountLine.invoiceNumber}' ==''){	
                 	 if(document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value 
                 			 && ( eval(document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value)  ) ) {
                 		agree = confirm("Same contract and billing currency but different FX rate in Payable Details section. Are you sure you want to save the accountline?");
                 	 }}}
                 	
                 			
                  }
             }
         </c:if>
       if(agree) { 
        if(document.forms['accountLineForms'].elements['billing.billComplete'].value!=''){
        	 agree = confirm("The billing has been completed, do you want to still save accountline?");
        	 if(agree){
        		 document.forms['accountLineForms'].action ='saveAccountLine!saveOnTabChange.html';
                 document.forms['accountLineForms'].submit(); 
        	 }else{
        		 if(id1 != '')  {
        	           if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.serviceorderDetails') {
        	             location.href = 'editServiceOrderUpdate.html?id='+id1;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.billingDetails') {
        	             location.href = 'editBilling.html?id='+id1;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.accountLineList')  {
        	              location.href = 'accountLineList.html?sid='+id1;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.containerList')  {
        	             <c:if test="${forwardingTabVal!='Y'}">
                     		location.href = 'containers.html?id='+id1;
         	  			</c:if>
        		  		<c:if test="${forwardingTabVal=='Y'}">
        					location.href = 'containersAjaxList.html?id='+id1;
        		  		</c:if>
        	           }
        	           if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.OI')   {
          	             location.href = 'operationResource.html?id='+id1;
          	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.domestic')   {
        	             location.href = 'editMiscellaneous.html?id='+id1;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.trackingStatusDetails')  {
        	              <c:if test="${serviceOrder.job =='RLO'}">
        	              location.href = 'editDspDetails.html?id='+id1; 
        				</c:if>
        	          <c:if test="${serviceOrder.job !='RLO'}">
        	          location.href =  'editTrackingStatus.html?id='+id1; 
        				</c:if> 
        				 }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.customerWorkTicketsList')  {
        	             location.href = 'customerWorkTickets.html?id='+id1;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.claimsList') {
        	             location.href = 'claims.html?id='+id1;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.CustomerFileDetails')  {
        	             var cidVal='${customerFile.id}';
        	        	  location.href ='editCustomerFile.html?id='+cidVal;
        	           }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.criticaldate')  {
        	          		location.href ='soAdditionalDateDetails.html?sid='+id1;
        	            }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveNextAccountLine')  {
        	              location.href ='editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
        	            }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.movePrevAccountLine')  {
        	              location.href ='editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
        	            }
        	          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveSelectAccount')  {
        	                location.href ='editAccountLine.html?sid=${serviceOrder.id}&id='+document.getElementById('setSelectedAccountId').value;
        	              }  }   }  }else{
           document.forms['accountLineForms'].action ='saveAccountLine!saveOnTabChange.html';
           document.forms['accountLineForms'].submit();
        	              }
        } else  {
         if(id1 != '')  {
           if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.serviceorderDetails') {
             location.href = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.billingDetails') {
             location.href = 'editBilling.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.accountLineList')  {
              location.href = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.containerList')  {
             <c:if test="${forwardingTabVal!='Y'}">
         		location.href = 'containers.html?id='+id1;
	  		</c:if>
	  		<c:if test="${forwardingTabVal=='Y'}">
				location.href = 'containersAjaxList.html?id='+id1;
	  		</c:if>
           }
           if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.OI')   {
               location.href = 'operationResource.html?id='+id1;
             }  
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.domestic')   {
             location.href = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.trackingStatusDetails')  {
              <c:if test="${serviceOrder.job =='RLO'}">
              location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
          <c:if test="${serviceOrder.job !='RLO'}">
          location.href =  'editTrackingStatus.html?id='+id1; 
			</c:if> 
			 }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.customerWorkTicketsList')  {
             location.href = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.claimsList') {
             location.href = 'claims.html?id='+id1;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.CustomerFileDetails')  {
             var cidVal='${customerFile.id}';
        	  location.href ='editCustomerFile.html?id='+cidVal;
           }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.criticaldate')  {
          		location.href ='soAdditionalDateDetails.html?sid='+id1;
            }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveNextAccountLine')  {
              location.href ='editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
            }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.movePrevAccountLine')  {
              location.href ='editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
            }
          if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveSelectAccount')  {
                location.href ='editAccountLine.html?sid=${serviceOrder.id}&id='+document.getElementById('setSelectedAccountId').value;
              }  } }
    } else {
     if(id1 != '')
      {
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.serviceorderDetails')  {
          location.href = 'editServiceOrderUpdate.html?id='+id1;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.billingDetails')  {
          location.href = 'editBilling.html?id='+id1;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.accountLineList')  {
          location.href = 'accountLineList.html?sid='+id1;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.containerList') {
          <c:if test="${forwardingTabVal!='Y'}">
      			location.href = 'containers.html?id='+id1;
			</c:if>
	  		<c:if test="${forwardingTabVal=='Y'}">
				location.href = 'containersAjaxList.html?id='+id1;
	  		</c:if>
        }
        if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.OI') {
            location.href = 'operationResource.html?id='+id1;
         }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.domestic') {
           location.href = 'editMiscellaneous.html?id='+id1;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.trackingStatusDetails')  {
           <c:if test="${serviceOrder.job =='RLO'}">
           location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
       <c:if test="${serviceOrder.job !='RLO'}">
       location.href =  'editTrackingStatus.html?id='+id1; 
			</c:if>   
          }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.customerWorkTicketsList') {
           location.href = 'customerWorkTickets.html?id='+id1;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.claimsList') {
           location.href = 'claims.html?id='+id1;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.CustomerFileDetails')  {
          var cidVal='${customerFile.id}';
    	   location.href ='editCustomerFile.html?id='+cidVal;
        }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.criticaldate')  {
       		location.href ='soAdditionalDateDetails.html?sid='+id1;
         }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveNextAccountLine')  {
           location.href ='editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
         }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.movePrevAccountLine')  {
           location.href ='editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}';
         }
       if(document.forms['accountLineForms'].elements['gotoPageString'].value =='gototab.moveSelectAccount')  {
           location.href ='editAccountLine.html?sid=${serviceOrder.id}&id='+document.getElementById('setSelectedAccountId').value;
         }  }  } } }  }
function changeStatus() {
   document.forms['accountLineForms'].elements['formStatus'].value = '1';
   document.forms['accountLineForms'].setAttribute('bypassDirtyCheck',false);
}
function setCurrencyCompanyDivision()  {
	      <c:if test="${!contractType}">
          <c:if test="${empty accountLine.id}"> 
		  var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;
		  var url="setCurrencyCompanyDivision.html?ajax=1&decorator=simple&popup=true&companyDivision="+companyDivision;
		  http10.open("GET", url, true); 
          http10.onreadystatechange = handleHttpResponseCompDivCurrency
          http10.send(null); 
          </c:if> </c:if> 
	}
  function handleHttpResponseCompDivCurrency(){
             if (http10.readyState == 4)  {
             			  var results = http10.responseText
			               results = results.trim();
			               if(results!='') { 
			                 	document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value=results; 
								document.forms['accountLineForms'].elements['accountLine.estCurrency'].value=results;
								document.forms['accountLineForms'].elements['accountLine.country'].value=results; 
								findExchangeRateAll(results);  
								<c:if test="${multiCurrency=='Y'}">  
									       var relo="";
										<c:forEach var="entry" items="${country}" > 
	            		                 if(relo==''){
				                              if(results=='${entry.key}'){
					                              relo="yes";
				                               }  }
			                            </c:forEach> 
										if(relo=="yes") {
						                  document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value=results;
						                  findExchangeRecRate(); 
										} else {
										  document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value="";
										  document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=1; 
										}
										 document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value=results;
										 document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value=results;
										 findBillingExchangeRateAll(results) 
								  </c:if>  			
			               }  }  } 
	 function findExchangeRateAll(country){ 
	       document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=findExchangeRateGlobal(country)
	       document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value=findExchangeRateGlobal(country)
	       document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value =findExchangeRateGlobal(country)
                
	       calculateRevisionExpense('none');
	       calculateEstimateExpense('none');
	       convertedAmount('none'); 
    	} 
   function findBillingExchangeRateAll(country){ 
             document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value=findExchangeRateGlobal(country)
             document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value=findExchangeRateGlobal(country)
               
              calculateEstimateRevenue('none','','','');
              calculateRevisionRevenueContract('none','','','')
         }   
function sameQuantityChange() {
       var quantity = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
 	   var quantity1 = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
 	   var quantityabs=Math.abs(quantity);
 	   var quantity1abs=Math.abs(quantity1); 
 	  <c:if test="${!contractType}">
 	   if(quantityabs==quantity1abs && document.forms['accountLineForms'].elements['accountLine.accrueRevenue'].value==''&& document.forms['accountLineForms'].elements['accountLine.accruePayable'].value==''){
   	   document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	   document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
   	   document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
   	   document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
   	   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
   	   document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
   	   document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value; 
   	    } </c:if>  }
function checkPaymentStatusPIR() { 
	<c:if test="${!contractType}">
	var paymentStatusNewVal = document.forms['accountLineForms'].elements['accountLine.paymentStatus'].value;
	var agree=true;
	if((paymentStatusNewVal=='PIR')&&(document.forms['accountLineForms'].elements['formStatus'].value == '1')){
		  agree =confirm("Updating Pre-Invoice Report accountline. Click OK to continue or Cancel to cancel.");
	}
    if(agree) { 
    	if(document.forms['accountLineForms'].elements['billing.billComplete'].value!=''){
       	agree = confirm("The billing has been completed, do you want to still save accountline?"); 	 
         }
    }
   if(agree) {
    return submit_form();
   }else { 
   return false;
    }  
	</c:if>
	<c:if test="${contractType}">
	var paymentStatusNewVal = document.forms['accountLineForms'].elements['accountLine.paymentStatus'].value;
	var agree=true;
	if((paymentStatusNewVal=='PIR')&&(document.forms['accountLineForms'].elements['formStatus'].value == '1')){
	 agree =confirm("Updating Pre-Invoice Report accountline. Click OK to continue or Cancel to cancel.");
	}
	if(agree) { 
	if(document.forms['accountLineForms'].elements['billing.billComplete'].value!=''){
   	agree = confirm("The billing has been completed, do you want to still save accountline?");
	}
	}
    if(agree) { 
    if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
        	    || document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
        	    || document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value
        	    || document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value
        	    || document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value
        	    || document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value){
    	if('${accountLine.invoiceNumber}' ==''){	
        	 if(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estCurrency'].value 
        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value)  ) ) {
        		agree = confirm("Same contract and billing currency but different FX rate in Estimate Expense section. Are you sure you want to save the accountline?");
        	 }}
        	 if(agree){
        		 if('${accountLine.recInvoiceNumber}' ==''){ 
        	 if(document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value 
        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value) ) ) {		 
        	 agree = confirm("Same contract and billing currency but different FX rate in Estimate Revenue section. Are you sure you want to save the accountline?");
        	  }}}
        	 if(agree){
        		 if('${accountLine.invoiceNumber}' ==''){	
        	 if(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value 
        			 && (eval(document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value) ) ) {
        		agree = confirm("Same contract and billing currency but different FX rate in Revision Expense section. Are you sure you want to save the accountline?");
        	 }}}
        	 if(agree){
        		 if('${accountLine.recInvoiceNumber}' ==''){ 
        	 if(document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value 
        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value) ) ) {
        		agree = confirm("Same contract and billing currency but different FX rate in Revision Revenue section. Are you sure you want to save the accountline?");
        	 }}}
        	 if(agree){
        		 if('${accountLine.recInvoiceNumber}' ==''){ 
        	 if(document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value 
        			 && ( eval(document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value)  ) ) {
        		agree = confirm("Same contract and billing currency but different FX rate in Receivable Details section. Are you sure you want to save the accountline?");
        	 }}}
        	 if(agree){
        		 if('${accountLine.invoiceNumber}' ==''){	
        	 if(document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value == document.forms['accountLineForms'].elements['accountLine.country'].value 
        			 && (eval(document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value) != eval(document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value)  ) ) {
        		agree = confirm("Same contract and billing currency but different FX rate in Payable Details section. Are you sure you want to save the accountline?");
        	 }}}
        	
        			
         }
    }
    if(agree) {
    	return submit_form();
	}else{
		 return false;
	} 
	  
	</c:if>
}
function autoPopulateStatusDate(targetElement) { 
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec"; 
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['accountLine.statusDate'].value=datam; 
		<configByCorp:fieldVisibility componentId="component.accountLine.receivedAmount">
		if(document.forms['accountLineForms'].elements['accountLine.paymentStatus'].value=='Fully Paid'){
		document.forms['accountLineForms'].elements['accountLine.receivedAmount'].readOnly=true;
		document.forms['accountLineForms'].elements['accountLine.receivedAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value;
		} else { 
		}
		</configByCorp:fieldVisibility>
	} 
  function autoPayPayableDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec"; 
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['accountLine.payPayableDate'].value=datam; 
	}
 function disableBYRecAcc() { 
<c:if test="${accountInterface=='Y'}">
 if (document.forms['accountLineForms'].elements['accountLine.recAccDate'].value!='') { 
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.itemNew'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=true; 
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.basisNew'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.basisNewType'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recXfer'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recXferUser'].readOnly=true; 
    <c:if test="${multiCurrency=='Y'}">
    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=true;
    </c:if>
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.contractCurrency'].disabled=true;
    document.forms['accountLineForms'].elements['accountLine.contractRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].readOnly=true;
    </c:if>
  }
  </c:if>
}
function disableBYPayAcc() {
<c:if test="${accountInterface=='Y'}">
 if (document.forms['accountLineForms'].elements['accountLine.payAccDate'].value!='') { 
    document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.exchangeRate'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.localAmount'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.actualExpense'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.note'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.payXfer'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.payXferUser'].readOnly=true; 
    <c:if test="${systemDefaultVatCalculation=='true'}">
     document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=true; 
     document.forms['accountLineForms'].elements['accountLine.payVatAmt'].readOnly=true;  
     document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-textUpper';
    document.forms['accountLineForms'].elements['accountLine.payVatAmt'].className='input-textUpper';
    </c:if>
    <c:if test="${contractType}"> 
    document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].readOnly=true;
    </c:if>
  }
</c:if>
}
function disableBYUnlinkedCMM() {
	<c:if test="${accountInterface=='Y'}">
	<c:if test="${((!(trackingStatus.soNetworkGroup))  && billingCMMContractType && networkAgent )}"> 
	<c:if test="${contractType}"> 
	document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].disabled = true;
	 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].onkeydown =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].onkeydown =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].onclick  =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.exchangeRate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.exchangeRate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.country'].disabled = true;
	 document.forms['accountLineForms'].elements['accountLine.valueDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.valueDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.valueDate'].onkeydown=function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.valueDate'].onclick  =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.localAmount'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.localAmount'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.actualExpense'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.actualExpense'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.receivedDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.receivedDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.receivedDate'].onkeydown =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.receivedDate'].onclick  =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.note'].disabled = true; 
	 document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].disabled = true;
	 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].onkeydown=function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].onclick =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.payPayableVia'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payPayableVia'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payPayableAmount'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payPayableAmount'].className='input-textUpper'; 
	 document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled = true;
	 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
	 document.forms['accountLineForms'].elements['accountLine.payVatAmt'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payVatAmt'].className='input-textUpper';
	 <c:if test="${not empty accountLine.id}">
	 if((document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='' || document.forms['accountLineForms'].elements['accountLine.payingStatus'].value!='A') && (document.forms['accountLineForms'].elements['accountLine.VATExclude'].value!='true') && document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!=''){
		 document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-text';
		 document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=function(){return false;};
		 }
	 </c:if>
	 document.forms['accountLineForms'].elements['accountLine.payGl'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payGl'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payPostDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payPostDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payPostDate'].onclick =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.payPostDate'].onkeydown=function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.accruePayable'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.accruePayable'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.accruePayableManual'].disabled = true;
	 document.forms['accountLineForms'].elements['accountLine.accruePayableReverse'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.accruePayableReverse'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payAccDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payAccDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.payAccDate'].onclick =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.payAccDate'].onkeydown=function(){return false;};
	 try{
	 document.forms['accountLineForms'].elements['accountLine.payXfer'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payXfer'].className='input-textUpper';
	 }catch(e){}
	 document.forms['accountLineForms'].elements['accountLine.payXferUser'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.payXferUser'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.settledDate'].readOnly=true; 
	 document.forms['accountLineForms'].elements['accountLine.settledDate'].className='input-textUpper';
	 document.forms['accountLineForms'].elements['accountLine.settledDate'].onclick =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.settledDate'].onkeydown =function(){return false;};
	 document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
	 try{
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].onclick =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].onkeydown =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estCurrency'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.estValueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estValueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estValueDate'].onclick =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.estValueDate'].onkeydown =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estLocalRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estLocalRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estimateRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estimateRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.estimateExpense'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.estimateExpense'].className='input-textUpper'; 
	 }catch(e){} 
	 try{
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].onclick =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].onkeydown =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].onclick =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].onkeydown =function(){return false;};
		 document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.revisionExpense'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.revisionExpense'].className='input-textUpper'; 
	 }catch(e){} 
	 var  imgArr =['settledDate_trigger','payAccDate_trigger','accruePayable_trigger','payPostDate_trigger','receivedDate_trigger','payableContractValueDate_trigger','invoiceDate_trigger','valueDate_trigger','payPayableDate_trigger','estimatePayableContractValueDate-trigger','estValueDate-trigger','revisionPayableContractValueDate-trigger','revisionValueDate-trigger'];
	for(var i=0; i<imgArr.length ; i++){
	try{	
	 var el = document.getElementById(imgArr[i]);  
	 el.src = 'images/navarrow.gif'; 
	 if((el.getAttribute("id")).indexOf('trigger')>0){ 
			el.removeAttribute('id');
		}
	}catch(e){}
	} 
	</c:if>     
	</c:if>  
	</c:if>
	}
function disableAmountRevenue(temp) { 
	<c:if test="${(((accountLine.createdBy != 'Networking' && ( !(((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 ) &&  not empty accountLine.networkSynchedId  ) && !(trackingStatus.accNetworkGroup)) )) && (accountLine.chargeCode != 'MGMTFEE')) || !(trackingStatus.soNetworkGroup) )}">
	<c:if test="${fn1:indexOf(accountLine.createdBy,'DMM')<0}">
	
    var rev = document.forms['accountLineForms'].elements['accountLine.category'].value; 
     if(temp=='load'){ 
    	     document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='${accountLine.payingStatus}';  
      }else{
        if(rev != "Internal Cost"){
    	      document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='' 
        }}
     if(rev == "Internal Cost"){
    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].readOnly=true;  
    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=0;
    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=0;
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=0;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recRate'].value=0; 
    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=0;
    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=0; 
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=0;
    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=0; 
    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=0; 
    <c:if test="${multiCurrency=='Y'}">
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=0; 
    </c:if> 
    <c:if test="${systemDefaultVatCalculation=='true'}">
    document.forms['accountLineForms'].elements['accountLine.recVatPercent'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0;
    document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recVatPercent'].className='input-textUpper';
    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].className='input-textUpper';
    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
    </c:if> 
    document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='I'
    }
    <c:if test="${accountInterface=='Y'}">
    else if ((document.forms['accountLineForms'].elements['accountLine.recAccDate'].value!='' || ( document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!='' && ${accNonEditFlag} )) || ${utsiRecAccDateFlag} || ${utsiPayAccDateFlag})
    { 
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.itemNew'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=true; 
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=false; 
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.basisNew'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.basisNewType'].readOnly=true;  
    document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recXfer'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recXferUser'].readOnly=true; 
    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
    document.forms['accountLineForms'].elements['accountLine.distributionAmount'].readOnly=true;
    </c:if>
    <c:if test="${multiCurrency=='Y'}">
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=true; 
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false;
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=true;
    </c:if>
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.contractCurrency'].disabled=true;
    document.forms['accountLineForms'].elements['accountLine.contractRate'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].readOnly=true;
    </c:if> 
    <c:if test="${systemDefaultVatCalculation=='true'}">
    document.forms['accountLineForms'].elements['accountLine.recVatPercent'].readOnly=true;
    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].readOnly=true; 
    document.forms['accountLineForms'].elements['accountLine.recVatPercent'].className='input-textUpper';
    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].className='input-textUpper'; 
    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
    <c:if test="${adminInvoiceAmountEdit}">
    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false;
    </c:if>
    </c:if> 
    } 
    </c:if>
    else{ 
    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=false; 
    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].readOnly=false;
    <c:if test="${multiCurrency=='Y'}">
     document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=false; 
     document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false;
     document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=false;
    </c:if>
    <c:if test="${contractType}">
    document.forms['accountLineForms'].elements['accountLine.contractCurrency'].disabled=false;
    document.forms['accountLineForms'].elements['accountLine.contractRate'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].readOnly=false;
    </c:if>
    <c:if test="${systemDefaultVatCalculation=='true'}">
    document.forms['accountLineForms'].elements['accountLine.recVatPercent'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].readOnly=false;
    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false; 
    </c:if>
    document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='${accountLine.payingStatus}';  
    } 
    <c:if test="${accountInterface=='Y' && multiCurrency=='Y' && !utsiPayAccDateFlag && trackingStatus.soNetworkGroup}">  
    if ((document.forms['accountLineForms'].elements['accountLine.recAccDate'].value=='') ){ 
    	document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false; 
    }
    </c:if>
    accrualQuantitycheck('1'); 
    setVatExcluded('1');
   </c:if> </c:if>
}
function checkDriverId(position){
	var category =document.forms['accountLineForms'].elements['accountLine.category'].value;
	var ship = document.forms['accountLineForms'].elements['accountLine.shipNumber'].value;
	if((category!=null)&&(category=='Driver')){
         var url1="checkPurchasePostingAjax.html?ajax=1&decorator=simple&popup=true&shipNumber="+ship;
         var url="checkDriverForMoreThanOneAjax.html?ajax=1&decorator=simple&popup=true&shipNumber="+ship;
		      http1987.open("GET", url, true);
		      http1987.onreadystatechange =function(){ handleHttpResponsecheckPurchasePostingAjax(url1,position);}; 
		      http1987.send(null); 
	} }
function handleHttpResponsecheckPurchasePostingAjax(url,position){
    if (http1987.readyState == 4)   {
         var results = http1987.responseText;
         results=results.trim();
         var resArr=results.split("~");
         if(parseInt(resArr[0])>1){
        	  ajax_showTooltip(url,position);
         }else if(parseInt(resArr[0])==1){
        		<c:if test="${accountInterface!='Y'}">
        		document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=resArr[1];
        		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=resArr[2];
        		checkVendorName();fillCurrencyByChargeCode();changeStatus();
        		</c:if>
        	    <c:if test="${accountInterface=='Y'}">
        	    <c:if test="${empty accountLine.payAccDate}">
        	    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=resArr[1];
        	    document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=resArr[2];
        		vendorCodeForActgCodeNew();
        	    </c:if>
        	    <c:if test="${not empty accountLine.payAccDate}">            
        	    alert('You can not change the Partner Code as Sent To Acc has been already filled');
        	    </c:if>
        	    </c:if>	
         }else{ 
         }  }  } 
function goToUrlAccDetails(partnerCode,partnerName){
	<c:if test="${accountInterface!='Y'}">
	document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=partnerCode;
	document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=partnerName;
	checkVendorName();fillCurrencyByChargeCode();changeStatus();
	</c:if>
    <c:if test="${accountInterface=='Y'}">
    <c:if test="${empty accountLine.payAccDate}">
    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=partnerCode;
    document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=partnerName;
	vendorCodeForActgCodeNew();
    </c:if>
    <c:if test="${not empty accountLine.payAccDate}">            
    alert('You can not change the Partner Code as Sent To Acc has been already filled');
    </c:if>
    </c:if>	 
} 
function disablePayableSec(){
	<c:if test="${accountLine.t20Process}">
		 document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.exchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.exchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.country'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.valueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.valueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.valueDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.localAmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.localAmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.actualExpense'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.actualExpense'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.receivedDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.receivedDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.receivedDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.note'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.payPayableVia'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payPayableVia'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payPayableAmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payPayableAmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payVatAmt'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payVatAmt'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payGl'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payGl'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payPostDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payPostDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.accruePayable'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.accruePayable'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.accruePayableManual'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.accruePayableReverse'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.accruePayableReverse'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payAccDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payAccDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payXfer'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payXfer'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payXferUser'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payXferUser'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.settledDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.settledDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
		 var  imgArr =['receivedDate_trigger','payableContractValueDate_trigger','invoiceDate_trigger','valueDate_trigger','payPayableDate_trigger'];
		for(var i=0; i<imgArr.length ; i++){
		 var el = document.getElementById(imgArr[i]);  
		 el.src = 'images/navarrow.gif'; 
			if((el.getAttribute("id")).indexOf('trigger')>0){ 
				el.removeAttribute('id');
			} } 
	</c:if>
} 
function calcDays() {
 document.forms['accountLineForms'].elements['accountLine.storageDays'].value="";
 var date2 = document.forms['accountLineForms'].elements['accountLine.storageDateRangeFrom'].value;	 
 var date1 = document.forms['accountLineForms'].elements['accountLine.storageDateRangeTo'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan') {   
     month = "01";
   } else if(month == 'Feb') {
       month = "02";
   } else if(month == 'Mar')  {
       month = "03"
   } else if(month == 'Apr') {
       month = "04"
   }  else if(month == 'May')  {
       month = "05"
   } else if(month == 'Jun') {
       month = "06"
   }  else if(month == 'Jul')  {
       month = "07"
   } else if(month == 'Aug')  {
       month = "08"
   }  else if(month == 'Sep')  {
       month = "09"
   }  else if(month == 'Oct')  {
       month = "10"
   }  else if(month == 'Nov')  {
       month = "11"
   }  else if(month == 'Dec') {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')  {
       month2 = "01";
   }  else if(month2 == 'Feb')  {
       month2 = "02";
   }  else if(month2 == 'Mar') {
       month2 = "03"
   }  else if(month2 == 'Apr')  {
       month2 = "04"
   }  else if(month2 == 'May')  {
       month2 = "05"
   }  else if(month2 == 'Jun') {
       month2 = "06"
   }   else if(month2 == 'Jul') {
       month2 = "07"
   }  else if(month2 == 'Aug') {
       month2 = "08"
   } else if(month2 == 'Sep')   {
       month2 = "09"
   }  else if(month2 == 'Oct')  {
       month2 = "10"
   }  else if(month2 == 'Nov')  {
       month2 = "11"
   }  else if(month2 == 'Dec')  {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['accountLineForms'].elements['accountLine.storageDays'].value = daysApart; 
  if(daysApart<0) {
    alert("Storage Date Range must be less than to date");
    document.forms['accountLineForms'].elements['accountLine.storageDays'].value=''; 
    document.forms['accountLineForms'].elements['accountLine.storageDateRangeTo'].value='';
  }
  if(document.forms['accountLineForms'].elements['accountLine.storageDays'].value=='NaN')  {
     document.forms['accountLineForms'].elements['accountLine.storageDays'].value = '';
   } 
 document.forms['accountLineForms'].elements['checkForDaysClick'].value = '';
} 
 function calReference() { 
 if(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value==0 || document.forms['accountLineForms'].elements['accountLine.billToCode'].value=='') { 
    alert("You can not fill Reference# ,Bill to and Revenue must have some values");  
    document.forms['accountLineForms'].elements['accountLine.externalReference'].value='';
 } else{
       <c:if test="${trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && accountLine.paymentStatus=='PIR' }">
		 var agree =confirm("This line already linked with Harmony as CMM contract. Would you like to chnage Reference# ? ");
         if(agree) {
         }
		 else{
		 document.forms['accountLineForms'].elements['accountLine.externalReference'].value='${accountLine.externalReference}';
		 }
		</c:if> 
 }  } 
  function chkSelect(a,target) { 
     		if(a == '2') {
     			if(target == 'save') {
     				document.forms['accountLineForms'].elements['accountLine.actualExpense'].select(); 	
     			} else {
     				document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].select();
     			} }
     		<c:if test="${!contractType}">
        	if (checkFloatWithArth('accountLineForms','accountLine.estimateQuantity','Invalid data in Quantity Estimate Detail') == false)  {
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value='0'; 
              return false
            }
        	</c:if>
        	<c:if test="${contractType}">
        	if (checkFloatWithArth('accountLineForms','accountLine.estimateQuantity','Invalid data in Buy Quantity Estimate Detail') == false)  {
                document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value='0';
                document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value='0';
                document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='0';
                return false 
              }
        	if (checkFloatWithArth('accountLineForms','accountLine.estimateSellQuantity','Invalid data in Sell Quantity Estimate Detail') == false)  {
                document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value='0';
                document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value='0';
                document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='0';
                return false 
              }
        	</c:if>
           if (checkFloatWithArth('accountLineForms','accountLine.estimateRate','Invalid data in Buy Rate Estimate Detail') == false)  {
              document.forms['accountLineForms'].elements['accountLine.estimateRate'].value='0'; 
              document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='0';
              if((document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=='NaN')||(document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=='NaN')){
              document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value='0';
              }
              return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.estimateSellRate','Invalid data in Sell Rate Estimate Detail') == false)  {  
              document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value='0';
              return false
           }
           <c:if test="${!contractType}">
           if (checkFloatWithArth('accountLineForms','accountLine.revisionQuantity','Invalid data in Quantity Estimate Detail') == false)  {
               document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value='0';
               document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value='0';
               document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='0';
               document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value='0';
               return false 
            } 
          </c:if>
          <c:if test="${contractType}">
          if (checkFloatWithArth('accountLineForms','accountLine.revisionQuantity','Invalid data in Buy Quantity Estimate Detail') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value='0';
              document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value='0';
              document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='0'; 
              return false 
           }
          if (checkFloatWithArth('accountLineForms','accountLine.revisionSellQuantity','Invalid data in Sell Quantity Estimate Detail') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value='0'; 
              document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='0';
              document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value='0';
              return false 
           }
          </c:if>
           if (checkFloatWithArth('accountLineForms','accountLine.estimateRevenueAmount','Invalid data in Revenue Estimate Detail') == false) {
              document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value='0';
              return false
           } 
           if (checkFloatWithArth('accountLineForms','accountLine.revisionRate','Invalid data in Buy Rate Revision Detail') == false) {
              document.forms['accountLineForms'].elements['accountLine.revisionRate'].value='0';
              document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value='0';
              document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='0';
              if((document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=='NaN')||(document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=='NaN')){
              document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value='0';  
              document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value='0'; 
              }
              return false
           }
            if (checkFloatWithArth('accountLineForms','accountLine.revisionSellRate','Invalid data in Sell Rate Revision Detail') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value='0';
              document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value='0';
              return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.revisionRevenueAmount','Invalid data in Revenue Revision Detail') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value='0';
              return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.recQuantity','Invalid data in Quantity Receivable Detail ') == false)    {
              document.forms['accountLineForms'].elements['accountLine.recQuantity'].value='0';
              return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.recRate','Invalid data in Sell Rate Receivable Detail ') == false)  {
              document.forms['accountLineForms'].elements['accountLine.recRate'].value='0'; 
              <c:if test="${multiCurrency=='Y'}">
                document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value='0'; 
               </c:if>
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.estExchangeRate','Invalid data in Ex. Rate Estimate Detail.') == false)  {
              document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value='0';  
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.estLocalRate','Invalid data in Buy Rate Estimate Detail .') == false)  {
              document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value='0';
              document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value='0';    
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.estLocalAmount','Invalid data in Curr Amount Estimate Detail.') == false)  {
              document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value='0';  
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.revisionExchangeRate','Invalid data in Ex. Rate Revision Detail.') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value='0';  
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.revisionLocalRate','Invalid data in Buy Rate Revision Detail.') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value='0';  
              document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value='0';  
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.revisionLocalAmount','Invalid data in Curr Amount Revision Detail.') == false)  {
              document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value='0';  
               return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.entitlementAmount','Invalid data in Expense Entitlement Detail ') == false)  {
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value='0';
              return false
           }
           if (checkNumber('accountLineForms','accountLine.accountLineNumber','Invalid data in Line # ') == false)   {
               document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value='0'; 
               return false 
           }
           if (document.forms['accountLineForms'].elements['accountLine.billToCode'].value=='' && document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value!=0) { 
               alert("Bill To Code is missing, please select. ");
              return false 
           } 
           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
           if (checkFloatWithArth('accountLineForms','accountLine.distributionAmount','Invalid data in distribution Amt') == false)  {
              document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value='0.00';
              return false
           } 
           </c:if>
            <c:if test="${multiCurrency=='Y'}">
            if (checkFloatWithArth('accountLineForms','accountLine.recCurrencyRate','Invalid data in Frgn. Curr. Rate Receivable Detail ') == false)  {
              document.forms['accountLineForms'].elements['accountLine.recRate'].value='0';
              document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value='0';  
               return false
             } 
            </c:if>
 } 
function forDays(){
 document.forms['accountLineForms'].elements['checkForDaysClick'].value ='1';
}  
function findAuthorizationNo(targ) { 
   if (document.forms['accountLineForms'].elements['accountLine.recAccDate'].value!='')  {
    alert("You can not change the authorization No as Sent To Acc has been already filled");
    }  else { 
	 document.getElementById('rateImage').src = "<c:url value='/images/image.gif'/>"; 
     if(targ == 'a')  {
    	 if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
    		 if('${billing.billToAuthority}'==''){
    			 document.forms['accountLineForms'].elements['accountLine.authorization'].value="";
    			 alert("please select authorization No from billing")
    		 }else{
    		 document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
    		 }
    	 }
    	 if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
    		 if('${billing.billTo2Authority}'==''){
    			 document.forms['accountLineForms'].elements['accountLine.authorization'].value="";
    			 alert("please select authorization No from billing (Secondary Billing section) ")
    		 }else{
    		 document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
    		 }
    	}
    	 document.getElementById('rateImage').src = "<c:url value='/images/image.jpg'/>";
      } else  {
    	  if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
    			document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
    			}
    	 if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
    				document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
    				}
     }
      
    } } 

function fillBranchCode() {
if(document.forms['accountLineForms'].elements['serviceOrder.Job'].value=='UVL'||document.forms['accountLineForms'].elements['serviceOrder.Job'].value=='DOM'||document.forms['accountLineForms'].elements['serviceOrder.Job'].value=='MVL')
{ 
} }
 function fillDescription() { 
    var description =document.forms['accountLineForms'].elements['accountLine.description'].value 
    var revenue =document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value 
    var category =document.forms['accountLineForms'].elements['accountLine.category'].value 
    var quantity =document.forms['accountLineForms'].elements['accountLine.recQuantity'].value 
    var itemNew =document.forms['accountLineForms'].elements['accountLine.itemNew'].value
    var recRate =document.forms['accountLineForms'].elements['accountLine.recRate'].value
    var basisNewType =document.forms['accountLineForms'].elements['accountLine.basisNewType'].value
    var checkDescription =document.forms['accountLineForms'].elements['checkComputeDescription'].value;
    if(document.forms['accountLineForms'].elements['accountLine.description'].value==''&& revenue!=0.00 && revenue!=0 && checkDescription!='computeButton')   {
     document.forms['accountLineForms'].elements['accountLine.description'].value=quantity+" "+itemNew+" "+"@"+" "+recRate+" "+basisNewType;
   }
    if(document.forms['accountLineForms'].elements['accountLine.description'].value==''&& revenue!=0.00 && revenue!=0 && checkDescription=='computeButton')   {
    	checkDescriptionfromCharges();
      }
       
 } 
   function checkMultiAuthorization() {  
    var billToCode = document.forms['accountLineForms'].elements['accountLine.billToCode'].value;  
    var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billToCode;  
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse41; 
     http55.send(null); 
} 
function handleHttpResponse41() { 
             if (http55.readyState == 4)  {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                var recAccDate; 
                <c:if test="${accountInterface=='Y'}"> 
                recAccDate=document.forms['accountLineForms'].elements['accountLine.recAccDate'].value;
                recAccDate=recAccDate.trim();
                </c:if> 
                if(results=='true')   {
                document.getElementById("hhid").style.display="none";
                document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true; 
                }
                else if (recAccDate!='')   { 
                	fillAuthorizationByBillToCode();
                	document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true;
                } else{  
                	fillAuthorizationByBillToCode();
                	document.getElementById("hhid").style.display="block"; 
                document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true; 
                } } } 
    function winOpenForActgCode() {
    var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
 	  openWindow('findVendorCode.html?sid=${serviceOrder.id}&partnerType=DF&accType=ACCNew&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
 } 
 function findTruckNumber() { 
      var vendorCode=document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
      vendorCode=vendorCode.trim();
      if(vendorCode==''){
 	  openWindow('findTruckNumber.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=accountLine.vendorCode&fld_description=thirdDescription&fld_code=accountLine.truckNumber',1024,500);
 	  } else{
 	  openWindow('findTruckNumber.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=accountLine.truckNumber',1024,500);
 	  }
 	  document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
 } 
   function vendorCodeForActgCode(){ 
   <c:if test="${accountInterface=='Y'}"> 
    var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value; 
    var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;  
    vendorId=vendorId.trim();
    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=vendorId;
    if(vendorId==''){
    	document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
		document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
    }
    if(vendorId!=''){
	     var url=""
	    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
	    var actgCodeFlag='';
	    try{
	    	actgCodeFlag=document.forms['accountLineForms'].elements['actgCodeFlag'].value;
	    }catch(e){} 
	     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);	     
	    </c:if>
	    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
	     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
	    </c:if>
        httpvandor.open("GET", url, true);
	    httpvandor.onreadystatechange = handleHttpResponse222;
	    httpvandor.send(null);
    }
    </c:if>
     <c:if test="${accountInterface!='Y'}"> 
      var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
	    if(vendorId == ''){
	    var actualExpense= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value 
	    if(actualExpense>0){
	    alert("You can not remove Partner Code as there is value in the payable details section");
		<c:if test="${not empty accountLine.id}">
		document.forms['accountLineForms'].elements['accountLine.vendorCode'].value='${accountLine.vendorCode}';
		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="${accountLine.estimateVendorName}";
		</c:if>
		<c:if test="${empty accountLine.id}">
		document.forms['accountLineForms'].reset();
		</c:if> 
	    }else {
		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
		} }
	if(vendorId != ''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    }
     </c:if>
} 
function handleHttpResponse222(){
		if (httpvandor.readyState == 4){
                var results = httpvandor.responseText
                results = results.trim();  
                var res = results.split("#");  
                if(res.length >= 2){ 
		                if(res[3]=='Approved'){
		                	if(res[2] != "null" && res[2] !=''){
		                		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value = res[1];
				                document.forms['accountLineForms'].elements['accountLine.actgCode'].value = res[2];
				                AddDocumentDetails();
				                <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
				                try{
				                	if(res[8] != "null" && res[8].trim() !=''){
				                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].value=res[8].trim();
				                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=true;
				                		document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=true;
				                		document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value='Y'; 
				                		
				                	}else{
					                	document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
					                	document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=false; 
					                	document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value=''; 
				                	}
				                	}catch(e){} 
				                 </c:if>
				                if(res[4] != "null" && res[4] !=''){
				                  if(res[4]!='Commission' && res[4]!='NoCommission' ){
				                    <c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 			  
				                    document.forms['accountLineForms'].elements['partnerCommisionCode'].value=res[4];
				                    if('${accountLine.commission}'==''){
                                      document.forms['accountLineForms'].elements['accountLine.commission'].value=res[4];  } 
                                      else{document.forms['accountLineForms'].elements['accountLine.commission'].value='${accountLine.commission}';}  
                                    </c:if>
				            }  }
				            }  else { 
				                document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=res[1]; 
								document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
								document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
			                	document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=false; 
								document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
				            }
		                	fillDefaultInvoiceNumber();
				        }else{
				            alert("Vendor code is not approved." ); 
							document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
							document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
							document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
							document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
				        }     
		            }else{
		                alert("Vendor code not valid." ); 
						document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
						document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
						document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
						document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
					}  } } 
function vendorCodeForActgCodeNew(){ 
	    var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value; 
	    var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;  
	    vendorId=vendorId.trim();
	    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value=vendorId;
	    if(vendorId==''){
	    	document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
			document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
	    }
	    if(vendorId!=''){
		     var url=""
		    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
			    try{
			    	actgCodeFlag=document.forms['accountLineForms'].elements['actgCodeFlag'].value;
			    }catch(e){} 		    
		     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);
		    </c:if>
		    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
		     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
		    </c:if>
	        httpvandor2221984.open("GET", url, true);
		    httpvandor2221984.onreadystatechange = handleHttpResponse2221984;
		    httpvandor2221984.send(null);
	    } }
function handleHttpResponse2221984(){
	if (httpvandor2221984.readyState == 4){
            var results = httpvandor2221984.responseText
            results = results.trim();  
            var res = results.split("#");  
            if(res.length >= 2){ 
	                if(res[3]=='Approved'){
	                	if(res[2] != "null" && res[2] !=''){
	                		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value = res[1];
			                document.forms['accountLineForms'].elements['accountLine.actgCode'].value = res[2];
			                  <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
			                  try{
		                	if(res[8] != "null" && res[8].trim() !=''){
		                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].value=res[8].trim();
		                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=true;
		                		document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=true;
		                		document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value='Y'; 
		                	}else{
		                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
		                		document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=false;
		                		document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value='';
		                	} 
		                	}catch(e){} 	                 
		                	</c:if>
			                if(res[4] != "null" && res[4] !=''){
			                  if(res[4]!='Commission' && res[4]!='NoCommission' ){
			                    <c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 			  
			                    document.forms['accountLineForms'].elements['partnerCommisionCode'].value=res[4];
			                    if('${accountLine.commission}'==''){
                                  document.forms['accountLineForms'].elements['accountLine.commission'].value=res[4];  } 
                                  else{document.forms['accountLineForms'].elements['accountLine.commission'].value='${accountLine.commission}';}  
                                </c:if>
			            }  }
			            }  else { 
			                document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=res[1]; 
							document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
							document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
		                	document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=false; 
							document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
			            }
	                	fillCurrencyByChargeCode();disableButton();changeStatus();		            	
			        }else{
						document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
						document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
						document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
						document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
			        }     
	            }else{
					document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
					document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
					document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
					document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
				}  } }
function fillRecVatByBillToCode() {
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var oldVat = document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value;
	if(document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value=='') {
	if(document.forms['accountLineForms'].elements['accountLine.VATExclude'].value!='true'){
	
	if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){ 
		if(oldVat !='${billing.primaryVatCode}'){
			var agree =confirm("Vat chosen in Revenue VAT Desc differs from the Default VAT of the Bill To, Do you want to apply the Default VAT of the Bill To?");
		     if(agree) {
		    	 document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${billing.primaryVatCode}';
		     }
		} 	
	}
	if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
			if(oldVat !='${billing.secondaryVatCode}'){
				var agree =confirm("Vat chosen in Revenue VAT Desc differs from the Default VAT of the Bill To, Do you want to apply the Default VAT of the Bill To?"); 
				if(agree) {
			    	 document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${billing.secondaryVatCode}'; 
			     }
			} 
		}
	if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.privatePartyBillingCode}'){
			if(oldVat !='${billing.privatePartyVatCode}'){
				var agree =confirm("Vat chosen in Revenue VAT Desc differs from the Default VAT of the Bill To, Do you want to apply the Default VAT of the Bill To?");
			     if(agree) {
			    	 document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${billing.privatePartyVatCode}';	 
			     }
			} 
		}
	<c:if test="${empty accountLine.id}">
		var sysreceivable='${systemDefault.receivableVat}';
	    if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value =='' && sysreceivable!=''){
			try{
				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=sysreceivable;
			}catch(e){}
		}
	</c:if>
	    if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value ==''){
	    	document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${accountLine.recVatDescr}'
	    }
	    if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value ==''){
	    	document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value = oldVat;
	    }
	getVatPercent();
	}else{
		document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
		document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
		document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=0; 
		} }
	</c:if>
	fillCurrencyByBillToCode();
	} 
function fillRecVatByChargeCode(){
if(document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value=='') {
if(document.forms['accountLineForms'].elements['accountLine.VATExclude'].value!='true'){
if(document.forms['accountLineForms'].elements['accRecVatDescr'].value !=''){	
 document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${accountLine.recVatDescr}';
 //getVatPercent();	
}else{
	//fillRecVatByBillToCode();
}
}else{
	document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value="";
	document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0; 
	} } }  
function fillPayVatByVendorCode() {
	var oldVat=document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value;
	if(document.forms['accountLineForms'].elements['accountLine.payPostDate'].value=='' && document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='') {
	if(document.forms['accountLineForms'].elements['accountLine.VATExclude'].value!='true'){
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.originAgentCode}'){
	document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.originAgentVatCode}';
	}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.destinationAgentCode}'){
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.destinationAgentVatCode}';
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.originSubAgentCode}'){
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.originSubAgentVatCode}';
		} 
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.destinationSubAgentCode}'){
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.destinationSubAgentVatCode}';
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.forwarderCode}'){
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.forwarderVatCode}';
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.brokerCode}'){
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.brokerVatCode}';
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${trackingStatus.networkPartnerCode}'){
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.networkPartnerVatCode}';
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${serviceOrder.bookingAgentCode}'){
		try{
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.bookingAgentVatCode}';
		}catch(e){}
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${billing.vendorCode}'){
		try{
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.vendorCodeVatCode}';
		}catch(e){}
		}
	if(document.forms['accountLineForms'].elements['accountLine.vendorCode'].value =='${billing.vendorCode1}'){
		try{
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${billing.vendorCodeVatCode1}';
		}catch(e){}
		}
	<c:if test="${empty accountLine.id}">	
        var syspayable='${systemDefault.payableVat}';
        if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value =='' && syspayable!=''){
    		try{
    		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=syspayable;
    		}catch(e){}
    		}
		</c:if>
        if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value ==''){
        	document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${accountLine.payVatDescr}'
        }	
        if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value ==''){
        	document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value = oldVat
            }
        
	getPayPercent();
	}  } } 
function fillPayVatByChargeCode(){
	if(document.forms['accountLineForms'].elements['accountLine.payPostDate'].value=='' && document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=='') {
	if(document.forms['accountLineForms'].elements['accountLine.VATExclude'].value!='true'){
	if(document.forms['accountLineForms'].elements['accPayVatDescr'].value !=''){	
	 document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${accountLine.payVatDescr}';
	 }else{
		fillPayVatByVendorCode();
	}
	}else{
		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value="";
		document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0; 
		document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=0; 
		} } }
function fillAuthorizationByBillToCode() { 
	var authorization=document.forms['accountLineForms'].elements['accountLine.authorization'].value;
	authorization=authorization.trim();
	if(authorization==''){
	if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
		document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
		}
		if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
			document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
			}
	}
}
function fillAuthorizationOnChangeBillToCode() { 
	if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
		document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
		}
		if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
			document.forms['accountLineForms'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
			}
}
function fillCurrencyByBillToCode() {
	fillAuthorizationOnChangeBillToCode(); 
	//fillRecVatByBillToCode(); 
	<c:if test="${contractType}"> 	 
    var billToCode = document.forms['accountLineForms'].elements['accountLine.billToCode'].value;  
    var url="fillCurrencyByBillToCode.html?ajax=1&decorator=simple&popup=true&billToCode="+billToCode;  
     http5512.open("GET", url, true); 
     http5512.onreadystatechange = handleHttpResponse4112; 
     http5512.send(null); 
     </c:if>   
} 
function handleHttpResponse4112() { 
    if (http5512.readyState == 4)  {
		       var results = http5512.responseText
		       results = results.trim();
		       if(results!="")
		       {
	                var res = results.split("~"); 
	                if(res[0].trim()!=''){ 
		                document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value=res[0];
		                document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value=res[0];
		                document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value=res[0];
		                document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=res[1];
		                document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value=res[1];
		                document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value=res[1];
		                document.forms['accountLineForms'].elements['accountLine.racValueDate'].value=currentDateGlobal();
		                document.forms['accountLineForms'].elements['accountLine.revisionSellValueDate'].value=currentDateGlobal();
		                document.forms['accountLineForms'].elements['accountLine.estSellValueDate'].value=currentDateGlobal();
	                <c:if test="${contractType}">
		                var estCtCurr = document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value;
		                if(estCtCurr==res[0]){
		                	document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value=res[1];
		                	document.forms['accountLineForms'].elements['accountLine.estimateContractValueDate'].value=currentDateGlobal();
		                }
		                var revCtCurr = document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value;
		                if(revCtCurr==res[0]){
		                	document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value=res[1];
		                	document.forms['accountLineForms'].elements['accountLine.revisionContractValueDate'].value=currentDateGlobal();
		                }
		                var ctCurr = document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
		                if(ctCurr==res[0]){
		                	document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value=res[1];
		                	document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
		                }
		                calEstSellLocalRate('cal','none');
		                calRevisionSellLocalRate('cal','none');
		                calRecCurrencyRate('none');
		            </c:if>
		            <c:if test="${!contractType}">
		              calculateEstimateRevenue('none','','','');
		              calculateRevisionRevenueContract('none','','','');
		              calculateRecRate('none','','',''); 
		            </c:if> 
	                }  } } } 
function fillCurrencyByChargeCode() {
	fillPayVatByVendorCode();
	<c:if test="${contractType}"> 
    var vendorCode = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;  
    var url="fillCurrencyByChargeCode.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorCode;  
     http5513.open("GET", url, true); 
     http5513.onreadystatechange = handleHttpResponse4113; 
     http5513.send(null); 
     </c:if> 
} 
function handleHttpResponse4113() { 
    if (http5513.readyState == 4)  {
		       var results = http5513.responseText
		       results = results.trim(); 
		       if(results!="")  {
	                var res = results.split("~");
	            if(res[0].trim()!=''){              	
	                document.forms['accountLineForms'].elements['accountLine.estCurrency'].value=res[0];		                	
	                document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value=res[0];
	                document.forms['accountLineForms'].elements['accountLine.country'].value=res[0]; 
	                document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value=res[1];
	                document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value=res[1];
	                document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=res[1];
	                document.forms['accountLineForms'].elements['accountLine.estValueDate'].value=currentDateGlobal();
	                document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].value=currentDateGlobal();
	                document.forms['accountLineForms'].elements['accountLine.valueDate'].value=currentDateGlobal();
	              <c:if test="${contractType}">
		              	var payCtCurr = document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value;
		                if(payCtCurr==res[0]){
		                	document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value=res[1];
		                	document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();
		                }
		                var revPayCtCurr = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value;
		                if(revPayCtCurr==res[0]){
		                	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value=res[1];
		                	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
		                }
		                var estPayCtCurr = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value;
		                if(estPayCtCurr==res[0]){
		                	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value=res[1];
		                	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].value=currentDateGlobal();
		                }
		          	  calEstLocalRate('cal','none'); 
		          	  calRevisionLocalRate('cal','none'); 
		          	  convertLocalAmount('none');
	              </c:if>
	              <c:if test="${!contractType}">
		              calculateEstimateExpense('none');
		              calculateRevisionExpense('none');
		              convertedAmount('none');
	              </c:if>
	            }   }  }    }
 function disableButton(){
	 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0||fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}">  
 	var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
 	if(vendorId == ''){
 		document.forms['accountLineForms'].elements['commissionBtn'].disabled = true;
 	}else{
 		document.forms['accountLineForms'].elements['commissionBtn'].disabled = false;
 	} </c:if>  } 
  function findExchangeRate(){
        var country =document.forms['accountLineForms'].elements['accountLine.country'].value; 
       
            document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=findExchangeRateGlobal(country)
			document.forms['accountLineForms'].elements['accountLine.valueDate'].value=currentDateGlobal(); 
               var contractRate=0;
               <c:if test="${contractType}">
                var payableContractCurrency = document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value;
                if(payableContractCurrency==country){
                	document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value  ;
                	document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.valueDate'].value  ;
                }
               	contractRate = document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value;
               </c:if>
               if(contractRate==0){
               		convertedAmount('none');
               }else{
               		convertLocalAmount('none');
               }
         }   
     function checkAccountLineNumber() { 
       var accountLineNumber=eval(document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value); 
       var accountLineNumber1=document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value;
       if(accountLineNumber>999) {
         alert("Manually you can not Enter Line # greater than 999");
         document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value=0;
       }
       else if(accountLineNumber1.length<2)  { 
       	document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value='00'+document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value;
       }
       else if(accountLineNumber1.length==2)  { 
       	var aa = document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value;
       	document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value='0'+aa;
       }  } 
     function checkActgCode(xyz) { 
        var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;   
     	var vendorCode= document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
     	var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value; 
     	var actgCode= document.forms['accountLineForms'].elements['accountLine.actgCode'].value;
     	var payGl= document.forms['accountLineForms'].elements['accountLine.payGl'].value; 
     	vendorCode =vendorCode.trim();
     	chargeCode =chargeCode.trim();
     	actgCode =actgCode.trim(); 
     	payGl=payGl.trim(); 
     	if(vendorCode=='') 	{
     	   alert("Vendor code is missing.");
     	} else if(chargeCode=='') {
     	   alert("Charge code needs to be selected")  
     	}else if (document.forms['accountLineForms'].elements['grossMarginActualization'].value=="Y") {
     		alert("Estimate or Revision gross margin is less than 10%")
     	}
     	else if(payGl==''){
     		checkChargeCodeOpenDiv('pay'); 
     	} else if(vendorCode!='' && chargeCode!='' && payGl!='' && actgCode=='') { 
		    var url=""
		    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
		    try{
		    	actgCodeFlag=document.forms['accountLineForms'].elements['actgCodeFlag'].value;
		    }catch(e){} 
		     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorCode)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);
		    </c:if>
		    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
		     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorCode)+"&companyDivision="+encodeURI(companyDivision);
		    </c:if>
		    http2.open("GET", url, true);
		    http2.onreadystatechange = function(){ handleHttpResponse2222(xyz);};
		    http2.send(null);  
     	} else if(vendorCode!='' && actgCode!='' && chargeCode!='' && payGl!='')
     	{
     	  fillDefaultInvoiceNumber();
       	  animatedcollapse.toggle('pay');
     	}  } 
     function handleHttpResponse2222(param){ 
     var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value; 
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                var res = results.split("#"); 
                if(res.length >= 2){ 
		                	if(res[2] != "null" && res[2] !=''){ 
				                document.forms['accountLineForms'].elements['accountLine.actgCode'].value = res[2];
				                <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
				                try{
			                	if(res[8] != "null" && res[8].trim() !=''){
			                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].value=res[8].trim();
			                		document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=true;
			                		document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=true;
			                		document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value='Y'; 
			                		
			                	}else{
				                	document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
				                	document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=false; 
				                	document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value='';
			                	}}catch(e){} 				                
			                	</c:if>
				                fillDefaultInvoiceNumber();
				                animatedcollapse.toggle('pay');
				            } else if (res[2] == "null" || res[2] =='') {
				            	document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
			                	document.forms['accountLineForms'].elements['accountLine.driverCompanyDivision'].value=false;   
				              <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
				              alert("This Vendor Code cannot be processed as the Accounting code for the partner is missing for company division "+companyDivision+", please contact your accounting dept.");
				              </c:if>
				              <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}">
				              alert("This Vendor Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept."); 
				              </c:if>
				            } }	  }   }
function disablePayableSectionAfterInvoice(){   
     if ((document.forms['accountLineForms'].elements['accountLine.receivedDate'].value!='' || document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value!='') && (document.forms['accountLineForms'].elements['accountLine.payPostDate'].value!='' && document.forms['accountLineForms'].elements['accountLine.payingStatus'].value=='A') && ${accNonEditFlag}){ 
    	 var  imgArr =['receivedDate_trigger','invoiceDate_trigger','valueDate_trigger'];
    	 <c:if test="${contractType}">
    	 document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].className='input-textUpper';
		 imgArr.push('payableContractValueDate_trigger');
		 </c:if>
		 document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.invoiceDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.exchangeRate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.exchangeRate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.country'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.valueDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.valueDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.valueDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.localAmount'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.localAmount'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.actualExpense'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.actualExpense'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.receivedDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.receivedDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.receivedDate'].onkeydown="";
		 document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.note'].disabled = true;
		 <c:if test="${systemDefaultVatCalculation=='true'}">
		 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payVatAmt'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payVatAmt'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
		 </c:if>
		 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payPayableDate'].onkeydown="";
		 
		 document.forms['accountLineForms'].elements['accountLine.payGl'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payGl'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payPostDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payPostDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.accruePayable'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.accruePayable'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.accruePayableManual'].disabled = true;
		 document.forms['accountLineForms'].elements['accountLine.accruePayableReverse'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.accruePayableReverse'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payAccDate'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payAccDate'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payXfer'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payXfer'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.payXferUser'].readOnly=true; 
		 document.forms['accountLineForms'].elements['accountLine.payXferUser'].className='input-textUpper';
		 document.forms['accountLineForms'].elements['accountLine.compute5'].disabled=true;
		 //document.forms['accountLineForms'].elements['accountLine.myFileFileName'].disabled = true; 
		 //document.forms['accountLineForms'].elements['ViewBtn'].disabled = true;
		 <configByCorp:fieldVisibility componentId="accountLine.settledDate">
		 	document.forms['accountLineForms'].elements['accountLine.settledDate'].readOnly=true; 
		 	document.forms['accountLineForms'].elements['accountLine.settledDate'].className='input-textUpper';
		 	//imgArr.push('settledDate_trigger');
		 </configByCorp:fieldVisibility>
		 
		 for(var i=0; i<imgArr.length ; i++){
			 var el = document.getElementById(imgArr[i]);  
			 el.src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){ 
					el.removeAttribute('id');
				} 
			}
     	}
    }
function flex1RecVatCheck(defaultVatBillingGroup,currentDefaultVatValue) { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"> 
	var companyDivision=document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;
	var vatBillingGroupFlexValue="";
	var defaultVatBillingGroupDesc="";
	var relo1="" 
	<c:forEach var="entry" items="${vatBillingGroupList}">
	  if(relo1==""){ 
      if(defaultVatBillingGroup=='${entry.key}') {
    	  defaultVatBillingGroupDesc='${entry.value}';
    	  relo1="yes";
      }} 
	</c:forEach>
	var relo=""
		<c:forEach var="entry" items="${flex1RecCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach> 
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	var currentDefaultVatValueTest =  currentDefaultVatValue+",";
	if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
		 return true; 
	}else
	{
		return false;
		}
	</configByCorp:fieldVisibility >  
	  }
function flex2PayVatCheck(defaultVatBillingGroup,currentDefaultVatValue) { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	var companyDivision=document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;
	var vatBillingGroupFlexValue="";
	var defaultVatBillingGroupDesc="";
	var relo1="" 
	<c:forEach var="entry" items="${vatBillingGroupList}">
	  if(relo1==""){ 
      if(defaultVatBillingGroup=='${entry.key}') {
    	  defaultVatBillingGroupDesc='${entry.value}';
    	  relo1="yes";
      }} 
	</c:forEach>
	var relo=""
		<c:forEach var="entry" items="${flex2PayCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach> 
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	var currentDefaultVatValueTest =  currentDefaultVatValue+",";
	if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
		 return true; 
	}else
	{
		return false;
		}
	</configByCorp:fieldVisibility >  
	  } 
	  
function findDefaultVatFromPartnerForBill(){
	<c:if test="${systemDefaultVatCalculation=='true'}">

	var bCode = document.forms['accountLineForms'].elements['accountLine.billToCode'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http52222.open("GET", url, true); 
	    http52222.onreadystatechange = handleHttp92222; 
	    http52222.send(null);	     
     }</c:if>}
function handleHttp92222(){
    if (http52222.readyState == 4){    	
    	var results = http52222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value==''){
					 	document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value = res[8];
			    	}  }
	        }else{
	       	 var recVatDescr = '${accountLine.recVatDescr}'; 
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex1RecVatCheck(res[12],res[8]); 
                      if(chkVatBillingGroup){
                    	  if(recVatDescr != res[8]){
                       		 var agree = confirm("Updating Billtocode code will also change the Receivable VAT code. Do you wish to continue? ");
             					if(agree){
             						document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value = res[8];
             						getVatPercent();
             					}
                            }
                    	  	 
                      } 
                       
		        	 }
		        
	        	
	        }
		    	}    	} }	





function findDefaultVatFromPartnerForVendor(){
	<c:if test="${systemDefaultVatCalculation=='true'}">

	var bCode = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5222.open("GET", url, true); 
	    http5222.onreadystatechange = handleHttpStorageVat92222; 
	    http5222.send(null);	     
     }</c:if>}
function handleHttpStorageVat92222(){
    if (http5222.readyState == 4){    	
    	var results = http5222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value==''){
					 	document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value = res[8];
			    	}  }
	        }else{
	       	 var payVatDescr = '${accountLine.payVatDescr}'; 
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex2PayVatCheck(res[12],res[8]); 
                      if(chkVatBillingGroup){
                    	  if(payVatDescr != res[8]){
                        		 var agree = confirm("Updating Vendor code will also change the Payable VAT code. Do you wish to continue?");
              					if(agree){
              						document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value = res[8];	
              						getPayPercent();
              					}
                             }
                    	  	 
                      } 
                       
		        	 }
		          
	        	
	        }
		    	}    	} }
	  
 </script>