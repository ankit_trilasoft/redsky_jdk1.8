<script type="text/javascript"> 
function callMobileMoverService(target){
	document.forms['workTicketForm'].action="getMobMoverService.html?sid=${serviceOrder.id}&wktId=${workTicket.id}&buttonType="+target;
	document.forms['workTicketForm'].submit();
}
function syncWithMobileMoverService(){
	document.forms['workTicketForm'].action="syncMobMoverService.html?sid=${serviceOrder.id}&wktId=${workTicket.id}";
	document.forms['workTicketForm'].submit();
}
function showMMButton(){
	var serviceType=document.forms['workTicketForm'].elements['workTicket.service'].value;
	if(serviceType=='PAD' || serviceType=='LOD' || serviceType=='PK' ||  serviceType=='DL' ){
		document.getElementById('MM').style.display = 'none';
		document.getElementById('MMInv').style.display = 'block';
	}else{
		document.getElementById('MM').style.display = 'block';
		document.getElementById('MMInv').style.display = 'none';
	}
}
function titleCase(element){ 
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
}
window.onload=function(){
};
function showAttachedDocList(id,position){
	var url="ticketDocs.html?ajax=1&decorator=simple&popup=true&id="+id;
	ajax_showTooltip(url,position);	
}
</script> 
<script type="text/javascript">
	function autoPopulate_workTicket_originCountry(targetElement) {
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States'){
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India'){
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada'){
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['workTicketForm'].elements['workTicket.state'].disabled = false;
			if(oriCountry == 'United States'){
			document.forms['workTicketForm'].elements['workTicket.zip'].focus();
			}
		}else{
			document.forms['workTicketForm'].elements['workTicket.state'].disabled = true;
			document.forms['workTicketForm'].elements['workTicket.state'].value = '';
			if(oriCountry == 'Netherlands'|| oriCountry == 'Austria' ||oriCountry == 'Belgium' ||oriCountry == 'Bulgaria' ||oriCountry == 'Croatia' ||oriCountry == 'Cyprus' ||oriCountry == 'Czech Republic' ||
			oriCountry == 'Denmark' ||oriCountry == 'Estonia' ||oriCountry == 'Finland' ||oriCountry == 'France' ||oriCountry == 'Germany' ||oriCountry == 'Greece' ||oriCountry == 'Hungary' ||
			oriCountry == 'Iceland' ||oriCountry == 'Ireland' ||oriCountry == 'Italy' ||oriCountry == 'Latvia' ||oriCountry == 'Lithuania' ||oriCountry == 'Luxembourg' ||oriCountry == 'Malta' ||
			oriCountry == 'Poland' ||oriCountry == 'Portugal' ||oriCountry == 'Romania' ||oriCountry == 'Slovakia' ||oriCountry == 'Slovenia' ||oriCountry == 'Spain' ||oriCountry == 'Sweden' ||
			oriCountry == 'Turkey' ||oriCountry == 'United Kingdom' ){
				document.forms['workTicketForm'].elements['workTicket.zip'].focus();
			}			
		}
	}
</script>
<script type="text/javascript">
	function autoPopulate_workTicket_destinationCountry(targetElement) {
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States'){
			dCountryCode = "USA";
		}
		if(dCountry == 'India')	{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada'){
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled = false;
			if(dCountry == 'United States'){
			document.forms['workTicketForm'].elements['workTicket.destinationZip'].focus();
			}
		}else{
			document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled = true;
			document.forms['workTicketForm'].elements['workTicket.destinationState'].value = '';
			if(dCountry== 'Netherlands'|| dCountry== 'Austria' ||dCountry== 'Belgium' ||dCountry== 'Bulgaria' ||dCountry== 'Croatia' ||dCountry== 'Cyprus' ||dCountry== 'Czech Republic' ||
			dCountry== 'Denmark' ||dCountry== 'Estonia' ||dCountry== 'Finland' ||dCountry== 'France' ||dCountry== 'Germany' ||dCountry== 'Greece' ||dCountry== 'Hungary' ||
			dCountry== 'Iceland' ||dCountry== 'Ireland' ||dCountry== 'Italy' ||dCountry== 'Latvia' ||dCountry== 'Lithuania' ||dCountry== 'Luxembourg' ||dCountry== 'Malta' ||
			dCountry== 'Poland' ||dCountry== 'Portugal' ||dCountry== 'Romania' ||dCountry== 'Slovakia' ||dCountry== 'Slovenia' ||dCountry== 'Spain' ||dCountry== 'Sweden' ||
			dCountry== 'Turkey' ||dCountry== 'United Kingdom' ){
				document.forms['workTicketForm'].elements['workTicket.destinationZip'].focus();
			}			
		}
	}		
	function handOutChkedUnchkedVolumePieces1(chk){
		var ids="";
		var url="";
		var ticket = document.forms['workTicketForm'].elements['workTicket.ticket'].value;
		var targetActual=document.forms['workTicketForm'].elements['workTicket.targetActual'].value;
    	if(targetActual!='C'){
		      if(chk.checked){
		    	ids = chk.value;
				url="handOutVolumePieces.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket)+"&ids="+encodeURI(ids);
				http661.open("GET", url, true);
				http661.onreadystatechange = handleHttpResponse441;
				http661.send(null);	
			    }
		       if(chk.checked == false){
			    	ids = chk.value;
					url="handOutVolumePieces.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket)+"&ids="+encodeURI(ids);
					http662.open("GET", url, true);
					http662.onreadystatechange = handleHttpResponse442;
					http662.send(null);	
				}			 			 
    	}else{
    		alert('You cannot check handout for cancelled ticket.');
        }
	}
	function handleHttpResponse441()  {
	    if (http661.readyState == 4)  {
          var results = http661.responseText
          results = results.trim();
          var resu=results.replace('[','');
          resu=resu.replace(']','');
	      var res = resu.split("~");      	      
		  var totPieces = eval(document.forms['workTicketForm'].elements['totalHandoutPieces'].value);
	      if(!isNaN(totPieces)){
		      if(res[0]!='null'){
		           totPieces = parseInt(totPieces) + parseInt(res[0]);
	          }else{
	               totPieces = parseInt(totPieces);
		      }
	      }
	      document.forms['workTicketForm'].elements['totalHandoutPieces'].value = totPieces;	

	      var totVol = eval(document.forms['workTicketForm'].elements['totalHandoutVolume'].value);
	      if(!isNaN(totVol)){
	    	  if(res[1]!=undefined){
	                 totVol = parseFloat(totVol) + parseFloat(res[1]);			
	          }else{
	                 totVol = parseFloat(totVol);
	          }
		  }
	      document.forms['workTicketForm'].elements['totalHandoutVolume'].value = totVol;   	
       }
   }
	function handleHttpResponse442()  {
	    if (http662.readyState == 4)  {
          var results = http662.responseText
          results = results.trim();
          var resu=results.replace('[','');
          resu=resu.replace(']','');
	      var res = resu.split("~");	     	      
	      var totPieces = eval(document.forms['workTicketForm'].elements['totalHandoutPieces'].value);
	      if(!isNaN(totPieces)){
		      if(res[0]!='null'){
		           totPieces = parseInt(totPieces) - parseInt(res[0]);
	          }else{
	               totPieces = parseInt(totPieces);
		      }
	      }
	      document.forms['workTicketForm'].elements['totalHandoutPieces'].value = totPieces;	

	      var totVol = eval(document.forms['workTicketForm'].elements['totalHandoutVolume'].value);
	      if(!isNaN(totVol)){
	    	  if(res[1]!=undefined){
	                 totVol = parseFloat(totVol) - parseFloat(res[1]);			
	          }else{
	                 totVol = parseFloat(totVol);
	          }
		  }
	      document.forms['workTicketForm'].elements['totalHandoutVolume'].value = totVol;   	
       }
   }
	function handOutChkedUnchkedVolumePieces(chk,handOutPieces,handOutVolume,ticketNumber){
        var handOutPiecesA = eval(document.forms['workTicketForm'].elements[handOutPieces].value);
        var handOutVolumeA = eval(document.forms['workTicketForm'].elements[handOutVolume].value);
		var targetActual = document.forms['workTicketForm'].elements['workTicket.targetActual'].value;
		var totalHandoutPieces = eval(document.forms['workTicketForm'].elements['totalHandoutPieces'].value);
		var totalHandoutVolume = eval(document.forms['workTicketForm'].elements['totalHandoutVolume'].value);
		var forceToHandOut =document.forms['workTicketForm'].elements['forceToHandOut'].value;
		var ticketNumber=ticketNumber;
		var listCustomTicketIn='${customsInTicketNumber}';
		var index="";
		if(forceToHandOut=='true' || forceToHandOut==true){
		index = (listCustomTicketIn.indexOf(ticketNumber)> -1);
		}
		if(index==''){
    	if(targetActual!='C'){
    		if(chk.checked){
        		if(handOutPiecesA == undefined){}else{
    			totalHandoutPieces = totalHandoutPieces + handOutPiecesA;
        		}
    			document.forms['workTicketForm'].elements['totalHandoutPieces'].value = totalHandoutPieces;
    			if(handOutVolumeA == undefined){}else{		    	
    		    	totalHandoutVolume = totalHandoutVolume + handOutVolumeA;
    		    	totalHandoutVolume=""+totalHandoutVolume;
    			        if(totalHandoutVolume.indexOf(".") == -1){
    			        	totalHandoutVolume = totalHandoutVolume+".00";
    			        } 
    			        if((totalHandoutVolume.indexOf(".")+3 != totalHandoutVolume.length)){
    			        	totalHandoutVolume = totalHandoutVolume+"0";
    			        }
    	          }			
    			document.forms['workTicketForm'].elements['totalHandoutVolume'].value = totalHandoutVolume;
			}
		    if(chk.checked == false){
		    	if(handOutPiecesA == undefined){}else{
	        		totalHandoutPieces = totalHandoutPieces - handOutPiecesA;
	        	}
		    	document.forms['workTicketForm'].elements['totalHandoutPieces'].value = totalHandoutPieces;
	        	if(handOutVolumeA == undefined){}else{		    	
		    	totalHandoutVolume = totalHandoutVolume - handOutVolumeA;
		    	totalHandoutVolume=""+totalHandoutVolume;
			        if(totalHandoutVolume.indexOf(".") == -1){
			        	totalHandoutVolume = totalHandoutVolume+".00";
			        } 
			        if((totalHandoutVolume.indexOf(".")+3 != totalHandoutVolume.length)){
			        	totalHandoutVolume = totalHandoutVolume+"0";
			        }
	        	}
    			document.forms['workTicketForm'].elements['totalHandoutVolume'].value = totalHandoutVolume; 
			}
    	}else{
    		alert('You cannot check handout for cancelled ticket.');
        }
		}
	}
	function HandOut(ticketNumber,chk){
		var ids="";
		var url="";
		var ticketNumber=ticketNumber;
		var listCustomTicketIn='${customsInTicketNumber}';	
		var index = (listCustomTicketIn.indexOf(ticketNumber)> -1);
		if(index==''){	
    	var hoTicket = document.forms['workTicketForm'].elements['workTicket.ticket'].value;
    	var targetActual=document.forms['workTicketForm'].elements['workTicket.targetActual'].value;
    	if(targetActual!='C'){
    	
	     if(chk.checked){
	    	ids = chk.value;
			url="updateForHandout.html?ajax=1&decorator=simple&handout=A&popup=true&hoTicket="+encodeURI(hoTicket)+"&ids="+encodeURI(ids);
	     }
	     if(chk.checked==false){
		    ids = chk.value;
		    url="updateForHandout.html?ajax=1&decorator=simple&handout=''&popup=true&hoTicket="+encodeURI(hoTicket)+"&ids="+encodeURI(ids);
		  }	     
		  http6.open("GET", url, true);
		  http6.onreadystatechange = handleHttpResponse4444;
		  http6.send(null);	
		}
		else{
		alert('You cannot check handout for cancelled ticket.');
         var len=document.forms['workTicketForm'].elements['checkho'].length;
         for(i=0;i<len;i++){
            document.forms['workTicketForm'].elements['checkho'][i].checked = false ; 
           }               
		  }
		}else{
			if(chk.checked){
			var str="THESE GOODS ARE NOT ALLOWED TO BE HANDED OUT - THEY ARE STILL AT CUSTOMS !\n\n WOULD YOU LIKE TO PROCEED PRESS Ok or Cancel.";
			var agree=confirm(str);
			if (agree){
			   	var hoTicket = document.forms['workTicketForm'].elements['workTicket.ticket'].value;
		    	var targetActual=document.forms['workTicketForm'].elements['workTicket.targetActual'].value;
		    	if(targetActual!='C'){
		    	
			     if(chk.checked){
			    	ids = chk.value;
					url="updateForHandout.html?ajax=1&decorator=simple&handout=A&popup=true&hoTicket="+encodeURI(hoTicket)+"&ids="+encodeURI(ids);
			     }
			     if(chk.checked==false){
				    ids = chk.value;
				    url="updateForHandout.html?ajax=1&decorator=simple&handout=''&popup=true&hoTicket="+encodeURI(hoTicket)+"&ids="+encodeURI(ids);
				  }	     
				  http6.open("GET", url, true);
				  http6.onreadystatechange = handleHttpResponse4444;
				  http6.send(null);	
				}
				else{
				alert('You cannot check handout for cancelled ticket.');
		         var len=document.forms['workTicketForm'].elements['checkho'].length;
		         for(i=0;i<len;i++){
		            document.forms['workTicketForm'].elements['checkho'][i].checked = false ; 
		           }               
				  }
		    	document.forms['workTicketForm'].elements['forceToHandOut'].value=false;
			}else{
			ids = chk.value;
			document.forms['workTicketForm'].elements['checkH'+ids].checked = false ;
			document.forms['workTicketForm'].elements['forceToHandOut'].value=true;
			}
			}
		}
	}
	function handleHttpResponse4444()  {
	    if (http6.readyState == 4)  {  }
        }
	var http6 = getHTTPObject2();
	var http661 = getHTTPObject2();
	var http662 = getHTTPObject2();
    function getHTTPObject2(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	  }
	  var httpHandOut=getHTTPObject2();
	  function changeHandoutAccessss(targetElement){
	  var targetValue=targetElement.value;   
	  var ticket=document.forms['workTicketForm'].elements['workTicket.ticket'].value;
	  var hoTicket='';
	  if(targetValue=='C'){
	   url="changeHandoutAccess.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket);
	   httpHandOut.open("GET",url,true);
	   httpHandOut.onreadystatechange=handOutHttpResponse;
	   httpHandOut.send(null);
	  }	  
	  }
	  function handOutHttpResponse()  {
	    if (httpHandOut.readyState == 4)  { 
	    var len=document.forms['workTicketForm'].elements['checkho'].length;
         for(i=0;i<len;i++){
            document.forms['workTicketForm'].elements['checkho'][i].checked = false ; 
         }       }
        }
        
        function disableEnter(evt){
       var keyCode= evt.which ? evt.which : evt.keyCode;
       if(keyCode==13){
       return false;
       }
       else { return true; } 

        }
</script>