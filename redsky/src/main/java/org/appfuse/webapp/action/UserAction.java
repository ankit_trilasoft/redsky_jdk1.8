package org.appfuse.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.AuthenticationTrustResolver;
import org.acegisecurity.AuthenticationTrustResolverImpl;
import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.LabelValue;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.util.RequestUtil;
import org.springframework.dao.DataIntegrityViolationException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.Permissions;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.webapp.action.CustomerFileAction;

public class UserAction extends BaseAction implements Preparable {
    private static final long serialVersionUID = 6776558938712115191L;
	private File file;
	private File signatureFile;
    private List users;
    private User user;
    private User loginUser;
    private String id;
    private String cid;
    private String fileFileName;
    private String category;
    private String userAccountSection="normal";
    private List leftList;
    private String signatureFileFileName;
    private List refMasters;
    private RefMasterManager refMasterManager;
    private SystemDefault systemDefault;
    private SystemDefaultManager systemDefaultManager;
	private DataSecurityFilterManager dataSecurityFilterManager;
    private DataSecuritySet dataSecuritySet;
	private DataSecurityFilter dataSecurityFilter;
    private CompanyManager companyManager;
    private static Map<String, String>PWD_HINT;
    private static Map<String, String> opscalendar;
    private static Map<String, String>RMDINTRVL;
    private static Map<String, String>ostates;
    private static Map<String, String>ocountry;
    private static Map<String, String> usertype;
    private Map<String, String> soDashboardViewList=new LinkedHashMap<String, String>();
    private String sessionCorpID;
    private List availRoles;
	private CustomerFileManager customerFileManager;
	private CustomerFile customerFile;
	private String location;
	private String defaultURL;
	private String defaultSoURL;
	private String usernm;
	private String url;
    private Date oldCreatedOn;
    private List availPermissions;
	private DataSecuritySetManager dataSecuritySetManager;
	private Company company;
	private static Map<String, String> defaultWarehouse;
	private String validateFormNav;
	private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
	private List gender;
	private List companyDivis = new ArrayList(); 
	private Boolean  mobile_serv_access;
	private String tableFieldName; 
	private String tableName;
	private List tableNameList;
	private Map<String, String> moveTypeList;
	private boolean accessQuotationFromCustomerFileFlag=false;
	private Map<String, String> defaultSortforFileCabinet;
	private List sortOrderForFileCabinet;
	private List multiSortforFileCabinet;
	private Map<String, String> commodits;
	private Map<String, String> service;
	/* Variable to check whether the user has Data security set or not */
	private Boolean hasDataSecuritySet;
	private PartnerManager partnerManager;
	private List recordLimitList;
	private List JobFunction;
	private Map<String, String> userJobTypeMap;
	private Set<Role> roles;
	private User addUser;
	private boolean adminAccess =false;
	
  /*  public UserAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
    }*/
	static final Logger logger = Logger.getLogger(CustomerFileAction.class);
	Date currentdate = new Date();
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){
	    	 
	        if (getRequest().getMethod().equalsIgnoreCase("post")) {
	            if (!"".equals(getRequest().getParameter("user.id"))) {
	                 user = userManager.getUser(getRequest().getParameter("user.id"));
	            }
	        }
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        loginUser = (User)auth.getPrincipal();
	        roles = loginUser.getRoles(); 
	        if(roles!=null){
				for (Role role : roles){				
					if(role.getName().toString().equals("ROLE_ADMIN")){
						adminAccess=true;
					}
			}
				 
			}
	        company= companyManager.findByCorpID(user.getCorpID()).get(0);
	        this.sessionCorpID = user.getCorpID();	        
	        getSession().setAttribute("sessionTimeZone", company.getTimeZone());
	        getSession().setAttribute("usernm", user.getUsername());
	        getSession().setAttribute("username", username);
	        getSession().setAttribute("url", user.getDefaultURL());
	        getSession().setAttribute("defaultURL", defaultURL);  
	        getSession().setAttribute("defaultSoURL", defaultSoURL);
	        systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
			gender=new ArrayList();
			gender.add("Male");
			gender.add("Female");
			coordinatorList = refMasterManager.findUser("UTSI","ROLE_COORD");
			if(company!=null){
				if((company.getAccessQuotationFromCustomerFile()!=null) && (company.getAccessQuotationFromCustomerFile())){
					accessQuotationFromCustomerFileFlag=true;
				}
				
			}
			recordLimitList = new ArrayList();
			recordLimitList.add("20");
			recordLimitList.add("40");
			recordLimitList.add("60");
			roles = user.getRoles();
			getComboList(sessionCorpID);
		}
    }
    
    public List getRefMasters() {
		return refMasters;
	}

	public List getUsers() {
        return users;
    }

    public void setId(String id) {
        this.id = id;
    } 
   
   
   @SkipValidation
	public String findtoolTip() {		
	   tableName = userManager.findToolTip(tableFieldName,"");
	   List arr=new ArrayList();
	   String tn[]=tableName.split(",");
	   if(tn!=null && tn.length>=10){
		   for(int i=(tn.length-1);i>=(tn.length-10);i--){
			   arr.add(tn[i])  ;
		   }
	   }else if(tn!=null && tn.length<10){
		   for(int i=(tn.length-1);i>=0;i--){
			   arr.add(tn[i])  ;
		   }
		   
	   }
	   tableNameList=arr;
		return SUCCESS;
	}

public String removePhotograph() throws IOException {
    userManager.removeUserPhotograph(location, username);
 return SUCCESS;
   
}

@SkipValidation
public String findtoolTipForPswd() {
	return SUCCESS;

}
protected void handleNotAuthorized(HttpServletRequest request,
        HttpServletResponse response)
throws ServletException, IOException {
response.sendError(HttpServletResponse.SC_FORBIDDEN);
}
   
    public String edit() throws IOException, ServletException {
        HttpServletRequest request = getRequest();
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
        User user1111 = (User)auth1.getPrincipal();
       commodits = refMasterManager.findByParameter(sessionCorpID, "COMMODIT");
        soDashboardViewList.put("soListView","Service Order List");
        soDashboardViewList.put("soDashboardView","Dashboard List");
        company= companyManager.findByCorpID(user1111.getCorpID()).get(0);
        boolean editProfile = (request.getRequestURI().indexOf("editProfile") > -1);
        if (editProfile) {
            if ((request.getParameter("id") != null) || (request.getParameter("from") != null)) {
                ServletActionContext.getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
                log.warn("User '" + request.getRemoteUser() + "' is trying to edit user '" +
                         request.getParameter("id") + "'");
                return null;
            }
        }

        if (id != null) {
            user = userManager.getUser(id);
            roles = loginUser.getRoles();
            boolean adminAccess =false;
            if(roles!=null){
    			for (Role role : roles){				
    				if(role.getName().toString().equals("ROLE_ADMIN")){
    					adminAccess=true;
    				}
    		}
    			 
    		} 
    		userType="USER";
    		enableUser="Y";
    		if(adminAccess) {
            
    		}else {
    			if(loginUser.getParentAgent()!=null && (!(loginUser.getParentAgent().toString().trim().equals("")))) { 
    				if(user.getParentAgent()!=null && (!(user.getParentAgent().toString().trim().equals(""))) && loginUser.getParentAgent().toString().equalsIgnoreCase(user.getParentAgent())) {
    					
    				}else {
    					HttpServletResponse response = ServletActionContext.getResponse();
    			        handleNotAuthorized(request, response);
    			        return null;
    				}
    			
    			}else { 
    				
    			}
    		}
            oldCreatedOn = user.getCreatedOn();
            getComboList(sessionCorpID);
          if((user.getDefaultSortforFileCabinet()!=null)&&(!user.getDefaultSortforFileCabinet().equalsIgnoreCase(""))){
				String[] ac = user.getDefaultSortforFileCabinet().split(",");
				Map newMap =new LinkedHashMap<String, String>();
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String s1 = ac[k];
					s1=s1.trim();
					if(s1!=null && !s1.equals("")){
					newMap.put(s1, defaultSortforFileCabinet.get(s1));
					}
				}
				Iterator mapIterator11 = defaultSortforFileCabinet.entrySet().iterator();
				while (mapIterator11.hasNext()) {
					Map.Entry entry = (Map.Entry) mapIterator11.next();
					String key1 = (String) entry.getKey();
					if (!(newMap.containsKey(key1))){ 
						newMap.put(key1,defaultSortforFileCabinet.get(key1));
					}
				}
				defaultSortforFileCabinet = new LinkedHashMap<String, String>();
				defaultSortforFileCabinet.putAll(newMap);
			}
			if((user.getDefaultSortforFileCabinet()!=null)&&(!user.getDefaultSortforFileCabinet().equalsIgnoreCase("")))
	    	 {
				multiSortforFileCabinet = new ArrayList();
				String[] ac = user.getDefaultSortforFileCabinet().split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					multiSortforFileCabinet.add(accConJobType.trim());
					}
			}
            ostates = customerFileManager.findDefaultStateList(user.getCountryHome(), sessionCorpID);

        } else if (editProfile) {
        	 user = userManager.getUserByUsername(request.getRemoteUser());
        	 oldCreatedOn = user.getCreatedOn();
        	 getComboList(sessionCorpID);
        	 if((user.getDefaultSortforFileCabinet()!=null)&&(!user.getDefaultSortforFileCabinet().equalsIgnoreCase(""))){
 				String[] ac = user.getDefaultSortforFileCabinet().split(",");
 				Map newMap =new LinkedHashMap<String, String>();
 				int arrayLength2 = ac.length;
 				for (int k = 0; k < arrayLength2; k++) {
 					String s1 = ac[k];
 					s1=s1.trim();
 					if(s1!=null && !s1.equals("")){
 					newMap.put(s1, defaultSortforFileCabinet.get(s1));
 					}
 				}
 				Iterator mapIterator11 = defaultSortforFileCabinet.entrySet().iterator();
 				while (mapIterator11.hasNext()) {
 					Map.Entry entry = (Map.Entry) mapIterator11.next();
 					String key1 = (String) entry.getKey();
 					if (!(newMap.containsKey(key1))){ 
 						newMap.put(key1,defaultSortforFileCabinet.get(key1));
 					}
 				}
 				defaultSortforFileCabinet = new LinkedHashMap<String, String>();
 				defaultSortforFileCabinet.putAll(newMap);
 			}
 			if((user.getDefaultSortforFileCabinet()!=null)&&(!user.getDefaultSortforFileCabinet().equalsIgnoreCase("")))
 	    	 {
 				multiSortforFileCabinet = new ArrayList();
 				String[] ac = user.getDefaultSortforFileCabinet().split(",");
 				int arrayLength2 = ac.length;
 				for (int k = 0; k < arrayLength2; k++) {
 					String accConJobType = ac[k];
 					multiSortforFileCabinet.add(accConJobType.trim());
 					}
 			} 
             ostates = customerFileManager.findDefaultStateList(user.getCountryHome(), sessionCorpID);
             if(company.getAccessQuotationFromCustomerFile()){
            		 if(user.getMoveType() == null || user.getMoveType().toString().equals("")){
            			 user.setMoveType("BookedMove");
 					} 				            	
             	}
 			
        } else {
            user = new User();
            user.setWorkStartTime("00:00");
            user.setWorkEndTime("00:00");
            user.setPasswordReset(true);
            user.setPasswordHintQues(" Default");
            user.setPasswordHint("Default");
            user.setCorpID(sessionCorpID);
            user.setCreatedOn(new Date());
            user.setUpdatedOn(new Date());
            user.addRole(new Role(Constants.USER_ROLE));
            user.setSoListView("soDashboardView");
            user.setFileCabinetView("Document Centre");
            if(company.getAccessQuotationFromCustomerFile()){
            	 if(user.getMoveType() == null || user.getMoveType().toString().equals("")){
        			 user.setMoveType("BookedMove");
					} 				            					            	
        	}
            getComboList(sessionCorpID);
            ostates = customerFileManager.findDefaultStateList("", sessionCorpID);

        }

        if (user.getUsername() != null) {
            user.setUpdatedOn(new Date());
        	user.setUpdatedBy(getRequest().getRemoteUser());
            user.setConfirmPassword(user.getPassword());
            log.debug("checking for remember me login...");
            AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
            SecurityContext ctx = SecurityContextHolder.getContext();

            if (ctx != null) {
                Authentication auth = ctx.getAuthentication();

                if (resolver.isRememberMe(auth)) {
                    getSession().setAttribute("cookieLogin", "true");
                    saveMessage(getText("userProfile.cookieLogin"));
                }
            }
        }
        try{
        	userJobTypeMap=new LinkedHashMap<String, String>() ;	
        	if(user !=null && user.getId() !=null){
        	user = userManager.getUser(user.getId().toString());
			String areaValue=user.getFunctionalArea();
			if(areaValue!=null && !(areaValue.equals(""))){
					String areaValue1[]=areaValue.split(","); 
					for(int i=0 ; i<areaValue1.length; i++){
						userJobTypeMap.put(areaValue1[i], areaValue1[i]);
					}
			      }
        }
        }
        catch(NullPointerException ex){
        	ex.printStackTrace();
        }
        catch(Exception e){
        	e.printStackTrace();
        }
       // if(user.getUserType() != null && !user.getUserType().equals("")){
        	availRoles = userManager.findUserRoles(sessionCorpID,user.getUserType());
        /*}else{
        	availRoles = userManager.findUserRoles(sessionCorpID);
        }*/
        availPermissions=userManager.getPermissionList();
        getSession().setAttribute("availPermissions", availPermissions);
        getSession().setAttribute("availRoles", availRoles);
        return SUCCESS;
    }
    
    public String ajaxRoleBasedOnCategory(){
    	// if(category != null && !category.equals("")){
         	leftList = userManager.findUserRoles(sessionCorpID,category);
       /*  }else{
        	 leftList = userManager.findUserRoles(sessionCorpID);
         }*/
    	return SUCCESS;
    }

    public String execute() {       
    	return SUCCESS;
    }

    public String cancel() {
        if (!"list".equals(from)) {
            return "mainMenu";
        }
        return "cancel";
    }

    public String save() throws Exception {
    	
       Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
       //commodits = refMasterManager.findByParameter(sessionCorpID, "COMMODIT");
       //soDashboardViewList.put("soListView","Service Order List");
       //soDashboardViewList.put("soDashboardView","Dashboard List");
       user.setUpdatedOn(new Date());
   	   user.setUpdatedBy(getRequest().getRemoteUser());
        if ("true".equals(getRequest().getParameter("encryptPass")) && (encrypt != null && encrypt)) {
            String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);

            if (algorithm == null) { // should only happen for test case
                log.debug("assuming testcase, setting algorithm to 'SHA'");
                algorithm = "SHA";
            }
            String pass=user.getPassword();
            user.setPassword(StringUtil.encodePassword(pass, algorithm));
            user.setConfirmPassword(StringUtil.encodePassword(pass, algorithm));
        }
      commodits = refMasterManager.findByParameter(sessionCorpID, "COMMODIT");
        soDashboardViewList.put("soListView","Service Order List");
        soDashboardViewList.put("soDashboardView","Dashboard List");
        if(user.getUserType() != null && !user.getUserType().equals("")){
        	availRoles = userManager.findUserRoles(sessionCorpID,user.getUserType());
        }else{
        	availRoles = userManager.findUserRoles(sessionCorpID);
        }
        getSession().setAttribute("availRoles", availRoles);
        //
        if(mobile_serv_access!=null){
        user.setMobile_access(mobile_serv_access);
        }else{
        	  user.setMobile_access(false);
        }
        if(user.getMobile_access()!=null && (user.getMobile_access())){
        	if(user.getMacid()!=null && !user.getMacid().equals("") && user.getMacid().equals("ACCOUNT_DISABLED")){
        		user.setMacid("");
        	}
        }else{
        	user.setMacid("ACCOUNT_DISABLED");
        }
        getComboList(sessionCorpID);
        if((user.getDefaultSortforFileCabinet()!=null)&&(!user.getDefaultSortforFileCabinet().equalsIgnoreCase("")))
   	 	{
			multiSortforFileCabinet = new ArrayList();
			String[] ac = user.getDefaultSortforFileCabinet().split(",");
			int arrayLength2 = ac.length;
			for (int k = 0; k < arrayLength2; k++) {
				String accConJobType = ac[k];
				multiSortforFileCabinet.add(accConJobType.trim());
				}
		} 
        
        if((user.getDefaultSortforFileCabinet()!=null)&&(!user.getDefaultSortforFileCabinet().equalsIgnoreCase(""))){
				String[] ac = user.getDefaultSortforFileCabinet().split(",");
				Map newMap =new LinkedHashMap<String, String>();
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String s1 = ac[k];
					s1=s1.trim();
					if(s1!=null && !s1.equals("")){
					newMap.put(s1, defaultSortforFileCabinet.get(s1));
					}
				}
				Iterator mapIterator11 = defaultSortforFileCabinet.entrySet().iterator();
				while (mapIterator11.hasNext()) {
					Map.Entry entry = (Map.Entry) mapIterator11.next();
					String key1 = (String) entry.getKey();
					if (!(newMap.containsKey(key1))){ 
						newMap.put(key1,defaultSortforFileCabinet.get(key1));
					}
				}
				defaultSortforFileCabinet = new LinkedHashMap<String, String>();
				defaultSortforFileCabinet.putAll(newMap);
			}
        availPermissions=userManager.getPermissionList();
        getSession().setAttribute("availPermissions", availPermissions);
        Set<Role> oldUserRoles=new HashSet<Role>();
        oldUserRoles.addAll(user.getRoles());
        Integer originalVersion = user.getVersion();
    	String[] userRolesss = getRequest().getParameterValues("userRoles");
    	Set<Role> newRoleList=new HashSet<Role>();
    	Set<Role> removeRoleList=new HashSet<Role>();
    	if(userRolesss!=null){
	    	for(String str:userRolesss){
	    		if(!oldUserRoles.contains(roleManager.getRole(str))){
	    			newRoleList.add(roleManager.getRole(str));
	    		}
	    	}
	    	user.getRoles().addAll(newRoleList);	    	
    	}else{
    		user.getRoles().clear();
    	}
    	for(Iterator<Role> i = oldUserRoles.iterator(); i.hasNext(); ) {
    		  Role item = i.next();
    		  if(!Arrays.asList(getRequest().getParameterValues("userRoles")).contains((item.getName()))){
    			  removeRoleList.add(item);
      			}
    		}
    	if((removeRoleList!=null)&&(!removeRoleList.isEmpty())){
        	if((removeRoleList!=null && !removeRoleList.isEmpty()) && user!=null && user.getId()!=null){
        		String temp="";
        		for(Role r:removeRoleList){
        			if(temp.equalsIgnoreCase("")){
        				temp="'"+r.getId()+"'";
        			}else{
        				temp=temp+",'"+r.getId()+"'";
        			}
        		}
        		userManager.updateUserRoleInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
        	} 
        	user.getRoles().removeAll(removeRoleList);
    	}
    	
        boolean isNew = ("".equals(getRequest().getParameter("user.version")));
        if ((getRequest().isUserInRole(Constants.ADMIN_ROLE)|| getRequest().isUserInRole(Constants.USER_ROLE)) && userAccountSection.equals("normal")) {
            String[] userRoles = getRequest().getParameterValues("userRoles");

            for (int i = 0; userRoles != null && i < userRoles.length; i++) {
            	try{
                String roleName = userRoles[i];
                user.addRole(roleManager.getRole(roleName));
            }
            
            catch(DataIntegrityViolationException ue){
            	// System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n Saurabh ");
            //	throw new UserExistsException("User '" + user.getUsername() + "' already exists!" + ue.toString());
            	log.warn("User '" + user.getUsername() + "' already exists!" + ue.toString());
                List<String> args = new ArrayList<String>();
                args.add(user.getUsername());
                args.add(user.getEmail());
                addActionError(getText("errors.existing.user", args));
                user.setVersion(originalVersion);
                user.setPassword(user.getConfirmPassword());
                user.setUsername("");
                user.setRoles(oldUserRoles); 
                if(mobile_serv_access!=null){
               	user.setMobile_access(mobile_serv_access);
                }else{
                	user.setMobile_access(false);
                }
                
                validateFormNav="NOTOK";
                return INPUT;
            }
            }
        }else{
        	user.setRoles(oldUserRoles);
        }
       Set<DataSecuritySet> DataSecuritySet23=new HashSet<DataSecuritySet>();
       DataSecuritySet23.addAll(user.getDatasecuritysets());
	   	Set<DataSecuritySet> newPermissionsList=new HashSet<DataSecuritySet>();
		Set<DataSecuritySet> removepermissionsList=new HashSet<DataSecuritySet>();

        try{
        	if(userAccountSection.equals("normal")){
        		 //Coding for Audit bug Start
        		 //user.getDatasecuritysets().clear();
        	    	String[] userpermissionss = getRequest().getParameterValues("permissions");
        	    	if(userpermissionss!=null){
	        	    	for(String str:userpermissionss){
	        	    		if(!DataSecuritySet23.contains((DataSecuritySet)userManager.getPermission(str).get(0))){
	        	    			newPermissionsList.add((DataSecuritySet)userManager.getPermission(str).get(0));
	        	    		}
	        	    	} 
	        	    	user.getDatasecuritysets().addAll(newPermissionsList);
        	    	}else{
        	    		user.getDatasecuritysets().clear();
        	    	}
        	    	for(Iterator<DataSecuritySet> i = DataSecuritySet23.iterator(); i.hasNext(); ) {
        	    		DataSecuritySet item = i.next();
        	    		  if(!Arrays.asList(getRequest().getParameterValues("permissions")).contains((item.getName()))){
        	    			  removepermissionsList.add(item);
        	      			}
        	    		}
        	    	if((removepermissionsList!=null)&&(!removepermissionsList.isEmpty())){
        	    		if((removepermissionsList!=null && !removepermissionsList.isEmpty()) && user!=null && user.getId()!=null){
        	        		String temp="";
        	        		for(DataSecuritySet r:removepermissionsList){
        	        			if(temp.equalsIgnoreCase("")){
        	        				temp="'"+r.getId()+"'";
        	        			}else{
        	        				temp=temp+",'"+r.getId()+"'";
        	        			}
        	        		}
        	        		userManager.updateUserInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
        	        	}
        	    		user.getDatasecuritysets().removeAll(removepermissionsList);
        	    	}
        	    	
           		 //Coding for Audit bug End        	    	
        	        String[] permissions = getRequest().getParameterValues("permissions");
        	        for (int i = 0; permissions != null && i < permissions.length; i++) {
        	        String permissionName = permissions[i];
        	        user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName).get(0));	
        	}
        }else{
        	user.addPermissions((DataSecuritySet) DataSecuritySet23);	
        }
        }catch(Exception ex){
        }
        try{
          if( (!(user.getUserType().toString().trim().equalsIgnoreCase("AGENT")) ) && (!(user.getUserType().toString().trim().equalsIgnoreCase("PARTNER"))) ) {       	
        	 String permissionName1 = "DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT";
             user.removePermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));       	
          }
        }catch (Exception e) {
			// TODO: handle exception
		}
       /* if(user.getRoles().toString().contains("ROLE_AGENT") || user.getRoles().toString().contains("ROLE_PARTNER"))
        { 
        	String permissionName1 = "DATA_SECURITY_SET_SO_NETWORKSO_PARTNERPORTAL";
            user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
        	//List partnerExpirationDuration=userManager.getPartnerExpirationDate(sessionCorpID, user.getUsername());
        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));
        	try{
        	List dataDiff=userManager.findDateDiff(user.getPwdexpiryDate(), compareWithDate);
        	if(user.getPwdexpiryDate()==null || Long.parseLong(dataDiff.get(0).toString())<=0)
        	{
        		user.setPwdexpiryDate(compareWithDate);
        	}
        	}catch(Exception ex)
        	{
        		user.setPwdexpiryDate(compareWithDate);
        	}

        
        }*/
        String checkSales="";
	        String[] salesUserRolesss = getRequest().getParameterValues("userRoles");
	        for (String string : salesUserRolesss) {
				if(string.equalsIgnoreCase("ROLE_SALES_PORTAL")){
					checkSales="SALES";
				break;	
				}				
			}
        
        if((user.getUserType().toString().trim().equalsIgnoreCase("AGENT") ) || (user.getUserType().toString().trim().equalsIgnoreCase("PARTNER")) ){       	
        	String permissionName1 = "DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT";
            user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
            newPermissionsList.add((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
        	//List partnerExpirationDuration=userManager.getPartnerExpirationDate(sessionCorpID, user.getUsername());
        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));
        	Date currentDate = DateUtils.addDays(getCurrentDate(), 0);
        	try{
	        	/*List dataDiff=userManager.findDateDiff(user.getPwdexpiryDate(), currentDate);
	        	if(user.getPwdexpiryDate()==null || Long.parseLong(dataDiff.get(0).toString())<=0){
	        		user.setPwdexpiryDate(compareWithDate);
	        	}else{
	        		user.setPwdexpiryDate(user.getPwdexpiryDate());
	        	}*/
        		if(user.getPwdexpiryDate()==null || compareWithDate.after(user.getPwdexpiryDate())){
	        		user.setPwdexpiryDate(compareWithDate);
	        	}else{
	        		user.setPwdexpiryDate(user.getPwdexpiryDate());
	        	}
        	}catch(Exception ex)
        	{
        		user.setPwdexpiryDate(compareWithDate);
        	}
        }else if((checkSales.equalsIgnoreCase("SALES")) ){
        	/*String permissionName1 = "DATA_SECURITY_SET_NOTES_SALESPORTAL_AGENTS";        	
            user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));*/
            if(user.getSalesPortalAccess().equals(true) && user.getParentAgent()!=null && !user.getParentAgent().equals("") ){
            	String permissionName2 = "DATA_SECURITY_SET_SO_SALESPORTAL_"+user.getParentAgent()+""; 
            	String prefix = "DATA_SECURITY_FILTER_SO_BOOKINGAGENTCODE_"+user.getParentAgent();
            	//List permissionList=userManager.getPermission(permissionName2);            	
            	List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
            	if (agentFilterList.isEmpty()) {
            		dataSecuritySet = new DataSecuritySet();
            		dataSecurityFilter = new DataSecurityFilter();					
					dataSecurityFilter.setName(prefix);				
					dataSecurityFilter.setTableName("serviceorder");
					dataSecurityFilter.setFieldName("BOOKINGAGENTCODE".toLowerCase());
					dataSecurityFilter.setFilterValues(user.getParentAgent().toString());
					dataSecurityFilter.setCorpID(sessionCorpID);
					dataSecurityFilter.setCreatedOn(new Date());
					dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
					dataSecurityFilter.setUpdatedOn(new Date());
					dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
					dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
					dataSecuritySet.addFilters(dataSecurityFilter);
					dataSecuritySet.setName(permissionName2);
					dataSecuritySet.setDescription(permissionName2);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySet.setCreatedOn(new Date());
					dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
					dataSecuritySet.setUpdatedOn(new Date());
					dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
            	}
            	
            	user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName2).get(0));            	
            	String permissionJob="DATA_SECURITY_SET_SO_JOB_SALESPORTAL";
            	List permissionList12=userManager.getPermission(permissionJob);
            	if(permissionList12==null || permissionList12.isEmpty()){
            	dataSecuritySet = new DataSecuritySet();
            	String permissionFilter1="DATA_SECURITY_FILTER_SO_JOB_STO";
            	String permissionFilter2="DATA_SECURITY_FILTER_SO_JOB_INT";
            	List agentFilterList12 = dataSecurityFilterManager.getAgentFilterList(permissionFilter1, sessionCorpID);
            	if (agentFilterList12.isEmpty()) {
            		dataSecurityFilter = new DataSecurityFilter();					
					dataSecurityFilter.setName(permissionFilter1);				
					dataSecurityFilter.setTableName("serviceorder");
					dataSecurityFilter.setFieldName("JOB".toLowerCase());
					dataSecurityFilter.setFilterValues("sto");
					dataSecurityFilter.setCorpID(sessionCorpID);
					dataSecurityFilter.setCreatedOn(new Date());
					dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
					dataSecurityFilter.setUpdatedOn(new Date());
					dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
					dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
					dataSecuritySet.addFilters(dataSecurityFilter);
            	}else{
            		dataSecuritySet.addFilters((DataSecurityFilter)dataSecurityFilterManager.getAgentFilterList(permissionFilter1, sessionCorpID).get(0));
            	}
					List agentFilterList14 = dataSecurityFilterManager.getAgentFilterList(permissionFilter2, sessionCorpID);
					if (agentFilterList14.isEmpty()) { 
	            		dataSecurityFilter = new DataSecurityFilter();					
						dataSecurityFilter.setName(permissionFilter2);				
						dataSecurityFilter.setTableName("serviceorder");
						dataSecurityFilter.setFieldName("JOB".toLowerCase());
						dataSecurityFilter.setFilterValues("int");
						dataSecurityFilter.setCorpID(sessionCorpID);
						dataSecurityFilter.setCreatedOn(new Date());
						dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
						dataSecurityFilter.setUpdatedOn(new Date());
						dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
						dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
						dataSecuritySet.addFilters(dataSecurityFilter);
					}else{
						dataSecuritySet.addFilters((DataSecurityFilter)dataSecurityFilterManager.getAgentFilterList(permissionFilter2, sessionCorpID).get(0));
					}
					dataSecuritySet.setName(permissionJob);
					dataSecuritySet.setDescription(permissionJob);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySet.setCreatedOn(new Date());
					dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
					dataSecuritySet.setUpdatedOn(new Date());
					dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
					
            	}
            	
            	user.addPermissions((DataSecuritySet)userManager.getPermission(permissionJob).get(0));
            }else{
            	String permissionName2 = "DATA_SECURITY_SET_SO_SALESPORTAL_"+user.getUsername();            
            	String prefix = "DATA_SECURITY_FILTER_SO_SALESMAN_"+user.getUsername();
            	List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
            	if (agentFilterList.isEmpty()){            	
            		dataSecuritySet = new DataSecuritySet();            	
            		dataSecurityFilter = new DataSecurityFilter();					
					dataSecurityFilter.setName(prefix);				
					dataSecurityFilter.setTableName("serviceorder");
					dataSecurityFilter.setFieldName("salesMan");
					dataSecurityFilter.setFilterValues(user.getUsername());
					dataSecurityFilter.setCorpID(sessionCorpID);
					dataSecurityFilter.setCreatedOn(new Date());
					dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
					dataSecurityFilter.setUpdatedOn(new Date());
					dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
					dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
					dataSecuritySet.addFilters(dataSecurityFilter);					
					dataSecuritySet.setName(permissionName2);
					dataSecuritySet.setDescription(permissionName2);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySet.setCreatedOn(new Date());
					dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
					dataSecuritySet.setUpdatedOn(new Date());
					dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
            	}   
            	
            	user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName2).get(0));	
            }
            
            
           // List partnerExpirationDuration=userManager.getPartnerExpirationDate(sessionCorpID, user.getUsername());
        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));
        	Date currentDate = DateUtils.addDays(getCurrentDate(), 0);
        	try{
            	/*List dataDiff=userManager.findDateDiff(user.getPwdexpiryDate(), currentDate);
            	if(user.getPwdexpiryDate()==null || Long.parseLong(dataDiff.get(0).toString())<=0){
            		user.setPwdexpiryDate(compareWithDate);
            	}else{
            		user.setPwdexpiryDate(user.getPwdexpiryDate());
            	}*/
        		if(user.getPwdexpiryDate()==null || compareWithDate.after(user.getPwdexpiryDate())){
	        		user.setPwdexpiryDate(compareWithDate);
	        	}else{
	        		user.setPwdexpiryDate(user.getPwdexpiryDate());
	        	}
            }catch(Exception ex){
            		user.setPwdexpiryDate(compareWithDate);
            }       
        	
        }else if((user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT")) ){
        	String permissionName1 = "DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS";
            user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
            newPermissionsList.add((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
            // List partnerExpirationDuration=userManager.getPartnerExpirationDate(sessionCorpID, user.getUsername());
        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));
        	Date currentDate = DateUtils.addDays(getCurrentDate(), 0);
        	try{
            	/*List dataDiff=userManager.findDateDiff(user.getPwdexpiryDate(), currentDate);
            	if(user.getPwdexpiryDate()==null || Long.parseLong(dataDiff.get(0).toString())<=0){
            		user.setPwdexpiryDate(compareWithDate);
            	}else{
            		user.setPwdexpiryDate(user.getPwdexpiryDate());
            	}*/
        		if(user.getPwdexpiryDate()==null || compareWithDate.after(user.getPwdexpiryDate())){
	        		user.setPwdexpiryDate(compareWithDate);
	        	}else{
	        		user.setPwdexpiryDate(user.getPwdexpiryDate());
	        	}
            }catch(Exception ex){
            		user.setPwdexpiryDate(compareWithDate);
            }
        }else{
        	//List userExpirationDuration=userManager.userExpirationDuration(sessionCorpID);
        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getUserPasswordExpiryDuration().toString()));
        	Date currentDate = DateUtils.addDays(getCurrentDate(), 0);
        	try{
            	/*List dataDiff=userManager.findDateDiff(user.getPwdexpiryDate(), currentDate);
            	if(user.getPwdexpiryDate()==null || Long.parseLong(dataDiff.get(0).toString())<=0){
            		user.setPwdexpiryDate(compareWithDate);
            	}else{
            		user.setPwdexpiryDate(user.getPwdexpiryDate());
            	}*/
        		if(user.getPwdexpiryDate()==null || compareWithDate.after(user.getPwdexpiryDate())){
	        		user.setPwdexpiryDate(compareWithDate);
	        	}else{
	        		user.setPwdexpiryDate(user.getPwdexpiryDate());
	        	}
            }catch(Exception ex){
            		user.setPwdexpiryDate(compareWithDate);
            	}
        }

        try {
			if(user.getUserType().toString().trim().equalsIgnoreCase("AGENT")){
			  String permissionName1 = "DATA_SECURITY_SET_SO_JOB_Agent";
			   // user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
				List permissionList12=userManager.getPermission(permissionName1);
			  	if(permissionList12==null || permissionList12.isEmpty()){
			  		dataSecuritySet = new DataSecuritySet();	                	
			    Set<String> vanlineJob=partnerManager.getVanlineJobDetails();
			    for (String string : vanlineJob) {
			  	  String permissionFilter="DATA_SECURITY_FILTER_SO_JOB_"+string.trim();
			  	  List agentFilterList = dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID);
			  	  if (agentFilterList.isEmpty()) {
			  		dataSecurityFilter = new DataSecurityFilter();					
						dataSecurityFilter.setName(permissionFilter);				
						dataSecurityFilter.setTableName("serviceorder");
						dataSecurityFilter.setFieldName("JOB".toLowerCase());
						dataSecurityFilter.setFilterValues(string.trim());
						dataSecurityFilter.setCorpID(sessionCorpID);
						dataSecurityFilter.setCreatedOn(new Date());
						dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
						dataSecurityFilter.setUpdatedOn(new Date());
						dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
						dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
						dataSecuritySet.addFilters(dataSecurityFilter);
			  	  }else{	                		 
			  		 dataSecuritySet.addFilters((DataSecurityFilter)dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID).get(0));
			  	  }
				}
			      dataSecuritySet.setName(permissionName1);
					dataSecuritySet.setDescription(permissionName1);
					dataSecuritySet.setCorpID(sessionCorpID);
					dataSecuritySet.setCreatedOn(new Date());
					dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
					dataSecuritySet.setUpdatedOn(new Date());
					dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
					dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
					user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
					newPermissionsList.add ((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
			  	}else{
			  	 DataSecuritySet DataSecuritySet1=((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
			  	 Set<String> vanlineJob=partnerManager.getVanlineJobDetails();
			          for (String string : vanlineJob) {
			        	  String permissionFilter="DATA_SECURITY_FILTER_SO_JOB_"+string.trim();
			        	  List agentFilterList = dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID);
			        	 if (agentFilterList!=null && !agentFilterList.isEmpty()) {
			  	      DataSecuritySet1.addFilters((DataSecurityFilter)dataSecurityFilterManager.getAgentFilterList(permissionFilter, sessionCorpID).get(0));
			        	  }else{
			        		    dataSecurityFilter = new DataSecurityFilter();					
			  					dataSecurityFilter.setName(permissionFilter);				
			  					dataSecurityFilter.setTableName("serviceorder");
			  					dataSecurityFilter.setFieldName("JOB".toLowerCase());
			  					dataSecurityFilter.setFilterValues(string.trim());
			  					dataSecurityFilter.setCorpID(sessionCorpID);
			  					dataSecurityFilter.setCreatedOn(new Date());
			  					dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
			  					dataSecurityFilter.setUpdatedOn(new Date());
			  					dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
			  					dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
			  				  DataSecuritySet1.addFilters(dataSecurityFilter);
			        		  DataSecuritySet1.setName(permissionName1);
			        		  DataSecuritySet1.setDescription(permissionName1);
			        		  DataSecuritySet1.setCorpID(sessionCorpID);
			        		  DataSecuritySet1.setCreatedOn(new Date());
			        		  DataSecuritySet1.setCreatedBy(getRequest().getRemoteUser());
			        		  DataSecuritySet1.setUpdatedOn(new Date());
			        		  DataSecuritySet1.setUpdatedBy(getRequest().getRemoteUser());
							dataSecuritySetManager.saveDataSecuritySet(DataSecuritySet1);  
			        	  }		                	 
			          }
			          user.addPermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
			          newPermissionsList.add((DataSecuritySet)userManager.getPermission(permissionName1).get(0)); 
			  	}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}try{
        userJobTypeMap=new LinkedHashMap<String, String>() ;
        String[] job = getRequest().getParameterValues("userJobType");
        if(job!=null){
    	StringBuilder result = new StringBuilder();
    	if (job.length > 0) {
            result.append(job[0]);
            for (int i=1; i < job.length; i++) {
                result.append(",");
                result.append(job[i]);
            }
        }
    	user.setFunctionalArea(result.toString()); 
		String areaValue=user.getFunctionalArea();
		if(areaValue!=null && !(areaValue.equals(""))){
				String areaValue1[]=areaValue.split(","); 
				for(int i=0 ; i<areaValue1.length; i++){
					userJobTypeMap.put(areaValue1[i], areaValue1[i]);
				}
		      } 
		}else{
	    		user.setFunctionalArea(null);
		}
		}catch(NullPointerException Np){
			Np.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
        try{
        if(!(user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT")))
        {
        	String permissionName1 = "DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS";
            user.removePermissions((DataSecuritySet)userManager.getPermission(permissionName1).get(0));
           
        }}catch (Exception e) {
			// TODO: handle exception
		}
        if(file != null){
        if ( file.length() > 2097152) {
            addActionError(getText("maxLengthExceeded"));
            return INPUT;
        }
        
        String uploadDir =
            ServletActionContext.getServletContext().getRealPath("/images") +
            "/" ;
        
        String rightUploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "userData" + "/" + "images" + "/"); 
        
        File dirPath = new File(rightUploadDir);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
        
        if(file.exists()){
        InputStream stream = new FileInputStream(file);
        
        OutputStream bos = new FileOutputStream(rightUploadDir + fileFileName);
        int bytesRead = 0;
        byte[] buffer = new byte[8192];

        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
            bos.write(buffer, 0, bytesRead);
        }

        bos.close();  
        stream.close();
        
        getRequest().setAttribute("location", dirPath.getAbsolutePath()
                + Constants.FILE_SEP + fileFileName);
        user.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
        
        if(user.getLocation()!=null && !user.getLocation().equalsIgnoreCase("")){
	        try{
	        	BufferedImage originalImage = ImageIO.read(new File(user.getLocation()));
		        int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
		        BufferedImage resizeImageJpg = resizeImage(originalImage, type);
				ImageIO.write(resizeImageJpg, "jpg", new File(user.getLocation()));
	        }catch(Exception e){
        		e.printStackTrace();
        	}
        }
        }
        }
        
        if(signatureFile != null){
            if ( signatureFile.length() > 2097152) {
                addActionError(getText("maxLengthExceeded"));
                return INPUT;
            }
            
            String uploadDir =
                ServletActionContext.getServletContext().getRealPath("/images") +
                "/" ;
            
            String rightUploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "userData" + "/" + "images" + "/");
            File dirPath = new File(rightUploadDir);
            if (!dirPath.exists()) {
                dirPath.mkdirs();
            }
            
            if(signatureFile.exists()){
            InputStream stream = new FileInputStream(signatureFile);
            
            OutputStream bos = new FileOutputStream(rightUploadDir + signatureFileFileName);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];

            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }

            bos.close();  
            stream.close();
            
            getRequest().setAttribute("location", dirPath.getAbsolutePath()
                    + Constants.FILE_SEP + signatureFileFileName);
            user.setDigitalSignature(dirPath.getAbsolutePath() + Constants.FILE_SEP + signatureFileFileName);
            if(user.getDigitalSignature()!=null && !user.getDigitalSignature().equalsIgnoreCase("")){
            	try {
	    	        BufferedImage originalImage = ImageIO.read(new File(user.getDigitalSignature()));
	    	        int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
	    	        BufferedImage resizeImageJpg = resizeImage(originalImage, type);
	    			ImageIO.write(resizeImageJpg, "jpg", new File(user.getDigitalSignature()));
            	}catch(Exception e){
            		e.printStackTrace();
            	}
            }
            }
        }
        try {
        	user.setCorpID(sessionCorpID);
        	if(isNew)
            {
            	user.setCreatedOn(new Date());
            	if(user.getAlias().equalsIgnoreCase("")){
            	user.setAlias(user.getFirstName().substring(0, 1).toUpperCase() + " " + user.getLastName().toUpperCase());
            	}
            }
    	     /* else
    	        {
    	    	  user.setCreatedOn(oldCreatedOn);
    	        }*/
        	userManager.saveUser(user);
        	if((newRoleList!=null && !newRoleList.isEmpty()) && user!=null && user.getId()!=null){
        		String temp="";
        		for(Role r:newRoleList){
        			if(temp.equalsIgnoreCase("")){
        				temp="'"+r.getId()+"'";
        			}else{
        				temp=temp+",'"+r.getId()+"'";
        			}
        		}
        		userManager.updateUserRoleInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
        	} 
        	
        	if((newPermissionsList!=null && !newPermissionsList.isEmpty()) && user!=null && user.getId()!=null){
        		String temp="";
        		for(DataSecuritySet r:newPermissionsList){
        			if(temp.equalsIgnoreCase("")){
        				temp="'"+r.getId()+"'";
        			}else{
        				temp=temp+",'"+r.getId()+"'";
        			}
        		}
        		userManager.updateUserInfo(getRequest().getRemoteUser(),user.getId().toString(),temp);
        		//updateUserInfo
        	}        	
           } catch (UserExistsException e) {
            log.warn(e.getMessage());
            List<String> args = new ArrayList<String>();
            args.add(user.getUsername());
            args.add(user.getEmail());
            addActionError(getText("errors.existing.user", args));
            user.setVersion(originalVersion);
            user.setPassword(user.getConfirmPassword());
            return INPUT;
        }
        

        if (!"list".equals(from)) {
            saveMessage(getText("user.saved"));
            getComboList(sessionCorpID);
            ostates = customerFileManager.findDefaultStateList(user.getCountryHome(), sessionCorpID);
            return SUCCESS;
        } else {
            // add success messages
            List<String> args = new ArrayList<String>();
            args.add(user.getFullName());
            oldCreatedOn = user.getCreatedOn();
            if (isNew) {
                saveMessage(getText("user.added", args));
                // Send an account information e-mail
               /* mailMessage.setSubject(getText("signup.email.subject"));
                sendUserMessage(user, getText("newuser.email.message", args),
                                RequestUtil.getAppURL(getRequest()));*/
                getComboList(sessionCorpID);
                ostates = customerFileManager.findDefaultStateList(user.getCountryHome(), sessionCorpID);

                return SUCCESS;
            } else {
                saveMessage(getText("user.updated.byAdmin", args));
                getComboList(sessionCorpID);
                ostates = customerFileManager.findDefaultStateList(user.getCountryHome(), sessionCorpID);
                return INPUT;
            }
        }
    }

    private BufferedImage resizeImage(BufferedImage originalImage, int type){
    	int IMG_WIDTH = 100;
    	int IMG_HEIGHT = 100;
    	BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
    	Graphics2D g = resizedImage.createGraphics();
    	g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
    	g.dispose();

    	return resizedImage;
    }
    
    private java.util.Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
		}
		return currentDate;
		
	}
    private String gotoPageString;
	private List supervisorList;
	private Map<String, String> jobs;
	private static Map<String, String> defURL;
	private static Map<String, String> defSoURL;
	private static Map<String, String> defaultSortForJob;
	private static Map<String, String> defaultSortForCso;
	private static Map<String, String> defaultSortForTicket;
	private static Map<String, String> defaultSortForClaim;
	private static Map<String, String> sortOrderForJob;
	private static Map<String, String> sortOrderForCso;
	private static Map<String, String> sortOrderForTicket;
	private static Map<String, String> sortOrderForClaim;

	private String enableUser;

	public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }

    public String saveOnTabChange() throws Exception {
        String s = save(); 
        return gotoPageString;
    }
    
    
    public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public String getComboList(String corpID){
		companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID, "");
		PWD_HINT = refMasterManager.findByParameter(corpID, "PWD_HINT");
		RMDINTRVL = refMasterManager.findByParameter(corpID, "RMDINTRVL");
		ocountry = refMasterManager.findByParameter(corpID, "COUNTRY");
		defURL = refMasterManager.findByParameter(corpID, "DEF_URL");
		defSoURL = refMasterManager.findByParameter(corpID, "DEF_SO_TAB");
		defaultSortForJob = refMasterManager.findByParameter(corpID, "DEFAULTSORTFORJOB");
		defaultSortForCso = refMasterManager.findByParameter(corpID, "DEFAULTSORTFORCSO");
		defaultSortForTicket = refMasterManager.findByParameter(corpID, "DEFAULTSORTFORTICKET");
		defaultSortForClaim = refMasterManager.findByParameter(corpID, "DEFAULTSORTFORCLAIM");
		defaultSortforFileCabinet = refMasterManager.findByParameter(corpID, "DEFAULTSORTFORFILECABINET");
		sortOrderForJob = refMasterManager.findByParameter(corpID, "SORTORDERFORJOB");
		sortOrderForCso = refMasterManager.findByParameter(corpID, "SORTORDERFORCSO");
		sortOrderForTicket = refMasterManager.findByParameter(corpID, "SORTORDERFORTICKET");
		sortOrderForClaim = refMasterManager.findByParameter(corpID, "SORTORDERFORCLAIM");
		
		usertype = refMasterManager.findByParameter(corpID, "USER_TYPE");
		if(!corpID.equalsIgnoreCase("TSFT"))
		{
		usertype.remove("AGENT");
		}
		
		supervisorList=userManager.getSuperVisor(corpID);
		jobs = refMasterManager.findByParameter(corpID, "JOB");
		defaultWarehouse = refMasterManager.findByParameter(corpID,"HOUSE");
		moveTypeList =new LinkedHashMap<String, String>();
		moveTypeList.put("Quote", "Quote");
		moveTypeList.put("BookedMove", "Booked Move");
		sortOrderForFileCabinet= new ArrayList();
		sortOrderForFileCabinet.add("Ascending");
		sortOrderForFileCabinet.add("Descending");
		service=refMasterManager.findByParameter(corpID, "SERVICE","notrelo");
		opscalendar = refMasterManager.findByParameter(corpID, "OPSCALENDAR");
		hasDataSecuritySet = isDataSecuritySetAvailable();
		JobFunction=refMasterManager.findByParameters(corpID, "JOBFUNCTION");
		return SUCCESS;
	}
	
	/* Method added to check the availability of DATA SECURITY SET for the user */
	private Boolean isDataSecuritySetAvailable(){
		Boolean hasSecuritySet = false;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		Map filterMap=user.getFilterMap(); 
    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
    		hasSecuritySet = true;
    	}else{
			hasSecuritySet = false;
		}
		return hasSecuritySet;
	}
	
	 public static Map<String, String> getRMDINTRVL() {
		return RMDINTRVL;
	}
	 public static Map<String, String> getPWD_HINT() {
			return PWD_HINT;
		}
	 public  Map<String, String> getOcountry() {
			return ocountry;
		}
	 public static Map<String, String> getOstates() {
			return ostates;
		}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String list() { 
        roles = loginUser.getRoles();
        boolean adminAccess =false;
        if(roles!=null){
			for (Role role : roles){				
				if(role.getName().toString().equals("ROLE_ADMIN")){
					adminAccess=true;
				}
		}
			 
		} 
		userType="USER";
		enableUser="Y";
		if(adminAccess) {
        users = userManager.getUsers(new User());
		}else {
			if(loginUser.getParentAgent()!=null && (!(loginUser.getParentAgent().toString().trim().equals("")))) { 
				users = userManager.getUsersByParentAgent(loginUser.getParentAgent());	 
			}else { 
				users = userManager.getUsers(new User());	
			}
		}
        getComboList(sessionCorpID);
        return SUCCESS;
    }
	@SkipValidation
	public String userListBySearch() {
		roles = loginUser.getRoles();
        boolean adminAccess =false;
        if(roles!=null){
			for (Role role : roles){				
				if(role.getName().toString().equals("ROLE_ADMIN")){
					adminAccess=true;
				}
		}
			 
		}
        if(adminAccess) {     
	      users = refMasterManager.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser,"");
        }else {
			if(loginUser.getParentAgent()!=null && (!(loginUser.getParentAgent().toString().trim().equals("")))) {
				System.out.println("\n\n\n\n loginUser.getParentAgent()"+loginUser.getParentAgent());
				 users = refMasterManager.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser,loginUser.getParentAgent().toString().trim());
			}else {
				System.out.println("\n\n\n\n loginUser.getParentAgent()"+loginUser.getParentAgent());
				 users = refMasterManager.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser,"");	
			}
		}
	getComboList(sessionCorpID);
        return SUCCESS;
    }
    public List findUserRoles(){
    	if(category != null && !category.equals("")){
    		return availRoles = userManager.findUserRoles(sessionCorpID,category);
        }else{
        	return availRoles = userManager.findUserRoles(sessionCorpID);
        }
    }
    
    public void setFile(File file) {
        this.file = file;
    }
    public File getFile() {
        return file;
    }
    public void setFileFileName(String fileFileName) {
        this.fileFileName = fileFileName;
    }
    public String getFileFileName() {
        return fileFileName;
    }

	public List getAvailRoles() {
		return availRoles;
	}

	public void setAvailRoles(List availRoles) {
		this.availRoles = availRoles;
	}
	
    public String searchPassword() throws Exception {
		List password = null;
		password = userManager.findUserPassword(sessionCorpID);
    	return SUCCESS;
	   }
	
    private UserManager userManager;
	String passwordNew;
	String confirmPasswordNew;
	String newPwd;
	String newConPwd;
	String pwdHintQues;
	String pwdHint;
	boolean passreset;
	private String decorator;
	private String username;
	private String userType;
	private String firstName;
	private String lastName;
	private String email;
	private String supervisor;
	
	public String getNewConPwd() {
		return newConPwd;
	}

	public void setNewConPwd(String newConPwd) {
		this.newConPwd = newConPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUsers(List users) {
		this.users = users;
	}

	public String getConfirmPasswordNew() {
		return confirmPasswordNew;
	}

	public void setConfirmPasswordNew(String confirmPasswordNew) {
		this.confirmPasswordNew = confirmPasswordNew;
	}

	public String getPasswordNew() {
		return passwordNew;
	}

	public void setPasswordNew(String passwordNew) {
		this.passwordNew = passwordNew;
	}

	public boolean getPassreset() {
		return passreset;
	}

	public void setPassreset(boolean passreset) {
		this.passreset = passreset;
	}
	
	public String getPwdHint() {
		return pwdHint;
	}

	public void setPwdHint(String pwdHint) {
		this.pwdHint = pwdHint;
	}

	public String getPwdHintQues() {
		return pwdHintQues;
	}

	public void setPwdHintQues(String pwdHintQues) {
		this.pwdHintQues = pwdHintQues;
	}
	
	public String findUserDetails(){
		decorator = "pwdreset";
		//getSession().setAttribute(decorator, "pwdreset")
		ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		getComboList(sessionCorpID);
		users = userManager.findUserDetails(username);
		//user = userManager.findUserDetails(getRequest().getRemoteUser()).get(0);
		if(user.isPasswordReset()){
			//user.setPasswordReset(false);
			return SUCCESS;
		}else{
			return INPUT;
		}
	}
	
	public String resetPassword(){
		getComboList(sessionCorpID);
		String oldPassword = user.getPassword();
		Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);

        if ("true".equals(getRequest().getParameter("encryptPass")) && (encrypt != null && encrypt)) {
            String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);

            if (algorithm == null) { // should only happen for test case
                log.debug("assuming testcase, setting algorithm to 'SHA'");
                algorithm = "SHA";
            }

            newPwd = StringUtil.encodePassword(passwordNew, algorithm);
            newConPwd = newPwd;
        }
        users = userManager.findUserDetails(username);
       // user = userManager.findUserDetails(getRequest().getRemoteUser()).get(0);
		//if(user.getPassword().equals(oldPassword)){
        userManager.resetPassword(newPwd,newConPwd,pwdHintQues,pwdHint,getRequest().getRemoteUser());
			return SUCCESS;
		//}else{
		//	return ERROR;
		//}
		
	}
	public String findPwordHQ(){
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		//users = userManager.findUserDetails(username);
		userManager.findPwordHintQ(user.getPasswordHintQues(),getRequest().getRemoteUser());
		return SUCCESS;
		
	}
	public String forgotPassword(){
		 user = userManager.getUserByUsername(getRequest().getRemoteUser());
		 //user = userManager.getUserByUsername(user.getPasswordHintQues());
		userManager.forgotPassword(pwdHintQues,pwdHint,getRequest().getRemoteUser());
		return SUCCESS;
	}
		@SkipValidation 
	public String copyUser() throws IOException{
        commodits = refMasterManager.findByParameter(sessionCorpID, "COMMODIT");
        soDashboardViewList.put("soListView","Service Order List");
        soDashboardViewList.put("soDashboardView","Dashboard List");
        getComboList(sessionCorpID);
        ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
           
        addUser = userManager.getUser(cid);
		user = new User();   
		            try
            {
                  BeanUtilsBean beanUtilsUser = BeanUtilsBean.getInstance();
                  beanUtilsUser.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
                  beanUtilsUser.copyProperties(user, addUser);
                  
            }catch(Exception e){
             e.printStackTrace();
            }
           
            user.setFirstName("");
            user.setLastName("");
            user.setUsername("");
            user.setAlias("");
            user.setId(null);
            user.setEmail("");
            user.setPassword("");
            user.setLogin_status(null);
            user.setSignature(null);
            user.setLogin_time(null);
            user.setPricePointStartDate(null);
            user.setConfirmPassword("");
            user.setPasswordReset(true);
            user.setMobileNumber("");
            user.setCorpID(sessionCorpID);
            user.setCreatedOn(new Date());
            user.setUpdatedOn(new Date());
            user.setWorkStartTime("00:00");
            user.setWorkEndTime("00:00");
            user.setDigitalSignature(null);
             user.setLocation(null);
             user.setVersion(null);
             user.setSoListView("soDashboardView");
             user.setDefaultSortforFileCabinet(addUser.getDefaultSortforFileCabinet());
             if((addUser.getDefaultSortforFileCabinet()!=null)&&(!addUser.getDefaultSortforFileCabinet().equalsIgnoreCase("")))
	    	 {
				multiSortforFileCabinet = new ArrayList();
				String[] ac = user.getDefaultSortforFileCabinet().split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					multiSortforFileCabinet.add(accConJobType.trim());
					}
			}
             try{
             	user.setJobType(addUser.getJobType());
             	user.setJobFunction(addUser.getJobFunction());
             	userJobTypeMap=new LinkedHashMap<String, String>() ;	
            
      			String areaValue=addUser.getFunctionalArea();
      			if(areaValue!=null && !(areaValue.equals(""))){
      					String areaValue1[]=areaValue.split(","); 
      					for(int i=0 ; i<areaValue1.length; i++){
      						userJobTypeMap.put(areaValue1[i], areaValue1[i]);
      					}      					
      			      }     		
              }
              catch(NullPointerException ex){
              	ex.printStackTrace();
              }
            ostates = customerFileManager.findDefaultStateList(addUser.getCountryHome(), sessionCorpID);
            availRoles = userManager.findUserRoles(sessionCorpID,addUser.getUserType());      
            availPermissions=userManager.getPermissionList();
            getSession().setAttribute("availPermissions", availPermissions);
            getSession().setAttribute("availRoles", availRoles);
         return SUCCESS;
		
	}
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getDecorator() {
		return decorator;
	}
	public void setDecorator(String decorator) {
		this.decorator = decorator;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDefaultURL() {
		return defaultURL;
	}

	public void setDefaultURL(String defaultURL) {
		this.defaultURL = defaultURL;
	}

	public Map<String, String> getDefURL() {
		return defURL;
	}
	public Map<String, String> getDefSoURL() {
		return defSoURL;
	}
	public Map<String, String> getUsertype() {
		return usertype;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUsernm() {
		return usernm;
	}

	public void setUsernm(String usernm) {
		this.usernm = usernm;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the defaultSoURL
	 */
	public String getDefaultSoURL() {
		return defaultSoURL;
	}

	/**
	 * @param defaultSoURL the defaultSoURL to set
	 */
	public void setDefaultSoURL(String defaultSoURL) {
		this.defaultSoURL = defaultSoURL;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public List getAvailPermissions() {
		return availPermissions;
	}

	public void setAvailPermissions(List availPermissions) {
		this.availPermissions = availPermissions;
	}

	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}

	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}

	public void setDataSecuritySetManager(DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}

	public List getSupervisorList() {
		return (supervisorList!=null && !supervisorList.isEmpty())?supervisorList:userManager.getSuperVisor(sessionCorpID);
		
	}

	public void setSupervisorList(List supervisorList) {
		
		this.supervisorList = supervisorList;
	}

	public Map<String, String> getJobs() {
		return (jobs!=null && !jobs.isEmpty())?jobs:refMasterManager.findByParameter(sessionCorpID, "JOB");
	}

	public void setJobs(Map<String, String> jobs) {
		this.jobs = jobs;
	}

	/**
	 * @return the defaultSortForJob
	 */
	public static Map<String, String> getDefaultSortForJob() {
		return defaultSortForJob;
	}

	/**
	 * @return the defaultSortForClaim
	 */
	public static Map<String, String> getDefaultSortForClaim() {
		return defaultSortForClaim;
	}

	/**
	 * @return the defaultSortForCso
	 */
	public static Map<String, String> getDefaultSortForCso() {
		return defaultSortForCso;
	}

	/**
	 * @return the defaultSortForTicket
	 */
	public static Map<String, String> getDefaultSortForTicket() {
		return defaultSortForTicket;
	}

	/**
	 * @return the sortOrderForClaim
	 */
	public static Map<String, String> getSortOrderForClaim() {
		return sortOrderForClaim;
	}

	/**
	 * @return the sortOrderForCso
	 */
	public static Map<String, String> getSortOrderForCso() {
		return sortOrderForCso;
	}

	/**
	 * @return the sortOrderForJob
	 */
	public static Map<String, String> getSortOrderForJob() {
		return sortOrderForJob;
	}

	/**
	 * @return the sortOrderForTicket
	 */
	public static Map<String, String> getSortOrderForTicket() {
		return sortOrderForTicket;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public static void setOcountry(Map<String, String> ocountry) {
		UserAction.ocountry = ocountry;
	}

	public static Map<String, String> getDefaultWarehouse() {
		return defaultWarehouse;
	}

	public static void setDefaultWarehouse(Map<String, String> defaultWarehouse) {
		UserAction.defaultWarehouse = defaultWarehouse;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getEnableUser() {
		return enableUser;
	}

	public void setEnableUser(String enableUser) {
		this.enableUser = enableUser;
	}

	public List getGender() {
		return gender;
	}

	public void setGender(List gender) {
		this.gender = gender;
	}

	public File getSignatureFile() {
		return signatureFile;
	}

	public void setSignatureFile(File signatureFile) {
		this.signatureFile = signatureFile;
	}

	public String getSignatureFileFileName() {
		return signatureFileFileName;
	}

	public void setSignatureFileFileName(String signatureFileFileName) {
		this.signatureFileFileName = signatureFileFileName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List getLeftList() {
		return leftList;
	}

	public void setLeftList(List leftList) {
		this.leftList = leftList;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}

	public String getUserAccountSection() {
		return userAccountSection;
	}

	public void setUserAccountSection(String userAccountSection) {
		this.userAccountSection = userAccountSection;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public Boolean isMobile_serv_access() {
		return mobile_serv_access;
	}

	public void setMobile_serv_access(Boolean mobile_serv_access) {
		this.mobile_serv_access = mobile_serv_access;
	}

	public String getTableFieldName() {
		return tableFieldName;
	}

	public void setTableFieldName(String tableFieldName) {
		this.tableFieldName = tableFieldName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List getTableNameList() {
		return tableNameList;
	}

	public void setTableNameList(List tableNameList) {
		this.tableNameList = tableNameList;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public Map<String, String> getMoveTypeList() {
		return moveTypeList;
	}

	public void setMoveTypeList(Map<String, String> moveTypeList) {
		this.moveTypeList = moveTypeList;
	}

	public boolean isAccessQuotationFromCustomerFileFlag() {
		return accessQuotationFromCustomerFileFlag;
	}

	public void setAccessQuotationFromCustomerFileFlag(
			boolean accessQuotationFromCustomerFileFlag) {
		this.accessQuotationFromCustomerFileFlag = accessQuotationFromCustomerFileFlag;
	}
	public Map<String, String> getDefaultSortforFileCabinet() {
		return defaultSortforFileCabinet;
	}

	public void setDefaultSortforFileCabinet(
			Map<String, String> defaultSortforFileCabinet) {
		this.defaultSortforFileCabinet = defaultSortforFileCabinet;
	}

	public List getMultiSortforFileCabinet() {
		return multiSortforFileCabinet;
	}

	public void setMultiSortforFileCabinet(List multiSortforFileCabinet) {
		this.multiSortforFileCabinet = multiSortforFileCabinet;
	}

	public List getSortOrderForFileCabinet() {
		return sortOrderForFileCabinet;
	}

	public void setSortOrderForFileCabinet(List sortOrderForFileCabinet) {
		this.sortOrderForFileCabinet = sortOrderForFileCabinet;
	}

	public Map<String, String> getSoDashboardViewList() {
		return soDashboardViewList;
	}

	public void setSoDashboardViewList(Map<String, String> soDashboardViewList) {
		this.soDashboardViewList = soDashboardViewList;
	}

	public void setDataSecurityFilterManager(
			DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public Map<String, String> getCommodits() {
		return commodits;
	}

	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}

	public Map<String, String> getService() {
		return service;
	}

	public void setService(Map<String, String> service) {
		this.service = service;
	}

	public static Map<String, String> getOpscalendar() {
		return opscalendar;
	}

	public static void setOpscalendar(Map<String, String> opscalendar) {
		UserAction.opscalendar = opscalendar;
	}

	public Boolean getHasDataSecuritySet() {
		return hasDataSecuritySet;
	}

	public void setHasDataSecuritySet(Boolean hasDataSecuritySet) {
		this.hasDataSecuritySet = hasDataSecuritySet;
	}

	public List getRecordLimitList() {
		return recordLimitList;
	}

	public void setRecordLimitList(List recordLimitList) {
		this.recordLimitList = recordLimitList;
	}

	public List getJobFunction() {
		return JobFunction;
	}

	public void setJobFunction(List jobFunction) {
		JobFunction = jobFunction;
	}

	public Map<String, String> getUserJobTypeMap() {
		return userJobTypeMap;
	}

	public void setUserJobTypeMap(Map<String, String> userJobTypeMap) {
		this.userJobTypeMap = userJobTypeMap;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Date getOldCreatedOn() {
		return oldCreatedOn;
	}

	public void setOldCreatedOn(Date oldCreatedOn) {
		this.oldCreatedOn = oldCreatedOn;
	}

	public User getAddUser() {
		return addUser;
	}

	public void setAddUser(User addUser) {
		this.addUser = addUser;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public boolean isAdminAccess() {
		return adminAccess;
	}

	public void setAdminAccess(boolean adminAccess) {
		this.adminAccess = adminAccess;
	}
  
}  

