package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.TaskDetailDao;
import com.trilasoft.app.model.TaskDetail;
import com.trilasoft.app.webapp.action.CommonUtil;

public class TaskDetailDaoHibernate extends GenericDaoHibernate<TaskDetail, Long> implements TaskDetailDao{

	public TaskDetailDaoHibernate()
	{
		super(TaskDetail.class);
	}

public void saveTaskDetails(Long id, String fieldName, String fieldValue, String sessionCorpID, String selectedWO,String userType){
		/*if(fieldName.equalsIgnoreCase("date")){
		   if (fieldValue != null) {	
			   String statusDateNew = null;
				try {
					CommonUtil.multiParse(fieldValue);
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(CommonUtil.multiParse(fieldValue)));
					statusDateNew = nowYYYYMMDD1.toString();
			 	}catch (Exception e) {
					e.printStackTrace();
					logger.error("Error executing query"+ e);	
				}
		getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET "+fieldName+" = '"+statusDateNew+"' WHERE id= '"+id+"' AND corpId = '"+sessionCorpID+"' ");
	     }} else */
			if(fieldName.equalsIgnoreCase("status")){
	    		 if(fieldValue.equalsIgnoreCase("Transfer") && (selectedWO == null || selectedWO.isEmpty()) ){
	    			 getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET "+fieldName+" = '"+fieldValue+"', workorder='', updatedBy='"+userType+"',updatedOn=now() WHERE id= '"+id+"' AND corpId = '"+sessionCorpID+"' ");		 
	    		 }else if(fieldValue.equalsIgnoreCase("Closed")){
	    			 getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET "+fieldName+" = '"+fieldValue+"', updatedBy='"+userType+"', time=DATE_FORMAT( NOW() ,  '%H:%i' ),Date=now(),updatedOn=now()   WHERE id= '"+id+"' AND corpId = '"+sessionCorpID+"' ");	 
	    		 }else{
	    			 getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET "+fieldName+" = '"+fieldValue+"',updatedBy='"+userType+"',time='',Date=null,updatedOn=now() WHERE id= '"+id+"' AND corpId = '"+sessionCorpID+"' "); 
	    		 }
	    	 } else if(fieldName.equalsIgnoreCase("workorder") ){
	    		 getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET "+fieldName+" = '"+fieldValue+"', updatedBy='"+userType+"',  time=DATE_FORMAT( NOW() ,  '%H:%i' ),Date=now(),updatedOn=now()  WHERE id= '"+id+"' AND corpId = '"+sessionCorpID+"' "); 
	    	 } else{
	    		 getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET "+fieldName+" = '"+StringEscapeUtils.escapeSql(fieldValue)+"',updatedBy='"+userType+"',updatedOn=now() WHERE id= '"+id+"' AND corpId = '"+sessionCorpID+"' ");
	    	 }
		}
	
       public List getTaskFromWorkOrder(String shipNumber, String selectedWO){
			 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' AND workorder='"+selectedWO+"' AND type <> 'T' AND (ticket is not null AND ticket <> '') group by ticket"); 
		 }	
       
       public String getValueFromTaskDetails(Long id, String shipNumber){
    	   String taskDetailsValue="";
    	   String query ="SELECT details,comment,category FROM taskdetails where id='" + id + "' AND shipNumber='" + shipNumber + "'";
    	   List al = getSession().createSQLQuery(query).list();
    	   if(al!=null && !al.isEmpty() && al.get(0)!=null){
    		 Iterator itr = al.iterator();
    		 while(itr.hasNext()){
				 Object obj[]=(Object[])itr.next();
				 if(obj[0] != null){
					 taskDetailsValue=obj[0].toString();
				 } if(obj[1] != null){
					 taskDetailsValue=taskDetailsValue +"~"+obj[1].toString();
				 } else{
					 taskDetailsValue=taskDetailsValue +"~"+"";
				 }
					 if(obj[2] != null){
					 taskDetailsValue=taskDetailsValue +"~"+obj[2].toString();
				 }
					 
    		 		}
    	   }
    	   return taskDetailsValue;
       }
	
       public void updateTransferField(Long id, String shipNumber){
    	   getHibernateTemplate().bulkUpdate("UPDATE TaskDetail SET transfer = 'Transferred' WHERE id= '"+id+"' AND shipNumber = '"+shipNumber+"' ");
       }
}
