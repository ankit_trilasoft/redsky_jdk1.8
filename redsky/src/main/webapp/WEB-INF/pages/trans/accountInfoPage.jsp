<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>
		<title><fmt:message key="accountInfoDetail.title"/></title> 
   		<meta name="heading" content="<fmt:message key='accountInfoDetail.heading'/>"/>  
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>   		
<style>

.listwhitetext2 {
background-color:#FFFFFF;
color:#444444;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
padding-bottom:5px;
}

</style>
</head>
<s:form id="accountProfileForm" action="saveAccountInfo" method="post" validate="true"> 
	<s:hidden name="popupval" value="${papam.popup}"/>
	<s:hidden id="countAccountProfileNotes" name="countAccountProfileNotes" value="<%=request.getParameter("countAccountProfileNotes") %>"/>
	<c:set var="countAccountProfileNotes" value="<%=request.getParameter("countAccountProfileNotes") %>"/>

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
    <s:hidden name="accountProfile.id" />
    <s:hidden name="accountProfile.corpID" />
    <s:hidden name="accountProfile.partnerCode" />
    <s:hidden name="partner.id" />
    <s:hidden name="partner.corpID" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	

<c:if test="${validateFormNav == 'OK'}">
<c:choose>
	<c:when test="${gotoPageString == 'gototab.accountlist' }">
		<c:if test="${paramValue == 'View'}">
			<c:redirect url="/searchPartnerView.html"/>
		</c:if>
		<c:if test="${paramValue != 'View'}">
			<c:redirect url="/searchPartnerAdmin.html?partnerType=${partnerType}"/>
		</c:if>
	</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerAddForm.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountcontact' }">
	<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=${partnerType}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.contractpolicy' }">
	<c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="layer4" style="100%">
<div id="newmnav">
				<ul>
				  <c:if test="${param.popup}" >
				    <%-- <li><a href='${empty param.popup?"partners.html":"javascript:history.go(-4)"}' ><span>List</span></a></li>  --%>
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account List</span></a></li>
			  	    <li><a href="editPartnerAddFormPopup.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Detail</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  		<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Contact</span></a></li>
			  		<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
			  		
			  	  </c:if>
			  	  <c:if test="${empty param.popup}" >
				    <li><a onclick="setReturnString('gototab.accountdetail');return checkdate('none');"><span>Account Detail</span></a></li>
			  		<!--<li><a href="editPartnerAddForm.html?id=${partner.id}&partnerType=AC"><span>Account Detail</span></a></li>
			  		-->
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		
			  		<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Profile</span></a></li>
			  		<li><a onclick="setReturnString('gototab.accountcontact');return checkdate('none');"><span>Account Contact</span></a></li>
			  		<!--<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
			  		--><li><a onclick="setReturnString('gototab.contractpolicy');return checkdate('none');"><span>Policy</span></a></li>
			  		<!--<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
			  		--><li><a onclick="setReturnString('gototab.accountlist');return checkdate('none');"><span>Partner List</span></a></li>
			  		<!--<li><a href="searchPartner.html?partnerType=AC"><span>Account List</span></a></li>
			  	  -->
			  	  <c:if test="${not empty accountProfile.id}">
			  	  <li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
			  	  </c:if>
			  	  </c:if>
			  	</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
<tbody>
  <tr>
  	<td>
  	
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="">
		  <tbody>  	
		  	<tr>
				<td height="5px"></td>
				</tr>
				<tr>			
				<td align="right" class="listwhitetext" width="">Name<font color="red" size="2">*</font></td>
				<s:hidden key="partner.firstName"/>
				<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text"
				size="25" maxlength="80"  onkeydown="return onlyCharsAllowed(event)"readonly="true"  /> </td>
				<td align="right" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
				<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
				required="true" cssClass="input-textUpper" maxlength="8" size="25" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /> </td>
			<td></td>
				<td></td>
			</tr>
			<tr>
			
				<td align="right" class="listwhitetext" width="150px"><fmt:message key='accountInfo.authorized'/></td>	
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.creditAuthorizedBy" size="25" maxlength="15" tabindex="1"/></td>	
				
			<td></td>
				<td></td>
			</tr>				
			<sec-auth:authComponent componentId="module.partner.section.storageBillingGroup.edit">
			<tr>				
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.storage'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="partner.storageBillingGroup" list="%{billgrp}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3"/></td>
				<td align="right" class="listwhitetext" width="100px"><fmt:message key='accountInfo.nationalAccount'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.validNationalCode" size="25" maxlength="15" tabindex="4"/></td>
				<td></td>
				<td></td>	
			</tr>
			</sec-auth:authComponent>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.abbreviation'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.abbreviation" size="25" maxlength="15" tabindex="5"/></td>
				
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.accountHolder'/></td>
				<td class="listwhitetext" colspan="2">
				<s:select cssClass="list-menu"  name="partner.accountHolder" list="%{sale}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="7"/>
				
				<!--<s:textfield cssClass="input-text"  name="partner.accountHolder" cssStyle="width:160px" maxlength="15"/></td>
			-->
			<td></td>
				<td></td>
			</tr>
			
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.paymentMethod'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="partner.paymentMethod" list="%{paytype}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="8"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.payOption'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="partner.payOption" list="%{payopt}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="9"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.billingInstruction'/></td>
				<td class="listwhitetext" colspan="5"><s:select cssClass="list-menu"  name="partner.billingInstruction" list="%{billinst}" cssStyle="width:422px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
				<!--<td align="right" class="listwhitetext"></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.billingInstructionMessage" cssStyle="width:160px" maxlength="15"/></td>
			-->
			</tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="" >
			<tr>				
				<c:set var="ischecked" value="false"/>						
				<c:set var="ischecked" value="true"/>
				<td align="right" class="listwhitetext" width="155"><fmt:message key='accountInfo.multiAuthorization'/></td>
				<td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partner.multiAuthorization" fieldValue="true" onchange="changeStatus();" tabindex="11"/></td>
			
				<c:set var="ischeckedddd" value="false"/>
				<c:if test="${partner.payableUploadCheck}">
				<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" class="listwhitetext" width="352">Don't send to payable upload</td>
				<td  class="listwhitetext" width="" ><s:checkbox key="partner.payableUploadCheck" onclick="changeStatus();"  value="${ischeckedddd}" fieldValue="true" tabindex="18"/></td>
			
				<c:set var="ischeckeddddd" value="false"/>
				<c:if test="${partner.invoiceUploadCheck}">
				<c:set var="ischeckeddddd" value="true"/>
				</c:if>
						
				<td align="right" class="listwhitetext" width="220">Don't send to invoice upload</td>
				<td  class="listwhitetext" width="86px" align="left" ><s:checkbox key="partner.invoiceUploadCheck" onclick="changeStatus();"  value="${ischeckeddddd}" fieldValue="true" tabindex="18"/></td>
			</tr>
			<tr>
			<td></td>
			<td></td>
			<td align="right" class="listwhitetext" width="">Don't generate unauthorized invoice</td>
		    <td  class="listwhitetext" width="" ><s:checkbox key="partner.stopNotAuthorizedInvoices" onclick="changeStatus();"  value="${partner.stopNotAuthorizedInvoices}" fieldValue="true" tabindex="18"/></td>
			<td align="right" class="listwhitetext" width="">Don't copy auth # from previous S/O#</td>
		    <td  class="listwhitetext" width="" ><s:checkbox key="partner.doNotCopyAuthorizationSO" onclick="changeStatus();"  value="${partner.doNotCopyAuthorizationSO}" fieldValue="true" tabindex="18"/></td>
			</tr>
			
			<tr>
			<td colspan="6" width="100%">
			<table width="">
			<tr>
										<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
										<td align="left" class="listwhitetext">Pricing</td>
										
										<td align="left" class="listwhitetext">Billing</td>
										
										<td align="left" class="listwhitetext">Payable</td>
								</tr>
							<tr>
									<td align="right" class="listwhitetext" style="width:155px">Account setup</td>
									<td style="width:222px"><s:select name="partner.pricingUser" cssClass="list-menu" list="%{pricing}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
									
									<td style="width:237px"><s:select name="partner.billingUser" cssClass="list-menu" list="%{billing}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
									
									<td colspan="2"><s:select cssClass="list-menu" name="partner.payableUser" list="%{payable}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
									<td align="left" class="listwhitetext" style="width:10px"></td>
							
							
							</tr>
							</table>
							</td>
							</tr>
							<tr>
							<td colspan="8"  align="left">
						<table>
						<tr>			
				<td align="right" style="width:155px" class="listwhitetext"><fmt:message key='accountInfo.creditTerms'/></td>
				<td class="listwhitetext" width="" ><s:select cssClass="list-menu"  name="partner.creditTerms" list="%{creditTerms}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
				</tr>
				</table>
				</td>
			</tr>
							
			</table>
			<table style="margin-bottom: -1px" width="100%">
			
			  <tr>
			    <td height="1px"></td>
			     </tr>
					<tr>
					  <td align="center" colspan="10" class="vertlinedata"></td>
					  </tr>
					<tr>
						<td height="10px"></td>
						</tr>
						<tr>
							<td  colspan="5"  style="width:100%">
								<table style="width:100%"  cellspacing="0" cellpadding="0" border="0">	
								<tr>
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isPrivateParty}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="right" class="listwhitetext" width="100px">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
						
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isAccount}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="right" class="listwhitetext" width="100px"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
						
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isAgent}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="right" class="listwhitetext" width="100px"><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
													
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isVendor}">
						<c:set var="ischecked" value="true"/>
						</c:if>					
						<td align="center" class="listwhitetext" width="100px" style="padding-left:20px"><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
														
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isCarrier}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext" width="100px"><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
								
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isOwnerOp}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext" width="100px"><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
					</tr>
				</table>
				</td>
			</tr>
			</table>		
		</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
<table style="width:750px">
					<tr>
					
					<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partner.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partner.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td width="120px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partner.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partner.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td width="120px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty partner.id}">
							<s:hidden name="partner.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
							<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
					
					</tr>
					

</table>
<c:if test="${empty param.popup}" >

	<s:submit cssClass="cssbutton"  key="button.save" cssStyle="width:70px; height:25px " tabindex="18"/>
	<s:reset cssClass="cssbutton" key="Reset" cssStyle="width:70px; height:25px " tabindex="19"/>   

</c:if>

<s:hidden name="partner.partnerPortalActive" />
<s:hidden name="partner.associatedAgents" />
<s:hidden name="partner.viewChild" />
<s:hidden name="partner.typeOfVendor"/>
<s:hidden name="partner.remarks1"  />
<s:hidden name="partner.partnerSuffix"/>
<s:hidden name="partner.partnerPrefix"/>
<s:hidden name="partner.middleInitial"/>
<s:hidden name="partner.mailingAddress1"/>
<s:hidden name="partner.mailingCity"/>
<s:hidden name="partner.mailingFax"/>
<s:hidden name="partner.mailingAddress2"/>
<s:hidden name="partner.mailingState" />
<s:hidden name="partner.mailingPhone"/>
<s:hidden name="partner.mailingAddress3"/>
<s:hidden name="partner.mailingZip"/>
<s:hidden name="partner.mailingTelex"/>
<s:hidden name="partner.mailingAddress4"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.mailingCountry"/>
<s:hidden name="partner.mailingEmail" />
<s:hidden name="partner.terminalAddress1"/>
<s:hidden name="partner.terminalCity"/>
<s:hidden name="partner.terminalFax"/>
<s:hidden name="partner.terminalAddress2"/>
<s:hidden name="partner.terminalState" />
<s:hidden name="partner.terminalPhone"/>
<s:hidden name="partner.terminalAddress3"/>
<s:hidden name="partner.terminalZip"/>
<s:hidden name="partner.terminalTelex"/>
<s:hidden name="partner.terminalAddress4"/>
<s:hidden name="partner.terminalCountryCode"/>
<s:hidden name="partner.terminalCountry"/>
<s:hidden name="partner.terminalEmail" />
<s:hidden name="partner.billingAddress1"/>
<s:hidden name="partner.billingCity"/>
<s:hidden name="partner.billingFax"/>
<s:hidden name="partner.billingAddress2"/>
<s:hidden name="partner.billingState" />
<s:hidden name="partner.billingPhone"/>
<s:hidden name="partner.billingAddress3"/>
<s:hidden name="partner.billingZip"/>
<s:hidden name="partner.billingTelex"/>
<s:hidden name="partner.billingAddress4"/>
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.billingCountry"/>
<s:hidden name="partner.billingEmail" />

<s:hidden name="partner.status"/>
<s:hidden name="partner.rank"/>
<s:hidden name="partner.billPayType"/>
<s:hidden name="partner.billPayOption"/>

<s:hidden name="partner.warehouse"/>

<s:hidden name="partner.commitionType"/>
<s:hidden name="partner.coordinator"/>
<s:hidden name="partner.billToGroup"/>
<s:hidden name="partner.client4"/>
<s:hidden name="partner.client5"/>
<s:hidden name="partner.client6"/>
<s:hidden name="partner.client7"/>
<s:hidden name="partner.client8"/>
<s:hidden name="partner.client9"/>
<s:hidden name="partner.client10"/>
<s:hidden name="partner.companyDivision"/>

<c:if test="${not empty partner.effectiveDate}">
		<s:text id="effectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.effectiveDate"/></s:text>
		<td><s:hidden cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{effectiveDateFormattedValue}" /></td>
</c:if>


<s:hidden name="partner.client3"/>
<s:hidden name="partner.agentParent"/>



<s:hidden name="partner.sea"/>
<s:hidden name="partner.surface"/>
<s:hidden name="partner.air"/>

<s:hidden name="partner.driverAgency"/>
<s:hidden name="partner.longPercentage"/>

<s:hidden name="partner.client1"/>
<s:hidden name="partner.salesMan"/>
<s:hidden name="partner.latitude" />
<s:hidden name="partner.longitude" />	
<s:hidden name="partner.location1" />
<s:hidden name="partner.location2" />
<s:hidden name="partner.location3" />
<s:hidden name="partner.acctDefaultJobType" />
<s:hidden name="partner.accountingDefault" />
<s:hidden name="partner.location4" />
<s:hidden name="partner.url" />

<s:hidden name="partner.companyProfile" />
<s:hidden name="partner.yearEstablished" />
<s:hidden name="partner.companyFacilities" />
<s:hidden name="partner.companyCapabilities" />
<s:hidden name="partner.companyDestiantionProfile" />
<s:hidden name="partner.serviceRangeKms" />
<s:hidden name="partner.serviceRangeMiles" />
<s:hidden name="partner.fidiNumber" />
<s:hidden name="partner.OMNINumber" />
<s:hidden name="partner.IAMNumber" />
<s:hidden name="partner.AMSANumber" />
<s:hidden name="partner.WERCNumber" />
<s:hidden name="partner.facilitySizeSQFT" />
<s:hidden name="partner.facilitySizeSQMT" />
<s:hidden name="partner.qualityCertifications" />
<s:hidden name="partner.vanLineAffiliation" />
<s:hidden name="partner.serviceLines" />

</s:form>
<%-- Script Shifted from Top to Botton on 06-Sep-2012 By Kunal --%>
<script>
	function notExists(){
		alert("The Account information has not been saved yet, please save account information to continue");
	}

	function openNotesPopup(targetElement){
		var id = document.forms['accountProfileForm'].elements['accountProfile.id'].value;
		var notesId = document.forms['accountProfileForm'].elements['accountProfile.partnerCode'].value;
		var noteSubType = document.forms['accountProfileForm'].elements['accountProfile.type'].value;
		var imgID = targetElement.id;
		var pId = document.forms['accountProfileForm'].elements['partner.id'].value;
		openWindow('accountNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AP&noteFor=AccountProfile&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fieldId=countAccountProfileNotes&decorator=popup&popup=true',740,400);
		document.forms['accountProfileForm'].elements['formStatus'].value = '';
	}

	function openNotesPopupTab(targetElement){
		var id = document.forms['accountProfileForm'].elements['accountProfile.id'].value;
		var notesId = document.forms['accountProfileForm'].elements['accountProfile.partnerCode'].value;
		var noteSubType = document.forms['accountProfileForm'].elements['accountProfile.type'].value;
		var imgID = targetElement.id;
		var pId = document.forms['accountProfileForm'].elements['partner.id'].value;
		openWindow('accountNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AP&noteFor=AccountProfile&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fromTab=yes&fieldId=countAccountProfileNotes&decorator=popup&popup=true',740,400);
		document.forms['accountProfileForm'].elements['formStatus'].value = '';
	}
</script>

<script>

	function checkdate(clickType){
		var j;
		var elementLength = document.forms['accountProfileForm'].elements.length;
		for(j=0;j<=(elementLength-1);j++){
			document.forms['accountProfileForm'].elements[j].disabled = false;
		}
		progressBarAutoSave('1');
		if ('${autoSavePrompt}' == 'No'){
				var noSaveAction = '<c:out value="${'accountProfile.id'}"/>';
	      		var id =document.forms['accountProfileForm'].elements['accountProfile.id'].value;
	      		var id1 = document.forms['accountProfileForm'].elements['partner.id'].value;
				var partnerType = document.forms['accountProfileForm'].elements['partnerType'].value;
		      	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
			        noSaveAction = 'editPartnerAddForm.html?id='+id1+'&partnerType=AC';
          		}
		      	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					noSaveAction = 'accountContactList.html?id='+id1+'&partnerType= AC';
				 }
				 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					noSaveAction = 'editContractPolicy.html?id='+id1+'&partnerType= AC';
				 }
				 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
			 		if('<%=session.getAttribute("paramView")%>' == 'View'){
						noSaveAction = 'searchPartnerView.html';
					}else{
						noSaveAction = 'searchPartnerAdmin.html?partnerType=AC';
					}
				}           
		    processAutoSave(document.forms['accountProfileForm'], 'saveAccountProfile!saveOnTabChange.html', noSaveAction);
			document.forms['accountProfileForm'].action = 'saveAccountProfile!saveOnTabChange.html';
		} else {
		   if(!(clickType == 'save')){
		     var id1 =document.forms['accountProfileForm'].elements['partner.id'].value;
		     if (document.forms['accountProfileForm'].elements['formStatus'].value == '1')
		     {
		       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='billingDetail.heading'/>");
		       if(agree)
		        {
		           document.forms['accountProfileForm'].action ='saveAccountProfile!saveOnTabChange.html';
		           document.forms['accountProfileForm'].submit();
		        }  else {
		           if(id1 != '')  {
		             if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
			      			location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType=AC';
	     				 }
				     if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
							location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
					 }
					 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
							location.href = 'editContractPolicy.html?id='+id1+'&partnerType=AC';
					 }
					 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
			 			if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'searchPartnerView.html';
						}else{
							location.href = 'searchPartnerAdmin.html?partnerType=AC';
						}
				     } 
		         }
     	 	 }
  		 }else{
		     if(id1 != '')  {
		        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				      location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType=AC';
		           		}
		        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
						location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
						}
			 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
						location.href= 'editContractPolicy.html?id='+id1+'&partnerType=AC';
						}
			 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'searchPartnerAdmin.html?partnerType=AC';
					}
				} 
	      	}
  		 }
  	}
  }
}

function changeStatus(){
	document.forms['accountProfileForm'].elements['formStatus'].value = '1';
}

</script>
<%-- Shifting Closed Here --%>
<script type="text/javascript">
try{
	var param = '<%=session.getAttribute("paramView")%>';
	if(param == 'View'){
		var i;
		var elementLength1 = document.forms['accountProfileForm'].elements.length;
		for(i=0;i<=(elementLength1-1);i++){
			document.forms['accountProfileForm'].elements[i].disabled = true;
		}
	}
}
catch(e){}
</script>