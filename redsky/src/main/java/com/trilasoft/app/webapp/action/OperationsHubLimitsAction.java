package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import com.trilasoft.app.dao.hibernate.OperationsHubLimitsDaoHibernate.HubLimitDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.OperationsHubLimitsManager;
import com.trilasoft.app.service.RefMasterManager;

public class OperationsHubLimitsAction extends BaseAction {

	private List hubLimits;

	private OperationsHubLimits operationsHubLimits;
	
	private OperationsHubLimitsManager operationsHubLimitsManager;

	private Long id;

	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;
	
	private String hitFlag;
	
	private Map<String, String> hub;
	
	private String hubID;
	
	private RefMasterManager refMasterManager;
	private Map<String, String> house;
	private CustomerFileManager customerFileManager;
	private String operationChk;
	private String hubWarehouseLimit;
	private List serviceType;
	String typeService="";

	
	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}

	public OperationsHubLimitsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	private List<SystemDefault> sysDefaultDetail;
	private Boolean opsMgmtWarehouseCheck=false;
	public String edit() {
		String opsPermKey = sessionCorpID +"-"+"component.field.opsMgmt.warehouse";
		opsMgmtWarehouseCheck=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(opsPermKey) ;
		
		if(!opsMgmtWarehouseCheck){
		serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
	    Iterator iter  =serviceType.iterator();
			while (iter.hasNext()) {
				if (typeService.equals("")) {
					typeService = iter.next().toString();
				} else {
					typeService = typeService + "," + iter.next().toString();
				}
			}
		}
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub);
		distinctHubDescription.putAll(house);
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
	            	operationChk ="H";
	            	hubLimits = operationsHubLimitsManager.findListByHub(sessionCorpID);
	            }else{
	            	operationChk ="W";
	            	hubLimits = operationsHubLimitsManager.getAllRecord(sessionCorpID);	
	            }
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					hubWarehouseLimit = "Crw";
				}else{
					hubWarehouseLimit = "Tkt";
				}
			}
		}
		if (id != null) {
			operationsHubLimits = operationsHubLimitsManager.get(id);
			if(opsMgmtWarehouseCheck){
				if(operationChk.equalsIgnoreCase("W")){
					typeService = "";
					serviceType=refMasterManager.getServiceTypeListByWarehouse(sessionCorpID, "TCKTSERVC",operationsHubLimits.getHubID());
				    Iterator iter1  =serviceType.iterator();
						while (iter1.hasNext()) {
							if (typeService.equals("")) {
								typeService = iter1.next().toString();
							} else {
								typeService = typeService + "," + iter1.next().toString();
							}
						}
				}else{
					serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
				    Iterator iter  =serviceType.iterator();
						while (iter.hasNext()) {
							if (typeService.equals("")) {
								typeService = iter.next().toString();
							} else {
								typeService = typeService + "," + iter.next().toString();
							}
						}
				}
			}
		} else {
			operationsHubLimits = new OperationsHubLimits();
			operationsHubLimits.setCorpID(sessionCorpID);
			
			operationsHubLimits.setCreatedOn(new Date());
			operationsHubLimits.setUpdatedOn(new Date());
						
		}

		return SUCCESS;
	}
	private String dailyDefLimitValue;
	private String dailyMinServValue;
	private BigDecimal dailyMaxEstVolValue;
	private BigDecimal dailyMaxEstWtValue;
	private String dailyOpHubLimitValue;
	private int tempHubLimitOld;
	private int tempHubLimitNew;
	private BigDecimal dailyMaxEstWtOld;
	private BigDecimal dailyMaxEstWtNew;
	private BigDecimal tempdailyMaxEstWt;
	private BigDecimal dailyMaxEstVolOld;
	private BigDecimal dailyMaxEstVolNew;
	private BigDecimal tempdailyMaxEstVol;
	private int tempHubLimit;
	private  Map<String, String> distinctHubDescription;
	private String crewCapacityValue;
	private String crewThreshHoldValue;
	public String save() throws Exception {
		String opsPermKey = sessionCorpID +"-"+"component.field.opsMgmt.warehouse";
		opsMgmtWarehouseCheck=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(opsPermKey) ;
		if(!opsMgmtWarehouseCheck){
		serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
	    Iterator iter  =serviceType.iterator();
			while (iter.hasNext()) {
				if (typeService.equals("")) {
					typeService = iter.next().toString();
				} else {
					typeService = typeService + "," + iter.next().toString();
				}
			}
		}
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub);
		distinctHubDescription.putAll(house);
		operationsHubLimits.setCorpID(sessionCorpID);
		operationsHubLimits.setUpdatedBy(getRequest().getRemoteUser());
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
	            if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
	            	operationChk ="H";
	            }else{
	            	operationChk ="W";
			}
            if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
				hubWarehouseLimit = "Crw";
			}else{
				hubWarehouseLimit = "Tkt";
			}
			}
	    }
	    if(opsMgmtWarehouseCheck){
			if(operationChk.equalsIgnoreCase("W")){
				typeService = "";
				serviceType=refMasterManager.getServiceTypeListByWarehouse(sessionCorpID, "TCKTSERVC",operationsHubLimits.getHubID());
			    Iterator iter1  =serviceType.iterator();
					while (iter1.hasNext()) {
						if (typeService.equals("")) {
							typeService = iter1.next().toString();
						} else {
							typeService = typeService + "," + iter1.next().toString();
						}
					}
			}else{
				serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
			    Iterator iter2  =serviceType.iterator();
					while (iter2.hasNext()) {
						if (typeService.equals("")) {
							typeService = iter2.next().toString();
						} else {
							typeService = typeService + "," + iter2.next().toString();
						}
					}
			}
		}
		boolean isNew = (operationsHubLimits.getId() == null);
		List isexisted = operationsHubLimitsManager.isExisted(operationsHubLimits.getHubID(), sessionCorpID);
		if (!isexisted.isEmpty() && isNew) {
			errorMessage("Operational Limit for hub&nbsp;/&nbsp;WH {"+operationsHubLimits.getHubID()+"} already exist.");
			
		} else {
			if (isNew) {
				operationsHubLimits.setCreatedOn(new Date());
			}
			operationsHubLimits.setUpdatedOn(new Date());
			operationsHubLimits = operationsHubLimitsManager.save(operationsHubLimits);
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "Operations Hub&nbsp;/&nbsp;WH Limits added successfully" : "Operations Hub&nbsp;/&nbsp;WH Limits updated successfully";
				saveMessage(getText(key));
			}
			hitFlag="1";
			if((!(operationsHubLimits.getHubID().equalsIgnoreCase("0"))) && hubWarehouseLimit.equalsIgnoreCase("Tkt")){				
				if(operationChk =="W"){
					if((dailyOpHubLimitValue==null)||(dailyOpHubLimitValue.equalsIgnoreCase(""))){
						dailyOpHubLimitValue="0";
					}	
					if((dailyMaxEstWtValue==null) || (dailyMaxEstWtValue.equals(""))){
						dailyMaxEstWtValue = new BigDecimal("0.00");
					}
					if((dailyMaxEstVolValue==null) || (dailyMaxEstVolValue.equals(""))){
						dailyMaxEstVolValue = new BigDecimal("0.00");
					}
				if(dailyOpHubLimitValue!=null && !dailyOpHubLimitValue.equalsIgnoreCase("") ){
					tempHubLimitNew = Integer.parseInt(operationsHubLimits.getDailyOpHubLimit());
					tempHubLimitOld =Integer.parseInt(dailyOpHubLimitValue);
					tempHubLimit = (tempHubLimitOld) - (tempHubLimitNew);
					tempHubLimit = Math.abs(tempHubLimit) ;
					if(tempHubLimitOld!=tempHubLimitNew){
						operationsHubLimitsManager.updateDailyOpHubLimitParent(tempHubLimit,tempHubLimitOld,tempHubLimitNew,sessionCorpID,operationsHubLimits.getHubID());
					}
				}
				if(dailyMaxEstWtValue!=null && !dailyMaxEstWtValue.equals("")){
					dailyMaxEstWtNew =operationsHubLimits.getDailyMaxEstWt();
					dailyMaxEstWtOld = dailyMaxEstWtValue;
					tempdailyMaxEstWt = dailyMaxEstWtOld.subtract(dailyMaxEstWtNew);
					tempdailyMaxEstWt = tempdailyMaxEstWt.abs();
					BigDecimal bg1, bg2;
					int res;
			        bg1 = (dailyMaxEstWtOld);
			        bg2 = (dailyMaxEstWtNew);
			        res = bg1.compareTo(bg2);
					if(res != 0){				
						operationsHubLimitsManager.updateDailyMaxEstWtParent(tempdailyMaxEstWt,dailyMaxEstWtOld,dailyMaxEstWtNew,sessionCorpID,operationsHubLimits.getHubID());
					}
				}
				if(dailyMaxEstVolValue!=null && !dailyMaxEstVolValue.equals("")){
					dailyMaxEstVolNew =operationsHubLimits.getDailyMaxEstVol();
					dailyMaxEstVolOld = dailyMaxEstVolValue;
					tempdailyMaxEstVol = dailyMaxEstVolOld.subtract(dailyMaxEstVolNew);
					tempdailyMaxEstVol = tempdailyMaxEstVol.abs();
					BigDecimal bg1, bg2;
					int res;
			        bg1 = (dailyMaxEstVolOld);
			        bg2 = (dailyMaxEstVolNew);
			        res = bg1.compareTo(bg2);
			        if(res != 0){
			        	operationsHubLimitsManager.updateDailyMaxEstVolParent(tempdailyMaxEstVol,dailyMaxEstVolOld,dailyMaxEstVolNew,sessionCorpID,operationsHubLimits.getHubID());
			        }
				}
				if(operationsHubLimits.getHubID().equalsIgnoreCase("DC-Metro") && hubWarehouseLimit.equalsIgnoreCase("Tkt")){
					if(!dailyDefLimitValue.equalsIgnoreCase(operationsHubLimits.getDailyOpHubLimitPC())){
						operationsHubLimitsManager.updateDailyDefLimit(sessionCorpID,operationsHubLimits.getDailyOpHubLimitPC());
					}
					if(!dailyMinServValue.equalsIgnoreCase(operationsHubLimits.getMinServiceDayHub())){
						operationsHubLimitsManager.updateMinServDay(sessionCorpID,operationsHubLimits.getMinServiceDayHub());
					}
					}
				}
			}
			if((!(operationsHubLimits.getHubID().equalsIgnoreCase("0"))) && hubWarehouseLimit.equalsIgnoreCase("Crw")){
				if(crewCapacityValue==null || crewCapacityValue.equalsIgnoreCase("")){
					crewCapacityValue="0";
				}
				if(crewCapacityValue!=null && !crewCapacityValue.equalsIgnoreCase("")){
					int tempCrewValueOld = Integer.parseInt(crewCapacityValue);
					int tempCrewValueNew = Integer.parseInt(operationsHubLimits.getDailyCrewCapacityLimit());
					int tempCrewLimit = (tempCrewValueOld)-(tempCrewValueNew);
					tempCrewLimit = Math.abs(tempCrewLimit);
					if(tempCrewValueOld!=tempCrewValueNew){
						operationsHubLimitsManager.updateCrewLimitParent(tempCrewLimit,tempCrewValueOld,tempCrewValueNew,sessionCorpID,operationsHubLimits.getHubID());
					}
				}
				if(operationsHubLimits.getHubID().equalsIgnoreCase("DC-Metro") && hubWarehouseLimit.equalsIgnoreCase("Crw")){
					if(!crewCapacityValue.equalsIgnoreCase(operationsHubLimits.getDailyCrewCapacityLimit())){
						operationsHubLimitsManager.updateCrewDailyLimit(sessionCorpID,operationsHubLimits.getDailyCrewCapacityLimit());
					}
					if(!crewThreshHoldValue.equalsIgnoreCase(operationsHubLimits.getDailyCrewThreshHoldLimit())){
						operationsHubLimitsManager.updateCrewThreshHoldLimit(sessionCorpID,operationsHubLimits.getDailyCrewThreshHoldLimit());
					}
				}
			}
		}
		
		return SUCCESS;
	}
	
	public String editHub() {
		serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
	    Iterator iter = serviceType.iterator();
	
			while (iter.hasNext()) {
				if (typeService.equals("")) {
					typeService = iter.next().toString();
				} else {
					typeService = typeService + "," + iter.next().toString();
				}
			}
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub);
		distinctHubDescription.putAll(house);
		operationsHubLimits = (OperationsHubLimits)operationsHubLimitsManager.getHub(hubID).get(0);
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {	            
            if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
				hubWarehouseLimit = "Crw";
			}else{
				hubWarehouseLimit = "Tkt";
			}
		}
	 }		
		return SUCCESS;
	}

	@SkipValidation
	public String list() {
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub);
		distinctHubDescription.putAll(house);
		
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
	            if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
	            	operationChk ="H";
	            	hubLimits = operationsHubLimitsManager.findListByHub(sessionCorpID);
	            }else{
	            	operationChk ="W";
	            	hubLimits = operationsHubLimitsManager.getAllRecord(sessionCorpID);	
	            }
	            if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					hubWarehouseLimit = "Crw";
				}else{
					hubWarehouseLimit = "Tkt";
				}
			}
		}
		return SUCCESS;
	}
	private String wareHouse;
	private List returnAjaxStringValue;
	public String getValueForUnassignedWHAjax(){		
		returnAjaxStringValue = operationsHubLimitsManager.findUnassignedParentValue(sessionCorpID,wareHouse);		
		return SUCCESS;
	}
	
	public void getComboList(String corpId) {
		
	}
	
	private String opHubLimitNewValue;
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	

	public OperationsHubLimits getOperationsHubLimits() {
		return operationsHubLimits;
	}

	public void setOperationsHubLimits(OperationsHubLimits operationsHubLimits) {
		this.operationsHubLimits = operationsHubLimits;
	}

	public void setOperationsHubLimitsManager(
			OperationsHubLimitsManager operationsHubLimitsManager) {
		this.operationsHubLimitsManager = operationsHubLimitsManager;
	}

	public Map<String, String> getHub() {
		return hub;
	}

	public void setHub(Map<String, String> hub) {
		this.hub = hub;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getHubID() {
		return hubID;
	}

	public void setHubID(String hubID) {
		this.hubID = hubID;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public String getOperationChk() {
		return operationChk;
	}

	public void setOperationChk(String operationChk) {
		this.operationChk = operationChk;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public List getHubLimits() {
		return hubLimits;
	}

	public void setHubLimits(List hubLimits) {
		this.hubLimits = hubLimits;
	}
	public Map<String, String> getDistinctHubDescription() {
		return distinctHubDescription;
	}

	public void setDistinctHubDescription(Map<String, String> distinctHubDescription) {
		this.distinctHubDescription = distinctHubDescription;
	}

	public String getOpHubLimitNewValue() {
		return opHubLimitNewValue;
	}

	public void setOpHubLimitNewValue(String opHubLimitNewValue) {
		this.opHubLimitNewValue = opHubLimitNewValue;
	}

	public String getDailyOpHubLimitValue() {
		return dailyOpHubLimitValue;
	}

	public void setDailyOpHubLimitValue(String dailyOpHubLimitValue) {
		this.dailyOpHubLimitValue = dailyOpHubLimitValue;
	}

	public BigDecimal getDailyMaxEstVolValue() {
		return dailyMaxEstVolValue;
	}

	public void setDailyMaxEstVolValue(BigDecimal dailyMaxEstVolValue) {
		this.dailyMaxEstVolValue = dailyMaxEstVolValue;
	}

	public BigDecimal getDailyMaxEstWtValue() {
		return dailyMaxEstWtValue;
	}

	public void setDailyMaxEstWtValue(BigDecimal dailyMaxEstWtValue) {
		this.dailyMaxEstWtValue = dailyMaxEstWtValue;
	}

	public int getTempHubLimitOld() {
		return tempHubLimitOld;
	}

	public void setTempHubLimitOld(int tempHubLimitOld) {
		this.tempHubLimitOld = tempHubLimitOld;
	}

	public int getTempHubLimitNew() {
		return tempHubLimitNew;
	}

	public void setTempHubLimitNew(int tempHubLimitNew) {
		this.tempHubLimitNew = tempHubLimitNew;
	}

	public BigDecimal getDailyMaxEstWtOld() {
		return dailyMaxEstWtOld;
	}

	public void setDailyMaxEstWtOld(BigDecimal dailyMaxEstWtOld) {
		this.dailyMaxEstWtOld = dailyMaxEstWtOld;
	}

	public BigDecimal getDailyMaxEstWtNew() {
		return dailyMaxEstWtNew;
	}

	public void setDailyMaxEstWtNew(BigDecimal dailyMaxEstWtNew) {
		this.dailyMaxEstWtNew = dailyMaxEstWtNew;
	}

	public BigDecimal getTempdailyMaxEstWt() {
		return tempdailyMaxEstWt;
	}

	public void setTempdailyMaxEstWt(BigDecimal tempdailyMaxEstWt) {
		this.tempdailyMaxEstWt = tempdailyMaxEstWt;
	}

	public BigDecimal getDailyMaxEstVolOld() {
		return dailyMaxEstVolOld;
	}

	public void setDailyMaxEstVolOld(BigDecimal dailyMaxEstVolOld) {
		this.dailyMaxEstVolOld = dailyMaxEstVolOld;
	}

	public BigDecimal getDailyMaxEstVolNew() {
		return dailyMaxEstVolNew;
	}

	public void setDailyMaxEstVolNew(BigDecimal dailyMaxEstVolNew) {
		this.dailyMaxEstVolNew = dailyMaxEstVolNew;
	}

	public BigDecimal getTempdailyMaxEstVol() {
		return tempdailyMaxEstVol;
	}

	public void setTempdailyMaxEstVol(BigDecimal tempdailyMaxEstVol) {
		this.tempdailyMaxEstVol = tempdailyMaxEstVol;
	}

	public int getTempHubLimit() {
		return tempHubLimit;
	}

	public void setTempHubLimit(int tempHubLimit) {
		this.tempHubLimit = tempHubLimit;
	}

	public String getWareHouse() {
		return wareHouse;
	}

	public void setWareHouse(String wareHouse) {
		this.wareHouse = wareHouse;
	}

	public List getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(List returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getDailyDefLimitValue() {
		return dailyDefLimitValue;
	}

	public void setDailyDefLimitValue(String dailyDefLimitValue) {
		this.dailyDefLimitValue = dailyDefLimitValue;
	}

	public String getDailyMinServValue() {
		return dailyMinServValue;
	}

	public void setDailyMinServValue(String dailyMinServValue) {
		this.dailyMinServValue = dailyMinServValue;
	}

	public String getHubWarehouseLimit() {
		return hubWarehouseLimit;
	}

	public void setHubWarehouseLimit(String hubWarehouseLimit) {
		this.hubWarehouseLimit = hubWarehouseLimit;
	}

	public String getCrewCapacityValue() {
		return crewCapacityValue;
	}

	public void setCrewCapacityValue(String crewCapacityValue) {
		this.crewCapacityValue = crewCapacityValue;
	}

	public String getCrewThreshHoldValue() {
		return crewThreshHoldValue;
	}

	public void setCrewThreshHoldValue(String crewThreshHoldValue) {
		this.crewThreshHoldValue = crewThreshHoldValue;
	}

	public List getServiceType() {
		return serviceType;
	}

	public void setServiceType(List serviceType) {
		this.serviceType = serviceType;
	}

	public String getTypeService() {
		return typeService;
	}

	public void setTypeService(String typeService) {
		this.typeService = typeService;
	}

	public Boolean getOpsMgmtWarehouseCheck() {
		return opsMgmtWarehouseCheck;
	}

	public void setOpsMgmtWarehouseCheck(Boolean opsMgmtWarehouseCheck) {
		this.opsMgmtWarehouseCheck = opsMgmtWarehouseCheck;
	}	

}