<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="WCRIShipStat"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="1008"
		 pageHeight="842"
		 columnWidth="948"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="ModeType" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
	"WCRIShipStat.jrxml",
     serviceorder.`shipnumber` AS ShipNumber,
     serviceorder.`job` AS Job,
     UPPER(serviceorder.`mode`) AS Mode,
     serviceorder.`status` AS Status,
     serviceorder.`firstName` AS FirstName,
     serviceorder.`lastName` AS LastName,
     serviceorder.`commodity` AS Commodity,
     serviceorder.`destinationCity` AS DestinationCity,
     serviceorder.`destinationState` AS DestinationState,
     serviceorder.`destinationCountryCode` AS DestinationCountryCode,
     serviceorder.`originState` AS OriginState,
     serviceorder.`originCity` AS OriginCity,
     trackingstatus.`beginLoad` AS BeginLoad,
     trackingstatus.`packDone` AS PackDone,
     trackingstatus.`requestGBL` AS RequestGBL,
     trackingstatus.`recivedGBL` AS RecivedGBL,
     trackingstatus.`leftWHOn` AS LeftWHOn,
  --   trackingstatus.`notes` AS Notes,
     billing.`billToCode` AS BillToCode,
     serviceorder.`nextCheckOn` AS NextCheckOn,
     serviceorder.`coordinator` AS Coordinator,
     serviceorder.`destinationCompany` AS DestinationCompany,
     serviceorder.`destinationFax` AS DestinationFax,
     serviceorder.`originCompany` AS OriginCompany,
     serviceorder.`corpID` AS CorpID,
     serviceorder.`routing` AS Routing,
     DATEDIFF(Now(),trackingstatus.`requestGBL`) AS Request,
     DATEDIFF(Now(),trackingstatus.`beginLoad`) AS OnHold,
     notes.`note` AS Note,
     CONCAT(serviceorder.`lastName`,if(CHAR_LENGTH(serviceorder.`firstName`)>1,CONCAT(',',' ',serviceorder.`firstName`,' '),' '),'') AS Name,
     CONCAT(serviceorder.`originCity`,if(CHAR_LENGTH(serviceorder.`originState`)>1,CONCAT(',',' ',serviceorder.`originState`,' '),' '),'') AS Origin,
     CONCAT(serviceorder.`destinationCity`,if(CHAR_LENGTH(serviceorder.`destinationCountryCode`)>1,CONCAT(',',' ',serviceorder.`destinationCountryCode`,' '),' '),'') AS Destination
FROM
     `serviceorder` serviceorder 
	LEFT OUTER JOIN `billing` billing ON serviceorder.`id` = billing.`id` and billing.corpID =$P{Corporate ID}
     LEFT OUTER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id`  and trackingstatus.corpID=$P{Corporate ID}
     LEFT OUTER JOIN `notes` notes ON trackingstatus.`shipNumber` = notes.`notesId` AND notes.`notetype` = 'Service Order' AND notes.`noteSubType` = 'OriginPackout' and notes.corpID=$P{Corporate ID}
WHERE serviceorder.corpID=$P{Corporate ID}
AND serviceorder.`status` NOT IN('CNCL','CLSD') 
AND serviceorder.`job` IN('GST','DOM') 
AND if($P{ModeType} ='Sea',serviceorder.`mode` IN('Sea','SEA','OVR','ovr','Overland','Truck','Rail'),serviceorder.`mode` IN('Air','AIR'))
AND (trackingstatus.`beginLoad` >= '2002-07-01' AND trackingstatus.`beginLoad` < NOW()) 
AND trackingstatus.`leftWHOn` IS NULL 
AND billing.`billToCode` IN('639490','639491','639492','639495') 
AND if(serviceorder.`mode` IN('Air','AIR'),DATEDIFF(Now(),trackingstatus.`beginLoad`) > 2,DATEDIFF(Now(),trackingstatus.`beginLoad`) > 6)
AND serviceorder.`routing` NOT IN('IMP')
ORDER BY serviceorder.`job`,serviceorder.`mode`,trackingstatus.`beginLoad`,Name]]></queryString>

	<field name="WCRIShipStat.jrxml" class="java.lang.String"/>
	<field name="ShipNumber" class="java.lang.String"/>
	<field name="Job" class="java.lang.String"/>
	<field name="Mode" class="java.lang.String"/>
	<field name="Status" class="java.lang.String"/>
	<field name="FirstName" class="java.lang.String"/>
	<field name="LastName" class="java.lang.String"/>
	<field name="Commodity" class="java.lang.String"/>
	<field name="DestinationCity" class="java.lang.String"/>
	<field name="DestinationState" class="java.lang.String"/>
	<field name="DestinationCountryCode" class="java.lang.String"/>
	<field name="OriginState" class="java.lang.String"/>
	<field name="OriginCity" class="java.lang.String"/>
	<field name="BeginLoad" class="java.sql.Timestamp"/>
	<field name="PackDone" class="java.sql.Timestamp"/>
	<field name="RequestGBL" class="java.sql.Timestamp"/>
	<field name="RecivedGBL" class="java.sql.Timestamp"/>
	<field name="LeftWHOn" class="java.sql.Timestamp"/>
	<field name="BillToCode" class="java.lang.String"/>
	<field name="NextCheckOn" class="java.sql.Timestamp"/>
	<field name="Coordinator" class="java.lang.String"/>
	<field name="DestinationCompany" class="java.lang.String"/>
	<field name="DestinationFax" class="java.lang.String"/>
	<field name="OriginCompany" class="java.lang.String"/>
	<field name="CorpID" class="java.lang.String"/>
	<field name="Routing" class="java.lang.String"/>
	<field name="Request" class="java.lang.Long"/>
	<field name="OnHold" class="java.lang.Long"/>
	<field name="Note" class="java.lang.String"/>
	<field name="Name" class="java.lang.String"/>
	<field name="Origin" class="java.lang.String"/>
	<field name="Destination" class="java.lang.String"/>

	<variable name="OriginCityState" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{OriginCity}==null ? "" : $F{OriginCity} + " " + "," + $F{OriginState}==null ? "" : $F{OriginState}]]></variableExpression>
	</variable>
	<variable name="DestCityState" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{DestinationCity}==null ? "" : $F{DestinationCity} + " " + "," + $F{DestinationCountryCode}==null ? "" : $F{DestinationCountryCode}]]></variableExpression>
	</variable>

		<group  name="mode" isStartNewPage="true" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{Mode}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="2"
						width="144"
						height="17"
						key="staticText-22"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipment Type:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="144"
						y="2"
						width="41"
						height="17"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Mode}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="177"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="12"
						y="11"
						width="38"
						height="17"
						key="staticText-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[From:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="12"
						y="37"
						width="38"
						height="17"
						key="staticText-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[To:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="12"
						y="53"
						width="38"
						height="17"
						key="staticText-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Fax:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="98"
						width="948"
						height="19"
						key="staticText-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica" size="14" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Notice of Shipments Waiting Release from Warehouse]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="144"
						y="134"
						width="41"
						height="17"
						key="staticText-6"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Mode]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="185"
						y="134"
						width="119"
						height="17"
						key="staticText-7"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Origin]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="185"
						y="151"
						width="119"
						height="17"
						key="staticText-8"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destination]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="304"
						y="134"
						width="74"
						height="17"
						key="staticText-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Load Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="377"
						y="134"
						width="74"
						height="17"
						key="staticText-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipment]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="377"
						y="151"
						width="74"
						height="17"
						key="staticText-11"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Ready]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="450"
						y="134"
						width="74"
						height="17"
						key="staticText-12"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Request]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="450"
						y="151"
						width="74"
						height="17"
						key="staticText-13"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[GBL]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="523"
						y="134"
						width="74"
						height="17"
						key="staticText-14"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Receive]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="523"
						y="151"
						width="74"
						height="17"
						key="staticText-15"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[GBL]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="596"
						y="151"
						width="74"
						height="17"
						key="staticText-16"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Date Left]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="669"
						y="117"
						width="94"
						height="17"
						key="staticText-17"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[# of days from]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="669"
						y="151"
						width="74"
						height="17"
						key="staticText-18"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="669"
						y="134"
						width="74"
						height="17"
						key="staticText-19"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Request]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="743"
						y="134"
						width="73"
						height="17"
						key="staticText-20"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Load]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="743"
						y="151"
						width="73"
						height="17"
						key="staticText-21"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="816"
						y="151"
						width="132"
						height="17"
						key="staticText-23"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Comments]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="174"
						width="948"
						height="0"
						key="line-4"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="0"
						y="76"
						width="948"
						height="17"
						key="staticText-25"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Security Storage Company]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="50"
						y="11"
						width="112"
						height="17"
						key="staticText-26"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Tracie]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="49"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="100"
						y="6"
						width="85"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="91"
						y="24"
						width="94"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Commodity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="185"
						y="6"
						width="119"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Origin}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="185"
						y="24"
						width="119"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destination}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="304"
						y="6"
						width="73"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{BeginLoad}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="377"
						y="6"
						width="74"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{PackDone}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="451"
						y="6"
						width="73"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{RequestGBL}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="524"
						y="6"
						width="73"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{RecivedGBL}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="597"
						y="6"
						width="73"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{LeftWHOn}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="816"
						y="6"
						width="132"
						height="36"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Note}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="46"
						width="948"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="670"
						y="6"
						width="73"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Request}==null ? "**" : $F{Request}.toString()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="743"
						y="6"
						width="73"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Long"><![CDATA[$F{OnHold}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="6"
						width="100"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ShipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="18"
						y="24"
						width="41"
						height="17"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Mode}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="25"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="5"
						width="948"
						height="17"
						key="textField-4"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Mode}.equals("Air") || $F{Mode}.equals("AIR")? "Please Note - This report shows all shipments that still remain in the warehouse after pick up from residence" : ""]]></textFieldExpression>
				</textField>
			</band>
		</columnFooter>
		<pageFooter>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="5"
						width="209"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="743"
						y="5"
						width="170"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="912"
						y="5"
						width="36"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="2"
						width="948"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<staticText>
					<reportElement
						x="209"
						y="5"
						width="534"
						height="15"
						key="staticText-24"/>
					<box></box>
					<textElement textAlignment="Center">
						<font size="8"/>
					</textElement>
				<text><![CDATA[WCRIShipStat]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
