<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerViewListForm'].elements['partner.lastName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.firstName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.partnerCode'].value = '';
		document.forms['partnerViewListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerViewListForm'].elements['stateSearch'].value = '';
		document.forms['partnerViewListForm'].elements['countrySearch'].value = '';
		document.forms['partnerViewListForm'].elements['partner.status'].value = '';
}

</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:0px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
font-size:.85em;
}
form {
margin-top:-45px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head>
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top" method="popupListAdmin" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerViewListForm" action='${empty param.popup?"searchPartnerAgent.html":"searchPartnerAgent.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>

<s:hidden name="partner.isPrivateParty" value="false"/>
<s:hidden name="partner.isAccount" value="false"/>
<s:hidden name="partner.isAgent" value="true"/>
<s:hidden name="partner.isVendor" value="false"/>
<s:hidden name="partner.isCarrier" value="false"/>
<s:hidden name="partner.isOwnerOp" value="false"/>
			
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
</c:if>
<div id="layer1" style="width:100%">
<div id="otabs">
		<ul>
			<li><a class=""><span>Search</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:888px;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Last Name/Company Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingState"/></th>
			<th><fmt:message key="partner.status"/></th>

			<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partner.partnerCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partner.firstName" size="20" cssClass="input-text" /></td>
			<td><s:textfield name="partner.lastName" size="20" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="20" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="5" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:100px" headerKey="" headerValue=""/></td>
		</tr>
		<tr>
			
			<td colspan="6"/>
			<td width="130px" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
</s:form>
<div id="layer2" style="width:100%">

<div id="otabs" style="margin-top: -15px;">
	<ul>
		<li><a class=""><span>Partner List</span></a></li>
	</ul>
</div><div class="spnblk">&nbsp;</div> 


<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="false" defaultsort="1" pagesize="10" style="width:100%;margin-top:2px;margin-left:5px; " decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>	
	
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>	
    <display:column titleKey="partner.vanLineCode" style="width:160px"><c:out value="${partnerList.vanLineCode}" /></display:column>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
 	
    <display:column style="width:105px;cursor:pointer;" title="View"><A onclick="location.href='<c:url value="/findPartnerProfileList.html?from=view&code=${partnerList.partnerCode}&partnerType=AG&id=${partnerList.id}"/>'">View Detail</A></display:column>
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<c:set var="isTrue" value="false" scope="session"/>