package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ContractPolicyDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ContractPolicy;

public class ContractPolicyDaoHibernate extends GenericDaoHibernate<ContractPolicy, Long> implements ContractPolicyDao {

private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	public ContractPolicyDaoHibernate() {   
        super(ContractPolicy.class);   
    } 
	
	public List checkById(String code){
		return getHibernateTemplate().find("select id from ContractPolicy where partnerCode=?",code);
	}

	public List getContractPolicy(String partnerCode, String corpID) {
		return getHibernateTemplate().find("from ContractPolicy where partnerCode='"+ partnerCode +"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')");
	}
public List findPolicyFileNewList(String partnerCode, String corpID){
		
		List temp=getHibernateTemplate().find("from ContractPolicy where partnerCode='"+ partnerCode +"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') order by docSequenceNumber asc");
			
	/*if(temp!=null && temp.size()<=0){		
		temp=getHibernateTemplate().find("from ContractPolicy where (partnerCode is null or partnerCode='' ) and corpID = '"+corpID+"'");
	}*/
	return temp;
		
	}
	public List findPolicyFileList(String partnerCode, String corpID){
		
		List temp=getHibernateTemplate().find("from ContractPolicy where partnerCode='"+ partnerCode +"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') order by docSequenceNumber asc");
			
	/**if(temp!=null && temp.size()<=0){		
		temp=getHibernateTemplate().find("from ContractPolicy where (partnerCode is null or partnerCode='' ) and corpID = '"+corpID+"'");
	}*/
	return temp;
		
	}
	public List findDocumentList(String partnerCode,String sectionName ,String language , String corpID){
		return getHibernateTemplate().find("from PolicyDocument where partnerCode='"+ partnerCode +"' and sectionName='"+sectionName+"' and language='"+language+"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')");
		
	}
	public List getPolicyReference(String partnerCode, String corpID,Long parentId){
		List al=getSession().createSQLQuery("select id from contractpolicy where partnerCode='"+ partnerCode +"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') and parentId='"+parentId+"' limit 1").list();
		return al;		
	}	
	public List getPolicyDocReference(String partnerCode, String corpID,Long parentId,String sectionName,String language){
		List al=getSession().createSQLQuery("select id from policydocument where partnerCode='"+ partnerCode +"' and sectionName='"+sectionName+"' and language='"+language+"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') and parentId='"+parentId+"' limit 1").list();
		return al;		
	}
	public int checkSectionName(String sectionName ,String Language,String code, String sessionCorpID){
		if(sectionName==null) sectionName="";
		if(Language==null) Language="";
		String temp=this.getSession().createSQLQuery("select count(*) from contractpolicy where partnerCode='"+code+"' and sectionName='"+sectionName+"' and language='"+Language+"' and  corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list().get(0).toString();
		int countValue=Integer.parseInt(temp);
		return countValue;
	}
	public int checkSequenceNumber(Long sequenceNumber ,String Language,String code, String sessionCorpID){
		if(Language==null) Language="";
		if(sequenceNumber==null) sequenceNumber=0L;
		String tempValue=this.getSession().createSQLQuery("select count(*) from contractpolicy where partnerCode='"+code+"' and docSequenceNumber='"+sequenceNumber+"' and language='"+Language+"' and  corpId in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list().get(0).toString();
		int valuecount=Integer.parseInt(tempValue);
		return valuecount;
	}
	public int updatePolicyDocument(String dummsectionName,String dummlanguage,String code, String sectionName ,String language ,String sessionCorpID){
	
		return getHibernateTemplate().bulkUpdate("update PolicyDocument set sectionName='"+sectionName+"' , language='"+language+"' where sectionName='"+dummsectionName+"' and language='"+dummlanguage+"' and partnerCode=?",code);
	}
	public int deletedDocument(String documentSectionName,String documentLanguage,String partnerCode,String sessionCorpID){
		return getHibernateTemplate().bulkUpdate("delete FROM PolicyDocument  where sectionName='"+documentSectionName+"' and language='"+documentLanguage+"' and partnerCode='"+partnerCode+"'");
	}
	public List getContractPolicyRef(String partnerCode,String sessionCorpID){
		return getHibernateTemplate().find("select id from ContractPolicy where corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and language='English' and sectionName='Policy' and partnerCode=?",partnerCode);
	}
	public List policyFilesList(String sessionCorpID){
		return getHibernateTemplate().find("from ContractPolicy where partnerCode='' and corpId= ?",sessionCorpID);
	}
	public int sectionNameCheck(String sectionName ,String Language, String sessionCorpID){
		if(sectionName==null) sectionName="";
		if(Language==null) Language="";
		String temp=this.getSession().createSQLQuery("select count(*) from contractpolicy where  sectionName='"+sectionName+"' and language='"+Language+"' and (partnerCode is null or partnerCode ='' ) and  corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list().get(0).toString();
		int countValue=Integer.parseInt(temp);
		return countValue;
	}
	public List documentFileList( String sectionName ,String language,String sessionCorpID){
		
		return getHibernateTemplate().find("from PolicyDocument where sectionName='"+sectionName+"' and language='"+language+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')");
	}
	public void deleteagentParentPolicyFile(String oldAgentParent,String partnerCode,String sessionCorpID){
		String tempPartnerId="";
		String policyDocumentId="";
		List tempParent= this.getSession().createSQLQuery("select id from contractpolicy where partnerCode='"+oldAgentParent+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list();
		List policyDocument= this.getSession().createSQLQuery("select id from policydocument where partnerCode='"+oldAgentParent+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerId.equals("")){
					tempPartnerId = "'"+it.next().toString()+"'";
				}else{
					tempPartnerId =tempPartnerId+ ",'"+it.next().toString()+"'";
				}
			}
		}
		if(!tempPartnerId.equals("")){
			this.getSession().createSQLQuery("delete FROM contractpolicy where parentid in ("+tempPartnerId+") and partnerCode='"+partnerCode+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')").executeUpdate();	
		}
		
		if(policyDocument.size()>0){
			Iterator it1=policyDocument.iterator();
			while(it1.hasNext()){
				
				if(policyDocumentId.equals("")){
					policyDocumentId = "'"+it1.next().toString()+"'";
				}else{
					policyDocumentId =policyDocumentId+ ",'"+it1.next().toString()+"'";
				}
			}
		}
         if(!policyDocumentId.equals("")){
        	 this.getSession().createSQLQuery("delete FROM policydocument where parentid in ("+policyDocumentId+") and partnerCode='"+partnerCode+"' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')").executeUpdate();
		     }
		
		
	}
	public void deleteChildDocument(Long id2, String sessionCorpID){
		String query="";
		query="delete from contractpolicy where parentId ="+id2+" and corpID in ('TSFT', '"+sessionCorpID+"')";
		this.getSession().createSQLQuery(query).executeUpdate();
	}
	public void deleteChildDocumentFromPolicy(Long id2, String sessionCorpID){
		String query="";
		query="delete from policydocument where parentId ="+id2+" and corpID in ('TSFT', '"+sessionCorpID+"')";
		this.getSession().createSQLQuery(query).executeUpdate();
}
	public int deletedPolicyContract(String partnerCode,String sessionCorpID){
		int i = getHibernateTemplate().bulkUpdate("delete FROM ContractPolicy  where partnerCode='"+partnerCode+"'");
		int j = getHibernateTemplate().bulkUpdate("delete FROM PolicyDocument  where partnerCode='"+partnerCode+"'");
		return i;
	}
	public String findByBookingLastName(String partnerCode, String corpid) {
		try {
			String bookingAgentName="";
			List size=new ArrayList();
			size = this.getSession().createSQLQuery("select lastName from partnerpublic where  partnerCode='"+partnerCode+"' and partnerCode is not null and partnerCode<>'' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') ").list();
			if((size!=null)&&(!size.isEmpty()) && size.get(0)!=null)
			{
				bookingAgentName=size.get(0).toString();
			}
			return bookingAgentName;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	//  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
	return "";	
	}
}
