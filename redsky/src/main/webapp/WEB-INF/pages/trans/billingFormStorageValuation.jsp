<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 
 
  <c:if test="${storageJob != 'true'}">
 	<s:hidden name="billing.warehouse"/>
 </c:if> 
 <s:hidden name="storageJob"/>	  
  <c:if test="${storageJob == 'true'}">
  <tr><c:set var="fullDayFlag" value="false"/>
				<c:if test="${billing.signedDisclaimer}">
					<c:set var="fullDayFlag" value="true"/>
				</c:if>
	  			
	<td height="10" align="left" class="listwhitetext">
   <div  onClick="javascript:animatedcollapse.toggle('storage');showDetailed();" style="margin: 0px"  >
<c:if test="${from=='rule'}">
	<c:if test="${field=='billing.storageOut'}">	 
		<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
		<tr>
		<td class="headtab_left">
		</td>
		<td class="headtab_center" ><font color="red">&nbsp;Storage&nbsp;/&nbsp;Valuation
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center" >&nbsp;</td>
		<td class="headtab_right">
		</td>
		</tr>
		</table>
	</c:if>
</c:if>
<c:if test="${from=='rule'}">
	<c:if test="${field!='billing.storageOut'}">	 
		<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
		<tr>
		<td class="headtab_left">
		</td>
		<td class="headtab_center" >&nbsp;Storage&nbsp;/&nbsp;Valuation
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center" >&nbsp;</td>
		<td class="headtab_right">
		</td>
		</tr>
		</table>
	</c:if>
</c:if>
<c:if test="${from!='rule'}">	 
	<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td class="headtab_center" >&nbsp;Storage&nbsp;/&nbsp;Valuation
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center" >&nbsp;</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
</c:if>
</div>   
  		<div id="storage">
  		<configByCorp:fieldVisibility componentId="component.tab.billing.StorageBillingParty">
		<table class="detailTabLabel" border="0" cellpadding="0"  cellspacing="0" width="100%">
 		  <tbody>
 		  	<tr><td align="left" height="2px"></td></tr>
 		  	<tr>
 		  	   <td colspan="2" class="subcontenttabChild" ><b>Storage Billing Party</b></td>
 		  	   <c:choose>
					<c:when test="${billing.storageBillingParty == '2'}" >
	  		   			<td class="subcontenttabChild" ><label for="r1">Primary<input type="radio" name="billing.storageBillingParty" id="r1"  value="1" onchange="changeStatus();" tabindex=""/></label></td>
               			<td class="subcontenttabChild"><label for="r2">Secondary<input type="radio" name="billing.storageBillingParty" id="r2" value="2" onchange="changeStatus();" tabindex="" checked /></label></td>
               			<td class="subcontenttabChild"><label for="r3">Private Party<input type="radio" name="billing.storageBillingParty" id="r3" value="3" onchange="changeStatus();" tabindex=""/></label></td>
                        <td width="200px" class="subcontenttabChild"></td>
                 </c:when>
               	<c:when test="${billing.storageBillingParty == '3'}" >
	  		   			<td class="subcontenttabChild"><label for="r1">Primary<input type="radio" name="billing.storageBillingParty" id="r1" value="1"  onchange="changeStatus();" tabindex=""/></label></td>
               			<td class="subcontenttabChild"><label for="r2">Secondary<input type="radio" name="billing.storageBillingParty" id="r2" value="2" onchange="changeStatus();" tabindex=""/></label></td>
               			<td class="subcontenttabChild"><label for="r3">Private Party<input type="radio" name="billing.storageBillingParty" id="r3" value="3" onchange="changeStatus();" tabindex="" checked/></label></td>
                         <td width="200px" class="subcontenttabChild"></td>
               </c:when>
               <c:otherwise>
                        <td class="subcontenttabChild"><label for="r1">Primary<input type="radio" name="billing.storageBillingParty" id="r1" value="1" onchange="changeStatus();" tabindex="" checked /></label></td>
               			<td class="subcontenttabChild"><label for="r2">Secondary<input type="radio" name="billing.storageBillingParty" id="r2" onchange="changeStatus();" value="2" tabindex=""/></label></td>
               			<td class="subcontenttabChild"><label for="r3">Private Party<input type="radio" name="billing.storageBillingParty" id="r3" value="3" onchange="changeStatus();" tabindex=""/></label></td>
                        <td width="200px" class="subcontenttabChild"></td>
                </c:otherwise>
            </c:choose>
 		  	</tr>
 		  	</tbody>
 		  	</table>
 		  	</configByCorp:fieldVisibility>
 		  	<table class="detailTabLabel" border="0" >
 		  <tbody>
	  		<tr>
	  			<td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.storageMeasurement"/></td>
	  			<td align="left">
	  			<configByCorp:customDropDown 	listType="map" list="${measure}" fieldValue="${billing.storageMeasurement}"
		attribute="id=billingForm_billing_storageMeasurement class=list-menu name=billing.storageMeasurement style=width:120px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
	  			
<%-- 	  			<s:select cssClass="list-menu" name="billing.storageMeasurement" list="%{measure}" cssStyle="width:120px" onchange="changeStatus();" tabindex=""/> --%>
	  			</td>
	  			<td align="right" class="listwhitetext" width="80px"><fmt:message key="billing.postGrate"/></td>
	  			<td align="left"><s:textfield name="billing.postGrate" maxlength="15" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" cssClass="input-text" size="7" tabindex=""/>
				<td align="right" class="listwhitetext" width="100"><img id="rateImage" class="openpopup" align="top" onclick="findQuantity();" src="<c:url value='/images/arrow-loadern.gif'/>"/><fmt:message key="billing.onHand"/></td>
		    	<td align="left"><s:textfield name="billing.onHand" maxlength="15" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" size="7" tabindex=""/></td>
		    	
		    	<td align="right" class="listwhitetext">Remarks</td>
	       		<td align="left"><s:textfield name="billing.invoiceRemarks4" cssClass="input-text" id="invoiceRemarks4"  maxlength="40"/></td> 
	       					
	       					
	  				
							<c:if test="${empty billing.id}">
							<td align="right" width="25" colspan="3"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty billing.id}">
							<c:choose>
								<c:when test="${countBillingValuationNotes == '0' || countBillingValuationNotes == '' || countBillingValuationNotes == null}">
								<td align="right" width="25" colspan="3"><img id="countBillingValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" width="25" colspan="3"><img id="countBillingValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
			  		</tr>
				    <tr>		    
		    <td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.charge"/></td>
				<td align="left"><s:textfield id="billingCharge" name="billing.charge" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivIdA','charge','A')" onkeydown="return onlyAlphaNumericAllowed(event)" required="true" onchange="findChargeRateFast(),chargePerValue();changeStatus();" onblur="checkValueAfterChargeCode('charge');"  cssClass="input-text" maxlength="25" readonly="${readableChargeCode}" size="18" tabindex=""/>
		    	<img class="openpopup" align="top" width="17" height="20" onclick="chk();" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" />
		    	<div id="ChargeNameDivIdA" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
		    	</td>
	  		    <c:if test="${weeklyBilling}">
	  		    <td align="right" class="listwhitetext">Bill Freq</td>
   		    	<td align="left"><configByCorp:customDropDown 	listType="map" list="${billingFrequencyList}" fieldValue="${billing.billingFrequency}"
		attribute="id=billingForm_billing_billingFrequency class=list-menu name=billing.billingFrequency style=width:80px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/></td>
<%--    		    	<s:select cssClass="list-menu" name="billing.billingFrequency" list="%{billingFrequencyList}" onchange="changeStatus();" cssStyle="width:80px"  tabindex=""/></td>
 --%>		    	</c:if>
		    	<c:if test="${weeklyBilling}">
		    	<td align="right" class="listwhitetext">Period To Bill</td>
		    	</c:if>
		    	<c:if test="${!weeklyBilling}">
		    	<td align="right" class="listwhitetext">Months To Bill</td>
		    	</c:if>
		    	<td align="left"><s:textfield name="billing.cycle" cssStyle="text-align:right" onchange="onlyNumeric(this);changeStatus();" required="true" cssClass="input-text" maxlength="11" size="7" tabindex=""/></td>
		    	<c:if test="${weeklyBilling}">
		    	<td colspan="2">
		    	</c:if>
		    	<c:if test="${!weeklyBilling}">
		    	<td colspan="7">
		    	</c:if>
		    	<table class="detailTabLabel"><tr>
		    	<c:if test="${!weeklyBilling}">
		    	<td align="right" class="listwhitetext" width="97" >Storage Per Month</td>
		    	</c:if>
		    	<c:if test="${weeklyBilling}">
		    	<td align="right" class="listwhitetext" width="97">Storage Per Period</td>
		    	</c:if>
		    	<td align="left">
		    	<c:set var="sign" value="${billing.storagePerMonth}"/>
	   			<c:choose>
					<c:when test="${fn:startsWith(sign, '-')}"><s:textfield name="billing.storagePerMonth"  cssClass="rules-textUpper" onchange="onlyFloat(this);changeStatus();" required="true"  maxlength="15" size="15" tabindex=""/>
				</c:when>
	  			<c:otherwise>
	  			  <s:textfield name="billing.storagePerMonth" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" maxlength="11" size="7" tabindex=""/>
	  			</c:otherwise>
				</c:choose> 
		    	</td>
		        <td align="left" colspan="2"><input type="button" id="compute1" class="cssbutton" style="width:125px;" value="Get Current Charges" onclick="computeCurrentCharges('StoragePerMonth1');" tabindex=""></td>		    
		    </tr></table></td></tr>
   		    <tr>
   		    	<td align="right" class="listwhitetext"><fmt:message key="billing.warehouse"/></td>
   		    	<td align="left"><configByCorp:customDropDown 	listType="map" list="${house}" fieldValue="${billing.warehouse}"
		attribute="id=billingForm_billing_warehouse class=list-menu name=billing.warehouse style=width:120px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
	  			</td>
<%--    		    	<s:select cssClass="list-menu" name="billing.warehouse" list="%{house}" cssStyle="width:120px" headerKey=""  headerValue="" onchange="changeStatus();" tabindex=""/></td>
 --%>   		    	<td align="right" class="listwhitetext"><fmt:message key="billing.storageOut"/></td>
   		    	<td align="left" width="100">
                <c:if test="${not empty billing.storageOut}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.storageOut"/></s:text>
				    <s:textfield id="billingForm_billing_storageOut" name="billing.storageOut" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.storageOut}">
					<s:textfield id="billingForm_billing_storageOut" name="billing.storageOut" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
				<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
				<img id="billingForm_billing_storageOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_storageOut"/></td>
               </c:if>
               <c:if test='${serviceOrder.status == "CNCL"}'>
                <img id="billingForm_billing_storageOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
               </c:if>
               <c:if test='${serviceOrder.status == "HOLD"}'>
                <img id="billingForm_billing_storageOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
              </c:if>
            <td align="right" class="listwhitetext"><fmt:message key="billing.billThrough"/></td>
            <td align="left">
                     <c:if test="${not empty billing.billThrough}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.billThrough"/></s:text>
				    <s:textfield id="billingForm_billing_billThrough" name="billing.billThrough" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
				
				<c:if test="${empty billing.billThrough}">
					<s:textfield id="billingForm_billing_billThrough" name="billing.billThrough" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
			</td>
			
			<td>
				<img id="billingForm_billing_billThrough-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_billThrough"/>
			</td>
			
			<td align="right" class="listwhitetext">Warehouse&nbsp;Receipt&nbsp;Date</td>
            <td align="left">
                     <c:if test="${not empty billing.wareHousereceiptDate}">
					<s:text id="FormatedwareHousereceiptDate" name="${FormDateValue}"><s:param name="value" value="billing.wareHousereceiptDate"/></s:text>
				    <s:textfield id="billing_wareHouse" name="billing.wareHousereceiptDate" value="%{FormatedwareHousereceiptDate}" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
				
				<c:if test="${empty billing.wareHousereceiptDate}">
					<s:textfield id="billing_wareHouse" name="billing.wareHousereceiptDate" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
			</td>
			<td>
				<img id="billing_wareHouse-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billing_wareHouse"/>
			</td>
			<configByCorp:fieldVisibility componentId="component.field.Billing.ContractReceivedDate">
			<td align="right" class="listwhitetext">Contract&nbsp;Received&nbsp;Date</td>
            <td align="left">
                     <c:if test="${not empty billing.contractReceivedDate}">
					<s:text id="FormatedContractReceivedDate" name="${FormDateValue}"><s:param name="value" value="billing.contractReceivedDate"/></s:text>
				    <s:textfield id="billing_contractReceivedDate" name="billing.contractReceivedDate" value="%{FormatedContractReceivedDate}" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
				
				<c:if test="${empty billing.contractReceivedDate}">
					<s:textfield id="billing_contractReceivedDate" name="billing.contractReceivedDate" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
				</c:if>
			</td>
			<td>
				<img id="billing_contractReceivedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billing_contractReceivedDate"/>
			</td>
			</configByCorp:fieldVisibility>
                <c:if test="${systemDefaultVatCalculation=='true'}">
                <td colspan="8">
	                <table style="margin:0px;padding:0px;">
	                	<tr>
			                <td class="listwhitetext">VAT&nbsp;Desc&nbsp;</td>
			                <td><configByCorp:customDropDown 	listType="map" list="${euVatList}" fieldValue="${billing.storageVatDescr}"
		attribute="id=billingForm_billing_storageVatDescr class=list-menu name=billing.storageVatDescr style=width:110px  headerKey='' headerValue='' onchange=getStorageVatPercent('billing.storageVatDescr','billing.storageVatPercent'),changeStatus() tabindex='' "/>
		    	</td>
<%-- 			                <s:select  cssClass="list-menu"  key="billing.storageVatDescr" cssStyle="width:100px" list="%{euVatList}" headerKey="" onchange='changeStatus()'; headerValue=""  tabindex=""/> </td>
 --%>
 			            <td width="80px" align="right" class="listwhitetext" >VAT&nbsp;%&nbsp;</td>
			                <td align="left" class="listwhitetext"><s:textfield  cssClass="input-text" cssStyle="text-align:right" key="billing.storageVatPercent" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="return checkFloat(this);changeStatus();" maxLength="6" tabindex=""/></td>
			                <td width="80px" align="right" class="listwhitetext" >Billing&nbsp;Currency&nbsp;</td>
			                <td><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.billingCurrency}"
		attribute="id=billingForm_billing_billingCurrency class=list-menu name=billing.billingCurrency style=width:60px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
			                </td>
<%-- 			                <s:select cssClass="list-menu" name="billing.billingCurrency" list="%{currency}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:60px" tabindex=""/></td>
 --%>	               			<td width="80px" align="right" class="listwhitetext" >Contract&nbsp;Currency&nbsp;</td>
			                <td><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.contractCurrency}"
		attribute="id=billingForm_billing_contractCurrency class=list-menu name=billing.contractCurrency style=width:60px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
			                </td>
<%-- 			                <s:select cssClass="list-menu" name="billing.contractCurrency" list="%{currency}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:60px" tabindex=""/></td>
 --%>	               		</tr>
	               </table>
               </td>
               </c:if> 
            </tr> 
           
            <tr>
            <td colspan="25">
            	<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%">
 		  			<tbody>
 		  				<tr><td align="left"><img height="1px" width="100%" src="<c:url value='/images/vertlinedata.jpg'/>" /></td></tr>
	 	  			</tbody>
 				</table> 	
 				</td>	
			</tr>
            <tr>
				<td valign="middle" colspan="12" align="left">
		  			<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
			  			<tr>
							<td align="right" class="listwhitetext" width="122">Vendor&nbsp;Code</td>					
							<td align="left" class="listwhitetext"><s:textfield onchange="checkVendorName1();findStorageVatWithVatGroup();" id="billingVendorCode1Id" key="billing.vendorCode1" cssClass="input-text" readonly="false" size="7" maxlength="8" tabindex=""/></td>
							<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen1()" id="openpopup5.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
												
							<td align="right" class="listwhitetext">Vendor&nbsp;Name</td>
							<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="billing.vendorName1" id="billingVendorName1Id" readonly="false" onkeyup="findPartnerDetails('billingVendorName1Id','billingVendorCode1Id','billingVendorName1DivId',' and (isVendor=true or isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','',event);" size="31" onfocus="changeStatus();" tabindex=""/>
							<div id="billingVendorName1DivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<td align="right" class="listwhitetext" style="display: none;"><fmt:message key="billing.charge"/></td>
						 	<td align="left" style="display: none;"><s:textfield id="billingInsCharge" name="billing.insuranceCharge1" onkeydown="return onlyAlphaNumericAllowed(event);changeStatus();" readonly="true" cssClass="input-text" maxlength="25" size="23" tabindex=""/>
				    	 		<img class="openpopup" align="top" width="17" height="20" onclick="chkInsurance1();" id="openpopup6.img" src="<c:url value='/images/open-popup.gif'/>" />
				    	 	</td>
				    	 	<td align="right" class="listwhitetext" width="">Payable&nbsp;Rate</td>
						 	<td align="left"><s:textfield id="payableRate" name="billing.payableRate" cssClass="input-text" cssStyle="text-align:right" maxlength="15" size="12"  onchange="onlyFloat(this);changeStatus();" tabindex=""/></td>		
				    	 	<td align="right" class="listwhitetext" width="">Storage&nbsp;Per&nbsp;Month</td>
						 	<td align="left"><s:textfield id="vendorStoragePerMonth" name="billing.vendorStoragePerMonth" onchange="changeStatus();" cssClass="input-text"  cssStyle="text-align:right" maxlength="25" size="12" tabindex=""/></td>		
							<td align="left" colspan="2"><input type="button" id="computeVendorStoragePerMonth" onclick="computeCurrentCharges('StoragePerMonth2');" class="cssbutton" value="Get Current Charges" style="width:125px;" tabindex=""></td>
				    </tr>
					</table>					
				</td>
			</tr> 
			<tr>	<td valign="middle" colspan="12" align="left">
		  			<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
			  			<tr>
				    <td align="right" class="listwhitetext" width="120">VAT Desc</td>
						 	<td align="left">
						 	<configByCorp:customDropDown 	listType="map" list="${venderpaylistEuVatList}" fieldValue="${billing.vendorStorageVatDescr}"
		attribute="id=billingForm_billing_vendorStorageVatDescr class=list-menu name= billing.vendorStorageVatDescr style=width:100px  headerKey='' headerValue='' onchange=getStoragePayVatPercent('billing.vendorStorageVatDescr','billing.vendorStorageVatPercent');changeStatus() tabindex='' "/>
				 	</td>
						 	<%-- <s:select cssClass="list-menu" name="billing.vendorStorageVatDescr" list="%{payVatList}" onchange="getStoragePayVatPercent('billing.vendorStorageVatDescr','billing.vendorStorageVatPercent');changeStatus();" headerKey="" headerValue="" cssStyle="width:100px" tabindex=""/></td>
							 --%>
							<td align="right" class="listwhitetext" width="51">VAT %</td>
						 	<td align="left"><s:textfield id="vendorStorageVatPercent" name="billing.vendorStorageVatPercent" onchange="changeStatus();" cssClass="input-text" maxlength="25" size="12" tabindex=""/></td>
							
							<td width="80px" align="right" class="listwhitetext" >Billing&nbsp;Currency&nbsp;</td>
					        <td><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.vendorBillingCurency}"
		attribute="id=billingForm_billing_vendorBillingCurency  class=list-menu name=billing.vendorBillingCurency  style=width:60px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/></td>
		        
<%-- 					        <s:select cssClass="list-menu" name="billing.vendorBillingCurency" list="%{currency}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:60px" tabindex=""/></td>
 --%>					        
					        <td width="80px" align="right" class="listwhitetext" >Payable&nbsp;Contract&nbsp;Currency&nbsp;</td>
					        <td><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.payableContractCurrency}"
		attribute="id=billingForm_billing_payableContractCurrency  class=list-menu name=billing.payableContractCurrency style=width:60px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/></td>
<%-- 					        <s:select cssClass="list-menu" name="billing.payableContractCurrency" list="%{currency}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:60px" tabindex=""/></td>
 --%>					        </tr>
					        </table>
					        </td>
				    </tr>  
                 
 
				    <configByCorp:fieldVisibility componentId="component.field.Billing.LocationAgentCode">
				    <tr>
				    	<td valign="middle" colspan="8" align="left">
		  				<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
				    		<td align="right" class="listwhitetext" width="120">Location&nbsp;Agent&nbsp;Code</td>					
							<td align="left" class="listwhitetext"><s:textfield onchange="checkAgentCode();" id="billingLocationAgentCodeId" key="billing.locationAgentCode" cssClass="input-text" readonly="false" size="7" maxlength="8" tabindex=""/></td>
							<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen2()" id="openpopup5.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
												
							<td align="right" class="listwhitetext">Location&nbsp;Agent&nbsp;Name</td>
							<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" id="billingLocationAgentNameId" key="billing.locationAgentName" onkeyup="findPartnerDetails('billingLocationAgentNameId','billingLocationAgentCodeId','billingLocationAgentNameDivId',' and (isAccount=true or isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','',event);" readonly="false" size="35" onfocus="changeStatus();" tabindex=""/>
				    	   <div id="billingLocationAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				    	   </td>
				    	</table>
					     </td>
				    </tr>  
				    </configByCorp:fieldVisibility>      
	      </tbody>
 		</table> 		
 		 <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%">
 		  <tbody>
 		  	<tr><td align="left"><img height="1px" width="100%" src="<c:url value='/images/vertlinedata.jpg'/>" /></td></tr>
 		  	
	 	  </tbody>
 		</table> 		
 		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  	<tr>
	  			<td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.insuranceHas"/></td>
	  			<td align="left" colspan="1"><configByCorp:customDropDown 	listType="map" list="${hasval}" fieldValue="${billing.insuranceHas}"
		attribute="id=billingForm_billing_insuranceHas class=list-menu name=billing.insuranceHas style=width:128px  headerKey='' headerValue='' onchange='fillVendorName();changeStatus();displayField(this);makeMandIssueDt()'; tabindex='' "/></td>
<%-- 	  			<s:select cssClass="list-menu" name="billing.insuranceHas" list="%{hasval}" cssStyle="width:128px" onchange="fillVendorName();changeStatus();displayField(this);makeMandIssueDt()" tabindex=""/></td>
 --%>			
			<configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">		
			<td align="right" width="" class="listwhitetext">Valuation</td>
			<td align="left" colspan="1"><configByCorp:customDropDown 	listType="map" list="${valuationList}" fieldValue="${billing.valuation}"
				attribute="id=billingForm_billing_valuation class=list-menu name=billing.valuation style=width:128px  headerKey='' headerValue='' onchange=''; tabindex='' "/></td>	
			</configByCorp:fieldVisibility>	
				<td valign="bottom" colspan="2">
		  			<div id="disDiv" style="height:20px">
		  			<table style="vertical-align:bottom" >
		  			<tr>
					<td align="right" class="listwhitetext" style="width:50px;padding-left:39px;">Obtain&nbsp;Waiver/Signed&nbsp;Disclaimer</td>
					<td><s:checkbox key="billing.signedDisclaimer" value="${fullDayFlag}" fieldValue="true" onclick="changeStatus();" tabindex=""/></td>
					</tr></table>
					</div>
				</td>
			</tr>	
			<tr>
				<td valign="middle" colspan="8">
		  			<div id="disDiv1" >
		  			<table class="detailTabLabel" style="height:20px" border="0" style="vertical-align:middle;" >
		  			<tr>
					<td align="right" class="listwhitetext" width="122px">Vendor&nbsp;Code</td>
					<c:if test="${accountInterface!='Y'}">
						<td align="left" class="listwhitetext"><s:textfield onchange="checkVendorName();findPayableCurrencyCodeAjax();changeStatus();findStorageVendorVatWithVatGroup();" id="billingVendorCodeId" cssClass="input-text" key="billing.vendorCode" readonly="false" size="7" maxlength="8"
												tabindex=""/></td>
					<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" 
									id="openpopup5.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
					</c:if>
					<c:if test="${accountInterface=='Y'}">
					<td align="left" class="listwhitetext"><s:textfield onchange="checkVendorName();findPayableCurrencyCodeAjax();changeStatus();findStorageVendorVatWithVatGroup();" id="billingVendorCodeId"  cssClass="input-text" key="billing.vendorCode" readonly="false" size="7" maxlength="8"
												 tabindex="" /></td>	
					<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()"
										id="openpopup5.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
					</c:if>				
					<td align="right" class="listwhitetext">Vendor&nbsp;Name</td>
					<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="billing.vendorName" id="billingVendorNameId" onfocus="changeStatus();" onkeyup="findPartnerDetails('billingVendorNameId','billingVendorCodeId','billingVendorNameDivId',' and (isVendor=true or isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','',event);" readonly="false" size="35"  tabindex=""/>
					<div id="billingVendorNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					</td>
					<td align="right" class="listwhitetext" width="68px"><fmt:message key="billing.charge"/></td>
				 <td align="left"><s:textfield id="billingInsCharge" name="billing.insuranceCharge" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivIdB','insuranceCharge','B')"  onblur="checkValueAfterChargeCode('insuranceCharge')" onkeydown="return onlyAlphaNumericAllowed(event);changeStatus();"   cssClass="input-text" maxlength="25" readonly="${readableChargeCode}" size="23" tabindex=""/>
		    	 <img class="openpopup" align="top" width="17" height="20" onclick="chkInsurance();" id="openpopup6.img" src="<c:url value='/images/open-popup.gif'/>" />
		    	 <div id="ChargeNameDivIdB" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
		    	 </td>
	  		
					</tr></table>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key="billing.insuranceOptionCode"/></td>
				<s:hidden name="billing.estMoveCost"/>
				<s:hidden name="billing.insuranceOption"/>
				<s:hidden name="billing.insuranceOptionCode"/>
				<td align="left" colspan="8"><configByCorp:customDropDown 	listType="map" list="${insopt}" fieldValue="${billing.insuranceOptionCodeWithDesc}"
		attribute="id=billingForm_billing_insuranceOptionCodeWithDesc class=list-menu name=billing.insuranceOptionCodeWithDesc style=width:350px  headerKey='' headerValue='' onchange='insValues(this);baseRateValues(this);changeStatus();autoPopulate_billingForm_insuranceOption(this);calculateTotal();' tabindex='' "/></td>
<%-- 				<s:select cssClass="list-menu" name="billing.insuranceOptionCodeWithDesc" list="%{insopt}" cssStyle="width:350px" onchange="insValues(this);baseRateValues(this);changeStatus();autoPopulate_billingForm_insuranceOption(this);calculateTotal();" tabindex=""/></td>
 --%>			</tr> 		

			<tr>
				<td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.insuranceValueEntitle"/></td>
				<td align="left"><s:textfield name="billing.insuranceValueEntitle" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();"  required="true" cssClass="input-text" maxlength="15" size="15" tabindex=""/></td>
				<td align="right" class="listwhitetext" ><fmt:message key="billing.insurancePerMonth"/></td><td align="left">
				<c:set var="sign" value="${billing.insurancePerMonth}"/>
	   			<c:choose>
					<c:when test="${fn:startsWith(sign, '-')}"><s:textfield name="billing.insurancePerMonth"  cssClass="rules-textUpper" onchange="onlyFloat(this);changeStatus();" required="true"  maxlength="15" size="15" tabindex=""/>
				</c:when>
	  			<c:otherwise>
	  			  <s:textfield name="billing.insurancePerMonth" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" maxlength="15" size="15" tabindex=""/>
	  			</c:otherwise>
				</c:choose> 
				</td>
				
				<td align="left" colspan="3">
				<table style="margin:0px;padding:0px;">
				<tr>
				<td>				
				<div id="disDiv2" >
					<input type="button" id="compute2" class="cssbutton" style="width:123px;" value="Get Current Charges" onclick="computeCurrentCharges('InsurancePerMonth');" tabindex="">
				</div>
				</td>
				<c:if test="${systemDefaultVatCalculation=='true'}">
					
					<td >
						<table  class="detailTabLabel">
							<tr>
								<td class="listwhitetext">VAT&nbsp;Desc&nbsp;</td>
				                <td><configByCorp:customDropDown 	listType="map" list="${venderlistpayVatList}" fieldValue="${billing.insuranceVatDescr}"
		attribute="id=billingForm_billing_insuranceVatDescr class=list-menu name=billing.insuranceVatDescr style=width:93px  headerKey='' headerValue='' onchange='getInsuranceVatPercent();changeStatus();' tabindex='' "/>
		    	</td>
<%-- 				                <s:select  cssClass="list-menu"  key="billing.insuranceVatDescr" cssStyle="width:93px" list="%{euVatList}" headerKey="" onchange="getInsuranceVatPercent();changeStatus();" headerValue=""  tabindex=""/> </td>
 --%>				                <td align="right" class="listwhitetext" >VAT&nbsp;%&nbsp;</td>
				                <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="billing.insuranceVatPercent" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="return checkFloat(this);changeStatus();" maxLength="6" tabindex=""/></td>
							</tr>
						</table>
					</td>
				</c:if>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key="billing.certnum"/></td>
				<td align="left"><s:textfield name="billing.certnum" required="true" cssStyle="width:95px" onchange="changeStatus();makeMandIssueDt()" cssClass="input-text" maxlength="15" size="15" tabindex=""/></td>
			   <configByCorp:fieldVisibility componentId="component.field.Alternative.AdditionalNoShow">
			     <c:if test="${serviceOrder.job !='STO'}"> 
                    <s:hidden name="billing.additionalNo"/>
                </c:if> 
				
				<c:if test="${serviceOrder.job == 'STO'}">                 
                <td colspan="5">
                <div id="displayDivAdditionalNo" >
                    <table style="vertical-align:bottom;margin-bottom:0px;" >
                    <tr>
                    <td align="right" width="65" class="listwhitetext"></td>
                <td align="right" width="" class="listwhitetext">Additional&nbsp;#</td>
                <td align="left" ><s:textfield name="billing.additionalNo" onchange="changeStatus();" readonly="true" cssClass="input-textUpper" size="15" tabindex=""/><img id="autoAdditionalImage" class="openpopup" align="top" onclick="findAdditionalNo();" src="<c:url value='/images/arrow-loadern.gif'/>"/></td>
                </tr>               
                </table>
                  </div>   
                </td>               
                </c:if> 
               </configByCorp:fieldVisibility>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" width="120px"><div id="isuDtDiv1" ><fmt:message key="billing.issueDate"/></div><div id="isuDtDiv2" style="display:none"><font color="red" size="2">*</font><fmt:message key="billing.issueDate"/></div></td>
				<td align="left">
				<c:if test="${not empty billing.issueDate}">
					<s:text id="FormatedIssueDate" name="${FormDateValue}"><s:param name="value" value="billing.issueDate"/></s:text>
				    <s:textfield id="billingForm_billing_issueDate" name="billing.issueDate" value="%{FormatedIssueDate}" onkeydown="onlyDel(event,this);" readonly="true" cssClass="input-text" cssStyle="width:70px;" onfocus="changeStatus();" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.issueDate}">
					<s:textfield id="billingForm_billing_issueDate" name="billing.issueDate" onkeydown="onlyDel(event,this)"  readonly="true" cssClass="input-text" cssStyle="width:70px;" onfocus="changeStatus();" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_issueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_issueDate"/></td>
		    	<td align="right" class="listwhitetext"><fmt:message key="billing.currency"/></td>
		    	<td align="left" colspan="2"><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.currency}"
		attribute="id=billingForm_billing_currency class=list-menu name=billing.currency style=width:99px  headerKey='' headerValue='' onchange='findExchangeEstRate();changeStatus();' tabindex='' "/>
		    	</td>
<%-- 		    	<s:select name="billing.currency" list="%{currency}" cssClass="list-menu" cssStyle="width:99px" headerKey="" headerValue="" onchange="findExchangeEstRate();changeStatus();" tabindex=""/></td>
 --%>				
				<td align="right" class="listwhitetext" width="160">&nbsp;&nbsp;&nbsp;Insurance&nbsp;Payable&nbsp;Currency&nbsp;</td>
		    	<td align="left" colspan="2">
		    	<configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.billingInsurancePayableCurrency}"
		attribute="id=billingForm_billing_billingInsurancePayableCurrency class=list-menu name=billing.billingInsurancePayableCurrency style=width:80px  headerKey='' headerValue='' onchange='changeStatus();' tabindex='' "/>
		    	</td>
<%-- 		    	<s:select name="billing.billingInsurancePayableCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:80px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
 --%>			
			</tr>
					<tr>
				<td align="right" class="listwhitetext">Insurance&nbsp;Value</td>
				<td align="left" ><s:textfield name="billing.insuranceValueActual" cssStyle="text-align:right" required="true" cssClass="input-text" maxlength="15" size="15" onchange="setDefaultvalue(this);calculateTotal(); onlyFloat(this);calBaseInsVal();changeStatus();makeMandIssueDt();validation();"  tabindex="" /></td>
				<td align="right" class="listwhitetext">Insurance&nbsp;Sell&nbsp;Rate&nbsp;%</td>
				<td align="left"><s:textfield name="billing.insuranceRate" cssStyle="text-align:right"  required="true" cssClass="input-text" maxlength="15" size="15" onchange="calculateTotal(); onlyFloat(this);changeStatus();" tabindex=""/></td>
				<td width="20"></td>
				<td align="right" class="listwhitetext">Ins.Total&nbsp;</td>
				<td align="left"><s:textfield name="billing.totalInsrVal" cssStyle="text-align:right; width:75px"  readonly="true" cssClass="input-text" maxlength="15" size="11" onchange="insOption(); onlyFloat(this);changeStatus();" tabindex=""/></td>
				
				<td align="right" class="listwhitetext">Deductible&nbsp;Amt.</td>
				<td align="left" style=""><configByCorp:customDropDown 	listType="map" list="${claimDeductible}" fieldValue="${billing.deductible}"
		        attribute="id=billingForm_billing_deductible class=list-menu name=billing.deductible  style=width:80px  headerKey='' headerValue='' onchange='changeStatus();'; tabindex='' "/></td>
				
						<c:if test="${empty billing.id}">
							<td width="110px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty billing.id}">
							<c:choose>
								<c:when test="${countBillingValuationNotes == '0' || countBillingValuationNotes == '' || countBillingValuationNotes == null}">
								<td width="110px" align="right"><img id="countBillingValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td width="110px" align="right"><img id="countBillingValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>		
			
			</tr>			
			
				<tr>
				<td align="right" class="listwhitetext">Base&nbsp;Insurance&nbsp;Value</td>
				<td align="left"> <s:textfield name="billing.baseInsuranceValue" cssStyle="text-align:right" cssClass="input-text" maxlength="10" size="10" onchange="onlyFloat(this);calBaseTotal();calInsval();changeStatus();" tabindex="" /></td>
				<td align="right" class="listwhitetext">Insurance&nbsp;Buy&nbsp;Rate&nbsp;%</td>
				<td align="left"> <s:textfield name="billing.insuranceBuyRate" cssStyle="text-align:right; width:95px" cssClass="input-text" maxlength="10" size="10" onchange="onlyFloat(this);calBaseTotal();changeStatus();" tabindex="" /></td>
				<td width="20"></td>
				<td align="right" class="listwhitetext">Base&nbsp;Ins.Total</td>
				<td align="left"> <s:textfield name="billing.baseInsuranceTotal" cssStyle="text-align:right" cssClass="input-textUpper" maxlength="10" size="11" onchange="onlyFloat(this);" tabindex=""  readonly="true" onfocus="changeStatus();"/></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext">Exchange&nbsp;Rate</td>
				<td align="left"> <s:textfield name="billing.exchangeRate" cssStyle="text-align:right" cssClass="input-text" maxlength="10" size="10" onchange="onlyFloat(this);changeStatus(); validation();"  onkeydown="return onlyFloatNumsAllowed(event);" tabindex="" /></td>
				<td align="right" class="listwhitetext">Exchange&nbsp;Date</td>
				<td align="left">
				<c:if test="${not empty billing.valueDate}">
					<s:text id="formatedValueDate" name="${FormDateValue}"><s:param name="value" value="billing.valueDate"/></s:text>
				    <s:textfield id="billingForm_billing_valueDate" name="billing.valueDate" value="%{formatedValueDate}" onkeydown="onlyDel(event,this);changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.valueDate}">
					<s:textfield id="billingForm_billing_valueDate" name="billing.valueDate" onkeydown="onlyDel(event,this)"  readonly="true" cssClass="input-text" cssStyle="width:65px;" onfocus="changeStatus();" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_valueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_valueDate"/></td>
				</td>
			</tr>
			<c:if test="${usertype!='ACCOUNT' || usertype!='AGENT'}">
			<tr>
			   <td align="right" class="listwhitetext">Cportal&nbsp;Insurance&nbsp;Options</td>
			   <%-- <td><s:select cssClass="list-menu" name="billing.noInsurance" list="{}" id="insuranceNo" onchange="showDetailed();" headerKey="" headerValue="" cssStyle="width:120px" /></td> --%>
			  <td><s:textfield name="billing.noInsurance" cssClass="input-textUpper" cssStyle="width:96px;" readonly="true" tabindex=""></s:textfield> </td>
			   <td align="right" class="listwhitetext">Insurance&nbsp;Submit&nbsp;Date</td>
			   <td align="left">
				<c:if test="${not empty billing.insSubmitDate}">
					<s:text id="formatedValueDate" name="${FormDateValue}"><s:param name="value" value="billing.insSubmitDate"/></s:text>
				    <s:textfield id="billingForm_billing_insSubmitDate" name="billing.insSubmitDate" value="%{formatedValueDate}" onkeydown="onlyDel(event,this);changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.insSubmitDate}">
					<s:textfield id="billingForm_billing_insSubmitDate" name="billing.insSubmitDate" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_insSubmitDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_insSubmitDate"/></td>
			<td colspan="3">
			<table style="margin:0px;padding:0px;">
			<tr>			
			<configByCorp:fieldVisibility componentId="component.field.billing.recSignedDisclaimerDate">
				<td align="right" class="listwhitetext" width="272">Received&nbsp;Signed&nbsp;Disclaimer&nbsp;				
				<c:if test="${not empty billing.recSignedDisclaimerDate}">
					<s:text id="formatedValueDate" name="${FormDateValue}"><s:param name="value" value="billing.recSignedDisclaimerDate"/></s:text>
				    <s:textfield id="billingForm_billing_recSignedDisclaimerDate" name="billing.recSignedDisclaimerDate" value="%{formatedValueDate}" onkeydown="onlyDel(event,this);" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.recSignedDisclaimerDate}">
					<s:textfield id="billingForm_billing_recSignedDisclaimerDate" name="billing.recSignedDisclaimerDate" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_recSignedDisclaimerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_recSignedDisclaimerDate"/>
				</td>
				</configByCorp:fieldVisibility>
			<td id="detailed12" style="display:none;" ><input id="detailed" class="cssbutton" type="button" name="detailedList" style="margin-right: 5px;height: 25px;width:100px; font-size:15" value="Detailed List"
			onclick="return opendetailedList();" />  </td>
			</tr>
			</table>
			</td>
			</c:if>
	
	      </tbody>
 		</table>
 		</div></td>
 		  </tr>  
 		 
 		  
 		  </c:if>
 	