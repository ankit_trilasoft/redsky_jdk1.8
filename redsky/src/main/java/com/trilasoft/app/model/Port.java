package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="port")
public class Port extends BaseObject implements ListLinkData{
	private Long id;
	private String portCode;
	private String portName;
	private String country;
	private String countryCode;
	private String mode;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private Date updatedOn;
	private Date createdOn;
	private Boolean active;
	private String refAbbreviation;
	private Boolean ICD ;
	private Boolean isCargoAirport;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("portCode",
				portCode).append("portName", portName).append("country",
				country).append("countryCode", countryCode)
				.append("mode", mode).append("corpID", corpID).append(
						"createdBy", createdBy).append("updatedBy", updatedBy)
				.append("latitude", latitude).append("longitude", longitude)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("active", active).append("refAbbreviation",
						refAbbreviation).append("ICD", ICD).append(
						"isCargoAirport", isCargoAirport).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Port))
			return false;
		Port castOther = (Port) other;
		return new EqualsBuilder().append(id, castOther.id).append(portCode,
				castOther.portCode).append(portName, castOther.portName)
				.append(country, castOther.country).append(countryCode,
						castOther.countryCode).append(mode, castOther.mode)
				.append(corpID, castOther.corpID).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(latitude,
						castOther.latitude).append(longitude,
						castOther.longitude).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(active, castOther.active)
				.append(refAbbreviation, castOther.refAbbreviation).append(ICD,
						castOther.ICD).append(isCargoAirport,
						castOther.isCargoAirport).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(portCode).append(
				portName).append(country).append(countryCode).append(mode)
				.append(corpID).append(createdBy).append(updatedBy).append(
						latitude).append(longitude).append(updatedOn).append(
						createdOn).append(active).append(refAbbreviation)
				.append(ICD).append(isCargoAirport).toHashCode();
	}
	@Column(length=45)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=100)
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Column(length=100)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the mode
	 */
	@Column(length=10)
	public String getMode() {
		return mode;
	}
	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column(length=200)
	public String getPortCode() {
		return portCode;
	}
	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}
	@Column(length=200)
	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	@Column(length=100)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Transient
	public String getListCode() {
		return portCode;
	}

	@Transient
	public String getListDescription() {
		return this.portName;
	}

	@Transient
	public String getListSecondDescription() {
		if(this.longitude==null)
		{
			this.longitude=new BigDecimal("0.00"); 
		}
		return this.longitude.toString();
	}

	@Transient
	public String getListThirdDescription() {
		if(this.latitude==null)
		{
			this.latitude=new BigDecimal("0.00"); 
		}
		return this.latitude.toString();
	}

	@Transient
	public String getListFourthDescription() {
		return "";
	}

	@Transient
	public String getListFifthDescription() {
		return "";
	}

	@Transient
	public String getListSixthDescription() {
		return "";
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	/**
	 * @return the countryCode
	 */
	@Column(length=3)
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the latitude
	 */
	@Column
	public BigDecimal getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	@Column
	public BigDecimal getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@Column
	public Boolean getICD() {
		return ICD;
	}
	public void setICD(Boolean icd) {
		ICD = icd;
	}
	
	@Column(length=50)
	public String getRefAbbreviation() {
		return refAbbreviation;
	}
	public void setRefAbbreviation(String refAbbreviation) {
		this.refAbbreviation = refAbbreviation;
	}
	/**
	 * @return the isCargoAirport
	 */
	@Column
	public Boolean getIsCargoAirport() {
		return isCargoAirport;
	}
	/**
	 * @param isCargoAirport the isCargoAirport to set
	 */
	public void setIsCargoAirport(Boolean isCargoAirport) {
		this.isCargoAirport = isCargoAirport;
	}
}
