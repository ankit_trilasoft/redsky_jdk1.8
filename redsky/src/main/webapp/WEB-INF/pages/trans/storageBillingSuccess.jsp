<%@ include file="/common/taglibs.jsp"%> 

<head> 

<title><fmt:message key="executeRules.title"/></title> 
<meta name="heading" content="<fmt:message key='executeRules.heading'/>"/> 
</head> 
<script language="javascript" type="text/javascript">
<%@ include file="/scripts/progressbar.js"%>
</script>
<s:form id="storageBillingSuccess" name="storageBillingSuccess" action="" method="post" validate="true">   
 <div id="Layer1" align="center">
<table class='searchLabel' cellspacing="2" cellpadding="4" border="0" align="center">

<tbody>
	<c:if test="${not empty presentCount}" >
      <tr>
      <td align="right" class="listwhitetext" width="700px"><h1><b>${presentCount}/${totCount} has been processed</b></h1></td>
    </tr>
      
      <tr>
      <td align="right">	
	<script type="text/javascript">
		var myProgBar = new progressBar(
			1,         //border thickness
			'#000000', //border colour
			'#f8f8ff', //background colour
			'#0000cd', //bar colour
			340,       //width of bar (excluding border)
			20,        //height of bar (excluding border)
			1          //direction of progress: 1 = right, 2 = down, 3 = left, 4 = up
		);
	</script>
	<script type="text/javascript">
	var presentCount = '${presentCount}';
	var totCount = '${totCount}';
	var finalPercentage = presentCount/totCount;
	myProgBar.setBar(finalPercentage);       //set the progress bar to indicate a progress of 50%
	//myProgBar.setBar(0.1,true);  //add 10% to the progress bar's progress
	//myProgBar.setBar(1.1,false); //subtract 10% from the progress bar's progress
	myProgBar.setCol('#023A73'); //change the colour of the progress bar
	//alert( 'Current progress is ' + myProgBar.amt ) // read current progress
	
	</script>
	
      </td>
     </tr>
     </c:if> 

</tbody></table>
</div>      
</s:form>

<script language="javascript" type="text/javascript">setTimeout("location.reload();",10000);</script>


