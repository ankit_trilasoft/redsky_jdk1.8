package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CompanyDao;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.CompanyManager;

public class CompanyManagerImpl extends GenericManagerImpl<Company, Long> implements CompanyManager {
	CompanyDao companyDao; 
    
	 
    public CompanyManagerImpl(CompanyDao companyDao) { 
        super(companyDao); 
        this.companyDao = companyDao; 
    }

    public List<Company> findByCorpID(String corpId){
    	return companyDao.findByCorpID(corpId);
    }
    public String findByCompanyDivis(String corpID){
    	return companyDao.findByCompanyDivis(corpID);             
    }

	public List getcorpID() {
		// TODO Auto-generated method stub
		return companyDao.getcorpID();
	}

	public List getcompanyName(String corpID) {
		// TODO Auto-generated method stub
		return companyDao.getcompanyName(corpID);
	}

	public List getDateDiffExp(String username, String sessionCorpID, Date passwordExp) {
		return companyDao.getDateDiffExp(username, sessionCorpID, passwordExp);
	}

	public List getDateDiffAlert(String username, String sessionCorpID, Date passwordExp, String pwdExpDate) {
		return companyDao.getDateDiffAlert(username, sessionCorpID, passwordExp, pwdExpDate);
	}

	public boolean findTracInternalUser(String sessionCorpID) {
		return companyDao.findTracInternalUser(sessionCorpID);
	}
	
	public List findCorpIdList(){
		return companyDao.findCorpIdList();
	}
	public String findAutomaticLinkup(String externalCorpID){
		return companyDao.findAutomaticLinkup(externalCorpID);
	}
	public String defaultBillingRate(String tempCorpId){
		return companyDao.defaultBillingRate(tempCorpId);
	}
	public boolean getCompanySurveyFlag(String sequenceNumber){
		return companyDao.getCompanySurveyFlag(sequenceNumber);
	}

	public void updateStorageBillingStatus(String sessionCorpID, boolean storageBillingStatus) {
		companyDao.updateStorageBillingStatus(sessionCorpID, storageBillingStatus);
		
	}
	public String getcorpIDName(String companyName){
		return companyDao.getcorpIDName(companyName);
	}
	public List getcompanyListBycompanyView( String sessionCorpID,String companyName, String countryID,String billGroup, String status) {
		return companyDao.getcompanyListBycompanyView(sessionCorpID, companyName, countryID, billGroup, status);
	}
	public List getcorpIDList(){
		return companyDao.getcorpIDList();
	}
	public List findCorpIdAgentList(){
		return companyDao.findCorpIdAgentList();
		}

	public int updateVendorExtractDate(String sessionCorpID,String userName) {
		return companyDao.updateVendorExtractDate(sessionCorpID, userName);
	}

	public int updateBillToExtractDate(String sessionCorpID,String userName) {
		return companyDao.updateBillToExtractDate(sessionCorpID, userName);
	}
}
