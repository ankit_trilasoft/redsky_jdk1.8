<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
			document.forms['partnerListForm'].elements['partner.aliasName'].value = "";
}

function setAccount(){
document.forms['partnerListForm'].elements['partnerType'].value = "AC";
document.forms['partnerListForm'].elements['findFor'].value = "account";

document.forms['partnerListForm'].submit();
}
function setPrivate(){
document.forms['partnerListForm'].elements['partnerType'].value = "DE";
document.forms['partnerListForm'].elements['findFor'].value = "account";

document.forms['partnerListForm'].submit();
}
function setAgent(){
document.forms['partnerListForm'].elements['partnerType'].value = "AG";
document.forms['partnerListForm'].elements['findFor'].value = "account";

document.forms['partnerListForm'].submit();
}

</script>

<style>

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-32.5px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>

</head>
<!-- 
<c:set var="forms"> 
<s:form id="partnerListDetailForm" action='${empty param.popup?"editPartner.html":"editPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="id"/>
<s:submit cssClass="button" method="edit" key="button.viewDetail"/>
</s:form>
</c:set>
 -->
  

 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="popupList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"extractPartnersPopup.html":"extractPartnersPopup.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>

<!-- Enhansment after 27-02-2008 -->

<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="firstName" value="<%= request.getParameter("firstName")%>" />
<s:hidden name="firstName" value="<%= request.getParameter("firstName")%>" />
<c:set var="lastName" value="<%= request.getParameter("lastName")%>" />
<s:hidden name="lastName" value="<%= request.getParameter("lastName")%>" />

<s:hidden name="compDiv" value="<%=request.getParameter("compDiv") %>"/>
<c:set var="compDiv" value="<%=request.getParameter("compDiv") %>"/>

<s:hidden name="flag" value="<%= request.getParameter("flag")%>" />
<c:set var="flag" value="<%= request.getParameter("flag")%>" />

<!-- Enhansment after 27-02-2008 -->

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:800px;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<th><fmt:message key="partner.billingCountryCode"/></th>
<th><fmt:message key="partner.billingState"/></th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partner.lastName" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partner.aliasName" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partner.billingCountryCode" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partner.billingState" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			</tr>
			<tr>
			<td colspan="4"></td>
			<td width="130px">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<div id="newmnav">   
 <ul>
 <c:choose>
 	<c:when test="${partnerType == 'DE'}">  
 		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setPrivate();"><span>Private<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setAccount();"><span>Account</span></a></li>
		<li><a onclick="setAgent();"><span>Agents</span></a></li>
	</c:when>
	<c:when test="${partnerType == 'AC'}"> 
		<li><a onclick="setPrivate();"><span>Private</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAccount();"><span>Account<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setAgent();"><span>Agents</span></a></li>
	</c:when>
	<c:otherwise>
		<li><a onclick="setPrivate();"><span>Private</span></a></li>
		<li><a onclick="setAccount();"><span>Account</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAgent();"><span>Agents<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</c:otherwise>
</c:choose>
</ul>
</div><div class="spn" style="width:800px">&nbsp;</div><br>
 

<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:100%" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" href="editPartnerAddForm.html" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" ><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column title="Alias Name" sortable="true" ><c:out value="${partnerList.aliasName}"/></display:column>
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:35px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:35px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:110px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
    
    <c:if test="${param.popup}">
    <c:choose>
    <c:when test="${!(partnerType == 'PP')}">  
    	<display:column style="width:80px;cursor:pointer;"><A onclick="location.href='<c:url value="/viewPartner.html?id=${partnerList.id}&partnerType=${partnerType}&firstName=${firstName}&lastName=${lastName}&mode=view&flag=${flag}&compDiv=${compDiv}&type=DE&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"/>'">View Detail</A></display:column>
    </c:when>
    <c:otherwise>
    	<display:column style="width:80px;cursor:pointer;"><A onclick="location.href='<c:url value="/editPrivatePartner.html?id=${partnerList.id}&partnerType=${partnerType}&firstName=${firstName}&lastName=${lastName}&mode=view&flag=${flag}&compDiv=${compDiv}&decorator=popup&popup=true"/>'">View Detail</A></display:column>
    </c:otherwise>
    </c:choose>
    </c:if>
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
<c:if test="${partnerType == 'PP'}">  
<input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerFormPopup.html?firstName=${firstName}&lastName=${lastName}&flag=${flag}&compDiv=${compDiv}&decorator=popup&popup=true"/>'"  
        value="<fmt:message key="button.add"/>"/> 
</c:if>
</div>

<c:set var="isTrue" value="false" scope="session"/> 
</s:form>

<script type="text/javascript">   
   //highlightTableRows("partnerList"); 
   Form.focusFirstElement($("partnerListForm"));      
</script>