<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Standard Deductions Details</title>   
    <meta name="heading" content="Standard Deductions Details"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="JavaScript">
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}

function chekcPercent(targetElement){
	if(targetElement.value > 100){
		document.forms['standardDeductionForm'].elements['standardDeductions.rate'].value = '';
		alert('Rate % should be within 100.');
		return false;
	}
}

function changeStatus(){
	document.forms['standardDeductionForm'].elements['formStatus'].value = '1';
}

function checkMaxDedAmt(){
	var amt = document.forms['standardDeductionForm'].elements['standardDeductions.amount'].value;
	var rate = document.forms['standardDeductionForm'].elements['standardDeductions.rate'].value;
	var maxDedAmt = Number(amt) + Number((amt*rate)/100);
	document.forms['standardDeductionForm'].elements['standardDeductions.maxDeductAmt'].value = maxDedAmt;
	
	document.forms['standardDeductionForm'].elements['standardDeductions.deduction'].value = Number((amt*rate)/100);
	var amtDeduction = document.forms['standardDeductionForm'].elements['standardDeductions.amtDeducted'].value;
	document.forms['standardDeductionForm'].elements['standardDeductions.balanceAmount'].value=Number(maxDedAmt)-Number(amtDeduction);
	dedValidate();
}

function changeSDStatus(){
	var status = document.forms['standardDeductionForm'].elements['standardDeductions.status'].value;
	if(status == 'Closed'){
		var amtDeduction = document.forms['standardDeductionForm'].elements['standardDeductions.amtDeducted'].value;
	 	var maxAmtDeduction = document.forms['standardDeductionForm'].elements['standardDeductions.maxDeductAmt'].value;
	 	if(amtDeduction != maxAmtDeduction){
	 		alert('This standard deduction cannot be closed, amount is pending');
	 		document.forms['standardDeductionForm'].elements['standardDeductions.status'].text = '';
			document.forms['standardDeductionForm'].elements['standardDeductions.status'].value = '';
	 		return false;
	 	}
	}	
}

function validateFields(){
	if(document.forms['standardDeductionForm'].elements['standardDeductions.chargeCode'].value == ''){
		alert('Please select charge code.');
		document.forms['standardDeductionForm'].elements['standardDeductions.chargeCode'].focus();
		return false;
	}
	if(document.forms['standardDeductionForm'].elements['standardDeductions.lastDeductionDate'].value == ''){
		alert('Please enter the last deduction date.');
		document.forms['standardDeductionForm'].elements['standardDeductions.lastDeductionDate'].focus();
		return false;
	}
	var status = document.forms['standardDeductionForm'].elements['standardDeductions.status'].value;
	if(status == 'Closed'){
		var amtDeduction = document.forms['standardDeductionForm'].elements['standardDeductions.amtDeducted'].value;
	 	var maxAmtDeduction = document.forms['standardDeductionForm'].elements['standardDeductions.maxDeductAmt'].value;
	 	if(amtDeduction != maxAmtDeduction){
	 		alert('This standard deduction cannot be closed, amount is pending');
	 		document.forms['standardDeductionForm'].elements['standardDeductions.status'].text = '';
			document.forms['standardDeductionForm'].elements['standardDeductions.status'].value = '';
	 		return false;
	 	}
	}
}

function dedValidate(){
	var maxAmtDeduction = document.forms['standardDeductionForm'].elements['standardDeductions.maxDeductAmt'].value;
	var deduction = document.forms['standardDeductionForm'].elements['standardDeductions.deduction'].value;
	if(Number(deduction) > Number(maxAmtDeduction) && Number(maxAmtDeduction)>0){
		alert('Amount exceeding the max amount, please re-enter.');
		document.forms['standardDeductionForm'].elements['standardDeductions.deduction'].value= '';
	}
}

function openSDChargePopWindow(){
	javascript:openWindow('chargess.html?contract=Driver Settlement&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=standardDeductions.chargeCodeDesc&fld_code=standardDeductions.chargeCode');
}

function findChargeCode(){
	var chargeCode = document.forms['standardDeductionForm'].elements['standardDeductions.chargeCode'].value;
    var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract=Driver Settlement&chargeCode="+chargeCode; 
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse4;
    http2.send(null);
}

function handleHttpResponse4(){
		if (http2.readyState == 4){
         var results = http2.responseText
         results = results.replace('[','');
         results=results.replace(']','');
         results = results.trim();
         var res = results.split("#"); 
             if(res.size() >= 2){
 					document.forms['standardDeductionForm'].elements['standardDeductions.chargeCodeDesc'].value = res[4];
 				}else{
                     alert("Invalid Charge Code, please select another");
                    
					 document.forms['standardDeductionForm'].elements['standardDeductions.chargeCodeDesc'].value="";
					 document.forms['standardDeductionForm'].elements['standardDeductions.chargeCode'].value="";
					 document.forms['standardDeductionForm'].elements['standardDeductions.chargeCode'].select();
               }
   		}
}


var http2 = getHTTPObject();

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function chkSelect()
	{
	if (checkFloatWithArth('standardDeductionForm','standardDeductions.amount','Invalid data in amount') == false)
           {
              document.forms['standardDeductionForm'].elements['standardDeductions.amount'].value='0';
              document.forms['standardDeductionForm'].elements['standardDeductions.maxDeductAmt'].value='0';
              document.forms['standardDeductionForm'].elements['standardDeductions.balanceAmount'].value='0';
              document.forms['standardDeductionForm'].elements['standardDeductions.deduction'].value='0';                         
              return false
           }
	}
function chkSelect1()
	{
	if (checkFloatWithArth('standardDeductionForm','standardDeductions.deduction','Invalid data in Deduction Per Period') == false)
           {
              document.forms['standardDeductionForm'].elements['standardDeductions.deduction'].value='0';                         
              return false
           }
	}
</script>
</head> 
<style>
.mainDetailTable {
	width:650px;
}
</style>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="OO" />
<c:set var="ppType" value="OO"/>

<s:form id="standardDeductionForm" action="saveStandardDeduction.html" method="post" validate="true">   
<s:hidden name="partner.id" />
<s:hidden name="standardDeductions.id" />
<s:hidden name="partnerId" value="${partner.id}"/>
<s:hidden name="standardDeductions.corpID" />
<s:hidden name="standardDeductions.partnerCode" />

<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="description" />

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<div id="newmnav">
	<ul>
	  	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=OO"><span>Partner Detail</span></a></li>
		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=OO"><span>Acct Ref</span></a></li>
		<li><a href="standardDeductionsList.html?id=${partner.id}&partnerType=OO"><span>Standard Deductions List</span></a></li>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Standard Deduction Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		<c:if test='${paramValue == "View"}'>
			<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
		</c:if>
		<c:if test='${paramValue != "View"}'>
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
	</ul>
</div>

		<div class="spn" style="!margin-bottom:2px;">&nbsp;</div>
		
		
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="10px"></td>
					  		<td align="right" class="listwhitetext">Partner Code</td>
							<td><s:textfield cssClass="input-textUpper" key="partner.partnerCode" required="true" readonly="true"  size="15" tabindex="1"/></td>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext" width="85px">Partner Name</td>
							<td><s:textfield key="partner.lastName" cssClass="input-textUpper" required="true" size="35" readonly="true" tabindex="2"/></td>
							<td align="right" class="listwhitetext">Partner Status</td>
							<td><s:textfield cssClass="input-textUpper" key="partner.status" required="true" readonly="true"  size="15" tabindex="3"/></td>
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="10px"></td>
							<td align="right" class="listwhitetext">Charge Code<font color="red" size="2">*</font></td>
							<td><s:textfield cssClass="input-text" name="standardDeductions.chargeCode" size="15" maxlength="25" onchange="findChargeCode();" tabindex="4"/>
							<td><img class="openpopup" width="17" height="20" onclick="changeStatus();openSDChargePopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="right" class="listwhitetext">Description</td>
							<td><s:textfield cssClass="input-text" name="standardDeductions.chargeCodeDesc"  size="35" maxlength="50" tabindex="5"/></td>
							<td align="right" class="listwhitetext">Deductions Status</td>
							<td><s:select cssClass="list-menu" name="standardDeductions.status" list="{'','Open','Hold','Cancel','Closed'}" cssStyle="width:105px" onchange="return changeSDStatus();" tabindex="6"/></td>
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="10px"></td>
							<td align="right" class="listwhitetext">Amount</td>
							<td><s:textfield cssClass="input-text" name="standardDeductions.amount" cssStyle="text-align:right" size="15" maxlength="19" onblur="chkSelect();" tabindex="7" onchange="checkMaxDedAmt();"/></td>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext">Annual Int. Rate</td>
							<td><s:textfield cssClass="input-text" name="standardDeductions.rate"  size="15" maxlength="3" tabindex="8" onchange="onlyFloat(this),checkMaxDedAmt();return chekcPercent(this);"/>%</td>
							<td align="right" class="listwhitetext">Max Amount Deduction</td>
							<td><s:textfield cssClass="input-textUpper" name="standardDeductions.maxDeductAmt" cssStyle="text-align:right" size="15" maxlength="19" tabindex="9" onchange="onlyFloatNumsAllowed(this);" readonly="true"/></td>
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="10px"></td>
							<td align="right" class="listwhitetext">Deduction Per Period</td>
							<td><s:textfield cssClass="input-text" name="standardDeductions.deduction"  size="15" cssStyle="text-align:right" maxlength="19" onblur="chkSelect1();" tabindex="10" onchange="dedValidate();"/></td>
						   <td align="right" width="" colspan="2" class="listwhitetext">Trip</td>
							<td><s:textfield cssClass="input-text" name="standardDeductions.serviceorder" size="15" maxlength="15" tabindex="11"/></td>
							<td align="right" class="listwhitetext">Balance&nbsp;Amount</td>
							<td><s:textfield cssClass="input-textUpper" name="standardDeductions.balanceAmount" cssStyle="text-align:right" size="15" maxlength="19" readonly="true" tabindex="12"/></td>
													
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
						<td></td>
		<c:set var="personalEscrow" value="false" />
					<c:if test="${standardDeductions.personalEscrow}">
						<c:set var="personalEscrow" value="true" />
					</c:if>
					<td align="right" class="listwhitetext">Personal&nbsp;Escrow</td>
					<td align="left" ><s:checkbox key="standardDeductions.personalEscrow" value="${personalEscrow}" fieldValue="true" onclick="changeStatus();" 
					cssStyle="vertical-align:middle;" tabindex="13"/></td>   
					
							<td align="right" class="listwhitetext" colspan="2">Deduction Date<font color="red" size="2">*</font></td>
							<c:if test="${not empty standardDeductions.lastDeductionDate}">
								<s:text id="deductionDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="standardDeductions.lastDeductionDate"/></s:text>
								<td><s:textfield cssClass="input-text" id="lastDeductionDate" name="standardDeductions.lastDeductionDate" value="%{deductionDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="lastDeductionDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty standardDeductions.lastDeductionDate}">
								<td><s:textfield cssClass="input-text" id="lastDeductionDate" name="standardDeductions.lastDeductionDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="lastDeductionDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td width="150px" align="right" class="listwhitetext">Amount Deducted Till Date</td>
							<td><s:textfield cssClass="input-textUpper" name="standardDeductions.amtDeducted" readonly="true"  size="15" maxlength="19" tabindex="14" onchange="onlyFloat(this);"/></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
  
<table>
		<tbody>
					<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdOn'/></b></td>
							<td valign="top"></td>
							<td style="width:120px">
							<fmt:formatDate var="agentBaseCreatedOnFormattedValue" value="${standardDeductions.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="standardDeductions.createdOn" value="${agentBaseCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${standardDeductions.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdBy' /></b></td>
							<c:if test="${not empty standardDeductions.id}">
								<s:hidden name="standardDeductions.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{standardDeductions.createdBy}"/></td>
							</c:if>
							<c:if test="${empty standardDeductions.id}">
								<s:hidden name="standardDeductions.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedOn'/></b></td>
							<fmt:formatDate var="agentBaseupdatedOnFormattedValue" value="${standardDeductions.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="standardDeductions.updatedOn" value="${agentBaseupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${standardDeductions.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedBy' /></b></td>
							<c:if test="${not empty standardDeductions.id}">
								<s:hidden name="standardDeductions.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{standardDeductions.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty standardDeductions.id}">
								<s:hidden name="standardDeductions.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
		</tbody>
</table>               
<s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px;margin-right:5px;" tabindex="15" onclick="return validateFields(); return changeSDStatus();"/>   
<s:reset type="button" cssClass="cssbutton1" key="Reset" />
<c:if test="${not empty standardDeductions.id}">
	<input type="button" class="cssbutton1" tabindex="16" onclick="location.href='<c:url value="/editStandardDeduction.html?partnerId=${partner.id}"/>'" value="<fmt:message key="button.add"/>"/>
</c:if>
<table><tr><td height="80px"></td></tr></table>
</s:form>
<script type="text/javascript">   
    <c:if test="${hitflag == 1}" >
    	<c:redirect url="/editStandardDeduction.html?partnerId=${partner.id}&id=${standardDeductions.id}"></c:redirect>
	</c:if>  
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>