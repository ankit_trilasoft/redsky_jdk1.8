<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="carrierList.title"/></title>   
    <meta name="heading" content="<fmt:message key='carrierList.heading'/>"/>   
</head>   
  
<s:set name="carriers" value="carriers12" scope="request"/>   
<display:table name="carriers" class="table" requestURI="" id="carrierList12" export="${empty param.popup}" pagesize="10" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
	<c:if test="${empty param.popup}">   
    
    <display:column property="carrierNumber" sortable="true" titleKey="carrier.carrierNumber"     
        paramId="id" paramProperty="id"/>   
        </c:if>		
	<c:if test="${param.popup}">
	<display:column property="listLinkParams" sortable="true" titleKey="carrier.carrierNumber"/>   
    </c:if>
    <display:column property="carrierName" sortable="true" titleKey="carrier.carrierName"/>
    <display:column property="miles" sortable="true" titleKey="carrier.miles"/>
    <display:column property="carrierDeparture" sortable="true" titleKey="carrier.carrierDeparture"/>
    <display:column property="carrierTD" sortable="true" titleKey="carrier.carrierTD" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="carrierArrival" sortable="true" titleKey="carrier.carrierArrival"/>
    <display:column property="carrierTA" sortable="true" titleKey="carrier.carrierTA" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="shipNumber" sortable="true" titleKey="serviceOrder.shipNumber"/>
</display:table>   
   </div>
<script type="text/javascript">   
    highlightTableRows("carrierList12");   
</script>  