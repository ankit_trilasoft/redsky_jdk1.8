package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.velocity.runtime.directive.Foreach;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.appfuse.model.User;

import com.trilasoft.app.dao.AuditTrailDao;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTOUserDataSecurity;
import com.trilasoft.app.model.AuditTrail;

public class AuditTrailDaoHibernate extends GenericDaoHibernate<AuditTrail, Long> implements AuditTrailDao {

	public AuditTrailDaoHibernate() {   
        super(AuditTrail.class);   
    }
	public List getAuditTrailListNew(Long xtranId, String tableName, String corpID) {

			String query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where tableName  in("+tableName+") AND corpID = '"+corpID+"' AND xtranId='"+xtranId+"' ORDER BY id desc";
			System.out.println(query);
			List idList= new ArrayList();
			idList=this.getSession().createSQLQuery(query).list();
			System.out.println(idList);
			List <Object> dataSecurityList = new ArrayList<Object>();
			DTOUserDataSecurity dtoUserDataSecurity;
			Iterator it1=idList.iterator();
			while(it1.hasNext())
			{
				Object []obj=(Object[]) it1.next();
				dtoUserDataSecurity=new DTOUserDataSecurity();
				   if(obj[0]!=null){
					   dtoUserDataSecurity.setXtranId(obj[0]);
					   }
					   else{
						   dtoUserDataSecurity.setXtranId("");
					   }
				   if(obj[1]!=null){
					   dtoUserDataSecurity.setTableName(obj[1]);
					   }
					   else{
						   dtoUserDataSecurity.setTableName("");
					   }
				   if(obj[2]!=null){
					   dtoUserDataSecurity.setFieldName(obj[2]);
					   }
					   else{
						   dtoUserDataSecurity.setFieldName("");
					   }
				   if(obj[3]!=null){
					   dtoUserDataSecurity.setDescription(obj[3]);
					   }
					   else{
						   dtoUserDataSecurity.setDescription("");
					   }
				   if(obj[4]!=null){
					   dtoUserDataSecurity.setOldValue(obj[4]);
					   }
					   else{
						   dtoUserDataSecurity.setOldValue("");
					   }
				   if(obj[5]!=null){ 
					   dtoUserDataSecurity.setNewValue(obj[5]);
					   }
					   else{
						   dtoUserDataSecurity.setNewValue("");
					   }
				   if(obj[6]!=null){
					   dtoUserDataSecurity.setUser(obj[6]);
					   }
					   else{
						   dtoUserDataSecurity.setUser("");
					   }
				   if(obj[7]!=null){
					   dtoUserDataSecurity.setDated(obj[7]);
					   }
					   else{
						   dtoUserDataSecurity.setDated("");
					   }
				   if(obj[8]!=null){
					   dtoUserDataSecurity.setAlertRole(obj[8]);
					   }
					   else{
						   dtoUserDataSecurity.setAlertRole("");
					   }
				   if(obj[9]!=null){
					   dtoUserDataSecurity.setAlertUser(obj[9]);
					   }
					   else{
						   dtoUserDataSecurity.setAlertUser("");
					   }
				   if(obj[10]!=null){
					   dtoUserDataSecurity.setIsViewed(obj[10]);
					   }
					   else{
						   dtoUserDataSecurity.setIsViewed("");
					   }
				/*   if(obj[11]!=null){
					   dtoUserDataSecurity.setAlertFileNumber(obj[11]);
					   }
					   else{
						   dtoUserDataSecurity.setAlertFileNumber("");
					   }*/
				   if(obj[1].toString().equalsIgnoreCase("myfile")){
					   if(obj[11]!=null && obj[11].toString().contains("#") ){
						   String myfileAlertFile[]=obj[11].toString().split("#");
						dtoUserDataSecurity.setAlertFileNumber(myfileAlertFile[0]);
							   }else{
							dtoUserDataSecurity.setAlertFileNumber(obj[11]);
							   }
				   }else{
					   if(obj[11]!=null){
						  dtoUserDataSecurity.setAlertFileNumber(obj[11]);
						   }
						   else{
						dtoUserDataSecurity.setAlertFileNumber("");
						   }  
				   }
				   if(obj[12]!=null){
					   dtoUserDataSecurity.setAlertShipperName(obj[12]);
					   }
					   else{
						   dtoUserDataSecurity.setAlertShipperName("");
					   }
				   if(obj[13]!=null){
					   dtoUserDataSecurity.setId(obj[13]);
				   }else{
					   dtoUserDataSecurity.setId("0");
				   }
				   if(obj[1].toString().equalsIgnoreCase("myfile")){
					   if(obj[11]!=null && obj[11].toString().contains("#")){
						   String myfileAlertFile[]=obj[11].toString().split("#");
						dtoUserDataSecurity.setDetailsId(myfileAlertFile[1]);
							   }else{
							dtoUserDataSecurity.setDetailsId("");
							   }
				   }else{
					   dtoUserDataSecurity.setDetailsId("");   
				   }
				   
				   List soIdList = new ArrayList();
				   if(((obj[0]!=null && !obj[0].toString().equalsIgnoreCase("")) && (obj[1]!=null && !obj[1].toString().equalsIgnoreCase(""))) && (obj[1].toString().equalsIgnoreCase("container") || obj[1].toString().equalsIgnoreCase("servicepartner"))){
					 String soQuery = "select serviceorderId from "+obj[1]+" where id='"+obj[0]+"'"  ;
					 soIdList = this.getSession().createSQLQuery(soQuery).list();
					 if(soIdList!=null && !soIdList.isEmpty() && soIdList.get(0)!=null){
						 dtoUserDataSecurity.setSoId(soIdList.toString().replace("[", "").replace("]", ""));
					 }else{
						 dtoUserDataSecurity.setSoId("");
					 }
				   }
				   
				dataSecurityList.add(dtoUserDataSecurity);
			}
			return dataSecurityList;	
		
	}
	public List getAuditTrailPartnerList(Long xtranId, String tableName, String corpID,String from) {
		String query="";
			query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where tableName  in("+tableName+") AND corpID in ('"+corpID+"','TSFT') AND xtranId='"+xtranId+"' ORDER BY id desc";
		List list=new ArrayList();
		list=this.getSession().createSQLQuery(query).list();
		List<Object> auditList=new ArrayList<Object>();
		DTOUserDataSecurity dtoUserDataSecurity;
		Iterator it=list.iterator();
		while(it.hasNext()){
			Object []obj=(Object[])it.next();
			dtoUserDataSecurity=new DTOUserDataSecurity();
			if(obj[0]!=null){
				   dtoUserDataSecurity.setXtranId(obj[0]);
				   }
				   else{
					   dtoUserDataSecurity.setXtranId("");
				   }
			   if(obj[1]!=null){
				   dtoUserDataSecurity.setTableName(obj[1]);
				   }
				   else{
					   dtoUserDataSecurity.setTableName("");
				   }
			   if(obj[2]!=null){
				   dtoUserDataSecurity.setFieldName(obj[2]);
				   }
				   else{
					   dtoUserDataSecurity.setFieldName("");
				   }
			   if(obj[3]!=null){
				   dtoUserDataSecurity.setDescription(obj[3]);
				   }
				   else{
					   dtoUserDataSecurity.setDescription("");
				   }
			   if(obj[4]!=null){
				   dtoUserDataSecurity.setOldValue(obj[4]);
				   }
				   else{
					   dtoUserDataSecurity.setOldValue("");
				   }
			   if(obj[5]!=null){ 
				   dtoUserDataSecurity.setNewValue(obj[5]);
				   }
				   else{
					   dtoUserDataSecurity.setNewValue("");
				   }
			   if(obj[6]!=null){
				   dtoUserDataSecurity.setUser(obj[6]);
				   }
				   else{
					   dtoUserDataSecurity.setUser("");
				   }
			   if(obj[7]!=null){
				   dtoUserDataSecurity.setDated(obj[7]);
				   }
				   else{
					   dtoUserDataSecurity.setDated("");
				   }
			   if(obj[8]!=null){
				   dtoUserDataSecurity.setAlertRole(obj[8]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertRole("");
				   }
			   if(obj[9]!=null){
				   dtoUserDataSecurity.setAlertUser(obj[9]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertUser("");
				   }
			   if(obj[13]!=null){
				   dtoUserDataSecurity.setIsViewed(obj[10]);
				   }
				   else{
					   dtoUserDataSecurity.setIsViewed("");
				   }
			  
		auditList.add(dtoUserDataSecurity);
		}
		return auditList;
	}
	
	public List getFullAuditTrailPartnerList(Long xtranId, String tableName, String corpID,String from,String secondID) {
		String query="";
		if(secondID!=null && !secondID.equalsIgnoreCase("")){
		query=	"SELECT distinct a.xtranId as xtranId, a.tableName as tableName, a.fieldName as fieldName , b.description as description, oldValue as oldValue, newValue as newValue, user as user,dated as dated, a.alertRole as alertRole, alertUser as alertUser, isViewed as isViewed, alertFileNumber as alertFileNumber, alertShipperName as alertShipperName,a.id as id" 
			     + " from history a,auditSetup b where a.tableName  in ("+tableName+") AND a.corpID in ('"+corpID+"','TSFT') AND b.corpID = '"+corpID+"' AND a.xtranId in ('"+xtranId+"','"+secondID+"') and a.tableName=b.tableName and b.fieldName=a.fieldName and b.auditable='true' ORDER BY id desc";
		}else{
			query=	"SELECT distinct a.xtranId as xtranId, a.tableName as tableName, a.fieldName as fieldName , b.description as description, oldValue as oldValue, newValue as newValue, user as user,dated as dated, a.alertRole as alertRole, alertUser as alertUser, isViewed as isViewed, alertFileNumber as alertFileNumber, alertShipperName as alertShipperName,a.id as id" 
				     + " from history a,auditSetup b where a.tableName  in ("+tableName+") AND a.corpID in ('"+corpID+"','TSFT') AND b.corpID = '"+corpID+"' AND a.xtranId='"+xtranId+"' and a.tableName=b.tableName and b.fieldName=a.fieldName and b.auditable='true' ORDER BY id desc";			
		}
			List list=new ArrayList();
		list=this.getSession().createSQLQuery(query).list();
		List<Object> auditList=new ArrayList<Object>();
		DTOUserDataSecurity dtoUserDataSecurity;
		Iterator it=list.iterator();
		while(it.hasNext()){
			Object []obj=(Object[])it.next();
			dtoUserDataSecurity=new DTOUserDataSecurity();
			if(obj[0]!=null){
				   dtoUserDataSecurity.setXtranId(obj[0]);
				   }
				   else{
					   dtoUserDataSecurity.setXtranId("");
				   }
			   if(obj[1]!=null){
				   dtoUserDataSecurity.setTableName(obj[1]);
				   }
				   else{
					   dtoUserDataSecurity.setTableName("");
				   }
			   if(obj[2]!=null){
				   dtoUserDataSecurity.setFieldName(obj[2]);
				   }
				   else{
					   dtoUserDataSecurity.setFieldName("");
				   }
			   if(obj[3]!=null){
				   dtoUserDataSecurity.setDescription(obj[3]);
				   }
				   else{
					   dtoUserDataSecurity.setDescription("");
				   }
			   if(obj[4]!=null){
				   dtoUserDataSecurity.setOldValue(obj[4]);
				   }
				   else{
					   dtoUserDataSecurity.setOldValue("");
				   }
			   if(obj[5]!=null){ 
				   dtoUserDataSecurity.setNewValue(obj[5]);
				   }
				   else{
					   dtoUserDataSecurity.setNewValue("");
				   }
			   if(obj[6]!=null){
				   dtoUserDataSecurity.setUser(obj[6]);
				   }
				   else{
					   dtoUserDataSecurity.setUser("");
				   }
			   if(obj[7]!=null){
				   dtoUserDataSecurity.setDated(obj[7]);
				   }
				   else{
					   dtoUserDataSecurity.setDated("");
				   }
			   if(obj[8]!=null){
				   dtoUserDataSecurity.setAlertRole(obj[8]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertRole("");
				   }
			   if(obj[9]!=null){
				   dtoUserDataSecurity.setAlertUser(obj[9]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertUser("");
				   }
			   if(obj[13]!=null){
				   dtoUserDataSecurity.setIsViewed(obj[10]);
				   }
				   else{
					   dtoUserDataSecurity.setIsViewed("");
				   }
			  
		auditList.add(dtoUserDataSecurity);
		}
		return auditList;
	}
	
	public List getAuditTrailList(Long xtranId, String tableName, String corpID,String from,String secondID) {
		Boolean fullAuditValue=false;
		String query = "";
		query = "Select fullAudit from company where corpID = '"+corpID+"' ";
		 List al=getSession().createSQLQuery(query).list();
		 if(al!=null && !al.isEmpty() && al.get(0)!=null){
			 fullAuditValue=(Boolean) al.get(0);
		 }
		if(fullAuditValue){
			return getFullAuditTrailPartnerList(xtranId,tableName,corpID,from,secondID);
		}else{
			if(from !=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("PartnerPublic")){
				return getAuditTrailPartnerList(xtranId,tableName,corpID,from);
			}else if(secondID !=null && !secondID.equalsIgnoreCase("")){
				return getHibernateTemplate().find("from AuditTrail where tableName  in("+tableName+") AND corpID = '"+corpID+"' AND xtranId in ('"+xtranId+"','"+secondID+"') ORDER BY id desc");				
			}else{
				return getHibernateTemplate().find("from AuditTrail where tableName  in("+tableName+") AND corpID = '"+corpID+"' AND xtranId in ('"+xtranId+"') ORDER BY id desc");
			}
		}
	}

	public List getAuditAccountContactList(String corpID,String partnerCode,String tableName) {
		String query="";
		String tempCode = "";
		query="SELECT group_concat(xtranId) from history where corpID in ('"+corpID+"','TSFT') AND oldValue='"+partnerCode+"' ORDER BY id desc";
		List accountContactList=new ArrayList();
		accountContactList=this.getSession().createSQLQuery(query).list();
		if((accountContactList!=null) && !accountContactList.isEmpty() && (accountContactList.get(0)!=null) && !accountContactList.get(0).toString().equalsIgnoreCase("")){
			return getHibernateTemplate().find("from AuditTrail where  corpID = '"+corpID+"' AND xtranId in ("+accountContactList.get(0).toString()+") ORDER BY id desc");
		}else{
			return null;
		}
		
		
	}
	public List findByAlertListFilter(String sessionCorpID,String alertUser,int daystoManageAlerts){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		Set<Role> roles  = user.getRoles();
		Role role=new Role();
		Iterator it = roles.iterator();
		String userRole = "";
		Boolean foundRole=false;
		while(it.hasNext()) {
			role=(Role)it.next();
			userRole = userRole+",'"+role.getName()+"'";
		}		
		userRole=userRole.substring(1, userRole.length());
		String query = "";
		if(alertUser!=null && !alertUser.equalsIgnoreCase("") && !alertUser.equalsIgnoreCase("UNASSIGNED") && daystoManageAlerts!=0){
			query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false and alertUser ='"+alertUser+"' AND corpID = '"+sessionCorpID+"' and dated >= date_format(DATE_SUB(now(), INTERVAL "+daystoManageAlerts+" DAY),'%y-%m-%d') ORDER BY id";
		}else if(alertUser!=null && !alertUser.equalsIgnoreCase("") && !alertUser.equalsIgnoreCase("UNASSIGNED") && daystoManageAlerts==0){
			query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false and alertUser ='"+alertUser+"' AND corpID = '"+sessionCorpID+"'  ORDER BY id";
		}
		/*else if(alertUser!=null && alertUser.equalsIgnoreCase("UNASSIGNED")){
			query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where UPPER(user)<>UPPER(alertUser) and isViewed is false and (alertUser ='' or alertUser is null) AND corpID = '"+sessionCorpID+"' ORDER BY id";
		}*/else{
			query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false AND corpID = '"+sessionCorpID+"' and date_format(dated,'%y-%m-%d')  BETWEEN date_format(DATE_SUB(now(), INTERVAL 14 DAY),'%y-%m-%d') and date_format(now(),'%y-%m-%d') ORDER BY id";	
		}
		//System.out.println(query);
		List idList= new ArrayList();
		idList=this.getSession().createSQLQuery(query).list();
		System.out.println(idList);
		List <Object> dataSecurityList = new ArrayList<Object>();
		DTOUserDataSecurity dtoUserDataSecurity;
		Iterator it1=idList.iterator();
		while(it1.hasNext())
		{
			Object []obj=(Object[]) it1.next();
			dtoUserDataSecurity=new DTOUserDataSecurity();
			   if(obj[0]!=null){
				   dtoUserDataSecurity.setXtranId(obj[0]);
				   }
				   else{
					   dtoUserDataSecurity.setXtranId("");
				   }
			   if(obj[1]!=null){
				   dtoUserDataSecurity.setTableName(obj[1]);
				   }
				   else{
					   dtoUserDataSecurity.setTableName("");
				   }
			   if(obj[2]!=null){
				   dtoUserDataSecurity.setFieldName(obj[2]);
				   }
				   else{
					   dtoUserDataSecurity.setFieldName("");
				   }
			   if(obj[3]!=null){
				   dtoUserDataSecurity.setDescription(obj[3]);
				   }
				   else{
					   dtoUserDataSecurity.setDescription("");
				   }
			   if(obj[4]!=null){
				   dtoUserDataSecurity.setOldValue(obj[4]);
				   }
				   else{
					   dtoUserDataSecurity.setOldValue("");
				   }
			   if(obj[5]!=null){ 
				   dtoUserDataSecurity.setNewValue(obj[5]);
				   }
				   else{
					   dtoUserDataSecurity.setNewValue("");
				   }
			   if(obj[6]!=null){
				   dtoUserDataSecurity.setUser(obj[6]);
				   }
				   else{
					   dtoUserDataSecurity.setUser("");
				   }
			   if(obj[7]!=null){
				   dtoUserDataSecurity.setDated(obj[7]);
				   }
				   else{
					   dtoUserDataSecurity.setDated("");
				   }
			   if(obj[8]!=null){
				   dtoUserDataSecurity.setAlertRole(obj[8]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertRole("");
				   }
			   if(obj[9]!=null){
				   dtoUserDataSecurity.setAlertUser(obj[9]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertUser("");
				   }
			   if(obj[10]!=null){
				   dtoUserDataSecurity.setIsViewed(obj[10]);
				   }
				   else{
					   dtoUserDataSecurity.setIsViewed("");
				   }
			   if(obj[1].toString().equalsIgnoreCase("myfile")){
				   if(obj[11]!=null && obj[11].toString().contains("#") ){
					   String myfileAlertFile[]=obj[11].toString().split("#");
					dtoUserDataSecurity.setAlertFileNumber(myfileAlertFile[0]);
						   }else{
						dtoUserDataSecurity.setAlertFileNumber(obj[11]);
						   }
			   }else{
				   if(obj[11]!=null){
					  dtoUserDataSecurity.setAlertFileNumber(obj[11]);
					   }
					   else{
					dtoUserDataSecurity.setAlertFileNumber("");
					   }  
			   }
			  
			   if(obj[12]!=null){
				   dtoUserDataSecurity.setAlertShipperName(obj[12]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertShipperName("");
				   }
			   if(obj[13]!=null){
				   dtoUserDataSecurity.setId(obj[13]);
			   }else{
				   dtoUserDataSecurity.setId("0");
			   }
			   if(obj[1].toString().equalsIgnoreCase("myfile")){
				   if(obj[11]!=null && obj[11].toString().contains("#")){
					   String myfileAlertFile[]=obj[11].toString().split("#");
					dtoUserDataSecurity.setDetailsId(myfileAlertFile[1]);
						   }else{
						dtoUserDataSecurity.setDetailsId("");
						   }
			   }else{
				   dtoUserDataSecurity.setDetailsId("");   
			   }
			   
			   List soIdList = new ArrayList();
			   if(((obj[0]!=null && !obj[0].toString().equalsIgnoreCase("")) && (obj[1]!=null && !obj[1].toString().equalsIgnoreCase(""))) && (obj[1].toString().equalsIgnoreCase("container") || obj[1].toString().equalsIgnoreCase("servicepartner"))){
				 String soQuery = "select serviceorderId from "+obj[1]+" where id='"+obj[0]+"'"  ;
				 soIdList = this.getSession().createSQLQuery(soQuery).list();
				 if(soIdList!=null && !soIdList.isEmpty() && soIdList.get(0)!=null){
					 dtoUserDataSecurity.setSoId(soIdList.toString().replace("[", "").replace("]", ""));
				 }else{
					 dtoUserDataSecurity.setSoId("");
				 }
			   }
			   
			dataSecurityList.add(dtoUserDataSecurity);
		}
		return dataSecurityList;
		

		
	}
	public List getAlertHistoryList(String corpID,int daystoManageAlerts){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		Set<Role> roles  = user.getRoles();
		Role role=new Role();
		Iterator it = roles.iterator();
		String userRole = "";
		Boolean foundRole=false;
		while(it.hasNext()) {
			role=(Role)it.next();
			userRole = userRole+",'"+role.getName()+"'";
		}		
		userRole=userRole.substring(1, userRole.length());
		String query="";
		if(daystoManageAlerts!=0){
		 query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false and corpID = '"+corpID+"' and dated >= date_format(DATE_SUB(now(), INTERVAL "+daystoManageAlerts+" DAY),'%y-%m-%d')  ORDER BY id";
		}else{
		 query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false and corpID = '"+corpID+"' ORDER BY id";
		}
			//return getHibernateTemplate().find("from AuditTrail where if(alertRole='Coordinator','ROLE_COORD',if(alertRole='Consultant','ROLE_SALE',if(alertRole='Billing','ROLE_BILLING',if(alertRole='Auditor','ROLE_AUDITOR',if(alertRole='Pricing','ROLE_PRICING','')))))  in ("+userRole+") and isViewed is true and user = '"+userName+"' ORDER BY id");
		
		//System.out.println(query);
		List idList= new ArrayList();
		idList=this.getSession().createSQLQuery(query).list();
		//System.out.println(idList);
		List <Object> dataSecurityList = new ArrayList<Object>();
		DTOUserDataSecurity dtoUserDataSecurity;
		Iterator it1=idList.iterator();
		while(it1.hasNext())
		{
			Object []obj=(Object[]) it1.next();
			dtoUserDataSecurity=new DTOUserDataSecurity();
			   if(obj[0]!=null){
				   dtoUserDataSecurity.setXtranId(obj[0]);
				   }
				   else{
					   dtoUserDataSecurity.setXtranId("");
				   }
			   if(obj[1]!=null){
				   dtoUserDataSecurity.setTableName(obj[1]);
				   }
				   else{
					   dtoUserDataSecurity.setTableName("");
				   }
			   if(obj[2]!=null){
				   dtoUserDataSecurity.setFieldName(obj[2]);
				   }
				   else{
					   dtoUserDataSecurity.setFieldName("");
				   }
			   if(obj[3]!=null){
				   dtoUserDataSecurity.setDescription(obj[3]);
				   }
				   else{
					   dtoUserDataSecurity.setDescription("");
				   }
			   if(obj[4]!=null){
				   dtoUserDataSecurity.setOldValue(obj[4]);
				   }
				   else{
					   dtoUserDataSecurity.setOldValue("");
				   }
			   if(obj[5]!=null){ 
				   dtoUserDataSecurity.setNewValue(obj[5]);
				   }
				   else{
					   dtoUserDataSecurity.setNewValue("");
				   }
			   if(obj[6]!=null){
				   dtoUserDataSecurity.setUser(obj[6]);
				   }
				   else{
					   dtoUserDataSecurity.setUser("");
				   }
			   if(obj[7]!=null){
				   dtoUserDataSecurity.setDated(obj[7]);
				   }
				   else{
					   dtoUserDataSecurity.setDated("");
				   }
			   if(obj[8]!=null){
				   dtoUserDataSecurity.setAlertRole(obj[8]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertRole("");
				   }
			   if(obj[9]!=null){
				   dtoUserDataSecurity.setAlertUser(obj[9]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertUser("");
				   }
			   if(obj[10]!=null){
				   dtoUserDataSecurity.setIsViewed(obj[10]);
				   }
				   else{
					   dtoUserDataSecurity.setIsViewed("");
				   }
			/*   if(obj[11]!=null){
				   dtoUserDataSecurity.setAlertFileNumber(obj[11]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertFileNumber("");
				   }*/
			   if(obj[1].toString().equalsIgnoreCase("myfile")){
				   if(obj[11]!=null && obj[11].toString().contains("#") ){
					   String myfileAlertFile[]=obj[11].toString().split("#");
					dtoUserDataSecurity.setAlertFileNumber(myfileAlertFile[0]);
						   }else{
						dtoUserDataSecurity.setAlertFileNumber(obj[11]);
						   }
			   }else{
				   if(obj[11]!=null){
					  dtoUserDataSecurity.setAlertFileNumber(obj[11]);
					   }
					   else{
					dtoUserDataSecurity.setAlertFileNumber("");
					   }  
			   }
			   if(obj[12]!=null){
				   dtoUserDataSecurity.setAlertShipperName(obj[12]);
				   }
				   else{
					   dtoUserDataSecurity.setAlertShipperName("");
				   }
			   if(obj[13]!=null){
				   dtoUserDataSecurity.setId(obj[13]);
			   }else{
				   dtoUserDataSecurity.setId("0");
			   }
			   if(obj[1].toString().equalsIgnoreCase("myfile")){
				   if(obj[11]!=null && obj[11].toString().contains("#")){
					   String myfileAlertFile[]=obj[11].toString().split("#");
					dtoUserDataSecurity.setDetailsId(myfileAlertFile[1]);
						   }else{
						dtoUserDataSecurity.setDetailsId("");
						   }
			   }else{
				   dtoUserDataSecurity.setDetailsId("");   
			   }
			   List soIdList = new ArrayList();
			   if(((obj[0]!=null && !obj[0].toString().equalsIgnoreCase("")) && (obj[1]!=null && !obj[1].toString().equalsIgnoreCase(""))) && (obj[1].toString().equalsIgnoreCase("container") || obj[1].toString().equalsIgnoreCase("servicepartner"))){
				 String soQuery = "select serviceorderId from "+obj[1]+" where id='"+obj[0]+"'"  ;
				 soIdList = this.getSession().createSQLQuery(soQuery).list();
				 if(soIdList!=null && !soIdList.isEmpty() && soIdList.get(0)!=null){
					 dtoUserDataSecurity.setSoId(soIdList.toString().replace("[", "").replace("]", ""));
				 }else{
					 dtoUserDataSecurity.setSoId("");
				 }
			   }
			   
			dataSecurityList.add(dtoUserDataSecurity);
		}
		return dataSecurityList;
	}
	public  class DTOUserDataSecurity{ 
		private Object xtranId;
		private Object tableName;
		private Object fieldName;
		private Object description;
		private Object oldValue;
		private Object newValue;
		private Object user;
		private Object dated;
		private Object alertRole;
		private Object alertUser;
		private Object isViewed;
		private Object alertFileNumber;
		private Object alertShipperName;
		private Object id;
		private Object detailsId;
		private Object soId;
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getXtranId() {
			return xtranId;
		}
		public void setXtranId(Object xtranId) {
			this.xtranId = xtranId;
		}
		public Object getTableName() {
			return tableName;
		}
		public void setTableName(Object tableName) {
			this.tableName = tableName;
		}
		public Object getFieldName() {
			return fieldName;
		}
		public void setFieldName(Object fieldName) {
			this.fieldName = fieldName;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getOldValue() {
			return oldValue;
		}
		public void setOldValue(Object oldValue) {
			this.oldValue = oldValue;
		}
		public Object getNewValue() {
			return newValue;
		}
		public void setNewValue(Object newValue) {
			this.newValue = newValue;
		}
		public Object getUser() {
			return user;
		}
		public void setUser(Object user) {
			this.user = user;
		}
		public Object getDated() {
			return dated;
		}
		public void setDated(Object dated) {
			this.dated = dated;
		}
		public Object getAlertRole() {
			return alertRole;
		}
		public void setAlertRole(Object alertRole) {
			this.alertRole = alertRole;
		}
		public Object getAlertUser() {
			return alertUser;
		}
		public void setAlertUser(Object alertUser) {
			this.alertUser = alertUser;
		}
		public Object getIsViewed() {
			return isViewed;
		}
		public void setIsViewed(Object isViewed) {
			this.isViewed = isViewed;
		}
		public Object getAlertFileNumber() {
			return alertFileNumber;
		}
		public void setAlertFileNumber(Object alertFileNumber) {
			this.alertFileNumber = alertFileNumber;
		}
		public Object getAlertShipperName() {
			return alertShipperName;
		}
		public void setAlertShipperName(Object alertShipperName) {
			this.alertShipperName = alertShipperName;
		}
		public Object getDetailsId() {
			return detailsId;
		}
		public void setDetailsId(Object detailsId) {
			this.detailsId = detailsId;
		}
		public Object getSoId() {
			return soId;
		}
		public void setSoId(Object soId) {
			this.soId = soId;
		}		
	}

public List getAlertTaskList(Long xtranId,String tableName,String sessionCorpID,String relatedTask,String sequenceNumber){

	
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	Set<Role> roles  = user.getRoles();
	Role role=new Role();
	Iterator it = roles.iterator();
	String userRole = "";
	Boolean foundRole=false;
	while(it.hasNext()) {
		role=(Role)it.next();
		userRole = userRole+",'"+role.getName()+"'";
	}		
	userRole=userRole.substring(1, userRole.length());
	String tempId="";
	//return getHibernateTemplate().find("from AuditTrail where if(alertRole='Coordinator','ROLE_COORD',if(alertRole='Consultant','ROLE_SALE',if(alertRole='Billing','ROLE_BILLING',if(alertRole='Auditor','ROLE_AUDITOR',if(alertRole='Pricing','ROLE_PRICING','')))))  in ("+userRole+") and isViewed is true and user = '"+userName+"' ORDER BY id");
	List tempParent=this.getSession().createSQLQuery("select id from customerfile where sequencenumber='"+sequenceNumber+"' union select id from serviceorder where sequencenumber='"+sequenceNumber+"' union select id from workticket where sequencenumber='"+sequenceNumber+"' union select id from claim where sequencenumber='"+sequenceNumber+"'").list();
	if(tempParent.size()>0){
		Iterator it11=tempParent.iterator();
		while(it11.hasNext()){
			if(tempId.equals("")){
				tempId = "'"+it11.next().toString()+"'";
			}else{
				tempId =tempId+ ",'"+it11.next().toString()+"'";
			}
		}
	}
	String query="";
	if(relatedTask.equalsIgnoreCase("relatedTask")){
	 query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where alertuser<>''  and isViewed is false and tableName in ('serviceorder','accountline','trackingstatus','miscellaneous','customerfile','workticket','claim') and xtranid in("+tempId+") and corpID = '"+sessionCorpID+"'  ORDER BY id";
	}else{
	 query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where alertuser<>''  and isViewed is false and tableName='"+tableName+"' and xtranid='"+xtranId+"' and corpID = '"+sessionCorpID+"'  ORDER BY id";	
	}	
	System.out.println(query);
	List idList= new ArrayList();
	idList=this.getSession().createSQLQuery(query).list();
	System.out.println(idList);
	List <Object> dataSecurityList = new ArrayList<Object>();
	DTOUserDataSecurity dtoUserDataSecurity;
	Iterator it1=idList.iterator();
	while(it1.hasNext())
	{
		Object []obj=(Object[]) it1.next();
		dtoUserDataSecurity=new DTOUserDataSecurity();
		   if(obj[0]!=null){
			   dtoUserDataSecurity.setXtranId(obj[0]);
			   }
			   else{
				   dtoUserDataSecurity.setXtranId("");
			   }
		   if(obj[1]!=null){
			   dtoUserDataSecurity.setTableName(obj[1]);
			   }
			   else{
				   dtoUserDataSecurity.setTableName("");
			   }
		   if(obj[2]!=null){
			   dtoUserDataSecurity.setFieldName(obj[2]);
			   }
			   else{
				   dtoUserDataSecurity.setFieldName("");
			   }
		   if(obj[3]!=null){
			   dtoUserDataSecurity.setDescription(obj[3]);
			   }
			   else{
				   dtoUserDataSecurity.setDescription("");
			   }
		   if(obj[4]!=null){
			   dtoUserDataSecurity.setOldValue(obj[4]);
			   }
			   else{
				   dtoUserDataSecurity.setOldValue("");
			   }
		   if(obj[5]!=null){ 
			   dtoUserDataSecurity.setNewValue(obj[5]);
			   }
			   else{
				   dtoUserDataSecurity.setNewValue("");
			   }
		   if(obj[6]!=null){
			   dtoUserDataSecurity.setUser(obj[6]);
			   }
			   else{
				   dtoUserDataSecurity.setUser("");
			   }
		   if(obj[7]!=null){
			   dtoUserDataSecurity.setDated(obj[7]);
			   }
			   else{
				   dtoUserDataSecurity.setDated("");
			   }
		   if(obj[8]!=null){
			   dtoUserDataSecurity.setAlertRole(obj[8]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertRole("");
			   }
		   if(obj[9]!=null){
			   dtoUserDataSecurity.setAlertUser(obj[9]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertUser("");
			   }
		   if(obj[10]!=null){
			   dtoUserDataSecurity.setIsViewed(obj[10]);
			   }
			   else{
				   dtoUserDataSecurity.setIsViewed("");
			   }
		  /* if(obj[11]!=null){
			   dtoUserDataSecurity.setAlertFileNumber(obj[11]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertFileNumber("");
			   }*/
		   if(obj[1].toString().equalsIgnoreCase("myfile")){
			   if(obj[11]!=null && obj[11].toString().contains("#") ){
				   String myfileAlertFile[]=obj[11].toString().split("#");
				dtoUserDataSecurity.setAlertFileNumber(myfileAlertFile[0]);
					   }else{
					dtoUserDataSecurity.setAlertFileNumber(obj[11]);
					   }
		   }else{
			   if(obj[11]!=null){
				  dtoUserDataSecurity.setAlertFileNumber(obj[11]);
				   }
				   else{
				dtoUserDataSecurity.setAlertFileNumber("");
				   }  
		   }
		   if(obj[12]!=null){
			   dtoUserDataSecurity.setAlertShipperName(obj[12]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertShipperName("");
			   }
		   if(obj[13]!=null){
			   dtoUserDataSecurity.setId(obj[13]);
		   }else{
			   dtoUserDataSecurity.setId("0");
		   }
		   if(obj[1].toString().equalsIgnoreCase("myfile")){
			   if(obj[11]!=null && obj[11].toString().contains("#")){
				   String myfileAlertFile[]=obj[11].toString().split("#");
				dtoUserDataSecurity.setDetailsId(myfileAlertFile[1]);
					   }else{
					dtoUserDataSecurity.setDetailsId("");
					   }
		   }else{
			   dtoUserDataSecurity.setDetailsId("");   
		   }
		   
		   List soIdList = new ArrayList();
		   if(((obj[0]!=null && !obj[0].toString().equalsIgnoreCase("")) && (obj[1]!=null && !obj[1].toString().equalsIgnoreCase(""))) && (obj[1].toString().equalsIgnoreCase("container") || obj[1].toString().equalsIgnoreCase("servicepartner"))){
			 String soQuery = "select serviceorderId from "+obj[1]+" where id='"+obj[0]+"'"  ;
			 soIdList = this.getSession().createSQLQuery(soQuery).list();
			 if(soIdList!=null && !soIdList.isEmpty() && soIdList.get(0)!=null){
				 dtoUserDataSecurity.setSoId(soIdList.toString().replace("[", "").replace("]", ""));
			 }else{
				 dtoUserDataSecurity.setSoId("");
			 }
		   }
		   
		dataSecurityList.add(dtoUserDataSecurity);
	}
	return dataSecurityList;

	}
public List findContainerCartonVehicleRoutingConsigneeList(Long sid,String tableName,String sessionCorpID){
	String tempTableName[] = tableName.split(",");
	String allContainerId="";
	List containerId  = this.getSession().createSQLQuery("select id from "+tempTableName[0]+" where corpID = '"+sessionCorpID+"' and serviceorderid = '"+sid+"'").list();
	String allCartonId="";
	List cartonId  = this.getSession().createSQLQuery("select id from "+tempTableName[1]+" where corpID = '"+sessionCorpID+"' and serviceorderid = '"+sid+"'").list();
	String allVehicleId="";
	List vehicleId  = this.getSession().createSQLQuery("select id from "+tempTableName[2]+" where corpID = '"+sessionCorpID+"' and serviceorderid = '"+sid+"'").list();
	String allRoutingId="";
	List routingId  = this.getSession().createSQLQuery("select id from "+tempTableName[3]+" where corpID = '"+sessionCorpID+"' and serviceorderid = '"+sid+"'").list();
	String allConsigneeId="";
	List consigneeId  = this.getSession().createSQLQuery("select id from "+tempTableName[4]+" where corpID = '"+sessionCorpID+"' and serviceorderid = '"+sid+"'").list();
	
	if(containerId!=null && !containerId.isEmpty() && containerId.get(0)!=null){
		allContainerId = containerId.toString().replace("[", "") .replace("]", "");		
	}
	if(cartonId!=null && !cartonId.isEmpty() && cartonId.get(0)!=null){
		allCartonId = cartonId.toString().replace("[", "") .replace("]", "");		
	}
	if(vehicleId!=null && !vehicleId.isEmpty() && vehicleId.get(0)!=null){
		allVehicleId = vehicleId.toString().replace("[", "") .replace("]", "");		
	}
	if(routingId!=null && !routingId.isEmpty() && routingId.get(0)!=null){
		allRoutingId = routingId.toString().replace("[", "") .replace("]", "");		
	}
	if(consigneeId!=null && !consigneeId.isEmpty() && consigneeId.get(0)!=null){
		allConsigneeId = consigneeId.toString().replace("[", "") .replace("]", "");		
	}
	List list = new ArrayList();
	String query = "";	
	if(allContainerId!=null && !allContainerId.equalsIgnoreCase("")){
		query= query + "select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allContainerId+") and tableName ='"+tempTableName[0]+"' ";
	}
	if(allCartonId!=null && !allCartonId.equalsIgnoreCase("") && (query!=null && !query.equalsIgnoreCase(""))){
		query= query + " UNION select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allCartonId+") and tableName ='"+tempTableName[1]+"' ";
	}else if(allCartonId!=null && !allCartonId.equalsIgnoreCase("") && (query==null || query.equalsIgnoreCase(""))){
		query= query + " select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allCartonId+") and tableName ='"+tempTableName[1]+"' ";
	}
	if(allVehicleId!=null && !allVehicleId.equalsIgnoreCase("") && (query!=null && !query.equalsIgnoreCase(""))){
		query= query + " UNION select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allVehicleId+") and tableName ='"+tempTableName[2]+"' ";
	}else if(allVehicleId!=null && !allVehicleId.equalsIgnoreCase("") && (query==null || query.equalsIgnoreCase(""))){
		query= query + " select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allVehicleId+") and tableName ='"+tempTableName[2]+"' ";
	}
	if(allRoutingId!=null && !allRoutingId.equalsIgnoreCase("") && (query!=null && !query.equalsIgnoreCase(""))){
		query= query + " UNION select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allRoutingId+") and tableName ='"+tempTableName[3]+"' ";
	}else if(allRoutingId!=null && !allRoutingId.equalsIgnoreCase("") && (query==null || query.equalsIgnoreCase(""))){
		query= query + " select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allRoutingId+") and tableName ='"+tempTableName[3]+"' ";
	}
	if(allConsigneeId!=null && !allConsigneeId.equalsIgnoreCase("") && (query!=null && !query.equalsIgnoreCase(""))){
		query= query + "UNION select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allConsigneeId+") and tableName ='"+tempTableName[4]+"' ";
	}else if(allConsigneeId!=null && !allConsigneeId.equalsIgnoreCase("") && (query==null || query.equalsIgnoreCase(""))){
		query= query + " select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid in ("+allConsigneeId+") and tableName ='"+tempTableName[4]+"' ";
	}
	if(query!=null && !query.equalsIgnoreCase("")){
		query= query +" ORDER BY dated desc";
	}
	if(query!=null && !query.equalsIgnoreCase("")){
		 list= this.getSession().createSQLQuery(query).list();
	}else{
		query= "select fieldName, description, oldValue, newValue, user, dated from history where corpid='"+sessionCorpID+"' and xtranid ='"+sid+"' and tableName ='"+tempTableName[0]+"' ";
		list= this.getSession().createSQLQuery(query).list();
	}
	List dataSecurityList = new ArrayList();
	Iterator it=list.iterator();
	while(it.hasNext()){
		 Object []obj=(Object[]) it.next();
		 DTOUserDataSecurity dtoUserDataSecurity=new DTOUserDataSecurity();

		   if(obj[0]!=null){
			   dtoUserDataSecurity.setFieldName(obj[0]);
			}else{
				dtoUserDataSecurity.setFieldName("");
			}
		   if(obj[1]!=null){
			   dtoUserDataSecurity.setDescription(obj[1]);
			}else{
				dtoUserDataSecurity.setDescription("");
			}
		   if(obj[2]!=null){
			   dtoUserDataSecurity.setOldValue(obj[2]);
			} else{
				dtoUserDataSecurity.setOldValue("");
			}
		   if(obj[3]!=null){ 
			   dtoUserDataSecurity.setNewValue(obj[3]);
			}else{
				dtoUserDataSecurity.setNewValue("");
			}
		   if(obj[4]!=null){
			   dtoUserDataSecurity.setUser(obj[4]);
			}else{
				dtoUserDataSecurity.setUser("");
			}
		   if(obj[5]!=null){
			   dtoUserDataSecurity.setDated(obj[5]);
			}else{
				dtoUserDataSecurity.setDated("");
			}
		   
		   dataSecurityList.add(dtoUserDataSecurity);
	}
	return dataSecurityList;	
}
public List findByAlertListFilterForExtCoordinator(String sessionCorpID,String alertUser,int daystoManageAlerts,String bookingAgentPopup,String billToCodePopup){
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	Set<Role> roles  = user.getRoles();
	Role role=new Role();
	Iterator it = roles.iterator();
	String userRole = "";
	Boolean foundRole=false;
	String billtocodecondition="";
	String bookingAgentcondition="";
	String alertUsers="";
	List list3=new ArrayList();
	String userAlert = alertUser.replaceAll("'", "");
	if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
	{
		
		billtocodecondition= " and billToCode in ("+billToCodePopup+")";
	}

	if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
	{
		bookingAgentcondition= " and bookingAgentCode in ("+bookingAgentPopup+")";
    }
	String query = "";
	while(it.hasNext()) {
		role=(Role)it.next();
		userRole = userRole+",'"+role.getName()+"'";
	}		
	userRole=userRole.substring(1, userRole.length());
	

	List list=  this.getSession().createSQLQuery("select group_concat(id) from serviceorder where corpID= '"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" ").list();
	String solist = list.toString().replace("[","").replace("]", "");
	
	List list1=  this.getSession().createSQLQuery("select group_concat(id) from customerfile where corpID= '"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" ").list();
	String cflist = list1.toString().replace("[","").replace("]", "");

    List list2=  this.getSession().createSQLQuery("select group_concat(id) from accountline where corpID= '"+sessionCorpID+"' 	and  serviceorderid in ("+solist+")").list();
	String acclist = list2.toString().replace("[","").replace("]", "");
	if((solist!=null) && (!solist.equals("")) && (cflist!=null) && (!cflist.equals("")) &&  (acclist!=null) && (!acclist.equals("")) ){
		
   	
	if(userAlert!=null && !userAlert.equalsIgnoreCase("")  && daystoManageAlerts!=0){
		query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false and alertUser in ("+alertUser+") AND corpID = '"+sessionCorpID+"' and dated >= date_format(DATE_SUB(now(), INTERVAL "+daystoManageAlerts+" DAY),'%y-%m-%d')and (((tableName ='serviceorder' or tableName ='trackingstatus' or tableName ='billing')  "+
	            "  and xtranId in ("+solist+")) or "+
		        " (tableName ='customerfile' and xtranId in ("+cflist+")) or  "+
		        " (tableName ='accountline' and xtranId in ("+acclist+")) )  ORDER BY id";
	}else if(userAlert!=null && !userAlert.equalsIgnoreCase("") && daystoManageAlerts==0){
		query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false and alertUser in("+alertUser+") AND corpID = '"+sessionCorpID+"' and (((tableName ='serviceorder' or tableName ='trackingstatus' or tableName ='billing')  "+
	            "  and xtranId in ("+solist+")) or "+
		        " (tableName ='customerfile' and xtranId in ("+cflist+")) or  "+
		        " (tableName ='accountline' and xtranId in ("+acclist+")) ) ORDER BY id";
	}
	/*else if(alertUser!=null && alertUser.equalsIgnoreCase("UNASSIGNED")){
		query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where UPPER(user)<>UPPER(alertUser) and isViewed is false and (alertUser ='' or alertUser is null) AND corpID = '"+sessionCorpID+"' ORDER BY id";
	}*/else{
		query="SELECT xtranId, tableName, fieldName, description, oldValue, newValue, user, dated, alertRole, alertUser, isViewed, alertFileNumber, alertShipperName,id from history where id > (select id from history_start_id) and alertuser<>'' and UPPER(user)<>UPPER(alertUser) and isViewed is false AND corpID = '"+sessionCorpID+"' and date_format(dated,'%y-%m-%d')  BETWEEN date_format(DATE_SUB(now(), INTERVAL 14 DAY),'%y-%m-%d') and date_format(now(),'%y-%m-%d') and (((tableName ='serviceorder' or tableName ='trackingstatus' or tableName ='billing')  "+
	            "  and xtranId in ("+solist+")) or "+
		        " (tableName ='customerfile' and xtranId in ("+cflist+")) or  "+
		        " (tableName ='accountline' and xtranId in ("+acclist+")) )ORDER BY id";	
	}}
	List idList= new ArrayList();
	idList=this.getSession().createSQLQuery(query).list();
	System.out.println(idList);
	List <Object> dataSecurityList = new ArrayList<Object>();
	DTOUserDataSecurity dtoUserDataSecurity;
	Iterator it1=idList.iterator();
	while(it1.hasNext())
	{
		Object []obj=(Object[]) it1.next();
		dtoUserDataSecurity=new DTOUserDataSecurity();
		   if(obj[0]!=null){
			   dtoUserDataSecurity.setXtranId(obj[0]);
			   }
			   else{
				   dtoUserDataSecurity.setXtranId("");
			   }
		   if(obj[1]!=null){
			   dtoUserDataSecurity.setTableName(obj[1]);
			   }
			   else{
				   dtoUserDataSecurity.setTableName("");
			   }
		   if(obj[2]!=null){
			   dtoUserDataSecurity.setFieldName(obj[2]);
			   }
			   else{
				   dtoUserDataSecurity.setFieldName("");
			   }
		   if(obj[3]!=null){
			   dtoUserDataSecurity.setDescription(obj[3]);
			   }
			   else{
				   dtoUserDataSecurity.setDescription("");
			   }
		   if(obj[4]!=null){
			   dtoUserDataSecurity.setOldValue(obj[4]);
			   }
			   else{
				   dtoUserDataSecurity.setOldValue("");
			   }
		   if(obj[5]!=null){ 
			   dtoUserDataSecurity.setNewValue(obj[5]);
			   }
			   else{
				   dtoUserDataSecurity.setNewValue("");
			   }
		   if(obj[6]!=null){
			   dtoUserDataSecurity.setUser(obj[6]);
			   }
			   else{
				   dtoUserDataSecurity.setUser("");
			   }
		   if(obj[7]!=null){
			   dtoUserDataSecurity.setDated(obj[7]);
			   }
			   else{
				   dtoUserDataSecurity.setDated("");
			   }
		   if(obj[8]!=null){
			   dtoUserDataSecurity.setAlertRole(obj[8]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertRole("");
			   }
		   if(obj[9]!=null){
			   dtoUserDataSecurity.setAlertUser(obj[9]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertUser("");
			   }
		   if(obj[10]!=null){
			   dtoUserDataSecurity.setIsViewed(obj[10]);
			   }
			   else{
				   dtoUserDataSecurity.setIsViewed("");
			   }
		   if(obj[1].toString().equalsIgnoreCase("myfile")){
			   if(obj[11]!=null && obj[11].toString().contains("#") ){
				   String myfileAlertFile[]=obj[11].toString().split("#");
				dtoUserDataSecurity.setAlertFileNumber(myfileAlertFile[0]);
					   }else{
					dtoUserDataSecurity.setAlertFileNumber(obj[11]);
					   }
		   }else{
			   if(obj[11]!=null){
				  dtoUserDataSecurity.setAlertFileNumber(obj[11]);
				   }
				   else{
				dtoUserDataSecurity.setAlertFileNumber("");
				   }  
		   }
		  
		   if(obj[12]!=null){
			   dtoUserDataSecurity.setAlertShipperName(obj[12]);
			   }
			   else{
				   dtoUserDataSecurity.setAlertShipperName("");
			   }
		   if(obj[13]!=null){
			   dtoUserDataSecurity.setId(obj[13]);
		   }else{
			   dtoUserDataSecurity.setId("0");
		   }
		   if(obj[1].toString().equalsIgnoreCase("myfile")){
			   if(obj[11]!=null && obj[11].toString().contains("#")){
				   String myfileAlertFile[]=obj[11].toString().split("#");
				dtoUserDataSecurity.setDetailsId(myfileAlertFile[1]);
					   }else{
					dtoUserDataSecurity.setDetailsId("");
					   }
		   }else{
			   dtoUserDataSecurity.setDetailsId("");   
		   }
		   
		   List soIdList = new ArrayList();
		   if(((obj[0]!=null && !obj[0].toString().equalsIgnoreCase("")) && (obj[1]!=null && !obj[1].toString().equalsIgnoreCase(""))) && (obj[1].toString().equalsIgnoreCase("container") || obj[1].toString().equalsIgnoreCase("servicepartner"))){
			 String soQuery = "select serviceorderId from "+obj[1]+" where id='"+obj[0]+"'"  ;
			 soIdList = this.getSession().createSQLQuery(soQuery).list();
			 if(soIdList!=null && !soIdList.isEmpty() && soIdList.get(0)!=null){
				 dtoUserDataSecurity.setSoId(soIdList.toString().replace("[", "").replace("]", ""));
			 }else{
				 dtoUserDataSecurity.setSoId("");
			 }
		   }
		   
		dataSecurityList.add(dtoUserDataSecurity);
	}
	return dataSecurityList;
	

	
}
}
