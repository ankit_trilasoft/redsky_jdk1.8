/**
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class CartonAction extends BaseAction implements Preparable{   
   
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private  Miscellaneous  miscellaneous;
	private CustomerFile customerFile;
	private Carton carton;
	private Company company;
	private CompanyManager companyManager;
    private ServiceOrderManager serviceOrderManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager;
	private CustomerFileManager customerFileManager;
	private CartonManager cartonManager;
	private NotesManager notesManager;
	private ToDoRuleManager toDoRuleManager;
	

	private List cartons; 
	private  List containerNumberList;
    private  List weightunits;
    private  List volumeunits;
    private Map<String,String> lengthunits;
    private String idNumber;
    private Long autoId;
    private Long sid;
    private Long id; 
    private Long idMax;
    private String sessionCorpID;
    private String countCortonNotes;
    private String gotoPageString;
    private String validateFormNav;
    private String shipNumber;
    private String hitFlag="";
    private String shipSize;
	private String minShip;
	private String countShip;
	private List customerSO;
	private String weightType;
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	private String maxChild;
	private String countChild;
	private String minChild;
	private Long soIdNum;
	private Long sidNum;
	private long tempcarton;
	private List cartonSO;
	private List<SystemDefault> sysDefaultDetail;
	private  Boolean UnitType;
	private  Boolean VolumeType;
	private CustomerFileAction customerFileAction;
	private CustomManager customManager;
	private String countBondedGoods;
	private String voxmeIntergartionFlag;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CartonAction.class);
	private String  usertype;
	private Boolean surveyTab = new Boolean(false);
	private String cartonStatus;
	private String saveAndAddFlag="";


	private String oiJobList;
//  A Method to Authenticate User, calling from main menu.
    
    public CartonAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
	} 
    
//  End of Method.
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList();
    	company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
        if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		}
		try {
			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
				 Long soId = null;
				 try {
					 soId = new Long(getRequest().getParameter("sid").toString());
					} catch (Exception e) {
						soId = new Long(getRequest().getParameter("id").toString());
						e.printStackTrace();
					}
			 if(soId!=null){
				 ServiceOrder agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(soId);
				 String corpId=agentServiceOrderCombo.getCorpID();
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(soId);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
//  Method used to save through tabs.
    
    public String saveOnTabChange() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    		validateFormNav = "OK";
            String s = save();
           logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return gotoPageString;
            
    } 
    
//  End of Method.
    
//  A Method to get the number of container.
    
    public List containerNumberList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	containerNumberList=cartonManager.getContainerNumber(carton.getShipNumber());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return containerNumberList;
    }
    
//  End of Method.
    
//  A Method to get the list.
	private Billing billing;
	private BillingManager billingManager;  
    public String cartonList() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	serviceOrder = serviceOrderManager.get(id);
    	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    	trackingStatus=trackingStatusManager.get(id);
		miscellaneous = miscellaneousManager.get(id);
		billing = billingManager.get(id);
		try{
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> miscellaneousRecords=findMiscellaneousRecords(linkedShipNumber,serviceOrder);
	    		synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
	    	}
            /*Long StartTime = System.currentTimeMillis();
    		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
    				//&& serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
    			if(agentShipNumber!=null && !agentShipNumber.equals(""))
    				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
       			
       		}
				
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
     
       	}catch(Exception ex){
    		System.out.println("\n\n\n\n\n checkRemoveLinks exception");
    		ex.printStackTrace();
    	}
		customerFile=serviceOrder.getCustomerFile(); 
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getWeightUnit().equals("Lbs")){
					UnitType=true;
				}else{
					UnitType=false;
				}
				if(systemDefault.getVolumeUnit().equals("Cft")){
					VolumeType=true;
				}else{
					VolumeType=false;
				}
			}
		}
    	carton = new Carton();
    	if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)== null || cartonManager.getNet(serviceOrder.getShipNumber()).get(0)== null ||cartonManager.getVolume(serviceOrder.getShipNumber()).get(0) == null || cartonManager.getPieces(serviceOrder.getShipNumber()).get(0) == null || cartonManager.getTare(serviceOrder.getShipNumber()).get(0) == null)
    	{
    		cartons = new ArrayList( serviceOrder.getCartons());
      	   	getSession().setAttribute("cartons", cartons);
      	 		
    	}
    	else{
    		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
    		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
    			for (SystemDefault systemDefault : sysDefaultDetail) {
    				if(systemDefault.getWeightUnit().equals("Lbs")){
    					UnitType=true;
    					carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(serviceOrder.getShipNumber())).get(0).toString()));
    				}else{
    					UnitType=false;
    					carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGrossKilo(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalTareWeight(new BigDecimal((cartonManager.getTareKilo(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalNetWeight(new BigDecimal((cartonManager.getNetKilo(serviceOrder.getShipNumber())).get(0).toString()));
    				}
    				if(systemDefault.getVolumeUnit().equals("Cft")){
    					VolumeType=true;
    					 carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(serviceOrder.getShipNumber())).get(0).toString()));
    				}else{
    					VolumeType=false;
    					 carton.setTotalVolume(new BigDecimal((cartonManager.getVolumeCbm(serviceOrder.getShipNumber())).get(0).toString()));
    				}
    			}
    		}
    	
       
  	   	carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
  	   	serviceOrder = serviceOrderManager.get(id);
        cartons = new ArrayList( serviceOrder.getCartons());
  	   	getSession().setAttribute("cartons", cartons);
    	}
    	shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        if(!customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0)!=null){
            countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0).toString();
        }
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;  
    }
    
    private List containerNumberListCarton;
//  End of Method.
    @SkipValidation
	public String getCartonDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			containerNumberListCarton=cartonManager.getContainerNumber(serviceOrder.getShipNumber());
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
					if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							if(systemDefault.getWeightUnit().equals("Lbs")){
								UnitType=true;
							}else{
								UnitType=false;
							}
							if(systemDefault.getVolumeUnit().equals("Cft")){
								VolumeType=true;
							}else{
								VolumeType=false;
							}
						}
					}
				carton = new Carton();
				if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)== null || cartonManager.getNet(serviceOrder.getShipNumber()).get(0)== null ||cartonManager.getVolume(serviceOrder.getShipNumber()).get(0) == null || cartonManager.getPieces(serviceOrder.getShipNumber()).get(0) == null || cartonManager.getTare(serviceOrder.getShipNumber()).get(0) == null){
		    		cartons = new ArrayList( serviceOrder.getCartons());
		      	   	getSession().setAttribute("cartons", cartons);		      	 		
		    	}else{
		    		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
		    			for (SystemDefault systemDefault : sysDefaultDetail) {
		    				if(systemDefault.getWeightUnit().equals("Lbs")){
		    					UnitType=true;
		    					carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(serviceOrder.getShipNumber())).get(0).toString()));
		    					carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
		    					carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(serviceOrder.getShipNumber())).get(0).toString()));
		    				}else{
		    					UnitType=false;
		    					carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGrossKilo(serviceOrder.getShipNumber())).get(0).toString()));
		    					carton.setTotalTareWeight(new BigDecimal((cartonManager.getTareKilo(serviceOrder.getShipNumber())).get(0).toString()));
		    					carton.setTotalNetWeight(new BigDecimal((cartonManager.getNetKilo(serviceOrder.getShipNumber())).get(0).toString()));
		    				}
		    				if(systemDefault.getVolumeUnit().equals("Cft")){
		    					VolumeType=true;
		    					 carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(serviceOrder.getShipNumber())).get(0).toString()));
		    				}else{
		    					VolumeType=false;
		    					 carton.setTotalVolume(new BigDecimal((cartonManager.getVolumeCbm(serviceOrder.getShipNumber())).get(0).toString()));
		    				}
		    			}		    			
		    		}
		    		carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
	    			cartons = new ArrayList( serviceOrder.getCartons());
	    	        getSession().setAttribute("cartons", cartons);
		    	}
		} catch (Exception e) {			
	    	 return CANCEL;
		}		
		return SUCCESS;
	}
    
    @SkipValidation
   	public String deleteCartonDetailsAjax(){
   		try {
   			carton = cartonManager.get(id);
   			//cartonManager.remove(id);
   			cartonManager.updateStatus(id,cartonStatus);
   			serviceOrder=carton.getServiceOrder();
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			deleteCarton(serviceOrderRecords,carton.getIdNumber(),cartonStatus);
          }
            if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
  	   		}else{
  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
  	   		}
         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
         	}else{
         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
         	}
         
         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
   	   		}else{
   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
   	   		}
          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
          	} else{
          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
        }
   		} catch (Exception e) {			
   	    	 return CANCEL;
   		}		
   		return SUCCESS;
   	}
    
    private void synchornizeMiscellaneous(List<Object> miscellaneousRecords,Miscellaneous miscellaneous,TrackingStatus trackingStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	String fieldType="";
		String uniqueFieldType=""; 
    	
    	Iterator  it=miscellaneousRecords.iterator();
    	while(it.hasNext()){
    		Miscellaneous miscellaneousToRecods=(Miscellaneous)it.next();
    		if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
    		TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(miscellaneousToRecods.getId());
    		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
    			uniqueFieldType=trackingStatusToRecods.getContractType();
    		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
    			fieldType="CMM/DMM";
    		}
    		List fieldToSync=customerFileManager.findFieldToSync("Miscellaneous",fieldType,uniqueFieldType);
    		 fieldType="";
    		 uniqueFieldType=""; 
    		Iterator it1=fieldToSync.iterator();
    		while(it1.hasNext()){
    			String field=it1.next().toString();
    			String[] splitField=field.split("~");
    			String fieldFrom="";
    			String fieldTo="";
    			if(splitField!=null && splitField[0]!=null){
				 fieldFrom=splitField[0];
    			}
    			if(splitField!=null && splitField[1]!=null){
				fieldTo=splitField[1];
    			}
    			try{
    				//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
    				try{
    				beanUtilsBean.setProperty(miscellaneousToRecods,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
    				}catch(Exception ex){
						System.out.println("\n\n\n\n\n error while setting property--->"+ex);
						ex.printStackTrace();
					}
    				
    			}catch(Exception ex){
    				System.out.println("\n\n\n\n Exception while copying");
    				ex.printStackTrace();
    			}
    			
    		}
    		try{
    		if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
    		miscellaneousToRecods.setUpdatedBy(miscellaneous.getCorpID()+":"+getRequest().getRemoteUser());
    		miscellaneousToRecods.setUpdatedOn(new Date());
    		}}catch(Exception e){
    			e.printStackTrace();	
    		}
    		miscellaneousManager.save(miscellaneousToRecods);
    	}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	private List<Object> findMiscellaneousRecords(List linkedShipNumber, ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
    	Iterator it =linkedShipNumber.iterator();
    	while(it.hasNext()){
    		String shipNumber= it.next().toString();
    		Long id=serviceOrderManager.findRemoteServiceOrder(shipNumber);
    		if((id!=null)&&(id!=0)&&(!id.toString().equalsIgnoreCase(""))){
	    		Miscellaneous miscellaneousRemote=miscellaneousManager.getForOtherCorpid(id);
	    		recordList.add(miscellaneousRemote);
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return recordList;
	}
    
    
    @SkipValidation 
    public String cartonNext(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	tempcarton=Long.parseLong((cartonManager.goNextSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    @SkipValidation 
    public String cartonPrev(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	tempcarton=Long.parseLong((cartonManager.goPrevSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    } 
      
    @SkipValidation 
    public String cartonSO(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	cartonSO=cartonManager.goSOChild(sidNum,sessionCorpID,soIdNum);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    private RefMasterManager refMasterManager;
    private List cartonTypeValue;
    public String getComboList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	weightunits = new ArrayList();
      	weightunits.add("Lbs");
      	weightunits.add("Kgs"); 
      	volumeunits = new ArrayList();
      	volumeunits.add("Cft");
      	volumeunits.add("Cbm");  
      	lengthunits=new LinkedHashMap<String,String>();
      	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,"'LENGTHUNITSCARTON'");
      	        for (RefMasterDTO  refObj : allParamValue) {
      	         	   if(refObj.getParameter().trim().equalsIgnoreCase("LENGTHUNITSCARTON")){
      	         	   lengthunits.put(refObj.getCode().trim(),refObj.getDescription().trim());}}
      	cartonTypeValue = cartonManager.findCartonTypeValue(sessionCorpID);
      	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      	return SUCCESS;
    	
    }
//  Method for editing and adding the record.
   public String edit() { 
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 //getComboList();
        if ( id != null)
        {   
        	carton = cartonManager.get(id);
        	serviceOrder = carton.getServiceOrder();
        	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
        	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
        	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
        	billing = billingManager.get(serviceOrder.getId());
        	customerFile = serviceOrder.getCustomerFile();
        	
        	try {
     			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
     				 
     			 if(serviceOrder!=null){
     				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(serviceOrder.getCorpID()); 
     				 if(serviceOrder.getIsNetworkRecord() && (trackingStatus.getSoNetworkGroup() || chkCompanySurveyFlag)){
     						surveyTab= true;
     				}
     				}
     			 }
     		 } catch (Exception e) {
     			e.printStackTrace();
     		 }
     		 
     		 
        	if(carton.getUnit1().equalsIgnoreCase("Lbs")){
				weightType="lbscft";
			}
			else{
				weightType="kgscbm";
			}
        	maxChild = cartonManager.findMaximumChild(serviceOrder.getShipNumber()).get(0).toString();
            minChild =  cartonManager.findMinimumChild(serviceOrder.getShipNumber()).get(0).toString();
            countChild =  cartonManager.findCountChild(serviceOrder.getShipNumber()).get(0).toString();
             } else {   
            	  carton = new Carton(); 
            	  serviceOrder=serviceOrderManager.get(sid);
            	  getRequest().setAttribute("soLastName",serviceOrder.getLastName());
            	  miscellaneous = miscellaneousManager.get(sid);
                  trackingStatus=trackingStatusManager.get(sid);
                  billing = billingManager.get(sid);
        	      customerFile = serviceOrder.getCustomerFile();
        	      
        	    	    carton.setShipNumber(serviceOrder.getShipNumber());
        	        	carton .setCreatedOn(new Date());
        	        	carton .setUpdatedOn(new Date());
        	        	carton.setCorpID(serviceOrder.getCorpID());
        	        	carton.setUnit1(miscellaneous.getUnit1());
        	        	carton.setUnit2(miscellaneous.getUnit2());
        	        	/*if(!serviceOrder.getCorpID().equalsIgnoreCase("CWMS")){
        	        		carton.setUnit3("Ft");
        	        	}else{
        	        		carton.setUnit3("Inches");
        	        	}*/
        	        	carton.setGrossWeight(new BigDecimal(0));
        	        	carton.setNetWeight(new BigDecimal(0));
        	        	carton.setVolume(new BigDecimal(0));
        	        	carton.setEmptyContWeight(new BigDecimal(0));
        	        	carton.setGrossWeightKilo(new BigDecimal(0));
        	        	carton.setNetWeightKilo(new BigDecimal(0));
        	        	carton.setVolumeCbm(new BigDecimal(0));
        	        	carton.setEmptyContWeightKilo(new BigDecimal(0));
        	        	carton.setPieces(0);
        	        	if(carton.getUnit1().equalsIgnoreCase("Lbs")){
        					weightType="lbscft";
        				}
        				else{
        					weightType="kgscbm";
        				}
        }
        shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        getNotesForIconChange();
        containerNumberList= containerNumberList();
        if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
    		
        		containerNumberList= new ArrayList();
        		
        	}
        if(!customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0)!=null){
            countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0).toString();
        }
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }   
    
// End of Method.
   
// Method getting the count of notes according to which the icon color changes in form
   
    @SkipValidation 
    public String getNotesForIconChange() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List m = notesManager.countForCartonNotes(carton.getId());
		if( m.isEmpty() ){
			countCortonNotes = "0";
		}else {
			countCortonNotes="";
			if((m).get(0)!=null){
			countCortonNotes = ((m).get(0)).toString() ;
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }   
   
//  End of Method.
    
//  Method used to save and update the record.
    private List listOfPieces=new ArrayList();
    public String save() throws Exception {   
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	  boolean isNew = (carton.getId() == null);   
        
        carton.setServiceOrder(serviceOrder); 
        miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	  billing = billingManager.get(serviceOrder.getId());
        carton.setUpdatedOn(new Date());
        //carton.setCorpID(sessionCorpID);
        carton.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	carton.setCreatedOn(new Date());
        }
         List maxIdNumber = cartonManager.findMaximumIdNumber(serviceOrder.getShipNumber());
	      if ( maxIdNumber.get(0) == null ) 
	      {          
	      	 idNumber = "01";
        }else {
	          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	        	if((autoId.toString()).length() == 1) 
	        		{
	        			idNumber = "0"+(autoId.toString());
	        		}else {
	        			 idNumber=autoId.toString();
	        			        
	        			}
	        	} 
	      if(isNew){
	    	  carton.setIdNumber(idNumber);
	      }else{
	    	  carton.setIdNumber(carton.getIdNumber());
	      }
	      if(carton.getDescription()==null || carton.getDescription().equalsIgnoreCase("")){
	    	  carton.setDescription(carton.getCartonType()); 
	      }
        carton=cartonManager.save(carton); 
        if(isNew && sessionCorpID.equalsIgnoreCase("UGWW")){
        	carton.setUgwIntId(carton.getId().toString());
        	 carton=cartonManager.save(carton); 
       	}
        serviceOrder=carton.getServiceOrder();
        try{
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			synchornizeCarton(serviceOrderRecords,carton,isNew,serviceOrder);
			
	   }
        /*Long StartTime = System.currentTimeMillis();
		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
				//&& serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
			if(agentShipNumber!=null && !agentShipNumber.equals(""))
				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
   			
   			}
			
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
   
       }catch(Exception ex){
			System.out.println("\n\n\n\n\n checkRemoveLinks exception");
			ex.printStackTrace();
       }
        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
 	   {   
 	   }
        else
        {
        cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
 	    }
        if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
        {	
        }
        else{
        	cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
        }
        
        if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
  	   {   
  	   }
         else
         {
         cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
  	    }
         if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
         {	
         }
         else{
         	cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
         	}
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "carton.added" : "carton.updated";   
        saveMessage(getText(key));
        }
        if (!isNew) { 
        	serviceOrder=serviceOrderManager.get(serviceOrder.getId());
            carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(carton.getShipNumber())).get(0).toString()));
            carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(carton.getShipNumber())).get(0).toString()));
            carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(carton.getShipNumber())).get(0).toString()));
           // carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
            listOfPieces=cartonManager.getPieces(serviceOrder.getShipNumber());
            if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
            	carton.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
            }
            else{
            	carton.setTotalPieces(0);
            	
            }
            carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
            cartons = new ArrayList( serviceOrder.getCartons());
      	   	getSession().setAttribute("cartons", cartons);
      	   	hitFlag="1";
            return SUCCESS;   
        } else {   
        	idMax = Long.parseLong(cartonManager.findMaxId().get(0).toString());
            carton = cartonManager.get(idMax);
        }
        serviceOrder=serviceOrderManager.get(serviceOrder.getId());
        carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(carton.getShipNumber())).get(0).toString()));
        carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(carton.getShipNumber())).get(0).toString()));
        carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(carton.getShipNumber())).get(0).toString()));
     //   carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
        listOfPieces=cartonManager.getPieces(serviceOrder.getShipNumber());
        if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
        	carton.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
        }
        else{
        	carton.setTotalPieces(0);
        	
        }           
        carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
        cartons = new ArrayList( serviceOrder.getCartons());
  	   	getSession().setAttribute("cartons", cartons);
  	   	hitFlag="1";
  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;  
    }
    
    public String saveAndAddAjax() throws Exception {   
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	  boolean isNew = (carton.getId() == null);   
        
        carton.setServiceOrder(serviceOrder); 
        miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	billing = billingManager.get(serviceOrder.getId());
        carton.setUpdatedOn(new Date());
        carton.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	carton.setCreatedOn(new Date());
        }
        List maxIdNumber = cartonManager.findMaximumIdNumber(serviceOrder.getShipNumber());
	    if ( maxIdNumber.get(0) == null ){          
	      	 idNumber = "01";
        }else {
	          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	        	if((autoId.toString()).length() == 1) 
	        		{
	        			idNumber = "0"+(autoId.toString());
	        		}else {
	        			 idNumber=autoId.toString();
	        			        
	        			}
	        	} 
	      if(isNew){
	    	  carton.setIdNumber(idNumber);
	      }else{
	    	  carton.setIdNumber(carton.getIdNumber());
	      }
	      if(carton.getDescription()==null || carton.getDescription().equalsIgnoreCase("")){
	    	  carton.setDescription(carton.getCartonType()); 
	      }
        carton=cartonManager.save(carton); 
        if(isNew && sessionCorpID.equalsIgnoreCase("UGWW")){
        	carton.setUgwIntId(carton.getId().toString());
        	 carton=cartonManager.save(carton); 
       	}
        serviceOrder=carton.getServiceOrder();
        try{
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			synchornizeCarton(serviceOrderRecords,carton,isNew,serviceOrder);
			
	   }
          
       }catch(Exception ex){
			ex.printStackTrace();
       }
        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
 	   	}else{
 	   		cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
 	    }
        if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
        }else{
        	cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
        }
        
        if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
  	   	}else{
         cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
  	    }
        if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
        }else{
         	cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
        }
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "carton.added" : "carton.updated";   
        saveMessage(getText(key));
        }
  	   	saveAndAddFlag="1";
  	   	cartonEditURL="editCartonAjax.html?decorator=popup&popup=true&sid="+serviceOrder.getId()+"&saveAndAddFlag="+saveAndAddFlag;
  	   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;  
    }
   private String cartonEditURL; 
   @SkipValidation 
   public String  saveAndAdd() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	  boolean isNew = (carton.getId() == null);   
        
        carton.setServiceOrder(serviceOrder); 
        miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
    	  billing = billingManager.get(serviceOrder.getId());
        carton.setUpdatedOn(new Date());
        //carton.setCorpID(sessionCorpID);
        carton.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	carton.setCreatedOn(new Date());
        }
         List maxIdNumber = cartonManager.findMaximumIdNumber(serviceOrder.getShipNumber());
	      if ( maxIdNumber.get(0) == null ) 
	      {          
	      	 idNumber = "01";
        }else {
	          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	        	if((autoId.toString()).length() == 1) 
	        		{
	        			idNumber = "0"+(autoId.toString());
	        		}else {
	        			 idNumber=autoId.toString();
	        			        
	        			}
	        	} 
	      if(isNew){
	    	  carton.setIdNumber(idNumber);
	      }else{
	    	  carton.setIdNumber(carton.getIdNumber());
	      }
	    carton.setDescription(carton.getCartonType());        
        carton=cartonManager.save(carton); 
        if(isNew && sessionCorpID.equalsIgnoreCase("UGWW")){
        	carton.setUgwIntId(carton.getId().toString());
        	 carton=cartonManager.save(carton); 
       	}
        serviceOrder=carton.getServiceOrder();
        try{
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			synchornizeCarton(serviceOrderRecords,carton,isNew,serviceOrder);
			
	   }
        /*Long StartTime = System.currentTimeMillis();
		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){ 
				//&& serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
			if(agentShipNumber!=null && !agentShipNumber.equals(""))
				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
   			
   			}
			
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
   
       }catch(Exception ex){
			System.out.println("\n\n\n\n\n checkRemoveLinks exception");
			ex.printStackTrace();
       }
        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
 	   {   
 	   }
        else
        {
        cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
 	    }
        if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
        {	
        }
        else{
        	cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
        }
        
        if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
  	   {   
  	   }
         else
         {
         cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
  	    }
         if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
         {	
         }
         else{
         	cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
         	}
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "carton.added" : "carton.updated";   
        saveMessage(getText(key));
        }
        if (!isNew) { 
        	serviceOrder=serviceOrderManager.get(serviceOrder.getId());
            carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(carton.getShipNumber())).get(0).toString()));
            carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(carton.getShipNumber())).get(0).toString()));
            carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(carton.getShipNumber())).get(0).toString()));
           // carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
            listOfPieces=cartonManager.getPieces(serviceOrder.getShipNumber());
            if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
            	carton.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
            }
            else{
            	carton.setTotalPieces(0);
            	
            }
            carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
            cartons = new ArrayList( serviceOrder.getCartons());
      	   	getSession().setAttribute("cartons", cartons);
      	   	hitFlag="1";
            return SUCCESS;   
        } else {   
        	idMax = Long.parseLong(cartonManager.findMaxId().get(0).toString());
            carton = cartonManager.get(idMax);
        }
        serviceOrder=serviceOrderManager.get(serviceOrder.getId());
        carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(carton.getShipNumber())).get(0).toString()));
        carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(carton.getShipNumber())).get(0).toString()));
        carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(carton.getShipNumber())).get(0).toString()));
     //   carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
        listOfPieces=cartonManager.getPieces(serviceOrder.getShipNumber());
        if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
        	carton.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
        }
        else{
        	carton.setTotalPieces(0);
        	
        }           
        carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
        cartons = new ArrayList( serviceOrder.getCartons());
  	   	getSession().setAttribute("cartons", cartons);
  	   	hitFlag="1";
  	   sid=serviceOrder.getId();
  	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;  
    
    }
    
    private void synchornizeCarton(List<Object> serviceOrderRecords,Carton carton, boolean isNew,ServiceOrder serviceOrderold) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){
    		try{
    			if(isNew){
    				Carton cartonNew= new Carton();
    				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(cartonNew, carton);
    				
    				cartonNew.setId(null);
    				cartonNew.setCorpID(serviceOrder.getCorpID());
    				cartonNew.setShipNumber(serviceOrder.getShipNumber());
    				cartonNew.setSequenceNumber(serviceOrder.getSequenceNumber());
    				cartonNew.setServiceOrder(serviceOrder);
    				cartonNew.setServiceOrderId(serviceOrder.getId());
    				cartonNew.setCreatedBy("Networking");
    				cartonNew.setUpdatedBy("Networking");
    				cartonNew.setCreatedOn(new Date());
    				cartonNew.setUpdatedOn(new Date()); 
    				// added to fixed issue of merge functionality in 2 step linking
    				try{
   					 List maxIdNumber = cartonManager.findMaximumIdNumber(serviceOrder.getShipNumber());
   				   	    if ( maxIdNumber.get(0) == null ) {          
   				           	 idNumber = "01";
   				           }else {
   				           	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
   					            //System.out.println(autoId);
   								if((autoId.toString()).length() == 1) {
   					            	idNumber = "0"+(autoId.toString());
   					            }else {
   					            	idNumber=autoId.toString();
   					        
   					            }
   				         }
    				}catch(Exception e){
   					System.out.println("Error in id number");
   					e.printStackTrace();
    				}
    				cartonNew.setIdNumber(idNumber);
    				try{
    					cartonNew.setTotalGrossWeight(new BigDecimal((cartonManager.getGrossNetwork(cartonNew.getShipNumber(),cartonNew.getCorpID())).get(0).toString()));
    					cartonNew.setTotalNetWeight(new BigDecimal((cartonManager.getNetNetwork(cartonNew.getShipNumber(),cartonNew.getCorpID())).get(0).toString()));
    					cartonNew.setTotalVolume(new BigDecimal((cartonManager.getVolumeNetwork(cartonNew.getShipNumber(),cartonNew.getCorpID())).get(0).toString()));
    	           // carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
    	            listOfPieces=cartonManager.getPiecesNetwork(cartonNew.getShipNumber(),cartonNew.getCorpID());
    	            if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
    	            	cartonNew.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
    	            }
    	            else{
    	            	cartonNew.setTotalPieces(0);
    	            	
    	            }
    	            cartonNew.setTotalTareWeight(new BigDecimal((cartonManager.getTareNetwork(cartonNew.getShipNumber(),cartonNew.getCorpID())).get(0).toString()));
    				}catch(Exception e){
    					 e.printStackTrace();	
    				}
    	            cartonNew = cartonManager.save(cartonNew); 
    	            
    	            if(cartonNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 cartonNew.setUgwIntId(cartonNew.getId().toString());
						 cartonNew= cartonManager.save(cartonNew);
						 }
					 
    				
    			}else{
    				Carton cartonTo= cartonManager.getForOtherCorpid(cartonManager.findRemoteCarton(carton.getIdNumber(),serviceOrder.getId())); 
        			Long id=cartonTo.getId();
        			String createdBy=cartonTo.getCreatedBy();
        			Date createdOn=cartonTo.getCreatedOn();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(cartonTo, carton);
    				
    				cartonTo.setId(id);
    				cartonTo.setCreatedBy(createdBy);
    				cartonTo.setCreatedOn(createdOn);
    				cartonTo.setCorpID(serviceOrder.getCorpID());
    				cartonTo.setShipNumber(serviceOrder.getShipNumber());
    				cartonTo.setSequenceNumber(serviceOrder.getSequenceNumber());
    				cartonTo.setServiceOrder(serviceOrder);
    				cartonTo.setServiceOrderId(serviceOrder.getId());
    				cartonTo.setUpdatedBy(carton.getCorpID()+":"+getRequest().getRemoteUser());
    				cartonTo.setUpdatedOn(new Date()); 
    				/*// added to fixed issue of merge functionality in 2 step linking
    				try{
   					 List maxIdNumber = cartonManager.findMaximumIdNumber(serviceOrder.getShipNumber());
   				   	    if ( maxIdNumber.get(0) == null ) {          
   				           	 idNumber = "01";
   				           }else {
   				           	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
   					            //System.out.println(autoId);
   								if((autoId.toString()).length() == 1) {
   					            	idNumber = "0"+(autoId.toString());
   					            }else {
   					            	idNumber=autoId.toString();
   					        
   					            }
   				         }
    				}catch(Exception e){
   					System.out.println("Error in id number");
    				}
    				cartonTo.setIdNumber(idNumber);*/
    				try{
    					cartonTo.setTotalGrossWeight(new BigDecimal((cartonManager.getGrossNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    					cartonTo.setTotalNetWeight(new BigDecimal((cartonManager.getNetNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    					cartonTo.setTotalVolume(new BigDecimal((cartonManager.getVolumeNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    	           // carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
    	            listOfPieces=cartonManager.getPiecesNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID());
    	            if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
    	            	cartonTo.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
    	            }
    	            else{
    	            	cartonTo.setTotalPieces(0);
    	            	
    	            }
    	            cartonTo.setTotalTareWeight(new BigDecimal((cartonManager.getTareNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    				}catch(Exception e){
    					e.printStackTrace();	
    				}
    				cartonTo = cartonManager.save(cartonTo);
    				 if(cartonTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    					 cartonTo.setUgwIntId(cartonTo.getId().toString());
    					 cartonTo= cartonManager.save(cartonTo);
						}    				
    			}
    			if(!cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
	  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
	  	   		}
    			if(!cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){    		         	
	         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
	         	}
	         
    			if(!cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){    		   	   		
	   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
	   	   		}
    			if(!cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){    		          	
	          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
    			}	
    			
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing carton"+ex);	
    			 ex.printStackTrace();
    		}
    	}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}

	private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
    	Iterator it =linkedShipNumber.iterator();
    	while(it.hasNext()){
    		String shipNumber= it.next().toString();
    		if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
        		Long id=serviceOrderManager.findRemoteServiceOrder(shipNumber);
        		if((id!=null)&&(id!=0)&&(!id.toString().equalsIgnoreCase(""))){
        			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(id);
    	    		recordList.add(serviceOrderRemote);
        		}
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return recordList;
	}

	private List findLinkedShipNumber(ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List linkedShipNumberList= new ArrayList();
    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	}else{
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	}
    	return linkedShipNumberList;
}

	public String updateStatus()
	{
    	String key = "Piece Count has been deleted successfully.";   
        saveMessage(getText(key));
        try{
            carton=cartonManager.get(id);
            serviceOrder=carton.getServiceOrder();
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			deleteCarton(serviceOrderRecords,carton.getIdNumber(),cartonStatus);
          }
          }catch(Exception ex){
        	  System.out.println("\n\n\n\n error while removing container--->"+ex);
        	  ex.printStackTrace();
          } 
		cartonManager.updateDeleteStatus(id);
		id=sid;
		cartonList();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	 private String fieldNetWeight1;
    private String fieldNetWeightVal1;
    private String fieldGrossWeight1;
    private String fieldGrossWeightVal1;
    private String fieldEmptyConWeight1;
    private String fieldEmptyConWeightVal1;
    private String fieldNetWeightKilo1;
    private String fieldNetWeightKiloVal1;
    private String fieldGrossWeightKilo1;
    private String fieldGrossWeightKiloVal1;
    private String fieldemptyContWeightKilo1;
    private String fieldemptyContWeightKiloVal1;
    private String fieldVolumeCbm1;
    private String fieldVolumeCbmVal1;
    private String fieldDensity1;
    private String fieldDensityVal1;
    private String fieldDensityMetric1;
    private String fieldDensityMetricVal1;
	 private String fieldVolume1;
	 private String  fieldVolumeVal1;
	 private String tableName1;
	 @SkipValidation
		public String updateCartonNetWeightDetailsAjax(){
		 		userName=getRequest().getRemoteUser();
		 		cartonManager.updateCartonNetWeightDetails(id,fieldNetWeight1,fieldNetWeightVal1,fieldNetWeightKilo1,fieldNetWeightKiloVal1,fieldGrossWeight1,fieldGrossWeightVal1,fieldGrossWeightKilo1,fieldGrossWeightKiloVal1,fieldEmptyConWeight1,fieldEmptyConWeightVal1,fieldemptyContWeightKilo1,fieldemptyContWeightKiloVal1,fieldVolume1,fieldVolumeVal1,fieldVolumeCbm1,fieldVolumeCbmVal1,fieldDensity1,fieldDensityVal1,fieldDensityMetric1,fieldDensityMetricVal1,tableName1,sessionCorpID,userName);
				carton = cartonManager.get(id);
				serviceOrder=carton.getServiceOrder();
		        try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeCarton(serviceOrderRecords,carton,false,serviceOrder);				
			   }
		       /* Long StartTime = System.currentTimeMillis();
				if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
					String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
					if(agentShipNumber!=null && !agentShipNumber.equals(""))
						customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
		   			
		   			}*/
		        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		  	   		}else{
		  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		  	   		}
		         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		         	}else{
		         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		         	}
		         
		         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		   	   		}else{
		   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		   	   		}
		          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		          	} else{
		          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		          	}
				    	   
							
						/*Long timeTaken =  System.currentTimeMillis() - StartTime;
						logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
			      
			   	}catch(Exception ex){
					System.out.println("\n\n\n\n\n checkRemoveLinks exception");
					ex.printStackTrace();
				} 		
			return SUCCESS;
		}
	
	 @SkipValidation
		public String updateCartonDetailsTareWtAjax(){
		 		userName=getRequest().getRemoteUser();
		 		cartonManager.updateCartonDetailsTareWt(id,fieldNetWeight1,fieldNetWeightVal1,fieldNetWeightKilo1,fieldNetWeightKiloVal1,fieldEmptyConWeight1,fieldEmptyConWeightVal1,fieldemptyContWeightKilo1,fieldemptyContWeightKiloVal1,fieldVolume1,fieldVolumeVal1,fieldVolumeCbm1,fieldVolumeCbmVal1,fieldDensity1,fieldDensityVal1,fieldDensityMetric1,fieldDensityMetricVal1,tableName1,sessionCorpID,userName);
				carton = cartonManager.get(id);
				serviceOrder=carton.getServiceOrder();
		        try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeCarton(serviceOrderRecords,carton,false,serviceOrder);				
			   }
		        /*Long StartTime = System.currentTimeMillis();
				if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
					String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
					if(agentShipNumber!=null && !agentShipNumber.equals(""))
						customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
		   			
		   			}*/
		        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		  	   		}else{
		  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		  	   		}
		         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		         	}else{
		         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		         	}
		         
		         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		   	   		}else{
		   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		   	   		}
		          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		          	} else{
		          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		          	}
				    	   
							
						/*Long timeTaken =  System.currentTimeMillis() - StartTime;
						logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
			      
			   	}catch(Exception ex){
					System.out.println("\n\n\n\n\n checkRemoveLinks exception");
					ex.printStackTrace();
				} 		
			return SUCCESS;
		}
	 private String fieldTypeName;
	 private String fieldTypeVal;
	 @SkipValidation
		public String updateCartonDetailsDensityAjax(){
		 		userName=getRequest().getRemoteUser();
		 		cartonManager.updateCartonDetailsDensity(id,fieldVolume1,fieldVolumeVal1,fieldVolumeCbm1,fieldVolumeCbmVal1,fieldDensity1,fieldDensityVal1,fieldDensityMetric1,fieldDensityMetricVal1,fieldTypeName,fieldTypeVal,tableName1,sessionCorpID,userName);
				carton = cartonManager.get(id);
				serviceOrder=carton.getServiceOrder();
		        try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeCarton(serviceOrderRecords,carton,false,serviceOrder);				
			   }
		        /*Long StartTime = System.currentTimeMillis();
				if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
					String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
					if(agentShipNumber!=null && !agentShipNumber.equals(""))
						customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
		   			
		   			}*/
		        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		  	   		}else{
		  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		  	   		}
		         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		         	}else{
		         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		         	}
		         
		         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		   	   		}else{
		   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		   	   		}
		          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		          	} else{
		          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		          	}
				    	   
							
						/*Long timeTaken =  System.currentTimeMillis() - StartTime;
						logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
			      
			   	}catch(Exception ex){
					System.out.println("\n\n\n\n\n checkRemoveLinks exception");
					ex.printStackTrace();
				} 		
			return SUCCESS;
		}
	 private String fieldUnit1;
	 private String fieldUnit1Val;
	 private String fieldUnit2;
	 private String fieldUnit2Val;
	 private String fieldUnit3;
	 private String fieldUnit3Val;
	 private String fieldLength;
	 private String fieldLengthVal;
	 private String fieldWidth;
	 private String fieldWidthVal;
	 private String fieldHeight;
	 private String fieldHeightVal;
	 private String fieldValueCartonVal;
	 private String userName;
	 @SkipValidation
		public String updateValueByCartonTypeAjax(){
		 		userName=getRequest().getRemoteUser();
		 		cartonManager.updateValueByCartonType(id,fieldUnit1,fieldUnit1Val,fieldUnit2,fieldUnit2Val,fieldUnit3,fieldUnit3Val,fieldLength,fieldLengthVal,fieldWidth,fieldWidthVal,fieldHeight,fieldHeightVal,fieldEmptyConWeight1,fieldEmptyConWeightVal1,fieldemptyContWeightKilo1,fieldemptyContWeightKiloVal1,fieldVolume1,fieldVolumeVal1,fieldVolumeCbm1,fieldVolumeCbmVal1,fieldDensity1,fieldDensityVal1,fieldDensityMetric1,fieldDensityMetricVal1,fieldValueCartonVal,tableName1,sessionCorpID,userName);
				carton = cartonManager.get(id);
				serviceOrder=carton.getServiceOrder();
		        try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeCarton(serviceOrderRecords,carton,false,serviceOrder);				
			   }
		        /*Long StartTime = System.currentTimeMillis();
				if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
					String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
					if(agentShipNumber!=null && !agentShipNumber.equals(""))
						customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
		   			
		   			}*/
		        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		  	   		}else{
		  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		  	   		}
		         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		         	}else{
		         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		         	}
		         
		         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
		   	   		}else{
		   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
		   	   		}
		          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
		          	} else{
		          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		          	}
				    	   
							
						/*Long timeTaken =  System.currentTimeMillis() - StartTime;
						logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
			      
			   	}catch(Exception ex){
					System.out.println("\n\n\n\n\n checkRemoveLinks exception");
					ex.printStackTrace();
				} 		
			return SUCCESS;
		}
    
//  End of Method.
    
    private void deleteCarton(List<Object> serviceOrderRecords, String idNumber,String cartonStatus) { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		cartonManager.deleteNetworkCarton(serviceOrder.getId(),idNumber,cartonStatus);
    	}
        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
  	   		}else{
  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
  	   		}
         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
         	}else{
         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
         	}
         
         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
   	   		}else{
   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
   	   		}
          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
          	} else{
          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
          	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
	}
    
    private String agentParentList1;
    private String cartonVal;
    @SkipValidation
	public String findUnitValuesByCartonAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			agentParentList1=cartonManager.findUnitValuesByCarton(cartonVal,sessionCorpID);
		} catch (Exception e) {
			
		}
	return SUCCESS;	
    }

	public void setSid(Long sid) {   
	       this.sid = sid;   
	   }
	public String getCountCortonNotes() {
		return countCortonNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public List getContainerNumberList() {
		return (containerNumberList!=null && !containerNumberList.isEmpty())?containerNumberList:cartonManager.getContainerNumber(shipNumber);
	 }

	public void setContainerNumberList(List containerNumberList) {
		this.containerNumberList = containerNumberList;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(sid);
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(sid);
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}  
	public String getValidateFormNav() {
        return validateFormNav;
        }
    public void setValidateFormNav(String validateFormNav) {
        this.validateFormNav = validateFormNav;
        }
    public String getGotoPageString() {
    return gotoPageString;
    }
    public void setGotoPageString(String gotoPageString) {
    this.gotoPageString = gotoPageString;
    }
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }
    public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
        this.serviceOrderManager = serviceOrderManager;
    }
    public  List getWeightunits() {
		return weightunits;
	}
    public  List getVolumeunits() {
		return volumeunits;
	}
    
      
    public void setId(Long id) {   
        this.id = id;   
    }   
      
    public Carton getCarton() {   
        return carton;   
    }   
      
    public void setCarton(Carton carton) {   
        this.carton = carton;   
    }   
    public ServiceOrder getServiceOrder() {   
        return serviceOrder;   
    }   
      
    public void setServiceOrder(ServiceOrder serviceOrder  ) {   
        this.serviceOrder = serviceOrder;   
    }   
    public List getCartons() {   
        return cartons;   
    }

	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}

	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public List getCustomerSO() {
		return customerSO;
	}

	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}

	/**
	 * @return the weightType
	 */
	public String getWeightType() {
		return weightType;
	}

	/**
	 * @param weightType the weightType to set
	 */
	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public List getCartonSO() {
		return cartonSO;
	}

	public void setCartonSO(List cartonSO) {
		this.cartonSO = cartonSO;
	}

	public String getCountChild() {
		return countChild;
	}

	public void setCountChild(String countChild) {
		this.countChild = countChild;
	}

	public String getMaxChild() {
		return maxChild;
	}

	public void setMaxChild(String maxChild) {
		this.maxChild = maxChild;
	}

	public String getMinChild() {
		return minChild;
	}

	public void setMinChild(String minChild) {
		this.minChild = minChild;
	}

	public Long getSidNum() {
		return sidNum;
	}

	public void setSidNum(Long sidNum) {
		this.sidNum = sidNum;
	}

	public Long getSoIdNum() {
		return soIdNum;
	}

	public void setSoIdNum(Long soIdNum) {
		this.soIdNum = soIdNum;
	}

	public long getTempcarton() {
		return tempcarton;
	}

	public void setTempcarton(long tempcarton) {
		this.tempcarton = tempcarton;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public Boolean getUnitType() {
		return UnitType;
	}

	public void setUnitType(Boolean unitType) {
		UnitType = unitType;
	}

	public Boolean getVolumeType() {
		return VolumeType;
	}

	public void setVolumeType(Boolean volumeType) {
		VolumeType = volumeType;
	}

	public String getCountBondedGoods() {
		return countBondedGoods;
	}

	public void setCountBondedGoods(String countBondedGoods) {
		this.countBondedGoods = countBondedGoods;
	}

	public void setCustomManager(CustomManager customManager) {
		this.customManager = customManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setCustomerFileAction(CustomerFileAction customerFileAction) {
		this.customerFileAction = customerFileAction;
	}

	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}

	public Long getSid() {
		return sid;
	}

	public Company getCompany() {
		return company;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Boolean getSurveyTab() {
		return surveyTab;
	}

	public void setSurveyTab(Boolean surveyTab) {
		this.surveyTab = surveyTab;
	}

	public List getContainerNumberListCarton() {
		return containerNumberListCarton;
	}

	public void setContainerNumberListCarton(List containerNumberListCarton) {
		this.containerNumberListCarton = containerNumberListCarton;
	}

	public List getCartonTypeValue() {
		return cartonTypeValue;
	}

	public void setCartonTypeValue(List cartonTypeValue) {
		this.cartonTypeValue = cartonTypeValue;
	}

	public String getAgentParentList1() {
		return agentParentList1;
	}

	public void setAgentParentList1(String agentParentList1) {
		this.agentParentList1 = agentParentList1;
	}

	public String getCartonVal() {
		return cartonVal;
	}

	public void setCartonVal(String cartonVal) {
		this.cartonVal = cartonVal;
	}

	public String getFieldNetWeight1() {
		return fieldNetWeight1;
	}

	public void setFieldNetWeight1(String fieldNetWeight1) {
		this.fieldNetWeight1 = fieldNetWeight1;
	}

	public String getFieldNetWeightVal1() {
		return fieldNetWeightVal1;
	}

	public void setFieldNetWeightVal1(String fieldNetWeightVal1) {
		this.fieldNetWeightVal1 = fieldNetWeightVal1;
	}

	public String getFieldGrossWeight1() {
		return fieldGrossWeight1;
	}

	public void setFieldGrossWeight1(String fieldGrossWeight1) {
		this.fieldGrossWeight1 = fieldGrossWeight1;
	}

	public String getFieldGrossWeightVal1() {
		return fieldGrossWeightVal1;
	}

	public void setFieldGrossWeightVal1(String fieldGrossWeightVal1) {
		this.fieldGrossWeightVal1 = fieldGrossWeightVal1;
	}

	public String getFieldEmptyConWeight1() {
		return fieldEmptyConWeight1;
	}

	public void setFieldEmptyConWeight1(String fieldEmptyConWeight1) {
		this.fieldEmptyConWeight1 = fieldEmptyConWeight1;
	}

	public String getFieldEmptyConWeightVal1() {
		return fieldEmptyConWeightVal1;
	}

	public void setFieldEmptyConWeightVal1(String fieldEmptyConWeightVal1) {
		this.fieldEmptyConWeightVal1 = fieldEmptyConWeightVal1;
	}

	public String getFieldNetWeightKilo1() {
		return fieldNetWeightKilo1;
	}

	public void setFieldNetWeightKilo1(String fieldNetWeightKilo1) {
		this.fieldNetWeightKilo1 = fieldNetWeightKilo1;
	}

	public String getFieldNetWeightKiloVal1() {
		return fieldNetWeightKiloVal1;
	}

	public void setFieldNetWeightKiloVal1(String fieldNetWeightKiloVal1) {
		this.fieldNetWeightKiloVal1 = fieldNetWeightKiloVal1;
	}

	public String getFieldGrossWeightKilo1() {
		return fieldGrossWeightKilo1;
	}

	public void setFieldGrossWeightKilo1(String fieldGrossWeightKilo1) {
		this.fieldGrossWeightKilo1 = fieldGrossWeightKilo1;
	}

	public String getFieldGrossWeightKiloVal1() {
		return fieldGrossWeightKiloVal1;
	}

	public void setFieldGrossWeightKiloVal1(String fieldGrossWeightKiloVal1) {
		this.fieldGrossWeightKiloVal1 = fieldGrossWeightKiloVal1;
	}

	public String getFieldemptyContWeightKilo1() {
		return fieldemptyContWeightKilo1;
	}

	public void setFieldemptyContWeightKilo1(String fieldemptyContWeightKilo1) {
		this.fieldemptyContWeightKilo1 = fieldemptyContWeightKilo1;
	}

	public String getFieldemptyContWeightKiloVal1() {
		return fieldemptyContWeightKiloVal1;
	}

	public void setFieldemptyContWeightKiloVal1(String fieldemptyContWeightKiloVal1) {
		this.fieldemptyContWeightKiloVal1 = fieldemptyContWeightKiloVal1;
	}

	public String getFieldVolumeCbm1() {
		return fieldVolumeCbm1;
	}

	public void setFieldVolumeCbm1(String fieldVolumeCbm1) {
		this.fieldVolumeCbm1 = fieldVolumeCbm1;
	}

	public String getFieldVolumeCbmVal1() {
		return fieldVolumeCbmVal1;
	}

	public void setFieldVolumeCbmVal1(String fieldVolumeCbmVal1) {
		this.fieldVolumeCbmVal1 = fieldVolumeCbmVal1;
	}

	public String getFieldDensity1() {
		return fieldDensity1;
	}

	public void setFieldDensity1(String fieldDensity1) {
		this.fieldDensity1 = fieldDensity1;
	}

	public String getFieldDensityVal1() {
		return fieldDensityVal1;
	}

	public void setFieldDensityVal1(String fieldDensityVal1) {
		this.fieldDensityVal1 = fieldDensityVal1;
	}

	public String getFieldDensityMetric1() {
		return fieldDensityMetric1;
	}

	public void setFieldDensityMetric1(String fieldDensityMetric1) {
		this.fieldDensityMetric1 = fieldDensityMetric1;
	}

	public String getFieldDensityMetricVal1() {
		return fieldDensityMetricVal1;
	}

	public void setFieldDensityMetricVal1(String fieldDensityMetricVal1) {
		this.fieldDensityMetricVal1 = fieldDensityMetricVal1;
	}

	public String getFieldVolume1() {
		return fieldVolume1;
	}

	public void setFieldVolume1(String fieldVolume1) {
		this.fieldVolume1 = fieldVolume1;
	}

	public String getFieldVolumeVal1() {
		return fieldVolumeVal1;
	}

	public void setFieldVolumeVal1(String fieldVolumeVal1) {
		this.fieldVolumeVal1 = fieldVolumeVal1;
	}

	public String getTableName1() {
		return tableName1;
	}

	public void setTableName1(String tableName1) {
		this.tableName1 = tableName1;
	}

	public String getFieldTypeName() {
		return fieldTypeName;
	}

	public void setFieldTypeName(String fieldTypeName) {
		this.fieldTypeName = fieldTypeName;
	}

	public String getFieldTypeVal() {
		return fieldTypeVal;
	}

	public void setFieldTypeVal(String fieldTypeVal) {
		this.fieldTypeVal = fieldTypeVal;
	}

	public String getFieldUnit1() {
		return fieldUnit1;
	}

	public void setFieldUnit1(String fieldUnit1) {
		this.fieldUnit1 = fieldUnit1;
	}

	public String getFieldUnit1Val() {
		return fieldUnit1Val;
	}

	public void setFieldUnit1Val(String fieldUnit1Val) {
		this.fieldUnit1Val = fieldUnit1Val;
	}

	public String getFieldUnit2() {
		return fieldUnit2;
	}

	public void setFieldUnit2(String fieldUnit2) {
		this.fieldUnit2 = fieldUnit2;
	}

	public String getFieldUnit2Val() {
		return fieldUnit2Val;
	}

	public void setFieldUnit2Val(String fieldUnit2Val) {
		this.fieldUnit2Val = fieldUnit2Val;
	}

	public String getFieldUnit3() {
		return fieldUnit3;
	}

	public void setFieldUnit3(String fieldUnit3) {
		this.fieldUnit3 = fieldUnit3;
	}

	public String getFieldUnit3Val() {
		return fieldUnit3Val;
	}

	public void setFieldUnit3Val(String fieldUnit3Val) {
		this.fieldUnit3Val = fieldUnit3Val;
	}

	public String getFieldLength() {
		return fieldLength;
	}

	public void setFieldLength(String fieldLength) {
		this.fieldLength = fieldLength;
	}

	public String getFieldLengthVal() {
		return fieldLengthVal;
	}

	public void setFieldLengthVal(String fieldLengthVal) {
		this.fieldLengthVal = fieldLengthVal;
	}

	public String getFieldWidth() {
		return fieldWidth;
	}

	public void setFieldWidth(String fieldWidth) {
		this.fieldWidth = fieldWidth;
	}

	public String getFieldWidthVal() {
		return fieldWidthVal;
	}

	public void setFieldWidthVal(String fieldWidthVal) {
		this.fieldWidthVal = fieldWidthVal;
	}

	public String getFieldHeight() {
		return fieldHeight;
	}

	public void setFieldHeight(String fieldHeight) {
		this.fieldHeight = fieldHeight;
	}

	public String getFieldHeightVal() {
		return fieldHeightVal;
	}

	public void setFieldHeightVal(String fieldHeightVal) {
		this.fieldHeightVal = fieldHeightVal;
	}

	public String getFieldValueCartonVal() {
		return fieldValueCartonVal;
	}

	public void setFieldValueCartonVal(String fieldValueCartonVal) {
		this.fieldValueCartonVal = fieldValueCartonVal;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCartonStatus() {
		return cartonStatus;
	}

	public void setCartonStatus(String cartonStatus) {
		this.cartonStatus = cartonStatus;
	}

	public String getSaveAndAddFlag() {
		return saveAndAddFlag;
	}

	public void setSaveAndAddFlag(String saveAndAddFlag) {
		this.saveAndAddFlag = saveAndAddFlag;
	}

	public String getCartonEditURL() {
		return cartonEditURL;
	}

	public void setCartonEditURL(String cartonEditURL) {
		this.cartonEditURL = cartonEditURL;
	}

	public Map<String, String> getLengthunits() {
		return lengthunits;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	
}
   
//End of Class.   