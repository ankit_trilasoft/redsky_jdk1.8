<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="Schedule Resources"/> 
<title>Schedule Resources</title> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
   
<script language="javascript" type="text/javascript">
function setServiceGroup() { 
  var serviceGroup = document.forms['timeSheetSearchForm'].elements['workticketServiceGroup'].value; 
  document.forms['timeSheetSearchForm'].elements['serviceGroup'].value=serviceGroup;
  var url="serviceType.html?ajax=1&decorator=simple&popup=true&serviceGroup="+encodeURI(serviceGroup);
  http6.open("GET", url, true);
  http6.onreadystatechange = handleHttpResponse4;
  http6.send(null);
 }		 
function handleHttpResponse4()  {
		 if (http6.readyState == 4)  {
         var results = http6.responseText
         results = results.trim();
         resu = results.replace("{",'');
         resu = resu.replace("}",'');
		 var res = resu.split(",");
		 document.forms['timeSheetSearchForm'].elements['workticketService'].options.length = res.length;		 
         for(i=0;i<res.length;i++) {
         if(res[i] == ''){
			document.forms['timeSheetSearchForm'].elements['workticketService'].options[i].text = '';
			document.forms['timeSheetSearchForm'].elements['workticketService'].options[i].value = '';
		 }else{
		var resd = res[i].split("=");
		document.forms['timeSheetSearchForm'].elements['workticketService'].options[i].text = resd[1].trim();
		document.forms['timeSheetSearchForm'].elements['workticketService'].options[i].value = resd[0].trim();
	}
   } 
  }
 }
              
var http6 = getHTTPObject2();
function getHTTPObject2(){
   var xmlhttp;
   if(window.XMLHttpRequest){
       xmlhttp = new XMLHttpRequest();
   }else if (window.ActiveXObject){
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       if (!xmlhttp){
           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
       }
   }
   return xmlhttp;
  }



function clear_fields(){
			document.forms['timeSheetSearchForm'].elements['workDt'].value = "";
			document.forms['timeSheetSearchForm'].elements['tktWareHse'].value = "";
			document.forms['timeSheetSearchForm'].elements['workticketService'].value = "";
			document.forms['timeSheetSearchForm'].elements['workticketServiceGroup'].value = "";
			document.forms['timeSheetSearchForm'].elements['workticketStatus'].value = "";
}
function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
function checkCondition(){
	if(document.forms['timeSheetSearchForm'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetSearchForm'].elements['workDt'].select();
 		return false;
 	}
 	
 	if(document.forms['timeSheetSearchForm'].elements['tktWareHse'].value == ""){
 		alert("Select WareHouse to continue.....");
 		return false;
 	}
 	selectTicketWarehouse();
}

function selectTicketWarehouse()
{
	document.forms['timeSheetSearchForm'].elements['wareHse'].value=document.forms['timeSheetSearchForm'].elements['tktWareHse'].value;
	document.forms['timeSheetSearchForm'].elements['truckWareHse'].value=document.forms['timeSheetSearchForm'].elements['tktWareHse'].value;
}

function clear_fields(){
	document.forms['timeSheetSearchForm'].elements['workDt'].value = "";
	document.forms['timeSheetSearchForm'].elements['tktWareHse'].value = "";
	document.forms['timeSheetSearchForm'].elements['workticketService'].value = "";
	document.forms['timeSheetSearchForm'].elements['workticketServiceGroup'].value = "";
	//setServiceGroup();
}
</script>

<style>

.topHead { background:url transparent (images/topbandnew.jpg) no-repeat scroll 0%;}

</style>
</head>
<s:form name="timeSheetSearchForm" action="searchScheduleResouce.html?filter=hidden" cssClass="form_magn">
<configByCorp:fieldVisibility componentId="component.field.scheduleResource.crewTypeAndClass">
	<s:hidden name="defaultSortValueFlag" value="typeofWork"/>
</configByCorp:fieldVisibility>
<s:hidden name="serviceGroup"/>
<c:set var="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="wareHse" />
<s:hidden name="truckWareHse"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="layer1" style="width:100%;"><!-- sandeep -->
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="4" border="0" style="width:564px" >
<thead>
<tr>
<td align="right" class="listwhitetext" >Work Date<font color="red" size="2">*</font></td>
<td align="left" style="width:450px"><s:textfield cssClass="input-text" id="date1" name="workDt" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</tr>
	<!--<tr><td align="right" class="listwhitetext" width="100px">Operation Hub<font color="red" size="2">*</font></td>
		<td style="width:250px"><s:select name="operationHub" list="{'A','B','C'}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" tabindex="1" /></td>
	</tr>
	-->
	<tr><td align="right" class="listwhitetext" width="100px">Warehouse<font color="red" size="2">*</font></td>
		<td style="width:250px"><s:select name="tktWareHse" list="%{wareHouse}" value="%{dfltWhse}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" tabindex="2" onchange="selectTicketWarehouse()"/></td>
	</tr>

	<tr>
	<td align="right" class="listwhitetext" width="100px">Service Group</td>
	<td><s:select name="workticketServiceGroup" list="%{tcktGrp}" cssStyle="width:250px" cssClass="list-menu" onchange="return setServiceGroup();" headerKey="" headerValue="" /></td>
	</tr>

	<tr>
	<td align="right" class="listwhitetext" width="100px">Service</td>
	<td><s:select name="workticketService" list="%{serviceType}" cssStyle="width:250px" cssClass="list-menu" /></td>
	</tr>
	<tr>
	<td align="right" class="listwhitetext" width="100px">Status</td>
	<td><s:select name="workticketStatus" list="%{tcktactn}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" /></td>
	</tr>
	<tr><td align="left" style="padding-left:114px;" colspan="2">
		<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.search" onclick="return checkCondition();" tabindex="6"/>
		<input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/></td> 
			
	</tr>
	<tr><td height="13px"></td></tr>
		</thead>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>

<script type="text/javascript">
	setCalendarFunctionality();
</script>