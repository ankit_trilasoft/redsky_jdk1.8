<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";
while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_FINANCE") || userRole.equalsIgnoreCase("ROLE_BILLING")){
		userRole=role.getName();
		break;
	}	
}%>
<head>
<title><fmt:message key="workTicketDetail.title" /></title>
<meta name="heading"
	content="<fmt:message key='workTicketDetail.heading'/>" />
<script type="text/JavaScript">
function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}
</script> 
<style type="text/css">
  h2 {background-color: #FBBFFF}
  .upper-case{
	text-transform:capitalize;
}
 .ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
   div.error, span.error, li.error, div.message {
  text-align:left;
  width:600px;
  }
   div#successMessages {
  text-align:center;
  }
  div.error img.icon {padding-left:10%;}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('itemData', 'fade=1,hide=0');
animatedcollapse.addDiv('handOutForCorex', 'fade=1,hide=0');
animatedcollapse.addDiv('handout', 'fade=1,hide=0');
<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
  animatedcollapse.addDiv('details', 'persist=0,show=1');
</configByCorp:fieldVisibility>
animatedcollapse.addDiv('storage', 'fade=1,hide=1');
animatedcollapse.addDiv('address', 'fade=1,hide=0');
animatedcollapse.addDiv('confirmation', 'fade=1,hide=1');
animatedcollapse.addDiv('billing', 'fade=1,hide=1');
animatedcollapse.addDiv('staff', 'fade=1,hide=1');
animatedcollapse.init();
</script>
<script language="javascript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" /> 

<jsp:include flush="true" page="workTicketFormJS.jsp"></jsp:include> 
<script>
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
}
function disable_elevator()
{
  var test13 = document.forms['workTicketForm'].elements['workTicket.destinationElevator'].checked; 
   if(test13 == true){		
		}else{		
		}		  	   }
function disable_site_origin(){
 var test13 = document.forms['workTicketForm'].elements['workTicket.siteS'].value; 
   var el = document.getElementById('hidStatusReg26');	
   var gg = document.getElementById('hidStatusReg27');	
   if((test13 != '')||(test13 == null)){	
		el.style.display = 'block';
		gg.style.display = 'block';
		}else{
		el.style.display = 'none';
		gg.style.display = 'none';
		}		  	   
}

function disable_site_destination()
{
 var test13 = document.forms['workTicketForm'].elements['workTicket.siteD'].value;    

   var el = document.getElementById('hidStatusReg24');	
   var gg = document.getElementById('hidStatusReg25');	
   if(test13 != ''){	
		el.style.display = 'block';
		gg.style.display = 'block';
		}else{
		el.style.display = 'none';
		gg.style.display = 'none';
		}		  	   
}

function disable_elevator_origin()
{
  var test13 = document.forms['workTicketForm'].elements['workTicket.originElevator'].checked;  
   if(test13 == true){			
		}else{	
		}		  	   
}
function findCityStateNotPrimary(targetField, position){
if(targetField=='OZ'){
	 var zipCode = document.forms['workTicketForm'].elements['workTicket.zip'].value;
	 var countryCode=document.forms['workTicketForm'].elements['workTicket.originCountryCode'].value;
	 } else if (targetField=='DZ'){
	 var zipCode = document.forms['workTicketForm'].elements['workTicket.destinationZip'].value;
	 var countryCode=document.forms['workTicketForm'].elements['workTicket.destinationCountryCode'].value;
	 }
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
     ajax_showTooltip(url,position);	
 }
function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['workTicketForm'].elements['workTicket.city'].value = id;
	document.forms['workTicketForm'].elements['workTicket.city'].focus();
	 } else if (targetField=='DZ'){
	document.forms['workTicketForm'].elements['workTicket.destinationCity'].value = id;
	document.forms['workTicketForm'].elements['workTicket.destinationCity'].focus();
	} }
	
	function findCityState1(targetElement,targetField){
	var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['workTicketForm'].elements['workTicket.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['workTicketForm'].elements['workTicket.destinationCountryCode'].value;
     }
     var url1="findCityStateFlex.html?ajax=1&decorator=simple&popup=true&countryCodeFlex="+ encodeURI(countryCode);
     http99.open("GET", url1, true);
     http99.onreadystatechange = function(){ handleHttpResponseCityStateFlex(targetElement,targetField);};
     http99.send(null);
    }
    
     function handleHttpResponseCityStateFlex(targetElement,targetField){
              if (http99.readyState == 4){
                var results = http99.responseText
                results = results.trim();
                if(results.length>0) {
                     if(results=='Y' ){
                        findCityState(targetElement,targetField);
                     }
                   }                                                
               }
           }
function findCityState(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['workTicketForm'].elements['workTicket.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['workTicketForm'].elements['workTicket.destinationCountryCode'].value;
     }
    var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
    }    
  function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
       if(targetField=='OZ'){
       var countryCode=document.forms['workTicketForm'].elements['workTicket.originCountryCode'].value;
        if(document.forms['workTicketForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['workTicketForm'].elements['workTicket.destinationCountryCode'].value;
          if( document.forms['workTicketForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{  
             } 
        }  
      }  
   function handleHttpResponseCityState(targetField){
                  if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['workTicketForm'].elements['workTicket.city'].value=res[0];
	           	 document.forms['workTicketForm'].elements['workTicket.state'].value=res[2];
	            if (document.forms['workTicketForm'].elements['workTicket.homePhone'].value=='') {
	           	 document.forms['workTicketForm'].elements['workTicket.homePhone'].value=(res[4].substring(0,19));
	             } 
	              document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['workTicketForm'].elements['workTicket.city'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['workTicketForm'].elements['workTicket.destinationCity'].value=res[0];
	           	 document.forms['workTicketForm'].elements['workTicket.destinationState'].value=res[2];
	            if (document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value=='') {
	           	 document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            if (document.forms['workTicketForm'].elements['workTicket.contactPhone'].value=='') {
		           	 document.forms['workTicketForm'].elements['workTicket.contactPhone'].value=(res[4].substring(0,19));
		           }
	            document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['workTicketForm'].elements['workTicket.destinationCity'].focus();
	           	} } } else { }
             }   
function zipCode(){
        var originCountryCode=document.forms['workTicketForm'].elements['workTicket.originCountry'].value; 
		var el = document.getElementById('zipCodeRequiredTrue');
		var el1 = document.getElementById('zipCodeRequiredFalse');
		if(originCountryCode == 'United States'){
		el.style.display = 'block';		
		el1.style.display = 'none';		
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		}
}// End Of Method
</script>  
<SCRIPT LANGUAGE="JavaScript">
var location1;
var location2;	
var address1;
var address2;
var latlng;
var geocoder;
var map;	
var distance;
var gDir;	
function initLoader() { 
	 var script = document.createElement("script");
     script.type = "text/javascript";
	script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=initialize";
     document.body.appendChild(script);
     setTimeout('',6000);
}
function initialize() {
	 geocoder = new GClientGeocoder();
	 gDir = new GDirections();
	 GEvent.addListener(gDir, "load", function() {
	 var drivingDistanceMiles = gDir.getDistance().meters / 1609.344;
	 var drivingDistanceKilometers = gDir.getDistance().meters / 1000;
	 var chkSelected=document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].value;
	 var sysUnit=document.forms['workTicketForm'].elements['unit'].value;
	
	if(chkSelected==''){
	chkSelected=sysUnit;
	}
	if(chkSelected=='Mile'){
	document.forms['workTicketForm'].elements['workTicket.distance'].value=Math.round(drivingDistanceMiles*100)/100;
	document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].options[2].text = 'Mile';
	document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].options[2].value = 'Mile';
	document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].options[2].selected=true;
	}
	if(chkSelected=='KM'){
	document.forms['workTicketForm'].elements['workTicket.distance'].value=Math.round(drivingDistanceKilometers*100)/100;
	document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].options[1].text = 'KM';
	document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].options[1].value = 'KM';
	document.forms['workTicketForm'].elements['workTicket.distanceInKmMiles'].options[1].selected=true;	
	}
});
}
function calculateDistance() {
	var origAddress1=document.forms['workTicketForm'].elements['workTicket.address1'].value;
	var origAddress2=document.forms['workTicketForm'].elements['workTicket.address2'].value;
	var origAddress3=document.forms['workTicketForm'].elements['workTicket.address3'].value;
	var origCity=document.forms['workTicketForm'].elements['workTicket.city'].value;
	var origZip=document.forms['workTicketForm'].elements['workTicket.zip'].value;
	var origState=document.forms['workTicketForm'].elements['workTicket.state'].value;
	var dd = document.forms['workTicketForm'].elements['workTicket.originCountry'].selectedIndex;
	var origCountry = document.forms['workTicketForm'].elements['workTicket.originCountry'].options[dd].text;
  	var address1 = origAddress1+" "+origAddress2+" "+origAddress3+","+origCity+" , "+origZip+","+origState+","+origCountry;
	var destAddress1=document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value;
	var destAddress2=document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value;
	var destAddress3=document.forms['workTicketForm'].elements['workTicket.destinationAddress3'].value;				
	var destCity=document.forms['workTicketForm'].elements['workTicket.destinationCity'].value;
	var destZip=document.forms['workTicketForm'].elements['workTicket.destinationZip'].value;
	var destState=document.forms['workTicketForm'].elements['workTicket.destinationState'].value;
	var dd = document.forms['workTicketForm'].elements['workTicket.destinationCountry'].selectedIndex;
	var destCountry = document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[dd].text;
  	var address2 = destAddress1+" "+destAddress2+" "+destAddress3+","+destCity+" , "+destZip+","+destState+","+destCountry;
  	
	geocoder.getLocations(address1, function (response) {
		if (!response || response.Status.code != 200){
				alert("Sorry, we were unable to geocode the first address");
			}else{
				location1 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
				geocoder.getLocations(address2, function (response) {
				if (!response || response.Status.code != 200){
					alert("Sorry, we were unable to geocode the second address");
				}else{
					location2 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
					gDir.load('from: ' + location1.address + ' to: ' + location2.address);
				}
			});
		}
	});
}
function serviceCheckingBeginDate()
{
if(document.forms['workTicketForm'].elements['serviceCheckingBeginDateVar'].value== '1'){
 var date1 = document.forms['workTicketForm'].elements['workTicket.date1'].value;
 var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate();
		if(daym<10)
		daym="0"+daym
		var date2 = daym+"-"+month+"-"+y.substring(2,4);
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan') {month = "01";
   } else if(month == 'Feb') {month = "02";
   } else if(month == 'Mar') {month = "03"
   } else if(month == 'Apr') {month = "04"
   } else if(month == 'May') {month = "05"
   } else if(month == 'Jun') {month = "06"
   } else if(month == 'Jul') {month = "07"
   } else if(month == 'Aug') {month = "08"
   } else if(month == 'Sep') {month = "09"
   } else if(month == 'Oct') {month = "10"
   } else if(month == 'Nov') {month = "11"
   } else if(month == 'Dec') {month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan') {month2 = "01";
   } else if(month2 == 'Feb') {month2 = "02";
   } else if(month2 == 'Mar') {month2 = "03"
   } else if(month2 == 'Apr') {month2 = "04"
   } else if(month2 == 'May') {month2 = "05"
   } else if(month2 == 'Jun') {month2 = "06"
   } else if(month2 == 'Jul') {month2 = "07"
   } else if(month2 == 'Aug') {month2 = "08"
   } else if(month2 == 'Sep') {month2 = "09"
   } else if(month2 == 'Oct') {month2 = "10"
   } else if(month2 == 'Nov') {month2 = "11"
   } else if(month2 == 'Dec') {month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  if(daysApart<0 && document.forms['workTicketForm'].elements['workTicket.date2'].value == ''){
  var daysApartAbs = Math.abs(daysApart);
    var agree= confirm("The service date is "+daysApartAbs+" days in the past, do you want to confirm? Hit OK to accept or Cancel to change the date.");
    if(agree){
    	document.forms['workTicketForm'].elements['workTicket.date2'].value = document.forms['workTicketForm'].elements['workTicket.date1'].value;   
    }else{
    document.forms['workTicketForm'].elements['workTicket.date2'].value='';
    }
  }else{
	  if(document.forms['workTicketForm'].elements['workTicket.date2'].value == ''){
	  document.forms['workTicketForm'].elements['workTicket.date2'].value = document.forms['workTicketForm'].elements['workTicket.date1'].value;
	  }
    }
    
}
document.forms['workTicketForm'].elements['serviceCheckingBeginDateVar'].value= '0';
}
function changeServiceCheckingBeginDateVar(){
document.forms['workTicketForm'].elements['serviceCheckingBeginDateVar'].value= '1';      
}
function serviceCheckingEndDate(){
if(document.forms['workTicketForm'].elements['serviceCheckingEndDateVar'].value== '1'){
 var date1 = document.forms['workTicketForm'].elements['workTicket.date2'].value;
 var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate();
		if(daym<10)
		daym="0"+daym
		var date2 = daym+"-"+month+"-"+y.substring(2,4);
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan') {month = "01";
   }  else if(month == 'Feb'){month = "02";
   } else if(month == 'Mar') {month = "03"
   } else if(month == 'Apr') {month = "04"
   } else if(month == 'May') {month = "05"
   } else if(month == 'Jun') {month = "06"
   } else if(month == 'Jul') {month = "07"
   } else if(month == 'Aug') {month = "08"
   } else if(month == 'Sep') {month = "09"
   } else if(month == 'Oct') {month = "10"
   } else if(month == 'Nov') {month = "11"
   } else if(month == 'Dec') {month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan') {month2 = "01";
   } else if(month2 == 'Feb') {month2 = "02";
   } else if(month2 == 'Mar') {month2 = "03"
   } else if(month2 == 'Apr') {month2 = "04"
   } else if(month2 == 'May') {month2 = "05"
   } else if(month2 == 'Jun') {month2 = "06"
   } else if(month2 == 'Jul') {month2 = "07"
   } else if(month2 == 'Aug') {month2 = "08"
   } else if(month2 == 'Sep') {month2 = "09"
   } else if(month2 == 'Oct') {month2 = "10"
   } else if(month2 == 'Nov') {month2 = "11"
   } else if(month2 == 'Dec') {month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  if(daysApart<0)
  {
  var daysApartAbs = Math.abs(daysApart);
    var agree= confirm("The service date is "+daysApartAbs+" days in the past, do you want to confirm? Hit OK to accept or Cancel to change the date.");
    if(agree){ 
    }else{
    document.forms['workTicketForm'].elements['workTicket.date2'].value='';
    }
  }
  }
document.forms['workTicketForm'].elements['serviceCheckingEndDateVar'].value='0';
}

function changeServiceCheckingEndDateVar(){
document.forms['workTicketForm'].elements['serviceCheckingEndDateVar'].value= '1';      
}
function findReleasedStorageDate(){
	var shipNumber = document.forms['workTicketForm'].elements['shipnum'].value;
	var url="countReleaseStorageDate.html?ajax=1&decorator=simple&popup=true&shipnum="+ encodeURI(shipNumber);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponseReleasedStorageDate;
     http9.send(null); 
     }     
  function handleHttpResponseReleasedStorageDate(){
             if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res1 = results.replace("[",'');
                res1 = res1.replace("]",'');
                if(res1.length>=1){
                document.forms['workTicketForm'].elements['releasedStorageDate'].value=res1;
                }else{
                document.forms['workTicketForm'].elements['releasedStorageDate'].value="";
                }
             }    
    } 

function checkVendorName(){
    var vendorId = 
document.forms['workTicketForm'].elements['workTicket.account'].value;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + 
encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1){
                 //alert(results);
                 } else {
                     alert("Invalid Bill to code, Please select another");
  document.forms['workTicketForm'].elements['workTicket.account'].value='';                   
  document.forms['workTicketForm'].elements['workTicket.account'].select();
                 }
             }
        }

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http5 = getHTTPObject();
    var http9 = getHTTPObject9();
   function getHTTPObject9(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
		function setTimeMask() {
		oTimeMask1 = new Mask("##:##", "time1");
		oTimeMask1.attach(document.forms['workTicketForm'].time1);
		
		oTimeMask2 = new Mask("##:##", "time2");
		oTimeMask2.attach(document.forms['workTicketForm'].time2);
		oTimeMask3 = new Mask("##:##", "time3");
		oTimeMask3.attach(document.forms['workTicketForm'].time3);
		
		oTimeMask4 = new Mask("##:##", "time4");
		oTimeMask4.attach(document.forms['workTicketForm'].time4);
		oTimeMask5 = new Mask("##:##", "time5");
		oTimeMask5.attach(document.forms['workTicketForm'].time5);
		
		oTimeMask6 = new Mask("##:##", "time6");
		oTimeMask6.attach(document.forms['workTicketForm'].time6);
		
		}
		function setTimeMask12() {
		oTimeMask1 = new Mask("##:##", "time3");
		oTimeMask1.attach(document.forms['workTicketForm'].time3);
		
		oTimeMask2 = new Mask("##:##", "time4");
		oTimeMask2.attach(document.forms['workTicketForm'].time4);
		}

	function getOriginCountryCode(){
		var countryName=document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
		var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
	    http4.open("GET", url, true);
	    http4.onreadystatechange = handleHttpResponseCountryName;
	    http4.send(null);
	}	
	function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['workTicketForm'].elements['workTicket.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['workTicketForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['workTicketForm'].elements['originCountryFlex3Value'].value='N';
				 	}try{
				 	document.forms['workTicketForm'].elements['workTicket.originCountry'].select();	
				 	}catch (e){}	     	 
 				}else{
                     
                 }
             }
            }
                    
	function getDestinationCountryCode(){
		var countryName=document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
		var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
	    http4.open("GET", url, true);
	    http4.onreadystatechange = httpDestinationCountryName;
	    http4.send(null);
	}
    function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['workTicketForm'].elements['workTicket.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['workTicketForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['workTicketForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}	try{
 					document.forms['workTicketForm'].elements['workTicket.destinationCountry'].select();
 					}catch(e){}
				}else{
                     
                 }
             }
        }
function myDate() {

	var f = document.getElementById('workTicketForm'); 
	f.setAttribute("autocomplete", "off"); 
	}
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
		function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['workTicket.statusDate'].value=datam;
		
	}
	
function autoPopulate_instruction(targetElement) {
		var instructionCode=targetElement.options[targetElement.selectedIndex].text;
	    var instructions = targetElement.form.elements['workTicket.instructions'].value;
	    if(instructions == '')
	    {
	    	targetElement.form.elements['workTicket.instructions'].value=instructionCode;
	    }
	}
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please enter a valid phone number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }
	function openOriginLocation() {
		var city = document.forms['workTicketForm'].elements['workTicket.city'].value;
		var country = document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
		var zip = document.forms['workTicketForm'].elements['workTicket.zip'].value;
		var address1 = document.forms['workTicketForm'].elements['workTicket.address1'].value;
		var address2 = document.forms['workTicketForm'].elements['workTicket.address2'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+ address1+','+address2+ ',' +city+','+zip+','+country);
}
	function openDestinationLocation() {
		var city = document.forms['workTicketForm'].elements['workTicket.destinationCity'].value;
		var country = document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
		var zip = document.forms['workTicketForm'].elements['workTicket.destinationZip'].value;
		var address1 = document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value;
		var address2 = document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+ address1+','+address2+ ',' +city+','+zip+','+country);
}
function IsTimeValid(clickType) {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
var time2Str = document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
var time3Str = document.forms['workTicketForm'].elements['workTicket.beginningTime'].value;
var matchTime3Array = time3Str.match(timePat);
if (matchTime3Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
hourTime3 = matchTime3Array[1];
minuteTime3 = matchTime3Array[2];
secondTime3 = matchTime3Array[4];
ampmTime3 = matchTime3Array[6];

if (hourTime3 < 0  || hourTime3 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
if (minuteTime3<0 || minuteTime3 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
if (secondTime3 != null && (secondTime3 < 0 || secondTime3 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
var time4Str = document.forms['workTicketForm'].elements['workTicket.endTime'].value;
var matchTime4Array = time4Str.match(timePat);
if (matchTime4Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
hourTime4 = matchTime4Array[1];
minuteTime4 = matchTime4Array[2];
secondTime4 = matchTime4Array[4];
ampmTime4 = matchTime4Array[6];

if (hourTime4 < 0  || hourTime4 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
if (minuteTime4<0 || minuteTime4 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
if (secondTime4 != null && (secondTime4 < 0 || secondTime4 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();

return false;
}
var time5Str = document.forms['workTicketForm'].elements['workTicket.timeFrom'].value;
var matchTime5Array = time5Str.match(timePat);
if (matchTime5Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
hourTime5 = matchTime5Array[1];
minuteTime5 = matchTime5Array[2];
secondTime5 = matchTime5Array[4];
ampmTime5 = matchTime5Array[6];

if (hourTime5 < 0  || hourTime5 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
if (minuteTime5<0 || minuteTime5 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
if (secondTime5 != null && (secondTime5 < 0 || secondTime5 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
var time6Str = document.forms['workTicketForm'].elements['workTicket.timeTo'].value;
var matchTime6Array = time6Str.match(timePat);
if (matchTime6Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
hourTime6 = matchTime6Array[1];
minuteTime6 = matchTime6Array[2];
secondTime6 = matchTime6Array[4];
ampmTime6 = matchTime6Array[6];

if (hourTime6 < 0  || hourTime6 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
if (minuteTime6<0 || minuteTime6 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
if (secondTime6 != null && (secondTime6 < 0 || secondTime6 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
}
function IsValidTime(clickType) {
	document.forms['workTicketForm'].elements['workTicket.storageOut'].disabled=false;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
return false;
}
var time2Str = document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
return false;
}
var time3Str = document.forms['workTicketForm'].elements['workTicket.beginningTime'].value;
var matchTime3Array = time3Str.match(timePat);
if (matchTime3Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
hourTime3 = matchTime3Array[1];
minuteTime3 = matchTime3Array[2];
secondTime3 = matchTime3Array[4];
ampmTime3 = matchTime3Array[6];

if (hourTime3 < 0  || hourTime3 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
if (minuteTime3<0 || minuteTime3 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
if (secondTime3 != null && (secondTime3 < 0 || secondTime3 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.beginningTime'].focus();
return false;
}
var time4Str = document.forms['workTicketForm'].elements['workTicket.endTime'].value;
var matchTime4Array = time4Str.match(timePat);
if (matchTime4Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
hourTime4 = matchTime4Array[1];
minuteTime4 = matchTime4Array[2];
secondTime4 = matchTime4Array[4];
ampmTime4 = matchTime4Array[6];

if (hourTime4 < 0  || hourTime4 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
if (minuteTime4<0 || minuteTime4 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
if (secondTime4 != null && (secondTime4 < 0 || secondTime4 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.endTime'].focus();
return false;
}
var time5Str = document.forms['workTicketForm'].elements['workTicket.timeFrom'].value;
var matchTime5Array = time5Str.match(timePat);
if (matchTime5Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
hourTime5 = matchTime5Array[1];
minuteTime5 = matchTime5Array[2];
secondTime5 = matchTime5Array[4];
ampmTime5 = matchTime5Array[6];
if (hourTime5 < 0  || hourTime5 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
if (minuteTime5<0 || minuteTime5 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
if (secondTime5 != null && (secondTime5 < 0 || secondTime5 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeFrom'].focus();
return false;
}
var time6Str = document.forms['workTicketForm'].elements['workTicket.timeTo'].value;
var matchTime6Array = time6Str.match(timePat);
if (matchTime6Array == null) {
alert("Time is not in a valid format. Please Use HH:MM Format");
document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
hourTime6 = matchTime6Array[1];
minuteTime6 = matchTime6Array[2];
secondTime6 = matchTime6Array[4];
ampmTime6 = matchTime6Array[6];

if (hourTime6 < 0  || hourTime6 > 23) {
alert("Hour must be between 0 and 23");
document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
if (minuteTime6<0 || minuteTime6 > 59) {
alert ("Minute must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
if (secondTime6 != null && (secondTime6 < 0 || secondTime6 > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['workTicketForm'].elements['workTicket.timeTo'].focus();
return false;
}
if(!(clickType == 'save')){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
progressBarAutoSave('1');
var id1 = document.forms['workTicketForm'].elements['serviceOrder.id'].value;
	var wid = document.forms['workTicketForm'].elements['workTicket.id'].value;
	var fmstatus= document.forms['workTicketForm'].elements['formStatus'].value;
if(fmstatus == '1'){
	if ('${autoSavePrompt}' == 'No'){	
	 var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 =document.forms['workTicketForm'].elements['serviceOrder.id'].value;
      		var wid = document.forms['workTicketForm'].elements['workTicket.id'].value;
	 if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.serviceorder')
           {
             noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.newAccounting')
          {
             noSaveAction = 'pricingList.html?sid='+id1;
          }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	  <c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.OI')
          {
            noSaveAction = 'operationResource.html?id='+id1;
          }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.status')
           {
        		<c:if test="${serviceOrder.job=='RLO'}">
        		noSaveAction = 'editDspDetails.html?id='+id1; 
        	   </c:if>
        	   <c:if test="${serviceOrder.job!='RLO'}">
        		noSaveAction =  'editTrackingStatus.html?id='+id1;
        	    </c:if>
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.claims')
           {
             noSaveAction = 'claims.html?id='+id1;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	  var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
          if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.criticaldate')  {
        	  noSaveAction ='soAdditionalDateDetails.html?sid='+id1;
           }
           if(wid != ''){        	 
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.workticket'){
				noSaveAction = 'customerWorkTickets.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.material'){
				noSaveAction = 'itemsJbkEquips.html?id='+wid+'&itemType=M&sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.equipment'){
				noSaveAction = 'itemsJbkResourceList.html?id='+wid+'&sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.crew'){
				noSaveAction = 'workTicketCrews.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.storage'){
				noSaveAction = 'bookStorages.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.storagelibrary'){
				noSaveAction = 'bookStorageLibraries.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.mss'){
				noSaveAction = 'editMss.html?wid='+wid;
				}
           }
          processAutoSave(document.forms['workTicketForm'], 'saveWorkTicket!saveOnTabChange.html', noSaveAction);
	
	}
	else{	
	var jobNumber = document.forms['workTicketForm'].elements['serviceOrder.shipNumber'].value;
	var wcontract = document.forms['workTicketForm'].elements['workTicket.contract'].value;
	if (document.forms['workTicketForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='workTicketDetail.heading'/>");
		if(agree){
			document.forms['workTicketForm'].action = 'saveWorkTicket!saveOnTabChange.html';
			document.forms['workTicketForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'editServiceOrderUpdate.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				location.href = 'pricingList.html?sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				 <c:if test="${forwardingTabVal!='Y'}">
				 	location.href = 'containers.html?id='+id1;
		  		</c:if>
			  	<c:if test="${forwardingTabVal=='Y'}">
			  		location.href = 'containersAjaxList.html?id='+id1;
			  </c:if>
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.OI')
	          {
				location.href = 'operationResource.html?id='+id1;
	          }
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.status'){
				<c:if test="${serviceOrder.job=='RLO'}">
				location.href = 'editDspDetails.html?id='+id1; 
			   </c:if>
			   <c:if test="${serviceOrder.job!='RLO'}">
			   location.href =  'editTrackingStatus.html?id='+id1;
			    </c:if>
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				location.href = 'editCustomerFile.html?id='+idc;
				}
			  if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.criticaldate')  {
				  location.href ='soAdditionalDateDetails.html?sid='+id1;
	           }
			if(wid != ''){
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.workticket'){
				location.href = 'customerWorkTickets.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.material'){
				location.href = 'itemsJbkEquips.html?id='+wid+'&itemType=M&sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.equipment'){
				location.href = 'itemsJbkResourceList.html?id='+wid+'&sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.crew'){
				location.href = 'workTicketCrews.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.storage'){
				location.href = 'bookStorages.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.storagelibrary'){
				location.href = 'bookStorageLibraries.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.mss'){
				location.href = 'editMss.html?wid='+wid;
				}}}	}}}
	}else{
	if(id1 != ''){
		if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'editServiceOrderUpdate.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				location.href = 'pricingList.html?sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				 <c:if test="${forwardingTabVal!='Y'}">
				 	location.href = 'containers.html?id='+id1;
		  		</c:if>
			  	<c:if test="${forwardingTabVal=='Y'}">
			  		location.href = 'containersAjaxList.html?id='+id1;
			  </c:if>
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.OI')
	          {
				location.href = 'operationResource.html?id='+id1;
	          }
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.status'){
			    <c:if test="${serviceOrder.job=='RLO'}">
        		location.href = 'editDspDetails.html?id='+id1; 
        	   </c:if>
        	   <c:if test="${serviceOrder.job!='RLO'}">
        		location.href =  'editTrackingStatus.html?id='+id1;
        	    </c:if>
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
				location.href = 'editCustomerFile.html?id='+cidVal;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value =='gototab.criticaldate')  {
				  location.href ='soAdditionalDateDetails.html?sid='+id1;
	           }
			if(wid != ''){
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.workticket'){
				location.href = 'customerWorkTickets.html?id='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.material'){
				location.href = 'itemsJbkEquips.html?id='+wid+'&itemType=M&sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.equipment'){
				location.href = 'itemsJbkResourceList.html?id='+wid+'&sid='+id1;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.crew'){
				location.href = 'workTicketCrews.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.storage'){
				location.href = 'bookStorages.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.storagelibrary'){
				location.href = 'bookStorageLibraries.html?id='+wid;
				}
			if(document.forms['workTicketForm'].elements['gotoPageString'].value == 'gototab.mss'){
				location.href = 'editMss.html?wid='+wid;
				}
			}		
	}
	}
}
}
function checkTime(){
	var tim1=document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value;
	var tim2=document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value;
	var tim3=document.forms['workTicketForm'].elements['workTicket.beginningTime'].value;
	var tim4=document.forms['workTicketForm'].elements['workTicket.endTime'].value;
	var tim5=document.forms['workTicketForm'].elements['workTicket.timeFrom'].value;
	var tim6=document.forms['workTicketForm'].elements['workTicket.timeTo'].value;	
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	time3=tim3.replace(":","");
	time4=tim4.replace(":","");
	time5=tim5.replace(":","");
	time6=tim6.replace(":","");		
	if(time5 > time1){
			alert("scheduledArrive(@Site) time can not be less than shedule1 time ");
		document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].focus();
		return false;
	}
	if(time6 > time2){
		alert("leaveWareHouse(@Yard)  can not be less than From shedule2 time");
		document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].focus();
		return false;
	}else{
		document.forms['workTicketForm'].submit();
	}
	
}
function assignDateApproved(){
		if(document.forms['workTicketForm'].elements['workTicket.approvedBy'].value == ''){
			document.forms['workTicketForm'].elements['workTicket.approvedDate'].value='';
		}else{
			var d = new Date();
			var tempdate;
			tempdate=d.getMonth();
			tempdate=Number(tempdate)+1;
			tempdate=tempdate+"/"+d.getDate();
			tempdate=tempdate+"/"+d.getFullYear();
			document.forms['workTicketForm'].elements['workTicket.approvedDate'].value=tempdate;
		}
}
	
	function assigntimeairthmaticNew()
	{
		var time1=0;
		var time2=0;
		var diffTime=0;
		var tim1=document.forms['workTicketForm'].elements['workTicket.timeFrom'].value;
		var tim2=document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value;
		var mySplitResult1 = tim1.split(":");
   		var hour1 = mySplitResult1[0];
   		var minute1 = mySplitResult1[1];
   		time1=(hour1*60)+(minute1*1);
        var mySplitResult2 = tim2.split(":");
   		var hour2 = mySplitResult2[0];
   		var minute2 = mySplitResult2[1];
   		time2=(hour2*60)+(minute2*1)
		if(time1 < time2){
			diffTime = time2 - time1;
		}else if(time1 > time2 && time2 !=0){
		alert("Departure Time should be less than Arrival Time");
		document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
		document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value='00:00';
		document.forms['workTicketForm'].elements['workTicket.timeFrom'].value='00:00';
		}
		
		var HH1 = diffTime / 60;
		var MM1 = diffTime % 60;
		var hrs1 = ((HH1.toString().split('.'))[0]);
		if(hrs1.length == '1'){
		 hrs1 = '0'+hrs1;
		}
		var min1 = (MM1.toString());
		if(min1.length == '1'){
		 min1 = '0'+min1;
		}
		document.forms['workTicketForm'].elements['workTicket.beginningTime'].value = hrs1 + ':' + min1;
		if(time1 >= time2){
	    document.forms['workTicketForm'].elements['workTicket.beginningTime'].value='00:00';
		}
	}	
	function assigntimeairthmaticNew12()
	{
		var time1=0;
		var time2=0;
		var diffTime=0;
		var tim1=document.forms['workTicketForm'].elements['workTicket.timeTo'].value;
		var tim2=document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value;
		var mySplitResult1 = tim1.split(":");
   		var hour1 = mySplitResult1[0];
   		var minute1 = mySplitResult1[1];
   		time1=(hour1*60)+(minute1*1);
        var mySplitResult2 = tim2.split(":");
   		var hour2 = mySplitResult2[0];
   		var minute2 = mySplitResult2[1];
   		time2=(hour2*60)+(minute2*1)
		if(time1 < time2){
			diffTime = time2 - time1;
		}else if(time1 > time2 && time2 !=0){
		alert("Departure Time should be less than Arrival Time");
		document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
		document.forms['workTicketForm'].elements['workTicket.timeTo'].value='00:00';
		document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value='00:00';
		}
		var HH1 = diffTime / 60;
		var MM1 = diffTime % 60;
		var hrs1 = ((HH1.toString().split('.'))[0]);
		if(hrs1.length == '1'){
		 hrs1 = '0'+hrs1;
		}
		var min1 = (MM1.toString());
		if(min1.length == '1'){
		 min1 = '0'+min1;
		}
		document.forms['workTicketForm'].elements['workTicket.endTime'].value = hrs1 + ':' + min1;
		if(time1 >= time2){
		document.forms['workTicketForm'].elements['workTicket.endTime'].value='00:00';
		}
	}
function changeStatus()
{
   document.forms['workTicketForm'].elements['formStatus'].value = '1';
}
	function completeTimeString() {
		var stime1=document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value;
		var stime2=document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value;
		var stime3=document.forms['workTicketForm'].elements['workTicket.timeFrom'].value;
		var stime4=document.forms['workTicketForm'].elements['workTicket.timeTo'].value;
		
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['workTicketForm'].elements['workTicket.scheduledArrive'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['workTicketForm'].elements['workTicket.leaveWareHouse'].value = "0" + stime2;
			}
		}
		if(stime3.substring(stime3.indexOf(":")+1,stime3.length) == "" || stime3.length==1 || stime3.length==2){
			if(stime3.length==1 || stime3.length==2){
				if(stime3.length==2){
					document.forms['workTicketForm'].elements['workTicket.timeFrom'].value = stime3 + ":00";
				}
				if(stime3.length==1){
					document.forms['workTicketForm'].elements['workTicket.timeFrom'].value = "0" + stime3 + ":00";
				}
			}else{
				document.forms['workTicketForm'].elements['workTicket.timeFrom'].value = stime3 + "00";
			}
		}else{
			if(stime3.indexOf(":") == -1 && stime3.length==3){
				document.forms['workTicketForm'].elements['workTicket.timeFrom'].value = "0" + stime3.substring(0,1) + ":" + stime3.substring(1,stime3.length);
			}
			if(stime3.indexOf(":") == -1 && (stime3.length==4 || stime3.length==5) ){
				document.forms['workTicketForm'].elements['workTicket.timeFrom'].value = stime3.substring(0,2) + ":" + stime3.substring(2,4);
			}
			if(stime3.indexOf(":") == 1){
				document.forms['workTicketForm'].elements['workTicket.timeFrom'].value = "0" + stime3;
			}
		}
		if(stime4.substring(stime4.indexOf(":")+1,stime4.length) == "" || stime4.length==1 || stime4.length==2){
			if(stime4.length==1 || stime4.length==2){
				if(stime4.length==2){
					document.forms['workTicketForm'].elements['workTicket.timeTo'].value = stime4 + ":00";
				}
				if(stime4.length==1){
					document.forms['workTicketForm'].elements['workTicket.timeTo'].value = "0" + stime4 + ":00";
				}
			}else{
				document.forms['workTicketForm'].elements['workTicket.timeTo'].value = stime4 + "00";
			}
		}else{
			if(stime4.indexOf(":") == -1 && stime4.length==3){
				document.forms['workTicketForm'].elements['workTicket.timeTo'].value = "0" + stime4.substring(0,1) + ":" + stime4.substring(1,stime4.length);
			}
			if(stime4.indexOf(":") == -1 && (stime4.length==4 || stime4.length==5) ){
				document.forms['workTicketForm'].elements['workTicket.timeTo'].value = stime4.substring(0,2) + ":" + stime4.substring(2,4);
			}
			if(stime4.indexOf(":") == 1){
				document.forms['workTicketForm'].elements['workTicket.timeTo'].value = "0" + stime4;
			}
		}
	}
function notExists(){
	alert("The Ticket information is not saved yet");
}
function validService(){	   	
	   	var serviceType=document.forms['workTicketForm'].elements['workTicket.service'].value;
      	var wareHouse=document.forms['workTicketForm'].elements['workTicket.warehouse'].value;
      	var el = document.getElementById('hid');
		var e2 = document.getElementById('hid1');
		var e3 = document.getElementById('hid3');
		var e4 = document.getElementById('hid4');
		if(serviceType == 'DLC' || serviceType == 'PUC'){
			el.style.visibility = 'visible';
			e2.style.visibility = 'visible';
			e3.style.visibility = 'collapse';
			e4.style.visibility = 'visible';		
		}else if(serviceType == 'MIS' && wareHouse=='T'){
			e2.style.visibility = 'visible';
			e3.style.visibility = 'collapse';
			el.style.visibility = 'collapse';
			e4.style.visibility = 'visible';		
		}else if(serviceType == 'RC' || serviceType == 'RL' || serviceType == 'HO'){
			e3.style.visibility = 'visible';
			e2.style.visibility = 'visible';
			el.style.visibility = 'collapse';
			e4.style.visibility = 'collapse';
		}else if(serviceType == 'DU' || serviceType == 'LD' || serviceType == 'UC'){
			el.style.visibility = 'visible';
			e2.style.visibility = 'collapse';
			e3.style.visibility = 'collapse';
			e4.style.visibility = 'collapse';
		}else if(serviceType == 'TPS'){
			el.style.visibility = 'collapse';
			e2.style.visibility = 'visible';
			e3.style.visibility = 'collapse';
			e4.style.visibility = 'visible';
		}else if(serviceType == 'MSS' && wareHouse=='T'){
			e2.style.visibility = 'visible';
			e3.style.visibility = 'collapse';
			el.style.visibility = 'collapse';
			e4.style.visibility = 'visible';		
		}else if(serviceType == 'DL' || serviceType=='PK'){
				e2.style.visibility = 'visible';
				e3.style.visibility = 'collapse';
				el.style.visibility = 'collapse';
				e4.style.visibility = 'collapse';
		}else{
			el.style.visibility = 'collapse';
			e2.style.visibility = 'collapse';
			e3.style.visibility = 'collapse';
			e4.style.visibility = 'collapse';		
		}
		var serviceMandatory="";
		 <configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">
		 var originElevatorMandatory = document.getElementById('originElevatorMandatory');
		 var originKindofparkingMandatory = document.getElementById('originKindofparkingMandatory');
		 var originSiteMandatory = document.getElementById('originSiteMandatory');
		 var originShuttleMandatory = document.getElementById('originShuttleMandatory');
		 var originLongCarryMandatory = document.getElementById('originLongCarryMandatory');		 
		 var destinationElevatorMandatory = document.getElementById('destinationElevatorMandatory');
		 var destinationKindofparkingMandatory = document.getElementById('destinationKindofparkingMandatory');
		 var destinationSiteMandatory = document.getElementById('destinationSiteMandatory');
		 var destinationShuttleMandatory = document.getElementById('destinationShuttleMandatory');
		 var destinationLongCarryMandatory = document.getElementById('destinationLongCarryMandatory');		 
		if(serviceType == 'LD' || serviceType=='PK' || serviceType=='PL' ){
			document.getElementById('originMandatoryForLongCarry').value=true;
			document.getElementById('destinationMandatoryForLongCarry').value=false;
			originElevatorMandatory.style.display = "block";
			originKindofparkingMandatory.style.display = "block";
			originSiteMandatory.style.display = "block";
			originShuttleMandatory.style.display = "block";
			originLongCarryMandatory.style.display = "block";			
			destinationElevatorMandatory.style.display = "none";
			destinationKindofparkingMandatory.style.display = "none";
			destinationSiteMandatory.style.display = "none";
			destinationShuttleMandatory.style.display = "none";
			destinationLongCarryMandatory.style.display = "none";			
			serviceMandatory="NA";			 			
		}else if(serviceType == 'DL' || serviceType=='DU' || serviceType=='UP'){
			document.getElementById('destinationMandatoryForLongCarry').value=true; 
			document.getElementById('originMandatoryForLongCarry').value=false; 
			destinationElevatorMandatory.style.display = "block";
			destinationKindofparkingMandatory.style.display = "block";
			destinationSiteMandatory.style.display = "block";
			destinationShuttleMandatory.style.display = "block";
			destinationLongCarryMandatory.style.display = "block";
			originElevatorMandatory.style.display = "none";
			originKindofparkingMandatory.style.display = "none";
			originSiteMandatory.style.display = "none";
			originShuttleMandatory.style.display = "none";
			originLongCarryMandatory.style.display = "none";				
			serviceMandatory="NA";		
		}else{			
			document.getElementById('originMandatoryForLongCarry').value=false; 
			document.getElementById('destinationMandatoryForLongCarry').value=false; 
			originElevatorMandatory.style.display = "none";
			originKindofparkingMandatory.style.display = "none";
			originSiteMandatory.style.display = "none";	
			originShuttleMandatory.style.display = "none";
			originLongCarryMandatory.style.display = "none";
			destinationElevatorMandatory.style.display = "none";
			destinationKindofparkingMandatory.style.display = "none";
			destinationSiteMandatory.style.display = "none";
			destinationShuttleMandatory.style.display = "none";
			destinationLongCarryMandatory.style.display = "none";
			serviceMandatory="NA";
		}
	  </configByCorp:fieldVisibility>
	  if(serviceMandatory==""){
		    originElevatorMandatory.style.display = "none";
			originKindofparkingMandatory.style.display = "none";
			originSiteMandatory.style.display = "none";	
			originShuttleMandatory.style.display = "none";
			originLongCarryMandatory.style.display = "none";
			destinationElevatorMandatory.style.display = "none";
			destinationKindofparkingMandatory.style.display = "none";
			destinationSiteMandatory.style.display = "none";
			destinationShuttleMandatory.style.display = "none";
			destinationLongCarryMandatory.style.display = "none";
			document.getElementById('originMandatoryForLongCarry').value=false; 
			document.getElementById('destinationMandatoryForLongCarry').value=false;
	  }	
}
function resetMandatory(){
	var serviceType='${workTicket.service}';
	 <configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">	
	 var originElevatorMandatory = document.getElementById('originElevatorMandatory');
	 var originKindofparkingMandatory = document.getElementById('originKindofparkingMandatory');
	 var originSiteMandatory = document.getElementById('originSiteMandatory');
	 var originShuttleMandatory = document.getElementById('originShuttleMandatory');
	 var originLongCarryMandatory = document.getElementById('originLongCarryMandatory');		 
	 var destinationElevatorMandatory = document.getElementById('destinationElevatorMandatory');
	 var destinationKindofparkingMandatory = document.getElementById('destinationKindofparkingMandatory');
	 var destinationSiteMandatory = document.getElementById('destinationSiteMandatory');
	 var destinationShuttleMandatory = document.getElementById('destinationShuttleMandatory');
	 var destinationLongCarryMandatory = document.getElementById('destinationLongCarryMandatory');		 
	if(serviceType == 'LD' || serviceType=='PK' || serviceType=='PL' ){
		document.getElementById('originMandatoryForLongCarry').value=true;
		document.getElementById('destinationMandatoryForLongCarry').value=false;
		originElevatorMandatory.style.display = "block";
		originKindofparkingMandatory.style.display = "block";
		originSiteMandatory.style.display = "block";
		originShuttleMandatory.style.display = "block";
		originLongCarryMandatory.style.display = "block";			
		destinationElevatorMandatory.style.display = "none";
		destinationKindofparkingMandatory.style.display = "none";
		destinationSiteMandatory.style.display = "none";
		destinationShuttleMandatory.style.display = "none";
		destinationLongCarryMandatory.style.display = "none";			
		serviceMandatory="NA";			 			
	}else if(serviceType == 'DL' || serviceType=='DU' || serviceType=='UP'){
		document.getElementById('destinationMandatoryForLongCarry').value=true; 
		document.getElementById('originMandatoryForLongCarry').value=false; 
		destinationElevatorMandatory.style.display = "block";
		destinationKindofparkingMandatory.style.display = "block";
		destinationSiteMandatory.style.display = "block";
		destinationShuttleMandatory.style.display = "block";
		destinationLongCarryMandatory.style.display = "block";
		originElevatorMandatory.style.display = "none";
		originKindofparkingMandatory.style.display = "none";
		originSiteMandatory.style.display = "none";
		originShuttleMandatory.style.display = "none";
		originLongCarryMandatory.style.display = "none";				
		serviceMandatory="NA";		
	}else{			
		document.getElementById('originMandatoryForLongCarry').value=false; 
		document.getElementById('destinationMandatoryForLongCarry').value=false; 
		originElevatorMandatory.style.display = "none";
		originKindofparkingMandatory.style.display = "none";
		originSiteMandatory.style.display = "none";	
		originShuttleMandatory.style.display = "none";
		originLongCarryMandatory.style.display = "none";
		destinationElevatorMandatory.style.display = "none";
		destinationKindofparkingMandatory.style.display = "none";
		destinationSiteMandatory.style.display = "none";
		destinationShuttleMandatory.style.display = "none";
		destinationLongCarryMandatory.style.display = "none";
		serviceMandatory="NA";
	}
  </configByCorp:fieldVisibility>
}
function copyCompanyToDestination(targetElement){
		document.forms['workTicketForm'].elements['workTicket.destinationCompany'].value = targetElement.value;
	}
function getState(from) {
	var abc147=document.forms['workTicketForm'].elements['workTicket.instructionCode'].value;
	document.forms['workTicketForm'].elements['instructionCodeHiddenValue'].value=abc147;
var country = "";
var getStateList = "";
var countryCode = "";
if(from == 'OA'){
	country = document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
	countryCode =getKeyByValue(country);
	getStateList = handleHttpResponse5;
}
if(from == 'DA'){
	country = document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
	countryCode =getKeyByValue(country);
	getStateList = handleHttpResponse6;
}	
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = getStateList;
     http2.send(null);
	}
	
function enableStateListOrigin(){ 
	var oriCon=document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['workTicketForm'].elements['workTicket.state'].disabled = false;
	  }else{
		  document.forms['workTicketForm'].elements['workTicket.state'].disabled = true;
	  }	        
}
function enableStateListDestin(){ 
	var desCon=document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled = false;
	  }else{
		  document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled = true;
	  }	        
}
function newFunctionForCountryState(){
	 var destinationAbc ='${workTicket.destinationCountry}';
	 var originAbc ='${workTicket.originCountry}';
	 var enbState = '${enbState}';
	 var index = (enbState.indexOf(originAbc)> -1);
	 var index1 = (enbState.indexOf(destinationAbc)> -1);
	 if(index != ''){
		 document.forms['workTicketForm'].elements['workTicket.state'].disabled =false;
		 getStateOnReset(originAbc);
	 }
	 else{
		 document.forms['workTicketForm'].elements['workTicket.state'].disabled =true;
	 }
	 if(index1 != ''){
		 document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled =false;
		 getDestinationStateOnReset(destinationAbc);
	 }
	 else{
		 document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled =true;
	 }
	 }
function getStateOnReset(target) {
	var country = target;
 	var countryCode = getKeyByValue(country);
 	document.forms['workTicketForm'].elements['workTicket.originCountryCode'].value=countryCode;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	httpState1.open("GET", url, true);
	httpState1.onreadystatechange = handleHttpResponse555555;
	httpState1.send(null);	
	}	
function getDestinationStateOnReset(target) {
	var country = target;
 	var countryCode = getKeyByValue(country);
 	document.forms['workTicketForm'].elements['workTicket.destinationCountryCode'].value=countryCode;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	httpDState1.open("GET", url, true);
	httpDState1.onreadystatechange = handleHttpResponse6666666666;
	httpDState1.send(null);	
	}
function handleHttpResponse6666666666(){
	if (httpDState1.readyState == 4){
            var results = httpDState1.responseText                
            results = results.trim();
            res = results.split("@");
            targetElement = document.forms['workTicketForm'].elements['workTicket.destinationState'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = '';
				document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = stateVal[1];
				document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = stateVal[0];
				
				if (document.getElementById("destinationState").options[i].value == '${workTicket.destinationState}'){
				   document.getElementById("destinationState").options[i].defaultSelected = true;
				} } }
				document.getElementById("destinationState").value = '${workTicket.destinationState}';
    } }          
 function handleHttpResponse555555(){
     if (httpState1.readyState == 4){
        var results = httpState1.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['workTicketForm'].elements['workTicket.state'];
		targetElement.length = res.length;
			for(i=0;i<res.length;i++) {
			if(res[i] == ''){
			document.forms['workTicketForm'].elements['workTicket.state'].options[i].text = '';
			document.forms['workTicketForm'].elements['workTicket.state'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['workTicketForm'].elements['workTicket.state'].options[i].text = stateVal[1];
			document.forms['workTicketForm'].elements['workTicket.state'].options[i].value = stateVal[0];
			
			if (document.getElementById("ostate").options[i].value == '${workTicket.state}'){
			   document.getElementById("ostate").options[i].defaultSelected = true;
			} } }
			document.getElementById("ostate").value = '${workTicket.state}';
     }
}  
  var httpState1 = getHTTPObjectState1();
  var httpDState1 = getHTTPObjectDState1();
function getHTTPObjectState1()	 {
     var xmlhttp;
     if(window.XMLHttpRequest)
     {
         xmlhttp = new XMLHttpRequest();
     }
     else if (window.ActiveXObject)
     {
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         if (!xmlhttp)
         {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
         }
     }
     return xmlhttp;
 } 
function getHTTPObjectDState1()	 {
     var xmlhttp;
     if(window.XMLHttpRequest)
     {
         xmlhttp = new XMLHttpRequest();
     }
     else if (window.ActiveXObject)
     {
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         if (!xmlhttp)
         {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
         }
     }
     return xmlhttp;
 } 
function getState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);	
	}	
function getDestinationState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);	
	}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
} 
 function handleHttpResponse5()
        {
             if (http2.readyState == 4)
             {
            	var tempState = document.forms['workTicketForm'].elements['addAddressState'].value;
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['workTicketForm'].elements['workTicket.state'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].text = '';
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].text = stateVal[1];
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].value = stateVal[0];					
					if ((tempState=='') && (document.getElementById("ostate").options[i].value == '${workTicket.state}')){					
						document.getElementById("ostate").options.selectedIndex = i;
						}
						if(tempState!='' && document.getElementById("ostate").options[i].value == tempState ){
							document.getElementById("ostate").options[i].selected = true;
						}
						}
						}	
 				getInstructionCodeValues();
						}
             
	             } 
function handleHttpResponse6()
        {

             if (http2.readyState == 4)
             {
            	var tempState = document.forms['workTicketForm'].elements['addAddressState'].value;
            	var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['workTicketForm'].elements['workTicket.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = '';
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = stateVal[1];
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = stateVal[0];

					if (tempState=='' && document.getElementById("destinationState").options[i].value == '${workTicket.destinationState}'){
						   document.getElementById("destinationState").options[i].defaultSelected = true;
						}
						if (document.getElementById("destinationState").options[i].value == tempState && tempState!='' ){
							   document.getElementById("destinationState").options[i].selected = true;
							}
					}
					}
 			 getInstructionCodeValues();
             	}
            
	        }  
function handleHttpResponse16()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['workTicketForm'].elements['workTicket.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = '';
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = stateVal[1];
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("ostate").value = '${workTicket.state}';
					document.getElementById("destinationState").value = '${workTicket.destinationState}';
            		var dCountry1=document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
					 if(dCountry1 == 'India' || dCountry1 == 'Canada' || dCountry1 == 'United States'){
						document.getElementById('destinationState').disabled = false;
		
					}else{
					document.getElementById('destinationState').disabled = true;
					document.getElementById('destinationState').value ="";
					}
             }
        }          
var http33 = getHTTPObject33();
var http99 = getHTTPObject99();
function getHTTPObject99()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();

function getHTTPObject1(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1(); 
    
   function chkBilling(){
	var billingContract = document.forms['workTicketForm'].elements['workTicket.contract'].value;
	if(billingContract==''){
	  alert("There is no pricing contract in billing: Please select.");
	  return false;
	}else{
	return true;
	}
} 
function changeStatus(){
	document.forms['workTicketForm'].elements['formStatus'].value = '1';
}
function autoPopulate(targetElement){
	document.forms['workTicketForm'].elements['workTicket.stgHow'].disabled = true;
	}	
 function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['workTicketForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['workTicketForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['workTicketForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['workTicketForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }   
 function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'customerWorkTickets.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['workTicketForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['workTicketForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrl(id)
	{
		location.href = "customerWorkTickets.html?id="+id;
	}	
function goPrevChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['workTicketForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['workTicketForm'].elements['workTicket.ticket'].value;
	var url="editPrevTicket.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }   
 function goNextChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['workTicketForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['workTicketForm'].elements['workTicket.ticket'].value;
	var url="editNextTicket.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }   
 function handleHttpResponseOtherShipChild(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editWorkTicketUpdate.html?id='+results;
             }
       }     
function findCustomerOtherSOChild(position) {
 var sidNum=document.forms['workTicketForm'].elements['serviceOrder.id'].value;
 var soIdNum=document.forms['workTicketForm'].elements['workTicket.ticket'].value;
 var url="ticketOtherSO.html?ajax=1&decorator=simple&popup=true&sidNum=" + encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrlChild(id){
		location.href = "editWorkTicketUpdate.html?id="+id;
	}	
function changeTicektStatus(target){
	var stgHow = document.forms['workTicketForm'].elements['workTicket.stgHow'].value;
	var stgAmoutn = document.forms['workTicketForm'].elements['workTicket.stgAmount'].value;
	var service = document.forms['workTicketForm'].elements['workTicket.service'].value;
	var storageRelease = document.forms['workTicketForm'].elements['releasedStorageDate'].value;
	var corpId = document.forms['workTicketForm'].elements['workTicket.corpID'].value;
	if('${workTicket.targetActual}'=='P' && target.value != 'P' && '${checkOpsRole}' == 'false'){
		alert('You do not have the permission to change the status of this ticket.');
		document.forms['workTicketForm'].elements['workTicket.targetActual'].value = '${workTicket.targetActual}';
		return false;
	}
	if('${workTicket.targetActual}'=='R' && target.value != 'R' && '${checkOpsRole}' == 'false'){
		alert('You do not have the permission to change the status of this ticket.');
		document.forms['workTicketForm'].elements['workTicket.targetActual'].value = '${workTicket.targetActual}';
		return false;
	}	
	if(target.value == 'A'){
			if(('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STG') &&  stgAmoutn != '0' && stgHow == ''){
			alert('Effect on Hand Storage value is missing.');
			document.forms['workTicketForm'].elements['workTicket.targetActual'].value = '';
			return false;
		}
	if((service=='DL' ||service=='DU' || service=='RL') && corpId!='SSCW')
	{
	 if(('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STF')){
	 var agree = confirm("Is the entire storage being delivered for this client? press OK to YES OR CANCEL to NO.");
			if(agree){
			if(storageRelease>0){
			var agree = confirm("There are " +storageRelease+ " active storage locations, do you want to release all these locations? press OK to Release All Locations or CANCEL.");
			if(agree){
			document.forms['workTicketForm'].elements['workTicket.storageOut'].value='F';
			}
			}else{
			alert("There is no location available for release.");
			document.forms['workTicketForm'].elements['workTicket.storageOut'].value='F';
			}	
			}else{
				document.forms['workTicketForm'].elements['workTicket.storageOut'].value='P';
				}
	           }
    }
}
}
function changeTicektActulizedStatus(){
	var service = document.forms['workTicketForm'].elements['workTicket.service'].value;
	var targetactual = document.forms['workTicketForm'].elements['workTicket.targetActual'].value;
	var storageRelease = document.forms['workTicketForm'].elements['releasedStorageDate'].value;
	var corpId = document.forms['workTicketForm'].elements['workTicket.corpID'].value;
	if(targetactual == 'A')
	{
	if((service=='DL' ||service=='DU' || service=='RL') && corpId!='SSCW')
	{
	 if(('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STF')){
	 var agree = confirm("Is the entire storage being delivered for this client? press OK to YES OR CANCEL to NO.");
			if(agree){
			if(storageRelease>0){
			var agree = confirm("There are " +storageRelease+ " active storage locations, do you want to release all these locations? press OK to Release All Locations or CANCEL.");
			if(agree){
			document.forms['workTicketForm'].elements['workTicket.storageOut'].value='F';
			}
			}else{
			alert("There is no location available for release.");
			document.forms['workTicketForm'].elements['workTicket.storageOut'].value='F';
			}	
			}else{
				document.forms['workTicketForm'].elements['workTicket.storageOut'].value='P';
				}
	           }
    }
}
}
function winOpen()
 {
 		finalVal='Other';
 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=workTicket.vendorName&fld_code=workTicket.vendorCode');
 } 
 function getVendorName(){
	 var vendorId = document.forms['workTicketForm'].elements['workTicket.vendorCode'].value;
	 if(vendorId==''){
		 document.forms['workTicketForm'].elements['workTicket.vendorName'].value = "";
	 }
	 if(vendorId!=''){
    	showOrHideAutoSave('1');
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     	http2.open("GET", url, true);
     	http2.onreadystatechange = handleHttpResponse5555;
     	http2.send(null);
	 }
}
function handleHttpResponse5555(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
           				document.forms['workTicketForm'].elements['workTicket.vendorName'].value = res[1];
	           		}else{
	           			alert("Vendor Code is not approved" ); 
					    document.forms['workTicketForm'].elements['workTicket.vendorName'].value="";
				 		document.forms['workTicketForm'].elements['workTicket.vendorCode'].value="";
	           		}
               	}else{
                     alert("Vendor Code not valid" );
                 	 document.forms['workTicketForm'].elements['workTicket.vendorName'].value="";
				 	 document.forms['workTicketForm'].elements['workTicket.vendorCode'].value="";
			   }
			   showOrHideAutoSave('0');
			   getInstructionCodeValues();
       }
}
function checkVendorNameOnService(){
		var serviceType=document.forms['workTicketForm'].elements['workTicket.service'].value;
		var wareHouse=document.forms['workTicketForm'].elements['workTicket.warehouse'].value;
		
	if(serviceType == 'DLC' || serviceType == 'PUC' || serviceType == 'TPS' ){
		var vendorCode=document.forms['workTicketForm'].elements['workTicket.vendorCode'].value;
		if(vendorCode=='')
		{
			alert('Please select the vendor code');
			progressBarAutoSave('0');
			return false;
		}
	} if(serviceType == 'MIS' && wareHouse=='T')
		{
		var vendorCode=document.forms['workTicketForm'].elements['workTicket.vendorCode'].value;
	     if(vendorCode=='')
		{
			alert('Please select the vendor code');
			progressBarAutoSave('0');
			return false;
		}
		}
		if(serviceType == 'MSS' && wareHouse=='T'){
		var vendorCode=document.forms['workTicketForm'].elements['workTicket.vendorCode'].value;
	     if(vendorCode=='')	{
			alert('Please select the vendor code');
			progressBarAutoSave('0');
			return false;
		}
		}
		if(serviceType == 'RC' && '${workTicket.corpID}' == 'VOER'){
			var boundedGood=document.forms['workTicketForm'].elements['workTicket.bondedGoods'].value;
			if(boundedGood=='')	{
				alert('Please select the Bonded Goods');
				progressBarAutoSave('0');
				return false;
			}
		}		
		if(document.forms['workTicketForm'].elements['originMandatoryForLongCarry'].value=="true" && (document.forms['workTicketForm'].elements['workTicket.originElevator'].value=='' || document.forms['workTicketForm'].elements['workTicket.parking'].value=='' || document.forms['workTicketForm'].elements['workTicket.siteS'].value==''|| document.forms['workTicketForm'].elements['workTicketOriginShuttle'].value==''|| document.forms['workTicketForm'].elements['workTicket.originLongCarry'].value=='')){
			alert('Please select the Origin Elevator ,Kind of parking permit,Shuttle,Long Carry and Site......');
			progressBarAutoSave('0');
			return false;
			
		}	
		if(document.forms['workTicketForm'].elements['destinationMandatoryForLongCarry'].value=="true" && (document.forms['workTicketForm'].elements['workTicket.destinationElevator'].value=='' || document.forms['workTicketForm'].elements['workTicket.dest_park'].value=='' || document.forms['workTicketForm'].elements['workTicket.siteD'].value==''|| document.forms['workTicketForm'].elements['workTicketDestinationShuttle'].value=='' || document.forms['workTicketForm'].elements['workTicket.destinationLongCarry'].value=='' )){
			alert('Please select the Destination Elevator ,Kind of parking permit ,Shuttle ,Long Carry  and Site......');
			progressBarAutoSave('0');
			return false;
			
		}
		 <configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">	
		 try{
			 saveTimeManagement();
		 }catch(e){
		 }		  
		 </configByCorp:fieldVisibility>	
	 return true;	
}
function autoPopulateVendorCodeWithName(){
	var wareHouse=document.forms['workTicketForm'].elements['workTicket.warehouse'].value;
	var serviceType = document.forms['workTicketForm'].elements['workTicket.service'].value;
	if(serviceType == 'MSS' && wareHouse=='T'){		
	var url="vendorCodeWithNameAjax.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(serviceType);
	http55555.open("GET", url, true);
    http55555.onreadystatechange = handleHttpResponse55555;
    http55555.send(null);
	}else{
		document.forms['workTicketForm'].elements['workTicket.vendorName'].value='';
 		document.forms['workTicketForm'].elements['workTicket.vendorCode'].value='';
	}
}
function handleHttpResponse55555(){
    if (http55555.readyState == 4){        
       var selected = http55555.responseText
       selected = selected.trim();
       if(selected!=""){
       var sel = selected.split("~"); 
			    document.forms['workTicketForm'].elements['workTicket.vendorName'].value=sel[1];
		 		document.forms['workTicketForm'].elements['workTicket.vendorCode'].value=sel[0];
       }
    }
}
var http55555 = getHTTPObject55555();
function getHTTPObject55555()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function checkHubLimit(){
	var checkValid = checkVendorNameOnService();
	if(checkValid){
	if('${workTicket.targetActual}' != 'P'){
	var checkLimit = document.forms['workTicketForm'].elements['checkLimit'].value;
	if(checkLimit == 'Y'){
     	var agree = confirm("This ticket is exceeding the limit. This Ticket will be sent in Dispatch Queue for Approval.");
     	if(agree){
     		document.forms['workTicketForm'].submit();
     		return true;
     	}else{     		
     		return false;	
     	}
	 }else{
		 document.forms['workTicketForm'].submit();
  		 return true; 
	 }
	}else{
		document.forms['workTicketForm'].submit();
  		return true;	
  	}
   }
}
function checkExceedLimit(){
	showOrHideAutoSave(1);
	 var checkValid = checkVendorNameOnService();
	 if(checkValid){
	document.forms['workTicketForm'].elements['workTicket.storageOut'].disabled=false;
	var userchecktype = document.forms['workTicketForm'].elements['usertype'].value;
	var date1 = document.forms['workTicketForm'].elements['workTicket.date1'].value;
	var date2 = document.forms['workTicketForm'].elements['workTicket.date2'].value;
	var estimatedWeight= document.forms['workTicketForm'].elements['workTicket.estimatedWeight'].value;
	var estimatedCubicFeet= document.forms['workTicketForm'].elements['workTicket.estimatedCubicFeet'].value;
	var warehouse = document.forms['workTicketForm'].elements['workTicket.warehouse'].value;
	var id = document.forms['workTicketForm'].elements['workTicket.id'].value;
	var service = document.forms['workTicketForm'].elements['workTicket.service'].value;
	var targetActual=document.forms['workTicketForm'].elements['workTicket.targetActual'].value;
	var dynamicWtServices='${wtServiceTypeDyna}';
	if('${workTicket.targetActual}'=='R'){
		document.forms['workTicketForm'].elements['oldTargetActual'].value = 'AR';
	}
	if('${workTicket.targetActual}'=='P'){
		document.forms['workTicketForm'].elements['oldTargetActual'].value = 'AP';
	}
	if(targetActual=='R'){
		document.forms['workTicketForm'].elements['oldTargetActual'].value = 'CR';
	}
	    if(targetActual!='C' && (dynamicWtServices.indexOf(service)!= -1) && date1!='' && date2!='' && warehouse!=''){
		var url="exceedHubLimit.html?ajax=1&decorator=simple&popup=true&workticketService="+encodeURI(service)+"&id="+id+"&status="+encodeURI(targetActual)+"&date1="+encodeURI(date1)+"&date2="+encodeURI(date2)+"&warehouse="+encodeURI(warehouse)+"&estimatedWeight="+encodeURI(estimatedWeight)+"&estimatedCubicFeet="+encodeURI(estimatedCubicFeet); 
	    httpHub.open("GET", url, true);
		httpHub.onreadystatechange = handleHttpHubResponse;
		httpHub.send(null);	
	} else if (userchecktype=='DRIVER' && document.getElementById('workTicketForm_workTicket_accepted').checked ==true){ 
		var crewName=document.forms['workTicketForm'].elements['workTicket.crewName'].value;
		var projectLeadName=document.forms['workTicketForm'].elements['workTicket.projectLeadName'].value;
		var projectLeadNumber=document.forms['workTicketForm'].elements['workTicket.projectLeadNumber'].value;
		if(crewName == ''){
			  alert("Crew Name is a Mandatory fields" );
			  showOrHideAutoSave('0');
			  return false;	 
		  }
		  if(projectLeadName =='')  {
			 alert("Project Lead Name  is a Mandatory fields" );
			 showOrHideAutoSave('0');
			 return false;	 
		   }
		   if(projectLeadNumber =='') {
			  alert("project Lead Number is a Mandatory fields" );
			  showOrHideAutoSave('0');
			  return false;	 
		  }else{
			  document.forms['workTicketForm'].submit();
	  		   return true;	 
		  } 
		
	      } else{
		    document.forms['workTicketForm'].submit();
  		   return true;	
  	}
	 } 
}
function handleHttpHubResponse()  {
    if (httpHub.readyState == 4)  { 
      var results = httpHub.responseText;
      results = results.trim();
      var checkValid = checkVendorNameOnService();
      if(checkValid){
     	if('${workTicket.targetActual}' != 'P'){
     	if(results == 'Y'){
          	var agree = confirm("This ticket is exceeding the limit. This Ticket will be sent in Dispatch Queue for Approval.");
          	if(agree){
                document.forms['workTicketForm'].elements['checkLimit'].value=results;
          		document.forms['workTicketForm'].submit();
          		return true;
          	}else{
          		document.forms['workTicketForm'].elements['checkLimit'].value='';
          		document.forms['workTicketForm'].submit();
          		return false;	
          	}
     	 }else{
     		 document.forms['workTicketForm'].submit();
       		 return true; 
     	 }
     	}else{
     		document.forms['workTicketForm'].submit();
       		return true;	
       	}
      }
    }
}
var httpHub = getHTTPObjectHub();
function getHTTPObjectHub(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function openStandardAddressesPopWindow(){
var jobType=document.forms['workTicketForm'].elements['serviceOrder.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=workTicket.originMobile&fld_ninthDescription=workTicket.city&fld_eigthDescription=workTicket.originFax&fld_seventhDescription=workTicket.homePhone&fld_sixthDescription=workTicket.phone&fld_fifthDescription=workTicket.zip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=workTicket.originCountry&fld_secondDescription=workTicket.address3&fld_description=workTicket.address2&fld_code=workTicket.address1');
document.forms['workTicketForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
function openStandardAddressesDestinationPopWindow(){
var jobType=document.forms['workTicketForm'].elements['serviceOrder.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=workTicket.destinationMobile&fld_ninthDescription=workTicket.destinationCity&fld_eigthDescription=workTicket.destinationFax&fld_seventhDescription=workTicket.destinationHomePhone&fld_sixthDescription=workTicket.destinationPhone&fld_fifthDescription=workTicket.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=workTicket.destinationCountry&fld_secondDescription=workTicket.destinationAddress3&fld_description=workTicket.destinationAddress2&fld_code=workTicket.destinationAddress1');
document.forms['workTicketForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}
// JASON country code change
function getKeyByValue(value){
	var countryList = JSON.parse('${jsonCountryCodeText}');
	for (key in countryList) {
	    if (countryList.hasOwnProperty(key)) {
	    	if(value==countryList[key]){
	    		return key;
	    	}
		}
	}
}
function enableState(){
var AddSection = document.forms['workTicketForm'].elements['checkConditionForOriginDestin'].value;
if(AddSection=='originAddSection'){
	var originCountry = document.forms['workTicketForm'].elements['workTicket.originCountry'].value
	getOriginCountryCode();
	var countryCode =getKeyByValue(originCountry);
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse12;
     http9.send(null);
}
if(AddSection=='destinAddSection'){
	var country = document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value
	getDestinationCountryCode();
	var countryCode = getKeyByValue(country);
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse11;
     http9.send(null);
}
}
function handleHttpResponse11(){		
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['workTicketForm'].elements['workTicket.destinationState'];
                var stdAddDestinState=document.forms['workTicketForm'].elements['stdAddDestinState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = '';
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].text = stateVal[1];
					document.forms['workTicketForm'].elements['workTicket.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${workTicket.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = stdAddDestinState;
        }dstateMandatory();
}  
function handleHttpResponse12(){
		
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['workTicketForm'].elements['workTicket.state'];
                var stdAddOriginState=document.forms['workTicketForm'].elements['stdAddOriginState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].text = '';
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].text = stateVal[1];
					document.forms['workTicketForm'].elements['workTicket.state'].options[i].value = stateVal[0];
					
					if (document.getElementById("ostate").options[i].value == '${workTicket.state}'){
					   document.getElementById("ostate").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("ostate").value = stdAddOriginState;
       		 	}zipCode();
			}  	
	
  </script>


<script type="text/javascript"> 
      function getAddress(targetElement,temp){        
        	 var address=targetElement.value;
        	 address=address.trim();
        	 var cid='${customerFile.id}';
        	 if(address!=''){
            	if(address=='Origin Address'){
            		   var url="getAddress.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&addType="+ encodeURI(temp);
            		   http31.open("GET",url,true);
            		   http31.onreadystatechange = function(){ handleHttpResponse3(temp,address);};
            		   http31.send(null);                	
            	}else if(address=='Destination Address'){
            		   var url="getAddress.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&addType="+ encodeURI(temp);
            		   http31.open("GET",url,true);
            		   http31.onreadystatechange = function(){ handleHttpResponse3(temp,address);};
            		   http31.send(null);                	
            	}else{
            		var tempAddress = address.split('^');
                	if(tempAddress[1]=='add'){
            		   var url="getAddressDescription.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&descType="+ encodeURI(tempAddress[0]);
            		   http31.open("GET",url,true);
            		   http31.onreadystatechange = function(){ handleHttpResponse3(temp,address);};
            		   http31.send(null);
                	}
                	if(tempAddress[1]=='std'){
                       var url="getStandardAddressDescription.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&stdAddId="+ encodeURI(tempAddress[0]);
					   http31.open("GET",url,true);
               		   http31.onreadystatechange = function(){ handleHttpResponse3(temp,address);};
               		   http31.send(null);
                   } }  } }
      function handleHttpResponse3(temp,address){
  		if (http31.readyState == 4){
      	var results = http31.responseText                  
                  results = results.trim();   
                  var res = results.split("~");                                                                   
                 	if(address=='Origin Address')  	{
                 		if(res[0]!='undefined' || res[0]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.address1'].value = res[0];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.address1'].value = "";
    		    		}
						if(res[1]!='undefined' || res[1]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.address2'].value = res[1];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.address2'].value ="";
    		    		}
						if(res[2]!='undefined' || res[2]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.address3'].value = res[2];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.address3'].value = "";
    		    		}
						if(res[4]!='undefined' || res[4]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.originCountry'].value = res[4];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.originCountry'].value = "";
    		    		}
						if(res[5]!='undefined' || res[5]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.state'].value = res[5];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.state'].value = "";
    		    		}
						if(res[6]!='undefined' || res[6]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.city'].value = res[6];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.city'].value = "";
    		    		}
						if(res[7]!='undefined' || res[7]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.zip'].value = res[7];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.zip'].value = "";
    		    		}
						if(res[8]!='undefined' || res[8]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.phone'].value = res[8];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.phone'].value = "";
    		    		}
						if(res[9]!='undefined' || res[9]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.phoneExt'].value = res[9];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.phoneExt'].value = "";
    		    		}
						if(res[10]!='undefined' || res[10]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.homePhone'].value = res[10];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.homePhone'].value = "";
    		    		}
						if(res[11]!='undefined' || res[11]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.originMobile'].value = res[11];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.originMobile'].value = "";
    		    		}
						if(res[12]!='undefined' || res[12]!=undefined){
							document.forms['workTicketForm'].elements['workTicket.originFax'].value = res[12];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.originFax'].value = "";
    		    		}
						if(res[5]!='undefined' || res[5]!=undefined){
							document.forms['workTicketForm'].elements['addAddressState'].value=res[5];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['addAddressState'].value="";
    		    		}
						getOriginCountryCode();
						getState('OA');
						ostateMandatory();
						zipCode();
						//getInstructionCodeValues();
  					} 
                 	else if(address=='Destination Address') 	{ 
                 		if(res[0]!='undefined' || res[0]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value = res[0];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value = "";
    		    		}
                 		if(res[1]!='undefined' || res[1]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value = res[1];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value = "";
    		    		}
                 		if(res[2]!='undefined' || res[2]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationAddress3'].value = res[2];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationAddress3'].value = "";
    		    		}
                 		if(res[4]!='undefined' || res[4]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value = res[4];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value = "";
    		    		}
                 		if(res[5]!='undefined' || res[5]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationState'].value = res[5];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationState'].value = "";
    		    		}
                 		if(res[6]!='undefined' || res[6]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationCity'].value = res[6];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationCity'].value = "";
    		    		}
                 		if(res[7]!='undefined' || res[7]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationZip'].value = res[7];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationZip'].value = "";
    		    		}
                 		if(res[8]!='undefined' || res[8]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationPhone'].value = res[8];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationPhone'].value ="";
    		    		}
                 		if(res[9]!='undefined' || res[9]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationPhoneExt'].value = res[9];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationPhoneExt'].value = "";
    		    		}
                 		if(res[10]!='undefined' || res[10]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value = res[10];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value = "";
    		    		}
                 		if(res[10]!='undefined' || res[10]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.contactPhone'].value = res[10];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.contactPhone'].value = "";
    		    		}
                 		if(res[11]!='undefined' || res[11]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationMobile'].value = res[11];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationMobile'].value = "";
    		    		}
                 		if(res[12]!='undefined' || res[12]!=undefined){
                 			document.forms['workTicketForm'].elements['workTicket.destinationFax'].value = res[12];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['workTicket.destinationFax'].value = "";
    		    		}
                 		if(res[5]!='undefined' || res[5]!=undefined){
                 			document.forms['workTicketForm'].elements['addAddressState'].value=res[5];
    		    		}else{
    		    			document.forms['workTicketForm'].elements['addAddressState'].value="";
    		    		}
						getDestinationCountryCode();
						getState('DA');	
						dstateMandatory();
						// getInstructionCodeValues();
					}else {
                 		var tempAddress = address.split('^');
                 		if(tempAddress[1]=='add'){
                 	if(temp=='origin'){ 
	                     	if(res!=''){                    		
	                 			document.forms['workTicketForm'].elements['workTicket.address1'].value = res[0];
	    						document.forms['workTicketForm'].elements['workTicket.address2'].value = res[1];
	    						document.forms['workTicketForm'].elements['workTicket.address3'].value = res[2];	                 			
	                 			targetElement = document.forms['workTicketForm'].elements['workTicket.originCountry'];            				
	             				for(i=0;i<targetElement.length;i++)	{
	                				if(res[3] == targetElement.options[i].value){	                				
	                 			document.forms['workTicketForm'].elements['workTicket.originCountry'].options[i].text = res[3];
	    						document.forms['workTicketForm'].elements['workTicket.originCountry'].options[i].value = res[3];
	    						document.forms['workTicketForm'].elements['workTicket.originCountry'].options[i].selected=true;
	         					}} 	             														
	    						document.forms['workTicketForm'].elements['workTicket.state'].value = res[4];
	    						document.forms['workTicketForm'].elements['workTicket.city'].value = res[5];
	    						document.forms['workTicketForm'].elements['workTicket.zip'].value = res[6];
	    						document.forms['workTicketForm'].elements['workTicket.phone'].value = res[7];
	    						document.forms['workTicketForm'].elements['workTicket.phoneExt'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.homePhone'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.originMobile'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.originFax'].value = '';
	    						document.forms['workTicketForm'].elements['addAddressState'].value=res[4];
	    						getOriginCountryCode();
	    						getState('OA')
	    						ostateMandatory();
	    						zipCode();
	    						// getInstructionCodeValues();
	                     	}else{
	                     		document.forms['workTicketForm'].elements['workTicket.address1'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.address2'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.address3'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.originCountry'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.state'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.state'].disabled =true;
	    						document.forms['workTicketForm'].elements['workTicket.city'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.zip'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.phone'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.phoneExt'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.homePhone'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.originMobile'].value = '';
	    						document.forms['workTicketForm'].elements['workTicket.originFax'].value = '';
	    						document.forms['workTicketForm'].elements['addAddressState'].value='';
	                        }}
                 		if(temp=='dest'){
                     		if(res!=''){
		                 			document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value = res[0];
		    						document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value = res[1];
		    						document.forms['workTicketForm'].elements['workTicket.destinationAddress3'].value = res[2];
									targetElement = document.forms['workTicketForm'].elements['workTicket.destinationCountry'];            				
		             				for(i=0;i<targetElement.length;i++)	{		                				
		                				if(res[3] == targetElement.options[i].value){			                				
		                 			document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[i].text = res[3];
		    						document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[i].value = res[3];
		    						document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[i].selected=true;
		         					}}
		    						document.forms['workTicketForm'].elements['workTicket.destinationState'].value = res[4];
		    						document.forms['workTicketForm'].elements['workTicket.destinationCity'].value = res[5];
		    						document.forms['workTicketForm'].elements['workTicket.destinationZip'].value = res[6];
		    						document.forms['workTicketForm'].elements['workTicket.destinationPhone'].value = res[7];
		    						document.forms['workTicketForm'].elements['workTicket.destinationPhoneExt'].value = '';
		    						document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value = '';
		    						document.forms['workTicketForm'].elements['workTicket.contactPhone'].value = '';
		    						document.forms['workTicketForm'].elements['workTicket.destinationMobile'].value = '';
		    						document.forms['workTicketForm'].elements['workTicket.destinationFax'].value = '';
		    						document.forms['workTicketForm'].elements['addAddressState'].value=res[4];
		    						getDestinationCountryCode();
		    						getState('DA');
		    						dstateMandatory();
		    						//getInstructionCodeValues();
	                     		}else{
	                     			document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationAddress3'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationState'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationState'].disabled =true;
	        						document.forms['workTicketForm'].elements['workTicket.destinationCity'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationZip'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationPhone'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationPhoneExt'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.contactPhone'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationMobile'].value = '';
	        						document.forms['workTicketForm'].elements['workTicket.destinationFax'].value = '';
	        						document.forms['workTicketForm'].elements['addAddressState'].value='';
		                     	}}}
                 		if(tempAddress[1]=='std'){
                         	if(temp=='origin'){                     		
                     			document.forms['workTicketForm'].elements['workTicket.address1'].value = res[0];
        						document.forms['workTicketForm'].elements['workTicket.address2'].value = res[1];
        						document.forms['workTicketForm'].elements['workTicket.address3'].value = res[2];
                     			targetElement = document.forms['workTicketForm'].elements['workTicket.originCountry'];            				
                 				for(i=0;i<targetElement.length;i++)	{                 					
                    				if(res[3] == targetElement.options[i].value){	
                     			document.forms['workTicketForm'].elements['workTicket.originCountry'].options[i].text = res[3];
        						document.forms['workTicketForm'].elements['workTicket.originCountry'].options[i].value = res[3];
        						document.forms['workTicketForm'].elements['workTicket.originCountry'].options[i].selected=true;
             					}} 
        						document.forms['workTicketForm'].elements['workTicket.state'].value = res[4];
        						document.forms['workTicketForm'].elements['workTicket.city'].value = res[5];
        						document.forms['workTicketForm'].elements['workTicket.zip'].value = res[6];
        						document.forms['workTicketForm'].elements['workTicket.phone'].value = res[7];
        						document.forms['workTicketForm'].elements['workTicket.phoneExt'].value = '';
        						document.forms['workTicketForm'].elements['workTicket.homePhone'].value = res[8];
        						document.forms['workTicketForm'].elements['workTicket.originMobile'].value = res[9];
        						document.forms['workTicketForm'].elements['workTicket.originFax'].value =res[10];
        						document.forms['workTicketForm'].elements['addAddressState'].value=res[4];
        						getOriginCountryCode();
        						getState('OA');
        						ostateMandatory();
        						zipCode();
        						// getInstructionCodeValues();
        					}
                     		if(temp=='dest'){
                     			document.forms['workTicketForm'].elements['workTicket.destinationAddress1'].value = res[0];
        						document.forms['workTicketForm'].elements['workTicket.destinationAddress2'].value = res[1];
        						document.forms['workTicketForm'].elements['workTicket.destinationAddress3'].value = res[2];
    							targetElement = document.forms['workTicketForm'].elements['workTicket.destinationCountry'];            				
                 				for(i=0;i<targetElement.length;i++)	{
                    				if(res[3] == targetElement.options[i].value){	
                     			document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[i].text = res[3];
        						document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[i].value = res[3];
        						document.forms['workTicketForm'].elements['workTicket.destinationCountry'].options[i].selected=true;
             					}} 
                 				document.forms['workTicketForm'].elements['workTicket.destinationState'].value = res[4];
        						document.forms['workTicketForm'].elements['workTicket.destinationCity'].value = res[5];
        						document.forms['workTicketForm'].elements['workTicket.destinationZip'].value = res[6];
        						document.forms['workTicketForm'].elements['workTicket.destinationPhone'].value = res[7];
        						document.forms['workTicketForm'].elements['workTicket.destinationPhoneExt'].value = '';
        						document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value = res[8];
        						document.forms['workTicketForm'].elements['workTicket.contactPhone'].value = res[8];
        						document.forms['workTicketForm'].elements['workTicket.destinationMobile'].value = res[9];
        						document.forms['workTicketForm'].elements['workTicket.destinationFax'].value = res[10];
        						document.forms['workTicketForm'].elements['addAddressState'].value=res[4];
        						getDestinationCountryCode();
        						getState('DA');
        						dstateMandatory();
        						// getInstructionCodeValues();
        					} }	}					
                    }
  				}      		      
	var http31 = getHTTPObject100();
	function getHTTPObject100()	{
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }	
    return xmlhttp;
}	
</script>
<script type="text/javascript">
<configByCorp:fieldVisibility componentId="component.field.State.NotManadotry">
<c:set var="stateShowOrHide" value="Y" />
</configByCorp:fieldVisibility>
	function ostateMandatory(){
	    var originCountry=document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
	    var reqTrue=document.getElementById("originStateRequiredTrue");
	    var reqFalse=document.getElementById("originStateRequiredFalse");
	    if(originCountry=='United States' ){
	    	reqTrue.style.display='block';
	    	reqFalse.style.display='none';
	    }else if(originCountry == 'India'){
		    <c:if test="${stateShowOrHide=='Y'}">
		    reqTrue.style.display = 'none';		
		    reqFalse.style.display = 'block';
			</c:if>
			 <c:if test="${stateShowOrHide!='Y'}">
			 reqTrue.style.display='block';
			    reqFalse.style.display='none';
			</c:if>
		
		}else{
	    	reqFalse.style.display='block';
	    	reqTrue.style.display='none';
	    }
	}
function dstateMandatory(){
	    var destinationCountry=document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
	    var reqTrue=document.getElementById("destinationStateRequiredTrue");
	    var reqFalse=document.getElementById("destinationStateRequiredFalse");
	    if(destinationCountry=='United States' ){
	    	reqTrue.style.display='block';
	    	reqFalse.style.display='none';
	    }else if(destinationCountry == 'India'){
		    <c:if test="${stateShowOrHide=='Y'}">
		    reqTrue.style.display = 'none';		
		    reqFalse.style.display = 'block';
			</c:if>
			 <c:if test="${stateShowOrHide!='Y'}">
			 reqTrue.style.display='block';
			    reqFalse.style.display='none';
			</c:if>
		
		}
	    else{
	    	reqFalse.style.display='block';
	    	reqTrue.style.display='none';
	    }
	}
	
function copyHomePhoneToContact(){
	if(document.forms['workTicketForm'].elements['workTicket.contactPhone'].value==''){
	document.forms['workTicketForm'].elements['workTicket.contactPhone'].value=document.forms['workTicketForm'].elements['workTicket.destinationHomePhone'].value;
	}
}	
	
</script>
<style type="text/css">
/* collapse */
.layer8{width:750px;}
</style>
<script type="text/javascript">
 function checkMultiAuthorization()
    {  
    var billCode = document.forms['workTicketForm'].elements['workTicket.account'].value; 
    var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billCode;  
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse41; 
     http55.send(null);
  
}

function handleHttpResponse41()
        {    if (http55.readyState == 4) {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='true') {
                document.getElementById("hidAuthor").style.display="block";
                }  
                else{
                document.getElementById("hidAuthor").style.display="none";     
             	}
            } 	
        }
var http55 = getHTTPObject55();
   function getHTTPObject55(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
   function checkBillToAuthority(){
		if(document.forms['workTicketForm'].elements['billToAuthority'].value == ""){
	 		alert("Enter Authorization No to continue.....");
	 		return false;
	 		}else{
	 		return authorizationNos();
	 	}
	}
   function authorizationNos(){
		var authNo = document.forms['workTicketForm'].elements['billToAuthority'].value;	
		javascript:openWindow('WorkTicketAuthorizationNos.html?billId=${billing.id}&billingId=${billing.shipNumber}&authNo='+authNo+'&serviceJob=${serviceOrder.job}&serviceId=${serviceOrder.id}&serviceCom=${serviceOrder.commodity}&workTicketId=${workTicket.id}&decorator=popup&popup=true');
		
	}
   function authUpdatedDate() {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['workTicketForm'].elements['authUpdated'].value=datam;
	} 
   </script>
   <script type="text/javascript">
   /* Disabled all element on workTicketForm */
   function disabledAll(){
		var elementsLen=document.forms['workTicketForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['workTicketForm'].elements[i].type=='text'){
				document.forms['workTicketForm'].elements[i].readOnly =true;
				document.forms['workTicketForm'].elements[i].className = 'input-textUpper'; 
			}else if(document.forms['workTicketForm'].elements[i].type=='textarea'){
				document.forms['workTicketForm'].elements[i].readOnly =true;
				document.forms['workTicketForm'].elements[i].className = 'textareaUpper';
			}else{
				document.forms['workTicketForm'].elements[i].disabled=true;
			} 
		}
	
		<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
        if(document.forms['workTicketForm'].elements["workTicket.acceptByDriver"].disabled=true){
			 document.forms['workTicketForm'].elements["workTicket.acceptByDriver"].disabled=false
	      } 
		if(document.forms['workTicketForm'].elements["wticket"].disabled=true){
			 document.forms['workTicketForm'].elements["wticket"].disabled=false
	      }
		if(document.forms['workTicketForm'].elements["workTicket.crewName"].type=='text'){
			document.forms['workTicketForm'].elements["workTicket.crewName"].readOnly =false;
			 document.forms['workTicketForm'].elements["workTicket.crewName"].className = "input-text"; 
	}
		if(document.forms['workTicketForm'].elements["workTicket.projectLeadName"].type=='text'){
			document.forms['workTicketForm'].elements["workTicket.projectLeadName"].readOnly =false;
			 document.forms['workTicketForm'].elements["workTicket.projectLeadName"].className = "input-text"; 
	}
		if(document.forms['workTicketForm'].elements["workTicket.projectLeadNumber"].type=='text'){
			document.forms['workTicketForm'].elements["workTicket.projectLeadNumber"].readOnly =false;
			 document.forms['workTicketForm'].elements["workTicket.projectLeadNumber"].className = "input-text"; 
	}
	 	var checkInvoiceStatus=document.getElementById('workTicketForm_workTicket_accepted').checked; 
		 if(checkInvoiceStatus)
			{
			<c:if test="${!checkOpsRole}"> 
		    document.forms['workTicketForm'].elements["workTicket.acceptByDriver"].disabled=true
		    </c:if>
			}
</configByCorp:fieldVisibility>
     
		
   }
   function trap() 
	  {
	  <c:if test="${usertype=='DRIVER'}">
	  if(document.images)
	    {
	      var totalImages = document.images.length;
	      	for (var i=0;i<totalImages;i++)
				{
	      		try{
	      			if(document.images[i].src.indexOf('calender.png')>0)
					{
						var el = document.getElementById(document.images[i].id);
						el.onclick = false;
						document.images[i].src = 'images/navarrow.gif';
					}
	            }catch(e){
	      		}
	            try{
	            	if(document.images[i].src.indexOf('open-popup.gif')>0)
					{ 						
							var el1 = document.getElementById(document.images[i].id);
							el1.onclick = false;
						    document.images[i].src = 'images/navarrow.gif';
					}	
	            }catch(e){
	            }
				
					if(document.images[i].src.indexOf('globe.png')>0)
					{
							var el = document.getElementById(document.images[i].id);
							el.onclick = false;
					        document.images[i].src = 'images/navarrow.gif';
					} 
					if(document.images[i].src.indexOf('navarrows_05.png')>0)
					{
						var el = document.getElementById(document.images[i].id);
						el.onclick = false;
						document.images[i].src = 'images/navarrow.gif';
					} 									
									
				}
	    }
	  </c:if>
	  }   
    </script>
    <script type="text/javascript">
    function addRowOrigin(tableId) {
        var table = document.getElementById(tableId);
        var rowCount = table.rows.length;
    	var row = table.insertRow(rowCount);
        var newlineid=document.getElementById('newlineIdTime12').value;
        if(newlineid==''){
            newlineid=rowCount;
          }else{
          	newlineid=newlineid+"~"+rowCount;
          }
        document.getElementById('newlineIdTime12').value=newlineid;    	
        
    	var cell1 = row.insertCell(0);
    	var element1 = document.createElement("input");
    	element1.type = "text";
    	element1.style.width="177px";
    	element1.setAttribute("class", "input-text" );
    	element1.id='Req'+rowCount;
    	element1.name='oriItemList';
    	element1.setAttribute("onblur","valid(this,'special')");
    	cell1.appendChild(element1);
    	
    	var cell2 = row.insertCell(1);    	
    	var element2 = document.createElement("input");
    	element2.type = "text";
    	element2.style.width="70px";
    	element2.setAttribute("class", "input-text" );
    	element2.setAttribute("value", "00:00" );
    	element2.id='ST'+rowCount;
    	element2.name='oriItemList';
    	element2.setAttribute("onkeydown","return onlyTimeFormatAllowed(event)");
    	var temp = 'ST'+rowCount;   
    	element2.setAttribute("onchange","return IsTimeValidFortimeRequirement("+temp+"),IsTimeassigntimeairthmatic("+rowCount+")");
    	cell2.appendChild(element2);

    	var cell7 = row.insertCell(2);
        var elementimg2=document.createElement("img");
        elementimg2.setAttribute("class","openpopup");
        elementimg2.setAttribute("style","vertical-align:top;padding-left:2px");
        elementimg2.setAttribute("src","<c:url value='/images/time.png'/>");
        cell7.appendChild(elementimg2); 

    	var cell3 = row.insertCell(3);
    	var element3 = document.createElement("input");
    	element3.type = "text";
    	element3.style.width="70px";
    	element3.setAttribute("class", "input-text" );
    	element3.setAttribute("value", "00:00" );
    	element3.id='ET'+rowCount;
    	element3.name='oriItemList';
    	element3.setAttribute("onkeydown","return onlyTimeFormatAllowed(event)");
    	var temp1 = 'ET'+rowCount;
    	element3.setAttribute("onchange","return IsTimeValidFortimeRequirement("+temp1+"),IsTimeassigntimeairthmatic("+rowCount+")");
    	cell3.appendChild(element3);
    	var cell8 = row.insertCell(4);
        var elementimg1=document.createElement("img");
        elementimg1.setAttribute("class","openpopup");
        elementimg1.setAttribute("style","vertical-align:top;padding-left:2px");
        elementimg1.setAttribute("src","<c:url value='/images/time.png'/>");
        cell8.appendChild(elementimg1); 
    	var cell4 = row.insertCell(5);
        var elementimg=document.createElement("img");
        elementimg.setAttribute("onclick","addRowOrigin('dataTableO');this.src='${pageContext.request.contextPath}/images/blankWhite.png';this.onclick=null;");
        elementimg.setAttribute("class","openpopup");
        elementimg.setAttribute("style","vertical-align:top;padding-left:8px");
        elementimg.setAttribute("src","<c:url value='/images/addNew.png'/>");
        cell4.appendChild(elementimg);        
    }    
    function timeRequirementDetails(){
    	<configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">	
    	var workTicketId='${workTicket.id}';    	
    	var ticketNo='${workTicket.ticket}';
    	$.get("timeRequirementDetailsAjax.html?ajax=1&decorator=simple&popup=true", 
    			{workTicketId:workTicketId,ticketNo:ticketNo},
    			function(data){
    				$("#timeRequirementDetails").html(data);
    				document.getElementById('newlineIdTime12').value="";
    				var timeTestLiat= document.getElementById('timeRequirementDetailsList').value;
    				if(timeTestLiat=='true'){
    					addRowOrigin('dataTableO');
    				}
    		});
    	</configByCorp:fieldVisibility>
    }
function timeRequirementRemove(timeRequirementId){
	var workTicketId='${workTicket.id}';
	var ticketNo='${workTicket.ticket}';
		var agree=confirm("Are you sure you wish to remove this Row ?");
	if (agree){
	$.get("timeRequirementRemoveAjax.html?ajax=1&decorator=simple&popup=true", 
			{timeRequirementId: timeRequirementId,workTicketId:workTicketId,ticketNo:ticketNo},
			function(data){
				$("#timeRequirementDetails").html(data);
				document.getElementById('newlineIdTime12').value="";
				var timeTestLiat= document.getElementById('timeRequirementDetailsList').value;
				if(timeTestLiat=='true'){
					addRowOrigin('dataTableO');
				}
		});
	}else{
		return false;
	}
}  
function IsTimeValidFortimeRequirement(clickType) {
	var clickType1=clickType.id;
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var timeStr =document.getElementById(clickType1).value;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
	alert("Time is not in a valid format. Please Use HH:MM Format");
	document.getElementById(clickType1).value='00:00';
	document.getElementById(clickType1).focus();
	return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	if (second=="") { second = null; }
	if (ampm=="") { ampm = null }

	if (hour < 0  || hour > 23) {
	alert("Hour must be between 0 and 23");
	document.getElementById(clickType1).value='00:00';
	document.getElementById(clickType1).focus();
	return false;
	}
	if (minute<0 || minute > 59) {
	alert ("Minute must be between 0 and 59.");
	document.getElementById(clickType1).value='00:00';
	document.getElementById(clickType1).focus();
	return false;
	}
	if (second != null && (second < 0 || second > 59)) {
	alert ("Second must be between 0 and 59.");
	document.getElementById(clickType1).focus();
	return false;
	}		
}
function IsTimeValidFortimeRequirement13(clickType,checkValue) {
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var timeStr =document.getElementById(clickType).value;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
	alert("Time is not in a valid format. Please Use HH:MM Format");
	document.getElementById(clickType).value=checkValue;
	document.getElementById(clickType).focus();
	return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	if (second=="") { second = null; }
	if (ampm=="") { ampm = null }

	if (hour < 0  || hour > 23) {
	alert("Hour must be between 0 and 23");
	document.getElementById(clickType).value='00:00';
	document.getElementById(clickType).focus();
	return false;
	}
	if (minute<0 || minute > 59) {
	alert ("Minute must be between 0 and 59.");
	document.getElementById(clickType).value='00:00';
	document.getElementById(clickType).focus();
	return false;
	}
	if (second != null && (second < 0 || second > 59)) {
	alert ("Second must be between 0 and 59.");
	document.getElementById(clickType).focus();
	return false;
	}
}
function IsTimeassigntimeairthmatic(clickType)
{
	var strtId="ST"+clickType;
	var endId="ET"+clickType;
	var time1=0;
	var time2=0;
	var diffTime=0;
	var tim1=document.getElementById(strtId).value;
	var tim2=document.getElementById(endId).value;
	var mySplitResult1 = tim1.split(":");
		var hour1 = mySplitResult1[0];
		var minute1 = mySplitResult1[1];
		time1=(hour1*60)+(minute1*1);
    var mySplitResult2 = tim2.split(":");
		var hour2 = mySplitResult2[0];
		var minute2 = mySplitResult2[1];
		time2=(hour2*60)+(minute2*1)
	if(time1 < time2){
		diffTime = time2 - time1;
	}else if(time1 > time2 && time2 !=0){
	alert("Start Time should be less than End Time");
	document.getElementById(strtId).value='00:00';
	document.getElementById(endId).value='00:00';
	}	
 }
function autoCompleterAjaxCallOriCity(){
	var countryName = document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
	var stateNameOri = document.forms['workTicketForm'].elements['workTicket.state'].value;
	var cityNameOri = document.forms['workTicketForm'].elements['workTicket.city'].value;
	var countryNameOri = "";
	countryNameOri =getKeyByValue(countryName);
	if(cityNameOri!=''){
		var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
		$("#originCity").autocomplete({				 
		      source: data		      
		    });
	}
}
function autoCompleterAjaxCallDestCity(){
	var stateNameDes = document.forms['workTicketForm'].elements['workTicket.destinationState'].value;
	var countryName = document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
	var cityNameDes = document.forms['workTicketForm'].elements['workTicket.destinationCity'].value;
	var countryNameDes = "";
	countryNameDes =getKeyByValue(countryName);
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#destinationCity" ).autocomplete({				 
	      source: data		      
	    });
	}
}
function printDailyPackage(){
	var wid = "${workTicket.id}";
	var url ="printWorkTicketReport.html?wid="+encodeURI(wid);
	location.href=url;	
}
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110|| (keyCode==173)||(keyCode==189)); 
}
    </script>
</head>
<c:set var="currencySign" value="${currencySign}"/>
<s:hidden name="noteFor" value="WorkTicket" /> 
<s:form id="workTicketForm" name="workTicketForm" action="saveWorkTicket" onsubmit="return submit_form()" method="post" validate="true"> 
<s:hidden name="checkLimit" value=""  />
<div id="Layer1" style="width:100%;">  
    <s:hidden name="forceToHandOut" value="true" />
    <s:hidden name="instructionCodeHiddenValue"/>
    <s:hidden name="instCode" value="${workTicket.instructionCode}"/>
	<s:hidden name="addAddressState" />
	<s:hidden name="customerFileId" value="${customerFile.id}"/>
    <s:hidden name="customerFileJob" value="${serviceOrder.job}"/>   
	<s:hidden id="countForTicketNotes" name="countForTicketNotes" value="<%=request.getParameter("countForTicketNotes") %>"/>
	<s:hidden id="countTicketBillingNotes" name="countTicketBillingNotes" value="<%=request.getParameter("countTicketBillingNotes") %>"/>
	<s:hidden id="countTicketStaffNotes" name="countTicketStaffNotes" value="<%=request.getParameter("countTicketStaffNotes") %>"/>
	<s:hidden id="countTicketStorageNotes" name="countTicketStorageNotes" value="<%=request.getParameter("countTicketStorageNotes") %>"/>
	<s:hidden id="countTicketSchedulingNotes" name="countTicketSchedulingNotes" value="<%=request.getParameter("countTicketSchedulingNotes") %>"/>
	<s:hidden id="countTicketOriginNotes" name="countTicketOriginNotes" value="<%=request.getParameter("countTicketOriginNotes") %>"/>
	<s:hidden id="countTicketDestinationNotes" name="countTicketDestinationNotes" value="<%=request.getParameter("countTicketDestinationNotes") %>"/>
	<s:hidden name="workTicket.id" value="%{workTicket.id}" />
	<s:hidden name="unit" id ="unit" value="%{dummyDistanceUnit}" />
	<c:set var="countForTicketNotes" value="<%=request.getParameter("countForTicketNotes") %>" />
	<c:set var="countTicketBillingNotes" value="<%=request.getParameter("countTicketBillingNotes") %>" />
	<c:set var="countTicketStaffNotes" value="<%=request.getParameter("countTicketStaffNotes") %>" />
	<c:set var="countTicketStorageNotes" value="<%=request.getParameter("countTicketStorageNotes") %>" />
	<c:set var="countTicketSchedulingNotes" value="<%=request.getParameter("countTicketSchedulingNotes") %>" />
	<c:set var="countTicketOriginNotes" value="<%=request.getParameter("countTicketOriginNotes") %>" />
	<c:set var="countTicketDestinationNotes" value="<%=request.getParameter("countTicketDestinationNotes") %>" />
	<c:set var="from" value="<%=request.getParameter("from") %>"/>
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
	<s:hidden name="field" value="<%=request.getParameter("field") %>" />
	<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
	<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
    <s:hidden name="serviceCheckingBeginDateVar" id="serviceCheckingBeginDateVar"/>
    <s:hidden name="serviceCheckingEndDateVar" id="serviceCheckingEndDateVar"/>
	<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
    <s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
    <s:hidden name="ppType" id ="ppType" value="" />
    <c:set var="ppType" value=""/>
	<s:hidden name="workTicket.corpID" /> 
	<s:hidden name="id" value="<%=request.getParameter("id")%>" />
	<s:hidden name="wid" value="<%=request.getParameter("wid")%>" />
	<s:hidden name="workTicket.unit1" value="%{workTicket.unit1}" />
	<s:hidden name="soid" value="%{serviceOrder.id}" />
	<s:hidden name="workTicket.unit2" value="%{workTicket.unit2}" />
	<s:hidden name="workTicket.attachedFile" />
	<s:hidden name="serviceOrder.id" /> 
	<c:set var="shipnum" value="${serviceOrder.shipNumber}"/>
	<s:hidden name="shipnum" value="${shipnum}"/>
	<s:hidden name="timeeq" />
	<s:hidden name="timees" />
	<s:hidden name="timeen" />
	<s:hidden name="timeem" />
	<s:hidden name="serviceOrder.shipNumber" />
	<s:hidden name="maxTicket" />
    <s:hidden name="minTicket" />
    <s:hidden name="countTicket" />
    <s:hidden name="shipSize" />
	<s:hidden name="minShip" />
	<s:hidden name="countShip"/> 
	<s:hidden name="usertype"/>
	<s:hidden name="newlineIdTime12" id="newlineIdTime12" value=""/>
	<s:hidden name="resourceExList" id="resourceExList" />
	<s:hidden name="timeManagementList" id="timeManagementList" value=""/>
	<s:hidden id="originMandatoryForLongCarry" name="originMandatoryForLongCarry" />
	<s:hidden id="destinationMandatoryForLongCarry" name="destinationMandatoryForLongCarry" />
	<s:hidden name="releasedStorageDate" />
	<s:hidden name="copyWorkTicketFlag" />
	<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
	<s:hidden  name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		<s:hidden name="checkConditionForOriginDestin" />
	 <s:hidden name="stdAddDestinState" />
	 <s:hidden name="stdAddOriginState" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="workTicket.revenueCalculation" />
	<s:hidden name="workTicket.serviceOrderId" value="%{serviceOrder.id}"/>
	<s:hidden name="workTicket.orderId" />
	<s:hidden name="workTicket.requiredCrew" />
	<s:hidden name="workTicket.ticketAssignedStatus"/>
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="closedCompanyDivision" value="N" />
	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
		<c:set var="closedCompanyDivision" value="Y" />
	</configByCorp:fieldVisibility> 
	<s:hidden name="oldTargetActual"  />
	 <c:if test="${not empty workTicket.reviewStatusDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="workTicket.reviewStatusDate" /></s:text>
			 <s:hidden  name="workTicket.reviewStatusDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty workTicket.reviewStatusDate}">
		 <s:hidden   name="workTicket.reviewStatusDate"/> 
	 </c:if>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
	<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
	<c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
<c:if test="${forwardingTabVal!='Y'}">
	<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:if>
<c:if test="${forwardingTabVal=='Y'}">
	<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.OI' }">
	<c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
	<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
  <c:if test="${serviceOrder.job =='RLO'}"> 
	 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
</c:if>
  <c:if test="${serviceOrder.job !='RLO'}"> 
	<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
	<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
	<c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.workticket' }">
	<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.material' }">
	<c:redirect url="/itemsJbkEquips.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.equipment' }">
	<c:redirect url="/itemsJbkResourceList.html?id=${workTicket.id}&sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.crew' }">
	<c:redirect url="/workTicketCrews.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.storage' }">
	<c:redirect url="/bookStorages.html?id=${workTicket.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.storagelibrary' }">
	<c:redirect url="/bookStorageLibraries.html?id=${workTicket.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.mss' }">
	<c:redirect url="/editMss.html?wid=${workTicket.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.criticaldate' }">
  <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  		<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}"/>
	</sec-auth:authComponent>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="newmnav" style="float: left">
		    <ul>

	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	        <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	               <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
		    <li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
		    </c:if>
		    </sec-auth:authComponent>
		    </configByCorp:fieldVisibility>
		
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
		      	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.serviceorder');return IsValidTime('none');"><span>S/O Details</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.billing');return IsValidTime('none');"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
				  <c:choose>
				     <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
				       <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
				     </c:when> --%>
				     <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 				<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
					</c:when>
				     <c:otherwise> 
			           <li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.accounting');return IsValidTime('none');"><span>Accounting</span></a></li> 
			         </c:otherwise>
			      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
				     <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 				<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
					</c:when>
				     <c:otherwise> 
			           <li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.newAccounting');return IsValidTime('none');"><span>Accounting</span></a></li> 
			         </c:otherwise>
			      </c:choose>
		      </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a onclick="setReturnString('gototab.OI');return IsValidTime('none');"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
		      <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
 		      <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">
			  		<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.forwarding');return IsValidTime('none');"><span>Forwarding</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
				  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
				  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.domestic');return IsValidTime('none');"><span>Domestic</span></a></li>
				  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                 <li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.domestic');return IsValidTime('none');"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.status');return IsValidTime('none');"><span>Status</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">
			  	<li  id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.claims');return IsValidTime('none');"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			  <sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			 	 <li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.customerfile');return IsValidTime('none');"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		  		<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.criticaldate');return IsValidTime('none');"><span>Critical Dates</span></a></li>
			</sec-auth:authComponent>
			<c:if test="${usertype=='USER'}">
	 	<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
  	      </c:if> </ul>
		</div>
		<c:if test="${usertype!='DRIVER'}">
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td> 
  		<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">
		<a><img class="openpopup" id="navarrows05" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		</c:if>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;!padding-bottom:7px;"></div>
	<%@ include file="/WEB-INF/pages/trans/workTicketHeader.jsp"%> 
<c:if test="${not empty workTicket.id}">
<div id="Layer8" style="width:100%;">
	<div id="newmnav" style="float:left;">
		    <ul>
		              <c:if test="${usertype=='DRIVER'}">
		              <li id="newmnav1" style="background:#FFF"><a href="#" class="current"><span>Work Ticket</span></a></li>
		              </c:if>		              
		    		<sec-auth:authComponent componentId="module.tab.workTicket.workTicketTab">
		    			<li id="newmnav1" style="background:#FFF"><a href="customerWorkTickets.html?id=${serviceOrder.id}" class="current"><span>Work Ticket</span></a></li>
			  		</sec-auth:authComponent>
			  		<sec-auth:authComponent componentId="module.tab.workTicket.materialEquipmentTab">
			  			 <li><a href="itemsJbkEquipExtraInfo.html?wid=${workTicket.id}&sid=${serviceOrder.id}&itemType=E"><span>Equipment&nbsp;And&nbsp;Material</span></a></li> 
			  			<%-- <li><a href="itemsJbkEquipExtraInfo.html?wid=${workTicket.id}&sid=${serviceOrder.id}"><span>Equipment&nbsp;And&nbsp;Material</span></a></li> --%>
			  								</sec-auth:authComponent>
					<sec-auth:authComponent componentId="module.tab.workTicket.materialTab">
			  			<li><a onmouseover="completeTimeString();assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');" onclick="setReturnString('gototab.material');return IsValidTime('none');"><span>Material</span></a></li>
					</sec-auth:authComponent>
					 <c:if test="${usertype!='DRIVER'}">
					<sec-auth:authComponent componentId="module.tab.workTicket.equipmentTab">
						<li><a onmouseover="completeTimeString();assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');" onclick="setReturnString('gototab.equipment');return IsValidTime('none');"><span>Resource</span></a></li>
					</sec-auth:authComponent>
					</c:if>
					 <c:if test="${usertype=='DRIVER'}">
					 <li><a href="itemsJbkResourceList.html?id=${workTicket.id}&sid=${serviceOrder.id}"><span>Resource</span></a></li>
					 </c:if>
					<sec-auth:authComponent componentId="module.tab.workTicket.crewTab">
						<li><a onmouseover="completeTimeString();assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');" onclick="setReturnString('gototab.crew');return IsValidTime('none');"><span>Crew</span></a></li>
					</sec-auth:authComponent>
					<configByCorp:fieldVisibility componentId="component.tab.workTicket.TruckingOperations">
					<c:if test="${usertype!='DRIVER'}">
					<li><a href="truckingOperationsList.html?ticket=${workTicket.ticket}&sid=${serviceOrder.id}&tid=${workTicket.id}"><span>Truck</span></a></li>
					</c:if>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
					<sec-auth:authComponent componentId="module.tab.workTicket.whseMgmtTab">
						<li><a onmouseover="completeTimeString();assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');" onclick="setReturnString('gototab.storage');return IsValidTime('none');"><span>Whse/Mgmt</span></a></li>
					</sec-auth:authComponent>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.tab.workTicket.storageMgmtTab">
					<sec-auth:authComponent componentId="module.tab.workTicket.storageMgmtTab">
					<li><a onmouseover="completeTimeString();assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');" onclick="setReturnString('gototab.storagelibrary');return IsValidTime('none');"><span>Storage</span></a></li>
					</sec-auth:authComponent>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.tab.workTicket.MSSTab">
					<c:if test="${workTicket.service =='MSS' && workTicket.warehouse=='T'}">
						<li><a onclick="setReturnString('gototab.mss');return IsValidTime('none');"><span>MSS</span></a></li>
					</c:if>
					</configByCorp:fieldVisibility>
					<sec-auth:authComponent componentId="module.tab.workTicket.formsTab">
						<li><a onclick="window.open('subModuleReports.html?id=${workTicket.id}&jobNumber=${serviceOrder.shipNumber}&noteID=${workTicket.ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					</sec-auth:authComponent>
					<sec-auth:authComponent componentId="module.tab.workTicket.auditTab">
						<li><a onclick="window.open('auditList.html?id=${workTicket.id}&tableName=workticket&decorator=popup&popup=true','audit','height=400,width=775,top=150, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
					</sec-auth:authComponent>
			</ul>
		</div>
		<c:if test="${usertype!='DRIVER'}">
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${workTicket.ticket> minTicket}" >
  		<a><img align="middle" onclick="goPrevChild();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${workTicket.ticket == minTicket}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<td width="20px" align="left">
  		<c:if test="${workTicket.ticket < maxTicket}" >
  		<a><img align="middle" onclick="goNextChild();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${workTicket.ticket == maxTicket}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		
		<c:if test="${countTicket != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">
		<a><img class="openpopup" id="navarrows06" onclick="findCustomerOtherSOChild(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="SO Ticket List" title="SO Ticket List" /></a> 
		</td>
		</c:if>
		<c:if test="${countTicket == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>		
		</c:if></tr></table>
		</c:if>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;!padding-bottom:7px;"></div>
		</div>
</c:if> 
<c:if test="${empty workTicket.id}">
<div id="Layer8" style="100%;" >
	<div id="newmnav">
		    <ul>
			  <li id="newmnav1" style="background:#FFF"><a href="customerWorkTickets.html?id=${serviceOrder.id}" class="current"><span>Work Ticket</span></a></li>			  		  	
			  		  	<sec-auth:authComponent componentId="module.tab.workTicket.materialTab">
			  		  	<li><a><span>Material</span></a></li>
			  		  	</sec-auth:authComponent>
			  		  	<sec-auth:authComponent componentId="module.tab.workTicket.materialEquipmentTab">
			  			<%-- <li><a href="itemsJbkEquipExtraInfo.html?wid=${workTicket.id}&sid=${serviceOrder.id}&itemType=E"><span>Equipment&nbsp;And&nbsp;Material</span></a></li> --%>
			  			<li><a href="itemsJbkEquipExtraInfo.html?wid=${workTicket.id}&sid=${serviceOrder.id}"><span>Equipment&nbsp;And&nbsp;Material</span></a></li>
					   </sec-auth:authComponent>
					  	<sec-auth:authComponent componentId="module.tab.workTicket.equipmentTab"><li><a><span>Resource</span></a></li></sec-auth:authComponent>
					  	<li><a><span>Crew</span></a></li>
					  	<li><a><span>Whse/Mgmt</span></a></li>					  	
						<li><a><span>Forms</span></a></li>						
						<li><a><span>Audit</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
	</c:if>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;margin: 0px;">
	<s:hidden name="workTicket.prefix" cssClass="input-textUpper" /> <s:hidden
		name="workTicket.firstName" cssClass="input-textUpper"
		onkeydown="return onlyCharsAllowed(event)" /> <s:hidden
		name="workTicket.mi" cssClass="input-textUpper"
		onkeydown="return onlyCharsAllowed(event)" /> <s:hidden
		key="workTicket.lastName" cssClass="input-textUpper"
		onkeydown="return onlyCharsAllowed(event)" /> <s:hidden
		key="workTicket.suffix" cssClass="input-textUpper"
		onkeydown="return onlyCharsAllowed(event)" /> <s:hidden
		name="workTicket.jobType" cssClass="input-text" /> <s:hidden
		name="workTicket.jobMode" cssClass="input-text" />
		<s:hidden name="workTicket.sequenceNumber"/>
		<s:hidden name="workTicket.registrationNumber" cssClass="input-text" value="%{serviceOrder.registrationNumber}" />
		<s:hidden name="workTicket.cancelled" />
		<s:hidden name="customerFile.id" />
		<s:hidden name="serviceOrder.sequenceNumber"/>
		<s:hidden name="serviceOrder.serviceOrderId"/>
		<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
    <div class="center-content">
<table  cellspacing="0" cellpadding="0" border="0" style="width:100%;">
		<tbody>
			<tr>
				<td>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"style="width:100%">
					<tr>
						<td class="" colspan="6" height="25"><table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Ticket
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table></td>
					</tr>
					<tr>
						<td colspan="6">
						<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
							<tbody>
								<tr>
									<td align="right" height="30" class="listwhitetext"><fmt:message key='workTicket.ticket' /></td>
									<td align="left" ><s:textfield id="ticketStatus" name="workTicket.ticket" size="14" cssStyle="width:93px" cssClass="input-text" readonly="true"  /></td>
									
									<td colspan="5" style="margin: 0px;">
									<div id="hid1" style="margin: 0px;">
								 	<table class="detailTabLabel" border="0" style="margin: 0px;">
									<tbody>
									<tr>
									<td align="right" width="96px" class="listwhitetext">Vendor Code<div id="hid4" style="margin: 0px;float:right;"><font color="red" size="2">*</font></div></td>
								    <td align="left"><s:textfield name="workTicket.vendorCode" id="vendorCode" required="true" cssClass="input-text" size="9" maxlength="8" onchange="getVendorName();"  onkeydown="return disableEnter(event)"/></td>
									<td align="left" style="width:113px"><img class="openpopup" width="17" height="20" onclick="javascript:winOpen()" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
									<td align="left" width="" class="listwhitetext">Vendor&nbsp;Name</td>
									<td align="left" colspan=""><input type="text" name="workTicket.vendorName" value="${workTicket.vendorName}" id="vendorName" onkeydown="return onlyCharsAllowed(event)"  class="input-text" size="38" maxlength="250" onkeyup="autoCompleteVendorNameAjax('vendorName','vendorCode','autoVendorNameDivId',event);" oncopy="return false;" oncut="return false;" onpaste="return false;"  />
									<div id="autoVendorNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div></td>
									</tr>
									</tbody>
									</table>
									</div>
									</td>
									<td align="right" width="" valign="middle" class="listwhitetext" rowspan="2">
										<c:if test="${serviceOrder.vip}">
											<div style="position:absolute;top:25px;margin-left:-15px;">
												<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
											</div>
										</c:if>
									</td>	
									<td width="116">
										<c:if test= "${mmValidation =='Yes' }" >
											<c:if test="${workTicket.orderId ne null && workTicket.orderId!='' }">
												<img src="${pageContext.request.contextPath}/images/sent-mobile.png" HEIGHT="25" WIDTH="116" border="0"  />
											</c:if>
										</c:if>
									</td>
									<td align="bottom">
									<c:if test="${not empty workTicket.id }">
										<c:if test= "${mmValidation =='Yes' }" >
											<sec-auth:authComponent componentId="module.tab.workTicket.MMDocAttached">
												<img src="${pageContext.request.contextPath}/images/docattach.png" HEIGHT="25" WIDTH="116" border="0" title="Attach documents to incorporate in daily print package." align="left"  alt="Attach Document" onclick="window.open('attachedDocToWorkTicket.html?id=${workTicket.id}&sid=${serviceOrder.id}&decorator=popup&popup=true','Attach Document','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"/>
												<c:if test = "${docCount == 0}">
													<br/>
													<div style="position:absolute;float:left;padding:12px 0px 0px 5px;">No doc attached yet.</div>
												</c:if>
												<c:if test = "${docCount != 0}">
													<c:if test = "${docCount == 1}">
														<br/>
														<div style="position:absolute;float:left;padding:12px 0px 0px 5px;">${docCount} doc attached <img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${workTicket.id}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/></div>
													</c:if>
													<c:if test = "${docCount != 1}">
														<br/>
														<div style="position:absolute;float:left;padding:12px 0px 0px 5px;">${docCount} docs attached <img align="top" title="Attached Docs" alt="showDocs" onclick="showAttachedDocList('${workTicket.id}',this);" src="${pageContext.request.contextPath}/images/add-docx16.png"/></div>
													</c:if>
												</c:if>
											</sec-auth:authComponent>
										</c:if>
										</c:if>
									</td>
								<c:if test="${empty workTicket.id}">
								<td align="right" width="" style="!width:168px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
								</c:if>
							
								<c:if test="${not empty workTicket.id}">
								<c:choose>
								<c:when test="${countForTicketNotes == '0' || countForTicketNotes == '' || countForTicketNotes == null}">
								<td align="right" width="" style="!width:168px;"><img id="countForTicketNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=JobTicket&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=JobTicket&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" width="" style="!width:168px;"><img id="countForTicketNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=JobTicket&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=JobTicket&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
							</tr>
								<tr>
									<td align="right" class="listwhitetext">Ticket&nbsp;Status</td>
									
									<td><configByCorp:customDropDown listType="map" list="${tcktactn_isactive}" fieldValue="${workTicket.targetActual}"
		                            attribute="id=workTicketForm_workTicket_targetActual class=list-menu name=workTicket.targetActual  style=width:95px  headerKey='' headerValue='' onchange='changeHandoutAccessss(this),autoPopulate_customerFile_statusDate(this),changeStatus(),return changeTicektStatus(this);'"/></td>
									</td>
									<td align="right" class="listwhitetext">Status&nbsp;Date</td>
									<c:if test="${not empty workTicket.statusDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="workTicket.statusDate" />
										</s:text>
										<td align="left" width="90"><s:textfield cssClass="input-textUpper" id="statusDate" name="workTicket.statusDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  /></td>
									</c:if>
									<c:if test="${empty workTicket.statusDate}">
										<td align="left" width="90"><s:textfield cssClass="input-textUpper" id="statusDate" name="workTicket.statusDate" required="true" cssStyle="width:65px" maxlength="11"  readonly="true" onkeydown="onlyDel(event,this)"  /></td>
									</c:if>
									<configByCorp:fieldVisibility componentId="component.field.Alternative.hse">
									<c:set var="isHSE" value="false"/>
									<c:if test="${workTicket.hse}">
										<c:set var="isHSE" value="true"/>
									</c:if>
									<td align="center"  colspan="2" class="listwhitetext" style="width:90px">HSE
									<s:checkbox cssStyle="vertical-align:middle;margin:0px;margin-right:17px;" id="workTicket.hse"  key="workTicket.hse" value="${isHSE}" fieldValue="true" onclick="" />
									</td>
									</configByCorp:fieldVisibility>
								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='workTicket.date1' /><font color="red" size="2">*</font></td>
									<c:if test="${not empty workTicket.date1}">
									<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="workTicket.date1" /></s:text>
									<td align="left"><s:textfield cssClass="input-text" id="date1" name="workTicket.date1" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11"  readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeServiceCheckingBeginDateVar();"/>
										<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['workTicketForm'].elements['workTicket.date1'].select(); return false;" /></td>
									</c:if>
									 <c:if test="${empty workTicket.date1}">
									<td align="left"><s:textfield cssClass="input-text" id="date1" name="workTicket.date1" required="true" cssStyle="width:65px" maxlength="11"  readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeServiceCheckingBeginDateVar();"  />
										<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['workTicketForm'].elements['workTicket.date1'].select();return false;" /></td>
									</c:if>
									<td align="right" class="listwhitetext"><fmt:message key='workTicket.date2' /><font color="red" size="2">*</font></td>
									 <c:if test="${not empty workTicket.date2}">
									 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="workTicket.date2" /></s:text>
									<td colspan="1" align="left"><s:textfield cssClass="input-text" id="date2"  name="workTicket.date2" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeServiceCheckingEndDateVar();" /> 
										<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['workTicketForm'].elements['workTicket.date2'].select(); return false;" /></td>
									</c:if>
									<c:if test="${empty workTicket.date2}">
									<td colspan="1" align="left"><s:textfield cssClass="input-text" id="date2"  name="workTicket.date2" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeServiceCheckingEndDateVar();" />
										<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['workTicketForm'].elements['workTicket.date2'].select(); return false;" /></td>
									</c:if>
									<td align="right"  colspan="1" class="listwhitetext" style="padding-left:10px;!padding-left:5px;">Weekend/Holiday&nbsp;work
									
									</td>
									<td><s:checkbox cssStyle="vertical-align:middle;margin:0px;"  name="workTicket.weekendFlag" value="${workTicket.weekendFlag}" /></td>									
								<td align="right"  colspan="1" class="listwhitetext" style="padding-left:10px;!padding-left:5px;">3rd&nbsp;Party&nbsp;Required
								</td>
								<td><s:checkbox cssStyle="vertical-align:middle;margin:0px;"  name="workTicket.thirdPartyRequired" value="${workTicket.thirdPartyRequired}" /></td>		
								<c:if test="${serviceOrder.corpID=='CWMS'}">
								<td align="right"  colspan="1" class="listwhitetext" style="padding-left:10px;!padding-left:5px;">Out&nbsp;of&nbsp;Scope</td>
								<td><s:checkbox cssStyle="vertical-align:middle;margin:0px;"  name="workTicket.outOfScope" value="${workTicket.outOfScope}" /></td>	
							  
                                	
                                                                </c:if>								
								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key="workTicket.service" /><font color="red" size="2">*</font></td>
									<td colspan="3"><configByCorp:customDropDown listType="map" list="${tcktservc_isactive}" fieldValue="${workTicket.service}"
		                            attribute="id=workTicketForm_workTicket_service class=list-menu name=workTicket.service  style=width:320px  headerKey='' headerValue=''  onchange='getInstructionCodeValues(),changeStatus(),validService();changeTicektActulizedStatus(),autoPopulateVendorCodeWithName();'"/></td>
									<td colspan="7">
									
									<table class="detailTabLabel" border="0"  style="width:500px; margin-left:62px">
									<tr>
									<td></td>
									<td >
									<div id="hid3" style="margin-left: -29px; width:47px">
								 	<table class="detailTabLabel" border="0" style="margin-bottom: 0px;">
									<tbody>
									<tr>
									<td width="22" align="right" class="listwhitetext"></td>
									<td align="right" class="listwhitetext"><fmt:message key="workTicket.bondedGoods" /></td>
									<td><s:select name="workTicket.bondedGoods" list="%{yesno}" cssStyle="width:66px"  cssClass="list-menu" onchange="changeStatus();" /></td>
									</tr>
									</tbody>
									</table>
									</div>
									</td>
									<td></td>
									<td colspan="3"></td>
									</tr>
									<tr>
									<td>
									
									</td>
									<td align="right" class="listwhitetext" style="width:48px;">Bill&nbsp;Status</td>
									<%if(userRole.equalsIgnoreCase("ROLE_BILLING")){ %>
									<c:if test="${empty workTicket.invoiceNumber || workTicket.invoiceNumber==''}">
									<td align="left"><configByCorp:customDropDown listType="map" list="${billStatus_isactive}" fieldValue="${workTicket.reviewStatus}"
		                            attribute="id=workTicketForm_workTicket_reviewStatus class=list-menu name=workTicket.reviewStatus  style=width:80px  headerKey='' headerValue=''  onchange='changeStatus();'"/></td>
									</c:if>
									<c:if test="${not empty workTicket.invoiceNumber &&  workTicket.invoiceNumber!=''}">
									<td align="left"><s:textfield name="workTicket.reviewStatus" size="7" cssStyle="width:75px;margin-left: 3px;" cssClass="input-text" readonly="true" /></td>
									</c:if>
									<%}else{ %>
									<td align="left"><s:textfield name="workTicket.reviewStatus" size="7" cssStyle="width:75px;margin-left: 3px;" cssClass="input-text" readonly="true" /></td>
									<%} %>
									<td align="right" class="listwhitetext" style="width:161px">Invoice&nbsp;Number:</td>
									<td><s:textfield cssStyle="text-align:right;width: 72px;"  name="workTicket.invoiceNumber" size="13" maxlength="7" cssClass="input-textUpper" readonly="true"  /></td>
									
									<td>
									<div id="hid" style="margin: 0px;">
								 	<table class="detailTabLabel" border="0" style="margin-bottom: 0px;">
									<tbody>
									<tr>
									<td align="right" class="listwhitetext" width="52px">Container&nbsp;#</td>

									<td><s:select name="workTicket.containerNumber" list="%{containerList}" cssStyle="width:55px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"  /></td>
									</tr>
									</tbody>
									</table>
									</div>
									</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Warehouse<font color="red" size="2">*</font></td> 
									<td colspan="3"><configByCorp:customDropDown listType="map" list="${house_isactive}" fieldValue="${workTicket.warehouse}"
		                            attribute="id=workTicketForm_workTicket_warehouse class=list-menu name=workTicket.warehouse  style=width:320px  headerKey='' headerValue=''  onchange='changeStatus(),validService(),autoPopulateVendorCodeWithName();'"/></td>
								   <td align="right" class="listwhitetext" width="105">Company&nbsp;Division</td>
									<td><s:select name="workTicket.companyDivision" list="%{companyDivis}" cssStyle="width:79px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();" /></td>
										<configByCorp:fieldVisibility componentId="component.tab.workTicket.fvpAcvValue">
									 	<td align="right" class="listwhitetext">FVP Value </td>
									<td><s:textfield cssStyle="text-align:right; width:70px" name="workTicket.fvpValue" id="fvpValue" size="12"  cssClass="input-text" maxlength="9" onkeypress="return numbersonly(event)"  /></td>
									
									<td align="right" class="listwhitetext">ACV Value </td>
									<td><s:textfield key="workTicket.acvValue" cssStyle="text-align:right;width:75px;"   id="acvValue" size="8" maxlength="9"  cssClass="input-text" onkeypress="return numbersonly(event)"/></td>
							</configByCorp:fieldVisibility>
								</tr>
								
								<tr>
									<td align="right" class="listwhitetext">Trucks</td>
									<td><s:textfield cssStyle="text-align:right;" id="serviceTruck" name="workTicket.trucks" size="13" maxlength="7" cssClass="input-text" onchange="onlyNumeric(this);"  onselect="serviceCheckingEndDate();"/></td>
								 	<td align="right" class="listwhitetext">Crews</td>
									<td><s:textfield cssStyle="text-align:right; width:63px" name="workTicket.crews" size="12" cssClass="input-text" maxlength="7" onchange="onlyNumeric(this);"  /></td>
									<td align="right" class="listwhitetext">Crates</td>
									<td><s:textfield key="workTicket.crates" cssStyle="text-align:right;width:75px;" required="true" size="8" maxlength="7"  cssClass="input-text" /></td>
									<configByCorp:fieldVisibility componentId="component.capturing.employee.expense">
									<td align="right" class="listwhitetext">Hours</td>
									<c:if test="${empty workTicket.invoiceNumber || workTicket.invoiceNumber==''}">
									<td><s:textfield key="workTicket.hours" cssStyle="text-align:right;width:75px;" required="true" size="8" maxlength="7"  cssClass="input-text" onkeydown="return onlyRateAllowed(event)" onchange="onlyFloat(this)" /></td>
									</c:if>
									<c:if test="${not empty workTicket.invoiceNumber &&  workTicket.invoiceNumber!=''}">
									<td><s:textfield key="workTicket.hours" cssStyle="text-align:right;width:75px;" required="true" size="8" maxlength="7"  cssClass="input-textUpper" readonly="true" onkeydown="return onlyRateAllowed(event)" onchange="onlyFloat(this)" /></td>
									</c:if>
									</configByCorp:fieldVisibility>
							
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Estimated&nbsp;Weight</td>
								<c:if test="${serviceOrder.corpID=='HOLL'}">
									<td class="listblacktextBig">
									<s:textfield cssStyle="text-align:right;" id='estimatedWeightvalue' key="workTicket.estimatedWeight"  required="true" size="13" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);" />
                                   <c:out value="${workTicket.unit1}" /></td>
								</c:if>
								<c:if test="${serviceOrder.corpID!='HOLL'}">
									<td class="listblacktextBig">
									<s:textfield cssStyle="text-align:right;" id='estimatedWeight' key="workTicket.estimatedWeight" required="true" size="13" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);" />
                                   <c:out value="${workTicket.unit1}" /></td>
                                   </c:if>
									<td align="right" class="listwhitetext">Estimated&nbsp;Cartons</td>
									<td><s:textfield cssStyle="text-align:right; width:63px" name="workTicket.estimatedCartoons" required="true" size="12" maxlength="7"  cssClass="input-textUpper" readonly="true" onchange="onlyNumeric(this);" /></td>
									<td align="right" class="listwhitetext">Estimated&nbsp;Volume</td>
									<td width="115px" class="listblacktextBig"><s:textfield id='estimatedCubicFeet' key="workTicket.estimatedCubicFeet" cssStyle="text-align:right;width:75px;" required="true" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);" /><c:out value="${workTicket.unit2}" /></td>
									<td align="right" style="width:115px" class="listwhitetext">Actual&nbsp;Volume</td>
									<td width="130px" colspan="0" class="listblacktextBig"><s:textfield id="actVolume" key="workTicket.actualVolume" cssStyle="text-align:right;width:72px;" required="true" size="8" maxlength="10" cssClass="input-text" onchange="onlyFloat(this);" /><c:out value="${workTicket.unit2}" /></td>
								</tr>
								<tr>
								<td align="right" class="listwhitetext">Actual&nbsp;Weight</td>
											<c:if test="${serviceOrder.corpID=='HOLL'}">
								<td style="width:145px" class="listblacktextBig"><s:textfield id="actWeightvalue" cssStyle="text-align:right;" key="workTicket.actualWeight" required="true" size="13" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);" /><c:out value="${workTicket.unit1}" /></td>
								</c:if>
								<c:if test="${serviceOrder.corpID!='HOLL'}">
								<td style="width:145px" class="listblacktextBig"><s:textfield id="actWeight" cssStyle="text-align:right;" key="workTicket.actualWeight" required="true" size="13" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);"  /><c:out value="${workTicket.unit1}" /></td>
								</c:if>
									<td align="right" class="listwhitetext" width="100">
									<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=workTicket.scanned',this);return false" >
									Scanned&nbsp;Date</a>
									</td>
									<c:if test="${not empty workTicket.scanned}">
									<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
										<s:param name="value" value="workTicket.scanned" />
									</s:text>
									<td align="left" width="150">
									
									<s:textfield cssClass="input-text" id="scanned" name="workTicket.scanned" value="%{customerFileSurveyFormattedValue}" cssStyle="width:63px" maxlength="11" readonly="true"  onkeydown="onlyDel(event,this)" onselect="serviceCheckingBeginDate();"/>
									
										<img id="scanned_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if>
									<c:if test="${empty workTicket.scanned}">
										<td align="left" width="150" style="!width:300px;" colspan="0">
									
										<s:textfield cssClass="input-text" id="scanned" name="workTicket.scanned" required="true" cssStyle="width:63px" maxlength="11" readonly="true"  onkeydown="onlyDel(event,this)" onselect="serviceCheckingBeginDate();"/>
									
										<img id="scanned_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if>
									<td align="right" class="listwhitetext">Estimated&nbsp;Pieces</td>
									<td><s:textfield id='estimatedPieces' key="workTicket.estimatedPieces" cssStyle="text-align:right;width:75px;" required="true" size="8" maxlength="7" cssClass="input-text"  /></td>
									
									</tr>
								<tr>
									<td align="right" class="listwhitetext" >Instruction&nbsp;Code<font color="red" size="2">*</font></td>
									<td colspan="3"><configByCorp:customDropDown listType="map" list="${woinstr_isactive}" fieldValue="${workTicket.instructionCode}"
		                            attribute="id=workTicketForm_workTicket_instructionCode class=list-menu name=workTicket.instructionCode  style=width:320px  headerKey='' headerValue='' onchange='changeStatus(),autoPopulate_instruction(this);'"/></td>
								  <td align="right" class="listwhitetext">Description</td>
									<td colspan="4"><s:textfield key="workTicket.description" cssStyle="text-align:left;width:311px;" maxlength="60" cssClass="input-text"  /></td>
								
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Instructions<font color="red" size="2">*</font></td>
									<td colspan="3"><s:textarea id="instructions" name="workTicket.instructions" cssStyle="width:316px;" rows="4" cssClass="textarea" /></td>
									<td align="right" class="listwhitetext">Changes</td>
									<td colspan="4"><s:textarea id="changes" name="workTicket.changes" cssStyle="width:311px;" rows="4" cssClass="textarea" />	 </td>
                                 
								</tr>
								<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"><tr>
								<td align="right" class="listwhitetext">O&I&nbsp;Overall&nbsp;Project&nbsp;View</td>
								<td align="left"  colspan="3"><s:textarea id="oIOverallProjectview" name="serviceOrder.oIOverallProjectview" cssStyle="width:316px;" rows="4" cssClass="textarea" readonly="true" /></td>	
									<td align="right" class="listwhitetext">Crew&nbsp;and&nbsp;Vehicle&nbsp;Summary</td>
									<td colspan="4"><s:textarea id="crewVehicleSummary" name="workTicket.crewVehicleSummary" cssStyle="width:311px;" rows="4" readonly="true" cssClass="textarea" /></td>
								</tr></c:if>
								<configByCorp:fieldVisibility componentId="component.field.Alternative.showDriverCrewList">
								<tr>
								<td colspan="10" class="listwhitetext"><div style="padding-left:40px;" id="driverRequiredTrue"><b>Drivers:</b>&nbsp;&nbsp;${crewName}</div></td>
								</tr>
								</configByCorp:fieldVisibility>	
								<tr><td></td></tr>
							</tbody>
						</table>


<configByCorp:fieldVisibility componentId="component.field.Alternative.showForVoermanOnly">												
<c:if test="${workTicket.service=='HO'}">					
						<tr>
							<td height="10" width="100%" align="left" >
							
							<div  onClick="javascript:animatedcollapse.toggle('handout')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Hand Out
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>	                        											
	<div id="handout" style="width:100%;">
    <s:set name="storages" value="storages" scope="request" />
	<div id="chkAllButton" style="text-align:left">
		<!--<label for="r1"><b>Check All</b><input type="radio" name="checkAll" id="chk" value="1" onclick="CheckAll(document.workTicketForm.checkH);"/></label>               
		<label for="r2"><b>Uncheck all</b><input type="radio" name="checkAll" id="chk" value="2" onclick="UnCheckAll(document.workTicketForm.checkH);"/></label>-->
	</div>
	<display:table name="storages" class="table" requestURI="" id="storageList" pagesize="10" >				
			    <display:column title="HO" style="width:20px;" >
				<c:if test="${storageList.releaseDate==null}">
					<c:if test="${storageList.handoutStatus=='A'}">
					<div style="text-align:center"><input type="checkbox" id="checkho" name="checkH${storageList.id}" value="${storageList.id}" onclick="HandOut('${storageList.ticket}',this);handOutChkedUnchkedVolumePieces(this,'handOutPieces${storageList.id}','handOutVolume${storageList.id}','${storageList.ticket}');" checked /></div>
					</c:if>
					<c:if test="${storageList.handoutStatus == '' || storageList.handoutStatus == null}">
					<div style="text-align:center"><input type="checkbox" id="checkho" name="checkH${storageList.id}" value="${storageList.id}" onclick="HandOut('${storageList.ticket}',this);handOutChkedUnchkedVolumePieces(this,'handOutPieces${storageList.id}','handOutVolume${storageList.id}','${storageList.ticket}');" /></div>
					</c:if>
				</c:if>
				<c:if test="${storageList.releaseDate!=null}">
					<c:if test="${storageList.handoutStatus=='A'}">
					<div style="text-align:center"><input type="checkbox" id="checkho" name="checkH${storageList.id}" disabled="disabled" value="${storageList.id}" onclick="HandOut('${storageList.ticket}',this);handOutChkedUnchkedVolumePieces(this,'handOutPieces${storageList.id}','handOutVolume${storageList.id}','${storageList.ticket}');" checked /></div>
					</c:if>
					<c:if test="${storageList.handoutStatus == '' || storageList.handoutStatus == null}">
					<div style="text-align:center"><input type="checkbox" id="checkho" name="checkH${storageList.id}" disabled="disabled" value="${storageList.id}" onclick="HandOut('${storageList.ticket}',this);handOutChkedUnchkedVolumePieces(this,'handOutPieces${storageList.id}','handOutVolume${storageList.id}','${storageList.ticket}');" /></div>
					</c:if>
				</c:if>
				</display:column>
				<display:column property="ticket" sortable="true" titleKey="storage.ticket" />
				<display:column property="description" sortable="true" titleKey="storage.description" maxLength="15"/>
		 		<display:column property="locationId" sortable="true" titleKey="storage.locationId" />
				<display:column property="storageId" sortable="true" title="Storage Id" />				
				<display:column property="releaseDate" sortable="true" titleKey="storage.releaseDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
				<display:column property="containerId" sortable="true" titleKey="storage.containerId" />
				<display:column property="itemTag" sortable="true" titleKey="storage.itemTag"  style="width:20px"/>
				<display:column property="model" sortable="true" titleKey="storage.model" style="width:20px" />
				<display:column property="serial" sortable="true" titleKey="storage.serial" style="width:20px" />
				<display:column headerClass="containeralign" titleKey="storage.pieces" style="width:25px; text-align: right"><div align="right"><s:hidden name="handOutPieces${storageList.id}" value="${storageList.pieces}"/>
				<fmt:formatNumber type="number" groupingUsed="true" value="${storageList.pieces}" /></div>
				</display:column>
				<display:column headerClass="containeralign" title="Volume" style="width:25px; text-align: right"><div align="right"><s:hidden name="handOutVolume${storageList.id}" value="${storageList.volume}" />
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageList.volume}" /></div>
				</display:column>			
				<display:column property="volUnit" sortable="true" title="VolUnit" style="width:20px"/>
				<display:footer>
			    <tr> 
			 	  	 <td align="right" colspan="10"><b><div align="right">Total:</div></b></td>
					 <td align="right"><div align="right"><s:textfield name="totalHandoutPieces" readonly="true" cssClass="input-textUpper" cssStyle="width:40px; text-align: right" value="${totalHandoutPieces}"/></div></td>
					 <td align="right"><div align="right"> <s:textfield name="totalHandoutVolume" readonly="true" cssClass="input-textUpper" cssStyle="width:40px; text-align: right" value="${totalHandoutVolume}"/></div></td>	 
			         <td></td> 	    
			     </tr>
               </display:footer>								
				<display:setProperty name="paging.banner.item_name" value="storage" />
				<display:setProperty name="paging.banner.items_name" value="storage" />				
				<display:setProperty name="export.excel.filename" value="Storage List.xls" />
				<display:setProperty name="export.csv.filename" value="Storage List.csv" />
				<display:setProperty name="export.pdf.filename" value="Storage List.pdf" />
	</display:table>
			
	     </div>
		</td>
	</tr>
	</c:if>						
	</configByCorp:fieldVisibility>	

<configByCorp:fieldVisibility componentId="component.workTicket.itemListTab">
    <c:if test="${not empty workTicket.id}">
    <c:if test="${workTicket.service=='HO'}">
                        <tr>
                            <td height="10" width="100%" align="left" >
                            
                            <div  onClick="javascript:animatedcollapse.toggle('handOutForCorex'); findAllHandOut();" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>

<td class="headtab_center" >&nbsp;Hand&nbsp;Out</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;</td>
<td class="headtab_right">
</td></tr></table></div>                                                        
  <div id="handOutForCorex" class="switchgroup1">
 <table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:0px;padding:0px;" >
  <td width="180" class="listwhitelargetext" style="vertical-align: middle;"><input type="checkbox" id="chkforHo" value="1" onclick="CheckAllHoTicket();"/><b> Release All Items</b></td>
  <td class="listwhitelargetext"><b> Item Search</b>&nbsp;
  <select id="itemSearch" style="width:150px; margin:5px 0px " class="list-menu myclass"  onchange="searchHandOutItem(this)" onmouseover="ajax_hideTooltip()">
   <option value="" disabled selected>Select your option</option>
  <c:forEach var="chrms" items="${sortedItemList}" varStatus="loopStatus">
    <option >
    <c:out value="${chrms.value}"></c:out>
    </option>
    </c:forEach>
  </select>
  </td>
   <td align="left" width="5px"></td>
    </tr>   
    </table>    
    <div id="handOutForCorexAjax">
                
                </div>
         </div>
        </td>
    </tr>
    </c:if> 
    </c:if>     
    </configByCorp:fieldVisibility> 


	<configByCorp:fieldVisibility componentId="component.workTicket.itemListTab">
	<c:if test="${not empty workTicket.id}">
	<c:if test="${workTicket.service=='RC'}">
						<tr>
							<td height="10" width="100%" align="left" >
							
							<div  onClick="javascript:animatedcollapse.toggle('itemData'); findAllResource();" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Item&nbsp;List</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;</td>
<td class="headtab_right">
</td></tr></table></div>	                        											
  <div id="itemData" class="switchgroup1">
	
	<div id="resourcMapAjax">
			 	
			 	</div>
	
<%--    <s:set name="dataItemList" value="dataItemList" scope="request" />
	<div id="chkAllButton" style="text-align:left">
		<!--<label for="r1"><b>Check All</b><input type="radio" name="checkAll" id="chk" value="1" onclick="CheckAll(document.workTicketForm.checkH);"/></label>               
		<label for="r2"><b>Uncheck all</b><input type="radio" name="checkAll" id="chk" value="2" onclick="UnCheckAll(document.workTicketForm.checkH);"/></label>-->
	</div>
	 <display:table name="dataItemList" class="table" style="margin:3px 0px 5px 0px" requestURI="" id="dataItemList" pagesize="10" >				
		<display:column title="Item List" style="width:250px;">
		  <a onclick="editDateItem('${dataItemList.id}','${dataItemList.serviceOrderId}','${dataItemList.workTicketId}')">${dataItemList.itemDesc}</a>
		</display:column>	   	
		<display:column title="line" >
		<a onclick="editDateItem('${dataItemList.id}','${dataItemList.serviceOrderId}','${dataItemList.workTicketId}')">${dataItemList.line}</a>
		</display:column>		
		<display:column property="itemUomid" title="Item UomId" style="width:50px;"></display:column>	   	
		<display:column style="width:170px;" title="WareHouse">
		
			<c:forEach var="chrms" items="${house}" varStatus="loopStatus">
			<c:choose>
			<c:when test="${chrms.key == dataItemList.warehouse}">
			<c:set var="selectedInd" value=" selected"></c:set>
			<c:out value="${chrms.value}"></c:out>
			</c:when>
			</c:choose>
    </option>
	</c:forEach> 
		</display:column>	
		<display:column property="quantityReceived" title="Quantity Received" headerClass="containeralign" style="text-align:right;"></display:column>
		<display:column property="qtyExpected" title="Quantity Expected" headerClass="containeralign" style="text-align:right;"></display:column>
		<display:column property="itemQtyReceived" title="Item Quantity Received" headerClass="containeralign" style="text-align:right;"></display:column>
		<display:column property="itemQtyExpected" title="Item Quantity Expected" headerClass="containeralign" style="text-align:right;"></display:column>	
		<display:column property="volume" headerClass="containeralign" style="text-align:right;"></display:column>	
		<display:column property="weight" headerClass="containeralign" style="text-align:right;"></display:column>	   	
		<display:column property="totalWeight" title="Total Weight" headerClass="containeralign" style="text-align:right;"></display:column>
		<display:column property="notes" title="Notes" style="width:200px;"></display:column>	
		 <display:column title="" style="width:5px;">
		<a><img align="middle" onclick="deleteItemData('${dataItemList.id}','${dataItemList.workTicketId}')" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
		</display:column>						
	</display:table> --%>
		<!-- <input type="button" class="cssbutton1" id="addForm" name="addForm" style="width:70px; margin-bottom:5px;" value="Add" onclick="dataListForm();" />	 -->
	     </div>
		</td>
	</tr>
	</c:if>	
	</c:if>		
	</configByCorp:fieldVisibility>	
	<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
	
	<!--  details for driver portal-->
			              <tr>
							<td height="10" width="100%" align="left" >
 <div  onClick="javascript:animatedcollapse.toggle('details') " style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
	
							<div class="dspcont" id="details" style="width:100%;">
							
								<table class="detailTabLabel" cellpadding="2" cellspacing="0" border="0">
								<tr>
				                <c:if test="${workTicket.acceptByDriver ==true}">
								<c:if test="${checkOpsRole}">
							 <td>Accepted<s:checkbox cssStyle="vertical-align:middle;margin:5px;"  id="workTicketForm_workTicket_accepted"  name="workTicket.acceptByDriver" onclick="driveruncheck();"/></td> 
								</c:if>
								<c:if test="${!checkOpsRole}">
								<td>Accepted<s:checkbox cssStyle="vertical-align:middle;margin:5px;"  id="workTicketForm_workTicket_accepted"  name="workTicket.acceptByDriver" disabled="true"/></td> 
								</c:if> 
								</c:if> 
								
								<c:if test="${workTicket.acceptByDriver !=true}"> 
								   <c:if test="${usertype=='DRIVER'}">
                                  <td>Accepted<s:checkbox cssStyle="vertical-align:middle;margin:5px;"  id="workTicketForm_workTicket_accepted"  name="workTicket.acceptByDriver" onclick="acceptByDriver();" /></td> 
                                 </c:if>
                                    <c:if test="${usertype !='DRIVER'}">
                              <td>Accepted<s:checkbox cssStyle="vertical-align:middle;margin:5px;"  id="workTicketForm_workTicket_accepted"  name="workTicket.acceptByDriver" /></td> 
                                 </c:if>
								</c:if> 
								
								<td  class="listwhitetext"><span class="relwcolist1" >Crew Name<font color="red" id="estrik1" style="display:none" size="2">*</font></span>
								<s:textfield cssStyle="padding-left:5px;" name="workTicket.crewName" required="true" size="15" cssClass="input-text"/>
								</td>
								 <td  class="listwhitetext"><span class="relwcolist1" >Project Lead Name<font color="red" id="estrik2" style="display:none" size="2">*</font></span>
								 <s:textfield cssStyle="padding-left:5px" name="workTicket.projectLeadName" required="true" size="15" cssClass="input-text"/>
								 </td>
								 <td class="listwhitetext"><span class="relwcolist1">Project Lead Number<font color="red" id="estrik3" style="display:none" size="2">*</font></span>
								  <s:textfield cssStyle="padding-left:5px" name="workTicket.projectLeadNumber" required="true" size="15" maxlength="20" cssClass="input-text" onkeydown="return onlyFloatNumsAllowed(event)"/>
								 </td>
							</tr>
								</table>
						         
						   </div>
							</td>
								</tr>
								
								  </configByCorp:fieldVisibility>
						<tr>
							<td height="10" width="100%" align="left" >
							
							<div  onClick="javascript:animatedcollapse.toggle('storage')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Storage
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>					
							<div class="dspcont" id="storage" style="width:100%;">
							
								<table class="detailTabLabel" cellpadding="2" cellspacing="0" border="0">
								<tr><td height="5px"></td></tr>
								<tr>
								<td class="listwhitetext" width="85px" align="right"><b>Storage:</b></font></td>
								<td align="right" class="listwhitetext">
									<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=workTicket.stgHow',this);return false" >
									Effect&nbsp;On&nbsp;Hand&nbsp;Storage
									</a>
									</td>
									<%if(userRole.equalsIgnoreCase("ROLE_FINANCE") || userRole.equalsIgnoreCase("ROLE_BILLING")){ %>
									<td class="listwhitetext"><configByCorp:customDropDown listType="map" list="${onhandstorage_isactive}" fieldValue="${workTicket.stgHow}"
		                            attribute="id=workTicketForm_workTicket_stgHow class=list-menu name=workTicket.stgHow  style=width:55px  headerKey='' headerValue='' "/></td>
								       <%}else{ %>
									<td class="listwhitetext"><configByCorp:customDropDown listType="map" list="${onhandstorage_isactive}" fieldValue="${workTicket.stgHow}"
		                            attribute="id=workTicketForm_workTicket_stgHow class=list-menu name=workTicket.stgHow"/></td>
									<%} %>
									<td align="right" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?decorator=simple&popup=true&tableFieldName=workTicket.stgAmount',this);return false" >Quantity</a></td>
									<%if(userRole.equalsIgnoreCase("ROLE_FINANCE") || userRole.equalsIgnoreCase("ROLE_BILLING")){ %>
									<td><s:textfield cssStyle="text-align:right;" name="workTicket.stgAmount" required="true" size="15" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);"  /></td>
									<%}else{ %>
									<td><s:textfield cssStyle="text-align:right;" name="workTicket.stgAmount" cssClass="input-textUpper" size="15" readonly="true"  /></td>
									<%} %>
									<td align="right" class="listwhitetext" width="175px"><fmt:message key="billing.storageMeasurement"/></td>
	  			                   <%if(userRole.equalsIgnoreCase("ROLE_FINANCE") || userRole.equalsIgnoreCase("ROLE_BILLING")){ %>
	  			                   <td align="left"><s:textfield cssClass="input-textUpper" name="workTicket.storageMeasurement" readonly="true" size="25" /></td>
	  								<%}else{ %>
									<td align="left"><s:textfield cssClass="input-textUpper" name="workTicket.storageMeasurement" readonly="true" size="25" /></td>
	  								<%} %>
	  								<td align="right" class="listwhitetext" style="width:119px">Storage&nbsp;Out</td>
	  								<td class="listwhitetext"><configByCorp:customDropDown listType="map" list="${storageOut_isactive}" fieldValue="${workTicket.storageOut}"
		                            attribute="id=workTicketForm_workTicket_storageOut class=list-menu name=workTicket.storageOut  style=width:65px  headerKey='' headerValue='' "/></td>
								  
									<c:if test="${empty workTicket.id}">
								<td align="right" width="110px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							
							<c:if test="${not empty workTicket.id}">
							<c:choose>
								<c:when test="${countTicketStorageNotes == '0' || countTicketStorageNotes == '' || countTicketStorageNotes == null}">
								<td align="right" width="110px" ><img id="countTicketStorageNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketStorage&imageId=countTicketStorageNotesImage&fieldId=countTicketStorageNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketStorage&imageId=countTicketStorageNotesImage&fieldId=countTicketStorageNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" width="110px" ><img id="countTicketStorageNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketStorage&imageId=countTicketStorageNotesImage&fieldId=countTicketStorageNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketStorage&imageId=countTicketStorageNotesImage&fieldId=countTicketStorageNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>	
							</tr>		
							<tr><td height="5px"></td></tr>
						</table>
										</div>
										</td>
									</tr>
				<tr>
										<td height="10" width="100%" align="left" >
										
										<div  onClick="javascript:animatedcollapse.toggle('address')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Address Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
										<div id="address" class="switchgroup1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
											<tbody>
												<tr>
													<c:set var="d2dFlag" value="false" />
													<c:set var="originFlag" value="false" />
													<c:set var="qcFlag" value="false" />
													<c:set var="noFlag" value="false" />
													<c:if test="${workTicket.originDock}">
													<c:set var="d2dFlag" value="true" />
													</c:if>
													<c:if test="${workTicket.originElevator}">
													<c:set var="originFlag" value="true" />
													</c:if>
													<c:if test="${workTicket.qc}">
													<c:set var="qcFlag" value="true" />
													</c:if>
													<c:if test="${workTicket.noCharge}">
													<c:set var="noFlag" value="true" />
													</c:if>
													<c:if test="${workTicket.collectCOD}">
													<c:set var="collectCOD" value="true" />
													</c:if>
	<c:set var="originShuttleTemp" value="false" />
	<c:set var="originParkingPermitTemp" value="false" />
	<c:if test="${workTicket.originShuttle}">
	<c:set var="originShuttleTemp" value="true" />
	</c:if>
	<c:if test="${workTicket.originParkingPermit}">
	<c:set var="originParkingPermitTemp" value="true" />
	</c:if>
													<td valign="top" align="left" class="listwhitetext" width="100%" >
													
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
													<tr>
													<td class="subcontenttabChild" colspan="4" style="width:100%;height:19px;"><font color="black"><b>&nbsp;Origin</b></font></td></tr>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>															
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																
																<td align="left" class="listwhitetext" style=""></td>
																<td align="left" class="listwhitetext-scope"><fmt:message key='customerFile.originCompany'/></td>
																<td align="left" width="120" class="listwhitetext"></td>
																<td align="left" id="addressBookFalse" class="listwhitetext" style="margin-left: 6px;">Address Book</td>
																<td align="left" id="addressBookTrue" class="listwhitetext">Residence Name</td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" cssStyle="width:194px;" name="workTicket.address1" size="35" maxlength="100" onblur="titleCase(this)"/></td>
																<td width="22"><img class="openpopup" id="openpopup11" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left" class="listwhitetext"><s:hidden cssClass="input-text" name="workTicket.originCompany" /></td>
															   	<td align="left" class="listwhitetext"></td>
															   	<td align="left" id="addressBookFalse1"><s:select cssClass="list-menu" cssStyle="width:121px;margin-left:6px;" list="%{originAddress}" headerKey="" headerValue="" onchange="getAddress(this,'origin');" name="addOriginAdd" id="addOriginAdd"  /></td>
																<td align="left" id="addressBookTrue1"><s:select cssClass="list-menu" name="workTicket.originAddressBook" list="%{addressBookList}" cssStyle="width:116px;margin-left:1px;" headerKey="" headerValue="" /></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" width="17" height="20" class="listwhitetext" id="addressBookImg"><img class="openpopup" style="vertical-align: text-bottom;" onclick="findStandardAddressDetail(document.forms['workTicketForm'].elements['workTicket.originAddressBook'].value,this);"  src="<c:url value='/images/plus-small.png'/>" /></td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><font color="gray">Address2</font><br><s:textfield cssClass="input-text upper-case" cssStyle="width:194px;" name="workTicket.address2" size="35" maxlength="100" onblur="titleCase(this)" /></td>
																<td align="left" class="listwhitetext"></td>
																<td class="listwhitetext" align="left" colspan="2">
																
																<table border="0" width="" class="detailTabLabel" cellspacing="0" cellpadding="1">
																<tr>																
																<td class="listwhitetext" colspan="2">&nbsp;</td>
																<!--<td class="listwhitetext" width="70"><div id="hidStatusReg5">Which Floor</div></td>-->
																<td class="listwhitetext" width="70"><div style="float:left;margin-left: 6px;">Elevator</div><div id="originElevatorMandatory"  style="margin: 0px; display:none;"><font color="red" size="2">*</font></div></td>
																<td class="listwhitetext" width="8"></td>
																<td class="listwhitetext" width="70"><div style="float:left;">Parking</div></td>														
																</tr>
																<tr>
																<td width=""><s:checkbox name="workTicket.originDock" cssStyle="margin:0px;" value="${d2dFlag}" fieldValue="true" onclick="changeStatus();"  /></td>
																<td class="listwhitetext" width="30">&nbsp;Dock </td>																
																<td align="right" width="30"><s:select cssClass="list-menu" name="workTicket.originElevator" list="%{elevatorList}" headerKey="" headerValue="" onclick="changeStatus();" cssStyle="width:123px;" /></td> 
																<td class="listwhitetext" width="8"></td>
																<td align="right" width="30"><s:select cssClass="list-menu" name="workTicket.parkingS" list="%{parkingList}" headerKey="" headerValue="" onclick="changeStatus();" cssStyle="width:157px;margin-left:0px;"  /></td>
																<!--<td width="5" >
																<div id="hidStatusReg4"><s:textfield  cssClass="input-text" name="workTicket.whichFloor" size="22"  maxlength="45"  /></div>
																</td>																																												
																--></tr>																
																</table>
																</td>
																<td align="center"></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:0px;" colspan="7"></td></tr>															
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td><font color="gray">Address3</font><br><s:textfield cssClass="input-text upper-case" cssStyle="width:194px;" name="workTicket.address3" size="35" maxlength="100" onblur="titleCase(this)"/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" colspan="3">
																<table border="0" width="" class="detailTabLabel" cellspacing="0" cellpadding="1">
																<tr>
																<td class="listwhitetext"><div style="float:left;">Kind of parking permit</div><div id="originKindofparkingMandatory"  style="margin: 0px; display:none;"><font color="red" size="2">*</font></div></td>
																<c:if test="${visibilityForSSCW}">
																<td class="listwhitetext" ><div style="float:left;padding-left:2px;">Shuttle</div><div id="originShuttleMandatory"  style="margin: 0px; display:none;"><font color="red" size="2">*</font></div></td>
																</c:if>
																<c:if test="${visibilityForSSCW==false}">
																<td class="listwhitetext" colspan="2"></td>
																</c:if>
																<td class="listwhitetext"><div id="hidStatusReg6">How far in meters</div></td>
																
																</tr>															
																<tr>
																<td width=""><s:select cssClass="list-menu" name="workTicket.parking" list="%{parking}" headerKey=""	headerValue=""  cssStyle="width:169px"  />&nbsp;</td>																
																<c:if test="${visibilityForSSCW==false}">
																<td align="left"><s:checkbox name="workTicket.originShuttle" value="${originShuttleTemp}" fieldValue="true" onclick="changeStatus();disable_shuttle_origin();" cssStyle="margin:0 0 0 6px;"  /></td>
																<td class="listwhitetext"><div style="float:left;padding-left:2px;">Shuttle</div></td>
																</c:if>
																<c:if test="${visibilityForSSCW}">
																<td><s:select cssClass="list-menu" name="workTicketOriginShuttle" list="{'Yes','No'}"   value="%{workTicketOriginShuttle}" headerKey="" 	headerValue=""  cssStyle="margin-right: 3px; margin-left: 3px; width: 48px;"  /></td>
																</c:if>
															<td>
																<s:textfield cssClass="input-text" name="workTicket.farInMeters" size="20" cssStyle="width:100px;" maxlength="45"  />&nbsp;
																 </td>
																 <configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">	
																 	<td class="listwhitetext"><div style="float:left;">Long&nbsp;Carry</div><div id="originLongCarryMandatory"  style="margin: 0px; display:none;float:right;"><font color="red" size="2">*</font></div></td>																
															     <td>
															     <s:select cssClass="list-menu" name="workTicket.originLongCarry" list="{'Yes','No'}" headerKey=""	headerValue=""  cssStyle="width:58px"  />
																</td>																
																 </configByCorp:fieldVisibility>														
																</tr>
																</table>
																<td align="left" class="listwhitetext"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" >
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:30px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
												                <td align="left" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" onmouseover ="gettooltip('workTicket.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
																<td width="10px"></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															     <td align="left" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
																<td align="left" id="zipCodeRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originZip' /><font color="red" size="2">*</font></td>
																</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="workTicket.originCountry" id="ocountry" list="%{country}" cssStyle="width:196px" onchange="ostateMandatory();zipCode();changeStatus();getOriginCountryCode(this);getState('OA');autoPopulate_workTicket_originCountry(this);enableStateListOrigin();"  headerKey="" headerValue=""  /></td>
																
																<td width="26"><img id="globe11" src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>
																<td><s:select cssClass="list-menu" name="workTicket.state" id="ostate" list="%{ostates}" value="%{check}" cssStyle="width:170px" onchange="changeStatus();autoPopulate_customerFile_originCityCode(this);" headerKey="" headerValue=""  /></td>
																
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text upper-case" name="workTicket.city" id="originCity" size="26" cssStyle="width:155px;" maxlength="30"  onblur="titleCase(this)" onkeypress="" onkeyup="autoCompleterAjaxCallOriCity();"/></td>
																<td align="left" class="listwhitetext" style="width:10px"><div id="zipCodeList" style="vertical-align:middle;"><a><img class="openpopup" id="navigation8" onclick="findCityStateNotPrimary('OZ', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></td>
																<td width="10px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.zip" size="15" cssStyle="width:91px;" maxlength="10" onchange="findCityStateOfZipCode(this,'OZ');getState('OA');" onkeydown="return onlyAlphaNumericAllowed(event)" /></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																  <configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
										                        <td>
				                                                <a id="myUniqueLinkId" onclick="callPostalCode();" >&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;"><spam >Look-Up Canadian Postal Code</spam></a>
				                                                </td>
                                               				 </configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style=""><fmt:message key='customerFile.originDayPhoneExt'/></td>
																<td align="right" class="listwhitetext" style="width:20px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
																
															</tr>
															<tr>	
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="workTicket.phone" cssStyle="width:146px;" size="25" maxlength="20"   /></td>
																<td width="3px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.phoneExt" cssStyle="width:41px;" size="4" maxlength="10" onchange="onlyNumeric(this);"  /></td>
																<td align="right" class="listwhitetext" style="width:22px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.homePhone" cssStyle="width:166px;" size="29" maxlength="20"   /></td>
																<td align="right" class="listwhitetext" style="width:11px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.originMobile" cssStyle="width:156px;" size="27" maxlength="25"   /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.originFax" cssStyle="width:118px" size="20" maxlength="20"   /></td>
															<td align="left" class="listwhitetext" style="width:5px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td style="width:90px;">&nbsp;</td>
															<td>
															<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">															
															<tr>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext">Contact&nbsp;Name</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext">Contact&nbsp;Phone(W)</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext">Contact&nbsp;Phone(H)</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext"><div style="float:left;">Site</div><div id="originSiteMandatory"  style="margin: 0px; display:none;"><font color="red" size="2">*</font></div></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>																
																<td align="left" class="listwhitetext" width="110px"><div id="hidStatusReg26">Which Floor</div></td>			
																
																<!--<td align="left" class="listwhitetext"><fmt:message key='customerFile.originSkypeId'/></td>
															--></tr>
															<tr>
																<td ><s:textfield cssClass="input-text" name="workTicket.originPreferredContactTime" cssStyle="width:120px;" size="20" maxlength="32"  /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="workTicket.originContactName" cssStyle="width:145px;" size="25" maxlength="32"  /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="workTicket.originContactWeekend" cssStyle="width:130px;" size="21" maxlength="20"   /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="workTicket.originContactPhone" cssStyle="width:121px;" size="21" maxlength="20"   /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																
																<td ><s:select cssClass="list-menu" name="workTicket.siteS" list="%{siteList}" headerKey="" headerValue="" cssStyle="width:120px" onclick="changeStatus();disable_site_origin();"   /></td>
  																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
  																<td width="110px"><div id="hidStatusReg27"><s:textfield cssClass="input-text" name="workTicket.origMeters" cssStyle="width:95px;" maxlength=""  /></div></td>
																<!--<td><s:textfield cssClass="input-text" name="workTicket.originSkypeId" size="20" maxlength="16"  /></td>-->
																<td align="left" class="listwhitetext" style="width:80px"></td>
																<c:if test="${empty workTicket.id}">
																	<td align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																
																<c:if test="${not empty workTicket.id}">
																<c:choose>
																	<c:when test="${countTicketOriginNotes == '0' || countTicketOriginNotes == '' || countTicketOriginNotes == null}">
																	<td align="right"><img id="countTicketOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketOrigin&imageId=countTicketOriginNotesImage&fieldId=countTicketOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketOrigin&imageId=countTicketOriginNotesImage&fieldId=countTicketOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right"><img id="countTicketOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketOrigin&imageId=countTicketOriginNotesImage&fieldId=countTicketOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketOrigin&imageId=countTicketOriginNotesImage&fieldId=countTicketOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>	
															</tr>
															</table>
															</td>
															</tr>
														</tbody>
													</table>
													</td>
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
												<tr>
													<td align="left" class="vertlinedata"></td>
												</tr>
												
												<tr>
													<td valign="top" align="left" class="listwhitetext" >													
												<table cellspacing="0" cellpadding="0" border="0" width="100%" >
													<tr><td class="subcontenttabChild" colspan="4"style="width:100%;height:19px;" ><font color="black"><b>&nbsp;Destination</b></font></td></tr>
													</table>												
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
															<c:set var="dedockFlag" value="false" /> 
															<c:set var="deelevatorFlag" value="false" /> 
															<c:if test="${workTicket.destinationDock}">
																<c:set var="dedockFlag" value="true" />
															</c:if> 
															<c:if test="${workTicket.destinationElevator}">
																<c:set var="deelevatorFlag" value="true" />
															</c:if>
								<c:set var="destinationShuttleTemp" value="false" />
							<c:set var="destinationParkingPermitTemp" value="false" />
							<c:if test="${workTicket.destinationShuttle}">
							<c:set var="destinationShuttleTemp" value="true" />
							</c:if>
							<c:if test="${workTicket.destinationParkingPermit}">
							<c:set var="destinationParkingPermitTemp" value="true" />
							</c:if> 
									<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td align="left" class="listwhitetext" style=""></td>
																<td align="left" class="listwhitetext-scope"><fmt:message key='customerFile.destinationCompany'/></td>
																<td align="left" width="120" class="listwhitetext"></td>
																<td align="left" id="addressBookFalse11" class="listwhitetext" style="margin-left:6px;">Address Book</td>
																<td align="left" id="addressBookTrue11" class="listwhitetext">Residence Name</td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" cssStyle="width:194px;" name="workTicket.destinationAddress1" size="35" maxlength="100"  /></td>
																<td width="22"><img class="openpopup" id="openpopup17" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left" class="listwhitetext"><s:hidden cssClass="input-text" name="workTicket.destinationCompany" /></td>
																<td align="left" class="listwhitetext"></td>
															    <td align="left" id="addressBookFalse111"><s:select cssClass="list-menu" cssStyle="width:121px;margin-left:6px;"  list="%{destinationAddress}" onchange="getAddress(this,'dest');" headerKey="" headerValue=""  name="addDestAdd" id="addDestAdd"  /></td>
																<td align="left" id="addressBookTrue111"><s:select cssClass="list-menu" name="workTicket.destinationAddressBook" list="%{addressBookList}" cssStyle="width:112px;margin-left:1px;" headerKey="" headerValue="" /></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" width="17" height="20" class="listwhitetext" id="addressBookImg1"><img class="openpopup" style="vertical-align: text-bottom;" onclick="findStandardAddressDetail(document.forms['workTicketForm'].elements['workTicket.destinationAddressBook'].value,this);"  src="<c:url value='/images/plus-small.png'/>" /></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:0px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><font color="gray">Address2</font><br><s:textfield cssClass="input-text upper-case" cssStyle="width:194px;" name="workTicket.destinationAddress2" size="35" maxlength="100"  /></td>
																<td align="left" class="listwhitetext"></td>
															
															<td class="listwhitetext" align="left" colspan="2">
																<table border="0" width="" class="detailTabLabel" cellspacing="0" cellpadding="1">
																
																<tr><td colspan="2">&nbsp;</td>
																<td colspan="2"><div style="float:left;">Elevator</div><div id="destinationElevatorMandatory"  style="display:none"><font color="red" size="2">*</font></div></td>
																<td class="listwhitetext" width="40"><div style="float:left;margin-left: 6px;">Parking</div></td><!--
																<td class="listwhitetext"><div id="hidStatusReg11" >Which Floor</div></td>--></tr>
																<tr>
																<td><s:checkbox name="workTicket.destinationDock" cssStyle="margin:0px;"  value="${dedockFlag}" fieldValue="true" onclick="changeStatus();"  /></td>
																<td class="listwhitetext" width="30">&nbsp;Dock </td>
																<td align="right" width="30"><s:select cssClass="list-menu" name="workTicket.destinationElevator" list="%{elevatorList}" headerKey="" headerValue="" onclick="changeStatus();" cssStyle="width:123px"  /></td> 																							 
																<td class="listwhitetext" width="8">  </td>

																<td align="right" width="30"><s:select cssClass="list-menu" name="workTicket.parkingD" list="%{parkingList}" headerKey="" headerValue="" onclick="changeStatus();" cssStyle="width:157px; margin-left: 6px;"  /></td> 																							 
																
																<!--<td width="50">
																<div id="hidStatusReg1" >
																<s:textfield cssClass="input-text" name="workTicket.dest_whichFloor" size="22" maxlength="45" />
																</div>
																</td>
															--></tr>
															</table>
															</td>
															<td align="center">
																<!--<input type="button" class="cssbutton1" value="Populate" style="width:85px; height:25px" />
																--></td>
															</tr>	
															<tr><td class="listwhitetext" style="width:50px; height:0px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td><font color="gray">Address3</font><br><s:textfield cssClass="input-text upper-case" cssStyle="width:194px;" name="workTicket.destinationAddress3" size="35" maxlength="100"  /></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" colspan="3">
																<table border="0" width="" class="detailTabLabel" cellspacing="0" cellpadding="1">
																<tr>
																<td class="listwhitetext"><div style="float:left;">Kind of parking permit</div><div id="destinationKindofparkingMandatory"  style="display:none;"><font color="red" size="2">*</font></div></td>
																<c:if test="${visibilityForSSCW}">
																<td class="listwhitetext"><div style="float:left;padding-left:2px;">Shuttle</div><div id="destinationShuttleMandatory"  style="display:none;"><font color="red" size="2">*</font></div></td>																	
																</c:if>
															    <c:if test="${visibilityForSSCW==false}">
															    <td class="listwhitetext" colspan="2"></td>
														        </c:if>	
																<td class="listwhitetext"> <div id="hidStatusReg22">How far in meters</div></td>																
																</tr>																
																<tr>
																<td width=""><s:select cssClass="list-menu" name="workTicket.dest_park" list="%{parking}" headerKey=""	headerValue=""  cssStyle="width:169px"  />&nbsp;</td>
																<c:if test="${visibilityForSSCW==false}">
																<td align="left" width=""><s:checkbox name="workTicket.destinationShuttle" value="${destinationShuttleTemp}" fieldValue="true" onclick="changeStatus();disable_shuttle();" cssStyle="margin:0 0 0 6px;" /> </td>
																 <td class="listwhitetext"><div style="float:left;padding-left:2px;">Shuttle</div></td>																 
																 </c:if>
																 <c:if test="${visibilityForSSCW}">
																<td><s:select cssClass="list-menu" name="workTicketDestinationShuttle" list="{'Yes','No'}"   value="%{workTicketDestinationShuttle}" headerKey="" 	headerValue=""  cssStyle="margin-right: 3px; margin-left: 3px; width: 48px;"  /></td>
																 </c:if>
																 <td>
																 <s:textfield cssClass="input-text" name="workTicket.dest_farInMeters" cssStyle="width:96px" maxlength="45" />
																 &nbsp;
																 </td>
																 <configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">
																  <td class="listwhitetext"><div style="float:left;">Long&nbsp;Carry</div><div id="destinationLongCarryMandatory"  style="display:none;float:right;"><font color="red" size="2">*</font></div></td>																 
																 <td>
																 <s:select cssClass="list-menu" name="workTicket.destinationLongCarry" list="{'Yes','No'}" headerKey=""	headerValue=""  cssStyle="width:58px"  />
																 </td>
																 </configByCorp:fieldVisibility>
																</tr>
																</table>
																</td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" >
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
												                <td align="left" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															    <td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
																</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="workTicket.destinationCountry" id="dcountry" list="%{country}" cssStyle="width:196px" onchange="dstateMandatory();getDestinationCountryCode(this);changeStatus();getState('DA');autoPopulate_workTicket_destinationCountry(this);enableStateListDestin();"  headerKey="" headerValue=""  /></td>
																<td width="26"><img id="globe125" src="${pageContext.request.contextPath}/images/globe.png" onclick="openDestinationLocation();"/></td>
																<td><s:select cssClass="list-menu" name="workTicket.destinationState" id="destinationState" list="%{dstates}" value="%{check1}" cssStyle="width:169px" onchange="changeStatus();" headerKey="" headerValue=""  /></td>
																
																<td align="left" class="listwhitetext" style="width:11px"></td>
																<td><s:textfield cssClass="input-text upper-case" name="workTicket.destinationCity" id="destinationCity" cssStyle="width:155px" maxlength="30" onblur="titleCase(this)" onkeypress=""  onkeyup="autoCompleterAjaxCallDestCity();" /></td>
																<td align="left"><div id="zipDestCodeList" style="vertical-align:middle;"><a><img id="navigation9" class="openpopup" onclick="findCityStateNotPrimary('DZ', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></div></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.destinationZip" cssStyle="width:91px" maxlength="10" onchange="findCityStateOfZipCode(this,'DZ');getState('DA')" onkeydown="return onlyAlphaNumericAllowed(event)"  /></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															  <configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
															  <td>
                                                				<a id="myUniqueLinkId" onclick="callPostalCode();" >&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;"><spam >Look-Up Canadian Postal Code</spam></a>
                                                				</td>
                                                				</configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" >
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style=""><fmt:message key='customerFile.destinationDayPhoneExt'/>&nbsp;</td>
																<td align="right" class="listwhitetext" style="width:20px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
														</tr>
															<tr>	
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="workTicket.destinationPhone" cssStyle="width:146px;" size="25" maxlength="20"   /></td>
																<td width="5px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.destinationPhoneExt" cssStyle="width:38px;" size="4" maxlength="10" onchange="onlyNumeric(this);"  /></td>
																<td align="right" class="listwhitetext" style="width:26px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.destinationHomePhone" cssStyle="width:165px;" size="29" maxlength="20" onchange="copyHomePhoneToContact()" /></td>
																<td align="right" class="listwhitetext" style="width:11px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.destinationMobile" cssStyle="width:154px;" size="27" maxlength="25"   /></td>
															    <td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="workTicket.destinationFax" cssStyle="width:119px" maxlength="20"   /></td>
															</tr>
														</tbody>
													</table>
													<c:if test='${serviceOrder.mode == "Overland" || serviceOrder.mode == "Truck"}'>
													<div id="map" style="width:0px; height:0px; display:none"></div>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
														<tr>
														<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
														</tr>
															<tr>
															<td align="right" class="listwhitetext" style="width:90px"></td>
															<td align="right" class="listwhitetext" style="width:60px"></td>
																	<td align="left" class="listwhitetext" style="width:10px"></td>										
																	<td align="left" class="listwhitetext" style="width:10px">Distance&nbsp;(Est.)</td>
																	<td align="left" class="listwhitetext" style="width:10px"></td>	
																	<td align="left" class="listwhitetext">Unit</td>									
																	</tr>
															<tr>
															<td align="right" class="listwhitetext"></td>
																	<td  align="left" class="listwhitetext">
																	<input type="button" name="Calc Distance" value="Calc Distance" class="cssbuttonB" onclick="calculateDistance();"  /></td>
																	<td align="right" class="listwhitetext"></td>
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:100px;"
																	name="workTicket.distance" size="15"
																	maxlength="10" required="true"  /></td>
																	<td align="left" class="listwhitetext" style="width:33px"></td>	
							                                       <td align="left" class="listwhitetext"><s:select cssClass="list-menu" cssStyle="width: 57px;" name="workTicket.distanceInKmMiles" list="{'KM','Mile'}" headerKey="" headerValue=""  /></td>	
															</tr>
														</tbody>
													</table>
													</c:if>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td style="width:90px;">&nbsp;</td>
															<td>
															<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">															
															<tr>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext">Contact&nbsp;Name</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext">Contact&nbsp;Phone(W)</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext">Contact&nbsp;Phone(H)</td>
																<td align="left" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td align="left" class="listwhitetext"><div style="float:left;">Site</div><div id="destinationSiteMandatory"  style="display:none"><font color="red" size="2">*</font></div></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>																
																<td align="left" class="listwhitetext" width="110px"><div id="hidStatusReg24">Which Floor</div></td>
																<!--<td align="left" class="listwhitetext"><fmt:message key='customerFile.originSkypeId'/></td>
															--></tr>
															<tr>
																<td ><s:textfield cssClass="input-text" name="workTicket.destPreferredContactTime" cssStyle="width:120px;" size="20" maxlength="32"  /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="workTicket.contactName" cssStyle="width:145px;" size="25" maxlength="32"  /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="workTicket.contactPhone" cssStyle="width:130px;" size="21" maxlength="20"   /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="workTicket.contactFax" cssStyle="width:121px;" size="21" maxlength="20"   /></td>
																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
																<td ><s:select cssClass="list-menu" cssStyle="width:122px" name="workTicket.siteD" list="%{siteList}" headerKey="" headerValue="" onclick="changeStatus();disable_site_destination();"   /></td>
  																<td align="right" class="listwhitetext" style="width:10px">&nbsp;</td>
  																<td width="110px"><div id="hidStatusReg25"><s:textfield cssClass="input-text" name="workTicket.destMeters" cssStyle="width:95px;" maxlength="105"  /></div></td>
																														
																<td ><!--<s:textfield cssClass="input-text" name="workTicket.destinationSkypeId" size="20" maxlength="16"  />--></td>
																<td align="left" class="listwhitetext" style="width:80px"></td>
																<c:if test="${empty workTicket.id}">
																	<td align="right" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																
																<c:if test="${not empty workTicket.id}">
																<c:choose>
																	<c:when test="${countTicketDestinationNotes == '0' || countTicketDestinationNotes == '' || countTicketDestinationNotes == null}">
																	<td align="right"><img id="countTicketDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketDestination&imageId=countTicketDestinationNotesImage&fieldId=countTicketDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketDestination&imageId=countTicketDestinationNotesImage&fieldId=countTicketDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" ><img id="countTicketDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketDestination&imageId=countTicketDestinationNotesImage&fieldId=countTicketDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketDestination&imageId=countTicketDestinationNotesImage&fieldId=countTicketDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
															</td>
															</tr>
															</table>
															</td>
															</tr>		
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
										</div>
										</td>
									</tr>	
									<tr>
									<td height="10" width="100%" align="left" >
									<div  onClick="javascript:animatedcollapse.toggle('confirmation'),trap();timeRequirementDetails(); " style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Confirmation/Scheduling
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>												
							<div class="dspcont" id="confirmation">
							<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0">
							<c:if test="${empty workTicket.id}">
								<c:set var="reqdAm" value="true" />
								<c:set var="reqdPm" value="true" />
							</c:if>
							<c:if test="${not empty workTicket.id}">
								<c:set var="reqdAm" value="false" />
								<c:set var="reqdPm" value="false" />
								<c:if test="${workTicket.resourceReqdAm}">
									<c:set var="reqdAm" value="true" />
								</c:if>
								<c:if test="${workTicket.resourceReqdPm}">
									<c:set var="reqdPm" value="true" />
								</c:if>
							</c:if>
							<tbody>
								<tr><td align="left" height="5px"></td></tr>
								<tr>	
								    <td colspan="4" class="listwhite"></td>										
									<%-- <td align="right" class="listwhitetext" width="90px">Estimated Hours</td>
									<td colspan="2"><s:textfield name="workTicket.estimatedHours" size="12" cssStyle="text-align:right;" maxlength="7" onchange="onlyFloat(this);" cssClass="input-text"  /></td> --%>
									<td class="listwhite"></td>
									<td class="listwhitetext" align="right" colspan="2">Departure&nbsp;Time</td>									
									<td class="listwhitetext" align="right" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;Arrival&nbsp;Time</td>									
									<td class="listwhitetext" align="center" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;Travel&nbsp;Time</td>
								</tr>
								 <tr>
								   <td align="right" class="listwhite" width="80">AM</td>		
								   <td align="left" class="listwhite"><s:checkbox name="workTicket.resourceReqdAm" value="${reqdAm}" onclick="changeStatus();"  /></td>
									<td align="right" class="listwhitetext">Scheduled1&nbsp;Date</td>
									<c:if test="${not empty workTicket.scheduled1}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="workTicket.scheduled1" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="scheduled1" name="workTicket.scheduled1" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"/></td>
										<td><img id="scheduled1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if>
									<c:if test="${empty workTicket.scheduled1}">
										<td align="left"><s:textfield cssClass="input-text"
											id="scheduled1" name="workTicket.scheduled1" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"/></td><td><img id="scheduled1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>

									<td class="listwhitetext" align="right" width="70px">@Site</td>
									<td><s:textfield cssClass="input-text" cssStyle="text-align:right" name="workTicket.timeFrom" id="time5" size="8" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange="completeTimeString();assigntimeairthmaticNew();return IsTimeValid();"  /></td><td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
									<td><s:textfield cssClass="input-text" cssStyle="text-align:right" name="workTicket.scheduledArrive" id="time1" size="8" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange="completeTimeString();assigntimeairthmaticNew();return IsTimeValid();"  /></td><td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
									<td class="listwhitetext" align="center">(To)</td> 
									<td width="150"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="workTicket.beginningTime" id="time3" size="8" maxlength="5" readonly="true" onchange="completeTimeString();return IsTimeValid();"  /> <img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
 								</tr>
								<tr>
								   <td align="right" class="listwhite">PM</td>		
								   <td align="left" class="listwhite"><s:checkbox name="workTicket.resourceReqdPm" value="${reqdPm}" onclick="changeStatus();"   /></td>	
								   <td align="right" class="listwhitetext">Scheduled2&nbsp;Date</td>

									<c:if test="${not empty workTicket.scheduled2}">
										<s:text id="customerFileSurveyFormattedValue"
											name="${FormDateValue}">
											<s:param name="value" value="workTicket.scheduled2" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text"
											id="scheduled2" name="workTicket.scheduled2"
											value="%{customerFileSurveyFormattedValue}"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"/></td><td><img id="scheduled2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>
									<c:if test="${empty workTicket.scheduled2}">
										<td align="left"><s:textfield cssClass="input-text"
											id="scheduled2" name="workTicket.scheduled2" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"/></td><td><img id="scheduled2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>
									<td class="listwhitetext"  align="right">@Yard</td>
									<td><s:textfield cssClass="input-text" cssStyle="text-align:right"
										name="workTicket.timeTo" id="time6" size="8" maxlength="5"
										onkeydown="return onlyTimeFormatAllowed(event)"
										onchange="completeTimeString();assigntimeairthmaticNew12();return IsTimeValid();"  /></td><td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
									<td><s:textfield cssClass="input-text" cssStyle="text-align:right"
										name="workTicket.leaveWareHouse" id="time2" size="8"
										maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)"
										onchange="completeTimeString();assigntimeairthmaticNew12();return IsTimeValid();"  /></td><td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
									<td class="listwhitetext" align="center" width="50px">(From)</td>

									<td><s:textfield cssClass="input-text" cssStyle="text-align:right"
										name="workTicket.endTime" id="time4" size="8" maxlength="5"
										readonly="true"
										onchange="completeTimeString();return IsTimeValid();"
										onkeydown="return onlyTimeFormatAllowed(event)"  /> <img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext"></td>
									<s:hidden name="carrier1" />
									<td><s:hidden name="workTicket.carrier"
										cssClass="input-text" /></td>
									<td align="left" class="listwhite"></td>
									<td></td>


									<td align="left" class="listwhite"></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2" class="listwhite"></td>		
									<td align="right" class="listwhitetext"><fmt:message
										key='workTicket.confirmDate' /></td>
									<c:if test="${not empty workTicket.confirmDate}">
										<s:text id="customerFileSurveyFormattedValue"
											name="${FormDateValue}">
											<s:param name="value" value="workTicket.confirmDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text"
											id="confirmDate" name="workTicket.confirmDate"
											value="%{customerFileSurveyFormattedValue}"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"/></td><td><img id="confirmDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>
									<c:if test="${empty workTicket.confirmDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="confirmDate" name="workTicket.confirmDate"
											required="true" cssStyle="width:65px" maxlength="11"
											readonly="true" onkeydown="onlyDel(event,this)"/></td><td><img id="confirmDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>
									<td align="right" class="listwhitetext">Confirmed</td>
									<td colspan="6"><s:select name="workTicket.confirm" list="%{confirm}"
										cssStyle="width:175px" cssClass="list-menu" headerKey=""
										headerValue="" onchange="changeStatus();"  /></td>
									<td align="left" class="listwhite"></td>
									<td align="left" class="listwhite"></td>

							<c:if test="${empty workTicket.id}">
								<td align="right" width="224px" style="!width:224px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							
							<c:if test="${not empty workTicket.id}">
							<c:choose>
								<c:when test="${countTicketSchedulingNotes == '0' || countTicketSchedulingNotes == '' || countTicketSchedulingNotes == null}">
								<td align="right" width="224px" style="!width:224px;" ><img id="countTicketSchedulingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketScheduling&imageId=countTicketSchedulingNotesImage&fieldId=countTicketSchedulingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketScheduling&imageId=countTicketSchedulingNotesImage&fieldId=countTicketSchedulingNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" width="224px" style="!width:224px;" ><img id="countTicketSchedulingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketScheduling&imageId=countTicketSchedulingNotesImage&fieldId=countTicketSchedulingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketScheduling&imageId=countTicketSchedulingNotesImage&fieldId=countTicketSchedulingNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>								
								</tr>
								<tr>
								<td></td>							
								<td colspan="20">
								 <configByCorp:fieldVisibility componentId="component.field.LongCarry.ShowForSSCW">	
								<div id="timeRequirementDetails">
								<table class="table"  id="dataTableO" style="margin:0px;margin-bottom:5px; " >
								<thead>
								<td class="listwhitetext" style="padding:0.2em;" >Time Requirements</td><td class="listwhitetext" style="padding:0.2em;">Start Time&nbsp;<img width="40" height="20" align="top" style="cursor:default;vertical-align:middle;" src="${pageContext.request.contextPath}/images/time.png"></td><td class="listwhitetext" style="padding:0.2em;">End Time&nbsp;<img width="40" height="20" align="top" style="cursor:default;vertical-align:middle;" src="${pageContext.request.contextPath}/images/time.png"></td><td class="listwhitetext" style="padding:0.2em;">Add&nbsp;Line</td><td class="listwhitetext" style="padding:0.2em;">Remove</td></tr>
								</thead>
								<tbody>
								<c:if test="${not empty timeRequirementList}">
								<c:forEach var="timeRequirementItem" items="${timeRequirementList}" >	
								<tr>								
								<td style="width:272px"><s:textfield cssClass="input-text" name="timeRequirementsList" value="${timeRequirementItem.timeRequirements}"  id="Req${timeRequirementItem.id}"  size="36" maxlength="60" /></td>
								<td><s:textfield cssClass="input-text" name="startTimeList" value="${timeRequirementItem.startTime}"  id="ST${timeRequirementItem.id}" onchange="return IsTimeValidFortimeRequirement('Req${timeRequirementItem.id}')" cssStyle="text-align:right"  size="12" maxlength="5"   /></td>
								<td><s:textfield name="endtimeList" cssClass="input-text" value="${timeRequirementItem.endtime}"  cssStyle="text-align:right" id="ET${timeRequirementItem.id}" onchange="return IsTimeValidFortimeRequirement('Req${timeRequirementItem.id}')" size="12" maxlength="5"  /></td>
								<td></td>
							    <td><a><img align="middle" onclick="timeRequirementRemove('${timeRequirementItem.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/deRecycle.png"/></a></td>
								</tr>
								</c:forEach>
								</c:if>							
								</tbody>
								</table>
								</div>
								</configByCorp:fieldVisibility>
								<!--	
								 <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:70px; height:25px;" value="Add Line" onclick="addRowOrigin('dataTableO');" />
								--></td>
                                </tr>					
<tr><td height="5px"></td></tr>
							</tbody>
						</table>
						</div>
						</td>
						<td valign="top" align="left" class="listwhite" />
						<td align="left" class="listwhite" />
					</tr>
<tr>
<td height="10" width="100%" align="left" >

	<div  onClick="javascript:animatedcollapse.toggle('billing')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Billing
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>																
<div class="dspcont" id="billing">
<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr><td align="left" height="5px"></td></tr>
<tr>
<td width="90px"></td>
		<td align="left" class="listwhitetext">Code</td>
		<td width="10px"></td>
		<td align="left" class="listwhitetext">Name</td>
		<td width="13px"></td>
		<td align="left" class="listwhitetext">Contract</td>
		<td width="10px"></td>
		<td align="left" class="listwhitetext">Pay Method</td>
</tr>
	<tr>
		<td align="right" class="listwhitetext" width="90px">Bill To:</td>
		<td><s:textfield name="workTicket.account" cssStyle="width:65px;" required="true" cssClass="input-textUpper"  readonly="true" onchange = "checkVendorName();"  /></td>
		<td align="right" class="listwhitetext"></td>
		<td><s:textfield name="workTicket.accountName" cssStyle="width:190px;" required="true" cssClass="input-textUpper"  readonly="true" onchange = "checkVendorName();" /></td>
		<td align="right" class="listwhitetext"></td>
		<td><s:textfield name="workTicket.contract" cssStyle="width:152px;" readonly="true" cssClass="input-textUpper"  /></td>
		<td align="right" class="listwhitetext"></td>
		<td><s:textfield name="workTicket.payMethod" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper"  /></td>
	</tr>
</tr>		
</tbody>
</table>		
<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr><td align="left" height="5px"></td></tr>
<tr>
<td style="width:90px"></td>
<td align="left" class="listwhitetext" style="width:90px;" colspan="3"><fmt:message key="billing.billToAuthority"/></td>

<td align="right" colspan="2" class="listwhitetext"><fmt:message key='billing.authUpdated'/></td>
</tr>
<tr>
<td align="right" class="listwhitetext" style="width:90px"></td>
<td align="left" colspan="3" ><s:textfield name="billToAuthority" value="${billToAuthority}" onfocus="check();" onchange="authUpdatedDate();" cssStyle="width:269px;" required="true" cssClass="input-text" maxlength="35" /></td>

  <td colspan="3">
  <table style="margin:0px;padding:0px;">
  <tr>
  <td align="left" width="83">
		<div id="hidAuthor" style="margin:0 0 0 7px; float:left; display:inline;">				
			<input type="button" class="cssbutton" onclick= "return checkBillToAuthority();" value="<fmt:message key="button.authorization"/>" style="height:22px" />
		</div>
  </td>  
   <c:if test="${not empty billing.authUpdated}">
	 <s:text id="billingAuthUpdatedFormattedValue" name="${FormDateValue}"><s:param name="value" value="billing.authUpdated" /></s:text>
	 <td align="left" width=""><s:textfield cssClass="input-textUpper" name="authUpdated" value="%{billingAuthUpdatedFormattedValue}" cssStyle="width:72px;" maxlength="11" readonly="true"  /></td>
  </c:if>
   <c:if test="${empty billing.authUpdated}">
     <td align="left" width=""><s:textfield cssClass="input-textUpper" name="authUpdated" cssStyle="width:72px;" maxlength="11" readonly="true"  /></td>
  </c:if>
  </tr>
  </table>
  </td>
</tr>
<tr>
<td style="width:90px"></td>
<td align="left" class="listwhitetext" style="width:90px">Actual Revenue&nbsp;${currencySign }</td>
<td width="10px"></td>
<td align="left" class="listwhitetext" style="width:115px">Extra&nbsp;Payroll</td>
<td width="10px"></td>
<td align="left" class="listwhitetext" style="width:115px">Crew&nbsp;Payroll&nbsp;Revenue&nbsp;${currencySign }</td>
<td width="12px"></td>
<td align="left" class="listwhitetext" style="width:150px">COD&nbsp;Collect</td>
</tr>
	<tr>
	<td align="right" class="listwhitetext" style="width:90px"></td>
	<td><s:textfield cssStyle="text-align:right;width:98px;" name="workTicket.revenue"  maxlength="7" cssClass="input-text" onchange="onlyFloat(this);" /></td>
	<td align="right" class="listwhitetext"></td>
	<td><s:textfield cssStyle="text-align:right;width:157px;" name="workTicket.extraPayroll" size="25" maxlength="7" cssClass="input-text" onchange="onlyFloat(this);"  /></td>
	<td align="right" class="listwhitetext" ></td>
	<td><s:textfield cssStyle="text-align:right;width:154px;" name="workTicket.crewRevenue" size="25" maxlength="7" cssClass="input-textUpper" onchange="onlyFloat(this);"  readonly="true" /></td>
	<td></td>
	<td align="left" class="listwhite" style="width:115px"><s:checkbox name="workTicket.collectCOD" value="${collectCOD}" fieldValue="true" onclick="changeStatus();"  /></td>
	</tr>
	<tr><td align="left" height="5px"></td></tr>
	<tr>
	<td width="90px" align="left" class="listwhite"></td>
	<td align="left" class="listwhitetext">No Charge</td>
	<td width="10px"></td>
	<td align="left" class="listwhitetext">Authorized&nbsp;By</td>
	<td width="10px"></td>
	<td align="left" class="listwhitetext" >Approved&nbsp;Date</td>
	</tr>
	<tr>
		<td width="90px" align="left" class="listwhite"></td>
		<td align="left" class="listwhite"><s:checkbox name="workTicket.noCharge" value="${noFlag}" fieldValue="true" onclick="changeStatus();"  /></td>
		<td align="left" class="listwhitetext"></td>
		<td align="left">
			<s:select cssClass="list-menu" name="workTicket.approvedBy" list="%{exec}"  cssStyle="width:161px" headerKey="" headerValue="" onchange="assignDateApproved();"  />
		</td>
	
		<td align="right" class="listwhitetext" ></td>
	<c:if test="${not empty workTicket.approvedDate}">
		<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="workTicket.approvedDate" /></s:text>
		<td align="left"><s:textfield cssClass="input-text"id="approvedDate" name="workTicket.approvedDate" value="%{customerFileSurveyFormattedValue}"	cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"/><img id="approvedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
		</td>
	</c:if>
									<c:if test="${empty workTicket.approvedDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="approvedDate" name="workTicket.approvedDate"
											required="true" cssStyle="width:65px" maxlength="11"
											readonly="true" onkeydown="onlyDel(event,this)"/><img id="approvedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>

							<td width="10px"></td>
							<c:if test="${empty workTicket.id}">
								<td align="right" width="500px" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							
							<c:if test="${not empty workTicket.id}">
							<c:choose>
								<c:when test="${countTicketBillingNotes == '0' || countTicketBillingNotes == '' || countTicketBillingNotes == null}">
								<td align="right" width="500px" ><img id="countTicketBillingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketBilling&imageId=countTicketBillingNotesImage&fieldId=countTicketBillingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketBilling&imageId=countTicketBillingNotesImage&fieldId=countTicketBillingNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" width="500px" ><img id="countTicketBillingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketBilling&imageId=countTicketBillingNotesImage&fieldId=countTicketBillingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketBilling&imageId=countTicketBillingNotesImage&fieldId=countTicketBillingNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
								</tr>
<tr><td height="5px"></td></tr>

							</tbody>
						</table>
						</div>
						</td>
						<td valign="top" align="left" class="listwhite" />
						<td align="left" class="listwhite" />
					</tr>
					<tr>
<td height="10" width="100%" align="left" >

<div  onClick="javascript:animatedcollapse.toggle('staff')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Staff/QC
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>												
<div class="dspcont" id="staff">
<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0" width="100%">
<tbody>
<tr><td align="left" height="5px"></td></tr>
<tr>			
								<tr>
									<td class="listwhitetext" align="right" >Salesman</td>
									<td width="140px"><s:textfield name="workTicket.salesMan"
										readonly="true" cssClass="input-text" cssStyle="width:141px"  /></td>
									<td class="listwhitetext" align="right" style="width:90px">Cancelled&nbsp;By</td>
									<td width="120px"><s:textfield name="workTicket.whoCancelled" cssStyle="width:120px"
										maxlength="20" cssClass="input-text" onkeydown="return onlyCharsAllowed(event)"  /></td>
									<td class="listwhitetext" align="right" style="width:60px">Estimator</td>
									<td><s:select cssClass="list-menu" name="workTicket.estimator" list="%{sale}" cssStyle="width:145px" headerKey=""  headerValue="" onchange="changeStatus();"/></td>

								</tr>
								<tr>
									<td class="listwhitetext" align="right" width="7%">Coordinator</td>
									<td><s:select cssClass="list-menu" name="workTicket.coordinator" list="%{coord}" cssStyle="width:145px" headerKey="" headerValue="" onchange="changeStatus();"  /></td>
									<td align="right" class="listwhitetext">Authorized&nbsp;By</td>
									<td><s:textfield name="workTicket.orderBy" 
										 maxlength="28" cssStyle="width:120px" cssClass="input-text" onkeydown="return onlyCharsAllowed(event)" /></td>
									<td class="listwhite" align="left"></td>
									<td></td>
								</tr>
								<tr><td height="5px"></td></tr>
								<tr>
									<td class="subcontenttabChild" colspan="8" align="left"><font color="#000"><b>QC&nbsp;Detail</b></font></td>
								</tr>
								<tr><td height="5px"></td></tr>
								<tr><td colspan="8" align="left">
								<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td align="right" width="105px"><s:checkbox name="workTicket.qc"
										value="${qcFlag}" fieldValue="true" onclick="changeStatus();" /></td>
									<td align="left" class="listwhitetext">QC&nbsp;Required</td>
									<td align="right" class="listwhitetext" style="width:150px"><fmt:message
										key='workTicket.qcId' /></td>
									<td><s:select name="workTicket.qcId"
										list="%{qc}"  cssStyle="width:124px"
										cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext" width="295"></td>	
							<td></td>			
							<c:if test="${empty workTicket.id}">
								<td align="right" width="266px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							
							<c:if test="${not empty workTicket.id}">
							<c:choose>
								<c:when test="${countTicketStaffNotes == '0' || countTicketStaffNotes == '' || countTicketStaffNotes == null}">
								<td align="right" width="266px"><img id="countTicketStaffNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketStaff&imageId=countTicketStaffNotesImage&fieldId=countTicketStaffNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketStaff&imageId=countTicketStaffNotesImage&fieldId=countTicketStaffNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" width="266px"><img id="countTicketStaffNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket}&noteFor=WorkTicket&subType=TicketStaff&imageId=countTicketStaffNotesImage&fieldId=countTicketStaffNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${workTicket.id }&notesId=${workTicket.ticket }&noteFor=WorkTicket&subType=TicketStaff&imageId=countTicketStaffNotesImage&fieldId=countTicketStaffNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
								</tr>
								</table>
								</td>
								</tr>
<tr><td height="5px"></td></tr>

							</tbody>
						</table>
						</div>
						</td>						
					</tr>						
				</td>
			</tr>
		</tbody>
	</table>
</td></tr>
<tr height="15px"></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

	</div>
	
	<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='workTicket.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="workTicketCreatedOnFormattedValue" value="${workTicket.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="workTicket.createdOn" value="${workTicketCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${workTicket.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='workTicket.createdBy' /></b></td>
							<c:if test="${not empty workTicket.id}">
								<s:hidden name="workTicket.createdBy"/>
								<td style="width:120px"><s:label name="createdBy" value="%{workTicket.createdBy}"/></td>
							</c:if>
							<c:if test="${empty workTicket.id}">
								<s:hidden name="workTicket.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='workTicket.updatedOn'/></b></td>
							<fmt:formatDate var="workTicketupdatedOnFormattedValue" value="${workTicket.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="workTicket.updatedOn" value="${workTicketupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${workTicket.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='workTicket.updatedBy' /></b></td>
							<c:if test="${not empty workTicket.id}">
								<s:hidden name="workTicket.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{workTicket.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty workTicket.id}">
								<s:hidden name="workTicket.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
						</tr>
					</tbody>
				</table>
	<div style="width: 750px; float: left;">
	<c:if test="${usertype!='DRIVER'}">
	<input type="button" class="cssbutton" style="width:55px; height:25px;float:left;"
		value="<fmt:message key="button.save"/>" onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');completeTimeString();IsValidTime('save');" onclick="return checkExceedLimit();" />
	 	<c:if test="${not empty workTicket.id}">
			<c:if test='${serviceOrder.status != "CNCL" }'>
				<c:if test='${billingCheckFlag == "Y" }'>
				<c:choose>					
					<c:when test="${(serviceOrder.coordinator != '' && serviceOrder.coordinator != NULL) && (billing.personBilling != '' && billing.personBilling != NULL)}">
						<input type="button" class="cssbutton" style="width:55px; height:25px;float:left;margin-left:5px;" onclick = "return findBillCompleteDate();" value="<fmt:message key="button.add"/>" />
					</c:when>
					<c:otherwise>
					<input type="button" class="cssbuttonA" style="width:55px; height:25px;float:left;margin-left:5px;" onclick="javascript:alert('Coordinator/ Billing Person missing for this Order. Please enter to create the Work Ticket.')" value="Add" />
					</c:otherwise>
					</c:choose>
					</c:if>
					<c:if test='${billingCheckFlag != "Y" }'>
        			<input type="button" class="cssbutton" style="width:55px; height:25px;float:left;margin-left:5px;" onclick = "return findBillCompleteDate();" value="<fmt:message key="button.add"/>" />
    				</c:if>
								
			</c:if>
			<c:if test='${serviceOrder.status == "CNCL"}'>
		     		<input type="button" class="cssbuttonA" style="width:60px; height:28px;float:left;margin-left:5px;" onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before add ticket.')" value="Add" />
		   </c:if>  
	</c:if>
	<c:if test="${not empty workTicket.id}">	
	<s:reset type="button" cssClass="cssbutton" key="Reset" onclick="newFunctionForCountryState();resetMandatory();"	cssStyle="width:55px; height:25px;float:left;margin-left:5px;" />
	</c:if>
	<c:if test="${empty workTicket.id}">
	<s:reset type="button" cssClass="cssbutton" key="Reset" onclick="newFunctionForCountryState();"	cssStyle="width:55px; height:25px;float:left;margin-left:5px;" />
	</c:if>
	</c:if>
	<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
   <c:if test="${usertype=='DRIVER'}">
	<input type="button" class="cssbutton" name="wticket" style="width:55px; height:25px;float:left;"
		value="<fmt:message key="button.save"/>" onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');completeTimeString();IsValidTime('save');" onclick="enableElements();return checkExceedLimit();" />
	</c:if>
	</configByCorp:fieldVisibility>

	<%--<configByCorp:fieldVisibility componentId="component.field.Alternative.showAddWithCopyButton">--%>
	<c:if test="${usertype!='DRIVER'}">
	<c:if test="${not empty workTicket.id}">
		<c:if test='${serviceOrder.status != "CNCL" }'>
			<input type="button" class="cssbuttonA" style="width:100px; height:25px;float:left;margin-left:5px;"	 
				 onclick="return findBillCompleteDate1();" value="Add With Copy" />
		</c:if>
	</c:if>
	</c:if>
	<%--</configByCorp:fieldVisibility>--%>
<%--	<configByCorp:fieldVisibility componentId="component.button.mobileMoverService.On"> --%>
<%-- <c:if test= "${mmValidation =='Yes' }" >
	<c:if test="${not empty workTicket.id}">
		<input type="button" id ="MM" class="cssbuttonA" style="width:150px; height:25px;float:left;margin-left:5px;" onclick="callMobileMoverService('MM')" value="Send To Mobile Mover" />
		</c:if> --%>
		<%--<input type="button" id ="MMInv" class="cssbuttonA" style="width:225px; height:25px;float:left;margin-left:5px;" onclick="callMobileMoverService('MMInv')" value="Send To Mobile Mover With Inventory" /> --%>
	<%--	<c:if test="${workTicket.orderId ne null && workTicket.orderId!='' }">
			<input type="button" id ="syncMM" class="cssbuttonA" style="width:150px; height:25px;float:left;margin-left:5px;" onclick="syncWithMobileMoverService()" value="Sync With Mobile Mover" />
		</c:if>
</c:if> --%>
<%--	</configByCorp:fieldVisibility>--%>
<configByCorp:fieldVisibility componentId="component.button.workTicket.printPackage">
	<c:if test="${not empty workTicket.id}">
			<input type="button" class="cssbuttonA" style="width:100px; height:25px;float:left;margin-left:5px;"	 
				 onclick="printDailyPackage();" value="Print Package" />
	</c:if>
	</configByCorp:fieldVisibility>
	</div>
	<c:set var="idOfWhom" value="${workTicket.id}" scope="session" />
	<c:set var="noteID" value="${workTicket.ticket}" scope="session" />
	<c:set var="noteFor" value="WorkTicket" scope="session" />
	<c:set var="idOfTasks" value="${workTicket.id}" scope="session"/>
    <c:set var="tableName" value="workticket" scope="session"/>
    <c:set var="tasksSoId" value="${serviceOrder.id}" scope="session"/>
	<c:if test="${empty workTicket.id}">
		<c:set var="isTrue" value="false" scope="request" />
	</c:if>
	<c:if test="${not empty workTicket.id}">
		<c:set var="isTrue" value="true" scope="request" />
	</c:if>
<s:hidden name="workTicket.shipNumber" value="${serviceOrder.shipNumber}"/>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="workTicket.originCountryCode" />
<s:hidden name="workTicket.destinationCountryCode" />
</div>
    <c:if test="${flag3 =='true'}" >
		<c:redirect url="/editWorkTicketUpdate.html?id=${workTicket.id}"  />
	</c:if>
</s:form>
<script type="text/javascript">
$(document).ready( function(){
	$("select,input,textfield,textarea,button").each(function() {
    $(this).addClass('myclass');
    
});

$(".myclass").each(function(i=0,ele) {
    $(this).attr('tabindex', i+1) ;
    });
    
})   
</script>
<script type="text/javascript">
var anchor = document.getElementsByTagName("a");
for( var i = 0, j =  anchor.length; i < j; i++ ) {
anchor[i].setAttribute( 'tabindex', '-1' );
}
</script>

<script type="text/javascript">
function dataListForm(flag){
	if(flag=='handout'){	
	window.open('dataListForm.html?decorator=popup&popup=true&sid=${serviceOrder.id}&workTicketId=${workTicket.id}&shipNumber=${serviceOrder.shipNumber}&ticket=${workTicket.ticket}','dataListForm','height=500,width=980,top=0, scrollbars=yes,resizable=yes').focus();
	}else{
		window.open('dataListForm.html?decorator=popup&popup=true&sid=${serviceOrder.id}&workTicketId=${workTicket.id}&shipNumber=${serviceOrder.shipNumber}&ticket=${workTicket.ticket}','dataListForm','height=500,width=980,top=0, scrollbars=yes,resizable=yes').focus();	
	}
	}

function editDateItem(id,sid,wid,shipNumber){
	var ticketAssignedStatus = '${workTicket.ticketAssignedStatus}';
	window.open('editDateItem.html?decorator=popup&popup=true&id='+ id+'&workTicketId='+wid+'&sid='+sid+'&shipNumber='+shipNumber+'&ticketAssignedStatus='+ticketAssignedStatus,'dataListForm','height=500,width=980,top=0, scrollbars=yes,resizable=yes').focus();
}

function openDataListForm(id,sid,wid,shipNumber,ticket,flagForHO){
	window.open('editDateItem.html?decorator=popup&popup=true&id='+ id+'&workTicketId='+wid+'&sid='+sid+'&shipNumber='+shipNumber+'&flagForHO='+flagForHO,'dataListForm','height=500,width=980,top=0, scrollbars=yes,resizable=yes').focus();  
}

function deleteItemData(id,workTicketId){
var agree=confirm("Are you sure you wish to remove it?");
    if (agree){
	$.ajax({
		  type: "POST",
		  url: "deleteFromItemData.html?ajax=1&decorator=simple&popup=true",
		  data: { id:id,workTicketId:workTicketId},
		  success: function (data, textStatus, jqXHR) {
			  //window.location.reload();
			  findAllResource();
		  },
	    error: function (data, textStatus, jqXHR) {
			    	showOrHide(0);
				}
		});
    }
}
</script>
<script type="text/javascript">
try{
findReleasedStorageDate();
}
catch(e){}
try{
	var c='${typeService}' ;
	var a=" Can not Save ticket as Operation Hub/Warehouse Limit is required for ";
	var b =" service Type";
	
		<c:if test="${not empty flag && flag =='true'}">
			if('${countCheck}' == 'Limit'){
				alert('The ticket start date cannot be accepted as it is under the minimum service limit for operations.\nPlease select another date.');
			}
			if('${countCheck}' == 'GreaterWeightVolume'){
				alert('Exceeding the Maximum value of Operational Limit - Please Select another Date.');
			}
			if('${countCheck}' == 'Greater'){
				alert('Can not Save ticket as Operation Limit for date ${exceedDate} has been exceeded.');
			}			
			//window.open('messagePopUp.html?id=${id}&exceedDate=${exceedDate}&countCheck=${countCheck}&decorator=popup&popup=true','','width=650,height=170') ;
		</c:if>
		<c:if test="${not empty flag && flag =='hub'}">
        	//alert('Can not Save ticket as Operation Hub/Warehouse Limit is required for PK,LD,DU,DL,UP,PAD and PL service Type.');
        	alert(a+c+b);
        </c:if>
        <c:if test="${hubWarehouseLimit=='Crw' && flag =='true'}">
	        if('${countCheck}' == 'CrewMax'){
		        	alert('Operational limit of the hub has been reached. Kindly contact the warehouse manager.');
	        }
	    </c:if>	
		}
		catch(e){}
</script>
<script type="text/javascript">
   try{
    validService();
    }
    catch(e){}
	try{
 	<c:if test="${userRole != 'ROLE_FINANCE' && userRole != 'ROLE_BILLING'}">
 	autoPopulate(document.forms['workTicketForm'].elements['workTicket.stgHow']);
 	</c:if>
 	}
 	catch(e){}
 	try{
 	assigntimeairthmaticNew();
 	}
 	catch(e){}
 	try{
 	assigntimeairthmaticNew12();
 	}
 	catch(e){}
 	try{
 	IsValidTime('save');
 }
 catch(e){}
try{
 var ocountry=document.getElementById('ocountry').value;
 }
 catch(e){}
 try{
 var dcountry=document.getElementById('dcountry').value;
 }
 catch(e){}
 try{
	 oriCon = document.forms['workTicketForm'].elements['workTicket.originCountry'].value;
	 var enbState = '${enbState}';
	 if(oriCon=="")	 {
			document.getElementById('ostate').disabled = true;
			document.getElementById('ostate').value ="";
	 }else{
	  		if(enbState.indexOf(oriCon)> -1){
				document.getElementById('ostate').disabled = false;		
				document.getElementById('ostate').value='${workTicket.state}';
				if(state=='')
				{
					getState('OA');
				}
			}else{
				document.getElementById('ostate').disabled = true;
				document.getElementById('ostate').value ="";
			}
	 	}
 } catch(e){}
try{
	var desCon=document.forms['workTicketForm'].elements['workTicket.destinationCountry'].value;
	 var enbState = '${enbState}';
	 if(desCon=="")	 {
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
	 }else{
	  		if(enbState.indexOf(desCon)> -1){
				document.getElementById('destinationState').disabled = false;		
				document.getElementById('destinationState').value='${workTicket.destinationState}';
				if(dstate==''){
					setTimeout("getState('DA')",2000);
				}
			}else{
				document.getElementById('destinationState').disabled = true;
				document.getElementById('destinationState').value ="";
			}
	 	}
	}
catch(e){}
try{
zipCode();
}
catch(e){}
</script>
<script type="text/javascript">
try{
	 ostateMandatory();
     dstateMandatory();
}
catch(e){}
</script>
<script>
disable_site_origin();
disable_site_destination();
</script>
<script type="text/javascript">
 	var fieldName = document.forms['workTicketForm'].elements['field'].value;
	var fieldName1 = document.forms['workTicketForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['workTicketForm'].elements[fieldName].className = 'rules-textUpper';
		animatedcollapse.addDiv('handout', 'persist=0,show=1');
	<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
		animatedcollapse.addDiv('details', 'persist=0,show=1');
	   </configByCorp:fieldVisibility>
		animatedcollapse.addDiv('storage', 'persist=0,show=1');
	    animatedcollapse.addDiv('address', 'persist=0,show=1');
		animatedcollapse.addDiv('confirmation', 'persist=0,show=1');
		animatedcollapse.addDiv('billing', 'persist=0,show=1');
		animatedcollapse.addDiv('staff', 'persist=0,show=1');
		animatedcollapse.init();
	}
	if(fieldName1!=''){
		document.forms['workTicketForm'].elements[fieldName1].className = 'rules-textUpper';
		animatedcollapse.addDiv('handout', 'persist=0,show=1');
	<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
		animatedcollapse.addDiv('details', 'persist=0,show=1');
	  </configByCorp:fieldVisibility>
		animatedcollapse.addDiv('storage', 'persist=0,show=1');
		animatedcollapse.addDiv('address', 'persist=0,show=1');
		animatedcollapse.addDiv('confirmation', 'persist=0,show=1');
		animatedcollapse.addDiv('billing', 'persist=0,show=1');
		animatedcollapse.addDiv('staff', 'persist=0,show=1');
		animatedcollapse.init();
	}
</script>
<script type="text/javascript">

<configByCorp:fieldVisibility componentId="component.workTicket.itemListTab">
<c:if test="${workTicket.service=='RC'}">	
var st2=document.getElementById('itemData');
findAllResource();
st2.style.display = 'block';
</c:if>
</configByCorp:fieldVisibility>
function findAllResource(){
var shipNumber= '${workTicket.shipNumber}'
var workTicketId = '${workTicket.id}';
var ticketAssignedStatus = '${workTicket.ticketAssignedStatus}';
$.get("allItemDataList.html?ajax=1&decorator=simple&popup=true", 
		{shipNumber:shipNumber,workTicketId:workTicketId,ticketAssignedStatus:ticketAssignedStatus},
		function(data){
			$("#resourcMapAjax").html(data);
	});
}
function showo(){
	var st2=document.getElementById('itemData');
			st2.style.display = 'none';
}
</script>
<script type="text/javascript">

<configByCorp:fieldVisibility componentId="component.workTicket.itemListTab">

<c:if test="${workTicket.service=='HO'}">   
var st2=document.getElementById('handOutForCorex');
findAllHandOut();
st2.style.display = 'block';
function ajax_hideTooltip(){if(ajax_tooltipObj){ajax_tooltipObj.style.display="none"}if(ajax_tooltipObjBig){ajax_tooltipObjBig.style.display="none"}document.getElementById("itemSearch").value='';}
</c:if>
</configByCorp:fieldVisibility>

function findAllHandOut(){
var shipNumber= '${workTicket.shipNumber}'
var workTicketId = '${workTicket.id}';
var ticketAssignedStatus = '${workTicket.ticketAssignedStatus}';
$.get("findAllHandOutList.html?ajax=1&decorator=simple&popup=true", 
        {shipNumber:shipNumber,ticketAssignedStatus:ticketAssignedStatus},
        function(data){
            $("#handOutForCorexAjax").html(data);
    });
}

function searchHandOutItem(position){
	var shipNumber = '${serviceOrder.shipNumber}';
	var url="itemSearchOnHandOut.html?ajax=1&decorator=simple&popup=true&itemDescription=" +position.value+"&shipNumber="+encodeURI(shipNumber);
	ajax_showTooltip(url,position);
}
ajax_hideTooltip(); 
function updateHoTicket(id,ticket){
	var shipNumber = '${serviceOrder.shipNumber}';
	$.ajax({
	    url : "updateHandoutItem.html?handoutItemId="+id+"&shipNumber="+encodeURI(shipNumber)+"&ticket="+encodeURI(ticket),
	    type : "POST",
	    async : false,
	    success : function(data) {
	    	findAllHandOut();
	    	document.getElementById("itemSearch").value='';
	    	ajax_hideTooltip(); 
	    } 
	});
	
}

function CheckAllHoTicket(){
	ajax_hideTooltip();
	var releaseAllItemsValue = document.getElementById("chkforHo").checked
	var shipNumber = '${workTicket.shipNumber}';
	var ticket = '${workTicket.ticket}';
	$.ajax({
        url : "updateAllHoTicket.html?shipNumber="+encodeURI(shipNumber)+"&ticket="+encodeURI(ticket)+"&releaseAllItemsValue="+releaseAllItemsValue,
        type : "POST",
        async : false,
        success : function(data) {
            findAllHandOut();
        } 
    });
}

function updateHoTicketByHoClick(target,id,shipNumber){
    ajax_hideTooltip();
    var hoValue = document.getElementById(target.id).checked
    var ticket = '${workTicket.ticket}';
     $.ajax({
        url : "updateHoTicketByHoClick.html?decorator=simple&popup=true&shipNumber="+encodeURI(shipNumber)+"&id="+encodeURI(id)+"&hoValue="+hoValue+"&ticket="+ticket,
        type : "POST",
        async : false,
        success : function(data) {
             document.getElementById('estimatedPieces').value= data.trim().split("~")[0]; 
            document.getElementById('actWeight').value = data.trim().split("~")[1];
             document.getElementById('actVolume').value = data.trim().split("~")[2];
             document.getElementById('totalWeight').value=data.trim().split("~")[1];
             document.getElementById('totalVolume').value=data.trim().split("~")[2];
             document.getElementById('totalActualQty').value=data.trim().split("~")[0]; 
            
        } 
    }); 
}
</script>
<script type="text/javascript">
try{
document.forms['workTicketForm'].elements['workTicket.storageOut'].disabled=true;
initLoader();
}catch(e){}
</script>
<script type="text/javascript">
	setOnSelectBasedMethods(["serviceCheckingBeginDate(),serviceCheckingEndDate()"]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
try{
	getState('OA')
	setTimeout("getState('DA')",2000);
	  checkMultiAuthorization();
}catch(e){
}
</script>
<script type="text/javascript">
try{	
	var st2=document.getElementById('driverRequiredTrue');
	var showList = '${showList}';
		if(showList == 'Show'){			
			st2.style.display = 'block';
		}
		if(showList == 'Hide'){			
			st2.style.display = 'none';
		}
}catch(e){}
</script>
<script type="text/javascript">
try{	
	//Created by subrat BUG #7530
	<sec-auth:authComponent componentId="module.tab.workTicket.DRIVER">
	 disabledAll();
	  trap();
	</sec-auth:authComponent>
}catch(e){
}
</script>
<script type="text/javascript">

	
function disableItemCheckBox(){
	try{  	
		document.getElementById('ticketStatus').focus();
		<c:if test ="${workTicket.ticketAssignedStatus=='Assigned'}">
		document.getElementById("chkforHo").disabled = true;
		</c:if>
	}catch(e){}
	
	}
setTimeout("disableItemCheckBox()",1000);
function findBillCompleteDate()
{
var shipNumber = document.forms['workTicketForm'].elements['shipnum'].value;
var url="billComplete.html?ajax=1&decorator=simple&popup=true&shipnum="+ encodeURI(shipNumber);
 http2.open("GET", url, true);
 http2.onreadystatechange = handleHttpResponse1;
 http2.send(null); 
 }     
function handleHttpResponse1() {
  var id = document.forms['workTicketForm'].elements['fileID'].value;
         if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            var res = results.split("#"); 
            if(((res[0]=='A' && res[1]!='1') || (res[0]!='T' && res[1]!='1')) || res[2]!='1' || res[3]!='1')
            {
            	alert('No more tickets can be added as Billing is Complete');
            	return false;
            }else{
            	<c:choose>
            	<c:when test="${closedCompanyDivision=='Y' && serviceOrder.status == 'CLSD'}">
            		findClosedCompanyDivision('${serviceOrder.companyDivision}');
            		</c:when>
    				<c:otherwise> 
            		location.href="editWorkTicket.html?id="+id;
            		return true;
            		</c:otherwise>
        		      </c:choose> 
            }
		 }    
}
function findBillCompleteDate1(){
	var shipNumber = document.forms['workTicketForm'].elements['shipnum'].value;
	var url="billComplete.html?ajax=1&decorator=simple&popup=true&shipnum="+ encodeURI(shipNumber);
	   http2.open("GET", url, true);
	   http2.onreadystatechange = handleHttpResponse111;
	   http2.send(null); 
}

function handleHttpResponse111(){
var wid = document.forms['workTicketForm'].elements['workTicket.id'].value;
var sid = document.forms['workTicketForm'].elements['serviceOrder.id'].value;

       if (http2.readyState == 4){
          var results = http2.responseText
          results = results.trim();
          var res = results.split("#"); 
          if(((res[0]=='A' && res[1]!='1') || (res[0]!='T' && res[1]!='1')) || res[2]!='1' || res[3]!='1')
          {
          	alert('No more tickets can be added as Billing is Complete');
          	return false;
          }else{
        	  <c:choose>
        	  	<c:when test="${closedCompanyDivision=='Y' && serviceOrder.status == 'CLSD'}">
      				findClosedCompanyDivision1('${serviceOrder.companyDivision}');
      				</c:when>
    				<c:otherwise> 
					location.href="addWithCopyWorkTicket.html?wid=${workTicket.id}&sid=${serviceOrder.id}";
	          		return true;
	          		</c:otherwise>
	    		      </c:choose> 
          }
		}  
}
function findClosedCompanyDivision(companyCode){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  location.href="editWorkTicket.html?id="+id;
                		return true;
			      }else{
			    	  alert("You can not add Work Ticket, As Company Division is closed.");
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}
function findClosedCompanyDivision1(companyCode){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  location.href="addWithCopyWorkTicket.html?wid=${workTicket.id}&sid=${serviceOrder.id}";
		          		return true;
			      }else{
			    	  alert("You can not add Work Ticket, As Company Division is closed.");
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}

function findStandardAddressDetail(residenceName,position){
	if(residenceName!=null && residenceName!=''){
		var url="findStandardAddressDetailAjax.html?ajax=1&decorator=simple&popup=true&residenceName="+residenceName;
		ajax_showTooltip(url,position);
	}else{
		alert("Please select a value to continue.");
	}
}

try{
	var str="0";
	<configByCorp:fieldVisibility componentId="component.field.StandardAddress.residenceManagement">
	str="1";
	</configByCorp:fieldVisibility>
		var e = document.getElementById('addressBookTrue');
		var e1 = document.getElementById('addressBookTrue1');
		var e11 = document.getElementById('addressBookTrue11');
		var e111 = document.getElementById('addressBookTrue111');
		var ad = document.getElementById('addressBookImg');
		var ad1 = document.getElementById('addressBookImg1');
		var s = document.getElementById('addressBookFalse');
		var s1 = document.getElementById('addressBookFalse1');
		var s11 = document.getElementById('addressBookFalse11');
		var s111 = document.getElementById('addressBookFalse111');
		if(str=="1"){
			e.style.display = 'block';
			e1.style.display = 'block';
			e11.style.display = 'block';
			e111.style.display = 'block';
			ad.style.display = 'block';
			ad1.style.display = 'block';
			s.style.display = 'none';
			s1.style.display = 'none';
			s11.style.display = 'none';
			s111.style.display = 'none';
		}else{
			e.style.display = 'none';
			e1.style.display = 'none';
			e11.style.display = 'none';
			e111.style.display = 'none';
			ad.style.display = 'none';
			ad1.style.display = 'none';
			s.style.display = 'block';
			s1.style.display = 'block';
			s11.style.display = 'block';
			s111.style.display = 'block';
		}	
}catch(e){
}

</script>
<script type="text/javascript">
function autoCompleteVendorNameAjax(e,t,n,s){
	var o=s.which?s.which:s.keyCode;
	if(o!=9){
		var u=document.getElementById(e).value;
		if(u.length<=1){
			document.getElementById(t).value=""
			}if(u.length>=1){
				$.get("vendorNameAutocompleteAjax.html?ajax=1&decorator=simple&popup=true",
						{
							partnerNameAutoCopmlete:u,
							partnerNameId:e,
							paertnerCodeId:t,
							autocompleteDivId:n
							
					},function(e){
						document.getElementById(n).style.display="block";
						$("#"+n).html(e)})
						}
			else{
				document.getElementById(n).style.display="none"
				}
			}else{
				document.getElementById(n).style.display="none"
				}
	}



function copyVendorDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
	
}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}
	
	
function getInstructionCodeValues(){
    var serviceCode = document.forms['workTicketForm'].elements['workTicket.service'].value;
	var instructionCode = document.forms['workTicketForm'].elements['instCode'].value;
    var url="getInstructionCodeList.html?ajax=1&decorator=simple&popup=true&serviceCode="+encodeURI(serviceCode)+"&instructionCode="+encodeURI(instructionCode);
		
	
	http2.open("GET", url, true);
	http2.onreadystatechange = handleHttpResponseInstructionCodeValues;
	http2.send(null);	
}
function handleHttpResponseInstructionCodeValues(){
	 var results = http2.responseText
      results = results.trim();
     res = results.split("@");
     targetElement = document.forms['workTicketForm'].elements['workTicket.instructionCode'];
		targetElement.length = res.length;
	 for(i=0; i<res.length; i++){
		 if(res[i] == ''){
				document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].text = '';
				document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
				document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].text = stateVal[1].replace("~",'@');
				document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].value = stateVal[0];
				if (document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].value == '${workTicket.instructionCode}'){
					document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].defaultSelected = true;
					}
				var duplicateValue=document.forms['workTicketForm'].elements['instructionCodeHiddenValue'].value;
				if (document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].value == duplicateValue){
					document.forms['workTicketForm'].elements['workTicket.instructionCode'].options[i].defaultSelected = true;
					}
				
				}
		 autoPopulate_instructionNew();
}
	 
	 	
	
}

function autoPopulate_instructionNew() {
	var instructions=document.forms['workTicketForm'].elements['workTicket.instructions'].value;
	if(instructions == '')
    {
		document.forms['workTicketForm'].elements['workTicket.instructions'].value=instructions;
    }
	
}

function driveruncheck(){
	var driverchecks=document.getElementById('workTicketForm_workTicket_accepted').checked; 
	 if(driverchecks)
		{
	 
	    document.forms['workTicketForm'].elements["workTicket.acceptByDriver"].disabled=false
	    
		}
		else{
			document.forms['workTicketForm'].elements['workTicket.acceptByDriver'].value='';
		}
}
/* enabled all element on workTicketForm  before save*/
function enableElements(){
	var elementsLen=document.forms['workTicketForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['workTicketForm'].elements[i].type=='text'){
			document.forms['workTicketForm'].elements[i].readOnly =false;
			document.forms['workTicketForm'].elements[i].className = 'input-textUpper';
			document.forms['workTicketForm'].elements[i].onkeydown ="";
		}else if(document.forms['workTicketForm'].elements[i].type=='textarea'){
			document.forms['workTicketForm'].elements[i].readOnly =false;
			document.forms['workTicketForm'].elements[i].className = 'textareaUpper';
		}else{
			document.forms['workTicketForm'].elements[i].disabled=false;
		} 
		
	}

	
}



function acceptByDriver()
{
    var driverchecks=document.getElementById('workTicketForm_workTicket_accepted').checked;
	if(driverchecks ==true){
	     document.getElementById('estrik1').style.display = 'inline-block'; 
		 document.getElementById('estrik2').style.display = 'inline-block'; 
		 document.getElementById('estrik3').style.display = 'inline-block'; 
	}
	else if(driverchecks ==false){
			 document.getElementById('estrik1').style.display = 'none'; 
		 document.getElementById('estrik2').style.display = 'none'; 
		 document.getElementById('estrik3').style.display = 'none'; 
	}
	else
		{
		 document.getElementByClassName('relwcolist').style.display = 'none';
		 document.getElementByClassName('testing').style.display = 'none';
		}
}


$('#fvpValue').on('change', function(){
	var fvpValue1 = '${workTicket.fvpValue}'; 
	  var acvcurrent = $(this).val();
	  acvcurrent = parseInt(acvcurrent);
	var estimatedWeight=document.forms['workTicketForm'].elements['workTicket.estimatedWeight'].value;
	var actualWeight=document.forms['workTicketForm'].elements['workTicket.actualWeight'].value;
	var estifvpvprev = Math.round((estimatedWeight*6.00)*0.001)*1000;
	var actacvprev =  Math.round((actualWeight*6.00)*0.001)*1000;
     if(actualWeight=='' ||actualWeight==0.0){
    if(acvcurrent >= estifvpvprev ){
    	document.forms['workTicketForm'].elements["workTicket.fvpValue"].value= acvcurrent;
		}
        else
        {
        	$(this).val(fvpValue1);
        alert("Lesser value then the current amount lumpsum") 
        }
    }
 else if(actualWeight!='' || actualWeight!=0.0){
    	 if(acvcurrent >= actacvprev ){
    			document.forms['workTicketForm'].elements["workTicket.fvpValue"].value= acvcurrent;
    			}
                else
                {
                	$(this).val(fvpValue1);
                alert("Lesser value then the current amount lumpsum") 
                }
    }
    else
    {
    	$(this).val(fvpValue1);
   		alert("Lesser value then the current amount lumpsum")  
    }
});	
	
$('#acvValue').on('change', function(){
    var acvValue1 = '${workTicket.acvValue}';
	var estimatedWeight=document.forms['workTicketForm'].elements['workTicket.estimatedWeight'].value;
	var actualWeight=document.forms['workTicketForm'].elements['workTicket.actualWeight'].value;
	var estifvpvprev =Math.round((estimatedWeight*2.00)*0.001)*1000;
	var actacvprev =  Math.round((actualWeight*2.00)*0.001)*1000;
    var acvcurrent = $(this).val();
    acvcurrent = parseInt(acvcurrent);
    if(actualWeight=='' ||actualWeight==0.0){
        if(acvcurrent >= estifvpvprev ){
        	document.forms['workTicketForm'].elements["workTicket.acvValue"].value= acvcurrent;
    		}
            else
            {
            	$(this).val(acvValue1);
            alert("Lesser value then the current amount lumpsum") 
            }
        }
     else if(actualWeight!=''|| actualWeight!=0.0 ){
        	 if(acvcurrent >= actacvprev ){
        			document.forms['workTicketForm'].elements["workTicket.acvValue"].value= acvcurrent;
        			}
                    else
                    {
                    	$(this).val(acvValue1);
                    alert("Lesser value then the current amount lumpsum") 
                    }
        }
        else
        {
        	$(this).val(acvValue1);
       		alert("Lesser value then the current amount lumpsum")  
        }
});


$( "#estimatedWeightvalue" ).change(function() {
	var data=document.forms['workTicketForm'].elements['workTicket.estimatedWeight'].value;
	var data1=document.forms['workTicketForm'].elements['workTicket.actualWeight'].value;
    if(data1 == null || data1 =='' || data1==0.0){
		if(data != null || data !=''){
            document.forms['workTicketForm'].elements["workTicket.fvpValue"].value=  Math.round((data*6.00)*0.001)*1000;
			document.forms['workTicketForm'].elements["workTicket.acvValue"].value= Math.round((data*2.00)*0.001)*1000;
	    }
	}
else{
    document.forms['workTicketForm'].elements["workTicket.fvpValue"].value= Math.round((data1*6.00)*0.001)*1000;
	document.forms['workTicketForm'].elements["workTicket.acvValue"].value=Math.round((data1*2.00)*0.001)*1000;
}
	});

	$( "#actWeightvalue" ).change(function() {
		var data=document.forms['workTicketForm'].elements['workTicket.estimatedWeight'].value;
		var data1=document.forms['workTicketForm'].elements['workTicket.actualWeight'].value;
	    if(data1 == null || data1 =='' || data1==0.0){
			if(data != null || data !=''){
	            document.forms['workTicketForm'].elements["workTicket.fvpValue"].value=  Math.round((data*6.00)*0.001)*1000;
				document.forms['workTicketForm'].elements["workTicket.acvValue"].value= Math.round((data*2.00)*0.001)*1000;
		    }
		}
	else{
	    document.forms['workTicketForm'].elements["workTicket.fvpValue"].value= Math.round((data1*6.00)*0.001)*1000;
		document.forms['workTicketForm'].elements["workTicket.acvValue"].value=Math.round((data1*2.00)*0.001)*1000;
	}
		});
   
window.onload = function() {
	var fvpValue11 = '${workTicket.fvpValue}';
	var acvValue11 = '${workTicket.acvValue}';
	var acvValue= document.forms['workTicketForm'].elements["workTicket.acvValue"].value
	var fvpValue = document.forms['workTicketForm'].elements["workTicket.fvpValue"].value
	var data=document.forms['workTicketForm'].elements['workTicket.estimatedWeight'].value;
	var data1=document.forms['workTicketForm'].elements['workTicket.actualWeight'].value;
	if(fvpValue11=='' || acvValue11==''){
		 if(data1 == null || data1 =='' || data1==0.0){
				if(data != null || data !=''){
					  document.forms['workTicketForm'].elements["workTicket.fvpValue"].value=  Math.round((data*6.00)*0.001)*1000;
						document.forms['workTicketForm'].elements["workTicket.acvValue"].value= Math.round((data*2.00)*0.001)*1000;
			    }
			}
		else{
			   document.forms['workTicketForm'].elements["workTicket.fvpValue"].value= Math.round((data1*6.00)*0.001)*1000;
				document.forms['workTicketForm'].elements["workTicket.acvValue"].value=Math.round((data1*2.00)*0.001)*1000;
		}
		}	
        if(fvpValue11!='' || acvValue11!=''){
	      document.forms['workTicketForm'].elements["workTicket.fvpValue"].value;
		  document.forms['workTicketForm'].elements["workTicket.acvValue"].value
	      }
	};

	function numbersonly(e){
	    var unicode=e.charCode? e.charCode : e.keyCode
	    if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
	        if (unicode<48||unicode>57) //if not a number
	            return false //disable key press
	    }
	}
</script>