<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Work Order</title>   
<meta name="heading" content="Tool Tip"/> 
<style>
.table thead th, .tableHeaderTable td {height:15px;}
</style>
</head>
<s:form action="" name="scopePopupOI">
<table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" width="302" style="!margin-top:5px;margin-bottom:5px">	
	<tbody>	
	<tr><td class="subcontent-tab" style="height:17px;font-size:12px;background:url(images/basic-inner-blue.png) repeat-x scroll 0 0 #3dafcb;border-bottom: 1px solid #3dafcb;border-top: 1px solid #3dafcb; color: #fff;font-family: arial,verdana;"><font>Scope of &nbsp;${workorder}</font></td></tr>
	<tr>
	<td valign="top">			
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;padding-top:10px;" >
	<tbody>
	<tr>
	<td style="width:11px;margin-top:5px;"> </td>
	<td valign="top">
	<%--   <s:textarea cssStyle="width:420px;height:150px;resize:vertical;" cssClass="input-text pr-f11" id="scopeOfWorkOrder" value="${scopeOfWorkOrder}" name="scopeOfWorkOrder1"> </s:textarea>  --%>
         <!-- to reflect Revision Scope of  WO_01 at O&I Tab-->
 <span style="white-space:pre-warp;width:300px;font-size:11px;"><c:out value="${operationsIntelligence.revisionScopeOfWorkOrder}"></c:out></span>
 	
	</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	
<c:if test="${usertype != 'ACCOUNT' }">
    <table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0"  style="width:100%;padding-top:10px;margin-bottom:5px;" >
    <tr>
    <td><b># Computers</b></td>
    <td><b># Employee</b></td>
    </tr>
    <tr><td>
    <s:textfield id="numberOfComputer" name="operationsIntelligence.revisionScopeOfNumberOfComputer" readonly="true" maxlength="6" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyNumberAllowed(this)"></s:textfield>
    </td>
    <td><s:textfield id="numberOfEmployee" name="operationsIntelligence.revisionScopeOfNumberOfEmployee" readonly="true" maxlength="6" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyNumberAllowed(this)"></s:textfield></td>
    </tr>
    
     <tr>
    <td><b>Sales Tax</b></td>
    <td><b>Equipment Rental</b></td>
   
    </tr>
   
    <tr><td>
    <s:textfield id="salesTax" name="operationsIntelligence.revisionScopeOfSalesTax" maxlength="15" readonly="true" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyFloatAllowed(this)"></s:textfield>
    </td>
    <td><s:textfield id="consumables" name="operationsIntelligence.revisionScopeOfConsumables" maxlength="15"  readonly="true" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyFloatAllowed(this)"></s:textfield></td>
    <c:set var="revisionPercentFlag" value="false"/>
	<c:if test="${operationsIntelligence.revisionConsumablePercentage}">
		<c:set var="revisionPercentFlag" value="true"/>
	</c:if>
	<td align="left" width="23" valign="top" ><s:checkbox id="revisionConsumablePercentage" key="operationsIntelligence.revisionConsumablePercentage" value="${revisionPercentFlag}" /></td>
    </tr>
    <tr>
          <td><b> AB5 Surcharge</b></td>
      <td><b> Edit AB5 Surcharge</b></td>
    </tr>
        <tr>
    <td><s:textfield id="consumables" name="operationsIntelligence.revisionScopeOfAB5Surcharge" maxlength="15"  readonly="true" cssStyle="width:120px;" cssClass="input-textUpper pr-f11" onchange="onlyFloatAllowed(this)"></s:textfield></td>
    <c:set var="revisionAB5SurchargePercentageFlag" value="false"/>
	<c:if test="${operationsIntelligence.revisionaeditAB5Surcharge}">
		<c:set var="revisionAB5SurchargePercentageFlag" value="true"/>
	</c:if>
	<td align="left" width="23" valign="top" ><s:checkbox id="revisionAB5SurchargePercentageFlag" key="operationsIntelligence.revisionaeditAB5Surcharge" value="${revisionAB5SurchargePercentageFlag}" /></td>
    </tr>
    <tbody>
   
    </tbody>
    </table>
    
    </c:if>
	
	</tbody>
</table>
</s:form >
	   
