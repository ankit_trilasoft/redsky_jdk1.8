<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <%@ page import="org.acegisecurity.context.SecurityContextHolder" %>
 <%@ page import="org.appfuse.model.User" %>
<%@ page import="org.acegisecurity.Authentication" %>

<head>   
    <title>SO Dashboard</title> 
    <meta name="heading" content="SO Dashboard"/>  
     <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<style type=text/css>
div.autocomplete {
      position:absolute;      
      background-color:white;     
      margin:0px;
      padding:0px;
      z-index:99999;
      
      
    }
    div.autocomplete ul {
      list-style-type:none;
      border-top:1px solid #219DD1;
      border-bottom:1px solid #219DD1;
      margin:0px;
      padding:0px;
    }
    div.autocomplete ul li.selected { background-color: #ffb;}
    div.autocomplete ul li {
      list-style-type:none;
      display:block;
      border-top:1px solid #dfdfdf;
      border-bottom:none;
      border-left:1px solid #219DD1;
      border-right:1px solid #219DD1;
      margin:0;
      padding:0px;
      padding-left:3px;   
      cursor:pointer;
    }
    
    div#autocomplete_choices{ width:200px !important; }
    

  .ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
  </style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
   ////////////10738//////////////////////
   
   function autoCompleterAjaxCallBilltoName(){
			var billtoName = document.forms['serviceOrderListForm'].elements['billToName'].value;
			if(billtoName.length>=4){
			var data = 'billToNameAutocompleteAjax.html?ajax=1&billtoName='+billtoName+'&decorator=simple&popup=true';
				$( "#billToName1" ).autocomplete({				 
				      source: data
				    });
			}else{
				$( "#billToName1" ).autocomplete({				 
				      source: null
				    });
			}
			}
   
   var httpState = getHTTPObjectState()
   function getHTTPObjectState()
{
   var xmlhttp;
   if(window.XMLHttpRequest)
   {
       xmlhttp = new XMLHttpRequest();
   }
   else if (window.ActiveXObject)
   {
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       if (!xmlhttp)
       {
           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
       }
   }
   return xmlhttp;
}		
   ///////////////////END//////////////////

   function showFailureToolTip(usr,obj){
		 // alert(usr);
		  ajax_showTooltip('findtoolTipClaims.html?ajax=1&decorator=simple&popup=true&tableFieldName='+usr,obj);
	  }

function clear_fields(){
	document.forms['serviceOrderListForm'].elements['shipNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['bookingAgentShipNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['registrationNumber'].value = "";	
	document.forms['serviceOrderListForm'].elements['status'].value = "";	
	document.forms['serviceOrderListForm'].elements['job1'].value = "";
	document.forms['serviceOrderListForm'].elements['coordinator'].value = "";	
	document.forms['serviceOrderListForm'].elements['firstName'].value = "";
	document.forms['serviceOrderListForm'].elements['lastName'].value = "";
	document.forms['serviceOrderListForm'].elements['billToName'].value = "";
	document.forms['serviceOrderListForm'].elements['originCityOrZip'].value='';
	document.forms['serviceOrderListForm'].elements['destinationCityOrZip'].value = '';
    document.forms['serviceOrderListForm'].elements['originCountry'].value='';
	document.forms['serviceOrderListForm'].elements['destinationCountry'].value = '';
  
	document.forms['serviceOrderListForm'].elements['companyDivision'].value = "";
	document.forms['serviceOrderListForm'].elements['salesMan'].value='';
	<configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">	
	document.forms['serviceOrderListForm'].elements['certificateNumber'].value='';
	
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">	
	document.forms['serviceOrderListForm'].elements['daShipmentNumber'].value='';
	
	</configByCorp:fieldVisibility>
	<c:if test="${checkAccessQuotation}">
	document.forms['serviceOrderListForm'].elements['serviceOrderMoveType'].value = "";	
	</c:if>
	document.forms['serviceOrderListForm'].elements['estimator'].value = "";	
	document.forms['serviceOrderListForm'].elements['activeStatus'].checked =false;
	var fld=document.forms['serviceOrderListForm'].elements['role'];	
	var values = [];
	for (var i = 0; i < fld.options.length; i++) {
	  if (fld.options[i].selected) {
		  fld.options[i].selected=false;
	  }
	}	

}


</script>

<style>
form { margin-top:-40px; !margin-top:-5px; }
div#main { margin:-5px 0 0; }
div#content { padding:0px; min-height:50px; margin-left:0px; }
span.pagelinks { display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-18px;padding:2px 0px;text-align:right;width:100%; }
div.error, span.error, li.error, div.message { width:450px; margin-top:0px; }
form { margin-top:-40px; !margin-top:-5px; }
div#main { margin:-5px 0 0;}
.table thead th, .tableHeaderTable td { padding: 1px 3px 1px 5px; }
.table td, .table th, .tableHeaderTable td { font-family: arial,verdana; font-size: 10px; padding: 0.3em; }	
.table tr.even { background-color: #E0FFFF;}
.key_so_dashboard { background: url("images/key_so_dashboard.jpg") no-repeat scroll 0 0 transparent; cursor: default; height: 22px; margin-left:22%; }
</style>

<script>

function goToSearch(){	
	var fld=document.forms['serviceOrderListForm'].elements['role'];
		//var opt=opt1.options.selectedIndex;
		var values = [];
		for (var i = 0; i < fld.options.length; i++) {
		  if (fld.options[i].selected) {
		    values.push(fld.options[i].value);
		  }
		}	
		document.forms['serviceOrderListForm'].elements['roleArray'].value=values;
		document.forms['serviceOrderListForm'].elements['recordLimitForm'].value = document.getElementById('recordLimit').value
	 	document.forms['serviceOrderListForm'].action = 'searchSoDashboardList.html';
     	document.forms['serviceOrderListForm'].submit();
}

function extractFunction(){
	var fld=document.forms['serviceOrderListForm'].elements['role'];
	var values = [];
	for (var i = 0; i < fld.options.length; i++) {
	  if (fld.options[i].selected) {
	    values.push(fld.options[i].value);
	  }
	}	
	document.forms['serviceOrderListForm'].elements['roleArray'].value=values;
	document.forms['serviceOrderListForm'].elements['recordLimitForm'].value = document.getElementById('recordLimit').value
	document.forms['serviceOrderListForm'].action = 'soDashboardExtract.html';
	document.forms['serviceOrderListForm'].submit(); 		
	}
function showHideCityCountry(){
	var cityCountryZipVal = document.forms['serviceOrderListForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
	document.forms['serviceOrderListForm'].elements['originCityOrZip'].value='';
	document.forms['serviceOrderListForm'].elements['destinationCityOrZip'].value='';
	document.forms['serviceOrderListForm'].elements['originCountry'].value ="";
	document.forms['serviceOrderListForm'].elements['destinationCountry'].value ="";
}

function selectSearchField(){
	
	 var originCityCountryOrZip="";
	    var destinationCityCountryOrZip="";
	var shipNumber=document.forms['serviceOrderListForm'].elements['shipNumber'].value;
    var bookingAgentShipNumber=document.forms['serviceOrderListForm'].elements['bookingAgentShipNumber'].value ;
	var registrationNumber=document.forms['serviceOrderListForm'].elements['registrationNumber'].value;	
	var status=document.forms['serviceOrderListForm'].elements['activeStatus'].checked;	
	var job1=document.forms['serviceOrderListForm'].elements['job1'].value;
	var statusDropDown=document.forms['serviceOrderListForm'].elements['status'].value;	
	var serviceOrderMoveType=document.forms['serviceOrderListForm'].elements['serviceOrderMoveType'].value; 
	var coordinator=document.forms['serviceOrderListForm'].elements['coordinator'].value;	
	var firstName=document.forms['serviceOrderListForm'].elements['firstName'].value;
	var lastName=document.forms['serviceOrderListForm'].elements['lastName'].value;
	var billToName=document.forms['serviceOrderListForm'].elements['billToName'].value;
	<configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">	
	var certificateNumber=document.forms['serviceOrderListForm'].elements['certificateNumber'].value;
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">	
	var daShipmentNumber=document.forms['serviceOrderListForm'].elements['daShipmentNumber'].value;
	</configByCorp:fieldVisibility>
	var cDivision=document.forms['serviceOrderListForm'].elements['companyDivision'].value;
	var salesMan=document.forms['serviceOrderListForm'].elements['salesMan'].value;
   var estimator=document.forms['serviceOrderListForm'].elements['estimator'].value; 

	var cityCountryZipVal = document.forms['serviceOrderListForm'].elements['cityCountryZipOption'].value;
	 if(cityCountryZipVal== 'City' || cityCountryZipVal == 'Zip'){
	        originCityCountryOrZip = document.forms['serviceOrderListForm'].elements['originCityOrZip'].value;
	        destinationCityCountryOrZip = document.forms['serviceOrderListForm'].elements['destinationCityOrZip'].value;
	 }else{
           originCityCountryOrZip = document.forms['serviceOrderListForm'].elements['originCountry'].value;
           destinationCityCountryOrZip = document.forms['serviceOrderListForm'].elements['destinationCountry'].value;
	  }
	if(status==false) {
		
    if(serviceOrderMoveType=='' && statusDropDown =='' && shipNumber=='' && bookingAgentShipNumber=='' &&  registrationNumber=='' && status==false && job1=='' && coordinator=='' && firstName=='' && lastName=='' && billToName=='' && cDivision =='' && originCityCountryOrZip=='' && destinationCityCountryOrZip=='' && salesMan=='' && certificateNumber=='' && daShipmentNumber=='' && estimator==''){
		alert('Please select any one of the search criteria!');	
		return false;	
	}else{
		return true	;			
	}
	}else{
		return true	;
	}
}


</script>

</head>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:20px" 
        onclick="location.href='<c:url value="/editServiceOrder.html?id=${customerFile.id}"/>'"
         value="<fmt:message key="button.add"/>"/>   
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>
<s:hidden name="fileID"  id= "fileID" value=""/>

<s:form id="serviceOrderListForm" name="serviceOrderListForm" action="searchSoDashboardList.html" method="post" > 
<s:hidden name="soTab"   value="${soTab}"/>
<c:set var="soTab"  value="${soTab}"/>
<s:hidden name="usertype" value="${usertype}" />
<c:set var="usertype"  value="${usertype}"/>
<s:hidden name="userPortalCheckType"   value="${userPortalCheckType}"/>
<c:set var="userPortalCheckType"  value="${userPortalCheckType}"/>
<s:hidden name="soSortOrder"   value="${soSortOrder}"/>
<c:set var="soSortOrder"  value="${soSortOrder}"/>
<s:hidden name="quickFileCabinetView"   value="${quickFileCabinetView}"/>
<c:set var="quickFileCabinetView"  value="${quickFileCabinetView}"/>
<s:hidden name="orderForJob"   value="${orderForJob}"/>
<s:hidden name="roleArray"   value="${roleArray}"/>
<c:set var="orderForJob"  value="${orderForJob}"/>
<configByCorp:fieldVisibility componentId="component.field.customerfile.socialSecurityNumber">
<s:hidden name="serviceOrder.socialSecurityNumber" />
<s:hidden name="serviceOrder.gbl" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="certificateNumberVal" value="N" />
<c:set var="certificateNumberColSpan" value="5" />
<configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">		
	<c:set var="certificateNumberVal" value="Y" />
	<c:set var="certificateNumberColSpan" value="6" />
</configByCorp:fieldVisibility>
<s:hidden name="recordLimitForm" />
<c:if test="${!checkAccessQuotation}">
<s:hidden name="serviceOrderMoveType" />
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk" >&nbsp;</div>
		
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%;margin-top:2px;" border="1" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
	<th><fmt:message key="serviceOrder.shipNumber"/></th>
	<th>External S/O#</th>
	<th><fmt:message key="serviceOrder.registrationNumber"/></th>
	<th>Last/Company&nbsp;Name</th>
	<th>First&nbsp;Name</th>
	<th><fmt:message key="serviceOrder.coordinator"/></th>
	<th><fmt:message key="serviceOrder.job"/></th>
	<c:if test="${certificateNumberVal=='Y'}">	
	<th>Certificate&nbsp;#</th>	
	</c:if>
	<th style="padding-left:15px;">Role</th>
	<th></th>
	</tr>
	</thead>	
		<tbody>
		<tr>			
			<td>
			    <s:textfield name="shipNumber" size="9" required="true" cssClass="input-text" cssStyle="width:125px"/>
			</td>			
				<td>
				    <s:textfield name="bookingAgentShipNumber" size="12"  cssStyle="width:125px" required="true" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
				</td>
					
			<td>
			    <s:textfield name="registrationNumber" size="6" required="true" cssClass="input-text" cssStyle="width:125px"/>
			</td>
			<td>
			    <s:textfield name="lastName" size="6" required="true" cssClass="input-text" cssStyle="width:125px"/>
			</td>
			<td>
			    <s:textfield name="firstName" size="6" required="true" cssClass="input-text" cssStyle="width:125px"/>
			</td>			
			<td  align="left">
				<c:if test="${coordinatr == null || coordinatr == ''}" >
			    	<s:select cssClass="list-menu" name="coordinator" list="%{coord}" cssStyle="width:125px" headerKey="" headerValue=""/>
			    </c:if>
			    <c:if test="${coordinatr != '' && coordinatr != null}" >
			    	<s:select cssClass="list-menu" name="coordinator" list="%{coord}" cssStyle="width:125px" headerKey="" headerValue="" value="${coordinatr}"/>
			    </c:if>
			</td>
			<td>
			    <s:select cssClass="list-menu" name="job1" list="%{job}" cssStyle="width:125px" headerKey="" headerValue="" />
			</td>
			<c:if test="${certificateNumberVal=='Y'}">	
			<td>
			<s:textfield name="certificateNumber" id="certificateNumber" cssStyle="width:125px;" cssClass="input-text" />
			</td>
			</c:if>			
						
				<td rowspan="2" style="text-align:center;">
			    <select multiple class="textarea" style="height:54px; width:55px; padding:3px;" name="role" value="${role}">			       
			       <option value="BA">BA</option>
			       <option value="BR">BR</option>
			       <option value="DA">DA</option>
			       <option value="FR">FR</option>
			       <option value="HA">HA</option>
			       <option value="NA">NA</option>
			     	<option value="OA">OA</option>      
			       <option value="SO">SOA</option>
			       <option value="SD">SDA</option>			       
			    </select>
			</td>
			
			<td rowspan="2" width="120">
			<table style="margin:0px;paddding:0px;">
			<tr>
			<c:if  test="${compDivFlag == 'Yes'}">
				<td style="border:0px solid #FFFFFF;font-size: 11px;vertical-align:bottom;width:119px;padding-left:5px;" class="listwhitetext">
				<s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 0px 10px 0px;"/>&nbsp;Active Status&nbsp;
				</td>
			</c:if>
			</tr>
			</table>
			<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" key="button.search" onclick="return goToSearch();"/>   
    		<input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> </td>		
			</tr>
			
			<tr>			
			<td style="border:0px solid #FFFFFF;font-size: 11px;"  class="listwhitetext"><fmt:message key="serviceOrder.status"/><br>
			<s:select cssClass="list-menu" name="status" list="%{JOB_STATUS}" cssStyle="width:126px" headerKey="" headerValue="" />
			 </td>
			 <c:if  test="${compDivFlag == 'Yes'}">
			<td style="border:0px solid #FFFFFF;font-size: 11px;"  class="listwhitetext">Company Division<br>
			    <s:select cssClass="list-menu" name="companyDivision" list="%{companyDivis}" cssStyle="width:126px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			</c:if>
			<c:if  test="${compDivFlag != 'Yes'}">
			<s:hidden name="companyDivision"/>
			</c:if>
			<td colspan="${certificateNumberColSpan}">
			<table style="margin:0px;padding:0px;border:none;">			
			<tr>
				<td style="border:0px solid #FFFFFF;font-size: 11px;"  class="listwhitetext">Bill To Name<br>
				
					<s:textfield name="billToName" id="billToName1" size="20" cssStyle="width:120px;margin-top:2px;!width:125px;margin-left:-2px;" cssClass="input-text" onkeyup="autoCompleterAjaxCallBilltoName()"/>
				</td>
				
				<c:if test="${certificateNumberVal=='Y'}">	
				<td style="border:0px solid #FFFFFF;font-size: 11px;"  class="listwhitetext">DA Shipment No<br>				
					<s:textfield name="daShipmentNumber" id="daShipmentNumber" cssStyle="width:125px;margin-top:2px;" cssClass="input-text" />
				</td>	
				</c:if>	
			 <td style="border:none;">
			 <table style="margin:0px;padding:0px;border:none; float:left;" cellpadding="0" cellspacing="0">
			 <tr>
			 	<td style="border:none;font-size: 11px;" class="listwhitetext">
				O/D Options&nbsp;<br>
				<s:select name="cityCountryZipOption" id="cityCountryZipOption" list="%{cityCountryZipSearchOption}" cssClass="list-menu" cssStyle="width:125px;margin-right:5px;margin-top:2px;" onchange="showHideCityCountry();"/>
 				</td>
			 </tr>
			 </table>
			 </td>
			 <td style="border:none;padding:0px;">
			<table id="showHideCityOrZip" style="margin:0px;padding:0px;border:none; float:left;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="border:none;font-size: 11px;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:textfield name="originCityOrZip" cssClass="input-text" cssStyle="width:125px;margin-right:5px;margin-top:2px;"/>
					</td>
					<td style="border:none;font-size: 11px;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:textfield name="destinationCityOrZip" cssClass="input-text" cssStyle="width:125px;margin-right:0px;margin-top:2px;"/>
					</td>
				</tr>
			</table>
			 </td>
			 <td style="border:none;padding:0px;">
			<table id="showHideCountry" style="margin:0px;padding:0px;border:none; float:left;" cellpadding="0" cellspacing="0">
				
					<td style="border:none;font-size: 11px;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:select name="originCountry" list="%{ocountry}" cssClass="list-menu" cssStyle="width:125px;margin-right:5px;margin-top:2px;" headerKey="" headerValue="" /></td>
					<td style="border:none;font-size: 11px;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:select name="destinationCountry" list="%{dcountry}" cssClass="list-menu" cssStyle="width:125px;margin-right:5px;margin-top:2px;" headerKey="" headerValue="" /></td>
				
			</table>
			 </td>
			 <td style="border:none;">
			<table id="sales" style="margin:0px;padding:0px;border:none; float:left;" cellpadding="0" cellspacing="0">
				
					<td style="border:none;font-size: 11px;" class="listwhitetext">
					Sales Person&nbsp;<br>
					<s:select cssClass="list-menu" name="salesMan" list="%{sale}" cssStyle="width:125px;margin-top:2px;" headerKey="" headerValue=""/>
				
		</table>
		 </td>
			 <td style="border:none;">
			<table style="margin:0px;padding:0px;border:none;float:left;" cellpadding="0" cellspacing="0">
			<tr>			
			<c:if test="${checkAccessQuotation}">
			<td style="border:none;font-size: 11px;padding-left:0px;"  class="listwhitetext">
			Move Type&nbsp;<br>
			<s:select id="moveType" cssClass="list-menu" name="serviceOrderMoveType" list="%{moveTypeList}"  cssStyle="width:125px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			</c:if>
			<td style="border:none;font-size: 11px;padding-left:0px;"  class="listwhitetext">
			Consultant&nbsp;<br>
			<s:select cssClass="list-menu" name="estimator" list="%{sale}" cssStyle="width:125px;margin-top:2px;" headerKey="" headerValue="" />
			</td>			
			<td style="border:0px solid #FFFFFF;font-size: 11px;padding-left:5px;"  class="listwhitetext">Record Limit<br>
			<s:select cssClass="list-menu" id="recordLimit" name="recordLimit" list="%{recordLimitList}" cssStyle="width:125px;margin-top:2px;"/>
			</td>
			</tr>
			</table>
			</td>
			
			</tr>
			</table>
			</td>
			<c:if  test="${compDivFlag != 'Yes'}">
				<td style="border:0px solid #FFFFFF;font-size: 11px;vertical-align:bottom;padding-left:5px;" class="listwhitetext">Active Status<br>
				<s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 0px 13px 25px;"/>
				</td>
			</c:if>
			</tr>
			
			
			
			
		</tbody>
	</table>
	<div style="!margin-top:7px;"></div>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:set name="serviceOrdersExt" value="serviceOrders" scope="request"/>

<div style="float:left; " id="newmnav">
		  <ul>
		    <li style="background:#FFF" id="newmnav1"><a class="current" href="soDashboard.html"><span>SO Dashboard</span></a></li>
		     <li><a href="serviceOrders.html?isSoListShow=yes"><span>Service Order List</span></a></li>
		  </ul>
		</div>
		<div id="KeyDiv" class="key_so_dashboard" style="float:left;width:270px;margin-left:15px;">&nbsp;</div>
		
		<div id="showExtract" style="width: 140px; float: right;">
		<img id="" src="${pageContext.request.contextPath}/images/Dashboard-btn.jpg" onclick="extractFunction();" />
		</div>	
		<div class="spn" style="!line-height:0.3em;">&nbsp;</div> 
<!--start -->
<display:table name="soDashboardList" class="table" requestURI="" id="individualItem" export="false"  pagesize="20" style="width:100%;">
<c:if test="${empty individualItem.serviceOrderMoveType || individualItem.serviceOrderMoveType=='BookedMove'}">
<c:choose>
			<c:when test='${empty soTab}'>
            <c:if test="${individualItem.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editDspDetails.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${individualItem.job!='RLO' && individualItem.job!='OFF'}">
           <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" /> 
                  </a> 
                </display:column>  
			</c:if>  
			<c:if test="${individualItem.job=='OFF'}">
           <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" /> 
                  </a> 
                </display:column>  
			</c:if>
			</c:when>
			<c:when test='${soTab =="containers.html?id="}'>
           <c:if test="${individualItem.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editDspDetails.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" />
                  </a> 
                </display:column> 
			</c:if>
			<c:if test="${individualItem.job!='RLO'}">
				 <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">                  
                  <c:if test="${forwardingTabVal!='Y'}">
						<a onclick="javascript:window.open('containers.html?id=${individualItem.id}','_blank');" >
						<c:out value="${individualItem.shipnumber}" />
						</a>
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
					<a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
						<c:out value="${individualItem.shipnumber}" />					
					</a>
				</c:if>                   
                </display:column> 
			</c:if>
             </c:when>
			<c:when test='${soTab =="editMiscellaneous.html?id="}'>
           <c:if test="${individualItem.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editDspDetails.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" />
                  </a> 
                </display:column> 
			</c:if>
			<c:if test="${individualItem.job=='DOM' || individualItem.job=='STO' || individualItem.job=='3RD' || individualItem.job=='EUR' || individualItem.job=='LOG' || individualItem.job=='ASS'}">
				 <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${individualItem.job!='RLO' && individualItem.job!='DOM' && individualItem.job!='STO' && individualItem.job!='3RD' && individualItem.job!='EUR' && individualItem.job!='LOG' && individualItem.job!='OFF' && individualItem.job!='ASS'}">
           <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" />
                  </a> 
                </display:column>  
			</c:if> 
			<c:if test="${individualItem.job=='OFF'}">
           <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
               <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}','_blank');" >
                 <c:out value="${individualItem.shipnumber}" /> 
                  </a> 
                </display:column>  
			</c:if>
             </c:when>
            <c:when test='${soTab =="claims.html?id="}'>
           <c:if test="${individualItem.job=='RLO'}">
           <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		       <a onclick="javascript:window.open('editDspDetails.html?id=${individualItem.id}','_blank');" >
        	   <c:out value="${individualItem.shipnumber}" />
            	</a>
            	</display:column> 
		       </c:if>
		     <c:if test="${individualItem.job!='RLO'}">
		     <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		       <a onclick="javascript:window.open('${soTab}${individualItem.id}','_blank');" >
        	   <c:out value="${individualItem.shipnumber}" />
            	</a>
            </display:column>
		       </c:if>
             </c:when>
             <c:when test='${soTab =="editServiceOrderUpdate.html?id="}'>
           <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		       <a onclick="javascript:window.open('${soTab}${individualItem.id}','_blank');" >
        	   <c:out value="${individualItem.shipnumber}" />
            	</a>
            </display:column>
             </c:when>
			<c:when test='${soTab =="customerWorkTickets.html?id="}'>
		       <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		       <a onclick="javascript:window.open('${soTab}${individualItem.id}','_blank');" >
        	   <c:out value="${individualItem.shipnumber}" />
            	</a>
            </display:column>
             </c:when>
             <c:when test='${soTab =="editBilling.html?id="}'>
		       <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		       <a onclick="javascript:window.open('${soTab}${individualItem.id}','_blank');" >
        	   <c:out value="${individualItem.shipnumber}" />
            	</a>
            </display:column>
             </c:when>
             <c:when test='${soTab =="accountLineList.html?sid=" || soTab =="pricingList.html?sid="}'>
             <c:if test="${individualItem.serviceOrderMoveType!='Quote'}">
		       <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		       <a onclick="javascript:window.open('${soTab}${individualItem.id}','_blank');" >
        	   <c:out value="${individualItem.shipnumber}" />
            	</a>
            </display:column>
            </c:if>
             </c:when>
			<c:otherwise>
		      <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
		      <c:if test="${soTab=='editTrackingStatus.html?from=list&id='}">
		      		<c:if test="${individualItem.job=='RLO'}">
		      		<a onclick="javascript:window.open('editDspDetails.html?id=${individualItem.id}','_blank');" >
                         <c:out value="${individualItem.shipnumber}"/>
                         </a> 
              		</c:if>
              		<c:if test="${individualItem.job!='RLO' && individualItem.job!='OFF'}">
    	                 <a onclick="javascript:window.open('${soTab}${individualItem.id}','_blank');" >
        	              <c:out value="${individualItem.shipnumber}" />
            	          </a> 
              		</c:if>
              		<c:if test="${individualItem.job=='OFF'}">
    	                 <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}','_blank');" >
        	              <c:out value="${individualItem.shipnumber}" />
            	          </a> 
              		</c:if>
              </c:if>
		      		
              </display:column>
		    </c:otherwise>
	  </c:choose>
</c:if>

<c:if test="${individualItem.serviceOrderMoveType=='Quote'}">
             <display:column sortable="true" style="width:50px" title="SO#" sortProperty="shipnumber">
             <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}','_blank');" >
             <c:out value="${individualItem.shipnumber}" />
             </a>
             </display:column>
</c:if>
					<c:if test="${not empty job1 }">
					<c:if test="${fn:indexOf(miscVLJob, job1)>1}">
					<display:column sortable="true" style="width:20px" title="Reg #" property="registrationnumber"/>
					</c:if>
					</c:if>
					<c:if test="${empty job1 }">
					<display:column sortable="true" style="width:20px" title="Reg #" property="registrationnumber"/>
					</c:if>
					<display:column sortable="true" style="width:10px" title="Last<br>Name" property="lastName"/>
					<display:column sortable="true" style="width:10px" title="First<br>Name" property="firstName"/>
					<display:column sortable="true" style="width:20px" title="Role<br>Defined" property="role"/>
					<display:column sortable="true" style="width:20px" title="Job" property="job"/>
					<display:column sortable="true" style="width:20px" title="Service" property="shipmenttype" maxLength="19"/>					
					<display:column sortable="true" style="width:20px" title="Mode" property="mode"/>
					<display:column sortable="true" style="width:20px" title="Routing" property="routing"/>
					<display:column sortable="true" style="width:20px" title="Commodity" property="commodity"/>
					<display:column sortable="true" style="width:20px" title="SO Status" property="status"/>
					<display:column sortable="true" style="width:10px;text-align:center;" title="Notes" >
					<img id="countVipDetailNotesImage${individualItem.id}" src="${pageContext.request.contextPath}/images/notes_16.png" 
										onclick="javascript:window.open('notess.html?id=${individualItem.id}&notesId=${individualItem.shipnumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes','_blank');" />
					</display:column>
					<display:column sortable="true" style="width:10px;text-align:center;" title="Docs" >
						  <c:if test="${quickFileCabinetView=='Document Centre'}">
					<img id="countVipDetailMyFileImage${individualItem.id}" src="${pageContext.request.contextPath}/images/file-cabinet-16.png" 
							onclick="javascript:window.open('myFilesDocType.html?id=${individualItem.id}&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false','_blank');" />
					 </c:if>
					  <c:if test="${quickFileCabinetView!='Document Centre'}">
					<img id="countVipDetailMyFileImage${individualItem.id}" src="${pageContext.request.contextPath}/images/file-cabinet-16.png" 
										onclick="javascript:window.open('myFiles.html?id=${individualItem.id}&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false','_blank');" />
				       </c:if>
					</display:column>
					<display:column sortable="true" style="width:10px;text-align:center;" title="Tasks" >
					<img id="countVipDetailTasksImage${individualItem.id}" src="${pageContext.request.contextPath}/images/task-16.png" 
										onclick="javascript:window.open('tasks.html?id=${individualItem.id}&tableName=serviceorder','_blank');" />
					</display:column>
					<c:choose>						
					 <c:when  test="${individualItem.sDFlag!='NA' && individualItem.survAT =='A'}">
					 <display:column sortable="true" style="width:20px" title="Survey" sortProperty="sortSurveyDt"> 
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.actualSurveyDate','_blank');" >
					 <c:out value="${individualItem.sD}"></c:out>
					 </display:column>
					 </c:when >
					 <c:when test="${individualItem.sDFlag!='NA' && individualItem.survAT=='T' }">
					 <c:choose>
					 <c:when test="${individualItem.sDFlag=='DT' }">
					  <display:column sortable="true"  style="background:#FFFF99;width:20px;" title="Survey" sortProperty="sortSurveyDt">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.actualSurveyDate','_blank');" >
					  <c:out value="${individualItem.sD}"></c:out>
					  </a>
					  </display:column>
					  </c:when>
					  <c:when test="${individualItem.sDFlag=='OD' }">
					  <display:column sortable="true" style="background:#FF6666;width:20px;" title="Survey" sortProperty="sortSurveyDt">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.actualSurveyDate','_blank');" >
					  <c:out value="${individualItem.sD}"></c:out>
					  </a>
					  </display:column>
					  </c:when>
					  <c:when test="${individualItem.sDFlag=='' && individualItem.sD!=null}">
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Survey" sortProperty="sortSurveyDt">
					  <c:if test="${individualItem.sD==''}">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.survey','_blank');" >
					  <c:out value="_"></c:out>
					  </a>
					  </c:if>
					  <c:if test="${individualItem.sD!=''}">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.actualSurveyDate','_blank');" >
					  <c:out value="${individualItem.sD}"></c:out>
					  </a>
					  </c:if>
					  </display:column>
					  </c:when>					
					 <c:when test="${empty individualItem.sDFlag }">
					 <display:column sortable="true" style="background:#FFFFFF;width:20px;text-align:center;" title="Survey" sortProperty="sortSurveyDt">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.survey','_blank');" >
					  <c:out value="_"></c:out>
					  </a>
					 </display:column>					 
					 </c:when>
					 </c:choose>
					 </c:when>
					 <c:otherwise>
					  <c:if test="${individualItem.sDFlag=='NA' }">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Survey" sortProperty="sortSurveyDt">
					  <c:if test="${individualItem.sD==''}">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.survey','_blank');" >
					  <c:out value="_"></c:out>
					  </a>
					  </c:if>
					  <c:if test="${individualItem.sD!=''}">
					  <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.actualSurveyDate','_blank');" >
					  <c:out value="${individualItem.sD}"></c:out>
					  </a>
					  </c:if>
					  </display:column>				 				 
					 </c:if>
					  <c:if test="${individualItem.sDFlag==''}">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Survey" sortProperty="sortSurveyDt">
					 <a onclick="javascript:window.open('editCustomerFile.html?id=${individualItem.cid}&field=customerFile.survey','_blank');" >
					  <c:out value="_"></c:out>
					  </a>
					 </display:column>					 
					 </c:if>
					 </c:otherwise>					 
					 </c:choose>
					 <c:choose>
					 <c:when test="${individualItem.estweight=='NA'}">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Est wt/vol" >
					 <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.estimatedNetWeightKilo','_blank');" >					 
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.estweight=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Est wt/vol" >
					  <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.estimatedNetWeightKilo','_blank');" >					 
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.estweight=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Est wt/vol" >
					 <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.estimatedNetWeightKilo','_blank');" >					 
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="text-align: right;width:25px;text-align:center;" title="Est wt/vol" >
					<c:if test="${weightUnit=='Kgs'}">
					<a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.estimatedNetWeightKilo&field1=miscellaneous.netEstimateCubicMtr','_blank');" >
					<c:if test="${not empty individualItem.estweight}">  
					<fmt:formatNumber pattern="#" type="number"  maxFractionDigits="0" minFractionDigits="0"
                  		groupingUsed="true" value="${individualItem.estweight}"  />
                  </c:if>
                  <c:if test="${empty individualItem.estweight}"><c:out value="-"></c:out></c:if>
                  <c:if test="${not empty individualItem.estVolume}"><c:out value="/"></c:out> 
                  <fmt:formatNumber pattern="#" type="number"  maxFractionDigits="0" minFractionDigits="0"
                  groupingUsed="true" value="${individualItem.estVolume}"  />
                  </c:if>
                  <c:if test="${ empty individualItem.estVolume}"><c:out value="/"></c:out>
                  <c:out value="-"></c:out>
                  </c:if>
                  </a>
                  </c:if>
                  <c:if test="${weightUnit=='Lbs'}">
					<a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.estimatedNetWeight&field1=miscellaneous.netEstimateCubicFeet','_blank');" >
					<c:if test="${not empty individualItem.estweight}">  
					<fmt:formatNumber pattern="#" type="number"  maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${individualItem.estweight}"  /></c:if>
                  <c:if test="${empty individualItem.estweight}"><c:out value="-"></c:out></c:if>
                  <c:if test="${not empty individualItem.estVolume}"><c:out value="/"></c:out> 
                  <fmt:formatNumber pattern="#" type="number"  maxFractionDigits="0" minFractionDigits="0"
                  groupingUsed="true" value="${individualItem.estVolume}"  />
                  </c:if>
                  <c:if test="${ empty individualItem.estVolume}"><c:out value="/"></c:out><c:out value="-"></c:out></c:if>
                  </a>
                  </c:if>	
					  </display:column>				 
					 </c:otherwise>
					 </c:choose>
					<c:choose>						
					 <c:when  test="${individualItem.ldFlag!='NA' && individualItem.loadAT =='A'}">
					 <display:column sortable="true" style="width:20px" title="Loading" sortProperty="sortLoadingDt"> 
					 <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=loadA','_blank');" >
					 <c:out value="${individualItem.ld}"></c:out>
					 </display:column>
					 </c:when >
					 <c:when test="${individualItem.ldFlag!='NA' && individualItem.loadAT=='T' }">
					 <c:choose>
					 <c:when test="${individualItem.ldFlag=='DT' }">
					 <display:column sortable="true"  style="background:#FFFF99;width:20px;" title="Loading" sortProperty="sortLoadingDt">
					 <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=loadA','_blank');" >
                     <c:out value="${individualItem.ld}"></c:out>
                     </a> 
					 </display:column>
					 </c:when>
					  <c:when test="${individualItem.ldFlag=='OD' }">
					  <display:column sortable="true" style="background:#FF6666;width:20px;" title="Loading" sortProperty="sortLoadingDt">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=loadA','_blank');" >
                          <c:out value="${individualItem.ld}"></c:out>
                      </a> 
					  </display:column>
					  </c:when>
					   <c:when test="${individualItem.ldFlag=='' && individualItem.ld!=null}">
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Loading" sortProperty="sortLoadingDt">
					 	<c:if test="${individualItem.ld==''}">
					 	<a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.beginLoad','_blank');" >
                          <c:out value="_"></c:out>
                      	</a>
                      	</c:if>
                      	<c:if test="${individualItem.ld!=''}">
					 	<a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=loadA','_blank');" >
                          <c:out value="${individualItem.ld}"></c:out>
                      	</a>
                      	</c:if>
					  </display:column>
					 </c:when>					
					 <c:when test="${empty individualItem.ldFlag }">
					 <display:column sortable="true" style="background:#FFFFFF;width:20px;text-align:center;" title="Loading" sortProperty="sortLoadingDt">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.beginLoad','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
					 </display:column>					 
					 </c:when>
					 </c:choose>
					 </c:when>
					 <c:otherwise>
					  <c:if test="${individualItem.ldFlag=='NA' }">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Loading" sortProperty="sortLoadingDt">
					  <c:if test="${individualItem.ld==''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.beginLoad','_blank');" >
					  <c:out value="_"></c:out>
					  </a>
					  </c:if>
					  <c:if test="${individualItem.ld!=''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.loadA','_blank');" >
					  <c:out value="_"></c:out></a>
					  </c:if>
					 </display:column>					 
					 </c:if>
					 </c:otherwise>					 
					 </c:choose>
					 <c:choose>
					 <c:when test="${individualItem.actualweight=='NA'}">					 
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Act wt/vol" >
					 <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.actualNetWeightKilo','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.actualweight=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Act wt/vol" >
					  <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.actualNetWeightKilo','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.actualweight=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Act wt/vol" >
					 <a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.actualNetWeightKilo','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>					 
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="text-align: right;width:20px;text-align:center;" title="Act wt/vol" >
					<c:if test="${weightUnit=='Kgs'}">
					<a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.actualNetWeightKilo&field1=miscellaneous.netActualCubicMtr','_blank');" >
					<c:if test="${not empty individualItem.actualweight}"> <fmt:formatNumber pattern="#" maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${individualItem.actualweight}"  /></c:if>
					<c:if test="${empty individualItem.actualweight}"><c:out value="-"></c:out></c:if><c:if test="${not empty individualItem.actualVolume}"><c:out value="/"></c:out><fmt:formatNumber type="number"  pattern="#" maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${individualItem.actualVolume}"  /></c:if>
					<c:if test="${empty individualItem.actualVolume}"><c:out value="/"></c:out><c:out value="-"></c:out></c:if>
					</a>
					</c:if>
					<c:if test="${weightUnit=='Lbs'}">
					<a onclick="javascript:window.open('editServiceOrderUpdate.html?id=${individualItem.id}&field=miscellaneous.actualNetWeight&field1=miscellaneous.netActualCubicFeet','_blank');" >
					<c:if test="${not empty individualItem.actualweight}"> <fmt:formatNumber pattern="#" maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${individualItem.actualweight}"  /></c:if>
					<c:if test="${empty individualItem.actualweight}"><c:out value="-"></c:out></c:if><c:if test="${not empty individualItem.actualVolume}"><c:out value="/"></c:out><fmt:formatNumber type="number"  pattern="#" maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${individualItem.actualVolume}"  /></c:if>
					<c:if test="${empty individualItem.actualVolume}"><c:out value="/"></c:out><c:out value="-"></c:out></c:if>
					</a>
					</c:if>
					  	</display:column>				 
					 </c:otherwise>
					 </c:choose>
					 <c:if test="${not empty job1 }">
					<c:if test="${fn:indexOf(miscVLJob, job1)>1}">
					<c:choose>
					 <c:when test="${individualItem.driver1=='NA'}">					 
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Driver" >
					 <c:if test="${individualItem.driver==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.driver!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="${individualItem.driver}"></c:out>
					 </a>
					 </c:if>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.driver1=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Driver" >
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.driver1=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Driver" >
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>					 
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Driver" >
					 <c:if test="${individualItem.driver==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.driver!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="${individualItem.driver}"></c:out>
					 </a>
					 </c:if>
					 </display:column>				 
					 </c:otherwise>
					 </c:choose>					
					</c:if>
					</c:if>
					<c:if test="${empty job1 }">
					<c:choose>
					 <c:when test="${individualItem.driver1=='NA'}">					 
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Driver" >
					 <c:if test="${individualItem.driver==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.driver!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="${individualItem.driver}"></c:out>
					 </a>
					 </c:if>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.driver1=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Driver" >
					  <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.driver1=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Driver" >
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>					 
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Driver" >
					  <c:if test="${individualItem.driver==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.driver!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.driverName','_blank');" >
					 <c:out value="${individualItem.driver}"></c:out>
					 </a>
					 </c:if>
					  	</display:column>				 
					 </c:otherwise>
					 </c:choose>					
					</c:if>
					<c:if test="${not empty job1 }">
					<c:if test="${fn:indexOf(miscVLJob, job1)>1}">
					  <c:choose>				 
					 <c:when test="${individualItem.truck1=='NA'}">					 
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Truck" >
					 <c:if test="${individualItem.truck==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.truck!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="${individualItem.truck}"></c:out>
					 </a>
					 </c:if>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.truck1=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Truck" >
					  <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.truck1=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Truck" >
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>					 
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="text-align: right;width:20px;text-align:center;" title="Truck" >
					  <c:if test="${individualItem.truck==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.truck!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="${individualItem.truck}"></c:out>
					 </a>
					 </c:if>
					  	</display:column>				 
					 </c:otherwise>
					 </c:choose>
					</c:if>
					</c:if>
					<c:if test="${empty job1 }">
					  <c:choose>				 
					 <c:when test="${individualItem.truck1=='NA'}">					 
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Truck" >
					 <c:if test="${individualItem.truck==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.truck!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="${individualItem.truck}"></c:out>
					 </a>
					 </c:if>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.truck1=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Truck" >
					  <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.truck1=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Truck" >
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					</display:column>					 
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="text-align: right;width:20px;text-align:center;" title="Truck" >
					  <c:if test="${individualItem.truck==''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="_"></c:out>
					 </a>
					 </c:if>
					 <c:if test="${individualItem.truck!=''}">
					 <a onclick="javascript:window.open('editMiscellaneous.html?id=${individualItem.id}&field=miscellaneous.carrierName','_blank');" >
					 <c:out value="${individualItem.truck}"></c:out>
					 </a>
					 </c:if>
					  	</display:column>				 
					 </c:otherwise>
					 </c:choose>
					</c:if>
					<c:choose>						
					 <c:when  test="${individualItem.depFlag!='NA' && individualItem.dptF =='A'}">
					 <display:column sortable="true" style="width:20px" title="Departure" sortProperty="sortDpt"> 
					 <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="${individualItem.dep}" />
					 </a>
					 </display:column>
					 </c:when >
					 <c:when test="${individualItem.depFlag!='NA' && individualItem.dptF=='T' }">
					 <c:choose>
					 <c:when test="${individualItem.depFlag=='DT' }">
					 <display:column sortable="true"  style="background:#FFFF99;width:20px;" title="Departure" sortProperty="sortDpt">
					 <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="${individualItem.dep}" />
					 </a>
					 </display:column>
					 </c:when>
					  <c:when test="${individualItem.depFlag=='OD' }">
					  <display:column sortable="true" style="background:#FF6666;width:20px;" title="Departure" sortProperty="sortDpt">
					 <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="${individualItem.dep}" />
					 </a>
					  </display:column>
					 </c:when>
					   <c:when test="${individualItem.depFlag=='' && individualItem.dep!=null}">
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Departure" sortProperty="sortDpt">
					 	 <c:if test="${individualItem.dep==''}">
					 	<a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 	<c:out value="_" />
					 	</c:if>
					 	<c:if test="${individualItem.dep!=''}">
					 	<a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 	<c:out value="${individualItem.dep}" />
					 	</c:if>
					 </a>
					  </display:column>
					 </c:when>					
					 <c:when test="${empty individualItem.depFlag }">
					 <display:column sortable="true" style="background:#FFFFFF;width:20px;text-align:center;" title="Departure" sortProperty="sortDpt">
					  <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="_" />
					 </a>
					 </display:column>					 
					 </c:when>
					 </c:choose>
					 </c:when>
					 <c:otherwise>
					  <c:if test="${individualItem.depFlag=='NA' }">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Departure" sortProperty="sortDpt">
					  <c:if test="${individualItem.dep==''}">
					 	<a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 	<c:out value="_" />
					 	</c:if>
					 	<c:if test="${individualItem.dep!=''}">
					 	<a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 	<c:out value="${individualItem.dep}" />
					 	</c:if>
					 </display:column>					 
					 </c:if>
					 </c:otherwise>					 
					 </c:choose>
					 <c:choose>
					  <c:when  test="${individualItem.arvFlag!='NA' && individualItem.arvlF=='A'}">
					 <display:column sortable="true" style="width:20px" title="Arrival" sortProperty="sortArv">
					 <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="${individualItem.arv}" />
					 </a>
					 </display:column>
					 </c:when >
					 <c:when  test="${individualItem.arvFlag!='NA' && individualItem.arvlF=='T' }">
					 <c:choose>
					 <c:when test="${individualItem.arvFlag=='DT' }">
					 <display:column sortable="true"  style="background:#FFFF99;width:20px;" title="Arrival" sortProperty="sortArv">
					 <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="${individualItem.arv}" />
					 </a>	
					 </display:column>
					 </c:when>
					  <c:when test="${individualItem.arvFlag=='OD' }">
					  <display:column sortable="true" style="background:#FF6666;width:20px;" title="Arrival" sortProperty="sortArv">
					  <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					  <c:out value="${individualItem.arv}" />
					  </a>	
					  </display:column>
					 </c:when>
					 <c:when test="${individualItem.arvFlag=='' && individualItem.arv!=null}">
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Arrival" sortProperty="sortArv">
					  <c:if test="${individualItem.arv==''}">
					  <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					  <c:out value="_" />
					  </a>
					  </c:if>
					  <c:if test="${individualItem.arv!=''}">
					  <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					  <c:out value="${individualItem.arv}" />
					  </a>
					  </c:if>	
					  </display:column>
					 </c:when>					
					 <c:when test="${empty individualItem.arvFlag }">
					 <display:column sortable="true"  style="background:#FFFFFF;width:20px;text-align:center;" title="Arrival" sortProperty="sortArv">
					 <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					 <c:out value="_" />
					 </a>	
					 </display:column>
					 </c:when >
					 </c:choose>
					 </c:when>
					 <c:otherwise>
					  <c:if test="${individualItem.arvFlag=='NA' }">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Arrival" sortProperty="sortArv">
					 <c:if test="${individualItem.arv==''}">
					  <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					  <c:out value="_" />
					  </a>
					  </c:if>
					  <c:if test="${individualItem.arv!=''}">
					  <a onclick="javascript:window.open('containersAjaxList.html?id=${individualItem.id}','_blank');" >
					  <c:out value="${individualItem.arv}" />
					  </a>
					  </c:if>	
					 </display:column>
					 </c:if>
					 </c:otherwise>
					 </c:choose>					 
					 <c:choose>								 
					<c:when test="${individualItem.custFlag!='NA' && individualItem.customDateF=='A' }">
					 <display:column sortable="true" style="width:20px" title="Clear Customs" sortProperty="sortCustDt">
					 <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="${individualItem.customDate}"></c:out>
                      </a>
					 </display:column>
					 </c:when>
					  <c:when test="${individualItem.custFlag!='NA' && individualItem.customDateF=='T' }">
					  <c:choose>
					   <c:when test="${individualItem.custFlag=='DT' }">
					 <display:column sortable="true" style="background:#FFFF99;width:20px;" title="Clear Customs"  sortProperty="sortCustDt">
					 <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="${individualItem.customDate}"></c:out>
                      </a>
					 </display:column>
					 </c:when>
					  <c:when test="${individualItem.custFlag=='OD' }">
					 <display:column sortable="true"  style="background:#FF6666;width:20px;" title="Clear Customs" sortProperty="sortCustDt">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="${individualItem.customDate}"></c:out>
                      </a>
					 </display:column>
					 </c:when>
					 <c:when test="${individualItem.custFlag=='' && individualItem.customDate!=null}">
					 <display:column sortable="true" style="width:20px;text-align:center;" title="Clear Customs" sortProperty="sortCustDt">
					  <c:if test="${individualItem.customDate==''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="-"></c:out>
                      </a>
                      </c:if>
                      <c:if test="${individualItem.customDate!=''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="${individualItem.customDate}"></c:out>
                      </a>
                      </c:if>
					  </display:column>
					 </c:when>
					 <c:when test="${empty individualItem.custFlag }">
					 <display:column sortable="true" style="background:#FFFFFF;width:20px;text-align:center;" title="Clear Customs" sortProperty="sortCustDt">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
					 </display:column>					 
					 </c:when>
					  </c:choose>
					  </c:when>
					   <c:otherwise>
					  <c:if test="${individualItem.custFlag=='NA' }">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Clear Customs" sortProperty="sortCustDt">
					  <c:if test="${individualItem.customDate==''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
                      </c:if>
                      <c:if test="${individualItem.customDate!=''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="${individualItem.customDate}"></c:out>
                      </a>
                      </c:if>
					 </display:column>					 
					 </c:if>
					  <c:if test="${empty individualItem.custFlag  }">
					 <display:column sortable="true" style="background:#FFFFFF;width:20px;text-align:center;" title="Clear Customs" sortProperty="sortCustDt">
					   <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.clearCustom','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
					 </display:column>					 
					 </c:if>
					 </c:otherwise>
						</c:choose>						
						<c:choose>						
					 <c:when  test="${individualItem.dLFlag!='NA' && individualItem.delAT =='A'}">
					 <display:column sortable="true" style="width:20px;text-align:center;" title="Delivery" sortProperty="sortDeliveryDt"> 
					 <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=deliveryA','_blank');" >
                          <c:out value="${individualItem.dL}"></c:out>
                      </a>
					 </display:column>
					 </c:when>
					 <c:when test="${individualItem.dLFlag!='NA' && individualItem.delAT=='T' }">
					 <c:choose>
					 <c:when test="${individualItem.dLFlag=='DT' }">
					 <display:column sortable="true"  style="background:#FFFF99;width:20px;text-align:center;" title="Delivery" sortProperty="sortDeliveryDt">
					 <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=deliveryA','_blank');" >
                          <c:out value="${individualItem.dL}"></c:out>
                      </a> 
					 </display:column>
					 </c:when>
					  <c:when test="${individualItem.dLFlag=='OD' }">
					  <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Delivery" sortProperty="sortDeliveryDt">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=deliveryA','_blank');" >
                          <c:out value="${individualItem.dL}"></c:out>
                      </a>
					  </display:column>
					 </c:when>
					   <c:when test="${individualItem.dLFlag=='' && individualItem.dL!=null}">
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Delivery" sortProperty="sortDeliveryDt">
					  <c:if test="${individualItem.dL==''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.deliveryShipper','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
                      </c:if>
                      <c:if test="${individualItem.dL!=''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=deliveryA','_blank');" >
                          <c:out value="${individualItem.dL}"></c:out>
                      </a>
                      </c:if>  
					  </display:column>
					 </c:when>					
					 <c:when test="${empty individualItem.dLFlag }">
					 <display:column sortable="true" style="background:#FFFFFF;width:20px;text-align:center;" title="Delivery" sortProperty="sortDeliveryDt">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.deliveryShipper','_blank');" >
                          <c:out value="_"></c:out>
                      </a> 
					 </display:column>					 
					 </c:when>
					 </c:choose>
					 </c:when>
					 <c:otherwise>
					  <c:if test="${individualItem.dLFlag=='NA' }">
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Delivery" sortProperty="sortDeliveryDt">
					 <c:if test="${individualItem.dL==''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=trackingStatus.deliveryShipper','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
                      </c:if>
                      <c:if test="${individualItem.dL!=''}">
					  <a onclick="javascript:window.open('editTrackingStatus.html?id=${individualItem.id}&field=deliveryA','_blank');" >
                          <c:out value="${individualItem.dL}"></c:out>
                      </a>
                      </c:if>  
					 </display:column>					 
					 </c:if>
					 </c:otherwise>					 
					 </c:choose>
					  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
					  <display:column sortable="true" style="text-align: right;width:20px;text-align:center;" title="Claims" >
					  	<c:if test="${not empty individualItem.claims}"><a onclick="javascript:window.open('claims.html?id=${individualItem.id}','_blank');"  >Y</a></c:if>
					  	<c:if test="${empty individualItem.claims}"><a onclick="javascript:window.open('claims.html?id=${individualItem.id}','_blank');"  >_</a></c:if>
					  </display:column>	
					  </configByCorp:fieldVisibility>
					  <c:choose>
					 <c:when test="${individualItem.qc=='NA'}">					 
					 <display:column sortable="true" style="background:#C0C0C0;width:20px;text-align:center;" title="Audited" sortProperty="qcSortDt">
					 <a onclick="javascript:window.open('editBilling.html?id=${individualItem.id}&field=billing.auditComplete','_blank');" >
                          <c:out value="${individualItem.qc}"></c:out>
                      </a>
					</display:column>
					 </c:when>
					  <c:when test="${individualItem.qc=='DT'}">					 
					  <display:column sortable="true" style="background:#FFFF99;width:20px;text-align:center;" title="Audited" sortProperty="qcSortDt">
					  <a onclick="javascript:window.open('editBilling.html?id=${individualItem.id}&field=billing.auditComplete','_blank');" >
                          <c:out value="${individualItem.qc}"></c:out>
                      </a>
					</display:column>
					 </c:when>
					 <c:when test="${individualItem.qc=='OD'}">
					 <display:column sortable="true" style="background:#FF6666;width:20px;text-align:center;" title="Audited" sortProperty="qcSortDt">
					 <a onclick="javascript:window.open('editBilling.html?id=${individualItem.id}&field=billing.auditComplete','_blank');" >
                          <c:out value="${individualItem.qc}"></c:out>
                      </a>
					</display:column>					 
					 </c:when>
					 <c:otherwise>
					  <display:column sortable="true" style="width:20px;text-align:center;" title="Audited" sortProperty="qcSortDt">
					   <c:if test="${individualItem.qc==''}">
					  <a onclick="javascript:window.open('editBilling.html?id=${individualItem.id}&field=billing.auditComplete','_blank');" >
                          <c:out value="_"></c:out>
                      </a>
                      </c:if>
                      <c:if test="${individualItem.qc!=''}">
					  <a onclick="javascript:window.open('editBilling.html?id=${individualItem.id}&field=billing.auditComplete','_blank');" >
                          <c:out value="${individualItem.qc}"></c:out>
                      </a>
                      </c:if>
					  </display:column>				 
					 </c:otherwise>
					 </c:choose>
					  <display:column  property="billToName" sortable="true" style="width:20px;" title="Billing Party" maxLength="19">					 
					  </display:column>	
					   <display:column sortable="true" style="width:20px;" title="Origin<br>Country/City" >
					  <c:out value="${individualItem.orginCC}"></c:out>
					  </display:column>
					   <display:column sortable="true" style="width:20px;" title="Destination<br>Country/City" >
					  <c:out value="${individualItem.destinCC}"></c:out>
					  </display:column>	
					  <display:column sortable="true" style="width:20px" title="Coordinator" property="coordinator"/>
					  <display:column sortable="true" style="width:20px" title="Sales Person" property="salesMan"/>
					  <display:column sortable="true" style="width:23px" title="Created On"  property="createdOn"/>
					 </display:table>
					<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="customerFile.id" />
<c:if test="${not empty customerFile.sequenceNumber}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<c:if test="${empty customerFile.sequenceNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>

<%-- <div id="showExtract">
<table style="margin-bottom:5px">
<tr><td width="10px;"></td><td class="bgblue" >SO Dashboard Extract</td></tr>
<tr><td height="5px"></td></tr>
</table>
<table style="margin-bottom:5px">
<tr>
<td width="10px"></td>
<td><button type="button" class="cssbutton" style="width:155px;" onclick="extractFunction();"	>	
        			<span style="color:black;">Download into Excel</span></button></td>
</tr>
</table>
</div> --%>


  </s:form>
<script type="text/javascript"> 
try{
	var cityCountryZipVal = document.forms['serviceOrderListForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
}catch(e){}
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	try{
	<c:if test="${not empty roleArray}">
	var rolarr='${roleArray}';	
	var rolaarrys=rolarr.split(",");
	for(var i=0;i<rolaarrys.length;i++){
		var fld=document.forms['serviceOrderListForm'].elements['role'];	
		var values = [];
		for (var k = 0; k < fld.options.length; k++) {
		  if (fld.options[k].value==rolaarrys[i]) {
			  fld.options[k].selected=true;
		  }
		}
	}
	</c:if>
	}catch(e){
	}
	
	try{
		var st2=document.getElementById('showExtract');
		var showList = '${individualItem}';
			if(showList != ''){			
				st2.style.display = 'block';
			}
			if(showList == ''){			
				st2.style.display = 'none';
			}
		}catch(e){}
</script> 
