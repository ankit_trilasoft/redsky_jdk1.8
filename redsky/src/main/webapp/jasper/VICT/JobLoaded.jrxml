<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="JobLoaded" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="86cc1ce2-b5d2-4d4f-a770-fceb9f49a67c">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	"JobLoaded-R-PDF-VICT.jrxml",
	serviceorder.shipNumber AS serviceorder_shipNumber,
	serviceorder.status AS serviceorder_Status,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=""),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))) AS Shipper_Name,
	billing.billtocode AS billing_Billtocode,
	billing.billtoname AS billing_Billtoname,
	serviceorder.job AS serviceorder_Job,
	serviceorder.companydivision AS serviceorder_companydivision,
	DATE_FORMAT(trackingstatus.loadA,'%d-%b-%y') AS trackingstatus_LoadA,
	DATE_FORMAT(trackingstatus.deliveryA,'%d-%b-%y') AS trackingstatus_DeliveryA,
	sum(if(accountline.actualRevenue is null || accountline.actualRevenue='',0.0,accountline.actualRevenue)) AS accountline_ActualRevenue,
	sum(if(accountline.revisionRevenueAmount is null || accountline.revisionRevenueAmount='',0.0,accountline.revisionRevenueAmount)) AS accountline_RevisionRevenueAmount,
	sum(if(accountline.estimateRevenueAmount is null || accountline.estimateRevenueAmount='',0.0,accountline.estimateRevenueAmount)) AS accountline_EstimateRevenueAmount
From serviceorder serviceorder
	inner join billing billing on serviceorder.id=billing.id AND billing.corpId =$P{Corporate ID}
	inner join trackingstatus trackingstatus on serviceorder.id=trackingstatus.id AND trackingstatus.corpId =$P{Corporate ID}
	inner join accountline accountline on serviceorder.id=accountline.serviceorderId AND accountline.corpId =$P{Corporate ID}
where serviceorder.corpId =$P{Corporate ID}
	AND (trackingstatus.loadA <= sysdate() or trackingstatus.deliveryA <= sysdate())
	AND accountline.status = true
	AND serviceorder.status not in ('CNCL','CANCEL','DWNLD','DWN','HOLD','CLSD','2CLS')
	AND serviceorder.job not in ('DWN')
	AND billing.billComplete is null
group by accountline.shipNumber
having sum(accountline.actualRevenue) <> 0 and
sum(accountline.actualRevenue) < if(sum(accountline.revisionRevenueAmount) >0, sum(accountline.revisionRevenueAmount),sum(accountline.estimateRevenueAmount))
	AND ((if(sum(accountline.revisionRevenueAmount) >0, sum(accountline.revisionRevenueAmount),sum(accountline.estimateRevenueAmount))) -
  (sum(accountline.actualRevenue)))/(if(sum(accountline.revisionRevenueAmount) >0, sum(accountline.revisionRevenueAmount),sum(accountline.estimateRevenueAmount))) > .15
order by serviceorder.companydivision, accountline.shipNumber,trackingstatus.loadA desc]]>
	</queryString>
	<field name="JobLoaded-R-PDF-VICT.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_Status" class="java.lang.String"/>
	<field name="Shipper_Name" class="java.lang.String"/>
	<field name="billing_Billtocode" class="java.lang.String"/>
	<field name="billing_Billtoname" class="java.lang.String"/>
	<field name="serviceorder_Job" class="java.lang.String"/>
	<field name="serviceorder_companydivision" class="java.lang.String"/>
	<field name="trackingstatus_LoadA" class="java.lang.String"/>
	<field name="trackingstatus_DeliveryA" class="java.lang.String"/>
	<field name="accountline_ActualRevenue" class="java.math.BigDecimal"/>
	<field name="accountline_RevisionRevenueAmount" class="java.math.BigDecimal"/>
	<field name="accountline_EstimateRevenueAmount" class="java.math.BigDecimal"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companydivision}]]></groupExpression>
		<groupHeader>
			<band height="16" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="115" height="16" uuid="18960d32-5592-4cad-9105-4b0c834acb16"/>
					<textElement>
						<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companydivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-14" x="0" y="0" width="102" height="16" uuid="0ec6798f-e8f6-4e4d-9f43-92ea96605102"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="29" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="3" width="802" height="26" uuid="14101502-0676-429a-9562-a36bb80259e7"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="14" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job Loaded Or Delivered And More Than 15% Of Revised/Estimated Amt Not Invoiced]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="32" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="0" y="8" width="79" height="24" uuid="e46445a7-1001-41b7-9f3c-e72ec1e5d45e"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SO #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="79" y="8" width="57" height="24" uuid="02a0bb7b-30c8-4f94-a1ed-fa645d73ac13"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="136" y="8" width="93" height="24" uuid="22f6944d-2a4b-43ee-9272-039d6d48d2e3"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="229" y="8" width="70" height="24" uuid="2904833e-c370-4eb2-9ec6-13da882c9f7f"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="299" y="8" width="116" height="24" uuid="165afe6a-5058-4309-bca9-4b2d67f8b470"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="415" y="8" width="39" height="24" uuid="134b9ce9-2b4d-4b50-b249-7f111e040357"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="454" y="8" width="60" height="24" uuid="70de1002-880a-4f82-82ad-756569adfb4d"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Load Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="514" y="8" width="60" height="24" uuid="4e6043da-a33e-477c-a8db-3224d80d991a"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Delivery Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="574" y="8" width="76" height="24" uuid="ad828c05-80bc-46d7-b67d-df52037b1fab"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Inv Amt.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="650" y="8" width="76" height="24" uuid="b98b2df5-12dc-45e0-9b15-1f8e12d7ef7b"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Rev Amt.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="726" y="8" width="76" height="24" uuid="9caecd4f-9e8b-4856-a651-a3f95c40de68"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Est Amt.]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="0" width="79" height="18" uuid="1a94fcb8-87a9-4c7f-97ca-15bbdfbcfed5"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="79" y="0" width="57" height="18" uuid="03aed86f-b3dc-489a-8c02-21dabb12574d"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_Status}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="136" y="0" width="93" height="18" uuid="6ae13c81-54ff-4467-aa95-c7fd7297175d"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper_Name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="229" y="0" width="70" height="18" uuid="2bc88246-4dbc-4a1e-8285-58261690b55a"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_Billtocode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="299" y="0" width="116" height="18" uuid="9251de00-eca6-4302-b9ee-a8905af42577"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_Billtoname}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="415" y="0" width="39" height="18" uuid="1877c794-29a4-40be-b546-08aa5ecb702b"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_Job}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="454" y="0" width="60" height="18" uuid="8458f952-473f-4e6d-865a-9079f460a3a3"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_LoadA}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="514" y="0" width="60" height="18" uuid="76ce4468-f8c0-4986-92e5-31bf24bbe79a"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_DeliveryA}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="574" y="0" width="76" height="18" uuid="8e44fb66-b4d7-453d-a792-1b10fd361927"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_ActualRevenue}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="650" y="0" width="76" height="18" uuid="b61ebf32-eb42-47ba-8aed-627f685e228b"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_RevisionRevenueAmount}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-4" x="726" y="0" width="76" height="18" uuid="a7dc077c-fa31-4af7-a978-04851163ddc2"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_EstimateRevenueAmount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="18" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="668" y="4" width="71" height="14" uuid="78c73b77-b0ac-4d3d-8c57-1572d63db374"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="739" y="4" width="44" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="75932eea-1b06-45db-9b46-b43ddf199f73"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-13" x="149" y="4" width="485" height="14" uuid="560863b9-3408-427c-b99d-5dea0c5aa255"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[JobLoaded.jrxml]]></text>
			</staticText>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-5" x="1" y="4" width="105" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="8ade34bb-1655-44e1-8004-5c8411032dc8"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="1" width="782" height="1" uuid="adbb3a8a-585c-4f96-88df-8cd6fa3c6f95"/>
				<graphicElement fill="Solid"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
