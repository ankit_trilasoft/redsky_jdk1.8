package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.UpdateRuleDao;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.UpdateRule;
import com.trilasoft.app.model.UpdateRuleResult;

public class UpdateRuleDaoHibernate extends GenericDaoHibernate<UpdateRule, Long> implements UpdateRuleDao {
	private List updateRules;

	public UpdateRuleDaoHibernate() {
		super(UpdateRule.class);
		// TODO Auto-generated constructor stub
	}
		public List getByCorpID(String sessionCorpID)
		{
			return getHibernateTemplate().find("from UpdateRule where corpID in ('TSFT','"+sessionCorpID+"')");
	}
		
		public List<DataCatalog> selectfield(String entity, String corpID) {
			return getHibernateTemplate().find("select rulesFiledName from DataCatalog where defineByToDoRule='true' and tableName='"+entity+"' and corpID in ('TSFT','"+corpID+"')");
		}
		
		public List<DataCatalog> selectDatefield(String corpID) {
			
			return getHibernateTemplate().find("select rulesFiledName from DataCatalog where isdateField='true' and defineByToDoRule='true' and corpID in ('TSFT','" + corpID + "') group by rulesFiledName order by rulesFiledName");
			 
		}
		
		public List searchById(Long id,String  fieldToUpdate,String  validationContion,String  ruleStatus) {
			List ls=new ArrayList();
			
			if(id==null){
				String stautsCheck=""; 
				String query = "from UpdateRule where fieldToUpdate like '%"+fieldToUpdate +"%' AND validationContion like '%" +validationContion+"%'  ";
				
				if(!ruleStatus.equalsIgnoreCase("All")){
					stautsCheck= " AND ruleStatus ='"+ ruleStatus+"'";
				}
				String queryString = new StringBuilder(query).append(stautsCheck).toString();
				
				System.err.println("\n\n\nQueryString------>>"+queryString);
				
				return  getHibernateTemplate().find(queryString);
				
			}
			else
			{
				return getHibernateTemplate().find("from UpdateRule where id="+id+" and   fieldToUpdate like '%"+fieldToUpdate+"%' AND validationContion like '%" +validationContion+"%' ");
			}
			
			
		}
		public List executeRule(String ruleExpression) {
			List tempErrorList = new ArrayList();
			String message = "";
				try {
					return this.getSession().createSQLQuery(ruleExpression).list();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					message = e.getMessage();
					e.printStackTrace();
					tempErrorList.add(message);
				}
				return tempErrorList;
		}
		
		public void changestatus(String status, Long id) {
			getHibernateTemplate().bulkUpdate("update UpdateRule set ruleStatus='"+status+"' where id=?",id);
		}
		public <UpdateRuleResult> List getToDoResultByRuleNumber(String ruleNumber)
		{
		List<UpdateRuleResult>  list =  (List<UpdateRuleResult>)getHibernateTemplate().find("from UpdateRuleResult where ruleNumber='"+ruleNumber+"' ");
		return list;
		}
		 public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId){
			 int i = this.getSession().createSQLQuery("delete from updateruleresult where ruleNumber='"+ruleNumber+"' and corpID='"+corpId+"' ").executeUpdate();
		 }
		 public List findNumberOfResultEntered(String sessionCorpID, Long id){
			 return this.getSession().createSQLQuery("select count(*) from updateruleresult where updatedRuleId ="+id+" and corpID ='"+sessionCorpID+"' ").list();
		 }
		 public List findMaximum(String sessionCorpID){
			 return getHibernateTemplate().find("select max(ruleNumber) from UpdateRule where corpID='"+sessionCorpID+"' ");
		 }
		 public List findParterNameList(String partnerCode,String corpId){
			 return this.getSession().createSQLQuery("select lastName from partner where partnerCode ='"+partnerCode+"' and corpID = '"+corpId+"' ").list();
		 }
		 public void updateResults(Long ruleNumber){
			 List<UpdateRuleResult>  list =  (List<UpdateRuleResult>)getHibernateTemplate().find("from UpdateRuleResult where ruleNumber='"+ruleNumber+"' ");
			 for(UpdateRuleResult result : list){
				 String a=result.getTableName();
				 String fieldValueDesc="";
				 if(result.getFieldName().equalsIgnoreCase("originGivenCode")){
					 fieldValueDesc="originGivenName";
				 }
				 if(result.getFieldName().equalsIgnoreCase("destinationGivenCode")){
					 fieldValueDesc="destinationGivenName";
				 }
				 if(result.getFieldName().equalsIgnoreCase("originReceivedCode")){
					 fieldValueDesc="originReceivedName";
				 }
				 if(result.getFieldName().equalsIgnoreCase("destinationReceivedCode")){
					 fieldValueDesc="destinationReceivedName";
				 }
				 String query = "update TrackingStatus set "+result.getFieldName()+"='"+result.getValueOfField()+"',"+fieldValueDesc+"='"+result.getUpdatefieldValue()+"' where id="+result.getOrderId()+" ";
				 System.out.println("\n\n\n query "+query);
				 getHibernateTemplate().bulkUpdate("update TrackingStatus set "+result.getFieldName()+"='"+result.getValueOfField()+"',"+fieldValueDesc+"='"+result.getUpdatefieldValue()+"' where id=?",result.getOrderId());
			 }
		 }
}
