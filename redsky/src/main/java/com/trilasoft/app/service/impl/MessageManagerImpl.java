package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MessageDao;
import com.trilasoft.app.model.MyMessage;
import com.trilasoft.app.service.MessageManager;

public class MessageManagerImpl extends GenericManagerImpl<MyMessage, Long> implements MessageManager {   
    MessageDao messageDao;   
  
    public MessageManagerImpl(MessageDao messageDao) {   
        super(messageDao);   
        this.messageDao = messageDao;   
    }   
  
    public List<MyMessage> findByToUser(String toUser) {   
        return messageDao.findByToUser(toUser);   
    }   
    
    public List<MyMessage> findByFromUser(String fromUser) {   
        return messageDao.findByFromUser(fromUser);   
    }
    
    public List<MyMessage> findByUnreadMessages(String fromUser) {
    	return messageDao.findByUnreadMessages(fromUser);
    }
    
    public List<MyMessage> findByOutbox(String status, String fromUser){
    	return messageDao.findByOutbox(status,fromUser);
    }
    
    public int updateFwdStatus(Long noteId){
    	return messageDao.updateFwdStatus(noteId);
    }
}