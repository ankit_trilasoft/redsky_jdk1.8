<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title>MSS Orders</title>
    <meta name="heading" content="MSS Orders"/>

   
   <style> span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
!margin-bottom:2px;
margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
<s:form id="weeklyXMLForm" action="" method="post" validate="true">
 
<!--  <p style="font-size:11px; font-weight:bold; margin-bottom:10px">Integration XML Summary</p> -->
<p style="font: bold 12px/2.5em arial,verdana; margin-bottom: 10px;" class="bgblue">Integration Centre</p>
 

  
	<div id="layer5" style="width:100%">	
	<div id="newmnav">
		  <ul>
		  <c:if test="${vanlineEnabled==true }">
		<li><a href="integrationCenterList.html"><span>Vanline / Docs Downloads</span></a></li>
		     <li><a href="centreVanlineUpload.html"><span>Vanline / Docs Uploads</span></a></li>
		      <li><a href="centerIntegrationLogs.html"><span>Vanline Integration Log</span></a></li>
		       </c:if>
		        <c:if test="${enableMSS==true }">
		       <li style="background:#FFF " id="newmnav1"><a class="current"><span>MSS Orders</span></a></li>
		       </c:if>
		         <li><a href="UGWWList.html"><span>UGWW</span></a></li>
		        <li><a href="centerintegrationLogList.html"><span>UGWW Log</span></a></li>
		         <c:if test="${voxmeIntegration==true }">
		         <li><a href="voxmeEstimatorList.html"><span>Voxme&nbsp;Estimator</span></a></li>
		         </c:if>
		          <c:if test="${enablePricePoint==true}">
		          <li><a href="pricePointList.html"><span>Price&nbsp;Point</span></a></li>
		          </c:if>
		           <c:if test="${enableMoveForYou==true }">
		        <li><a href="moveForUList.html"><span>Move&nbsp;4&nbsp;u</span></a></li>
		        </c:if>
		  </ul>
	</div>
	<div class="spn" style="height:0px;!margin-bottom:1px;">&nbsp;</div>	
<s:set name="mssLogList" value="mssLogList" scope="request"/>
<display:table name="mssLogList" class="table" requestURI="" id="mssLogListId" export="true" pagesize="30" sort="page" style="width:1000px" defaultorder="descending">
	<display:column property="workTicketNumber" sortable="true" title="W/T #"/>	
     <display:column sortable="true" style="width:200px;" title="DATE"  sortProperty="updatedDefaultSort">
					 <c:out value="${mssLogListId.updatedon}"></c:out>
	</display:column>
    <display:column property="mssOrderNumber" sortable="true" title="MSS Order #"/>	
     <display:column property="updatedBy" sortable="true" title="Created By"/> 
   <display:setProperty name="paging.banner.first" value=""/>
   <%--  <display:setProperty name="paging.banner.item_name" value="integrationLog"/>
    <display:setProperty name="paging.banner.items_name" value="people"/>--%>

    <display:setProperty name="export.excel.filename" value="Weekly XML Summary.xls"/>
    <display:setProperty name="export.csv.filename" value="Weekly XML Summary.csv"/>
    <display:setProperty name="export.pdf.filename" value="Weekly XML Summary.pdf"/> 
</display:table>
</div>
</s:form>