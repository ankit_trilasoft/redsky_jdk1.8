package com.trilasoft.app.init;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.trilasoft.app.service.AppInitServletManager;

public class AppInitServlet extends HttpServlet {

	public static Map pageActionUrls = new ConcurrentHashMap();
	public static Map roleBasedComponentPerms = new ConcurrentHashMap();
	public static Map pageFieldVisibilityComponentMap = new ConcurrentHashMap();
	public static Map redskyConfigMap = new ConcurrentHashMap();
	
	public static String byPassUrls =new String();
	String pantahooUrl ="";
	String tsftUrl ="";
	String voxmeMailId ="";
	String qtgUrl ="";
	String qtgUrl1 ="";
	String dipatchUrl ="";
	String emailOut ="";
	String emailDomain="";
	String serverUrl="";
	String MOLMailID="";
	String byPassemail="";
	public static String byPassAjaxUrls =new String();
	  
	public ResourceBundle resourceBundle;	
	private String sessionCorpID;
	AppInitServletManager appInitServletManager ;
	
	String byAjPassUrls="";
	
	public String getByAjPassUrls() {
		return byAjPassUrls;
	}

	public void setByAjPassUrls(String byAjPassUrls) {
		this.byAjPassUrls = byAjPassUrls;
	}	
	String byPassUrl="";
	public String getByPassUrl() {
		return byPassUrl;
	}

	public void setByPassUrl(String byPassUrl) {
		this.byPassUrl = byPassUrl;
	}
	public AppInitServletManager getAppInitServletManager() {
		return appInitServletManager;
	}

	public void setAppInitServletManager(AppInitServletManager appInitServletManager) {
		this.appInitServletManager = appInitServletManager;
	}

	@Override
	public void init() throws ServletException {

		super.init();
		
	}
	
	public void startup() {
		System.out.println("Application Startup Servlet Called !!!!!!!!!!!!");
		redskyConfigMap.put("qtg.url",qtgUrl );
		redskyConfigMap.put("qtg.url1",qtgUrl1 );
		redskyConfigMap.put("Voxme_MailId",voxmeMailId );
		redskyConfigMap.put("BY_PASS_URL",byPassUrl );
		redskyConfigMap.put("TSFT_URL",tsftUrl );
		redskyConfigMap.put("BY_PASS_AJAX_URL",byAjPassUrls );
		redskyConfigMap.put("pentah.url",pantahooUrl );
		redskyConfigMap.put("dispatch.url",dipatchUrl );
		redskyConfigMap.put("email.out",emailOut );
		redskyConfigMap.put("email.domainfilter",emailDomain );
		redskyConfigMap.put("server.url",serverUrl );
		redskyConfigMap.put("Mol_MailId",MOLMailID );
		redskyConfigMap.put("email.email",byPassemail );
		AppInitServlet.byPassAjaxUrls=byAjPassUrls;
		AppInitServlet.byPassUrls=byPassUrl;
		appInitServletManager.populatePermissionCache(pageActionUrls);
		appInitServletManager.populateRolebasedComponentPermissions(roleBasedComponentPerms);
		appInitServletManager.populatePageFieldVisibilityPermissions(pageFieldVisibilityComponentMap);
		
		//resourceBundle=ResourceBundle.getBundle("redsky-configuration-local");
		//byPassAjaxUrls = resourceBundle.getString("BY_PASS_AJAX_URL");
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getPantahooUrl() {
		return pantahooUrl;
	}

	public  void setPantahooUrl(String pantahooUrl) {
		this.pantahooUrl = pantahooUrl;
	}

	public  String getTsftUrl() {
		return tsftUrl;
	}

	public  void setTsftUrl(String tsftUrl) {
		this.tsftUrl = tsftUrl;
	}

	public  String getVoxmeMailId() {
		return voxmeMailId;
	}

	public  void setVoxmeMailId(String voxmeMailId) {
		this.voxmeMailId = voxmeMailId;
	}

	public  String getQtgUrl() {
		return qtgUrl;
	}

	public  void setQtgUrl(String qtgUrl) {
		this.qtgUrl = qtgUrl;
	}

	public  String getQtgUrl1() {
		return qtgUrl1;
	}

	public  void setQtgUrl1(String qtgUrl1) {
		this.qtgUrl1 = qtgUrl1;
	}

	public String getDipatchUrl() {
		return dipatchUrl;
	}

	public void setDipatchUrl(String dipatchUrl) {
		this.dipatchUrl = dipatchUrl;
	}

	public String getEmailOut() {
		return emailOut;
	}

	public void setEmailOut(String emailOut) {
		this.emailOut = emailOut;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getMOLMailID() {
		return MOLMailID;
	}

	public void setMOLMailID(String mOLMailID) {
		MOLMailID = mOLMailID;
	}

	public String getByPassemail() {
		return byPassemail;
	}

	public void setByPassemail(String byPassemail) {
		this.byPassemail = byPassemail;
	}
		
	
	
}
