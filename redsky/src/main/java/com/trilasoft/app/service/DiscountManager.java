package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Discount;


public interface DiscountManager extends GenericManager<Discount, Long> {
	public List discountList(String contract, String chargeCode,String corpId);
	public List discountMapList(String corpId);
}
