package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.service.CrewCapacityManager;
import com.trilasoft.app.service.RefMasterManager;

public class CrewCapacityAction extends BaseAction{
	
	private Long id;
	private String sessionCorpID;
	private CrewCapacity crewCapacity;
	private CrewCapacityManager crewCapacityManager;
	private RefMasterManager refMasterManager;
	
	
	public CrewCapacityAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 		
	} 
	
	private String crewGroupval;
	public String edit() {
		
		//serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		getComboList(sessionCorpID);
		if (id != null) {
			crewCapacity = crewCapacityManager.get(id);
		} else {			
			crewCapacity = new CrewCapacity(); 
			crewCapacity.setCreatedOn(new Date());
			crewCapacity.setUpdatedOn(new Date());		
		}	
		return SUCCESS;
	}
	private String hitFlag="0";
	private String crewCapacityUrl;
	private String CrewCapacityChildFormFlag;
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (crewCapacity.getId() == null);  
				if (isNew) { 
					crewCapacity.setCreatedOn(new Date());
					crewCapacity.setCreatedBy(getRequest().getRemoteUser());
		        } 				
				crewCapacity.setCorpID(sessionCorpID);
				crewCapacity.setUpdatedOn(new Date());
				crewCapacity.setUpdatedBy(getRequest().getRemoteUser()); 
				crewCapacity=crewCapacityManager.save(crewCapacity);  
			    String key = (isNew) ?"Crew Capacity have been saved." :"Crew Capacity have been updated." ;
			    saveMessage(getText(key)); 
			    hitFlag="1";
		if(CrewCapacityChildFormFlag!=null && !CrewCapacityChildFormFlag.equalsIgnoreCase("") && CrewCapacityChildFormFlag.equalsIgnoreCase("N")){
				    if(crewCapacity.getId()!=null){
				    	crewCapacityUrl ="?id="+crewCapacity.getId(); 
				    }
			    }
		if(CrewCapacityChildFormFlag!=null && !CrewCapacityChildFormFlag.equalsIgnoreCase("") && CrewCapacityChildFormFlag.equalsIgnoreCase("Y")){
			crewCapacityList = crewCapacityManager.findRecordByHub(crewCapacity.getCrewGroup(),sessionCorpID);
				if(crewCapacityList!=null && !crewCapacityList.isEmpty() && crewCapacityList.get(0)!=null){
						for(CrewCapacity crlist:crewCapacityList){
							crewCapacity=crlist;
						}
						crewCapacityUrl ="?id="+crewCapacity.getId(); 
					}
				}
		return SUCCESS;
			
	}
	public String delete(){		
		crewCapacityManager.remove(crewCapacity.getId()); 
		list();
	  return SUCCESS;		    
	}
	
	private List crewCapacityMode;
	private List serviceType;
	private List physicalConditionList;
	private List crewGroupList;
	private Map<String, String> crewGroupAndJobMap;
	public String getComboList(String corpId){
		crewCapacityMode = new ArrayList();
		crewCapacityMode.add("Air");
		crewCapacityMode.add("Sea");
		crewCapacityMode.add("All");
		
		/*serviceType = new ArrayList();
		serviceType.add("PK");
		serviceType.add("LD");
		serviceType.add("DU");
		serviceType.add("DL");
		serviceType.add("UP");
		serviceType.add("PL");
		serviceType.add("LAB");*/
		
		physicalConditionList = new ArrayList();
		physicalConditionList.add("Multiple Jobs");
		physicalConditionList.add("Elevator");
		physicalConditionList.add("Long Carry");
		physicalConditionList.add("Truck Restrictions");
		physicalConditionList.add("Shuttle");
		physicalConditionList.add("Hoist");
		
		serviceType=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		crewGroupList=crewCapacityManager.findAllCrewGroup(sessionCorpID);
		crewGroupAndJobMap=crewCapacityManager.findExistingJobName(sessionCorpID);
		return SUCCESS;
	}
	private List<CrewCapacity> crewCapacityList;
	
	@SkipValidation
	public String list() {
		getComboList(sessionCorpID);
		if(crewGroupList!=null && !crewGroupList.isEmpty() && crewGroupList.get(0)!=null){
			crewGroupval = crewGroupList.get(0).toString();
			if(crewGroupval!=null && !crewGroupval.equalsIgnoreCase("")){
				crewCapacityList = crewCapacityManager.findRecordByHub(crewGroupval,sessionCorpID);
					for(CrewCapacity crlist:crewCapacityList){
						crewCapacity=crlist;
					}
					crewCapacityUrl ="?id="+crewCapacity.getId(); 
				}
			}
		return SUCCESS;
	}
	private String gName;
	private List checkContract;
	@SkipValidation
    public String findOldGroupName(){
    	checkContract=crewCapacityManager.findOldGroupName(sessionCorpID,gName);            	      		
    return SUCCESS;
    }

	
	public String editCrewCapacityChild() {
		getComboList(sessionCorpID);
		return SUCCESS;
	}
	@SkipValidation
	public String searchRecordsByGroupName(){
		getComboList(sessionCorpID);
		crewCapacityList = crewCapacityManager.findRecordByHub(crewCapacity.getCrewGroup(),sessionCorpID);
		if(crewCapacityList!=null && !crewCapacityList.isEmpty() && crewCapacityList.get(0)!=null){
				for(CrewCapacity crlist:crewCapacityList){
					crewCapacity=crlist;
				}
				crewCapacityUrl ="?id="+crewCapacity.getId(); 
			}
		return SUCCESS;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public CrewCapacity getCrewCapacity() {
		return crewCapacity;
	}
	public void setCrewCapacity(CrewCapacity crewCapacity) {
		this.crewCapacity = crewCapacity;
	}
	public void setCrewCapacityManager(CrewCapacityManager crewCapacityManager) {
		this.crewCapacityManager = crewCapacityManager;
	}

	public List getCrewCapacityMode() {
		return crewCapacityMode;
	}

	public void setCrewCapacityMode(List crewCapacityMode) {
		this.crewCapacityMode = crewCapacityMode;
	}

	public List getServiceType() {
		return serviceType;
	}

	public void setServiceType(List serviceType) {
		this.serviceType = serviceType;
	}

	public List getPhysicalConditionList() {
		return physicalConditionList;
	}

	public void setPhysicalConditionList(List physicalConditionList) {
		this.physicalConditionList = physicalConditionList;
	}

	public String getCrewCapacityUrl() {
		return crewCapacityUrl;
	}

	public void setCrewCapacityUrl(String crewCapacityUrl) {
		this.crewCapacityUrl = crewCapacityUrl;
	}

	public List<CrewCapacity> getCrewCapacityList() {
		return crewCapacityList;
	}

	public void setCrewCapacityList(List<CrewCapacity> crewCapacityList) {
		this.crewCapacityList = crewCapacityList;
	}

	public List getCrewGroupList() {
		return crewGroupList;
	}

	public void setCrewGroupList(List crewGroupList) {
		this.crewGroupList = crewGroupList;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public String getCrewGroupval() {
		return crewGroupval;
	}
	public void setCrewGroupval(String crewGroupval) {
		this.crewGroupval = crewGroupval;
	}
	public List getCheckContract() {
		return checkContract;
	}
	public void setCheckContract(List checkContract) {
		this.checkContract = checkContract;
	}
	public String getgName() {
		return gName;
	}
	public void setgName(String gName) {
		this.gName = gName;
	}
	public String getCrewCapacityChildFormFlag() {
		return CrewCapacityChildFormFlag;
	}
	public void setCrewCapacityChildFormFlag(String crewCapacityChildFormFlag) {
		CrewCapacityChildFormFlag = crewCapacityChildFormFlag;
	}
	public Map<String, String> getCrewGroupAndJobMap() {
		return crewGroupAndJobMap;
	}
	public void setCrewGroupAndJobMap(Map<String, String> crewGroupAndJobMap) {
		this.crewGroupAndJobMap = crewGroupAndJobMap;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
		
}
