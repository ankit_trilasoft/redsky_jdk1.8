package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.InventoryLocation;

public interface InventoryLocationManager extends GenericManager<InventoryLocation, Long> {
	public List getAllLocation(String corpId,String shipNumber);
	public List checkInventoryLocation(String corpID, String locType,String ticket);

}
