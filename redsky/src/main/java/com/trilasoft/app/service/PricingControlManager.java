package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PricingControl;

public interface PricingControlManager  extends GenericManager<PricingControl, Long>{
	 public List checkById(Long id);
	 public List findPricingId(String code);
     public List maxPricingIdCode(String sessionCorpID);
	 public List getAddress(Long serviceOrderId, String tariffApplicability);
	 public List getList(String shipNumber);
	 public List search(String pricingID, String shipNumber, String tariffApplicability, String packing, String country, String customerName, String parentAgent); 
	 public String locateAgents(BigDecimal weight, String latitude, String longitude, String tariffApplicability, String country, String packing, String poe, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military, BigDecimal volume);
	 public List searchAgents(BigDecimal weight, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String destinationCountry, String packing, String destinationPOE, String sessionCorpID, Long pricingControlID, Date createdOn, String createdBy, String updatedBy, Date updatedOn, Date expectedLoadDate, BigDecimal volume, String baseCurrency, String contract, boolean applyCompletemarkUp, String prefferedPortCode);
	 public String locateAgents1(Long id, String tarrifApplicability, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military);
	 public void deletePricingDetails(Long id, String tarrifApplicability);
	 public void updateSelectedResult(Long pricingDetailsId, Long pricingControlID, String tarrifApplicability);
	 public int getCount(Long pricingControlID);
	public String getPorts(String countryCode, String latitude, String longitude, String mode);
	public List getContract(String sessionCorpID);
	public String locateFreightDetails(String originPOE, String destinationPOE, String containerSize, String sessionCorpID);
	public void updateFreightResult(Long freightId, Long pricingControlID, String tarrifApplicability, String freightCurrency, String baseCurrency, String mode, String contract, String freightPrice);
	public List getCurrency(String sessionCorpID);
	public List getAcceptedQuotes(String sessionCorpID, String parentAgent);
	public List getAgentList(String parentAgent, String sessionCorpID);
	public List getPortLocation(String originPOE, String sessionCorpID);
	public void updateServiceOrder(Long id, String shipNumber, String sessionCorpID);
	public List findRateDeptEmailAddress(String sessionCorpID);
	public List getRecentTariffs(String sessionCorpID);
	public void updateCustomerFileNumber(Long id, String sequenceNumber, String shipNumber, Long serviceOrderId, String sessionCorpID);
	public void updateDTHCFlag(Long pricingControlID, String tarrifApplicability, Boolean dthcFlag);
	public List getPricingControId(String shipnumber, String sessionCorpID);
	public List checkControlFlag(String sequenceNumber, String sessionCorpID);
	public List searchRecentTariffs(String sessionCorpID, String let);
	public boolean checkMarkUp(String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String originCountry, String destinationCountry, String packing, String originPOE, String destinationPOE, String sessionCorpID, Date expectedLoadDate, String contract);
	public void updateFreightMarkUpFlag(Long pricingControlID, String tarrifApplicability, Boolean freightMarkUpFlag);
	public void updatePricingControlPorts(Long id, String freightOriginPortCode, String freightDestinationPortCode, String sessionCorpID, String freightOriginPortCodeName, String freightDestinationPortCodeName);
	public void deletePricingFreightDetails(Long pricingControlId);
	public List getRecordList(Long pricingControlId, String sessionCorpID, String tarrifApplicability, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military);
	public String getPortsPerSection(Long id, String tarrifApplicability);     
	
	 
	 
	
	 
}
