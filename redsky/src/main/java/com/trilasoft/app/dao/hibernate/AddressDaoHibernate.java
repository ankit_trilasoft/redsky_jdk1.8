
package com.trilasoft.app.dao.hibernate;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;
import com.trilasoft.app.dao.AccountLineDao;
import com.trilasoft.app.dao.AccrualProcessDao;
import com.trilasoft.app.dao.AddressDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.dao.AddressDao;

import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.DtoAccountInvoicePost;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ShowLineDetail;
import com.trilasoft.app.model.VanLine;


public class AddressDaoHibernate extends GenericDaoHibernate<Partner,Long> implements AddressDao {
	private HibernateUtil hibernateUtil;
	
	public AddressDaoHibernate() {   
        super(Partner.class);   
    }   
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
	public List search(String partnerCode,String originalCorpID,String accountLineBillingParam){
		if(accountLineBillingParam!=null && accountLineBillingParam.equals("Billing")){
			return this.getSession().createSQLQuery( "SELECT billingAddress1,billingAddress2,billingAddress3,billingAddress4,billingCity,billingState,billingZip,billingCountry,billingFax,billingPhone,billingTelex,billingEmail from partner where partnerCode='"+partnerCode+"' and  corpID='"+originalCorpID+"' ").list();	
		}else if(accountLineBillingParam!=null && accountLineBillingParam.equals("YES")){
			return this.getSession().createSQLQuery( "select mailingAddress1,mailingAddress2,mailingAddress3,mailingAddress4,mailingCity,mailingState,mailingZip,mailingCountry,mailingFax,mailingPhone,mailingTelex,mailingEmail from partner where partnerCode='"+partnerCode+"' and corpID = '"+originalCorpID+"' ").list();	
		}else{
			return this.getSession().createSQLQuery( "select terminalAddress1,terminalAddress2,terminalAddress3,terminalAddress4,terminalCity,terminalState,terminalZip,terminalCountry,terminalFax,terminalPhone,terminalTelex,terminalEmail from partner where partnerCode='"+partnerCode+"' and corpID = '"+originalCorpID+"' ").list();
		}
	}
	public List searchBookerAddress(String shipNumberA){		
		List list1= getHibernateTemplate().find( "select p.terminalAddress1,p.terminalAddress2,p.terminalAddress3,p.terminalAddress4,p.terminalCity,p.terminalState,p.terminalZip,p.terminalCountry,p.terminalFax,p.terminalPhone,p.terminalTelex,p.terminalEmail from Partner p,ServiceOrder s where p.partnerCode=s.bookingAgentCode and s.shipNumber='"+shipNumberA+"' ");
		return list1;
	}
	public List partnerAddressDetails(String partnerCode,String corpID){		
		List list1= getHibernateTemplate().find( "SELECT billingAddress1,billingAddress2,billingCity,billingState,billingZip,billingCountry,billingFax,billingPhone,billingEmail, concat(vatBillingGroup,'~','VATBILLINGGROUP') as vatBillingGroup,concat(creditTerms,'~','CRTERMS') as creditTerms,concat(paymentMethod,'~','PAYTYPE') as paymentMethod,extReference,agentParentName,agentParent ,vatNumber from Partner where partnerCode='"+partnerCode+"' and corpID='"+corpID+"' ");
		return list1;
	}
	
	public List findOriginAddress(String sequenceNumber, String corpID){
		List list1= getHibernateTemplate().find( "SELECT originAddress1, originAddress2, originAddress3, originCity, originState, originCountry, originZip, email, originFax, originHomePhone from CustomerFile where sequenceNumber = '"+sequenceNumber+"' and corpID ='"+corpID+"' ");
		return list1;
	}
}

