/**
 *  @Class Name	 SODashboardAction
 *  @author      Surya 
 *  @Date        14-Jul-2014
 */
package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.acegisecurity.Authentication;
import org.apache.commons.collections.list.SetUniqueList;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.RevenueTrackerDTO;
import com.trilasoft.app.dao.hibernate.ServiceOrderDashboardDaoHibernate.SODashboardDTO;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ErrorLog;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ServiceOrderDashboardManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
public class SODashboardAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1L;
	static final Logger logger = Logger.getLogger(SODashboardAction.class);	
	private Long id;	
	private ServiceOrderDashboardManager serviceOrderDashboardManager;	
	Date currentdate = new Date();
	private String soNumber;
	private String action;
	private String userName;
	private Date actionTime;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private List integrationErrorList;
	private ToDoRuleManager toDoRuleManager;
	private String integrationId;
 	private String operation;
	private String orderComplete;
	private String statusCode;
	private Date effectiveDate;
	private List ugwwErrorLogList;
	private String sessionCorpID;
	private List integrationLogs;
	private String trackCorpID;
	private String so;
	private List mssLogList;
	private List centreVanlineList;
	private List serviceOrderSearchType;  
	private String serviceOrderSearchVal;
	private String defaultCompanyCode="";
	private ExtendedPaginatedList serviceOrdersExt;
	private List packingServiceList;
	private Map<String,String> localParameter;
	private Map<String,String> grpStatusList;
	private Map<String,String> grpCountryList;
	private Map<String,String> grpContinentList;
	private Map<String,String> grpModeList;
	private Map<String,String> grpAgeStatusList;
	private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>();
	private Map<String, String> countryCod;
    private Map<String, String> state;
    private Map<String, String> ocountry;
    private Map<String, String> dcountry;
    private Map<String, String> job;
    private Map<String, String> relocationServices;
    private Map<String, String> routing;
    private List mode;
    private Map<String,String> country;
    private Map<String, String> ostates;
    private Map<String, String> dstates;
    private Map<String, String> commodit;
    private Map<String, String> commodits;
    private ServiceOrder serviceOrderCombo;
    private RefMasterManager refMasterManager;
    private CustomerFileManager customerFileManager;   
    private  Map<String, String> coord = new LinkedHashMap<String, String>();
    private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
    private List companyDivis=new ArrayList();
    private String usertype; 
    private String companyCode;
    private Boolean activeStatus;
    private Map<String, String> JOB_STATUS;
    List soDashboardList;
    private String shipNumber;
    private String registrationNumber;
    private String job1;
    private String coordinator;
    private String status;
    private String roleArray;
    private String firstName;
    private  String lastName;
    private ErrorLog errorLog;
    private List distinctCorpId;
    private ErrorLogManager errorLogManager;
    private List errorLogList;
    private String module;
	private String miscVLJob;	
	private String companyDivision;
	private String billToName;
	private String originCity;
	private String destinationCity;
	private String bookingAgentShipNumber;
	private String serviceOrderMoveType;
	private Map visibleFields = new LinkedHashMap<String, String>();
	private ExtendedPaginatedList SOListExt;
	private PagingLookupManager pagingLookupManager;
	private PaginateListFactory paginateListFactory;
	private Map<String, String> moveTypeList=new HashMap<String, String>();
	private boolean checkAccessQuotation=false;
	private Company company;
	private CompanyManager companyManager;
	private String weightUnit;
	private String compDivFlag;
	private String billtoName;
	private User user;
	private List recordLimitList;
	private String recordLimit;
	private String recordLimitForm;
	private String cityCountryZipOption;
	private String originCityOrZip;
	private String destinationCityOrZip;
	private String originCountry; 
	private String destinationCountry;
	private Map<String, String> cityCountryZipSearchOption;
	private String salesMan;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	private String  certificateNumber;
	private String daShipmentNumber;
	private String estimator;
	private String quickFileCabinetView;
	
	public SODashboardAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();		
        usertype=user.getUserType();
        companyCode=user.getCompanyDivision();
        quickFileCabinetView=user.getFileCabinetView();
        if(user.isDefaultSearchStatus()){
 		   activeStatus=true;
 	   }
 	   else{
 		   activeStatus=false;
 	   }
  
	}
	public void prepare() throws Exception {
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
	      company=companyManager.findByCorpID(sessionCorpID).get(0);
	    	 if(company!=null){
	    		checkAccessQuotation=company.getAccessQuotationFromCustomerFile();
	    		compDivFlag=company.getCompanyDivisionFlag();
	    	 }
	    	recordLimitList = new ArrayList();
			recordLimitList.add("20");
			recordLimitList.add("40");
			recordLimitList.add("60");
			recordLimitList.add("80");
			recordLimitList.add("100");
			recordLimit = user.getRecordsForSoDashboard();
			if(recordLimit==null || recordLimit.equals("")){
			  recordLimit = "20"; 
			}
	}
	
	 @SkipValidation
	    public String getComboList(String corpId){
		 Long StartTime = System.currentTimeMillis();
		    
			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		    	     try {
		    	    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Getting parameter values from combolist");
		    	    	 String parameters="'CUST_STATUS','groupagestatus','STATE','COUNTRY', 'JOB','ROUTING','COMMODIT','COMMODITS','PKMODE','LOADSITE', 'SPECIFIC','JOBTYPE','MILITARY','SPECIAL','HOWPACK','AGTPER','EQUIP','PEAK','TCKTSERVC','DEPT','HOUSE', 'CONFIRM','TCKTACTN','WOINSTR','PAYTYPE','STATUSREASON','GLCODES','ISDRIVER','YESNO','SHIPTYPE','partnerType','SITOUTTA','omni',  'QUOTESTATUS','JOB_STATUS','ACC_CATEGORY','QUALITYSURVEYBY','ACC_BASIS','ACC_STATUS','CustomerSettled','AccEstmateStatus'" +
		    	    	 		",'RELOCATIONSERVICES','PackingService','CUST_STATUS','MODE','customerfeedback','OAEVALUATION','DAEVALUATION','CURRENCY','EUVAT','AssignmentType','SERVICE','FLAGCARRIER','LANGUAGE'";
		    	    	
		    	    	 JOB_STATUS  =findByParameterLocal("JOB_STATUS");
		 //  performence  query call
		    	    	 LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
		    	    	 packingServiceList= new ArrayList();
		    	    	 grpCountryList = new LinkedHashMap<String, String>();
		    	    	 grpContinentList = new LinkedHashMap<String, String>();
		    	    	 grpModeList = new LinkedHashMap<String, String>();
		    	    	 relocationServices = new LinkedHashMap<String, String>();
		    	    	 mode = new ArrayList();
		    	    	 companyDivis = customerFileManager.findCompanyDivision(sessionCorpID);	
		    	    	 country = new LinkedHashMap<String, String>(); 	    	
		    	    	 ostates = new LinkedHashMap<String, String>();
		    			 dstates = new LinkedHashMap<String, String>();
		    			 state = new LinkedHashMap<String, String>();		
		    			 countryCod = new LinkedHashMap<String, String>();
		    			 routing = new TreeMap<String, String>();
		    			 job = new TreeMap<String, String>();		    			
		    			 commodits = new TreeMap<String, String>();
		    			 commodit = new TreeMap<String, String>();
		    			 sale = new TreeMap<String, String>();
		    			 //Gson gson = new Gson();
		    			 String CompanyDivisionCombo=getRequest().getParameter("serviceOrder.companyDivision");
		    			 if(CompanyDivisionCombo!=null && (!(CompanyDivisionCombo.toString().equals("")))){
		    					
		    				}else{
		    					if(serviceOrderCombo!=null){
		    					CompanyDivisionCombo=serviceOrderCombo.getCompanyDivision();
		    					}
		    				}
		    			 String jobCombo=getRequest().getParameter("serviceOrder.job");
		    				if(jobCombo!=null && (!(jobCombo.toString().equals("")))){
		    					
		    				}else{
		    					if(serviceOrderCombo!=null){
		    					jobCombo=serviceOrderCombo.getJob();	
		    					}
		    				}
		    				String originCountry="";
				 			String destCountry ="";
				 			if(serviceOrderCombo!=null){
				 				if(serviceOrderCombo.getOriginCountryCode()!=null){
				 					String temp=getRequest().getParameter("serviceOrder.OriginCountryCode");
				 		            if(temp!=null && (!(temp.toString().equals("")))){
				 					
				 				    }else{
				 					temp = serviceOrderCombo.getOriginCountryCode();
				 				    }
				 				}
				 				if(serviceOrderCombo.getDestinationCountryCode()!=null){
				 					String temp=getRequest().getParameter("serviceOrder.destinationCountryCode");
				 		            if(temp!=null && (!(temp.toString().equals("")))){
				 					
				 				    }else{
				 					temp = serviceOrderCombo.getDestinationCountryCode();
				 				    }
				 				}
				 			}	
		    	    	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
		    	    	 for (RefMasterDTO refObj : allParamValue) {
		    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("PackingService")){
		    	    		 		packingServiceList.add(refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("COUNTRY")){
		    	    		 		grpCountryList.put(refObj.getDescription().trim(),refObj.getDescription().trim());
		    	    		 		countryCod.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("COUNTRY") && !refObj.getFlex4().trim().equals("") ){
		    	    		 		grpContinentList.put(refObj.getFlex4().trim(),refObj.getFlex4().trim());    
		    	    		 	}else if(refObj.getParameter().trim().equals("MODE")){
		    	    		 		grpModeList.put(refObj.getDescription().trim(),refObj.getDescription().trim());
		    	    		 		mode.add(refObj.getDescription().trim());
		    	    		 	}else if(refObj.getParameter().trim().equals("RELOCATIONSERVICES")){
		    	    		 		relocationServices.put(refObj.getCode().trim(), refObj.getFlex1().trim());
		    	    		 	}else if (refObj.getParameter().trim().equals("customerfeedback")){
		    	    		 	}else if (refObj.getParameter().trim().equals("OAEVALUATION")){
		    	    		 	}else if (refObj.getParameter().trim().equals("DAEVALUATION")){
		    	    		 	}else if (refObj.getParameter().trim().equals("CURRENCY")){
		    	    		 		country.put(refObj.getCode().trim(),refObj.getCode().trim()); 
		    	    		 	}else if (refObj.getParameter().trim().equals("EUVAT")){
		    	    		 	}else if (refObj.getParameter().trim().equals("SPECIAL")){
		    	    		 	}else if (refObj.getParameter().trim().equals("GLCODES")){
		    	    		 	}else if (refObj.getParameter().trim().equals("MILITARY")){
		    	    		 	}else if(refObj.getParameter().trim().equals("AssignmentType")){
		    	    		 		if(refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("ROLE_CORP_ACC_ASML")){
		    	    		 		}
		    	    	 		}else if (refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals("Relo"))){
		    	    		 	}else if (refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("Relo"))){
		    	    		 	}else if (refObj.getParameter().trim().equals("JOB")){
		    	    		 		if(CompanyDivisionCombo!=null && (!(CompanyDivisionCombo.toString().equals(""))) && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(CompanyDivisionCombo)) ){
		    	    		 			job.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}else{
		    	    		 			job.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}
		    	    		 		if( jobCombo!=null && (!(jobCombo.toString().equals(""))) && refObj.getCode().trim().equals(jobCombo) && refObj.getFlex3()!=null  ){
		        	    		 	}
		    	    		 	}else if(refObj.getParameter().trim().equals("COMMODITS")){
		    	    		 		if(jobCombo!=null && (!(jobCombo.toString().equals("")))&& (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(jobCombo)) ){
		    	    		 			commodits.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}else{
		    	    		 			commodits.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}
		    	    		 	}else if(refObj.getParameter().trim().equals("COMMODIT")){
		    	    		 		if(jobCombo!=null && (!(jobCombo.toString().equals(""))) && (refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") && refObj.getFlex1().trim().equals(jobCombo)) ){
		    	    		 			commodit.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}else{
		    	    		 			commodit.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 		}
		    	    		 	}else if(refObj.getParameter().trim().equals("FLAGCARRIER")){
		    	    		 	}else if(refObj.getParameter().trim().equals("LANGUAGE")){
		    	    		 		 if(refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("") ){
		    	    		 		 }
		    	    		 	}else {
		    	    		 		if(refObj.getParameter().trim().equals("STATE")){
		    	    		 			
		    	    		 			if(originCountry.equals(refObj.getBucket2().trim()))
		    	    		 				ostates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
		    	    		 			if(destCountry.equals(refObj.getBucket2().trim()))
		    	    		 				dstates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
		    	    		 		}
		    	    		 		if(parameterMap.containsKey(refObj.getParameter().trim())){
		    	    		 			tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
		    	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		    	    		 		}else{
		    	    		 			tempParemeterMap = new LinkedHashMap<String, String>();
		    	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		    	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		    	    		 		}
		    	    		 	}
		    	    		
						}
		    	    	 
		    	    	mode = SetUniqueList.decorate(mode);		    	    	
						 ocountry=grpCountryList;
						 dcountry=grpCountryList;			
						coord = refMasterManager.findUser(corpId, "ROLE_COORD");
						 JOB_STATUS  =findByParameterLocal("JOB_STATUS");
						String roles ="'ROLE_COORD','ROLE_SALE','ROLE_QC','ROLE_PRICING','ROLE_PAYABLE','ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC'";
						List <UserDTO> allUser = refMasterManager.getAllUser(corpId,roles);
						for (UserDTO userDTO : allUser) {
							if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
								if(jobCombo!=null && (!(jobCombo.toString().equals("")))  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo)  ) ){
									coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
								}
								coord.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){
								sale.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_QC")){
								//qc.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
								//pricingData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
								//billingData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
								//payableData.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
							}
						}
						cityCountryZipSearchOption = new LinkedHashMap<String, String>();
						cityCountryZipSearchOption.put("City","City");
						cityCountryZipSearchOption.put("Country","Country");
						cityCountryZipSearchOption.put("Zip","Zip");
							
					} catch (Exception e) {
						logger.error(" ERROR in ServiceOrderAction getComboList ");
						e.printStackTrace();
					}
		    	     			  	
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
					  
					return SUCCESS;
	   }
	 
	 @SkipValidation
	    public String soDashboardSearchList() {
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		  //getComboList(sessionCorpID);
		  if(shipNumber==null){
			  shipNumber="";
		  }
		  if(bookingAgentShipNumber==null){
			  bookingAgentShipNumber="";
		  }
		  if(registrationNumber==null){
			  registrationNumber="";
		  }
		  if(lastName==null){
			  lastName="";
		  }
		  if(firstName==null){
			  firstName="";
		  }
		  if(job1==null){
			  job1="";
		  }
		  if(coordinator==null){
			  coordinator="";
		  }
		  if(status==null){
			  status="";
		  }
		  if(companyDivision==null){
			  companyDivision="";
		  }
		  if(billToName==null){
			  billToName="";
		  }
		  if(originCountry == null){
				originCountry = "";
			}
			if(destinationCountry == null){
				destinationCountry = "";
			}
			if(originCityOrZip == null){
				originCityOrZip = "";
			}
			if(destinationCityOrZip == null){
				destinationCityOrZip = "";
			}
		  if(activeStatus==null){
			  activeStatus=true;
		  }
		  if(serviceOrderMoveType==null){
			  serviceOrderMoveType="";
		  }
		  if(salesMan==null){
			  salesMan="";
		  }
		  if(certificateNumber==null)
		  {
			  
            certificateNumber="";
		  }
		  if(daShipmentNumber==null)
		  {
			  
			  daShipmentNumber="";
		  }
		  if(estimator==null)
		  {
			  
			  estimator="";
		  }
		  //System.out.println("so actionnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"+roleArray);
		  soDashboardList = serviceOrderDashboardManager.soDashboardSearchList(shipNumber,registrationNumber,job1, coordinator,status,sessionCorpID,roleArray,lastName,firstName,companyDivision,billToName,activeStatus,bookingAgentShipNumber,serviceOrderMoveType,recordLimitForm,cityCountryZipOption,originCityOrZip,destinationCityOrZip,originCountry,destinationCountry,salesMan,certificateNumber,daShipmentNumber,estimator);
		  //
		  getRequest().setAttribute("soLastName","");			
		   getComboList(sessionCorpID);
			serviceOrderSearchType = new ArrayList();
			serviceOrderSearchType.add("Default Match");
			serviceOrderSearchType.add("Start With");
			serviceOrderSearchType.add("End With");
			serviceOrderSearchType.add("Exact Match");
			if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
				serviceOrderSearchVal="Default Match";
			}
			moveTypeList =new LinkedHashMap<String, String>();
			moveTypeList.put("Quote", "Quote");
			moveTypeList.put("BookedMove", "Booked Move");
			JOB_STATUS.put("Not Delivered","Not Delivered");
			JOB_STATUS.put("Not Loaded","Not Loaded");
			defaultCompanyCode="1";
		   serviceOrdersExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		   List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		   if(sysDefaultDetail!=null && !(sysDefaultDetail.isEmpty()) && sysDefaultDetail.get(0)!=null){
			   SystemDefault sys= (SystemDefault)sysDefaultDetail.get(0);
			    miscVLJob= sys.getMiscVl();
			    weightUnit=sys.getWeightUnit();
		   }
		  // soDashboardList = ugwwActionTrackerManager.findSoDashboardList(sessionCorpID);
	   	  //String key = "Please enter your search criteria below";
	   	  //saveMessage(getText(key));
		  //
		 // String key = "Please enter your search criteria below";
	   	 // saveMessage(getText(key));
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS; 
		  
	  }
	  @SkipValidation
	    public String soDashboardSearchCriteria() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getRequest().setAttribute("soLastName","");	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user = (User)auth.getPrincipal();
		   getComboList(sessionCorpID);
			serviceOrderSearchType = new ArrayList();
			serviceOrderSearchType.add("Default Match");
			serviceOrderSearchType.add("Start With");
			serviceOrderSearchType.add("End With");
			serviceOrderSearchType.add("Exact Match");
			if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
				serviceOrderSearchVal="Default Match";
			}
			moveTypeList =new LinkedHashMap<String, String>();
			moveTypeList.put("Quote", "Quote");
			moveTypeList.put("BookedMove", "Booked Move");
			JOB_STATUS.put("Not Delivered","Not Delivered");
			JOB_STATUS.put("Not Loaded","Not Loaded");
			defaultCompanyCode="1";
		   serviceOrdersExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		   Long t1=System.currentTimeMillis();
		   logger.warn("so dasboard list start .................................. ................");
		   //soDashboardList = ugwwActionTrackerManager.findSoDashboardList(sessionCorpID);
		   soDashboardList =new ArrayList<SODashboardDTO>();
		   Long t2=System.currentTimeMillis();
			logger.warn("so dasboard total execution time.................................. "+(t2-t1));
		   List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		   if(sysDefaultDetail!=null && !(sysDefaultDetail.isEmpty()) && sysDefaultDetail.get(0)!=null){
			   SystemDefault sys= (SystemDefault)sysDefaultDetail.get(0);
			    miscVLJob= sys.getMiscVl();
		   }
	   	  //String key = "Please enter your search criteria below";
	   	  //saveMessage(getText(key));
	   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   	   return SUCCESS; 
	   }
	  @SkipValidation
	  public Map<String,String> findByParameterLocal(String parameter){ 
	  	localParameter = new HashMap<String, String>();
	  	if(parameterMap.containsKey(parameter)){
	  		localParameter = parameterMap.get(parameter);
	  	}
	  	return localParameter;
	  }
	  
	  private List SearchforBilltoNameAutocompleteList;
	  private String SearchforBilltoNameGsonData;  
	  @SkipValidation
		public String searchBilltoNameAutocompleteList(){
			try {
				if(billtoName.length()>=3){
				SearchforBilltoNameAutocompleteList = customerFileManager.findBilltoNameAutocompleteList(billtoName,sessionCorpID);
				}
				SearchforBilltoNameGsonData = new Gson().toJson(SearchforBilltoNameAutocompleteList);
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return CANCEL;
			}
			return SUCCESS;
		}
	  @SkipValidation
	  public void soDashboardExtract() throws ServletException,IOException{
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			StringBuffer query = new StringBuffer();
			StringBuffer fileName = new StringBuffer();
			String header = new String();
			if(certificateNumber==null)
			  {
				  
	            certificateNumber="";
			  }
			if(daShipmentNumber==null)
			  {
				  
				daShipmentNumber="";
			  }
			soDashboardList = serviceOrderDashboardManager.soDashboardSearchList(shipNumber,registrationNumber,job1, coordinator,status,sessionCorpID,roleArray,lastName,firstName,companyDivision,billToName,activeStatus,bookingAgentShipNumber,serviceOrderMoveType,recordLimitForm,cityCountryZipOption,originCityOrZip,destinationCityOrZip,originCountry,destinationCountry,salesMan,certificateNumber,daShipmentNumber,estimator); 
		  try{
	    		if (!soDashboardList.isEmpty()) {    					
	    			HttpServletResponse response = getResponse();
	    			response.setContentType("application/vnd.ms-excel");
	    			ServletOutputStream outputStream = response.getOutputStream();
	    			File file = new File("SODashboardExtract");
	    			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
	    			response.setHeader("Pragma", "public");
	    			response.setHeader("Cache-Control", "max-age=0");
	    			outputStream.write("ShipNumber\t".getBytes());
					outputStream.write("Registration Number\t".getBytes());
					outputStream.write("Last Name\t".getBytes());
					outputStream.write("First Name\t".getBytes());
					outputStream.write("Role\t".getBytes());
					outputStream.write("Job\t".getBytes());
					outputStream.write("Service\t".getBytes());
					outputStream.write("Mode\t".getBytes());
					outputStream.write("Routing\t".getBytes());
					outputStream.write("Commodity\t".getBytes());
					outputStream.write("SO Status\t".getBytes());
					outputStream.write("Survey\t".getBytes());
					outputStream.write("Est wt/vol\t".getBytes());
					outputStream.write("Loading\t".getBytes());
					outputStream.write("Act wt/vol\t".getBytes());
					outputStream.write("Driver\t".getBytes());
					outputStream.write("Truck\t".getBytes());
					outputStream.write("Departure\t".getBytes());
					outputStream.write("Arrival\t".getBytes());
					outputStream.write("Clear Customs\t".getBytes());
					outputStream.write("Delivery\t".getBytes());
					outputStream.write("Audited\t".getBytes());
					outputStream.write("Billing Party\t".getBytes());
					outputStream.write("Origin Country/City\t".getBytes());
					outputStream.write("Destination Country/City\t".getBytes());
					outputStream.write("Coordinator\t".getBytes());
					outputStream.write("Created On\t".getBytes());
					
					outputStream.write("\r\n".getBytes());
					
					Iterator it = soDashboardList.iterator();
					
					while (it.hasNext()) {
						Object extract = (Object) it.next();
						
						if(((SODashboardDTO)extract).getShipnumber()!=null){
							outputStream.write((((SODashboardDTO)extract).getShipnumber().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getRegistrationnumber()!=null){
							outputStream.write((((SODashboardDTO)extract).getRegistrationnumber().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getLastName()!=null){
							outputStream.write((((SODashboardDTO)extract).getLastName().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getFirstName()!=null){
							outputStream.write((((SODashboardDTO)extract).getFirstName().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getRole()!=null){
							outputStream.write((((SODashboardDTO)extract).getRole().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getJob()!=null){
							outputStream.write((((SODashboardDTO)extract).getJob().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getShipmenttype()!=null){
							outputStream.write((((SODashboardDTO)extract).getShipmenttype().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getMode()!=null){
							outputStream.write((((SODashboardDTO)extract).getMode().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getRouting()!=null){
							outputStream.write((((SODashboardDTO)extract).getRouting().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getCommodity()!=null){
							outputStream.write((((SODashboardDTO)extract).getCommodity().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getStatus()!=null){
							outputStream.write((((SODashboardDTO)extract).getStatus().toString() + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getsD()!=null){	
							outputStream.write((((SODashboardDTO)extract).getsD().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if( ( ((SODashboardDTO)extract).getEstweight()!=null && !((SODashboardDTO)extract).getEstweight().equals("")) && (((SODashboardDTO)extract).getEstVolume()!=null && !((SODashboardDTO)extract).getEstVolume().equals(""))){
							outputStream.write((((SODashboardDTO)extract).getEstweight().toString() +"/"+ ((SODashboardDTO)extract).getEstVolume().toString() + "\t").getBytes());	
						}else if((((SODashboardDTO)extract).getEstweight()==null || ((SODashboardDTO)extract).getEstweight().equals("")) && (((SODashboardDTO)extract).getEstVolume()!=null) && !((SODashboardDTO)extract).getEstVolume().equals("")){
							outputStream.write(("-" +"/"+ ((SODashboardDTO)extract).getEstVolume().toString() + "\t").getBytes());
						}else if((((SODashboardDTO)extract).getEstweight()!=null && !((SODashboardDTO)extract).getEstweight().equals("")) && (((SODashboardDTO)extract).getEstVolume()==null || ((SODashboardDTO)extract).getEstVolume().equals(""))){
							outputStream.write((((SODashboardDTO)extract).getEstweight().toString() +"/"+ "-" + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getLd()!=null){
							outputStream.write((((SODashboardDTO)extract).getLd().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if((((SODashboardDTO)extract).getActualweight()!=null && !((SODashboardDTO)extract).getActualweight().equals("")) && (((SODashboardDTO)extract).getActualVolume()!=null && !((SODashboardDTO)extract).getActualVolume().equals(""))){
							outputStream.write((((SODashboardDTO)extract).getActualweight().toString() +"/"+ ((SODashboardDTO)extract).getActualVolume().toString() + "\t").getBytes());	
						}else if((((SODashboardDTO)extract).getActualweight()==null || ((SODashboardDTO)extract).getActualweight().equals("")) && (((SODashboardDTO)extract).getActualVolume()!=null && !((SODashboardDTO)extract).getActualVolume().equals(""))){
							outputStream.write(("-" +"/"+ ((SODashboardDTO)extract).getActualVolume().toString() + "\t").getBytes());
						}else if((((SODashboardDTO)extract).getActualweight()!=null && !((SODashboardDTO)extract).getActualweight().equals("")) && (((SODashboardDTO)extract).getActualVolume()==null || ((SODashboardDTO)extract).getActualVolume().equals(""))){
							outputStream.write((((SODashboardDTO)extract).getActualweight().toString() +"/"+ "-" + "\t").getBytes());
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getDriver()!=null){
							outputStream.write((((SODashboardDTO)extract).getDriver().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getTruck()!=null){
							outputStream.write((((SODashboardDTO)extract).getTruck().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getDep()!=null){
							outputStream.write((((SODashboardDTO)extract).getDep().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getArv()!=null){
							outputStream.write((((SODashboardDTO)extract).getArv().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getCustomDate()!=null){
							outputStream.write((((SODashboardDTO)extract).getCustomDate().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getdL()!=null){
							outputStream.write((((SODashboardDTO)extract).getdL().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getQc()!=null){
							outputStream.write((((SODashboardDTO)extract).getQc().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getBillToName()!=null){
							outputStream.write((((SODashboardDTO)extract).getBillToName().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getOrginCC()!=null){
							outputStream.write((((SODashboardDTO)extract).getOrginCC().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getDestinCC()!=null){
							outputStream.write((((SODashboardDTO)extract).getDestinCC().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getCoordinator()!=null){
							outputStream.write((((SODashboardDTO)extract).getCoordinator().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						
						if(((SODashboardDTO)extract).getCreatedOn()!=null){
							outputStream.write((((SODashboardDTO)extract).getCreatedOn().toString() + "\t").getBytes());	
						}else{
							outputStream.write("\t".getBytes());
						}
						outputStream.write("\r\n".getBytes());
					}
	    			
	    		}else{
	    			HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					File file1 = new File("SODashboardExtract");
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());
	    		}
		  } catch (Exception e) {
				System.out.println("ERROR:" + e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
	  }

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Date getCurrentdate() {
		return currentdate;
	}


	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}


	public String getSoNumber() {
		return soNumber;
	}


	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public Date getActionTime() {
		return actionTime;
	}


	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}


	public String getCorpID() {
		return corpID;
	}


	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	public List getIntegrationErrorList() {
		return integrationErrorList;
	}


	public void setIntegrationErrorList(List integrationErrorList) {
		this.integrationErrorList = integrationErrorList;
	}


	public String getIntegrationId() {
		return integrationId;
	}


	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public String getOrderComplete() {
		return orderComplete;
	}


	public void setOrderComplete(String orderComplete) {
		this.orderComplete = orderComplete;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public Date getEffectiveDate() {
		return effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public List getUgwwErrorLogList() {
		return ugwwErrorLogList;
	}


	public void setUgwwErrorLogList(List ugwwErrorLogList) {
		this.ugwwErrorLogList = ugwwErrorLogList;
	}


	public String getSessionCorpID() {
		return sessionCorpID;
	}


	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public List getIntegrationLogs() {
		return integrationLogs;
	}


	public void setIntegrationLogs(List integrationLogs) {
		this.integrationLogs = integrationLogs;
	}


	public String getTrackCorpID() {
		return trackCorpID;
	}


	public void setTrackCorpID(String trackCorpID) {
		this.trackCorpID = trackCorpID;
	}


	public String getSo() {
		return so;
	}


	public void setSo(String so) {
		this.so = so;
	}


	public List getMssLogList() {
		return mssLogList;
	}


	public void setMssLogList(List mssLogList) {
		this.mssLogList = mssLogList;
	}


	public List getCentreVanlineList() {
		return centreVanlineList;
	}


	public void setCentreVanlineList(List centreVanlineList) {
		this.centreVanlineList = centreVanlineList;
	}


	public List getServiceOrderSearchType() {
		return serviceOrderSearchType;
	}


	public void setServiceOrderSearchType(List serviceOrderSearchType) {
		this.serviceOrderSearchType = serviceOrderSearchType;
	}


	public String getServiceOrderSearchVal() {
		return serviceOrderSearchVal;
	}


	public void setServiceOrderSearchVal(String serviceOrderSearchVal) {
		this.serviceOrderSearchVal = serviceOrderSearchVal;
	}


	public String getDefaultCompanyCode() {
		return defaultCompanyCode;
	}


	public void setDefaultCompanyCode(String defaultCompanyCode) {
		this.defaultCompanyCode = defaultCompanyCode;
	}


	public ExtendedPaginatedList getServiceOrdersExt() {
		return serviceOrdersExt;
	}


	public void setServiceOrdersExt(ExtendedPaginatedList serviceOrdersExt) {
		this.serviceOrdersExt = serviceOrdersExt;
	}


	public List getPackingServiceList() {
		return packingServiceList;
	}


	public void setPackingServiceList(List packingServiceList) {
		this.packingServiceList = packingServiceList;
	}


	public Map<String, String> getLocalParameter() {
		return localParameter;
	}


	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}


	public Map<String, String> getGrpStatusList() {
		return grpStatusList;
	}


	public void setGrpStatusList(Map<String, String> grpStatusList) {
		this.grpStatusList = grpStatusList;
	}


	public Map<String, String> getGrpCountryList() {
		return grpCountryList;
	}


	public void setGrpCountryList(Map<String, String> grpCountryList) {
		this.grpCountryList = grpCountryList;
	}


	public Map<String, String> getGrpContinentList() {
		return grpContinentList;
	}


	public void setGrpContinentList(Map<String, String> grpContinentList) {
		this.grpContinentList = grpContinentList;
	}


	public Map<String, String> getGrpModeList() {
		return grpModeList;
	}


	public void setGrpModeList(Map<String, String> grpModeList) {
		this.grpModeList = grpModeList;
	}


	public Map<String, String> getGrpAgeStatusList() {
		return grpAgeStatusList;
	}


	public void setGrpAgeStatusList(Map<String, String> grpAgeStatusList) {
		this.grpAgeStatusList = grpAgeStatusList;
	}


	public Map<String, LinkedHashMap<String, String>> getParameterMap() {
		return parameterMap;
	}


	public void setParameterMap(
			Map<String, LinkedHashMap<String, String>> parameterMap) {
		this.parameterMap = parameterMap;
	}


	public Map<String, String> getCountryCod() {
		return countryCod;
	}


	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}


	public Map<String, String> getState() {
		return state;
	}


	public void setState(Map<String, String> state) {
		this.state = state;
	}


	public Map<String, String> getOcountry() {
		return ocountry;
	}


	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}


	public Map<String, String> getDcountry() {
		return dcountry;
	}


	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}


	public Map<String, String> getJob() {
		return job;
	}


	public void setJob(Map<String, String> job) {
		this.job = job;
	}


	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}


	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}


	public Map<String, String> getRouting() {
		return routing;
	}


	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}


	public List getMode() {
		return mode;
	}


	public void setMode(List mode) {
		this.mode = mode;
	}


	public Map<String, String> getCountry() {
		return country;
	}


	public void setCountry(Map<String, String> country) {
		this.country = country;
	}


	public Map<String, String> getOstates() {
		return ostates;
	}


	public void setOstates(Map<String, String> ostates) {
		this.ostates = ostates;
	}


	public Map<String, String> getDstates() {
		return dstates;
	}


	public void setDstates(Map<String, String> dstates) {
		this.dstates = dstates;
	}


	public Map<String, String> getCommodit() {
		return commodit;
	}


	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}


	public Map<String, String> getCommodits() {
		return commodits;
	}


	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}


	public ServiceOrder getServiceOrderCombo() {
		return serviceOrderCombo;
	}


	public void setServiceOrderCombo(ServiceOrder serviceOrderCombo) {
		this.serviceOrderCombo = serviceOrderCombo;
	}


	public Map<String, String> getCoord() {
		return coord;
	}


	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}


	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}


	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}


	public List getCompanyDivis() {
		return companyDivis;
	}


	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}


	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}


	public String getCompanyCode() {
		return companyCode;
	}


	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}


	public Boolean getActiveStatus() {
		return activeStatus;
	}


	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}


	public Map<String, String> getJOB_STATUS() {
		return JOB_STATUS;
	}


	public void setJOB_STATUS(Map<String, String> jOB_STATUS) {
		JOB_STATUS = jOB_STATUS;
	}


	public List getSoDashboardList() {
		return soDashboardList;
	}


	public void setSoDashboardList(List soDashboardList) {
		this.soDashboardList = soDashboardList;
	}


	public String getShipNumber() {
		return shipNumber;
	}


	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}


	public String getRegistrationNumber() {
		return registrationNumber;
	}


	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}


	public String getJob1() {
		return job1;
	}


	public void setJob1(String job1) {
		this.job1 = job1;
	}


	public String getCoordinator() {
		return coordinator;
	}


	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getRoleArray() {
		return roleArray;
	}


	public void setRoleArray(String roleArray) {
		this.roleArray = roleArray;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public ErrorLog getErrorLog() {
		return errorLog;
	}


	public void setErrorLog(ErrorLog errorLog) {
		this.errorLog = errorLog;
	}


	public List getDistinctCorpId() {
		return distinctCorpId;
	}


	public void setDistinctCorpId(List distinctCorpId) {
		this.distinctCorpId = distinctCorpId;
	}


	public List getErrorLogList() {
		return errorLogList;
	}


	public void setErrorLogList(List errorLogList) {
		this.errorLogList = errorLogList;
	}


	public String getModule() {
		return module;
	}


	public void setModule(String module) {
		this.module = module;
	}


	public String getMiscVLJob() {
		return miscVLJob;
	}


	public void setMiscVLJob(String miscVLJob) {
		this.miscVLJob = miscVLJob;
	}


	public String getCompanyDivision() {
		return companyDivision;
	}


	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}


	public String getBillToName() {
		return billToName;
	}


	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}


	public String getOriginCity() {
		return originCity;
	}


	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}


	public String getDestinationCity() {
		return destinationCity;
	}


	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}


	public Map getVisibleFields() {
		return visibleFields;
	}


	public void setVisibleFields(Map visibleFields) {
		this.visibleFields = visibleFields;
	}


	public void setServiceOrderDashboardManager(ServiceOrderDashboardManager serviceOrderDashboardManager) {
		this.serviceOrderDashboardManager = serviceOrderDashboardManager;
	}


	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}


	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}


	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}


	public ExtendedPaginatedList getSOListExt() {
		return SOListExt;
	}


	public void setSOListExt(ExtendedPaginatedList sOListExt) {
		SOListExt = sOListExt;
	}


	public PagingLookupManager getPagingLookupManager() {
		return pagingLookupManager;
	}


	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}


	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}


	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}


	public String getBookingAgentShipNumber() {
		return bookingAgentShipNumber;
	}


	public void setBookingAgentShipNumber(String bookingAgentShipNumber) {
		this.bookingAgentShipNumber = bookingAgentShipNumber;
	}


	public Map<String, String> getMoveTypeList() {
		return moveTypeList;
	}


	public void setMoveTypeList(Map<String, String> moveTypeList) {
		this.moveTypeList = moveTypeList;
	}


	public boolean isCheckAccessQuotation() {
		return checkAccessQuotation;
	}


	public void setCheckAccessQuotation(boolean checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}


	public String getServiceOrderMoveType() {
		return serviceOrderMoveType;
	}


	public void setServiceOrderMoveType(String serviceOrderMoveType) {
		this.serviceOrderMoveType = serviceOrderMoveType;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	public String getCompDivFlag() {
		return compDivFlag;
	}
	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}
	public String getBilltoName() {
		return billtoName;
	}
	public void setBilltoName(String billtoName) {
		this.billtoName = billtoName;
	}
	public List getSearchforBilltoNameAutocompleteList() {
		return SearchforBilltoNameAutocompleteList;
	}
	public void setSearchforBilltoNameAutocompleteList(
			List searchforBilltoNameAutocompleteList) {
		SearchforBilltoNameAutocompleteList = searchforBilltoNameAutocompleteList;
	}
	public String getSearchforBilltoNameGsonData() {
		return SearchforBilltoNameGsonData;
	}
	public void setSearchforBilltoNameGsonData(String searchforBilltoNameGsonData) {
		SearchforBilltoNameGsonData = searchforBilltoNameGsonData;
	}
	public String getRecordLimit() {
		return recordLimit;
	}
	public void setRecordLimit(String recordLimit) {
		this.recordLimit = recordLimit;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List getRecordLimitList() {
		return recordLimitList;
	}
	public void setRecordLimitList(List recordLimitList) {
		this.recordLimitList = recordLimitList;
	}
	public String getRecordLimitForm() {
		return recordLimitForm;
	}
	public void setRecordLimitForm(String recordLimitForm) {
		this.recordLimitForm = recordLimitForm;
	}
	public String getCityCountryZipOption() {
		return cityCountryZipOption;
	}
	public void setCityCountryZipOption(String cityCountryZipOption) {
		this.cityCountryZipOption = cityCountryZipOption;
	}
	public String getOriginCityOrZip() {
		return originCityOrZip;
	}
	public void setOriginCityOrZip(String originCityOrZip) {
		this.originCityOrZip = originCityOrZip;
	}
	public String getDestinationCityOrZip() {
		return destinationCityOrZip;
	}
	public void setDestinationCityOrZip(String destinationCityOrZip) {
		this.destinationCityOrZip = destinationCityOrZip;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	public Map<String, String> getCityCountryZipSearchOption() {
		return cityCountryZipSearchOption;
	}
	public void setCityCountryZipSearchOption(Map<String, String> cityCountryZipSearchOption) {
		this.cityCountryZipSearchOption = cityCountryZipSearchOption;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	
	public Map<String, String> getSale() {
		return sale;
	}
	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public String getDaShipmentNumber() {
		return daShipmentNumber;
	}
	public void setDaShipmentNumber(String daShipmentNumber) {
		this.daShipmentNumber = daShipmentNumber;
	}
	public String getEstimator() {
		return estimator;
	}
	public void setEstimator(String estimator) {
		this.estimator = estimator;
	}
	public String getQuickFileCabinetView() {
		return quickFileCabinetView;
	}
	public void setQuickFileCabinetView(String quickFileCabinetView) {
		this.quickFileCabinetView = quickFileCabinetView;
	}
}
