<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="carrierDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='carrierDetail.heading'/>"/>
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>

 
</head>   
<s:form id="carrierForm" action="saveCarrier" method="post" validate="true">   
<s:hidden name="id" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="carrier.id" />
<s:hidden name="serviceOrder.shipNumber"/> 
<s:hidden name="serviceOrder.id"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="agent.id" />
<s:hidden name="agent.shipNumber" />
<s:hidden name="carrier.shipNumber" value="%{serviceOrder.shipNumber}"/>
 
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editAgent.html?id=${ServiceOrderID}" >Agent</td>
			<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Carrier</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editServiceOrderUpdate.html?id=${ServiceOrderID}">Service Orders</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			
			
		</tr>
	</tbody>
</table>
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
	<tr><td align="left" class="listwhite">
		<table>
		  <tbody>  	
		  	<tr><td align="left" class="listwhite"><fmt:message key="billing.shipper"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.firstName"  size="14" readonly="true"  onfocus="myDate();"/><td align="left" class="listwhite"><s:textfield name="serviceOrder.lastName"  size="11" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.originCity"  size="10" readonly="true"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.originCountryCode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.Job"   size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.commodity"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.commodity"  size="6" readonly="true"/></td><td align="left" class="listwhite"><fmt:message key="billing.routing"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.routing"   size="1" readonly="true"/></td></tr>
		  </tbody>
		 </table>
 	</td></tr>
 	<tr><td align="left" class="listwhite">
		 <table>
		  <tbody>  	
		  	<tr><td align="right" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left" class="listwhite"><s:textfield name="customerFileNumber" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.registrationNo"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.registrationNumber"  size="11" readonly="true"/></td><td align="left" class="listwhite">&nbsp;<fmt:message key="billing.destination"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.destinationCity"  size="10" readonly="true"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.destinationCountryCode"  size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.mode"   size="1" readonly="true"/></td><td align="left" class="listwhite">&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.billTo"/></td><td align="left" class="listwhite"><s:textfield name="serviceOrder.billToCode"   size="12" readonly="true"/></td></tr>
		  </tbody>
		 </table>
	 </td></tr>
   </tbody>
 </table>


    <table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
			<table cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
                     <tr>
  <td align="left" class="listwhite"><fmt:message key="carrier.carrier"/></td>
  <td align="left" class="listwhite"><s:textfield key="carrier.carrierNumber" required="true" size="17" readonly="true" maxlength="2"/></td>
  </tr>
  <tr>
 <td align="left" class="listwhite"><fmt:message key="carrier.carrierCode"/></td>
<td align="left" class="listwhite"><s:textfield key="carrier.carrierCode" required="true" size="17" maxlength="8" readonly="true" /></td>
<td align="left" class="listwhite"><s:textfield key="carrier.carrierName"  required="true" size="17" maxlength="35" readonly="true" /></td>
<td width="17"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('partners.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=carrier.carrierName&fld_code=carrier.carrierCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
</tr>
  <tr>
  <td align="left" class="listwhite"><fmt:message key="carrier.carrierPhone"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.carrierPhone" required="true" size="17" maxlength="14"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="carrier.carrierVessels"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.carrierVessels" required="true" size="17" maxlength="20"/></td>
 </tr>
 <tr>
  <td align="left" class="listwhite"><fmt:message key="carrier.carrierDeparture"/></td>
<td align="left" class="listwhite"><s:textfield key="carrier.carrierDeparture" required="true" size="17" maxlength="20"/></td>
<td></td><td align="left" class="listwhite"><fmt:message key="carrier.ETDTA"/></td>
<td align="left" class="listwhite"><s:select name="carrier.eTDTA" list="%{SITOUTTA}" cssStyle="width:57px"/></td>
<s:text id="customerFileNextCheckFormattedValue" name="FormDateValue"><s:param name="value" value="carrier.carrierTD"/></s:text>
<td><s:textfield name="carrier.carrierTD" value="%{customerFileNextCheckFormattedValue}" required="true" size="10" maxlength="11" onclick="displayDatePicker(this, this);" readonly="true"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="carrier.carrierArrival"/></td>
<td align="left" class="listwhite"><s:textfield key="carrier.carrierArrival" required="true" size="17" maxlength="20"/></td>
<td></td><td align="left" class="listwhite"><fmt:message key="carrier.ETATA"/></td>
<td align="left" class="listwhite"><s:select name="carrier.eTATA" list="%{SITOUTTA}" cssStyle="width:57px"/></td>
<s:text id="customerFileNextCheckFormattedValue" name="FormDateValue"><s:param name="value" value="carrier.carrierTA"/></s:text>
<td><s:textfield name="carrier.carrierTA" value="%{customerFileNextCheckFormattedValue}" required="true" size="10" maxlength="11" onclick="displayDatePicker(this, this);" readonly="true"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="carrier.bookNumber"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.bookNumber" required="true" size="17" maxlength="25"/></td>
</tr>

<tr>
  <td align="left" class="listwhite"><fmt:message key="carrier.miles"/></td>
<td align="left" class="listwhite"><s:textfield key="carrier.miles" required="true" size="17" maxlength="4" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" /></td>
<td></td><td align="left" class="listwhite"><fmt:message key="carrier.omini"/></td>
<td align="left" class="listwhite"><s:select name="carrier.omni" list="%{omni}" cssStyle="width:57px" /></td>
</tr>
</tbody>
</table>
<table width='98%' >
<tbody>
<tr>
<td align="left" class="listwhite"><fmt:message key="carrier.pdFreedays"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.pdFreedays" required="true" size="3"  maxlength="3" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" /></td>
<td align="left" class="listwhite"><fmt:message key="carrier.pdCost1"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.pdCost1" required="true" size="5"  maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" /></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="carrier.pdDays2"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.pdDays2" required="true" size="3"  maxlength="3" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" /></td>
<td align="left" class="listwhite"><fmt:message key="carrier.pdCost2"/></td>
<td align="left" class="listwhite"><s:textfield name="carrier.pdCost2" required="true" size="5"  maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" /></td>
</tr>

<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											
										</tbody>
									</table>
					<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='carrier.createdOn'/></td>
						<s:hidden name="carrier.createdOn" />
						<td><s:date name="carrier.createdOn" format="dd-MMM-yyyy"/></td>		
						<td align="right" class="listwhite"><fmt:message key='carrier.createdBy' /></td>
						
						
						<c:if test="${not empty carrier.id}">
								<s:hidden name="carrier.createdBy"/>
								<td><s:label name="createdBy" value="%{carrier.createdBy}"/></td>
							</c:if>
							<c:if test="${empty carrier.id}">
								<s:hidden name="carrier.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
												
						<td align="right" class="listwhite"><fmt:message key='carrier.updatedOn'/></td>
						<s:hidden name="carrier.updatedOn"/>
						<td><s:date name="carrier.updatedOn" format="dd-MMM-yyyy"/></td>
						<td align="right" class="listwhite"><fmt:message key='carrier.updatedBy' /></td>
						<s:hidden name="carrier.updatedBy" value="${pageContext.request.remoteUser}" />
						<td><s:label name="carrier.updatedBy" /></td>
					</tr>
				</tbody>
			</table>
									</td>
									<td background="<c:url value='/images/bg-right.jpg'/>"
										align="left" class="listwhite"></td>
								</tr>
								<tr>
									<td></td>
									</tr>
						<td valign="top" align="left" class="listwhite" />
						<td align="left" class="listwhite" />
								<tr>
									<td colspan="5"></td>
								</tr>
						<td align="left" class="listwhite"></td>
					<tr>
						<td align="left" class="listwhite" />
						<td valign="top" align="left" class="listwhite">
						<div id="layer" />
						</td>
						<td align="left" class="listwhite" />
					</tr>
					<tr>
						<td align="left" class="listwhite"></td>
						<td valign="top" align="left" class="listwhite" />
						<td align="left" class="listwhite" />
					</td>
					</tr>
					</tbody>
									</table>						
									</div>
    <li class="buttonBar bottom">            
        <s:submit cssClass="button" method="save" key="button.save"/>   
        <c:if test="${not empty carrier.id}">    
            <s:reset type="button" key="Reset" onmouseout="myDate();"/>   
        </c:if>   
    </li> 
    <s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
</s:form>   

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	var tim1=document.forms['carrierForm'].elements['carrier.carrierTA'].value;
	var tim2=document.forms['carrierForm'].elements['carrier.carrierTD'].value;
	if(tim1.length == 5 || tim2.length == 5 ) {
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	}
	if(document.forms['carrierForm'].elements['carrier.carrierTA'].value == "null" ){
		document.forms['carrierForm'].elements['carrier.carrierTA'].value="";
	}
	if(document.forms['carrierForm'].elements['carrier.carrierTD'].value == "null" ){
		document.forms['carrierForm'].elements['carrier.carrierTD'].value="";
	}
}
</script>  
 
<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>  
<%-- Shifting Closed Here --%> 
  
<script type="text/javascript">   
    Form.focusFirstElement($("carrierForm"));
</script>