





delimiter $$
create trigger redsky.trigger_add_history_contract BEFORE UPDATE on redsky.contract
for each row 

BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
     
  


  IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'contract.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;


  IF (OLD.billingInstruction <> NEW.billingInstruction ) THEN
      select CONCAT(fieldNameList,'contract.billingInstruction~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstruction,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstruction,'~') into newValueList;
  END IF;
  
  IF (OLD.accountCode <> NEW.accountCode ) THEN
      select CONCAT(fieldNameList,'contract.accountCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountCode,'~') into newValueList;
  END IF;
  
  IF (OLD.clientPostingGroup <> NEW.clientPostingGroup ) THEN
      select CONCAT(fieldNameList,'contract.clientPostingGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.clientPostingGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.clientPostingGroup,'~') into newValueList;
  END IF;
  
  IF (OLD.accountName <> NEW.accountName ) THEN
      select CONCAT(fieldNameList,'contract.accountName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountName,'~') into newValueList;
  END IF;



 IF (OLD.contract <> NEW.contract ) THEN
      select CONCAT(fieldNameList,'contract.contract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contract,'~') into newValueList;
  END IF;




       IF ((OLD.discount <> NEW.discount) or (OLD.discount is null and NEW.discount is not null)
      or (OLD.discount is not null and NEW.discount is null)) THEN
        select CONCAT(fieldNameList,'contract.discount~') into fieldNameList;
        IF(OLD.discount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.discount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.discount is not null) then
            select CONCAT(oldValueList,OLD.discount,'~') into oldValueList;
        END IF;
        IF(NEW.discount is not null) then
            select CONCAT(newValueList,NEW.discount,'~') into newValueList;
        END IF;
    END IF;


 IF (OLD.insuranceHas <> NEW.insuranceHas ) THEN
      select CONCAT(fieldNameList,'contract.insuranceHas~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceHas,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceHas,'~') into newValueList;
  END IF;



       IF ((OLD.otherDiscount <> NEW.otherDiscount) or (OLD.otherDiscount is null and NEW.otherDiscount is not null)
      or (OLD.otherDiscount is not null and NEW.otherDiscount is null)) THEN
        select CONCAT(fieldNameList,'contract.otherDiscount~') into fieldNameList;
        IF(OLD.otherDiscount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.otherDiscount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.otherDiscount is not null) then
            select CONCAT(oldValueList,OLD.otherDiscount,'~') into oldValueList;
        END IF;
        IF(NEW.otherDiscount is not null) then
            select CONCAT(newValueList,NEW.otherDiscount,'~') into newValueList;
        END IF;
    END IF;



    
       IF ((OLD.sitDiscount <> NEW.sitDiscount) or (OLD.sitDiscount is null and NEW.sitDiscount is not null)
      or (OLD.sitDiscount is not null and NEW.sitDiscount is null)) THEN
        select CONCAT(fieldNameList,'contract.sitDiscount~') into fieldNameList;
        IF(OLD.sitDiscount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sitDiscount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.sitDiscount is not null) then
            select CONCAT(oldValueList,OLD.sitDiscount,'~') into oldValueList;
        END IF;
        IF(NEW.sitDiscount is not null) then
            select CONCAT(newValueList,NEW.sitDiscount,'~') into newValueList;
        END IF;
    END IF;



    
 IF (OLD.billingInstructionCode <> NEW.billingInstructionCode ) THEN
      select CONCAT(fieldNameList,'contract.billingInstructionCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstructionCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstructionCode,'~') into newValueList;
  END IF;




    



 IF (OLD.tariff <> NEW.tariff ) THEN
      select CONCAT(fieldNameList,'contract.tariff~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tariff,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tariff,'~') into newValueList;
  END IF;



    IF ((OLD.ending <> NEW.ending) or (OLD.ending is null and NEW.ending is not null)
      or (OLD.ending is not null and NEW.ending is null)) THEN

       select CONCAT(fieldNameList,'contract.ending~') into fieldNameList;
       IF(OLD.ending is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.ending is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.ending is not null) THEN
        	select CONCAT(oldValueList,OLD.ending,'~') into oldValueList;
     	END IF;
      	IF(NEW.ending is not null) THEN
        	select CONCAT(newValueList,NEW.ending,'~') into newValueList;
      	END IF;

   END IF;


   
    IF ((OLD.begin <> NEW.begin) or (OLD.begin is null and NEW.begin is not null)
      or (OLD.begin is not null and NEW.begin is null)) THEN

       select CONCAT(fieldNameList,'contract.begin~') into fieldNameList;
       IF(OLD.begin is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.begin is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.begin is not null) THEN
        	select CONCAT(oldValueList,OLD.begin,'~') into oldValueList;
     	END IF;
      	IF(NEW.begin is not null) THEN
        	select CONCAT(newValueList,NEW.begin,'~') into newValueList;
      	END IF;

   END IF;



   
 IF (OLD.details <> NEW.details ) THEN
      select CONCAT(fieldNameList,'contract.details~') into fieldNameList;
      select CONCAT(oldValueList,OLD.details,'~') into oldValueList;
      select CONCAT(newValueList,NEW.details,'~') into newValueList;
  END IF;


    
 IF (OLD.entitled <> NEW.entitled ) THEN
      select CONCAT(fieldNameList,'contract.entitled~') into fieldNameList;
      select CONCAT(oldValueList,OLD.entitled,'~') into oldValueList;
      select CONCAT(newValueList,NEW.entitled,'~') into newValueList;
  END IF;


 IF (OLD.group1 <> NEW.group1 ) THEN
      select CONCAT(fieldNameList,'contract.group1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.group1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.group1,'~') into newValueList;
  END IF;



 IF (OLD.insoptCode <> NEW.insoptCode ) THEN
      select CONCAT(fieldNameList,'contract.insoptCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insoptCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insoptCode,'~') into newValueList;
  END IF;



 IF (OLD.insThrough <> NEW.insThrough ) THEN
      select CONCAT(fieldNameList,'contract.insThrough~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insThrough,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insThrough,'~') into newValueList;
  END IF;



 IF ((OLD.insvaluEntitled <> NEW.insvaluEntitled) or (OLD.insvaluEntitled is null and NEW.insvaluEntitled is not null)
      or (OLD.insvaluEntitled is not null and NEW.insvaluEntitled is null)) THEN
        select CONCAT(fieldNameList,'contract.insvaluEntitled~') into fieldNameList;
        IF(OLD.insvaluEntitled is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.insvaluEntitled is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.insvaluEntitled is not null) then
            select CONCAT(oldValueList,OLD.insvaluEntitled,'~') into oldValueList;
        END IF;
        IF(NEW.insvaluEntitled is not null) then
            select CONCAT(newValueList,NEW.insvaluEntitled,'~') into newValueList;
        END IF;
    END IF;




 IF (OLD.invnum <> NEW.invnum ) THEN
      select CONCAT(fieldNameList,'contract.invnum~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invnum,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invnum,'~') into newValueList;
  END IF;


  
 IF (OLD.jobType <> NEW.jobType ) THEN
      select CONCAT(fieldNameList,'contract.jobType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
  END IF;



  IF ((OLD.minimumPay <> NEW.minimumPay) or (OLD.minimumPay is null and NEW.minimumPay is not null)
      or (OLD.minimumPay is not null and NEW.minimumPay is null)) THEN
        select CONCAT(fieldNameList,'contract.minimumPay~') into fieldNameList;
        IF(OLD.minimumPay is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.minimumPay is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.minimumPay is not null) then
            select CONCAT(oldValueList,OLD.minimumPay,'~') into oldValueList;
        END IF;
        IF(NEW.minimumPay is not null) then
            select CONCAT(newValueList,NEW.minimumPay,'~') into newValueList;
        END IF;
    END IF;




IF ((OLD.packDisc <> NEW.packDisc) or (OLD.packDisc is null and NEW.packDisc is not null)
      or (OLD.packDisc is not null and NEW.packDisc is null)) THEN
        select CONCAT(fieldNameList,'contract.packDisc~') into fieldNameList;
        IF(OLD.packDisc is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.packDisc is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.packDisc is not null) then
            select CONCAT(oldValueList,OLD.packDisc,'~') into oldValueList;
        END IF;
        IF(NEW.packDisc is not null) then
            select CONCAT(newValueList,NEW.packDisc,'~') into newValueList;
        END IF;
    END IF;



    IF (OLD.section <> NEW.section ) THEN
      select CONCAT(fieldNameList,'contract.section~') into fieldNameList;
      select CONCAT(oldValueList,OLD.section,'~') into oldValueList;
      select CONCAT(newValueList,NEW.section,'~') into newValueList;
  END IF;


  IF (OLD.timeFrom <> NEW.timeFrom ) THEN
      select CONCAT(fieldNameList,'contract.timeFrom~') into fieldNameList;
      select CONCAT(oldValueList,OLD.timeFrom,'~') into oldValueList;
      select CONCAT(newValueList,NEW.timeFrom,'~') into newValueList;
  END IF;


  IF (OLD.timeTo <> NEW.timeTo ) THEN
      select CONCAT(fieldNameList,'contract.timeTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.timeTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.timeTo,'~') into newValueList;
  END IF;


  
IF ((OLD.trnsDisc <> NEW.trnsDisc) or (OLD.trnsDisc is null and NEW.trnsDisc is not null)
      or (OLD.trnsDisc is not null and NEW.trnsDisc is null)) THEN
        select CONCAT(fieldNameList,'contract.trnsDisc~') into fieldNameList;
        IF(OLD.trnsDisc is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.trnsDisc is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.trnsDisc is not null) then
            select CONCAT(oldValueList,OLD.trnsDisc,'~') into oldValueList;
        END IF;
        IF(NEW.trnsDisc is not null) then
            select CONCAT(newValueList,NEW.trnsDisc,'~') into newValueList;
        END IF;
    END IF;



    
  IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'contract.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;



   IF (OLD.domesticSalesCommission <> NEW.domesticSalesCommission ) THEN
       select CONCAT(fieldNameList,'contract.domesticSalesCommission~') into fieldNameList;
       IF(OLD.domesticSalesCommission = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.domesticSalesCommission = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.domesticSalesCommission = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.domesticSalesCommission = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



   IF (OLD.displayonAgentTariff <> NEW.displayonAgentTariff ) THEN
       select CONCAT(fieldNameList,'contract.displayonAgentTariff~') into fieldNameList;
       IF(OLD.displayonAgentTariff = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.displayonAgentTariff = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.displayonAgentTariff = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.displayonAgentTariff = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



   
  IF (OLD.companyDivision <> NEW.companyDivision ) THEN
      select CONCAT(fieldNameList,'contract.companyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
  END IF;


IF (OLD.internalCostContract <> NEW.internalCostContract ) THEN
       select CONCAT(fieldNameList,'contract.internalCostContract~') into fieldNameList;
       IF(OLD.internalCostContract = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.internalCostContract = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.internalCostContract = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.internalCostContract = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


    IF (OLD.payMethod <> NEW.payMethod ) THEN
      select CONCAT(fieldNameList,'contract.payMethod~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payMethod,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payMethod,'~') into newValueList;
  END IF;
  
  IF (OLD.storageBillingGroup <> NEW.storageBillingGroup ) THEN
      select CONCAT(fieldNameList,'contract.storageBillingGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageBillingGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageBillingGroup,'~') into newValueList;
  END IF;
  
  IF (OLD.storageEmail <> NEW.storageEmail ) THEN
      select CONCAT(fieldNameList,'contract.storageEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageEmail,'~') into newValueList;
  END IF;
  
  IF (OLD.emailPrintOption <> NEW.emailPrintOption ) THEN
      select CONCAT(fieldNameList,'contract.emailPrintOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailPrintOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailPrintOption,'~') into newValueList;
  END IF;
  
  IF (OLD.creditTerms <> NEW.creditTerms ) THEN
      select CONCAT(fieldNameList,'contract.creditTerms~') into fieldNameList;
      select CONCAT(oldValueList,OLD.creditTerms,'~') into oldValueList;
      select CONCAT(newValueList,NEW.creditTerms,'~') into newValueList;
  END IF;
  
  IF (OLD.customerFeedback <> NEW.customerFeedback ) THEN
      select CONCAT(fieldNameList,'contract.customerFeedback~') into fieldNameList;
      select CONCAT(oldValueList,OLD.customerFeedback,'~') into oldValueList;
      select CONCAT(newValueList,NEW.customerFeedback,'~') into newValueList;
  END IF;
  
  IF (OLD.ratingScale <> NEW.ratingScale ) THEN
      select CONCAT(fieldNameList,'contract.ratingScale~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ratingScale,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ratingScale,'~') into newValueList;
  END IF;
  
  IF (OLD.lumpPerUnit <> NEW.lumpPerUnit ) THEN
      select CONCAT(fieldNameList,'contract.lumpPerUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lumpPerUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lumpPerUnit,'~') into newValueList;
  END IF;
  
  IF (OLD.minReplacementvalue <> NEW.minReplacementvalue ) THEN
      select CONCAT(fieldNameList,'contract.minReplacementvalue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minReplacementvalue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minReplacementvalue,'~') into newValueList;
  END IF;
  
  IF (OLD.networkPartnerCode <> NEW.networkPartnerCode ) THEN
      select CONCAT(fieldNameList,'contract.networkPartnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerCode,'~') into newValueList;
  END IF;
  
  IF (OLD.networkPartnerName <> NEW.networkPartnerName ) THEN
      select CONCAT(fieldNameList,'contract.networkPartnerName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerName,'~') into newValueList;
  END IF;
  
  IF (OLD.source <> NEW.source ) THEN
      select CONCAT(fieldNameList,'contract.source~') into fieldNameList;
      select CONCAT(oldValueList,OLD.source,'~') into oldValueList;
      select CONCAT(newValueList,NEW.source,'~') into newValueList;
  END IF;
  
  IF (OLD.vendorCode <> NEW.vendorCode ) THEN
      select CONCAT(fieldNameList,'contract.vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
  END IF;
  
  IF (OLD.vendorName <> NEW.vendorName ) THEN
      select CONCAT(fieldNameList,'contract.vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorName,'~') into newValueList;
  END IF;
  
  IF (OLD.insuranceOption <> NEW.insuranceOption ) THEN
      select CONCAT(fieldNameList,'contract.insuranceOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceOption,'~') into newValueList;
  END IF;
  
  IF (OLD.defaultVat <> NEW.defaultVat ) THEN
      select CONCAT(fieldNameList,'contract.defaultVat~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultVat,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultVat,'~') into newValueList;
  END IF;
  
  IF (OLD.billToAuthorization <> NEW.billToAuthorization ) THEN
      select CONCAT(fieldNameList,'contract.billToAuthorization~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToAuthorization,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToAuthorization,'~') into newValueList;
  END IF;
  
  IF (OLD.billingMoment <> NEW.billingMoment ) THEN
      select CONCAT(fieldNameList,'contract.billingMoment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingMoment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingMoment,'~') into newValueList;
  END IF;
  
  IF (OLD.creditTermsCmmAgent <> NEW.creditTermsCmmAgent ) THEN
      select CONCAT(fieldNameList,'contract.creditTermsCmmAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.creditTermsCmmAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.creditTermsCmmAgent,'~') into newValueList;
  END IF;
  
 IF (OLD.contractType <> NEW.contractType ) THEN
      select CONCAT(fieldNameList,'contract.contractType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractType,'~') into newValueList;
  END IF;


 IF (OLD.owner <> NEW.owner ) THEN
      select CONCAT(fieldNameList,'contract.owner~') into fieldNameList;
      select CONCAT(oldValueList,OLD.owner,'~') into oldValueList;
      select CONCAT(newValueList,NEW.owner,'~') into newValueList;
  END IF;


 IF (OLD.contractCurrency <> NEW.contractCurrency ) THEN
      select CONCAT(fieldNameList,'contract.contractCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractCurrency,'~') into newValueList;
  END IF;



 IF (OLD.origContract <> NEW.origContract ) THEN
      select CONCAT(fieldNameList,'contract.origContract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.origContract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.origContract,'~') into newValueList;
  END IF;


 IF (OLD.published <> NEW.published ) THEN
      select CONCAT(fieldNameList,'contract.published~') into fieldNameList;
      select CONCAT(oldValueList,OLD.published,'~') into oldValueList;
      select CONCAT(newValueList,NEW.published,'~') into newValueList;
  END IF;



 IF (OLD.payableContractCurrency <> NEW.payableContractCurrency ) THEN
      select CONCAT(fieldNameList,'contract.payableContractCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableContractCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableContractCurrency,'~') into newValueList;
  END IF;




IF (OLD.dmmInsurancePolicy <> NEW.dmmInsurancePolicy ) THEN
       select CONCAT(fieldNameList,'contract.dmmInsurancePolicy~') into fieldNameList;
       IF(OLD.dmmInsurancePolicy = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.dmmInsurancePolicy = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.dmmInsurancePolicy = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.dmmInsurancePolicy = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   
   
   
    IF (OLD.recClearing <> NEW.recClearing ) THEN
      select CONCAT(fieldNameList,'contract.recClearing~') into fieldNameList;
      select CONCAT(oldValueList,OLD.recClearing,'~') into oldValueList;
      select CONCAT(newValueList,NEW.recClearing,'~') into newValueList;
  END IF;

  
  
  IF (OLD.payClearing <> NEW.payClearing ) THEN
      select CONCAT(fieldNameList,'contract.payClearing~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payClearing,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payClearing,'~') into newValueList;
  END IF;
  

   IF (OLD.fXRateOnActualizationDate <> NEW.fXRateOnActualizationDate ) THEN
       select CONCAT(fieldNameList,'contract.fXRateOnActualizationDate~') into fieldNameList;
       IF(OLD.fXRateOnActualizationDate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.fXRateOnActualizationDate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.fXRateOnActualizationDate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.fXRateOnActualizationDate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
IF (OLD.noTransfereeEvaluation <> NEW.noTransfereeEvaluation ) THEN
       select CONCAT(fieldNameList,'contract.noTransfereeEvaluation~') into fieldNameList;
       IF(OLD.noTransfereeEvaluation = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noTransfereeEvaluation = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noTransfereeEvaluation = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noTransfereeEvaluation = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
   
   IF (OLD.lumpSum <> NEW.lumpSum ) THEN
       select CONCAT(fieldNameList,'contract.lumpSum~') into fieldNameList;
       IF(OLD.lumpSum = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.lumpSum = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.lumpSum = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.lumpSum = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
   IF (OLD.detailedList <> NEW.detailedList ) THEN
       select CONCAT(fieldNameList,'contract.detailedList~') into fieldNameList;
       IF(OLD.detailedList = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.detailedList = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.detailedList = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.detailedList = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
   IF (OLD.noInsurance <> NEW.noInsurance ) THEN
       select CONCAT(fieldNameList,'contract.noInsurance~') into fieldNameList;
       IF(OLD.noInsurance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noInsurance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noInsurance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noInsurance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
  
  
   
   
 CALL add_tblHistory (OLD.id,"contract", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$

delimiter;