package com.trilasoft.app.service.impl;


import java.util.List;
import java.util.Set;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RefJobDocumentTypeDao;
import com.trilasoft.app.model.RefJobDocumentType;
import com.trilasoft.app.service.RefJobDocumentTypeManager;


public class RefJobDocumentTypeManagerImpl extends GenericManagerImpl<RefJobDocumentType, Long> implements RefJobDocumentTypeManager {   
	RefJobDocumentTypeDao refJobDocumentTypeDao;   
  
    public RefJobDocumentTypeManagerImpl(RefJobDocumentTypeDao refJobDocumentTypeDao) {   
        super(refJobDocumentTypeDao);   
        this.refJobDocumentTypeDao = refJobDocumentTypeDao;   
    }
	public List findRefJobDocumentList(String corpID)
	{
		return refJobDocumentTypeDao.findRefJobDocumentList(corpID);
	}
	public List searchRefJobDocument(String jobType,String corpID)
	{
		return refJobDocumentTypeDao.searchRefJobDocument(jobType, corpID);
	}
	 public String getAllDoument(String jobType,String corpID)
	 {
		 return refJobDocumentTypeDao.getAllDoument(jobType, corpID);
	 }
}