<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<style type="text/css"> 
	.input-textarea{border:1px solid #219DD1;color:#000000;	font-family:arial,verdana;	font-size:12px;	height:45px;text-decoration:none;	}
	.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;
		font-weight: bold;	color: #007a8d; padding-left: 40px; }	
	div#bar{	z-index:999;background-color:#fff;	}
</style>

</head>

<div id="Layer1" style="width:99%; margin:0px auto">
	
		<display:table name="workList" class="table" requestURI="" id="workList"  pagesize="14">
		   <c:choose>
		    	<c:when test="${workList.day == 'Sunday' || workList.day == 'Saturday' || workList.holiday == '1' }">
		    		<display:column property="date" title="Date" sortable="true" style="color: #fff; background-color: #959695" format="{0,date,dd-MMM-yyyy}"/>
		   			<display:column property="day" sortable="true"  title="Day" style="width:30px; color: #fff; background-color: #959695"/>
		    		<display:column property="status" sortable="true" title="Status" style="width:20px;color: #fff; background-color: #959695"/>
		    		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
		    			<display:column property="availableCrew" sortable="true"  title="Available Crew" style="width:100px; color: #fff; background-color: #959695;text-align:right"/>
		    			<c:if test="${workList.crewUsed > workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;background:#FF3232;text-align:right"/>
		    			</c:if>
		    			<c:if test="${workList.crewUsed < workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;color: #fff; background-color: #959695;text-align:right"/>
		    			</c:if>
		    		</c:if>
		    		<display:column property="comment" sortable="true" maxLength="100" title="Comments" style="width:400px;color: #fff; background-color: #959695"/>
		    	</c:when>
		    	<c:otherwise>
		    		<display:column property="date" title="Date" sortable="true" style="width:70px" format="{0,date,dd-MMM-yyyy}"/>
		    		<display:column property="day" sortable="true"  title="Day" style="width:30px"/>
		    		<display:column property="status" sortable="true" title="Status" style="width:20px"/>	
		    		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
		    			<display:column property="availableCrew" sortable="true"  title="Available Crew" style="width:100px;text-align:right"/>
		    			<c:if test="${workList.crewUsed > workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;background:#FF3232;text-align:right"/>
		    			</c:if>
		    			<c:if test="${workList.crewUsed < workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;text-align:right"/>
		    			</c:if>
		    		</c:if>
		    		<display:column property="comment" sortable="true" maxLength="100" title="Comments" style="width:400px"/>
   		    	</c:otherwise>
		   </c:choose>
		    
		</display:table>
	</div>
	
	<script type="text/javascript">
try{
	window.print();
}catch(e){}
</script>