package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="brokerdetails")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class BrokerDetails extends BaseObject {
	
	private String corpID;
	private Long id;
	private Long serviceOrderId;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn; 
	private String vendorCode;
	private String vendorName;
	private Date entryDate;
	private Date lastChangeDate;
	private BigDecimal quoted= new BigDecimal(0);
	private String quotedRef;
	private Date docsOutDate;
	private BigDecimal dutie= new BigDecimal(0);
	private Date customER;
	private Boolean cf3299;
	private Boolean supplement;
	private Boolean powerOfAttorney;
	private Boolean passPort;
	private Boolean visa;
	private Boolean doc194;
	private String isfBroker;
	private String isfReference;
	private Date isfDocsReceived;
	private Date isfSentBroker;
	private Date receivedBroker;
	private Date matchRecord;


	@Override
	public String toString() {
		return new ToStringBuilder(this)
		.append("corpID", corpID)
		.append("id",id)
		.append("serviceOrderId", serviceOrderId)
		.append("sequenceNumber", sequenceNumber)
		.append("ship", ship)
		.append("shipNumber", shipNumber)
		.append("createdBy",createdBy)
		.append("updatedBy", updatedBy)
		.append("createdOn", createdOn)
		.append("updatedOn", updatedOn)
		.append("vendorCode", vendorCode)
		.append("vendorName", vendorName)
		.append("entryDate", entryDate)
		.append("lastChangeDate", lastChangeDate)
		.append("quoted", quoted)
		.append("quotedRef", quotedRef)
		.append("docsOutDate", docsOutDate)
		.append("dutie", dutie)
		.append("customER", customER)
		.append("cf3299", cf3299)
		.append("supplement", supplement)
		.append("powerOfAttorney", powerOfAttorney)
		.append("passPort", passPort)
		.append("visa", visa)
		.append("doc194", doc194)
		.append("isfBroker", isfBroker)
		.append("isfReference", isfReference)
		.append("isfDocsReceived", isfDocsReceived)
		.append("isfSentBroker", isfSentBroker)
		.append("receivedBroker", receivedBroker)
		.append("matchRecord", matchRecord)
		.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof BrokerDetails))
			return false;
		BrokerDetails castOther = (BrokerDetails)other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id,castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship,castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther. updatedBy)
				.append(createdOn, castOther. createdOn)
				.append(updatedOn, castOther. updatedOn)
				.append(vendorCode, castOther. vendorCode)
				.append(vendorName, castOther. vendorName)
				.append(entryDate, castOther. entryDate)
				.append(lastChangeDate, castOther.lastChangeDate)
				.append(quoted, castOther.quoted)
				.append(quotedRef, castOther.quotedRef)
				.append(docsOutDate, castOther. docsOutDate)
				.append(dutie, castOther. dutie)
				.append(customER, castOther. customER)
				.append(cf3299, castOther. cf3299)
				.append(supplement, castOther. supplement)
				.append(powerOfAttorney, castOther. powerOfAttorney)
				.append(passPort, castOther. passPort)
				.append(visa, castOther. visa)
				.append(doc194, castOther. doc194)
				.append(isfBroker, castOther. isfBroker)
				.append(isfReference, castOther. isfReference)
				.append(isfDocsReceived, castOther. isfDocsReceived)
				.append(isfSentBroker, castOther. isfSentBroker)
				.append(receivedBroker, castOther. receivedBroker)
				.append(matchRecord, castOther. matchRecord).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
		.append(corpID)
		.append(id)
		.append(serviceOrderId)
		.append(sequenceNumber)
		.append(ship)
		.append(shipNumber)
		.append(createdBy)
		.append(updatedBy)
		.append(createdOn)
		.append(updatedOn)
		.append(vendorCode)
		.append(vendorName)
		.append(entryDate)
		.append(lastChangeDate)
		.append(quoted)
		.append(quotedRef)
		.append(docsOutDate)
		.append(dutie)
		.append(customER)
		.append(cf3299)
		.append(supplement)
		.append(powerOfAttorney)
		.append(passPort)
		.append(visa)
		.append(doc194)
		.append(isfBroker)
		.append(isfReference)
		.append(isfDocsReceived)
		.append(isfSentBroker)
		.append(receivedBroker)
		.append(matchRecord)
		.toHashCode();
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column
	public String getShip() {
		return ship;
	}

	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Date getLastChangeDate() {
		return lastChangeDate;
	}

	public void setLastChangeDate(Date lastChangeDate) {
		this.lastChangeDate = lastChangeDate;
	}

	public String getQuotedRef() {
		return quotedRef;
	}

	public void setQuotedRef(String quotedRef) {
		this.quotedRef = quotedRef;
	}

	public Date getDocsOutDate() {
		return docsOutDate;
	}

	public void setDocsOutDate(Date docsOutDate) {
		this.docsOutDate = docsOutDate;
	}

	public Date getCustomER() {
		return customER;
	}

	public void setCustomER(Date customER) {
		this.customER = customER;
	}

	public Boolean getCf3299() {
		return cf3299;
	}

	public void setCf3299(Boolean cf3299) {
		this.cf3299 = cf3299;
	}

	public Boolean getSupplement() {
		return supplement;
	}

	public void setSupplement(Boolean supplement) {
		this.supplement = supplement;
	}

	public Boolean getPowerOfAttorney() {
		return powerOfAttorney;
	}

	public void setPowerOfAttorney(Boolean powerOfAttorney) {
		this.powerOfAttorney = powerOfAttorney;
	}

	public Boolean getPassPort() {
		return passPort;
	}

	public void setPassPort(Boolean passPort) {
		this.passPort = passPort;
	}

	public Boolean getVisa() {
		return visa;
	}

	public void setVisa(Boolean visa) {
		this.visa = visa;
	}

	public Boolean getDoc194() {
		return doc194;
	}

	public void setDoc194(Boolean doc194) {
		this.doc194 = doc194;
	}

	public String getIsfBroker() {
		return isfBroker;
	}

	public void setIsfBroker(String isfBroker) {
		this.isfBroker = isfBroker;
	}

	public String getIsfReference() {
		return isfReference;
	}

	public void setIsfReference(String isfReference) {
		this.isfReference = isfReference;
	}

	public Date getIsfDocsReceived() {
		return isfDocsReceived;
	}

	public void setIsfDocsReceived(Date isfDocsReceived) {
		this.isfDocsReceived = isfDocsReceived;
	}

	public Date getIsfSentBroker() {
		return isfSentBroker;
	}

	public void setIsfSentBroker(Date isfSentBroker) {
		this.isfSentBroker = isfSentBroker;
	}

	public Date getReceivedBroker() {
		return receivedBroker;
	}

	public void setReceivedBroker(Date receivedBroker) {
		this.receivedBroker = receivedBroker;
	}

	public Date getMatchRecord() {
		return matchRecord;
	}

	public void setMatchRecord(Date matchRecord) {
		this.matchRecord = matchRecord;
	}

	public BigDecimal getQuoted() {
		return quoted;
	}

	public void setQuoted(BigDecimal quoted) {
		this.quoted = quoted;
	}

	public BigDecimal getDutie() {
		return dutie;
	}

	public void setDutie(BigDecimal dutie) {
		this.dutie = dutie;
	}

}
