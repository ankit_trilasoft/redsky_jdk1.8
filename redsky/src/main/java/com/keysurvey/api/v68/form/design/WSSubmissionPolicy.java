/**
 * WSSubmissionPolicy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSSubmissionPolicy  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private boolean backButton;

    private int duplicateAction;

    private java.util.Calendar endDate;

    private long formId;

    private long maxRespondents;

    private java.util.Calendar startDate;

    private int timeLimitToAnswer;

    public WSSubmissionPolicy() {
    }

    public WSSubmissionPolicy(
           long versionId,
           boolean backButton,
           int duplicateAction,
           java.util.Calendar endDate,
           long formId,
           long maxRespondents,
           java.util.Calendar startDate,
           int timeLimitToAnswer) {
        super(
            versionId);
        this.backButton = backButton;
        this.duplicateAction = duplicateAction;
        this.endDate = endDate;
        this.formId = formId;
        this.maxRespondents = maxRespondents;
        this.startDate = startDate;
        this.timeLimitToAnswer = timeLimitToAnswer;
    }


    /**
     * Gets the backButton value for this WSSubmissionPolicy.
     * 
     * @return backButton
     */
    public boolean isBackButton() {
        return backButton;
    }


    /**
     * Sets the backButton value for this WSSubmissionPolicy.
     * 
     * @param backButton
     */
    public void setBackButton(boolean backButton) {
        this.backButton = backButton;
    }


    /**
     * Gets the duplicateAction value for this WSSubmissionPolicy.
     * 
     * @return duplicateAction
     */
    public int getDuplicateAction() {
        return duplicateAction;
    }


    /**
     * Sets the duplicateAction value for this WSSubmissionPolicy.
     * 
     * @param duplicateAction
     */
    public void setDuplicateAction(int duplicateAction) {
        this.duplicateAction = duplicateAction;
    }


    /**
     * Gets the endDate value for this WSSubmissionPolicy.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this WSSubmissionPolicy.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the formId value for this WSSubmissionPolicy.
     * 
     * @return formId
     */
    public long getFormId() {
        return formId;
    }


    /**
     * Sets the formId value for this WSSubmissionPolicy.
     * 
     * @param formId
     */
    public void setFormId(long formId) {
        this.formId = formId;
    }


    /**
     * Gets the maxRespondents value for this WSSubmissionPolicy.
     * 
     * @return maxRespondents
     */
    public long getMaxRespondents() {
        return maxRespondents;
    }


    /**
     * Sets the maxRespondents value for this WSSubmissionPolicy.
     * 
     * @param maxRespondents
     */
    public void setMaxRespondents(long maxRespondents) {
        this.maxRespondents = maxRespondents;
    }


    /**
     * Gets the startDate value for this WSSubmissionPolicy.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this WSSubmissionPolicy.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the timeLimitToAnswer value for this WSSubmissionPolicy.
     * 
     * @return timeLimitToAnswer
     */
    public int getTimeLimitToAnswer() {
        return timeLimitToAnswer;
    }


    /**
     * Sets the timeLimitToAnswer value for this WSSubmissionPolicy.
     * 
     * @param timeLimitToAnswer
     */
    public void setTimeLimitToAnswer(int timeLimitToAnswer) {
        this.timeLimitToAnswer = timeLimitToAnswer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSSubmissionPolicy)) return false;
        WSSubmissionPolicy other = (WSSubmissionPolicy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.backButton == other.isBackButton() &&
            this.duplicateAction == other.getDuplicateAction() &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            this.formId == other.getFormId() &&
            this.maxRespondents == other.getMaxRespondents() &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            this.timeLimitToAnswer == other.getTimeLimitToAnswer();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += (isBackButton() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getDuplicateAction();
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        _hashCode += new Long(getFormId()).hashCode();
        _hashCode += new Long(getMaxRespondents()).hashCode();
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        _hashCode += getTimeLimitToAnswer();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSSubmissionPolicy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSubmissionPolicy"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("backButton");
        elemField.setXmlName(new javax.xml.namespace.QName("", "backButton"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duplicateAction");
        elemField.setXmlName(new javax.xml.namespace.QName("", "duplicateAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxRespondents");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maxRespondents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeLimitToAnswer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timeLimitToAnswer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
