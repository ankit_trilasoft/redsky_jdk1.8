package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CorpComponentPermissionDao;
import com.trilasoft.app.model.CorpComponentPermission;
import com.trilasoft.app.service.CorpComponentPermissionManager;

public class CorpComponentPermissionManagerImpl extends GenericManagerImpl<CorpComponentPermission, Long> implements CorpComponentPermissionManager{
	
	CorpComponentPermissionDao corpComponentPermissionDao;
	

    public CorpComponentPermissionManagerImpl(CorpComponentPermissionDao corpComponentPermissionDao) { 
        super(corpComponentPermissionDao); 
        this.corpComponentPermissionDao = corpComponentPermissionDao; 
    }
    
	public boolean getFieldVisibilityForComponent(String userCorpID, String componentId) {
		return corpComponentPermissionDao.getFieldVisibilityForComponent(userCorpID, componentId);
	}

	public List findMaximumId() {
		return corpComponentPermissionDao.findMaximumId();
	}

	public List searchcompanyPermission(String componentId, String description,String corpId) {
		return corpComponentPermissionDao.searchcompanyPermission(componentId, description,corpId);
	}
	public List findPermissionsByCorpId(String corpId){
		return corpComponentPermissionDao.findPermissionsByCorpId(corpId);
	}
	public List findCorpID(){
		return corpComponentPermissionDao.findCorpID();
	}

	public Map<String, Integer> getComponentVisibilityAttrbuteDetail(String userCorpID, String componentId) {
		return corpComponentPermissionDao.getComponentVisibilityAttrbuteDetail(userCorpID, componentId);
	}

	public void updateCurrentFieldVisibilityMap() {
		corpComponentPermissionDao.updateCurrentFieldVisibilityMap();
		
	}
}
