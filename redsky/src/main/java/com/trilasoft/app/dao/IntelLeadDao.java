package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.IntelLead;


public interface IntelLeadDao extends GenericDao<IntelLead, Long>  {
	
	public List<IntelLead> getIntelLeadLists(String corpId);
	public void updateActionValue(Long id, String sessionCorpID, String shipnumber, String seqNum);

}
