package com.trilasoft.app.service.impl;


import java.util.List;
import java.util.Set;


import java.util.List;


import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AuthorizationNoDao;
import com.trilasoft.app.model.AuthorizationNo;
import com.trilasoft.app.service.AuthorizationNoManager;

public class AuthorizationNoManagerImpl extends GenericManagerImpl<AuthorizationNo, Long> implements AuthorizationNoManager { 
	AuthorizationNoDao authorizationNoDao; 
    public AuthorizationNoManagerImpl(AuthorizationNoDao authorizationNoDao) { 
        super(authorizationNoDao); 
        this.authorizationNoDao = authorizationNoDao; 
    }
        

	public List searchAccAuthorization(String shipNumber) {
		
		return authorizationNoDao.searchAccAuthorization(shipNumber);
	} 
    
    
     public List findByJobAuth(String authNo,  String shipNumber){
    	return authorizationNoDao.findByJobAuth(authNo, shipNumber);   
    }
     public List findMaximum(String shipNumber)  {
     	return authorizationNoDao.findMaximum(shipNumber);
     }


	public List searchAuthorizationNo(String shipNumber) {
		return authorizationNoDao.searchAuthorizationNo(shipNumber);
	}


	public List findAuthorizationNo(String authNoInvoice) {
		
		return authorizationNoDao.findAuthorizationNo(authNoInvoice);
	}


	public List findAuthNumber(String shipNumber, String sessionCorpID) {
		return authorizationNoDao.findAuthNumber(shipNumber, sessionCorpID);
	}
}
