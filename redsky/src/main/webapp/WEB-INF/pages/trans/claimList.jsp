<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="claimList.title"/></title>   
    <meta name="heading" content="<fmt:message key='claimList.heading'/>"/> 
 

<style type="text/css"> 


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style> 
<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:0px;
!margin-bottom:1px;
margin-top:-18px;
padding:2px 0px;
!padding-left:50px;
text-align:right;
width:100%;
!width:94%;
}

div.error, span.error, li.error, div.message {
background:#BCD2EF none repeat scroll 0%;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:bold;
margin:10px auto;
padding:3px;
text-align:center;
vertical-align:bottom;
width:450px;
}

.table tbody td a {
background:transparent none repeat scroll 0% 50%;
cursor:pointer;
text-decoration:underline;
}

form {
margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}

/*jitendra ss*/
.table th.containeralign a {text-align:left;}
/*end jitendra*/
</style>

</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="claimForm" action="searchClaim" method="post" validate="true">

<s:hidden name="claimSortOrder"   value="${claimSortOrder}"/>
<c:set var="claimSortOrder" value="${claimSortOrder}"/>
<s:hidden name="orderForClaim"   value="${orderForClaim}"/>
<c:set var="orderForClaim"  value="${orderForClaim}"/>
 <c:set var="button1">
 <c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
     <sec-auth:authComponent componentId="module.tab.claims.serviceorderTab">
     <input type="button" id="addButton" class="cssbutton" style="width:60px; height:28px" onclick="location.href='<c:url value="/editClaim.html?sid=${ServiceOrderID}"/>'"  value="<fmt:message key="button.add"/>"/>  
	</sec-auth:authComponent>
</c:set>
<s:hidden name="id" />
<s:hidden name="claim.id" value="%{claim.id}"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.registrationNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="serviceOrder.id"/>
<s:hidden name="customerFile.id"/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkPropertyAmountComponent" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="checkPropertyAmountComponent" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkMsgClickedValue" value="<%=request.getParameter("msgClicked") %>" />
<sec-auth:authComponent componentId="module.script.form.corpAccountScript"> 	
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
</sec-auth:authComponent>
	<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer3" style="width:100%;">
<div id="newmnav" style="float: left;">
		  
		  <c:if test="${not empty serviceOrder.id}">
		    <ul>
		    
		

        
	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	                    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	                      <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
					<li><a href="redskyDashboard.html?sid=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}" /><span>Dashboard</span></a></li>
				</c:if>
				</sec-auth:authComponent>
				</configByCorp:fieldVisibility>
				
		     <sec-auth:authComponent componentId="module.tab.claims.serviceorderTab">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>S/O Details</span></a></li>
				</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.billingTab">
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editBilling.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Billing</span></a></li>
				</c:if>
			  </sec-auth:authComponent>
			  </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.claims.accountingTab">
			 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			 <c:choose>
			   <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			   </c:when> --%>
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			   <c:otherwise> 
		          <c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="accountLineList.html?sid=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Accounting</span></a></li>
				</c:if>
		       </c:otherwise>
		      </c:choose>
		      </c:if>  
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			 <c:choose> 
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			   <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		       </c:otherwise>
		      </c:choose>
		      </c:if>
		      </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
   	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="operationResource.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>O&I</span></a></li>
				</c:if>
	         </sec-auth:authComponent>
	         </c:if>
		        <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
		        <sec-auth:authComponent componentId="module.tab.claims.forwardingTab">
		        <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
		      <%--   <c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}"> --%>
		        <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		        <c:if test="${userType!='ACCOUNT'}">
		         <c:if test="${forwardingTabVal!='Y'}"> 
			  		<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  	</c:if>
			  	<c:if test="${forwardingTabVal=='Y'}">
	  				<c:if test="${checkPropertyAmountComponent!='Y'}">
					   	<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="containersAjaxList.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Forwarding</span></a></li>
					</c:if>
	  			</c:if>
			  </c:if>
			  <c:if test="${userType=='ACCOUNT' && serviceOrder.job !='RLO'}">
              <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
             </c:if>
             </c:if>
             </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.domesticTab">
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Domestic</span></a></li>
				</c:if>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
              <c:if test="${serviceOrder.job =='INT'}">
               <c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Domestic</span></a></li>
				</c:if>
              </c:if>
              </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.statusTab">
			   	<c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
				</c:if>
				<c:if test="${serviceOrder.job !='RLO'}"> 
					<c:if test="${checkPropertyAmountComponent!='Y'}">
					   	<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="editTrackingStatus.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Status</span></a></li>
					</c:if>
				</c:if>	
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.summaryTab">
			  	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
			  	</sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.claims.ticketTab">
			   <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  		<c:if test="${checkPropertyAmountComponent!='Y'}">
					   	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="customerWorkTickets.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Ticket</span></a></li>
					</c:if>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Claims</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			       <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			    <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  					<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}"><span>Critical Dates</span></a></li>
				</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.claims.reportTab">
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Claims&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
           	  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		  </configByCorp:fieldVisibility>
	 		  <c:if test="${userType=='USER'}">
		 		  <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  				<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  			  </configByCorp:fieldVisibility>
  			  </c:if>
             </ul>
			</c:if>
			</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">		
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>	
		</c:if></tr></table>		
		<c:if test="${not empty serviceOrder.id }">
		<div class="spn">&nbsp;</div>
		</c:if>
</div>
<div id="Layer1" style="width:100%">

<c:if test="${not empty serviceOrder.id}">
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
</c:if>
</tbody>
</table>


<c:if test="${empty serviceOrder.id }">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-5px;"><span></span></div>
<div class="center-content">
<table class="table" width="100%" >
<thead>
<tr>

<th><fmt:message key="claim.claimNumber"/></th>
  <sec-auth:authComponent componentId="component.claim.unigroupclaim.edit">
<th>Unigroup&nbsp;Claim&nbsp;Number</th>
</sec-auth:authComponent>
<th><fmt:message key="claim.lastName"/></th>
<th><fmt:message key="claim.shipNumber"/></th>
<th>Reg#</th>
<th>Close Date</th>
<th style="width:270px;">Claim Handler</th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			 	<s:textfield name="claim.claimNumber" required="true" cssClass="input-text" size="10" onkeypress="return checkIt(event)" onfocus="onFormLoad();" 	cssStyle="width:100px;" />
			 </td>
			   <sec-auth:authComponent componentId="component.claim.unigroupclaim.edit">
			 <td>
			    <s:textfield name="claim.clmsClaimNbr" onkeypress="return checkIt(event)" size="20" required="true" cssClass="input-text" cssStyle="width:120px;"/>
			</td>
			</sec-auth:authComponent>
			<td>
			    <s:textfield name="claim.lastName" size="20" required="true" cssClass="input-text" cssStyle="width:100px;"/>
			</td>
			
			<td>
			    <s:textfield name="claim.shipNumber" size="20" required="true" cssClass="input-text" cssStyle="width:100px;"/>
			</td>
			<td>
			    <s:textfield name="claim.registrationNumber" size="15" required="true" cssClass="input-text" cssStyle="width:100px;" />
			</td>
		    <c:if test="${not empty claim.closeDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.closeDate" /></s:text>
				<td><s:textfield cssClass="input-text" id="date1" name="claim.closeDate" value="%{customerFiledate1FormattedValue}" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true" cssStyle="width:100px;"/><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty claim.closeDate}">
				<td><s:textfield cssClass="input-text" id="date1" name="claim.closeDate" required="true" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td style="border-right:2px solid #74B3DC; ">
				<s:select name="claim.claimPerson" cssClass="list-menu" list="%{claimPersonList}"  cssStyle="width:105px" headerKey="" headerValue="" onchange="changeStatus();" />
			</td>
			</tr>
			<tr>
			<td colspan="5"></td>
			 <sec-auth:authComponent componentId="component.claim.unigroupclaim.edit">
             <td></td>
             </sec-auth:authComponent>
			<td width="200px" colspan="2" style="border-left:hidden;text-align:right1;">
			    <s:submit cssClass="cssbutton" cssStyle="width:57px; height:25px;" method="search" key="button.search" onclick="return isNumeric();"/>
			   <input type="button" class="cssbutton" value="Clear" style="width:57px; height:25px;" onclick="clear_fields();"/>   
			</td>
		</tr>
		</tbody>
	</table>
	<div style="!margin-top:7px;"></div>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>

<c:if test="${not empty claim.id}">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<div id="otabs" style="margin-top:-8px;">
		  <ul>
		    <li><a class="current"><span>Claim List</span></a></li>
		  </ul>
		</div>
				<div class="spnblk">&nbsp;</div>
</tbody></table>
</c:if>

<c:if test="${empty claim.id}">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<div id="otabs" style="margin-top:-8px;">
		  <ul>
		    <li><a class="current"><span>Claims</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		</tbody>
</table>
<c:out value="${searchresults}" escapeXml="false" /> 
<s:set name="claimsExt" value="claims" scope="request"/> 
<c:if test="${empty serviceOrder.id}">  
<display:table name="claimsExt" class="table" requestURI="" id="claimList" export="true" defaultsort="${claimSortOrder}" defaultorder="${orderForClaim}" pagesize="10" style="width:99%;margin-top:0px;margin-left:5px; ">   
  
     <display:column property="claimNumber" headerClass="containeralign" style="text-align: right" sortable="true" titleKey="claim.claimNumber" href="editClaim.html" paramId="id" paramProperty="id"/> 
    <c:if test="${empty serviceOrder.id}">
     <sec-auth:authComponent componentId="component.claim.unigroupclaim.edit">
     <display:column property="clmsClaimNbr" sortable="true" title="Unigroup&nbsp;Claim&nbsp;Number" /> 
     </sec-auth:authComponent>
    <display:column property="lastName" sortable="true" titleKey="claim.lastName" />      
    <display:column property="firstName" sortable="true" titleKey="claim.firstName" />
    <display:column property="shipNumber" sortable="true" title="S/O#" /> 
    <display:column property="registrationNumber" sortable="true" title="Reg#" /> 
    <display:column sortable="true" titleKey="claim.claimPerson" >
         <c:forEach var="entry" items="${claimPersonList}">
           <c:if test="${claimList.claimPerson == entry.key}">
             <c:out value="${entry.value}" />
           </c:if>
         </c:forEach>
    </display:column>
    </c:if>
    <display:column property="createdOn" sortable="true" titleKey="claim.createdOn" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="formSent" sortable="true" titleKey="claim.formSent" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="formRecived" sortable="true" titleKey="claim.formRecived" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="cancelled" sortable="true" titleKey="claim.cancelled" format="{0,date,dd-MMM-yyyy}"/> 
  	<display:column property="closeDate" sortable="true" title="Close Date" format="{0,date,dd-MMM-yyyy}"/>  
   <display:column headerClass="containeralign" style="text-align: right" property="reciveReimbursement" sortable="true" titleKey="claim.reciveReimbursement"/>  
     <display:setProperty name="paging.banner.items_name" value="claims"/>   
  
    <display:setProperty name="export.excel.filename" value="Claim List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Claim List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Claim List.pdf"/> 
</display:table>   
</c:if>

</c:if>
<c:if test="${not empty serviceOrder.id}">  
<display:table name="claims" class="table" requestURI="" id="claimList" export="true" defaultsort="1" style="width:100%">   
    <display:column property="claimNumber" sortable="true" titleKey="claim.claimNumber" href="editClaim.html" paramId="id" paramProperty="id"/>   
   <c:if test="${empty serviceOrder.id}">
    <sec-auth:authComponent componentId="component.claim.unigroupclaim.edit">
     <display:column property="clmsClaimNbr" sortable="true" title="Unigroup&nbsp;Claim&nbsp;Number" /> 
     </sec-auth:authComponent>
    <display:column property="lastName" sortable="true" titleKey="claim.lastName" />   
    <display:column property="firstName" sortable="true" titleKey="claim.firstName" />
    <display:column property="shipNumber" sortable="true" titleKey="claim.shipNumber" /> 
     <display:column property="registrationNumber" sortable="true" title="Reg#" />
     <display:column sortable="true" titleKey="claim.claimPerson" >
     <c:forEach var="entry" items="${claimPersonList}">
             <c:if test="${claimList.claimPerson == entry.key}">
             <c:out value="${entry.value}" />
             </c:if>
       </c:forEach>
    </display:column>
    </c:if>
    <display:column property="createdOn" sortable="true" titleKey="claim.createdOn" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="formSent" sortable="true" titleKey="claim.formSent" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="formRecived" sortable="true" titleKey="claim.formRecived" format="{0,date,dd-MMM-yyyy}"/>
     <display:column property="cancelled" sortable="true" titleKey="claim.cancelled" format="{0,date,dd-MMM-yyyy}"/> 
   <display:column property="reciveReimbursement" sortable="true" titleKey="claim.reciveReimbursement"/>  
    <display:setProperty name="paging.banner.item_name" value="claim"/>   
    <display:setProperty name="paging.banner.items_name" value="claims"/>   
      <display:setProperty name="export.excel.filename" value="Claim List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Claim List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Claim List.pdf"/> 
  </display:table>   
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD" && serviceOrder.status != "TCNL"}'>
<c:out value="${button1}" escapeXml="false" /> 
</c:if>
<c:if test='${serviceOrder.status == "CNCL" || serviceOrder.status == "TCNL"}'>
<input type="button" class="cssbuttonA" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before add claim.')" value="Add" />
 </c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<input type="button" class="cssbuttonA" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before add claim.')" value="Add" />
</c:if> 
</c:if>
</div> 

</s:form>   
    
<script type="text/javascript">   
try{
	<c:if test="${defaultVal == 'true'}">  
		location.href = 'editClaim.html?id='+${claimList.id};
	</c:if> 
}catch(e){}
try{
document.forms['claimForm'].elements['claim.claimNumber'].focus();
}
catch(e){}
try{
<c:if test="${detailPage == true}" >
    <c:redirect url="/editClaim.html?id=${claimList.id}"  />
</c:if>  
}
catch(e){}

</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript"> 
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
   
<script language="javascript" type="text/javascript">
<sec-auth:authComponent componentId="module.script.form.agentScript">
 	window.onload = function() { 
		trap();
		document.forms['claimForm'].elements['addButton'].style.display='none';
	}
</sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
 	window.onload = function() { 
		//trap();
		document.forms['claimForm'].elements['addButton'].style.display='none';
	}
</sec-auth:authComponent>
function trap(){
	if(document.images){
		  for(i=0;i<document.images.length;i++){
		      if(document.images[i].src.indexOf('nav')>0){
					document.images[i].onclick= right; 
		        	document.images[i].src = 'images/navarrow.gif';  
			  }
		  }
	 }
}
		  
function right(e) {
		//var msg = "Sorry, you don't have permission.";
	if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
	}
	if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
	}
	else return true;
}

function onlyNumsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
}
function onlyFloatNumsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
}
	
function onlyCharsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
}
function notExists(){
	alert("The Claim Item is not been saved yet");
}
function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    //alert(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
}
function isNumeric()
{   var i;
    var s = document.forms['claimForm'].elements['claim.claimNumber'].value;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid Claim Number");
        document.forms['claimForm'].elements['claim.claimNumber'].select();
        
        return false;
        }
    }
    return true;
}
	
function clear_fields(){
	document.forms['claimForm'].elements['claim.claimNumber'].value = "";
	document.forms['claimForm'].elements['claim.lastName'].value = "";
	document.forms['claimForm'].elements['claim.shipNumber'].value = "";
	document.forms['claimForm'].elements['claim.closeDate'].value = "";
	document.forms['claimForm'].elements['claim.registrationNumber'].value = "";
	document.forms['claimForm'].elements['claim.claimPerson'].value = "";
	document.forms['claimForm'].elements['claim.clmsClaimNbr'].value = "";
}
</SCRIPT>
<script>
function goToClaimDetail(targetValue){
   document.forms['claimForm'].elements['id'].value = targetValue;
   document.forms['claimForm'].action = 'editClaim.html?from=list';
   document.forms['claimForm'].submit();
}

function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['claimForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['claimForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
}
   
function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['claimForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['claimForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
}
   
function handleHttpResponseOtherShip(){
    if (http5.readyState == 4)
    {
      var results = http5.responseText
      results = results.trim();
      location.href = 'claims.html?id='+results;
    }
}     
function findCustomerOtherSO(position) {
	 var sid=document.forms['claimForm'].elements['customerFile.id'].value;
	 var soIdNum=document.forms['claimForm'].elements['serviceOrder.id'].value;
	 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  	ajax_showTooltip(url,position);	
} 
function goToUrl(id)
{
	location.href = "claims.html?id="+id;
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http5 = getHTTPObject();
</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>