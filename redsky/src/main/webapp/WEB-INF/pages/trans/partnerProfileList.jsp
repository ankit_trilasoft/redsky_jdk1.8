<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.appfuse.model.Role"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_AGENT_ADMIN")){
		userRole=role.getName();
		
		break;
	}
	
}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<!--<meta name="gmapkey" content="ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRGCK2dYb6E9n4B1E2585HyXOYBiBTkm8DZjmBAmvbTx4LAPz_RRfRfSg" />
<meta name="gmapkey" content="${googlekey}" />
-->
<script src="http://n01se.net/gmapez/gmapez-2.js" type="text/javascript"></script>


<title>Agent Profile</title> 

<style type="text/css">

div.wrapper-img{ width:570px;}
div.thumbline
  {
  margin:2px;
  border:none;
  float:left;
  text-align:center;
 
  background:transparent url(images/agent_prf_bg.png) no-repeat scroll 0 0;
  height:152px;
  padding:20px 20px 15px;
  width:238px;
  }
div.thumbline img
  {
  display:inline;
  
  margin:3px 40px 0px 0px;
  border:1px solid #ffffff;
  }

.table td, .table th, .tableHeaderTable td {
	border:1px;
	padding:0.5em;
}

.table tr.odd {
	background:#FFFFFF ;
	border-top:0px ;
	height:0px;
	padding:0px;
}

.table thead th, .tableHeaderTable td {
	border-style:1px;
	border-width:0;
}

.table caption {
	padding:0px;
}

.table tbody tr:hover, .table tr.over, .contribTable tr:hover {
   border-bottom: none;
   border-top: none;
   cursor:default;
   background-color:#fff !important; /* important needed for Tapestry, as is .table tr:hover */
}

.contribTable tr.even {
   border-top:none;
   background-color:#fff !important;
}

.contribTable tr.odd {
 	border-top: none;
}

.profile_black{color:#000000;}

div#main p {
margin-top:0;

}
.new {display:none;}

.myClass{ height:150px; width:auto}

.urClass{ height:auto; width:150px; }
</style>
<script language="javascript" type="text/javascript">
function getHTTPObject() {

  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {

    try {

      xmlhttp = new XMLHttpRequest();

    } catch (e) {

      xmlhttp = false;

    }

  }

  return xmlhttp;

}

var http = getHTTPObject(); 
</script>

<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
try{
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
}catch(e){}
}
</script>

<script language="javascript">
function secImage1(){


if(document.getElementById('userImage1').height > 150)
{
 document.getElementById('userImage1').style.height = "120px";
 document.getElementById('userImage1').style.width = "auto";
}
}
function secImage2(){

if(document.getElementById('userImage2').height > 150 )
{
 document.getElementById('userImage2').style.height = "120px";
 document.getElementById('userImage2').style.width = "auto";
}        

}
function secImage3(){
if(document.getElementById('userImage3').height > 150)
{
 document.getElementById('userImage3').style.height = "120px";
 document.getElementById('userImage3').style.width = "auto";
} 
}
function secImage4(){
if(document.getElementById('userImage4').height > 150)
{
 document.getElementById('userImage4').style.height = "120px";
 document.getElementById('userImage4').style.width = "auto";
} 
}

</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<script type="text/javascript">
animatedcollapse.addDiv('rating', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('activity', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('mailingadd', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('billingadd', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('terminaladd', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('information', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('description', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('profile_photo', 'fade=0,persist=0,show=1')


animatedcollapse.init();
var GOOGLE_MAP_KEY = googlekey;

function initLoader() { 
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=loadGoogleGeoCode&key='+GOOGLE_MAP_KEY; 
	  document.body.appendChild(script);
}
var geocoder;
var map;

function loadGoogleGeoCode(address1,address2,city,zip,state,country) {
	var address1 = '${partnerPublic.mailingAddress1}';
	var address2 = '${partnerPublic.mailingAddress2}';
		var city = '${partnerPublic.mailingCity}';
			var zip = '${partnerPublic.mailingZip}';
				var state = '${partnerPublic.mailingState}';
					var country = '${partnerPublic.mailingCountry}';
					var lastName='${partnerPublic.lastName}';
	var address =address1+" "+address2+" "+city+"  "+zip+" "+state+" "+country;
	// Create new map object
	//alert(address);
    // Create new geocoding object
    var contentString ='<h5>'+lastName+'</h5>'+
      '<div id="bodyContent">'+address+
      '</div>'+
      '</div>';
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
     	 results[0].geometry.location.lat();
         results[0].geometry.location.lng();
         var uluru = {lat: results[0].geometry.location.lat(), lng:  results[0].geometry.location.lng()};
         document.forms['servicePartnerForm'].elements['latitude'].value = results[0].geometry.location.lat();
     	document.forms['servicePartnerForm'].elements['longitude'].value = results[0].geometry.location.lng();
     	
     	// Center the map on this point
         map = new google.maps.Map(document.getElementById('map'), {zoom: 16, mapTypeControl: false, center: uluru});
      	var marker = new google.maps.Marker({position: uluru, map: map});
      	marker.addListener('click', function() {
             infowindow.open(map, marker);           
             });
      	var infowindow = new google.maps.InfoWindow({
            content: contentString
           });
	
           }});
    // Retrieve location information, pass it to addToMap()
  
}

function ratingList(){
	var partnerCode = document.forms['servicePartnerForm'].elements['code'].value;
	var url="partnerRating.html?decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
}
</script>

</head> 

<s:form id="servicePartnerForm" name="servicePartnerForm">
<s:set name="findPartnerProfileList" value="findPartnerProfileList" scope="request" id="findPartnerProfileList"/>
<c:set var="agentRatingVal" value="N" />
<configByCorp:fieldVisibility componentId="component.partnerProfile.agentRatingCalculation">
	<c:set var="agentRatingVal" value="Y" />
</configByCorp:fieldVisibility>
<div id="Layer1" style="width:100%" >
<div id="newmnav"> 
	<ul>
	
		<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
		<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
		<c:set var="from" value="<%= request.getParameter("from")%>" />
		<s:hidden name="from" value="<%= request.getParameter("from")%>" />
		<c:set var="id" value="<%= request.getParameter("id")%>" />
		<s:hidden name="id" value="<%= request.getParameter("id")%>" />
		<c:set var="code" value="<%= request.getParameter("code")%>" />
		<s:hidden name="code" value="<%= request.getParameter("code")%>" />
		
		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
			<c:if test="${partnerType == 'AG'}">
				<c:if test="${from == 'view'}">
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Profile</span></a></li>
					<c:if test="${!param.popup}"> 
						<li><a href="partnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test="${param.popup}"> 
						<li><a href="searchPartnerVanline.html?decorator=popup&popup=true"><span>Partner List</span></a></li>
					</c:if>
					<!--<li><a href="partnerRateGridsView.html?from=view&partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
				-->
				   <div id="AGR" >
			       <li><a href="partnerviewUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			      </div>
				</c:if>
				
				<c:if test="${from != 'view'}">
					<li><a href="editPartnerPublic.html?id=${id}&partnerType=${partnerType}" ><span>Agent Detail</span></a></li>
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Profile</span></a></li>
					<c:if test="${sessionCorpID!='TSFT' }">  
					<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
					</c:if>
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${code}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${code}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
					<li><a href="baseList.html?id=${id}"><span>Base</span></a></li>
					<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
					    <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
					</c:if>-->
					<li><a href="partnerPublics.html?partnerType=AG"><span>Partner List</span></a></li>
					<!--<c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
					    <li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>         
					</c:if> 
					--><li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
					<c:if test="${partnerPublic.partnerPortalActive == true && partnerType == 'AG'}">
					     <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
					       <li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
					       <li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
					       </configByCorp:fieldVisibility>
					</c:if>
				<c:if test="${partnerType == 'AG'}">
			    <div id="AGR" >
			       <li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			  </div>
			 </c:if>
				</c:if>
			</c:if>
		<c:if test="${partnerType == 'VN'}">
			<li><a href="editPartnerPublic.html?id=${id}&partnerType=${partnerType}" ><span>Vendor Detail</span></a></li>
			<c:if test="${sessionCorpID!='TSFT' }">  
			<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
			</c:if>
		</c:if>
		<c:if test="${partnerType == 'CR'}">
			<li><a href="editPartnerPublic.html?id=${id}&partnerType=${partnerType}" ><span>Carrier Detail</span></a></li>
		</c:if>
		<c:if test="${partnerType == 'OO'}">
			<li><a href="editPartnerPublic.html?id=${id}&partnerType=${partnerType}" ><span>Owner Ops</span></a></li>
		</c:if>
	</sec-auth:authComponent>

<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<c:if test="${from == 'view'}">
	<s:hidden name="from"  id= "from" value="View"/>
	<s:hidden name="ppCode"  id= "ppCode" value="${code}"/>
</c:if>
<c:if test="${from != 'view'}">
	<s:hidden name="from"  id= "from" value=""/>
	<s:hidden name="ppCode"  id= "ppCode" value=""/>
</c:if>
<s:hidden name="fileID" id ="fileID" value="<%= request.getParameter("id")%>" />
<c:set var="fileID" value="<%= request.getParameter("id")%>"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

	
	<c:if test="${!param.popup}">
	<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
					<%if(userRole.equalsIgnoreCase("ROLE_AGENT_ADMIN")){ %>
					 <li><a href="editPartnerPublic.html?id=${id}&partnerType=${partnerType}" ><span>Agent Detail</span></a></li>
					<%} %>	
					   	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Profile<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
    			<%if(userRole.equalsIgnoreCase("ROLE_AGENT_ADMIN")){ %>
					   	<c:if test="${partnerPrivate.partnerPortalActive == true}">
				  			<li><a href="partnerUsersList.html?id=${partnerPublic.id}"><span>Users & Contacts</span></a></li>
						</c:if>
					   	<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
			                <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			            </c:if>
			            <c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
			                <li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
			             </c:if> 
  				<%} %>
					    <li><a href="partnerAgent.html"><span>Agent List</span></a></li>
					    
					   <li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
	</sec-auth:authComponent>
	</c:if>
	</ul>
</div>
<div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top" style="!margin-top:-46px;">
   	<div class="top" style="!margin-top:3px;"><span></span></div>
   	<div class="center-content">
   		<table  cellspacing="1" cellpadding="0" border="0" width="100%" style="margin:0px;padding: 0px;">
   			<tbody>
			   	<tr>
    				<td align="left" class="listwhitebox" valign="top" width="80%">
    					<table border="0" width="100%" cellpadding="1" cellspacing="0" style="margin: 0px;">
    						<tr>
    							<td valign="top" width="100%" style="margin: 0px;">
    								<display:table name="findPartnerProfileList" class="new" requestURI="" id="findPartnerProfileList" style="width:100%;border:0px;margin-bottom: 0px;cursor:auto;padding:0px !important;"  >
	 									<display:column  style="border:0px; padding:0px height:0px !important;" />
	 									<display:column  style="border:0px; padding:0px height:0px !important;"/>
	 									<display:caption style="border: 0px;height:0px  !important;"></display:caption>
									</display:table>
								</td>
       						</tr>
       						<configByCorp:fieldVisibility componentId="component.field.profile.showRatingSection">
    						<tr>
       							<td  colspan="5" width="100%" style="margin: 0px;">
       								<span onclick="swapImg(img1);"><div onClick="javascript:animatedcollapse.toggle('rating'); " style="margin:0px;">
       									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img1" src="${pageContext.request.contextPath}/images/129.png"/></a>&nbsp;Rating</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										
									</div></span>
									<c:if test="${agentRatingVal=='N'}">
										<div id="rating" style="margin:0px; padding:0px; margin-left:10px; margin-bottom:0.3em;">
	    									<iframe src ="partnerRating.html?decorator=simple&popup=true&partnerCode=${code}&id=${id}"  WIDTH="100%" FRAMEBORDER=0>
		  										<p>Your browser does not support iframes.</p>
											</iframe>
										</div>
									</c:if>
									<c:if test="${agentRatingVal=='Y'}">
										<div id="rating" style="margin:0px; padding:0px; margin-left:10px; margin-bottom:0.3em;">
	    									<iframe src ="agentRatingCalculation.html?decorator=simple&popup=true&partnerCode=${code}"  WIDTH="100%" FRAMEBORDER=0>
		  										<p>Your browser does not support iframes.</p>
											</iframe>
										</div>
									</c:if>
								</td>
							</tr>
							<tr>
       							<td  colspan="8" width="100%" style="margin: 0px;">
       								<span onclick="swapImg(img2);"><div  onClick="javascript:animatedcollapse.toggle('activity');" style="margin:0px;">
       									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img2" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Activity Analysis</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										
									</div></span>
									<div id="activity" style="margin:0px;padding:0px; margin-left:10px; margin-bottom:0.3em;">
    									<iframe src ="partnerActivity.html?decorator=simple&popup=true&partnerCode=${code}&id=${id}"  WIDTH="100%" FRAMEBORDER=0>
	  											<p>Your browser does not support iframes.</p>
										</iframe>
									</div>
								</td>
							</tr>
							</configByCorp:fieldVisibility>
    						<tr>
       							<td  colspan="5" width="100%">
       								<!--      
       									<div  onClick="javascript:animatedcollapse.toggle('mailingadd')" style="margin: 0px"  class="subcontenttabChild">Mailing Address</div>		
									-->
									<span onclick="swapImg(img3);"><div  onClick="javascript:animatedcollapse.toggle('mailingadd')" style="margin: 0px;">
       									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img3" src="${pageContext.request.contextPath}/images/130.png" /></a>&nbsp;Mailing Address</td>
												<td class="probg_right"></td>
											</tr>
										</table>
									</div>
									</span>
									<div id="mailingadd" >
    									<table width="100%" style="padding-left:10px; margin-bottom:0.3em;"> 
											<tr>
											 	<td width="12%"><b>Address</b></td>
												<td width="50%">${findPartnerProfileList.mailingAddress1}</td>
												<td width="8%"><b>Fax</b></td>
												<td>${findPartnerProfileList.mailingFax}</td>
											</tr>
											<c:if test="${findPartnerProfileList.mailingAddress2 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.mailingAddress2}</td>
												</tr>
											</c:if>
											<c:if test="${findPartnerProfileList.mailingAddress3 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.mailingAddress3}</td>
												</tr>
											</c:if>
											<c:if test="${findPartnerProfileList.mailingAddress4 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.mailingAddress4}</td>
												</tr>
											</c:if>
											<tr>
												<td><b>Country</b></td>
												<td>${findPartnerProfileList.mailingCountry}</td>
												<td><b>Phone</b></td>
												<td>${findPartnerProfileList.mailingPhone}</td>
											</tr>
											<tr>
												<td><b>State</b></td>
												<td>${findPartnerProfileList.mailingState}</td>
												<td><b>Email</b></td>
												<td>${findPartnerProfileList.mailingEmail}</td>
											</tr>
											<tr>
												<td><b>City</b></td>
												<td>${findPartnerProfileList.mailingCity}</td>
												<c:if test="${findPartnerProfileList.mailingTelex !=''}">
													<td><b>Telex</b></td>
													<td>${findPartnerProfileList.mailingTelex}</td>
												</c:if>
											</tr>
											<tr>
												<td><b>Zip</b></td>
												<td>${findPartnerProfileList.mailingZip}</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td  colspan="5" width="100%">	
									<span onclick="swapImg(img4);"><div  onClick="javascript:animatedcollapse.toggle('billingadd')" style="margin: 0px;">
								    	<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img4" src="${pageContext.request.contextPath}/images/130.png" /></a>&nbsp;Billing Address</td>
												<td class="probg_right"></td>
											</tr>
										</table>
									</div>
									</span>			
									<div id="billingadd" >
										<table width="100%" style="padding-left:10px; margin-bottom:0.3em;">
											<tr>
												<td width="12%"><b>Address</b></td>
												<td width="50%">${findPartnerProfileList.billingAddress1}</td>
												<td width="8%"><b>Fax</b></td>
												<td>${findPartnerProfileList.billingFax}&nbsp;</td>
											</tr>
											<c:if test="${findPartnerProfileList.billingAddress2 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.billingAddress2}</td>
												</tr>
											</c:if>
											<c:if test="${findPartnerProfileList.billingAddress3 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.billingAddress3}</td>
												</tr>
											</c:if>
											<c:if test="${findPartnerProfileList.billingAddress4 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.billingAddress4}</td>
												</tr>
											</c:if>
											<tr>
												<td><b>Country</b></td>
												<td>${findPartnerProfileList.billingCountry}</td>
												<td><b>Phone</b></td>
												<td>${findPartnerProfileList.billingPhone}</td>
											</tr>
											<tr>
												<td><b>State</b></td>
												<td>${findPartnerProfileList.billingState}</td>
												<td><b>Email</b></td>
												<td>${findPartnerProfileList.billingEmail}</td>
											</tr>
											<tr>
												<td><b>City</b></td>
												<td>${findPartnerProfileList.billingCity}</td>
												<c:if test="${findPartnerProfileList.billingTelex !=''}">
													<td><b>Telex</b></td>
													<td>${findPartnerProfileList.billingTelex}</td>
												</c:if>
											</tr>
											<tr>
												<td><b>Zip</b></td>
												<td>${findPartnerProfileList.billingZip}</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td  colspan="5" width="100%">
									<span onclick="swapImg(img5);"><div onClick="javascript:animatedcollapse.toggle('terminaladd')" style="margin: 0px;">
						       			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img5" src="${pageContext.request.contextPath}/images/130.png" /></a>&nbsp;Terminal Address</td>
												<td class="probg_right"></td>
											</tr>
										</table>
									</div>
									</span>	
									<div id="terminaladd" >
						    			<table width="100%" style="padding-left:10px; margin-bottom:0.3em;">
											<tr>
												<td width="12%"><b>Address</b></td>
												<td width="50%">${findPartnerProfileList.terminalAddress1}</td>
												<td width="8%"><b>Fax</b></td>
												<td>${findPartnerProfileList.terminalFax}</td>
											</tr>
											<c:if test="${findPartnerProfileList.terminalAddress2 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.terminalAddress2}</td>
												</tr>
											</c:if>
											<c:if test="${findPartnerProfileList.terminalAddress3 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.terminalAddress3}</td>
												</tr>
											</c:if>
											<c:if test="${findPartnerProfileList.terminalAddress4 !=''}">
												<tr>
													<td></td>
													<td>${findPartnerProfileList.terminalAddress4}</td>
												</tr>
											</c:if>
											<tr>
												<td><b>Country</b></td>
												<td>${findPartnerProfileList.terminalCountry}</td>
												<td><b>Phone</b></td>
												<td>${findPartnerProfileList.terminalPhone}</td>
											</tr>
											<tr>
												<td><b>State</b></td>
												<td>${findPartnerProfileList.terminalState}</td>
												<td><b>Email</b></td>
												<td>${findPartnerProfileList.terminalEmail}</td>
											</tr>
											<tr>
												<td><b>City</b></td>
												<td>${findPartnerProfileList.terminalCity}</td>
												<c:if test="${findPartnerProfileList.terminalTelex !=''}">
													<td><b>Telex</b></td>
													<td>${findPartnerProfileList.terminalTelex}</td>
												</c:if>
											</tr>
											<tr>
												<td><b>Zip</b></td>
												<td>${findPartnerProfileList.terminalZip}</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td  colspan="5" width="100%">
									<span onclick="swapImg(img6);"><div  onClick="javascript:animatedcollapse.toggle('information')" style="margin: 0px;">
							       		<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img6" src="${pageContext.request.contextPath}/images/130.png" /></a>&nbsp;Information</td>
												<td class="probg_right"></td>
											</tr>
										</table>
									</div>
									</span>	
									<div id="information" >
							    		<table width="100%" style="padding-left:10px;margin-bottom:0.3em;">
											<tr>
												<%-- 
												<td width="21%"><b>Service Range</b><font style="font-size:8px;">(Km)</font></td>
												<td width="14%">${findPartnerProfileList.serviceRangeKms}</td>
												<td width="17%"><b>Service Range</b><font style="font-size:8px;">(Mi)</font></td>
												<td width="16%">${findPartnerProfileList.serviceRangeMiles}</td>
												--%>
												<td width="20%"><b>Facility Size</b><font style="font-size:8px;">(SQMT)</font></td>
												<td width="10%">${findPartnerProfileList.facilitySizeSQMT}</td>
												<td width="20%" valign="top"><b>Facility Size</b><font style="font-size:8px;">(SQFT)</font></td>
												<td width="10%" valign="top">${findPartnerProfileList.facilitySizeSQFT}</td>
												<td></td><td></td>
											</tr>
											<tr>
												<td width="20%" valign="top"><b>FIDI</b></td>
												<td width="10%" valign="top">${findPartnerProfileList.fidiNumber}</td>
												<td width="20%" valign="top"><b>OMNI Number</b></td>
												<td width="20%" valign="top">${findPartnerProfileList.OMNINumber}</td>
												<td></td><td></td>
											</tr>
											<tr>
												<td width="20%" valign="top"><b>AMSA</b></td>
												<td width="10%" valign="top">${findPartnerProfileList.AMSANumber}</td>
												<td width="20%" valign="top"><b>WERC</b></td>
												<td width="10%" valign="top">${findPartnerProfileList.WERCNumber}</td>
												<td width="10%" valign="top"><b>IAM</b></td>
												<td width="10%" valign="top">${findPartnerProfileList.IAMNumber}</td>
											</tr>
											<tr>
												<td colspan="6">
													<table style="background-color:#F4F7F8;border:1px solid #CCC; width:100%" class="detailTabLabel">
														<tr>
															<td valign="top" width="20%"><b>VanLine Affiliation(s)</b></td>
															<td valign="top" >${findPartnerProfileList.vanLineAffiliation}</td>
														</tr>
														<tr>
															<td valign="top" width="20%"><b>Service Lines</b></td>
															<td valign="top" >${findPartnerProfileList.serviceLines}</td>
														</tr>
														<tr>
															<td valign="top" width="20%"><b>Quality Certifications</b></td>
															<td valign="top" >${findPartnerProfileList.qualityCertifications}</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<%-- 
							<c:if test="${!(findPartnerProfileList.companyProfile == null || findPartnerProfileList.companyProfile == '')}">
							--%>
							<tr>
								<td  colspan="5" width="100%">
									<span onclick="swapImg(img7);"><div  onClick="javascript:animatedcollapse.toggle('description')" style="margin: 0px;">
							       		<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img7" src="${pageContext.request.contextPath}/images/130.png" /></a>&nbsp;Description</td>
												<td class="probg_right"></td>
											</tr>
										</table>
									</div>
									</span>	
									<div id="description" >
							    		<table width="100%" style="padding-left:10px; margin-bottom:0.3em;">
											<c:if test="${(findPartnerProfileList.companyProfile == null || findPartnerProfileList.companyProfile == '') 
											&& (findPartnerProfileList.companyFacilities == null || findPartnerProfileList.companyFacilities == '')
											&& (findPartnerProfileList.companyCapabilities == null || findPartnerProfileList.companyCapabilities == '')
											&& (findPartnerProfileList.companyDestiantionProfile == null || findPartnerProfileList.companyDestiantionProfile == '')}">
											<br>
											<font style="font-weight:bold; padding-left:11px;">No description available.</font>
											</c:if>
											<c:if test="${!(findPartnerProfileList.companyProfile == null || findPartnerProfileList.companyProfile == '')}">
												<tr>
													<td style="padding-left:5px;padding-bottom:3px;"><b>Company Profile</b></td></tr>
												<tr>
													<td><p align="justify" style="color:#000000;padding-left:5px;">${findPartnerProfileList.companyProfile}</p></td></tr>
											</c:if>
											<tr>
												<td height="10"></td>
											</tr>
											<c:if test="${!(findPartnerProfileList.companyFacilities == null || findPartnerProfileList.companyFacilities == '')}">
												<tr>
													<td style="padding-left:5px;padding-bottom:3px;"><b>Facilities</b></td>
												</tr>
												<tr>
													<td><p align="justify" style="color:#000000; padding-left:5px;">${findPartnerProfileList.companyFacilities}</p></td>
												</tr>
											</c:if>
											<tr>
												<td height="10"></td>
											</tr>
											<c:if test="${!(findPartnerProfileList.companyCapabilities == null || findPartnerProfileList.companyCapabilities == '')}">
												<tr>
													<td style="padding-left:5px;padding-bottom:3px;"><b>Capabilities</b></td>
												</tr>
												<tr>
													<td><p align="justify" style="color:#000000; padding-left:5px;">${findPartnerProfileList.companyCapabilities}</p></td>
												</tr>
											</c:if>
											<tr>
												<td height="10"></td>
											</tr>
											<c:if test="${!(findPartnerProfileList.companyDestiantionProfile == null || findPartnerProfileList.companyDestiantionProfile == '')}">
												<tr>
													<td style="padding-left:5px;padding-bottom:3px;"><b>Destination Profile</b></td>
												</tr>
												<tr>
													<td><p align="justify" style="color:#000000;padding-left:5px;">${findPartnerProfileList.companyDestiantionProfile}</p></td>
												</tr>
											</c:if>
									</table>
								</div>
							</td>
						</tr>		
						<tr>
							<td  colspan="5" width="100%">
								<span onclick="swapImg(img8);"><div  onClick="javascript:animatedcollapse.toggle('profile_photo')" style="margin: 0px;">
       								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
										<tr>
											<td class="probg_left"></td>
											<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img8" src="${pageContext.request.contextPath}/images/130.png" /></a>&nbsp;Profile Photos</td>
											<td class="probg_right"></td>
										</tr>
									</table>
								</div>
								</span>	
								<div id="profile_photo" >
    								<table width="100%" style="padding-left:10px;">    
								      <tr>
								      	<td colspan="5">
								      		<div class="wrapper-img">
											    <c:if test="${(findPartnerProfileList.location1 == null || findPartnerProfileList.location1 == '')
											    && (findPartnerProfileList.location2 == null || findPartnerProfileList.location2 == '')
											    && (findPartnerProfileList.location3 == null || findPartnerProfileList.location3 == '')
											    && (findPartnerProfileList.location4 == null || findPartnerProfileList.location4 == '')
											    }">
												    <br>
												    <b>No photo available.</b>
											    </c:if>  
      											<c:if test="${!(findPartnerProfileList.location1 == null || findPartnerProfileList.location1 == '')}">      
													<div class="thumbline">
												 		<img id="userImage1" class="urClass"  src="UserImage?location=${findPartnerProfileList.location1}" alt=""  onload="secImage1()" style="border:thin solid #219DD1; vertical-align: middle; "/>
												 	</div>
												</c:if>
												<c:if test="${!(findPartnerProfileList.location2 == null || findPartnerProfileList.location2 == '')}">
													<div class="thumbline">
														<img id="userImage2" class="urClass" src="UserImage?location=${findPartnerProfileList.location2}" alt=""  onload="secImage2()"    style="border:thin solid #219DD1; vertical-align: middle; "/>
													</div>
												</c:if>
												<c:if test="${!(findPartnerProfileList.location3 == null || findPartnerProfileList.location3 == '')}">
													<div class="thumbline">
														<img id="userImage3" class="urClass" src="UserImage?location=${findPartnerProfileList.location3}" alt="" onload="secImage3()"  style="border:thin solid #219DD1; vertical-align: middle; "/>
													</div>
												</c:if>
												<c:if test="${!(findPartnerProfileList.location4 == null || findPartnerProfileList.location4 == '')}">
													<div class="thumbline">
														<img id="userImage4" class="urClass" src="UserImage?location=${findPartnerProfileList.location4}" alt="" onload="secImage4()"   style="border:thin solid #219DD1; vertical-align: middle;"/>
													</div>
												</c:if>
											</div>
      									</td>
      								</tr>     
								</table>
							</div>
						</td>
					</tr>
     		</table>
       </td>
       <td valign="top" width="20%" style="padding-left:18px;padding-top:2px;">
       		<table class="detailTabLabel" border="0" width="100%" cellpadding="2" cellspacing="1">    
        		<tr>
			        <td colspan="3">
			        	<div style="border:1px solid #999; background-color:#EFEFEF;padding-top:5px;padding-left:5px;">
			        		<table width="100%">
			        			<tr>
									<td class="listwhitebox"><b>Name</b></td>
									<td class="listwhitebox">${findPartnerProfileList.lastName}</td>
								</tr>
								<tr>
									<td class="listwhitebox"><b>Code</b></td>
									<td class="listwhitebox">${findPartnerProfileList.partnerCode}</td>
								</tr>
								<tr>
									<td class="listwhitebox"><b>Status</b></td>
									<td class="listwhitebox">${findPartnerProfileList.status}</td>
								</tr>
								<tr>
									<td class="listwhitebox"><b>Website</b></td>
									<td class="listwhitebox"><a href="${findPartnerProfileList.url}" target="_blank">${findPartnerProfileList.url}</a></td>
								</tr>
								<tr>
									<td class="listwhitebox"><b>External Ref.</b></td>
									<td class="listwhitebox">${findPartnerProfileList.extReference}</td>
								</tr>
							</table>
						</div>
        			</td>        
        		</tr>
        		
        		<tr>
        			<td colspan="3">
				        <div style="border:1px solid #3978A8; background-color:#EFEFEF;">
				        	<table width="100%" style="margin:0px;">
				        		<tr>
				        			<td align="left" class="listwhitetext" >
				        				<div id ="map" 
										    style="width: 300px; height: 300px;">
										  <a href="http://maps.google.com/maps?ll=${findPartnerProfileList.latitude},${findPartnerProfileList.longitude}&amp;spn=0.006130,0.009795&amp;hl=en">
										  </a>
										</div>
         							</td>        
								</tr>
       							
       						</table>
					       	<s:hidden name="latitude"/>
					       	<s:hidden name="longitude" />
						   	<c:if test="${not empty findPartnerProfileList.latitude}">
					<div style="height:55px;background-image:-moz-linear-gradient(center bottom,rgb(185,220,250) 19%,rgb(214,235,255) 60%,rgb(204,229,255) 80%);
						background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0.19, rgb(185,220,250)),color-stop(0.6, rgb(214,235,255)),color-stop(0.8, rgb(204,229,255)));">
						     		<table width="100%" cellpadding="3">
						      			<tr>
											<td align="left" class="listwhitetext" width="55">Latitude :</td>
											<td class="listwhitebox">${findPartnerProfileList.latitude}</td>
										</tr>
										<tr>
									 		<td align="left" class="listwhitetext" width="55">Longitude :</td>
											<td class="listwhitebox">${findPartnerProfileList.longitude}</td>
										</tr>
									</table>
     							</div>
							</c:if>
       					</div>
       				</td><B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
       			</tr>       
       		</table>
       	</td>
	</tr>
	<tr><td height="25">&nbsp;</td></tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form> 
<script type="text/javascript">
try{
	initLoader();
	<c:if test="${partnerType=='AG'}">
	var checkRoleAG = 0;
	//Modified By subrat BUG #6471 
	<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
		checkRoleAG  = 14;
	</sec-auth:authComponent>
	if(checkRoleAG < 14){
		document.getElementById('AGR').style.visibility = 'visible';
	}else{
		document.getElementById('AGR').style.visibility = 'hidden';
	}
	</c:if>
}catch(e){}

</script>