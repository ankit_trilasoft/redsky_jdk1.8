<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
  
<head>   
    <title>Module Item List</title>   
    <meta name="heading" content="Module Item List"/>
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>
<style type="text/css">
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<script language="javascript" type="text/javascript">
function updateIdField(target,pageActionId,role,moduleC,url)
{
  if((target.checked==true)){
    var newIds=document.forms['form2'].elements['sidL'].value;
    var agree = confirm(" Click Ok to update related page permissions\n[Clicking cancel will not update related page permissions\n you can choose individual pages to grant access to]");
	 if(agree) {
		 document.getElementById("loader").style.display = "block"
			<c:forEach items="${moduleItems}" var="list1">
  			var module='${list1.module}';
  			var sid="${list1.id}";
  			var url="${list1.actionuri}";
  			 if(module.trim()==moduleC.trim()){		
   			document.getElementById(sid).checked=true;
		   			if(newIds==''){
					    newIds=sid+"~"+role+"~"+url;
					}else{
					    newIds=newIds + ',' + sid+"~"+role+"~"+url;
				    }   			
  			 }
   			</c:forEach>
	 }else{
		 	document.getElementById("loader").style.display = "block";
		    if(newIds==''){
			    newIds=pageActionId+"~"+role+"~"+url;
			}else{
			    newIds=newIds + ',' + pageActionId+"~"+role+"~"+url;
		    }
	 }
	    document.forms['form2'].elements['sidL'].value=newIds;
   }
  if((target.checked==false) ){
	    var newIds=document.forms['form2'].elements['sidL'].value;
	    var agree = confirm(" Click Ok to update related page permissions\n[Clicking cancel will not update related page permissions\n you can choose individual pages to grant access to]");
		 if(agree) {
			 document.getElementById("loader").style.display = "block"
				<c:forEach items="${moduleItems}" var="list1">
	  			var module='${list1.module}';
	  			var sid="${list1.id}";
	  			var url="${list1.actionuri}";	  			
	  			 if(module.trim()==moduleC.trim()){		
	 			    if(newIds.indexOf(sid+"~"+role+"~"+url)>-1){
					    newIds=newIds.replace(sid+"~"+role+"~"+url,"");
					    newIds=newIds.replace(",,",",");
					    var len=newIds.length-1;
					    if(len==newIds.lastIndexOf(",")){
					    newIds=newIds.substring(0,len);
					    }
					    if(newIds.indexOf(",")==0){
					    newIds=newIds.substring(1,newIds.length);
					    }
				    }
	 			   document.getElementById(sid).checked=false;
		  		}
	   			</c:forEach>
		 }else{
			 	document.getElementById("loader").style.display = "block";
			    if(newIds.indexOf(pageActionId+"~"+role+"~"+url)>-1){
				    newIds=newIds.replace(pageActionId+"~"+role+"~"+url,"");
				    newIds=newIds.replace(",,",",");
				    var len=newIds.length-1;
				    if(len==newIds.lastIndexOf(",")){
				    newIds=newIds.substring(0,len);
				    }
				    if(newIds.indexOf(",")==0){
				    newIds=newIds.substring(1,newIds.length);
				    }
				   
			    }
		 }
		 document.forms['form2'].elements['sidL'].value=newIds;
	    /*
	    
		    if(newIds.indexOf(pageActionId+"~"+role)>-1){
			    newIds=newIds.replace(pageActionId+"~"+role,"");
			    newIds=newIds.replace(",,",",");
			    var len=newIds.length-1;
			    if(len==newIds.lastIndexOf(",")){
			    newIds=newIds.substring(0,len);
			    }
			    if(newIds.indexOf(",")==0){
			    newIds=newIds.substring(1,newIds.length);
			    }
			    document.forms['form2'].elements['sidL'].value=newIds;
		    }*/
	    }
	document.forms['form2'].submit();
	
}
function refreshThisPage(){
	
	 showOrHide(1);
	document.forms['form2'].action ='moduleItems.html?perRec=${perRec}&decorator=popup&popup=true';
	document.forms['form2'].submit();
	 showOrHide(0);
}
function clear_fields(){
	document.forms['form2'].elements['moduleName'].value = '';
	document.forms['form2'].elements['moduleDescr'].value = '';
	document.forms['form2'].elements['moduleUrl'].value = '';
}
function findModuleItemList(){
 	document.getElementById("loader").style.display = "block";
    document.forms['form2'].action ="moduleItems.html?perRec=${perRec}&decorator=popup&popup=true";
	document.forms['form2'].submit();
 	//document.getElementById("loader").style.display = "none";
}
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay111"].visibility='hide';
        else
           document.getElementById("overlay111").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay111"].visibility='show';
       	else
          document.getElementById("overlay111").style.visibility='visible';
   	}
}
function backMenuItemList(){
	document.getElementById("loader").style.display = "block"
    document.forms['form2'].action ="menuItems.html?perRec=${perRec}&decorator=popup&popup=true";
	document.forms['form2'].submit();
	 //showOrHide(0);
}
</script>
<s:form id="form2" action="assignModuleItem.html?perRec=${perRec}&decorator=popup&popup=true" method="post">

<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
    <input type="button" class="cssbutton" value="Search" style="width:55px; height:25px;" onclick="findModuleItemList();"/>
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Module</th>
		<th>Description</th>		
		<th>URL</th>		
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left"   width="227">
					    <s:select cssClass="list-menu" name="moduleName" list="%{modulelist}" headerKey="" headerValue="" cssStyle="width:190px" />
					</td>
					<td align="left" width="340">
					    <s:textfield name="moduleDescr" required="true" cssClass="input-text" size="50"/>
					</td>					
					<td align="left">
					    <s:textfield name="moduleUrl" required="true" cssClass="input-text" size="35"/>
					</td>					
				</tr>		
				<tr><td></td><td></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<s:hidden name="sidL" value=""/>
<s:hidden name="oldSidL" value=""/>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  <li class="current"><a><span>Module List</span></a></li>  
  </ul>
  <td style="padding-top:10px;">
<span style="font-weight: bold; font-size: 13px; padding: 20px 0pt 0pt 20px;">${perRec}</span>
</td>
</tr>
</td></tr>
</tbody></table>
<s:set name="moduleItems" value="moduleItems" scope="request"/> 
<display:table name="moduleItems" class="table" requestURI="" defaultsort="4" id="moduleItemList" export="false">
<display:column title="Assign" sortable="true">
<c:if test="${moduleItemList.permission == '2'}" >
<s:checkbox id="${moduleItemList.id}" name="permision12" value="true" onclick="updateIdField(this,'${moduleItemList.id}','${perRec}','${moduleItemList.module}','${moduleItemList.actionuri}')" required="true" />
</c:if>
<c:if test="${moduleItemList.permission != '2'}" >
		<s:checkbox id="${moduleItemList.id}" name="permision12" value="false" onclick="updateIdField(this,'${moduleItemList.id}','${perRec}','${moduleItemList.module}','${moduleItemList.actionuri}')" required="true" />
</c:if>
</display:column>
<display:column property="module" sortable="true" title="Module" />
<display:column property="description" sortable="true" titleKey="menuItem.description" />
<display:column property="actionuri" sortable="true" titleKey="menuItem.url" />
</display:table>
<table><tr><td align="left">

<input type="button" class="cssbutton" value="Back To Menu List" style="width:125px; height:25px;" onclick="backMenuItemList();"/>
</td></tr>
</table>


<div id="loader" style="text-align:center; display:none">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form>
 <script>
setTimeout("showOrHide(0)",2000);
</script>
<script language="javascript" type="text/javascript">
try{
  var role='${perRec}';
	<c:forEach items="${moduleItems}" var="list1">
		var pageActionId='${list1.id}';
		var permission='${list1.permission}';
		var url="${list1.actionuri}";
		 if(permission.trim()=='2'){		
			 var newIds=document.forms['form2'].elements['sidL'].value;
			 if(newIds==''){
			     newIds=pageActionId+"~"+role+"~"+url;
			 }else{
			     newIds=newIds + ',' + pageActionId+"~"+role+"~"+url;
			 }
			     document.forms['form2'].elements['sidL'].value=newIds;
			     
		 }
		</c:forEach>
		document.forms['form2'].elements['oldSidL'].value=document.forms['form2'].elements['sidL'].value;
}catch(e){}
</script>
