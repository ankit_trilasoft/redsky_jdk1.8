package com.keysurvey.api.v68.form.design;

public class FormDesignManagementServiceProxy implements com.keysurvey.api.v68.form.design.FormDesignManagementService {
  private String _endpoint = null;
  private com.keysurvey.api.v68.form.design.FormDesignManagementService formDesignManagementService = null;
  
  public FormDesignManagementServiceProxy() {
    _initFormDesignManagementServiceProxy();
  }
  
  public FormDesignManagementServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initFormDesignManagementServiceProxy();
  }
  
  private void _initFormDesignManagementServiceProxy() {
    try {
      formDesignManagementService = (new com.keysurvey.api.v68.form.design.FormDesignManagementLocator()).getFormDesignManagementServicePort();
      if (formDesignManagementService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)formDesignManagementService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)formDesignManagementService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (formDesignManagementService != null)
      ((javax.xml.rpc.Stub)formDesignManagementService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.keysurvey.api.v68.form.design.FormDesignManagementService getFormDesignManagementService() {
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService;
  }
  
  public com.keysurvey.api.v68.form.design.WSQuestion[] getQuestions(long formId, boolean withAnswer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getQuestions(formId, withAnswer);
  }
  
  public void deleteQuestion(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.deleteQuestion(questionId);
  }
  
  public com.keysurvey.api.v68.form.design.WSQuestion getQuestion(long questionId, boolean withAnswer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getQuestion(questionId, withAnswer);
  }
  
  public long addQuestion(com.keysurvey.api.v68.form.design.WSQuestion question) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.addQuestion(question);
  }
  
  public com.keysurvey.api.v68.form.design.WSAnswer[] getAnswers(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getAnswers(questionId);
  }
  
  public com.keysurvey.api.v68.form.design.WSAnswer getAnswer(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getAnswer(answerId);
  }
  
  public long addAnswer(com.keysurvey.api.v68.form.design.WSAnswer answer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.addAnswer(answer);
  }
  
  public com.keysurvey.api.v68.form.design.WSForm getForm(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getForm(formId);
  }
  
  public void deleteAnswer(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.deleteAnswer(answerId);
  }
  
  public com.keysurvey.api.v68.form.design.WSFormTree getFormTree(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getFormTree(formId);
  }
  
  public long createCompleteForm(com.keysurvey.api.v68.form.design.WSForm form, com.keysurvey.api.v68.form.design.WSFormDesign formDesign, com.keysurvey.api.v68.form.design.WSSubmissionPolicy submissionPolicy, com.keysurvey.api.v68.form.design.WSFormNotes formNotes, com.keysurvey.api.v68.form.design.WSQuestion[] questionList) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.createCompleteForm(form, formDesign, submissionPolicy, formNotes, questionList);
  }
  
  public long createForm(com.keysurvey.api.v68.form.design.WSForm form, com.keysurvey.api.v68.form.design.WSQuestion[] questionList) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.createForm(form, questionList);
  }
  
  public void updateForm(com.keysurvey.api.v68.form.design.WSForm form) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.updateForm(form);
  }
  
  public void deleteForm(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.deleteForm(formId);
  }
  
  public long createCompleteQuestion(com.keysurvey.api.v68.form.design.WSQuestion question, com.keysurvey.api.v68.form.design.WSQuestionSettings questionSettings) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.createCompleteQuestion(question, questionSettings);
  }
  
  public void updateQuestion(com.keysurvey.api.v68.form.design.WSQuestion question) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.updateQuestion(question);
  }
  
  public void updateAnswer(com.keysurvey.api.v68.form.design.WSAnswer answer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.updateAnswer(answer);
  }
  
  public com.keysurvey.api.v68.form.design.WSSubmissionPolicy getSubmissionPolicy(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getSubmissionPolicy(formId);
  }
  
  public void setSubmissionPolicy(com.keysurvey.api.v68.form.design.WSSubmissionPolicy submissionPolicy) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.setSubmissionPolicy(submissionPolicy);
  }
  
  public com.keysurvey.api.v68.form.design.WSFormNotes getFormNotes(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getFormNotes(formId);
  }
  
  public void setFormNotes(com.keysurvey.api.v68.form.design.WSFormNotes formNotes) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.setFormNotes(formNotes);
  }
  
  public com.keysurvey.api.v68.form.design.WSFormDesign getFormDesign(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getFormDesign(formId);
  }
  
  public void setFormDesign(com.keysurvey.api.v68.form.design.WSFormDesign formDesign) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.setFormDesign(formDesign);
  }
  
  public com.keysurvey.api.v68.form.design.WSQuestionSettings getQuestionSettings(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getQuestionSettings(questionId);
  }
  
  public void setQuestionSettings(com.keysurvey.api.v68.form.design.WSQuestionSettings questionSetting) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.setQuestionSettings(questionSetting);
  }
  
  public void moveQuestion(long questionId, int offset) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.moveQuestion(questionId, offset);
  }
  
  public int getQuestionPosition(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getQuestionPosition(questionId);
  }
  
  public void moveAnswer(long answerId, int offset) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.moveAnswer(answerId, offset);
  }
  
  public int getAnswerPosition(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getAnswerPosition(answerId);
  }
  
  public com.keysurvey.api.v68.form.design.WSAnswer getAnswerByPosition(long questionId, int position) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getAnswerByPosition(questionId, position);
  }
  
  public com.keysurvey.api.v68.form.design.WSQuestion getQuestionByPosition(long formId, int position) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getQuestionByPosition(formId, position);
  }
  
  public void setFormLogic(com.keysurvey.api.v68.form.design.WSFormLogic formLogic) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.setFormLogic(formLogic);
  }
  
  public com.keysurvey.api.v68.form.design.WSFormLogic getFormLogic(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getFormLogic(formId);
  }
  
  public com.keysurvey.api.v68.form.design.WSQuestionLogic getQuestionLogic(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getQuestionLogic(questionId);
  }
  
  public void setQuestionLogic(long questionId, com.keysurvey.api.v68.form.design.WSQuestionLogic questionLogic) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    formDesignManagementService.setQuestionLogic(questionId, questionLogic);
  }
  
  public com.keysurvey.api.v68.form.design.WSForm[] getForms(long accountId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault{
    if (formDesignManagementService == null)
      _initFormDesignManagementServiceProxy();
    return formDesignManagementService.getForms(accountId);
  }
  
  
}