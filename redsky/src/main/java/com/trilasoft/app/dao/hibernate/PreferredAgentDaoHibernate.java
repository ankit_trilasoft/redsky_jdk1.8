package com.trilasoft.app.dao.hibernate;

import java.util.*;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PreferredAgentDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.PreferredAgent;

public class PreferredAgentDaoHibernate extends GenericDaoHibernate<PreferredAgent, Long> implements PreferredAgentDao {
	
	private HibernateUtil hibernateUtil;
	public PreferredAgentDaoHibernate() {
		super(PreferredAgent.class);
	}
	
	public class agentDetailsList
	{
		
		private Object id;
		private Object corpID;
		private Object partnerCode;
		private Object firstName;
		private Object lastName;
		private Object aliasName;
		private Object partnerType;
		private Object billingCountryCode;
		private Object billingState;
		private Object billingCity;
		private Object billingCountry;
		private Object terminalAddress1;
		private Object terminalAddress2;
		private Object terminalCity;
		private Object terminalZip;
		private Object terminalState;
		private Object terminalCountry;
		private Object rank;
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getAliasName() {
			return aliasName;
		}
		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}
		public Object getPartnerType() {
			return partnerType;
		}
		public void setPartnerType(Object partnerType) {
			this.partnerType = partnerType;
		}
		public Object getBillingCountryCode() {
			return billingCountryCode;
		}
		public void setBillingCountryCode(Object billingCountryCode) {
			this.billingCountryCode = billingCountryCode;
		}
		public Object getBillingState() {
			return billingState;
		}
		public void setBillingState(Object billingState) {
			this.billingState = billingState;
		}
		public Object getBillingCity() {
			return billingCity;
		}
		public void setBillingCity(Object billingCity) {
			this.billingCity = billingCity;
		}
		public Object getBillingCountry() {
			return billingCountry;
		}
		public void setBillingCountry(Object billingCountry) {
			this.billingCountry = billingCountry;
		}
		public Object getTerminalAddress1() {
			return terminalAddress1;
		}
		public void setTerminalAddress1(Object terminalAddress1) {
			this.terminalAddress1 = terminalAddress1;
		}
		public Object getTerminalAddress2() {
			return terminalAddress2;
		}
		public void setTerminalAddress2(Object terminalAddress2) {
			this.terminalAddress2 = terminalAddress2;
		}
		public Object getTerminalCity() {
			return terminalCity;
		}
		public void setTerminalCity(Object terminalCity) {
			this.terminalCity = terminalCity;
		}
		public Object getTerminalZip() {
			return terminalZip;
		}
		public void setTerminalZip(Object terminalZip) {
			this.terminalZip = terminalZip;
		}
		public Object getTerminalState() {
			return terminalState;
		}
		public void setTerminalState(Object terminalState) {
			this.terminalState = terminalState;
		}
		public Object getTerminalCountry() {
			return terminalCountry;
		}
		public void setTerminalCountry(Object terminalCountry) {
			this.terminalCountry = terminalCountry;
		}
		public Object getRank() {
			return rank;
		}
		public void setRank(Object rank) {
			this.rank = rank;
		}
	}

	public List getPreferredAgentListByPartnerCode(String sessionCorpID,String partnerCode) {
		return getHibernateTemplate().find("from PreferredAgent where partnerCode='"+partnerCode+"' ");
	}
	
	public List findPreferredAgentsList(String partnerCode,String agentCode,String agentName,String status,String corpID,String agentGroup) {
		if(agentCode==null)agentCode="";
		if(agentName==null)agentName="";
		String query="from PreferredAgent where  (partnerCode ='"+partnerCode+"') ";
			   query=query+" and ( preferredAgentCode like '%"+agentCode.replaceAll("'", "''")+"%') ";
			   query=query+" and ( preferredAgentName like '%"+agentName.replaceAll("'", "''")+"%') ";
			   if(!agentGroup.equals("")){
			   query=query+" and  agentGroup= '"+agentGroup+"' ";
			   }
			   if(status.equals("Inactive")){
			   query=query+" and (status=false)";
			   }
			   if(status.equals("Active")){
				   query=query+" and (status=true)";
				   }
		return getHibernateTemplate().find(query);
	}
	public List findSearchAgentDetailsAutocompleteList(String searchName,String corpID,String fieldType,String partnerCode){
		List agentDetailReturnList=new ArrayList();
		String searchList="";
		searchName=searchName.replaceAll("'", "");
		if(fieldType.equals("agentName")){
			searchList=" lastName like'"+searchName+"%'";
		}
		if(fieldType.equals("agentCode")){
			searchList=" partnerCode like'"+partnerCode+"%'";
		}
		String query="";
		query="select distinct(id),firstName,lastName,aliasName,billingcountry,billingstate,billingcity,partnerCode,billingCountryCode,partnertype,terminalAddress1,terminalAddress2,terminalCity,terminalZip,terminalState,terminalCountry from partner where isAgent=true and status='Approved' and "+searchList+" and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') order by lastName limit 100 ";
		List chargeDetails =  this.getSession().createSQLQuery(query).list();
		agentDetailsList dto = null;
		Iterator it = chargeDetails.iterator();
		while (it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new agentDetailsList();
			dto.setId(row[0]);
			dto.setFirstName(row[1]);
			dto.setLastName(row[2]);
			dto.setAliasName(row[3]);
			dto.setBillingCountry(row[4]);
			dto.setBillingState(row[5]);
			dto.setBillingCity(row[6]);
			dto.setPartnerCode(row[7]);
			dto.setBillingCountryCode(row[8]);
			dto.setPartnerType(row[9]);
			dto.setTerminalAddress1(row[10]);
			dto.setTerminalAddress2(row[11]);
			dto.setTerminalCity(row[12]);
			dto.setTerminalZip(row[13]);
			dto.setTerminalState(row[14]);
			dto.setTerminalCountry(row[15]);
			agentDetailReturnList.add(dto);
		}
		return agentDetailReturnList;
	}
	
	public List findPartnerCode(String partnerCode,String corpid){
		List checkPartnerList=new ArrayList();
		try
		{
			checkPartnerList=this.getSession().createSQLQuery("select distinct concat(if(firstName is null or firstName='',' ',firstName),'#',lastName) from partner where partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') and isAgent=true and status='Approved'").list();
		}catch(Exception e){
			checkPartnerList=null;	
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	e.printStackTrace();
		}
		
		return checkPartnerList;
	}
	public List checkForPartnerCodeExists(String partnerCode,String corpid,String agentCode){
		List checkPartnerList=new ArrayList();
		try
		{
			checkPartnerList=this.getSession().createSQLQuery("select distinct concat(partnerCode,'#',preferredAgentCode,'#','Exists') from preferredagentforaccount where preferredAgentCode='"+agentCode+"' and corpID ='"+corpid+"' and partnercode='"+partnerCode+"'").list();
		}catch(Exception e){
			checkPartnerList=null;	
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	e.printStackTrace();
		}
		
		return checkPartnerList;
	}
	
	public List findAgentGroupList(String sessionCorpID){
		List agentGroupList=new ArrayList();
		try
		{
			String Query="select distinct agentGroup from partnerpublic where agentGroup is not null and status='Approved' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') order by agentGroup ";
			agentGroupList=this.getSession().createSQLQuery(Query).list();
		}catch(Exception e){
			agentGroupList=null;	
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	e.printStackTrace();
		}
		
		return agentGroupList;
	}
	
	public List getAgentGroupDataList(String sessionCorpID,String agentGroup){
				List agentGroupId = this.getSession().createSQLQuery(" select id from partnerpublic where agentGroup='"+agentGroup+"' and status='Approved' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')").list();
				return agentGroupId;
	}
	

	public HibernateUtil getHibernateUtil() {
		return hibernateUtil;
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	
	public void updateAgentStatus(Long preIdNum, Boolean status){
		String query="";
		try {
		    query = "update preferredagentforaccount set status="+status+" where id="+preIdNum;
			this.getSession().createSQLQuery(query).executeUpdate();
		} catch (Exception e) {}
	}
	
	 

}	