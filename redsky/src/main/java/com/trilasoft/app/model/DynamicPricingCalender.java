package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="dynamicpricingcalender")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DynamicPricingCalender extends BaseObject{
	private Long id;
	private String corpId;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private Date currentDate;
	private String menRate2;
	private String menRate3;
	private String menRate4;
	private String extraManRate;
	private String dynamicPricingType;
	private Integer noOfMen=0;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn).append("menRate2", menRate2)
				.append("menRate3", menRate3).append("menRate4", menRate4)
				.append("extraManRate", extraManRate).append("currentDate",currentDate).append("dynamicPricingType",dynamicPricingType).append("noOfMen",noOfMen).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DynamicPricingCalender))
			return false;
		DynamicPricingCalender castOther = (DynamicPricingCalender) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(menRate2, castOther.menRate2)
				.append(menRate3, castOther.menRate3)
				.append(menRate4, castOther.menRate4)
				.append(extraManRate, castOther.extraManRate).append(currentDate, castOther.currentDate)
				.append(dynamicPricingType, castOther.dynamicPricingType)
				.append(noOfMen, castOther.noOfMen).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(menRate2).append(menRate3)
				.append(menRate4).append(extraManRate).append(currentDate)
				.append(dynamicPricingType)
				.append(noOfMen).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getMenRate2() {
		return menRate2;
	}
	public void setMenRate2(String menRate2) {
		this.menRate2 = menRate2;
	}
	@Column
	public String getMenRate3() {
		return menRate3;
	}
	public void setMenRate3(String menRate3) {
		this.menRate3 = menRate3;
	}
	@Column
	public String getMenRate4() {
		return menRate4;
	}
	public void setMenRate4(String menRate4) {
		this.menRate4 = menRate4;
	}
	@Column
	public String getExtraManRate() {
		return extraManRate;
	}
	public void setExtraManRate(String extraManRate) {
		this.extraManRate = extraManRate;
	}
	@Column
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	@Column
	public String getDynamicPricingType() {
		return dynamicPricingType;
	}
	public void setDynamicPricingType(String dynamicPricingType) {
		this.dynamicPricingType = dynamicPricingType;
	}
	@Column
	public Integer getNoOfMen() {
		return noOfMen;
	}
	public void setNoOfMen(Integer noOfMen) {
		this.noOfMen = noOfMen;
	}
}
