package com.trilasoft.app.webapp.action;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.trilasoft.app.model.Port;
import com.trilasoft.app.service.PortManager;
import com.trilasoft.app.service.RefMasterManager;

public class PortAction extends BaseAction{
	private PortManager portManager;
	private List ports;
	private Port port;
	private Long id;
	private String sessionCorpID;
	private RefMasterManager refMasterManager;
	private static List mode;
	private static Map<String, String> ocountry;
	private List portList;
	private List countryCode;
	private String portCode;
	private String portName;
	private String active;
	private String country;
	private String modeType;
	private String pMode;
	private String hitFlag="";
	private String Status;
	private List portNameList;
	// private List day;

	public PortAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	
	public List getPorts() {
		return ports;
	}

	public String list() {
		getComboList(sessionCorpID);
		port = new Port();
		port.setActive(true);
		ports = portManager.portListByCorpId(sessionCorpID);
		return SUCCESS;
	}
	/*public String portModelist() {
		portList = portManager.findPortByMode(pMode);
		return SUCCESS;
	}*/
	public void setId(Long id) {
		this.id = id;
	}

	public Port getPort() {
		return port;
	}
	@SkipValidation
    public String searchPortList() {
		if(port != null){
			if(port.getActive() == true){
				active ="true";
			}else{
				active ="false";
			}
		}
		getComboList(sessionCorpID);
    	ports=portManager.searchByPort(portCode, portName, country, modeType, sessionCorpID, active);
        return SUCCESS;   
    }
	@SkipValidation
    public String  searchHaulingPortList(){
		getComboList(sessionCorpID);
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		ports=portManager.searchByPortCountry(portCode, portName, country, modeType, sessionCorpID);
    	 return SUCCESS; 
    	
    }
    
    
	public String getComboList(String corpID){
	 	 ocountry = refMasterManager.findCountry(corpID, "COUNTRY");
	 	 mode = refMasterManager.findByParameters(corpID, "MODE");
	 	 if(mode !=null && mode.size()>1){
	 		 
	 	 }else{
	 		mode.add("Air");
	 		mode.add("Sea");
	 	 }
	 	 return SUCCESS;
	}
//method for country code
	@SkipValidation
	public String portCountryCode() {
				countryCode = refMasterManager.findCountryCode(country, sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	public String findPortName(){
		portNameList=portManager.getPortName(country,sessionCorpID,portCode);
		return SUCCESS;	
	}
	
//  ****for auto save******
    private String gotoPageString;
    private String validateFormNav;
    public String getValidateFormNav() {

        return validateFormNav;

        }

        public void setValidateFormNav(String validateFormNav) {

        this.validateFormNav = validateFormNav;

        }
    public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }
    public String saveOnTabChange() throws Exception {
        //if (option enabled for this company in the system parameters table then call save) {
    		validateFormNav = "OK";
            String s = save();    // else simply navigate to the requested page)
            return gotoPageString;
    }
////////********************************************
	
	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			port = portManager.get(id);
		} else {
			port = new Port();
			port.setActive(true);
			port.setIsCargoAirport(true);
			port.setCreatedOn(new Date());
			port.setUpdatedOn(new Date());
		}
		
        //port.setUpdatedOn(new Date());
		return SUCCESS;
	}

	public String save() throws Exception {
		/*if (cancel != null) {
			return "cancel";
		}

		if (delete != null) {
			return delete();
		}*/

		boolean isNew = (port.getId() == null);
		
		List isexisted=portManager.portExisted(port.getPortCode(), sessionCorpID);
			if(!isexisted.isEmpty() && isNew){
				errorMessage("PortCode is already exist. Please Enter another");
				return INPUT; 
			}else{
				if (isNew) { 
					port.setCreatedOn(new Date());
		        } 
			port.setCorpID(sessionCorpID);
			port.setUpdatedOn(new Date());
		     port.setUpdatedBy(getRequest().getRemoteUser());
	          portManager.save(port);
	        
	        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "port.added" : "port.updated";
			saveMessage(getText(key));
	        }
	       	if (!isNew) {
				//oldCreatedOn = port.getCreatedOn();
	       		hitFlag="1";
				return "PortDetail";
			} else {
				Long i = Long.parseLong(portManager.findMaximumId().get(0).toString());
				port = portManager.get(i);
				hitFlag="1";
				return SUCCESS;
			}
		}	
	}

	@SkipValidation
	public String updatePortActiveStatus() {
		portManager.updatePortActiveStatus(Status, id,getRequest().getRemoteUser());
		return SUCCESS;
	}
	
	
	public String readXML() throws IOException, SAXException{
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			URL url = new URL("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
			InputStream stream = url.openStream();
			Document doc = docBuilder.parse(stream);
			doc.getDocumentElement ().normalize ();
			System.out.println ("Root element of the doc is " +
		                 doc.getDocumentElement().getNodeName());
			NodeList listOfPersons = doc.getElementsByTagName("Cube");
			int totalPersons = listOfPersons.getLength();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	public void setPortManager(PortManager portManager) {
		this.portManager = portManager;
	}


	public void setPorts(List ports) {
		this.ports = ports;
	}


	public void setPort(Port port) {
		this.port = port;
	}


	public static List getMode() {
		return mode;
	}


	public static void setMode(List mode) {
		PortAction.mode = mode;
	}


	public static Map<String, String> getOcountry() {
		return ocountry;
	}


	public static void setOcountry(Map<String, String> ocountry) {
		PortAction.ocountry = ocountry;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}


	public List getPortList() {
		return portList;
	}


	public void setPortList(List portList) {
		this.portList = portList;
	}


	public String getPortCode() {
		return portCode;
	}


	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}


	public String getPortName() {
		return portName;
	}


	public void setPortName(String portName) {
		this.portName = portName;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getModeType() {
		return modeType;
	}


	public void setModeType(String modeType) {
		this.modeType = modeType;
	}


	public String getPMode() {
		return pMode;
	}


	public void setPMode(String mode) {
		pMode = mode;
	}


	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}


	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}


	/**
	 * @return the countryCode
	 */
	public List getCountryCode() {
		return countryCode;
	}


	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(List countryCode) {
		this.countryCode = countryCode;
	}


	/**
	 * @return the active
	 */
	public String getActive() {
		return active;
	}


	/**
	 * @param active the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}


	public List getPortNameList() {
		return portNameList;
	}


	public void setPortNameList(List portNameList) {
		this.portNameList = portNameList;
	}

}
