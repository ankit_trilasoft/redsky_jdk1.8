package com.trilasoft.app.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="partnerbankinformation")


public class PartnerBankInformation extends BaseObject{
	private String corpId; 
	private Long   id;
	private String updatedBy;
	private Date   updatedOn;
	private String createdBy;
	private Date   createdOn;
	private String partnerCode;
	private String currency;
	private String bankAccountNumber;
	private Boolean status=true;
	
	
	
	

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpId", corpId)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("partnerCode", partnerCode)
				.append("currency", currency)
				.append("bankAccountNumber", bankAccountNumber)
				.append("status", status)
				.toString();
	}
		
		
		@Override
		public boolean equals(final Object other) {
			PartnerBankInformation castOther = (PartnerBankInformation) other;
			return new EqualsBuilder()
			.append(id, castOther.id)
			.append(corpId, castOther.corpId)
			.append(updatedBy, castOther.updatedBy)
			.append(updatedOn, castOther.updatedOn)
			.append(createdBy,castOther.createdBy)
			.append(createdOn,castOther.createdOn)
			.append(partnerCode,castOther.partnerCode)
			.append(currency,castOther.currency)
			.append(bankAccountNumber,castOther.bankAccountNumber)
			.append(status,castOther.status)
			.isEquals();
		}
		
		
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(corpId)
					.append(updatedBy).append(updatedOn)
					.append(createdBy).append(createdOn)
					.append(partnerCode).append(currency).append(bankAccountNumber).append(status)
					.toHashCode();
		}


		
		@Column
		public String getCorpId() {
			return corpId;
		}


		public void setCorpId(String corpId) {
			this.corpId = corpId;
		}

		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}

		@Column
		public String getUpdatedBy() {
			return updatedBy;
		}


		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		@Column
		public Date getUpdatedOn() {
			return updatedOn;
		}


		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}

		@Column  
		public String getCreatedBy() {
			return createdBy;
		}


		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		@Column
		public Date getCreatedOn() {
			return createdOn;
		}


		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		@Column
		public String getPartnerCode() {
			return partnerCode;
		}


		public void setPartnerCode(String partnerCode) {
			this.partnerCode = partnerCode;
		}

		@Column
		public String getCurrency() {
			return currency;
		}


		public void setCurrency(String currency) {
			this.currency = currency;
		}

		@Column
		public String getBankAccountNumber() {
			return bankAccountNumber;
		}


		public void setBankAccountNumber(String bankAccountNumber) {
			this.bankAccountNumber = bankAccountNumber;
		}

		@Column
		public Boolean getStatus() {
			return status;
		}


		public void setStatus(Boolean status) {
			this.status = status;
		}

	
	
	
	
	
	
	
	

}

