
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValidateLoginResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validateLoginResult"
})
@XmlRootElement(name = "ValidateLoginResponse")
public class ValidateLoginResponse {

    @XmlElement(name = "ValidateLoginResult")
    protected boolean validateLoginResult;

    /**
     * Gets the value of the validateLoginResult property.
     * 
     */
    public boolean isValidateLoginResult() {
        return validateLoginResult;
    }

    /**
     * Sets the value of the validateLoginResult property.
     * 
     */
    public void setValidateLoginResult(boolean value) {
        this.validateLoginResult = value;
    }

}
