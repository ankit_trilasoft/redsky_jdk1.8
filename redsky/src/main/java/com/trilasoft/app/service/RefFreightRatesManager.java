package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;


import com.trilasoft.app.model.RefFreightRates;

public interface RefFreightRatesManager extends GenericManager<RefFreightRates, Long>{

	public 	String saveData(String sessionCorpID, String file, String userName);

	public List getExistingContracts(String sessionCorpID);

	public List getMissingPort(String sessionCorpID);

}
