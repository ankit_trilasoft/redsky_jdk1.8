<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="workTicketList.title" /></title>
<meta name="heading" content="<fmt:message key='workTicketList.heading'/>" />
<style type="text/css">
div.exportlinks {margin:-5px 0px 10px 10px;	padding:2px 4px 2px 0px;!padding:2px 4px 18px 0px;	width:100%;	}
form {margin-top:0px;!margin-top:-5px;}
div#main {margin:-5px 0 0;}
.table td, .table th, .tableHeaderTable td {padding: 0.4em;}
.table th.alignCenter a {text-align: center;}
#overlay11 {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png); }
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:98%;}
div.error, span.error, li.error, div.message {width:450px;}
</style>   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['searchForm'].elements['workTicket.ticket'].value = "";
			document.forms['searchForm'].elements['workTicket.shipNumber'].value = "";
			document.forms['searchForm'].elements['workTicket.lastName'].value = "";
			document.forms['searchForm'].elements['workTicket.firstName'].value = "";
			document.forms['searchForm'].elements['workTicket.targetActual'].value = "";
			document.forms['searchForm'].elements['workTicket.date1'].value = "";
			document.forms['searchForm'].elements['workTicket.date2'].value = "";
			document.forms['searchForm'].elements['workTicket.service'].value = "";
			document.forms['searchForm'].elements['workTicket.warehouse'].value = "";
			document.forms['searchForm'].elements['hubId'].value = "";
			document.forms['searchForm'].elements['workTicket.bondedGoods'].value = "";
			
}
</script>

<SCRIPT LANGUAGE="JavaScript">
function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
}

function isNumeric()
	{   
	
	//var ticket = document.forms['searchForm'].elements['workTicket.ticket'].value;
	//var serviceOrder = document.forms['searchForm'].elements['workTicket.shipNumber'].value;
	//var lastName = document.forms['searchForm'].elements['workTicket.lastName'].value;
	//var firstName = document.forms['searchForm'].elements['workTicket.firstName'].value;
	//var ticketStatus = document.forms['searchForm'].elements['workTicket.targetActual'].value;
	var beginDate = document.forms['searchForm'].elements['workTicket.date1'].value;
	var endDate = document.forms['searchForm'].elements['workTicket.date2'].value;
	//var service = document.forms['searchForm'].elements['workTicket.service'].value;
	//var wareHouse = document.forms['searchForm'].elements['workTicket.warehouse'].value;
	
	
	var i;
	    var s = document.forms['searchForm'].elements['workTicket.ticket'].value;
	    var shipNumber = document.forms['searchForm'].elements['workTicket.shipNumber'].value;
	    
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid ticket number");
	        document.forms['searchForm'].elements['workTicket.ticket'].select();
	        
	        return false;
	        }
	    }
	    
	    if(s=='' && shipNumber==''){
		    if(beginDate==''){
		    	alert('Please select Begin Date ');
		    	return false;
		    }
		    
		    if(beginDate!='' && endDate=='')
		    {
		    	alert('Please select the End Date');
		    	return false;
		    } 
		   return true; 
		}
	    
	    return true;
	    
	}
	
function findBillCompleteDate()
	{
	var shipNumber = document.forms['searchForm'].elements['shipnum'].value;
	var url="billComplete.html?ajax=1&decorator=simple&popup=true&shipnum="+ encodeURI(shipNumber);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse1;
     http2.send(null); 
     }
     
  function handleHttpResponse1()
     {
      var id = document.forms['searchForm'].elements['fileID'].value;

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                
                if((res[0]=='A' && res[1]!='1') || res[2]!='1' || res[3]!='1')
                {
                	alert('No more tickets can be added as Billing is Complete.');
                	return false;
                }else
                {	
                	location.href="editWorkTicket.html?id="+id;
                	return true;
                }
    }
    
    }

var http2 = getHTTPObject();    
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function copyDate1Date2()
{
	document.forms['searchForm'].elements['date2'].value = document.forms['searchForm'].elements['date1'].value;
}
function focusDate(target)
{	
	document.forms['searchForm'].elements[target].focus();
}
	
</SCRIPT>
<script language="javascript" type="text/javascript">
function searchWorkTicketsforDays(){
 document.forms['searchForm'].elements['checkSearchWorkTicketsClick'].value = '1';
}
	
function searchWorkTicketsDays()
{ 
 var date1 = document.forms['searchForm'].elements['date1'].value;	 
 var date2 = document.forms['searchForm'].elements['date2'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("End Date should be greater than Begin Date.");
    document.forms['searchForm'].elements['workTicket.date2'].value='';
  } 
  else{
  	if(document.forms['searchForm'].elements['workTicket.date2'].value == ''){
  		document.forms['searchForm'].elements['workTicket.date2'].value = document.forms['searchForm'].elements['workTicket.date1'].value;
  	}
  }
 	document.forms['searchForm'].elements['checkSearchWorkTicketsClick'].value = '';
}

function findWorkTicketForms(ticket,jobNumber,companyDivision,jobType,modes,billToCode,position){
	var url="reportBySubModuleWorkTicket.html?ajax=1&decorator=simple&popup=true&ticket=" + encodeURI(ticket)+"&id="+ticket+"&jobNumber="+jobNumber+"&companyDivision="+companyDivision+"&jobType="+jobType+"&modes="+modes+"&billToCode="+billToCode+"&reportModule=workTicket&reportSubModule=workTicket";
	ajax_showTooltip(url,position);	
	
}
function findContractPackers(ticket,position){
	var url="crewNameByContractPackers.html?ajax=1&decorator=simple&popup=true&ticket=" + encodeURI(ticket);
	ajax_showTooltip(url,position);	
	
}
function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer){
	window.open('viewFormParam.html?id='+id+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID='+invNum+'&custID=${custID}&reportModule=workTicket&reportSubModule==workTicket&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
	//&reportName=${reportsList.description}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&
	//window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
}
//  created this method as per Bug 6535 
function findToolTipService(code,position){
	var code1=code+"";
	var url="findToolTipService.html?ajax=1&decorator=simple&popup=true&code="+code1;
	ajax_showTooltip(url,position);	
	
}
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
function generatePrintPackage(ticket){
	var printFlag = "workTicketList";
	 var url="generatePrintPackage.html?ticket="+encodeURI(ticket)+"&printFlag="+printFlag+"";
	 showOrHide(1);
	 location.href=url;
	 setTimeout("showOrHide(0)",2500);
}
function validatefields(id,invNum,jobNumber,reportName,docsxfer,jobType,val){
	var url='viewReportWithParam.html?formFrom=list&id='+id+'&noteID='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=workTicket&reportSubModule=workTicket&reportName='+reportName+'&docsxfer='+docsxfer+'';
	location.href=url;	  
}
function winClose(id,invNum,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal){
	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&invoiceNumber="+invNum+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&custID="+custID;
	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
}
</script>
<script type="text/javascript">
function dispatchMap(){
var date1 = document.forms['searchForm'].elements['workTicket.date1'].value;
var date2 = document.forms['searchForm'].elements['workTicket.date2'].value;
var hubId=document.forms['searchForm'].elements['hubId'].value;
	if((date1 != '') && (date2 != '')){
		if(date1 == date2){
			window.open("dispathMapWorkTicket.html?date1="+date1+"&date2="+date2+"&hubId="+hubId+"&decorator=popup&popup=true&from=main","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
		}else{
			alert('Begin date and end date should be equal to view dispatch map.');
			return false;
		}
	}else{
		alert('Begin date and end date should be equal to view dispatch map.');
		return false;
	}
}

function openStorage(){
	var date1 = document.forms['searchForm'].elements['workTicket.date1'].value;
	var date2 = document.forms['searchForm'].elements['workTicket.date2'].value;
	var house = document.forms['searchForm'].elements['workTicket.warehouse'].value;	
	if(date1==''){
	    	alert('Please select Begin Date for Storage');
	    	return false;
	    }
	    
	if(date1!='' && date2=='')
	    {
	    	alert('Please select the End Date for Storage');
	    	return false;
	    } 
    window.open("workTicketStorages.html?date1="+date1+"&date2="+date2+"&warehouse="+house+"&decorator=popup&popup=true","forms","height=600,width=800,top=5, left=100, scrollbars=yes,resizable=yes");
   
}

function onlyDelNoQuotes(evt,targetElement) 
	{
   		var keyCode = evt.which ? evt.which : evt.keyCode;
	  if(keyCode==46 || keyCode==8){
	  		targetElement.value = '';
	  }else{
	 return false;
	  	
	  }
   }

function checkWarehouse(){
	var house1= document.forms['searchForm'].elements['workTicket.warehouse'].value;
	document.forms['searchForm'].elements['warehouseValue'].value=house1;
	return true;
	}
</script>
</head> 


<s:form id="searchForm" action="searchWorkTickets" method="post">
<s:hidden name="ticketSortOrder"   value="${ticketSortOrder}"/>
<c:set var="ticketSortOrder"  value="${ticketSortOrder}"/>

<s:hidden name="orderForTicket"   value="${orderForTicket}"/>
<c:set var="orderForTicket"  value="${orderForTicket}"/>

<div id="Layer1" style="width:100%;margin-top:-30px;!margin-top:-10px;">
<c:set var="shipnum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden id="checkSearchWorkTicketsClick" name="checkSearchWorkTicketsClick"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="shipnum" value="${shipnum}"/>
 	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <s:hidden name="warehouseValue"/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${not empty serviceOrder.id}">
<div id="newmnav">
		  
		  
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			  	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>			 
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			       <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		       </sec-auth:authComponent>
		       <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		       <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		       </sec-auth:authComponent>
		       <c:if test="${forwardingTabVal!='Y'}">  
			  		<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  </c:if>
			  <c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>			  
			  
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			    <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  					<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}"><span>Critical Dates</span></a></li>
				</sec-auth:authComponent>
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Ticket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  
			</ul>
		</div>
		<div class="spn">&nbsp;</div>	
		<%@ include file="/WEB-INF/pages/trans/workTicketHeader.jsp"%>
</c:if>
<c:if test="${empty serviceOrder.id}">
	<div id="otabs" >
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top: -5px;"><span></span></div>
    <div class="center-content">
		
	    <table class="table" cellspacing="0" cellpadding="0" style="width:100%">
		<thead>
			<tr>
			<th><fmt:message key="workTicket.ticket" /></th>
			<th><fmt:message key="workTicket.shipNumber" /></th>
			<th><fmt:message key="workTicket.lastName" /></th>
			<th><fmt:message key="workTicket.firstName" /></th>
			<th>Ticket&nbsp;Status</th>
			<th><fmt:message key="workTicket.date1" /></th>
			<th><fmt:message key="workTicket.date2" /></th>
			<th><fmt:message key="workTicket.service" /></th>			
			<th colspan=2><fmt:message key="workTicket.warehouse" /></th>
			
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><s:textfield id="workTicketClickStatusCount" name="workTicket.ticket" cssClass="input-text" cssStyle="width:125px;" onblur="searchWorkTicketsDays();" /></td>
				<td><s:textfield name="workTicket.shipNumber" required="true" cssClass="input-text" cssStyle="width:125px;"/></td>
				<td><s:textfield name="workTicket.lastName" required="true" cssClass="input-text" cssStyle="width:125px;" onfocus="myDate();"/></td>
				<td><s:textfield name="workTicket.firstName" required="true" cssClass="input-text" cssStyle="width:125px;"/></td>
				<td><s:select name="workTicket.targetActual" list="%{tcktactn}" cssStyle="width:125px;" cssClass="list-menu" headerKey="" headerValue="" onchange="autoPopulate_customerFile_statusDate(this);" tabindex="1" /></td>
				
				
				<c:if test="${not empty workTicket.date1}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="workTicket.date1" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="date1" name="workTicket.date1" value="%{customerFiledate1FormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true" onselect="searchWorkTicketsDays();"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchWorkTicketsforDays();"/></td>
				</c:if>
				<c:if test="${empty workTicket.date1}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="date1" name="workTicket.date1" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDelNoQuotes(event,this)" onselect="searchWorkTicketsDays();"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchWorkTicketsforDays();"/></td>
				</c:if>
				<c:if test="${not empty workTicket.date2}">
				<s:text id="customerFiledate2FormattedValue" name="${FormDateValue}"><s:param name="value" value="workTicket.date2" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="date2" name="workTicket.date2" value="%{customerFiledate2FormattedValue}" cssStyle="width:65px;" maxlength="11" readonly="true" onkeydown="return onlyDelNoQuotes(event,this)" onblur="" onselect="searchWorkTicketsDays();"/>
				<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchWorkTicketsforDays();"/></td>
				</c:if>
				<c:if test="${empty workTicket.date2}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="date2" name="workTicket.date2" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onblur="" onkeydown="return onlyDelNoQuotes(event,this)" onselect="searchWorkTicketsDays();"/>
				<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchWorkTicketsforDays();"/></td>
				</c:if>
				
				
				<td><s:select name="workTicket.service" list="%{tcktservc}" cssStyle="width:125px; !width:130px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
				<td colspan=2><s:select name="workTicket.warehouse" list="%{house}" value="%{dfltWhse}"  cssStyle="width:135px; !width:125px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
			</tr>
			
			<tr>			
			<td colspan="11" style="margin:0;padding:0;border:1px solid #FFFFFF;"  class="listwhitetext">
			<table style="margin:0px;padding:0px;border:none; float:right;" class="listwhitetext">
				<tr>
				<td style="border:none;" class="listwhitetext">
			Bonded&nbsp;Goods<br>
			<s:select name="workTicket.bondedGoods" list="%{yesno}" cssStyle="width:72px;margin-top:2px;margin-right:8px;" cssClass="list-menu" onchange="changeStatus();" />
			</td>
			<configByCorp:fieldVisibility componentId="component.tab.workTicket.HubIdDisplaySscw">
			<td style="border:none;" class="listwhitetext">
          	HUB<br>
          	<s:select name="hubId" list="%{opshub}" cssStyle="width:85px;margin-top:2px;margin-right:8px;" cssClass="list-menu" headerKey="" headerValue="" />
			</td>
			</configByCorp:fieldVisibility>
			</td>
			<td style="border:none;" class="listwhitetext">
					Search Options<br>
			<s:select name="serviceOrderSearchVal" list="%{serviceOrderSearchType}" cssClass="list-menu" cssStyle="margin-top:2px;margin-right:8px;"/>
				</td>
			<td style="border:none;vertical-align:bottom;width:204px;" class="listwhitetext rbrSF">
			<s:submit cssClass="cssbutton" method="search" cssStyle="width:55px; height:25px" key="button.search" onclick="return checkWarehouse(), isNumeric();"/>
		    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/>
		    </td> 
			</tr>
			</table>
			</td>
			</tr>
		</tbody>		
	</table>
	
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

</c:if>

<div style=" width:100%;overflow-x:auto;margin-top:-8px; ">

	<table width="100%" border="0" style="margin:0px" cellpadding="0" cellspacing="0">
		<tr>
			<td id="otabs" style="width:3%;!width:10%;" align="left"> 
				<ul><li><a class="current"><span>Work Ticket List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li></ul>
			</td>
			<td id="otabs" style="width:3%;!width:6%;" align="left"> 
				<ul><li><a onclick="openStorage()"><span>Storage<img src="images/navarrow.gif" align="absmiddle" /></span></a></li></ul>
			</td>
 <configByCorp:fieldVisibility componentId="component.workticketlist.map.WorkTicketDispatchMap">
			<c:if test="${showMap == 1}">
				<td id="otabs" width="10%" align="left"> 
					<ul><li><a style="cursor: pointer;" onclick="return dispatchMap();"><span>Dispatch Map</span></a></li></ul>
				</td>
			</c:if>
		</configByCorp:fieldVisibility>	
			<td class="key_workticket" style="width:70%;!width:60%;">&nbsp;</td>
		</tr>
	</table>
				
			
	<c:if test="${empty serviceOrder.id}">	
	<s:set name="workTicketsExt" value="workTicketsExt" scope="request" />
	<display:table name="workTicketsExt" class="table" requestURI="" id="workTicketList" export="true"  pagesize="50"  style="margin-top:0px;!margin-top:0px;">
		<display:column style="width:5px" sortable="true" title="ST">
			<img id="target" src="${pageContext.request.contextPath}/images/${workTicketList.targetActual}.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		</display:column>
		<display:column sortable="true" style="text-align:right;width:5px;" title="Pt"><img align="top" title="Forms" onclick="findWorkTicketForms('${workTicketList.ticket}','${workTicketList.shipNumber}','${workTicketList.companyDivision}','${workTicketList.jobType}','${workTicketList.jobMode}','${workTicketList.account}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></display:column>
		<display:column property="date1" sortable="true" titleKey="workTicket.date1" style="" format="{0,date,dd-MMM-yy}" url="/editWorkTicketUpdate.html" paramId="id" paramProperty="id"/>
		<display:column property="date2" sortable="true" titleKey="workTicket.date2" style="" format="{0,date,dd-MMM-yy}"/>
		<display:column property="jobType" sortable="true" titleKey="workTicket.jobType" />
		<display:column sortable="true" titleKey="workTicket.service" >
		   <div align="left" onmouseover="findToolTipService('${workTicketList.service}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.service}" /></div>
		</display:column>
		<c:choose>
		<c:when test="${workTicketList.crews == '0'}">
		<display:column sortable="true" style="width:50px;text-align:right;" title="Whse"><c:out value="${workTicketList.warehouse}"></c:out>
		</display:column>
		</c:when>		
		<c:otherwise>
		<display:column sortable="true" style="width:50px;text-align:right;" title="Whse">
		<div style="width:100%" onmouseover="findContractPackers('${workTicketList.ticket}',this);" onmouseout="ajax_hideTooltip();" >
		<c:out value="${workTicketList.warehouse}"></c:out>
		<img id="target" src="${pageContext.request.contextPath}/images/user-v1.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		</div>
		</display:column>
		</c:otherwise>
		</c:choose>				
		<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">			
		<display:column title="Est. Weight" sortable="true" style="text-align:right;">			    	
				   <c:out value="${workTicketList.estimatedWeight}" />&nbsp;&nbsp;${workTicketList.unit1}			   
		</display:column>
		<display:column property="estimatedCartoons" sortable="true" title="# of Cartons" style="text-align:right;"/>
		<display:column title="Act. Weight" sortable="true" style="text-align:right;">			
					<c:out value="${workTicketList.actualWeight}" />&nbsp;&nbsp;${workTicketList.unit1}			  
		</display:column>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">	
		<display:column  sortable="true" title="Est. Volume" headerClass="containeralign" style="text-align:right;"><fmt:formatNumber  value="${workTicketList.estimatedCubicFeet}" /></display:column>
		<display:column  sortable="true" title="Act. Volume" nulls="true" headerClass="containeralign" style="text-align:right;"><fmt:formatNumber  value="${workTicketList.actualVolume}" /></display:column>
        </configByCorp:fieldVisibility>	
        <display:column property="lastName" sortable="true" titleKey="workTicket.lastName" style="width:10px" maxLength="12"/>	
		<display:column property="firstName" sortable="true" titleKey="workTicket.firstName" />
		<display:column property="jobMode" sortable="true" titleKey="workTicket.jobMode" />
		<display:column property="description" sortable="true" title="Description" />
		<display:column property="city" sortable="true" titleKey="workTicket.city" />
		<display:column property="destinationCity" sortable="true" titleKey="workTicket.destinationCity" />
		<display:column property="scheduledArrive" sortable="true" title="Arrvl"  />		                                                                        
		<display:column property="shipNumber" sortable="true" titleKey="workTicket.shipNumber"  />
		<display:column property="registrationNumber" sortable="true" title="Reg#"  />
		<display:column  sortable="true" titleKey="workTicket.ticket" > 
			${workTicketList.ticket }
			<c:if test= "${mmValidation =='Yes' }" >
				<c:if test="${workTicketList.orderId ne null && workTicketList.orderId!='' }">
						<img id="mmIntegration" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/MM_12.png"  width="23" height="9"/>
				</c:if>
			</c:if>
		</display:column>
		<configByCorp:fieldVisibility componentId="component.button.workTicket.printPackage">
		<display:column sortable="true" style="text-align:right;width:5px;"
			title="Pkg">
			<img align="top" title="Forms"
				onclick="generatePrintPackage('${workTicketList.id}');"
				src="${pageContext.request.contextPath}/images/printer-icon-16.png" />
		</display:column>
		</configByCorp:fieldVisibility>	
		<display:column sortable="true" title="Billed Status" headerClass="alignCenter" >
		<c:if test="${workTicketList.reviewStatus=='Billed' }">
			<div ALIGN="center">
				<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
		<c:if test="${workTicketList.reviewStatus=='UnBilled' }">
			<div ALIGN="center">
				<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
		</display:column>
		<display:column property="createdBy" sortable="true" title="Created&nbsp;By"  />
		<display:column property="crews" sortable="true" title="# of crews" /> 
		<display:setProperty name="paging.banner.item_name" value="workticket" />
		<display:setProperty name="paging.banner.items_name" value="workTickets" />
		<display:setProperty name="paging.banner.placement" value="both" />
		<display:setProperty name="export.excel.filename" value="WorkTicket List.xls" />
		<display:setProperty name="export.csv.filename" value="WorkTicket List.csv" />
		<display:setProperty name="export.pdf.filename" value="WorkTicket List.pdf" />
	</display:table>
</div>
</c:if>


<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="formStatus" value=""/>
<c:if test="${not empty serviceOrder.shipNumber}">
<input type="button" class="cssbutton" onclick = "return findBillCompleteDate();" style="width:55px; height:25px; margin-top:25px"  
	          
	        value="<fmt:message key="button.add"/>"/>
	        
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</c:if>
<c:if test="${empty serviceOrder.shipNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>

<table><tr>
	<td></td></tr>
	</table>
	</div>
	<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait<br>Print Package PDF is being populated!...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
</s:form>
<script type="text/javascript">
	setOnSelectBasedMethods(["searchWorkTicketsDays()"]);
	setCalendarFunctionality();
	showOrHide(0);
</script>
<script type="text/javascript"> 
document.forms['searchForm'].elements['workTicket.ticket'].focus(); 
    highlightTableRows("workTicketList");
     Form.focusFirstElement($("searchForm")); 
</script>
<script type="text/javascript">

try{
	  var wValue1=document.forms['searchForm'].elements['warehouseValue'].value;
	  if(wValue1!=null && wValue1!=''){
	    document.forms['searchForm'].elements['workTicket.warehouse'].value=document.forms['searchForm'].elements['warehouseValue'].value;
	  }
	}catch(e){}

</script>

