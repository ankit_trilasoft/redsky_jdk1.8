package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.StorageBillingMonitorDao;
import com.trilasoft.app.model.StorageBillingMonitor;
import com.trilasoft.app.service.StorageBillingMonitorManager;

public class StorageBillingMonitorManagerImpl extends GenericManagerImpl<StorageBillingMonitor, Long> implements StorageBillingMonitorManager{
	StorageBillingMonitorDao storageBillingMonitorDao;
	public StorageBillingMonitorManagerImpl(StorageBillingMonitorDao storageBillingMonitorDao){
		super(storageBillingMonitorDao);
		this.storageBillingMonitorDao=storageBillingMonitorDao;
	}
	public List checkBillingFlag(String corpId){
		return storageBillingMonitorDao.checkBillingFlag(corpId);
	}
	public List getStorageBillingMonitorList(String corpId){
		return storageBillingMonitorDao.getStorageBillingMonitorList(corpId);
	}
}
