package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ControlExpirations;

public interface ControlExpirationsManager extends GenericManager<ControlExpirations, Long>{
	public List getDriverValidation(String driverCode,String corpId,String parentId);
}
