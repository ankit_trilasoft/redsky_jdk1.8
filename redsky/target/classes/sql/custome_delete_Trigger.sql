DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_custom BEFORE DELETE ON custom
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

  
      IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'custom.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;
  
   IF (OLD.movement <> '') THEN
       select CONCAT(fieldNameList,'custom.movement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.movement,'~') into oldValueList;
   END IF;
   IF (OLD.ticket <> '') THEN
       select CONCAT(fieldNameList,'custom.ticket~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ticket,'~') into oldValueList;
   END IF;
   IF (OLD.transactionId <> '') THEN
       select CONCAT(fieldNameList,'custom.transactionId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.transactionId,'~') into oldValueList;
   END IF;
   IF (OLD.status <> '') THEN
       select CONCAT(fieldNameList,'custom.status~') into fieldNameList;
       select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
   END IF;
   IF (OLD.documentType <> '') THEN
       select CONCAT(fieldNameList,'custom.documentType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentType,'~') into oldValueList;
   END IF;
   IF (OLD.documentRef <> '') THEN
       select CONCAT(fieldNameList,'custom.documentRef~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentRef,'~') into oldValueList;
   END IF;
   IF (OLD.goods <> '') THEN
       select CONCAT(fieldNameList,'custom.goods~') into fieldNameList;
       select CONCAT(oldValueList,OLD.goods,'~') into oldValueList;
   END IF;
   IF (OLD.entryDate <> '') THEN
       select CONCAT(fieldNameList,'custom.entryDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.entryDate,'~') into oldValueList;
   END IF;
   IF (OLD.volumeUnit <> '') THEN
       select CONCAT(fieldNameList,'custom.volumeUnit~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volumeUnit,'~') into oldValueList;
   END IF;
   IF (OLD.weightUnit <> '') THEN
       select CONCAT(fieldNameList,'custom.weightUnit~') into fieldNameList;
       select CONCAT(oldValueList,OLD.weightUnit,'~') into oldValueList;
   END IF;
   IF (OLD.volume <> '') THEN
       select CONCAT(fieldNameList,'custom.volume~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
   END IF;
   IF (OLD.weight <> '') THEN
       select CONCAT(fieldNameList,'custom.weight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.weight,'~') into oldValueList;
   END IF;
   IF (OLD.pieces <> '') THEN
       select CONCAT(fieldNameList,'custom.pieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
   END IF;
   IF (OLD.serviceOrderId <> '') THEN
       select CONCAT(fieldNameList,'custom.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;
   IF (OLD.networkId <> '') THEN
       select CONCAT(fieldNameList,'custom.networkId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkId,'~') into oldValueList;
   END IF;
  
   CALL add_tblHistory (OLD.serviceOrderId,"custom", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$