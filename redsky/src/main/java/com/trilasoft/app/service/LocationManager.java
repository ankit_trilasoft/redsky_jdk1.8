package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.Location;

public interface LocationManager extends GenericManager<Location, Long> {
	
	public List<Location> findByLocation(String locationId, String type,String occupied,String capacity,String corpId);
	
	public List<Location> findByLocationWH(String locationId, String occupied, String type, String warehouse);
	
	public List getOccupiedList();
	
	public Location getByLocation(String locationId);
	
	public List locationList(String corpId);

	public List findByLocationWHMove(String locationId, String occupied, String type, String warehouse);

	public List<Location> findByLocationWithWarehouse(String locationId, String type,String occupieType, String capacity, String sessionCorpID,
			String warehouse);

	public List findLocationList(String sessionCorpID, String whouse, String code);
	
	public List searchLocationForStorage(String locationId, String type,String sessionCorpID, String warehouse);
	public List<Location> findByLocationRearrList(String sessionCorpID,String locationId,String type, String warehouse);
	public List<Location> findAllLocationId(String sessionCorpID);
	public List workTicketByLocation(String locationId);
}
