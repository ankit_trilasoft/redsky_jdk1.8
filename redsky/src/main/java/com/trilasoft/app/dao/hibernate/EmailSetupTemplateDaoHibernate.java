package com.trilasoft.app.dao.hibernate;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;

import com.trilasoft.app.dao.EmailSetupTemplateDao;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EmailSetupTemplate;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;


public class EmailSetupTemplateDaoHibernate extends GenericDaoHibernate<EmailSetupTemplate, Long> implements EmailSetupTemplateDao{

	public EmailSetupTemplateDaoHibernate() {
		super(EmailSetupTemplate.class);
	}
	
	public List getListByCorpId(String corpId) {
		String query="from EmailSetupTemplate where corpId='"+corpId+"' order by orderNumber desc";
		List list = getHibernateTemplate().find(query);
		return list;	
	}
	public List getListBySearch(String moduleName,String languageName,String descriptionName,String corpId){
		String query="from EmailSetupTemplate where corpId='"+corpId+"' ";
		if(moduleName!=null && !moduleName.equalsIgnoreCase("")){
			query = query + " and module= '"+moduleName+"'";
		}
		if(languageName!=null && !languageName.equalsIgnoreCase("")){
			query = query + " and language= '"+languageName+"'";
		}
		if(descriptionName!=null && !descriptionName.equalsIgnoreCase("")){
			query = query + " and saveAs like '%"+descriptionName+"%'";
		}
		query = query +" order by orderNumber desc";
		List list = getHibernateTemplate().find(query);
		return list;
	}
	public void updateFormsIdInEmailSetupTemplate(String reportsId,long id){
		String query = "update emailsetuptemplate set formsId='"+reportsId+"' where id='"+id+"' ";
	    this.getSession().createSQLQuery(query).executeUpdate();
	}
	public Map<String,String> getTableWithColumnsNameMap(){
		Map<String,String> tableWithColumnNameMap= new LinkedHashMap<String,String>();
		List cfColumnsList = new ArrayList();
		String cfColumns="";
		List soColumnsList = new ArrayList();
		String soColumns="";
		List tsColumnsList = new ArrayList();
		String tsColumns="";
		String query = "select GROUP_CONCAT(column_name SEPARATOR '~') from information_schema.columns where table_schema = 'redsky' and table_name = 'customerfile'";
		cfColumnsList = this.getSession().createSQLQuery(query).list();
		if(cfColumnsList!=null && !cfColumnsList.isEmpty() && cfColumnsList.get(0)!=null){			
			cfColumns = cfColumnsList.toString().replace("[", "").replace("]", "");
    	}
		tableWithColumnNameMap.put("customerfile", cfColumns);
		String query1 = "select GROUP_CONCAT(column_name SEPARATOR '~') from information_schema.columns where table_schema = 'redsky' and table_name = 'serviceorder'";
		soColumnsList = this.getSession().createSQLQuery(query1).list();
		if(soColumnsList!=null && !soColumnsList.isEmpty() && soColumnsList.get(0)!=null){			
			soColumns = soColumnsList.toString().replace("[", "").replace("]", "");
    	}
		tableWithColumnNameMap.put("serviceorder", soColumns);
		String query2 = "select GROUP_CONCAT(column_name SEPARATOR '~') from information_schema.columns where table_schema = 'redsky' and table_name = 'trackingstatus'";
		tsColumnsList = this.getSession().createSQLQuery(query2).list();
		if(tsColumnsList!=null && !tsColumnsList.isEmpty() && tsColumnsList.get(0)!=null){			
			tsColumns = tsColumnsList.toString().replace("[", "").replace("]", "");
    	}
		tableWithColumnNameMap.put("trackingstatus", tsColumns);
		return tableWithColumnNameMap;
	}
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private User userNew;
	private UserManager userManager;
	public List getListByModuleName(String sessionCorpID,String moduleName,String job,String language,String moveType, String companyDivision,String mode,String routing,String serviceType,String fileNumber,Long id){
		List list = new ArrayList();
		List emailTemplateList = new ArrayList();
		if(moduleName.equalsIgnoreCase("CustomerFile")){
			String query="select emailTo,emailFrom,saveAs,emailSubject,id,others from emailsetuptemplate "
						+ " where corpid='"+sessionCorpID+"' and module='"+moduleName+"' and (language='"+language+"' or language is null or language='') and (moveType='"+moveType+"' or moveType is null or moveType='')"
						+ " and (jobType = '"+job+"' or jobType is null or jobType='') and (companyDivision='"+companyDivision+"' or companyDivision is null or companyDivision='') and enable is true order by orderNumber desc";
			list = this.getSession().createSQLQuery(query).list();
		}else if(moduleName.equalsIgnoreCase("ServiceOrder")){
			String query="select emailTo,emailFrom,saveAs,emailSubject,id,others from emailsetuptemplate "
						+ " where corpId='"+sessionCorpID+"' and module='"+moduleName+"' and (language='"+language+"' or language is null or language='') and (moveType='"+moveType+"' or moveType is null or moveType='')"
						+ " and (jobType = '"+job+"' or jobType is null or jobType='') and (companyDivision='"+companyDivision+"' or companyDivision is null or companyDivision='') "
						+ " and (mode='"+mode+"' or mode is null or mode='') and (routing='"+routing+"' or routing is null or routing='') and (service='"+serviceType+"' or service is null or service='') and enable is true order by orderNumber desc";
			list = this.getSession().createSQLQuery(query).list();
		}
		try{
			if(moduleName.equalsIgnoreCase("CustomerFile") && id!=null){
				customerFile = customerFileManager.get(id);
			}else if(moduleName.equalsIgnoreCase("ServiceOrder") && id!=null){
				serviceOrder = serviceOrderManager.get(id);
				trackingStatus = trackingStatusManager.get(id);
				miscellaneous = miscellaneousManager.get(id);
				customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		EmailTemplateDTO dto = null;
		Iterator it = list.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new EmailTemplateDTO();
			
			String emailTo = "";
			String tempEmailToVal="";
			if(row[0]!=null && !row[0].equals("")){
				emailTo = row[0].toString();
				String[] splittedEmailToValue = emailTo.split(",");
					for (String splittedEmailTo : splittedEmailToValue){
						if(splittedEmailTo.contains("^$")){
							splittedEmailTo = splittedEmailTo.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
				        	if(splittedEmailTo.contains(".")){
				        		String [] tableFieldnameArr = splittedEmailTo.split("\\.");
				        		String tableName = tableFieldnameArr[0];
				        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = customerFile.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(customerFile);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = tempFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
														tempEmailToVal = usrFieldValue+"";
													}else{
														tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
				        			}
				        		}
				        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
				        			if(id==null){
				        				tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN - You can,t use ServiceOrder table as module is CustomerFile}</font></strong>";
				        			}else{
					        			String fieldName = tableFieldnameArr[1];
					        			try{
						        			Class<?> cls = serviceOrder.getClass();
						        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
						        			modelFieldName.setAccessible(true);
											Object tempFieldValue =modelFieldName.get(serviceOrder);
											if(tempFieldValue!=null && !tempFieldValue.equals("")){
												if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
													if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
														tempEmailToVal = tempFieldValue+"";
													}else{
														tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
													}
												}else{
													userNew = userManager.getUserByUsername(tempFieldValue+"");
								        			Class<?> cls1 = userNew.getClass();
								        			Field usrModelFieldName = cls1.getDeclaredField("email");
								        			usrModelFieldName.setAccessible(true);
													Object usrFieldValue =usrModelFieldName.get(userNew);
													if(usrFieldValue!=null && !usrFieldValue.equals("")){
														if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
															tempEmailToVal = usrFieldValue+"";
														}else{
															tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
														}
													}
												}
											}
					        			}catch(IllegalAccessException e){
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
					        			} catch (NoSuchFieldException e) {
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
					        			}
				        			}
				        		}
				        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
				        			if(id==null){
				        				tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN - You can,t use TrackingStatus table as module is CustomerFile}</font></strong>";
				        			}else{
					        			String fieldName = tableFieldnameArr[1];
					        			try{
						        			Class<?> cls = trackingStatus.getClass();
						        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
						        			Object fieldType = modelFieldName.getType();
						        			modelFieldName.setAccessible(true);
											Object tempFieldValue =modelFieldName.get(trackingStatus);
											if(tempFieldValue!=null && !tempFieldValue.equals("")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = tempFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
												}
											}
					        			}catch(IllegalAccessException e){
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
					        			} catch (NoSuchFieldException e) {
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
					        			}
				        			}
				        		}
				        	}
						}else{
							if(splittedEmailTo!=null && !splittedEmailTo.equalsIgnoreCase("")){
								if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
									tempEmailToVal  = splittedEmailTo;
								}else{
									tempEmailToVal  = tempEmailToVal+","+splittedEmailTo;
								}
							}
						}
				}
				emailTo = tempEmailToVal;
			}
			dto.setEmailTo(emailTo);
			
			String tempEmailFromVal="";
			String emailFrom = "";
			if(row[1]!=null && !row[1].equals("")){
				emailFrom = row[1].toString();
				String[] splittedEmailFromValue = emailFrom.split(",");
				for (String splittedEmailFrom : splittedEmailFromValue){
					if(splittedEmailFrom.contains("^$")){
						splittedEmailFrom = splittedEmailFrom.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
			        	if(splittedEmailFrom.contains(".")){
			        		String [] tableFieldnameArr = splittedEmailFrom.split("\\.");
			        		String tableName = tableFieldnameArr[0];
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = customerFile.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(customerFile);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = tempFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
													tempEmailFromVal = usrFieldValue+"";
												}else{
													tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
			        			} catch (NoSuchFieldException e) {
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
			        			}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
			        			if(id==null){
			        				tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN - You can,t use ServiceOrder table as module is CustomerFile}</font></strong>";
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = serviceOrder.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(serviceOrder);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
												if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
													tempEmailFromVal = tempFieldValue+"";
												}else{
													tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
														tempEmailFromVal = usrFieldValue+"";
													}else{
														tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
				        			}
				        		}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
			        			if(id==null){
			        				tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN - You can,t use TrackingStatus table as module is CustomerFile}</font></strong>";
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = trackingStatus.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			Object fieldType = modelFieldName.getType();
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(trackingStatus);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = tempFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal = "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>";
				        			}
				        		}
			        		}
			        	}
					}else{
						if(splittedEmailFrom!=null && !splittedEmailFrom.equalsIgnoreCase("")){
							if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
								tempEmailFromVal  = splittedEmailFrom;
							}else{
								tempEmailFromVal  = tempEmailFromVal+","+splittedEmailFrom;
							}
						}
					}
				}
				emailFrom = tempEmailFromVal;
			}
			dto.setEmailFrom(emailFrom);
			dto.setSaveAs(row[2]);
			if(row[2]!=null && !row[2].equals("")){
				try {
					String query="select emailStatus from emailsetup where corpId='"+sessionCorpID+"' and fileNumber='"+fileNumber+"' and module='"+moduleName+"' and saveas='"+row[2].toString()+"' order by id desc";
					List l1= this.getSession().createSQLQuery(query).list();
					if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
						dto.setEmailStatus(l1.get(0).toString().replace("[", "").replace("]", ""));	
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
			
			String subject = "";
			String subjectTemp = "";
			String subjectEmptyTokenizer="";
			StringBuilder emailSubjectValue = new StringBuilder();
			if(row[3]!=null && !row[3].equals("")){
				subjectTemp = row[3].toString();
				if(subjectTemp.contains("^$")){
					StringTokenizer subjectTokenizer = new StringTokenizer(subjectTemp);
			        while (subjectTokenizer.hasMoreTokens()) {
			        	subjectEmptyTokenizer = subjectTokenizer.nextToken();
			        	if(subjectEmptyTokenizer.contains("^$")){
			        		String[] splittedValue = subjectEmptyTokenizer.split(Pattern.quote("^$"));
							for (String splittedWord : splittedValue){
								if(splittedWord.contains("$^") && splittedWord.contains(".")){
									String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
									  for (String splittedWordNew : splittedValueNew){
											String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
								        	if(tempTableAndFieldname.contains(".") && (tempTableAndFieldname.toLowerCase().contains("customerfile") || tempTableAndFieldname.toLowerCase().contains("serviceorder") || tempTableAndFieldname.toLowerCase().contains("trackingstatus"))){
								        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
								        		String tableName = tableFieldnameArr[0];
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = customerFile.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			Object fieldType = modelFieldName.getType();
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(customerFile);
														if(tempFieldValue!=null && !tempFieldValue.equals("")){
															if(fieldType.toString().equals("class java.util.Date")){
																SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																Date tempDateValue = (Date) tempFieldValue;
													    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
													    		String dateValue = nowYYYYMMDD1.toString();
													    		emailSubjectValue.append(dateValue+" ");
															}else{
																emailSubjectValue.append(tempFieldValue+" ");
															}
														}
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
								        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>");
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
								        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>");
								        			}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
								        			if(id==null){
								        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN - You can,t use ServiceOrder table as module is CustomerFile}</font></strong>" );
								        			}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = serviceOrder.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(serviceOrder);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
														    		emailSubjectValue.append(dateValue+" ");
																}else{
																	emailSubjectValue.append(tempFieldValue+" ");
																}
															}
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>");
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>");
									        			}
								        			}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
								        			if(id==null){
								        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN - You can,t use TrackingStatus table as module is CustomerFile}</font></strong>" );
								        			}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = trackingStatus.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(trackingStatus);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
														    		emailSubjectValue.append(dateValue+" ");
																}else{
																	emailSubjectValue.append(tempFieldValue+" ");
																}
															}
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>");
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				emailSubjectValue.append( "<strong><font color=#ff0000>{ErrorIN "+errorField+" in "+tableName+"}</font></strong>");
									        			}
								        			}
								        		}
								        	}else{
								        		emailSubjectValue.append(tempTableAndFieldname+" ");
								        	}
								  		}
									}else{
										if(splittedWord!=null && !splittedWord.equalsIgnoreCase("")){
											emailSubjectValue.append(splittedWord+" ");
										}
									}
								}
							}else{
					        	emailSubjectValue.append(subjectEmptyTokenizer+" ");
					        }
			        	}
				}else{
					emailSubjectValue.append(subjectTemp+" ");
				}
			subject = emailSubjectValue.toString();
			}
			dto.setEmailSubject(subject);
			dto.setId(row[4]);
			if(row[5]!=null && !row[5].equals("")){
				try {
					String query="select * from "+moduleName.toLowerCase()+" "+ moduleName.toLowerCase();
					if(moduleName.equalsIgnoreCase("CustomerFile")){
						query = query + " left outer join serviceorder on customerfile.id=serviceorder.customerFileId ";
						if(row[5].toString().toLowerCase().contains("billing")){
							query = query + " left outer join billing on serviceorder.id=billing.id ";
						}
						if(row[5].toString().toLowerCase().contains("trackingstatus")){
							query = query + " left outer join trackingstatus on serviceorder.id=trackingstatus.id ";
						}
						if(row[5].toString().toLowerCase().contains("miscellaneous")){
							query = query + " left outer join miscellaneous on serviceorder.id=miscellaneous.id ";
						}
					}
					if(moduleName.equalsIgnoreCase("ServiceOrder")){
						query = query + " inner join customerfile on serviceorder.customerFileId=customerfile.id ";
						if(row[5].toString().toLowerCase().contains("billing")){
							query = query + " inner join billing on serviceorder.id=billing.id ";
						}
						if(row[5].toString().toLowerCase().contains("trackingstatus")){
							query = query + " inner join trackingstatus on serviceorder.id=trackingstatus.id ";
						}
						if(row[5].toString().toLowerCase().contains("miscellaneous")){
							query = query + " inner join miscellaneous on serviceorder.id=miscellaneous.id ";
						}
					}
					
					query = query + " where "+moduleName.toLowerCase()+".corpID='"+sessionCorpID+"' and "+row[5].toString().toLowerCase()+" and "+moduleName.toLowerCase()+".id="+id;
					List al= this.getSession().createSQLQuery(query).list();
					if(al!=null && !al.isEmpty() && al.get(0)!=null){
						emailTemplateList.add(dto);	
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}else{
				emailTemplateList.add(dto);
			}
		}
		return emailTemplateList;
	}
	
	public int getMaxOrderNumber(String sessionCorpID){
		int i=0;
			String query="select max(orderNumber) from emailsetuptemplate where corpid='"+sessionCorpID+"'";
			List maxNumber= this.getSession().createSQLQuery(query).list();
			if(maxNumber!=null && !(maxNumber.isEmpty()) && maxNumber.get(0)!=null){
				i=Integer.parseInt(maxNumber.get(0).toString());
			}
		return i;
	}
	
	public String findDocsFromFileCabinet(String fileType, String fileId,String sessionCorpID){
		String docLocation = "";
		try {
			String query = "select GROUP_CONCAT(location SEPARATOR '~') from myfile WHERE active is true AND fileId='"+fileId+"' and corpID='"+sessionCorpID+"' and fileType='"+fileType+"'";
			List docList = this.getSession().createSQLQuery(query).list();
			if((docList!=null && !docList.isEmpty()) && docList.get(0)!=null){
				docLocation = docList.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
	    	e.printStackTrace();
		}
		return docLocation;
	}
	
	public List findAllDocsFromFileCabinet(String fileType,String fileId,String sessionCorpID){
		List docList = new ArrayList();
		try {
			// and fileType!='"+fileType+"' -----------> If required , just need to append in query.
			String query = "select id,location,filefilename,filetype,description,filesize,updatedby,updatedon from myfile WHERE active is true AND fileId='"+fileId+"' and corpID='"+sessionCorpID+"' ";
			List docResultList = this.getSession().createSQLQuery(query).list();
			FileCabinetDTO dto = null;
			Iterator it = docResultList.iterator();
			while(it.hasNext()){
				Object []row= (Object [])it.next();
				dto = new FileCabinetDTO();
				dto.setFileId(row[0]);
				dto.setFileLocation(row[1]);
				dto.setFileFileName(row[2]);
				dto.setFileType(row[3]);
				dto.setFileDescription(row[4]);
				dto.setFileSize(row[5]);
				dto.setUpdatedBy(row[6]);
				dto.setUpdatedOn(row[7]);
				
				docList.add(dto);
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
	    	e.printStackTrace();
		}
		return docList;
	}
	
	public String testEmailSetupTestConditionQuery(String moduleFieldValue,String othersFieldValue,String sessionCorpID){
		String message = "";
			try {
				String query="select * from "+moduleFieldValue.toLowerCase()+" "+ moduleFieldValue.toLowerCase();
				if(moduleFieldValue.equalsIgnoreCase("CustomerFile")){
					query = query + " left outer join serviceorder on customerfile.id=serviceorder.customerFileId ";
					if(othersFieldValue.toLowerCase().contains("billing")){
						query = query + " left outer join billing on serviceorder.id=billing.id ";
					}
					if(othersFieldValue.toLowerCase().contains("trackingstatus")){
						query = query + " left outer join trackingstatus on serviceorder.id=trackingstatus.id ";
					}
					if(othersFieldValue.toLowerCase().contains("miscellaneous")){
						query = query + " left outer join miscellaneous on serviceorder.id=miscellaneous.id ";
					}
				}
				if(moduleFieldValue.equalsIgnoreCase("ServiceOrder")){
					query = query + " inner join customerfile on serviceorder.customerFileId=customerfile.id ";
					if(othersFieldValue.toLowerCase().contains("billing")){
						query = query + " inner join billing on serviceorder.id=billing.id ";
					}
					if(othersFieldValue.toLowerCase().contains("trackingstatus")){
						query = query + " inner join trackingstatus on serviceorder.id=trackingstatus.id ";
					}
					if(othersFieldValue.toLowerCase().contains("miscellaneous")){
						query = query + " inner join miscellaneous on serviceorder.id=miscellaneous.id ";
					}
				}
				query = query + " where "+moduleFieldValue.toLowerCase()+".corpID='"+sessionCorpID+"' and "+moduleFieldValue.toLowerCase()+".id='1' and "+othersFieldValue.toLowerCase();
				Long StartTime = System.currentTimeMillis();
				List list = this.getSession().createSQLQuery(query).list();
				Long timeTaken = System.currentTimeMillis() - StartTime;
				System.out.println("Time To Execute EmailSetupTemplate Test Condition Query in milisecond ---> "+ timeTaken+ "\n" +"Query is ---> " +query);
				message = "Tested";
			} catch (Exception e) {
				//message = e.getMessage();
				e.printStackTrace();
				message = "Error";
			}
		
		return message;
	}
	
	public List findLastSentMail(String sessionCorpID,String moduleName,String fileNumber,String saveAsVal){
		List mailList = new ArrayList();
		String query = "from EmailSetup where corpid='"+sessionCorpID+"' and filenumber='"+fileNumber+"' and module='"+moduleName+"' and saveAs = '"+saveAsVal+"' and (lastSentMailBody is not null and lastSentMailBody!='') order by id desc limit 1 ";
		return getHibernateTemplate().find(query);
	}
	
	public class EmailTemplateDTO{
		private Object id;
		private Object emailTo;
		private Object emailFrom;
		private Object saveAs;
		private Object emailSubject;
		private Object emailStatus;
		public Object getEmailTo() {
			return emailTo;
		}
		public void setEmailTo(Object emailTo) {
			this.emailTo = emailTo;
		}
		public Object getEmailFrom() {
			return emailFrom;
		}
		public void setEmailFrom(Object emailFrom) {
			this.emailFrom = emailFrom;
		}
		public Object getSaveAs() {
			return saveAs;
		}
		public void setSaveAs(Object saveAs) {
			this.saveAs = saveAs;
		}
		public Object getEmailSubject() {
			return emailSubject;
		}
		public void setEmailSubject(Object emailSubject) {
			this.emailSubject = emailSubject;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getEmailStatus() {
			return emailStatus;
		}
		public void setEmailStatus(Object emailStatus) {
			this.emailStatus = emailStatus;
		}
	}
	
	public class FileCabinetDTO{
		private Object fileId;
		private Object fileLocation;
		private Object fileFileName;
		private Object fileType;
		private Object fileDescription;
		private Object fileSize;
		private Object updatedBy;
		private Object updatedOn;
		public Object getFileId() {
			return fileId;
		}
		public void setFileId(Object fileId) {
			this.fileId = fileId;
		}
		public Object getFileLocation() {
			return fileLocation;
		}
		public void setFileLocation(Object fileLocation) {
			this.fileLocation = fileLocation;
		}
		public Object getFileFileName() {
			return fileFileName;
		}
		public void setFileFileName(Object fileFileName) {
			this.fileFileName = fileFileName;
		}
		public Object getFileType() {
			return fileType;
		}
		public void setFileType(Object fileType) {
			this.fileType = fileType;
		}
		public Object getFileDescription() {
			return fileDescription;
		}
		public void setFileDescription(Object fileDescription) {
			this.fileDescription = fileDescription;
		}
		public Object getFileSize() {
			return fileSize;
		}
		public void setFileSize(Object fileSize) {
			this.fileSize = fileSize;
		}
		public Object getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public User getUserNew() {
		return userNew;
	}

	public void setUserNew(User userNew) {
		this.userNew = userNew;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
}
