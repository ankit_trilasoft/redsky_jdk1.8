package com.trilasoft.app.dao.hibernate;


import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.GLCommissionTypeDao;

import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.GLCommissionType;

import com.trilasoft.app.model.VanLineGLType;

public class GLCommissionTypeDaoHibernate extends GenericDaoHibernate<GLCommissionType, Long> implements GLCommissionTypeDao {

	private HibernateUtil hibernateUtil;
    
	 
	public GLCommissionTypeDaoHibernate() {   
        super(GLCommissionType.class);   
    }
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original;
		}
		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());
		return partBefore + replacement + partAfter;
	}

	
	public List<GLCommissionType> gLCommissionTypeList(String contract, String charge){
	  		List glCommList = null;
        String columns = "id, charge, glType, code, type, contract ";
        glCommList = getHibernateTemplate().find("select " + columns + " from GLCommissionType where contract = '" + contract + "' AND charge= '" +charge+ "'");
        try {
        	glCommList = hibernateUtil.convertScalarToComponent(columns, glCommList, GLCommissionType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return glCommList;
		
	}
	public List<GLCommissionType> glList(String gl){
		
		return getHibernateTemplate().find("select distinct type from VanLineCommissionType where glCode = '"+gl+"'");
	}
    
	


	
	/*public List<VanLineGLType> searchVanLineGLType(String glType, String description, String glCode){
		List vanLineGLTypeList = null;
        String columns = "id,glType, description, glCode, discrepancy, shortHaul, bucket";
        vanLineGLTypeList = getHibernateTemplate().find("select " + columns + " from VanLineGLType where glType like '" + glType.replaceAll("'", "''") + "%' AND description like '" +description.replaceAll("'", "''") + "%' AND glCode like '" +glCode.replaceAll("'", "''") + "%'");
        try {
        	vanLineGLTypeList = hibernateUtil.convertScalarToComponent(columns, vanLineGLTypeList, VanLineGLType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return vanLineGLTypeList;
		
	}*/

}
