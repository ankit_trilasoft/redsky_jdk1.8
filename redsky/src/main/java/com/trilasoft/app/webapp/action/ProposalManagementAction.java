package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.ProposalManagement;
import com.trilasoft.app.service.ProposalManagementManager;
import com.trilasoft.app.service.RefMasterManager;

import org.appfuse.model.User;
import org.appfuse.service.GenericManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;

public class ProposalManagementAction extends BaseAction{
	 /**
	 * 
	 */
	//private static final long serialVersionUID = 3631502569872807639L;
	private ProposalManagementManager proposalManagementManager;
	public ProposalManagementManager getProposalManagementManager() {
		return proposalManagementManager;
	}

	public void setProposalManagementManager(ProposalManagementManager proposalManagementManager) {
		this.proposalManagementManager = proposalManagementManager;
	}

	private RefMasterManager refMasterManager;
    public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	private List proposalManagements;
    private List distinctCorpId;
    private List ls;
    private ProposalManagement proposalManagement;
    private ProposalManagement addProposalManagementForm;
    private Long id;
    private Long cid;
    private String corpId;
    private String sessionCorpID;
    private String subject;
    private String description;
    private String module;
    private String clientRequestor;
    private String clientApprover;
    private Boolean isFixed ;
    private BigDecimal amountProposed;
    private String projectManager;
    private Date modifyOn;
    private String createdBy;
    private String modifyBy;
    private Date createdOn;
    private ProposalManagement addCopyProposalManagement;
    private String fileLocation;
    private String file1Location;
    private String fileFileName;
    private String file1FileName;
    private String approvalFileName;
    private File file;
    private File file1;
    private String approvalFileLocation;
    private String location1;
    private String fileFileName1;
    private String location2;
    private String fileFileName2;
    private String urlValue;
    private String ticketNo;
    private Date initiationDate;
    private Date approvalDate;
    private Date approvalStart;
    private Date approvalEnd;
    private String autoPopulateDate;
    
    private String currency;
    private String fileLocationApproval;
    private Map<String,String> baseCurrency;
    private Map<String, String> moduleReport;
    private Map<String, String> statusReport;
    private Boolean activeStatus=true;
    static final Logger logger = Logger.getLogger(ProposalManagementAction.class);
    Date currentdate = new Date();
    
    public ProposalManagementAction(){
 	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
     User user = (User)auth.getPrincipal();
     this.sessionCorpID = user.getCorpID();
	}
 
    public String getComboList(String corpId) {
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	       	
 	moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE");
 	statusReport=refMasterManager.findByParameter(sessionCorpID, "RSPROPOSALSTATUS");
 	distinctCorpId=proposalManagementManager.findDistinct();
 	baseCurrency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
 	return SUCCESS;
 	}
    
    public String distinctCorpId(){
        distinctCorpId=proposalManagementManager.findDistinct();
    	return SUCCESS;
    }
    @SkipValidation
    public String list() {
    	getComboList(sessionCorpID);
    	proposalManagements = proposalManagementManager.listOnLoad();
    	return SUCCESS;
    }
    public String delete() {
    	proposalManagementManager.remove(proposalManagement.getId());
        saveMessage(getText("proposalManagement.deleted"));
        getComboList(sessionCorpID);
        return SUCCESS;
    }
    public String edit() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	distinctCorpId();
        if (id != null) {
        	proposalManagement = proposalManagementManager.get(id);
        	fileLocation = proposalManagement.getLocation();
        	fileLocationApproval=proposalManagement.getApprovalFileLocation();
        } else {
        	proposalManagement = new ProposalManagement();
        	proposalManagement.setCreatedOn(new Date());
        	proposalManagement.setModifyOn(new Date());
        }
        getComboList(sessionCorpID);
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
       
    @SkipValidation
    public String deleteProposalDoc() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	proposalManagementManager.remove(id);
    	saveMessage(getText("proposalManagementDetail.deleted"));
    	 	 urlValue="true&corpID=${sessionCorpID}";
    	   list();
    	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 return SUCCESS;
    	}
    public String save() throws Exception {
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
        if (cancel != null) {
            return "cancel";
        }

        if (delete != null) {
            return delete();
        }
        distinctCorpId();
        getComboList(sessionCorpID);
        boolean isNew = (proposalManagement.getId() == null);
        if(isNew){
        	proposalManagement.setCreatedOn(new Date());
        }
        
        String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "ProposalManagement" +  "/");
		File dirPath = new File(uploadDir);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		Long kid;
		{
			kid = 1L;
		}
		try{		
			InputStream stream = new FileInputStream(file);
			//write the file to the file specified
			OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
         	InputStream copyStream = new FileInputStream(file);
			OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
			int bytesReadCopy = 0;
			byte[] bufferCopy = new byte[8192];
			while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
				boscopy.write(bufferCopy, 0, bytesReadCopy);
			}
			boscopy.close();
			copyStream.close();
			getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
			proposalManagement.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			} 
			if(fileFileName!=null && !fileFileName.equalsIgnoreCase("")){
				proposalManagement.setFileName(proposalManagement.getFileName());
				proposalManagement.setFileName(fileFileName);
			}else{
			proposalManagement.setFileName(fileFileName1);
			proposalManagement.setLocation(location1);
			}
			
			/***Approval File Upload**/
			String uploadDir1 = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir1 = uploadDir1.replace(uploadDir1, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "ProposalManagement" +  "/");
			File dirPath1 = new File(uploadDir1);
			if (!dirPath1.exists()) {
				dirPath1.mkdirs();
			}
			Long kaid;
			{
				kaid = 1L;
			}
			
			try{					
				InputStream stream = new FileInputStream(file1);

				//write the file to the file specified
				OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + file1FileName);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}
				bos.close();
				stream.close();
	         	InputStream copyStream = new FileInputStream(file1);
				OutputStream boscopy = new FileOutputStream(uploadDir + file1FileName);
				int bytesReadCopy = 0;
				byte[] bufferCopy = new byte[8192];
				while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
					boscopy.write(bufferCopy, 0, bytesReadCopy);
				}
				boscopy.close();
				copyStream.close();
				getRequest().setAttribute("approvalFileLocation", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
				proposalManagement.setApprovalFileLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + file1FileName);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			if(file1FileName!=null && !file1FileName.equalsIgnoreCase(""))
			{
				proposalManagement.setApprovalFileName(proposalManagement.getApprovalFileName());
				proposalManagement.setApprovalFileName(file1FileName);
			}else{
			proposalManagement.setApprovalFileName(fileFileName2);
			proposalManagement.setApprovalFileLocation(location2);
			}
			/**Approvl File close**/
   // }
		proposalManagement.setModifyOn(new Date());
        proposalManagement = proposalManagementManager.save(proposalManagement);
        String key = (isNew) ? "proposalManagement.added" : "proposalManagement.updated";
        saveMessage(getText(key));
       String str=list();
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       return str;
    }
    
    @SkipValidation
    public String addAndCopyProposalMangForm() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id == null) {
					getComboList(sessionCorpID);
					
					proposalManagement = new ProposalManagement();
					addCopyProposalManagement = (ProposalManagement) proposalManagementManager.get(cid);
					proposalManagement.setCorpId(addCopyProposalManagement.getCorpId());
					proposalManagement.setModule(addCopyProposalManagement.getModule());
					proposalManagement.setClientRequestor(addCopyProposalManagement.getClientRequestor());
					proposalManagement.setClientApprover(addCopyProposalManagement.getClientApprover());
					proposalManagement.setProjectManager(addCopyProposalManagement.getProjectManager());
					proposalManagement.setSubject(addCopyProposalManagement.getSubject());
					proposalManagement.setDescription(addCopyProposalManagement.getDescription());
					proposalManagement.setInitiationDate(addCopyProposalManagement.getInitiationDate());
					proposalManagement.setApprovalDate(addCopyProposalManagement.getApprovalDate());
					proposalManagement.setStatus(addCopyProposalManagement.getStatus());
					proposalManagement.setCurrency(addCopyProposalManagement.getCurrency());
					proposalManagement.setTicketNo(addCopyProposalManagement.getTicketNo());
					proposalManagement.setAmountProposed(addCopyProposalManagement.getAmountProposed());
					proposalManagement.setFileName(addCopyProposalManagement.getFileName());
					proposalManagement.setLocation(addCopyProposalManagement.getLocation());
					proposalManagement.setApprovalFileName(addCopyProposalManagement.getApprovalFileName());
					proposalManagement.setApprovalFileLocation(addCopyProposalManagement.getApprovalFileLocation());
					proposalManagement.setCreatedBy(addCopyProposalManagement.getCreatedBy());
					proposalManagement.setCreatedOn(new Date());
					proposalManagement.setModifyBy(addCopyProposalManagement.getModifyBy());
					proposalManagement.setModifyOn(new Date());
					proposalManagement.setIsFixed(addCopyProposalManagement.getIsFixed());
					proposalManagement.setBillTillDate(addCopyProposalManagement.getBillTillDate());
					distinctCorpId();
					list();
				//save();
					
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
    }
//  Method to search module
    @SkipValidation
    public String searchProposalManagement() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	autoPopulateDate="yes";
    	getComboList(sessionCorpID);
    	if(activeStatus){
    	proposalManagements = proposalManagementManager.findListByModuleCorpStatus(proposalManagement.getModule(),proposalManagement.getCorpId(),proposalManagement.getStatus(),proposalManagement.getApprovalDate(),proposalManagement.getInitiationDate(),proposalManagement.getTicketNo(),activeStatus);
    	}else{
        proposalManagements = proposalManagementManager.findListByModuleCorpStatus(proposalManagement.getModule(),proposalManagement.getCorpId(),proposalManagement.getStatus(),proposalManagement.getApprovalDate(),proposalManagement.getInitiationDate(),proposalManagement.getTicketNo(),activeStatus);
    	}
    	setCorpId(proposalManagement.getCorpId());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
	}
    private String proposalDesc;
	@SkipValidation
	public String findToolTipDescriptionForProposal(){		
		proposalDesc = proposalManagementManager.findProposalDescForToolTip(id,sessionCorpID);	
		return SUCCESS;
	}
	
	@SkipValidation
	public String autoSaveProposalManagementAjax(){
		String fieldName=getRequest().getParameter("fieldName");
		String fieldVal=getRequest().getParameter("fieldVal");
		if(fieldVal==null || fieldVal.toString().trim().equalsIgnoreCase("")){
			fieldVal=null;
		}
		String fieldId=getRequest().getParameter("fieldId");
		if(fieldId!=null){
			Long serviceId=Long.parseLong(fieldId);
			proposalManagementManager.autoSaveProposalManagementAjax(fieldName,fieldVal,serviceId);
		}
		return SUCCESS;
	}
	
	public List getProposalManagements() {
		return proposalManagements;
	}

	public List getDistinctCorpId() {
		return distinctCorpId;
	}

	public ProposalManagement getProposalManagement() {
		return proposalManagement;
	}

	public Long getId() {
		return id;
	}

	public String getCorpId() {
		return corpId;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public String getSubject() {
		return subject;
	}

	public String getDescription() {
		return description;
	}

	public String getModule() {
		return module;
	}

	public String getClientRequestor() {
		return clientRequestor;
	}

	public String getClientApprover() {
		return clientApprover;
	}

	public Boolean getIsFixed() {
		return isFixed;
	}

	public BigDecimal getAmountProposed() {
		return amountProposed;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public Date getModifyOn() {
		return modifyOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public Map<String, String> getModuleReport() {
		return moduleReport;
	}

	public Map<String, String> getStatusReport() {
		return statusReport;
	}

	public void setProposalManagements(List proposalManagements) {
		this.proposalManagements = proposalManagements;
	}

	public void setDistinctCorpId(List distinctCorpId) {
		this.distinctCorpId = distinctCorpId;
	}

	public void setProposalManagement(ProposalManagement proposalManagement) {
		this.proposalManagement = proposalManagement;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public void setClientRequestor(String clientRequestor) {
		this.clientRequestor = clientRequestor;
	}

	public void setClientApprover(String clientApprover) {
		this.clientApprover = clientApprover;
	}

	public void setIsFixed(Boolean isFixed) {
		this.isFixed = isFixed;
	}

	public void setAmountProposed(BigDecimal amountProposed) {
		this.amountProposed = amountProposed;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public void setModuleReport(Map<String, String> moduleReport) {
		this.moduleReport = moduleReport;
	}

	public void setStatusReport(Map<String, String> statusReport) {
		this.statusReport = statusReport;
	}

	public String getUrlValue() {
		return urlValue;
	}

	public void setUrlValue(String urlValue) {
		this.urlValue = urlValue;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public String getLocation1() {
		return location1;
	}

	public String getFileFileName1() {
		return fileFileName1;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	public void setFileFileName1(String fileFileName1) {
		this.fileFileName1 = fileFileName1;
	}

	public Date getInitiationDate() {
		return initiationDate;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setInitiationDate(Date initiationDate) {
		this.initiationDate = initiationDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Map<String, String> getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Map<String, String> baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getProposalDesc() {
		return proposalDesc;
	}

	public void setProposalDesc(String proposalDesc) {
		this.proposalDesc = proposalDesc;
	}

	public File getFile1() {
		return file1;
	}

	public void setFile1(File file1) {
		this.file1 = file1;
	}

	public String getFile1Location() {
		return file1Location;
	}

	public String getFile1FileName() {
		return file1FileName;
	}

	public String getApprovalFileName() {
		return approvalFileName;
	}

	public String getLocation2() {
		return location2;
	}

	public String getFileFileName2() {
		return fileFileName2;
	}

	public void setFile1Location(String file1Location) {
		this.file1Location = file1Location;
	}

	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	public void setApprovalFileName(String approvalFileName) {
		this.approvalFileName = approvalFileName;
	}

	public void setLocation2(String location2) {
		this.location2 = location2;
	}

	public void setFileFileName2(String fileFileName2) {
		this.fileFileName2 = fileFileName2;
	}

	public String getApprovalFileLocation() {
		return approvalFileLocation;
	}

	public void setApprovalFileLocation(String approvalFileLocation) {
		this.approvalFileLocation = approvalFileLocation;
	}

	public String getFileLocationApproval() {
		return fileLocationApproval;
	}

	public void setFileLocationApproval(String fileLocationApproval) {
		this.fileLocationApproval = fileLocationApproval;
	}

	public Date getApprovalStart() {
		return approvalStart;
	}

	public void setApprovalStart(Date approvalStart) {
		this.approvalStart = approvalStart;
	}

	public Date getApprovalEnd() {
		return approvalEnd;
	}

	public void setApprovalEnd(Date approvalEnd) {
		this.approvalEnd = approvalEnd;
	}

	public String getAutoPopulateDate() {
		return autoPopulateDate;
	}

	public void setAutoPopulateDate(String autoPopulateDate) {
		this.autoPopulateDate = autoPopulateDate;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

}
