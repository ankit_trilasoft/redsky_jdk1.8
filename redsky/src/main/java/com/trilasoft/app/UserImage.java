package com.trilasoft.app;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class for Servlet: UserImage
 *
 */
 public class UserImage extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	     
		 private String imageURL;
		 private String contentType = "image/jpeg";
		 
		public UserImage() {
			super();
		}   	
		
		public void doGet(HttpServletRequest request, HttpServletResponse response) 
		  throws ServletException, IOException {  
			ServletOutputStream out = response.getOutputStream();
			
			imageURL = request.getParameter("location");
			
			byte[] imageData = getImageBytes( imageURL );
			response.setContentType(contentType);
			response.setContentLength(imageData.length);
			out.write(imageData);		

	}
		
	    private byte[] getImageBytes(String imageFileName)
	    {
	       FileInputStream in = null;
	       try {
	          in = new FileInputStream(imageFileName);
	          ByteArrayOutputStream out = new ByteArrayOutputStream();
	          byte[] buf = new byte[10240];
	          int len = 0;
	          while((len = in.read(buf)) > 0) out.write(buf, 0, len);
	          return out.toByteArray();
	       } catch(Throwable t) {
	          throw new RuntimeException(t);
	       } finally {
	          if(in != null) try { in.close(); } catch(Throwable t) {}
	       }
	    }	
		
}